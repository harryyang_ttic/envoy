// --------------------------------------------------------------
/** @file StrUtils.h
 **
 ** @author Ingmar Posner
 ** @date 14/3/2011
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#ifndef __LEGET_STRUTILS_H__
#define __LEGET_STRUTILS_H__

#include <string>

namespace leget
{

    // strip characters
    bool lStrip(std::string& rStr, const std::string& rToken=" ");
    bool rStrip(std::string& rStr, const std::string& rToken=" ");
    bool strip(std::string& rStr, const std::string& rToken=" ");

    
    // split string at token
    bool split(const std::string& rIn, std::string& rBefore, std::string& rAfter, const std::string& rToken=" ");
    // reverse split: split at token but start from the end
    bool rSplit(const std::string& rIn, std::string& rBefore, std::string& rAfter, const std::string& rToken=" ");
    
    // get a token:value pair
    bool parseNameValuePair(std::string& rIn, std::string& rName, std::string& rValue, const std::string& rToken=":");

    
    // replace all occurances of a given char with another
    void replaceChars(std::string& rIn, const std::string& rCharToReplace, const std::string& rReplacement);
    
    // throw out everything that is not alpha-numeric
    void filterForAlphaNumeric(const std::string rIn, 
                               std::string& rOut, 
                               const char& rRep=' ');
    

}


#endif // __LEGET_STRUTILS_H__

