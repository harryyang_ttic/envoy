 //This script collect laser_annotation_t messages, compute feature values and write to files
//Requires: er-laser-annotation(laser_annotation_t messages)
//Output: A "data.txt" text file contains feature values


#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
//#include <image_io_utils/image_io_utils.hpp>
#include <image_utils/jpeg.h>

//#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>
#include <lcmtypes/perception_lcmtypes.h>
//#include <lcmtypes/perception_place_classification_class_t.h>

#include <image-features/image_feature_extractor.hpp>
#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <CSystem.h>
#include <CImage.h>
#include <CCrfh.h> 
#include <QString>

int get_class_id(char *annotation){
    if(!strcmp(annotation, "room")){
        return 0;
    }
    else if(!strcmp(annotation, "hallway")){
        return 1;
    }
    else if(!strcmp(annotation, "openarea")){
        return 2;
    }
}

typedef struct
{
    lcm_t      *lcm;
    GMainLoop *mainloop;
    perception_image_annotation_t *last_image_annotation;
    int verbose;
    char *file_name;
    FILE *file;
    GHashTable *class_count;
    int skip_doorways;
    image_feature_extractor_t *extractor;
    //CSystem *crfh_sys;
    //double minHistValue;
} image_annotation;

typedef struct
{
  int *id;
  int annotation_count;
  int feature_count;
} counts;

void print_stat(gpointer key,
                  gpointer value,
                  gpointer user_data){
    char *class_name = NULL;
    int class_id = *(int *) key;

    if(class_id == 0){
        class_name = "room";
    }
    else if(class_id == 1){
        class_name = "hallway";
    }
    else if(class_id == 2){
        class_name = "openarea";
    }
    else{        
        class_name = "unknown";
    }
 
    fprintf(stderr, "Class Name [%d] : %s Count : %d\n", *(int *) key, class_name, *(int *) value);
}

void print_stats(image_annotation *self){
    //GHashTableIter iter;
    //gpointer key, value;
    
    //g_hash_table_iter_init (&iter, self->class_count);
    g_hash_table_foreach(self->class_count, (GHFunc) print_stat, NULL);
    
}

CImage *
decode_image(const bot_core_image_t &img)//, CImage &c_image)
{
    CImage *c_img = NULL;
    switch (img.pixelformat) {
    case BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY:
        fprintf(stderr, "Image is gray\n");
        c_img = new CImage((char *) img.data, img.width, img.height, IT_BYTE);
        break;
    case BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB:
        c_img = new CImage((char *) img.data, img.width, img.height, IT_RGB24);
        break;
    case BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG:
      {
	int num_channels = 3;
      c_img = new CImage(img.width, img.height, IT_RGB24);
      uint8_t *data = (uint8_t *)calloc(img.height * img.width * num_channels, sizeof(uint8_t));
      jpeg_decompress_8u_rgb(img.data,
			     img.size,
			     data, 
			     img.width,
			     img.height,
			     img.width * num_channels);
      c_img->createFromBuffer((char *) data, img.width, img.height,IT_RGB24);
      free(data);
      break;
      }
    default:
        fprintf(stderr, "Unrecognized image format\n");
        break;
    }
    return c_img;
    return NULL;
}

static void
on_image_annotation (const lcm_recv_buf_t *rbuf, const char *channel,
                     const perception_image_annotation_t *msg, void *user){

//    fprintf(stderr, "Receive a log annotation\n");
    FILE *file;
    static int count = 0;
    count++;
    fprintf(stderr, "Count: %d\n", count);
    image_annotation *self= (image_annotation *)user;
    file = self->file;//fopen(self->file_name, "a");

    if(file == NULL){
        fprintf(stderr, "Error : File is NULL\n");
        return;
    }

    if(self->last_image_annotation != NULL){
	perception_image_annotation_t_destroy(self->last_image_annotation);
    }

    self->last_image_annotation = perception_image_annotation_t_copy(msg);
    bot_core_image_t image = self->last_image_annotation->image;
    char *annotation = self->last_image_annotation->annotation;
    int label = get_class_id(annotation);

    fprintf(stderr, "Annotation : %s - Label : %d\n", annotation, label);
    
    if(label < 0){
        fprintf(stderr, "Skipping\n");
    }
    
    GHashTableIter iter;
    gpointer key, value;
    
    int *class_count = (int *) g_hash_table_lookup(self->class_count, &label);
    fprintf(stderr, "label: %d\n", label);
    if(!class_count){
        class_count = (int *)calloc(1,sizeof(int));
        int *id = (int *)calloc(1,sizeof(int));
        *id = label;
	fprintf(stderr, "Annotation: %s\n", annotation);
	fprintf(stderr, "Class count not found. New ID: %d\n", *id);
	fprintf(stderr, "Label: %d\n", label);
        *class_count = 0;
        g_hash_table_insert(self->class_count, id, class_count);
    }
    else{
        *class_count = *class_count+1;
	fprintf(stderr, "Class count: %d\n", *class_count);
    }
    
    CSvmNode *svm_features = calculate_image_features(self->extractor, &image);

    if(svm_features==NULL){
      fprintf(stderr, "Feature calculation is null.\n");
      return;
    }

    fprintf(file, "%d ", label);
    int i=0;
    while(1){
      if(svm_features[i].index == -1) 
            break;
      //fprintf(stderr, "\t[%d] : %f\n", svm_features[i].index , svm_features[i].value);
      fprintf(file, "%d:%f ", svm_features[i].index , svm_features[i].value);
      i++;
    }

    fprintf(file, "\n");
    
    delete svm_features;
}

void subscribe_to_channels(image_annotation *self)
{
    perception_image_annotation_t_subscribe(self->lcm, "IMAGE_ANNOTATION", on_image_annotation, self);
}  

static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -l LOG             Specify Log File to save training data\n"
             "  -i input_lcm_file  (optinal) Specify LCM log file to read annotated laser data from\n", 
             "  -d                 Include Doorways\n", 
             "  -v                 Verbose Mode\n", 
             "  -h                 This help message\n", 
             g_path_get_basename(progname));
    exit(1);
}


int 
main(int argc, char **argv)
{
    image_annotation *state = (image_annotation*) calloc(1, sizeof(image_annotation));
    const char *optstring = "i:l:vhd";

    int c;
    state->file_name = NULL;
    state->skip_doorways = 1;
    char *input_log_file = NULL;
    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'l':
            state->file_name = strdup(optarg);  //output filename for feature data
            fprintf(stderr, "Saving to LOG file %s\n", state->file_name);
            break;
        case 'i':
            input_log_file = strdup(optarg);  //annotated lcm log 
            fprintf(stderr, "Reading from LOG file %s\n", input_log_file);
            break;
        case 'd':
            state->skip_doorways = 0;
            fprintf(stderr, "Training for doorways as well\n");
            break;
        case 'v':
            fprintf(stderr, "Verbose mode\n");
            state->verbose = 1;
            break;
        case 'h': //help
            usage(argv[0]);
            break;
        default:
            usage(argv[0]);
            break;
        }
    }

    fprintf(stderr, "Don't use the live mode - the handler can drop messages (because a lot of messages are sent out in bursts\n");

    if(state->file_name == NULL){
        fprintf(stderr, "No log file specified - defaulting to data.txt\n");
        //state->file = fopen("data.txt", "a");
        state->file_name = strdup("data.txt");
    }

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);

    //This descriptor is currently completely arbitrary.
    QString str = "Lx(2,20)+Ly(2,20)+Lx(4,20)+Ly(4,20)";//"Lxx(8,28)";
    //another option to try 
    //Lxx(8,28)+Lxy(8,28)+Lyy(8,28)+Lxx(2,28)+Lxy(2,28)+Lyy(2,28)
     //QString str = "";
    //state->crfh_sys =  new CSystem(str);
    state->extractor = create_image_feature_extractor();
    state->class_count = g_hash_table_new(g_int_hash, g_int_equal);
    state->file =  fopen(state->file_name, "w");
    if(input_log_file){
        fprintf(stderr, "System is loading the annotation messages in log to calculate features.\n");
        lcm_eventlog_t *read_log = lcm_eventlog_create(input_log_file, "r");
        fprintf(stderr, "Log open success.\n");
  
        //Adding to annotation circular buffer
        char *annotation_channel_name = "IMAGE_ANNOTATION";
        int count = 0;
        for(lcm_eventlog_event_t *event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){
            int decode_status = 0;
            if(strcmp(annotation_channel_name, event->channel) == 0){
	      perception_image_annotation_t *msg = (perception_image_annotation_t *)calloc(1, sizeof(perception_image_annotation_t));
                decode_status = perception_image_annotation_t_decode(event->data, 0, event->datalen, msg);
                if(decode_status < 0){
                    fprintf(stderr, "Error decoding message.\n");
                    //return -1;
                }
                count++;
                on_image_annotation(NULL, annotation_channel_name, msg, state);
                perception_image_annotation_t_destroy(msg);
            }
            lcm_eventlog_free_event(event);
        }
        fclose(state->file);
        print_stats(state);
        fprintf(stderr, "No of examples : %d\n", count);
        return 0;
    }

    else{
        subscribe_to_channels(state);
  
        state->mainloop = g_main_loop_new( NULL, FALSE );  
  
        if (!state->mainloop) {
            printf("Couldn't create main loop\n");
            return -1;
        }

        //add lcm to mainloop 
        bot_glib_mainloop_attach_lcm (state->lcm);

        //read_parameters_from_conf(state);

        //adding proper exiting 
        bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
        //    fprintf(stderr, "Starting Main Loop\n");

        ///////////////////////////////////////////////
        g_main_loop_run(state->mainloop);
  
        bot_glib_mainloop_detach_lcm(state->lcm);
        return 0;
    }
}


