/*
 * event.c
 *
 * Event handler registry and event queue for applications that 
 * don't implement event handler.
 *
 * This file is part of libariane
 *
 * Copyright (C) 2009-2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include "internals.h"
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SID_COMPARE_WITHOUT_LOCATION(_sid0, _sid1)	(((_sid0) & 0xFF0F) == ((_sid1) & 0xFF0F))

typedef struct listener_struct {
	struct listener_struct *next;
	
	ariane_sensor_t *sensor;
	ariane_event_handler_t handler;
	void *handler_param;
} listener_t;

// TODO this stuff could be in sensordev structure (?)
static listener_t listeners = {.next = NULL};
static pthread_mutex_t listener_mutex = PTHREAD_MUTEX_INITIALIZER;

int ariane_register_listener(ariane_sensor_t *sensor, ariane_event_handler_t handler, void *handler_param) {
	listener_t *l = malloc(sizeof(listener_t));
	if (!l)
		return -errno;

	l->sensor = sensor;
	l->handler = handler;
	l->handler_param = handler_param;

	pthread_mutex_lock(&listener_mutex);
	
	l->next = listeners.next;
	listeners.next = l;

	pthread_mutex_unlock(&listener_mutex);

	return 0;
}

int ariane_unregister_listener(ariane_sensor_t *sensor) {
	if (!sensor)
		return -EINVAL;

	int retval = -EINVAL;

	pthread_mutex_lock(&listener_mutex);

	listener_t *l = &listeners;
	while (l->next) {
		if (l->next->sensor == sensor) {	// ptr comparison should be enough, the clients are not supposed to copy the structs!
			listener_t *removed = l->next;
			
			l->next = l->next->next;
			retval = 0;

			free(removed);
			break;
		}

		l = l->next;
	}

	pthread_mutex_unlock(&listener_mutex);

	return retval;
}

int ariane_deliver_event(ariane_sensordev_t *dev, ariane_sid_t sid) {
	assert(dev);

	pthread_mutex_lock(&listener_mutex);

	listener_t *l = listeners.next;
	while (l) {
		if (SID_COMPARE_WITHOUT_LOCATION(l->sensor->sid, sid) && l->sensor->dev == dev) {
			dev->event.sensor = l->sensor;
			l->handler(&dev->event, l->handler_param);
			// there could be several listeners to we need to loop through the entire list :(
		}

		l = l->next;
	}

	pthread_mutex_unlock(&listener_mutex);

	return 0;
}

void ariane_deliver_error(ariane_sensordev_t *dev, ariane_event_t *event) {
	assert(dev && event);

	pthread_mutex_lock(&listener_mutex);

	listener_t *l = listeners.next;
	while (l) {
		if (l->sensor->dev == dev) {
			event->sensor = l->sensor;
			l->handler(event, l->handler_param);
		}

		l = l->next;
	}

	pthread_mutex_unlock(&listener_mutex);
}

void ariane_async_error(ariane_sensordev_t *dev, int err) {
	assert(dev);
	dev->state = ARIANE_DEVICE_STATE_ERROR;

	ariane_event_t event = {
		.timestamp = 0,
		.sensor = NULL,
		.datafmt = ARIANE_EVENT_DATAFMT_ERROR,
		.data.error = err
	};

	ariane_deliver_error(dev, &event);
}


/*
	Event queue things are below this comment
*/

static pthread_mutex_t queue_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t queue_cond = PTHREAD_COND_INITIALIZER;

static ariane_event_t *queue = NULL;
static unsigned int queue_size = 0;
static volatile int queue_readi;				// read index
static volatile unsigned int queue_writei;		// write index

int ariane_init_event_queue(unsigned int nevents) {
	if (!nevents)
		return -EINVAL;

	if (queue) {
		ariane_release_event_queue();
	}

	queue = malloc(sizeof(ariane_event_t) * nevents);
	if (!queue)
		return -errno;

	queue_size = nevents;
	queue_readi = -1;
	queue_writei = 0;

	return 0;
}

void ariane_release_event_queue() {
	if (queue) {
		free(queue);
		queue = NULL;
	}
}

void ariane_event_handler(const ariane_event_t *event, void *param) {
	assert(queue && queue_size);

	pthread_mutex_lock(&queue_mutex);

	memcpy(&queue[queue_writei], event, sizeof(ariane_event_t));

	if (queue_readi == queue_writei) {
		// reader was too slow, queue_readi points to the oldest, so skip that to keep the order
		queue_readi++;
		if (queue_readi >= queue_size)
			queue_readi = 0;
	}

	if (queue_readi < 0)
		queue_readi = queue_writei;	// queue was empty

	queue_writei++;
	if (queue_writei >= queue_size)
		queue_writei = 0;

	pthread_cond_signal(&queue_cond);
	pthread_mutex_unlock(&queue_mutex);
}


bool ariane_get_event(bool wait, ariane_event_t *event) {
	assert(queue && queue_size && event);

	pthread_mutex_lock(&queue_mutex);

	while (queue_readi < 0) {
		if (!wait) {
			pthread_mutex_unlock(&queue_mutex);
			return false;
		}
		pthread_cond_wait(&queue_cond, &queue_mutex);
	}
	
	memcpy(event, &queue[queue_readi], sizeof(ariane_event_t));

	queue_readi++;
	if (queue_readi >= queue_size)
		queue_readi = 0;

	if (queue_readi == queue_writei)
		queue_readi = -1;	// it's empty

	pthread_mutex_unlock(&queue_mutex);

	return true;
}
