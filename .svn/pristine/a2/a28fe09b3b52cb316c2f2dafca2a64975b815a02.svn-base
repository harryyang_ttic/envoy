
//ik.c
#include <bot_core/bot_core.h>
#include <bot_param/param_client.h>
#include <bot_frames/bot_frames.h>
#include <stdio.h>
#include <string.h>

#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif

#include <math.h>
#include <stdbool.h>
#include <lcm/lcm.h>

#include <lcmtypes/arm_cmd_ik_t.h>
#include <lcmtypes/arm_cmd_joint_list_t.h>
#include "ik.h"

#define PI 3.14159265

#define JOINT_PREFIX "arm_config.joints"
#define MAX_SPEED 50

enum JOINTS{
    base,
    shoulder,
    elbow,
    lateral_wrist,
    gripper,
    twist_wrist,
    effector_static,
};

IK *ik_get_global(lcm_t *lcm){
    IK *self = (IK *) calloc(1, sizeof(IK));
    self->lcm = lcm;
  
    // Get BotFrames instance
    BotParam * param = bot_param_new_from_server(self->lcm, 0);
    if (!param) {
        fprintf (stderr, "Error getting BotParam. Are you running the server?\n");
    }

    //get information from param server
    bot_param_get_int(param, "arm_config.num_servos", &self->num_servos);

    self->joint_names = bot_param_get_subkeys(param, JOINT_PREFIX);

    self->num_joints = 0;
    if (self->joint_names) {
        for (int pind = 0; self->joint_names[pind] != NULL; pind++) {
            //fprintf(stderr, "Joint Name : %s\n", self->joint_names[pind]);
            self->num_joints++;
        }
    }

    self->jlist.no_joints = self->num_joints; 
    self->jlist.joints = (joint_t *) calloc(self->jlist.no_joints, sizeof(joint_t));

    self->h_servos = g_hash_table_new(g_int_hash, g_int_equal);
    self->h_joints = g_hash_table_new(g_int_hash, g_int_equal);

    
    //incrementally fill them 
    char key[1024]; 
    for(int i=0; i< self->num_joints; i++){
        self->jlist.joints[i].joint_name = strdup(self->joint_names[i]);
        
        sprintf(key,"%s.%s.joint_frame", JOINT_PREFIX, self->joint_names[i]);
       
        char *joint_frame;
        if (0 != bot_param_get_str(param, key, &self->jlist.joints[i].joint_frame)){
            fprintf(stderr, "%s - frame not defined\n", key);
        }
        sprintf(key,"%s.%s.relative_to", JOINT_PREFIX, self->joint_names[i]);
        char *relative_to;
        if (0 != bot_param_get_str(param, key, &self->jlist.joints[i].relative_to)){
            fprintf(stderr, "%s - relative to not defined\n", key);
        }

        sprintf(key,"%s.%s.variable_ind", JOINT_PREFIX, self->joint_names[i]);
        bot_param_get_int(param, key, &self->jlist.joints[i].rpy_update_ind);

        

        sprintf(key,"%s.%s.joint_index", JOINT_PREFIX, self->joint_names[i]);
        bot_param_get_int(param, key, &self->jlist.joints[i].joint_id);

        //fprintf(stderr,"Joint Index : %d\n", self->jlist.joints[i].joint_id);
        
        //fprintf(stderr, "Variable Ind : %d\n", self->jlist.joints[i].rpy_update_ind);
        
        //fprintf(stderr, "Frame : %s\n", self->jlist.joints[i].joint_frame);

        sprintf(key,"coordinate_frames.%s.initial_transform.translation", self->jlist.joints[i].joint_frame);
        //get fixed translation values for each arm segment
        bot_param_get_double_array (param, key, (self->jlist.joints[i].frame_translation), 3);

        sprintf(key,"coordinate_frames.%s.initial_transform.rpy", self->jlist.joints[i].joint_frame);
        //get fixed translation values for each arm segment
        bot_param_get_double_array (param, key, (self->jlist.joints[i].frame_rpy), 3);

        //fprintf(stderr, "Initial translation : %f,%f,%f\n", self->jlist.joints[i].frame_translation[0],            
        //    self->jlist.joints[i].frame_translation[1], self->jlist.joints[i].frame_translation[2]);

        //fprintf(stderr, "Initial rpy : %f,%f,%f\n", self->jlist.joints[i].frame_rpy[0],            
        //    self->jlist.joints[i].frame_rpy[1],            
        //    self->jlist.joints[i].frame_rpy[2]);  

        self->jlist.joints[i].frame_rpy[0] = bot_to_radians(self->jlist.joints[i].frame_rpy[0]);
        self->jlist.joints[i].frame_rpy[1] = bot_to_radians(self->jlist.joints[i].frame_rpy[1]);
        self->jlist.joints[i].frame_rpy[2] = bot_to_radians(self->jlist.joints[i].frame_rpy[2]);

        sprintf(key,"coordinate_frames.%s.update_channel", self->jlist.joints[i].joint_frame);

        if (0 != bot_param_get_str(param, key, &self->jlist.joints[i].update_channel)){
            fprintf(stderr, "%s - frame not defined\n", key);
            //return -1;
            self->jlist.joints[i].update_channel = NULL;
        }
        
        //fprintf(stderr, "Update Channel : %s\n", self->jlist.joints[i].update_channel);
        
        sprintf(key,"%s.%s.servos", JOINT_PREFIX, self->joint_names[i]);

        char **servos = bot_param_get_subkeys(param, key);

        int s_count = 0;

        if (servos) {
            for (int pind = 0; servos[pind] != NULL; pind++) {
                //fprintf(stderr, "Servo Name : %s\n", servos[pind]);
                s_count++;
            }
        }

        sprintf(key,"%s.%s.deg_to_ticks", JOINT_PREFIX, self->joint_names[i]);

        bot_param_get_double(param, key, &self->jlist.joints[i].deg_to_tick);
        
        //fprintf(stderr, "Deg to ticks : %f\n", self->jlist.joints[i].deg_to_tick);
        self->jlist.joints[i].no_servos = s_count;
        self->jlist.joints[i].servos = (servo_t *) calloc(s_count , sizeof (servo_t));
        
        //fprintf(stderr, "No of servos : %d\n", s_count);

        char servo_sub[1024];

        for(int j=0; j < s_count; j++){
            sprintf(servo_sub,"%s.%s.servos.%s", JOINT_PREFIX, self->joint_names[i], servos[j]);

            sprintf(key,"%s.servo_index", servo_sub);
            bot_param_get_int(param, key, &(self->jlist.joints[i].servos[j].servo_id));

            sprintf(key,"%s.min_range", servo_sub);
            bot_param_get_int(param, key, &self->jlist.joints[i].servos[j].min_tick);
            
            sprintf(key,"%s.max_range", servo_sub);
            bot_param_get_int(param, key, &self->jlist.joints[i].servos[j].max_tick);
            

            sprintf(key,"%s.offset", servo_sub);
            bot_param_get_int(param, key, &self->jlist.joints[i].servos[j].offset);

            sprintf(key,"%s.sign", servo_sub);
            bot_param_get_int(param, key, &self->jlist.joints[i].servos[j].sign);

            sprintf(key,"%s.use_for_frame", servo_sub);
            bot_param_get_int(param, key, &self->jlist.joints[i].servos[j].use_for_frame);

            //fprintf(stderr, "\t %s - %d [%d-%d] : %d\n", 
            //servos[j], self->jlist.joints[i].servos[j].servo_id, 
            //self->jlist.joints[i].servos[j].min_tick, 
            //self->jlist.joints[i].servos[j].max_tick,
            //self->jlist.joints[i].servos[j].offset);

            self->jlist.joints[i].servos[j].parent = &self->jlist.joints[i];

            g_hash_table_insert(self->h_servos, &self->jlist.joints[i].servos[j].servo_id, &self->jlist.joints[i].servos[j]);
        }
        if (servos) {
            for (int pind = 0; servos[pind] != NULL; pind++) {
                free(servos[pind]);
            }
        }
    }

    self->frames = bot_frames_get_global(self->lcm, param);

    return self;
}


/*
  ik_is_in_self_collision
*/
bool ik_is_in_self_collision(qs_t *qs, int num_joints, double *max_angles, double *min_angles)
{
    bool is_safe = true;
    for(int j=0; j<num_joints; j++){
        if(qs->q_vals[j] < min_angles[j] || qs->q_vals[j] > max_angles[j]){
            is_safe = false;
        }
    }
    return is_safe;
}



/*
  places joint at frame_name in location xyz[3] in orientation rpy[3]; joint angles are stored at q_out.

  Returns true if succesful, false if failed

  all angles taken to be in radians. All lengths taken in meters.

  maybe you want this to work for position and orientation of different joints? e.g. elbow here, with joint at this orientation?

  work this out later. Maybe this should be included in cost calculation/constraints.


  if at 0,0,z, then theta[base] will be set to [insert number here when you decide].
*/ 

bool ik_q_gen(IK *self, double loc[3], double ef_rpy[3], double gripper_pos,  bool gripper_en, qs_list_t *qlist){

    //ASSUMES THE FOLLOWING:
    //joint 0: base, relative to body
    //joint 1: shoulder, relative to base
    //joint 2: elbow, relative to shoulder
    //joint 3: lateral_wrist, relative to elbow
    //joint 4: twist_wrist, relative to lateral_wrist
    //joint 5: gripper, relative to twist_wrist
    //joint 7: effector_static, relative to gripper
    //
    //maybe we should make this more flexible in terms of joint numbers/relative to's? But since
    //IK depends on the exact configuration of the arm anyways maybe it won't really make sense to.

    joint_list_t jlist = self->jlist;
    BotFrames *frames = self->frames;

    int num_joints = jlist.no_joints;
    double j_len[num_joints];
    qs_list_t *q_out = (qs_list_t *)calloc(1, sizeof(qs_list_t));

    j_len[0] = 0;

    BotTrans trans, trans1;

    for (int i=1; i < num_joints; i++){
        int joint_index = jlist.joints[i].joint_id;
        if (!bot_frames_get_trans(frames, jlist.joints[joint_index].joint_frame,"base", &trans)) {
            fprintf(stderr,"Frame Error\n");
            return false;
        }
        if (!bot_frames_get_trans(frames, jlist.joints[joint_index].relative_to, "base", &trans1)) {
            fprintf(stderr,"Frame Error\n");
            return false;
        }

        double diff[3] = {trans.trans_vec[0]-trans1.trans_vec[0],trans.trans_vec[1]-trans1.trans_vec[1],trans.trans_vec[2]-trans1.trans_vec[2]};

        j_len[i] = sqrt(diff[0]*diff[0]+diff[1]*diff[1]+diff[2]*diff[2]);
    }


    //to do: needs length of static gripper which is not in jlist
    double eff_length = j_len[effector_static]+j_len[twist_wrist]+j_len[gripper]+.111125;
  
    double x = loc[0];
    double y = loc[1];
    double z = loc[2];
    
    double xyz[3];
    double theta[num_joints];
    double theta_star[num_joints];

    theta[twist_wrist] = ef_rpy[0];
    if(gripper_en)
        theta[gripper] = gripper_pos*PI/180;
    else{
        theta[gripper] = 0;
    }
    theta_star[lateral_wrist] = ef_rpy[1];
  
    if(!((x == 0 && y == 0) || (ef_rpy[2] == atan2(y,x)))){
        fprintf(stderr,
                "Error: attempt to position wrist in direction other than direction of arm.\n");
        fprintf(stderr, "Modifying yaw to allow IK generation at given location.\n");
        ef_rpy[2] = atan2(y,x);
    }
  
    xyz[0] = x - eff_length * cos(theta_star[lateral_wrist]) * cos(ef_rpy[2]);
    xyz[1] = y - eff_length * cos(theta_star[lateral_wrist]) * sin(ef_rpy[2]);
    xyz[2] = z - eff_length * sin(theta_star[lateral_wrist]);

    q_out->num_lists = 4;
    q_out->qs = (qs_t *) calloc(4, sizeof(qs_t));

    double arm_length, theta_a, theta_w, x_const, y_const;

    for(int i=0; i<4; i++){

        if(i==0 || i==1){
            theta[base] = ef_rpy[2];
            x_const = j_len[shoulder]*cos(theta[base]);
            y_const = j_len[shoulder]*sin(theta[base]);
        }
        else{
            if(theta[base] >= PI){
                theta[base] = ef_rpy[2] - PI;
            }
            else{
                theta[base] = ef_rpy[2] + PI;
            }
            x = xyz[0] + x_const;
            y = xyz[1] + y_const;
        }
       
        x = xyz[0] - x_const;
        y = xyz[1] - y_const;
        z = xyz[2];

        //calc theta[shoulder], theta[elbow], theta[lateral_wrist]

        arm_length = sqrt(x*x+y*y+z*z);

        if(arm_length > (j_len[elbow]+j_len[lateral_wrist]) || (arm_length+j_len[lateral_wrist]) < j_len[elbow]){
            fprintf(stderr,"Error: the point is out of reach of the arm. Q's not calulated.\n");
            return false;
        }

        theta_star[elbow] = acos((-arm_length*arm_length + j_len[lateral_wrist]*j_len[lateral_wrist] + j_len[elbow]*j_len[elbow])/(2*j_len[lateral_wrist]*j_len[elbow]));

        theta_star[shoulder] = acos((-j_len[lateral_wrist]*j_len[lateral_wrist] + j_len[elbow]*j_len[elbow] + arm_length*arm_length)/(2*arm_length*j_len[elbow]));

        theta_a = atan2(z, copysign(sqrt(y*y+x*x), cos(atan2(y,x))/cos(theta[base])));

        if(i == 1 || i == 3){
            theta[lateral_wrist] = theta_star[lateral_wrist]+theta_star[elbow]+theta_star[shoulder]-PI-theta_a;
            theta[shoulder] = theta_a-theta_star[shoulder];
            theta[elbow] = PI - theta_star[elbow];
        }
        else{
            theta[lateral_wrist] = theta_star[lateral_wrist]-theta_star[elbow]-theta_star[shoulder]+PI-theta_a;
            theta[shoulder] = theta_a+theta_star[shoulder];
            theta[elbow] = -PI + theta_star[elbow];
        }
      
        q_out->qs[i].num_qs = num_joints; //change if wheelchair.cfg changes
        q_out->qs[i].q_vals = (double *) calloc(q_out->qs[i].num_qs, sizeof(double));
        q_out->qs[i].joint_index = (int *) calloc(q_out->qs[i].num_qs, sizeof(int));

        //all angles between -PI, PI

        for(int k=0; k<6; k++){
            if(theta[k]<-PI){
                while(theta[k]<-PI)
                    theta[k] = theta[k] + 2*PI;
            }
            if(theta[k]>PI){
                while(theta[k]>=PI)
                    theta[k] = theta[k] - 2*PI;
            }
        }

        //store things

        if(x==0 &&y == 0 && ef_rpy[2] == 0){
            q_out->qs[i].q_vals[base] = -1000;
        }
        else{
            q_out->qs[i].q_vals[base] = theta[base];
        }
        q_out->qs[i].q_vals[shoulder] = theta[shoulder];
        q_out->qs[i].q_vals[elbow] = theta[elbow];
        q_out->qs[i].q_vals[lateral_wrist] = theta[lateral_wrist];
        q_out->qs[i].q_vals[twist_wrist] = theta[twist_wrist];
        q_out->qs[i].q_vals[gripper] = theta[gripper];

        q_out->qs[i].joint_index[base] = base;
        q_out->qs[i].joint_index[shoulder] = shoulder;
        q_out->qs[i].joint_index[elbow] = elbow;
        q_out->qs[i].joint_index[lateral_wrist] = lateral_wrist;
        q_out->qs[i].joint_index[twist_wrist] = twist_wrist;
        q_out->qs[i].joint_index[gripper] = gripper;
    
        // printf("base: %f\n shoulder: %f\n elbow %f\n lateral_wrist: %f\n twist_wrist: %f\n gripper: %f\n",
        //       theta[base], theta[shoulder], theta[elbow], theta[lateral_wrist], theta[twist_wrist], theta[gripper]);
        // printf("theta_a: %f\n", theta_a);
    
    
    }
  
    qlist->num_lists = 0;
    qlist->qs = (qs_t *) calloc(qlist->num_lists, sizeof(qs_t));
  
    double max_angles[jlist.no_joints];
    double min_angles[jlist.no_joints];
  
    for(int i = 0; i < jlist.no_joints; i++){
        int joint_id = jlist.joints[i].joint_id;
        max_angles[joint_id] = (jlist.joints[joint_id].servos[0].max_tick - jlist.joints[joint_id].servos[0].offset)/jlist.joints[joint_id].deg_to_tick*PI/180;
        min_angles[i] = (jlist.joints[joint_id].servos[0].min_tick - jlist.joints[joint_id].servos[0].offset)/jlist.joints[joint_id].deg_to_tick*PI/180;
    }

    for(int i=0; i<4; i++){
        if(ik_is_in_self_collision(&q_out->qs[i], jlist.no_joints, &max_angles, &min_angles)){
            qlist->num_lists++;
            qlist->qs = (qs_t *)realloc(qlist->qs, (qlist->num_lists) * sizeof(qs_t));
            qlist->qs[(qlist->num_lists)-1].num_qs = q_out->qs[i].num_qs;
            qlist->qs[(qlist->num_lists)-1].q_vals = 
                (double *)calloc(qlist->qs[(qlist->num_lists)-1].num_qs, sizeof(double));
            qlist->qs[(qlist->num_lists)-1].joint_index = 
                (int *)calloc(qlist->qs[(qlist->num_lists)-1].num_qs, sizeof(int));
            for(int j=0; j<q_out->qs[i].num_qs; j++){
                qlist->qs[(qlist->num_lists)-1].q_vals[j] = q_out->qs[i].q_vals[j];
                qlist->qs[(qlist->num_lists)-1].joint_index[j] = q_out->qs[i].joint_index[j];
            }
        }
    }


    if(qlist->num_lists == 0){
        free(q_out);
        printf("No valid joint angles!\n");
        return false;
    }
    else {
        /*forward kinematics*/
        double joint_length[7] = {0,0.047625,0.38735,0.381,0.06985,0.038735,0.111125};
    
        //calculate magnitude of arm length
        double arm_magnitude_in_xy = 0;
        double cur_angle = 0;
        double z_pos = 0;
        for(int i = 1; i < 7; i++){
            arm_magnitude_in_xy = arm_magnitude_in_xy + joint_length[i]*cos(cur_angle);
            z_pos = z_pos + joint_length[i]*sin(cur_angle);
            if(i < 4){
                cur_angle = cur_angle + q_out->qs[0].q_vals[i];
            }
        }
        double x_pos = arm_magnitude_in_xy*cos(q_out->qs[0].q_vals[0]);
        double y_pos = arm_magnitude_in_xy*sin(q_out->qs[0].q_vals[0]);

        //fprintf(stderr, "Forward Kinematics | X: %f | Y: %f | Z: %f\n", x_pos, y_pos, z_pos);
        /*End forward kinematics*/

        for(int i=0; i<q_out->num_lists; i++){
            free(q_out->qs[i].q_vals);
        }
        for(int i=0; i<q_out->num_lists; i++){
            free(q_out->qs[i].joint_index);
        }
        free(q_out->qs);
        free(q_out);
        return true;
    }
}
