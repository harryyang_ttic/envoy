// lcm
#include <lcm/lcm-cpp.hpp>
#include <lcmtypes/bot2_param.h>

// libbot/lcm includes
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>

// libbot cpp
#include <lcmtypes/bot_core.hpp>

// non-free opencv includes
#include <opencv2/nonfree/nonfree.hpp>

// opencv-utils includes
#include <perception_opencv_utils/opencv_utils.hpp>

// pcl-utils includes
#include <pcl-utils/pcl_utils.hpp>

// vis-utils includes
#include <vis-utils/vis_utils.hpp>

// lcm log player wrapper
#include <lcm-utils/lcm-reader-util.hpp>

// optargs
#include <ConciseArgs>

// pcl includes
#include <pcl/common/common_headers.h>
#include <pcl/io/pcd_io.h>

#include <pcl/range_image/range_image.h>
#include <pcl/range_image/range_image_planar.h>

#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/range_image_visualizer.h>
#include <pcl/features/range_image_border_extractor.h>
#include <pcl/keypoints/narf_keypoint.h>
#include <pcl/keypoints/harris_3d.h>

#include <pcl/keypoints/sift_keypoint.h>


// Profiler
// #include "profiler.hpp"

using namespace std;

//----------------------------------
// State representation
//----------------------------------
struct state_t {
    lcm::LCM lcm;
    cv::VideoWriter writer;
    // fsvision::Profiler profiler;

    // fsvision::GeneralPurposeFeatureTracker gpft;
    pcl::visualization::RangeImageVisualizer* ri_widget;

    boost::shared_ptr<pcl::RangeImagePlanar> rimage_ptr;
    pcl::RangeImagePlanar rimage; 

    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange>::Ptr rimage_handler;
    state_t() {
        // profiler.setName("******************** GPFT ******************** ");

        // Setup pcl stuff
        rimage_ptr = boost::shared_ptr<pcl::RangeImagePlanar>(new pcl::RangeImagePlanar);
        rimage = *rimage_ptr;

        ri_widget = new pcl::visualization::RangeImageVisualizer ("Range image");

        rimage_handler = pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange>::Ptr (new pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange>  (rimage_ptr, 0, 0, 0) );

    }

    ~state_t() { 
    }

    void* ptr() { 
        return (void*) this;
    }
    
    void process(const cv::Mat& img);

    void on_kinect_image_frame (const lcm::ReceiveBuffer* rbuf, 
                                const std::string& chan,
                                const kinect::frame_msg_t *msg);
};
state_t state;

struct GPFTOptions { 
    bool vLIVE_MODE;
    bool vDEBUG;
    float vMAX_FPS;
    float vSCALE;
    std::string vLOG_FILENAME;
    std::string vCHANNEL;


    GPFTOptions () : 
        vLOG_FILENAME(""), vCHANNEL("KINECT_FRAME"), 
        vMAX_FPS(30.f) , vSCALE(1.f), 
        vDEBUG(false), vLIVE_MODE(false) {}
};
GPFTOptions options;

// LCM Log player wrapper
LCMLogReaderOptions poptions;
LCMLogReader player;



void normal_corner_est(const cv::Mat_<cv::Vec3f>& normals, 
                       std::vector<cv::Point2f>& pts, int block_size, 
                       const cv::Mat_<uchar>& mask = cv::Mat()) { 
    
    // int min_count = block_size * block_size; 
    cv::Mat tmp, dst;
    cv::Sobel(normals, dst, CV_64F, 1, 0, block_size);
    cv::Sobel(normals, tmp, CV_64F, 0, 1, block_size);
    dst = dst * 0.5 + tmp * 0.5;

    cv::Mat maskf; 
    mask.convertTo(maskf, CV_64F);

    cv::Mat mask3; 
    cv::Mat mask3_[] = {maskf, maskf, maskf};
    cv::merge(&mask3_[0], 3, mask3);

    opencv_utils::imshow("dst", dst);

    cv::multiply(mask3, dst, dst);

    opencv_utils::imshow("dst-withmask", dst);
}

cv::Mat1b build_viewpoint_mask(const cv::Mat_<cv::Vec3f>& normals, 
                               const cv::Mat1b& nmask = cv::Mat()) { 
    cv::Mat1b mask = cv::Mat1b::zeros(normals.size());
    // cv::Mat_<float> debug = cv::Mat_<float>::zeros(normals.size());

    const cv::Vec3f view(0,0,-1);
    const float dot = cos(75 * CV_PI/180);

    for (int y=0; y<normals.rows; y++) { 
        for (int x=0; x<normals.rows; x++) { 
            if (nmask.data && nmask(y,x)) { 
                const cv::Vec3f& n = normals(y,x);
                if (n.dot(view) > dot) {  
                    // debug(y,x) = abs(n.dot(view));
                    mask(y,x) = 255;                
                }
            }
        }
    }
    // cv::imshow("view debug", debug);
    // cv::imshow("view mask", mask);

    return mask;
}

static void on_kinect_image_frame(const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                                   const kinect::frame_msg_t *msg) {
    state.on_kinect_image_frame(rbuf, chan, msg);
}

void state_t::on_kinect_image_frame (const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                                   const kinect::frame_msg_t *msg) {

    // profiler.enter("on_kinect");

    //----------------------------------
    // Unpack Point cloud
    //----------------------------------
    cv::Mat img;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>());
    pcl_utils::unpack_kinect_frame_with_cloud(msg, img, cloud, options.vSCALE);

    //----------------------------------
    // Compute GFTT
    //----------------------------------
    {

        double quality = 1e-3;
        int block_size = 7;

        std::vector<cv::Point2f> corners;
        cv::Mat gray, disp = img.clone();
        cv::cvtColor(img, gray, cv::COLOR_BGR2GRAY);

#if 0
        //----------------------------------
        // Compute SIFT
        //----------------------------------
        cv::SiftFeatureDetector detector;
        cv::Mat desc;
        std::vector<cv::KeyPoint> kpts;
        detector.detect(gray, kpts, cv::Mat());

        for (int j=0; j<kpts.size (); ++j) { 
            // if (j < 10) std::cerr << kpts[j] << std::endl;
            cv::Size s = cv::Size(std::max(double(2.f),0.6*kpts[j].size),
                                  std::max(double(1.f),0.3*kpts[j].size)) ;
            cv::ellipse(disp, kpts[j].pt, s, 
                        kpts[j].angle, 0, 360, 
                        cv::Scalar(0,200,0), 1, CV_AA);

        }


        opencv_utils::imshow("sift", disp);

#else
        //----------------------------------
        // Compute Evecs on normals
        //----------------------------------
        cv::Mat evalvecs;
        cv::cornerEigenValsAndVecs(gray, evalvecs, 4, 7);

        std::vector<cv::Mat> vec_evalvecs;
        cv::split(evalvecs, vec_evalvecs);

        cv::Mat flow1(gray.size(), CV_32FC2), flow2(gray.size(), CV_32FC2); 
        cv::Mat out[] = {flow1, flow2};
        int from_to[] = {2,0, 3,1, 4,2, 5,3};
        cv::mixChannels(&evalvecs, 1, out, 2, from_to, 4);

        cv::Mat flow1_img, flow2_img;
        opencv_utils::drawFlow(flow1, flow1_img);
        opencv_utils::drawFlow(flow2, flow2_img);
        opencv_utils::imshow("evec1", flow1_img);
        opencv_utils::imshow("evec2", flow2_img);

        cv::Mat3b eval1 = opencv_utils::color_depth_map(vec_evalvecs[0], 1.f, false);
        cv::Mat3b eval2 = opencv_utils::color_depth_map(vec_evalvecs[1], 1.f, false);
        opencv_utils::imshow("eval1", eval1);
        opencv_utils::imshow("eval2", eval2);
        // std::merge()
        // cv::Mat lambda1(m_normals.size(), CV_32FC1); 
        // cv::Mat lambda2(m_normals.size(), CV_32FC1);
        // cv::Mat evec1(m_normals.size(), CV_32FC3);
        // cv::Mat evec2(m_normals.size(), CV_32FC3);    
        // cv::Mat out[] = {lambda1, lambda2, evec1, evec2};
        // int from_to[] = {0,0, 1,1, 2,2, 3,3, 4,4, 5,5};
        // cv::mixChannels(&m_normals_evecs, 1, out, 4, from_to, 6);

        // cv::imshow("lambda1x", lambda1x);
        // cv::imshow("lambda2x", lambda2x);
        // cv::imshow("evec1x", evec1x);
        // cv::imshow("evec2x", evec2x);

        cv::goodFeaturesToTrack(gray, corners, 1000, quality, 5, cv::Mat(), block_size);
        
        for (int j=0; j<corners.size (); ++j) { 
            cv::circle(disp, corners[j], 3, cv::Scalar(0,255,0), 1,CV_AA);

            float norm = abs(eval1.at<float>(corners[j])) + abs(eval2.at<float>(corners[j]));
            if (norm == 0.f) continue;

            float l1 = eval1.at<float>(corners[j]) / norm;
            float l2 = eval2.at<float>(corners[j]) / norm;

            cv::line(disp, corners[j], 
                     corners[j] + cv::Point2f(flow1.at<cv::Vec2f>(corners[j])) * l1 * 10.f, 
                     CV_RGB(200,0,0), 1, CV_AA, 0);
            cv::line(disp, corners[j], 
                     corners[j] + cv::Point2f(flow2.at<cv::Vec2f>(corners[j])) * l2 * 10.f, 
                     CV_RGB(0,0,200), 1, CV_AA, 0);

        }
        opencv_utils::imshow("gftt", disp);
#endif

    }

    //----------------------------------
    // Convert to cv::cloud
    //----------------------------------
    cv::Mat_<cv::Vec3f> cvcloud; 
    pcl_utils::convert(cloud, cvcloud);

    std::vector<cv::Mat> cloud_comps;
    cv::split(cvcloud, cloud_comps);
    vis_utils::publish_cloud("CLOUD", cloud);

    //----------------------------------
    // Compute normals
    //----------------------------------
    pcl::PointCloud<pcl::Normal>::Ptr normals;
    float depth_change_factor = 0.05;
    float smoothing_size = 5.f; // 20.f;
    pcl_utils::compute_normals(cloud, normals, depth_change_factor, smoothing_size);

    //----------------------------------
    // Compute depth/normals mask
    //----------------------------------
    cv::Vec3f normals_NaN = cv::Vec3f(0.4,.4,.4);

    //----------------------------------
    // Convert normals to cv::Vec3f
    //----------------------------------
    cv::Mat_<cv::Vec3f> m_normals;
    pcl_utils::convert(normals, m_normals, normals_NaN);

    //----------------------------------
    // Build Normals mask
    //----------------------------------
    cv::Mat1b normals_mask(m_normals.size());
    for (int j=0; j<m_normals.cols * m_normals.rows; j++)  { 
        const cv::Vec3f& v = m_normals.at<cv::Vec3f>(j);
        normals_mask.at<uchar>(j) = ((v == v) && v != normals_NaN) ? 255 : 0;
    }

    //----------------------------------
    // Build viewpoint mask with normals
    // and optionally normals mask
    //----------------------------------
    cv::Mat1b viewpoint_mask = build_viewpoint_mask(m_normals, normals_mask);
    cv::erode(viewpoint_mask, viewpoint_mask, cv::Mat(), cv::Point(-1,-1), 2);

    // COmpute normals mean, stddev
    std::vector<cv::Point2f> normal_pts;
    normal_corner_est(m_normals, normal_pts, 1, viewpoint_mask);

    //----------------------------------
    // Compute distance transform to determine confidences
    //----------------------------------
    cv::Mat normals_conf;
    distanceTransform( viewpoint_mask, normals_conf, cv::DIST_L2, 3 );
    cv::Mat3b normals_conf3 = opencv_utils::color_depth_map(normals_conf, 1.f, false);
    opencv_utils::imshow("normals_conf", normals_conf3);    

    //----------------------------------
    // Split normal components
    //----------------------------------
    std::vector<cv::Mat> m_normals_comps;
    cv::split(m_normals, m_normals_comps);
    opencv_utils::imshow("m_normals_x", m_normals_comps[0]);
    opencv_utils::imshow("m_normals_y", m_normals_comps[1]);
    opencv_utils::imshow("m_normals_z", m_normals_comps[2]);
    opencv_utils::imshow("m_normals", m_normals);
    opencv_utils::imshow("viewpoint+normals_mask", viewpoint_mask);


    // //----------------------------------
    // // Compute Evecs on normals
    // //----------------------------------
    // cv::Mat m_normals_evecsx, m_normals_evecsy, m_normals_evecsz;
    // cv::cornerEigenValsAndVecs(m_normals_comps[0], m_normals_evecsx, 4, 7);
    
    // cv::Mat lambda1x(m_normals.size(), CV_32FC1); 
    // cv::Mat lambda2x(m_normals.size(), CV_32FC1);
    // cv::Mat evec1x(m_normals.size(), CV_32FC3);
    // cv::Mat evec2x(m_normals.size(), CV_32FC3);    
    // cv::Mat out[] = {lambda1x, lambda2x, evec1x, evec2x};
    // int from_to[] = {0,0, 1,1, 2,2, 3,3, 4,4, 5,5};
    // cv::mixChannels(&m_normals_evecsx, 1, out, 4, from_to, 6);

    // cv::imshow("lambda1x", lambda1x);
    // cv::imshow("lambda2x", lambda2x);
    // cv::imshow("evec1x", evec1x);
    // cv::imshow("evec2x", evec2x);



    //----------------------------------
    // Compute Good features on normals
    //----------------------------------
    double quality = 1e-3;
    int block_size = 7;
    std::vector<cv::Point2f> cornersx;
    goodFeaturesToTrack(m_normals_comps[0], cornersx, 400, quality, 5, normals_mask, block_size);
    std::cerr << "x: " << cornersx.size() << std::endl;
    std::vector<cv::Point2f> cornersy;
    goodFeaturesToTrack(m_normals_comps[1], cornersy, 400, quality, 5, normals_mask, block_size);
    std::cerr << "y: " << cornersy.size() << std::endl;
    std::vector<cv::Point2f> cornersz;
    goodFeaturesToTrack(m_normals_comps[2], cornersz, 400, quality, 5, normals_mask, block_size);
    std::cerr << "z: " << cornersz.size() << std::endl;

    //----------------------------------
    // Aggregate corners
    //----------------------------------
    std::vector<cv::Point2f> corners;
    corners.insert(corners.end(), cornersx.begin(), cornersx.end());
    corners.insert(corners.end(), cornersy.begin(), cornersy.end());
    corners.insert(corners.end(), cornersz.begin(), cornersz.end());
    std::cerr << corners.size() << std::endl;
    
    for (int j=0; j<corners.size (); ++j)
        circle(img, corners[j], 3, cv::Scalar(0,255,0), 1,CV_AA);

    opencv_utils::imshow("img", img);

    cv::waitKey(10);

    return;

    //----------------------------------
    // Range Image
    //----------------------------------
    pcl_utils::unpack_kinect_frame_with_range(msg, img, rimage, options.vSCALE);

    //----------------------------------
    // Organized Nearest Neighbor
    //----------------------------------
    pcl::search::OrganizedNeighbor
        <pcl::PointXYZRGB>::Ptr tree(new pcl::search::OrganizedNeighbor<pcl::PointXYZRGB>());
    tree->setInputCloud(cloud);

    // // Compute normals 
    // pcl::NormalEstimationOMP<pcl::PointXYZRGB, pcl::PointNormal> ne;
    // ne.setInputCloud (cloud);
    // ne.setSearchMethod (tree);
    // ne.setViewPoint (std::numeric_limits<float>::max (), 
    //                  std::numeric_limits<float>::max (), 
    //                  std::numeric_limits<float>::max ());  

    // Parameters for sift computation
    const float min_scale = 0.01f;
    const int n_octaves = 3;
    const int n_scales_per_octave = 4;
    const float min_contrast = 0.001f;

    pcl::SIFTKeypoint<pcl::PointXYZRGB, pcl::PointWithScale> sift3D;
    sift3D.setScales(min_scale, n_octaves, n_scales_per_octave);
    sift3D.setMinimumContrast(min_contrast);
    // sift3D.setScales(0.25, 3, 1);
    // sift3D.setMinimumContrast(0.1);
    sift3D.setSearchMethod(tree);
    sift3D.setInputCloud(cloud);
    sift3D.setKSearch(5);

    pcl::PointCloud<pcl::PointWithScale>::Ptr keypoints (new pcl::PointCloud<pcl::PointWithScale>); 
    sift3D.compute(*keypoints); 
    cerr << "Computed " << keypoints->points.size () << " Keypoints  " << std:: endl;

    // ----------------------------------------------
    // -----Show keypoints in range image widget-----
    // ----------------------------------------------
    cv::resize(img, img, cv::Size(rimage.width,rimage.height), cv::INTER_NEAREST);


    pcl::PointCloud<pcl::PointXYZRGB>::Ptr kpts3D(new pcl::PointCloud<pcl::PointXYZRGB>());
    kpts3D->points.resize(keypoints->points.size());
    for (size_t i=0; i<keypoints->points.size (); ++i) { 
        std::cerr << keypoints->points[i] << std::endl;;
        kpts3D->points[i].x = keypoints->points[i].x,
            kpts3D->points[i].y = keypoints->points[i].y, 
            kpts3D->points[i].z = keypoints->points[i].z;
        kpts3D->points[i].r = 0.f,
            kpts3D->points[i].g = 255.f, 
            kpts3D->points[i].b = 0.f;

    }
    vis_utils::publish_cloud("KEYPOINTS", kpts3D);

    // std::vector<cv::Point2f> surepts;
    // pcl_utils::project_points(kpts3D, surepts); 
        // circle(img, cv::Point(keypoints->points[i].x,
        //                       keypoints->points[i].y), 
        //        3, cv::Scalar(0,255,0), 1,CV_AA);

    //----------------------------------
    // Show range image
    //----------------------------------
    ri_widget->showRangeImage (rimage);

    // pcl::RangeImageBorderExtractor range_image_border_extractor;
    // pcl::NarfKeypoint narf_keypoint_detector;
    // narf_keypoint_detector.setRangeImageBorderExtractor (&range_image_border_extractor);
    // narf_keypoint_detector.setRangeImage(&rimage);
    // narf_keypoint_detector.getParameters().support_size = 0.5;
    // narf_keypoint_detector.getParameters().calculate_sparse_interest_image=false; 
    // narf_keypoint_detector.getParameters().use_recursive_scale_reduction=true; 

    // // narf_keypoint_detector.getParameters ().min_interest_value
    // // narf_keypoint_detector.getParameters ().add_points_on_straight_edges = false;
    // // narf_keypoint_detector.getParameters ().distance_for_additional_points = 0.5;
  
    // pcl::PointCloud<int> keypoint_indices;
    // narf_keypoint_detector.compute (keypoint_indices);
    // std::cout << "Found "<<keypoint_indices.points.size ()<<" key points.\n";
    
    // // ----------------------------------------------
    // // -----Show keypoints in range image widget-----
    // // ----------------------------------------------
    // cv::resize(img, img, cv::Size(rimage.width,rimage.height), cv::INTER_NEAREST);

    // for (size_t i=0; i<keypoint_indices.points.size (); ++i)
    //     circle(img, cv::Point(keypoint_indices.points[i]%rimage.width,
    //                           keypoint_indices.points[i]/rimage.width), 
    //            3, cv::Scalar(0,255,0), 1,CV_AA);
  
    opencv_utils::imshow("img", img);
    // Set timestamp

    // profiler.leave("on_kinect");

}

void state_t::process(const cv::Mat& img) { 

    double t1 = bot_timestamp_now();

    return;
}

int main( int argc, char** argv )
{

    //----------------------------------
    // Opt args
    //----------------------------------
    ConciseArgs opt(argc, (char**)argv);
    opt.add(options.vLOG_FILENAME, "l", "log-file","Log file name");
    opt.add(options.vCHANNEL, "c", "channel","Kinect Channel");
    opt.add(options.vMAX_FPS, "r", "max-rate","Max FPS");
    opt.add(options.vSCALE, "s", "scale","Scale");    
    opt.parse();

    //----------------------------------
    // args output
    //----------------------------------
    std::cerr << "===========  pcl-utils-test-app ============" << std::endl;
    std::cerr << "MODES 1: pcl-utils-test-app -l log-file -r max-rate\n";
    std::cerr << "=============================================\n";
    if (options.vLOG_FILENAME != "") { 
        std::cerr << "=> LOG FILENAME : " << options.vLOG_FILENAME << std::endl;    
    } else { 
        std::cerr << "=> LIVE MODE" << std::endl; options.vLIVE_MODE = true;
    }

    std::cerr << "=> CHANNEL : " << options.vCHANNEL << std::endl;
    std::cerr << "=> MAX FPS : " << options.vMAX_FPS << std::endl;
    std::cerr << "=> SCALE : " << options.vSCALE << std::endl;

    std::cerr << "===============================================" << std::endl;

    //----------------------------------
    // Setup Player Options
    //----------------------------------
    poptions.fn = options.vLOG_FILENAME;
    poptions.ch = options.vCHANNEL;
    poptions.fps = options.vMAX_FPS;
    poptions.lcm = &state.lcm;
    poptions.handler = on_kinect_image_frame;
    poptions.user_data = state.ptr();

    //----------------------------------
    // Subscribe, and start main loop
    //----------------------------------
    if (options.vLIVE_MODE) {
        state.lcm.subscribe(options.vCHANNEL, &state_t::on_kinect_image_frame, &state ); 
        while (state.lcm.handle() == 0);
    } else { 
        player.init(poptions); 
    }

    return 0;
}
