group "1.c.visualization" {
    cmd "viewer" {
        exec = "er-viewer";
        host = "mwalter-agile";
    }
    cmd "spy" {
        exec = "er-spy";
        host = "mwalter-agile";
    }
}

group "2.b.navigation" {
    cmd "rrt_star" {
        exec = "er-rrtstar -l";
        host = "mwalter-agile";
    }
    cmd "floor_detector" {
        exec = "er-floor-detector -m predict -f ${ENVOY_HOME}/data/pressure_data/stata_actual.log";
        host = "mwalter-agile";
    }
    cmd "topological-navigator" {
        exec = "topo-navigator -m load -d check";
        host = "mwalter-agile";
    }
    cmd "waypoint_follower" {
        exec = "er-pp_controller -a";
        host = "mwalter-agile";
    }
    cmd "localizer" {
        exec = "er-localizer";
        host = "mwalter-agile";
    }
    cmd "person-follower" {
        exec = "er-person-follower -d -j -c"; #run the person follower in subservient mode to the joystick - also don't have the follower shift velocity
        host = "mwalter-agile";
    }
    cmd "navigator" {
        exec = "er-navigator";
        host = "mwalter-agile";
    }
    cmd "navigation-server" {
        exec = "er-navigation-server -a -x demo_stata_4_locations.xml";
        host = "mwalter-agile";
    }
}
group "1.2.cameras" {
    cmd "camera-general" {
        exec = "camview -c $ENVOY_HOME/config/camera/dragonfly_color_jpeg.xml";
        host = "mwalter-agile";
    }
    cmd "camera-left" {
        exec = "camview -c $ENVOY_HOME/config/camera/dragonfly_color_jpeg_left.xml";
        host = "mwalter-agile";
    }
    cmd "camera-middle" {
        exec = "camview -c $ENVOY_HOME/config/camera/dragonfly_color_jpeg_middle.xml";
        host = "mwalter-agile";
    }
    cmd "camera-right" {
        exec = "camview  -c $ENVOY_HOME/config/camera/dragonfly_color_jpeg_right.xml";
        host = "mwalter-agile";
    }
}

group "1.2.cameras.no_gui" {
    cmd "camera-general-nogui" {
        exec = "camview --no-gui -c $ENVOY_HOME/config/camera/dragonfly_color_jpeg.xml";
        host = "mwalter-agile";
    }
    cmd "camera-left-nogui" {
        exec = "camview --no-gui -c $ENVOY_HOME/config/camera/dragonfly_color_jpeg_left.xml";
        host = "mwalter-agile";
    }
    cmd "camera-middle-nogui" {
        exec = "camview --no-gui -c $ENVOY_HOME/config/camera/dragonfly_color_jpeg_middle.xml";
        host = "mwalter-agile";
    }
    cmd "camera-right-nogui" {
        exec = "camview --no-gui -c $ENVOY_HOME/config/camera/dragonfly_color_jpeg_right.xml";
        host = "mwalter-agile";
    }
}

group "5.slam" {
    cmd "run-slam" {
        exec = "isam_multi -c SKIRT_FRONT -s -a -p -f -n 3 -F";
        host = "mwalter-agile";
    }
    cmd "run-slam-with-rear" {
        exec = "isam_multi -c SKIRT_FRONT -r SKIRT_REAR -s -a -p -f -n 3 -F";
        host = "mwalter-agile";
    }
    cmd "save-map" {
      	  exec = "er-mapserver -n tour.lcmlog -l -m listen";
    	  host = "mwalter-agile";
    }
}     

group "1.d.maps" {
    cmd "map_server (2,3, and 4)" {
        exec = "er-mapserver -n stata_3_4_2_with_obs_and_places.lcmlog -f 3";
        host = "mwalter-agile";
    }
    cmd "map_server (2014-demo)" {
        exec = "er-mapserver -n stata_4_2014_03.lcmlog -f 4";
        host = "mwalter-agile";
    }
    cmd "map_server (4 and 8)" {
        exec = "er-mapserver -n stata_envoy_modified.lcmlog -f 8";
        host = "mwalter-agile";
    }
    cmd "map_server (2 and 4)" {
        exec = "er-mapserver -n stata_4_2_with_simobs_and_places.lcmlog -f 4";
        host = "mwalter-agile";
    }
}

group "2.c.test" {
    cmd "headset-playback" {
        exec = "er-lcm2audio -c HEADSET_AUDIO -S";
        host = "mwalter-agile";
    }
    cmd "mic-playback" {
        exec = "er-lcm2audio -c MIC_AUDIO -S";
        host = "mwalter-agile";
    }
}

group "4.simulation" {
    cmd "simulator" {
        exec = "er-simulator";
        host = "mwalter-agile";
    }
    cmd "simulator-laser" {
        exec = "er-simulator -f";
        host = "mwalter-agile";
    }
}

group "6.utils" {
    cmd "rectify-images" {
        exec = "camview -c dragonfly_rectify.xml";
        host = "mwalter-agile";
    }
}

group "0.deputy" {
    cmd "deputy" {
        exec = "bot-procman-deputy -n mwalter-agile";
        host = "localhost";
    }
}

group "6.bling" {
    cmd "tunnel" {
        exec = "bot-lcm-tunnel -r \"\"";
        host = "mwalter-agile";
    }
    cmd "demo-tunnel" {
        exec = "bot-lcm-tunnel -S \"LCMGL_localize3d_laser|HEADSET_AUDIO|MIC_AUDIO|IMU|DagonFly_IMAGE\" 192.168.1.100";
        host = "mwalter-agile"; 
    }
}

group "1.a.core" {
    cmd "er-deadreckon" {
        exec = "er-deadreckon -c -p";
        host = "mwalter-agile";
    }
    cmd "robot" {
        exec = "er-robot";
        host = "mwalter-agile";
    }
    cmd "param-server" {
        exec = "er-param-server";
        host = "mwalter-agile";
    }
    cmd "base" {
        exec = "er-base";
        host = "mwalter-agile";
    }
    cmd "robot-status" {
        exec = "er-robot-status";
        host = "mwalter-agile";
    }
    cmd "host-status" {
        exec = "er-host-status";
        host = "mwalter-agile";
    }
    cmd "host-status-backlap" {
        exec = "er-host-status";
        host = "saclap";
    }
}
group "4.deprecated" {
    cmd "front-laser" {
        exec = "er-hokuyo -a --channel=\"SKIRT_FRONT\"";
        host = "mwalter-agile";
    }
}
group "1.b.drivers" {
    cmd "laser-0" {
        exec = "er-hokuyo -a -d /dev/ttyACM0";
        host = "mwalter-agile";
    }
    cmd "laser-2" {
        exec = "er-hokuyo -a  -d /dev/ttyACM1";
        host = "mwalter-agile";
    }
    cmd "laser-3" {
        exec = "er-hokuyo -a  -d /dev/ttyACM2";
        host = "mwalter-agile";
    }
    cmd "ps3-handler" {
        exec = "er-ps3-teleop";
        host = "mwalter-agile";
    }
        cmd "IMU" {
        exec = "xsens -c IMU -d /dev/xsens";
        host = "mwalter-agile";
    }
    cmd "nodder" {
        exec = "nodder -c 0 -s 100 -d 0";
        host = "mwalter-agile";
    }
        cmd "pressure_sensor" {
        exec = "er-pressure-ariane";
        host = "mwalter-agile";
    }
    cmd "audio-recorder" {
        exec = "er-audio2lcm -i headset -i mic -l HEADSET_AUDIO -l MIC_AUDIO";
        host = "mwalter-agile";
    }
        cmd "kinect" {
        exec = "kinect-openni-lcm -j -r 10";
        host = "mwalter-agile";
    }
    cmd "robot-head" {
        exec = "er-head -s 0 -u 0";
        host = "mwalter-agile";
    }
    cmd "servo-controller" {
        exec = "dynamixel-cm700";
        host = "mwalter-agile";
    }
    cmd "velodyne" {
        exec = "velodyne-lcm";
        host = "saclap";
    }
}

group "2.a.perception" {
    cmd "person-tracker-basic" {
        exec = "er-person-tracker -c -b";
        host = "mwalter-agile";
    }
    cmd "person-tracker-with-kinect" {
        exec = "er-person-tracker -k -b";
        host = "mwalter-agile";
    }
    cmd "obstacle-tracker" {
        exec = "er-obstacles";
        host = "mwalter-agile";
    }
    cmd "kinect-person-segmenter" {
        exec = "er-person-segmenter -d";
        host = "mwalter-agile";
    }
    cmd "foviation" {
        exec = " er-gesture-foviation";
        host = "mwalter-agile";
    }
    cmd "person-moderator" {
        exec = "er-person-moderator -d";
        host = "mwalter-agile";
    }
}

group "3.dialog" {
    cmd "speech-server" {
        exec = "er-speech-server";
        host = "mwalter-agile";
    }
    cmd "dialog-manager" {
        exec = "er-dialog-manager -d lcm -c HEADSET_AUDIO";
        host = "mwalter-agile";
    }
    cmd "dialog-manager-basic" {
        exec = "er-dialog-manager-basic -d lcm -c HEADSET_AUDIO";
        host = "mwalter-agile";
    }
}

script "data-collection" {
    start group "0.deputy";
    start group "1.a.core";
    start group "1.b.drivers";
    start cmd "camera-general";	
    start group "1.c.visualization";
}

script "navigation-demo" {
    start group "0.deputy";
    start group "1.a.core";
    start group "1.b.drivers";
    #start cmd "camera-general";	
    start cmd "map_server (2014-demo)";     
    start cmd "localizer";
    start cmd "navigation-server";
    start cmd "navigator";
    start cmd "rrt_star"; 
    start cmd "obstacle-tracker";
    start cmd "waypoint_follower";
    start group "1.c.visualization";  
    start cmd "speech-server";
    start cmd "dialog-manager-basic";  
    start cmd "demo-tunnel";
}

script "navigation-demo-simulation" {
    start group "0.deputy";
    start group "1.a.core";
    start group "1.b.drivers";  
    start cmd "simulator-laser";
    start cmd "map_server (2014-demo)";
    start cmd "navigation-server";
    start cmd "navigator";
    start cmd "rrt_star"; 
    start cmd "obstacle-tracker";   
    start cmd "waypoint_follower";
    start group "1.c.visualization";    
    start cmd "speech-server";
    start cmd "dialog-manager-basic";    
}

script "start-all-cameras" {
    start cmd "camera-left";	
    wait ms 5000;
    start cmd "camera-right";	
    wait ms 5000;
    start cmd "camera-middle";
}

script "start-all-cameras-nogui" {
    start cmd "camera-left-nogui";	
    wait ms 5000;
    start cmd "camera-right-nogui";	
    wait ms 5000;
    start cmd "camera-middle-nogui";
}

script "data-collection-all-cameras" {
    start group "0.deputy";
    start group "1.a.core";
    start group "1.b.drivers";
    run_script "start-all-cameras";
    start group "1.c.visualization";
}

script "data-collection-all-cameras-nogui" {
    start group "0.deputy";
    start group "1.a.core";
    start group "1.b.drivers";
    run_script "start-all-cameras-nogui";
    start group "1.c.visualization";
}

script "person-following" {
    start group "0.deputy";
    start group "1.a.core";
    start group "1.b.drivers";	
    start cmd "camera-general";	
    start cmd "person-tracker-basic";
    start cmd "person-follower";
    start group "1.c.visualization";
}

script "person-following-all-cameras" {
    start group "0.deputy";
    start group "1.a.core";
    start group "1.b.drivers";	
    run_script "start-all-cameras";
    start cmd "person-tracker-basic";
    start cmd "person-follower";
    start group "1.c.visualization";
}

script "simulation-person-following" {
    start cmd "param-server";
    start cmd "robot";
    start group "1.c.visualization";
    start cmd "map_server (kiva)";
    start cmd "person-follower";
    start cmd "simulator-laser";
}
