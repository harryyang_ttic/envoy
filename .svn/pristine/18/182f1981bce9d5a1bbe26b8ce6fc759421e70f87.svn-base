cmake_minimum_required(VERSION 2.6.0)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x -Wno-deprecated-declarations -Wno-write-strings")

SET(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake_modules)

FIND_PACKAGE(LAPACK REQUIRED)
FIND_PACKAGE(CXSparse REQUIRED) 
find_package (OpenGL REQUIRED)

find_package(PkgConfig REQUIRED)
set(PCL_LIBRARIES  pcl_search-1.7 pcl_filters-1.7 pcl_features-1.7 pcl_registration-1.7
	pcl_segmentation-1.7 pcl_sample_consensus-1.7
	pcl_io-1.7 pcl_tracking-1.7 pcl_visualization-1.7)


# Make the scripts available in the 'cmake' directory available for the
# 'include()' command, 'find_package()' command.
set( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_LIST_DIR}/cmake_modules )

# =========== SHARED LIBS ==============
find_package(PythonInterp REQUIRED)
find_package(PythonLibs REQUIRED)
find_package(Python REQUIRED)
#find_package(NumPy REQUIRED)

# Include the CMake script UseCython.cmake.  This defines add_cython_module().
# Instruction for use can be found at the top of cmake/UseCython.cmake.
include( UseCython )

# # With CMake, a clean separation can be made between the source tree and the
# # build tree.  When all source is compiled, as with pure C/C++, the source is
# # no-longer needed in the build tree.  However, with pure *.py source, the
# # source is processed directly.  To handle this, we reproduce the availability
# # of the source files in the build tree.
# add_custom_target( ReplicatePythonSourceTree ALL ${CMAKE_COMMAND} -P
#   ${CMAKE_CURRENT_SOURCE_DIR}/cmake/ReplicatePythonSourceTree.cmake
#   ${CMAKE_CURRENT_BINARY_DIR}
#   WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} )

# If the pyx file is a C++ file, we should specify that here.
set_source_files_properties(
  ${CMAKE_CURRENT_SOURCE_DIR}/pcl_utils.pyx
  PROPERTIES CYTHON_IS_CXX 1 )

# Add numpy to the includes
include_directories( ${CMAKE_CURRENT_SOURCE_DIR} ${PYTHON_INCLUDE_PATH} ${NUMPY_INCLUDE_DIRS})

cmake_minimum_required(VERSION 2.6.0)

# Add cython module
include(cmake_modules/pods.cmake)
cython_add_module( pcl_utils pcl_utils.pyx pcl-utils.cpp)

pods_use_pkg_config_packages (pcl_utils lcm 
  bot2-core bot2-lcmgl-client bot2-param-client bot2-frames
  visualization
  glib-2.0 
  ${PCL_LIBRARIES}
)

pods_install_libraries(pcl_utils)
pods_install_pkg_config_file(pcl_utils
    LIBS -lpcl_utils
    REQUIRES pcl_io-1.7
    VERSION 0.0.1)
#pods_install_python_packages(${CMAKE_CURRENT_SOURCE_DIR})

#set_target_properties(pointcloud_lcm PROPERTIES SOVERSION 1)
#pods_install_headers(pointcloud_lcm.hpp DESTINATION pcl_tools)
target_link_libraries(pcl_utils boost_system)
