// pcl includes
#include <pcl/io/pcd_io.h>
#include <pcl/PointIndices.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/random_sample.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>

// opencv includes
#include <opencv2/opencv.hpp>

#include <fovis2/fovis.hpp>

// Feature types
#include "feature_types.hpp"

namespace spvision { 
   class FOVISTracker { 
       // // Max number of features being tracked
       // const int MAX_COUNT = 500;
       // const int MAX_TRACK_TIMESPAN = 0.5f; // in seconds
       // const int MAX_TRACK_TIMESTAMP_DELAY = 1.f; // in seconds
       // const int MAX_FRAME_QUEUE_SIZE = 2; 

    private: 
       fovis::Fast3DTracker* tracker; 
       fovis::Rectification* rect;

       // This needs to be updated if the tracks are updated via timestamps
       bool VERBOSE_MODE; 

       cv::Mat pimg; 

       bool _inited; 

       fovis::CameraIntrinsicsParameters rgb_params;
       fovis::DepthImage* depth_image; 

       void convertToTracks(); 
   public: 
       TrackMap tracklet;

    public: 
       FOVISTracker();
       FOVISTracker(const FOVISTracker& tracker);
       ~FOVISTracker();       
       
       bool inited() { return _inited; };

       void init(); 
       
       void reinit();

       void initTracker();

       void setParams(char* str, double value); 
       void setDefaultParams(); 

       // template <typename T>
       // normals size and tree size should be same
       void update(int64_t utime, cv::Mat& img, 
                   cv::Mat& depth, 
                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, 
                   pcl::PointCloud<pcl::Normal>::Ptr& normals,
                   pcl::search::KdTree<pcl::PointXYZRGB>::Ptr& tree);

       // std::vector<SuperFeature>
       // convertToFeatures(int64_t utime, const TSimpleFeatureList& _feats, 
       //                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud,                     
       //                   pcl::PointCloud<pcl::Normal>::Ptr& normals, 
       //                   pcl::search::KdTree<pcl::PointXYZRGB>::Ptr& tree);

       void
       normalEstimation(std::vector<SuperFeature>& feats, 
                        const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, 
                        const std::vector<int>& inds);           

       void setVerbose(bool mode) { VERBOSE_MODE = mode; }
   }; 

}
