//Subscribe to place_classification_t messages and apply HMM.
//Output: HMM_place_classification channel & HMM_place_classification_debug_t channel


#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>
#include <math.h>

#include <stdlib.h>
#include <stdio.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>

#define NO_CLASS 8
#define ANNOTATION_BIN_SIZE 100

typedef struct
{
  lcm_t      *lcm;
  GMainLoop *mainloop;
  erlcm_place_classification_t *last_classification;
  bot_core_pose_t *last_pose;   //Most recent pose
  bot_core_pose_t *pose_at_last_classification;    //pose at last classification utime
  bot_core_pose_t *first_pose;   //First pose message

  FILE *f0;  //File for observation histograms
  FILE *f1;  //File for stationary transition model
  FILE *f2; //File for moving transition model
  FILE *f3;
  double p[NO_CLASS][NO_CLASS];  //Probability from histogram
  double transition[NO_CLASS][NO_CLASS][2]; //Probability of transition model
  double alter_transition[NO_CLASS][NO_CLASS][3];  //Another option of transition model
  double belief[NO_CLASS];

  int transition_option;    //Determine to use three_class transition model or 2 class transition model collected from data. 
  int histogram_option; //Determine use handcraft or data collected histograms

  int num_annotations;
  int correct;
  int total;
  BotPtrCircular *annotation_list;
  int doorway;

} state_t;


double *compute_histogram(FILE *file){
  double *p = (double *) calloc(NO_CLASS * NO_CLASS, sizeof(double));
  GList **class = NULL;
  class = (GList **) calloc(NO_CLASS, sizeof(GList *));
  char str[5000];
  while(fgets(str,sizeof(str),file) != NULL){
    int len = strlen(str)- 1;
    if(str[len] == '\n')
      str[len] = 0;
    int class_id = atoi(&str[0]) - 1;
    if(class_id >= NO_CLASS){
      fprintf(stderr, "Class id exceeds no of classes.\n");
    }
    class[class_id] = g_list_prepend(class[class_id], strdup(str));
  }
	
  int d[NO_CLASS];
  for(int i = 0;i < NO_CLASS;i++){
    d[i] = (int)ceil(g_list_length(class[i]) / 100);
  }

  //Probability distribution of histograms
  int result[NO_CLASS][NO_CLASS];
  for(int i = 0;i < NO_CLASS;i++){
    for(int j = 0;j < NO_CLASS;j++){
      result[i][j] = 0;
    }
  }
  for(int i = 0;i < NO_CLASS;i++){
    int length = g_list_length(class[i]);
    for(int j = 0; j < length;j++){
      if(j % d[i] == 0){
	char *s = (char *)g_list_nth(class[i], j)->data;
	int actual_class = atoi(&s[0]) - 1;
	int predict_class = atoi(&s[2]) - 1;
	result[actual_class][predict_class]++;
      }
    }
  }
  free(class);
  for(int i = 0; i < NO_CLASS;i++){
    int count = 0;
    for(int j = 0;j < NO_CLASS;j++){
      count += result[i][j];
    }
    for(int j = 0;j < NO_CLASS;j++){
      int index = i * NO_CLASS + j;
      p[index] = (double)result[i][j] / count;
    }
  }
  fclose(file);
  return p;
}


double *compute_transition(FILE *file1, FILE *file2, FILE *file3, int trans){
  double *p = (double *) calloc(NO_CLASS * NO_CLASS * 2, sizeof(double));
  double *alter = (double *)calloc(NO_CLASS * NO_CLASS * 3, sizeof(double));

  //Transition model from data collected
  if(!trans){
    fprintf(stderr, "Using two class transition model.\n");
    int stationary_count[NO_CLASS][NO_CLASS];
    int move_count[NO_CLASS][NO_CLASS];
    int stat_total[NO_CLASS];
    int move_total[NO_CLASS];
    for(int i = 0;i < NO_CLASS * NO_CLASS * 2;i++)
      p[i] = 0;
    for(int i = 0;i < NO_CLASS;i++){
      for(int j = 0;j < NO_CLASS;j++){
	stationary_count[i][j] = 0;
	move_count[i][j] = 0;
      }
      stat_total[i] = 0;
      move_total[i] = 0;
    }

    char str[5000];
    //Read from stationary model
    while(fgets(str,sizeof(str),file1) != NULL){
      int len = strlen(str)- 1;
      if(str[len] == '\n')
	str[len] = 0;
      //Get the start class
      int start_class = atoi(&str[0]) - 1;
      int end_class = atoi(&str[2]) - 1;
      if(start_class >= NO_CLASS || end_class >= NO_CLASS){
	fprintf(stderr, "Class id exceeds no of classes.\n");
      }
      stationary_count[start_class][end_class]++;
      stat_total[start_class]++;
    }

    //Read from dynamic model
    while(fgets(str,sizeof(str),file2) != NULL){
      int len = strlen(str)- 1;
      if(str[len] == '\n')
	str[len] = 0;
      //Get the start class
      int start_class = atoi(&str[0]) - 1;
      int end_class = atoi(&str[2]) - 1;
      if(start_class >= NO_CLASS || end_class >= NO_CLASS){
	fprintf(stderr, "Class id exceeds no of classes.\n");
      }
      move_count[start_class][end_class]++;
      move_total[start_class]++;
    }

    //Do add-1 smoothing
    for(int i = 0;i < NO_CLASS;i++){
      for(int j = 0;j < NO_CLASS;j++){
	p[i * NO_CLASS * 2 + j * 2] = (double)(stationary_count[i][j] + 1) / (stat_total[i] + NO_CLASS);
	p[i * NO_CLASS * 2 + j * 2 + 1] = (double)(move_count[i][j] + 1)/ (move_total[i] + NO_CLASS);
      }
    }
    fclose(file1);
    fclose(file2);
    return p;
  }

  //three class transition model(stationary, slow_motion, fast_motion)
  else{
    fprintf(stderr, "Using three class transition model.\n");
    int stationary_count[NO_CLASS][NO_CLASS];
    int slow_move_count[NO_CLASS][NO_CLASS];
    int fast_move_count[NO_CLASS][NO_CLASS];
    int stat_total[NO_CLASS];
    int slow_move_total[NO_CLASS];
    int fast_move_total[NO_CLASS];
    for(int i = 0;i < NO_CLASS * NO_CLASS * 2;i++)
      p[i] = 0;
    for(int i = 0;i < NO_CLASS;i++){
      for(int j = 0;j < NO_CLASS;j++){
	stationary_count[i][j] = 0;
	slow_move_count[i][j] = 0;
	fast_move_count[i][j] = 0;
      }
      stat_total[i] = 0;
      slow_move_total[i] = 0;
      fast_move_total[i] = 0;
    }

    char str[5000];
    //Read from stationary model
    while(fgets(str,sizeof(str),file1) != NULL){
      int len = strlen(str)- 1;
      if(str[len] == '\n')
	str[len] = 0;
      //Get the start class
      int start_class = atoi(&str[0]) - 1;
      int end_class = atoi(&str[2]) - 1;
      if(start_class >= NO_CLASS || end_class >= NO_CLASS){
	fprintf(stderr, "Class id exceeds no of classes.\n");
      }
      stationary_count[start_class][end_class]++;
      stat_total[start_class]++;
    }

    //Read from slow_motion model
    while(fgets(str,sizeof(str),file2) != NULL){
      int len = strlen(str)- 1;
      if(str[len] == '\n')
	str[len] = 0;
      //Get the start class
      int start_class = atoi(&str[0]) - 1;
      int end_class = atoi(&str[2]) - 1;
      if(start_class >= NO_CLASS || end_class >= NO_CLASS){
	fprintf(stderr, "Class id exceeds no of classes.\n");
      }
      slow_move_count[start_class][end_class]++;
      slow_move_total[start_class]++;
    }

    //Read from fast_motion model
    while(fgets(str,sizeof(str),file3) != NULL){
      int len = strlen(str)- 1;
      if(str[len] == '\n')
	str[len] = 0;
      //Get the start class
      int start_class = atoi(&str[0]) - 1;
      int end_class = atoi(&str[2]) - 1;
      if(start_class >= NO_CLASS || end_class >= NO_CLASS){
	fprintf(stderr, "Class id exceeds no of classes.\n");
      }
      fast_move_count[start_class][end_class]++;
      fast_move_total[start_class]++;
    }

    //Do add-1 smoothing
    for(int i = 0;i < NO_CLASS;i++){
      for(int j = 0;j < NO_CLASS;j++){
	alter[i * NO_CLASS * 3 + j * 3] = (double)(stationary_count[i][j] + 1) / (stat_total[i] + NO_CLASS);
	alter[i * NO_CLASS * 3 + j * 3 + 1] = (double)(slow_move_count[i][j] + 1)/ (slow_move_total[i] + NO_CLASS);
	alter[i * NO_CLASS * 3 + j * 3 + 2] = (double)(fast_move_count[i][j] + 1)/ (fast_move_total[i] + NO_CLASS);
      }
    }
    fclose(file1);
    fclose(file2);
    fclose(file3);
    return alter;
  }
}



static void
on_classification(const lcm_recv_buf_t *rbuf, const char *channel,
		  const erlcm_place_classification_t *msg, void *user)
{
  state_t *self = (state_t *)user;
  if(self->last_classification != NULL)
    erlcm_place_classification_t_destroy(self->last_classification);
  
  self->last_classification = erlcm_place_classification_t_copy(msg);
 
  /*
  if(self->publish){

  }
  */
  double classification_prob[NO_CLASS];
  for(int i = 0;i < NO_CLASS;i++){
    classification_prob[i] = self->last_classification->classes[i].probability;
  }

  double observation_model[NO_CLASS];
  //Use the histograms collected from data and use kld. 
  if(!self->histogram_option){
    double kld[NO_CLASS];
    //Use KL_divergence to measure similarity
    for(int i = 0;i < NO_CLASS;i++){
      double value = 0;
      for(int j = 0;j < NO_CLASS;j++){
	//  fprintf(stderr, "Weird: %f\n", (double)classification_prob[j] / (p[i][j] + 0.0001));
	double v = classification_prob[j] * log2((double)classification_prob[j] /(self->p[i][j] + 0.0001));
	value += v;
      }
      kld[i] = value;
    }
  
    //compute observation model, e to the pow of -sqrt(abs(kld)).
    for(int i = 0;i < NO_CLASS;i++){
      observation_model[i] = pow(M_E, (0 - fabs(kld[i])));
    }
  }
  //only use histograms as the observation model, do not use SVM's probability
  else{
    char *classes[NO_CLASS];
    classes[0] = "elevator";
    classes[1] = "conference_room";
    classes[2] = "office";
    classes[3] = "lab";
    classes[4] = "open_area";
    classes[5] = "hallway";
    classes[6] = "corridor";
    classes[7] = "large_meeting_room";
    int predict_class;
    for(int i = 0;i < NO_CLASS;i++){
      if(strcmp(self->last_classification->label, classes[i]) == 0)
	predict_class = i;
    }
    for(int i = 0;i < NO_CLASS;i++){
      observation_model[i] = self->p[i][predict_class];
    }
  }

  
  //Determine which transition model to use(stationary or moving)
  int transition_model;
  double current_pose[2];
  double prev_pose[2];
  
  if(!self->last_pose)
    return;
  current_pose[0] = self->last_pose->pos[0];
  current_pose[1] = self->last_pose->pos[1];
  if(!self->pose_at_last_classification){     //If pose_at_last_classification is NULL, means at the initial state
    prev_pose[0] = self->first_pose->pos[0];
    prev_pose[1] = self->first_pose->pos[1];
  }
  else{
    prev_pose[0] = self->pose_at_last_classification->pos[0];
    prev_pose[1] = self->pose_at_last_classification->pos[1];
  }
  self->pose_at_last_classification = bot_core_pose_t_copy(self->last_pose);
  
  if(!self->transition_option){
    if(hypot(current_pose[0] - prev_pose[0], current_pose[1] - prev_pose[1]) < 0.3)
      transition_model = 0;
    else
      transition_model = 1;
  }
  else{
    if(hypot(current_pose[0] - prev_pose[0], current_pose[1] - prev_pose[1]) < 0.1)
      transition_model = 0;
    else if(hypot(current_pose[0] - prev_pose[0], current_pose[1] - prev_pose[1]) >= 0.1 && hypot(current_pose[0] - prev_pose[0], current_pose[1] - prev_pose[1]) <= 0.3)
      transition_model = 1;
    else
      transition_model = 2;
  }
  
  
  double current_belief[NO_CLASS];
  double belief_sum = 0;
  double transition_model_value[NO_CLASS];
  for(int i = 0;i < NO_CLASS;i++){
    transition_model_value[i] = 0;
    for(int j = 0;j < NO_CLASS;j++){
      //Transition model
      if(!self->transition_option)
	transition_model_value[i] += self->transition[j][i][transition_model] * self->belief[j];
      else
	transition_model_value[i] += self->alter_transition[j][i][transition_model] * self->belief[j];
    }
    //Update belief, Add small constant to observation model
    current_belief[i] = (observation_model[i] + 0.00001) * transition_model_value[i];
    belief_sum += current_belief[i];
  }

  //Store a copy of last belief to publish debug messages later
  double previous_belief_value[NO_CLASS];
  for(int i = 0;i < NO_CLASS;i++)
    previous_belief_value[i] = self->belief[i];


  //Normalize & update belief for next HMM. 
  for(int i = 0;i < NO_CLASS;i++){
    self->belief[i] = (double)current_belief[i] / belief_sum;
  }

  //Publish the HMM_place_classification messages & HMM_place_classification_debug message
  erlcm_place_classification_t message;
  erlcm_HMM_place_classification_debug_t debug_msg;
  message.utime = bot_timestamp_now();
  char *label;
  char *classes[NO_CLASS];
  classes[0] = "elevator";
  classes[1] = "conference_room";
  classes[2] = "office";
  classes[3] = "lab";
  classes[4] = "open_area";
  classes[5] = "hallway";
  classes[6] = "corridor";
  classes[7] = "large_meeting_room";


  //If include doorway class & place classification messages predicts doorway
  if(self->doorway && strcmp(self->last_classification->label, "doorway") == 0){
    fprintf(stderr, "HMM found a possible doorway.\n");
    message.label = "doorway";
    message.pose_utime = self->last_classification->pose_utime;
    message.sensor_utime = message.pose_utime;
    message.sensor_type = self->last_classification->sensor_type;
    message.no_class = self->last_classification->no_class;
    message.classes = (erlcm_place_classification_class_t *)calloc(message.no_class, sizeof(erlcm_place_classification_class_t));
    for(int i = 0; i < message.no_class - 1;i++){
      message.classes[i].name = i + 1;
      message.classes[i].probability = self->belief[i];
    }
    //only set the doorway class probability to 1.
    message.classes[message.no_class - 1].name = message.no_class;
    message.classes[message.no_class - 1].probability = 1;
  }
  //else
  else{
    double highest = -1;
    int index = -1;
    for(int i = 0;i < NO_CLASS;i++){
      if(self->belief[i] > highest){
	highest = self->belief[i];
	index = i;
      }
    }

    message.label = classes[index];
    message.pose_utime = self->last_classification->pose_utime;
    message.sensor_utime = message.pose_utime;
    message.sensor_type = self->last_classification->sensor_type;
    message.no_class = self->last_classification->no_class;
    message.classes = (erlcm_place_classification_class_t *)calloc(message.no_class, sizeof(erlcm_place_classification_class_t));
    for(int i = 0; i < message.no_class;i++){
      message.classes[i].name = i + 1;
      message.classes[i].probability = self->belief[i];
    }
  }
  //Check whether this is a correct prediction
  if(self->num_annotations){
    char *real_class;
    for(int i = 0;i < bot_ptr_circular_size(self->annotation_list);i++){
      erlcm_annotation_t *iter = (erlcm_annotation_t *)bot_ptr_circular_index(self->annotation_list, i);
      int64_t start_utime = iter->start_utime;
      int64_t end_utime = iter->end_utime;
      if(start_utime <= message.sensor_utime && end_utime >= message.sensor_utime){
	real_class = iter->annotation;
	break;
      }
    }
    if(strcmp(real_class, message.label) == 0){
      fprintf(stderr, "Correct prediction: %s\n", real_class);
      self->correct++;
    }
    else{
      fprintf(stderr, "Wrong prediction. Prediction by classfier: %s, Real: %s\n" ,message.label, real_class);
    }
    self->total++;
    fprintf(stderr, "Accuracy so far: %.2f\n", (double)self->correct / self->total);
  }

  debug_msg.utime = message.utime;
  debug_msg.sensor_utime = message.sensor_utime;
  debug_msg.no_class = message.no_class;
  debug_msg.label = message.label;
  debug_msg.model_type = transition_model;

  debug_msg.prior = (erlcm_place_classification_class_t *)calloc(debug_msg.no_class, sizeof(erlcm_place_classification_class_t));
  debug_msg.transition = (erlcm_place_classification_class_t *)calloc(debug_msg.no_class, sizeof(erlcm_place_classification_class_t));
  debug_msg.observation = (erlcm_place_classification_class_t *)calloc(debug_msg.no_class, sizeof(erlcm_place_classification_class_t));
  debug_msg.without_HMM = (erlcm_place_classification_class_t *)calloc(debug_msg.no_class, sizeof(erlcm_place_classification_class_t));
  
  for(int i = 0; i < message.no_class;i++){
    debug_msg.prior[i].name = i + 1;
    debug_msg.prior[i].probability = previous_belief_value[i];
    debug_msg.transition[i].name = i + 1;
    debug_msg.transition[i].probability = transition_model_value[i];
    debug_msg.observation[i].name = i + 1;
    debug_msg.observation[i].probability = observation_model[i];
    debug_msg.without_HMM[i].name = i + 1;
    debug_msg.without_HMM[i].probability = self->last_classification->classes[i].probability;
  }
  erlcm_place_classification_t_publish(self->lcm, "HMM_PLACE_CLASSIFICATION", &message);
  erlcm_HMM_place_classification_debug_t_publish(self->lcm, "HMM_PLACE_CLASSIFICATION_DEBUG", &debug_msg);
}


static void
on_pose(const lcm_recv_buf_t *rbuf, const char *channel,
	const bot_core_pose_t *msg, void *user){
  state_t *self = (state_t *)user;
  if(self->last_pose != NULL)
    bot_core_pose_t_destroy(self->last_pose);
  self->last_pose = bot_core_pose_t_copy(msg);

  //Update the first pose message in the log
  if(self->first_pose == NULL)
    self->first_pose = bot_core_pose_t_copy(msg);
}


void subscribe_to_channels(state_t *self)
{
  erlcm_place_classification_t_subscribe(self->lcm, "PLACE_CLASSIFICATION", on_classification, self);
  bot_core_pose_t_subscribe(self->lcm, "POSE", on_pose, self);
}  


void
circ_free_annotation(void *user, void *p){
  erlcm_annotation_t *np = (erlcm_annotation_t *)p;
  erlcm_annotation_t_destroy(np);
}


static void usage(const char* progname)
{
  fprintf (stderr, "Usage: %s [options]\n"
	   "\n"
	   "Options:\n"
	   "  -o option  Specify observation histogram option. 0:KL divergence 1:histograms directly\n"
	   "  -l LOG     Specify Log File\n"
	   "  -d         Consider doorways.\n"
	   "  -t option  Specify transition model option. 0:stationary,motion 1:stationary,slow_motion,fast_motion\n"
	   "  -h         This help message\n", 
	   g_path_get_basename(progname));
  exit(1);
}


int 
main(int argc, char **argv)
{

  const char *optstring = "o:l:pdt:h";

  int c;
  int histogram = 1;
  int transition = 0;
  int doorway = 0;
  char *log_file = NULL;

  while ((c = getopt (argc, argv, optstring)) >= 0) {
    switch (c) {
    case 'o': //choose observation model
      histogram = atoi(optarg);
      printf("Using histogram option \"%d\"\n", histogram);
      break;
    case 'l':
      log_file = strdup(optarg);  //choose log file for accuracy checking
      fprintf(stderr, "Using LOG file \"%s\"\n", log_file);
      break;
    case 'd':
      doorway = 1;
      fprintf(stderr, "Doorway option set to :%d.\n", doorway);
      break;
    case 't': //choose tranistion model
      transition = atoi(optarg);  //choose transition model
      printf("Using transition option \"%d\"\n", transition);
      break;
    case 'h': //help
      usage(argv[0]);
      break;
    default:
      usage(argv[0]);
      break;
    }
  }

  state_t *state = (state_t *) calloc(1, sizeof(state_t));

  //this does not attach the lcm to glib mainloop - so we need to do this mannully
  state->lcm =  bot_lcm_get_global(NULL);

  subscribe_to_channels(state);
  
  state->mainloop = g_main_loop_new( NULL, FALSE );  
  
  if (!state->mainloop) {
    printf("Couldn't create main loop\n");
    return -1;
  }

  state->histogram_option = histogram;
  state->transition_option = transition;
  state->doorway = doorway;

  if(!state->transition_option){
    state->f0 = fopen("observation_model.txt", "r");
    state->f1 = fopen("stationary_model.txt", "r");
    state->f2 = fopen("move_model.txt", "r");
    if(!state->f0 || !state->f1 || !state->f2){
      fprintf(stderr, "Cannot open file.\n");
      return -1;
    }
  }
  else{
    state->f0 = fopen("observation_model.txt", "r");
    state->f1 = fopen("3_stationary.txt", "r");
    state->f2 = fopen("3_slow.txt", "r");
    state->f3 = fopen("3_fast.txt", "r");
    if(!state->f0 || !state->f1 || !state->f2 || !state->f3){
      fprintf(stderr, "Cannot open file.\n");
      return -1;
    }
  }

  
  //Compute the histogram
  if(!state->histogram_option)
    fprintf(stderr, "Using KL divergence function for observation model.\n");  
  else
    fprintf(stderr, "Using histograms directly as observation model.\n");
    
  double *v = compute_histogram(state->f0);
  for(int i = 0;i < NO_CLASS;i++){
    for(int j = 0;j < NO_CLASS;j++){
      state->p[i][j] = v[i * NO_CLASS + j];
    }      
  }

  //Compute the transition probability
  double *trans = compute_transition(state->f1, state->f2, state->f3, state->transition_option);
  if(!state->transition_option){
    for(int i = 0;i < NO_CLASS;i++){
      for(int j = 0;j < NO_CLASS;j++){
	state->transition[i][j][0] = trans[i * NO_CLASS * 2 + j * 2];
	state->transition[i][j][1] = trans[i * NO_CLASS * 2 + j * 2 + 1];
      }
    }
  }
  //(stationary, slow_motion, fast_motion) transition model
  else{
    for(int i = 0;i < NO_CLASS;i++){
      for(int j = 0;j < NO_CLASS;j++){
	state->alter_transition[i][j][0] = trans[i * NO_CLASS * 3 + j * 3];
	state->alter_transition[i][j][1] = trans[i * NO_CLASS * 3 + j * 3 + 1];
	state->alter_transition[i][j][2] = trans[i * NO_CLASS * 3 + j * 3 + 2];
      }
    }
  }


  //Initialize the belief as uniform distribution
  for(int i = 0;i < NO_CLASS;i++){
    state->belief[i] = (double)1 / NO_CLASS;
  }
    
  state->last_pose = NULL;
  state->pose_at_last_classification = NULL;
  state->first_pose = NULL; 

  state->num_annotations = 0;
  //Load log into memory for accuracy checking
  if(log_file){
    if(state->doorway)
      fprintf(stderr, "HMM includes doorway class. Ensure your annotation log has doorways.\n");
    fprintf(stderr, "System is loading the annotation messages in log for accuracy checking.Please wait a while.\n");
    lcm_eventlog_t *read_log = lcm_eventlog_create(log_file, "r");
    fprintf(stderr, "Log open success.\n");
    state->annotation_list = bot_ptr_circular_new(ANNOTATION_BIN_SIZE, circ_free_annotation, state);

    state->correct = 0;
    state->total = 0;
  
    //Adding to annotation circular buffer
    lcm_eventlog_event_t *event = (lcm_eventlog_event_t *)calloc(1, sizeof(lcm_eventlog_event_t));
    char *annotation_channel_name = "LOG_ANNOTATION";
    for(event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){
      int decode_status = 0;
      if(strcmp(annotation_channel_name, event->channel) == 0){
	erlcm_annotation_t *msg = (erlcm_annotation_t *)calloc(1, sizeof(erlcm_annotation_t));
	decode_status = erlcm_annotation_t_decode(event->data, 0, event->datalen, msg);
	if(decode_status < 0){
	  fprintf(stderr, "Error decoding message.\n");
	  return -1;
	}
      
	bot_ptr_circular_add(state->annotation_list, msg);
	state->num_annotations++;
      }
    }
    fprintf(stderr, "No. of annotations in the log: %d\n", state->num_annotations);
    free(event);
    if(state->num_annotations)
      fprintf(stderr, "Finish loading annotations in the log.\n");
    else
      fprintf(stderr, "No annotation messages in the log.\n");
  }


  //add lcm to mainloop 
  bot_glib_mainloop_attach_lcm (state->lcm);

  //read_parameters_from_conf(state);

  //adding proper exiting 
  bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
  //    fprintf(stderr, "Starting Main Loop\n");

  ///////////////////////////////////////////////
  g_main_loop_run(state->mainloop);
  
  bot_glib_mainloop_detach_lcm(state->lcm);
}
