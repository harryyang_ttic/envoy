
#if !defined( __PTP_SENSORMODELS_sensor_model_t_HPP__ )
#define __PTP_SENSORMODELS_sensor_model_t_HPP__

#include <ptp-object-detectors/common.hpp>
#include <ptp-common/matrix.hpp>

namespace ptp {
  namespace sensormodels {

    using namespace ptp::coordinates;
    using namespace ptp::detectors;
    using namespace ptp::common;


    //===============================================================

    // Description:
    // A simple structure which encodes a mean and variance
    struct mean_variance_t
    {
      float mean;
      float var;
    };

    //===============================================================

    // Description:
    // Shared pointer typedef
    class sensor_model_t;
    typedef boost::shared_ptr< sensor_model_t > sensor_model_tp;


    // Description:
    // This class encapsulates a sensor model for an object detector.
    // It allows to query for the probability distribution of a 
    // detection at a given location.
    // Currently these models are gaussian, so the returned
    // 'distributions' consist of a mean and variance
    class sensor_model_t
    {
    public:

      sensor_model_t() {}
      virtual ~sensor_model_t() {}

      // Description:
      // Retruns the distribution ( as mean and variance ) for
      // the detector output given a location.
      // We also give the probability of an object existing
      // ( this is the belief y in our paper )
      // This DOES NOT UPDATE the sensor model.
      virtual mean_variance_t query_no_update( const dist_and_orientation_t& x, const float& object_exists_p ) const = 0;

      // Description:
      // Updates the sensor model with the
      // location and detection for the next call to query*
      virtual void update( const detection_and_location_t& dx ) = 0;


      // Description:
      // Returns the derivative of teh exponential family cacnonical parameters
      // for this sensor model centerd at the given belief
      virtual math_vector_t gradient_of_theta_about( const float& state, const dist_and_orientation_t& x, const bool& use_independent_model_only = false ) const = 0;

      // Description:
      // For the simulator, returns the precition for the object
      // existing or not 
      // and returns the probability of being independent (not correlated)
      virtual void query_separate( const dist_and_orientation_t& x, 
				   const float& exists,
				   mean_variance_t& mv_ind,
				   mean_variance_t& mv_corr, 
				   float& prob_independent ) const = 0;
      
      // Description:
      // Some friends
      friend class learned_sensor_models;

      // Description:
      // Dumsp the internal GPs to files prefix with given
      // string
      virtual void dump_gps( const std::string& prefix ) const
      {}

      // Description:
      // The ligistic alpha multiplication constant
      float param_b;

      // Description:
      // Clones this sensor model.
      virtual sensor_model_tp clone() const
      { return sensor_model_tp(); }


    protected:


    };

    
    //===============================================================

    // Description:
    // IO operator for mean_var
    std::ostream& operator<< ( std::ostream& os, const mean_variance_t& mv );

    //===============================================================


  }
}

#endif

