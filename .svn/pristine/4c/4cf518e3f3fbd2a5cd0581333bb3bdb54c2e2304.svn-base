// spvision::KLT Tracker

// pcl includes
#include <pcl/io/pcd_io.h>
#include <pcl/PointIndices.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/random_sample.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/features/normal_3d.h>

// opencv includes
#include <opencv2/opencv.hpp>

// feature types
#include "feature_types.hpp"

namespace spvision { 
    // Max number of features being tracked
    const int MAX_COUNT = 500;
    const int MAX_TRACK_TIMESPAN = 4.f; // in seconds
    const int MAX_TRACK_TIMESTAMP_DELAY = 3.f; // in seconds
    const int MAX_FRAME_QUEUE_SIZE = 2; 

   class TrackerKLT { 
    private: 
       // CGenericFeatureTrackerAutoPtr  tracker;

       // This needs to be updated if the tracks are updated via timestamps
       // TSimpleFeatureList  simple_feats;
       std::vector<cv::Point> features; 
       bool VERBOSE_MODE; 

       cv::Mat pimg; 
       bool _inited; 
       void convertToTracks(); 
   public: 

       TrackMap tracklet;
       std::map<std::string, double> params; 

    public: 
       TrackerKLT();
       TrackerKLT(const TrackerKLT& tracker);
       ~TrackerKLT();       
       
       bool inited() { return _inited; };

       void init(); 
       
       void reinit();

       void initTracker();

       void setParams(char* str, double value); 
       void setDefaultParams(); 

       // template <typename T>
       void update(int64_t utime, cv::Mat& img, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud);

       std::vector<SuperFeature>
       convertToFeatures(int64_t utime, const TSimpleFeatureList& _feats, 
                           pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud);

       void setVerbose(bool mode) { VERBOSE_MODE = mode; }
   }; 

}
