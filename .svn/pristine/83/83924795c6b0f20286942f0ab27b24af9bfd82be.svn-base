
#if !defined( __PTP_TEXTDETECTOR_text_detector_t_HPP__ )
#define __PTP_TEXTDETECTOR_text_detector_t_HPP__

#include <leget/TextSpotter.h>
#include <ptp-stereo/stereo_image_t.hpp>
#include "text_detection_t.hpp"

namespace ptp {
  namespace detectors {


    //===============================================================
    
    namespace textdetector {

      using namespace ptp::coordinates;
      using namespace ptp::stereo;


      //===============================================================

      // Description:
      // Parameters for text detector
      // Default constructor build params with default values
      struct text_detector_params_t
      {
	double vote_threshold;
	int min_area_threshold;
	double alpha;
	unsigned int num_scales;
	unsigned int base_window_width;
	unsigned int base_window_height;
	unsigned int x_step;
	unsigned int y_step;
	unsigned int rescale_size_x;
	unsigned int rescale_size_y;
	double threshold;

	text_detector_params_t();
	
      };

      //===============================================================

      // Description:
      // A text detector which uses Leget to detect text in a 
      // stereo image
      class text_detector_t
      {
      public:

	// Description:
	// Create a default text detector
	text_detector_t();
	
	
	// Description:
	// Processes a given stereo image and results in a detection list
	text_detection_list_t process( const stereo_image_t& image ) const;

	// Description:
	// Sets the parameters for this detector
	void set_params( const text_detector_params_t& p );

      protected:

	// Description:
	// The leget text spotter.
	// It is mutable because we will use it as a one shot
	// even though it is not. Currently the "state" of the text spotter
	// changes on each detection, but we use it as a once shot hence
	// we decided to make it mutable and make our process call const.
	mutable boost::shared_ptr<leget::TextSpotter> _text_spotter;

	// Description:
	// The current params
	text_detector_params_t _params;
      };

      //===============================================================

      //===============================================================

    }

  }
}

#endif

