// --------------------------------------------------------------
/** @file StrUtils.cpp
 ** Implementation of a few useful string utilities.
 **
 ** @brief Implementation of string utilities.
 **
 ** @author Ingmar Posner 
 ** @date 01/06/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#include <set>
#include <string>
#include "leget/misc/StrUtils.h"

namespace leget
{
    
    // --------------------------------------------------------------
    /**
     ** lStrip() strips token characters from the beginning of a string.
     ** @param[in]     rStr the string to operate on.
     ** @param[in]     rToken the token to strip away. [" "]
     ** @return        boolean: false if all characters had to be removed (rStr is cleared).
     */
    // --------------------------------------------------------------
    bool lStrip(std::string& rStr, const std::string& rToken)
    {
        std::size_t pos = rStr.find_first_not_of(rToken);
        if (pos == std::string::npos)
        {
            // all characters need to be removed...
            rStr.clear();
            return false;
        }
        else
        {
            rStr = rStr.substr(pos, rStr.length() - pos);
            return true;
        }    
    }
    
    
    // --------------------------------------------------------------
    /**
     ** rStrip() strips token characters from the end of a string.
     ** @param[in]     rStr the string to operate on.
     ** @param[in]     rToken the token to strip away. [" "]
     ** @return        boolean: false if all characters had to be removed (rStr is cleared).
     */
    // --------------------------------------------------------------
    bool rStrip(std::string& rStr, const std::string& rToken)
    {
        std::size_t pos = rStr.find_last_not_of(rToken);
        if (pos == std::string::npos)
        {
            // all characters need to be removed...
            rStr.clear();
            return false;
        }
        else
        {
            rStr = rStr.substr(0, pos+1);
            return true;
        }    
        
    }
    
    
    // --------------------------------------------------------------
    /**
     ** strip() strips spaces and tabs from beginning and end of a string.
     ** @param[in]     rStr the string to operate on.
     ** @param[in]     rToken the token to strip away. [" "]
     ** @return        boolean: false if all characters had to be removed (rStr is cleared).
     */
    // --------------------------------------------------------------
    bool strip(std::string& rStr, const std::string& rToken)
    {
        // strip from beginning && end
        return (lStrip(rStr, rToken) && rStrip(rStr, rToken));
    }
    
    
    
    // --------------------------------------------------------------
    /**
     ** split() splits a string into two parts by some token.
     ** @param[in]     rIn the string to operate on.
     ** @param[in]     rBefore the string preceeding the token.
     ** @param[in]     rAfter the string following the token.
     ** @param[in]     rToken the token by which to split the string. [" "]
     ** @return        boolean: false if no token was found. The original string is copied to sBefore.
     */
    // --------------------------------------------------------------
    bool split(const std::string& rIn, std::string& rBefore, std::string& rAfter, const std::string& rToken)
    {
        std::size_t pos = rIn.find_first_of(rToken);
        if ( pos == std::string::npos )
        {
            // no token, stick everything in 'before'
            rBefore = rIn;
            strip(rBefore);
            rAfter.clear();
            return false;
        }
        else
        {
            rBefore = rIn.substr(0, pos + 1);
            strip(rBefore);
            rAfter  = rIn.substr(pos+1, rIn.size() - (pos + 1));
            strip(rAfter);
            return true;
        }
    }
    
    
    // --------------------------------------------------------------
    /**
     ** rSplit() splits a string into two parts by some token starting from the END of the original string!
     ** @param[in]     rIn the string to operate on.
     ** @param[in]     rBefore the string preceeding the token.
     ** @param[in]     rAfter the string following the token.
     ** @param[in]     rToken the token by which to split the string. [" "]
     ** @return        boolean: false if no token was found. The original string is copied to sBefore.
     */
    // --------------------------------------------------------------
    bool rSplit(const std::string& rIn, std::string& rBefore, std::string& rAfter, const std::string& rToken)
    {
        std::size_t pos = rIn.find_last_of(rToken);
        if ( pos == std::string::npos )
        {
            // no token, stick everything in 'after'
            rBefore.clear();
            rAfter = rIn;
            strip(rAfter);
            return false;
        }
        else
        {
            rBefore = rIn.substr(0, pos);
            strip(rBefore);
            rAfter  = rIn.substr(pos, rIn.size() - (pos));
            strip(rAfter);
            return true;
        }
    }
    
    
    
    // --------------------------------------------------------------
    /**
     ** parseNameValuePair() parses a name:value pair.
     ** @param[in]     rIn the string to operate on.
     ** @param[in]     rBefore the string preceeding the token.
     ** @param[in]     rAfter the string following the token.
     ** @param[in]     rToken the token by which to split the string. [":"]
     ** @return        boolean: false if no token was found. The original string is copied to sName.
     */
    // --------------------------------------------------------------
    bool parseNameValuePair(std::string& rIn, std::string& rName, std::string& rValue, const std::string& rToken)
    {
        // let's start by stripping all whitespaces
        strip(rIn);
        
        // this is like the split function but the token is discarded
        std::size_t pos = rIn.find_first_of(rToken);
        if ( pos == std::string::npos )
        {
            // no token, stick everything in 'sName'
            rName = rIn;
            strip(rName);
            rValue.clear();
            return false;
        }
        else
        {
            rName = rIn.substr(0, pos);
            strip(rName);
            rValue  = rIn.substr(pos+1, rIn.size() - (pos + 1));
            strip(rValue);
            return true;
        }
    }
    
    
    
    // --------------------------------------------------------------
    /**
     ** replaceChars() replaces all occurances of a given char with another.
     ** @param[in]     rIn the string to operate on.
     ** @param[in]     rCharToReplace the char to replace.
     ** @param[in]     rReplacement the replacement.
     */
    // --------------------------------------------------------------
    void replaceChars(std::string& rIn, const std::string& rCharToReplace, const std::string& rReplacement)
    {
        std::size_t pos = rIn.find_first_of(rCharToReplace);
        while (pos != std::string::npos)
        {
            rIn.replace(pos, 1, rReplacement);
            
            pos = rIn.find_first_of(rCharToReplace);
        }
    }
    
    
    
    // --------------------------------------------------------------
    /**
     ** filterForAlphaNumeric() replace all chars that are not alphanumeric.
     ** @param[in]     sIn the string to operate on.
     */
    // --------------------------------------------------------------
    void filterForAlphaNumeric(const std::string rIn, 
                               std::string& rOut, 
                               const char& rRep)
    {
        rOut = rIn;
        for (unsigned int i = 0; i < rOut.size(); ++i)
        {
            if ( !isalnum(rOut[i]) )
            {
                rOut[i] = rRep;
            }
        }
    }
    
    
}
