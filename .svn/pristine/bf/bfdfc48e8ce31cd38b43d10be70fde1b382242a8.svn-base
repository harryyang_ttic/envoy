
#include <ptp-common-coordinates/translation_manifold_t.hpp>
#include <ptp-common-coordinates/identity_manifold_t.hpp>
#include <ptp-common-test/test.hpp>
#include <iostream>

using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;

//================================================================

int test_translation()
{
  start_of_test;

  // Create a new base manifold
  manifold_tp base = manifold_tp( new identity_manifold_t( manifold_t::BASE_MANIFOLD ) );
  
  // Create a set of translation manifolds and test that they work
  translation_manifold_tp t1 = translate( coordinate( 1, 2, 3 ).on( base ),
					  base );
  
  // Make sure the x, y, z axis of translated manifold are what we
  // expect them to be
  coordinate_t c1 = coordinate( 1, 0, 0 ).on( t1 );
  coordinate_t c1_true = coordinate( 2, 2, 3 ).on( base );
  coordinate_t c1_base = c1.on( base );
  coordinate_t c2 = coordinate( 0, 0, 1 ).on( t1 );
  coordinate_t c2_true = coordinate( 1, 2, 4 ).on( base );
  coordinate_t c2_base = c2.on( base );
  coordinate_t c3 = coordinate( 0, 1, 0 ).on( t1 );
  coordinate_t c3_true = coordinate( 1, 3, 3 ).on( base );
  coordinate_t c3_base = c3.on( base );
  test_assert( float_equal( c1_true, c1_base ) );
  test_assert( float_equal( c2_true, c2_base ) );
  test_assert( float_equal( c3_true, c3_base ) );

  // Make sure the base axis get mapped to the translation correctly
  coordinate_t c4 = coordinate( 1, 0, 0 ).on( base );
  coordinate_t c4_true = coordinate( 0, -2, -3 ).on( t1 );
  coordinate_t c4_t = c4.on( t1 );
  coordinate_t c5 = coordinate( 0, 0, 1 ).on( base );
  coordinate_t c5_true = coordinate( -1, -2, -2 ).on( t1 );
  coordinate_t c5_t = c5.on( t1 );
  coordinate_t c6 = coordinate( 0, 1, 0 ).on( base );
  coordinate_t c6_true = coordinate( -1, -1, -3 ).on( t1 );
  coordinate_t c6_t = c6.on( t1 );
  test_assert( float_equal( c4_true, c4_t ) );
  test_assert( float_equal( c5_true, c5_t ) );
  test_assert( float_equal( c6_true, c6_t ) );
  
  end_of_test;
}

//================================================================
//================================================================

int main( int argc, char** argv )
{

  run_test( test_translation() );

  return 0;
}
