
#include "rotation_t.hpp"
#include "matrix_util.hpp"

using namespace ptp;
using namespace ptp::coordinates;
using namespace ptp::common;

//==================================================================

rotation_t::rotation_t( const simplex_tp& simplex, const angle_t& angle )
  : _simplex( simplex ), _angle( angle )
{
  init( simplex, angle );
}

//==================================================================

void rotation_t::init( const simplex_tp& simplex, const angle_t& angle )
{
  _simplex = simplex;
  _angle = angle;
  _dimensionality = simplex->dimensionality() + 2;
  _matrix = generate_generalized_rotation_matrix( simplex, angle, _pre_translation_vector, _post_translation_vector );
}

//==================================================================

matrix_t rotation_t::main_rotation_matrix( const int& a, const int& b, const angle_t& theta ) const
{
  
  // Create a new NxN matrix
  matrix_t m( dimensionality(), dimensionality() );

  // Get the theta in radians from the x axis
  realf_t theta_rad = theta.from( angle_t::X_AXIS ).radians();
  
  // Fill to be a rotation about a plane from 
  // vector Xa to Xb ( hence the a, b integers as indices into axis )
  for( int i = 0; i < dimensionality(); ++i ) {
    for( int j = 0; j < dimensionality(); ++j ) {
      if( i == a && j == a ) {
	m(i,j) = cos( theta_rad );
      }
      else if( i ==  b && j == b ) {
	m(i,j) = cos( theta_rad ); 
      }
      else if( i == a && j == b ) {
	m(i,j) = -sin( theta_rad );
      } 
      else if( i == b && j == a ) {
	m(i,j) = sin( theta_rad );
      }
      else if( i == j && j != a && j != b ) {
	m(i,j) = 1;
      }
      else {
	m(i,j) = 0;
      }
    }
  }

  // return this matrix
  return m;
}

//==================================================================
    
matrix_t rotation_t::generate_generalized_rotation_matrix( const simplex_tp& simplex, const angle_t& theta, math_vector_t& pre_translation, math_vector_t& post_translation ) const
{
  
  // First, center the first axis ( first row ) of the simple to
  // be at the origin
  matrix_t v0 = create_matrix_from_simplex_points_col( simplex );
  matrix_t m1 = -1 * create_tiled_col_matrix( v0, 0 );
  math_vector_t translation_vector = v0.col( 0 );
  pre_translation = -1 * translation_vector;
  post_translation = translation_vector;

  // treat the dimensionality as n
  int n = dimensionality();

  
  matrix_t v1 = v0 + m1;
  matrix_t m = matrix_t::Identity(n,n);

  
  // Ok, create rotations to center subsequent axis 
  int k = 1;
  matrix_t v = v1;
  for( int r = 2; r <= n-1; ++r ) {
    for( int c = n; c >= r; --c ) {
      ++k;
      //angle_t inner_angle = rad( atan2( v(r-1,c-1), v(r-1,c-1-1)  ) ).from( *theta.reference() );
      angle_t inner_angle = rad( atan2( v(c-1,r-1), v(c-1-1,r-1)  ) ).from( *theta.reference() );
      matrix_t m_k = main_rotation_matrix( c - 1, c-1 - 1, inner_angle );
      v = m_k * v ;
      matrix_t m_temp = m_k * m;
      m = m_temp;
    }
  }

  // Now, compute the inverse of the rotation required to get
  // the simplex aligned with the axis
  FullPivLU<matrix_t> lu = m.fullPivLu();
  if( lu.isInvertible() == false ){
    BOOST_THROW_EXCEPTION( common::exceptions::matrix_invert_error() );
  }
  matrix_t m_inv = lu.inverse();

  // Ok, the rotation is as follows:
  //    Align symplex with axis
  //    Perform a planar rotation with now-aligned simplex held fixed
  //    Undo the aligned of simplex to get back to original
  // So, Rotation = M * PlanarRot * M^-1
  matrix_t main_r = main_rotation_matrix(n-1-1,n-1,theta.from(angle_t::X_AXIS));
  matrix_t temp = m_inv * main_r;
  matrix_t m_temp = temp * m;
  m = m_temp;

  // debug printout
  //std::cout << "Rotation Matrix: ";
  //print_matrix( m );
  //std::cout << "Translation (Pre/post): " << std::endl;
  //print_vector( pre_translation );
  //print_vector( post_translation );
  

  // return the rotation matrix
  return m;
}

//==================================================================

coordinate_t rotation_t::rotate_coordinate( const coordinate_t& a ) const
{
  // check that the coordinate is in the manifold for this rotation
  if( manifold() != a.manifold() ) {
    BOOST_THROW_EXCEPTION( exceptions::reference_manifolds_differ_error() );
  }

  // Turn into a math vector and multiply by rotation matrix
  // applying pre and post translations
  math_vector_t v = math_vector_from_raw_coordinate( a.raw_coordinate() );
  v += _pre_translation_vector;
  math_vector_t rot_v = _matrix * v;
  v += _post_translation_vector;
  
  return coordinate_t( raw_coordinate_from_math_vector( rot_v ), a.manifold() );
}

//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
