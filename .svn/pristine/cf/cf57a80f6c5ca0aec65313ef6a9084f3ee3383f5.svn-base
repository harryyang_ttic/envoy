#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <nodder/nodder_extractor.h>
#include <lcmtypes/erlcm_xyz_point_list_t.h>

#include <occ_map/VoxelMap.hpp>
#include <lcmtypes/occ_map_voxel_map_t.h>
#include <lcmtypes/erlcm_slam_full_graph_t.h>
#include <bot_lcmgl_client/lcmgl.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif 

#define NODDER_DATA_CIRC_SIZE 10

typedef struct _state_t state_t;

typedef struct {
    xyz_point_list_t *points;  
    int node_id; //we will use the node id to draw the map 
    double tf_pos[3]; 
    double theta;
} point_pose_t;

struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    BotFrames *frames; 
    BotParam *param;
    BotPtrCircular   *nodder_data_circ;
    GArray   *point_cloud_array;      
    GArray   *slam_cloud_array;      
    nodder_extractor_state_t *nodder;
    
    double p_pos[3];
    int p_size;
    int s_size;
    int verbose; 
    GMutex *mutex;
};

void
circ_free_nodder_data(void *user, void *p) {
    xyz_point_list_t *np = (xyz_point_list_t *) p; 
    destroy_xyz_list(np);
}

/*static void slam_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                           const char * channel __attribute__((unused)), 
                           const erlcm_slam_graph_full_graph_t * msg,
                           void * user  __attribute__((unused)))
{
    state_t *s = (state_t *)user;
    
    erlcm_slam_full_graph_node_t *last_node = &msg->nodes[msg->no_nodes-1];

    double time_gap = 10000000.0;
    int c_ind = -1;

    //we should make it a circular buffer
    fprintf(stderr, "Buffer size : %d\n" , s->p_size);
    for (int i=0; i<(s->p_size); i++) {
        xyz_point_list_t *points = g_array_index(s->point_cloud_array, xyz_point_list_t *, i);

        //xyz_point_list_t *points = bot_ptr_circular_index(self->nodder_data_circ, i);
        fprintf(stderr, "Points : %p Last Node : %p\n", (void *) points, (void *) last_node);
        if(fabs(last_node->utime - points->utime)/1.0e6 < time_gap){
            time_gap = fabs(last_node->utime - points->utime)/ 1.0e6;
            c_ind = i;
        }
    }

    fprintf(stderr, "Time gap %f -> ind :%d\n", time_gap, c_ind);

    if(c_ind == -1 || time_gap > 1.0){
        fprintf(stderr,"No Close node found\n");
        return;
    }
    
    point_pose_t *p_pose = (point_pose_t *) calloc(1, sizeof(point_pose_t));

    p_pose->points = g_array_index(s->point_cloud_array, xyz_point_list_t *, c_ind);
    p_pose->node_id = last_node->id;

    g_array_append_val (s->slam_cloud_array, p_pose);
    
    s->s_size++;
    
    for (int i=0; i<(s->p_size); i++) {
        xyz_point_list_t *points = g_array_index(s->point_cloud_array, xyz_point_list_t *, i);
        if(i==c_ind)
            continue;
        destroy_xyz_list(points);
        s->p_size = 0;
    }

    //not sure if this clears the data in it 
    g_array_free (s->point_cloud_array, TRUE);
    s->point_cloud_array =  g_array_new (FALSE, TRUE, sizeof (xyz_point_list_t *));
    
    //check and find the closest one 

    //check the prev list and find the closest match 
    //add to the permanant list and dump the cache
    bot_lcmgl_t *lcmgl = bot_lcmgl_init(s->lcm,"nodder_points");
    
    bot_lcmgl_color3f(lcmgl, 1, 0, 0);

    bot_lcmgl_point_size(lcmgl, 1.0);
    bot_lcmgl_begin(lcmgl, GL_POINTS);


    for (int i=0; i<(s->s_size); i++) {
        point_pose_t *p_list = g_array_index(s->slam_cloud_array, point_pose_t *, i);
        xyz_point_list_t *points = p_list->points;
        //find the size of the map based on the current position 
        //copy and convert 
        //we know the global position of the anchor node 
        //convert the points to the global position and then convert to the local position 
        //figure out how to convert directly 
        
        double pos_l[3];
        for (int k = 0; k < points->no_points; ++k){
            //bot_vector_affine_transform_3x4_3d (global_to_local, points->points[k].xyz, pos_l);
            //fvm.writeValue(ret->points[k].xyz,0.99);
            bot_lcmgl_vertex3f(lcmgl, pos_l[0], pos_l[1], pos_l[2]);
        }
    }

    bot_lcmgl_end(lcmgl);
    bot_lcmgl_switch_buffer(lcmgl);
    }*/

void get_rpy_frame(BotFrames *frame, int64_t utime, double rpy[3], char *from_frame, char *to_frame, double rpy_to_frame[3]){
    double quat_from_frame[4], quat_to_frame[4];
    bot_roll_pitch_yaw_to_quat(rpy, quat_from_frame);
    
    BotTrans from_to_trans;
    bot_frames_get_trans_with_utime (frame, from_frame, to_frame, utime, 
                                     &from_to_trans);
    
    bot_quat_mult (quat_to_frame, from_to_trans.rot_quat, quat_from_frame);
    bot_quat_to_roll_pitch_yaw (quat_to_frame, rpy_to_frame);

}

void nodder_cb(int64_t utime, void *data)
{
    state_t *s = (state_t *)data;
    g_mutex_lock(s->mutex);
    //fprintf(stderr, "Called \n"); 
    xyz_point_list_t *ret = nodder_extract_new_points_frame(s->nodder, "local");

    if(ret != NULL){
        if(s->verbose)
            fprintf(stderr, "Size of Points : %d \n", ret->no_points);

        double body_to_local[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                      "local",  ret->utime,
                                                      body_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            g_mutex_unlock(s->mutex);
            return;
        }

        double b_pos[3] = {0}, g_pos[3];
        bot_vector_affine_transform_3x4_3d (body_to_local, b_pos, g_pos);
        double dist = hypot(g_pos[0] - s->p_pos[0], g_pos[1] - s->p_pos[1]);
        if(dist< 2.0){
            //fprintf(stderr, "Skipping\n");
            destroy_xyz_list(ret);
            g_mutex_unlock(s->mutex);
            return;
        }
    
        fprintf(stderr, "Adding Dist traveled: %f\n", dist);
    
        memcpy(s->p_pos, g_pos, 3*sizeof(double));
        g_array_append_val (s->point_cloud_array, ret);
    
        //bot_ptr_circular_add (s->nodder_data_circ, ret);

        s->p_size++;

        //check the prev list and find the closest match 
        //add to the permanant list and dump the cache
        bot_lcmgl_t *lcmgl = bot_lcmgl_init(s->lcm,"nodder_points");
    
        bot_lcmgl_color3f(lcmgl, 1, 0, 0);

        bot_lcmgl_point_size(lcmgl, 1.0);
        bot_lcmgl_begin(lcmgl, GL_POINTS);


        for (int i=0; i<(s->p_size); i++) {
            xyz_point_list_t *points = g_array_index(s->point_cloud_array, xyz_point_list_t *, i);
            //fprintf(stderr, "Size : %d\n", points->no_points);
            for (int k = 0; k < points->no_points; ++k){
                bot_lcmgl_vertex3f(lcmgl, points->points[k].xyz[0],
                                   points->points[k].xyz[1],
                                   points->points[k].xyz[2]);
            }
        }

        bot_lcmgl_end(lcmgl);
        bot_lcmgl_switch_buffer(lcmgl);
    }
    g_mutex_unlock(s->mutex);
}    


int main(int argc, char **argv)
{
    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    state->lcm =  bot_lcm_get_global(NULL);

    state->param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->param);

    state->nodder = nodder_extractor_init_full(state->lcm, 1, -40, 40, NULL, 
					       &nodder_cb, state);

    state->point_cloud_array = g_array_new (FALSE, TRUE, sizeof (xyz_point_list_t *));
    
    state->slam_cloud_array = g_array_new (FALSE, TRUE, sizeof (point_pose_t *));

    state->nodder_data_circ = bot_ptr_circular_new (NODDER_DATA_CIRC_SIZE,
                                                    circ_free_nodder_data, state);
    
    if(state->nodder== NULL){
        fprintf(stderr,"Issue getting collector\n");
        return -1;
    }

    state->mainloop = g_main_loop_new( NULL, FALSE );  

    const char *optstring = "vh";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "verbose", no_argument, 0, 'v' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    //usage(argv[0]);
            fprintf(stderr, "No help found - please add help\n");
	    break;
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
                state->verbose = 1;
		break;
	    }
        }
    }
  
    if (!state->mainloop){
	printf("Couldn't create main loop\n");
	return -1;
    }

    state->mutex = g_mutex_new ();
    
    //erlcm_slam_graph_full_graph_t_subscribe(state->lcm, "SLAM_FULL_GRAPH", slam_handler, state);

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);
    
    /* heart beat*/
    //g_timeout_add (100, heartbeat_cb, state);
    
    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
    
    bot_glib_mainloop_detach_lcm(state->lcm);
}
