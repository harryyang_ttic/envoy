
#if !defined( __PTP_SENSORMODELS_sensor_models_t_HPP__ )
#define __PTP_SENSORMODELS_sensor_models_t_HPP__

#include "sensor_model_t.hpp"
#include <vector>

namespace ptp {
  namespace sensormodels {


    //============================================================

    // Description:
    // The sensor models. This structure will allow programs to query
    // for the individual object models using ->object(i)
    class sensor_models_t
    {
    public:

      // Description:
      // Creates a new empty sensor models structure
      sensor_models_t()
      {}

      // Description:
      // Returns a copy of this sensor model
      sensor_models_t clone() const;

      // Description:
      // Returns the actual sensor model for a particular object
      // The given object id must be the global id for the specific 
      // object as returned by the call to add_new_object below.
      sensor_model_tp object( const int& object_id ) const;
   

      // Description:
      // Add a new object, returns the id
      // The given detection is the first from the object
      int add_new_object( const int& object_type_id,
			  const detection_and_location_t& dx );

    protected:

      // Description:
      // The set of sensor models for object so far
      std::vector< sensor_model_tp > _models;
      
    };

    //============================================================


  }
}

#endif

