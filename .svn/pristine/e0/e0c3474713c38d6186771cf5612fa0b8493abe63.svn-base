//Play back the log with laser annotation & place classification. Find those places with ambiguous predictions. Check whether they are doorway. Collect data for training a binary classifier for doorway

#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>

#include <laser_features/laser_feature.h>

#define LASER_BIN_SIZE 10000
#define ANNOTATION_BIN_SIZE 100
#define THRESHOLD 0.5

void
circ_free_annotation(void *user, void *p){
  erlcm_annotation_t *np = (erlcm_annotation_t *)p;
  erlcm_annotation_t_destroy(np);
}

void
circ_free_laser_annotation(void *user, void *p){
  erlcm_laser_annotation_t *np = (erlcm_laser_annotation_t *)p;
  erlcm_laser_annotation_t_destroy(np);
}

//Check whether a clasisfication message is confident of its prediction
int check_classification(erlcm_place_classification_t *classification){
  int certain = 0;
  for(int i = 0;i < classification->no_class;i++){
    //If probability of one class > 0.5, confident
    if(classification->classes[i].probability > THRESHOLD)
      certain = 1;
  }
  return certain;
}


int 
main(int argc, char **argv)
{
  lcm_eventlog_t *read_log;
  char *log_filename;
  FILE* file;
  char *laser_annotation_channel_name = "LASER_ANNOTATION";
  char *classification_channel_name = "PLACE_CLASSIFICATION";
  char *annotation_channel_name = "LOG_ANNOTATION";
  BotPtrCircular *laser_annotation_buffer = bot_ptr_circular_new(LASER_BIN_SIZE, circ_free_laser_annotation, NULL);
  BotPtrCircular *annotation_buffer = bot_ptr_circular_new(ANNOTATION_BIN_SIZE, circ_free_annotation, NULL);
  if(argc < 3){
    fprintf(stderr, "Provide file names.\n");
    return -1;
  }
  log_filename = argv[1];
  file = fopen(argv[2], "w");
  if(!file){
    fprintf(stderr, "Cannot open file.\n");
  }
  if(log_filename){
    read_log = lcm_eventlog_create(log_filename, "r");
  }
  //Store laser annotations into a circular buffer
  int decode_status;
  fprintf(stderr, "Start reading log.\n");
  for(lcm_eventlog_event_t *event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){

    if(strcmp(laser_annotation_channel_name, event->channel) == 0){
      //      fprintf(stderr, "Decoding event: %s\n", event->channel);
      erlcm_laser_annotation_t *msg = (erlcm_laser_annotation_t *) calloc(1, sizeof(erlcm_laser_annotation_t));
      decode_status = erlcm_laser_annotation_t_decode(event->data, 0, event->datalen, msg);
      if (decode_status < 0)
	fprintf (stderr, "Error %d decoding message\n", decode_status);
      if(strcmp(msg->annotation, "doorway2") == 0){
	bot_ptr_circular_add(laser_annotation_buffer, erlcm_laser_annotation_t_copy(msg));
      }
      erlcm_laser_annotation_t_destroy(msg);
    }
    
    if(strcmp(annotation_channel_name, event->channel) == 0){
      //      fprintf(stderr, "Decoding event: %s\n", event->channel);
      erlcm_annotation_t *msg = (erlcm_annotation_t *) calloc(1, sizeof(erlcm_annotation_t));
      decode_status = erlcm_annotation_t_decode(event->data, 0, event->datalen, msg);
      if (decode_status < 0)
	fprintf (stderr, "Error %d decoding message\n", decode_status);
      if(strcmp(msg->annotation, "doorway2") == 0)
	bot_ptr_circular_add(annotation_buffer, erlcm_annotation_t_copy(msg));
      erlcm_annotation_t_destroy(msg);
    }

    lcm_eventlog_free_event (event);
  }
  fprintf(stderr, "End reading log.\n");
  fprintf(stderr, "Size of annotation buffer: %d\n", bot_ptr_circular_size(annotation_buffer));
  fprintf(stderr, "Size of laser annotation buffer: %d\n", bot_ptr_circular_size(laser_annotation_buffer));

  for(int i = 0;i < bot_ptr_circular_size(annotation_buffer);i++){
    erlcm_annotation_t *annotation = (erlcm_annotation_t *)bot_ptr_circular_index(annotation_buffer, i);
    fprintf(stderr, "start utime of annotation: %lld\n", annotation->start_utime);
  }


  lcm_eventlog_destroy(read_log);
  read_log = lcm_eventlog_create(log_filename, "r");

  fprintf(stderr, "Start reading log.\n");

  //check how many doorways can be detected
  int count[bot_ptr_circular_size(annotation_buffer)];
  for(int i = 0;i < bot_ptr_circular_size(annotation_buffer);i++)
    count[i] = 0;

  //Go through the log again to get those not so confident classifications & write them to text file for training & test
  for(lcm_eventlog_event_t *event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){
    int decode_status = 0;
    if(strcmp(classification_channel_name, event->channel) == 0){
      erlcm_place_classification_t *msg = (erlcm_place_classification_t *) calloc(1, sizeof(erlcm_place_classification_t));
      decode_status = erlcm_place_classification_t_decode(event->data, 0, event->datalen, msg);
      if (decode_status < 0)
	fprintf (stderr, "Error %d decoding message\n", decode_status);
      
      //Check confidence
      int confidence = check_classification(msg);
      //If classifier is uncertain about its prediction
      if(confidence == 0){
	int64_t utime = msg->sensor_utime;
	//Check whether there exist a corresponding annotation. If no, mark it as not a doorway. Otherwise search for laser
	int exist = 0;
	for(int i = 0;i < bot_ptr_circular_size(annotation_buffer);i++){
	  erlcm_annotation_t *annotation = (erlcm_annotation_t *)bot_ptr_circular_index(annotation_buffer, i);
	  if((annotation->start_utime - 1 * pow(10, 6)) <= utime && (annotation->end_utime + 1 * pow(10, 6))>= utime){
	    exist = 1;
	    count[i]++;
	  }
	}
	if(exist == 1)
	  fprintf(stderr, "Annotation exists.\n");
	//Search for a laser
	for(int i = 0;i < bot_ptr_circular_size(laser_annotation_buffer);i++){
	  erlcm_laser_annotation_t *laser = (erlcm_laser_annotation_t *)bot_ptr_circular_index(laser_annotation_buffer, i);
	  if(laser->laser.utime <= utime){
	    //Dump laser features into a text file.
	    feature_config_t config;
	    config.min_range = 0.1;
	    config.max_range = 30;
	    config.cutoff = 5;
	    config.gap_threshold_1 = 3;
	    config.gap_threshold_2 = 0.5;
	    config.deviation = 0.001;
	    config.no_of_fourier_coefficient = 2;
	    
	    bot_core_planar_lidar_t l = laser->laser;

	    raw_laser_features_t *raw_features = extract_raw_laser_features(&l, config);
	    polygonal_laser_features_t *polygonal_features = extract_polygonal_laser_features(&l, config);
	    fprintf(file, "%d %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f ", exist, 1, raw_features->avg_difference_consecutive_beams, 2, raw_features->std_dev_difference_consecutive_beams, 3, raw_features->cutoff_avg_difference_consecutive_beams, 4, raw_features->avg_beam_length, 5, raw_features->std_dev_beam_length, 6, raw_features->no_gaps_in_scan_threshold_1, 7, raw_features->no_relative_gaps, 8, raw_features->no_gaps_to_num_beam, 9, raw_features->no_relative_gaps_to_num_beam, 10, raw_features->distance_between_two_smalled_local_minima, 11, raw_features->index_distance_between_two_smalled_local_minima, 12, raw_features->avg_ratio_consecutive_beams, 13, raw_features->avg_to_max_beam_length, 14, polygonal_features->area, 15, polygonal_features->perimeter, 16, polygonal_features->area_to_perimeter, 17, polygonal_features->PI_area_to_sqrt_perimeter, 18, polygonal_features->PI_area_to_perimeter_pow, 19, polygonal_features->sides_of_polygon);
	    for(int i = 0; i <= config.no_of_fourier_coefficient * 2; i++){
	      fprintf(file, "%d:%f %d:%f ", 20 + i * 2, polygonal_features->fourier_transform_descriptors[0].results->real, 21 + i * 2, polygonal_features->fourier_transform_descriptors[0].results->complex);
	    }
	    int start = config.no_of_fourier_coefficient * 4 + 22;
	    fprintf(file, "%d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f\n", start, polygonal_features->major_ellipse_axis_length_coefficient, start + 1, polygonal_features->minor_ellipse_axis_length_coefficient, start + 2, polygonal_features->major_to_minor_coefficient, start + 3, polygonal_features->central_moment_invariants[0], start + 4, polygonal_features->central_moment_invariants[1], start + 5, polygonal_features->central_moment_invariants[2], start + 6, polygonal_features->central_moment_invariants[3], start + 7, polygonal_features->central_moment_invariants[4], start + 8, polygonal_features->central_moment_invariants[5], start + 9, polygonal_features->central_moment_invariants[6], start + 10, polygonal_features->normalized_feature_of_compactness, start + 11, polygonal_features->normalized_feature_of_eccentricity, start + 12, polygonal_features->mean_centroid_to_shape_boundary, start + 13, polygonal_features->max_centroid_to_shape_boundary, start + 14, polygonal_features->mean_centroid_over_max_centroid, start + 15, polygonal_features->std_dev_centroid_to_shape_boundry, start + 16, polygonal_features->kurtosis);
	    //If have found one, break the loop.
	    break;
	  }
	}
      }
    }
    lcm_eventlog_free_event(event);
  }

  /*
  int suc = 0;
  for(int i = 0;i < bot_ptr_circular_size(annotation_buffer);i++){
    if(count[i] >= 3)
      suc++
  }
  fprintf(stderr, "Recognition rate: %f\n", (double)suc/(bot_ptr_circular_size(annotation_buffer)));
  */

  fprintf(stderr, "End reading log.\n");
  fclose(file);

  lcm_eventlog_destroy (read_log);
}


