//ik-kinematic-decoupling.c

#include <malloc.h>
#include <stdio.h>
#include "ik-kinematic-decoupling.h"
#include "ik-frames.h"

const uint32_t 	FIRST_Q		=	0x80000000;

const uint32_t	ROT_ROLL	=	0x00000001;
const uint32_t	ROT_PITCH	=	0x00000002;
const uint32_t	ROT_YAW		=	0x00000004;

const uint32_t 	ROT_MASK	=	0x00000007;

const uint32_t	POS_AZIM	=	0x00000010;
const uint32_t	POS_DIST	=	0x00000020;

const uint32_t	POS_MASK	=	0x00000030;

const uint32_t  WRST_FLAG	=	0x00000100;
const uint32_t	WRST_START	=	0x00000200;

const uint32_t  WRST_MASK	=	0x00000300;

const uint32_t	END_EFCTR	=	0x00001000;

struct _kd_t {
	ik_frames_t *ikf;
	uint32_t *attr_list;
};

//function prototypes
int kd_analyze_kinematic_chain(kd_t *kd);

kd_t *kd_new(BotParam *params){
	//generate a new instance of kd
	kd_t *kd = (kd_t *) calloc(1, sizeof(kd_t));
	if (kd == NULL){
		printf ("Error: Failed to malloc kd\n");
		return NULL;
	}
	//get a inverse kinematics frame
	kd->ikf = ik_frames_new(params);
	int count = ik_frames_get_size(kd->ikf);
	kd->attr_list = (uint32_t *) calloc(count, sizeof(uint32_t));

	if (kd->ikf == NULL){
		printf ("Error: Failed to generate inverse kinimatics frame\n");
		free(kd);
		return NULL;
	}
	return kd;
}

void kd_destroy(kd_t *kd){
	if (kd->ikf != NULL){
		ik_frames_destroy(kd->ikf);
	}
	free (kd);
}

int kd_generate_qs (
				kd_t *kd,
				const double loc[3],
				const double ef_rpy[3],
				bool	rot_dk,
				const double *present_qs,
				double *out_qs){

	//printf ("in %s\n", __func__);

	printf ("loc = %4f, %4f, %4f\n", loc[0], loc[1], loc[2]);
	printf ("ef_rpy = %4f, %4f, %4f\n", ef_rpy[0], ef_rpy[1], ef_rpy[2]);
	//from the rot_quat determine roll pitch yaw of end effector
	//double ef_rpy[3];	

	//bot_quat_to_roll_pitch_yaw(ef_rot_quat, ef_rpy);
	
	int joint_count = ik_frames_get_size(kd->ikf);
	//out_qs should be the correct size

	//extrapolate the x/y coordinates
	double x 			= loc[0];
	double y 			= loc[1];
	double z 			= loc[2];


	//find out what the angle of the
	double pos_roll 	= 0.0;
	//double pos_pitch 	= 0.0;
	double pos_yaw 		= 0.0;

	//Find Position Yaw

	//if (y == 0.0){
	//	if (x > 0.0){
	//		pos_yaw = -1.57;
	//	}
	//	else {
	//		pos_yaw = 1.57;
	//	}
	//}
	//else {
	pos_yaw = -atan2(x, y);
	//}
	printf ("theta1 = %4f\n", pos_yaw);


	//Find Position Roll
//	if (z == 0.0){
//		if (y > 0.0){
//			pos_roll = 1.57;
//		}
//		else {
//			pos_roll = -1.57;
//		}
//	}
//	else {
		pos_roll = atan2 (y, z);
//	}
	printf ("theta2 = %4f\n", pos_roll);

	double ee_length = sqrt (
						x * x +
						y * y +
						z * z);
	printf ("Length to End Effector = %4f\n", ee_length);

	printf ("Calculating projected x y values (on y,z plane)\n");
	//get the projected values on to the virtual x, y plane
	double xp;
	if (y == 0){
		if (x < 0.0){
			xp = -x;
		}
		else {
			xp = x;
		}
	}
	else {
		xp = y / cos(pos_yaw);
	}
	//double yp = y;
	double yp = z;

	printf ("projected x = %4f\n", xp);
	printf ("projected y = %4f\n", yp);
	

	printf ("Calculating the wrist x, y's\n");

	//note this needs to be modified when more axes are available to the wrist
	double wrist_length = ik_frames_get_frame_length(kd->ikf, "lateral_wrist");
	printf ("Wrist length = %4f\n", wrist_length);
	double xw;
	double yw;
	xw = xp - sin(ef_rpy[0] + 1.57) * wrist_length;
	/*
	if (cos(ef_rpy[0]) > 0){
		xw = xp - cos(ef_rpy[0]) * wrist_length;
	}
	else {
		xw = xp + cos(ef_rpy[0]) * wrist_length;
	}
	*/
	yw = yp - sin(ef_rpy[0]) * wrist_length;

	printf ("wrist x = %4f\n", xw);
	printf ("wrist y = %4f\n", yw);

	double b_w_length = sqrt(
							xw * xw +
							yw * yw);
	printf ("Length from base to wrist = %4f\n", b_w_length);

	printf ("Finding the length from shoulder to wrist\n");
	double base_length = ik_frames_get_frame_length(kd->ikf, "base");
	printf ("Base Height = %4f\n", base_length);
	//radian version of 90 deg - atan(yw, xw)
	double theta_bw = 1.57 - atan2(yw, xw);
	printf ("theta base to wrist = %4f\n", theta_bw);
	//THERE IS A POSITIVE AND NEGATIVE SOLUTION!!
	double sh_w_length = sqrt(
			base_length * base_length +
			b_w_length * b_w_length -
			(2 * base_length * b_w_length * cos(theta_bw)));
	printf ("Sholder to wrist length = %4f\n", sh_w_length);


	printf ("Calculating the angle of the elbow\n");
	double elbow_length = ik_frames_get_frame_length(kd->ikf, "elbow");
	double shoulder_length = ik_frames_get_frame_length (kd->ikf, "shoulder");
	printf ("Shoulder length = %4f\n", shoulder_length);
	printf ("Elbow length = %4f\n", elbow_length);

	double theta_elbow = 0.0;
/*	
	double a;
	double b;
	double c;
	double theta_out;
*/	
	printf ("Using law of cosines...\n");
	double int_top_value = 	elbow_length * elbow_length +
							shoulder_length * shoulder_length - 
							(sh_w_length * sh_w_length);
	double int_bot_value = 2 * elbow_length * shoulder_length;

	double pre_acos_value = int_top_value / int_bot_value;

	printf ("intermediate values = %4f / %4f = %4f\n", int_top_value, int_bot_value, pre_acos_value);
	theta_elbow = acos( pre_acos_value);


	printf ("Angle of elbow = %4f\n", theta_elbow);
	double q_elbow = 3.14 - theta_elbow;
	printf ("q_elbow = %4f\n", q_elbow); 


	printf ("Calculating the angle of the shoulder\n");
	int_top_value = 		sh_w_length * sh_w_length +
							base_length * base_length - 
							(b_w_length * b_w_length);
	int_bot_value = 2 * sh_w_length * base_length;
	pre_acos_value = int_top_value / int_bot_value;

	printf ("theta 1 intermediate values = %4f / %4f = %4f\n", int_top_value, int_bot_value, pre_acos_value);

	double theta1 = acos(pre_acos_value);

	int_top_value = 		sh_w_length * sh_w_length +
							shoulder_length * shoulder_length - 
							(elbow_length * elbow_length);
	int_bot_value = 2 * sh_w_length * shoulder_length;
	pre_acos_value = int_top_value / int_bot_value;

	printf ("theta 2 intermediate values = %4f / %4f = %4f\n", int_top_value, int_bot_value, pre_acos_value);

	double theta2 = acos( pre_acos_value);
	printf ("theta 1 = %4f\n", theta1);
	printf ("theta 2 = %4f\n", theta2);

	printf ("shoulder angle = %4f\n", theta1 + theta2);

	double q_shoulder = 3.14 - (theta1 + theta2);

	printf ("q_shoulder = %4f\n", q_shoulder);


	printf ("Calculating angle of lateral wrist\n");

	double q_lateral_wrist = 1.57 - q_shoulder - q_elbow - ef_rpy[0];

	printf ("q_lateral_wrist = %4f\n", q_lateral_wrist);
	//the elbow sets the distance dc, so using the lengths of the frames
	// and the angle between the links, generate that distance

	char ** names = ik_frames_get_names(kd->ikf);
	//const char * root_frame = ik_frames_get_root_name(kd->ikf);

	for (int i = 0; i < joint_count; i++){
		out_qs[i] = 0;
		if (strcmp(names[i], "base") == 0){
			out_qs[i] = pos_yaw;	
		}
		if (strcmp(names[i], "shoulder") == 0){
			out_qs[i] = q_shoulder;
		}
		if (strcmp(names[i], "elbow") == 0){
			out_qs[i] = q_elbow;	
		}
		if (strcmp(names[i], "lateral_wrist") == 0){
			
			out_qs[i] = q_lateral_wrist;
		}

		printf ("%s q = %4f\n", names[i], out_qs[i]);
	}
	printf ("\n");
	return 0;
}

/*
 * analyze_kinematic_chain
 *
 * Description: go through each node, and analyze what it's effect is
 * 	on the entire chain
 *
 * Parameters:
 *	kd : self
 *
 * Return
 *	Status:
 *		1 = SUCCESS
 *		0 = FAIL
 */
 
int kd_analyze_kinematic_chain(kd_t *kd){
	
	int count = ik_frames_get_size(kd->ikf);
	const char * root_name = ik_frames_get_root_name(kd->ikf);
	char **names = ik_frames_get_names(kd->ikf);

	for (int i = 0; i < count; i++){
		int world_axis = ik_frames_get_world_axis(kd->ikf, names[i]);
		//clear the attribute list for this item
		kd->attr_list[i] = 0;
		//get the name of previous item
		//const char * prev = ik_frames_get_relative_to(kd->ikf, names[i]);

		if (strcmp(root_name, names[i]) == 0){
			//ignore the root frame
			continue;
		}

		//set up the rotation flag
		switch (world_axis){
			case (IK_ROLL):
				kd->attr_list[i] |= ROT_ROLL;
			break;
			case (IK_PITCH):
				kd->attr_list[i] |= ROT_PITCH;
			break;
			case (IK_YAW):
				kd->attr_list[i] |= ROT_YAW;
			break;
			default:
				printf ("Error: determing the world axis failes!\n");
			break;
		}
		
//NEED a better way to detect these
		if (strcmp(names[i], "base") == 0){
			kd->attr_list[i] |= FIRST_Q;
			continue;
		}
		if (strcmp(names[i], "shoulder") == 0){
			kd->attr_list[i] |= POS_DIST;
			continue;
		}
		if (strcmp(names[i], "elbow") == 0){
			kd->attr_list[i] |= POS_DIST;
			continue;
		}
		if (strcmp(names[i], "lateral_wrist") == 0){
			kd->attr_list[i] |= WRST_START; 
			continue;
		}
		if (strcmp(names[i], "twist_wrist") == 0){
			kd->attr_list[i] |= WRST_FLAG;
			continue;
		}
		if (strcmp(names[i], "gripper") == 0){
			kd->attr_list[i] |= END_EFCTR;
			continue;
		}
		

/*
		//is this the first frame?
		if (strcmp(root_name, prev) == 0){
			//first frame has a large impact on the entire chain
			kd->attr_list[i] |= FIRST_Q;
			continue;
		}

		if ((ROT_MASK & kd_attr_list[i - 1]) != (ROT_MASK & kd_attr_list[i])){
			//perpendicular world axis, good for setting azimuth,
			attr_list[i] |= POS_AZIM;
		}
		else {
			
		}
*/
	}

	free (names);
	return 0;
}

int kd_get_chain_size(kd_t * kd){
	return ik_frames_get_size(kd->ikf);
};

char ** kd_get_frame_names(kd_t *kd){
	return ik_frames_get_names(kd->ikf);
}
double kd_get_frame_max(kd_t *kd, const char *frame_name){
	return ik_frames_get_frame_max(kd->ikf, frame_name);
}
double kd_get_frame_min(kd_t *kd, const char *frame_name){
	return ik_frames_get_frame_min(kd->ikf, frame_name);
}
