/**
  * @file bytecoding.h
  *
  * Encode and decode bytes to the byte order of the protocol. If necessary these function
  * could implement some other coding also.
  *
  * This file is part of libariane
  *
  * Copyright (C) 2009-2010 Nokia Corporation and/or its subsidiary(-ies).
  *
  * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public License
  * version 2.1 as published by the Free Software Foundation.
  *
  * This library is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  * Lesser General Public License for more details.
  *
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
  * 02110-1301 USA
*/

#ifndef __BYTECODING_H
#define __BYTECODING_H

#include <stdint.h>

static inline uint16_t decode_uint16(const uint8_t *buf) {
	return (buf[0] << 8) | buf[1];
}

static inline uint32_t decode_uint32(const uint8_t *buf) {
	return (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3];
}

static inline uint8_t *encode_uint16(uint8_t *buf, uint16_t data) {
	*buf++ = data >> 8;
	*buf++ = data & 0xFF;
	return buf;
}

static inline  uint8_t *encode_uint32(uint8_t *buf, uint32_t data) {
	*buf++ = data >> 24;
	*buf++ = (data >> 16) & 0xFF;
	*buf++ = (data >> 8) & 0xFF;
	*buf++ = data & 0xFF;
	return buf;
}

#endif
