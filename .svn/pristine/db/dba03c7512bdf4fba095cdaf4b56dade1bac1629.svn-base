#include <cmath>
#include <cstdlib>

#include <iostream>

#include "system_single_integrator.h"

using namespace std;
using namespace SingleIntegrator;
using namespace arm_collision_check;

#define DISCRETIZATION_STEP 0.001
#define MAX_DISTANCE 100//.3//0.3  //in radians

region::region () {
    
    numDimensions = 0;
    
    center = NULL;
    size = NULL;
}


region::~region () {

    if (center!=NULL)
         delete [] center;
    if (size!=NULL)
        delete [] size;
    
}


int region::setNumDimensions (int numDimensionsIn) {
    
    numDimensions = numDimensionsIn;
    
    if (center!=NULL) 
         delete [] center;
    center = new double[numDimensions];

    if (size!=NULL) 
         delete [] size;
    size = new double[numDimensions];

//center.resize(numDimensions);
    //size.resize(numDimensions);
    
    return 1;
    
}


State::State () {
    
    numDimensions = 0;
    
    x = NULL;
}


State::~State () {
    
    if (x)
        delete [] x;
}

void State::printState () {
    return;
    for(int i=0; i < numDimensions; i++){
	//std::cout << x[i];
	fprintf(stderr, "%0.3f\t", x[i]);
    }
    std::cerr << std::endl;
}


State::State (const State &stateIn) {
    //cout << "Num Dimensions: " << stateIn.numDimensions << endl;
    numDimensions = stateIn.numDimensions;
    
    if (numDimensions > 0) {
        x = new double[numDimensions];
        
        for (int i = 0; i < numDimensions; i++) 
            x[i] = stateIn.x[i];
    }
    else {
        x = NULL;
    }
}


State& State::operator=(const State &stateIn){
    
    if (this == &stateIn)
        return *this;
    
    if (numDimensions != stateIn.numDimensions) {
        if (x) 
            delete [] x;
        numDimensions = stateIn.numDimensions;
        if (numDimensions > 0)
            x = new double[numDimensions];
    }
    
    for (int i = 0; i < numDimensions; i++) 
        x[i] = stateIn.x[i];
    
    return *this;
}


int State::setNumDimensions (int numDimensionsIn) {
    
    if (x)
        delete [] x;
    
    if (numDimensions < 0)
        return 0;
    
    numDimensions = numDimensionsIn;
    
    if (numDimensions > 0)
        x = new double[numDimensions];
    
    return 1;
}


Trajectory::Trajectory () {
    
    endState = NULL;
}


Trajectory::~Trajectory () {
    
    if (endState)
        delete endState;
}


Trajectory::Trajectory (const Trajectory &trajectoryIn) {
    
    endState = new State (trajectoryIn.getEndState()); 

}


Trajectory& Trajectory::operator=(const Trajectory &trajectoryIn) {
    
    if (this == &trajectoryIn)
        return *this;
    
    if (endState)
        delete endState;
    
    
    endState = new State (trajectoryIn.getEndState());
    
    totalVariation = trajectoryIn.totalVariation;
    
    return *this;
}


double Trajectory::evaluateCost () {
    
    return totalVariation;
}


System::System (ArmCollisionCheck *_arm_cc) {
    
    numDimensions = 0;
    arm_cc = _arm_cc;
}


System::~System () {
    
}


int System::setNumDimensions (int numDimensionsIn) {
    
    if (numDimensions < 0)
        return 0;
    
    numDimensions = numDimensionsIn;
    
    rootState.setNumDimensions (numDimensions);
    
    return 1;
}

int System::setCostWeights (double *weights) {
    if (numDimensions < 0)
	return 0;

    costWeights = weights;
  
}

int System::getStateKey (State& stateIn, double* stateKey) {
    
    for (int i = 0; i < numDimensions; i++) 
        stateKey[i] =  stateIn.x[i] / regionOperating.size[i];
    
    return 1;
}



bool System::isReachingTarget (State &stateIn) {
    
    
    for (int i = 0; i < numDimensions; i++) {
        
        if (fabs(stateIn.x[i] - regionGoal.center[i]) > regionGoal.size[i]/2.0 ) 
            return false;
    }
    
    return true;
}


bool System::IsInCollision (double *stateIn) {
    
    for (list<region*>::iterator iter = obstacles.begin(); iter != obstacles.end(); iter++) {
        
        region *obstacleCurr = *iter;
        bool collisionFound = true;
        
        for (int i = 0; i < numDimensions; i++) 
            if (fabs(obstacleCurr->center[i] - stateIn[i]) > obstacleCurr->size[i]/2.0 ) {
                collisionFound = false;
                break;
            }
        
        if (collisionFound) {
            return true;
        }
    }

    if(arm_cc){
        ArmCollisionCheckResult cc_result;
        arm_cc->checkConfiguration (stateIn, &cc_result);
        if (cc_result.in_collision == 1)
            return true;
    }
    
    return false;
} 


int System::sampleState (State &randomStateOut) {
  
    randomStateOut.setNumDimensions (numDimensions);
    
    if (numSamples % 200 == 0){
	for (int i = 0; i < numDimensions; i++) {
	    if(useDimensions[i]){
		randomStateOut.x[i] = (double)rand()/(RAND_MAX + 1.0)*regionGoal.size[i]
		    - regionGoal.size[i]/2.0 + regionGoal.center[i];
	    }
	    else randomStateOut.x[i] = sampleDefaults[i];
	}
    }

    else{
	for (int i = 0; i < numDimensions; i++) {
	    if(useDimensions[i]){
		randomStateOut.x[i] = (double)rand()/(RAND_MAX + 1.0)*regionOperating.size[i] 
		    - regionOperating.size[i]/2.0 + regionOperating.center[i];
	    }
	    else randomStateOut.x[i] = sampleDefaults[i];
	}
    }

    numSamples++;
    
    if (IsInCollision (randomStateOut.x))
        return 0;
    
    return 1;
}



int System::extendTo (State &stateFromIn, State &stateTowardsIn, Trajectory &trajectoryOut, bool &exactConnectionOut) {
  
    // TODO: Update to return exactConnectionOut=True if we reached the sample and
    //                        exactConnectionOut=False if we didn't get all the way there
    // Done

    //We assume that the goal is in freespace 

    //if (IsInCollision (stateTowardsIn.x))
    //return 0;

    exactConnectionOut = true;

  

    double *dists = new double[numDimensions];
    for (int i = 0; i < numDimensions; i++) 
        dists[i] = bot_mod2pi(stateTowardsIn.x[i] - stateFromIn.x[i]);
    
    double distTotal = 0.0;
    for (int i = 0; i < numDimensions; i++) 
        distTotal += dists[i]*dists[i];
    distTotal = sqrt (distTotal);

    if (distTotal > MAX_DISTANCE){
	double alpha = MAX_DISTANCE / distTotal;
	for (int i = 0; i < numDimensions; i++)
	    dists[i] *= alpha;

	distTotal = 0.0;
	for (int i = 0; i < numDimensions; i++) 
	    distTotal += dists[i]*dists[i];
	distTotal = sqrt (distTotal);
   
	for (int i = 0; i < numDimensions; i++)
	    stateTowardsIn.x[i] = stateFromIn.x[i] + dists[i];
      
	exactConnectionOut = false;
    }
  
    double incrementTotal = distTotal/DISCRETIZATION_STEP;
    
    // normalize the distance according to the disretization step
    for (int i = 0; i < numDimensions; i++)
        dists[i] /= incrementTotal;
    
    int numSegments = (int)floor(incrementTotal);
    
    double *stateCurr = new double[numDimensions];
    for (int i = 0; i < numDimensions; i++) 
        stateCurr[i] = stateFromIn.x[i];
    
    for (int i = 0; i < numSegments; i++) {
        
	if (IsInCollision (stateCurr)){
	    exactConnectionOut = false;
	    break;
	}//            return 0;
        
	for (int i = 0; i < numDimensions; i++)
	    stateCurr[i] += dists[i];
    }
    
    
    trajectoryOut.endState = new State ();//stateTowardsIn);
    trajectoryOut.endState->setNumDimensions(numDimensions);
    trajectoryOut.endState->x = new double[numDimensions];
    for (int i = 0; i < numDimensions; i++) 
	trajectoryOut.endState->x[i] = stateCurr[i];
    trajectoryOut.totalVariation = distTotal;
    
    delete [] dists;
    delete [] stateCurr;
        
    return 1;
}

int System::extendToFully (State &stateFromIn, State &stateTowardsIn, Trajectory &trajectoryOut, bool &exactConnectionOut) {
  
    // TODO: Update to return exactConnectionOut=True if we reached the sample and
    //                        exactConnectionOut=False if we didn't get all the way there
    // Done

    //We assume that the goal is in freespace 

    //if (IsInCollision (stateTowardsIn.x))
    //return 0;

    exactConnectionOut = true;

  

    double *dists = new double[numDimensions];
    for (int i = 0; i < numDimensions; i++) 
        dists[i] = bot_mod2pi(stateTowardsIn.x[i] - stateFromIn.x[i]);
    
    double distTotal = 0.0;
    for (int i = 0; i < numDimensions; i++) 
        distTotal += dists[i]*dists[i];
    distTotal = sqrt (distTotal);

    /*if (distTotal > MAX_DISTANCE){
	double alpha = MAX_DISTANCE / distTotal;
	for (int i = 0; i < numDimensions; i++)
	    dists[i] *= alpha;

	distTotal = 0.0;
	for (int i = 0; i < numDimensions; i++) 
	    distTotal += dists[i]*dists[i];
	distTotal = sqrt (distTotal);
   
	for (int i = 0; i < numDimensions; i++)
	    stateTowardsIn.x[i] = stateFromIn.x[i] + dists[i];
      
	exactConnectionOut = false;
	}*/
  
    double incrementTotal = distTotal/DISCRETIZATION_STEP;
    
    // normalize the distance according to the disretization step
    for (int i = 0; i < numDimensions; i++)
        dists[i] /= incrementTotal;
    
    int numSegments = (int)floor(incrementTotal);
    
    double *stateCurr = new double[numDimensions];
    for (int i = 0; i < numDimensions; i++) 
        stateCurr[i] = stateFromIn.x[i];
    
    for (int i = 0; i < numSegments; i++) {
        
	if (IsInCollision (stateCurr)){
	    exactConnectionOut = false;
	    break;
	}//            return 0;
        
	for (int i = 0; i < numDimensions; i++)
	    stateCurr[i] += dists[i];
    }
    
    
    trajectoryOut.endState = new State ();//stateTowardsIn);
    trajectoryOut.endState->setNumDimensions(numDimensions);
    trajectoryOut.endState->x = new double[numDimensions];
    for (int i = 0; i < numDimensions; i++) 
	trajectoryOut.endState->x[i] = stateCurr[i];
    trajectoryOut.totalVariation = distTotal;
    
    delete [] dists;
    delete [] stateCurr;
        
    return 1;
}


double System::evaluateExtensionCost (State& stateFromIn, State& stateTowardsIn, bool &exactConnectionOut) {
    
    
    //exactConnectionOut = true;
    
    double distTotal = 0.0;
    double costTotal = 0.0;
    for (int i = 0; i < numDimensions; i++) {
        double costCurr = bot_mod2pi(stateTowardsIn.x[i] - stateFromIn.x[i]) * costWeights[i];
        double distCurr = bot_mod2pi(stateTowardsIn.x[i] - stateFromIn.x[i]);
        costTotal += costCurr*costCurr;
	distTotal += distCurr*distCurr;
    }

    double dist = sqrt(distTotal);

    /*if(MAX_DISTANCE > dist){
	exactConnectionOut = true;
	}*/
    
    return sqrt(costTotal);
    
}


int System::getTrajectory (State& stateFromIn, State& stateToIn, list<double*>& trajectoryOut) {
    
    double *stateArr = new double[numDimensions];
    for (int i = 0; i < numDimensions; i++)
        stateArr[i] = stateToIn[i];
    trajectoryOut.push_front (stateArr);
    
    return 1;
    
}


double System::evaluateCostToGo (State& stateIn) {
    
    double radius = 0.0;
    for (int i = 0; i < numDimensions; i++) 
        radius += pow(regionGoal.size[i] * costWeights[i],2);// * regionGoal.size[i];
    radius = sqrt(radius);
    
    double dist = 0.0;
    for (int i = 0; i < numDimensions; i++){ 
	double delta = bot_mod2pi(stateIn[i] - regionGoal.center[i]) * costWeights[i];
	dist += pow(delta, 2);//*bot_mod2pi(stateIn[0] - regionGoal.center[i]);
    }
    dist = sqrt(dist);
    
    return fmax(0, dist - radius);
}
