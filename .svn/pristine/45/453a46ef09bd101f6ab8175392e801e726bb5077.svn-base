#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include "utils/template.h"
#include "utils/container.h"
#include "utils/conversion.h"
#include "utils/pair.h"
#include <fovis/fovis.hpp>

// frame utils
#include <pcl-utils/frame_utils.hpp>

// opencv-utils includes
#include <perception_opencv_utils/opencv_utils.hpp>

namespace py = boost::python;

using namespace fovis;

namespace fs { namespace python { 

class FovisWrapper {
 public:
  FovisWrapper():
      odom(NULL), kinect_calib(NULL), depth_producer(NULL) {
    
    // Setup Kinect Params
    // make up an initial calibration
    kinect_params.width = 640;
    kinect_params.height = 480;

    memset(&kinect_params.depth_params, 0, sizeof(CameraIntrinsicsParameters));
    kinect_params.depth_params.width = kinect_params.width;
    kinect_params.depth_params.height = kinect_params.height;
    kinect_params.depth_params.fx = 576.09757860;
    kinect_params.depth_params.fy = kinect_params.depth_params.fx;
    kinect_params.depth_params.cx = 321.06398107;
    kinect_params.depth_params.cy = 242.97676897;

    memset(&kinect_params.rgb_params, 0, sizeof(CameraIntrinsicsParameters));
    kinect_params.rgb_params.width = kinect_params.width;
    kinect_params.rgb_params.height = kinect_params.height;
    kinect_params.rgb_params.fx = 528.49404721;
    kinect_params.rgb_params.fy = kinect_params.rgb_params.fx;
    kinect_params.rgb_params.cx = 319.50000000;
    kinect_params.rgb_params.cy = 239.50000000;

    kinect_params.shift_offset = 1093.4753;
    kinect_params.projector_depth_baseline = 0.07214;

    Eigen::Matrix3d R;
    R << 0.999999, -0.000796, 0.001256,
        0.000739, 0.998970, 0.045368,
        -0.001291, -0.045367, 0.998970;
    
    kinect_params.depth_to_rgb_translation[0] = -0.015756;
    kinect_params.depth_to_rgb_translation[1] = -0.000923;
    kinect_params.depth_to_rgb_translation[2] =  0.002316;

    Eigen::Quaterniond Q(R);
    kinect_params.depth_to_rgb_quaternion[0] = Q.w();
    kinect_params.depth_to_rgb_quaternion[1] = Q.x();
    kinect_params.depth_to_rgb_quaternion[2] = Q.y();
    kinect_params.depth_to_rgb_quaternion[3] = Q.z();

    // Setup Kinect Calib
    kinect_calib = new PrimeSenseCalibration(kinect_params);
    const Rectification* rectification = kinect_calib->getRgbRectification();

    // Setup VO
    odom = new VisualOdometry(rectification, VisualOdometry::getDefaultOptions());

    // Setup depth image
    depth_producer = new PrimeSenseDepth(kinect_calib);
  }

  std::vector<double>
  getPose() {
    Eigen::Isometry3d cam_to_local = odom->getPose();
    Eigen::Vector3d translation(cam_to_local.translation());
    Eigen::Quaterniond rotation(cam_to_local.rotation());
    std::vector<double> pose(7);
    pose[0] = translation[0], pose[1] = translation[1], pose[2] = translation[2];
    pose[3] = rotation.w(), pose[4] = rotation.x(),
        pose[5] = rotation.y(), pose[6] = rotation.z();
    return pose;
  }

  // std::vector<double>
  // getMotionEstimate() {
  // }

  void processFrame(opencv_utils::Frame& frame) {
    const cv::Mat_<uint16_t>& disparity = frame.getDepthRef();
    depth_producer->setDisparityData((uint16_t*)disparity.data);

    const cv::Mat1b& gray = frame.getGrayRef();
    odom->processFrame((uint8_t*) gray.data, depth_producer);
  }
  
  ~FovisWrapper() {
    delete kinect_calib;
    delete depth_producer;
    delete odom;
  }

 private:
  VisualOdometry* odom;
  PrimeSenseCalibrationParameters kinect_params;
  PrimeSenseCalibration* kinect_calib;
  PrimeSenseDepth* depth_producer;
};
}
}

BOOST_PYTHON_MODULE(fs_fovis_utils)
{
  // Py Init and Main types export
  fs::python::init_and_export_converters();
  
  // Class def
  py::class_<fs::python::FovisWrapper>("FOVIS")
      .def("processFrame", &fs::python::FovisWrapper::processFrame)
      .def("getPose", &fs::python::FovisWrapper::getPose)
      // .def("getMotionEstimate", &fsvision::FovisWrapper::getMotionEstimate)
      ;      
  
}

