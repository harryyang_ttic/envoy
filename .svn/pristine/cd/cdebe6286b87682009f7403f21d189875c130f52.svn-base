#ifndef FS_DENSE_TRAJECTORIES_HPP_
#define FS_DENSE_TRAJECTORIES_HPP_

// #define DENSE_SAMPLING 1

// features, frame_utils, opencv_utils include
#include <features3d/feature_types.hpp>
#include <pcl-utils/frame_utils.hpp>
#include <perception_opencv_utils/opencv_utils.hpp>

// fs-utils
#include <fs-utils/profiler.hpp>
#include <fs-utils/thread_safe_queue.hpp>

// dense trajectories includes
#include <fs_perception_wrappers/fs_dense_trajectories/fs_OpticalFlow.h>

#include <time.h>

namespace fs { namespace vision {

// parameters for tracking
static const int PATCH_SIZE = 32;
static const double QUALITY = 0.08; // 0.001
static const int MIN_DISTANCE = 5;
static const int TRACK_LENGTH = 15;
static const float FEATURE_MATCH_THRESHOLD = 0.5;
static const int MAX_PREDICTIONS = 5;

// TODO:
// 1. Save descriptions for each track, top k-dictionary items
// 2. 
class Track
{
public:
  FixedLengthQueue<cv::Point2f, TRACK_LENGTH> point;
  FixedLengthQueue<cv::Mat, TRACK_LENGTH> desc;
  FixedLengthQueue<cv::Mat, TRACK_LENGTH> mipmap;
  FixedLengthQueue<float, TRACK_LENGTH> match;
 
  int id, index;
  int check_length, track_length;

  int predictions;
  unsigned int status;
  enum TrackingStatus { ADDED=0, MATCHED=1, PREDICTED=2, LOST=3, PREP_LOST=4 };

  Track() {};  
  Track(const int id_, const Point2f& point_, const cv::Mat& desc_, const cv::Mat& mipmap_)
  {
    // Add point, and description to new track
    point.push(point_);
    desc.push(desc_);
    mipmap.push(mipmap_);
    id = id_;

    // Init match value
    match.push(1.f);

    // Init params
    check_length = 0; // TRACK_LENGTH;
    track_length = TRACK_LENGTH;

    // Init predictions, and track status
    predictions = 0;
    status = Track::ADDED;

  }

  inline bool checkPoint(const cv::Mat& c_desc) {
    // Push dummy match value
    match.push(1.f);

    // Only match after track has reached 'check length'
    if (desc.size() < check_length ) return true;
      
    // Compute descriptor match between first and latest frame
    cv::Mat& p_desc = desc.get_oldest();
    assert(!p_desc.empty() && !c_desc.empty());
    // std::cerr << "p_desc" << p_desc << " " << c_desc << std::endl;
        
    // Match and remove if not good
    cv::Mat out;
    cv::batchDistance(c_desc, p_desc, out, p_desc.type(),
                      cv::noArray(), cv::NORM_L2, 0, cv::Mat(), 0, false);
    float m_distance = out.at<float>(0);

    // Check matches, and set match value
    match.end()[-1] = m_distance * 1.f;

    // Case 1: Matched, add to track, and reset predictions
    // if (m_distance < std::min((double)FEATURE_MATCH_THRESHOLD,
    //                           (FEATURE_MATCH_THRESHOLD-1.0) + 1.0*point.size()/TRACK_LENGTH)) {
      if (m_distance < FEATURE_MATCH_THRESHOLD) { 
      // std::cerr << "GOOD match: " << m_distance << std::endl;
      status = Track::MATCHED;
      predictions = 0; 
      return true;
    }
    
    // If above match threshold, increment predictions
    // std::cerr << "BAD match: " << m_distance << std::endl;
#if 0
    // Case 1: If outside predictions limit, don't add
    if (predictions > MAX_PREDICTIONS)
      return false;
    
    // Case 2: If within predictions limit, add and increment predictions
    status = Track::PREDICTED;
    predictions++;
    return true;
#else
    return false;
#endif
  }
    
  inline bool addPoint(const Point2f& point_, const cv::Mat& desc_, const cv::Mat& mipmap_) {
    // 1. Check if valid match
    bool valid_match = checkPoint(desc_);
    if (!valid_match)
      return false;

    // 2. Add to list
    point.push(point_);
    desc.push(desc_);
    mipmap.push(mipmap_);

    // if (id == 10) { 
    //   // cv::Mat1b mm = cv::Mat1b::zeros(128,128*(mipmap.size() + 1));
    //   // for (int j=0; j<mipmap.size(); j++) {
    //   //   cv::Mat mmj; // (mm, cv::Rect(j*128, 0, 127, 127));
    //   //   cv::resize(mipmap[j], mmj, cv::Size(128,128), 0, 0, cv::INTER_AREA);
    //   //   std::cerr << mipmap[j].size() << " " << mmj << std::endl;
    //   //   // cv::Mat mm_(mm, cv::Rect(j*128,0,127,127));
    //   //   // std::cerr << mmj.size() << " " << mm_.size() << " " << j*128 << "," << 0 << std::endl;
    //   // }
    //   for (int j=0; j<mipmap.size(); j++)
    //     cv::imshow(std::string("mipmap") + std::to_string(j), mipmap[j]);
    // }
    
    assert(desc.size() == point.size());
    assert(status == Track::MATCHED);
    return true;
  }

};


static cv::RNG rng(12345);
class DenseTrajectories { 

 public:
  cv::Mat image, prev_image, grey, prev_grey;
  std::vector<float> fscales;
  std::vector<cv::Size> sizes;

  std::vector<cv::Mat> prev_grey_pyr, grey_pyr, flow_pyr;
  std::vector<cv::Mat> prev_poly_pyr, poly_pyr; // for optical flow
  std::vector<std::vector<Track> > xyScaleTracks;

  // Feature Detector/Extractor
  opencv_utils::Frame frame_;
  cv::Ptr<cv::FeatureDetector> detector_;
  cv::Ptr<cv::DescriptorExtractor> extractor_; 

  // Profiler
  fsvision::Profiler profiler_;

  // Viz colors
  std::vector<cv::Scalar> colors;

  // kd-tree for recovery 
  cv::Ptr<cv::flann::Index> c_index; 
  
  int frame_num;
  int init_counter;
  int show_track;
  int track_id;

  int scale_num;

public: 
  DenseTrajectories(); 
  ~DenseTrajectories();
  // bool IsValid(std::vector<Point2f>& track,
  //              float& mean_x, float& mean_y, float& var_x, float& var_y, float& length);
  void SparseSample(const cv::Mat& grey, const cv::Mat& mask, std::vector<Point2f>& points);
  void DenseSample(const Mat& grey, std::vector<Point2f>& points,
                   const double quality, const int min_distance);

  void InitImagePyramids(const cv::Size& sz);
  void BuildImagePyramids(const cv::Mat& img, std::vector<cv::Mat>& img_pyr);
  void InitPry(const cv::Size& sz, std::vector<float>& scales, std::vector<Size>& sizes);
  void BuildPry(const std::vector<Size>& sizes, const int type, std::vector<Mat>& grey_pyr);
  void DrawTrack(const Track& track, const int iScale, Mat& image);

  void PrepareRecovery();
  // void ExtractFeatures(const std::vector<cv::Point2f>& pred_points,
  //                      const int& iScale,
  //                      std::vector<fsvision::Feature3D>& pred_features);
  void ExtractFeatures(std::vector<cv::Point2f>& pred_points,
                       std::vector<cv::Mat>& pred_desc,
                       std::vector<cv::Mat>& pred_mipmap,
                       const int& iScale);

  
  void visualize();

  void processFrame(opencv_utils::Frame& frame, const cv::Mat& mask=cv::Mat());
  void processImage(const cv::Mat& frame, const cv::Mat& mask=cv::Mat()); 
  // void draw(cv::Mat& frame);
  void write();

  std::vector<fsvision::Feature3D> getStableFeatures();
  
  // Track IDing 
  int64_t nextTrackID() { return ++track_id; } 
  int64_t trackID() { return track_id;  }
  bool resetTrackID() { track_id = 1; return true; }

};
    
} // namespace vision
} // namespace fs
#endif // #ifndef FS_DENSE_TRAJECTORIES_HPP_
