#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <bot_vis/bot_vis.h>
#include <image_utils/pixels.h>
#include <image_utils/jpeg.h>
#include <opencv_utils/opencv_utils.hpp>
#include <jpeg-utils/jpeg-utils.h>
#include <bot_lcmgl_client/lcmgl.h>
//#include <er_common/envoy_opencv_utils.hpp>
#include "opencv2/objdetect/objdetect.hpp"
#include <opencv2/nonfree/features2d.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <er_common/path_util.h>

using namespace std;
using namespace cv;

typedef struct _er_cam_renderer {
    bot_core_image_t *last_image;
    BotGlTexture *texture;  
    uint8_t *uncompresed_buffer;
    int uncompressed_buffer_size;
    int width, height;
} er_cam_renderer_t;

typedef struct
{
    BotParam   *b_server;
    lcm_t      *lcm;
    GMainLoop *mainloop;
    GMutex *mutex;
    bot_core_image_t *received_image; 
    bot_core_image_t *tmp_image; 
    pthread_t  work_thread;
    er_cam_renderer_t *cr;
    bot_lcmgl_t *lcmgl;
    int verbose;  
    double scale;
    CascadeClassifier *cascade;
    CascadeClassifier *nestedCascade; 
    
} state_t;

int envoy_decompress_bot_core_image_t(const bot_core_image_t * jpeg_image, bot_core_image_t *uncomp_image)
{
  //copy metadata
  uncomp_image->utime = jpeg_image->utime;
  if (uncomp_image->metadata != NULL)
    bot_core_image_metadata_t_destroy(uncomp_image->metadata);
  uncomp_image->nmetadata = jpeg_image->nmetadata;
  if (jpeg_image->nmetadata > 0) {
    uncomp_image->metadata = bot_core_image_metadata_t_copy(jpeg_image->metadata);
  }
  
  uncomp_image->width = jpeg_image->width;
  uncomp_image->height = jpeg_image->height;
  
  uncomp_image->row_stride = jpeg_image->width * 3;//jpeg_image->row_stride;
  fprintf(stderr, "R : %d D : %d\n", jpeg_image->width, jpeg_image->row_stride);
  int depth=0;
  if (uncomp_image->width>0)
      depth= uncomp_image->row_stride / (double)uncomp_image->width;

  int ret;
  fprintf(stderr,"called1 : %d\n", uncomp_image->row_stride);//depth);

  if(uncomp_image->data == NULL){
      int uncomp_size = (uncomp_image->width) * (uncomp_image->height) * depth;
      uncomp_image->data = (unsigned char *) realloc(uncomp_image->data, uncomp_size * sizeof(uint8_t));
  }

  /*if(depth == 1){
      ret = jpeg_decompress_8u_gray(jpeg_image->data, 
                                    jpeg_image->size, uncomp_image->data, 
                                    uncomp_image->width, 
                                    uncomp_image->height, 
                                    uncomp_image->row_stride);
  }
  else if(depth == 3){*/
  ret = jpeg_decompress_8u_rgb(jpeg_image->data, 
                               jpeg_image->size, uncomp_image->data, 
                               uncomp_image->width, 
                               uncomp_image->height, 
                               uncomp_image->row_stride);
  
  fprintf(stderr,"called\n");
      //  }
  /*envoy_decompress_jpeg_image(jpeg_image->data, jpeg_image->size, &uncomp_image->data,
              &uncomp_image->width, &uncomp_image->height, &depth);*/
  uncomp_image->row_stride = uncomp_image->width * depth;
  uncomp_image->size = uncomp_image->width *uncomp_image->height* depth;
  if (depth == 1) {
    uncomp_image->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY;
  }
  else if (depth == 3) {
    uncomp_image->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB; //TODO: lets just assume its rgb... dunno what else to do :-/
  }
  return ret;
}

/*
 * Creates an opencv image header for a carmen3d_image_t structure
 * FYI: THIS FUNCTION DOES NOT COPY THE DATA!
 */
static inline CvMat get_opencv_header_for_bot_core_image_t(const bot_core_image_t * c3d_im)
{
  if (c3d_im->pixelformat == BOT_CORE_IMAGE_T_PIXEL_FORMAT_BGR || c3d_im->pixelformat
      == BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB) {
    return cvMat(c3d_im->height, c3d_im->width, CV_8UC3, c3d_im->data);
  }
  else if (c3d_im->pixelformat == BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY) {
    return cvMat(c3d_im->height, c3d_im->width, CV_8UC1, c3d_im->data);
  }
  else if (c3d_im->pixelformat == BOT_CORE_IMAGE_T_PIXEL_FORMAT_FLOAT_GRAY32) {
    return cvMat(c3d_im->height, c3d_im->width, CV_32FC1, c3d_im->data);
  }
  else {
    fprintf(stderr, "ERROR: invalid bot_core_image_t pixel format!\n");
    assert(false);
    return cvMat(0, 0, 0, NULL); //make compiler happy
  }
}

void draw_image(state_t *s){

    if(s->received_image == NULL){
        return;
    }

    if(s->cr==NULL){
        s->cr = (er_cam_renderer_t*) calloc (1, sizeof (er_cam_renderer_t));
        s->cr->texture = NULL;
        s->cr->last_image = NULL;
        s->cr->uncompressed_buffer_size = s->received_image->width * s->received_image->height * 3;
        s->cr->uncompresed_buffer =
            (uint8_t*) malloc (s->cr->uncompressed_buffer_size);
    }  
  
    if(s->cr->last_image !=NULL){
        bot_core_image_t_destroy(s->cr->last_image);
    }
    
    s->cr->last_image = bot_core_image_t_copy (s->received_image);

    s->cr->width = s->cr->last_image->width;//(int)(cr->last_image->width/4.0);
    s->cr->height = s->cr->last_image->height; //(int)(cr->last_image->height/4.0);

    /*
      double xscale = cr->width / bot_camtrans_get_image_width(cr->camtrans);
      double yscale = cr->width / bot_camtrans_get_image_width(cr->camtrans);
      assert(fabs(xscale - yscale) < 1e-6);
      bot_camtrans_scale_image(cr->camtrans, xscale);
    */
    uint8_t *tex_src = NULL;
    int stride = 0;
    GLenum gl_format;

    bot_lcmgl_t *lcmgl = s->lcmgl;

    if (s->cr->last_image->pixelformat == 0 ||
        s->cr->last_image->pixelformat == PIXEL_FORMAT_GRAY ||
        s->cr->last_image->pixelformat == PIXEL_FORMAT_BAYER_BGGR ||
        s->cr->last_image->pixelformat == PIXEL_FORMAT_BAYER_RGGB ||
        s->cr->last_image->pixelformat == PIXEL_FORMAT_BAYER_GRBG ||
        s->cr->last_image->pixelformat == PIXEL_FORMAT_BAYER_GBRG) {
    
        stride = s->cr->last_image->width;
        gl_format = GL_LUMINANCE;
        tex_src = s->cr->last_image->data;
    }
    else if (s->cr->last_image->pixelformat == PIXEL_FORMAT_MJPEG) {
        bot_core_image_t * msg = s->cr->last_image;
    
        // might need to JPEG decompress...
        stride = s->cr->last_image->width * 3;
        int buf_size = msg->height * stride;
        if (s->cr->uncompressed_buffer_size < buf_size) {
            s->cr->uncompresed_buffer =
                (uint8_t *) realloc (s->cr->uncompresed_buffer, buf_size);
            s->cr->uncompressed_buffer_size = buf_size;
        }
        jpeg_decompress_8u_rgb (msg->data, msg->size,
                                   s->cr->uncompresed_buffer, msg->width, msg->height, stride);

        gl_format = GL_RGB;
        tex_src = s->cr->uncompresed_buffer;	  
    }
    else{
        fprintf(stderr, "Unhandled format\n");
        return;
    }

    lcmglColor3f(1.0, 1.0, 1.0);

    //need to scale this image - too large 
  
    int prev_gray_texid = bot_lcmgl_texture2d(lcmgl, tex_src,
        s->cr->width, s->cr->height, stride,
                                              BOT_LCMGL_RGB, BOT_LCMGL_UNSIGNED_SHORT, BOT_LCMGL_COMPRESS_NONE);
  
    fprintf(stderr, "Width : %d Height : %d stride : %d\n", s->cr->width, s->cr->height, stride);

    bot_lcmgl_push_matrix(lcmgl);
    bot_lcmgl_translated(lcmgl, 0, s->cr->height + 10, 0);
  
    bot_lcmgl_texture_draw_quad(lcmgl, prev_gray_texid,
                                0, 0, 0,
                                0, s->cr->height, 0,
                                s->cr->width, s->cr->height, 0,
                                s->cr->width, 0, 0);

    /*
      bot_lcmgl_texture_draw_quad(bot_lcmgl_t *lcmgl, int texture_id,
      double x_top_left,  double y_top_left,  double z_top_left,
      double x_bot_left,  double y_bot_left,  double z_bot_left,
      double x_bot_right, double y_bot_right, double z_bot_right,
      double x_top_right, double y_top_right, double z_top_right)    
    */
  

    bot_lcmgl_pop_matrix(lcmgl);

    bot_lcmgl_switch_buffer (lcmgl);
    
}


void sift_extractor(state_t *s){
    if(s->tmp_image ==NULL){
        return;
    }

    IplImage* img;

    static bot_core_image_t * localFrame=(bot_core_image_t *) calloc(1,sizeof(bot_core_image_t));

    g_mutex_lock (s->mutex);
    if(s->received_image != NULL){
        if(s->received_image->utime == s->tmp_image->utime){
            g_mutex_unlock (s->mutex);
            return;
        }

        bot_core_image_t_destroy(s->received_image);
    }

    s->received_image = bot_core_image_t_copy(s->tmp_image);
    g_mutex_unlock (s->mutex);

    //this doesn't work - figure out how to do this properly - or port the renderer
    //draw_image(s);

    fprintf(stderr, "Extracting Sift\n");

    const bot_core_image_t * newFrame=NULL;
    if (s->received_image->pixelformat==BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG){
        //create space for decompressed image
        envoy_decompress_bot_core_image_t(s->received_image,localFrame);
        //copy pointer for working locally
        newFrame = localFrame;
    }
    else{
        //uncompressed, just copy pointer
        newFrame = s->received_image;
    }  
    

    //better copy this - otherwise will crash stuff
    CvMat cvImg = get_opencv_header_for_bot_core_image_t(newFrame);

    CvMat *frame = &cvImg;//NULL;
    //Copy(&cvImg,frame);

    static IplImage tmpHeader;
    IplImage * currentFrame;

    currentFrame = cvGetImage(frame,&tmpHeader);

    const cv::Mat input = cv::Mat(currentFrame, false);

    //cv::SiftFeatureDetector detector;
    //cv::StarFeatureDetector detector;
    //cv::FastFeatureDetector detector;
    cv::SurfFeatureDetector surf_detector;
    std::vector<cv::KeyPoint> surf_keypoints;
    surf_detector.detect(input, surf_keypoints);

    // Add results to image and save.
    cv::Mat surf_output;
    cv::drawKeypoints(input, surf_keypoints, surf_output);
    cv::imwrite("surf_result.jpg", surf_output);

    cv::StarFeatureDetector star_detector;
    std::vector<cv::KeyPoint> star_keypoints;
    star_detector.detect(input, star_keypoints);

    // Add results to image and save.
    cv::Mat star_output;
    cv::drawKeypoints(input, star_keypoints, star_output);
    cv::imwrite("star_result.jpg", star_output);

    cv::SiftFeatureDetector sift_detector;
    std::vector<cv::KeyPoint> sift_keypoints;
    sift_detector.detect(input, sift_keypoints);

    // Add results to image and save.
    cv::Mat sift_output;
    cv::drawKeypoints(input, sift_keypoints, sift_output);
    cv::imwrite("sift_result.jpg", sift_output);
}

void sift_extractor_read_from_file(state_t *s){
    /*if(s->received_image ==NULL){
        return;
        }*/

    fprintf(stderr, "Extracting Sift\n");
    
    //IplImage* img;
    //img=cvLoadImage("input.jpg", 1);

    //CvMat cvImg = get_opencv_header_for_bot_core_image_t(s->received_image);

    //const cv::Mat input = cv::Mat(img, false);

    //    const cv::Mat input =  
    const cv::Mat input = cv::imread("input.jpg", 0); //Load as grayscale
    
    //cv::SiftFeatureDetector detector;
    //cv::StarFeatureDetector detector;
    //cv::FastFeatureDetector detector;
    cv::SurfFeatureDetector surf_detector;
    std::vector<cv::KeyPoint> surf_keypoints;
    surf_detector.detect(input, surf_keypoints);

    // Add results to image and save.
    cv::Mat surf_output;
    cv::drawKeypoints(input, surf_keypoints, surf_output);
    cv::imwrite("surf_result.jpg", surf_output);

    cv::StarFeatureDetector star_detector;
    std::vector<cv::KeyPoint> star_keypoints;
    star_detector.detect(input, star_keypoints);

    // Add results to image and save.
    cv::Mat star_output;
    cv::drawKeypoints(input, star_keypoints, star_output);
    cv::imwrite("star_result.jpg", star_output);

    cv::SiftFeatureDetector sift_detector;
    std::vector<cv::KeyPoint> sift_keypoints;
    sift_detector.detect(input, sift_keypoints);

    // Add results to image and save.
    cv::Mat sift_output;
    cv::drawKeypoints(input, sift_keypoints, sift_output);
    cv::imwrite("sift_result.jpg", sift_output);
}

// ============ LCM thread ========

static void
on_image (const lcm_recv_buf_t *rbuf, const char *channel, 
        const bot_core_image_t *image, void *user_data)
{
    // this method is always invoked from the LCM thread.
    state_t *self = (state_t *) user_data;
    g_mutex_lock (self->mutex);

    if (self->tmp_image) {
        bot_core_image_t_destroy(self->tmp_image);
    }
    self->tmp_image = bot_core_image_t_copy(image);
    fprintf(stderr,".");
    //sift_extractor(self);
    
    g_mutex_unlock (self->mutex);
}

void detectAndDraw( state_t *s ){

    /*                 CascadeClassifier& cascade, CascadeClassifier& nestedCascade,
                   double scale)
                   {*/
    //////////////////////////////////////////////////////
    if(s->tmp_image ==NULL){
        return;
    }
    


    IplImage* img;

    static bot_core_image_t * localFrame=(bot_core_image_t *) calloc(1,sizeof(bot_core_image_t));

   

    g_mutex_lock (s->mutex);
    if(s->received_image != NULL){
        if(s->received_image->utime == s->tmp_image->utime){
            g_mutex_unlock (s->mutex);
            return;
        }

        bot_core_image_t_destroy(s->received_image);
    }

    int64_t time_start = bot_timestamp_now();
    
    s->received_image = bot_core_image_t_copy(s->tmp_image);
    g_mutex_unlock (s->mutex);

    //this doesn't work - figure out how to do this properly - or port the renderer
    //draw_image(s);

    fprintf(stderr, "Extracting Sift\n");

    const bot_core_image_t * newFrame=NULL;
    if (s->received_image->pixelformat==BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG){
        //create space for decompressed image
        envoy_decompress_bot_core_image_t(s->received_image,localFrame);
        //copy pointer for working locally
        newFrame = localFrame;
    }
    else{
        //uncompressed, just copy pointer
        newFrame = s->received_image;
    }  

    //better copy this - otherwise will crash stuff
    CvMat cvImg = get_opencv_header_for_bot_core_image_t(newFrame);

    CvMat *frame = &cvImg;//NULL;
    //Copy(&cvImg,frame);

    static IplImage tmpHeader;
    IplImage * currentFrame;

    currentFrame = cvGetImage(frame,&tmpHeader);

    ///////////////////////////////////////////////////////////////////////

    Mat imageFrame , frameCopy, image;

    imageFrame = currentFrame;

    if( imageFrame.empty() )
            return;

    if( currentFrame->origin == IPL_ORIGIN_TL )
        imageFrame.copyTo( frameCopy );
    else
        flip( imageFrame, frameCopy, 0 );

    cvtColor(frameCopy, frameCopy, CV_RGB2BGR);

    int64_t time_end_1 = bot_timestamp_now();

    fprintf(stderr,"Time to convert (ms): %f\n", ((double)(time_end_1 - time_start))/1.0e3);
    
    //    cv::imshow( "input", frameCopy );
    //cv::imshow( "result", frameCopy );

    int i = 0;
    double t = 0;
    vector<Rect> faces;
    const static Scalar colors[] =  { CV_RGB(0,0,255),
        CV_RGB(0,128,255),
        CV_RGB(0,255,255),
        CV_RGB(0,255,0),
        CV_RGB(255,128,0),
        CV_RGB(255,255,0),
        CV_RGB(255,0,0),
        CV_RGB(255,0,255)} ;
    
    double scale = s->scale;

    Mat gray, smallImg( cvRound (frameCopy.rows/scale), cvRound(frameCopy.cols/scale), CV_8UC1 );

    cvtColor( frameCopy, gray, CV_BGR2GRAY );
    resize( gray, smallImg, smallImg.size(), 0, 0, INTER_LINEAR );
    equalizeHist( smallImg, smallImg );

    CascadeClassifier cascade = *s->cascade;
    CascadeClassifier nestedCascade = *s->nestedCascade; 

    t = (double)cvGetTickCount();
    cascade.detectMultiScale( smallImg, faces,
        1.1, 2, 0
        //|CV_HAAR_FIND_BIGGEST_OBJECT
        //|CV_HAAR_DO_ROUGH_SEARCH
        |CV_HAAR_SCALE_IMAGE
        ,
        Size(30, 30) );
    t = (double)cvGetTickCount() - t;
    printf( "detection time = %g ms\n", t/((double)cvGetTickFrequency()*1000.) );

    int64_t time_end_2 = bot_timestamp_now();
    fprintf(stderr,"Time to Detect (ms) : %f\n", ((double)(time_end_2 - time_end_1))/1.0e3);

    //this searches through the face detections and looks for the nested features 
    for( vector<Rect>::const_iterator r = faces.begin(); r != faces.end(); r++, i++ )
    {
        Mat smallImgROI;
        vector<Rect> nestedObjects;
        Point center;
        Scalar color = colors[i%8];
        int radius;
        center.x = cvRound((r->x + r->width*0.5)*scale);
        center.y = cvRound((r->y + r->height*0.5)*scale);
        radius = cvRound((r->width + r->height)*0.25*scale);
        circle( frameCopy, center, radius, color, 3, 8, 0 );
        if( nestedCascade.empty() )
            continue;
        smallImgROI = smallImg(*r);
        nestedCascade.detectMultiScale( smallImgROI, nestedObjects,
            1.1, 2, 0
            //|CV_HAAR_FIND_BIGGEST_OBJECT
            //|CV_HAAR_DO_ROUGH_SEARCH
            //|CV_HAAR_DO_CANNY_PRUNING
            |CV_HAAR_SCALE_IMAGE
            ,
            Size(30, 30) );
        for( vector<Rect>::const_iterator nr = nestedObjects.begin(); nr != nestedObjects.end(); nr++ )
        {
            center.x = cvRound((r->x + nr->x + nr->width*0.5)*scale);
            center.y = cvRound((r->y + nr->y + nr->height*0.5)*scale);
            radius = cvRound((nr->width + nr->height)*0.25*scale);
            circle( frameCopy, center, radius, color, 3, 8, 0 );
        }
    }
    
    


    cv::imshow( "result", frameCopy );
    //waitKey(0);
}


//pthread
static void *track_work_thread(void *user)
{
    state_t *s = (state_t*) user;

    printf("obstacles: track_work_thread()\n");

    

    while(1){

        
        /*IplImage* iplImg = cvQueryFrame( capture );
        frame = iplImg;
        if( frame.empty() )
            break;
        if( iplImg->origin == IPL_ORIGIN_TL )
            frame.copyTo( frameCopy );
        else
            flip( frame, frameCopy, 0 );
        
        detectAndDraw( frameCopy, cascade, nestedCascade, scale );
        
        if( waitKey( 10 ) >= 0 )
            goto _cleanup_;
        */
        detectAndDraw(s);
	usleep(50000);
    }
}


int main(int argc, const char* argv[])
{
    g_thread_init(NULL);
    setlinebuf (stdout);

    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    state->lcm =  bot_lcm_get_global(NULL);
    state->b_server = bot_param_new_from_server(state->lcm, 1);

    //pthread_create(&state->work_thread, NULL, track_work_thread, state);

    state->mutex = g_mutex_new ();

    state->received_image = NULL;

    state->cr = NULL;

    state->tmp_image = NULL;

    state->scale = 1.3;
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  

    CascadeClassifier cascade, nestedCascade;

    char cascadePath[2048];
    sprintf(cascadePath, "%s/%s", getDataPath(), "opencv/haarcascades/haarcascade_frontalface_alt.xml");
    char nestedCascadePath[2048];
    sprintf(nestedCascadePath, "%s/%s", getDataPath(), "opencv/haarcascades/haarcascade_eye_tree_eyeglasses.xml");

    String cascadeName = cascadePath;
    String nestedCascadeName = nestedCascadePath;

    cascadeName.assign(cascadePath);
    nestedCascadeName.assign(nestedCascadePath);
    
    if(!nestedCascade.load(nestedCascadeName)){
        fprintf(stderr,"Nested Classifier Did not load properly\n");
    }

    if(!cascade.load( cascadeName )){
        fprintf(stderr,"Cascade Classifier Did not load properly\n");
    }

    state->cascade = &cascade;

    state->nestedCascade = &nestedCascade; 
  
    if (!state->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    state->lcmgl = bot_lcmgl_init(state->lcm, "person_img");

    bot_core_image_t_subscribe(state->lcm , "CAMLCM_IMAGE", on_image, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    //add the image window to draw the image 
    cvNamedWindow( "result", 1 );
    
    //cvNamedWindow( "input", 1 );

    pthread_create(&state->work_thread, NULL, track_work_thread, state);

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
    
    

    return 0;
}


































































































































