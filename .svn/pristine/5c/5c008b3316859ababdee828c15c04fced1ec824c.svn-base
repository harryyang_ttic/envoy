#ifndef PARTFILTER_H_
#define PARTFILTER_H_

// Opencv includes
#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/legacy/compat.hpp"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/highgui/highgui.hpp"

#include "colorFeatures.h"
#include <time.h>

#define TRANS_X_STD 0.5
#define TRANS_Y_STD 1.0
#define TRANS_S_STD 0.001
#define  MAX_PARTICLES 50

#define SHOW_ALL 0
#define SHOW_SELECTED 1

#define A1  2.0
#define A2 -1.0
#define B0  1.0000

typedef struct particle {
  float x;          /**< current x coordinate */
  float y;          /**< current y coordinate */
  float s;          /**< scale */
  float xp;         /**< previous x coordinate */
  float yp;         /**< previous y coordinate */
  float sp;         /**< previous scale */
  float x0;         /**< original x coordinate */
  float y0;         /**< original y coordinate */
  int width;        /**< original width of region described by particle */
  int height;       /**< original height of region described by particle */
 // histogram* histo; /**< reference histogram describing region being tracked */
  float w;          /**< weight */
} particle;


class ParticleFilter  
{
public:
	void resetCCVWeights(void);
	cv::Rect getParticleRect();
	cv::Point getParticleCentre();
	ParticleFilter();
	~ParticleFilter();
        std::vector<particle> particles;
        // particle particles[MAX_PARTICLES];
	int nParticles;
	/* gsl_rng* rng; */
        cv::RNG rng;

	float weight;
	int objectID;
	
	void initParticles( cv::Rect region, int particlesPerObject);
    void resetParticles( cv::Rect region);                              
	void transition( int w, int h);
	void normalizeWeights( void);
	void resample( void );

	void displayParticles( cv::Mat& img, cv::Scalar nColor, cv::Scalar hColor , int c);
	void updateWeight(IplImage* frameHSV, histogram* objectHisto);
        void updateWeight(std::vector<cv::Point>& pts, cv::Mat& desc, cv::Mat& object_desc);
private:

	particle calTransition( particle p, int w, int h );
	void displayParticle( cv::Mat& img, particle p, cv::Scalar color);

};

#endif //PARTFILTER_H_
