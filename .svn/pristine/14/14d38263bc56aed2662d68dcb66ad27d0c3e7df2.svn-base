#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <bot_frames/bot_frames.h>
#include <bot_lcmgl_client/lcmgl.h>

#include <gsl/gsl_fit.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_blas.h>

#ifdef __APPLE
#include <GL/gl.h>
#else
#include <GL/gl.h>
#endif


#define POSE_LIST_SIZE 10


typedef struct
{
    double xy[2]; 
} xy_t; 

typedef struct
{
    BotParam   *b_server;
    BotFrames  *frames; 
    lcm_t      *lcm;
    GMainLoop *mainloop;
    pthread_t  work_thread;

    GList *pose_list;     

    bot_core_pose_t *last_pose;
    int publish_all;
    int verbose; 
} state_t;

static void on_laser(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
			     const bot_core_planar_lidar_t * msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 

    fprintf(stderr,".");

    bot_core_planar_lidar_t *laser = bot_core_planar_lidar_t_copy(msg);

    /*static float *new_ranges = NULL;
    if(new_ranges ==NULL){
        new_ranges = (float *) calloc(laser->nranges, sizeof(float));
    }
  
    for(i = 0; i < laser->num_readings; i++) {//prefilter points 
        float median = get_median(laser->range, laser->num_readings, i, 3);
        if(fabs(median - laser->range[i]) > 0.5){//2.0){
            new_ranges[i] = median;
        }    
        else{
            new_ranges[i] = laser->range[i];
        }
    }

    static float *sharp_ranges = NULL;
    if(sharp_ranges ==NULL){
        sharp_ranges = (float *) calloc(laser->num_readings, sizeof(float));
    }
    sharp_ranges[0] = new_ranges[0];
    sharp_ranges[laser->num_readings-1] = new_ranges[laser->num_readings-1];
    for(i = 1; i < laser->num_readings-1; i++) {
        sharp_ranges[i] = (-1*new_ranges[i-1] + 4 * new_ranges[i] - new_ranges[i+1])/2;

        //fprintf(stderr,"NR : %f , PR : %f, NeR : %f, Sharp : %f\n", new_ranges[i-1] , 
        //	    new_ranges[i], new_ranges[i+1], sharp_ranges[i]);
        if(sharp_ranges[i] - new_ranges[i] > 0.5){
            //fprintf(stderr,"+");
            //fprintf(stderr,"R : %f NR : %f , PR : %f, NeR : %f, Sharp : %f\n", laser->range[i], 
            //	      new_ranges[i-1] , new_ranges[i], new_ranges[i+1], sharp_ranges[i]);
        }
        } */

    double body_to_local[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                  "local", laser->utime,
                                                  body_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;        
    }

    double sensor_to_body[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "SKIRT_FRONT",
                                                  "body", laser->utime,
                                                  sensor_to_body)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;        
    }

    fprintf(stderr,"\n");

  
    int last_h_l_ind = -1;

    xy_t *local_pos = (xy_t *) calloc(laser->nranges, sizeof(xy_t));


    erlcm_cylinder_list_t cyl_msg; 
    cyl_msg.count = 0;

    cyl_msg.list = NULL;
    cyl_msg.utime = msg->utime;

    //FILE *file; 
    //file = fopen("poles_groundtruth.txt","a+"); /* apend file (add text to 
    //a file or create a file if it does not exist.*/ 

    for(int i=0; i < laser->nranges; i++){
        double theta = laser->rad0 + 
                (double)i * laser->radstep;

        double pos[3] = {.0,.0,.0};
        pos[0] = laser->ranges[i]*cos(theta); // + (laser->laser_pose.x - laser->robot_pose.x) ;
        pos[1] = laser->ranges[i]*sin(theta); // + (laser->laser_pose.y - laser->robot_pose.y) ;

        double l_pos[3] = {.0,.0,.0};
        //points[i].theta = theta;    
        bot_vector_affine_transform_3x4_3d (sensor_to_body, pos, l_pos);  
        local_pos[i].xy[0] = l_pos[0];
        local_pos[i].xy[1] = l_pos[1];
        
        //fprintf(stderr,"%f,%f,%f -> %f,%f,%f\n", pos[0], pos[1], pos[2], l_pos[0], l_pos[1], l_pos[2]);
    }

    double invalid_range = 30.0; 

    bot_lcmgl_t *lcmgl = bot_lcmgl_init(s->lcm, "pole_segments");

    lcmglLineWidth (3);      
    
    for(int i=1; i < laser->nranges; i++){
        if(laser->ranges[i] < 0.1 || laser->ranges[i] > invalid_range)
            continue;
        
        //this method is good to use for the person tracking also - port 
        if((laser->ranges[i] - laser->ranges[i-1]) > 0.3){
            fprintf(stderr,"Low to High : %f\n", laser->ranges[i] - laser->ranges[i-1]);
            if(last_h_l_ind != -1){
                fprintf(stderr, "Possible pole Found : %d - %d\n", last_h_l_ind, i-1);
                double avg_dist = 0; 
                double size = 0;
                 
                //do some additional filtering here
                int found_close = 0;
                int lower_ind = last_h_l_ind; 
                int higher_ind = i-1;
                for(int j= last_h_l_ind + 1; j < i; j++){
                    //fprintf(stderr, "\t %f\n", fabs(laser->ranges[j] - laser->ranges[j-1]));
                    if(fabs((laser->ranges[j] - laser->ranges[j-1])) > 0.05){ //laser issues - should prob discard 
                        if(found_close){
                            higher_ind = j-1; 
                            break;
                        }                        
                    }
                    else if(!found_close){
                        lower_ind = j-1; 
                        found_close = 1; 
                    }
                }

                size = hypot(local_pos[higher_ind].xy[0] - local_pos[lower_ind].xy[0], 
                      local_pos[higher_ind].xy[1] - local_pos[lower_ind].xy[1]);
                fprintf(stderr," Size : %f Dist : %f \n", size, avg_dist);

                fprintf(stderr, "%d-%d => %d-%d\n", last_h_l_ind, i-1, lower_ind, higher_ind); 

                //radius seems to be .105 m 

                if(higher_ind - lower_ind < 2){
                    last_h_l_ind = -1;
                    continue;
                }

                for(int j= lower_ind; j <= higher_ind; j++){
                    avg_dist += laser->ranges[j];
                }
                
                int data_size = higher_ind - lower_ind +1; 
                //circulatrity 
                double *a_data = calloc(data_size * 3, sizeof(double)); 
                double *b_data = calloc(data_size, sizeof(double)); 

                /*double *a_mul_data = calloc(size*size, sizeof(double)); 
                  memset(a_mul_data, 0, size*size*sizeof(double)); 
                */
                for(int k=0; k< data_size; k++){
                //for(int j= lower_ind; j <= higher_ind; j++){
                    double x = local_pos[k+lower_ind].xy[0];
                    double y = local_pos[k+lower_ind].xy[1]; 

                    a_data[3*k] = -2 * x;
                    a_data[3*k+1] = -2 * y;
                    a_data[3*k+2] = 1;

                    b_data[k] = -pow(x,2) -pow(y,2);
                }

                fprintf(stderr, "Done\n");
  
                gsl_matrix_view a_mat = gsl_matrix_view_array (a_data, data_size, 3);
                gsl_matrix_view b_mat = gsl_matrix_view_array (b_data, data_size, 1);
                gsl_matrix *a_trans_mat = gsl_matrix_alloc(3,data_size);

                gsl_matrix *a_result = gsl_matrix_alloc(3,3);
                gsl_matrix *a_inv_result = gsl_matrix_alloc(3,3);
                gsl_matrix *a_inv_trans_result = gsl_matrix_alloc(3,data_size);
  
                gsl_matrix *solution = gsl_matrix_alloc(3,1);
  
                gsl_matrix_transpose_memcpy(a_trans_mat, &a_mat.matrix); 

                gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
                                1.0, a_trans_mat, &a_mat.matrix,//->matrix,
                                0.0, a_result);//->matrix);
                int s;
                gsl_permutation * p = gsl_permutation_alloc (3);

                gsl_linalg_LU_decomp (a_result, p, &s);
                gsl_linalg_LU_invert(a_result, p, a_inv_result);

                gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
                                1.0, a_inv_result, a_trans_mat,//->matrix,
                                0.0, a_inv_trans_result);

                gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
                                1.0, a_inv_trans_result, &b_mat.matrix,//->matrix,
                                0.0, solution);

                //gsl_matrix_fprintf(stdout, solution, "%g");

                double x_c = gsl_matrix_get(solution, 0,0);
                double y_c = gsl_matrix_get(solution, 1,0);
                double k = gsl_matrix_get(solution, 2,0);
                double radius = pow(pow(x_c,2) + pow(y_c,2) - k, 0.5); 

                /**residual_sum = 0;
  
                for(int i=0; i< data_size; i++){
                    *residual_sum += pow(*radius - pow(pow(x[i] - x_c,2) + pow(y[i] - y_c, 2),0.5),2); 
                    } */ 

                fprintf(stderr, " Radius : %f\n",   radius);

                free(a_data);
                free(b_data);
                gsl_matrix_free (a_trans_mat);
                gsl_matrix_free (a_result);
                gsl_matrix_free (a_inv_result);
                gsl_matrix_free (a_inv_trans_result);
                gsl_matrix_free (solution);  

                avg_dist /= (higher_ind - lower_ind);//(i -last_h_l_ind);

                if(size < 0.5 && avg_dist < 5.0 && radius < 0.1){
                    cyl_msg.list = (erlcm_cylinder_model_t *) realloc(cyl_msg.list, sizeof(erlcm_cylinder_model_t) * (cyl_msg.count +1)); 
                    cyl_msg.list[cyl_msg.count].line_point[0] = x_c;//param[0]; 
                    cyl_msg.list[cyl_msg.count].line_point[1] = y_c; //param[1]; 
                    cyl_msg.list[cyl_msg.count].line_point[2] = 0;//param[2]; 

                    cyl_msg.list[cyl_msg.count].line_direction[0] = 0;//param[3]; 
                    cyl_msg.list[cyl_msg.count].line_direction[1] = 0;//param[4]; 
                    cyl_msg.list[cyl_msg.count].line_direction[2] = 1;//param[5]; 
            
                    cyl_msg.list[cyl_msg.count].radius = radius;//param[6]; 
                    cyl_msg.count++;
                    

                    //fprintf(file,"%f,%f,%f\n",laser->utime / 1.0e6, x_c, y_c, radius); /*writes*/ 
                    if(lcmgl){
                        lcmglColor3f(0.0, 1.0, 0);
                        lcmglBegin(GL_LINES);
                    }
                    
                    for(int j= lower_ind; j <= higher_ind; j++){
                        avg_dist += laser->ranges[j];
                        if(lcmgl){
                            double b_pos[3] = {local_pos[j].xy[0],local_pos[j].xy[1],.0};
                            double ls_pos[3] = {.0,.0,.0}; 
                            //points[i].theta = theta;    
                            bot_vector_affine_transform_3x4_3d (body_to_local, b_pos, ls_pos);  
                            lcmglVertex3d(ls_pos[0], ls_pos[1],0);                                      
                        }
                    }
                    
                    lcmglEnd();
                    
                    double c_center[3] = {x_c, y_c,0};
                    double l_center[3]; 

                    bot_vector_affine_transform_3x4_3d (body_to_local, c_center, l_center);  
                    lcmglCircle(l_center, radius);
                    
                    char seg_info[512];
                    sprintf(seg_info,"size : %f Rad : %f\n", size, radius);
                    double b_pos[3] = {local_pos[lower_ind].xy[0], local_pos[lower_ind].xy[1],0};
                    double ls_pos[3] = {.0,.0,.0}; 
                    bot_vector_affine_transform_3x4_3d (body_to_local, b_pos, ls_pos);  
                    double pos[3] = {ls_pos[0], ls_pos[1],0};
                    bot_lcmgl_text(lcmgl, pos, seg_info);
                }
            }                
            last_h_l_ind = -1;
        }
        else if((laser->ranges[i] - laser->ranges[i-1]) < -0.3){
            //fprintf(stderr,"High to Low : %f\n", laser->ranges[i] - laser->ranges[i-1]);     
            
            last_h_l_ind = i;
        }
    }

    if(cyl_msg.count){
        erlcm_cylinder_list_t_publish(s->lcm, "LIDAR_CYLINDER_LIST", &cyl_msg);
        
        free(cyl_msg.list);    
    }

    bot_lcmgl_switch_buffer (lcmgl);
    
    free(local_pos);
    
    //fclose(file);
    
    bot_core_planar_lidar_t_destroy(laser);
}

static void on_pose(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
			     const bot_core_pose_t * msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 
    if(s->last_pose !=NULL){
	bot_core_pose_t_destroy(s->last_pose);
    }
    s->last_pose = bot_core_pose_t_copy(msg);
    if(s->verbose){
	fprintf(stderr,"Pose : (%f,%f)\n",msg->pos[0], msg->pos[1]);//, rpy[2], vel[0] , vel[1]);
    }

    bot_core_pose_t *pose = bot_core_pose_t_copy(msg);
    
}

//pthread
static void *track_work_thread(void *user)
{
    state_t *s = (state_t*) user;

    printf("obstacles: track_work_thread()\n");

    while(1){
	fprintf(stderr, " T - called ()\n");
	
	usleep(50000);
    }
}

void read_parameters_from_conf(state_t *s)
{
    BotParam *b_param = s->b_server; 
    double max_t_vel =  bot_param_get_double_or_fail(b_param, "robot.max_t_vel");
    double max_r_vel = bot_param_get_double_or_fail(b_param, "robot.max_r_vel");
  
    char **planar_lidar_names = bot_param_get_all_planar_lidar_names(b_param);
  
    if(planar_lidar_names) {
	for (int pind = 0; planar_lidar_names[pind] != NULL; pind++) {
	    fprintf(stderr, "Channel : %s\n", planar_lidar_names[pind]);
	}
    }
  
    g_strfreev(planar_lidar_names);
}

//doesnt do anything right now - timeout function
gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;
  
    //do the periodic stuff 
    if(s->verbose){
	fprintf(stderr, "Callback - Timeout\n");
    }
    //return true to keep running
    return TRUE;
}

void subscribe_to_channels(state_t *s)
{
    //bot_core_pose_t_subscribe(s->lcm, "POSE", on_pose ,s);
    bot_core_planar_lidar_t_subscribe(s->lcm, "SKIRT_FRONT", on_laser, s); 
}  


static void usage(char * funcName)
{
    printf("Usage: %s [options]\n"
	   "options are:\n"
	   "--help,        -h    display this message \n"
	   "--publish_all, -a    publish robot laser messages for all channels \n"
	   "--verbose,     -v    be verbose", funcName);
    exit(1);

}

int 
main(int argc, char **argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));
    state->last_pose = NULL;
    state->pose_list = NULL;

    const char *optstring = "hav";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "publish_all", no_argument, 0, 'a' }, 
				  { "verbose", no_argument, 0, 'v' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    usage(argv[0]);
	    break;
	case 'a':
	    {
		fprintf(stderr,"Publishing all laser channels\n");
		break;
	    }
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
		state->verbose = 1;
		break;
	    }
	default:
	    {
		usage(argv[0]);
		break;
	    }
	}
    }

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
    state->b_server = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->b_server);

    //pthread_create(&state->work_thread, NULL, track_work_thread, state);

    subscribe_to_channels(state);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    read_parameters_from_conf(state);

    /* heart beat*/
    //    g_timeout_add_seconds (1, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


