#include "pcl-utils.hpp"

IntegralNormalEstimation::IntegralNormalEstimation() { 
}

IntegralNormalEstimation::~IntegralNormalEstimation() { 
    std::cerr << "integral normal estimation dtor" << std::endl;
}

void IntegralNormalEstimation::apply (const float* ubuff, const int width, const int height, const int scale, 
               const float depth_change_factor, float smoothing_size, float* normals) { 

    pcl::IntegralImageNormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ> ());
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal> ());
    // Populate point cloud
    cloud->width    = (int) (width / (double) scale) ;
    cloud->height   = (int) (height / (double) scale);
    cloud->is_dense = true;
    cloud->points.resize (cloud->width * cloud->height);

    // std::cerr << "pts: " << cloud->points.size() << std::endl;

    // boost::shared_ptr<vector<int> > search_inds (new vector<int> ()); // cloud_decimated->points.size()));
    for(int v=0, j=0; v<cloud->height; v++)
        for(int u=0; u<cloud->width; u++ ) { 
            int idx = v*cloud->width+u; 
            int fidx = 3*idx;
            // search_inds->push_back(j);
            cloud->points[idx].x = ubuff[fidx+0];
            cloud->points[idx].y = ubuff[fidx+1];
            cloud->points[idx].z = ubuff[fidx+2];
            // std::cerr << "xyz" << cloud->points[idx].x << " " << cloud->points[idx].y
            //           << " " << cloud->points[idx].z << std::endl;
        }

    // options: COVARIANCE_MATRIX, AVERAGE_3D_GRADIENT, AVERAGE_DEPTH_CHANGE, SIMPLE_3D_GRADIENT
    ne.setNormalEstimationMethod (ne.AVERAGE_3D_GRADIENT);
    ne.setDepthDependentSmoothing(true);
    ne.setMaxDepthChangeFactor(depth_change_factor); // 0.5f
    ne.setNormalSmoothingSize(smoothing_size);    // 10.f
    // options: BORDER_POLICY_MIRROR, BORDER_POLICY_IGNORE
    ne.setBorderPolicy(ne.BORDER_POLICY_MIRROR);
    ne.setInputCloud(cloud);
    ne.compute(*cloud_normals);

    // Allocate memory for normals_ output
    //--------------------------------------------------
    int sz = width*height*3;
    assert(normals != 0);

    for (int j=0; j<sz; j++) normals[j] = 0;
    // std::cerr << "ubuff:"; 
    // for (int j=0; j<sz; j++) std::cerr << ubuff[j] << " ";
    // std::cerr << std::endl;
    //--------------------------------------------------

    for (int j=0,j3=0; j<cloud_normals->points.size(); j++,j3=j*3) 
        if (!((cloud_normals->points[j].normal[0] != cloud_normals->points[j].normal[0]) || 
              (cloud_normals->points[j].normal[1] != cloud_normals->points[j].normal[1]) || 
              (cloud_normals->points[j].normal[2] != cloud_normals->points[j].normal[2])))
            normals[j3+0] = cloud_normals->points[j].normal[0], 
                normals[j3+1] = cloud_normals->points[j].normal[1], 
                normals[j3+2] = cloud_normals->points[j].normal[2];
        else
            normals[j3+0] = 0, 
                normals[j3+1] = 0, 
                normals[j3+2] = 0;

    return;
}
