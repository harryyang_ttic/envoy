#include "RobustWeighting.hpp"

#include <vector>
#include <algorithm>

using namespace std;
using namespace reacq;

double NoWeighting::computeCost(MatrixDynamic& iErrors) const {
    double cost = 0;
    for (int i = 0; i < iErrors.getNumElements(); ++i) {
        double e = iErrors(i);
        cost += e*e;
    }
    return cost;
}

MatrixDynamic NoWeighting::computeWeights(MatrixDynamic& iErrors) const {
    MatrixDynamic weights(iErrors.getNumElements());
    for (int i = 0; i < weights.getNumElements(); ++i) {
        weights(i) = 1;
    }
    return weights;
}

void NoWeighting::computeWeightParams(MatrixDynamic& iErrors) {
}


TukeyWeighting::TukeyWeighting() {
    mSigma2 = 1;
    setMinSigma(1);
}

double TukeyWeighting::computeCost(MatrixDynamic& iErrors) const {
    double cost = 0;
    for (int i = 0; i < iErrors.getNumElements(); ++i) {
        double e2 = iErrors(i);
        e2 *= e2;
        if (e2 > mSigma2) {
            cost += 1;
        }
        else {
            double d = 1-e2/mSigma2;
            cost += (1 - d*d*d);
        }
    }
    return cost;
}

MatrixDynamic TukeyWeighting::computeWeights(MatrixDynamic& iErrors) const {
    MatrixDynamic weights(iErrors.getNumElements());
    for (int i = 0; i < iErrors.getNumElements(); ++i) {
        double e2 = iErrors(i);
        e2 *= e2;
        weights(i) = (e2 > mSigma2) ? 0 : 1-e2/mSigma2;
    }
    return weights;
}

void TukeyWeighting::computeWeightParams(MatrixDynamic& iErrors) {
    vector<double> err2(iErrors.getNumElements());
    for (int i = 0; i < err2.size(); ++i) {
        double e = iErrors(i);
        err2[i] = e*e;
    }
    sort(err2.begin(), err2.end());

    double medianVal;
    if ((err2.size() % 2) == 0) {
        medianVal = 0.5*(err2[err2.size()/2] + err2[(err2.size()-1)/2]);
    }
    else {
        medianVal = err2[(err2.size()-1)/2];
    }
    mSigma2 = 1.5*sqrt(medianVal)*4.8;
    mSigma2 = std::max(mSigma2, mMinSigma);
    mSigma2 *= mSigma2;
}
