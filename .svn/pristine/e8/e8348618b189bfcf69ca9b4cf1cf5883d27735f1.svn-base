#include <stdio.h>
#include "ImPyramid.hpp"

ImPyramid::ImPyramid()
{
    nLevels=0;
    pImages=NULL;
    pXDerivatives=NULL;
    pYDerivatives=NULL;
}

ImPyramid::ImPyramid(const ImPyramid& other)
{
    pImages=NULL;
    pXDerivatives=NULL;
    pYDerivatives=NULL;
    copyData(other);
}

ImPyramid::~ImPyramid()
{
    if(pImages!=NULL)
        delete []pImages;
    if(pXDerivatives!=NULL)
        delete []pXDerivatives;
    if(pYDerivatives!=NULL)
        delete []pYDerivatives;
}

void ImPyramid::clear()
{
    if(pImages!=NULL)
        delete []pImages;
    if(pXDerivatives!=NULL)
        delete []pXDerivatives;
    if(pYDerivatives!=NULL)
        delete []pYDerivatives;
    pImages=NULL;
    pXDerivatives=NULL;
    pYDerivatives=NULL;
}

void ImPyramid::copyData(const ImPyramid& other)
{
    clear();
    nLevels=other.nLevels;
    
    pImages=new ImageType[nLevels];
    pXDerivatives=new ImageType[nLevels];
    pYDerivatives=new ImageType[nLevels];

    for(int i=0;i<nLevels;i++)
    {
        pImages[i]=other.pImages[i];
        pXDerivatives[i]=other.pXDerivatives[i];
        pYDerivatives[i]=other.pYDerivatives[i];
    }
}

void ImPyramid::CreatePyramid(const BiImage &m_Image, int nlevels)
{
    clear();
    nLevels=nlevels;

    pImages=new ImageType[nLevels];
    pXDerivatives=new ImageType[nLevels];
    pYDerivatives=new ImageType[nLevels];

    ImageType orgImage;
    orgImage.copy(m_Image);
    orgImage.im2double();

    ImageType smoothImage,grayImage,featureImage;
    ImageType concatenatedImage;
    double Laplacian[9]={0,-1,0,-1,4,-1,0,-1,0};

    for(int i=0;i<nLevels;i++)
    {
        orgImage.smoothing(smoothImage,3.5);
        smoothImage.desaturate(grayImage);
        grayImage.imfilter(featureImage,Laplacian,1);
        orgImage.concatenate(pImages[i],featureImage);

        // compute the derivatives
        pImages[i].setAdvancedDerivativeFilter();
        pImages[i].dx(pXDerivatives[i]);
        pImages[i].dy(pYDerivatives[i]);

        if(i<nLevels-1)
            smoothImage.imresize(orgImage,0.5);
    }
    //for(int i=0;i<nLevels;i++)
    //{
    //    pImages[i]=orgImage;
    //    orgImage.smoothing(smoothImage,3.5);
    //    smoothImage.dx(pXDerivatives[i]);
    //    smoothImage.dy(pYDerivatives[i]);
    //    pXDerivatives[i].setDerivative();
    //    pYDerivatives[i].setDerivative();
    //    if(i<nLevels-1)
    //        smoothImage.imresize(orgImage,0.5);
    //}
}

void ImPyramid::showPyramid(const QString &basename)
{
    ImageType orgDer,featureDer;
    QString extName=".jpg";
    //for(int i=0;i<nLevels;i++)
    //{
    //    pImages[i].separate(pImages[i].nchannels()-1,orgDer,featureDer);
    //    orgDer.setDerivative(false);
    //    orgDer.imwrite(basename+"_org_"+QString::number(i)+extName);
    //    featureDer.setDerivative();
    //    featureDer.imwrite(basename+"_feature_"+QString::number(i)+extName);


    //    pXDerivatives[i].separate(pImages[i].nchannels()-1,orgDer,featureDer);
    //    orgDer.imwrite(basename+"_org_dx_"+QString::number(i)+extName);
    //    featureDer.imwrite(basename+"_feature_dx_"+QString::number(i)+extName);

    //    pYDerivatives[i].separate(pImages[i].nchannels()-1,orgDer,featureDer);
    //    orgDer.imwrite(basename+"_org_dy_"+QString::number(i)+extName);
    //    featureDer.imwrite(basename+"_feature_dy_"+QString::number(i)+extName);
    //}
    for(int i=0;i<nLevels;i++)
    {
        pImages[i].imwrite(basename+"_org_"+QString::number(i)+extName);
        pXDerivatives[i].imwrite(basename+"_org_dx_"+QString::number(i)+extName);
        pYDerivatives[i].imwrite(basename+"_org_dy_"+QString::number(i)+extName);
    }
}

