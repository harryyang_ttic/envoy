#include "GridMap.hpp"
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <vector>
#include <algorithm>
#include <interfaces/map3d_interface.h>
#include <zlib.h>

using namespace std;
GridMap::~GridMap()
{
  free(hits);
  free(full_hits);
  free(visits);
  if (likelihoods != NULL)
    free(likelihoods);

}


GridMap::GridMap(double _xy0[2], double _xy1[2], double mPP, uint8_t _maxEvidence) :
  maxEvidence(_maxEvidence),msg(NULL)
{
  memcpy(xy0, _xy0, 2 * sizeof(double));
  memcpy(xy1, _xy1, 2 * sizeof(double));
  metersPerPixel = mPP;

  width = ceil((1.0 / metersPerPixel) * (xy1[0] - xy0[0]));
  height = ceil((1.0 / metersPerPixel) * (xy1[1] - xy0[1]));


  //make top right align with pixels
  xy1[0] = xy0[0]+width*metersPerPixel;
  xy1[1] = xy0[1]+height*metersPerPixel;

  if (width <= 0 || height < 0) {
    printf("ERROR:width or height is less than 0\n");
    exit(1);
  }
  numCells = width * height;
  hits = (uint8_t *) calloc(numCells, sizeof(uint8_t));
  full_hits = (uint8_t *) calloc(numCells, sizeof(uint8_t));
  visits = (uint8_t *) calloc(numCells, sizeof(uint8_t));
  likelihoods = (float *) calloc(numCells, sizeof(float));
}


const float * GridMap::getFloatMap()
{
  memset(likelihoods, 0, width * height * sizeof(float));
  int ixy[2];
  for (ixy[1] = 0; ixy[1] < height; ixy[1]++) {
    for (ixy[0] = 0; ixy[0] < width; ixy[0]++) {
      likelihoods[getInd(ixy)] = readValue(ixy);
    }
  }
  return likelihoods;
}

/**
 * This function adapted from the Python Imaging Library
 */
void GridMap::rayTrace(const int start[2], const int end[2], int hitInc, int visitInc, bool hitAll)
{
  int curr[2] = { start[0], start[1] };

  // normalize
  int xstep = 1;
  int ystep = 1;
  int dx = end[0] - start[0];
  if (dx < 0) {
    dx = -dx;
    xstep = -1;
  }
  int dy = end[1] - start[1];
  if (dy < 0) {
    dy = -dy;
    ystep = -1;
  }

  if (dx == 0) {
    // vertical
    for (int i = 0; i <= dy; i++) {
      int wasHit = hitAll || (curr[0] == end[0] && curr[1] == end[1]);
      updateValue(curr, wasHit * hitInc, visitInc);
      curr[1] = curr[1] + ystep;
    }
  }
  else if (dy == 0) {
    // horizontal
    for (int i = 0; i <= dx; i++) {
      int wasHit = hitAll || (curr[0] == end[0] && curr[1] == end[1]);
      updateValue(curr, wasHit * hitInc, visitInc);
      curr[0] += xstep;
    }
  }
  else if (dx > dy) {
    // bresenham, horizontal slope
    int n = dx;
    dy += dy;
    int e = dy - dx;
    dx += dx;

    for (int i = 0; i <= n; i++) {
      int wasHit = hitAll || (curr[0] == end[0] && curr[1] == end[1]);
      updateValue(curr, wasHit * hitInc, visitInc);
      if (e >= 0) {
        curr[1] += ystep;
        e -= dx;
      }
      e += dy;
      curr[0] += xstep;
    }
  }
  else {
    // bresenham, vertical slope
    int n = dy;
    dx += dx;
    int e = dx - dy;
    dy += dy;

    for (int i = 0; i <= n; i++) {
      int wasHit = hitAll || (curr[0] == end[0] && curr[1] == end[1]);
      updateValue(curr, wasHit * hitInc, visitInc);
      if (e >= 0) {
        curr[0] += xstep;
        e -= dy;
      }
      e += dx;
      curr[1] += ystep;
    }
  }
}

void GridMap::generate_likelihoods()
{
  int ixy[2];
  for (ixy[1] = 0; ixy[1] < height; ixy[1]++) {
    for (ixy[0] = 0; ixy[0] < width; ixy[0]++) {
      float cellvalue = readValue(ixy);
      if (cellvalue < -0.99)
        cellvalue = MAP_UNEXPLORED_VALUE; //unexplored regions
      else if (cellvalue > -0.2 && cellvalue < 0.2) {
        cellvalue = MAP_FREE_VALUE; //known free space
      }
      //this is where the clearing needs to happen
      likelihoods[ixy[0] * height + ixy[1]] = cellvalue; //msg/viewer/other stuff expects column major...
    }
  }
}

erlcm_gridmap_t * GridMap::get_comp_gridmap_t()
{
  //TODO: move to gridmap...

  static erlcm_gridmap_t lcm_msg;
  lcm_msg.config.x_size = width;
  lcm_msg.config.y_size = height;
  lcm_msg.config.resolution = metersPerPixel;
  lcm_msg.config.map_name = (char *) "isamMap";
  double center[2];
  getCenterLocation(center);
  lcm_msg.center.x = center[0];
  lcm_msg.center.y = center[1];
  lcm_msg.type = ERLCM_GRIDMAP_T_MAP_TYPE_OCCUPANCY;
  lcm_msg.sizeX2 = -1;
  lcm_msg.sizeY2 = -1;
  lcm_msg.map_version++;

  uLong uncompressed_size = lcm_msg.config.x_size * lcm_msg.config.y_size * sizeof(float);
  double global_pos[3];

  uLong compress_buf_size;
  int compress_return = -1;

  compress_buf_size = uncompressed_size * 1.1 + 12; //with extra space for zlib
  lcm_msg.map = (uint8_t *) malloc(compress_buf_size);
  //  carmen_test_alloc(lcm_msg.map);

  compress_return = compress2((Bytef *) lcm_msg.map, (uLong *) &compress_buf_size, (Bytef *) likelihoods,
      (uLong) uncompressed_size, Z_BEST_SPEED);
  if (compress_return != Z_OK) {
    fprintf(stderr, "ERROR: Could not compress map!\n");
    exit(1);
  }

  lcm_msg.size = compress_buf_size;
  lcm_msg.compressed = 1;
  lcm_msg.utime = bot_timestamp_now();
  return &lcm_msg;
}

erlcm_gridmap_t * GridMap::get_gridmap_t()
{
  //TODO: move to gridmap...

  static erlcm_gridmap_t lcm_msg;
  lcm_msg.config.x_size = width;
  lcm_msg.config.y_size = height;
  lcm_msg.config.resolution = metersPerPixel;
  lcm_msg.config.map_name = (char *) "isamMap";
  double center[2];
  getCenterLocation(center);
  lcm_msg.center.x = center[0];
  lcm_msg.center.y = center[1];
  lcm_msg.type = ERLCM_GRIDMAP_T_MAP_TYPE_OCCUPANCY;
  lcm_msg.sizeX2 = -1;
  lcm_msg.sizeY2 = -1;
  lcm_msg.map_version++;

  uLong uncompressed_size = lcm_msg.config.x_size * lcm_msg.config.y_size * sizeof(float);
  double global_pos[3];
  int ixy[2];
  for (ixy[1] = 0; ixy[1] < height; ixy[1]++) {
    for (ixy[0] = 0; ixy[0] < width; ixy[0]++) {
      float cellvalue = readValue(ixy);
      if (cellvalue < -0.99)
        cellvalue = MAP_UNEXPLORED_VALUE; //unexplored regions
      else if (cellvalue > -0.2 && cellvalue < 0.2) {
        cellvalue = MAP_FREE_VALUE; //known free space
      }
      //this is where the clearing needs to happen
      likelihoods[ixy[0] * height + ixy[1]] = cellvalue; //msg/viewer/other stuff expects column major...
    }
  }

  uLong compress_buf_size;
  int compress_return = -1;

  compress_buf_size = uncompressed_size * 1.1 + 12; //with extra space for zlib
  lcm_msg.map = (uint8_t *) malloc(compress_buf_size);
  //  carmen_test_alloc(lcm_msg.map);

  compress_return = compress2((Bytef *) lcm_msg.map, (uLong *) &compress_buf_size, (Bytef *) likelihoods,
      (uLong) uncompressed_size, Z_BEST_SPEED);
  if (compress_return != Z_OK) {
    fprintf(stderr, "ERROR: Could not compress map!\n");
    exit(1);
  }

  lcm_msg.size = compress_buf_size;
  lcm_msg.compressed = 1;
  lcm_msg.utime = bot_timestamp_now();
  return &lcm_msg;

}

