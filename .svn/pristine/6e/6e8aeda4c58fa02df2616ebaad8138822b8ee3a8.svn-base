// --------------------------------------------------------------
/** @file HaarBlock.h
 **
 ** @author Ingmar Posner
 ** @date 13/6/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#ifndef __LEGET_HAARBLOCK_H__
#define __LEGET_HAARBLOCK_H__

#include <iostream>
#include <utility>
#include <typeinfo>
#include <cv.h>


namespace leget
{
    namespace features
    {
        
        // --------------------------------------------------------------
        /** HaarBlock  base class of a sub-block of a haar geometry. 
         ** Each block knows how to calculate a specified statistic given an **integral image** to work with.
         **/
        // --------------------------------------------------------------
        template <typename MetricType> 
        class HaarBlock
        {
        public:
            // ************************************************
            // ***************** CON-/DESTRUCT ****************
            // ************************************************
            
            // --------------------------------------------------------------
            /**
             ** HaarBlock::HaarBlock() ....
             */
            // --------------------------------------------------------------
            HaarBlock(float fTopLeftX, float fTopLeftY, float fBottomRightX, 
                      float fBottomRightY, const MetricType &metric = MetricType()) 
            : _fTopLeftX(fTopLeftX), _fTopLeftY(fTopLeftY), _fBottomRightX(fBottomRightX), _fBottomRightY(fBottomRightY), 
            _dfValue(0.0), _bHasBeenSet(false), _pPrevRoiData(0), _uPrevRoiWidth(0), _uPrevRoiHeight(0), _metric(metric)
            {}
            
            
            // ****************************************
            // ***************** UTILS ****************
            // ****************************************
            
            // --------------------------------------------------------------
            /**
             **  ....
             */
            // --------------------------------------------------------------
            void SetRelativeDims(unsigned int uRoiWidth, unsigned int uRoiHeight)
            {
                // check if the values are correct already
                if ( (uRoiWidth == _uPrevRoiWidth) && (uRoiHeight == _uPrevRoiHeight) )
                {
                    // nothing to be done
                    _bHasBeenSet = true;
                    return;
                }
                else
                {
                    
                    // update cache
                    _uPrevRoiWidth = uRoiWidth;
                    _uPrevRoiHeight = uRoiHeight;
                    
                    // calculate the actual block dimensions
                    // ... adjust width to account for zero-indexing (=> -1)
                    uRoiWidth -= 1;
                    uRoiHeight -= 1;
                    
                    // ...extract corner point coordinates of the block within the region of interest
                    _uRelativeTopLeftX = static_cast<unsigned int>(uRoiWidth * _fTopLeftX);
                    _uRelativeTopLeftY = static_cast<unsigned int>(uRoiHeight * _fTopLeftY);
                    _uRelativeBottomRightX = static_cast<unsigned int>(uRoiWidth * _fBottomRightX);
                    _uRelativeBottomRightY = static_cast<unsigned int>(uRoiHeight * _fBottomRightY);
                    
                    unsigned int uRelativeWidth = (_uRelativeBottomRightX - _uRelativeTopLeftX);
                    unsigned int uRelativeHeight = (_uRelativeBottomRightY - _uRelativeTopLeftY); 
                    _uBlockArea = (uRelativeWidth) * (uRelativeHeight);
                    
                    // housekeeping 
                    _bHasBeenSet = true;
                }
            }
            
            
            // --------------------------------------------------------------
            /**
             **  ....
             ** @note ASSUMPTION: rois are of the same size!
             ** @note We are forced here to use Double images(!) becasue of opencv.
             */
            // --------------------------------------------------------------
            double computeMetric(const std::pair< cv::Mat_<double>, cv::Mat_<double> > &rois)
            {
                // rois here will contain two rois: intImg and intSqrImg which will already point to the correct sub-matrix of the images!
                
                // compute the metric
                _dfValue = _metric(rois, _uBlockArea, _uRelativeTopLeftX, _uRelativeTopLeftY, _uRelativeBottomRightX, _uRelativeBottomRightY);
                
                return _dfValue;
            } 
            
            
            // --------------------------------------------------------------
            /**
             ** HaarBlock::Print() prints the contents of the current object.
             */
            // --------------------------------------------------------------
            void print(const int nID=-1) const
            {
                if(nID < 0)
                    std::cout << "**** [BLOCK] ****" << std::endl;
                else
                    std::cout << "**** [BLOCK " << nID << "] ****" << std::endl;
                
                std::cout << "Type: " << typeid(_metric).name() << std::endl;
                
                std::cout << "Proportions: TopLeftX: " << _fTopLeftX << " TopLeftY: " << _fTopLeftY << " BottomRightX: " << _fBottomRightX
                << " BottomRightY: " << _fBottomRightY << std::endl;
                
                if (_bHasBeenSet)
                {
                    std::cout << "Relative Pixel Values: TopLeftX: " << _uRelativeTopLeftX << " TopLeftY: " << _uRelativeTopLeftY
                    << " BottomRightX: " << _uRelativeBottomRightX << " BottomRightY: " << _uRelativeBottomRightY
                    << " Area: " << _uBlockArea << std::endl;
                }
                
                if (_bHasBeenSet)
                    std::cout << "Value: " << _dfValue << std::endl;
            }
            
            
            // ******************************************
            // ***************** MEMBERS ****************
            // ******************************************
            float _fTopLeftX;         /**< Relative X position of top left corner within ROI(!). */
            float _fTopLeftY;         /**< Relative Y position of top left corner within ROI(!). */
            float _fBottomRightX;     /**< Relative X position of bottom right corner within ROI(!). */
            float _fBottomRightY;     /**< Relative Y position of bottom right corner within ROI(!). */
            double _dfValue;          /**< The feature value once computed. */
            bool _bHasBeenSet;        /**< Indicator of whether dimensions have been set. */
            
            // Some cache variables
            const unsigned int * _pPrevRoiData;
            cv::Size _prevRoiSize;
            unsigned int _uPrevRoiWidth;
            unsigned int _uPrevRoiHeight;
            
            MetricType _metric;        /**< The metric to be calculated. */
            
            
            // block-box coors relative to roi origin as per roi dimensions
            unsigned int _uRelativeTopLeftX;
            unsigned int _uRelativeTopLeftY;
            unsigned int _uRelativeBottomRightX; 
            unsigned int _uRelativeBottomRightY;
            unsigned int _uBlockArea;
            
        };
        
        // --------------------------------------------------------------
        /** CMeanMetric  a metric to calculate the mean of a block
         ** @note We are forced here to use Double images(!) becasue of opencv.
         **/
        // --------------------------------------------------------------
        class CMeanMetric
        {
        public:
            
            // ****************************************
            // ***************** UTILS ****************
            // ****************************************
            
            /** Operator() used to calculate the mean. */
            inline double operator()(const std::pair< cv::Mat_<double>, cv::Mat_<double> > &rois, const unsigned int uArea, 
                                     const unsigned int uTopLeftX, const unsigned int uTopLeftY, 
                                     const unsigned int uBottomRightX, const unsigned int uBottomRightY)
            {
                
                // note: the left-most column and top-most row does not contribute.
                
                // get values from integral image
                double dfIntImTopLeftVal = rois.first.at<double>(uTopLeftY, uTopLeftX);
                double dfIntImBottomRightVal = rois.first.at<double>(uBottomRightY, uBottomRightX);
                double dfIntImTopRightVal = rois.first.at<double>(uTopLeftY, uBottomRightX);
                double dfIntImBottomLeftVal = rois.first.at<double>(uBottomRightY, uTopLeftX);
                
                double dfBlockSum = dfIntImBottomRightVal + dfIntImTopLeftVal - (dfIntImTopRightVal + dfIntImBottomLeftVal);
                
                // this should give us the mean
                return (dfBlockSum / uArea);
            }
        };
        
        // --------------------------------------------------------------
        /** MeanBlock  a Haar block for mean features.
         **/
        // --------------------------------------------------------------
        class MeanBlock : public HaarBlock<CMeanMetric>
        {
        public:
            // ************************************************
            // ***************** CON-/DESTRUCT ****************
            // ************************************************
            MeanBlock(float fTopLeftX, float fTopLeftY, float fWidth, float fHeight) 
            : HaarBlock<CMeanMetric>(fTopLeftX, fTopLeftY, fWidth, fHeight)
            {}
        };
        
        // --------------------------------------------------------------
        /** VarianceMetric  a metric to calculate the variance of a block
         ** @note We are forced here to use Double images(!) becasue of opencv.
         **/
        // --------------------------------------------------------------
        class VarianceMetric
        {
        public:
            
            // ****************************************
            // ***************** UTILS ****************
            // ****************************************
            
            /** Operator() used to calculate the variance. */
            inline double operator()(const std::pair< cv::Mat_<double>, cv::Mat_<double> > &rois, const unsigned int uArea, 
                                     const unsigned int uTopLeftX, const unsigned int uTopLeftY, 
                                     const unsigned int uBottomRightX, const unsigned int uBottomRightY)
            {
                
                // get values from integral image
                
                double dfIntImTopLeftVal = rois.first.at<double>(uTopLeftY, uTopLeftX);
                double dfIntImBottomRightVal = rois.first.at<double>(uBottomRightY, uBottomRightX);
                double dfIntImTopRightVal = rois.first.at<double>(uTopLeftY, uBottomRightX);
                double dfIntImBottomLeftVal = rois.first.at<double>(uBottomRightY, uTopLeftX);
                double dfIntBlockSum = dfIntImBottomRightVal + dfIntImTopLeftVal - (dfIntImTopRightVal + dfIntImBottomLeftVal);
                
                // get values from integral images of squared data
                double dfIntSqrImTopLeftVal = rois.second.at<double>(uTopLeftY, uTopLeftX);
                double dfIntSqrImBottomRightVal = rois.second.at<double>(uBottomRightY, uBottomRightX);
                double dfIntSqrImTopRightVal = rois.second.at<double>(uTopLeftY, uBottomRightX);
                double dfIntSqrImBottomLeftVal = rois.second.at<double>(uBottomRightY, uTopLeftX);
                double dfIntSqrBlockSum = dfIntSqrImBottomRightVal + dfIntSqrImTopLeftVal - (dfIntSqrImTopRightVal + dfIntSqrImBottomLeftVal);
                
                double dfIntImBlockMean = dfIntBlockSum / uArea;
                double dfIntSqrImBlockMean = dfIntSqrBlockSum / uArea;
                
                // return the variance
                return ( dfIntSqrImBlockMean - (dfIntImBlockMean * dfIntImBlockMean) );
            }
        };
        
        // --------------------------------------------------------------
        /** VarianceBlock  a Haar block for variance features.
         **/
        // --------------------------------------------------------------
        class VarianceBlock : public HaarBlock<VarianceMetric>
        {
        public:
            // ************************************************
            // ***************** CON-/DESTRUCT ****************
            // ************************************************
            VarianceBlock(float fTopLeftX, float fTopLeftY, float fWidth, float fHeight) 
            : HaarBlock<VarianceMetric>(fTopLeftX, fTopLeftY, fWidth, fHeight)
            {}
        };
    }
}

#endif //__LEGET_HAARBLOCK_H__
