#!/bin/bash 
arg0=$1

function _status
{
    echo -e "Report\n===============\n" 
    echo -e "cpuid\tfreq\tgovernor"
    
    for i in `seq 0 $pn`; do 
	echo $i>>/tmp/Itmp; 
	cat /sys/devices/system/cpu/cpu$i/cpufreq/scaling_cur_freq>>/tmp/Xtmp; 
	cat /sys/devices/system/cpu/cpu$i/cpufreq/scaling_governor>>/tmp/Ytmp; 
    done; 
    paste /tmp/Itmp /tmp/Xtmp /tmp/Ytmp ; 
    rm /tmp/Xtmp /tmp/Ytmp /tmp/Itmp
}


PNUM=`cat /proc/cpuinfo |  grep "^processor" -c`

if [ "X$arg0" == "X" ]; then
    echo "$0 [ (governor govname) | ([freq freq-odd freq-even freq-half1 freq-half2] freq) | (freq-single pid freq) ]"
    echo
    echo "Frequency governors: "
    cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors | sed 's/ /\n/g'
    echo
    echo "Frequencies: "
    cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies 

    exit 0;
fi


pn=`echo "$PNUM - 1" | bc`
if [ "X$arg0" == "Xstatus" ]; then
    _status
    exit 0;
fi

val=$2

start=0
step=1
end=$pn

if [ "X$arg0" == "Xfreq-odd" ]; then
    start=1
    step=2
    arg0="freq"
elif [ "X$arg0" == "Xfreq-even" ]; then
    start=0
    step=2
    arg0="freq"
elif [ "X$arg0" == "Xfreq-half1" ]; then
    start=0
    step=1
    end=`echo $PNUM / 2 | bc`
    arg0="freq"
elif [ "X$arg0" == "Xfreq-half2" ]; then
    start=`echo $PNUM / 2 | bc`
    step=1
    end=$pn
    arg0="freq"
elif [ "X$arg0" == "Xfreq-single" ]; then
    start=$2
    step=1
    end=$2
    val=$3
    arg0="freq"
fi


if [ "X$val" ==  "X" ]; then echo "Expecting new value as 2nd parameter"; exit -1; fi

LOG="/tmp/scale.log"
if [ "X$arg0" == "Xgovernor" ]; then
    file=scaling_governor

    for i in `seq $start $step $end`; do
	echo $2 | sudo tee /sys/devices/system/cpu/cpu$i/cpufreq/$file > /dev/null
    done;
    
    sudo /etc/init.d/cpufrequtils start
    echo "Restarted Frequency Utils"

elif [ "X$arg0" == "Xfreq" ]; then
    file=scaling_cur_freq    
    
    touch $LOG
    echo  > $LOG
    date > $LOG
    for i in `seq $start $step $end`; do
	echo "userspace" | sudo tee /sys/devices/system/cpu/cpu$i/cpufreq/scaling_governor > /dev/null
	echo "$val" | sudo tee /sys/devices/system/cpu/cpu$i/cpufreq/scaling_setspeed > /dev/null

    done;

else
    echo "Unknown command. Skipping..."
fi

_status;


