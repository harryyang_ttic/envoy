#! /bin/bash

#echo $ENVOY_HOME/$1

#ls $ENVOY_HOME/$1
#`cd $ENVOY_HOME/$1`

function cdenvoy() {
  cd $ENVOY_HOME/$1
}

function cdenvoysoftware() {
  cd $ENVOY_HOME/software/$1
}

function cdenvoyconfig() {
  cd $ENVOY_HOME/config
}

function cdlog() {
  cd ~/lcm_logs
}

function sshenvoy() {
    ssh envoy@envoy-wireless
}