############################################################
# 
# determine LCM_DEFAULT_URL
# if this is the robot use one
# if not use another

MY_HOST := $(shell hostname)
ROBOT_HOSTS := mwalter-agile envoy-netbook-01 saclap



ifneq (,$(findstring $(MY_HOST),$(ROBOT_HOSTS)))
    # is a robot host
    MY_LCM_DEFAULT_URL := "udpm://239.255.12.67:6712?ttl=1" #should be 1 - at some point
else
    # is not a robot host
    MY_LCM_DEFAULT_URL := "udpm://239.255.12.67:6712?ttl=0"
endif

