#!/usr/bin/perl
#
# Produces a # (and changes)

$ARGC = $#ARGV + 1;

if ($ARGC < 1)
{
    print ("Usage: svnvers DEFINENAME [svn-path] [svn-path] ...");
}

$definename = $ARGV[0];

$svnversion = 0;
$status = "";

for ($i = 1; $i < $ARGC; ++$i)
{
    chdir $ARGV[$i];
    $_ = `svn info`;

    /Last\ Changed\ Rev: ([0-9]+)/ ;

    if ($1 > $svnversion)
    {
	$svnversion = $1;
    }
    $_ = `svn status`;

    if (/^[^?].*/m)
    {
	$status = "+changes";	
    }
}

$_ = `svn status`;

if (lc($status) eq "")
{
    $status = "svn";
}

print "const char ".$definename."[] = \"$svnversion$status\";\n";

