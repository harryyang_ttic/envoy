# Default 8kHz sapphire configuration file.

# Recognized variables are:
#   -spcs       (segment pcs file)
#   -bpcs       (boundary pcs file)
#   -smodels    (segment models file)
#   -sfmmodels  (segment fastmatch models file)
#   -sweights   (segment weights file)
#   -bmodels    (boundary models file)
#   -dmodels    (duration models file)
#   -slabels    (segment labels file, default $domain.slabels)
#   -blabels    (boundary labels file, default $domain.blabels)
#   -strim      (segment trim file)
#   -bcd        (bcd file)
#   -bigram     (viterbi bigram file)
#   -wconf      (word confidence model file)
#   -uconf      (utterance confidence model file)
#   -as_ngram   (astar ngram file)
#   -nbest      (N A* paths to keep)
#   -vprune	(viterbi pruning, default 800)

# automatically optimized weights 
set sscale 0.161
set bscale 0.249
set word_weight -1.593
set segment_weight -0.346

# set defaults
set_if_unset bpcs	None
set_if_unset bmodels	None
set_if_unset blabels	None
set_if_unset smodels	None
set_if_unset slabels	None
set_if_unset ffst	None
set_if_unset sfst	None
set_if_unset lfst	None
set_if_unset wconf	None
set_if_unset uconf	None
set_if_unset wlex	None
set_if_unset vprunenodes 1000
set_if_unset vprune	20
set_if_unset aprune	$vprune
set_if_unset nbest      10

puts "nbest: $nbest"
puts "bpcs: $bpcs"
puts "smodels: $smodels"
puts "slabels: $slabels"
puts "bmodels: $bmodels"
puts "bmodels: $smodels"
puts "blabels: $blabels"
puts "ffst: $ffst"
puts "sfst: $sfst"
puts "lfst: $lfst"
puts "wconf: $wconf"
puts "uconf: $uconf"
puts "wlex: $wlex"
puts "vprune: $vprune"
puts "aprune: $aprune"
puts "vprunenodes: $vprunenodes"

set_if_unset server_client_order 0

if {[info command w] == ""} {
  s_waveform w
}

s_transform_waveform tw \
    -waveform w \
    -samplerate 8000 \
    -preemphasis 0.97

s_stft stft \
    -waveform tw \
    -framedurationms 5.0 \
    -windowdurationms 25.6 \
    -preemphasis 0.0 \
    -dft 256 

s_mfsc_from_spectrum mfsc \
    -spectrum stft \
    -upperfreq 4000.0

s_rotate_spectrum unnorm_mfcc \
    -spectrum mfsc \
    -type cepstrum \
    -numout 14

s_segment_from_mfcc segs \
    -mfcc unnorm_mfcc

s_spectrum_mean_norm mfcc \
    -spectrum unnorm_mfcc \
    -steptime 0.5

# Duration measurement
s_duration log_duration \
    -segs segs \
    -segstype segs \
    -log yes

# Boundary measurements
# boundary measurements: end_frames are not included in average
# "-15 -7 -7 -3 -3 -1 -1 0 0 1 1 3 3 7 7 15" # 1 2 4 8 -> 5 10-15 20-35 40-75

s_avg_vector b-4 \
    -segs segs \
    -boundsorsegstype bounds \
    -parent mfcc \
    -starttime bound-75ms \
    -endtime bound-35ms

s_avg_vector b-3 \
    -segs segs \
    -boundsorsegstype bounds \
    -parent mfcc \
    -starttime bound-35ms \
    -endtime bound-15ms

s_avg_vector b-2 \
    -segs segs \
    -boundsorsegstype bounds \
    -parent mfcc \
    -starttime bound-15ms \
    -endtime bound-5ms

s_avg_vector b-1 \
    -segs segs \
    -boundsorsegstype bounds \
    -parent mfcc \
    -starttime bound-5ms \
    -endtime bound

s_avg_vector b+1 \
    -segs segs \
    -boundsorsegstype bounds \
    -parent mfcc \
    -starttime bound \
    -endtime bound+5ms

s_avg_vector b+2 \
    -segs segs \
    -boundsorsegstype bounds \
    -parent mfcc \
    -starttime bound+5ms \
    -endtime bound+15ms

s_avg_vector b+3 \
    -segs segs \
    -boundsorsegstype bounds \
    -parent mfcc \
    -starttime bound+15ms \
    -endtime bound+35ms

s_avg_vector b+4 \
    -segs segs \
    -boundsorsegstype bounds \
    -parent mfcc \
    -starttime bound+35ms \
    -endtime bound+75ms

set_if_unset bound_meas {b-4 b-3 b-2 b-1 b+1 b+2 b+3 b+4}
set_if_unset bnumdims	50

s_pcs rot_bound_meas \
    -parent $bound_meas \
    -trpooled yes \
    -trcorrelation yes \
    -labelsfile $blabels \
    -pcsfile $bpcs \
    -numout $bnumdims

s_classifier seg_scores \
    -type "mixture_diagonal_gaussian" \
    -labelsfile "$slabels" \
    -parent {log_duration} \
    -modelsfile "$smodels" \
    -expscale 1.0 \
    -scale $sscale \
    -mindatapoints 50 \
    -maxmix 5 \
    -lazy yes

s_classifier bound_scores \
   -type "mixture_diagonal_gaussian" \
   -labelsfile $blabels \
   -parent rot_bound_meas \
   -expscale 1.0 \
   -scale $bscale \
   -mindatapoints $bnumdims \
   -maxmix 50 \
   -modelsfile $bmodels \
   -lazy yes

s_fst_viterbi viterbi \
    -segs segs \
    -segstype segs \
    -bounds segs \
    -boundstype bounds \
    -boundaryscores bound_scores \
    -segmentscores seg_scores \
    -vprunenodes $vprunenodes \
    -vprune $vprune \
    -aprune $aprune \
    -ffst $ffst \
    -sfst $sfst \
    -lfst $lfst \
    -segment_weight $segment_weight \
    -word_weight $word_weight \
    -nbest $nbest \
    -nbestpathscmd "lazy" \
    -precomposeboost no \
    -sortnodes no


s_process_nbest process_nbest \
    -segs segs \
    -segstype segs \
    -bounds segs \
    -boundstype bounds \
    -viterbi viterbi \
    -bound_scores bound_scores \
    -seg_scores seg_scores \
    -redistribute_multiwords "- _"

#s_confidence confidence \
#    -nbest_list process_nbest \
#    -bound_scores bound_scores \
#    -wconf_file $wconf \
#    -uconf_file $uconf

# for Emacs...
# Local Variables:
# mode: tcl
# fill-column: 96
# comment-column: 50
# tcl-indent-level: 2
# End:
