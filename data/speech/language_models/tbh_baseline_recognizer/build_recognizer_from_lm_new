#!/bin/tcsh -f

set SRILM = /usr/local/srilm
set path = ($SRILM/bin $SRILM/bin/i686 $path)

if ($# < 1 || $# > 2) then
    echo "usage: `basename $0` domain_prefix [default_prefix]"
    exit 1
endif

set domain = $1
if ($# < 2) then
    set default = "~/6.345/students/lectures/models/lexicon/lectures"
#   set default = "/usr/sls/summit/lexicon/sls_dict"
else
    set default = $2
endif

###############################################################################

# Required pre-existing files
set lm           = $domain.lm                # N-gram language model (ARPA format)

# Optional pre-existing file with defaults
set diphones     = acoustic_models/$domain.diphones.fstb     # Diphone:Phone FST
set pron_rules   = acoustic_models/$domain.pron.rules        # Phonological rules
set dict         = acoustic_models/$domain.full.baseforms    # Baseform pronunciation dictionary
set reductions   = acoustic_models/$domain.reductions.rules  # Word reduction rules 
set sweights     = acoustic_models/$domain.sweights          # Phone segment weights
set wweights     = acoustic_models/$domain.wweights          # Word weights
if (! -f $diphones)   set diphones   = $default.diphones.fstb
if (! -f $pron_rules) set pron_rules = $default.pron.rules
if (! -f $dict)       set dict       = $default.full.baseforms
if (! -f $reductions) set reductions = $default.reductions.rules
if (! -f $sweights)   set sweights   = $default.sweights
if (! -f $wweights)   set wweights   = $default.wweights

# Optional pre-existing file

# Automatically created files
set baseforms    = $domain.baseforms     # In-use subset of pronunciation dictionary
set vocab        = $domain.vocab         # In-use vocabulary
set spoken_vocab = $domain.spoken.vocab  # In-use vocabulary + reduced variations
set bigram       = $domain.2.lm          # Bigram derived from lm (ARPA format)
set bigram_fst   = $domain.2.fst         # Bigram FST

# Final output files
set ffst         = $domain.ffst  # First-pass lexical/language model FST
set sfst         = $domain.sfst  # Second-pass reranking model FST
set lfst         = $domain.lfst  # Language model FST (for evaluation)

###############################################################################

if (! -f $diphones) then
    echo "Diphone mapping file '$diphones' not found"
    exit 1
endif

if (! -f $pron_rules) then
    echo "Pronunciation rules file '$pron_rules' not found"
    exit 1
endif

if (! -f $dict) then
    echo "Pronunciation dictionary file '$dict' not found"
    exit 1
endif

if (! -f $lm) then
    echo "N-gram grammar file '$lm' not found"
    exit 1
endif

###############################################################################

echo "Diphone mapping:          $diphones"
echo "Pronunciation rules:      $pron_rules"
echo "Pronunciation dictionary: $dict"
echo "N-gram grammar:           $lm"
if (-f $sweights)   echo "Phone weights:            $sweights"
if (-f $wweights)   echo "Word weights:             $wweights"
if (-f $reductions) echo "Word reductions:          $reductions"
echo

###############################################################################

# FST Composition: C [S] P L [W] R G

echo "extracting vocabulary from grammar"
ngram -lm $lm -unk -write-vocab - | grep -v "<unk>\|<s>\|</s>\|-pau-" > tmp.vocab
echo "<unknown>" >>! tmp.vocab
sort tmp.vocab >! $vocab

echo "building grammar: G"
ngram -lm $lm -unk -order 2 -write-lm $bigram
ngram_fst $bigram $bigram_fst
ngram_fst $lm $lfst
fst_ngram_difference $lfst $bigram_fst $sfst

if ( -f $reductions ) then
    echo "creating reductions FST: R"
    build_reductions_fst.pl -output_vocab $vocab -reduction_rules $reductions -fst_out $domain.R.fst
    fst_alphabet -i $domain.R.fst >! $spoken_vocab
else
    ln -s -f $vocab $spoken_vocab
endif

echo "looking up baseforms for vocabulary"
baseform_tool.pl -vocab $spoken_vocab -dict $dict -out $baseforms

echo "converting baseforms to FST: L"
fst_from_baseforms -c -i_ -o -p -P- $baseforms $domain.L.fst

if (-f $sweights ) then
    echo "  applying phone weights"
    fst_alphabet -i $domain.L.fst >! tmp.fst
    fst_weights.pl tmp.fst $sweights | fst_compose -t - $domain.L.fst $domain.L.fst
endif

if ( -f $reductions ) then
    echo "  composing: LoR -> L"
    fst_compose -t $domain.L.fst $domain.R.fst $domain.L.fst
endif

if ( -f $wweights ) then
    echo "  applying word weights"
    fst_weights.pl $vocab $wweights | fst_compose -t $domain.L.fst - $domain.L.fst
endif

# Composing CoPoLoG"  
fst_build_cplg $diphones $pron_rules $domain.L.fst $bigram_fst $ffst

# Remove temporary files
rm -f tmp.vocab
rm -f tmp.fst

