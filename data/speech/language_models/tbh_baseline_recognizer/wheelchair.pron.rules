
/* Define the input alphabet */
alphabet {
- _ aa ae ah ah_fp ao aw ax ay b bd ch d dd df dh dr eh el en er ey
f g gd hh ih iy jh k k- kd l m n ng nt ow oy p p- pd r s sh
t t- td tf th tq tr uh uw v w y z zh 
_oov_
_b1 _b2 _b3 _b4 
_c1 _c2 _c3 _c4 
_h1 _h2 _h3 
_l1 _l2 _l3 _l4 
_n1 _n2 _n3 _n4 _n5 _n6
}; 

/* Define the initial and final phones */
/* initial {-}; */
/* final {-}; */

/* Define special rule connection symbols. */
/* connect axr$ $axr; */

/* Define the symbols not taking part in rules. */
ignore {#};

/* Define some broad phonetic classes */
DSTOP = {bd dd gd pd td kd tq};
STOP = {b d g p t k p- t- k- tr dr tf df ch jh};
VOWEL = {aa ae ah ah_fp ao aw ax ay eh el er ey ih iy ow oy uh uw}; 
VOWEL_NO_R = {aa ae ah ah_fp ao aw ax ay eh el ey ih iy ow oy uh uw}; 
VOWEL_NO_Y = {aa ae ah ah_fp ao aw ax eh el er ih ow uh uw}; 
VOWEL_NO_W = {aa ae ah ah_fp ao ax ay eh el er ey ih iy oy uh}; 
SEMIVOWEL = {l y w r};
NASAL = {n en m ng};
FRIC = {f th s sh v dh z zh};
ALVEOLAR = {en n t t- td tf tr d dd df dr s z th dh}; // Alveolar & Dental sounds
PALATAL = {sh ch zh jh};// Palatal sounds 
AFFRIC = {ch jh};

/**************************************************************/

/*** Rules for /_/ ***/
{} _ {} => _ ;

/*** Rules for /-/ ***/
{} - {} => - | _ ;

/*** Rules for /aa/ ***/
{} aa {} => aa ;

/*** Rules for /ae/ ***/
{} ae {} => ae ;

/*** Rules for /ah/ ***/
{} ah {} => ah ;

/*** Rules for /ah_fp/ ***/
{} ah_fp {} => ah_fp ;

/*** Rules for /ao/ ***/
{} ao {} => ao ;

/*** Rules for /aw/ ***/
{} aw {} => aw ;

/*** Rules for /ax/ ***/
{f v} ax {n} => ax | en ;
{s z sh zh th dh} ax {n} => ax | [epi] en ;
{r} ax {n} => ax | axr ;
{} ax {n} => ax | <{tcl dcl pcl kcl bcl gcl} en ;
{f v} ax {m} => ax | em ;
{s z sh zh th dh} ax {m} => ax | [epi] em ;
{r} ax {m} => ax | axr ;
{} ax {m} => ax | <{pcl kcl bcl gcl tcl dcl} em ;
{r} ax {r} => ax | axr ;
{r} ax {l} => ax | axr | el ;
{} ax {l} => ax | el ;
{} ax {r} => ax | axr ;
{r} ax {} => ax | axr ;
{} ax {} => ax ;

/*** Rules for /ay/ ***/
{} ay {} => ay ;

/*** Rules for /b/ ***/
{VOWEL SEMIVOWEL} b {VOWEL} => bcl [b] ;
{} b {} => bcl b ;

/*** Rules for /bd/ ***/
{m} bd {} => [bcl] [b] ;
{} bd {} => bcl [b] ;

/*** Rules for /ch/ ***/
{} ch {} => tcl ch ;

/*** Rules for /d/ ***/
{n} d {} => [dcl] d;
{} d {} => dcl d;

/*** Rules for /dd/ ***/
{VOWEL SEMIVOWEL} dd {VOWEL SEMIVOWEL hh} => dcl [d] | dx ; 
{VOWEL} dd {en} => dx {ax}> | dcl ( d {ax}> | {en}> ) ;
{SEMIVOWEL} dd {en} => dx {ax}> | dcl ( d {ax}> | {en}> ) ;
{} dd {en} => dcl ( d {ax}> | {en}> ) ;
{en n} dd {} => [dcl] [d] ;
{} dd {} => dcl [d] ;

/*** Rules for /df/ ***/
{VOWEL SEMIVOWEL} df {VOWEL SEMIVOWEL hh} => dcl d | dx;
{} df {} => dcl d ;

/*** Rules for /dh/ ***/
{DSTOP FRIC AFFRIC NASAL} dh {} => [dcl] dh ;
{} dh {} => dh ;

/*** Rules for /dr/ ***/
{n} dr {} => [dcl] d ;
{} dr {} => dcl d ;

/*** Rules for /eh/ ***/
{} eh {} => eh ;

/*** Rules for /el/ ***/
{} el {} => el ;

/*** Rules for /en/ ***/
{tq td dd} en {} => <{tcl dcl} en | <{t d} ax n ;
{} en {} => en | ax n ;

/*** Rules for /er/ ***/
{} er {} => er ;

/*** Rules for /ey/ ***/
{} ey {} => ey ;

/*** Rules for /f/ ***/
{} f {} => f ;

/*** Rules for /g/ ***/
{ng} g {} => [gcl] g ;
{} g {} => gcl g ;

/*** Rules for /gd/ ***/
{ng} gd {} => [gcl] [g] ;
{} gd {} => gcl [g] ;

/*** Rules for /hh/ ***/
{} hh {} => hh ;

/*** Rules for /ih/ ***/
{} ih {} => ih ;

/*** Rules for /iy/ ***/
{} iy {} => iy ;

/*** Rules for /jh/ ***/
{n} jh {} => [dcl] jh ;
{} jh {} => dcl jh ;

/*** Rules for /k/ ***/
{} k {} => kcl k ;

/*** Rules for /k-/ ***/
{} k- {} => kcl k ;

/*** Rules for /kd/ ***/
{} kd {} => kcl [ k ] ;

/*** Rules for /l/ ***/
{ax} l {} => <{el} | <{ax axr} l ;
{} l {} => l ;

/*** Rules for /m/ ***/
{ax} m {} => <{em} | <{ax axr} m ;
{} m {} => m ;

/*** Rules for /n/ ***/
{ax} n {} => <{en} | <{ax axr} n ;
{} n {} => n ;

/*** Rules for /ng/ ***/
{} ng {} => ng ;

/*** Rules for /nt/ ***/
{} nt {} => n [tcl t] ;

/*** Rules for /ow/ ***/
{} ow {} => ow ;

/*** Rules for /oy/ ***/
{} oy {} => oy ;

/*** Rules for /p/ ***/
{} p {} => pcl p ;

/*** Rules for /p-/ ***/
{} p- {} => pcl p ;

/*** Rules for /pd/ ***/
{} pd {} => pcl [ p ] ;

/*** Rules for /r/ ***/
{ax} r {ax} => <{ax} r {ax el}> | <{axr} | {axr}> ;
{th} r {ax} => ( axr | [ax] r ) {ax el}> | {axr}> ;
{th} r {} => axr | [ax] r ;
{} r {ax} => r {ax el}> | {axr}> ;
{ax} r {} => <{ax} r | <{axr} ;
{} r {} => r ;

/*** Rules for /s/ ***/
{en n} s {en n m ng w r l el} => [epi | tcl [t]] s [epi] ;
{en n} s {} => [epi | tcl [t]] s ;
{m ng l el} s {en n m ng w r l el} => [epi] s [epi] ;
{m ng l el} s {} => [epi] s ;
{} s {en n m ng w y r l el} => s [epi] ;
{} s {} => s ;

/*** Rules for /sh/ ***/
{en n m ng l el} sh {en n m ng w r l el} => [epi] sh [epi] ;
{en n m ng l el} sh {} => [epi] sh ;
{} sh {en n m ng w r l el} =>  sh [epi] ;
{} sh {} => sh ;

/*** Rules for /t/ ***/
{s} t {ax} => [tcl t] ;
{} t {} => tcl t ;

/*** Rules for /t-/ ***/
{s} t- {ax} => [tcl t] ;
{} t- {} => tcl t ;

/*** Rules for /td/ ***/
{VOWEL SEMIVOWEL} td {VOWEL SEMIVOWEL hh} => tcl [t] | dx ;
{n} td {ax} => [ tcl [t] ] {ax ix}> | tcl {en}> ;
{f s} td {en} => ( tcl {en}> | [ tcl [t] ] {ax}> ) ;
{} td {en} => tcl ( {en}> | [t] {ax}> ) ;
{f s} td {} => [ tcl [t] ] ;
{} td {} => tcl [t] ;

/*** Rules for /tf/ ***/
{VOWEL SEMIVOWEL} tf {VOWEL SEMIVOWEL hh} => tcl t | dx ;
{} tf {} => tcl t ;

/*** Rules for /th/ ***/
{DSTOP FRIC AFFRIC NASAL} th {} => [tcl] th;
{} th {} => th ;

/*** Rules for /tq/ ***/
{} tq {en} => tcl {en}> | tcl t {ax}> ;
{} tq {} => tcl ;

/*** Rules for /tr/ ***/
{} tr {} => tcl t ;

/*** Rules for /uh/ ***/
{} uh {} => uh ;

/*** Rules for /uw/ ***/
{} uw {} => uw ;

/*** Rules for /v/ ***/
{} v {} => v ;

/*** Rules for /w/ ***/
{} w {} => w ;

/*** Rules for /y/ ***/
{} y {} => y ;

/*** Rules for /z/ ***/
{en n m ng l el} z {en n m ng w r l el} => [epi] z [epi] ;
{en n m ng l el} z {} => [epi] z ;
{} z {en n m ng w r l el} => z [epi] ;
{} z {} => z  ;

/*** Rules for /zh/ ***/
{en n m ng l el} zh {en n m ng w r l el} => [epi] zh [epi] ;
{en n m ng l el} zh {} => [epi] zh ;
{} zh {en n m ng w r l el} => zh [epi] ;
{} zh {} => zh ;

/*** Rules for artifacts and OOV ***/
{} _b1 {} => _b1 ;
{} _b2 {} => _b2 ;
{} _b3 {} => _b3 ;
{} _b4 {} => _b4 ;
{} _c1 {} => _c1 ;
{} _c2 {} => _c2 ;
{} _c3 {} => _c3 ;
{} _c4 {} => _c4 ;
{} _h1 {} => _h1 ;
{} _h2 {} => _h2 ;
{} _h3 {} => _h3 ;
{} _l1 {} => _l1 ;
{} _l2 {} => _l2 ;
{} _l3 {} => _l3 ;
{} _l4 {} => _l4 ;
{} _n1 {} => _n1 ;
{} _n2 {} => _n2 ;
{} _n3 {} => _n3 ;
{} _n4 {} => _n4 ;
{} _n5 {} => _n5 ;
{} _n6 {} => _n6 ;
{} _oov_ {} => _oov_ ;

