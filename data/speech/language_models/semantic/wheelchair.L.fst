#FSTBasic MinPlus
I 0
F 0
T 0 1 -
T 0 2 ax a
T 0 2 ey a
T 0 3 ae amphitheater
T 0 4 ax around
T 0 5 ae at
T 0 6 b behind
T 0 7 k cafeteria
T 0 8 k computer
T 0 9 k corner
T 0 10 k courtyard
T 0 11 s csail
T 0 12 d daniella_s
T 0 13 d dining_room
T 0 14 d done
T 0 15 d down
T 0 5 ey eight
T 0 16 ey eighth
T 0 17 eh elevator
T 0 18 eh elevator_lobby
T 0 19 eh end
T 0 20 eh entering
T 0 21 eh entrance
T 0 22 eh envoy
T 0 22 aa envoy
T 0 23 eh exit
T 0 24 f first
T 0 25 f five
T 0 26 f floor
T 0 27 f follow
T 0 28 f following
T 0 29 f four
T 0 30 f fourth
T 0 31 g glass
T 0 32 g go
T 0 33 g going
T 0 34 g good_afternoon
T 0 35 g good_evening
T 0 36 g good_morning
T 0 37 g go_to_sleep
T 0 4 g ground
T 0 38 jh gym
T 0 39 hh hallway
T 0 40 hh headquarters
T 0 2 ay i
T 0 41 ay i_am
T 0 42 ih in
T 0 42 ax in
T 0 43 ih in_front
T 0 43 ax in_front
T 0 44 ax is
T 0 44 ih is
T 0 45 k kitchen
T 0 46 l lab
T 0 47 l left
T 0 48 l let
T 0 49 l lets
T 0 50 l let's
T 0 51 l like
T 0 52 l living_room
T 0 53 l lobby
T 0 54 l lounge
T 0 55 m mark
T 0 56 m matt
T 0 57 m me
T 0 58 m meeting
T 0 59 m my
T 0 60 n name
T 0 61 n near
T 0 32 n no
T 0 62 n not
T 0 63 ax of
T 0 63 ah of
T 0 64 ao office
T 0 42 aa on
T 0 42 ah on
T 0 14 w one
T 0 65 p please
T 0 66 p p_r_two
T 0 67 p pub
T 0 68 k question
T 0 69 k question_mark
T 0 70 aa r_and_d
T 0 71 aa r_and_d_pub
T 0 72 r right
T 0 73 r room
T 0 74 r ross
T 0 75 s sachi
T 0 76 s second
T 0 77 s seth
T 0 78 s seth's
T 0 32 sh show
T 0 79 s standing
T 0 80 s start_listening
T 0 81 s stop
T 0 82 s stop_listening
T 0 83 t take
T 0 84 t ted's
T 0 85 th thank
T 0 86 th thanks
T 0 87 dh the
T 0 88 th third
T 0 89 th three
T 0 90 th through
T 0 91 t tig
T 0 92 tf to
T 0 29 t tour
T 0 93 t two
T 0 94 v victor's
T 0 95 v voice_off
T 0 96 v voice_on
T 0 97 w wake_up
T 0 57 w we
T 0 98 w we_are
T 0 99 w we're
T 0 100 w william
T 0 101 w with
T 0 102 w would
T 0 103 y yes
T 0 92 y you
T 0 104 y you_are
T 0 105 g going
T 0 106 w we
T 0 107 w would
T 0 108 f four
T 0 109 l let
T 0 110 g going
T 0 111 y you
T 0 112 t two
T 0 113 ay i
T 0 114 w one
T 0 115 th three
T 0 116 f five
T 0 117 ih is
T 0 117 ax is
T 0 118 ey eight
F 1
T 1 119 #
T 1 1 -
F 2
T 2 120 #
T 2 121 _
T 3 122 m
T 4 123 r
T 5 121 td
T 6 124 ax
T 7 125 ae
T 8 126 ax
T 9 127 ao
T 10 128 ao
T 11 129 iy
T 12 130 ae
T 13 131 ay
T 14 132 ah
T 15 132 aw
T 16 121 th
T 16 133 td
T 17 134 l
T 18 135 l
T 19 136 n
T 20 137 nt
T 21 138 n
T 22 139 n
T 23 140 gd
T 23 141 kd
T 24 142 er
T 25 143 ay
T 26 144 l
T 27 145 aa
T 28 146 aa
T 29 147 ao
T 30 148 ao
T 31 149 l
T 32 121 ow
T 33 150 ow
T 34 151 uh
T 35 152 uh
T 36 153 uh
T 37 154 ow
T 38 155 ih
T 39 156 ao
T 40 157 eh
T 41 158 _
T 41 155 ae
T 42 121 n
T 43 159 n
T 44 121 z
T 45 160 ih
T 46 161 ae
T 47 162 eh
T 48 163 eh
T 49 164 eh
T 50 165 eh
T 51 166 ay
T 52 167 ih
T 53 168 aa
T 54 169 aw
T 55 170 aa
T 56 163 ae
T 57 121 iy
T 58 171 iy
T 59 121 ay
T 60 155 ey
T 61 147 ih
T 62 163 aa
T 63 121 v
T 64 172 f
T 65 173 l
T 66 174 iy
T 67 161 ah
T 68 175 w
T 69 176 w
T 70 177 r
T 71 178 r
T 72 163 ay
T 73 155 uw
T 74 179 ao
T 75 180 ae
T 76 181 eh
T 77 133 eh
T 78 182 eh
T 79 183 t-
T 80 184 t-
T 81 185 t-
T 82 186 t-
T 83 166 ey
T 84 187 eh
T 85 188 ae
T 86 189 ae
T 87 121 ax
T 87 121 iy
T 87 121 ih
T 87 121 ah
T 88 136 er
T 89 190 r
T 90 191 r
T 91 192 ih
T 92 121 ax
T 92 121 uw
T 93 121 uw
T 94 193 ih
T 95 194 oy
T 96 195 oy
T 97 196 ey
T 98 197 iy
T 99 198 iy
T 99 147 ih
T 100 199 ih
T 101 200 ax
T 101 200 ih
T 102 136 ax
T 102 136 uh
T 103 179 eh
T 104 197 ax
T 104 197 uw
T 105 201 ow
T 106 202 iy would
T 107 203 uh not
T 108 204 ao is
T 109 205 eh me
T 110 206 ow to
T 110 207 uh to
T 111 202 ax would
T 111 202 uw would
T 112 44 uw is
T 113 2 dd would
T 114 208 ah is
T 115 209 r is
T 116 210 ay is
T 117 211 z not
T 118 212 td is
T 119 2 ax a
T 119 2 ey a
T 119 43 ih in_front
T 119 43 ax in_front
T 119 117 ih is
T 119 117 ax is
T 119 16 ey eighth
T 119 3 ae amphitheater
T 119 56 m matt
T 119 58 m meeting
T 119 59 m my
T 119 24 f first
T 119 25 f five
T 119 116 f five
T 119 26 f floor
T 119 27 f follow
T 119 28 f following
T 119 108 f four
T 119 30 f fourth
T 119 85 th thank
T 119 86 th thanks
T 119 88 th third
T 119 89 th three
T 119 115 th three
T 119 90 th through
T 119 72 r right
T 119 74 r ross
T 119 60 n name
T 119 61 n near
T 119 62 n not
T 119 6 b behind
T 119 39 hh hallway
T 119 40 hh headquarters
T 119 2 ay i
T 119 41 ay i_am
T 119 7 k cafeteria
T 119 8 k computer
T 119 9 k corner
T 119 10 k courtyard
T 119 45 k kitchen
T 119 68 k question
T 119 69 k question_mark
T 119 83 t take
T 119 84 t ted's
T 119 91 t tig
T 119 112 t two
T 119 66 p p_r_two
T 119 65 p please
T 119 103 y yes
T 119 92 y you
T 119 111 y you
T 119 104 y you_are
T 119 64 ao office
T 119 22 aa envoy
T 119 22 eh envoy
T 119 70 aa r_and_d
T 119 71 aa r_and_d_pub
T 119 11 s csail
T 119 75 s sachi
T 119 76 s second
T 119 77 s seth
T 119 78 s seth's
T 119 79 s standing
T 119 80 s start_listening
T 119 81 s stop
T 119 82 s stop_listening
T 119 46 l lab
T 119 47 l left
T 119 109 l let
T 119 48 l let
T 119 50 l let's
T 119 49 l lets
T 119 51 l like
T 119 52 l living_room
T 119 54 l lounge
T 119 12 d daniella_s
T 119 13 d dining_room
T 119 15 d down
T 119 17 eh elevator
T 119 18 eh elevator_lobby
T 119 19 eh end
T 119 20 eh entering
T 119 21 eh entrance
T 119 23 eh exit
T 119 94 v victor's
T 119 95 v voice_off
T 119 96 v voice_on
T 119 31 g glass
T 119 37 g go_to_sleep
T 119 105 g going
T 119 110 g going
T 119 34 g good_afternoon
T 119 35 g good_evening
T 119 36 g good_morning
T 119 4 g ground
T 119 38 jh gym
T 119 14 w one
T 119 114 w one
T 119 97 w wake_up
T 119 106 w we
T 119 99 w we're
T 119 98 w we_are
T 119 100 w william
T 119 101 w with
T 119 102 w would
T 119 107 w would
T 119 87 dh the
T 119 4 ax around
T 119 42 ih in
T 119 42 ax in
T 119 44 ax is
T 119 44 ih is
T 119 63 ax of
T 119 63 ah of
T 119 5 ey eight
T 119 118 ey eight
T 119 5 ae at
T 119 55 m mark
T 119 57 m me
T 119 29 f four
T 119 92 tf to
T 119 73 r room
T 119 32 n no
T 119 113 ay i
T 119 29 t tour
T 119 93 t two
T 119 67 p pub
T 119 42 aa on
T 119 42 ah on
T 119 53 l lobby
T 119 14 d done
T 119 32 g go
T 119 33 g going
T 119 57 w we
T 119 32 sh show
T 120 213 -
T 120 2 ax a
T 120 2 ey a
T 120 43 ih in_front
T 120 43 ax in_front
T 120 117 ih is
T 120 117 ax is
T 120 16 ey eighth
T 120 3 ae amphitheater
T 120 56 m matt
T 120 58 m meeting
T 120 59 m my
T 120 24 f first
T 120 25 f five
T 120 116 f five
T 120 26 f floor
T 120 27 f follow
T 120 28 f following
T 120 108 f four
T 120 30 f fourth
T 120 85 th thank
T 120 86 th thanks
T 120 88 th third
T 120 89 th three
T 120 115 th three
T 120 90 th through
T 120 72 r right
T 120 74 r ross
T 120 60 n name
T 120 61 n near
T 120 62 n not
T 120 6 b behind
T 120 39 hh hallway
T 120 40 hh headquarters
T 120 2 ay i
T 120 41 ay i_am
T 120 7 k cafeteria
T 120 8 k computer
T 120 9 k corner
T 120 10 k courtyard
T 120 45 k kitchen
T 120 68 k question
T 120 69 k question_mark
T 120 83 t take
T 120 84 t ted's
T 120 91 t tig
T 120 112 t two
T 120 66 p p_r_two
T 120 65 p please
T 120 103 y yes
T 120 92 y you
T 120 111 y you
T 120 104 y you_are
T 120 64 ao office
T 120 22 eh envoy
T 120 22 aa envoy
T 120 70 aa r_and_d
T 120 71 aa r_and_d_pub
T 120 11 s csail
T 120 75 s sachi
T 120 76 s second
T 120 77 s seth
T 120 78 s seth's
T 120 79 s standing
T 120 80 s start_listening
T 120 81 s stop
T 120 82 s stop_listening
T 120 46 l lab
T 120 47 l left
T 120 109 l let
T 120 48 l let
T 120 50 l let's
T 120 49 l lets
T 120 51 l like
T 120 52 l living_room
T 120 54 l lounge
T 120 12 d daniella_s
T 120 13 d dining_room
T 120 15 d down
T 120 17 eh elevator
T 120 18 eh elevator_lobby
T 120 19 eh end
T 120 20 eh entering
T 120 21 eh entrance
T 120 23 eh exit
T 120 94 v victor's
T 120 95 v voice_off
T 120 96 v voice_on
T 120 31 g glass
T 120 37 g go_to_sleep
T 120 105 g going
T 120 110 g going
T 120 34 g good_afternoon
T 120 35 g good_evening
T 120 36 g good_morning
T 120 4 g ground
T 120 38 jh gym
T 120 14 w one
T 120 114 w one
T 120 97 w wake_up
T 120 106 w we
T 120 99 w we're
T 120 98 w we_are
T 120 100 w william
T 120 101 w with
T 120 102 w would
T 120 107 w would
T 120 87 dh the
T 120 4 ax around
T 120 42 ih in
T 120 42 ax in
T 120 44 ax is
T 120 44 ih is
T 120 63 ax of
T 120 63 ah of
T 120 5 ey eight
T 120 118 ey eight
T 120 5 ae at
T 120 55 m mark
T 120 57 m me
T 120 29 f four
T 120 92 tf to
T 120 73 r room
T 120 32 n no
T 120 113 ay i
T 120 29 t tour
T 120 93 t two
T 120 67 p pub
T 120 42 aa on
T 120 42 ah on
T 120 53 l lobby
T 120 14 d done
T 120 32 g go
T 120 33 g going
T 120 57 w we
T 120 32 sh show
F 121
T 121 120 #
T 121 121 _
T 122 214 f
T 123 215 aw
T 124 216 hh
T 125 217 f
T 126 218 m
T 127 219 r
T 128 220 r
T 129 221 s
T 130 222 n
T 131 223 n
T 132 121 n
T 133 121 th
T 134 224 ax
T 135 225 ax
T 136 121 dd
T 137 150 er
T 138 226 tr
T 139 227 v
T 140 228 z
T 141 228 s
T 142 163 s
T 143 121 v
T 144 147 ao
T 145 229 l
T 146 230 l
T 147 121 r
T 148 133 r
T 149 179 ae
T 150 231 ih
T 151 232 dd
T 152 233 dd
T 153 234 dd
T 154 154 _
T 154 235 tf
T 155 121 m
T 156 236 l
T 157 237 dd
T 158 158 _
T 158 155 ae
T 159 159 _
T 159 238 f
T 160 201 ch
T 161 121 bd
T 162 163 f
T 163 121 td
T 164 179 td
T 165 121 z
T 165 179 td
T 166 121 kd
T 167 223 v
T 168 190 b
T 169 239 n
T 170 166 r
T 171 150 tf
T 172 179 ax
T 173 240 iy
T 174 174 _
T 174 241 aa
T 175 242 eh
T 176 243 eh
T 177 177 _
T 177 244 ax
T 177 244 ae
T 178 178 _
T 178 245 ax
T 178 245 ae
T 179 121 s
T 180 190 ch
T 181 246 k
T 182 179 th
T 183 247 ae
T 184 248 aa
T 185 249 aa
T 186 250 aa
T 187 240 dd
T 188 166 ng
T 189 251 ng
T 190 121 iy
T 191 121 uw
T 192 121 gd
T 193 252 kd
T 194 253 s
T 195 254 s
T 196 255 kd
T 197 197 _
T 197 147 aa
T 198 121 er
T 199 256 l
T 200 121 th
T 200 121 dh
T 201 132 ax
T 202 121 dd
T 203 257 dd
T 204 240 r
T 205 190 m
T 206 258 ax
T 206 259 n
T 207 259 n
T 208 240 n
T 209 240 iy
T 210 240 v
T 211 260 ax
T 212 121 s
F 213
T 213 213 -
T 214 261 ax
T 215 136 n
T 216 215 ay
T 217 262 ax
T 218 263 p
T 219 198 n
T 220 264 td
T 221 265 ey
T 222 266 y
T 223 267 ih
T 224 268 v
T 225 269 v
T 226 270 r
T 227 121 oy
T 228 163 ax
T 229 121 ow
T 230 150 ow
T 231 121 ng
T 232 232 _
T 232 271 ae
T 233 233 _
T 233 272 iy
T 234 234 _
T 234 273 m
T 235 274 ax
T 235 274 uw
T 236 275 w
T 237 276 k
T 238 277 r
T 239 121 jh
T 240 121 z
T 241 278 r
T 242 160 s
T 242 279 sh
T 243 280 s
T 243 281 sh
T 244 282 n
T 245 283 n
T 246 215 ax
T 247 284 n
T 248 285 r
T 249 121 pd
T 250 286 pd
T 251 179 kd
T 252 287 t
T 253 253 _
T 253 288 ao
T 254 254 _
T 254 132 aa
T 254 132 ah
T 255 255 _
T 255 249 ah
T 256 289 y
T 257 163 en
T 258 259 n
F 259
T 259 120 #
T 259 121 _
T 259 121 ax
T 260 163 n
T 261 290 th
T 262 291 t
T 263 292 y
T 264 293 y
T 265 121 l
T 266 294 eh
T 267 295 ng
T 268 296 ey
T 269 297 ey
T 270 298 ax
T 270 298 ae
T 271 299 f
T 272 300 v
T 273 301 ao
T 274 274 _
T 274 302 s
T 275 121 ey
T 276 303 ao
T 276 304 w
T 277 260 ah
T 278 278 _
T 278 191 t
T 279 201 ch
T 279 132 ax
T 280 305 ch
T 281 306 ax
T 281 305 ch
T 282 307 dd
T 283 308 dd
T 284 150 d
T 285 286 td
T 286 286 _
T 286 309 l
T 287 240 er
T 288 121 f
T 289 155 ax
T 290 310 iy
T 291 311 ih
T 292 296 uw
T 293 312 aa
T 294 313 l
T 295 295 _
T 295 314 r
T 296 198 tf
T 297 315 tf
T 298 179 n
T 299 316 t
T 300 317 ax
T 300 150 n
T 301 317 r
T 302 318 l
T 303 319 r
T 304 303 ao
T 305 306 ax
T 306 320 n
T 307 307 _
T 307 190 d
T 308 308 _
T 308 321 d
T 309 322 ih
T 310 296 ax
T 311 323 r
T 312 136 r
T 313 324 ax
T 314 155 uw
T 315 325 er
T 316 326 er
T 317 150 n
T 318 249 iy
T 319 287 tf
T 320 320 _
T 320 327 m
T 321 328 iy
T 322 300 s
T 323 329 iy
T 324 324 _
T 324 179 eh
T 325 325 _
T 325 330 l
T 326 331 n
T 327 170 aa
T 328 328 _
T 328 332 p
T 329 121 ax
T 330 168 aa
T 331 132 uw
T 332 161 ah
