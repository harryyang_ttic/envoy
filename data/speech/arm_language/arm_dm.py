#!/usr/bin/python
#from festival import Festival
import sys, os
import pygtk, gtk, gobject
pygtk.require('2.0')
if gtk.pygtk_version < (2,3,90):
   print "PyGtk 2.3.90 or later required for this example"
   raise SystemExit

import pygst
pygst.require("0.10")
import gst
import string
import platform
from time import *
import pango
import cPickle
import math
from sys import argv
import lcm
from arm_cmd.object_manipulation_cmd_t import object_manipulation_cmd_t

sys.path.append( '../../build/lib/python2.6/site-packages/' )

import threading, thread

class GTK_Main:
	
	def __init__(self, speaker, mode, current_floor):

                #----------Speech Synthesizer-----------------------
		self.speaker = speaker

                self.response_path = "./responses/"
                self.response_map = {'greet_with_name' : 'Hello_my_name_is_envoy.wav', 
                                     'ask_pickup' : 'Ask_pickup_can.wav', 
                                     'say_failed_to_see' : 'Say_failed_to_spot.wav', 
                                     'say_putdown': 'Say_putdown.wav', 
                                     'say_failed': 'Say_failed.wav', 
                                     'ask_putdown': 'Ask_putdown_can.wav', 
                                     'say_pickup' : 'Say_pickup.wav'}

                self.lcm = lcm.LCM()

                self.keywords = ['CMD','LOC','DIR','KEYWORD', 'PRON' , 'PROP', 'LABEL','VIEW','POS','PREFIX','SUFFIX']  

                #----------System info
		release = platform.release()

                #-----------Speech Processing-----------------------

                #----Keywords
                
		#add keywords that you want extracted - for keyword extraction
		self.keywords = ['KEYWORD','ACTION','GREETING','CONFIRMATION']

                #----------Speech Acquisition #Depricated in the current implementation 
		self.device = 'usb'  #options = bluetooth, usb, default

                self.player_made = 0
                self.gst_output_pipe = None
                self.speaking = False

                #-----Speech Recognizer Parameters 
		#Floor Files to load - changes which vocab is loaded 
		self.current_floor = 'stata_floor_3' #'tbh' #'stata_floor_8' 

		#----------Speech Detector Parameters
		self.threshold = -25.0 #-55
		self.minspeech = 100000
		self.prespeech = 500000 
                self.holdover = 500000

		#-----------------GUI Parameters----------------------------------

		#Main window 
		self.width = 700
		self.height = 500

		window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		window.set_title("Wheelchair DM")
		window.set_default_size(self.width, self.height) 
		window.connect("destroy", self.destroy, "WM destroy")

		#Notebook - for tabs		
		self.notebook = gtk.Notebook()
		self.notebook.set_tab_pos(gtk.POS_TOP)
		self.notebook.show()
		self.show_tabs = True
		self.show_border = True

		window.add(self.notebook)

		#####################################################################
		#Main tab
		
		results_vbox =  gtk.VBox()		
		results_label = gtk.Label("Dialog Manager")
		
		#add dialog manager tab 
		self.notebook.append_page(results_vbox, results_label)

		#Main Section 
		
		main_hbox = gtk.HBox()
		results_vbox.pack_start(main_hbox)

		#Speech Command buttons
		
		cmd_frame = gtk.Frame("Dialog Manager")
		cmd_vbox =  gtk.VBox()
		cmd_frame.add(cmd_vbox)

		main_hbox.pack_start(cmd_frame)                

		#Current Status Button - Listening/Sleep/Not Listening
		self.buttonLabelSize = pango.FontDescription('Sans 20')
		self.btnStartStop = gtk.Button()
		self.start_label = gtk.Label()
		self.start_label.set_markup('Not Listening')
		self.start_label.modify_font(self.buttonLabelSize)
		self.btnStartStop.add(self.start_label)
		self.btnStartStop.connect("clicked", self.start_stop)
		self.btnStartStop.set_size_request(self.width/2,40)
		
                button_hbox = gtk.HBox()
                cmd_vbox.pack_start(button_hbox)
                dm_button_vbox = gtk.VBox()

                button_hbox.pack_start(dm_button_vbox)
		dm_button_vbox.pack_start(self.btnStartStop)
                
                #Mode Display - Displays the Current DM - Tourguide/Navigation
                buttonLabelSize = pango.FontDescription('Sans 24')
		self.btnModeDisp = gtk.Button()
		self.mode_label = gtk.Label()
		self.mode_label.modify_font(buttonLabelSize)
		self.btnModeDisp.add(self.mode_label)
		self.btnModeDisp.set_size_request(self.width/2,40)
		dm_button_vbox.pack_start(self.btnModeDisp)
                
		#Dialog Manager Status Button
                cur_result_hbox = gtk.HBox()
		cur_results_frame = gtk.Frame("Recognition List")
		cur_results_frame.add(cur_result_hbox)
                
                #Current Recognition result
                recog_results_frame = gtk.Frame("Current Recog")
		self.recog_label = gtk.Label()
		self.recog_label.set_markup('None\n')
		self.recogLabelSize = pango.FontDescription('Sans 11')
		self.recog_label.set_line_wrap(1)
                self.recog_label.set_size_request(self.width/2,75)

		self.recog_label.modify_font(self.recogLabelSize)
		recog_results_frame.add(self.recog_label)

                #Full Recognition List
		scrollWin = gtk.ScrolledWindow()
		scrollWin.set_size_request(self.width/2,self.height/2)
		cur_result_hbox.pack_start(scrollWin)

		#Text Buffer for current recognition list
		self.result = gtk.TextBuffer()
		resultView = gtk.TextView(self.result)
		resultView.set_justification(gtk.JUSTIFY_LEFT)
		scrollWin.add(resultView)

                action_hbox=  gtk.HBox()
                #Current Action - i.e. speech output
                action_results_frame = gtk.Frame("Current Action")
		self.action_label = gtk.Label()
		self.action_label.set_markup('--No action--\n')
		self.actionLabelSize = pango.FontDescription('Sans 11')
		self.action_label.set_line_wrap(1)
                self.action_label.set_size_request(self.width/4,50)

		self.action_label.modify_font(self.actionLabelSize)
		action_results_frame.add(self.action_label)
                
                #Current State of the Dialog Manager 
                dialog_state_frame = gtk.Frame("Dialog State")
		self.dialog_state_label = gtk.Label()
                self.dialog_state_label.set_justify(gtk.JUSTIFY_CENTER)
		dialogLabelSize = pango.FontDescription('Sans 11')
		self.dialog_state_label.set_line_wrap(1)
                self.dialog_state_label.set_size_request(self.width/4,50)

		self.dialog_state_label.modify_font(dialogLabelSize)
		dialog_state_frame.add(self.dialog_state_label)

                #Navigation Information
                navigation_state_frame = gtk.Frame("Navigation State")
		self.navigation_state_label = gtk.Label()
		self.navigation_state_label.set_markup("No Goal")
		navigationLabelSize = pango.FontDescription('Sans 11')
		self.navigation_state_label.set_line_wrap(1)
                self.navigation_state_label.set_size_request(self.width/2,75)

		self.navigation_state_label.modify_font(navigationLabelSize)
                self.navigation_state_label.set_justify(gtk.JUSTIFY_LEFT)
		navigation_state_frame.add(self.navigation_state_label)
                
                action_hbox.pack_start(dialog_state_frame)
                action_hbox.pack_start(action_results_frame)
                
                self.say_response('greet_with_name')

                #DM Information 
                cmd_vbox.pack_start(action_hbox)
                cmd_vbox.pack_start(navigation_state_frame)
                cmd_vbox.pack_start(recog_results_frame)                
                cmd_vbox.pack_start(cur_results_frame)

		window.show_all()

                self.state = 'idle'
                
		self.get_colors(window)
		self.set_color('offline')
                print "Changing DM Mode"
                
                self.start_listening()
                
		return

        #=================================================================================#
        #======================Helper Functions===========================================#

        #--------GUI Element Handlers------------
        def get_colors(self,window):		
		self.gc = window.get_style().fg_gc[gtk.STATE_NORMAL]
		self.colormap = self.gc.get_colormap()
		self.white = self.colormap.alloc_color('white')
		self.black = self.colormap.alloc_color('black')
		self.magenta = self.colormap.alloc_color('magenta')
		self.green = self.colormap.alloc_color('green')
		self.gray = self.colormap.alloc_color('gray')
		self.red = self.colormap.alloc_color('red')
		self.lightgray = self.colormap.alloc_color(60000,60000,60000)
		self.pink = self.colormap.alloc_color('pink')
		self.blue = self.colormap.alloc_color('blue')
		self.orange = self.colormap.alloc_color('orange')
		self.yellow = self.colormap.alloc_color('yellow') 

        def set_color(self, state):

		if state == 'offline':
			self.btnStartStop.modify_bg(gtk.STATE_NORMAL,self.white)
			self.btnStartStop.modify_bg(gtk.STATE_PRELIGHT,self.white)
			self.btnStartStop.modify_bg(gtk.STATE_ACTIVE,self.white)
			self.start_label.set_text("Not Listening")

                elif state == 'sleep':
			self.btnStartStop.modify_bg(gtk.STATE_NORMAL,self.gray)
			self.btnStartStop.modify_bg(gtk.STATE_PRELIGHT,self.gray)
			self.btnStartStop.modify_bg(gtk.STATE_ACTIVE,self.gray)
			self.start_label.set_text("Sleeping")

		elif state == 'listening':

			self.btnStartStop.modify_bg(gtk.STATE_NORMAL,self.green)
			self.btnStartStop.modify_bg(gtk.STATE_PRELIGHT,self.green)
			self.btnStartStop.modify_bg(gtk.STATE_ACTIVE,self.green)
			self.start_label.set_text("Listening")			

		elif state == 'heard':
			
			self.btnStartStop.modify_bg(gtk.STATE_NORMAL,self.orange)
			self.btnStartStop.modify_bg(gtk.STATE_PRELIGHT,self.orange)
			self.btnStartStop.modify_bg(gtk.STATE_ACTIVE,self.orange)
			self.start_label.set_text("Heard")

                elif state == 'processing':
			
			self.btnStartStop.modify_bg(gtk.STATE_NORMAL,self.red)
			self.btnStartStop.modify_bg(gtk.STATE_PRELIGHT,self.red)
			self.btnStartStop.modify_bg(gtk.STATE_ACTIVE,self.red)
			self.start_label.set_text("Processing")

		print "Changed Color " 

        def send_lcm_command(self, command):
           cmd_msg = object_manipulation_cmd_t()
           cmd_msg.utime =  int(time() * 1000000)
           cmd_msg.requester = cmd_msg.SPEECH
           if(command == 'putdown'):
              cmd_msg.type = cmd_msg.TYPE_PUT_OBJECT_BACK
           elif(command == 'pickup'):
              cmd_msg.type = cmd_msg.TYPE_DETECT_OBJECT
           elif(command == 'go_home'):
              cmd_msg.type = cmd_msg.TYPE_SEND_ARM_HOME

           self.lcm.publish("OBJECT_MANIPULATION_REQUEST", cmd_msg.encode())

        def update_state(self, recog_text):
           #self.state
           print "Updating State"
           print "Heard :" , recog_text

           parse = self.parse_uttarance(recog_text)
           print parse 

           
           if(parse['action'] == 'pickup' and parse['keyword'] != 'none'):
              self.state = 'ask_pickup'
              self.say_response('ask_pickup')

           elif(parse['action'] == 'putdown' and parse['keyword'] != 'none'):
              self.state = 'ask_putdown'
              self.say_response('ask_putdown')

           elif(parse['action'] == 'go_home'):
              self.state = 'ask_gohome'
              self.say_response('ask_gohome')

           elif(parse['confirmation'] == 'yes'):
              print "Confirmed - Sending LCM message"
              if(self.state == 'ask_pickup'):
                 print "Picking up"
                 self.say_response('say_pickup')
                 self.send_lcm_command('pickup')

              elif(self.state == 'ask_putdown'):
                 print "Putting down"
                 self.say_response('say_putdown')
                 self.send_lcm_command('putdown')

              self.state = 'idle'
           elif(parse['confirmation'] == 'no'):
              self.state = 'idle'
              print "Ignoring"

           elif(parse['greeting'] != 'none'):
              self.state = 'idle'
              self.say_response('greet_with_name')
              

        # Parse Uttarance
        def parse_uttarance(self, recognized):		
           #Should return the actual name as well 
           
           print "Parsing Uttarance New"
           output = {}
           print recognized
           for key in self.keywords:
              output[key.lower()] = (self.get_command(recognized,key)).lower()
           return output

        def say_response(self, response):
           if(self.response_map.has_key(response)):
              file_src =  self.response_path + self.response_map[response]
              gst_parse_str = "filesrc location= " + file_src + " ! wavparse ! alsasink"
              
              if(self.gst_output_pipe != None): 
                 print "(((((((Stopping Pipeline)))))))" 
                 self.gst_output_pipe.set_state( gst.STATE_NULL)
                 print "Stopped State : " , self.gst_output_pipe.get_state()
              
              self.gst_output_pipe = gst.parse_launch(gst_parse_str)

              pipeline_bus = self.gst_output_pipe.get_bus()
              
              self.speaking = True
              
              self.gst_output_pipe.set_state( gst.STATE_PLAYING )
              
              # start the pipeline
              pipeline_bus.add_signal_watch()
              pipeline_bus.connect( "message", self.process_message )

           else:
              print "Error : Response doesn't have audio output"

        def process_message( self, bus, message ):
           if(message.type == gst.MESSAGE_EOS):
              print "+++++++++++ Done speaking" 
              self.speaking = False

        #Start/Stop Button for Listener           
	def start_stop(self, w):
                if self.listening==0:
                        self.start_listening()
		else:
                        self.stop_listening()			

 	def destroy(self, widget, data=None):
                self.stop_listening()
		print "Exiting"
		print "Done"
		gtk.main_quit()


        #### Speech Command Parsing Helper Functions 

        # Parse command and get keywords
	def get_command(self, result, key):		
		found = result.find(key)		
		if (found >=0):
			start_loc = result.find("=",found)
			end_loc = result.find("]",start_loc)
			return result[start_loc+1:end_loc]
		else:
			return 'none'


        #====================================================================================================#
        #======================== Speech Listener Handlers ==================================================#

        #--------------GST Message Handling----------------#

	def on_message(self, bus, message):
           
           t = message.type

           if t == gst.MESSAGE_EOS:

                  if self.logging_mode==1:

                          if self.mode == 'recording':
                                  self.recorder.set_state(gst.STATE_NULL)					
                          else:
                                  self.player.set_state(gst.STATE_NULL)
                  else:
                          self.recorder.set_state(gst.STATE_NULL)

                  self.start_label.set_text("Not Listening")

                  print "End of Stream"
                  self.set_color('offline')


           elif t == gst.MESSAGE_ERROR:

                  if self.logging_mode==1:
                          if self.mode == 'recording':
                                  self.recorder.set_state(gst.STATE_NULL)
                          else:
                                  self.player.set_state(gst.STATE_NULL)
                  else:
                          self.recorder.set_state(gst.STATE_NULL)

                  self.start_label.set_text("Not Listening")
                  print "Error Discovered"
                  err, debug = message.parse_error()
                  print "Error: %s" % err, debug
                  self.set_color('offline')

           elif t == gst.MESSAGE_APPLICATION:

             #Dealing with the SAD and SUMMIT Messages
             print "Received Message " , message
             
                #Audio logging off
             if message.structure.has_field("items") and len( message.structure["items"])>0 and \
                       not ( message.structure.keys()[0] == 'start_timestamp'):  
                   #Valid recognition result
                   rec_time = time()
                   recognition = {'type': 'recognition', 'info': {'file_name':'none',\
                                                                     'results':message.structure['items']}, 'time': rec_time}
                   #Save to recognition tag file with time stamps
                   print message.structure['items']

                   results_text = ""
                   count = 0
                   for rec in message.structure['items']: 
                      count = count + 1
                      results_text = results_text + str(count)+ " : " + rec + "\n"

                   self.update_state(message.structure['items'][0])
                   self.result.set_text(results_text)						


           elif message.structure.keys()[0] == 'start_timestamp':
                   print "Heard"
                   if self.state == 'active':
                      self.set_color('heard')
                      self.result.set_text('-------------------')
                            
        #------------------ GST Functions --------------------------#
	def make_recorder(self):
		# Define the GStreamer Source Element -- will need to be alter to fit hardware
		print "Making Recorder"

                   
                self.recorder = gst.parse_launch('lcmsrc channel=MIC_AUDIO ! rtpL16depay '
                                                    + '! queue ! audioconvert'
                                                    + '! audioresample ! ' 
                                                    + 'audio/x-raw-int, rate = 8000, channels=1, width=16, depth=16 ! ' 
                                                    + ' SAD name =sad ! queue ! summitsink config=./rec/recognizer.cfg')

                print "Recorder " , self.recorder

		sad = self.recorder.get_by_name("sad") 

		sad.set_property("enable_noise_est", True)
		sad.set_property("threshold", self.threshold)
		sad.set_property("minspeech", self.minspeech)
		sad.set_property("prespeech",self.prespeech)
                sad.set_property("holdover",self.holdover)
                sad.set_property('framelength', 32000)
                print sad.get_property('framelength')
		bus = self.recorder.get_bus()
		bus.add_signal_watch()
		bus.connect("message", self.on_message)

	
        #Starts and stops listener
        def start_listening(self):
           #if self.listening ==0:
              self.listening = 1
              self.make_recorder()	
              self.state = 'active'
              self.start_label.set_text("Listening")				
              self.recorder.set_state(gst.STATE_PLAYING)
              print "Started Listening"
              self.set_color('listening')
              
        def stop_listening(self):
           if self.listening ==1:  # we are currently listening
              self.recorder.set_state(gst.STATE_NULL)
              self.listening =0

           print "Set to ready"
           self.set_color('offline')
           self.state = "inactive"
                 
#====================================================================================================#

if __name__ == "__main__":
   #speaker = Festival()
   
   print "------Tourguide-------"
   GTK_Main(None,"tourguide", None)
   #GTK_Main(speaker,"navigation",None)
   #GTK_Main(speaker,"navigation", 3)#"normally -"tourguide"
   gtk.gdk.threads_init()
   gtk.main()

