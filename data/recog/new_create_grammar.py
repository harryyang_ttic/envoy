from sys import argv
import os
import commands

def write_rules(grammar_file, recog_places, recog_objects=None):
    write_main(grammar_file)  #writes the main set of rules
    write_tagging(grammar_file)
    write_places(grammar_file, recog_places)
    write_objects(grammar_file, recog_objects)
    write_direction(grammar_file)
    write_mode_change(grammar_file)
    write_floor_change(grammar_file)
    write_confirm(grammar_file)
    write_wakeup(grammar_file)
    write_follower(grammar_file)
    write_noise(grammar_file)       
    
def write_main(grammar_file):
    grammar_file.write("\npublic <command> = ( <entering> \n")
    grammar_file.write("\t| <visible> )\n")
    #grammar_file.write("\t| <visible> \n")
    #grammar_file.write("\t| (<on_the_direction>))\n")
    grammar_file.write("\t| (<noise>)\n")
    grammar_file.write("\t| <follower>\n")
    grammar_file.write("\t| <confirm>\n")
    grammar_file.write("\t| <current_floor>\n")
    grammar_file.write("\t| <wakeup>\n")
    grammar_file.write("\t| <mode>\n")
    grammar_file.write(";\n")

def write_objects(grammar_file, recog_objects):
    grammar_file.write("\n<objects> = ")
    no_objects = len(recog_objects)
    if no_objects ==0:
        grammar_file.write("<NULL>\n")
    else:
        for k in range(no_objects):
            i = recog_objects[k]
            if(k==0):
                grammar_file.write("( ")
                write_item(grammar_file,  i)
            elif((k >0) and (k < no_objects-1)):
                grammar_file.write(" \n| ")
                write_item(grammar_file, i)
            elif(k == no_objects-1):
                grammar_file.write(" \n| ")
                write_item(grammar_file, i)
                grammar_file.write(")")
    grammar_file.write(";")

def write_places(grammar_file, recog_places):
    grammar_file.write("\n<places> = (")
    no_places = len(recog_places)
    for k in range(no_places):
        i = recog_places[k]
        if(k==0):
            write_item(grammar_file, i)
        if((k >0)):
            grammar_file.write(" \n| ")
            write_item(grammar_file, i)
    grammar_file.write(");")

def write_item(grammar_file, i):
    if "optional_prefix" in i:
        write_prefix(grammar_file, i["optional_prefix"])
    for j in range(len(i["recog"])):
        if len(i["recog"])==1:            
            grammar_file.write(i["recog"][0])         
            grammar_file.write(" {[PRON="+i["recog"][0]+"]}") 
        elif (j == 0):
            grammar_file.write("( " + i["recog"][j])
            grammar_file.write(" {[PRON="+i["recog"][j]+"]}") 
        elif( (j >0) and (j<len(i["recog"])-1)):
            grammar_file.write(" | " + i["recog"][j])
            grammar_file.write(" {[PRON="+i["recog"][j]+"]}") 
        else:
            grammar_file.write(" | " + i["recog"][j])
            grammar_file.write("{[PRON="+i["recog"][j]+"]} )") 
    if "optional_suffix" in i:
        write_suffix(grammar_file,i["optional_suffix"])
    grammar_file.write("{[LABEL="+i["tag"] + "]}")

def write_prefix(grammar_file, pre):
    if len(pre)==1:
        grammar_file.write("[" + pre[0] + "]")
    elif len(pre)>1:
        for j in range(len(pre)):
            if (j == 0):
                grammar_file.write("( [" + pre[j] + "]")
                grammar_file.write(" {[PREFIX="+pre[j]+"]}") 
            elif( (j >0) and (j < len(pre)-1)):
                grammar_file.write(" | [" + pre[j] + "]")
                grammar_file.write(" {[PREFIX="+pre[j]+"]}") 
            else:
                grammar_file.write(" | [" + pre[j] + "]")
                grammar_file.write(" {[PREFIX="+pre[j]+"]})") 

def write_suffix(grammar_file, suf):
    if len(suf)==1:
        grammar_file.write("[" + suf[0] + "]")
        grammar_file.write(" {[SUFFIX="+suf[0]+"]}") 
    elif len(suf)>1:
        for j in range(len(suf)):
            if (j == 0):
                grammar_file.write("( [" + suf[j] + "]")
                grammar_file.write(" {[SUFFIX="+suf[j]+"]}") 
            elif( (j >0) and (j < len(suf)-1)):
                grammar_file.write(" | [" + suf[j] + "]")
                grammar_file.write(" {[SUFFIX="+suf[j]+"]}") 
            else:
                grammar_file.write(" | [" + suf[j] + "] ")
                grammar_file.write(" {[SUFFIX="+suf[j]+"]})") 

def write_mode_change(grammar_file):
    grammar_file.write("\n<mode>  = [computer {[KEYWORD=computer]}] (we are done [touring | with the tour] | go to navigation [mode] | end of [the] tour ) {[CMD=change_mode]} {[PROP=navigation]};\n")


def write_floor_change(grammar_file):
    grammar_file.write("\n<floor_no>  =  (ground {[PROP=0]} {[PRON=GROUND]}| first {[PROP=1]} {[PRON=FIRST]}  | second {[PROP=2]} {[PRON=SECOND]}  | third {[PROP=3]} {[PRON=THIRD]} | fourth {[PROP=4]} {[PRON=FOURTH]}| fifth {[PROP=5]} {[PRON=FIFTH]} );\n")
    grammar_file.write("\n<floor_val>  =  (one {[PROP=1]} {[PRON=FIRST]} | two {[PROP=2]} {[PRON=SECOND]} | three {[PROP=3]} {[PRON=THIRD]} | four {[PROP=4]} {[PRON=FOURTH]} | five {[PROP=5]}{[PRON=FIFTH]} );\n")
    #grammar_file.write("\n<current_floor>  = i am on the first floor;\n")
    grammar_file.write("\n<current_floor>  = [computer {[KEYWORD=computer]}] ((i am |we are) on ([the <floor_no> floor | floor <floor_val> ] {[CMD=FLOOR_CHANGE]}));\n")

def write_confirm(grammar_file):
    grammar_file.write("\n<confirm> = ( (yes [i did] | yep ) {[PROP=YES]} | (no [i did] | nope) {[PROP=NO]} ) {[CMD=confirm]};\n")

def write_wakeup(grammar_file):
    grammar_file.write("\n<wakeup> = ([computer {[KEYWORD=computer]}](wake up | wakeup | start listening) {[CMD=ACTIVE]}{[PROP=YES]}) |\n")
    grammar_file.write("([computer {[KEYWORD=computer]}](stop listening | go to sleep){[CMD=ACTIVE]}{[PROP=NO]});\n")

def write_follower(grammar_file):
    grammar_file.write("\n<follower> = (start following me | follow me) {[PROP=start_following]}{[CMD=follower]}\n") 	    
    #grammar_file.write("| go through the door {[PROP=go_through_door]}{[CMD=follower]} \n")
    #grammar_file.write("| go [into | inside] the elevator {[PROP=go_into_elevator]}{[CMD=follower]} \n")
    grammar_file.write("| (stop following [me] | stop) {[PROP=idle]}{[CMD=follower]}\n")
    grammar_file.write("| i am ahead of you {[PROP=start_looking]}{[CMD=tracker]} \n")
    grammar_file.write("| i am in front of you {[PROP=start_looking]}{[CMD=tracker]} \n")
    grammar_file.write("| do you see me {[PROP=status]}{[CMD=tracker]} \n")
    grammar_file.write("| turn towards me {[PROP=turn]}{[CMD=follower]};\n")

def write_direction(grammar_file):
    grammar_file.write("\n<directions> = (right {[DIR=RIGHT]} | left {[DIR=LEFT]}) [hand side];\n")
def write_noise(grammar_file):
    grammar_file.write("\n<noise> = err;\n")

def write_tagging(grammar_file):    
    grammar_file.write("<entering> = (i am {[VIEW=GUIDE]}| we are {[VIEW=BOTH]} | you are {[VIEW=WHEELCHAIR]}) \n")
    grammar_file.write("\t[now] ((in | at |going through) {[POS=AT]} |facing {[POS=FACING]}| entering {[POS=ENTERING]}| approaching {[POS=APPROACHING]})\n")
    grammar_file.write("<places> {[CMD=place]} |\n")
    grammar_file.write("\t(i am {[VIEW=GUIDE]}| we are {[VIEW=BOTH]} | you are {[VIEW=WHEELCHAIR]}) \n")
    grammar_file.write("\t[now] ((at | near) {[POS=AT]}| facing {[POS=FACING]} | approaching  {[POS=APPROACHING]} ) <objects> {[CMD=object]};\n")     
    grammar_file.write("<on_the_direction> = (on | in) [(our {[VIEW=BOTH]}| the {[VIEW=NONE]} | your {[VIEW=WHEELCHAIR]} | my {[VIEW=GUIDE]})] \n")
    grammar_file.write("\t<directions> (is |are) (<places>{[CMD=place]} | <objects>{[CMD=object]});\n")
    grammar_file.write("<visible> = i can see (<places>{[CMD=place]} | <objects>{[CMD=object]}) \n")
    grammar_file.write("\t on [(our {[VIEW=BOTH]}| the {[VIEW=NONE]} | your {[VIEW=WHEELCHAIR]} | my {[VIEW=GUIDE]})][ <directions>];\n")
    

if __name__ == "__main__":
    if (len(argv) < 3):        
        print "No place file given"
    else:
        print "Place Name File : ", argv[2] 
        placefile = open(argv[2],'r')
        
        recog_places = []
        print "Loading Places from File : " , argv[2]
        
        while 1:            
            places = placefile.readline()
            if places=='':
                print "====== Done Loading Places ======"
                placefile.close()
                break
            else:
                temp_loc = eval(places)
                print temp_loc
                recog_places.append(temp_loc)

        recog_objects = []
                
        if (len(argv) == 4): #there is an object file as well
            

            print "Object Name File : ", argv[3] 
            objectfile = open(argv[3],'r')
            
            print "Loading Objects from File : " , argv[3]
            while 1:
                
                objects = objectfile.readline()
                if objects=='':
                    print "====== Done Loading Objects ======"
                    objectfile.close()
                    break
                else:
                    temp_obj = eval(objects)
                    print temp_obj
                    recog_objects.append(temp_obj)
        
        print "Writing grammar file"
        grammar_file = open(argv[1]+".jsgf",'w')

        #write the rules - at some point make this changable so we can pick and choose which rules to include (e.g. whether to add objects etc)

        grammar_file.write("#JSGF V1.0;\n")        
        grammar_file.write("\ngrammar " + argv[1] + ";\n")
        write_rules(grammar_file, recog_places, recog_objects)
        grammar_file.close()

        print 'Building Grammar'


        result = commands.getstatusoutput('build_rec '+ argv[1])

       
        print result[1]

