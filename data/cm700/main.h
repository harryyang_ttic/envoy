//arm_controller.h

#ifndef __ARM_CONTROLLER_H__
#define __ARM_CONTROLLER_H__

#define F_CPU 16000000UL

#define ALIVE_ON 		PORTC &= 	~(1 << 5)
#define ALIVE_OFF 		PORTC |= 	(1 << 5)
#define IS_ALIVE_ON 	((1 << 5) & ~(PORTC))

#define TX_ACTIVITY_ON	PORTC &= 	~(1 << 4)
#define TX_ACTIVITY_OFF	PORTC |=	(1 << 4)
#define IS_TX_ON		((1 << 4) & ~(PORTC))
#define TOGGLE_TX		if(IS_TX_ON) {TX_ACTIVITY_OFF;} else {TX_ACTIVITY_ON;}

#define RX_ACTIVITY_ON	PORTC &= 	~(1 << 3)
#define RX_ACTIVITY_OFF	PORTC |=	(1 << 3)
#define IS_RX_ON		((1 << 3) & ~(PORTC))
#define TOGGLE_RX		if(IS_RX_ON) {RX_ACTIVITY_OFF;} else {RX_ACTIVITY_ON;}


#define D3_ACTIVITY_ON	PORTC &= 	~(1 << 2)
#define D3_ACTIVITY_OFF	PORTC |=	(1 << 2)
#define IS_D3_ON		((1 << 2) & ~(PORTC))
#define TOGGLE_D3		if(IS_D3_ON) {D3_ACTIVITY_OFF;} else {D3_ACTIVITY_ON;}

#define D2_ACTIVITY_ON	PORTC &= 	~(1 << 1)
#define D2_ACTIVITY_OFF	PORTC |=	(1 << 1)
#define IS_D2_ON		((1 << 1) & ~(PORTC))
#define TOGGLE_D2		if(IS_D2_ON) {D2_ACTIVITY_OFF;} else {D2_ACTIVITY_ON;}

#define D1_ACTIVITY_ON	PORTC &= 	~(1 << 0)
#define D1_ACTIVITY_OFF	PORTC |=	(1 << 0)
#define IS_D1_ON		((1 << 0) & ~(PORTC))
#define TOGGLE_D1		if(IS_D1_ON) {D1_ACTIVITY_OFF;} else {D1_ACTIVITY_ON;}

#define ONE_SEC_TIMEOUT	50

#define DEFAULT_BAUDNUM	1	/*1Mbps*/

#define MAX_NUM_SERVOS	10	/*max number of servos chain will support*/
#define MAX_NUM_SENSORS	10	/*number of sensor to report to the host*/

#define SIZE_OF_FRAME	15  /* ID = 2, TE = 1, GP = 4, SP = 4, TQ = 4 */

#define SERVO_COMMAND			"SC"
#define SERVO_DEBUG				"SD"
#define SERVO_COMMAND_FAIL		"SRF"
#define SERVO_COMMAND_SUCCESS	"SRS"
#define SERVO_STATUS			"SS"

#define SERVO_COMMAND_DATA 		'D'

#define SERVO_ADD_SENSOR 		'+'
#define SERVO_REMOVE_SENSOR 	'-'
#define SERVO_ADD_SERVO			'X'
#define SERVO_REMOVE_SERVO		'Z'
#define SERVO_QUERY_SERVO		'Y'
#define SERVO_QUERY_COUNT 		'C'
#define SERVO_QUERY_SENSOR 		'S'
#define SERVO_CLEAR_SENSORS 	'K'
#define SERVO_START				'['
#define SERVO_STOP				']'
#define SERVO_HELP				'?'


#include "dynamixel_defines.h"



/*
 *	Commands:
 *		Add Sensor 				SC+[#]
 *		Remove Sensor 			SC-[#]
 *		Add Servo				SCX[#]
 *		Remove Servo			SCY[#]
 *		Is Servo # Connected?	SCY[#]
 *		Number of Servos		SCC
 *		Is Sensor # In List?	SCS
 *		Servo Commands			SCD[# of servos(1)][ID(2)][torqe en(1)][goal pos(4)][speed(4)][torque(4)]
 *			EXAMPLE:			SCD2001051201000200020102302220123
 *
 *					Number of servos		2
 *						Servo ID			00
 *							Torque En		1
 *							Goal Position	0512
 *							Speed			0100
 *							Torque			0200
 *				
 *						Servo ID			02
 *							Torque En		0
 *							Goal Position	1023
 *							Speed			0222
 *							Torque			0123	
 *
 *	
 *		Servo Response			SS[# of servos(1)][# of sensors(1)][Servo ID(2)][Address(2)][Value(4)]...
 *			EXAMPLE:			SS220000123402432102000001022222
 *					Number of servos		2
 *					Number of sensors		2
 *						Servo ID			00
 *							Sensor Address	00
 *							Sensor Value	1234
 *
 *							Sensor Address	02
 *							Sensor Value	4321
 *
 *						Servo ID			02
 *							Sensor Adderss 	00
 *							Sensor Value	0001
 *
 *							Sensor Address	02
 *							Sensor Value	2222
 *
 */

//length of "SS" = 2
//length of # of servos = 1
//length of # of sensors = 1

#define SS_HEADER_SIZE 2 + 1 + 1
#define SS_SERVO_ID_OFFSET(i, j) SS_HEADER_SIZE + i * (2 + (j * (2 + 4)))
#define SS_SENSOR_ADDRESS_OFFSET(i, j, k) SS_SERVO_ID_OFFSET(i, j) + k * (2 + 4) + 2
#define SS_SENSOR_VALUE_OFFSET(i, j, k) SS_SENSOR_ADDRESS_OFFSET(i, j, k) + 2
#define SS_LENGTH(a, b) SS_HEADER_SIZE + a * (2 + b * (2 + 4))


#endif
