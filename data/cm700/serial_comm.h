//serial_comm

#ifndef __SERIAL_COMM_H__
#define __SERIAL_COMM_H__

//NOTE: MOST OF THIS WAS TAKEN FROM THE SERIAL LIBRARY FOR CM700

#include <stdbool.h>
#include "main.h"

#define SERIAL_BUFF_SIZE 512

/*
 * scom_init
 *
 * Description: initialize the global serial controller
 *
 * Parameters:
 *
 *	ubrr: baudrate
 *		
 *
 * Return:
 *	none
 */
void scom_init(long ubrr);

/*
 * scom_write
 *
 * Description: Write data out over the serial port
 *
 * Parameters
 *	p_data: character array to send out
 *	num:	number of characters in the array
 *
 * Return:
 *	none
 */
void scom_write(unsigned char *p_data, int num);

/*
 * scom_read
 *
 * Description:
 *	reads data from the serial buffer
 *
 * Parameters:
 * 	buffer to put the data (note this buffer can be SERIAL_BUFF_SIZE bits long
 *
 * Return
 *	the length of the data read
 */
unsigned char scom_read(unsigned char *p_data, int num);

/*
 * scom_get_buffer_state
 *
 * Description:
 *	reads the state of the serial buffer
 *
 * Parameters
 *	none
 *
 * Return
 *	Number of bytes avaialable
 *	
 */
int scom_get_buffer_state(void);

/*
 * scom_found_eol
 *
 * Description:
 *	returns true if an '\n' was detected
 *	any read clears the flag
 *
 * Parameters
 *	none
 *
 * Return
 *	true = '\n' detected
 *	false = '\n' not detected
 *
 */
bool scom_found_eol(void);


#endif //__SERIAL_COMM_H__
