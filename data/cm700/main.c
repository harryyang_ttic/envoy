//main.c

#include "main.h"
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "serial_comm.h"
#include "servo_controller.h"
#include <dynamixel.h>



//#define DBG_SLOW

volatile bool startup_flag = true;
volatile bool read_sensor_flag = false;

volatile uint16_t response_length = SS_LENGTH(MAX_NUM_SERVOS, MAX_NUM_SENSORS) + 1;
volatile unsigned char response_buffer[SS_LENGTH(MAX_NUM_SERVOS, MAX_NUM_SENSORS) + 1];
/*
 *	# of servos:  	1 byte 	('0'	- 	'F')
 *	# of sensors: 	1 byte 	('0'	- 	'F')
 *	Servo ID's: 	2 bytes ('00'	- 	'FF')
 *	Sensor Address:	2 bytes ('00'	-	'FF')
 *	Sesnor Value	4 bytes ('0000'	-	'FFFF')
 *	Return			1 byte	'\n'
 *  Line Feed		1 byte  '\r'
 *	Termination 	1 byte	'\0'
 */




//Function Prototypes
int init(void);
bool read_serial_command_data(servo_list_t * servo_list);
bool parse_command_data(servo_list_t *sl, const char *buffer, int count);
void generate_sensor_response(servo_list_t *sl);
bool check_crc(char * buffer, int size);

int main(void)
{

	servo_list_t servo_list;

	int servo_count = 0;
	int sensor_count = 0;
	
	//initialize everyting
	init();


	printf( "\nSOS (Servo OS!) at your service\n" );

	//start up dynamixel controller
	servo_init(&servo_list, 1);

	PORTC = 0x00;
	_delay_ms(500);
	
	PORTC = 0xFF;
	//look for servos
	for (unsigned char i = 0; i < MAX_NUM_SERVOS; i++){
		//initialize each servo to -1 (not used) for now
		//clear the lower byte
		_delay_ms(500);
		PORTC |= 0x1E;
		//set only the lower byte value with the number
		PORTC &=  ~(0x1E & (i << 1));
		//ping
		if (!servo_ping(i)){
			printf ("Servo %d did not respond\n", i);
			PORTC |= 0xF0;
			PORTC &= ~(1);

		}
		else {
			printf ("Servo %d responded\n", i);
			servo_add_servo(&servo_list, i);
			PORTC |= 0xF0;
			PORTC &= ~(1 << 6);
		}
	}


	servo_set_delay(&servo_list, 10);

	//got all my servos
	PORTC = 0xFF;
	//Flash to show we are moving to next stage
	for (int i = 0; i < 7; i++){
		PORTC = ~(1 << i);	
		_delay_ms(250);
	}
	PORTC = 0xFF;

	//flash for the users so they know things are working correctly
	startup_flag = false;

	//main loop
	while (1){
		
		//check if there is any new info from the master
		if (scom_found_eol()){
			TOGGLE_TX;
			if (read_serial_command_data(&servo_list)){
				servo_commands_delta(&servo_list);
				//printf ("Failed to read serial commands!\n");
			}
		}
		servo_count = servo_get_num_servos(&servo_list);
		sensor_count = servo_get_num_sensors(&servo_list);
		//if there is new data confirm that the servo's are not already set with the desired values
		//servos are not set with the desired value, update
		//read the status
		if (read_sensor_flag){


			for (int i = 0; i < servo_count; i++){
				for (int j = 0; j < sensor_count; j++){
					if (!servo_read_sensor_by_index(&servo_list, i, j)){
/*
						printf ("Failed to read sensor: %d from servo: %d\n",
								servo_get_address(&servo_list, j),
								servo_get_id(&servo_list, i));
						printf ("Error: %d\n", servo_get_comm_status());
*/
					}
				}
			}
			if (servo_count * sensor_count > 0){
				TOGGLE_RX;
				generate_sensor_response(&servo_list);

				printf ("%s\n", &response_buffer[0]);

				//scom_write(&response_buffer[0], 1 + 1 + (servo_count * 2) * (sensor_count * 4 * 2) + 1);
			}

		}
	}
	return 1;
}

int init(void){

	DDRC  = 0x7F;
	PORTC = 0x7E;

#ifdef DBG_SLOW
	scom_init(57600);
#else
	scom_init(1000000);
#endif
	TCCR0A = 0b00000000;   	// Normal Mode 
   	TCCR0B = 0b00000101;   	// Div1024 Prescaler 
   	TCNT0 = 0;             	// Initial value
   	TIMSK0 |= (1<<0);	 	// Enable timer
	
	//allow atmel serial interrupts
	sei();								//interrupt enable

	//initialize the buffer
	for (int i = 0; i < response_length; i++){
		response_buffer[i] = 0;
	}
	
	return 0;
}

//ISR
ISR(TIMER0_OVF_vect) {
	//globals
	static unsigned int	alive_timeout = 0;
	if (startup_flag){
		return;
	}


	alive_timeout++;
	if (alive_timeout >= ONE_SEC_TIMEOUT){

		//D1_ACTIVITY_OFF;
		//D2_ACTIVITY_OFF;
		D3_ACTIVITY_OFF;
		
		if (IS_ALIVE_ON){
			ALIVE_OFF;
		}
		else {
			ALIVE_ON;
		}
		alive_timeout = 0;
	}
}

bool read_serial_command_data(servo_list_t * servo_list){
	char buffer[SERIAL_BUFF_SIZE] = {0};
	int size = scom_read(buffer, (int)SERIAL_BUFF_SIZE);
	int address = 0;
	int id = 0;
	int i = 0;
	char * buf = NULL;
	//printf ("%s\n", &buffer[0]);
	if (size < 2){
		//not even enough to compare
		return false;
	}
	buffer[size] = 0;
	
	buf = strstr(&buffer[0], SERVO_COMMAND);
	size = strlen(buf);

	if(buf != NULL){
		switch (buf[2]){
			case (SERVO_ADD_SENSOR):
				//TOGGLE_D1;
				if (size < 3){
					printf ("%s: %s\n",	SERVO_COMMAND_FAIL, buf);
					return false;
				}
				if (servo_get_num_servos(servo_list) >= MAX_NUM_SENSORS){
					printf ("%s: too many sensors\n", SERVO_COMMAND_FAIL);
				}

				i = 3;
				while (i < size - 1){
					if (buf[i] < '0' || buffer[i] > '9'){
						break;
					}
					address = address * 10;
					address = address + buffer[i] - '0';
					i++;
				}

					
				//printf ("%s found sensor_address = %d\n", SERVO_DEBUG, address);
				if (!servo_add_sensor(servo_list, address)){
					printf ("%s: %s\n",	SERVO_COMMAND_FAIL, buf);
				}
				else {
					printf ("%s\n", SERVO_COMMAND_SUCCESS);
				}
				break;
			case (SERVO_REMOVE_SENSOR):
				i = 3;
				while (i < size - 1){
					if (buf[i] < '0' || buf[i] > '9'){
						break;
					}
					address = address * 10;
					address = address + buf[i] - '0';
					i++;
				}
					
				//printf ("%s found sensor_address = %d\n", SERVO_DEBUG, address);
				if (!servo_remove_sensor(servo_list, address)){
					printf ("%s\n",	SERVO_COMMAND_FAIL);
				}
				else {
					printf ("%s\n",	SERVO_COMMAND_SUCCESS);
				}
				break;

			case (SERVO_ADD_SERVO):
			
				i = 3;
				id = 0;
				while (i < size - 1){
					if (buf[i] < '0' || buf[i] > '9'){
						break;
					}
					id = id * 10;
					id += buf[i++] - '0';
				}
				//printf ("%s servo id = %d\n", SERVO_DEBUG);
				if (!servo_add_servo(&servo_list, id)){
					printf ("%s\n",	SERVO_COMMAND_FAIL);
				}
				else {
					printf ("%s\n",	SERVO_COMMAND_SUCCESS);
				}
				
				break;
			case (SERVO_REMOVE_SERVO):
				i = 3;
				id = 0;
				while (i < size - 1){
					if (buf[i] < '0' || buf[i] > '9'){
						break;
					}
					id = id * 10;
					id += buf[i++] - '0';
				}
				if (!servo_remove_servo(&servo_list, id)){
					printf ("%s\n",	SERVO_COMMAND_FAIL);
				}
				else {
					printf ("%s\n",	SERVO_COMMAND_SUCCESS);
				}
			case (SERVO_QUERY_SERVO):
				i = 3;
				id = 0;
				while (i < size - 1){
					if (buf[i] < '0' || buf[i] > '9'){
						break;
					}
					id = id * 10;
					id += buf[i++] - '0';
				}
				if (servo_get_index(servo_list, id) == -1){
					printf ("%s\n",	SERVO_COMMAND_FAIL);
				}
				else {
					printf ("%s\n",	SERVO_COMMAND_SUCCESS);
				}
				
				break;
			case (SERVO_QUERY_COUNT):
				printf ("%s%d\n", 
						SERVO_COMMAND_SUCCESS,
						servo_get_num_servos(servo_list));
				break;
			case (SERVO_QUERY_SENSOR):
				printf ("%s%d\n", 
						SERVO_COMMAND_SUCCESS, 
						servo_get_num_sensors(servo_list));
				break;
			case (SERVO_HELP):
				printf ("%s\n", SERVO_COMMAND_SUCCESS);
				break;
			case (SERVO_START):
				read_sensor_flag = true;
				printf ("%s\n", SERVO_COMMAND_SUCCESS);
				break;
			case (SERVO_STOP):
				read_sensor_flag = false;
				printf ("%s\n", SERVO_COMMAND_SUCCESS);
				break;
			case (SERVO_COMMAND_DATA):
				//printf ("%s\n", &buf[0]);
				if (!check_crc(&buf[0], size)){
					return false;
				}

				if (!parse_command_data(servo_list, &buf[3], size - 4) ){
					printf ("%s\n",	SERVO_COMMAND_FAIL);
				}
				break;
			case (SERVO_CLEAR_SENSORS):
				if (servo_clear_sensors(servo_list)){
					printf ("%s\n",	SERVO_COMMAND_FAIL);
				}
				else {
					printf ("%s\n",	SERVO_COMMAND_SUCCESS);
				}
				break;
			default:
				printf ("%s\n", SERVO_COMMAND_FAIL);
				break;
		}
	}
	return true;
}

bool parse_command_data(servo_list_t *sl, const char *buffer, int count){
	int offset = 0;
	if (count <= 0){
		return false;
	}
	int num_of_servos = buffer[0] - '0';

		
	offset++;
	//for (int i = 0; i < 1; i++){
	for (int i = 0; i < num_of_servos; i++){
		if (count - offset < SIZE_OF_FRAME){
			return false;
		}
		int servo_id = buffer[offset++] - '0';
		servo_id *= 10;
		servo_id = buffer[offset++] - '0';
		int index = servo_get_index(sl, servo_id);
		if (index == -1){
			offset += SIZE_OF_FRAME - 2;
			continue;
		}
		if(index == 0){
			TOGGLE_D2;
		}

		//get the torque enable
		uint8_t torque_en = buffer[offset++] - '0';

		//goal position
		uint16_t goal_position = buffer[offset++] - '0';
		goal_position *= 10;
		goal_position += buffer[offset++] - '0';
		goal_position *= 10;
		goal_position += buffer[offset++] - '0';
		goal_position *= 10;
		goal_position += buffer[offset++] - '0';

		//speed
		uint16_t speed = buffer[offset++] - '0';
		speed *= 10;
		speed += buffer[offset++] - '0';
		speed *= 10;
		speed += buffer[offset++] - '0';
		speed *= 10;
		speed += buffer[offset++] - '0';
			
		//torque
		uint16_t torque = buffer[offset++] - '0';
		torque *= 10;
		torque += buffer[offset++] -'0';	
		torque *= 10;
		torque += buffer[offset++] -'0';	
		torque *= 10;
		torque += buffer[offset++] -'0';	
/*
		printf ("index: %d : torque_en = %d, goal = %d, speed = %d, torque = %d\n", 
					index, torque_en, goal_position, speed, torque); 
*/	
		servo_set_commands(sl, index, torque_en, goal_position, speed, torque);

		
	}


/*	if (sl->servos[0].cmd_goal_position == 512){
		TOGGLE_D1;
	}
*/
//	printf ("Exiting %s\n", __func__);
	return true;
}


void generate_sensor_response(servo_list_t *sl){
	uint16_t servo_count 	= 	servo_get_num_servos(sl);
	uint16_t sensor_count	=	servo_get_num_sensors(sl);
	sprintf (&response_buffer[0], SERVO_STATUS);
	response_buffer[2] = servo_count + '0';
	response_buffer[3] = sensor_count + '0';
	
	for (int i = 0; i < servo_count; i++){
		sprintf (
				&response_buffer[SS_SERVO_ID_OFFSET(i, sensor_count)],
				"%02X", (int) servo_get_id(sl, i));
		for (int j = 0; j < sensor_count; j++){
		sprintf (
				&response_buffer[SS_SENSOR_ADDRESS_OFFSET(i, sensor_count, j)],
				"%02X", (int) servo_get_address(sl, j));
		sprintf (
				&response_buffer[SS_SENSOR_VALUE_OFFSET(i, sensor_count, j)],
				"%04X", (int) servo_get_value(sl, i, j));


		}
	}


//	response_buffer[SS_LENGTH(servo_count, sensor_count) + 1] = '\r';
//	response_buffer[SS_LENGTH(servo_count, sensor_count) + 2] = '\n';

}

bool check_crc(char * buffer, int size){
	char my_crc = 0;
	for (int i = 0; i < size - 3; i++){
		my_crc += buffer[i];
	}
	int high_byte = (0xF0 & my_crc) >> 4;
	int low_byte = (0x0F & my_crc);

//	printf ("high byte = %c\n", high_byte + '0');
//	printf ("low byte  = %c\n", low_byte + '0');

//	printf ("buffer[size - 3] = %c\n", buffer[size - 3]);
//	printf ("buffer[size - 2] = %c\n", buffer[size - 2]);

	if (((high_byte + '0') != buffer[size - 3]) || ((low_byte + '0') != buffer[size - 2])){
		TOGGLE_D1;
		return false;
	}
	return true;

}
