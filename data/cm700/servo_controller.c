#include "servo_controller.h"
#include "dynamixel.h"
#include "main.h"
#include <stdio.h>
#include <avr/io.h>

int status_num = -1;



unsigned char out_buffer[(MAX_NUM_SERVOS * 3) + 4];

//private functions
int get_control_nbits(int address);

bool servo_init(servo_list_t * servo_list, int baud){

	servo_list->cmd_delta = false;
	for (int i = 0; i < MAX_NUM_SENSORS; i++){
		servo_list->servos[i].servo_index = -1;
		
		servo_list->servos[i].cmd_goal_position 		= 0;
		servo_list->servos[i].cmd_goal_position_delta 	= false;
		servo_list->servos[i].cmd_speed_value 			= 0;
		servo_list->servos[i].cmd_speed_value_delta 	= false;
		servo_list->servos[i].cmd_torque_value 			= 0;
		servo_list->servos[i].cmd_torque_value_delta 	= false;
		servo_list->servos[i].cmd_torque_en 			= 0;
		servo_list->servos[i].cmd_torque_en_delta 		= false;
		
		for (int j = 0; j < MAX_NUM_SENSORS; j++){
			servo_list->servos[i].sensors[j].address = 0;
			servo_list->servos[i].sensors[j].value = 0;
		}
	}
	servo_list->servo_count = 0;
	servo_list->sensor_count = 0;

	return (dxl_initialize(0, baud) == 1);
}
void servo_destroy(){
	dxl_terminate();
}
bool servo_ping(int id){
	dxl_ping(id);
	while (1){
		status_num = dxl_get_result();
		switch (status_num){
			case (COMM_RXWAITING):
			case (COMM_TXSUCCESS):
				break;
			case (COMM_RXSUCCESS):
				return true;
			default:
				return false;
				break;
		}
	}
}
bool servo_read(int id, int address, unsigned int *value){
	int size = get_control_nbits(address);
	if (size == 8){
		*value = dxl_read_byte(id, address);
	}
	else if (size == 16){
		*value = dxl_read_word(id, address);
	}
	else {
		//didn't recognize the address!
		status_num = COMM_RXFAIL;
		return false;
	}

	status_num = dxl_get_result();
	while (1) {
		switch (status_num){
			case (COMM_RXWAITING):
				status_num = dxl_get_result();
				//printf(".");
				break;
			case (COMM_RXSUCCESS):
				return true;
			default:
				return false;
		}
	}
	return false;
}
bool servo_write(int id, int address, unsigned int value){
	
	D3_ACTIVITY_ON;
	int size = get_control_nbits(address);
	if (size == 8){
		dxl_write_byte(id, address, value);
	}
	else if (size == 16){
		dxl_write_word( id, address, value);
	}
	else {
		status_num = COMM_TXFAIL;
		return false;
	}
	while (1) {
		switch (status_num){
			case (COMM_TXSUCCESS):
				return true;
			default:
				return false;
		}
	}
	return false;
}
bool servo_sync_write_word(
							int address, 
							int nservos, 
							unsigned char * unit_ids,
							unsigned short * values){


	//printf ("in %s\n", __func__);

	//put the servo ID in the packet
	dxl_set_txpacket_id(BROADCAST_ID);
	dxl_set_txpacket_instruction(INST_SYNC_WRITE);
	//write the address
	dxl_set_txpacket_parameter(0, address);
	dxl_set_txpacket_parameter(1, 3);
	for (int i = 0; i < nservos; i++){
		//printf ("ID: %d : %d\n", unit_ids[i], values[i]);
		dxl_set_txpacket_parameter(2 + 3 * i, unit_ids[i]);
		dxl_set_txpacket_parameter(2 + 3 * i + 1, (values[i] & 0xFF));
		dxl_set_txpacket_parameter(2 + 3 * i + 2, (values[i] >> 8));
	}
	printf("\n");
	dxl_set_txpacket_length(nservos * 3 + 4);
	dxl_txrx_packet();
	status_num = dxl_get_result();
	if (status_num != COMM_TXSUCCESS){
		return false;
	}

	return true;
}
bool servo_sync_write_byte(int address, 
							int nservos, 
							unsigned char * unit_ids,
							unsigned short * values){

	//printf ("in %s\n", __func__);
	//setting up the packet

	//put the servo ID in the packet
	dxl_set_txpacket_id(BROADCAST_ID);
	dxl_set_txpacket_instruction(INST_SYNC_WRITE);
	//write the address
	dxl_set_txpacket_parameter(0, address);
	dxl_set_txpacket_parameter(1, 2);
	for (int i = 0; i < nservos; i++){
		dxl_set_txpacket_parameter(2 + 2 * i, unit_ids[i]);
		dxl_set_txpacket_parameter(2 + 2 * i + 1, (values[i] & 0xFF));
	}
	dxl_set_txpacket_length(nservos * 2 + 4);

	//print out the buffer
	dxl_txrx_packet();
	status_num = dxl_get_result();
	if (status_num != COMM_TXSUCCESS){
		return false;
	}


	return true;

}

bool servo_sync_write(	int address, 
						int nservos, 
						unsigned char * unit_ids, 
						unsigned short *values){

		int size = get_control_nbits(address);
	if (size == 8){
		return servo_sync_write_byte(	
								address, 
								nservos,
								unit_ids,
								values);
	}
	else if (size == 16){
		return servo_sync_write_word(	
								address, 
								nservos,
								unit_ids,
								values);
	}
	else {
		status_num = COMM_TXFAIL;
		return false;
	}

	return false;
}

int  servo_get_comm_status(){
	return status_num;
}

int get_control_nbits(int address){
	switch (address){
		case DYNAMIXEL_CTL_MODEL_NUMBER:
		case DYNAMIXEL_CTL_CW_ANGLE_LIMIT:
		case DYNAMIXEL_CTL_CCW_ANGLE_LIMIT:
		case DYNAMIXEL_CTL_TORQUE_MAX:
		case DYNAMIXEL_CTL_DOWN_CALIBRATION:
		case DYNAMIXEL_CTL_UP_CALIBRATION:
		case DYNAMIXEL_CTL_GOAL_POSITION:
		case DYNAMIXEL_CTL_MOVING_SPEED:
		case DYNAMIXEL_CTL_TORQUE_LIMIT:
		case DYNAMIXEL_CTL_PRESENT_POSITION:
		case DYNAMIXEL_CTL_PRESENT_SPEED:
		case DYNAMIXEL_CTL_PRESENT_LOAD:
		case DYNAMIXEL_CTL_PUNCH:
			return 16;
			break;
		case DYNAMIXEL_CTL_FIRMWARE_VERSION:
		case DYNAMIXEL_CTL_ID:
		case DYNAMIXEL_CTL_BAUD_RATE:
		case DYNAMIXEL_CTL_RETURN_DELAY_TIME:
		case DYNAMIXEL_CTL_TEMP_LIMIT_HIGH:
		case DYNAMIXEL_CTL_VOLTAGE_LIMIT_LOW:
		case DYNAMIXEL_CTL_VOLTAGE_LIMIT_HIGH:
		case DYNAMIXEL_CTL_STATUS_RETURN_LEVEL:
		case DYNAMIXEL_CTL_ALARM_LED:
		case DYNAMIXEL_CTL_ALARM_SHUTDOWN:
		case DYNAMIXEL_CTL_TORQUE_ENABLE:
		case DYNAMIXEL_CTL_LED:
		case DYNAMIXEL_CTL_CW_COMPLIANCE_MARGIN:
		case DYNAMIXEL_CTL_CCW_COMPLIANCE_MARGIN:
		case DYNAMIXEL_CTL_CW_COMPLIANCE_SLOPE:
		case DYNAMIXEL_CTL_CCW_COMPLIANCE_SLOPE:
		case DYNAMIXEL_CTL_PRESENT_VOLTAGE:
		case DYNAMIXEL_CTL_PRESENT_TEMP:
		case DYNAMIXEL_CTL_REGISTERED_INSTRUCTION:
		case DYNAMIXEL_CTL_MOVING:
		case DYNAMIXEL_CTL_LOCK:
			return 8;
			break;
		default:
			return 0;
			break;
		}
	return 0;
}


int servo_get_num_servos(servo_list_t *sl){
	return sl->servo_count;
}
int servo_get_num_sensors(servo_list_t *sl){
	return sl->sensor_count;
}
bool servo_add_sensor(servo_list_t *sl, unsigned char address){
	//printf ("in %s\n", __func__);
	//check if we have the max number of sensors
	if (sl->sensor_count >= MAX_NUM_SENSORS){
		//printf ("Filled up on sensors\n");
		return false;
	}
	if (get_control_nbits(address) == 0){
		//printf ("Unknown address!\n");
		return false;
	}
	//check to see if we already have this address in our sensor list
	for (int i = 0; i < MAX_NUM_SERVOS; i++){
		for (int j = 0; j < sl->sensor_count; j++){
			if (sl->servos[i].sensors[j].address == address) {
				if (j <= sl->sensor_count){
					//we already have it
					//printf ("Already in list\n");
					return true;
				}
				//this is that rare chance that address is equal to 0
				//printf ("added successfullyt\n");
				sl->sensor_count++;
				return true;
			}
		}
	}
	for (int i = 0; i < MAX_NUM_SERVOS; i++){
		//tack the new servo value to the end of the address list
		sl->servos[i].sensors[sl->sensor_count].address = address;
	}
	sl->sensor_count++;
	//printf ("exiting %s\n", __func__);
	return true;
}
bool servo_remove_sensor(servo_list_t *sl, unsigned char address){
	//check if we have anything in the list
	if (sl->sensor_count == 0){
		return false;
	}
	for (int i = 0; i < MAX_NUM_SERVOS; i++){
		for (int j = 0; j < sl->sensor_count; j++){
			if (sl->servos[i].sensors[j].address == address){
				//FOUND IT!
				//shift all the other address over in order to wipe out the address value
				while (j < sl->sensor_count - 2){
					sl->servos[i].sensors[j].address = sl->servos[i].sensors[j+1].address;
				}
				sl->servos[i].sensors[sl->sensor_count - 1].address = 0;
			}
		}
	}
	sl->sensor_count--;
	return true;
}

int sensor_get_address(servo_list_t *sl, int index){
	if (index >= sl->sensor_count){
		return -1;
	}
	return sl->servos[0].sensors[index].address;
	
}
bool servo_set_id(servo_list_t *sl, int index, int servo_id){
	//printf ("in %s\n", __func__);

	//printf ("servo index = %d, servo id = %d\n", index, servo_id);
	if (index >= MAX_NUM_SERVOS){
		return false;
	}
	if ((sl->servos[index].servo_index == -1) &&
		(servo_id != -1)){
		sl->servo_count++;
	}
	else if ((servo_id == -1) &&
			 (sl->servos[index].servo_index != -1)){
		if (sl->servo_count > 0){
			sl->servo_count--;
		}
	}
	sl->servos[index].servo_index = servo_id;
	//printf ("value = %d\n", sl->servos[index].servo_index);
	return true;
}
bool servo_add_servo(servo_list_t *sl, int servo_id){
	//printf ("in %s\n", __func__);

	if (sl->servo_count >= MAX_NUM_SERVOS){
		//printf ("Maximum number of servos reached\n");
		return false;
	}
	if (servo_id < 0){
		//printf ("servo id is an illigal value\n");
		return false;
	}
	for (int i = 0; i < sl->servo_count; i++){
		if (sl->servos[i].servo_index == servo_id){
			//printf ("servo already exists in servo list\n");
			return false;
		}
	}
	
	sl->servos[sl->servo_count].servo_index = servo_id;
	//printf ("added servo %d to location\n", servo_id, sl->servo_count);
	sl->servo_count++;
	return true;

}
//XXX VERIFY THIS CODE!
bool servo_remove_servo(servo_list_t *sl, int servo_id){
	int i = 0;
	for (i = 0; i < sl->servo_count; i++){
		if (sl->servos[i].servo_index == servo_id){
			//found it!
			
			break;
		}
	}
	if (i == sl->servo_count){
		return false;
	}
	
	//need to remove the servo, and fix the list
	for (; i < (sl->servo_count - 1); i++){
		servo_copy_servo(&sl->servos[i + 1], &sl->servos[i]);
	}
	//servo was at the end of the list
	if (i == (sl->servo_count -1)){
		sl->servos[i].servo_index = -1;
	}
	return true;
	
}
void servo_copy_servo(servo_t *servo_in, servo_t *servo_out){
	servo_out->cmd_goal_position 		= servo_in->cmd_goal_position;
	servo_out->cmd_goal_position_delta 	= servo_in->cmd_goal_position_delta;
	servo_out->cmd_speed_value			= servo_in->cmd_speed_value;
	servo_out->cmd_speed_value_delta 	= servo_in->cmd_speed_value_delta;
	servo_out->cmd_torque_value 		= servo_in->cmd_torque_value;
	servo_out->cmd_torque_value_delta 	= servo_in->cmd_torque_value_delta;
	servo_out->cmd_torque_en			= servo_in->cmd_torque_en;
	servo_out->cmd_torque_en_delta 		= servo_in->cmd_torque_en_delta;

	servo_out->servo_index 				= servo_in->servo_index;

	for (int i = 0; i < MAX_NUM_SENSORS; i++){
		servo_out->sensors[i].address	= servo_in->sensors[i].address;
		servo_out->sensors[i].value		= servo_in->sensors[i].value;
	}
}
int servo_get_id(servo_list_t *sl, int index){
	if (index >= MAX_NUM_SERVOS){
		return -1;
	}
	return sl->servos[index].servo_index;
}
int servo_get_index(servo_list_t *sl, int id){
	//printf ("in %s\n", __func__);
	//printf ("looking for %d\n", id);
	for (int i = 0; i < MAX_NUM_SERVOS; i++){
		//printf ("servo[%d].servo_index = %d\n", i, sl->servos[i].servo_index);
		if (sl->servos[i].servo_index == id){
			return i;
		}
	}
	return -1;
}

bool servo_clear_sensors(servo_list_t *sl){
	for (int i = 0; i < sl->servo_count; i++){
		for (int j = 0; j < sl->sensor_count; j++){
			sl->servos[i].sensors[j].address = -1;
			sl->servos[i].sensors[j].value = -1;
			
		}
	}
	sl->sensor_count = 0;
	return true;
}
bool servo_commands_delta (servo_list_t * sl){

	uint16_t address;

	if (!sl->cmd_delta){
		//DONE!
		return false;
	}

	//printf("New DATA!\n");
	sl->cmd_delta = false;

	//TORQUE CONTROL ENABLE
	address = DYNAMIXEL_CTL_TORQUE_ENABLE;
	for (int i = 0; i < sl->servo_count; i++){
		//check if torque_enable is set
		if (sl->servos[i].cmd_torque_en_delta){			
			servo_write(i, address, sl->servos[i].cmd_torque_en);
			sl->servos[i].cmd_torque_en_delta = false;

		}
	}

	//TORQUE LIMIT
	address = DYNAMIXEL_CTL_TORQUE_LIMIT;
		
	for (int i = 0; i < sl->servo_count; i++){
		//check if torque_enable is set
		if (sl->servos[i].cmd_torque_value_delta){
			sl->servos[i].cmd_torque_value_delta = false;
			servo_write(i, address, sl->servos[i].cmd_torque_value);
			sl->servos[i].cmd_torque_en_delta = false;

		}
	}

	
	//SPEED LIMIT
	address = DYNAMIXEL_CTL_MOVING_SPEED;
		
	for (int i = 0; i < sl->servo_count; i++){
		//check if torque_enable is set
		if (sl->servos[i].cmd_speed_value_delta){
			sl->servos[i].cmd_speed_value_delta = false;
			servo_write(i, address, sl->servos[i].cmd_speed_value);
		}
	}

	//GOAL POSITION
	address = DYNAMIXEL_CTL_GOAL_POSITION;
		
	for (int i = 0; i < sl->servo_count; i++){
		//check if torque_enable is set
		if (sl->servos[i].cmd_goal_position_delta){

			sl->servos[i].cmd_goal_position_delta = false;
			servo_write(i, address, sl->servos[i].cmd_goal_position);
		}
	}
	return true;
}

bool servo_set_commands(
							servo_list_t *sl, 
							int index, 
							uint8_t torque_en, 
							uint16_t goal_position,
							uint16_t speed,
							uint16_t torque){

	if (index >= MAX_NUM_SERVOS){
		return false;
	}

	if (sl->servos[index].cmd_torque_en != torque_en){
		sl->servos[index].cmd_torque_en_delta = true;
		sl->servos[index].cmd_torque_en = torque_en;
		sl->cmd_delta = true;
	}
	if (sl->servos[index].cmd_torque_value != torque){
		sl->servos[index].cmd_torque_value_delta = true;
		sl->servos[index].cmd_torque_value = torque;
		sl->cmd_delta = true;
	}
	if (sl->servos[index].cmd_goal_position != goal_position){
		sl->servos[index].cmd_goal_position_delta = true;
		sl->servos[index].cmd_goal_position = goal_position;
		sl->cmd_delta = true;
	}
	if (sl->servos[index].cmd_speed_value != speed){
		sl->servos[index].cmd_speed_value_delta = true;
		sl->servos[index].cmd_speed_value = speed;
		sl->cmd_delta = true;
	}

	return true;
}

bool servo_set_delay (servo_list_t *sl, int delay){
	unsigned char unit_ids[MAX_NUM_SERVOS];
	unsigned char values[MAX_NUM_SERVOS];
	for (int i = 0; i< sl->servo_count; i++){
		unit_ids[i] = servo_get_id(sl, i);
		values[i] = delay;
	}
//TODO FIX THE SYNCRONOUS SET DELAY FUNCTION
/*
	servo_sync_write(	DYNAMIXEL_CTL_RETURN_DELAY_TIME,
						sl->servo_count,
						&unit_ids[0],
						&values[0]);
*/
	return true;
}

bool servo_read_sensor_by_index(servo_list_t * sl, int servo_index, int sensor_index){
	int id = servo_get_id(sl, servo_index);
	int address = servo_get_address(sl, sensor_index);
	uint16_t *value = &sl->servos[servo_index].sensors[sensor_index].value;
	return servo_read(id, address, value);
}
int servo_get_address(servo_list_t *sl, int index){
	return sl->servos[0].sensors[index].address;
}

uint16_t servo_get_value(servo_list_t *sl, int servo_index, int sensor_index){
	return sl->servos[servo_index].sensors[sensor_index].value;
}
