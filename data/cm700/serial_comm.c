
#include <avr/io.h>
#include <stdio.h>
#include <avr/interrupt.h>

#include "serial_comm.h"


#define DEFAULT_BUADRATE 34 // 57132(57600)bps

#define DIR_RXD 	PORTE &= ~0x04, PORTE |= 0x08

volatile unsigned char 	s_buffer[SERIAL_BUFF_SIZE] = {0};
volatile unsigned char 	s_buffer_head = 0;
volatile unsigned char 	s_buffer_tail = 0;
volatile bool			eol_detected = false;

static FILE *device;

//overwrite STDIO lowlevel STDIO function calls
int std_putchar(char c);
int std_getchar(void);

void scom_put_queue(unsigned char data);
unsigned char scom_get_queue(void);


//INTERRUPT entry point
SIGNAL(USART1_RX_vect)
{
	scom_put_queue( UDR1 );
}


void scom_init(long ubrr){
	// Serial communication using UART1
	int baud = (unsigned short)(2000000.0 / ubrr) - 1;

	// set UART register A
	//Bit 7: USART Receive Complete
	//Bit 6: USART Transmit Complete
	//Bit 5: USART Data Resigter Empty 
	//Bit 4: Frame Error
	//Bit 3: Data OverRun
	//Bit 2: Parity Error
	//Bit 1: Double The USART Transmission Speed
	//Bit 0: Multi-Processor Communication Mode
	UCSR1A = 0b01000010;
	
	// set UART register B
	// bit7: enable rx interrupt
    // bit6: enable tx interrupt
    // bit4: enable rx
    // bit3: enable tx
    // bit2: set sendding size(0 = 8bit)
	UCSR1B = 0b10011000;
	
	// set UART register C
	// bit6: communication mode (1 = synchronize, 0 = asynchronize)
    // bit5,bit4: parity bit(00 = no parity) 
    // bit3: stop bit(0 = stop bit 1, 1 = stop bit 2)
    // bit2,bit1: data size(11 = 8bit)
	UCSR1C = 0b00000110;

	// initialize
	UDR1 = 0xFF;
	s_buffer_head = 0;
	s_buffer_tail = 0;

	// set baudrate
	UBRR1H = (unsigned char)(baud>>8);
	UBRR1L = (unsigned char)(baud & 0xFF);
	DIR_RXD;
	device = fdevopen( std_putchar, std_getchar );
}

void scom_write(unsigned char *p_data, int num){
	int count;
	for( count = 0; count < num; count++){
		while (!bit_is_set(UCSR1A, 5));
		UDR1 = p_data[count];
	}
}
unsigned char scom_read(unsigned char *p_data, int num){
	int count;
	int num_read;

	//clear the eol
	eol_detected = false;

	num_read = scom_get_buffer_state();
	//if there is nothing in the buffer then just return 0
	if (num_read == 0){
		return 0;
	}
	//if there are more items in the buffer than the user requested
	if (num_read > num){
		//only get the number of items the user requested
		num_read = num;
	}
	for (count = 0; count < num_read; count++){
		p_data[count] = scom_get_queue();
	}
	return num_read;
}
int scom_get_buffer_state(void){
	short num;

	if (s_buffer_head == s_buffer_tail){
		num = 0;
	}
	else if (s_buffer_head < s_buffer_tail){
		num = s_buffer_tail - s_buffer_head;
	}
	else {
		num = SERIAL_BUFF_SIZE - (s_buffer_head - s_buffer_tail);
	}

	return (int) num;
}
void scom_put_queue(unsigned char data){
	//if the buffer is full just return
	if (data == '\n' || data == '\r'){
		eol_detected = true;
	}
	if (scom_get_buffer_state() == (SERIAL_BUFF_SIZE - 1)){
		return;
	}
	s_buffer[s_buffer_tail] = data;
	if (s_buffer_tail == (SERIAL_BUFF_SIZE - 1)){
		s_buffer_tail = 0;
	}
	else {
		s_buffer_tail++;
	}
}

unsigned char scom_get_queue(void){
	unsigned char data;
	if (s_buffer_head == s_buffer_tail){
		return 0xff;
	}
	data = s_buffer[s_buffer_head];

	if (s_buffer_head == (SERIAL_BUFF_SIZE - 1)){
		s_buffer_head = 0;
	}
	else {
		s_buffer_head++;
	}
	return data;
}

bool scom_found_eol(void){
	return eol_detected;
}

int std_putchar(char c){
	char tx[2];
	if (c == '\n'){
		tx[0] = '\r';
		tx[1] = '\n';
		scom_write ( (unsigned char *) tx, 2);
	}
	else {
		tx[0] = c;
		scom_write ( (unsigned char *) tx, 1);
	}
	return 0;
}
int std_getchar (void){
	eol_detected = false;
	char rx;
	while (scom_get_buffer_state() == 0);
	rx = scom_get_queue();

	if (rx == '\r'){
		rx = '\n';
	}
	return rx;
}
