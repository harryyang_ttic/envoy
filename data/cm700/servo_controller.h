//servo_controller.h

#ifndef __SERVO_CONTROLLER_H__
#define __SERVO_CONTROLLER_H__

#include "main.h"
#include <dynamixel.h>
#include "dynamixel_defines.h"
#include <stdbool.h>

typedef struct _servo_t servo_t;
typedef struct _servo_sensor_t servo_sensor_t;
typedef struct _servo_list_t servo_list_t;


//structure defines
struct _servo_sensor_t {
	uint16_t address;
	uint16_t value;
};

struct _servo_t{
	int servo_index;
	servo_sensor_t sensors[MAX_NUM_SENSORS];

	//commands
	uint16_t cmd_goal_position;
	bool	 cmd_goal_position_delta;
	uint16_t cmd_speed_value;
	bool	 cmd_speed_value_delta;
	uint16_t cmd_torque_value;
	bool	 cmd_torque_value_delta;
	uint16_t cmd_torque_en;
	bool	 cmd_torque_en_delta;
};
struct _servo_list_t{
	servo_t servos[MAX_NUM_SERVOS];
	int servo_count;
	int sensor_count;
	bool cmd_delta;
};

/*
 *		1	=	1000000
 *		3	=	500000
 *		4	= 	400000
 *		7	=	250000
 *		9	=	200000
 *		16	=	115200
 *		34	=	57600
 *		103	=	19200
 *		207	=	9600
*/


/*
 * servo_init
 *
 * Description:
 *	open up the dynamixel servo
 *
 * Parameters
 *	baud: baudrate
 *
 * Return
 *	true = success
 *	false = fail
 */
bool servo_init(servo_list_t * servo_list, int baud);

/*
 * servo_destroy
 *
 * Description
 *	close servos
 *
 * Parameters
 *	none
 *
 * Return
 *	none
 */
void servo_destroy();

/*
 * servo_ping
 *
 * Description:
 *	Attempts to ping the servo
 *
 * Parameters
 * 	id: id of the servo to ping
 *
 * Return:
 *	true = success
 *	false = failed use servo_get_comm_status() to get the error
 *	
 */
bool servo_ping(int id);

/*
 * servo_read
 *
 * Description:
 *	reads a value from a servo
 *
 * Parameters
 *	id: id of servo to read
 *	parameter: address of the parameter to read
 *	value: value read
 *
 * Return:
 *	true = successful
 *	false = failed use servo_get_comm_status() to get the error
 */
bool servo_read(int id, int address, unsigned int *value);

/* 
 * servo_write
 *
 * Description
 *	writes a value to the servo
 *
 * Parameters
 *	id: id of servo to write to
 *	parameter: address of the parameter to read
 *	value:	unsigned value to write
 *
 * Return:
 *	true = successful
 *	false = failed use servo_get_comm_status() to get the error
 */
bool servo_write(int id, int address, unsigned int value);


/* 
 * servo_sync_write
 *
 * Description
 *	writes a value to the servo register only, use servo_sync to execute
 *
 * Parameters
 *	address: address of the parameter to read
 *	nservos: number of servos to write to
 *	unit_ids:	list of servo ids
 *	values:	 values to send... this must match the length of the servo id list
 *
 * Return:
 *	true = successful
 *	false = failed use servo_get_comm_status() to get the error
 */
bool servo_sync_write(	int address, 
						int nservos, 
						unsigned char *unit_ids, 
						unsigned short *values);

/* 
 * servo_sync
 *
 * Description
 *	executes the write command, use this with servo_sync_write to write many registers at once
 *
 * Parameters
 *	id: id of servo to write to
 *
 * Return:
 *	true = successful
 *	false = failed use servo_get_comm_status() to get the error
 */
bool servo_sync(int id);


/*
 * servo_get_error
 *
 * Description
 *	gets the error associated with a previous function call
 *
 * Paramter
 *	none
 *
 * Return
 * 	integer representation of error
 *		COMM_TXSUCCESS 	= 0
 *		COMM_RXSUCCESS 	= 1
 *		COMM_TXFAIL 	= 2
 *		COMM_RXFAIL		= 3
 *		COMM_TXERROR	= 4
 *		COMM_RXWAITING	= 5
 *		COMM_RXTIMEOUT	= 6
 *		COMM_RXCORRUPT	= 7
 */
int  servo_get_comm_status();



int servo_get_num_servos(servo_list_t *sl);
int servo_get_num_sensors(servo_list_t *sl);
bool servo_add_sensor(servo_list_t *sl, unsigned char address);
bool servo_add_servo(servo_list_t *sl, int servo_id);
bool servo_remove_servo(servo_list_t *sl, int servo_id);
bool servo_remove_sensor(servo_list_t *sl, unsigned char address);
int servo_get_address(servo_list_t *sl, int index);
bool servo_set_id(servo_list_t *sl, int index, int servo_id);
int servo_get_id(servo_list_t *sl, int index);
int servo_get_index(servo_list_t *sl, int id);
void servo_copy_servo(servo_t *servo_in, servo_t *servo_out);
uint16_t servo_get_value(servo_list_t *sl, int servo_index, int sensor_index);
bool servo_clear_sensors(servo_list_t *sl);
bool servo_set_delay(servo_list_t *sl, int delay);
/*
 * servo_read_sensor_by_index
 *
 * Description:
 *	reads a specific sensor, from a spicific servo referenced by numbers
 *	this simplifies the reading process
 *
 * Parameter
 *	self
 *	servo_index		index of the servo to read
 *	sensor_index	index of the sensor to read
 *
 * Return
 *	true = success
 *	false = fail
 */
bool servo_read_sensor_by_index(servo_list_t * sl, int servo_index, int sensor_index);

/*
 * servo_commands_delta
 *
 * Description
 * 	servo_list keeps a local copy of the servo commands, and only updates the 
 *	servos when there is a change in the commands, reducing commands to the servos
 *
 * Parameters:
 *	self
 *
 * Return:
 *	success = 	true
 *	fail	=	false
 */
bool servo_commands_delta(servo_list_t *sl);

/*
 * servo_set_commands
 *
 * Description
 *	sets the commands obtaines from the serial port to the servo at the index
 *
 * Parameters
 *	self
 *	index: index of the servo in the list
 *	torque_en
 *	goal_position
 *	speed
 *	torque
 *
 *	Return
 *	 true = servo was found and values set
 */
bool servo_set_commands(
							servo_list_t *sl, 
							int index, 
							uint8_t torque_en, 
							uint16_t goal_position,
							uint16_t speed,
							uint16_t torque);

uint16_t servo_get_value(servo_list_t *sl, int servo_index, int sensor_index);
#endif //__SERVO_CONTROLLER_H__
