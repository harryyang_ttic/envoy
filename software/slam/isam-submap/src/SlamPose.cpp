/*
 * SlamPose.cpp
 *
 *  Created on: Oct 22, 2009
 *      Author: abachrac
 */

#include "SlamPose.h"

using namespace scanmatch;
SlamPose::SlamPose(int64_t _utime, int _node_id, Scan * _scan, Scan * _maxRangeScan,double _height,double _rp[2], int _floor_no) :
    utime(_utime), node_id(_node_id), scan(_scan), maxRangeScan(_maxRangeScan),height(_height), floor_no(_floor_no),loopClosureChecked(false),
    rendered(false)
{
    pose2d_node = new Pose2d_Node();
    constraint_ids.push_back(node_id - 1); //assume constraint between current and prev
    ap = NULL;
    region_id = -1;
    allScans.push_back(scan);
    allScans.push_back(maxRangeScan);

    memcpy(rp,_rp,2*sizeof(double));
}

SlamPose::SlamPose(int64_t _utime, int _node_id, Scan * _scan, Scan * _maxRangeScan,double _height,double _rp[2], int _floor_no, int _region_id) :
    utime(_utime), node_id(_node_id), scan(_scan), maxRangeScan(_maxRangeScan),height(_height), floor_no(_floor_no),loopClosureChecked(false),
    rendered(false)
{
    pose2d_node = new Pose2d_Node();
    constraint_ids.push_back(node_id - 1); //assume constraint between current and prev
    ap = NULL;
    region_id = _region_id;
    allScans.push_back(scan);
    allScans.push_back(maxRangeScan);

    memcpy(rp,_rp,2*sizeof(double));
}

SlamPose::~SlamPose()
{
    delete scan;
    delete maxRangeScan;
    delete pose2d_node;
    constraint_ids.clear();
}

int SlamPose::updateScan()
{
    int updated = 0;
    Pose2d value;
    if(ap){
        value = ap->anchor2d_node->value().oplus(pose2d_node->value());
    }
    else{
        value = pose2d_node->value();
    }

    for (unsigned i = 0; i < allScans.size(); i++) {
        Scan * s = allScans[i];
        //check whether the ScanTransform needs to be updated
        if (sqrt(sm_sq(s->T.x - value.x()) + sm_sq(s->T.y - value.y())) > .001 || fabs(sm_angle_subtract(s->T.theta,
                                                                                                         value.t())) > .001) {
            //need to update ths ScanTranform
            ScanTransform newT;
            newT.x = value.x();
            newT.y = value.y();
            newT.theta = value.t();
            s->applyTransform(newT);
            updated = 1;
        }
    }
    return updated;
}
