/*
 * IsamSlam.cpp
 *
 *  Created on: Oct 22, 2009
 *      Author: abachrac
 */

#include "RegionSlam.h"
#include <math.h>
#include <unistd.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif 

#include <er_carmen/global.h>
#include <interfaces/map3d_interface.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>

using namespace Eigen;
using namespace scanmatch;
RegionSlam::RegionSlam(lcm_t * _lcm,double metersPerPixel_, double thetaResolution_, int useGradientAscentPolish_, int useMultires_,
		       int doDrawing_, bool useThreads_, BotFrames *_frames) :
    loopClosedUpTill(0), gridmap(NULL), gridmapMetersPerPixel(.1), gridmapMargin(10), doDrawing(doDrawing_), useThreads(useThreads_)
{
    //supply dummy values for things we don't care about
    sm = new ScanMatcher(metersPerPixel_, thetaResolution_, useMultires_, false, false);

    slam = new Slam();
    
    last_region_no = 0;
    
    // Create first node at this pose: we add a prior to keep the
    // first pose in place, which is an arbitrary choice.
    Noise sqrtinf3 = SqrtInformation(10. * eye(3));

    gridmap = NULL;
    // create a first pose (a node)
    origin_node = new Pose2d_Node();
    // add it to the graph
    slam->add_node(origin_node);
    // create a prior measurement (a factor)
    Pose2d origin(0., 0., 0.);
    Pose2d_Factor* prior = new Pose2d_Factor(origin_node, origin, sqrtinf3);
    // add it to the graph
    slam->add_factor(prior);
 
    gridmapFreeIncrement = -50;
  
    lcm = _lcm;
    frames = _frames;

    lcmgl_anchors = bot_lcmgl_init(lcm, "regionslam_anchors");
    lcmgl_point_cloud = bot_lcmgl_init(lcm,"isam_pointcloud");
    lcmgl_graph = bot_lcmgl_init(lcm, "regionslam_graph");

    if (useThreads) {
        //remember to make sure that sm_tictoc gets initialized
        killThread = 0;

        /* Initialize mutex and condition variable objects */
        pthread_mutex_init(&trajectory_mutex, NULL);
        pthread_cond_init(&loop_closer_cond, NULL);

        //create rebuilder thread
        pthread_create(&loop_closer, 0, (void *(*)(void *)) loopClose_thread_func, (void *) this);
    }
}

void RegionSlam::drawGraph()
{

    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);

    double usPerMeter = 60 * 1e6;

    double global_to_local[12];
    if (!bot_frames_get_trans_mat_3x4 (frames, "global",
                                       "local",  
                                       global_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;        
    }

    //lets draw the anchors also 

    
    char anchor_info[512];
    bot_lcmgl_color3f(lcmgl_anchors, 0,1.0,0);            
    
    fprintf(stderr, "No of Anchors : %d\n", anchors.size());
    
    for (unsigned i = 0; i < anchors.size(); i++) {
        AnchorPose *ap = anchors[i];
        Pose2d value = ap->anchor2d_node->value();
        double xyz[3] = { value.x(), value.y(), 0};
        double pose_l[3];
        bot_vector_affine_transform_3x4_3d (global_to_local, 
                                            xyz, 
                                            pose_l);

        bot_lcmgl_circle(lcmgl_anchors, pose_l, .3);

        sprintf(anchor_info,"A[%d]", i);
        
        bot_lcmgl_text(lcmgl_anchors, pose_l, anchor_info);

    }
    
    bot_lcmgl_switch_buffer(lcmgl_anchors);
  
   

    bot_lcmgl_color3f(lcmgl_graph, 1,0,0);        
    
    char pose_info[512];
    //draw nodes
    for (unsigned i = 0; i < trajectory.size(); i++) {
        SlamPose * slampose = trajectory[i];
        Pose2d value = slampose->getPose();
        
        double xyz[3] = { value.x(), value.y(), 0};
        double pose_l[3];
        bot_vector_affine_transform_3x4_3d (global_to_local, 
                                            xyz, 
                                            pose_l);
        bot_lcmgl_circle(lcmgl_graph, pose_l, .3);

        sprintf(pose_info,"%d-%d", i, trajectory[i]->region_id);

           

        bot_lcmgl_circle(lcmgl_graph, pose_l, .3);
        bot_lcmgl_text(lcmgl_graph, pose_l, pose_info);
    }

    //draw odometry edges
    bot_lcmgl_color3f(lcmgl_graph, 0, 1, 1);
    bot_lcmgl_line_width(lcmgl_graph, 3);
    bot_lcmgl_begin(lcmgl_graph, GL_LINES);
      
    for (unsigned i = 1; i < trajectory.size(); i++) {
        SlamPose * slampose_prev = trajectory[i - 1];
        SlamPose * slampose_curr = trajectory[i];
                
        Pose2d value_prev = slampose_prev->getPose(); 
        Pose2d value_curr = slampose_curr->getPose(); 
      
        double p_pose_g[3] = {value_prev.x(), value_prev.y(), 0}, p_pose_l[3], c_pose_g[3] = {value_curr.x(), value_curr.y(), 0} , c_pose_l[3];
    
        bot_vector_affine_transform_3x4_3d (global_to_local, 
                                            p_pose_g, 
                                            p_pose_l);

        bot_vector_affine_transform_3x4_3d (global_to_local, 
                                            c_pose_g, 
                                            c_pose_l);
    
        bot_lcmgl_vertex3f(lcmgl_graph, p_pose_l[0], p_pose_l[1], 0);//(slampose_prev->utime - trajectory[0]->utime)
        //    / usPerMeter);
        bot_lcmgl_vertex3f(lcmgl_graph, c_pose_l[0], c_pose_l[1], 0);//(slampose_curr->utime - trajectory[0]->utime)
        //  / usPerMeter);
    }
    
    bot_lcmgl_end(lcmgl_graph);
    bot_lcmgl_line_width(lcmgl_graph, 3);
    bot_lcmgl_color3f(lcmgl_graph, 1, 0, 0);

    //draw loop closures
  
    for (unsigned i = 1; i < trajectory.size(); i++) {
        SlamPose * slampose = trajectory[i];
        for (unsigned j = 1; j < slampose->constraint_ids.size(); j++) { //constraints after the first are loop closures
            SlamPose * slampose2 = trajectory[slampose->constraint_ids[j]];

            Pose2d_Node* pose1node = slampose->pose2d_node;
            Pose2d_Node* pose2node = slampose2->pose2d_node;
            Pose2d value1 = slampose->getPose();
            Pose2d value2 = slampose2->getPose();
            
            bot_lcmgl_begin(lcmgl_graph, GL_LINES);

            double p_s_g[3] = {value1.x(), value1.y(), 0}, p_e_g[3] = {value2.x(), value2.y(), };
            double p_s_l[3], p_e_l[3];
      
            bot_vector_affine_transform_3x4_3d (global_to_local, 
                                                p_s_g, 
                                                p_s_l);
      
            bot_vector_affine_transform_3x4_3d (global_to_local, 
                                                p_e_g, 
                                                p_e_l);
      
            bot_lcmgl_vertex3f(lcmgl_graph, p_s_l[0], p_s_l[1], 0);
            bot_lcmgl_vertex3f(lcmgl_graph, p_e_l[0], p_e_l[1], 0);
            bot_lcmgl_end(lcmgl_graph);
        }
    }

    bot_lcmgl_switch_buffer(lcmgl_graph);

    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);

}

//search through the anchorposes to see if the region is already there 
AnchorPose *RegionSlam::getAnchorPose(int region){
    for(unsigned t = 0; t < anchors.size(); t++) {
        if(region == anchors[t]->region_id){
            //fprintf(stderr,"Found anchor\n");
            return anchors[t];
        }
    }
  
    //fprintf(stderr,"Anchor not found\n"); 
    return NULL;
}

RegionSlam::~RegionSlam()
{
    if (useThreads) {
        //kill the loop closer thread
        while (killThread != -1) {
            killThread = 1;
            pthread_cond_broadcast(&loop_closer_cond);
            usleep(10000);
        }
        //aquire all locks so we can destroy them
        pthread_mutex_lock(&trajectory_mutex);
        // destroy mutex and condition variable objects
        pthread_mutex_destroy(&trajectory_mutex);
        pthread_cond_destroy(&loop_closer_cond);
    }
    fprintf(stderr, "Killed loop closing thread");

    delete origin_node;
    delete slam;
    delete sm;

    //clear the trajectory
    /*for (unsigned i = 0; i < trajectory.size(); i++)
      delete trajectory[i];
      trajectory.clear();*/
}

void RegionSlam::addNodeToSlamAnchor(Pose2d &prev_curr_tranf, Noise &cov, Scan * scan, Scan * maxRangeScan, int64_t utime,
				     double height, double rp[2], int floor_no, int region_no)
{
    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);

    int add_anchor = 0;

    if(region_no != last_region_no){
        fprintf(stderr,"++++ New region : %d - Adding anchor node\n", region_no);
        add_anchor = 1;
    }
    
    SlamPose * slampose = new SlamPose(utime, trajectory.size() + 1, scan, maxRangeScan, height, rp, floor_no, region_no);

    slam->add_node(slampose->pose2d_node);
    
    //fprintf(stderr,"Added slam pose : %d\n", trajectory.size());

    Pose2d_Pose2d_Factor* constraint;

    if(!add_anchor){
        if (!trajectory.empty())
            constraint = new Pose2d_Pose2d_Factor(trajectory.back()->pose2d_node, slampose->pose2d_node, prev_curr_tranf, cov);
        else
            constraint = new Pose2d_Pose2d_Factor(origin_node, slampose->pose2d_node, prev_curr_tranf, cov);
     
        slam->add_factor(constraint);
    }

    //if the region has changed - we check and add an anchor node and add a constraint for the 
    //anchor node and the involved nodes 

    if(add_anchor){
        if (!trajectory.empty()){
            //for a new region add an origin node 
            //hmm - i wonder if this will break if its a new region 
            Pose2d prior_origin(0., 0., 0.);
            Noise noise = SqrtInformation(10. * eye(3));
            Pose2d_Factor *p_a0 = new Pose2d_Factor(slampose->pose2d_node, prior_origin, noise);
            slam->add_factor(p_a0);

            AnchorPose *anchorpose = getAnchorPose(last_region_no);
	  
            if(anchorpose == NULL){
                anchorpose = new AnchorPose(0, slam, anchors.size() +1, last_region_no);
                slam->add_node(anchorpose->anchor2d_node);
                anchors.push_back(anchorpose);
            }
      
            AnchorPose *anchorpose1 = new AnchorPose(utime, slam, anchors.size() +1, region_no);
            slam->add_node(anchorpose1->anchor2d_node);

            Pose2d_Pose2d_Factor *d_a1_b1 =  new Pose2d_Pose2d_Factor(trajectory.back()->pose2d_node, slampose->pose2d_node, prev_curr_tranf, cov, anchorpose->anchor2d_node, anchorpose1->anchor2d_node);
            slam->add_factor(d_a1_b1);

            anchors.push_back(anchorpose1);
        }
    }
    
    trajectory.push_back(slampose);

    last_region_no = region_no;

    for (unsigned t = 0; t < trajectory.size(); t++) {
        SlamPose * slampose = trajectory[t];
        
        //hmm, maybe we should just attach the anchor node to the slam poses 
        AnchorPose *ap = getAnchorPose(slampose->region_id);
        
        slampose->ap = ap;
    }

    if (useThreads) {
        slam->batch_optimization();
        
        pthread_mutex_unlock(&trajectory_mutex);
        pthread_cond_broadcast(&loop_closer_cond);//tell the loop closing thread to have at it...
    }
    
    else{
        doLoopClosing(trajectory.size() - 1);
    }
}

void RegionSlam::checkForUpdates()
{
    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);
    
    // Compute the bounds of the current map.

    double minxy[2] = { DBL_MAX, DBL_MAX };
    double maxxy[2] = { -DBL_MAX, -DBL_MAX };
    
    bool needToDelete = false;
    for (unsigned t = 0; t < trajectory.size(); t++) {
      
        int updated = trajectory[t]->updateScan();//update the scan to match the current slam pos
        if (updated && trajectory[t]->rendered)
            needToDelete = true;
        for (unsigned j = 0; j < trajectory[t]->allScans.size(); j++) {
            Scan * s = trajectory[t]->allScans[j];
	
            for (unsigned cidx = 0; cidx < s->contours.size(); cidx++) {
                for (unsigned k = 0; k < s->contours[cidx]->points.size(); k++) {
                    smPoint p = s->contours[cidx]->points[k];
                    minxy[0] = fmin(minxy[0], p.x);
                    maxxy[0] = fmax(maxxy[0], p.x);
                    minxy[1] = fmin(minxy[1], p.y);
                    maxxy[1] = fmax(maxxy[1], p.y);
                }
            }
        }
    }

    fprintf(stderr,"Map size : %f,%f - %f,%f \n", minxy[0], minxy[1], maxxy[0], maxxy[1]);
    
    //check if the map needs to be increased 
    if (gridmap != NULL) {
        if (minxy[0] < gridmap->xy0[0] || maxxy[0] > gridmap->xy1[0] || minxy[1] < gridmap->xy0[1] || maxxy[1]
            > gridmap->xy1[1])
            needToDelete = true;
        if (needToDelete) {
            delete gridmap;
            gridmap = NULL;
            //mark all floor poses as unrendered 
            for (unsigned t = 0; t < trajectory.size(); t++){
                trajectory[t]->rendered = false;
            }
        }
    }
      
    if (gridmap == NULL) {
        printf("creating new gridmap\n");
        minxy[0] -= gridmapMargin;
        minxy[1] -= gridmapMargin;
        maxxy[0] += gridmapMargin;
        maxxy[1] += gridmapMargin;
        gridmap = new GridMap(minxy, maxxy, gridmapMetersPerPixel);
    }

    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);
}

void RegionSlam::draw3DPointCloud()
{
    checkForUpdates();
    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);

    double blue_height = .2;
    double red_height = 3;
    double z_norm_scale = 1 / (red_height - blue_height);

    bot_lcmgl_push_attrib(lcmgl_point_cloud, GL_DEPTH_BUFFER_BIT | GL_POINT_BIT | GL_CURRENT_BIT);
    bot_lcmgl_enable(lcmgl_point_cloud, GL_DEPTH_TEST);
    bot_lcmgl_depth_func(lcmgl_point_cloud, GL_LESS);

    bot_lcmgl_point_size(lcmgl_point_cloud, 2);
    bot_lcmgl_begin(lcmgl_point_cloud, GL_POINTS);

    for (unsigned t = 0; t < trajectory.size(); t++) {
        SlamPose * slampose = trajectory[t];
        Pose2d value = slampose->getPose();

        Scan * scan = slampose->scan;
        double pBody[3] = { 0, 0, 0 };
        double pLocal[3];
        BotTrans bodyToLocal;
        bodyToLocal.trans_vec[0] = value.x();
        bodyToLocal.trans_vec[1] = value.y();
        bodyToLocal.trans_vec[2] = slampose->height;
        double rpy[3] = { slampose->rp[0], slampose->rp[1], value.t() };
        bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
        for (unsigned i = 0; i < scan->numPoints; i++) {
            pBody[0] = scan->points[i].x;
            pBody[1] = scan->points[i].y;
            //transform to local frame
            bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
            double z_norm = (pLocal[2] - blue_height) * z_norm_scale;
            float * color3fv = bot_color_util_jet(z_norm);
            bot_lcmgl_color3f(lcmgl_point_cloud, color3fv[0], color3fv[1], color3fv[2]);
            bot_lcmgl_vertex3f(lcmgl_point_cloud, pLocal[0], pLocal[1], pLocal[2]);
        }
    }

    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);

    bot_lcmgl_end(lcmgl_point_cloud);
    bot_lcmgl_pop_attrib(lcmgl_point_cloud);
    bot_lcmgl_switch_buffer(lcmgl_point_cloud);

}

void RegionSlam::renderCurrentGridmap()
{
    checkForUpdates();

    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);
    sm_tictoc("renderGridmap_nolocking");
    fprintf(stderr,"Rendering gridmap\n");
    for (unsigned t = 0; t < trajectory.size(); t++) {
        SlamPose * slampose = trajectory[t];
        if (slampose->rendered)
            continue;
        else
            slampose->rendered = true;
    
    
        Pose2d bodyPos = slampose->getPose();

        double bodyP[2] = { bodyPos.x(), bodyPos.y() };

        //fprintf(stderr,"Body Pos : %f,%f\n", bodyPos.x(), bodyPos.y());

        for (unsigned s = 0; s < slampose->allScans.size(); s++) {
            if (s > 0 && trajectory.size() - t > 3)
                continue; //only draw maxranges for last 3 scans
            Scan * scan = slampose->allScans[s];
            //ray trace to compute the map...
            sm_tictoc("render_raytrace");
            for (unsigned i = 0; i < scan->numPoints; i++) {
                //fprintf(stderr, "%d - %d - %f,%f\n", t, s, scan->ppoints[i].x, scan->ppoints[i].y);
                gridmap->rayTrace(bodyP, smPoint_as_array(&scan->ppoints[i]), (s == 0), 1, false);
            }

            if (s == 0) {
                //draw the contours as lines for cleaner walls
                for (unsigned cidx = 0; cidx < scan->contours.size(); cidx++) {
                    for (unsigned i = 0; i + 1 < scan->contours[cidx]->points.size(); i++) {
                        //draw the occupied regions
                        smPoint p0 = scan->contours[cidx]->points[i];
                        smPoint p1 = scan->contours[cidx]->points[i + 1];
                        gridmap->rayTrace(smPoint_as_array(&p0), smPoint_as_array(&p1), 1, 0, true);
                    }
                }
            }
	    
            sm_tictoc("render_raytrace");
        }       
    }
    
    gridmap->generate_likelihoods();
    erlcm_gridmap_t * lcm_msg = gridmap->get_comp_gridmap_t();
    lcm_msg->utime = bot_timestamp_now();
    erlcm_gridmap_t_publish(lcm, GMAPPER_GRIDMAP_CHANNEL, lcm_msg);
    
    sm_tictoc("renderGridmap_nolocking");
    
    //  cvSaveImage("gridmap.bmp", &gridmap->distim);
    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);
}


void RegionSlam::addNodeToSlam(Pose2d &prev_curr_tranf, Noise &cov, Scan * scan, Scan * maxRangeScan, int64_t utime,
			       double height, double rp[2], int floor_no)
{
    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);

    SlamPose * slampose = new SlamPose(utime, trajectory.size() + 1, scan, maxRangeScan, height, rp, floor_no);

    slam->add_node(slampose->pose2d_node);

    Pose2d_Pose2d_Factor* constraint;

    if (!trajectory.empty())
        constraint = new Pose2d_Pose2d_Factor(trajectory.back()->pose2d_node, slampose->pose2d_node, prev_curr_tranf, cov);
    else
        constraint = new Pose2d_Pose2d_Factor(origin_node, slampose->pose2d_node, prev_curr_tranf, cov);

    //current_floor = getFloorIndex(floor_no);
    slam->add_factor(constraint);
    trajectory.push_back(slampose);
    
    if (useThreads) {
   
        pthread_mutex_unlock(&trajectory_mutex);
        pthread_cond_broadcast(&loop_closer_cond);//tell the loop closing thread to have at it...
    }
    else{
        doLoopClosing(trajectory.size() - 1);
    }
}

void RegionSlam::doLoopClosing(int loop_close_ind)
{
  
    //the loop closing doesn't work 

    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);
    if (loop_close_ind < 20) {
        trajectory[loop_close_ind]->loopClosureChecked = true;
        if (useThreads)
            pthread_mutex_unlock(&trajectory_mutex);
        return; //nothin to loop close against yet
    }

    //see if the end of the trajectory matches something toward the back
    SlamPose * slampose_tocheck = trajectory[loop_close_ind];
    SlamPose * slampose_toverify = trajectory[loop_close_ind - 1];
    slampose_tocheck->loopClosureChecked = true;

    Pose2d tocheck_value = slampose_tocheck->getPose() ;
    
    Pose2d toverify_value = slampose_toverify->getPose();
    
    smPoint p0 = { tocheck_value.x(), tocheck_value.y() };
  
    int curr_node_floor = slampose_tocheck->floor_no;

    int closestInd = -1;
    double closestDist = 1e9;
    for (int i = 0; i < (loop_close_ind - 30); i++) {
        Pose2d value = trajectory[i]->getPose();
      
        if(curr_node_floor != trajectory[i]->floor_no){
            continue;
        }
        smPoint p1 = { value.x(), value.y() };
        double d = sm_dist(&p0, &p1);
        if (d < closestDist) {
            closestInd = i;
            closestDist = d;
        }
    }
    
    //nothing close found 
    if (closestDist > 10) {
        if (useThreads)
            pthread_mutex_unlock(&trajectory_mutex);
        return;
    }

    sm_tictoc("update_transforms");
    //lets try to match against the closest
    //TODO: may want to match against multiple trajectory sections
    int rangeStart = sm_imax(0, closestInd - 10);
    int rangeEnd = closestInd + 10; //has to be at least 40 from the end

    for (int i = rangeStart; i <= rangeEnd; i++) {
        //update the scan match transform with the current SLAM value estimate
        Pose2d value = trajectory[i]->getPose();;
        
        ScanTransform newT;
        newT.x = value.x();
        newT.y = value.y();
        newT.theta = value.t();
        trajectory[i]->scan->applyTransform(newT);

        sm->addScan(trajectory[i]->scan, false);
    }

    sm_tictoc("update_transforms");

    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);

    sm_tictoc("build_map");
    sm->addScan(NULL, true); //actually do the raster rebuild
    sm_tictoc("build_map");

    sm_tictoc("LC_Match");
    slampose_tocheck->scan->T.score = 0;
  
    //transform the points
    ScanTransform newT;
    newT.x = tocheck_value.x();
    newT.y = tocheck_value.y();
    newT.theta = tocheck_value.t();
    slampose_tocheck->scan->applyTransform(newT);

    ScanTransform lc_r = sm->gridMatch(slampose_tocheck->scan->points, slampose_tocheck->scan->numPoints,
                                       &slampose_tocheck->scan->T, 6.0, 6.0, M_PI / 6.0);
    sm_tictoc("LC_Match");
    double hitpct = lc_r.hits / (double) slampose_tocheck->scan->numPoints;
    double sxx = lc_r.sigma[0];
    double sxy = lc_r.sigma[1];
    double syy = lc_r.sigma[4];
    double stt = lc_r.sigma[8];

    bool accepted = false;
    if (hitpct > .60) {
        fprintf(stderr, "tentatively ACCEPTED match for node %d had %f%% hits...", slampose_tocheck->node_id, hitpct);
        accepted = true;
    }
    else {
        fprintf(stderr, "REJECTED match for node %d had %f%% hits, sxx=%f,sxy=%f,syy=%f,stt=%f\n",
                slampose_tocheck->node_id, hitpct, lc_r.sigma[0], lc_r.sigma[1], lc_r.sigma[4], lc_r.sigma[8]);
    }

    if (accepted) {
        double sigma[9];
        memcpy(sigma, lc_r.sigma, 9 * sizeof(double));
        double evals[3] = { 0 };
        double evals_sq[9] = { 0 };
        double evecs[9] = { 0 };
        CvMat cv_sigma = cvMat(3, 3, CV_64FC1, sigma);
        CvMat cv_evals = cvMat(3, 1, CV_64FC1, evals);
        CvMat cv_evecs = cvMat(3, 3, CV_64FC1, evecs);
        cvEigenVV(&cv_sigma, &cv_evecs, &cv_evals);
        if (evals[0] < .01) {
            fprintf(stderr, "sxx=%f,sxy=%f,syy=%f,stt=%f, eigs=[%f %f %f]\n", sxx, sxy, syy, stt, evals[0], evals[1],
                    evals[2]);
            accepted = true;
        }
        else {
            fprintf(stderr, "REJECTED sxx=%f,sxy=%f,syy=%f,stt=%f, eigs=[%f %f %f]\n", sxx, sxy, syy, stt, evals[0],
                    evals[1], evals[2]);
            accepted = false;
        }
    }
    if (accepted) {
        if (doDrawing) {
            sm->drawGUI(slampose_tocheck->scan->points, slampose_tocheck->scan->numPoints, lc_r, NULL, "LC_accept",
                        CV_RGB(0,255,0));
        }
        //perform rigidity check
        Pose2d newToCheckValue(lc_r.x, lc_r.y, lc_r.theta);
        Pose2d oldDelta = toverify_value.ominus(tocheck_value);
        Pose2d newToVerifyValue = newToCheckValue.oplus(oldDelta); //if match is rigid, verify scan should be here...
        Pose2d sanity = tocheck_value.oplus(oldDelta);

        ScanTransform newT;
        memset(&newT, 0, sizeof(newT));
        newT.x = newToVerifyValue.x();
        newT.y = newToVerifyValue.y();
        newT.theta = newToVerifyValue.t();

        ScanTransform vT;
        vT.x = toverify_value.x();
        vT.y = toverify_value.y();
        vT.theta = toverify_value.t();
        slampose_toverify->scan->applyTransform(newT);

        ScanTransform verify_r = sm->gridMatch(slampose_toverify->scan->points, slampose_toverify->scan->numPoints, &newT,
                                               1.0, 1.0, M_PI / 12.0);
        smPoint p0 = { verify_r.x, verify_r.y };
        smPoint p1 = { newT.x, newT.y };
        double dist = sm_dist(&p0, &p1);
        double adist = fabs(sm_angle_subtract(verify_r.theta, newT.theta));
        if (dist < .04 && adist < .01) {
            fprintf(stderr, "Match PASSED rigidity check... dist=%f, adist=%f\n", dist, adist);
            if (doDrawing) {
                sm->drawGUI(slampose_toverify->scan->points, slampose_toverify->scan->numPoints, verify_r, NULL, "LC_verify",
                            CV_RGB(0,255,0));
            }
        }
        else {
            fprintf(stderr, "Match FAILED rigidity check... dist=%f, adist=%f\n", dist, adist);
            if (doDrawing) {
                sm->drawGUI(slampose_toverify->scan->points, slampose_toverify->scan->numPoints, verify_r, NULL,
                            "LC_REJECTED_verify", CV_RGB(255,0,0));
            }
            accepted = false;
        }

    }

    if (accepted) {
        sm_tictoc("loopCloseOptimize");
        if (useThreads)
            pthread_mutex_lock(&trajectory_mutex);

        //add the edge to isam
        Pose2d matchedPos(lc_r.x, lc_r.y, lc_r.theta);
        Pose2d closestPos = trajectory[closestInd]->getPose();
        Pose2d transf = matchedPos.ominus(closestPos); //get the delta between that pose and the current

        //TODO: probably want to scale the covariance...
        //rotate the cov to body frame
        double Rcov[9];
        sm_rotateCov2D(lc_r.sigma, -lc_r.theta, Rcov);
        //Matrix cov(3, 3, Rcov);
        Matrix3d cv(Rcov);

        Noise cov = Covariance(10000.0 * cv);
        double cov_hardcode[9] = { .25, 0, 0, 0, .25, 0, 0, 0, .1 };
        //Matrix cov_hc(3, 3, cov_hardcode);
        Matrix3d cv_hc(cov_hardcode);// =// MatrixXd::Random(3,3);
        Noise cov_hc = Covariance(cv_hc);

        int same_region = 0;
        int different_ind = 0;
        if(trajectory[closestInd]->ap && slampose_tocheck->ap){
            if(trajectory[closestInd]->ap->region_id ==  slampose_tocheck->ap->region_id){
                same_region = 1;
            }
            else{
                different_ind = 1;
            }
        }

        if(same_region){
            fprintf(stderr,"Matches are in the same region\n");
            Pose2d_Pose2d_Factor* constraint = new Pose2d_Pose2d_Factor(trajectory[closestInd]->pose2d_node,
                                                                        slampose_tocheck->pose2d_node, transf, cov_hc);
            slampose_tocheck->constraint_ids.push_back(closestInd);
            slam->add_factor(constraint);
        }
        else if(different_ind){
            fprintf(stderr,"Matches are in a different region\n");
            Pose2d_Pose2d_Factor *d_a1_b1 =  new Pose2d_Pose2d_Factor(trajectory[closestInd]->pose2d_node, slampose_tocheck->pose2d_node,  transf, cov_hc, trajectory[closestInd]->ap->anchor2d_node, slampose_tocheck->ap->anchor2d_node);
            slampose_tocheck->constraint_ids.push_back(closestInd);
            slam->add_factor(d_a1_b1);
        }
	
        //optimize the graph
        sm_tictoc("batch_optimization");
        slam->batch_optimization();
        sm_tictoc("batch_optimization");

        if (useThreads)
            pthread_mutex_unlock(&trajectory_mutex);
        sm_tictoc("loopCloseOptimize");
    }
    else if (doDrawing) {
        sm->drawGUI(slampose_tocheck->scan->points, slampose_tocheck->scan->numPoints, lc_r, NULL, "LC_reject",
                    CV_RGB(255,0,0));
    }

    sm->clearScans(false);//clear out scans to get ready for next time
}

void * RegionSlam::loopClose_thread_func(RegionSlam *parent)
{
  
    fprintf(stderr, "Loop closure thread started\n");
  
    pthread_mutex_lock(&parent->trajectory_mutex);
    while (!parent->killThread) {
        int ind_to_check = -1;
        for (int i = parent->trajectory.size() - 1; i >= parent->loopClosedUpTill; i--) {
            if (!parent->trajectory[i]->loopClosureChecked) {
                ind_to_check = i;
                break;
            }
        }
        if (ind_to_check < 0) {
            //all nodes have been checked already... so lets go to sleep
            parent->loopClosedUpTill = parent->trajectory.size();
            pthread_cond_wait(&parent->loop_closer_cond, &parent->trajectory_mutex);
            continue;
        }
        else {
            //need to check this node
            pthread_mutex_unlock(&parent->trajectory_mutex); //unlock since lock is reacquired in doLoopClosing
            parent->doLoopClosing(ind_to_check);
            pthread_mutex_lock(&parent->trajectory_mutex); //lock to go back around the loop
        }
    
    }
    pthread_mutex_unlock(&parent->trajectory_mutex);
    parent->killThread = -1;
    fprintf(stderr, "Loop closure thread stopped\n");
    return NULL;
}
