/**
 * @file anchorNodes.cpp
 * @brief Multiple relative pose graphs with relative constraints.
 * @author Michael Kaess
 * @version $Id: anchorNodes.cpp 6335 2012-03-22 23:13:52Z kaess $
 *
 * For details on the concept of anchor nodes see:
 * “Multiple Relative Pose Graphs for Robust Cooperative Mapping”
 * B. Kim, M. Kaess, L. Fletcher, J. Leonard, A. Bachrach, N. Roy, and S. Teller
 * IEEE Intl. Conf. on Robotics and Automation, ICRA, (Anchorage, Alaska), May 2010, pp. 3185-3192.
 * online available at http://www.cc.gatech.edu/~kaess/pub/Kim10icra.html
 *
 * Copyright (C) 2009-2012 Massachusetts Institute of Technology.
 * Michael Kaess, Hordur Johannsson, David Rosen and John J. Leonard
 *
 * This file is part of iSAM.
 *
 * iSAM is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * iSAM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with iSAM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

//local stuff
#include <iostream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include <opencv/highgui.h>
#include <getopt.h>

#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
//bot param stuff
#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>

#include <scanmatch/ScanMatcher.hpp>
#include <lcmtypes/sm_rigid_transform_2d_t.h>
#include <bot_frames/bot_frames.h>
#include <lcmtypes/er_lcmtypes.h>

#include <isam/isam.h>
#include "isam/Anchor.h"
#include "RegionSlam.h"

using namespace scanmatch;
using namespace std;
using namespace isam;
using namespace Eigen;

//add a new node every this many meters
#define LINEAR_DIST_TO_ADD 1.0
//and or this many radians
#define ANGULAR_DIST_TO_ADD 1.0
#define LASER_DATA_CIRC_SIZE 40

typedef struct {
    lcm_t * lcm;
    RegionSlam * regionslam;
    ScanMatcher * sm;
    ScanMatcher * sm_incremental;

    BotFrames *frames;
    BotParam *param;

    int scanmatchBeforAdd;
    double odometryConfidence;
    int publish_pose;
    int useOdom; 
    char * chan;

    int beam_skip; //downsample ranges by only taking 1 out of every beam_skip points
    double spatialDecimationThresh; //don't discard a point if its range is more than this many std devs from the mean range (end of hallway)
    double maxRange; //discard beams with reading further than this value
    double maxUsableRange; //only draw map out to this far for MaxRanges...
    float validBeamAngles[2]; //valid part of the field of view of the laser in radians, 0 is the center beam

    sm_rigid_transform_2d_t * prev_odom; //odometry computed outside module
    sm_rigid_transform_2d_t * prev_sm_odom; //internally scan matched odometry
    int create_regions;
    int verbose;
    int slave_mode;
    BotPtrCircular   *front_laser_circ;
    BotPtrCircular   *rear_laser_circ;

    bot_core_planar_lidar_t * r_laser_msg;
    bot_core_pose_t *last_deadreakon; 
    int64_t r_utime;

    int use_frames; 
    int region;

} app_t;

Scan *get_maxrange_scan(float * ranges, int numPoints, double thetaStart, 
                        double thetaStep, double maxRange,
                        double maxUsableRange, double validRangeStart, double validRangeEnd)
{
    Scan * scan = new Scan();
    smPoint * points = (smPoint *) malloc(numPoints * sizeof(smPoint));
    scan->points = points;
    int numMaxRanges = 0;
    double theta = thetaStart;
    Contour * contour = new Contour();
    scan->contours.push_back(contour);
    for (int i = 0; i < numPoints; i++) {
        double r = ranges[i];
        if ((r <= 0.01) || (r >= maxRange)) {
            continue;
        }
        if (r < .1) {
            r = maxRange;
        }
        if (r >= maxRange && theta > validRangeStart && theta < validRangeEnd) {
            r = maxUsableRange;
            //project to body centered coordinates
            points[numMaxRanges].x = r * cos(theta);
            points[numMaxRanges].y = r * sin(theta);
            contour->points.push_back(points[numMaxRanges]);
            numMaxRanges++;

        }
        else if (contour->points.size() > 0) {
            //end that contour
            if (contour->points.size() > 2) {
                //clear out middle points
                contour->points[1] = contour->points.back();
                contour->points.resize(2);
            }
            contour = new Contour();
            scan->contours.push_back(contour);
        }
        else if (contour->points.size() == 1) {
            //discard single maxrange beams...
            contour->points.clear();
        }
        theta += thetaStep;
    }

    if (contour->points.size() < 2) {
        scan->contours.pop_back();
        delete contour;
    }

    points = (smPoint*) realloc(points, numMaxRanges * sizeof(smPoint));
    scan->numPoints = numMaxRanges;
    scan->ppoints = (smPoint *) malloc(numMaxRanges * sizeof(smPoint));
    memcpy(scan->ppoints, points, numMaxRanges * sizeof(smPoint));
    return scan;
}



sig_atomic_t still_groovy = 1;

static void sig_action(int signal, siginfo_t *s, void *user)
{
    still_groovy = 0;
}



static void aligned_laser_handler(const bot_core_planar_lidar_t * laser_msg, const sm_rigid_transform_2d_t * odom,
				  int64_t utime, double height, double rp[2], app_t * app)
{
    static int first_scan = 1;

    //compute the distance between this scan and the last one that got added.
    double dist[2];
    sm_vector_sub_2d(odom->pos, app->prev_odom->pos, dist);

    double ld = sm_norm(SMPOINT(dist));
    double ad = sm_angle_subtract(odom->theta, app->prev_odom->theta);
    bool addScan = false;

    if (fabs(ld) > LINEAR_DIST_TO_ADD) {
        //fprintf(stderr, "Scan is %f meters from last add, adding scan\n", ld);
        addScan = true;
    }
    if (fabs(ad) > ANGULAR_DIST_TO_ADD) {
        //fprintf(stderr, "Scan is %f degrees from last add, adding scan\n", ad* 180.0/M_PI);
        addScan = true;
    }

    if(first_scan){
        addScan = true;
        first_scan = 0;
    }

    if (!addScan) {
        if (!app->use_frames && app->publish_pose) {
            Pose2d curr_pose(odom->pos[0], odom->pos[1], odom->theta);
            Pose2d prev_pose(app->prev_odom->pos[0], app->prev_odom->pos[1], app->prev_odom->theta);
            Pose2d prev_curr_tranf = curr_pose.ominus(prev_pose);
            Pose2d curr_slam_pose = app->regionslam->getCurrentPose();
            Pose2d prop_pose = curr_slam_pose.oplus(prev_curr_tranf);

            bot_core_pose_t pose;
            memset(&pose, 0, sizeof(pose));
            pose.pos[0] = prop_pose.x();
            pose.pos[1] = prop_pose.y();
            double rpy[3] = { 0, 0, prop_pose.t() };
            bot_roll_pitch_yaw_to_quat(rpy, pose.orientation);
            pose.utime = laser_msg->utime;
            bot_core_pose_t_publish(app->lcm, "POSE", &pose);
        }
        return;
    }
    sm_tictoc("addScanToGraph");

    static int count = 0;
    static int last_region_no = 0;
    
    if(app->create_regions){
        if(count == 3){
            last_region_no++;
            count = 0;
        }
        count++;
    }


    fprintf(stderr,"a");
    //Project ranges into points, and decimate points so we don't have too many
    smPoint * points = (smPoint *) calloc(laser_msg->nranges, sizeof(smPoint));
    int numValidPoints = sm_projectRangesAndDecimate(app->beam_skip, app->spatialDecimationThresh, laser_msg->ranges,
                                                     laser_msg->nranges, laser_msg->rad0, laser_msg->radstep, points, app->maxRange, app->validBeamAngles[0],
                                                     app->validBeamAngles[1]);
    if (numValidPoints < 30) {
        fprintf(stderr, "WARNING! NOT ENOUGH VALID POINTS! numValid=%d\n", numValidPoints);
        free(points);
        return;
    }
    else {
        points = (smPoint *) realloc(points, numValidPoints * sizeof(smPoint));//crop memory down to size
    }
    
    //adding laser offset
    double sensor_to_body[12];
    if (!bot_frames_get_trans_mat_3x4 (app->frames, app->chan,
                                       "body",
                                       sensor_to_body)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;
    }
    
    //transform to body pose
    double pos_s[3] = {0}, pos_b[3];
    for(int i=0; i < numValidPoints; i++){
        pos_s[0] = points[i].x;
        pos_s[1] = points[i].y;
        bot_vector_affine_transform_3x4_3d (sensor_to_body, pos_s , pos_b);
        points[i].x = pos_b[0];
        points[i].y = pos_b[1];
    }

    ScanTransform T;
    //just use the zero transform... it'll get updated the first time its needed
    memset(&T, 0, sizeof(T));

    Scan * scan;
  
    scan = new Scan(numValidPoints, points, T, SM_HOKUYO_UTM, utime, true);
    Scan * maxRangeScan = get_maxrange_scan(laser_msg->ranges, laser_msg->nranges, laser_msg->rad0, laser_msg->radstep,
                                            app->maxRange, app->maxUsableRange, app->validBeamAngles[0], app->validBeamAngles[1]);
  
    //get the odometry delta
    Pose2d curr_pose(odom->pos[0], odom->pos[1], odom->theta);
    Pose2d prev_pose(app->prev_odom->pos[0], app->prev_odom->pos[1], app->prev_odom->theta);
    Pose2d prev_curr_tranf = curr_pose.ominus(prev_pose);

    if (app->prev_odom != NULL)
        sm_rigid_transform_2d_t_destroy(app->prev_odom);
    app->prev_odom = sm_rigid_transform_2d_t_copy(odom);

    //rotate the cov to body frame
    double Rcov[9];
    sm_rotateCov2D(odom->cov, -odom->theta, Rcov);
    //Matrix cov(3, 3, Rcov);
    Matrix3d cv(Rcov); //= MatrixXd::Random(3,3); //(3,3,Rcov);
    Noise cov = Covariance(cv);
    
    if (!app->scanmatchBeforAdd) {
        app->regionslam->addNodeToSlamAnchor(prev_curr_tranf, cov, scan, maxRangeScan, utime, height, rp, 3, last_region_no);
    }
    else {
        sm_tictoc("IncrementalSMRefineMent");
        //do incremental scan matching to refine the odometry estimate
        Pose2d prev_sm_pose(app->prev_sm_odom->pos[0], app->prev_sm_odom->pos[1], app->prev_sm_odom->theta);
        Pose2d curr_sm_pose = prev_sm_pose.oplus(prev_curr_tranf);
        ScanTransform sm_prior;
        memset(&sm_prior, 0, sizeof(sm_prior));
        sm_prior.x = curr_sm_pose.x();
        sm_prior.y = curr_sm_pose.y();
        sm_prior.theta = curr_sm_pose.t();
        sm_prior.score = app->odometryConfidence; //wide prior
        ScanTransform r = app->sm->matchSuccessive(points, numValidPoints, SM_HOKUYO_UTM, sm_get_utime(), false, &sm_prior);
        curr_sm_pose.set(r.x, r.y, r.theta);
        sm_rigid_transform_2d_t sm_odom;
        memset(&sm_odom, 0, sizeof(sm_odom));
        sm_odom.utime = laser_msg->utime;
        sm_odom.pos[0] = r.x;
        sm_odom.pos[1] = r.y;
        sm_odom.theta = r.theta;
        memcpy(sm_odom.cov, r.sigma, 9 * sizeof(double));
        if (app->prev_sm_odom != NULL)
            sm_rigid_transform_2d_t_destroy(app->prev_sm_odom);
        app->prev_sm_odom = sm_rigid_transform_2d_t_copy(&sm_odom);

        Pose2d prev_curr_sm_tranf = curr_sm_pose.ominus(prev_sm_pose);

        double Rcov_sm[9];
        sm_rotateCov2D(r.sigma, -r.theta, Rcov_sm);
        Matrix3d cv_sm(Rcov_sm); 
        Noise cov_sm = Covariance(1000 * cv_sm);

        double cov_hardcode[9] = { .05, 0, 0, 0, .05, 0, 0, 0, .01 };
        Matrix3d cv_hardcode(cov_hardcode);
        Noise cov_hc = Covariance(1000 * cv_hardcode);

        sm_tictoc("IncrementalSMRefineMent");

        app->regionslam->addNodeToSlamAnchor(prev_curr_sm_tranf, cov_hc, scan, maxRangeScan, utime,                                                                                    height, rp, 3, last_region_no);
    }
    sm_tictoc("addScanToGraph");

    app->regionslam->renderCurrentGridmap();

    app->regionslam->drawGraph();
    //app->regionslam->draw3DPointCloud();
    /*sm_tictoc("publishSlamPos");
      app->isamslam->publishSlamPos();
      sm_tictoc("publishSlamPos");
    */
  
    if (!app->use_frames){
        if(app->publish_pose) {
            Pose2d slam_pose = app->regionslam->getCurrentPose();
            bot_core_pose_t pose;
            memset(&pose, 0, sizeof(pose));
            pose.pos[0] = slam_pose.x();
            pose.pos[1] = slam_pose.y();
            double rpy[3] = { 0, 0, slam_pose.t() };
            bot_roll_pitch_yaw_to_quat(rpy, pose.orientation);
            pose.utime = laser_msg->utime;
            bot_core_pose_t_publish(app->lcm, "POSE", &pose);
        }
    }
    else {
        //publish frame update 
        Pose2d slam_pose = app->regionslam->getCurrentPose();
        bot_core_pose_t pose;
        memset(&pose, 0, sizeof(pose));
        bot_core_pose_t last_pose;
        memset(&last_pose, 0, sizeof(last_pose));
        pose.pos[0] = slam_pose.x();
        pose.pos[1] = slam_pose.y();
        double rpy[3] = { 0, 0, slam_pose.t() };
        bot_roll_pitch_yaw_to_quat(rpy, pose.orientation);
        pose.utime = laser_msg->utime;
        fprintf(stderr, "SLAM Pose : %f,%f,%f\n", pose.pos[0], 
                pose.pos[1],rpy[2]); 

        last_pose.pos[0] = odom->pos[0];
        last_pose.pos[1] = odom->pos[1];
        rpy[2] = odom->theta;
        bot_roll_pitch_yaw_to_quat(rpy, last_pose.orientation);
        last_pose.utime = laser_msg->utime;

        bot_core_rigid_transform_t global_to_local;
        global_to_local.utime = laser_msg->utime;
    
        BotTrans bt_global_to_local;

        // The transformation from GLOBAL to LOCAL is defined as
        // T_global-to-local = T_body-to-local * T_global-to-body
        //                   = T_body-to-local * inv(T_body-to-global)
        BotTrans bt_body_to_global;
        BotTrans bt_global_to_body;
        BotTrans bt_body_to_local;
 
        bot_trans_set_from_quat_trans (&bt_body_to_global, pose.orientation, pose.pos);
        bot_trans_set_from_quat_trans (&bt_body_to_local, last_pose.orientation, last_pose.pos);
        bot_trans_invert (&bt_body_to_global);
        bot_trans_copy (&bt_global_to_body, &bt_body_to_global);
        bot_trans_apply_trans_to (&bt_body_to_local, &bt_global_to_body, &bt_global_to_local);
    
        memcpy (global_to_local.trans, bt_global_to_local.trans_vec, 3*sizeof(double));
        memcpy (global_to_local.quat, bt_global_to_local.rot_quat, 4*sizeof(double));

        bot_core_rigid_transform_t_publish (app->lcm, "GLOBAL_TO_LOCAL", &global_to_local);
    }

    return;
}


void app_destroy(app_t *app)
{
    //TODO: there is a double free mess cuz scan matcher free's scans that are held onto elsewhere :-/
    //LEAK everything for now!!!

    //  if (app) {
    //    if (app->chan)
    //      free(app->chan);
    //
    //    lcm_destroy(app->lcm);
    //    if (app->prev_odom)
    //      botlcm_rigid_transform_2d_t_destroy(app->prev_odom);
    //
    //    if (app->isamslam)
    //      delete app->isamslam;
    //    fprintf(stderr, "deleted isamSlam object\n");
    //    if (app->sm)
    //      delete app->sm;
    //    if (app->sm_incremental)
    //      delete app->sm_incremental;
    //
    //    free(app);
    //  }

    // dump timing stats
    sm_tictoc(NULL);
    exit (1);

}

void
circ_free_lidar_data(void *user, void *p) {
    bot_core_planar_lidar_t *np = (bot_core_planar_lidar_t *) p; 
    bot_core_planar_lidar_t_destroy(np);
}


void process_laser(bot_core_planar_lidar_t *flaser, app_t *app, int64_t utime){
    if(app->useOdom){
        sm_rigid_transform_2d_t odom;
        memset(&odom, 0, sizeof(odom));

        double rpy[3];
        
        double cov[9] = { 0 };
        cov[0] = 1;
        cov[1] = 0;
        cov[3] = 0;
        cov[4] = 1;
        cov[8] = 0.1;
        
        if(!app->use_frames){
            if(app->last_deadreakon != NULL){
                //use the odom msg
                memcpy(odom.pos, app->last_deadreakon->pos, 2 * sizeof(double));
                bot_quat_to_roll_pitch_yaw(app->last_deadreakon->orientation, rpy);
            }
            else{
                return;
            }
        }
        else{ 
            int64_t frame_utime;
            int status = bot_frames_get_latest_timestamp(app->frames, "body",
                                                         "local",&frame_utime);

            if(fabs(frame_utime - utime) / 1.0e6 > 1.0){
                fprintf(stderr, "Frames hasn't been updated yet - retruning\n");
                return;
            }

            double body_to_local[12];
            if (!bot_frames_get_trans_mat_3x4_with_utime (app->frames, "body",
                                                          "local",  flaser->utime,
                                                          body_to_local)) {
                fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
                return;        
            }
            double pose_b[3] = {.0}, pose_l[3];
            bot_vector_affine_transform_3x4_3d (body_to_local, 
                                                pose_b, 
                                                pose_l);
            odom.pos[0] = pose_l[0];
            odom.pos[1] = pose_l[1];

            BotTrans body_to_local_t;

            if (!bot_frames_get_trans_with_utime (app->frames, "body", "local", flaser->utime, 
                                                  &body_to_local_t)){
                fprintf (stderr, "frame error\n");
                return;
            }
            
            double rpy_b[3] = {0};
            double quat_b[4], quat_l[4];
            bot_roll_pitch_yaw_to_quat (rpy_b, quat_b);
            
            bot_quat_mult (quat_l, body_to_local_t.rot_quat, quat_b);
            bot_quat_to_roll_pitch_yaw (quat_l, rpy);
        }

        odom.theta = rpy[2];
        //cov was published as body frame already... rotate to global for now
        double Rcov[9];
        sm_rotateCov2D(cov, odom.theta, odom.cov);

        static int64_t utime_prev = flaser->utime;

        sm_tictoc("aligned_laser_handler");
        aligned_laser_handler(flaser, &odom, flaser->utime, 0 , rpy, app);
        sm_tictoc("aligned_laser_handler");
    }
    else{
        smPoint * points = (smPoint *) calloc(flaser->nranges, sizeof(smPoint));
        int numValidPoints = sm_projectRangesAndDecimate(app->beam_skip, app->spatialDecimationThresh, flaser->ranges,
                                                         flaser->nranges, flaser->rad0, flaser->radstep, points, app->maxRange, app->validBeamAngles[0], app->validBeamAngles[1]);
        if (numValidPoints < 30) {
            fprintf(stderr, "WARNING! NOT ENOUGH VALID POINTS! numValid=%d\n", numValidPoints);
            return;
        }

        double sensor_to_body[12];
        if (!bot_frames_get_trans_mat_3x4 (app->frames, app->chan,
                                           "body",
                                           sensor_to_body)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            return;
        }

        //transform to body pose
        double pos_s[3] = {0}, pos_b[3];
        for(int i=0; i < numValidPoints; i++){
            pos_s[0] = points[i].x;
            pos_s[1] = points[i].y;
            bot_vector_affine_transform_3x4_3d (sensor_to_body, pos_s , pos_b);
            points[i].x = pos_b[0];
            points[i].y = pos_b[1];
        }
        
        ////////////////////////////////////////////////////////////////////
        //Actually do the matching
        ////////////////////////////////////////////////////////////////////
        ScanTransform r = app->sm_incremental->matchSuccessive(points, numValidPoints, SM_HOKUYO_UTM, sm_get_utime(), NULL); //don't have a better estimate than prev, so just set prior to NULL

        sm_rigid_transform_2d_t odom;
        memset(&odom, 0, sizeof(odom));
        odom.utime = flaser->utime;
        odom.pos[0] = r.x;
        odom.pos[1] = r.y;
        odom.theta = r.theta;
        memcpy(odom.cov, r.sigma, 9 * sizeof(double));

        //  app->sm_incremental->drawGUI(points, numValidPoints, r, NULL,"ScanMatcher");
        
        free(points);
        sm_tictoc("laser_handler");
        double rp[2] = { 0, 0 };//call the slam/loop closing handler
        aligned_laser_handler(flaser, &odom, flaser->utime, 0, rp, app);
    }
}

////////////////////////////////////////////////////////////////////
//where all the work is done
////////////////////////////////////////////////////////////////////
static void laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const bot_core_planar_lidar_t * msg,
			  void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    
    if(app->slave_mode)
        bot_ptr_circular_add (app->front_laser_circ,  bot_core_planar_lidar_t_copy(msg));
    else{
        process_laser( (bot_core_planar_lidar_t *) msg,app, msg->utime);
    }
    
    static int64_t utime_prev = msg->utime;
}

static void usage(const char *name)
{
    fprintf(stderr, "usage: %s [options]\n"
            "\n"
            "  -h, --help                      Shows this help text and exits\n"
            "  -a,  --aligned                  Subscribe to aligned_laser (ROBOT_LASER) msgs\n"
            "  -c, --chan <LCM CHANNEL>        Input lcm channel default:\"LASER\" or \"ROBOT_LASER\"\n"
            "  -s, --scanmatch                 Run Incremental scan matcher every time a node gets added to map\n"
            "  -d, --draw                      Show window with scan matches \n"
            "  -p, --publish_pose              publish POSE messages\n"
            "  -v, --verbose                   Be verbose\n"
            "  -m, --mode  \"HOKUYO_UTM\"|\"SICK\" configures low-level options.\n"
            "\n"
            "Low-level laser options:\n"
            "  -A, --mask <min,max>            Mask min max angles in (radians)\n"
            "  -B, --beamskip <n>              Skip every n beams \n"
            "  -D, --decimation <value>        Spatial decimation threshold (meters?)\n"
            "  -F                              Use frames to align laser scans\n"
            "  -R                              Use regions\n"
            "  -S                              Use Slave mode - pose additions will be triggered by external source\n"
            "  -M, --range <range>             Maximum range (meters)\n", name);
}

int main(int argc, char *argv[])
{
    setlinebuf(stdout);

    app_t *app = (app_t *) calloc(1, sizeof(app_t));
    app->prev_odom = (sm_rigid_transform_2d_t *) calloc(1, sizeof(sm_rigid_transform_2d_t)); //set initial prev_odom to zero...
    app->prev_sm_odom = (sm_rigid_transform_2d_t *) calloc(1, sizeof(sm_rigid_transform_2d_t)); //set initial prev_odom to zero...
    memset(app->prev_odom, 0, sizeof(sm_rigid_transform_2d_t));
    memset(app->prev_sm_odom, 0, sizeof(sm_rigid_transform_2d_t));
    
    int draw_scanmatch = 0;
    bool alignLaser = false;
    app->chan = NULL;
    app->verbose = 0;
    app->publish_pose = 0;
    app->scanmatchBeforAdd = 0;
    app->odometryConfidence = LINEAR_DIST_TO_ADD;

    app->useOdom = 0;
    // set to default values
    app->validBeamAngles[0] = -100;
    app->validBeamAngles[1] = +100;
    app->beam_skip = 3;
    app->spatialDecimationThresh = .2;
    app->maxRange = 29.7;
    app->maxUsableRange = 8;

    /* LCM */
    app->lcm = lcm_create(NULL);
    if (!app->lcm) {
        fprintf(stderr, "ERROR: lcm_create() failed\n");
        return 1;
    }   
   
    const char *optstring = "hr:c:dafvm:M:B:CD:M:pn:s::FSR";
    char c;
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
                                  { "chan", required_argument, 0, 'c' },
                                  { "draw", no_argument, 0,'d' },
                                  { "mode", required_argument, 0, 'm' },
                                  { "Frames", required_argument, 0, 'F' }, //uses frames to extract odom 
                                  { "range", required_argument, 0, 'M' },
                                  { "beamskip", required_argument, 0,'B' },
                                  { "regions", required_argument, 0,'R' },
                                  { "decimation", required_argument, 0, 'D' },                                  { "countour", required_argument, 0, 'C' },  //draw contour map
                                  { "publish_pose", no_argument, 0, 'p' },
                                  { "mask", required_argument,   0, 'A' },
                                  { "aligned", no_argument, 0, 'a' },
                                  { "scanmatch", optional_argument, 0, 's' },
                                  { "verbose", no_argument, 0,'v' },
                                  { 0, 0, 0, 0 } };

    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
        case 'c':
            free(app->chan);
            app->chan = strdup(optarg);
            fprintf(stderr,"Main Laser Channel : %s\n", app->chan);
            break;
        case 'F':
            app->use_frames = 1;
            break;
        case 'R':
            app->create_regions = 1;
            break;
        case 'd':
            draw_scanmatch = 1;
            break;
        case 'v':
            app->verbose = 1;
            break;
        case 'p':
            app->publish_pose = 1;
            break;
        case 's':
            app->scanmatchBeforAdd = 1;
            if (optarg != NULL) {
                app->odometryConfidence = atof(optarg);
            }
            break;
        case 'M':
            app->maxRange = strtod(optarg, 0);
            break;
        case 'B':
            app->beam_skip = atoi(optarg);
            break;
        case 'D':
            app->spatialDecimationThresh = strtod(optarg, 0);
            break;
        case 'A':
            sscanf(optarg, "%f,%f", &app->validBeamAngles[0], &app->validBeamAngles[1]);
            break;
        case 'a':
            alignLaser = true;
            app->useOdom = 1;
            break;
        case 'h':
        default:
            usage(argv[0]);
            return 1;
        }
    }
    
    app->param = bot_param_new_from_server(app->lcm, 1);
    
    app->frames = bot_frames_get_global (app->lcm, app->param);
    
    if (app->chan == NULL) {
        if (alignLaser)
            app->chan = strdup("SKIRT_FRONT");
        else
            app->chan = strdup("LASER");
    }

    if (app->verbose) {
        if (alignLaser)
            printf("INFO: Listening for robot_laser msgs on %s\n", app->chan);
        else
            printf("INFO: Listening for laser msgs on %s\n", app->chan);

        printf("INFO: publish_pose:%d\n", app->publish_pose);
        printf("INFO: Max Range:%lf\n", app->maxRange);
        printf("INFO: SpatialDecimationThresh:%lf\n", app->spatialDecimationThresh);
        printf("INFO: Beam Skip:%d\n", app->beam_skip);
        printf("INFO: validRange:%f,%f\n", app->validBeamAngles[0], app->validBeamAngles[1]);
    }

    //hardcoded loop closing scan matcher params
    double metersPerPixel_lc = .02; //translational resolution for the brute force search
    double thetaResolution_lc = .01; //angular step size for the brute force search
    int useGradientAscentPolish_lc = 1; //use gradient descent to improve estimate after brute force search
    int useMultires_lc = 3; // low resolution will have resolution metersPerPixel * 2^useMultiRes
    bool useThreads_lc = true;

    //hardcoded scan matcher params
    double metersPerPixel = .02; //translational resolution for the brute force search
    double thetaResolution = .02; //angular step size for the brute force search

    sm_incremental_matching_modes_t matchingMode = SM_GRID_COORD; //use gradient descent to improve estimate after brute force search
    int useMultires = 3; // low resolution will have resolution metersPerPixel * 2^useMultiRes

    double initialSearchRangeXY = .15; //nominal range that will be searched over
    double initialSearchRangeTheta = .1;

    //SHOULD be set greater than the initialSearchRange
    double maxSearchRangeXY = .3; //if a good match isn't found I'll expand and try again up to this size...
    double maxSearchRangeTheta = .2; //if a good match isn't found I'll expand and try again up to this size...

    int maxNumScans = 30; //keep around this many scans in the history
    double addScanHitThresh = .90; //add a new scan to the map when the number of "hits" drops below this

    int useThreads = 1;

    //create the actual scan matcher object
    app->regionslam = new RegionSlam(app->lcm, metersPerPixel_lc, 
                                     thetaResolution_lc, useGradientAscentPolish_lc, 
                                     useMultires_lc,
                                     draw_scanmatch , useThreads_lc, app->frames);
  

    //create the incremental scan matcher object
    app->sm_incremental = new ScanMatcher(metersPerPixel, thetaResolution, useMultires, useThreads, false);
    //set the scan matcher to start at pi/2...
    ScanTransform startPose;
    memset(&startPose, 0, sizeof(startPose));
    app->sm_incremental->initSuccessiveMatchingParams(maxNumScans, initialSearchRangeXY, maxSearchRangeXY,
                                                      initialSearchRangeTheta, maxSearchRangeTheta, matchingMode, addScanHitThresh, false, .3, &startPose);

    //create the SLAM scan matcher object
    app->sm = new ScanMatcher(metersPerPixel, thetaResolution, useMultires, useThreads, false);

    //set the scan matcher to start at pi/2...
    app->sm->initSuccessiveMatchingParams(maxNumScans, LINEAR_DIST_TO_ADD, LINEAR_DIST_TO_ADD, ANGULAR_DIST_TO_ADD,
                                          ANGULAR_DIST_TO_ADD, matchingMode, addScanHitThresh, false, .3, &startPose);

    //set the scan matcher to start at pi/2... cuz it looks better
    memset(&app->sm->currentPose, 0, sizeof(app->sm->currentPose));
    app->sm->currentPose.theta = M_PI / 2;
    app->sm->prevPose = app->sm->currentPose;
    app->r_laser_msg = NULL;
    app->r_utime = 0;

    app->front_laser_circ = bot_ptr_circular_new (LASER_DATA_CIRC_SIZE,
                                                  circ_free_lidar_data, app);
    
    app->rear_laser_circ = bot_ptr_circular_new (LASER_DATA_CIRC_SIZE,
                                                 circ_free_lidar_data, app);
    if (app->sm->isUsingIPP())
        fprintf(stderr, "Using IPP\n");
    else
        fprintf(stderr, "NOT using IPP\n");

    //if (!alignLaser){
    bot_core_planar_lidar_t_subscribe(app->lcm, app->chan, laser_handler, app);
    /*}
      else{
      if(!app->use_frames){
      fprintf(stderr,"Subscribing to Robot deadreackon channel\n");            
      //bot_core_pose_t_subscribe (app->lcm, "ROBOT_DEADRECKON", on_deadreckon_pose, app);
      }
      bot_core_planar_lidar_t_subscribe(app->lcm, app->chan, laser_handler, app);
      }*/
    
    // setup sigaction();
    struct sigaction new_action;
    new_action.sa_sigaction = sig_action;
    sigemptyset(&new_action.sa_mask);
    new_action.sa_flags = 0;

    sigaction(SIGINT, &new_action, NULL);
    sigaction(SIGTERM, &new_action, NULL);
    sigaction(SIGKILL, &new_action, NULL);
    sigaction(SIGHUP, &new_action, NULL);

    /* sit and wait for messages */
    while (still_groovy)
        lcm_handle(app->lcm);

    app_destroy(app);

    return 0;
}
