#include "VoxelMap.hpp"
#include <math.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>

//#include <ipp.h>
#include <opencv/cv.h>

using namespace std;

////////////////////////////////////////////////////////////////////////////
//VoxelMapReader
////////////////////////////////////////////////////////////////////////////
VoxelMapReader::~VoxelMapReader()
{
  if (likelihoods != NULL)
    free(likelihoods);
  if (vox_map_msg != NULL)
    erlcm_voxel_map_t_destroy(vox_map_msg);
}

VoxelMapReader::VoxelMapReader(const erlcm_voxel_map_t * msg) :
  vox_map_msg(NULL)
{
  memcpy(xyz0, msg->xyz0, 3 * sizeof(double));
  memcpy(xyz1, msg->xyz1, 3 * sizeof(double));
  memcpy(metersPerPixel, msg->mpp, 3 * sizeof(double));
  memcpy(dimensions, msg->dimensions, 3 * sizeof(int));

  num_cells = 1;
  for (int i = 0; i < 3; i++)
    num_cells *= dimensions[i];
  uLong uncompressed_size = num_cells * sizeof(float);
  likelihoods = (float *) malloc(uncompressed_size);

  if (msg->compressed) {
    uLong uncompress_size_result = uncompressed_size;
    uLong uncompress_return = uncompress((Bytef *) likelihoods, (uLong *) &uncompress_size_result,
        (Bytef *) msg->mapData, (uLong) msg->datasize);
    if (uncompress_return != Z_OK || uncompress_size_result != uncompressed_size) {
      fprintf(stderr, "ERROR uncompressing the map, ret = %lu", uncompress_return);
      exit(1);
    }

  }
  else {
    assert(msg->datasize == (int)uncompressed_size);
    memcpy(likelihoods, msg->mapData, num_cells * sizeof(float));
  }
}

VoxelMapReader::VoxelMapReader(const VoxelMap * voxmap) :
  vox_map_msg(NULL)
{
  likelihoods = NULL;
  set(voxmap);
}

void VoxelMapReader::set(const VoxelMap * voxmap, int blur)
{
  memcpy(xyz0, voxmap->xyz0, 3 * sizeof(double));
  memcpy(xyz1, voxmap->xyz1, 3 * sizeof(double));
  memcpy(metersPerPixel, voxmap->metersPerPixel, 3 * sizeof(double));
  memcpy(dimensions, voxmap->dimensions, 3 * sizeof(int));

  num_cells = 1;
  for (int i = 0; i < 3; i++)
    num_cells *= dimensions[i];
  uLong uncompressed_size = num_cells * sizeof(float);
  likelihoods = (float *) realloc(likelihoods, uncompressed_size);

  const float * l;
  if (blur > 0)
    l = voxmap->getBlurredFloatMap(blur);
  else
    l = voxmap->getFloatMap();
  memcpy(likelihoods, l, num_cells * sizeof(float));
}

////////////////////////////////////////////////////////////////////////////
//VoxelMap
////////////////////////////////////////////////////////////////////////////

VoxelMap::~VoxelMap()
{
  free(hits);
  free(visits);
  if (likelihoods != NULL)
    free(likelihoods);
  if (vox_map_msg != NULL)
    erlcm_voxel_map_t_destroy(vox_map_msg);
}

VoxelMap::VoxelMap(double _xyz0[3], double _xyz1[3], double _metersPerPixel[3], uint8_t _maxEvidence) :
  maxEvidence(_maxEvidence), vox_map_msg(NULL)
{
  memcpy(xyz0, _xyz0, 3 * sizeof(double));
  memcpy(xyz1, _xyz1, 3 * sizeof(double));
  memcpy(metersPerPixel, _metersPerPixel, 3 * sizeof(double));

  num_cells = 1;
  for (int i = 0; i < 3; i++) {
    dimensions[i] = ceil((1.0 / metersPerPixel[i]) * (xyz1[i] - xyz0[i]));
    num_cells *= dimensions[i];
  }

  hits = (uint8_t *) calloc(num_cells, sizeof(uint8_t));
  visits = (uint8_t *) calloc(num_cells, sizeof(uint8_t));
  likelihoods = (float *) calloc(num_cells, sizeof(float));
}

void VoxelMap::reset()
{
  memset(hits, 0, num_cells * sizeof(uint8_t));
  memset(visits, 0, num_cells * sizeof(uint8_t));
  memset(likelihoods, 0, num_cells * sizeof(float));
}

void VoxelMap::raytrace(int origin[3], int endpoint[3], int hit_inc, int visit_inc, bool updateBorder)
{
  //3D Bresenham implimentation copied from:
  //http://www.cit.griffith.edu.au/~anthony/info/graphics/bresenham.procs
  //

  int x1, y1, z1, x2, y2, z2;
  x1 = origin[0];
  y1 = origin[1];
  z1 = origin[2];
  x2 = endpoint[0];
  y2 = endpoint[1];
  z2 = endpoint[2];
  int i, dx, dy, dz, l, m, n, x_inc, y_inc, z_inc, err_1, err_2, dx2, dy2, dz2;
  int voxel[3];

  voxel[0] = x1;
  voxel[1] = y1;
  voxel[2] = z1;
  dx = x2 - x1;
  dy = y2 - y1;
  dz = z2 - z1;
  x_inc = (dx < 0) ? -1 : 1;
  l = abs(dx);
  y_inc = (dy < 0) ? -1 : 1;
  m = abs(dy);
  z_inc = (dz < 0) ? -1 : 1;
  n = abs(dz);
  dx2 = l << 1;
  dy2 = m << 1;
  dz2 = n << 1;

  if ((l >= m) && (l >= n)) {
    err_1 = dy2 - l;
    err_2 = dz2 - l;
    for (i = 0; i < l; i++) {
      updateValue(voxel, 0, visit_inc, updateBorder);
      if (err_1 > 0) {
        voxel[1] += y_inc;
        err_1 -= dx2;
      }
      if (err_2 > 0) {
        voxel[2] += z_inc;
        err_2 -= dx2;
      }
      err_1 += dy2;
      err_2 += dz2;
      voxel[0] += x_inc;
    }
  }
  else if ((m >= l) && (m >= n)) {
    err_1 = dx2 - m;
    err_2 = dz2 - m;
    for (i = 0; i < m; i++) {
      updateValue(voxel, 0, visit_inc, updateBorder);
      if (err_1 > 0) {
        voxel[0] += x_inc;
        err_1 -= dy2;
      }
      if (err_2 > 0) {
        voxel[2] += z_inc;
        err_2 -= dy2;
      }
      err_1 += dx2;
      err_2 += dz2;
      voxel[1] += y_inc;
    }
  }
  else {
    err_1 = dy2 - n;
    err_2 = dx2 - n;
    for (i = 0; i < n; i++) {
      updateValue(voxel, 0, visit_inc, updateBorder);

      if (err_1 > 0) {
        voxel[1] += y_inc;
        err_1 -= dz2;
      }
      if (err_2 > 0) {
        voxel[0] += x_inc;
        err_2 -= dz2;
      }
      err_1 += dy2;
      err_2 += dx2;
      voxel[2] += z_inc;
    }
  }
  updateValue(voxel, hit_inc, visit_inc, updateBorder);
}

void VoxelMap::raytrace(double origin[3], double endpoint[3], int hit_inc, int visit_inc, bool updateBorder)
{
  int iorigin[3];
  int iendpoint[3];
  worldToTable(origin, iorigin);
  worldToTable(endpoint, iendpoint);
  raytrace(iorigin, iendpoint, hit_inc, visit_inc, updateBorder);
}

const float * VoxelMap::getFloatMap() const
{
  int ixyz[3];
  for (ixyz[2] = 0; ixyz[2] < dimensions[2]; ixyz[2]++) {
    for (ixyz[1] = 0; ixyz[1] < dimensions[1]; ixyz[1]++) {
      for (ixyz[0] = 0; ixyz[0] < dimensions[0]; ixyz[0]++) {
        likelihoods[getInd(ixyz)] = readValue(ixyz);
      }
    }
  }
  return likelihoods;
}

const float * VoxelMap::getBlurredFloatMap(int kernel_size) const
{
  int kernel_size_2 = kernel_size / 2;
  getFloatMap();
  int ixyz[3] = { 0 };
  for (ixyz[2] = kernel_size_2; ixyz[2] < dimensions[2] - kernel_size_2 - 1; ixyz[2]++) {
    CvMat m = cvMat(dimensions[1], dimensions[0], CV_32FC1, &likelihoods[getInd(ixyz)]);
    cvSmooth(&m, &m, CV_GAUSSIAN, kernel_size, kernel_size);
  }

  //  int ixyz[3];
  //  int ixyzTmp[3];
  //  int kernel_size_2 = kernel_size/2;
  //  double normalizer = kernel_size * kernel_size * kernel_size;
  //  for (ixyz[2] = kernel_size_2; ixyz[2] < dimensions[2] - kernel_size_2 - 1; ixyz[2]++) {
  //    for (ixyz[1] = kernel_size_2; ixyz[1] < dimensions[1] - kernel_size_2 - 1; ixyz[1]++) {
  //      for (ixyz[0] = kernel_size_2; ixyz[0] < dimensions[0] - kernel_size_2 - 1; ixyz[0]++) {
  //
  //        float like = 0;
  //        int count = 0;
  //        for (ixyzTmp[2] = ixyz[2] - kernel_size_2; ixyzTmp[2] <= ixyz[2] + kernel_size_2; ixyzTmp[2]++) {
  //          for (ixyzTmp[1] = ixyz[1] - kernel_size_2; ixyzTmp[1] <= ixyz[1] + kernel_size_2; ixyzTmp[1]++) {
  //            for (ixyzTmp[0] = ixyz[0] - kernel_size_2; ixyzTmp[0] <= ixyz[0] + kernel_size_2; ixyzTmp[0]++) {
  //              float l = readValue(ixyzTmp);
  //              if (l >= 0) {
  //                count++;
  //                like += l;
  //              }
  //              else
  //                like += .5;
  //            }
  //          }
  //        }
  //        if (count > 0) {
  //          float l = like / (float) normalizer;
  //          likelihoods[getInd(ixyz)] = l;
  //        }
  //        else
  //          likelihoods[getInd(ixyz)] = .5;
  //      }
  //    }
  //  }

  //    int anchor = kernel_size >> 1;
  //    IpprVolume dstVolume = { dimensions[0], dimensions[1], dimensions[2] };
  //    IpprVolume kernelVolume = { kernel_size, kernel_size, kernel_size };
  //    IpprPoint ippanchor = { anchor, anchor, anchor };
  //    int BufferSize, sstep, dstep;
  //    Ipp8u *pBuffer;
  //
  //    /* all kernel values are 1 */
  //    int kernelSum = 0;
  //    Ipp32s pKernel[kernel_size * kernel_size * kernel_size];
  //    for (int k = 0; k < kernel_size; k++)
  //      for (int j = 0; j < kernel_size * kernel_size; j++) {
  //        pKernel[k * (kernel_size * kernel_size) + j] = 1;
  //        kernelSum += pKernel[k * (kernel_size * kernel_size) + j];
  //      }
  //
  //    const Ipp16s *stp[dimensions[2] + kernel_size - 1], *st[dimensions[2] + kernel_size - 1], *dt[dimensions[2]];
  //    Ipp16s *s, *d, *stt, *dtt;
  //
  //    /* dst volume has to be zeroed */
  //    for (int k = 0; k < dimensions[2]; k++)
  //      dt[k] = ippiMalloc_16s_C1(dimensions[0], dimensions[1], &dstep);
  //    for (int k = 0; k < dimensions[2]; k++) {
  //      d = (Ipp16s*) dt[k];
  //      for (int i = 0; i < dimensions[1]; i++) {
  //        dtt = d;
  //        for (int j = 0; j < dimensions[0]; j++)
  //          dtt[j] = 0;
  //        d = (Ipp16s*) ((Ipp8u*) d + dstep);
  //      }
  //    }
  //
  //    /* src volume has to be initialized */
  //    int FIXED_POINT_SCALER = 128;
  //    for (int k = 0; k < dimensions[2] + kernel_size - 1; k++) {
  //      stp[k] = ippiMalloc_16s_C1(dimensions[0] + kernel_size - 1, dimensions[1] + kernel_size - 1, &sstep);
  //      st[k] = (Ipp16s*) ((Ipp8u*) stp[k] + sstep * anchor) + anchor;
  //    }
  //    for (int k = 0; k < dimensions[2] + kernel_size - 1; k++) {
  //      s = (Ipp16s*) stp[k];
  //      for (int i = 0; i < dimensions[1] + kernel_size - 1; i++) {
  //        stt = s;
  //        for (int j = 0; j < dimensions[0] + kernel_size - 1; j++) {
  //          if (i >= anchor && j >= anchor && k >= anchor && i < dimensions[1] + anchor && j < dimensions[0] + anchor && k
  //              < dimensions[2] + anchor) {
  //            int ixyz[3] = { j - anchor, i - anchor, k - anchor };
  //            stt[j] = (Ipp16s) FIXED_POINT_SCALER * readValue(ixyz);
  //          }
  //          else
  //            stt[j] = FIXED_POINT_SCALER / 2;
  //        }
  //        s = (Ipp16s*) ((Ipp8u*) s + sstep);
  //      }
  //    }
  //
  //    ipprFilterGetBufSize(dstVolume, kernelVolume, 1, &BufferSize);
  //    pBuffer = ippsMalloc_8u(BufferSize);
  //
  //    ipprFilter_16s_C1PV(&st[anchor], sstep, dt, dstep, dstVolume, pKernel, kernelVolume, ippanchor, 1, pBuffer);
  //
  //    for (int k = 0; k < dimensions[2]; k++) {
  //      d = (Ipp16s*) dt[k];
  //      for (int i = 0; i < dimensions[1]; i++) {
  //        dtt = d;
  //        for (int j = 0; j < dimensions[0]; j++) {
  //          int ixyz[3] = { j, i, k };
  //          likelihoods[getInd(ixyz)] = (float) dtt[j] / (float) (FIXED_POINT_SCALER * kernelSum);
  //        }
  //        d = (Ipp16s*) ((Ipp8u*) d + dstep);
  //      }
  //    }
  //
  //    if (pBuffer)
  //      ippsFree(pBuffer);
  //    for (int k = 0; k < dimensions[2] + kernel_size - 1; k++)
  //      ippiFree((void*) stp[k]);
  //    for (int k = 0; k < dimensions[2]; k++)
  //      ippiFree((void*) dt[k]);

  return likelihoods;
}

erlcm_voxel_map_t * VoxelMap::get_voxel_map_t(int blur)
{
  if (vox_map_msg == NULL)
    vox_map_msg = (erlcm_voxel_map_t *) calloc(1, sizeof(erlcm_voxel_map_t));

  memcpy(vox_map_msg->xyz0, xyz0, 3 * sizeof(double));
  memcpy(vox_map_msg->xyz1, xyz1, 3 * sizeof(double));
  memcpy(vox_map_msg->mpp, metersPerPixel, 3 * sizeof(double));
  memcpy(vox_map_msg->dimensions, dimensions, 3 * sizeof(int));

  uLong uncompressed_size = num_cells * sizeof(float);
  const float * complete_map;
  if (blur > 0)
    complete_map = getBlurredFloatMap(blur);
  else
    complete_map = getFloatMap();

  uLong compress_buf_size = uncompressed_size * 1.01 + 12; //with extra space for zlib
  vox_map_msg->mapData = (uint8_t *) realloc(vox_map_msg->mapData, compress_buf_size);
  int compress_return = compress2((Bytef *) vox_map_msg->mapData, &compress_buf_size, (Bytef *) complete_map,
      uncompressed_size, Z_BEST_SPEED);
  if (compress_return != Z_OK) {
    fprintf(stderr, "ERROR: Could not compress voxel map!\n");
    exit(1);
  }
  fprintf(stderr, "uncompressed_size=%ld compressed_size=%ld\n", uncompressed_size, compress_buf_size);
  vox_map_msg->datasize = compress_buf_size;
  vox_map_msg->compressed = 1;
  vox_map_msg->utime = -1;

  return vox_map_msg;
}

erlcm_voxel_map_t * VoxelMapReader::get_voxel_map_t()
{
  if (vox_map_msg == NULL)
    vox_map_msg = (erlcm_voxel_map_t *) calloc(1, sizeof(erlcm_voxel_map_t));

  memcpy(vox_map_msg->xyz0, xyz0, 3 * sizeof(double));
  memcpy(vox_map_msg->xyz1, xyz1, 3 * sizeof(double));
  memcpy(vox_map_msg->mpp, metersPerPixel, 3 * sizeof(double));
  memcpy(vox_map_msg->dimensions, dimensions, 3 * sizeof(int));

  uLong uncompressed_size = num_cells * sizeof(float);
  const float * complete_map;
  complete_map = getFloatMap();

  uLong compress_buf_size = uncompressed_size * 1.01 + 12; //with extra space for zlib
  vox_map_msg->mapData = (uint8_t *) realloc(vox_map_msg->mapData, compress_buf_size);
  int compress_return = compress2((Bytef *) vox_map_msg->mapData, &compress_buf_size, (Bytef *) complete_map,
      uncompressed_size, Z_BEST_SPEED);
  if (compress_return != Z_OK) {
    fprintf(stderr, "ERROR: Could not compress voxel map!\n");
    exit(1);
  }
  fprintf(stderr, "uncompressed_size=%ld compressed_size=%ld\n", uncompressed_size, compress_buf_size);
  vox_map_msg->datasize = compress_buf_size;
  vox_map_msg->compressed = 1;
  vox_map_msg->utime = -1;

  return vox_map_msg;
}
