#ifndef GRIDMAP_H_
#define GRIDMAP_H_

#include <vector>
#include <string.h>
//#include <common3d/agile-math_util.h>
//#include <common3d_utils/math_util.h>
//#include <lcmtypes/wheelchair3d_lcmtypes.h>
#include <bot_core/bot_core.h>
#include <lcmtypes/er_lcmtypes.h>
#include <interfaces/map3d_interface.h>

using namespace std;
class GridMap {
public:
  GridMap(double xy0[2], double xy1[2], double mPP, uint8_t _maxEvidence = 64);
  virtual ~GridMap();

  double xy0[2], xy1[2];
  double metersPerPixel;

  int width, height;

  int numCells;
  uint8_t * hits;
  uint8_t * full_hits;
  uint8_t * visits;
  uint8_t maxEvidence;//cap the number of hits/visits allow to prevent overflow, and make the map react to changes faster
  float * likelihoods;
  erlcm_gridmap_t* msg;

  const float * getFloatMap();

  inline int getInd(const int ixy[2])
  {
    return ixy[1] * width + ixy[0];
  }

  static inline int iclamp(int v, int minv, int maxv)
  {
    return bot_max(minv, bot_min(v, maxv));
  }

  inline void worldToTable(const double xy[2], int ixy[2])
  {
    ixy[0] = iclamp(round((xy[0] - xy0[0]) / metersPerPixel), 0, width - 1);
    ixy[1] = iclamp(round((xy[1] - xy0[1]) / metersPerPixel), 0, height - 1);
  }

  inline void clearValue(int ixy[2])
  {
    if((ixy[1] >= 0 && ixy[1] < height) && (ixy[0] >= 0 && ixy[0] < width)){
      likelihoods[ixy[0] * height + ixy[1]] = 0.0;
      //likelihoods[ixy[0] * height + ixy[1]] = 0.0;//MAP_FREE_VALUE;   
      //the following might be a way to detect doors 
      /*if(likelihoods[ixy[0] * height + ixy[1]] > 0.2)
	likelihoods[ixy[0] * height + ixy[1]] = MAP_UNEXPLORED_VALUE;
      else
      likelihoods[ixy[0] * height + ixy[1]] = MAP_FREE_VALUE;*/   
    }
  }

  inline float getLikelihoodValue(int ixy[2])
  {
    if((ixy[1] >= 0 && ixy[1] < height) && (ixy[0] >= 0 && ixy[0] < width)){
      return likelihoods[ixy[0] * height + ixy[1]];
      //likelihoods[ixy[0] * height + ixy[1]] = 0.0;//MAP_FREE_VALUE;   
      //the following might be a way to detect doors 
      /*if(likelihoods[ixy[0] * height + ixy[1]] > 0.2)
	likelihoods[ixy[0] * height + ixy[1]] = MAP_UNEXPLORED_VALUE;
      else
      likelihoods[ixy[0] * height + ixy[1]] = MAP_FREE_VALUE;*/   
    }
    else{
      return -1;
    }
  }


  inline void tableToWorld(const int ixy[2], double xy[2])
  {
    //    *xy[0] = ((double)ixy[0]+0.5) * metersPerPixel + xy0[0]; //+.5 puts it in the center of the cell
    //    *xy[1] = ((double)ixy[1]+0.5) * metersPerPixel + xy0[1];
    xy[0] = ((double) ixy[0]) * metersPerPixel + xy0[0];
    xy[1] = ((double) ixy[1]) * metersPerPixel + xy0[1];

  }

  inline float readValue(const int ixy[2])
  {
    int ind = getInd(ixy);
    if (!visits[ind])
      return -1;
    else{
      /*float prob = 0;
      if(hits[ind] > 0){
	prob = 1;
	}*/
      return fmin(1.0,(float) hits[ind] / (float) visits[ind]);
    }
  }

  inline int readOccCount(const int ixy[2])
  {
    int ind = getInd(ixy);
    if (!visits[ind])
      return -1;
    else{
      return full_hits[ind];
    }
  }

  inline float readConsValue(const int ixy[2]) //return the worst case reading - i.e. if it was ever occupied 
  {
    int ind = getInd(ixy);
    if (!visits[ind])
      return -1;
    else{
      float prob = 0;
      if(hits[ind] > 0){
	prob = 1;
      }
      return prob;
    }
  }

  /*inline float checkDoorValue(const int ixy[2]) //return the worst case reading - i.e. if it was ever occupied 
  {
    int ind = getInd(ixy);
    if (!visits[ind])
      return -1;
    else{
      float prob = 0;
      if(hits[ind] > 0){
	prob = 1;
      }
      return prob;
    }
    }*/

  inline float readOptValue(const int ixy[2]) //return best case value - i.e. if it was ever free 
  {
    int ind = getInd(ixy);
    if (!visits[ind])
      return -1;
    else{
      float prob = 1;
      if(hits[ind] < visits[ind]){
	prob = 0;
      }
      return prob;
    }
  }

  inline float readValue(const double xy[2])
  {
    int ixy[2];
    worldToTable(xy, ixy);
    return readValue(ixy);
  }
  inline void updateValue(const int ixy[2], int hit, int visit = 1)
  {
    //TODO: NEED to figure out what to do with capping if hit/visit isn't 1
    int ind = ixy[1] * width + ixy[0];
    if (visits[ind] < maxEvidence) {
      visits[ind] += visit;
      hits[ind] += hit;
      full_hits[ind] +=hit;
    }
    else if (hit>0 && hits[ind]<maxEvidence){
      hits[ind] += hit;
      full_hits[ind] += hit;
    }
    else if (hit == 0 && hits[ind] > 0)
      hits[ind] -= visit; //can't i
  }

  inline void updateValue(const double xy[2], int hit, int visit = 1)
  {
    int ixy[2];
    worldToTable(xy, ixy);
    updateValue(ixy, hit, visit);
  }

  inline void getCenterLocation(double center[2])
  {
    int icenter[2] = { width / 2, height / 2 };
    tableToWorld(icenter, center);
  }

  void rayTrace(const int start[2], const int end[2], int hitInc, int visitInc, bool hitAll);
  inline void rayTrace(const double start[2], const double end[2], int hitInc, int visitInc, bool hitAll)
  {
    int istart[2], iend[2];
    worldToTable(start, istart);
    worldToTable(end, iend);
    rayTrace(istart, iend, hitInc, visitInc, hitAll);
  }

  erlcm_gridmap_t *get_gridmap_t();

  erlcm_gridmap_t * get_comp_gridmap_t();

  //add the uncompress stuff here 

  void generate_likelihoods();

};
#endif /*GRIDMAP_H_*/
