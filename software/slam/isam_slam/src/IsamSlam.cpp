/*
 * IsamSlam.cpp
 *
 *  Created on: Oct 22, 2009
 *      Author: abachrac
 */

#include "IsamSlam.h"
#include <math.h>
#include <unistd.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif 

//publishing gridmap
#include <er_carmen/global.h>
#include <interfaces/map3d_interface.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>

using namespace Eigen;
using namespace scanmatch;
IsamSlam::IsamSlam(lcm_t * _lcm,double metersPerPixel_, double thetaResolution_, int useGradientAscentPolish_, int useMultires_,
                   int doDrawing_, bool useThreads_, BotFrames *_frames) :
    loopClosedUpTill(0), gridmap(NULL), gridmapMetersPerPixel(.1), gridmapMargin(10), doDrawing(doDrawing_), useThreads(
                                                                                                                        useThreads_)
{
    //supply dummy values for things we don't care about
    sm = new ScanMatcher(metersPerPixel_, thetaResolution_, useMultires_, false, false);

    no_floors = 0;//increment this value when a new floor is encountered

    regionmap = NULL;
    gridmap = (GridMap **) malloc(no_floors*sizeof(GridMap *));
    for(int i=0; i < no_floors; i++){
        gridmap[i] = NULL;
    }
    //at the start we have no floors
    floor_map = NULL;

    memset(&tagged_places,0,sizeof(tagged_places));
    memset(&plist,0,sizeof(plist));
    poss_node = NULL;
    current_region = -1;
    previous_region = -1;
    slam = new Slam();
    // Create first node at this pose: we add a prior to keep the
    // first pose in place, which is an arbitrary choice.
    //Matrix sqrtinf3 = 10. * Matrix::eye(3);
    Noise sqrtinf3 = SqrtInformation(10. * eye(3));
    //in the new isam 
    //it should be Noise noise = SqrtInformation(10. * eye(3));
    //using namespace Eigen;
    // create a first pose (a node)
    origin_node = new Pose2d_Node();
    // add it to the graph
    slam->add_node(origin_node);
    // create a prior measurement (a factor)
    Pose2d origin(0., 0., 0.);
    Pose2d_Factor* prior = new Pose2d_Factor(origin_node, origin, sqrtinf3);
    // add it to the graph
    slam->add_factor(prior);
    pallet = NULL;
 
    gridmapFreeIncrement = -50;
  
    lcm = _lcm;
    frames = _frames;
    //lcm = globals_get_lcm();
    //lcmgl_graph = globals_get_lcmgl("isam_graph", 1);
    lcmgl_tagged = bot_lcmgl_init(lcm,"isam_tagged");
    lcmgl_map = bot_lcmgl_init(lcm,"isam_map");
    lcmgl_doors = bot_lcmgl_init(lcm,"isam_doors");
    lcmgl_points = bot_lcmgl_init(lcm,"isam_points");
    lcmgl_point_cloud = bot_lcmgl_init(lcm,"isam_pointcloud");
    if (useThreads) {
        //remember to make sure that sm_tictoc gets initialized
        killThread = 0;

        /* Initialize mutex and condition variable objects */
        pthread_mutex_init(&trajectory_mutex, NULL);
        pthread_cond_init(&loop_closer_cond, NULL);

        //create rebuilder thread
        pthread_create(&loop_closer, 0, (void *(*)(void *)) loopClose_thread_func, (void *) this);
    }

}

IsamSlam::~IsamSlam()
{

    if (useThreads) {
        //kill the loop closer thread
        while (killThread != -1) {
            killThread = 1;
            pthread_cond_broadcast(&loop_closer_cond);
            usleep(10000);
        }
        //aquire all locks so we can destroy them
        pthread_mutex_lock(&trajectory_mutex);
        // destroy mutex and condition variable objects
        pthread_mutex_destroy(&trajectory_mutex);
        pthread_cond_destroy(&loop_closer_cond);
    }
    fprintf(stderr, "Killed loop closing thread");

    delete origin_node;
    delete slam;
    delete sm;

    //clear the trajectory
    for (unsigned i = 0; i < trajectory.size(); i++)
        delete trajectory[i];
    trajectory.clear();

}

void IsamSlam::updateNewFloor(int floor_no){ //floor no is the actual floor
    //add new floor
    //add this floor no to the mapping array 
    if(floor_map[current_floor]==-100){
        fprintf(stderr,"ISAM : We were on an unknown floor - we now know that this floor was : %d\n", floor_no);
    }

    int new_floor = 1;
    for(int i=0;i<no_floors; i++){
        if(floor_map[i] == floor_no){ //we have this floor in our mapping already
            new_floor = 0;
            break;
        }
    }

  
    if(new_floor){//we have the correct no of floors - just need to rename the poses 
        floor_map[no_floors-1] = floor_no; //just replace the floor no
        //go through the pose tracjectory and replace all the old floor no's
        for (unsigned t = 0; t < trajectory.size(); t++) {
            SlamPose * slampose = trajectory[t];
            if(slampose->floor_no == -100){ 
                //this might need to be done from the back end - in case there were unknown floors earlier
                fprintf(stderr,"Found an unknown floor pose - replacing floor no\n");
                slampose->floor_no = floor_no;
            }
        }    
    }
    else{//we have revisited an old floor - need to do something a bit more complicated 
        int old_ind = getFloorIndex(floor_no);
        if(old_ind >-1){
            fprintf(stderr,"Found the index of the floor\n");
            for (unsigned t = 0; t < trajectory.size(); t++) {
                SlamPose * slampose = trajectory[t];
                if(slampose->floor_no == -100){ 
                    //this might need to be done from the back end - in case there were unknown floors earlier
                    fprintf(stderr,"Found an unknown floor pose - replacing floor no\n");
                    slampose->floor_no = floor_no;
                }
            }    
      
            current_floor = old_ind;
            no_floors--;
            floor_map = (int *)realloc(floor_map, no_floors * sizeof(int));
            fprintf(stderr,"Trying to free the unnec - gridmap\n");
            free(gridmap[no_floors]);
            fprintf(stderr,"Freed the old gridmap\n");
            //actually reducing the no of gridmaps 
            gridmap = (GridMap **)realloc(gridmap, no_floors*sizeof(GridMap *));
        }
        else{
            fprintf(stderr,"+++++++ Error- Could not find the index of the floor\n");
            //not sure what we should do here 
        }
    }

}

void IsamSlam::addNewFloor(int floor_no){ //floor no is the actual floor
    //add new floor
    //add this floor no to the mapping array 

    int new_floor = 1;
    for(int i=0;i<no_floors; i++){
        if(floor_map[i] == floor_no){ //we have this floor in our mapping already
            new_floor = 0;
            break;
        }
    }
  
    if(new_floor){
        no_floors++;
        floor_map = (int *)realloc(floor_map, no_floors * sizeof(int));
        floor_map[no_floors-1] = floor_no; 

        //need to have a simmilar data structure that keeps track of all the floors that we have visited 
        if (gridmap==NULL){
            //gridmap = calloc(no_floors, sizeof(GridMap *));
            gridmap = (GridMap **) malloc(no_floors*sizeof(GridMap *));
            for(int i=0; i < no_floors; i++){
                //memset(&gridmap[i],0,sizeof(GridMap *));	
                gridmap[i] = NULL;
            }
        }
        else{//we need to realloc 
            gridmap = (GridMap **)realloc(gridmap, no_floors*sizeof(GridMap *));
            gridmap[no_floors -1] = NULL;
        }
    }
}

int IsamSlam::getFloorIndex(int floor_no){
    for(int i=0;i<no_floors; i++){
        if(floor_map[i] == floor_no){ 
            return i;
        }    
    }
    return -1;
}

//need a function that looks through the last -1 labeled regions and relables them 
/*void IsamSlam::relableRegions(){
//the input should be the Node indexes /utime and their new indexes 

}*/

void IsamSlam::addNodeToSlam(Pose2d &prev_curr_tranf, Noise &cov, Scan * scan, Scan * maxRangeScan, int64_t utime,
			     double height, double rp[2], int floor_no)
{
    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);

    int floor_ind = getFloorIndex(floor_no);

    SlamPose * slampose = new SlamPose(utime, trajectory.size() + 1, scan, maxRangeScan, height, rp, floor_no);
    slam->add_node(slampose->pose2d_node);

    Pose2d_Pose2d_Factor* constraint;

    //in the new isam 
    //cov should be of Type Noise 
    //not Matrix
    if (!trajectory.empty())
        constraint = new Pose2d_Pose2d_Factor(trajectory.back()->pose2d_node, slampose->pose2d_node, prev_curr_tranf, cov);
    else
        constraint = new Pose2d_Pose2d_Factor(origin_node, slampose->pose2d_node, prev_curr_tranf, cov);

    current_floor = getFloorIndex(floor_no);
    slam->add_factor(constraint);
    trajectory.push_back(slampose);
    
    publishGraphNode(trajectory.size() - 1);
    publishGraphTransforms();
    publishSlamGraph(utime);

    if (useThreads) {
        pthread_mutex_unlock(&trajectory_mutex);
        pthread_cond_broadcast(&loop_closer_cond);//tell the loop closing thread to have at it...
    }
    else
        doLoopClosing(trajectory.size() - 1);
}

void IsamSlam::updateNodeRegions(erlcm_slam_graph_node_list_t *node_list)
{
    //count the regions 
    int no_regions = 0;
    int *regions = (int *) calloc(node_list->size, sizeof(int));
    
    for(int i=0; i < node_list->size; i++){
        int found = 0;
        for(int j=0; j < no_regions; j++){
            if(regions[j] == node_list->list[i].region_id){
                found = 1; 
                break;
            }
        }
        if(!found){
            regions[no_regions] = node_list->list[i].region_id;
            no_regions++;
        }
    }

    int original_pallet_size = previous_region+1;

    int *region_id = (int *) calloc(no_regions, sizeof(int));
    for(int i=0; i < no_regions; i++){
        region_id[i] = ++previous_region;
    }

    pallet = (color_t *)realloc(pallet, sizeof(color_t)*(previous_region+1));
    
    //creates and maintains a consistant color for each segment 
    for(int i= original_pallet_size; i <=previous_region; i++){
        bot_color_util_rand_color(pallet[i].c, 0.7, 0.3);
    }

    for(int i=0; i < node_list->size; i++){
        if(node_list->list[i].id < trajectory.size()){
            int id = node_list->list[i].id;
            SlamPose * slampose = trajectory[id];
            if(slampose->utime == node_list->list[i].utime){
                int region_index = 0;
                
                for(int j=0; j < no_regions; j++){
                    if(regions[j] == node_list->list[i].region_id){
                        region_index = region_id[j];
                        break;
                    }
                }
                
                /*fprintf(stderr, "Found [%d] - replacing [%d] => [%d]\n", 
                  id, slampose->region_id, region_index);*/
                slampose->region_id = region_index;
            }
            else{
                fprintf(stderr,"Error - Node id time not found\n");
            }
        }
        else{
            fprintf(stderr,"Error - Node id not found\n");
        }        
    }

    free(regions);
    free(region_id);
}

void IsamSlam::addNodeToSlam(Pose2d &prev_curr_tranf, Noise &cov, Scan * scan, Scan * maxRangeScan, int64_t utime,
			     double height, double rp[2], int floor_no, int region_id)
{
    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);

    int floor_ind = getFloorIndex(floor_no);

    current_region = region_id;

    fprintf(stderr,"Region ID : %d\n", region_id);

    SlamPose * slampose = new SlamPose(utime, trajectory.size() + 1, scan, maxRangeScan, height, rp, floor_no, region_id);
    slam->add_node(slampose->pose2d_node);

    Pose2d_Pose2d_Factor* constraint;

    //in the new isam 
    //cov should be of Type Noise 
    //not Matrix
    if (!trajectory.empty())
        constraint = new Pose2d_Pose2d_Factor(trajectory.back()->pose2d_node, slampose->pose2d_node, prev_curr_tranf, cov);
    else
        constraint = new Pose2d_Pose2d_Factor(origin_node, slampose->pose2d_node, prev_curr_tranf, cov);

    current_floor = getFloorIndex(floor_no);
    slam->add_factor(constraint);
    trajectory.push_back(slampose);

    publishGraphNode(trajectory.size() - 1);
    publishGraphTransforms();
    publishSlamGraph(utime);

    if (useThreads) {
        pthread_mutex_unlock(&trajectory_mutex);
        pthread_cond_broadcast(&loop_closer_cond);//tell the loop closing thread to have at it...
    }
    else
        doLoopClosing(trajectory.size() - 1);
}

void IsamSlam::doLoopClosing(int loop_close_ind)
{
    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);
    if (loop_close_ind < 20) {
        trajectory[loop_close_ind]->loopClosureChecked = true;
        if (useThreads)
            pthread_mutex_unlock(&trajectory_mutex);
        return; //nothin to loop close against yet
    }
    //see if the end of the trajectory matches something toward the back
    SlamPose * slampose_tocheck = trajectory[loop_close_ind];
    SlamPose * slampose_toverify = trajectory[loop_close_ind - 1];
    slampose_tocheck->loopClosureChecked = true;
    Pose2d tocheck_value = slampose_tocheck->pose2d_node->value();
    Pose2d toverify_value = slampose_toverify->pose2d_node->value();
    smPoint p0 = { tocheck_value.x(), tocheck_value.y() };
  
    int curr_node_floor = slampose_tocheck->floor_no;

    //find closest point in the trajectory to build map from
    int closestInd = -1;
    double closestDist = 1e9;
    for (int i = 0; i < (loop_close_ind - 30); i++) {
        Pose2d value = trajectory[i]->pose2d_node->value();

        if(curr_node_floor != trajectory[i]->floor_no){
            continue;
        }
        smPoint p1 = { value.x(), value.y() };
        double d = sm_dist(&p0, &p1);
        if (d < closestDist) {
            closestInd = i;
            closestDist = d;
        }
    }

    if (closestDist > 10) {
        //    fprintf(stderr, "no point is within 6m, closest was %f\n", closestDist);
        if (useThreads)
            pthread_mutex_unlock(&trajectory_mutex);
        return;
    }

    fprintf(stderr,"Loop closure => Close Node : %f\n", closestDist);

    sm_tictoc("update_transforms");
    //lets try to match against the closest
    //TODO: may want to match against multiple trajectory sections
    int rangeStart = sm_imax(0, closestInd - 10);
    int rangeEnd = closestInd + 10; //has to be at least 40 from the end
    //  fprintf(stderr, "closest is %f trying to do loop closure on scans %d-%d\n", closestDist,rangeStart,rangeEnd);
    for (int i = rangeStart; i <= rangeEnd; i++) {
        //update the scan match transform with the current SLAM value estimate
        Pose2d value = trajectory[i]->pose2d_node->value();
        ScanTransform newT;
        newT.x = value.x();
        newT.y = value.y();
        newT.theta = value.t();
        trajectory[i]->scan->applyTransform(newT);
        sm->addScan(trajectory[i]->scan, false);
    }
    sm_tictoc("update_transforms");

    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);

    sm_tictoc("build_map");
    sm->addScan(NULL, true); //actually do the raster rebuild
    sm_tictoc("build_map");

    sm_tictoc("LC_Match");
    slampose_tocheck->scan->T.score = 0;
    ScanTransform lc_r = sm->gridMatch(slampose_tocheck->scan->points, slampose_tocheck->scan->numPoints,
                                       &slampose_tocheck->scan->T, 6.0, 6.0, M_PI / 6.0);
    sm_tictoc("LC_Match");
    double hitpct = lc_r.hits / (double) slampose_tocheck->scan->numPoints;
    double sxx = lc_r.sigma[0];
    double sxy = lc_r.sigma[1];
    double syy = lc_r.sigma[4];
    double stt = lc_r.sigma[8];

    bool accepted = false;
    if (hitpct > .60) {
        fprintf(stderr, "tentatively ACCEPTED match for node %d had %f%% hits...", slampose_tocheck->node_id, hitpct);
        accepted = true;
    }
    else {
        fprintf(stderr, "REJECTED match for node %d had %f%% hits, sxx=%f,sxy=%f,syy=%f,stt=%f\n",
                slampose_tocheck->node_id, hitpct, lc_r.sigma[0], lc_r.sigma[1], lc_r.sigma[4], lc_r.sigma[8]);
    }

    if (accepted) {
        double sigma[9];
        memcpy(sigma, lc_r.sigma, 9 * sizeof(double));
        double evals[3] = { 0 };
        double evals_sq[9] = { 0 };
        double evecs[9] = { 0 };
        CvMat cv_sigma = cvMat(3, 3, CV_64FC1, sigma);
        CvMat cv_evals = cvMat(3, 1, CV_64FC1, evals);
        CvMat cv_evecs = cvMat(3, 3, CV_64FC1, evecs);
        cvEigenVV(&cv_sigma, &cv_evecs, &cv_evals);
        if (evals[0] < .01) {
            fprintf(stderr, "sxx=%f,sxy=%f,syy=%f,stt=%f, eigs=[%f %f %f]\n", sxx, sxy, syy, stt, evals[0], evals[1],
                    evals[2]);
            accepted = true;
        }
        else {
            fprintf(stderr, "REJECTED sxx=%f,sxy=%f,syy=%f,stt=%f, eigs=[%f %f %f]\n", sxx, sxy, syy, stt, evals[0],
                    evals[1], evals[2]);
            accepted = false;
        }
    }
    if (accepted) {
        if (doDrawing) {
            sm->drawGUI(slampose_tocheck->scan->points, slampose_tocheck->scan->numPoints, lc_r, NULL, "LC_accept",
                        CV_RGB(0,255,0));
        }
        //perform rigidity check
        Pose2d newToCheckValue(lc_r.x, lc_r.y, lc_r.theta);
        Pose2d oldDelta = toverify_value.ominus(tocheck_value);
        Pose2d newToVerifyValue = newToCheckValue.oplus(oldDelta); //if match is rigid, verify scan should be here...
        Pose2d sanity = tocheck_value.oplus(oldDelta);

        ScanTransform newT;
        memset(&newT, 0, sizeof(newT));
        newT.x = newToVerifyValue.x();
        newT.y = newToVerifyValue.y();
        newT.theta = newToVerifyValue.t();
        ScanTransform verify_r = sm->gridMatch(slampose_toverify->scan->points, slampose_toverify->scan->numPoints, &newT,
                                               1.0, 1.0, M_PI / 12.0);
        smPoint p0 = { verify_r.x, verify_r.y };
        smPoint p1 = { newT.x, newT.y };
        double dist = sm_dist(&p0, &p1);
        double adist = fabs(sm_angle_subtract(verify_r.theta, newT.theta));
        if (dist < .04 && adist < .01) {
            fprintf(stderr, "Match PASSED rigidity check... dist=%f, adist=%f\n", dist, adist);
            if (doDrawing) {
                sm->drawGUI(slampose_toverify->scan->points, slampose_toverify->scan->numPoints, verify_r, NULL, "LC_verify",
                            CV_RGB(0,255,0));
            }
        }
        else {
            fprintf(stderr, "Match FAILED rigidity check... dist=%f, adist=%f\n", dist, adist);
            if (doDrawing) {
                sm->drawGUI(slampose_toverify->scan->points, slampose_toverify->scan->numPoints, verify_r, NULL,
                            "LC_REJECTED_verify", CV_RGB(255,0,0));
            }
            accepted = false;
        }

    }

    if (accepted) {
        sm_tictoc("loopCloseOptimize");
        if (useThreads)
            pthread_mutex_lock(&trajectory_mutex);

        //add the edge to isam
        Pose2d matchedPos(lc_r.x, lc_r.y, lc_r.theta);
        Pose2d transf = matchedPos.ominus(trajectory[closestInd]->pose2d_node->value()); //get the delta between that pose and the current

        //TODO: probably want to scale the covariance...
        //rotate the cov to body frame
        double Rcov[9];
        sm_rotateCov2D(lc_r.sigma, -lc_r.theta, Rcov);
        //Matrix cov(3, 3, Rcov);
        Matrix3d cv(Rcov);

        Noise cov = Covariance(10000.0 * cv);
        //cov = 10000.0 * cov;
        //    printf("LC cov:\n");
        //    cov.print();
        //    printf("\n");
        double cov_hardcode[9] = { .25, 0, 0, 0, .25, 0, 0, 0, .1 };
        //Matrix cov_hc(3, 3, cov_hardcode);
        Matrix3d cv_hc(cov_hardcode);// =// MatrixXd::Random(3,3);
        Noise cov_hc = Covariance(cv_hc);

        Pose2d_Pose2d_Factor* constraint = new Pose2d_Pose2d_Factor(trajectory[closestInd]->pose2d_node,
                                                                    slampose_tocheck->pose2d_node, transf, cov_hc);
        slampose_tocheck->constraint_ids.push_back(closestInd);
        slam->add_factor(constraint);

        //optimize the graph
        sm_tictoc("batch_optimization");
        slam->batch_optimization();
        sm_tictoc("batch_optimization");

        //    sm_tictoc("update");
        //    slam->update();
        //    sm_tictoc("update");


        if (useThreads)
            pthread_mutex_unlock(&trajectory_mutex);
        sm_tictoc("loopCloseOptimize");
    }
    else if (doDrawing) {
        sm->drawGUI(slampose_tocheck->scan->points, slampose_tocheck->scan->numPoints, lc_r, NULL, "LC_reject",
                    CV_RGB(255,0,0));
    }

    sm->clearScans(false);//clear out scans to get ready for next time
}

void * IsamSlam::loopClose_thread_func(IsamSlam*parent)
{
    fprintf(stderr, "Loop closure thread started\n");

    pthread_mutex_lock(&parent->trajectory_mutex);
    while (!parent->killThread) {
        int ind_to_check = -1;
        for (int i = parent->trajectory.size() - 1; i >= parent->loopClosedUpTill; i--) {
            if (!parent->trajectory[i]->loopClosureChecked) {
                ind_to_check = i;
                break;
            }
        }
        if (ind_to_check < 0) {
            //all nodes have been checked already... so lets go to sleep
            parent->loopClosedUpTill = parent->trajectory.size();
            pthread_cond_wait(&parent->loop_closer_cond, &parent->trajectory_mutex);
            continue;
        }
        else {
            //need to check this node
            pthread_mutex_unlock(&parent->trajectory_mutex); //unlock since lock is reacquired in doLoopClosing
            parent->doLoopClosing(ind_to_check);
            pthread_mutex_lock(&parent->trajectory_mutex); //lock to go back around the loop
        }

    }
    pthread_mutex_unlock(&parent->trajectory_mutex);
    parent->killThread = -1;
    fprintf(stderr, "Loop closure thread stopped\n");
    return NULL;
}

void IsamSlam::publishSlamGraph(int64_t utime){
    //publish the entire graph 
    erlcm_slam_full_graph_t msg;
    
    msg.utime = utime;
    msg.no_nodes = trajectory.size();
    
    msg.nodes = (erlcm_slam_full_graph_node_t *)calloc(msg.no_nodes, sizeof(erlcm_slam_full_graph_node_t));
    for (int t = 0; t < trajectory.size(); t++) {
        SlamPose * slampose = trajectory[t];
        Pose2d value = slampose->pose2d_node->value();
        msg.nodes[t].id = t;
        msg.nodes[t].pos[0] = value.x();
        msg.nodes[t].pos[1] = value.y();
        msg.nodes[t].t = value.t();
        msg.nodes[t].region_id = -1;
        msg.nodes[t].floor_id = slampose->floor_no;
        msg.nodes[t].utime = slampose->utime;
    }
    
    erlcm_slam_full_graph_t_publish(lcm, "SLAM_FULL_GRAPH", &msg);
    free(msg.nodes);
}


void IsamSlam::publishGraphNode(int id){
    //convert the last node to 
    SlamPose *sp = trajectory[id];
    Scan *p_scan = sp->scan;
    erlcm_slam_graph_node_t msg;
    msg.utime = trajectory[id]->utime;
    msg.id = id;
    msg.region_id = trajectory[id]->region_id;
    msg.floor_id = sp->floor_no;
    msg.pl.no = p_scan->numPoints;
    msg.pl.points = (erlcm_scan_point_t *)calloc(p_scan->numPoints, sizeof(erlcm_scan_point_t));
    
    msg.rp[0] = sp->rp[0];
    msg.rp[1] = sp->rp[1];
    
    for(int i= p_scan->numPoints -1; i>=0; i--){
        msg.pl.points[p_scan->numPoints -1 - i].pos[0] = p_scan->points[i].x;
        msg.pl.points[p_scan->numPoints -1 - i].pos[1] = p_scan->points[i].y;       
    }
    erlcm_slam_graph_node_t_publish(lcm, "SLAM_NODE", &msg);

    free(msg.pl.points);
}

void IsamSlam::publishGraphTransforms(){
    erlcm_slam_pose_transform_list_t msg;
    msg.utime = bot_timestamp_now();
    msg.size = trajectory.size();
    msg.trans = (erlcm_slam_pose_transform_t *)calloc(msg.size, sizeof(erlcm_slam_pose_transform_t));
    for (int t = 0; t < trajectory.size(); t++) {
        SlamPose * slampose = trajectory[t];
        Pose2d value = slampose->pose2d_node->value();
        msg.trans[t].utime = trajectory[t]->utime;
        msg.trans[t].id = t;
        msg.trans[t].pos[0] = value.x();
        msg.trans[t].pos[1] = value.y();
        msg.trans[t].t = value.t();
    }
    erlcm_slam_pose_transform_list_t_publish(lcm, "SLAM_NODE_TRANSFORMS", &msg);
    free(msg.trans);
}

void IsamSlam::checkForUpdates()
{
  

    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);

    // Compute the bounds of the current map.
  
    for(int i=0; i < no_floors; i++){
        double minxy[2] = { DBL_MAX, DBL_MAX };
        double maxxy[2] = { -DBL_MAX, -DBL_MAX };
    
        bool needToDelete = false;
        for (unsigned t = 0; t < trajectory.size(); t++) {
            if(getFloorIndex(trajectory[t]->floor_no) != i){
                continue;
            }
            int updated = trajectory[t]->updateScan();//update the scan to match the current slam pos
            if (updated && trajectory[t]->rendered)
                needToDelete = true;
            for (unsigned j = 0; j < trajectory[t]->allScans.size(); j++) {
                Scan * s = trajectory[t]->allScans[j];
	
                for (unsigned cidx = 0; cidx < s->contours.size(); cidx++) {
                    for (unsigned k = 0; k < s->contours[cidx]->points.size(); k++) {
                        smPoint p = s->contours[cidx]->points[k];
                        minxy[0] = fmin(minxy[0], p.x);
                        maxxy[0] = fmax(maxxy[0], p.x);
                        minxy[1] = fmin(minxy[1], p.y);
                        maxxy[1] = fmax(maxxy[1], p.y);
                    }
                }
            }
        }
    
        //check if the map needs to be increased 
        if (gridmap[i] != NULL) {
            if (minxy[0] < gridmap[i]->xy0[0] || maxxy[0] > gridmap[i]->xy1[0] || minxy[1] < gridmap[i]->xy0[1] || maxxy[1]
                > gridmap[i]->xy1[1])
                needToDelete = true;
            if (needToDelete) {
                delete gridmap[i];
                gridmap[i] = NULL;
                //mark all floor poses as unrendered 
                for (unsigned t = 0; t < trajectory.size(); t++){
                    if(getFloorIndex(trajectory[t]->floor_no) != i){
                        //fprintf(stderr,"Skipping\n");
                        continue;
                    }
                    trajectory[t]->rendered = false;
                }
            }
        }
        if (gridmap[i] == NULL) {
            printf("creating new gridmap\n");
            minxy[0] -= gridmapMargin;
            minxy[1] -= gridmapMargin;
            maxxy[0] += gridmapMargin;
            maxxy[1] += gridmapMargin;
            gridmap[i] = new GridMap(minxy, maxxy, gridmapMetersPerPixel);
        }
    }
    //  else {
    //    printf("reusing old gridmap\n");
    //  }
    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);

}

void IsamSlam::publishSlamPos()
{
    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);

    SlamPose * slampose = trajectory.back();
    Pose2d value = slampose->pose2d_node->value();

    erlcm_SLAM_pose_t slam_pos;
    memset(&slam_pos, 0, sizeof(slam_pos));
    slam_pos.utime = slampose->utime;
    slam_pos.pos[0] = value.x();
    slam_pos.pos[1] = value.y();
    slam_pos.pos[2] = slampose->height;
    double rpy[3] = { 0, 0, value.t() };
    bot_roll_pitch_yaw_to_quat(rpy, slam_pos.orientation);
    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);
    erlcm_SLAM_pose_t_publish(lcm, SLAM_POSITION_CHANNEL, &slam_pos);

}

void IsamSlam::publishSlamTrajectory()
{
    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);
    erlcm_pose_list_t pose_list;
    memset(&pose_list, 0, sizeof(pose_list));
    pose_list.num_poses = trajectory.size();
    pose_list.poses = (bot_core_pose_t *) calloc(pose_list.num_poses, sizeof(bot_core_pose_t));

    for (unsigned int i = 0; i < trajectory.size(); i++) {
        SlamPose * slampose = trajectory[i];
        Pose2d value = slampose->pose2d_node->value();

        pose_list.poses[i].utime = slampose->utime;
        pose_list.poses[i].pos[0] = value.x();
        pose_list.poses[i].pos[1] = value.y();
        pose_list.poses[i].pos[2] = slampose->height;
        double rpy[3] = { 0, 0, value.t() };
        bot_roll_pitch_yaw_to_quat(rpy, pose_list.poses[i].orientation);
    }
    pose_list.utime = trajectory.back()->utime;
    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);
    erlcm_pose_list_t_publish(lcm, SLAM_TRAJECTORY_CHANNEL, &pose_list);

}

void IsamSlam::clearRobotPose(GridMap *gridmap, double gxy[2], double theta){
    int lxy[2];
    gridmap->worldToTable(gxy, lxy);

    int x = lxy[0];
    int y = lxy[1];
    
    int j,k;
    double s = sin(theta);
    double c = cos(theta);
    
    for(j=-10;j<11;j++){
        for(k=-10;k<11;k++){
            int curr_x,curr_y;
            curr_x = x + j;
            curr_y = y + k;
	
            double map_x,map_y;
            map_x = j*gridmap->metersPerPixel;
            map_y = k*gridmap->metersPerPixel;

            double local_x,local_y;
            local_x = map_x *c + map_y*s;
            local_y = - map_x*s + map_y*c;
            if((local_x <= 0.5 &&  local_x >= -0.5) && (local_y <= 0.4 &&  local_y >= -0.4)){//ideally we can do a better job of cleaning up the map 
                int ixy[2] = {curr_x,curr_y};
                gridmap->clearValue(ixy);
            }
        }
    }  
}

void IsamSlam::detectDoors(GridMap *gridmap, double gxy[2], int floor, erlcm_elevator_node_t *curr_ele){
    int lxy[2];
    gridmap->worldToTable(gxy, lxy);
    float search_radius = 2.0;
    int span = (int) (search_radius / gridmap->metersPerPixel);
    fprintf(stderr,"Span : %d \n", span);
    int x = lxy[0];  
    int y = lxy[1];
    double size[2] = {gridmap->metersPerPixel, gridmap->metersPerPixel};

    typedef struct _ele_points {
        double pos[2];
        int floor;
    }ele_points;

    ele_points *ele_door = NULL;
    int point_count = 0;
    char ele_lcm[100];
    sprintf(ele_lcm,"ele_floor_%d", floor_map[floor]);
    bot_lcmgl_t *lcmgl_ele = bot_lcmgl_init(lcm,ele_lcm);
    bot_lcmgl_line_width(lcmgl_ele, 1);
    bot_lcmgl_point_size(lcmgl_ele, 10);
      
    for(int j=-span;j<span+1;j++){
        for(int k=-span;k<span+1;k++){
            int curr_x,curr_y;
            curr_x = x + j;
            curr_y = y + k;

            int ixy[2] = {curr_x,curr_y};
            //float visit_value = gridmap->readValue(ixy);
            int occCount = gridmap->readOccCount(ixy);
            float avg_occ = 0;
            int count = 0;
            for(int l=-2;l<3;l++){
                for(int m=-2;m<3;m++){
                    int temp_x = curr_x + l;
                    int temp_y = curr_y + m;
                    int ixy[2] = {temp_x,temp_y};
                    float temp_likeli_value = gridmap->getLikelihoodValue(ixy);
                    if(temp_likeli_value >=0){
                        count++;
                        avg_occ +=temp_likeli_value;
                    }
                }
            }
            avg_occ /=count;

            double global_loc[3] = {.0,.0,.0};
            gridmap->tableToWorld(ixy,global_loc);

            /*bot_lcmgl_color3f(lcmgl_ele_full, 0, 1, 1);  
            //bot_lcmgl_rect(lcmgl_ele_full, global_loc, size, 1);
            char occ_info[100];
            sprintf(occ_info,"O:%d=A:%f\n",occCount, avg_occ);
            bot_lcmgl_text(lcmgl_ele_full,global_loc, occ_info);*/
      
            //float likeli_value = gridmap->getLikelihoodValue(ixy);
            //if((likeli_value == 0.0 && occCount > 1)){// || (visit_value > 0.2 && visit_value < 0.8)){
            if((avg_occ < 0.1 && occCount > 1)){

                //fprintf(stderr,"Mismatch Found\n");
	

                /*bot_lcmgl_color3f(lcmgl_ele_points, 0, 1, 0);  
                  bot_lcmgl_rect(lcmgl_ele_points, global_loc, size, 1);
                */
                point_count++;
                ele_door = (ele_points *) realloc(ele_door,sizeof(ele_points)*point_count);
                ele_door[point_count-1].pos[0] = global_loc[0];
                ele_door[point_count-1].pos[1] = global_loc[1];
                ele_door[point_count-1].floor = floor;
            }
        }
    }

    /*bot_lcmgl_switch_buffer(lcmgl_ele_points);
      bot_lcmgl_switch_buffer(lcmgl_ele_full);
    */
    fprintf(stderr, "No of points found : %d\n\tClustering", point_count);

    int no_clusters = point_count;

    typedef struct _ele_clusters {
        ele_points *points;
        int no_ele;
    }ele_clusters;


    ele_clusters *ele_clus = (ele_clusters *) malloc(sizeof(ele_clusters)*no_clusters);
    for(int i=0;i< no_clusters; i++){
        ele_clus[i].no_ele = 1;
        ele_clus[i].points = (ele_points *)malloc(sizeof(ele_points)*1);
        memcpy(&ele_clus[i].points[0],&ele_door[i], sizeof(ele_points));
        //fprintf(stderr,"%d =>(%f,%f)\n",i, ele_clus[i].points[0].pos[0],ele_clus[i].points[0].pos[1]);
    }
  
    free(ele_door);
    double closest_dist;
    double mearging_dist = 0.2;
    do{
        closest_dist = 1000;
        int best_ind[2] = {-1,-1};
        //find the closest clusters 
        for(int i=0;i< no_clusters; i++){
            for(int j=0;j< no_clusters; j++){
                if(i==j){
                    continue;
                }
                for(int k=0;k< ele_clus[i].no_ele; k++){
                    for(int l=0;l< ele_clus[j].no_ele; l++){
                        //fprintf(stderr,"(%f,%f) = (%f,%f)\n",ele_clus[i].points[k].pos[0],ele_clus[i].points[k].pos[1],
                        //	    ele_clus[j].points[l].pos[0],ele_clus[j].points[l].pos[1]);
                        double temp_dist = hypot(ele_clus[i].points[k].pos[0]- ele_clus[j].points[l].pos[0],
                                                 ele_clus[i].points[k].pos[1]- ele_clus[j].points[l].pos[1]);
                        //fprintf(stderr,"%d [%d],%d [%d] Temp Dist : %f\n", i,k,j,l,temp_dist);
                        if(closest_dist > temp_dist){
                            //fprintf(stderr,"(%f,%f) = (%f,%f)\n",ele_clus[i].points[k].pos[0],ele_clus[i].points[k].pos[1],
                            //	      ele_clus[j].points[l].pos[0],ele_clus[j].points[l].pos[1]);
                            //fprintf(stderr,"%d [%d],%d [%d] Temp Dist : %f\n", i,k,j,l,temp_dist);
                            closest_dist = temp_dist;
                            best_ind[0] = i;
                            best_ind[1] = j;	      
                        }
                    }
                }
            }
        }
        //fprintf(stderr,"Closest Clusters : %d <=> %d: %f\n", best_ind[0], best_ind[1], closest_dist);
        if(closest_dist <= mearging_dist){
            //mearge the clusters 
            /*fprintf(stderr,"--------Before Merging--------\n");
              fprintf(stderr,"====== Cluster : %d =======\n", best_ind[0]);
              for(int k=0;k< ele_clus[best_ind[0]].no_ele; k++){
              fprintf(stderr,"%d =>(%f,%f)\n",k, ele_clus[best_ind[0]].points[k].pos[0],ele_clus[best_ind[0]].points[k].pos[1]);
              }
              fprintf(stderr,"====== Cluster : %d =======\n", best_ind[1]);
              for(int k=0;k< ele_clus[best_ind[1]].no_ele; k++){
              fprintf(stderr,"%d =>(%f,%f)\n",k, ele_clus[best_ind[1]].points[k].pos[0],ele_clus[best_ind[1]].points[k].pos[1]);
              }*/

            int original_count = ele_clus[best_ind[0]].no_ele;
            ele_clus[best_ind[0]].no_ele += ele_clus[best_ind[1]].no_ele;
            ele_clus[best_ind[0]].points = (ele_points *)realloc(ele_clus[best_ind[0]].points,sizeof(ele_points)*ele_clus[best_ind[0]].no_ele);
            memcpy(ele_clus[best_ind[0]].points+original_count, ele_clus[best_ind[1]].points, sizeof(ele_points)*ele_clus[best_ind[1]].no_ele);

            /*fprintf(stderr,"Merging\n");
              fprintf(stderr,"++++++ After Merging ++++++\n");
              fprintf(stderr,"====== Cluster : %d =======\n", best_ind[0]);
              for(int k=0;k< ele_clus[best_ind[0]].no_ele; k++){
              fprintf(stderr,"%d =>(%f,%f)\n", k, ele_clus[best_ind[0]].points[k].pos[0],ele_clus[best_ind[0]].points[k].pos[1]);
              }*/
            ele_clus[best_ind[1]].no_ele = 0;
            free(ele_clus[best_ind[1]].points);
        }
    }while(closest_dist <= mearging_dist);

    fprintf(stderr,"Done merging\n");
    int final_cluster_count = 0;
    double dist_from_ele = 1000;
    int clostest_cluster = -1;
    for(int i=0;i< no_clusters; i++){
        if(ele_clus[i].no_ele >0){
            final_cluster_count++;
            //fprintf(stderr,"%d => %d\n",i,ele_clus[i].no_ele);

            //calculate the span
            //double span = 0;

            /*for(int j=0; j < ele_clus[i].no_ele; j++){
              for(int k=0; k < ele_clus[i].no_ele; k++){
              double t_gap = hypot(ele_clus[i].points[k].pos[0] - ele_clus[i].points[j].pos[0],
              ele_clus[i].points[k].pos[1] - ele_clus[i].points[j].pos[1]);
              if(t_gap > span){
              span = t_gap;
              }
              }
              }*/

	  
      
            double mean_loc[2] = {.0,.0};
            if(ele_clus[i].no_ele > 8){
                for(int k=0;k< ele_clus[i].no_ele; k++){
                    double g_loc[3] = {ele_clus[i].points[k].pos[0],ele_clus[i].points[k].pos[1],.0};
                    mean_loc[0] += ele_clus[i].points[k].pos[0];
                    mean_loc[1] += ele_clus[i].points[k].pos[1];
                    /*if(ele_clus[i].points[k].floor==0){
                      bot_lcmgl_color3f(lcmgl_doors, 1, 0, 0);  
                      }
                      else{
                      bot_lcmgl_color3f(lcmgl_doors, 0, 1, 0);  
                      }
                      bot_lcmgl_circle(lcmgl_doors, g_loc, .3);
                      char info[100];
                      sprintf(info,"Fl:%d Cl: %d\n",ele_clus[i].points[k].floor,i);
                      bot_lcmgl_color3f(lcmgl_doors, 1, 1.0, 0);  
                      bot_lcmgl_text(lcmgl_doors,g_loc, info);*/
                }
                mean_loc[0] /=ele_clus[i].no_ele;
                mean_loc[1] /=ele_clus[i].no_ele;
	
                //check the distance from the elevator location 
                double t_dist_ele = hypot(mean_loc[0] - gxy[0], 
                                          mean_loc[1] - gxy[1]);
                fprintf(stderr,"[%d] => Distance from Elevator : %f\n", i , t_dist_ele);
                if(dist_from_ele > t_dist_ele){
                    dist_from_ele = t_dist_ele;
                    clostest_cluster = i;
                }
            }      
        }
    }
    fprintf(stderr,"Closest Cluster : %d Dist :%f\n", clostest_cluster, dist_from_ele);
    double mean_loc[2] = {.0,.0};

    if(dist_from_ele < 5.0){
        fprintf(stderr,"Drawing\n");
        int i = clostest_cluster;
        fprintf(stderr,"@@@@@@@@ Fl No: %d Cl: %d\n", floor_map[floor],i);
        for(int k=0;k< ele_clus[i].no_ele; k++){
            double g_loc[3] = {ele_clus[i].points[k].pos[0],ele_clus[i].points[k].pos[1],.0};
            mean_loc[0] += ele_clus[i].points[k].pos[0];
            mean_loc[1] += ele_clus[i].points[k].pos[1];
            bot_lcmgl_color3f(lcmgl_ele, 0, 0, 1);  
      

            bot_lcmgl_rect(lcmgl_ele, g_loc, size, 1);
            //bot_lcmgl_circle(lcmgl_doors, g_loc, .3);
            char info[100];
            sprintf(info,"Fl:%d Cl: %d\n",floor_map[floor],i);     
            fprintf(stderr,"Fl:%d Cl: %d\n",floor_map[floor],i);     
            bot_lcmgl_color3f(lcmgl_ele, 1, 1.0, 0);  
            bot_lcmgl_text(lcmgl_ele,g_loc, info);
        }
        mean_loc[0] /= ele_clus[i].no_ele;
        mean_loc[1] /= ele_clus[i].no_ele;

        memcpy(&curr_ele->door_pos, mean_loc, sizeof(double)*2);
        curr_ele->door_det = 1;
    }
    
    //curr_ele

    fprintf(stderr,"Total Clusters : %d\n", final_cluster_count);

    for(int i=0;i< no_clusters; i++){
        if(ele_clus[i].no_ele >0){
            free(ele_clus[i].points);
        }
    }
    free(ele_clus);

    bot_lcmgl_switch_buffer(lcmgl_ele);
}


void IsamSlam::renderMultiGridmap()
{

    checkForUpdates();

    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);
    sm_tictoc("renderGridmap_nolocking");
    for (unsigned t = 0; t < trajectory.size(); t++) {
        SlamPose * slampose = trajectory[t];
        if(t >0) {
            SlamPose * p_slampose = trajectory[t-1];
            if(getFloorIndex(slampose->floor_no) != getFloorIndex(p_slampose->floor_no)){
                fprintf(stderr,"---------------Floor Change Detected (Pos Ele)- Prev Floor : %d, New Floor : %d\n", 
                        p_slampose->floor_no, slampose->floor_no);

                if(p_slampose->floor_no !=-100 && slampose->floor_no !=-100){
                    //neither floor is an unknown floor
                    fprintf(stderr,"Adding elevator nodes on both floors\n");
                    addElevatorNode(p_slampose->floor_no, t-1);
                    addElevatorNode(slampose->floor_no, t);
                    //search near these areas for closed/open doors 
	  
                }
            }

            //check for changes of floors 
      
        }
        if (slampose->rendered)
            continue;    
        else
            slampose->rendered = true;
        Pose2d bodyPos = slampose->pose2d_node->value();
        double bodyP[2] = { bodyPos.x(), bodyPos.y() };
        //    slampose->updateScan();//update the scan to match the current slam pos
        int map_ind = getFloorIndex(slampose->floor_no);

        for (unsigned s = 0; s < slampose->allScans.size(); s++) {
            if (s > 0 && trajectory.size() - t > 3)
                continue; //only draw maxranges for last 3 scans
            Scan * scan = slampose->allScans[s];
            //ray trace to compute the map...
            sm_tictoc("render_raytrace");
            for (unsigned i = 0; i < scan->numPoints; i++) {
                gridmap[map_ind]->rayTrace(bodyP, smPoint_as_array(&scan->ppoints[i]), (s == 0), 1, false);
            }
            if (s == 0) {
                //draw the contours as lines for cleaner walls
                for (unsigned cidx = 0; cidx < scan->contours.size(); cidx++) {
                    for (unsigned i = 0; i + 1 < scan->contours[cidx]->points.size(); i++) {
                        //draw the occupied regions
                        smPoint p0 = scan->contours[cidx]->points[i];
                        smPoint p1 = scan->contours[cidx]->points[i + 1];
                        gridmap[map_ind]->rayTrace(smPoint_as_array(&p0), smPoint_as_array(&p1), 1, 0, true);
                    }
                }
            }

            sm_tictoc("render_raytrace");
        }
    }


    //code for clearing the robot path 
    for(int m=0;m< no_floors; m++){
        gridmap[m]->generate_likelihoods();
    }
  
    for (unsigned int i = 0; i < trajectory.size(); i++) {    
        SlamPose * slampose = trajectory[i];    
        /*if(getFloorIndex(slampose->floor_no) != current_floor){
          continue; //skip rendering nodes on other floors
          }*/
        int map_ind = getFloorIndex(slampose->floor_no);
        Pose2d value = slampose->pose2d_node->value();
    
        double gxy[2] = {value.x(), value.y()};
        clearRobotPose(gridmap[map_ind], gxy,value.t()); 

        if(i< trajectory.size()-1){//find the distance traveled and then the middle and clear that
            SlamPose * next_slampose = trajectory[i+1]; 
            //check if these poses are on the same floor
            if(getFloorIndex(slampose->floor_no) == getFloorIndex(next_slampose->floor_no)){
                Pose2d n_value = next_slampose->pose2d_node->value();
                double m_gxy[2] = {(n_value.x()+value.x())/2, (value.y()+n_value.y())/2};
                clearRobotPose(gridmap[map_ind], m_gxy,(value.t() + n_value.t())/2); 
            }      
        }
        /*int lxy[2];
          gridmap[map_ind]->worldToTable(gxy, lxy);

          int x = lxy[0];
          int y = lxy[1];
    
          int j,k;
          double s = sin(value.t());
          double c = cos(value.t());
    
          for(j=-10;j<11;j++){
          for(k=-10;k<11;k++){
          int curr_x,curr_y;
          curr_x = x + j;
          curr_y = y + k;
	
          double map_x,map_y;
          map_x = j*gridmap[map_ind]->metersPerPixel;
          map_y = k*gridmap[map_ind]->metersPerPixel;

          double local_x,local_y;
          local_x = map_x *c + map_y*s;
          local_y = - map_x*s + map_y*c;
          if((local_x <= 0.5 &&  local_x >= -0.5) && (local_y <= 0.25 &&  local_y >= -0.25)){//ideally we can do a better job of cleaning up the map 
	  int ixy[2] = {curr_x,curr_y};
	  gridmap[map_ind]->clearValue(ixy);
          }
          }
          }*/
    }

  

    /*for (int i = 0; i < tagged_places.place_count; i++) {   
    //check for doors if there is an elevator nearby
    if(strcmp(tagged_places.places[i].label,"elevator")==0){
    fprintf(stderr,"Found elevator - Checking for Doors\n");
    double elevator_loc[2] = {tagged_places.places[i].x,tagged_places.places[i].y};
    detectDoors(gridmap[tagged_places.places[i].floor_index],elevator_loc, tagged_places.places[i].floor_index); 
    }
    }
    bot_lcmgl_switch_buffer(lcmgl_doors);*/


  
    sm_tictoc("renderGridmap_nolocking");

    //  cvSaveImage("gridmap.bmp", &gridmap->distim);
    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);

}

void IsamSlam::publishMultiGridMap()
{  
    if(!no_floors){
        fprintf(stderr,"No floors visited\n");
        return;
    }
  
    erlcm_multi_gridmap_t lcm_full_map_msg;

    int utime = bot_timestamp_now();

    //message that holds the multi-floor map
    lcm_full_map_msg.no_floors = no_floors;
    lcm_full_map_msg.current_floor_ind = current_floor;  //this current floor is the index for the map 
    lcm_full_map_msg.maps = (erlcm_floor_gridmap_t *)malloc(no_floors * sizeof(erlcm_floor_gridmap_t));
    for(int m=0;m< no_floors; m++){
        erlcm_gridmap_t * lcm_msg = gridmap[m]->get_comp_gridmap_t();//get_gridmap_t();

        memcpy(&lcm_full_map_msg.maps[m].gridmap,lcm_msg, sizeof(erlcm_gridmap_t));
    
        lcm_full_map_msg.maps[m].floor_no = floor_map[m];
    
    }
    erlcm_multi_gridmap_t_publish(lcm, "MULTI_FLOOR_MAPS", &lcm_full_map_msg);
}

void IsamSlam::publishFinalMultiGridMap()
{  
    if(!no_floors){
        fprintf(stderr,"No floors visited\n");
        return;
    }

    //mearge the detected elevators -- 
    fprintf(stderr,"====== Doing post processing on the elevator nodes\n");
    //first create only one node for each elevator 
    erlcm_tagged_node_list_t elevators;
    elevators.places = NULL;
    elevators.place_count = 0;

    //find the actual elevators in this loop - 
    //take the mean location if there was more than one instance of the same elevator on the same floor 
    for (int i = 0; i < tagged_places.place_count; i++) {   
        if(strcmp(tagged_places.places[i].label,"elevator_act")==0){
            double dist_to_ele = 10;
            int closest_ele_ind = -1;
      
            for (int j = 0; j < elevators.place_count; j++) {   
                //search and calthe dist from this to the closest same floor elevators 
                if(tagged_places.places[i].floor_no != elevators.places[j].floor_no){
                    continue;
                }
                else{
                    double temp_dist = hypot(tagged_places.places[i].x - elevators.places[j].x,
                                             tagged_places.places[i].y - elevators.places[j].y);
                    if(temp_dist < dist_to_ele){
                        dist_to_ele = temp_dist;
                        closest_ele_ind = j;
                    }
                }	
            }
            if(dist_to_ele < 2.0){ //not sure which value to use - might be useful to use visibility 
                fprintf(stderr,"Found the same elevator\n");
                elevators.places[closest_ele_ind].x  = (elevators.places[closest_ele_ind].x + tagged_places.places[i].x)/2;
                elevators.places[closest_ele_ind].y  = (elevators.places[closest_ele_ind].y + tagged_places.places[i].y)/2;
            }
            else{
                fprintf(stderr,"Coppied Over elevator locations\n");
                elevators.place_count++;
                elevators.places = (erlcm_tagged_node_t *) realloc(elevators.places, sizeof(erlcm_tagged_node_t) * elevators.place_count);
                memcpy(&elevators.places[elevators.place_count-1], &tagged_places.places[i] , sizeof(erlcm_tagged_node_t));
                elevators.places[elevators.place_count-1].label = strdup("elevator"); //reset the name as elevator
            }
        }
    }

    //now search for labeled elevators
    for (int i = 0; i < tagged_places.place_count; i++) {   
        if(strcmp(tagged_places.places[i].label,"elevator")==0){
            double dist_to_ele = 10;
            int closest_ele_ind = -1;
      
            for (int j = 0; j < elevators.place_count; j++) {   
                //search and calthe dist from this to the closest same floor elevators 
                if(tagged_places.places[i].floor_no != elevators.places[j].floor_no){
                    continue;
                }
                else{
                    double temp_dist = hypot(tagged_places.places[i].x - elevators.places[j].x,
                                             tagged_places.places[i].y - elevators.places[j].y);
                    if(temp_dist < dist_to_ele){
                        dist_to_ele = temp_dist;
                        closest_ele_ind = j;
                    }
                }	
            }
            if(dist_to_ele < 2.0){ //not sure which value to use - might be useful to use visibility 
                fprintf(stderr,"Found the same elevator - ignoring the label\n");
            }
            else{
                fprintf(stderr,"No matching elevator found - Coppied Over elevator locations\n");
                elevators.place_count++;
                elevators.places = (erlcm_tagged_node_t *) realloc(elevators.places, sizeof(erlcm_tagged_node_t) * elevators.place_count);
                memcpy(&elevators.places[elevators.place_count-1], &tagged_places.places[i] , sizeof(erlcm_tagged_node_t));
            }
        }
    }

    
    /*for (int i = 0; i < tagged_places.place_count; i++) {   
    //check for doors if there is an elevator nearby
    if(strcmp(tagged_places.places[i].label,"elevator_act")==0){
    fprintf(stderr,"Found elevator - Checking for Doors\n");
    double elevator_loc[2] = {tagged_places.places[i].x,tagged_places.places[i].y};
    detectDoors(gridmap[tagged_places.places[i].floor_index],elevator_loc, tagged_places.places[i].floor_index); 
    }
    }*/

    //create a message to hold the elevator info
    erlcm_elevator_list_t elevator_msg;
    elevator_msg.utime = bot_timestamp_now();
    elevator_msg.count = elevators.place_count;
    elevator_msg.elevators = (erlcm_elevator_node_t *) malloc(sizeof(erlcm_elevator_node_t)* elevator_msg.count);

    //note that the doors on the same elevator should be roughly on the same position - on the different floors

    for (int i = 0; i < elevators.place_count; i++) {   
        //check for doors if there is an elevator nearby
        fprintf(stderr,"++++ [%d] Found elevator (%f,%f) Floor : %d - Checking for Doors\n", i, 
                elevators.places[i].x,elevators.places[i].y, elevators.places[i].floor_no);
        double elevator_loc[2] = {elevators.places[i].x,elevators.places[i].y};

        memset(&elevator_msg.elevators[i],0, sizeof(erlcm_elevator_node_t));
        elevator_msg.elevators[i].floor_ind = elevators.places[i].floor_index;
        elevator_msg.elevators[i].floor_no = floor_map[elevators.places[i].floor_index];
        memcpy(elevator_msg.elevators[i].pos, elevator_loc, sizeof(double)*2);
        detectDoors(gridmap[elevators.places[i].floor_index],elevator_loc, elevators.places[i].floor_index, &elevator_msg.elevators[i]); 
    }

    //publish the elevator info
    erlcm_elevator_list_t_publish(lcm,"ELEVATOR_LIST", &elevator_msg);
    free(elevator_msg.elevators);
  
    //adjust the tagged node list
    erlcm_tagged_node_t * temp_nodes = (erlcm_tagged_node_t *) malloc(sizeof(erlcm_tagged_node_t) *tagged_places.place_count);
    int no_nodes = 0;
    for (int i = 0; i < tagged_places.place_count; i++) {   
        if(strcmp(tagged_places.places[i].label,"elevator")!=0 &&
           strcmp(tagged_places.places[i].label,"elevator_act")!=0){
            //copy over the node 
            no_nodes++;
            memcpy(&temp_nodes[no_nodes-1], &tagged_places.places[i], sizeof(erlcm_tagged_node_t));
            fprintf(stderr,"Coppied Over a node : %s\n", tagged_places.places[i].label);
        }
        else{
            fprintf(stderr,"Skipped the elevator nodes\n");
        }
    }

    //this list is now without the elevators

    fprintf(stderr,"Done displaying\n");
    temp_nodes = (erlcm_tagged_node_t *) realloc(temp_nodes, sizeof(erlcm_tagged_node_t) * no_nodes);
    tagged_places.places = (erlcm_tagged_node_t *) realloc(tagged_places.places, sizeof(erlcm_tagged_node_t) * no_nodes);
    tagged_places.place_count = no_nodes;
    memcpy(tagged_places.places, temp_nodes, sizeof(erlcm_tagged_node_t) * no_nodes);
    fprintf(stderr,"Done Coppying\n");
    free(temp_nodes);
    fprintf(stderr,"Done reallocating\n");
   
    //bot_lcmgl_switch_buffer(lcmgl_doors);
  
    erlcm_multi_gridmap_t lcm_full_map_msg;

    int utime = bot_timestamp_now();

    //message that holds the multi-floor map
    lcm_full_map_msg.no_floors = no_floors;
    lcm_full_map_msg.current_floor_ind = current_floor;  //this current floor is the index for the map 
    lcm_full_map_msg.maps = (erlcm_floor_gridmap_t *)malloc(no_floors * sizeof(erlcm_floor_gridmap_t));
    for(int m=0;m< no_floors; m++){
        erlcm_gridmap_t * lcm_msg = gridmap[m]->get_comp_gridmap_t();//->get_gridmap_t();

        //lcm_full_map_msg.maps[m].gridmap = erlcm_gridmap_t_copy(lcm_msg);
        memcpy(&lcm_full_map_msg.maps[m].gridmap,lcm_msg, sizeof(erlcm_gridmap_t));
    
        lcm_full_map_msg.maps[m].floor_no = floor_map[m];
        if(m == current_floor){
            erlcm_floor_gridmap_t_publish(lcm, "FINAL_FLOOR_SLAM", &lcm_full_map_msg.maps[m]);
        }
    }
    //erlcm_gridmap_t_publish(lcm, GMAPPER_GRIDMAP_CHANNEL, lcm_msg);
    erlcm_multi_gridmap_t_publish(lcm, "FINAL_MULTI_SLAM", &lcm_full_map_msg);
}

void IsamSlam::initializeLocalize()
{
    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);
  
    Pose2d slam_pose = trajectory.back()->pose2d_node->value();
    int x = 0, y = 0;
    //gridmap->worldToTable(slam_pose.x(), slam_pose.y(), &x, &y); //get the location in map coordinates
    fprintf(stderr,"Robot Location (%f,%f) => (%d,%d)\n", slam_pose.x(), slam_pose.y(), x, y);
  
    erlcm_localize_reinitialize_cmd_t msg;
    msg.utime = bot_timestamp_now();
    msg.mean[0] = slam_pose.x();//x * gridmap->metersPerPixel;;
    msg.mean[1] = slam_pose.y();//y * gridmap->metersPerPixel;;
    msg.mean[2] = slam_pose.t();

    double v = 0.01;//self->particle_std * self->particle_std;
    msg.variance[0] = v;
    msg.variance[1] = v;
    msg.variance[2] = carmen_degrees_to_radians(0.1);
    erlcm_localize_reinitialize_cmd_t_publish(lcm, LOCALIZE_REINITIALIZE_CHANNEL, &msg);
  
    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);
}

void IsamSlam::addPersonLoc(int64_t create_time, float person_x, float person_y) //only call if the tagging was with respect to Guide/Both
{
    plist.trajectory_length++;
    plist.trajectory = (erlcm_node_t *)realloc(plist.trajectory,plist.trajectory_length*sizeof(erlcm_node_t));
    plist.trajectory[plist.trajectory_length-1].x = person_x;
    plist.trajectory[plist.trajectory_length-1].y = person_y;
    plist.trajectory[plist.trajectory_length-1].utime = create_time;
    fprintf(stderr,"\tPerson Loc (%f,%f)\n", person_x, person_y);
}

// ***************** Tourguide Functions ********************* //
void IsamSlam::addTaggedNode(int node_id, erlcm_tagged_node_t *new_pos) //add a node for a possible tagging
{ 
    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);

    if(poss_node !=NULL){
        erlcm_tagged_node_t_destroy(poss_node);
        poss_node = NULL;    
        //add this to possible node     
    }
    poss_node = erlcm_tagged_node_t_copy(new_pos);
    poss_node->node_id = node_id;

    fprintf(stderr, "Added Possible Node Name : %s , ID : %d \n", poss_node->label, poss_node->node_id);

    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);
}

void IsamSlam::confirmTaggedNode(char *name, int64_t create_time) //confirm the possible tagging node and add that to tagged_places list
{ 
    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);

    if (poss_node !=NULL){ //we can confirm this node 
        if(poss_node->utime == create_time){//same node
            tagged_places.place_count++;
            tagged_places.places = (erlcm_tagged_node_t *) realloc(tagged_places.places, 
                                                                   tagged_places.place_count*sizeof(erlcm_tagged_node_t));
            memcpy(&tagged_places.places[tagged_places.place_count-1],erlcm_tagged_node_t_copy(poss_node),sizeof(erlcm_tagged_node_t));      
            erlcm_tagged_node_t_destroy(poss_node);
      
            if(strcmp(tagged_places.places[tagged_places.place_count-1].label,"elevator_controller")==0){
                //if this is an elevator controller node - make sure that the perspective is from the wheelchair 
                tagged_places.places[tagged_places.place_count-1].view = strdup("wheelchair");
            }
            poss_node = NULL; //prevents the old node being added again by any chance

            //print the confirmed list 
            fprintf(stderr,"Node Confirmed : %s\n",tagged_places.places[tagged_places.place_count-1].label);

            fprintf(stderr,"========== Confirmed Nodes ===============\n");
            for(int i=0; i < tagged_places.place_count; i++){
                fprintf(stderr, "Confirmed Node Name : %s , ID : %d \n", tagged_places.places[i].label, tagged_places.places[i].node_id);
            }
            fprintf(stderr,"==========================================\n");
        }
    }  
    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);
}

void IsamSlam::addElevatorNode(int floor_no, int node_id) //confirm the possible tagging node and add that to tagged_places list
{ 
    for(int i=0; i < tagged_places.place_count; i++){
        if(tagged_places.places[i].node_id == node_id){
            fprintf(stderr,"Node already added - %s , %d\n",tagged_places.places[i].label, tagged_places.places[i].node_id);
            return;
        }
    }

    tagged_places.place_count++;
    tagged_places.places = (erlcm_tagged_node_t *) realloc(tagged_places.places, 
                                                           tagged_places.place_count*sizeof(erlcm_tagged_node_t));

    memset(&tagged_places.places[tagged_places.place_count-1], 0 , sizeof(erlcm_tagged_node_t));

    SlamPose * slampose = trajectory[node_id];
    tagged_places.places[tagged_places.place_count-1].utime = slampose->utime;
    tagged_places.places[tagged_places.place_count-1].label = strdup("elevator_act");//_act"; //to differentiate between tagged elevators - which we wont use for navigation
    tagged_places.places[tagged_places.place_count-1].type = strdup("place");
    tagged_places.places[tagged_places.place_count-1].view = strdup("wheelchair");
    tagged_places.places[tagged_places.place_count-1].pos = strdup("at");
    tagged_places.places[tagged_places.place_count-1].node_id = node_id;
    tagged_places.places[tagged_places.place_count-1].floor_index = getFloorIndex(floor_no);
    tagged_places.places[tagged_places.place_count-1].floor_no = floor_no;
    tagged_places.places[tagged_places.place_count-1].dir = strdup("none");
    tagged_places.places[tagged_places.place_count-1].prop = strdup("none");
    tagged_places.places[tagged_places.place_count-1].pron = strdup("none");


    fprintf(stderr,"Added an elevator node\n");
}


void IsamSlam::updateTaggedNodes() //update the x,y values with the latest wheelchair location estimates for the nodes
{//these locations are in the map co-ordinates - not the same as the one being draw by isam module
    if(!tagged_places.place_count)
        return;
  
    fprintf(stderr,"Updating Nodes => Node Count : %d \n", tagged_places.place_count);
    int i;
    for (i = 0; i < tagged_places.place_count; i++) {   
        int node_id = tagged_places.places[i].node_id;
        SlamPose * slampose = trajectory[node_id];
        Pose2d node_pos = slampose->pose2d_node->value();
        int x =0,y=0;
        fprintf(stderr,"Floor index : %d World (%f, %f)\n", slampose->floor_no, node_pos.x(),node_pos.y());
        //convert to map coordinates    
        int floor_id = getFloorIndex(slampose->floor_no);
    
        //gridmap[floor_id]->worldToTable(node_pos.x(),node_pos.y(),&x,&y);    
   
        //converting to global co-oridnates
        tagged_places.places[i].x = node_pos.x();//x * gridmap[floor_id]->metersPerPixel;//node_pos.x();//x * gridmap->metersPerPixel;
        tagged_places.places[i].y = node_pos.y();//y * gridmap[floor_id]->metersPerPixel; //node_pos.y();//y * gridmap->metersPerPixel;
        tagged_places.places[i].theta = node_pos.t();
        tagged_places.places[i].floor_index = floor_id;
        tagged_places.places[i].floor_no = floor_map[floor_id];

        fprintf(stderr,"World (%f, %f) => Map  : %f,%f\n", node_pos.x(),node_pos.y(), tagged_places.places[i].x, tagged_places.places[i].y);
        fprintf(stderr,"World Coordinates : %f,%f Floor : %d\n", node_pos.x(),node_pos.y(), tagged_places.places[i].floor_index);
    }
    fprintf(stderr,"Done Updating\n");
    /*if (useThreads)
      pthread_mutex_unlock(&trajectory_mutex);*/
}

//match the tagged nodes with the person location if the tagging was done in the tourguide perspective 
void IsamSlam::matchTaggedLocations(){
    if(!tagged_places.place_count)
        return;
    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);
    fprintf(stderr,"Matching Locations\n");
    //update the tagged nodes - i.e. the x,y are updated to the latest values (needed because of loop closure
    updateTaggedNodes();
  
    int i,j;
    fprintf(stderr,"===================================================================\n");
    //cycle through the confirmed tagged locations - prob should cycle through the confirmed locations
    for(i=0;i< tagged_places.place_count; i++){
        if(strcmp(tagged_places.places[i].view,"wheelchair")==0){
            //the x,y points are already correct - need to do nothing 
            fprintf(stderr,"Location : %s => (%f,%f) Prespective : Wheelchair\n", 
                    tagged_places.places[i].label, tagged_places.places[i].x, tagged_places.places[i].y);
        }
        else if ((strcmp(tagged_places.places[i].view,"guide")==0) || (strcmp(tagged_places.places[i].view,"both")==0)){
            //if not with respect to wheelchair look for the person location with the same time 
            int found = 0;
            for (j=0;j< plist.trajectory_length;j++){
                if(plist.trajectory[j].utime == tagged_places.places[i].utime){
                    erlcm_tagged_node_t tpos = tagged_places.places[i];
                    erlcm_node_t ppos = plist.trajectory[j];	  	  

                    double p_global_x, p_global_y, s,c;
                    s = sin(tpos.theta);
                    c = cos(tpos.theta);
                    p_global_x = tpos.x + ppos.x *c - ppos.y * s;
                    p_global_y = tpos.y + ppos.x *s + ppos.y * c;
                    tagged_places.places[i].x = p_global_x;
                    tagged_places.places[i].y = p_global_y;
	  
                    fprintf(stderr,"Location : %s => (%f,%f) Prespective : Guide (Node loc %f,%f,%f) , (Person Loc (relative) %f,%f) \n", 
                            tagged_places.places[i].label, tagged_places.places[i].x, tagged_places.places[i].y, 
                            tpos.x,tpos.y,tpos.theta, plist.trajectory[j].x,plist.trajectory[j].y);
                    found = 1;
                    break;		
                }
            }
            if(!found){
                fprintf(stderr, "Did not find person for Location %s \n",tagged_places.places[i].label);
            }
        }
    }
    fprintf(stderr,"===================================================================\n");
    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);
}

void IsamSlam::publishTaggedNodes()
{
    if(!tagged_places.place_count)
        return;
    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);

    bot_lcmgl_line_width(lcmgl_tagged, 1);
    bot_lcmgl_point_size(lcmgl_tagged, 10);
    bot_lcmgl_color3f(lcmgl_tagged, 1, 0, 0);  

    erlcm_place_list_t place_list;
    memset(&place_list,0,sizeof(place_list));
    place_list.trajectory = (erlcm_place_node_t *)calloc(tagged_places.place_count, sizeof(erlcm_place_node_t));
    memset(place_list.trajectory,0,tagged_places.place_count*sizeof(erlcm_place_node_t));
  
    place_list.place_count = tagged_places.place_count;
    int i;
    fprintf(stderr,"=========== Publishing Places ===========\n");
    for (i = 0; i < tagged_places.place_count; i++) {   
        place_list.trajectory[i].name = tagged_places.places[i].label;
        place_list.trajectory[i].type = strdup("");
        place_list.trajectory[i].x = tagged_places.places[i].x;
        place_list.trajectory[i].y = tagged_places.places[i].y;
        place_list.trajectory[i].theta = tagged_places.places[i].theta; //theta is published 
        fprintf(stderr,"\tName: %s, (%f,%f)\n",place_list.trajectory[i].name, place_list.trajectory[i].x, place_list.trajectory[i].y);
        double global_to_local[12];
        if (!bot_frames_get_trans_mat_3x4 (frames, "global",
                                           "local",  
                                           global_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            return;        
        }
        double pose_b[3] = {place_list.trajectory[i].x, place_list.trajectory[i].y, 0.2}, pose_l[3];
        bot_vector_affine_transform_3x4_3d (global_to_local, 
                                            pose_b, 
                                            pose_l);
    
        //if(tagged_places.places[i].floor_index == current_floor)
        bot_lcmgl_circle(lcmgl_tagged, pose_l, .3);
    }
    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);

    bot_lcmgl_switch_buffer(lcmgl_tagged);
    //erlcm_place_list_t_publish(lcm, "MATCHED_TAGGED_PLACELIST",&place_list);
    erlcm_tagged_node_list_t_publish(lcm, "TAGGED_NODES",&tagged_places);
    free(place_list.trajectory);
}

void IsamSlam::renderCurrentGridmap()
{

    checkForUpdates();

    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);
    sm_tictoc("renderGridmap_nolocking");
    for (unsigned t = 0; t < trajectory.size(); t++) {
        SlamPose * slampose = trajectory[t];
        if (slampose->rendered)
            continue;
        if(getFloorIndex(slampose->floor_no) != current_floor)
            continue; //skip rendering nodes on other floors
        else
            slampose->rendered = true;
        Pose2d bodyPos = slampose->pose2d_node->value();
        double bodyP[2] = { bodyPos.x(), bodyPos.y() };
        //    slampose->updateScan();//update the scan to match the current slam pos
        int map_ind = getFloorIndex(slampose->floor_no);

        for (unsigned s = 0; s < slampose->allScans.size(); s++) {
            if (s > 0 && trajectory.size() - t > 3)
                continue; //only draw maxranges for last 3 scans
            Scan * scan = slampose->allScans[s];
            //ray trace to compute the map...
            sm_tictoc("render_raytrace");
            for (unsigned i = 0; i < scan->numPoints; i++) {
                gridmap[map_ind]->rayTrace(bodyP, smPoint_as_array(&scan->ppoints[i]), (s == 0), 1, false);
            }
            if (s == 0) {
                //draw the contours as lines for cleaner walls
                for (unsigned cidx = 0; cidx < scan->contours.size(); cidx++) {
                    for (unsigned i = 0; i + 1 < scan->contours[cidx]->points.size(); i++) {
                        //draw the occupied regions
                        smPoint p0 = scan->contours[cidx]->points[i];
                        smPoint p1 = scan->contours[cidx]->points[i + 1];
                        gridmap[map_ind]->rayTrace(smPoint_as_array(&p0), smPoint_as_array(&p1), 1, 0, true);
                    }
                }
            }

            sm_tictoc("render_raytrace");
        }
        gridmap[current_floor]->generate_likelihoods();
        //clear values 
        for (unsigned int i = 0; i < trajectory.size(); i++) {    
            SlamPose * slampose = trajectory[i];
            if(getFloorIndex(slampose->floor_no) != current_floor){
                continue; //skip rendering nodes on other floors
            }
            Pose2d value = slampose->pose2d_node->value();
            int lxy[2];
            double gxy[2] = {value.x(), value.y()};
            gridmap[current_floor]->worldToTable(gxy, lxy);

            int x = lxy[0];
            int y = lxy[1];
    
            int j,k;
            double s = sin(value.t());
            double c = cos(value.t());
    
            for(j=-10;j<11;j++){
                for(k=-10;k<11;k++){
                    int curr_x,curr_y;
                    curr_x = x + j;
                    curr_y = y + k;
	
                    double map_x,map_y;
                    map_x = j*gridmap[current_floor]->metersPerPixel;
                    map_y = k*gridmap[current_floor]->metersPerPixel;

                    double local_x,local_y;
                    local_x = map_x *c + map_y*s;
                    local_y = - map_x*s + map_y*c;

                    if((local_x <= 0.5 &&  local_x >= -0.5) && (local_y <= 0.25 &&  local_y >= -0.25)){//ideally we can do a better job of cleaning up the map 
                        int ixy[2] = {curr_x,curr_y};
                        gridmap[current_floor]->clearValue(ixy);
                    }
                }
            }
        }
    
    }
    sm_tictoc("renderGridmap_nolocking");

    //  cvSaveImage("gridmap.bmp", &gridmap->distim);
    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);

}


void IsamSlam::checkForRegionMapUpdates()
{
    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);
    
    double minxy[2] = { DBL_MAX, DBL_MAX };
    double maxxy[2] = { -DBL_MAX, -DBL_MAX };
    
    bool needToDelete = true;
    
    for (unsigned t = 0; t < trajectory.size(); t++) {
        if(trajectory[t]->region_id != current_region)
            continue;

        int updated = trajectory[t]->updateScan();//update the scan to match the current slam pos
        
        if (updated && trajectory[t]->rendered)
            needToDelete = true;
        
        for (unsigned j = 0; j < trajectory[t]->allScans.size(); j++) {
            Scan * s = trajectory[t]->allScans[j];
            
            for (unsigned cidx = 0; cidx < s->contours.size(); cidx++) {
                for (unsigned k = 0; k < s->contours[cidx]->points.size(); k++) {
                    smPoint p = s->contours[cidx]->points[k];
                    minxy[0] = fmin(minxy[0], p.x);
                    maxxy[0] = fmax(maxxy[0], p.x);
                    minxy[1] = fmin(minxy[1], p.y);
                    maxxy[1] = fmax(maxxy[1], p.y);
                }
            }
        }
    }
        
    //check if the map needs to be increased 
    if (regionmap != NULL) {
        if (minxy[0] < regionmap->xy0[0] || maxxy[0] > regionmap->xy1[0] || minxy[1] < regionmap->xy0[1] || maxxy[1]
            > regionmap->xy1[1])
            needToDelete = true;
        
        if (needToDelete) {
            delete regionmap;
            regionmap = NULL;
            //mark all floor poses as unrendered 
            for (unsigned t = 0; t < trajectory.size(); t++){
                if(trajectory[t]->region_id != current_region)
                    continue;
                //trajectory[t]->rendered = false;
            }
        }
    }
    
    if (regionmap == NULL) {
        printf("creating new Regionmap\n");
        minxy[0] -= gridmapMargin;
        minxy[1] -= gridmapMargin;
        maxxy[0] += gridmapMargin;
        maxxy[1] += gridmapMargin;
        regionmap = new GridMap(minxy, maxxy, gridmapMetersPerPixel);
    }
    
    printf("Done");
    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);
    
}

void IsamSlam::renderRegionGridmap()
{
    checkForRegionMapUpdates();

    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);
    sm_tictoc("renderRegionGridmap_nolocking");

    
    for (unsigned t = 0; t < trajectory.size(); t++) {
        if(trajectory[t]->region_id != current_region)
            continue;
        SlamPose * slampose = trajectory[t];

        //fprintf(stderr, "Curr Ind : %d => Curr Region : %d\n", t , current_region);
        //if (slampose->rendered)
        //  continue;
        if(getFloorIndex(slampose->floor_no) != current_floor)
            continue; //skip rendering nodes on other floors
        //else
        //  slampose->rendered = true;
        
        Pose2d bodyPos = slampose->pose2d_node->value();
        double bodyP[2] = { bodyPos.x(), bodyPos.y() };
        int map_ind = getFloorIndex(slampose->floor_no);

        for (unsigned s = 0; s < slampose->allScans.size(); s++) {
            if (s > 0 && trajectory.size() - t > 3)
                continue; //only draw maxranges for last 3 scans
            Scan * scan = slampose->allScans[s];
            //ray trace to compute the map...
            sm_tictoc("render_raytrace");
            for (unsigned i = 0; i < scan->numPoints; i++) {
                regionmap->rayTrace(bodyP, smPoint_as_array(&scan->ppoints[i]), (s == 0), 1, false);
            }
            if (s == 0) {
                //draw the contours as lines for cleaner walls
                for (unsigned cidx = 0; cidx < scan->contours.size(); cidx++) {
                    for (unsigned i = 0; i + 1 < scan->contours[cidx]->points.size(); i++) {
                        //draw the occupied regions
                        smPoint p0 = scan->contours[cidx]->points[i];
                        smPoint p1 = scan->contours[cidx]->points[i + 1];
                        regionmap->rayTrace(smPoint_as_array(&p0), smPoint_as_array(&p1), 1, 0, true);
                    }
                }
            }

            sm_tictoc("render_raytrace");
        }

        regionmap->generate_likelihoods();
        //clear values 
        for (unsigned int i = 0; i < trajectory.size(); i++) {  
            if(trajectory[i]->region_id != current_region)
                continue;
            SlamPose * slampose = trajectory[i];
            if(getFloorIndex(slampose->floor_no) != current_floor){
                continue; //skip rendering nodes on other floors
            }
            Pose2d value = slampose->pose2d_node->value();
            int lxy[2];
            double gxy[2] = {value.x(), value.y()};
            regionmap->worldToTable(gxy, lxy);

            int x = lxy[0];
            int y = lxy[1];
    
            int j,k;
            double s = sin(value.t());
            double c = cos(value.t());
    
            for(j=-10;j<11;j++){
                for(k=-10;k<11;k++){
                    int curr_x,curr_y;
                    curr_x = x + j;
                    curr_y = y + k;
	
                    double map_x,map_y;
                    map_x = j*regionmap->metersPerPixel;
                    map_y = k*regionmap->metersPerPixel;

                    double local_x,local_y;
                    local_x = map_x *c + map_y*s;
                    local_y = - map_x*s + map_y*c;

                    if((local_x <= 0.5 &&  local_x >= -0.5) && (local_y <= 0.25 &&  local_y >= -0.25)){//ideally we can do a better job of cleaning up the map 
                        int ixy[2] = {curr_x,curr_y};
                        regionmap->clearValue(ixy);
                    }
                }
            }
        }
    }
    
    sm_tictoc("renderRegionGridmap_nolocking");

    //  cvSaveImage("gridmap.bmp", &gridmap->distim);
    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);

}

void IsamSlam::publishRegionGridMap()
{    
    if(!regionmap){
        fprintf(stderr,"No region map\n");
        return;
    }
    erlcm_gridmap_t * lcm_msg = regionmap->get_comp_gridmap_t();
    lcm_msg->utime = bot_timestamp_now();
    erlcm_gridmap_t_publish(lcm, GMAPPER_GRIDMAP_CHANNEL, lcm_msg);
}

void IsamSlam::publishCurrentGridMap()
{    
    /*erlcm_gridmap_t * lcm_msg = gridmap[current_floor]->get_comp_gridmap_t();
      lcm_msg->utime = bot_timestamp_now();
      erlcm_gridmap_t_publish(lcm, GMAPPER_GRIDMAP_CHANNEL, lcm_msg);*/
}

void IsamSlam::publishFinalGridMap()
{  
    erlcm_gridmap_t * lcm_msg = gridmap[current_floor]->get_comp_gridmap_t();
    lcm_msg->utime = bot_timestamp_now();
    erlcm_gridmap_t_publish(lcm, "FINAL_SLAM", lcm_msg);

    //erlcm_floor_gridmap_t lcm_f_msg;
    //lcm_f_msg.gridmap
  
}

void IsamSlam::drawGraph()
{

    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);

    double usPerMeter = 60 * 1e6;
  
    bot_lcmgl_t **lcmgl_graph;

    lcmgl_graph = (bot_lcmgl_t **) malloc(sizeof(bot_lcmgl_t *)*no_floors);
    for(int i=0;i<no_floors; i++){
        char lcmgl_text[100];
        sprintf(lcmgl_text,"isam_graph_%d", floor_map[i]);
        lcmgl_graph[i] = bot_lcmgl_init(lcm,lcmgl_text);
        bot_lcmgl_line_width(lcmgl_graph[i], 1);
        bot_lcmgl_point_size(lcmgl_graph[i], 10);
        bot_lcmgl_color3f(lcmgl_graph[i], 1, 0, 0);

        /*bot_lcmgl_color4f(lcmgl_graph[i], pallet[i].c[0], 
          pallet[i].c[1], 
          pallet[i].c[2], 
          pallet[i].c[3]);*/
    }

    double global_to_local[12];
    if (!bot_frames_get_trans_mat_3x4 (frames, "global",
                                       "local",  
                                       global_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;        
    }

    char pose_info[512];
    //draw nodes
    for (unsigned i = 0; i < trajectory.size(); i++) {
        SlamPose * slampose = trajectory[i];
        int t_floor_ind = getFloorIndex(slampose->floor_no);
        Pose2d value = slampose->pose2d_node->value();
        double xyz[3] = { value.x(), value.y(), 0};//(trajectory[i]->utime - trajectory[0]->utime) / usPerMeter };
        double pose_l[3];
        bot_vector_affine_transform_3x4_3d (global_to_local, 
                                            xyz, 
                                            pose_l);
        bot_lcmgl_circle(lcmgl_graph[t_floor_ind], pose_l, .3);

        sprintf(pose_info,"%d-%d", i, trajectory[i]->region_id);

        if(trajectory[i]->region_id >=0){
            bot_lcmgl_color4f(lcmgl_graph[t_floor_ind], pallet[trajectory[i]->region_id].c[0], 
                              pallet[trajectory[i]->region_id].c[1], 
                              pallet[trajectory[i]->region_id].c[2], 
                              pallet[trajectory[i]->region_id].c[3]);
        }
        else{
            bot_lcmgl_color3f(lcmgl_graph[t_floor_ind], 
                              1,0,0);            
        }
        bot_lcmgl_circle(lcmgl_graph[t_floor_ind], pose_l, .3);
        bot_lcmgl_text(lcmgl_graph[t_floor_ind], pose_l, pose_info);
    }

    //draw odometry edges
    for(int i=0;i<no_floors; i++){
        bot_lcmgl_color3f(lcmgl_graph[i], 0, 1, 1);
        bot_lcmgl_line_width(lcmgl_graph[i], 3);
        bot_lcmgl_begin(lcmgl_graph[i], GL_LINES);
    }
  
    for (unsigned i = 1; i < trajectory.size(); i++) {
        SlamPose * slampose_prev = trajectory[i - 1];
        SlamPose * slampose_curr = trajectory[i];
        int t_floor_ind = getFloorIndex(slampose_curr->floor_no);
        if(getFloorIndex(slampose_curr->floor_no) != getFloorIndex(slampose_prev->floor_no))
            continue; //skip when the prev node was from a different floor
        Pose2d value_prev = slampose_prev->pose2d_node->value();
        Pose2d value_curr = slampose_curr->pose2d_node->value();

        double p_pose_g[3] = {value_prev.x(), value_prev.y(), 0}, p_pose_l[3], c_pose_g[3] = {value_curr.x(), value_curr.y(), 0} , c_pose_l[3];
    
        bot_vector_affine_transform_3x4_3d (global_to_local, 
                                            p_pose_g, 
                                            p_pose_l);

        bot_vector_affine_transform_3x4_3d (global_to_local, 
                                            c_pose_g, 
                                            c_pose_l);
    
        bot_lcmgl_vertex3f(lcmgl_graph[t_floor_ind], p_pose_l[0], p_pose_l[1], 0);//(slampose_prev->utime - trajectory[0]->utime)
        //    / usPerMeter);
        bot_lcmgl_vertex3f(lcmgl_graph[t_floor_ind], c_pose_l[0], c_pose_l[1], 0);//(slampose_curr->utime - trajectory[0]->utime)
        //  / usPerMeter);
    }
    for(int i=0;i<no_floors; i++){
        bot_lcmgl_end(lcmgl_graph[i]);
        bot_lcmgl_line_width(lcmgl_graph[i], 3);
        bot_lcmgl_color3f(lcmgl_graph[i], 1, 0, 0);
    }
    //draw loop closures
  
    for (unsigned i = 1; i < trajectory.size(); i++) {
        SlamPose * slampose = trajectory[i];
        int t_floor_ind = getFloorIndex(slampose->floor_no);
        for (unsigned j = 1; j < slampose->constraint_ids.size(); j++) { //constraints after the first are loop closures
            SlamPose * slampose2 = trajectory[slampose->constraint_ids[j]];

            Pose2d_Node* pose1node = slampose->pose2d_node;
            Pose2d_Node* pose2node = slampose2->pose2d_node;
            Pose2d value1 = pose1node->value();
            Pose2d value2 = pose2node->value();
            bot_lcmgl_begin(lcmgl_graph[t_floor_ind], GL_LINES);

            double p_s_g[3] = {value1.x(), value1.y(), 0}, p_e_g[3] = {value2.x(), value2.y(), };
            double p_s_l[3], p_e_l[3];
      
            bot_vector_affine_transform_3x4_3d (global_to_local, 
                                                p_s_g, 
                                                p_s_l);
      
            bot_vector_affine_transform_3x4_3d (global_to_local, 
                                                p_e_g, 
                                                p_e_l);
      
            bot_lcmgl_vertex3f(lcmgl_graph[t_floor_ind], p_s_l[0], p_s_l[1], 0);//(slampose->utime - trajectory[0]->utime) / usPerMeter);
            bot_lcmgl_vertex3f(lcmgl_graph[t_floor_ind], p_e_l[0], p_e_l[1], 0);//(slampose2->utime - trajectory[0]->utime) / usPerMeter);
            bot_lcmgl_end(lcmgl_graph[t_floor_ind]);
        }
    }

    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);

    for(int i=0;i<no_floors; i++){
        bot_lcmgl_switch_buffer(lcmgl_graph[i]);
    }
}

void IsamSlam::drawMap()
{

    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);

    bot_lcmgl_line_width(lcmgl_map, 3);

    for (unsigned i = 0; i < trajectory.size(); i++) {
        SlamPose * slampose = trajectory[i];
        slampose->updateScan();//update the scan to match the current slam pos
        for (unsigned s = 0; s < 1; s++) {
            //      for (unsigned s = 0; s < slampose->allScans.size(); s++) {
            Scan * scan = slampose->allScans[s];
            if (s == 0)
                bot_lcmgl_color3f(lcmgl_map, 0, 0, 1);
            else
                bot_lcmgl_color3f(lcmgl_map, 1, 1, 0);

            //actually draw the contours for this node
            for (unsigned cidx = 0; cidx < scan->contours.size(); cidx++) {
                for (unsigned i = 0; i + 1 < scan->contours[cidx]->points.size(); i++) {
                    bot_lcmgl_begin(lcmgl_map, GL_LINES);
                    bot_lcmgl_vertex3f(lcmgl_map, scan->contours[cidx]->points[i].x, scan->contours[cidx]->points[i].y, 0);
                    bot_lcmgl_vertex3f(lcmgl_map, scan->contours[cidx]->points[i + 1].x, scan->contours[cidx]->points[i + 1].y, 0);
                    bot_lcmgl_end(lcmgl_map);
                }
            }
        }
    }

    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);

    bot_lcmgl_switch_buffer(lcmgl_map);
}

void IsamSlam::draw3DPointCloud()
{
    checkForUpdates();
    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);

    double blue_height = .2;
    double red_height = 3;
    double z_norm_scale = 1 / (red_height - blue_height);

    bot_lcmgl_push_attrib(lcmgl_point_cloud, GL_DEPTH_BUFFER_BIT | GL_POINT_BIT | GL_CURRENT_BIT);
    bot_lcmgl_enable(lcmgl_point_cloud, GL_DEPTH_TEST);
    bot_lcmgl_depth_func(lcmgl_point_cloud, GL_LESS);

    bot_lcmgl_point_size(lcmgl_point_cloud, 2);
    bot_lcmgl_begin(lcmgl_point_cloud, GL_POINTS);

    for (unsigned t = 0; t < trajectory.size(); t++) {
        SlamPose * slampose = trajectory[t];
        Pose2d value = slampose->pose2d_node->value();
        Scan * scan = slampose->scan;
        double pBody[3] = { 0, 0, 0 };
        double pLocal[3];
        BotTrans bodyToLocal;
        bodyToLocal.trans_vec[0] = value.x();
        bodyToLocal.trans_vec[1] = value.y();
        bodyToLocal.trans_vec[2] = slampose->height;
        double rpy[3] = { slampose->rp[0], slampose->rp[1], value.t() };
        bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
        for (unsigned i = 0; i < scan->numPoints; i++) {
            pBody[0] = scan->points[i].x;
            pBody[1] = scan->points[i].y;
            //transform to local frame
            bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
            double z_norm = (pLocal[2] - blue_height) * z_norm_scale;
            float * color3fv = bot_color_util_jet(z_norm);
            bot_lcmgl_color3f(lcmgl_point_cloud, color3fv[0], color3fv[1], color3fv[2]);
            bot_lcmgl_vertex3f(lcmgl_point_cloud, pLocal[0], pLocal[1], pLocal[2]);
        }
    }

    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);

    bot_lcmgl_end(lcmgl_point_cloud);
    bot_lcmgl_pop_attrib(lcmgl_point_cloud);
    bot_lcmgl_switch_buffer(lcmgl_point_cloud);

}

double IsamSlam::computeTrajectoryLength()
{

    if (useThreads)
        pthread_mutex_lock(&trajectory_mutex);

    double trajLength = 0;
    //draw odometry edges
    for (unsigned i = 1; i < trajectory.size(); i++) {
        SlamPose * slampose_prev = trajectory[i - 1];
        SlamPose * slampose_curr = trajectory[i];
        Pose2d value_prev = slampose_prev->pose2d_node->value();
        Pose2d value_curr = slampose_curr->pose2d_node->value();
        double pp[2] = { value_prev.x(), value_prev.y() };
        double pc[2] = { value_curr.x(), value_curr.y() };
        double dist = bot_vector_dist_2d(pp, pc);
        trajLength += dist;
    }
    if (useThreads)
        pthread_mutex_unlock(&trajectory_mutex);

    return trajLength;
}
