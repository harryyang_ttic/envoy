/*
 * IsamSlam.h
 *
 *  Created on: Oct 22, 2009
 *      Author: abachrac
 */

#ifndef ISAMSLAM_H_
#define ISAMSLAM_H_
//#include <bot/scanmatch/ScanMatcher.hpp>
#include <scanmatch/ScanMatcher.hpp>
#include <isam/isam.h>
#include "SlamPose.h"
#include <bot_core/bot_core.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <pthread.h>
#include <er_lcmtypes/lcm_channel_names.h>
//#include <carmen3d/GridMap.hpp>
//now calling the modified library
#include <simpleMMap/GridMap.hpp>
#include <vector>
#include <bot_frames/bot_frames.h>

// batch solve with variable reordering and relinearization every MOD_BATCH steps
const int MOD_BATCH = 100;
// for incremental steps, solve by backsubstitution every MOD_SOLVE steps
const int MOD_SOLVE = 10;

using namespace std;
using namespace scanmatch;

typedef struct{
    float c[4]; 
} color_t;

class IsamSlam {
public:
  IsamSlam(lcm_t * _lcm, double metersPerPixel_, double thetaResolution_, int useGradientAscentPolish_, int useMultires_,
           int doDrawing_, bool useThreads_, BotFrames *_frames);
  virtual ~IsamSlam();

  void initializeLocalize();


  void doLoopClosing(int loop_close_ind);
  void addNodeToSlam(Pose2d &prev_curr_tranf, Noise &cov, Scan * scan, 
		     Scan * maxRangeScan, int64_t utime,
		     double height, double rp[2],int floor_no);

  void addNodeToSlam(Pose2d &prev_curr_tranf, Noise &cov, Scan * scan, 
		     Scan * maxRangeScan, int64_t utime,
		     double height, double rp[2], int floor_no, int region_id);

  Pose2d getCurrentPose(){
    if (useThreads)
       pthread_mutex_lock(&trajectory_mutex);
    Pose2d curr_pose;
    if (!trajectory.empty())
      curr_pose = trajectory.back()->pose2d_node->value();
    else
      curr_pose = origin_node->value();
    if (useThreads)
      pthread_mutex_unlock(&trajectory_mutex);
    return curr_pose;
  }

  void addTaggedNode(int node_id, erlcm_tagged_node_t *poss_node);
  void confirmTaggedNode(char *name, int64_t create_time);
  void matchTaggedLocations();
  void updateTaggedNodes();
  void publishTaggedNodes();	
  void addPersonLoc(int64_t create_time, float person_x, float person_y);

  void publishSlamPos();
  void publishSlamTrajectory();
  void addNewFloor(int current_floor);
  void updateNewFloor(int current_floor);
  int getFloorIndex(int floor_no);

  double computeTrajectoryLength();

  void renderRegionGridmap();
  void publishRegionGridMap();
  void checkForRegionMapUpdates();
  void updateNodeRegions(erlcm_slam_graph_node_list_t *node_list);

  //gridmap rendering functions
  void checkForUpdates();
  void renderMultiGridmap();
  void publishMultiGridMap();
  void publishFinalMultiGridMap();
  void publishFinalGridMap();
  void addElevatorNode(int floor_no, int node_id);
  void renderCurrentGridmap();
  void publishCurrentGridMap();
  void publishClearedCurrentGridMap();
  void drawGraph();
  void drawMap();
  void draw3DPointCloud();
  void clearRobotPose(GridMap *gridmap, double gxy[2], double theta);
  void detectDoors(GridMap *gridmap, double gxy[2], int floor_no, erlcm_elevator_node_t *curr_ele);
  void publishGraphNode(int i);
  void publishGraphTransforms();
  void publishSlamGraph(int64_t utime);

  BotFrames *frames;
  Pose2d_Node * origin_node;
  ScanMatcher * sm; //scan matcher object for loop closing
  Slam * slam; //isam slam object
  std::vector<SlamPose *> trajectory;
  int loopClosedUpTill;

  GridMap * regionmap;
  GridMap ** gridmap;
  int * floor_map;
  int no_floors;
  int current_floor;
  double gridmapMetersPerPixel;
  double gridmapMargin;
  unsigned int kernelSize;
  unsigned char * squareKernel;
  unsigned char * lineKernel;
  unsigned char * diagLineKernel;
  float gridmapFreeIncrement;

  //tour tagging 
  erlcm_node_list_t plist;
  erlcm_tagged_node_t *poss_node;
  erlcm_tagged_node_list_t tagged_places;

  int doDrawing;

  lcm_t * lcm;
  //bot_lcmgl_t * lcmgl_graph;
  bot_lcmgl_t * lcmgl_doors;
  bot_lcmgl_t * lcmgl_tagged;
  bot_lcmgl_t * lcmgl_map;
  bot_lcmgl_t * lcmgl_points;
  bot_lcmgl_t * lcmgl_point_cloud;

  //threading stuff
  bool useThreads;
  int killThread;
  pthread_t loop_closer;
  static void * loopClose_thread_func(IsamSlam*parent);
  pthread_mutex_t trajectory_mutex;
  pthread_cond_t loop_closer_cond;
  int current_region;
  int previous_region;
  color_t *pallet;
};

#endif /* ISAMSLAM_H_ */
