/*
 * bot_loop_closer.cpp
 *
 *  Created on: Oct 22, 2009
 *      Author: abachrac
 */
//local stuff
#include "IsamSlam.h"

#include <iostream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include <opencv/highgui.h>
#include <getopt.h>

#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
//bot param stuff
#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>

#include <scanmatch/ScanMatcher.hpp>
#include <lcmtypes/sm_rigid_transform_2d_t.h>
#include <bot_frames/bot_frames.h>
#include <lcmtypes/er_lcmtypes.h>

using namespace std;
using namespace scanmatch;
using namespace Eigen;

//add a new node every this many meters
#define LINEAR_DIST_TO_ADD 1.0
//and or this many radians
#define ANGULAR_DIST_TO_ADD 1.0
#define PERSON_CLEAR_RADIUS 0.45 //distance from the person that is cleared
#define LASER_DATA_CIRC_SIZE 40

//multi-floor stuff
typedef struct _floor_t{
    int current_floor;
    int prev_floor;
    int floor_change;
    GHashTable *floor_hash;
} floor_t;

//multi-floor stuff
typedef struct _region_t{
    int current_region;
    int prev_region;
    //int floor_count;
    //int *floors;
    int region_change;
    GHashTable *region_hash;
} region_t;

typedef struct {
    lcm_t * lcm;
    IsamSlam * isamslam;
    ScanMatcher * sm;
    ScanMatcher * sm_incremental;
    BotFrames *frames;
    BotParam *param;
    int optimize;
    int scanmatchBeforAdd;
    double odometryConfidence;
    int publish_pose;
    int do_drawing;
    int useOdom; 
    int draw_map;
    char * chan;
    char * rchan;
    int beam_skip; //downsample ranges by only taking 1 out of every beam_skip points
    double spatialDecimationThresh; //don't discard a point if its range is more than this many std devs from the mean range (end of hallway)
    double maxRange; //discard beams with reading further than this value
    double maxUsableRange; //only draw map out to this far for MaxRanges...
    float validBeamAngles[2]; //valid part of the field of view of the laser in radians, 0 is the center beam
    sm_rigid_transform_2d_t * prev_odom; //odometry computed outside module
    sm_rigid_transform_2d_t * prev_sm_odom; //internally scan matched odometry
    int verbose;

    BotPtrCircular   *front_laser_circ;
    BotPtrCircular   *rear_laser_circ;

    bot_core_planar_lidar_t * r_laser_msg;
    bot_core_pose_t *last_deadreakon; 
    int64_t r_utime;

    int slave_mode; //in slave mode nodes are added on external trigger 
    int use_frames; 
    int region;

    //multi-floor stuff
    /*int current_floor;
    int prev_floor;
    int floor_count;
    int *floors;
    int floor_change;*/
    floor_t floor_info;
    region_t region_info;

    int publish_full_maps;
    int conclude_tour;
    //tour guide stuff
    char *mode;
    int possible_tagging;
    int64_t tag_utime;
    erlcm_tagged_node_t *tagged_node;
    erlcm_people_pos_msg_t * person_msg; //should be depricated 
    erlcm_guide_info_t *guide_msg;
} app_t;


void app_destroy(app_t *app);

Scan *get_maxrange_scan(float * ranges, int numPoints, double thetaStart, 
                        double thetaStep, double maxRange,
                        double maxUsableRange, double validRangeStart, double validRangeEnd)
{
    Scan * scan = new Scan();
    smPoint * points = (smPoint *) malloc(numPoints * sizeof(smPoint));
    scan->points = points;
    int numMaxRanges = 0;
    double theta = thetaStart;
    Contour * contour = new Contour();
    scan->contours.push_back(contour);
    for (int i = 0; i < numPoints; i++) {
        double r = ranges[i];
        if ((r <= 0.01) || (r >= maxRange)) {
            continue;
        }
        if (r < .1) {
            r = maxRange;
        }
        if (r >= maxRange && theta > validRangeStart && theta < validRangeEnd) {
            r = maxUsableRange;
            //project to body centered coordinates
            points[numMaxRanges].x = r * cos(theta);
            points[numMaxRanges].y = r * sin(theta);
            contour->points.push_back(points[numMaxRanges]);
            numMaxRanges++;

        }
        else if (contour->points.size() > 0) {
            //end that contour
            if (contour->points.size() > 2) {
                //clear out middle points
                contour->points[1] = contour->points.back();
                contour->points.resize(2);
            }
            contour = new Contour();
            scan->contours.push_back(contour);
        }
        else if (contour->points.size() == 1) {
            //discard single maxrange beams...
            contour->points.clear();
        }
        theta += thetaStep;
    }

    if (contour->points.size() < 2) {
        scan->contours.pop_back();
        delete contour;
    }

    points = (smPoint*) realloc(points, numMaxRanges * sizeof(smPoint));
    scan->numPoints = numMaxRanges;
    scan->ppoints = (smPoint *) malloc(numMaxRanges * sizeof(smPoint));
    memcpy(scan->ppoints, points, numMaxRanges * sizeof(smPoint));
    return scan;
}

Scan * get_person_free_scan(int numP, smPoint * points_, ScanTransform T_,
			    sm_laser_type_t laser_type_, int64_t utime_, bool buildContours, 
			    erlcm_guide_info_t* guide_msg)
{
    //reduce the points - remove person observation and then call Sacn() and return
    if(guide_msg->tracking_state!=1){
        return new Scan(numP,points_, T_, laser_type_, utime_, buildContours);
    }
  
    smPoint * points = (smPoint *) malloc(numP * sizeof(smPoint));
    int act_num_points = 0;
    smPoint personPoint = {guide_msg->pos[0], guide_msg->pos[1]};
    //iterate through the points 
    for(int i=0;i < numP; i++){
        if(sm_dist(&points_[i], &personPoint)< PERSON_CLEAR_RADIUS){ //skip
            continue;
        }
        points[act_num_points] = points_[i];
        act_num_points++;
    }
    return new Scan(act_num_points,points, T_, laser_type_, utime_, buildContours);
}

static void conclude_tour(app_t * app){//end of map building   
    //render the gridmap
    if (app->optimize) {
        sm_tictoc("SlamIncrementalOptim");
        app->isamslam->slam->update();
        sm_tictoc("SlamIncrementalOptim");
    }

    sm_tictoc("publishDraw");

    //publish the SLAM_POS
    sm_tictoc("publishSlamPos");
    app->isamslam->publishSlamPos();
    sm_tictoc("publishSlamPos");

    sm_tictoc("renderMultiGridmap");
    //we will look for transition portals here at the end
    app->isamslam->renderMultiGridmap();
    sm_tictoc("renderMultiGridmap");

    sm_tictoc("publishMultiGridMap");
    app->isamslam->publishFinalMultiGridMap();
    app->isamslam->publishMultiGridMap();
    sm_tictoc("publishMulitGridMap");

    sm_tictoc("publishCurrentGridMap");
    app->isamslam->publishCurrentGridMap();
    app->isamslam->publishFinalGridMap();
    sm_tictoc("publishCurrentGridMap");

    if(app->region){
        app->isamslam->renderRegionGridmap();
        app->isamslam->publishRegionGridMap();
    }

    //publish pose list
    sm_tictoc("matchinglocation");
    app->isamslam->matchTaggedLocations();
    sm_tictoc("matchinglocation");

    sm_tictoc("publishTaggedLocs");
    app->isamslam->publishTaggedNodes();
    sm_tictoc("publishTaggedLocs");

    //do drawing :-)
    sm_tictoc("drawGraph");
    app->isamslam->drawGraph();
    sm_tictoc("drawGraph");

    app->isamslam->initializeLocalize();
    fprintf(stderr,"Concluding SLAM\n");
    app_destroy(app);

}

static void on_deadreckon_pose(const lcm_recv_buf_t *rbuf, const char * channel,
                               const bot_core_pose_t * msg, void * user) 
{
    app_t * app = (app_t *) user;

    if(app->last_deadreakon){
        bot_core_pose_t_destroy(app->last_deadreakon);
    }
    app->last_deadreakon = bot_core_pose_t_copy(msg);
}

//when there is a region change event - 
//a temp region is created and then once we get the new region no - 
//all old poses are chaged to this id  
//this should not effect scan matching 
void region_change_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), 
			  const erlcm_region_change_t * msg,
			  void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    
    if(app->region_info.current_region == msg->region_no){
        return; 
    }
    app->region_info.prev_region = app->region_info.current_region;
    app->region_info.current_region = msg->region_no;
    fprintf(stderr,"Region Change event received - starting poses on a new region\n");
    app->region_info.region_change = 1;
}

void region_id_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), 
			  const erlcm_slam_graph_node_list_t * msg,
			  void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    fprintf(stderr, "New region id list received - need to apply changes\n");
    app->isamslam->updateNodeRegions((erlcm_slam_graph_node_list_t *) msg);
}

//when there is a floor change event - i.e. onset of an elevator transit 
//a temp floor is created and then once we get the new floor no - 
//all old poses are chaged to this id  
void floor_change_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), 
			  const erlcm_floor_change_msg_t * msg,
			  void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    //these are actual floor no's
    
    if(app->floor_info.current_floor == msg->floor_no){
        return; //no floor change event has occured
    }
    app->floor_info.prev_floor = app->floor_info.current_floor;
    app->floor_info.current_floor = msg->floor_no;
    fprintf(stderr,"Floor Change event received - starting poses on a new floor - No floor no recievd\n");

    fprintf(stderr,"Visiting a Different Floor\n");
    //this will cause an error with -100 valued floors??
    
    app->isamslam->addNewFloor(msg->floor_no); //at some point this floor id needs to be also sent along
    app->floor_info.floor_change = 1;

    //a floor change should create a region change as well - lets ignore it for now
}

void floor_id_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), 
		      const erlcm_floor_change_msg_t * msg,
		      void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    //these are actual floor no's
    //if the current floor was an unknown floor - this is not an actual floor change
    //then this should result in changing the pose floor no's
    //if this new floor is a revisit - then we should decrement the floor counts - and update the floor maps


    //app->current_floor = -100 - is the floor no used for an unknown floor
    if(app->floor_info.current_floor != -100){//normal operation - this prob wont happen now because of the use of the pressure sensor
        if(app->floor_info.current_floor == msg->floor_no){
            return; //no floor change event has occured
        }

        app->floor_info.prev_floor = app->floor_info.current_floor;
        app->floor_info.current_floor = msg->floor_no;
        fprintf(stderr,"Floor Change Current_floor : %d, Prev Floor: %d\n", app->floor_info.current_floor, app->floor_info.prev_floor);
        int *ind = (int *) g_hash_table_lookup(app->floor_info.floor_hash, &app->floor_info.current_floor);

        if(ind ==NULL){
            fprintf(stderr,"Visiting New Floor\n");
            int *floor_id = (int *) calloc(1, sizeof(int));
            int *floor_val = (int *) calloc(1, sizeof(int));
            *floor_val = app->floor_info.current_floor;
            *floor_id = app->floor_info.current_floor;
            g_hash_table_insert(app->floor_info.floor_hash, floor_id, floor_val);
            
            //---------add to Isam
            app->isamslam->addNewFloor(msg->floor_no); //at some point this floor id needs to be also sent along
        }

        app->floor_info.floor_change = 1;
    }
    else{
        //only now we know the floor no of the current floor
        //app->prev_floor = app->current_floor;
        app->floor_info.current_floor = msg->floor_no;
        fprintf(stderr,"We have received the floor id of the current floor : %d\n", app->floor_info.current_floor);

        int *ind = (int *) g_hash_table_lookup(app->floor_info.floor_hash, &app->floor_info.current_floor);

        if(ind ==NULL){
            fprintf(stderr,"Visiting New Floor\n");
            int *floor_id = (int *) calloc(1, sizeof(int));
            int *floor_val = (int *) calloc(1, sizeof(int));
            *floor_val = app->floor_info.current_floor;
            *floor_id = app->floor_info.current_floor;
            g_hash_table_insert(app->floor_info.floor_hash, floor_id, floor_val);
            
            //call the replace floor function
            app->isamslam->updateNewFloor(msg->floor_no); //at some point this floor id needs to be also sent along
        }
        else{//remove this floor from the floor list
            fprintf(stderr,"Revisitng old Floor\n");
            app->isamslam->updateNewFloor(msg->floor_no);
        }

        if (app->optimize) {
            sm_tictoc("SlamIncrementalOptim");
            app->isamslam->slam->update();
            sm_tictoc("SlamIncrementalOptim");
        }

        sm_tictoc("publishDraw");

        //publish the SLAM_POS
        sm_tictoc("publishSlamPos");
        app->isamslam->publishSlamPos();
        sm_tictoc("publishSlamPos");

        //render the gridmap
        if(app->publish_full_maps){
            sm_tictoc("renderMultiGridmap");
            app->isamslam->renderMultiGridmap();
            sm_tictoc("renderMultiGridmap");

            sm_tictoc("publishGridMap");
            app->isamslam->publishMultiGridMap();
            sm_tictoc("publishGridMap");

            sm_tictoc("publishCurrentGridMap");
            app->isamslam->publishCurrentGridMap();
            sm_tictoc("publishCurrentGridMap");

            if(app->region){
                app->isamslam->renderRegionGridmap();
                app->isamslam->publishRegionGridMap();
            }
        }
        else{
            sm_tictoc("renderCurrentGridmap");
            app->isamslam->renderCurrentGridmap();
            sm_tictoc("renderCurrentGridmap");

            sm_tictoc("publishCurrentGridMap");
            app->isamslam->publishCurrentGridMap();
            sm_tictoc("publishCurrentGridMap");
     
            if(app->region){
                app->isamslam->renderRegionGridmap();
                app->isamslam->publishRegionGridMap();
            }
        }
        //do drawing :-)
        sm_tictoc("drawGraph");
        app->isamslam->drawGraph();
        sm_tictoc("drawGraph");

        sm_tictoc("matchinglocation");
        app->isamslam->matchTaggedLocations();
        sm_tictoc("matchinglocation");

        sm_tictoc("publishTaggedLocs");
        app->isamslam->publishTaggedNodes();
        sm_tictoc("publishTaggedLocs");
    }
}

void people_pos_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), 
			const erlcm_people_pos_msg_t * msg,
			void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    if(app->person_msg!=NULL){
        erlcm_people_pos_msg_t_destroy(app->person_msg);
    }
    app->person_msg = erlcm_people_pos_msg_t_copy(msg);
    int ind = app->person_msg->followed_person;
}

void guide_pos_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), 
		       const  erlcm_guide_info_t* msg,
		       void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    if(app->guide_msg!=NULL){
        erlcm_guide_info_t_destroy(app->guide_msg);
    }
    app->guide_msg = erlcm_guide_info_t_copy(msg);
}

void tagging_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), 
		     const erlcm_tagged_node_t * msg,
		     void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;

    char* type = msg->type;
    char* name = msg->label;
    //SLAM related speech commands
    if((strcmp(type,"place")==0)|| (strcmp(type,"object")==0)){
        //a possible tagging event
        app->possible_tagging = 1;
        app->tag_utime = msg->utime;
        if(app->tagged_node !=NULL)
            erlcm_tagged_node_t_destroy(app->tagged_node);
        app->tagged_node = erlcm_tagged_node_t_copy(msg);

        if(((app->guide_msg !=NULL) &&
            (fabs(app->guide_msg->utime - msg->utime) < 1e6) && (app->guide_msg->tracking_state==1))){
            //we have a valid person estimate
            //---------add to Isam
            app->isamslam->addPersonLoc(msg->utime,app->guide_msg->pos[0],app->guide_msg->pos[1]);
        }    
        else{
            fprintf(stderr,"No person Estimate\n");
        }
        fprintf(stderr,"Received Possible Location\n");
    }
    if(strcmp(type,"confirm")==0){
        //a confirmed tagging
        fprintf(stderr,"Confirmed Location\n");
        //---------add to Isam
        app->isamslam->confirmTaggedNode(name,msg->utime);
    }
    if(strcmp(type,"mode")==0){
        //mode has changed - we should start building the map only when we are in tour guide mode
        if((app->mode!=NULL && !strcmp(app->mode,"tourguide")) && !strcmp(name,"navigation")){
            fprintf(stderr,"Done with the tour\n");
            //this is wrong - we should add the last node before we conclude tour
            //conclude_tour(app);
            app->conclude_tour = 1;
        }
        else{
            if(app->mode !=NULL)
                free(app->mode);
            app->mode = strdup(name);
            fprintf(stderr,"Mode Changed : %s \n",app->mode);
        }
    }
    fprintf(stderr,"Mode : %d\n",  app->conclude_tour);
}

////////////////////////////////////////////////////////////////////
//where all the work is done
////////////////////////////////////////////////////////////////////
static void aligned_laser_handler(const bot_core_planar_lidar_t * laser_msg, const sm_rigid_transform_2d_t * odom,
				  int64_t utime, double height, double rp[2], app_t * app)
{
    static int first_scan = 1;
    int tagged_node = 0;

    if(g_hash_table_size(app->floor_info.floor_hash)==0){//app->floor_count==0){
        return;
    }

    //compute the distance between this scan and the last one that got added.
    double dist[2];
    sm_vector_sub_2d(odom->pos, app->prev_odom->pos, dist);

    double ld = sm_norm(SMPOINT(dist));
    double ad = sm_angle_subtract(odom->theta, app->prev_odom->theta);
    bool addScan = false;
    if (fabs(ld) > LINEAR_DIST_TO_ADD) {
        addScan = true;
    }
    if (fabs(ad) > ANGULAR_DIST_TO_ADD) {
        addScan = true;
    }
    if(app->possible_tagging){
        app->possible_tagging = 0;
        tagged_node = 1;
        addScan = true;
    }
    
    if(app->floor_info.floor_change){
        addScan = true;
    }

    if(app->region_info.region_change){
        addScan = true;
    }

    if(app->conclude_tour){
        addScan = true;
    }
    if(first_scan){
        addScan = true;
        first_scan = 0;
    }

    if (!addScan) {
        if (!app->use_frames && app->publish_pose) {
            Pose2d curr_pose(odom->pos[0], odom->pos[1], odom->theta);
            Pose2d prev_pose(app->prev_odom->pos[0], app->prev_odom->pos[1], app->prev_odom->theta);
            Pose2d prev_curr_tranf = curr_pose.ominus(prev_pose);
            Pose2d curr_slam_pose = app->isamslam->getCurrentPose();
            Pose2d prop_pose = curr_slam_pose.oplus(prev_curr_tranf);

            bot_core_pose_t pose;
            memset(&pose, 0, sizeof(pose));
            pose.pos[0] = prop_pose.x();
            pose.pos[1] = prop_pose.y();
            double rpy[3] = { 0, 0, prop_pose.t() };
            bot_roll_pitch_yaw_to_quat(rpy, pose.orientation);
            pose.utime = laser_msg->utime;
            bot_core_pose_t_publish(app->lcm, "POSE", &pose);
        }
        return;
    }
    sm_tictoc("addScanToGraph");

    if(tagged_node){
        app->isamslam->addTaggedNode(app->isamslam->trajectory.size()-1,app->tagged_node);
    }

    ////////////////////////////////////////////////////////////////////
    //we want to process and add this scan!
    ////////////////////////////////////////////////////////////////////
    fprintf(stderr,"a");
    //Project ranges into points, and decimate points so we don't have too many
    smPoint * points = (smPoint *) calloc(laser_msg->nranges, sizeof(smPoint));
    int numValidPoints = sm_projectRangesAndDecimate(app->beam_skip, app->spatialDecimationThresh, laser_msg->ranges,
                                                     laser_msg->nranges, laser_msg->rad0, laser_msg->radstep, points, app->maxRange, app->validBeamAngles[0],
                                                     app->validBeamAngles[1]);
    if (numValidPoints < 30) {
        fprintf(stderr, "WARNING! NOT ENOUGH VALID POINTS! numValid=%d\n", numValidPoints);
        free(points);
        return;
    }
    else {
        points = (smPoint *) realloc(points, numValidPoints * sizeof(smPoint));//crop memory down to size
    }
    
    //adding laser offset
    double sensor_to_body[12];
    if (!bot_frames_get_trans_mat_3x4 (app->frames, app->chan,
                                       "body",
                                       sensor_to_body)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;
    }
    
    //transform to body pose
    double pos_s[3] = {0}, pos_b[3];
    for(int i=0; i < numValidPoints; i++){
        pos_s[0] = points[i].x;
        pos_s[1] = points[i].y;
        bot_vector_affine_transform_3x4_3d (sensor_to_body, pos_s , pos_b);
        points[i].x = pos_b[0];
        points[i].y = pos_b[1];
    }

    smPoint * rpoints = NULL;
    int rnumValidPoints = 0;
    if(fabs(utime - app->r_utime) < 1e6 && app->r_laser_msg !=NULL){
        fprintf(stderr,"r");
        //Project ranges into points, and decimate points so we don't have too many
        rpoints = (smPoint *) calloc(app->r_laser_msg->nranges, sizeof(smPoint));
        rnumValidPoints = sm_projectRangesAndDecimate(app->beam_skip, app->spatialDecimationThresh, app->r_laser_msg->ranges,
                                                      app->r_laser_msg->nranges, app->r_laser_msg->rad0,
                                                      app->r_laser_msg->radstep, rpoints, app->maxRange, -100, +100);
        if (rnumValidPoints < 30) {
            fprintf(stderr, "WARNING! NOT ENOUGH VALID POINTS! numValid=%d\n", rnumValidPoints);
            free(rpoints);
            return;
        }
        else {
            rpoints = (smPoint *) realloc(rpoints, rnumValidPoints * sizeof(smPoint));//crop memory down to size
            //adding laser offset
            
            double sensor_r_to_body[12];
            if (!bot_frames_get_trans_mat_3x4 (app->frames, app->rchan,
                                               "body",
                                               sensor_r_to_body)) {
                fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
                return;
            }
            
            //transform to body pose
            double pos_s[3] = {0}, pos_b[3];
            for(int i=0; i < rnumValidPoints; i++){
                pos_s[0] = rpoints[i].x;
                pos_s[1] = rpoints[i].y;
                bot_vector_affine_transform_3x4_3d (sensor_r_to_body, pos_s , pos_b);
                rpoints[i].x = pos_b[0];
                rpoints[i].y = pos_b[1];
            }
        }
    }

    if(rnumValidPoints){
        points = (smPoint *) realloc(points, (numValidPoints+rnumValidPoints) * sizeof(smPoint));
        memcpy(points + numValidPoints, rpoints, rnumValidPoints * sizeof(smPoint));
        numValidPoints += rnumValidPoints;
        free(rpoints);
    }

    ScanTransform T;
    //just use the zero transform... it'll get updated the first time its needed
    memset(&T, 0, sizeof(T));

    Scan * scan;
    //if we have a valid person location - then we should clear the laser of person leg observations 
    if(((app->guide_msg !=NULL) &&
        (fabs(app->guide_msg->utime - utime) < 1e6) && (app->guide_msg->tracking_state==1))){
        scan = get_person_free_scan(numValidPoints, points, T, SM_HOKUYO_UTM, utime, true, app->guide_msg);
    }
    else{//use as is 
        scan = new Scan(numValidPoints, points, T, SM_HOKUYO_UTM, utime, true);
    }

    Scan * maxRangeScan = get_maxrange_scan(laser_msg->ranges, laser_msg->nranges, laser_msg->rad0, laser_msg->radstep,
                                            app->maxRange, app->maxUsableRange, app->validBeamAngles[0], app->validBeamAngles[1]);

    //get the odometry delta
    Pose2d curr_pose(odom->pos[0], odom->pos[1], odom->theta);
    Pose2d prev_pose(app->prev_odom->pos[0], app->prev_odom->pos[1], app->prev_odom->theta);
    Pose2d prev_curr_tranf = curr_pose.ominus(prev_pose);

    if (app->prev_odom != NULL)
        sm_rigid_transform_2d_t_destroy(app->prev_odom);
    app->prev_odom = sm_rigid_transform_2d_t_copy(odom);

    //rotate the cov to body frame
    double Rcov[9];
    sm_rotateCov2D(odom->cov, -odom->theta, Rcov);
    //Matrix cov(3, 3, Rcov);
    Matrix3d cv(Rcov); //= MatrixXd::Random(3,3); //(3,3,Rcov);
    Noise cov = Covariance(cv);
    
    double low_cov[9] = { 0 };
    low_cov[0] = 0.01;
    low_cov[4] = 0.01;
    low_cov[8] = 0.001;

    Matrix3d low_cv(low_cov); //= MatrixXd::Random(3,3); //(3,3,Rcov);
    Noise low_cov_noise = Covariance(low_cv);
    
    //the tranform ends up being unity - i.e. the same pose needs to get added as poses in both floors
    Pose2d prev_curr_tranf_new_floor = curr_pose.ominus(curr_pose);
    Pose2d prev_curr_tranf_new_region = curr_pose.ominus(curr_pose);

    if (!app->scanmatchBeforAdd) {
        if(app->floor_info.floor_change){//floor change has occured
            //fprintf(stderr,"Adding same floor nodes - Prev Floor : %d, Current Floor: %d\n", app->floor_info.prev_floor, app->floor_info.current_floor);
            if(g_hash_table_size(app->floor_info.floor_hash) >1){//we were on a previous floor
                sm_tictoc("addNodeToSlam"); 
                app->isamslam->addNodeToSlam(prev_curr_tranf, cov, scan, maxRangeScan, utime, height, rp, app->floor_info.prev_floor, app->region_info.current_region);
                app->isamslam->addNodeToSlam(prev_curr_tranf_new_floor, low_cov_noise, scan, maxRangeScan, utime, height, rp, app->floor_info.current_floor, app->region_info.current_region);
            }
            else{
                if(app->region_info.region_change){
                    //if there was a valid previous region
                    if(app->region_info.prev_region >=0){
                        app->isamslam->addNodeToSlam(prev_curr_tranf, cov, scan, maxRangeScan, utime, height, rp, app->floor_info.current_floor, app->region_info.prev_region);
                        app->isamslam->addNodeToSlam(prev_curr_tranf_new_region, low_cov_noise, scan, maxRangeScan, utime, height, rp, app->floor_info.current_floor, app->region_info.current_region);
                    }                    
                    else{
                        app->isamslam->addNodeToSlam(prev_curr_tranf, cov, scan, maxRangeScan, utime, height, rp, app->floor_info.current_floor, app->region_info.current_region);
                    }
                    app->region_info.region_change = 0;
                }
                else{
                    app->isamslam->addNodeToSlam(prev_curr_tranf, cov, scan, maxRangeScan, utime, height, rp, app->floor_info.current_floor, app->region_info.current_region);
                }
            }
            app->floor_info.floor_change = 0;
        }
        
        else{
            //the same corresponding node gets added as being on the other floor
            if(app->region_info.region_change){
                if(app->region_info.prev_region >=0){
                    app->isamslam->addNodeToSlam(prev_curr_tranf, cov, scan, maxRangeScan, utime, height, rp, app->floor_info.current_floor, app->region_info.prev_region);
                    app->isamslam->addNodeToSlam(prev_curr_tranf_new_region, low_cov_noise, scan, maxRangeScan, utime, height, rp, app->floor_info.current_floor, app->region_info.current_region);
                }
                else{
                    app->isamslam->addNodeToSlam(prev_curr_tranf, cov, scan, maxRangeScan, utime, height, rp, app->floor_info.current_floor, app->region_info.current_region);
                }
                app->region_info.region_change = 0;
            }
            else{
                app->isamslam->addNodeToSlam(prev_curr_tranf, cov, scan, maxRangeScan, utime, height, rp, app->floor_info.current_floor, app->region_info.current_region);
            }
        }
        sm_tictoc("addNodeToSlam");
    }
    else {
        sm_tictoc("IncrementalSMRefineMent");
        //do incremental scan matching to refine the odometry estimate
        Pose2d prev_sm_pose(app->prev_sm_odom->pos[0], app->prev_sm_odom->pos[1], app->prev_sm_odom->theta);
        Pose2d curr_sm_pose = prev_sm_pose.oplus(prev_curr_tranf);
        ScanTransform sm_prior;
        memset(&sm_prior, 0, sizeof(sm_prior));
        sm_prior.x = curr_sm_pose.x();
        sm_prior.y = curr_sm_pose.y();
        sm_prior.theta = curr_sm_pose.t();
        sm_prior.score = app->odometryConfidence; //wide prior
        ScanTransform r = app->sm->matchSuccessive(points, numValidPoints, SM_HOKUYO_UTM, sm_get_utime(), false, &sm_prior);
        curr_sm_pose.set(r.x, r.y, r.theta);
        sm_rigid_transform_2d_t sm_odom;
        memset(&sm_odom, 0, sizeof(sm_odom));
        sm_odom.utime = laser_msg->utime;
        sm_odom.pos[0] = r.x;
        sm_odom.pos[1] = r.y;
        sm_odom.theta = r.theta;
        memcpy(sm_odom.cov, r.sigma, 9 * sizeof(double));
        if (app->prev_sm_odom != NULL)
            sm_rigid_transform_2d_t_destroy(app->prev_sm_odom);
        app->prev_sm_odom = sm_rigid_transform_2d_t_copy(&sm_odom);

        Pose2d prev_curr_sm_tranf = curr_sm_pose.ominus(prev_sm_pose);
        Pose2d prev_curr_sm_tranf_new_floor = curr_sm_pose.ominus(curr_sm_pose);

        Pose2d prev_curr_sm_tranf_new_region = curr_sm_pose.ominus(curr_sm_pose);
	cout << " Transform : " << prev_curr_sm_tranf << endl;


        double Rcov_sm[9];
        sm_rotateCov2D(r.sigma, -r.theta, Rcov_sm);
        //Matrix cov_sm(3, 3, Rcov_sm);
        //cov_sm = 1000 * cov_sm;
        Matrix3d cv_sm(Rcov_sm); 
        Noise cov_sm = Covariance(1000 * cv_sm);

        double cov_hardcode[9] = { .05, 0, 0, 0, .05, 0, 0, 0, .01 };
        Matrix3d cv_hardcode(cov_hardcode);
        Noise cov_hc = Covariance(1000 * cv_hardcode);

        sm_tictoc("IncrementalSMRefineMent");

        if(app->floor_info.floor_change){
            //fprintf(stderr,"Adding same floor nodes Prev Floor : %d, Current Floor: %d\n", app->floor_info.prev_floor, app->floor_info.current_floor);
            //adding current and last floors
            if(g_hash_table_size(app->floor_info.floor_hash) >1){
                //the region types might be messy 
                app->isamslam->addNodeToSlam(prev_curr_sm_tranf, cov_hc, scan, maxRangeScan, utime,                                                                                    height, rp, app->floor_info.prev_floor, app->region_info.current_region);
                app->isamslam->addNodeToSlam(prev_curr_sm_tranf_new_floor, low_cov_noise, scan, maxRangeScan, utime,height,rp, app->floor_info.current_floor, app->region_info.current_region);
            }
            else{
                //fprintf(stderr,"Adding only Current +++ \n");
                //this looks wrong - new_floor???
                if(app->region_info.region_change){
                    if(app->region_info.prev_region >=0){
                        //fprintf(stderr,"Valid region change\n");
                        app->isamslam->addNodeToSlam(prev_curr_sm_tranf, cov, scan, maxRangeScan, utime, height, rp, app->floor_info.current_floor, app->region_info.prev_region);
                        app->isamslam->addNodeToSlam(prev_curr_sm_tranf_new_region, low_cov_noise, scan, maxRangeScan, utime,height,rp, app->floor_info.current_floor, app->region_info.current_region);
                    }
                    else{
                        //fprintf(stderr,"+++ Called\n");
                        app->isamslam->addNodeToSlam(prev_curr_sm_tranf, cov_hc, scan, maxRangeScan, utime,height,rp, app->floor_info.current_floor, app->region_info.current_region);
                    } 
                    app->region_info.region_change = 0;
                }
                else{
                    //fprintf(stderr,"+++ Called\n");
                    app->isamslam->addNodeToSlam(prev_curr_sm_tranf, cov_hc, scan, maxRangeScan, utime,height,rp, app->floor_info.current_floor, app->region_info.current_region);
                }                
            }
            app->floor_info.floor_change = 0;
        }
        else{
            if(app->region_info.region_change){
                if(app->region_info.prev_region >=0){
                    app->isamslam->addNodeToSlam(prev_curr_sm_tranf, cov, scan, maxRangeScan, utime, height, rp, app->floor_info.current_floor, app->region_info.prev_region);
                    app->isamslam->addNodeToSlam(prev_curr_sm_tranf_new_region, low_cov_noise, scan, maxRangeScan, utime,height,rp, app->floor_info.current_floor, app->region_info.current_region);
                }
                else{
                    app->isamslam->addNodeToSlam(prev_curr_sm_tranf, cov_hc, scan, maxRangeScan, utime,height,rp, app->floor_info.current_floor, app->region_info.current_region);
                }
                app->region_info.region_change = 0;
            }
            else{
                app->isamslam->addNodeToSlam(prev_curr_sm_tranf, cov_hc, scan, maxRangeScan, utime,height,rp, app->floor_info.current_floor, app->region_info.current_region);
            }
        }
    }
    sm_tictoc("addScanToGraph");

    if (app->optimize) {
        //fprintf(stderr,"Updating Nodes\n");
        sm_tictoc("SlamIncrementalOptim");
        app->isamslam->slam->update();
        sm_tictoc("SlamIncrementalOptim");
    }

    sm_tictoc("publishDraw");

    //publish the SLAM_POS
    sm_tictoc("publishSlamPos");
    app->isamslam->publishSlamPos();
    sm_tictoc("publishSlamPos");
    
    //render the gridmap
    if(app->publish_full_maps){
        sm_tictoc("renderMultiGridmap");
        app->isamslam->renderMultiGridmap();
        sm_tictoc("renderMultiGridmap");

        sm_tictoc("publishGridMap");
        app->isamslam->publishMultiGridMap();
        sm_tictoc("publishGridMap");

        sm_tictoc("publishCurrentGridMap");
        app->isamslam->publishCurrentGridMap();
        sm_tictoc("publishCurrentGridMap");
        
        if(app->region){
            app->isamslam->renderRegionGridmap();
            app->isamslam->publishRegionGridMap();
        }
    }
    else{
        sm_tictoc("renderCurrentGridmap");
        app->isamslam->renderCurrentGridmap();
        sm_tictoc("renderCurrentGridmap");

        sm_tictoc("publishCurrentGridMap");
        app->isamslam->publishCurrentGridMap();
        sm_tictoc("publishCurrentGridMap");

        if(app->region){
            app->isamslam->renderRegionGridmap();
            app->isamslam->publishRegionGridMap();
        }
    }

    if(app->draw_map){
        sm_tictoc("drawMap");
        app->isamslam->drawMap();
        sm_tictoc("drawMap");
    }
    //do drawing :-)
    sm_tictoc("drawGraph");
    app->isamslam->drawGraph();
    sm_tictoc("drawGraph");

    sm_tictoc("matchinglocation");
    app->isamslam->matchTaggedLocations();
    sm_tictoc("matchinglocation");

    sm_tictoc("publishTaggedLocs");
    app->isamslam->publishTaggedNodes();
    sm_tictoc("publishTaggedLocs");

    sm_tictoc("publishDraw");

    if (!app->use_frames){
        if(app->publish_pose) {
            Pose2d slam_pose = app->isamslam->getCurrentPose();
            bot_core_pose_t pose;
            memset(&pose, 0, sizeof(pose));
            pose.pos[0] = slam_pose.x();
            pose.pos[1] = slam_pose.y();
            double rpy[3] = { 0, 0, slam_pose.t() };
            bot_roll_pitch_yaw_to_quat(rpy, pose.orientation);
            pose.utime = laser_msg->utime;
            bot_core_pose_t_publish(app->lcm, "POSE", &pose);
        }
    }
    else {
        //publish frame update 
        Pose2d slam_pose = app->isamslam->getCurrentPose();
        bot_core_pose_t pose;
        memset(&pose, 0, sizeof(pose));
        bot_core_pose_t last_pose;
        memset(&last_pose, 0, sizeof(last_pose));
        pose.pos[0] = slam_pose.x();
        pose.pos[1] = slam_pose.y();
        double rpy[3] = { 0, 0, slam_pose.t() };
        bot_roll_pitch_yaw_to_quat(rpy, pose.orientation);
        pose.utime = laser_msg->utime;
        //fprintf(stderr, "SLAM Pose : %f,%f,%f\n", pose.pos[0], 
        //      pose.pos[1],rpy[2]); 

        last_pose.pos[0] = odom->pos[0];
        last_pose.pos[1] = odom->pos[1];
        rpy[2] = odom->theta;
        bot_roll_pitch_yaw_to_quat(rpy, last_pose.orientation);
        last_pose.utime = laser_msg->utime;

        bot_core_rigid_transform_t global_to_local;
        global_to_local.utime = laser_msg->utime;
    
        BotTrans bt_global_to_local;

        // The transformation from GLOBAL to LOCAL is defined as
        // T_global-to-local = T_body-to-local * T_global-to-body
        //                   = T_body-to-local * inv(T_body-to-global)
        BotTrans bt_body_to_global;
        BotTrans bt_global_to_body;
        BotTrans bt_body_to_local;
 
        bot_trans_set_from_quat_trans (&bt_body_to_global, pose.orientation, pose.pos);
        bot_trans_set_from_quat_trans (&bt_body_to_local, last_pose.orientation, last_pose.pos);
        bot_trans_invert (&bt_body_to_global);
        bot_trans_copy (&bt_global_to_body, &bt_body_to_global);
        bot_trans_apply_trans_to (&bt_body_to_local, &bt_global_to_body, &bt_global_to_local);
    
        memcpy (global_to_local.trans, bt_global_to_local.trans_vec, 3*sizeof(double));
        memcpy (global_to_local.quat, bt_global_to_local.rot_quat, 4*sizeof(double));

        bot_core_rigid_transform_t_publish (app->lcm, "GLOBAL_TO_LOCAL", &global_to_local);
    }

    if(app->conclude_tour){
        fprintf(stderr,"Concluding Tour\n");
        conclude_tour(app);
    }

    return;
}

static void rear_laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const bot_core_planar_lidar_t * msg,
				     void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;

    if(app->r_laser_msg !=NULL){
        bot_core_planar_lidar_t_destroy(app->r_laser_msg);
    }
    app->r_laser_msg = bot_core_planar_lidar_t_copy(msg);
    
    bot_ptr_circular_add (app->rear_laser_circ,  bot_core_planar_lidar_t_copy(msg));
    
    static int64_t utime_prev = msg->utime;
    app->r_utime = msg->utime;    
}

void process_laser(bot_core_planar_lidar_t *flaser, app_t *app, int64_t utime){
    if(app->useOdom){
        sm_rigid_transform_2d_t odom;
        memset(&odom, 0, sizeof(odom));

        double rpy[3];
        
        double cov[9] = { 0 };
        cov[0] = 1;
        cov[1] = 0;
        cov[3] = 0;
        cov[4] = 1;
        cov[8] = 0.1;
        
        if(!app->use_frames){
            if(app->last_deadreakon != NULL){
                //use the odom msg
                memcpy(odom.pos, app->last_deadreakon->pos, 2 * sizeof(double));
                bot_quat_to_roll_pitch_yaw(app->last_deadreakon->orientation, rpy);
            }
            else{
                return;
            }
        }
        else{ 
            int64_t frame_utime;
            int status = bot_frames_get_latest_timestamp(app->frames, "body",
                                                         "local",&frame_utime);

            if(fabs(frame_utime - utime) / 1.0e6 > 1.0){
                fprintf(stderr, "Frames hasn't been updated yet - retruning\n");
                return;
            }

            double body_to_local[12];
            if (!bot_frames_get_trans_mat_3x4_with_utime (app->frames, "body",
                                                          "local",  flaser->utime,
                                                          body_to_local)) {
                fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
                return;        
            }
            double pose_b[3] = {.0}, pose_l[3];
            bot_vector_affine_transform_3x4_3d (body_to_local, 
                                                pose_b, 
                                                pose_l);
            odom.pos[0] = pose_l[0];
            odom.pos[1] = pose_l[1];

            BotTrans body_to_local_t;

            if (!bot_frames_get_trans_with_utime (app->frames, "body", "local", flaser->utime, 
                                                  &body_to_local_t)){
                fprintf (stderr, "frame error\n");
                return;
            }
            
            double rpy_b[3] = {0};
            double quat_b[4], quat_l[4];
            bot_roll_pitch_yaw_to_quat (rpy_b, quat_b);
            
            bot_quat_mult (quat_l, body_to_local_t.rot_quat, quat_b);
            bot_quat_to_roll_pitch_yaw (quat_l, rpy);
        }

        odom.theta = rpy[2];
        //cov was published as body frame already... rotate to global for now
        double Rcov[9];
        sm_rotateCov2D(cov, odom.theta, odom.cov);

        static int64_t utime_prev = flaser->utime;

        sm_tictoc("aligned_laser_handler");
        aligned_laser_handler(flaser, &odom, flaser->utime, 0 , rpy, app);
        sm_tictoc("aligned_laser_handler");
    }
    else{
        smPoint * points = (smPoint *) calloc(flaser->nranges, sizeof(smPoint));
        int numValidPoints = sm_projectRangesAndDecimate(app->beam_skip, app->spatialDecimationThresh, flaser->ranges,
                                                         flaser->nranges, flaser->rad0, flaser->radstep, points, app->maxRange, app->validBeamAngles[0], app->validBeamAngles[1]);
        if (numValidPoints < 30) {
            fprintf(stderr, "WARNING! NOT ENOUGH VALID POINTS! numValid=%d\n", numValidPoints);
            return;
        }

        double sensor_to_body[12];
        if (!bot_frames_get_trans_mat_3x4 (app->frames, app->chan,
                                           "body",
                                           sensor_to_body)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            return;
        }

        //transform to body pose
        double pos_s[3] = {0}, pos_b[3];
        for(int i=0; i < numValidPoints; i++){
            pos_s[0] = points[i].x;
            pos_s[1] = points[i].y;
            bot_vector_affine_transform_3x4_3d (sensor_to_body, pos_s , pos_b);
            points[i].x = pos_b[0];
            points[i].y = pos_b[1];
        }
        
        ////////////////////////////////////////////////////////////////////
        //Actually do the matching
        ////////////////////////////////////////////////////////////////////
        ScanTransform r = app->sm_incremental->matchSuccessive(points, numValidPoints, SM_HOKUYO_UTM, sm_get_utime(), NULL); //don't have a better estimate than prev, so just set prior to NULL

        sm_rigid_transform_2d_t odom;
        memset(&odom, 0, sizeof(odom));
        odom.utime = flaser->utime;
        odom.pos[0] = r.x;
        odom.pos[1] = r.y;
        odom.theta = r.theta;
        memcpy(odom.cov, r.sigma, 9 * sizeof(double));

        //  app->sm_incremental->drawGUI(points, numValidPoints, r, NULL,"ScanMatcher");
        
        free(points);
        sm_tictoc("laser_handler");
        double rp[2] = { 0, 0 };//call the slam/loop closing handler
        aligned_laser_handler(flaser, &odom, flaser->utime, 0, rp, app);
    }
}


////////////////////////////////////////////////////////////////////
//where all the work is done
////////////////////////////////////////////////////////////////////
static void laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const bot_core_planar_lidar_t * msg,
			  void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    
    if(app->slave_mode)
        bot_ptr_circular_add (app->front_laser_circ,  bot_core_planar_lidar_t_copy(msg));
    else{
        process_laser( (bot_core_planar_lidar_t *) msg,app, msg->utime);
    }
    
    static int64_t utime_prev = msg->utime;
}

static void node_init_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), 
                              const erlcm_slam_node_init_t * msg,
                              void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;

    //hmm find the closest node and call algined laser hander??
    sm_tictoc("laser_handler");
    ////////////////////////////////////////////////////////////////////
    //Project ranges into points, and decimate points so we don't have too many
    ////////////////////////////////////////////////////////////////////
    bot_core_planar_lidar_t *flaser = NULL;
    bot_core_planar_lidar_t *rlaser = NULL;
        
    //fprintf(stderr, "Buffer size : %d\n" , bot_ptr_circular_size(app->front_laser_circ));
    double min_utime = 0.5;//we dont consider if the closest laser is 0.5 s apart
    int close_ind = -1;

    //fprintf(stderr, "Node handler called"); 
    
    int64_t s_utime =bot_timestamp_now();

    for (int i=0; i<(bot_ptr_circular_size(app->front_laser_circ)); i++) {
        bot_core_planar_lidar_t *laser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(app->front_laser_circ, i);
        double time_diff = fabs(laser->utime - msg->utime)  / 1.0e6;
        //fprintf(stderr, "Time difference : %f\n", time_diff); 
        if(time_diff< min_utime){
            min_utime = time_diff;
            close_ind = i;
        }
    }

    int64_t e_utime =bot_timestamp_now();

    //fprintf(stderr, "Time to search : %f\n", (e_utime-s_utime)/1.0e6);
    //fprintf(stderr,"Close ind : %d\n", close_ind);
    if(close_ind == -1) 
        return;
    
    flaser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(app->front_laser_circ, close_ind);
    
    process_laser(flaser,app, msg->utime);
}


static void optim_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const erlcm_log_annotate_msg_t * msg,
			  void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    switch (msg->enumerate) {
    case 0:
        app->optimize = 0;
        break;
    case 1:
        app->optimize = 1;
        break;
    case 2:
        fprintf(stderr, "batch_optimization_step\n");
        app->isamslam->slam->batch_optimization();
        break;
    default:
        fprintf(stderr, "full_optimization\n");
        app->isamslam->slam->update();
        break;
    }
    //do drawing :-)
    app->isamslam->publishSlamPos();
    app->isamslam->publishSlamTrajectory();
    app->isamslam->renderCurrentGridmap();
    app->isamslam->publishCurrentGridMap();
    if(app->region)
        app->isamslam->publishRegionGridMap();
    app->isamslam->draw3DPointCloud();
    app->isamslam->drawGraph();
    app->isamslam->drawMap();
    double trajlength = app->isamslam->computeTrajectoryLength();
}

void app_destroy(app_t *app)
{
    // dump timing stats
    sm_tictoc(NULL);
    exit (1);

}

void
circ_free_lidar_data(void *user, void *p) {
    bot_core_planar_lidar_t *np = (bot_core_planar_lidar_t *) p; 
    bot_core_planar_lidar_t_destroy(np);
}

static void usage(const char *name)
{
    fprintf(stderr, "usage: %s [options]\n"
            "\n"
            "  -h, --help                      Shows this help text and exits\n"
            "  -a,  --aligned                  Subscribe to aligned_laser (ROBOT_LASER) msgs\n"
            "  -c, --chan <LCM CHANNEL>        Input lcm channel default:\"LASER\" or \"ROBOT_LASER\"\n"
            "  -s, --scanmatch                 Run Incremental scan matcher every time a node gets added to map\n"
            "  -d, --draw                      Show window with scan matches \n"
            "  -p, --publish_pose              publish POSE messages\n"
            "  -v, --verbose                   Be verbose\n"
            "  -m, --mode  \"HOKUYO_UTM\"|\"SICK\" configures low-level options.\n"
            "\n"
            "Low-level laser options:\n"
            "  -A, --mask <min,max>            Mask min max angles in (radians)\n"
            "  -B, --beamskip <n>              Skip every n beams \n"
            "  -D, --decimation <value>        Spatial decimation threshold (meters?)\n"
            "  -F                              Use frames to align laser scans\n"
            "  -R                              Use regions\n"
            "  -S                              Use Slave mode - pose additions will be triggered by external source\n"
            "  -M, --range <range>             Maximum range (meters)\n", name);
}

sig_atomic_t still_groovy = 1;

static void sig_action(int signal, siginfo_t *s, void *user)
{
    still_groovy = 0;
}

int main(int argc, char *argv[])
{
    setlinebuf(stdout);

    app_t *app = (app_t *) calloc(1, sizeof(app_t));
    app->prev_odom = (sm_rigid_transform_2d_t *) calloc(1, sizeof(sm_rigid_transform_2d_t)); //set initial prev_odom to zero...
    app->prev_sm_odom = (sm_rigid_transform_2d_t *) calloc(1, sizeof(sm_rigid_transform_2d_t)); //set initial prev_odom to zero...
    memset(app->prev_odom, 0, sizeof(sm_rigid_transform_2d_t));
    memset(app->prev_sm_odom, 0, sizeof(sm_rigid_transform_2d_t));

    bool alignLaser = false;
    app->chan = NULL;
    app->rchan = NULL;
    app->verbose = 0;
    app->do_drawing = 0;
    app->publish_pose = 0;
    app->scanmatchBeforAdd = 0;
    app->odometryConfidence = LINEAR_DIST_TO_ADD;
    app->draw_map = 0;

    //this is not set - Sachi
    //app->optimize = 1;
    app->useOdom = 0;
    // set to default values
    app->validBeamAngles[0] = -100;
    app->validBeamAngles[1] = +100;
    app->beam_skip = 3;
    app->spatialDecimationThresh = .2;
    app->maxRange = 29.7;
    app->maxUsableRange = 8;


    app->floor_info.floor_hash = g_hash_table_new(g_int_hash, g_int_equal);
    app->floor_info.current_floor = -1;
    app->floor_info.prev_floor = -1;
    app->floor_info.floor_change = -1;

    app->region_info.region_hash = g_hash_table_new(g_int_hash, g_int_equal);
    app->region_info.current_region = -1;
    app->region_info.prev_region = -1;
    app->region_info.region_change = 1;
    
  
    app->person_msg = NULL;
    app->guide_msg = NULL;
    app->tagged_node = NULL;
    app->tag_utime = 0;
    app->possible_tagging = 0;

    app->mode = strdup("tourguide");
    app->publish_full_maps = 0;
    app->conclude_tour = 0;

    /* LCM */
    app->lcm = lcm_create(NULL);
    if (!app->lcm) {
        fprintf(stderr, "ERROR: lcm_create() failed\n");
        return 1;
    }   
   
    const char *optstring = "hr:c:dafvm:M:B:CD:M:pn:s::FSR";
    char c;
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
                                  { "chan", required_argument, 0, 'c' },
                                  { "draw", no_argument, 0,'d' },
                                  { "mode", required_argument, 0, 'm' },
                                  { "rchan", required_argument, 0, 'r' },
                                  { "regions", required_argument, 0, 'R' },
                                  { "Frames", required_argument, 0, 'F' }, //uses frames to extract odom 
                                  { "no", required_argument, 0, 'n' },//floor no
                                  { "full", no_argument, 0, 'f' },//publish full maps
                                  { "range", required_argument, 0, 'M' },
                                  { "beamskip", required_argument, 0,'B' },
                                  { "decimation", required_argument, 0, 'D' },
                                  { "countour", required_argument, 0, 'C' },  //draw contour map
                                  { "publish_pose", no_argument, 0, 'p' },
                                  { "mask", required_argument,   0, 'A' },
                                  { "aligned", no_argument, 0, 'a' },
                                  { "scanmatch", optional_argument, 0, 's' },
                                  { "slave", optional_argument, 0, 'S' },
                                  { "verbose", no_argument, 0,'v' },
                                  { 0, 0, 0, 0 } };

    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
        case 'c':
            free(app->chan);
            app->chan = strdup(optarg);
            fprintf(stderr,"Main Laser Channel : %s\n", app->chan);
            break;
        case 'F':
            app->use_frames = 1;
            break;
        case 'S':
            app->slave_mode = 1;
            break;
        case 'R':
            app->region = 1;
            break;
        case 'r':
            free(app->rchan);
            app->rchan = strdup(optarg);
            fprintf(stderr,"Second Laser Channel : %s\n", app->rchan);
            break;
        case 'n':
            app->floor_info.current_floor = atoi(optarg);
            break;
        case 'f':
            app->publish_full_maps = 1;
            break;
        case 'v':
            app->verbose = 1;
            break;
        case 'd':
            app->do_drawing = 1;
            break;
        case 'p':
            app->publish_pose = 1;
            break;
        case 's':
            app->scanmatchBeforAdd = 1;
            if (optarg != NULL) {
                app->odometryConfidence = atof(optarg);
            }
            break;
        case 'm':
            if (optarg && optarg[0] == 'S') {
                // assume we want sick mode:
                app->maxRange = 79.0;
                app->beam_skip = 0;
                // anything else required?
            }
            break;
        case 'M':
            app->maxRange = strtod(optarg, 0);
            break;
        case 'C':
            app->draw_map = 1;
            break;
        case 'B':
            app->beam_skip = atoi(optarg);
            break;
        case 'D':
            app->spatialDecimationThresh = strtod(optarg, 0);
            break;
        case 'A':
            sscanf(optarg, "%f,%f", &app->validBeamAngles[0], &app->validBeamAngles[1]);
            break;
        case 'a':
            alignLaser = true;
            app->useOdom = 1;
            break;
        case 'h':
        default:
            usage(argv[0]);
        return 1;
        }
    }

    app->param = bot_param_new_from_server(app->lcm, 1);
    
    app->frames = bot_frames_get_global (app->lcm, app->param);

    if (app->chan == NULL) {
        if (alignLaser)
            app->chan = strdup("ROBOT_LASER");
        else
            app->chan = strdup("LASER");
    }

    if (app->verbose) {
        if (alignLaser)
            printf("INFO: Listening for robot_laser msgs on %s\n", app->chan);
        else
            printf("INFO: Listening for laser msgs on %s\n", app->chan);

        printf("INFO: Do Draw:%d\n", app->do_drawing);
        printf("INFO: publish_pose:%d\n", app->publish_pose);
        printf("INFO: Max Range:%lf\n", app->maxRange);
        printf("INFO: SpatialDecimationThresh:%lf\n", app->spatialDecimationThresh);
        printf("INFO: Beam Skip:%d\n", app->beam_skip);
        printf("INFO: validRange:%f,%f\n", app->validBeamAngles[0], app->validBeamAngles[1]);
    }

    //hardcoded loop closing scan matcher params
    double metersPerPixel_lc = .02; //translational resolution for the brute force search
    double thetaResolution_lc = .01; //angular step size for the brute force search
    int useGradientAscentPolish_lc = 1; //use gradient descent to improve estimate after brute force search
    int useMultires_lc = 3; // low resolution will have resolution metersPerPixel * 2^useMultiRes
    bool useThreads_lc = true;

    //create the actual scan matcher object
    app->isamslam = new IsamSlam(app->lcm, metersPerPixel_lc, thetaResolution_lc, useGradientAscentPolish_lc, useMultires_lc,
                                 app->do_drawing, useThreads_lc, app->frames);

    //hardcoded scan matcher params
    double metersPerPixel = .02; //translational resolution for the brute force search
    double thetaResolution = .02; //angular step size for the brute force search
    sm_incremental_matching_modes_t matchingMode = SM_GRID_COORD; //use gradient descent to improve estimate after brute force search
    int useMultires = 3; // low resolution will have resolution metersPerPixel * 2^useMultiRes

    double initialSearchRangeXY = .15; //nominal range that will be searched over
    double initialSearchRangeTheta = .1;

    //SHOULD be set greater than the initialSearchRange
    double maxSearchRangeXY = .3; //if a good match isn't found I'll expand and try again up to this size...
    double maxSearchRangeTheta = .2; //if a good match isn't found I'll expand and try again up to this size...

    int maxNumScans = 30; //keep around this many scans in the history
    double addScanHitThresh = .90; //add a new scan to the map when the number of "hits" drops below this

    int useThreads = 1;

    //create the incremental scan matcher object
    app->sm_incremental = new ScanMatcher(metersPerPixel, thetaResolution, useMultires, useThreads, false);
    //set the scan matcher to start at pi/2...
    ScanTransform startPose;
    memset(&startPose, 0, sizeof(startPose));
    app->sm_incremental->initSuccessiveMatchingParams(maxNumScans, initialSearchRangeXY, maxSearchRangeXY,
                                                      initialSearchRangeTheta, maxSearchRangeTheta, matchingMode, addScanHitThresh, false, .3, &startPose);

    //create the SLAM scan matcher object
    app->sm = new ScanMatcher(metersPerPixel, thetaResolution, useMultires, useThreads, false);
    //set the scan matcher to start at pi/2...
    app->sm->initSuccessiveMatchingParams(maxNumScans, LINEAR_DIST_TO_ADD, LINEAR_DIST_TO_ADD, ANGULAR_DIST_TO_ADD,
                                          ANGULAR_DIST_TO_ADD, matchingMode, addScanHitThresh, false, .3, &startPose);

    //set the scan matcher to start at pi/2... cuz it looks better
    memset(&app->sm->currentPose, 0, sizeof(app->sm->currentPose));
    app->sm->currentPose.theta = M_PI / 2;
    app->sm->prevPose = app->sm->currentPose;
    app->r_laser_msg = NULL;
    app->r_utime = 0;

    app->front_laser_circ = bot_ptr_circular_new (LASER_DATA_CIRC_SIZE,
                                                  circ_free_lidar_data, app);
    
    app->rear_laser_circ = bot_ptr_circular_new (LASER_DATA_CIRC_SIZE,
                                                 circ_free_lidar_data, app);
    if (app->sm->isUsingIPP())
        fprintf(stderr, "Using IPP\n");
    else
        fprintf(stderr, "NOT using IPP\n");

    if(app->floor_info.current_floor >=0){
        app->floor_info.floor_change = 1;
        int *floor_id = (int *) calloc(1, sizeof(int));
        int *floor_val = (int *) calloc(1, sizeof(int));
        *floor_val = app->floor_info.current_floor;
        *floor_id = app->floor_info.current_floor;
        g_hash_table_insert(app->floor_info.floor_hash, floor_id, floor_val);
        
        app->isamslam->addNewFloor(app->floor_info.current_floor);
        fprintf(stderr,"Current Floor : %d %d\n", app->floor_info.current_floor, g_hash_table_size(app->floor_info.floor_hash));
    }
  
    if (!alignLaser){
        bot_core_planar_lidar_t_subscribe(app->lcm, app->chan, laser_handler, app);
    }
    else{
        if(!app->use_frames){
            fprintf(stderr,"Subscribing to Robot deadreackon channel\n");            
            bot_core_pose_t_subscribe (app->lcm, "ROBOT_DEADRECKON", on_deadreckon_pose, app);
        }
        bot_core_planar_lidar_t_subscribe(app->lcm, app->chan, laser_handler, app);
    }
    
    if(app->rchan!=NULL){//subscribe to the rear laser channel
        bot_core_planar_lidar_t_subscribe(app->lcm, app->rchan , rear_laser_handler, app);
    }

    erlcm_log_annotate_msg_t_subscribe(app->lcm, "SLAM_OPTIM", optim_handler, app);

    if(app->region){
        erlcm_slam_graph_node_list_t_subscribe(app->lcm, "REGION_SEGMENTATION_RESULT",region_id_handler ,app);
    }

    //multi-floor and tour handling
    erlcm_tagged_node_t_subscribe(app->lcm, "SPEECH_TAG", tagging_handler, app);
    erlcm_tagged_node_t_subscribe(app->lcm, "WHEELCHAIR_MODE", tagging_handler, app);

    //subscribe to person location -might be better off using the guide's location
    erlcm_people_pos_msg_t_subscribe(app->lcm, "PEOPLE_LIST",people_pos_handler, app);

    erlcm_guide_info_t_subscribe(app->lcm, "GUIDE_POS", guide_pos_handler, app);

    //when we get a new floor id
    erlcm_floor_change_msg_t_subscribe(app->lcm, "FLOOR_STAUS", floor_id_handler, app);

    //when we get a floor change event
    erlcm_floor_change_msg_t_subscribe(app->lcm, "FLOOR_CHANGE", floor_change_handler, app);

    //when we get a floor change event
    erlcm_region_change_t_subscribe(app->lcm, "REGION_CHANGE", region_change_handler, app);

    if(app->slave_mode){
        fprintf(stderr,"Slave mode : Subscribing to external trigger\n");
        erlcm_slam_node_init_t_subscribe(app->lcm, "SLAM_NODE_INIT", node_init_handler, app);
    }

    // setup sigaction();
    struct sigaction new_action;
    new_action.sa_sigaction = sig_action;
    sigemptyset(&new_action.sa_mask);
    new_action.sa_flags = 0;

    sigaction(SIGINT, &new_action, NULL);
    sigaction(SIGTERM, &new_action, NULL);
    sigaction(SIGKILL, &new_action, NULL);
    sigaction(SIGHUP, &new_action, NULL);

    /* sit and wait for messages */
    while (still_groovy)
        lcm_handle(app->lcm);

    app_destroy(app);

    return 0;
}

