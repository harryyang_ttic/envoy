/*
 * SlamPose.h
 *
 *  Created on: Oct 22, 2009
 *      Author: abachrac
 */

#ifndef SLAMPOSE_H_
#define SLAMPOSE_H_
//#include <bot/scanmatch/Scan.hpp>
#include <scanmatch/Scan.hpp>
#include <vector>
#include <isam/isam.h>
#include "AnchorPose.h"

using namespace std;
using namespace scanmatch;
using namespace isam;
class SlamPose {
public:
  SlamPose(int64_t _utime, int _node_id, Scan * _scan, Scan *_maxRangeScan,double _height,double _rp[2], int _floor_no);

  SlamPose(int64_t _utime, int _node_id, Scan * _scan, Scan *_maxRangeScan,double _height,double _rp[2], int _floor_no, int region_id);

  virtual ~SlamPose();

  int updateScan();
  
  int64_t utime;
  int node_id;
  Scan * scan;
  Scan * maxRangeScan;

  //Scan * fullScan;

  //for drawing in 3D
  double height;
  int floor_no;
  double rp[2];
  int region_id;

  vector<Scan *> allScans;

  bool loopClosureChecked;
  bool rendered;
  Pose2d_Node * pose2d_node;
  vector<int> constraint_ids;
  
  AnchorPose *ap;

};

#endif /* SLAMPOSE_H_ */
