#include <iostream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <getopt.h>

#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>

#include <lcmtypes/er_lcmtypes.h>
#include <simpleMMap/GridMap.hpp>
#include <vector>
#include <bot_frames/bot_frames.h>

using namespace std;

int main(int argc, char *argv[])
{
    setlinebuf(stdout);

    double minxy[2];
    double maxxy[2];
    minxy[0] = -10; 
    minxy[1] = -10;
    maxxy[0] = 10;
    maxxy[1] = 10;
    
    lcm_t *lcm = lcm_create(NULL);
    
    double gridmapMetersPerPixel = 0.1;
    
    GridMap *gridmap = new GridMap(minxy, maxxy, gridmapMetersPerPixel);
    
    int no_points = 500;
    double r = 4.0;
    
    for(int i=0; i< no_points; i++){
        double theta = 2*M_PI / no_points *i;
        
        double xy[2] = {r * cos(theta), r* sin(theta)}, xy_c[2] = {0};
        
        //void GridMap::rayTrace(const int start[2], const int end[2], int hitInc, int visitInc, bool hitAll)
        gridmap->rayTrace(xy_c, xy, 1.0, 1.0, false);
    }
    
    gridmap->generate_likelihoods();
    erlcm_gridmap_t * lcm_msg = gridmap->get_comp_gridmap_t();
    erlcm_gridmap_t_publish(lcm, "MAP_SERVER", lcm_msg);

    erlcm_floor_gridmap_t floor_msg;
    floor_msg.gridmap = *lcm_msg;
    floor_msg.floor_no = 2;
    erlcm_floor_gridmap_t_publish(lcm, "FMAP_SERVER", &floor_msg);
}
