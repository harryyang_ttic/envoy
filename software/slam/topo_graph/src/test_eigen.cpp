#include <iostream>
#include <Eigen/Dense>

using namespace Eigen;
using namespace std;


MatrixXf cov(const MatrixXf& x, const MatrixXf& y)
{
  const float num_observations = static_cast<float>(x.rows());
  const RowVectorXf x_mean = x.colwise().sum() / num_observations;
  const RowVectorXf y_mean = y.colwise().sum() / num_observations;
  return (x.rowwise() - x_mean).transpose() * (y.rowwise() - y_mean) / num_observations;
}

template <typename Derived, typename OtherDerived>
void calCov(const MatrixBase<Derived>& x, const MatrixBase<Derived>& y, MatrixBase<OtherDerived> const & C_)
{
  typedef typename Derived::Scalar Scalar;
  typedef typename internal::plain_row_type<Derived>::type RowVectorType;

  const Scalar num_observations = static_cast<Scalar>(x.rows());

  const RowVectorType x_mean = x.colwise().sum() / num_observations;
  const RowVectorType y_mean = y.colwise().sum() / num_observations;

  MatrixBase<OtherDerived>& C = const_cast< MatrixBase<OtherDerived>& >(C_);
  
  C.derived().resize(x.cols(),x.cols()); // resize the derived object
  C = (x.rowwise() - x_mean).transpose() * (y.rowwise() - y_mean) / num_observations;
}

template <typename Derived, typename OtherDerived>
void calCov1(const MatrixBase<Derived>& x, const MatrixBase<Derived>& y, MatrixBase<OtherDerived> const & C)
{
  typedef typename Derived::Scalar Scalar;
  typedef typename internal::plain_row_type<Derived>::type RowVectorType;

  const Scalar num_observations = static_cast<Scalar>(x.rows());

  const RowVectorType x_mean = x.colwise().sum() / num_observations;
  const RowVectorType y_mean = y.colwise().sum() / num_observations;

  const_cast< MatrixBase<OtherDerived>& >(C) =
    (x.rowwise() - x_mean).transpose() * (y.rowwise() - y_mean) / num_observations;
}

int main()
{
    double test[9] = {1, 0, 0, 0, 1, 1, 1,1,1};
    
    MatrixXf m = MatrixXf::Random(10000,3);

    //cout<< "Matrix " << m <<endl;

    MatrixXf C = cov(m, m);

    cout << "Covariance Mat : " <<  C <<endl;

    MatrixXf x = MatrixXf::Random(100,3);
    MatrixXf Cov;
    calCov(m, m, Cov);

    cout << "Cov Mat new Method : " << Cov <<endl;

    /*m = (m + MatrixXf::Constant(3,3,1.2)) * 50;
    cout << "m =" << endl << m << endl;
    VectorXf v(3);
    v << 1, 2, 3;
    cout << "m * v =" << endl << m * v << endl;
    */
    cout << "Solving for eigen values " <<endl;

    EigenSolver<MatrixXf> es( C );

    cout << "Done " << endl;

    cout << es.eigenvalues() << endl;

    /*for(int i=0;i< 5; i++){
        complex<double> lambda1 = es.eigenvalues()[i];
        cout << "Eigen Value : " << lambda1 << endl;
    }*/
}
