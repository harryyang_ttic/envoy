#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <bot_frames/bot_frames.h>

#ifdef __BOOST__
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/io/wkt/wkt.hpp>
#endif
#include <bot_lcmgl_client/lcmgl.h>
// ANN declarations
#include <ANN/ANN.h> 
#include <ANN/ANNx.h>
#include <ANN/ANNperf.h>

#ifdef __BOOST__
#include <boost/foreach.hpp>

#include <boost/geometry/geometries/adapted/boost_tuple.hpp>
BOOST_GEOMETRY_REGISTER_BOOST_TUPLE_CS(cs::cartesian)
using namespace boost::geometry;

using boost::make_tuple;
using boost::geometry::append;
typedef boost::geometry::model::polygon<boost::geometry::model::d2::point_xy<double> > polygon;
#endif

#include <Eigen/Eigenvalues>

using namespace std;

using namespace Eigen;

#define POSE_LIST_SIZE 10

typedef struct{
    float c[4]; 
} color_t;

typedef struct
{
    BotParam   *b_server;
    lcm_t      *lcm;
    GMainLoop *mainloop;
    pthread_t  work_thread;
    BotFrames *frames; 

    bot_lcmgl_t *lcmgl;

    GList *pose_list;     

    GList *node_list; 
    int no_segments; 
    color_t *pallet;
    //better to add a color pallet for each segmentation - to make things consistant
    erlcm_slam_pose_transform_list_t *tf;

    bot_core_pose_t *last_pose;
    int publish_all;
    int verbose;
    int recursive; 
    double n_cut_threshold; 
    
} state_t;

typedef struct{
    ANNpointArray dataPts;
    int no_points;
} ann_t;

int getTransformInd(erlcm_slam_pose_transform_list_t *tf, int64_t utime, int node_id){
    for(int i=0; i < tf->size; i++){
        //fprintf(stderr, "[%d] => Utime : %f %f ID : %d, %d\n", i, utime /1.0e6,  tf->trans[i].utime/1.0e6, 
        //      node_id, tf->trans[i].id);

        if(tf->trans[i].id == node_id && 
           tf->trans[i].utime == utime){
            return i;
        }
    }
    return -1;
}


//solve the eigen stuff

//need to pass along the indexes also 
//return the matrix ids
MatrixXd do_spectral_bisection(state_t *s, MatrixXd A, int size, MatrixXd ind, int segment_id, int *segmented, int recursive){

    *segmented = 0;

    //cout << "Here is the similarity matrix, A:" << endl << A << endl << endl;
    
    MatrixXd S = MatrixXd::Ones(size,1);

    MatrixXd D = MatrixXd::Zero(size,size);
    MatrixXd D_s = A*S; 

    for(int i=0; i < size; i++){
        D(i,i) = D_s(i,0);
    }

    //cout << "Diagonal Mat:" << endl << D << endl << endl;

    MatrixXd D_W = D - A;

    //cout << "Diagonal - W :" << endl << D_W << endl << endl;

    //

    //EigenSolver<MatrixXd> D_sq(D);
    SelfAdjointEigenSolver<MatrixXd> D_sq(D);
    MatrixXd D_sq_inv_val = D_sq.operatorInverseSqrt();

    MatrixXd D_sq_val = D_sq.operatorInverseSqrt();

    EigenSolver<MatrixXd> es( D_sq_inv_val * D_W * D_sq_inv_val);
    EigenSolver<MatrixXd> es_1( D_W );
    //cout << "The eigenvalues of A are:" << endl << es.eigenvalues() << endl;

    //cout << "The eigenvalues of A are:" << endl << es_1.eigenvalues() << endl;
    //cout << "The matrix of eigenvectors, V, is:" << endl << es.eigenvectors() << endl << endl;

    //calculate the Ncut associated with the segmentation 

    //figure out the second smallest eigen vector
    double min = 10000;
    double second_min = 10000;
    int second_ind = -1;
    int min_ind = -1;

    //MatrixXf::Index minRow, minCol;
     
    //float min = es.eigenvalues().minCoeff(&minRow, &minCol);

    for(int i=0;i< size; i++){
        complex<double> lambda1 = es.eigenvalues()[i];
        if(min > lambda1.real()){
            min = lambda1.real();
            min_ind = i; 
        }
    }

    for(int i=0;i< size; i++){
        complex<double> lambda1 = es.eigenvalues()[i];
        if(second_min > lambda1.real() && min < lambda1.real()){
            second_min = lambda1.real();
            second_ind = i; 
        }
    }

    //cout << "Min Value Ind : " << min_ind << " Second Min " << second_ind << "Second Val : " << second_min << endl;
    
    //cout << "The matrix of eigenvectors, V, is:" << endl << es.eigenvectors() << endl << endl;
    
    if(second_ind >=0){
        //VectorXcd v1 = es.eigenvectors().col(second_ind);
        MatrixXd v = MatrixXd::Zero(size,1);
        for(int i=0;i< size; i++){
            v(i,0) = es.eigenvectors()(i, second_ind).real();
        }

        MatrixXd v_eig = D_sq_inv_val * v;

        double mean = 0;
        for(int i=0;i< size; i++){
            mean += v_eig(i,0);
        }

        mean = mean / (double) size;

        //cout << "Second Smallest Eigen Vector " <<endl << v_eig<< endl;
        complex<double> lambda1 = es.eigenvalues()[second_ind];
        MatrixXd top =  v_eig.transpose() * D_W * v_eig;

        //not sure why bottom is always 1
        MatrixXd bottom = v_eig.transpose() * D * v_eig;
        //cout << "Top : " << top << endl;

        //cout << "Bottom : " << bottom << endl;

        double N_cut = top(0,0) / bottom(0,0);
        cout << "N Cut " << N_cut << endl; 

        if(N_cut > s->n_cut_threshold){// > 0.7){
            MatrixXd final_result = MatrixXd::Zero(size,1);
            for(int i=0; i< size; i++){
                final_result(i) = segment_id;
            }
            *segmented = 1;
            return final_result;
        }        

        //calculate the mean of the eigen vector 
        int count_1 = 0;
        int count_2 = 0;

        for(int i=0;i< size; i++){
            if(v(i,0) <= mean){
                count_1 += 1;
                cout << "Node : " << ind(i) << " : -1" << endl;
            }
            else{
                count_2 += 1;
                cout << "Node : " << ind(i) << " : +1" << endl;
            }
        }

        if(recursive == 0){// > 0.7){
            MatrixXd final_result = MatrixXd::Zero(size,1);

            for(int i=0;i< size; i++){
                if(v(i,0) <= mean){
                    final_result(i) = segment_id;
                }
                else{
                    final_result(i) = segment_id +1;
                }
            }

            *segmented = 2;
            return final_result;
        }

        MatrixXd ind_1 = MatrixXd::Zero(count_1,1);
        MatrixXd ind_2 = MatrixXd::Zero(count_2,1);

        int index_1 = 0;
        int index_2 = 0;

        for(int i=0;i< size; i++){
            if(v(i,0) <= mean){
                ind_1(index_1) = i;
                index_1 += 1;
            }
            else{
                ind_2(index_2) = i;
                index_2 += 1;
            }
        }

        //cout << "Index 1 " << endl<< ind_1 << endl;
        //cout << "Index 2 " << endl << ind_2 << endl;

        MatrixXd A_1 = MatrixXd::Zero(count_1,count_1);
        MatrixXd A_2 = MatrixXd::Zero(count_2,count_2);
        MatrixXd index_list_1 = MatrixXd::Zero(count_1,1);
        MatrixXd index_list_2 = MatrixXd::Zero(count_2,1);
                
        for(int i=0;i< count_1; i++){
            int i_1 = ind_1(i);
            for(int j=0;j< count_1; j++){
                int j_1 = ind_1(j);
                //cout << i_1 << "," << j_1 << " : " << A(i_1,j_1); 
                A_1(i,j) = A(i_1,j_1);                
            }
            index_list_1(i) = ind(i_1);
        }

        //cout << "A 1 " << endl<< A_1 << endl;
        
        for(int i=0;i< count_2; i++){
            int i_2 = ind_2(i);
            for(int j=0;j< count_2; j++){
                int j_2 = ind_2(j);
                //cout << i_2 << "," << j_2 << " : " << A(i_2,j_2); 
                A_2(i,j) = A(i_2,j_2);
            }
            index_list_2(i) = ind(i_2);
        }

        MatrixXd result_1, result_2;

        //just indicate the split for now 
        int segmented_1 = 0, segmented_2 = 0;

        MatrixXd final_result = MatrixXd::Zero(size,1);

        if(count_1 >1){
            fprintf(stderr,"Calling Bisection on the minus set : %d\n", count_1);
            result_1 = do_spectral_bisection(s, A_1, count_1, index_list_1, segment_id, &segmented_1, 1);

            cout << "Segmented Result : " << segmented_1 << endl; 
            cout << "result 1 : " << endl << result_1; 
            for(int i=0;i< count_1; i++){
                int i_1 = ind_1(i);
                final_result(i_1) = result_1(i);                
            }
            *segmented += segmented_1;
        }
        else{
            //one element 
            for(int i=0;i< count_1; i++){
                int i_1 = ind_1(i);
                final_result(i_1) = segment_id;                
            }
            *segmented++;
        }

        segment_id += *segmented; 
       
        if(count_2>1){
            fprintf(stderr,"Calling Bisection on the plus set : %d\n", count_2);
            result_2 = do_spectral_bisection(s, A_2, count_2, index_list_2, segment_id, &segmented_2, 1);

            for(int i=0;i< count_2; i++){
                int i_2 = ind_2(i);
                final_result(i_2) = result_2(i);                
            }
            *segmented += segmented_2;
        }
        else{
            //one element 
            for(int i=0;i< count_2; i++){
                int i_1 = ind_2(i);
                final_result(i_1) = segment_id + *segmented;                
            }
            *segmented++;
        }
       
        cout << "Done "<< endl; 
        
        return final_result;
    }
}

static void on_slam_node(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
			     const erlcm_slam_graph_node_t* msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 

    //check if the region is -1 or known region 
    fprintf(stderr, "Node : %f Region ID : %d\n", msg->utime / 1.0e6, msg->region_id);
    if(msg->region_id >=0)
        return;

    fprintf(stderr,"Adding Node to list\n");
    
    erlcm_slam_graph_node_t *node = erlcm_slam_graph_node_t_copy(msg);

    fprintf(stderr, "Size of List (at start): %d\n", g_list_length(s->node_list));

    fprintf(stderr,"Adding\n");
    s->node_list = g_list_append(s->node_list, (gpointer)(node));

    if(s->verbose){
        fprintf(stderr,"N");
    }
}

//similarity calculation seems to work fine 

//other option is to use the point corrospondance function
//use a check gridmap or a kdtree 

void calculatePointOverlapkdTreeOld(state_t *s){
    if(s->tf == NULL || s->node_list == NULL || s->tf->size <=1){
        fprintf(stderr,"Not enough nodes\n");
        return;
    }

    erlcm_slam_graph_node_t **node_list = (erlcm_slam_graph_node_t **)calloc(g_list_length(s->node_list), sizeof(erlcm_slam_graph_node_t *));

    int size_list = g_list_length(s->node_list);
    
    GList *list = s->node_list;
    GList *elem;
    int ind = 0;
    int64_t start_utime = bot_timestamp_now();
    int size = 0;
    int unassigned_size = 0;

    bot_lcmgl_t *lcmgl = s->lcmgl;

    for(elem = list; elem; elem = elem->next) {
        erlcm_slam_graph_node_t *curr_node = (erlcm_slam_graph_node_t *)(elem->data);
        //now all of these should be different regions
        //we need to see if these are also valid and in the slam graph
        if(curr_node->region_id == -1){
            //serach through the transform list 
            int ind = getTransformInd(s->tf, curr_node->utime, curr_node->id);
            if(ind >=0){
                unassigned_size++;            
            }
            else{
                fprintf(stderr,"No correct transform found\n");
            }
        }

    }

    if(lcmgl){
        double global_to_local[12];
        if (!bot_frames_get_trans_mat_3x4 (s->frames, "global",
                                           "local",  
                                           global_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            return;
        }

        bot_lcmgl_line_width(lcmgl, 2);
        bot_lcmgl_point_size(lcmgl, 3);
        
        /*for(int i=0; i < s->no_segments; i++){
            for(elem = list; elem; elem = elem->next) {
                erlcm_slam_graph_node_t *curr_node = (erlcm_slam_graph_node_t *)(elem->data);
                if(curr_node->region_id != i){
                    continue;
                }
                    
                double pos_g[3] = { s->tf->trans[curr_node->id].pos[0], 
                                  s->tf->trans[curr_node->id].pos[1],
                                    0}, pos_l[3];

                bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);
                    
                lcmglColor4f(s->pallet[i].c[0], 
                             s->pallet[i].c[1], 
                             s->pallet[i].c[2], 
                             s->pallet[i].c[3]);
                lcmglCircle(pos_l, 0.4);
                    
                char score_info[100];
                sprintf(score_info,"[%d] => %d\n", curr_node->id, curr_node->region_id);
                bot_lcmgl_text(lcmgl,pos_l,score_info);
            }
            }*/

        for(elem = list; elem; elem = elem->next) {
            erlcm_slam_graph_node_t *curr_node = (erlcm_slam_graph_node_t *)(elem->data);
            if(curr_node->region_id == -1){
                int index = getTransformInd(s->tf, curr_node->utime, curr_node->id);//-1;
                
                if(index == -1){
                    fprintf(stderr,"Correct transform not found\n");
                    continue;
                }

                double pos_g[3] = { s->tf->trans[index].pos[0], 
                                    s->tf->trans[index].pos[1],
                                    0}, pos_l[3];
                    
                bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);

                lcmglColor3f(1.0, 0.0, 0.0);
                lcmglCircle(pos_l, 0.4);
                    
                char score_info[100];
                sprintf(score_info,"[%d] => %d\n", curr_node->id, curr_node->region_id);
                bot_lcmgl_text(lcmgl,pos_l,score_info);
            }
        }
    }

    //switch buffer 
    bot_lcmgl_switch_buffer(lcmgl);
    
    fprintf(stderr," Unassigned Size : %d\n", unassigned_size);
    size = unassigned_size; 

    ann_t *plist = (ann_t *)calloc(size_list, sizeof(ann_t));

    MatrixXd index_list = MatrixXd::Zero(size,1);

    unassigned_size = 0;

    ind = 0;
    
    for(elem = list; elem; elem = elem->next) {
        erlcm_slam_graph_node_t *curr_node = (erlcm_slam_graph_node_t *)(elem->data);
        node_list[ind] = curr_node;
        fprintf(stderr,"ID : %d => region : %d\n", curr_node->id, curr_node->region_id);

        index_list(unassigned_size) = ind; 
        
        if(curr_node->region_id != -1){
            ind++;  
            continue;
        }

        int index = getTransformInd(s->tf, curr_node->utime, curr_node->id);

        if(index == -1){
            fprintf(stderr,"Not adding points - skipping - note: this can break the code\n");
            ind++;  
            continue;
        }
        
        double pBody[3] = { 0, 0, 0 };
        double pLocal[3];
        BotTrans bodyToLocal;
        bodyToLocal.trans_vec[0] = s->tf->trans[index].pos[0];
        bodyToLocal.trans_vec[1] = s->tf->trans[index].pos[1];
        bodyToLocal.trans_vec[2] = 0;
        double rpy[3] = { curr_node->rp[0], curr_node->rp[1], s->tf->trans[index].t };
        fprintf(stderr, "%d (%d) => %f,%f,%f\n", ind, unassigned_size, s->tf->trans[index].pos[0], 
                s->tf->trans[ind].pos[1], s->tf->trans[index].t);
        bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);

        plist[unassigned_size].dataPts = annAllocPts(curr_node->pl.no, 2);
        
        plist[unassigned_size].no_points = curr_node->pl.no;
        
        for(int i=0; i< curr_node->pl.no; i++){
            //fprintf(stderr,"Adding : %d\n", i);
            pBody[0] = curr_node->pl.points[i].pos[0];
            pBody[1] = curr_node->pl.points[i].pos[1];
            bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
            plist[unassigned_size].dataPts[i][0] = pLocal[0];
            plist[unassigned_size].dataPts[i][1] = pLocal[1];            
        }

        unassigned_size++;            
        ind++;  
    }
    
    
    int dim = 2; 

    //create multiple kd trees 
    ANNkd_tree**  kdTree = (ANNkd_tree **)calloc(size, sizeof(ANNkd_tree *));
    for(int i = 0; i< size; i++){
        kdTree[i] = new ANNkd_tree(plist[i].dataPts, 
                                   plist[i].no_points,
                                   dim);
    }

    double radius_bound = 0.2;
    int query_size = 100;
    double epsilon =  0.0;

    ANNidxArray  nnIdx = new ANNidx[query_size];
    ANNdistArray dists = new ANNdist[query_size];

    fprintf(stderr,"Done building tree\n");

    //the count seems to be wrong 

    //need a matrix that (not symetric)
    double **score_matrix = (double **) calloc(size, sizeof(double *));
    
    for(int i = 0; i< size; i++){
        //fprintf(stderr,"Searching in : %d\n", i);
        score_matrix[i] = (double *) calloc(size, sizeof(double));
        for(int j=0; j < size; j++){
            int count = 0;
            if(i == j){
                score_matrix[i][j] = 1;//plist[j].no_points;
                continue;
            }
            //fprintf(stderr,"\tSearching : %d => %d\n", j, plist[j].no_points);
            
            for(int qp=0; qp < plist[j].no_points; qp++){ 
                kdTree[i]->annkFRSearch(
                                       plist[j].dataPts[qp],			// query point
                                       ANN_POW(radius_bound),	// squared radius search bound
                                       query_size,				// number of near neighbors
                                       nnIdx,			// nearest neighbors (returned)
                                       dists,				// distance (returned)
                                       epsilon);
                
                int found = 0;
                for(int l=0;l< query_size ;l++){
                    if(nnIdx[l]==qp){//same point or invalid points
                        continue;
                    }
                    if(nnIdx[l] < 0){//because we have reached the end of the valid neighbours
                        break; 
                        //continue;
                    }
                    found = 1;
                }
                if(found)
                    count++;
            }
            if(plist[j].no_points > 0){
                score_matrix[i][j] = count / (double) plist[j].no_points;
            }
            else{
                //ideally we should not even check for this here 
                score_matrix[i][j] = 0;
            }
        }
    }

    for(int i=0; i<size;i++){
        for(int j=0; j< size; j++){
            if(i<j)
                continue;
            //sim_matrix
            double comb_score = (score_matrix[i][j] + score_matrix[j][i])/2;
            score_matrix[i][j] = comb_score;
            score_matrix[j][i] = comb_score;
        }
    }

    fprintf(stderr,"\nScore Matrix - Sym\n");
    for(int i=0; i<size;i++){
        fprintf(stderr,"\n");
        for(int j=0; j< size; j++){
            fprintf(stderr,"\t%f", score_matrix[i][j]);
        }
    }

    fprintf(stderr,"\n");
    //solve the eigen stuff

    MatrixXd A = MatrixXd::Zero(size,size);

    for(int i=0; i<size;i++){
        for(int j=0; j< size; j++){
            A(i,j) = score_matrix[i][j];
        }
    }
    
    int no_segments = 0;

    MatrixXd result = do_spectral_bisection(s, A,size, index_list, s->no_segments, &no_segments, s->recursive);

    if(no_segments > 1){
        //this is a bit funky - since the segment numbering isnt ordered 
        s->pallet = (color_t *)realloc(s->pallet, sizeof(color_t)*(s->no_segments+no_segments));
        
        //creates and maintains a consistant color for each segment 
        for(int i=0; i < no_segments; i++){
            bot_color_util_rand_color(s->pallet[i +  s->no_segments].c, 0.7, 0.3);
        }
        s->no_segments += no_segments;// -1; 
        //name the id's of the oldest ones (not the last one) 
        fprintf(stderr,"Segments Found : -> naming \n");

        //we need to figure out which ones to label and which ones not to 
        //check the last index - and ignore that one's label
        int ignore_segment = result(size-1);
        
        for(int i=0; i< size; i++){
            if(result(i) != ignore_segment){
                int i_1 = index_list(i);
                node_list[i_1]->region_id = result(i); 
            }            
        }
        //we should give them region id's
    }
    
    cout << "Final Segmentation " << endl << result << endl;

    //we need to look at the second smallest eigen value (and it's vector)

    erlcm_graph_similarity_t g_msg; 
    g_msg.no_nodes = size;
    g_msg.node_ids = (int32_t *) calloc(size, sizeof(int32_t));
    g_msg.similarity = score_matrix; 

    erlcm_graph_similarity_t_publish(s->lcm, "SLAM_GRAPH_SIMILARITY", &g_msg);
    free(g_msg.node_ids);

    for(int i=0; i<size;i++){
        delete kdTree[i];
        delete plist[ind].dataPts;
    }
    free(kdTree);
    free(plist);
    delete nnIdx;
    delete dists;

    for(int i=0; i < size; i++){
        free(score_matrix[i]);
    }
    free(score_matrix);
    //save the scores and do a segmentation 
    
    int64_t end_utime = bot_timestamp_now();

    fprintf(stderr,"\n++++ Time elapse : %f\n", (end_utime - start_utime)/1.0e3);
}


void calculatePointOverlapkdTree(state_t *s){
    if(s->tf == NULL || s->node_list == NULL || s->tf->size <=1){
        fprintf(stderr,"Not enough nodes\n");
        return;
    }
    
    GList *list = s->node_list;
    GList *elem;
    int ind = 0;
    int64_t start_utime = bot_timestamp_now();
    int size = 0;
    int unassigned_size = 0;

    bot_lcmgl_t *lcmgl = s->lcmgl;

    for(elem = list; elem; elem = elem->next) {
        erlcm_slam_graph_node_t *curr_node = (erlcm_slam_graph_node_t *)(elem->data);
        //now all of these should be different regions
        //we need to see if these are also valid and in the slam graph
        if(curr_node->region_id == -1){
            //serach through the transform list 
            int ind = getTransformInd(s->tf, curr_node->utime, curr_node->id);
            if(ind >=0){
                unassigned_size++;            
            }
            else{
                fprintf(stderr,"No correct transform found\n");
            }
        }

    }

    if(unassigned_size < 2){
        fprintf(stderr,"Not enough valid nodes : %d \n", unassigned_size);
        return;
    }

    erlcm_slam_graph_node_t **node_list = (erlcm_slam_graph_node_t **)calloc(unassigned_size, sizeof(erlcm_slam_graph_node_t *));

    int size_list = unassigned_size; 

    if(lcmgl){
        double global_to_local[12];
        if (!bot_frames_get_trans_mat_3x4 (s->frames, "global",
                                           "local",  
                                           global_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            return;
        }

        bot_lcmgl_line_width(lcmgl, 2);
        bot_lcmgl_point_size(lcmgl, 3);
        
        for(elem = list; elem; elem = elem->next) {
            erlcm_slam_graph_node_t *curr_node = (erlcm_slam_graph_node_t *)(elem->data);
            if(curr_node->region_id == -1){
                int index = getTransformInd(s->tf, curr_node->utime, curr_node->id);//-1;
                
                if(index == -1){
                    fprintf(stderr,"Correct transform not found\n");
                    continue;
                }

                double pos_g[3] = { s->tf->trans[index].pos[0], 
                                    s->tf->trans[index].pos[1],
                                    0}, pos_l[3];
                    
                bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);

                lcmglColor3f(1.0, 0.0, 0.0);
                lcmglCircle(pos_l, 0.4);
                    
                char score_info[100];
                sprintf(score_info,"[%d] => %d\n", curr_node->id, curr_node->region_id);
                bot_lcmgl_text(lcmgl,pos_l,score_info);
            }
        }
    }

    //switch buffer 
    bot_lcmgl_switch_buffer(lcmgl);
    
    fprintf(stderr," Unassigned Size : %d\n", unassigned_size);
    size = unassigned_size; 

    ann_t *plist = (ann_t *)calloc(size_list, sizeof(ann_t));

    MatrixXd index_list = MatrixXd::Zero(size,1);

    unassigned_size = 0;

    ind = 0;

    //you might not need to create a kd tree every time - but keep them in body frame 
    for(elem = list; elem; elem = elem->next) {
        erlcm_slam_graph_node_t *curr_node = (erlcm_slam_graph_node_t *)(elem->data);
        node_list[ind] = curr_node;
        fprintf(stderr,"ID : %d => region : %d\n", curr_node->id, curr_node->region_id);

        index_list(ind) = ind; 
        
        if(curr_node->region_id != -1){
            //ind++;  
            continue;
        }

        int index = getTransformInd(s->tf, curr_node->utime, curr_node->id);

        if(index == -1){
            fprintf(stderr,"Not adding points - skipping - note: this can break the code\n");
            //ind++;  
            continue;
        }
        
        double pBody[3] = { 0, 0, 0 };
        double pLocal[3];
        BotTrans bodyToLocal;
        bodyToLocal.trans_vec[0] = s->tf->trans[index].pos[0];
        bodyToLocal.trans_vec[1] = s->tf->trans[index].pos[1];
        bodyToLocal.trans_vec[2] = 0;
        double rpy[3] = { curr_node->rp[0], curr_node->rp[1], s->tf->trans[index].t };
        fprintf(stderr, "%d (%d) => %f,%f,%f\n", ind, ind, s->tf->trans[index].pos[0], 
                s->tf->trans[ind].pos[1], s->tf->trans[index].t);
        bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);

        plist[ind].dataPts = annAllocPts(curr_node->pl.no, 2);
        
        plist[ind].no_points = curr_node->pl.no;
        
        for(int i=0; i< curr_node->pl.no; i++){
            pBody[0] = curr_node->pl.points[i].pos[0];
            pBody[1] = curr_node->pl.points[i].pos[1];
            bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
            plist[ind].dataPts[i][0] = pLocal[0];
            plist[ind].dataPts[i][1] = pLocal[1];            
        }

        //        unassigned_size++;            
        ind++;  
    }
    
    int dim = 2; 

    //create multiple kd trees 
    ANNkd_tree**  kdTree = (ANNkd_tree **)calloc(size, sizeof(ANNkd_tree *));
    for(int i = 0; i< size; i++){
        kdTree[i] = new ANNkd_tree(plist[i].dataPts, 
                                   plist[i].no_points,
                                   dim);
    }

    double radius_bound = 0.2;
    int query_size = 100;
    double epsilon =  0.0;

    ANNidxArray  nnIdx = new ANNidx[query_size];
    ANNdistArray dists = new ANNdist[query_size];

    fprintf(stderr,"Done building tree\n");

    //the count seems to be wrong 

    //need a matrix that (not symetric)
    double **score_matrix = (double **) calloc(size, sizeof(double *));
    
    for(int i = 0; i< size; i++){
        //fprintf(stderr,"Searching in : %d\n", i);
        score_matrix[i] = (double *) calloc(size, sizeof(double));
        for(int j=0; j < size; j++){
            int count = 0;
            if(i == j){
                score_matrix[i][j] = 1;
                continue;
            }
            
            for(int qp=0; qp < plist[j].no_points; qp++){ 
                kdTree[i]->annkFRSearch(
                                       plist[j].dataPts[qp],			// query point
                                       ANN_POW(radius_bound),	// squared radius search bound
                                       query_size,				// number of near neighbors
                                       nnIdx,			// nearest neighbors (returned)
                                       dists,				// distance (returned)
                                       epsilon);
                
                int found = 0;
                for(int l=0;l< query_size ;l++){
                    if(nnIdx[l]==qp){//same point or invalid points
                        continue;
                    }
                    if(nnIdx[l] < 0){//because we have reached the end of the valid neighbours
                        break; 
                        //continue;
                    }
                    found = 1;
                }
                if(found)
                    count++;
            }
            if(plist[j].no_points > 0){
                score_matrix[i][j] = count / (double) plist[j].no_points;
            }
            else{
                //ideally we should not even check for this here 
                score_matrix[i][j] = 0;
            }
        }
    }

    for(int i=0; i<size;i++){
        for(int j=0; j< size; j++){
            if(i<j)
                continue;
            //sim_matrix
            double comb_score = (score_matrix[i][j] + score_matrix[j][i])/2;
            score_matrix[i][j] = comb_score;
            score_matrix[j][i] = comb_score;
        }
    }

    fprintf(stderr,"\nScore Matrix - Sym\n");
    for(int i=0; i<size;i++){
        fprintf(stderr,"\n");
        for(int j=0; j< size; j++){
            fprintf(stderr,"\t%f", score_matrix[i][j]);
        }
    }

    fprintf(stderr,"\n");
    //solve the eigen stuff

    MatrixXd A = MatrixXd::Zero(size,size);

    for(int i=0; i<size;i++){
        for(int j=0; j< size; j++){
            A(i,j) = score_matrix[i][j];
        }
    }
    
    int no_segments = 0;
    
    //should start with 0
    MatrixXd result = do_spectral_bisection(s, A,size, index_list, /*s->no_segments*/ 0, &no_segments, s->recursive);

    erlcm_slam_graph_node_list_t region_id_msg;
    
    region_id_msg.utime = bot_timestamp_now();
    region_id_msg.size = 0;
    region_id_msg.list = (erlcm_slam_graph_node_t *) calloc(size, sizeof(erlcm_slam_graph_node_t));
    
    if(no_segments > 1){
        //this is a bit funky - since the segment numbering isnt ordered 
        s->pallet = (color_t *)realloc(s->pallet, sizeof(color_t)*(s->no_segments+no_segments));
        
        //creates and maintains a consistant color for each segment 
        for(int i=0; i < no_segments; i++){
            bot_color_util_rand_color(s->pallet[i +  s->no_segments].c, 0.7, 0.3);
        }
        s->no_segments += no_segments;// -1; 
        //name the id's of the oldest ones (not the last one) 
        fprintf(stderr,"Segments Found : -> naming \n");

        //we need to figure out which ones to label and which ones not to 
        //check the last index - and ignore that one's label
        int ignore_segment = result(size-1);
        
        //now we know which ones have been labled
        fprintf(stderr,"Ignore Segment ID : %d\n", ignore_segment);
        for(int i=0; i< size; i++){
            if(result(i) != ignore_segment){
                region_id_msg.size++;
                //int i_1 = index_list(i);
                fprintf(stderr,"Node ID : %d => Segment : %d\n", node_list[i]->id , (int) result(i));
                region_id_msg.list[region_id_msg.size-1].utime = node_list[i]->utime;
                region_id_msg.list[region_id_msg.size-1].id = node_list[i]->id;
                region_id_msg.list[region_id_msg.size-1].floor_id = node_list[i]->floor_id;
                region_id_msg.list[region_id_msg.size-1].region_id = (int) result(i);   
                node_list[i]->region_id = result(i);
            }            
        }
        //we should give them region id's
    }
    if(region_id_msg.size >0)
        erlcm_slam_graph_node_list_t_publish(s->lcm, "REGION_SEGMENTATION_RESULT", &region_id_msg);

    free(region_id_msg.list);

    list = s->node_list;
    for(guint i=0; i < g_list_length(s->node_list); i++){
        elem = g_list_nth(s->node_list, i);
        erlcm_slam_graph_node_t *curr_node = (erlcm_slam_graph_node_t *) (elem->data);
        if(curr_node->region_id >=0){            
            fprintf(stderr, "Removing Node ID : %d - region : %d\n", curr_node->id, curr_node->region_id);        
            erlcm_slam_graph_node_t_destroy(curr_node);
            s->node_list = g_list_delete_link (s->node_list, elem);
            i--;
        }        
    }
    
    /*for(elem = list; elem; elem = elem->next) {
        erlcm_slam_graph_node_t *curr_node = (erlcm_slam_graph_node_t *)(elem->data);
        fprintf(stderr, "Node ID : %d - region : %d\n", curr_node->id, curr_node->region_id);        
        if(curr_node->region_id >=0){
            GList *elem_prev = g_list_previous(elem);
            //g_list_remove (elem);
            s->node_list = g_list_delete_link (s->node_list, elem);
            elem = elem_prev; 
            fprintf(stderr, "Destroying ID : %d\n", curr_node->id); 
            erlcm_slam_graph_node_t_destroy(curr_node);
        }
        }*/

    cout << "Final Segmentation " << endl << result << endl;

    //we need to look at the second smallest eigen value (and it's vector)

    erlcm_graph_similarity_t g_msg; 
    g_msg.no_nodes = size;
    g_msg.node_ids = (int32_t *) calloc(size, sizeof(int32_t));
    g_msg.similarity = score_matrix; 

    erlcm_graph_similarity_t_publish(s->lcm, "SLAM_GRAPH_SIMILARITY", &g_msg);
    free(g_msg.node_ids);

    for(int i=0; i<size;i++){
        delete kdTree[i];
        delete plist[ind].dataPts;
    }
    free(kdTree);
    free(plist);
    delete nnIdx;
    delete dists;

    free(node_list);

    for(int i=0; i < size; i++){
        free(score_matrix[i]);
    }
    free(score_matrix);
    //save the scores and do a segmentation 
    
    int64_t end_utime = bot_timestamp_now();

    fprintf(stderr,"\n++++ Time elapse : %f\n", (end_utime - start_utime)/1.0e3);
}



void calculatePointOverlapkdTree_single(state_t *s){
    if(s->tf == NULL || s->node_list == NULL || s->tf->size <=1){
        fprintf(stderr,"Not enough nodes\n");
        return;
    }

    erlcm_slam_graph_node_t **node_list = (erlcm_slam_graph_node_t **)calloc(g_list_length(s->node_list), sizeof(erlcm_slam_graph_node_t *));

    int size = g_list_length(s->node_list);
    
    GList *list = s->node_list;
    GList *elem;
    int ind = 0;


    int64_t start_utime = bot_timestamp_now();
    //ANNpointArray *dataPts = (ANNpointArray *) calloc(size, sizeof(ANNpointArray));
    ann_t *plist = (ann_t *)calloc(size, sizeof(ann_t));
        
    /*bot_lcmgl_line_width(s->lcmgl, 1);
    bot_lcmgl_point_size(s->lcmgl, 3);
    bot_lcmgl_begin(s->lcmgl, GL_POINTS);
    bot_lcmgl_color3f(s->lcmgl, 1,0,0);
    */
    //while (g_list_next(list)) {
    for(elem = list; elem; elem = elem->next) {
        erlcm_slam_graph_node_t *curr_node = (erlcm_slam_graph_node_t *)(elem->data);
        node_list[ind] = curr_node;
        fprintf(stderr,"ID : %d\n", curr_node->id);

        double pBody[3] = { 0, 0, 0 };
        double pLocal[3];
        BotTrans bodyToLocal;
        bodyToLocal.trans_vec[0] = s->tf->trans[curr_node->id].pos[0];
        bodyToLocal.trans_vec[1] = s->tf->trans[curr_node->id].pos[1];
        bodyToLocal.trans_vec[2] = 0;
        double rpy[3] = { curr_node->rp[0], curr_node->rp[1], s->tf->trans[curr_node->id].t };
        fprintf(stderr, "%d => %f,%f,%f\n", ind, s->tf->trans[curr_node->id].pos[0], 
                s->tf->trans[curr_node->id].pos[1], s->tf->trans[curr_node->id].t);
        bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);

        plist[ind].dataPts = annAllocPts(curr_node->pl.no, 2);
        
        //ANNpointArray dp = plist[ind].dataPts;
        plist[ind].no_points = curr_node->pl.no;
        
        for(int i=0; i< curr_node->pl.no; i++){
            //fprintf(stderr,"Adding : %d\n", i);
            pBody[0] = curr_node->pl.points[i].pos[0];
            pBody[1] = curr_node->pl.points[i].pos[1];
            bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
            plist[ind].dataPts[i][0] = pLocal[0];//curr_node->pl.points[i].pos[0];
            plist[ind].dataPts[i][1] = pLocal[1];//curr_node->pl.points[i].pos[1];

            //bot_lcmgl_color3f(lcmgl_point_cloud, color3fv[0], color3fv[1], color3fv[2]);
            //bot_lcmgl_vertex3f(lcmgl_point_area, pLocal[0], pLocal[1], t);
        }
        ind++;  
    }
    
    
    int dim = 2; 

    ANNkd_tree**  kdTree = (ANNkd_tree **)calloc(size, sizeof(ANNkd_tree *));
    for(int i = 0; i< size; i++){
        kdTree[i] = new ANNkd_tree(plist[i].dataPts, 
                                   plist[i].no_points,
                                   dim);

    }

    double radius_bound = 0.2;
    int query_size = 100;
    double epsilon =  0.0;

    ANNidxArray  nnIdx = new ANNidx[query_size];
    ANNdistArray dists = new ANNdist[query_size];

    fprintf(stderr,"Done building tree\n");

    //the count seems to be wrong 

    //need a matrix that (not symetric)
    double **score_matrix = (double **) calloc(size, sizeof(double *));
    
    for(int i = 0; i< size; i++){
        //fprintf(stderr,"Searching in : %d\n", i);
        score_matrix[i] = (double *) calloc(size, sizeof(double));
        for(int j=0; j < size; j++){
            int count = 0;
            if(i == j){
                score_matrix[i][j] = 1;//plist[j].no_points;
                continue;
            }
            //fprintf(stderr,"\tSearching : %d => %d\n", j, plist[j].no_points);
            
            for(int qp=0; qp < plist[j].no_points; qp++){ 
                kdTree[i]->annkFRSearch(
                                       plist[j].dataPts[qp],			// query point
                                       ANN_POW(radius_bound),	// squared radius search bound
                                       query_size,				// number of near neighbors
                                       nnIdx,			// nearest neighbors (returned)
                                       dists,				// distance (returned)
                                       epsilon);
                
                int found = 0;
                for(int l=0;l< query_size ;l++){
                    if(nnIdx[l]==qp){//same point or invalid points
                        continue;
                    }
                    if(nnIdx[l] < 0){//because we have reached the end of the valid neighbours
                        break; 
                        //continue;
                    }
                    found = 1;
                }
                if(found)
                    count++;
            }           
            //fprintf(stderr,"Count :%d,%d => %d == Score : %f\n", i, j, count, count / (double) plist[j].no_points);
            score_matrix[i][j] = count / (double) plist[j].no_points;
        }
    }

    /*fprintf(stderr,"Score Matrix - Prior\n");
    for(int i=0; i<size;i++){
        fprintf(stderr,"\n");
        for(int j=0; j< size; j++){
            fprintf(stderr,"\t%f", score_matrix[i][j]);
        }
        }*/

    for(int i=0; i<size;i++){
        for(int j=0; j< size; j++){
            if(i<j)
                continue;
            //sim_matrix
            double comb_score = (score_matrix[i][j] + score_matrix[j][i])/2;
            score_matrix[i][j] = comb_score;
            score_matrix[j][i] = comb_score;
        }
    }

    fprintf(stderr,"\nScore Matrix - Sym\n");
    for(int i=0; i<size;i++){
        fprintf(stderr,"\n");
        for(int j=0; j< size; j++){
            fprintf(stderr,"\t%f", score_matrix[i][j]);
        }
    }

    fprintf(stderr,"\n");

    //solve the eigen stuff

    MatrixXd A = MatrixXd::Zero(size,size);

    for(int i=0; i<size;i++){
        for(int j=0; j< size; j++){
            A(i,j) = score_matrix[i][j];
        }
    }

    //do_spectral_bisection(A,size);

    cout << "Here is the similarity matrix, A:" << endl << A << endl << endl;
    
    MatrixXd S = MatrixXd::Ones(size,1);

    MatrixXd D = MatrixXd::Zero(size,size);
    MatrixXd D_s = A*S; 

    for(int i=0; i < size; i++){
        D(i,i) = D_s(i,0);
    }

    cout << "Diagonal Mat:" << endl << D << endl << endl;

    MatrixXd D_W = D - A;

    cout << "Diagonal - W :" << endl << D_W << endl << endl;

    //

    //EigenSolver<MatrixXd> D_sq(D);
    SelfAdjointEigenSolver<MatrixXd> D_sq(D);
    MatrixXd D_sq_inv_val = D_sq.operatorInverseSqrt();

    MatrixXd D_sq_val = D_sq.operatorInverseSqrt();

    EigenSolver<MatrixXd> es( D_sq_inv_val * D_W * D_sq_inv_val);
    EigenSolver<MatrixXd> es_1( D_W );
    cout << "The eigenvalues of A are:" << endl << es.eigenvalues() << endl;

    cout << "The eigenvalues of A are:" << endl << es_1.eigenvalues() << endl;
    //cout << "The matrix of eigenvectors, V, is:" << endl << es.eigenvectors() << endl << endl;

    //calculate the Ncut associated with the segmentation 

    //figure out the second smallest eigen vector
    double min = 10000;
    double second_min = 10000;
    int second_ind = -1;
    int min_ind = -1;

    //MatrixXf::Index minRow, minCol;
     
    //float min = es.eigenvalues().minCoeff(&minRow, &minCol);

    for(int i=0;i< size; i++){
        complex<double> lambda1 = es.eigenvalues()[i];
        if(min > lambda1.real()){
            min = lambda1.real();
            min_ind = i; 
        }
    }

    for(int i=0;i< size; i++){
        complex<double> lambda1 = es.eigenvalues()[i];
        if(second_min > lambda1.real() && min < lambda1.real()){
            second_min = lambda1.real();
            second_ind = i; 
        }
    }

    //cout << "Min Value Ind : " << min_ind << " Second Min " << second_ind << "Second Val : " << second_min << endl;
    
    //cout << "The matrix of eigenvectors, V, is:" << endl << es.eigenvectors() << endl << endl;
    
    if(second_ind >=0){
        //VectorXcd v1 = es.eigenvectors().col(second_ind);
        MatrixXd v = MatrixXd::Zero(size,1);
        for(int i=0;i< size; i++){
            v(i,0) = es.eigenvectors()(i, second_ind).real();
        }

        MatrixXd v_eig = D_sq_inv_val * v;

        double mean = 0;
        for(int i=0;i< size; i++){
            mean += v_eig(i,0);
        }

        mean = mean / (double) size;

        cout << "Second Smallest Eigen Vector " <<endl << v_eig<< endl;
        complex<double> lambda1 = es.eigenvalues()[second_ind];
        MatrixXd top =  v_eig.transpose() * D_W * v_eig;

        //not sure why bottom is always 1
        MatrixXd bottom = v_eig.transpose() * D * v_eig;
        cout << "Top : " << top << endl;

        cout << "Bottom : " << bottom << endl;

        double N_cut = top(0,0) / bottom(0,0);
        cout << "N Cut " << N_cut << endl; 

        //calculate the mean of the eigen vector 
        for(int i=0;i< size; i++){
            if(v(i,0) <= mean){
                cout << "Node : " << i << " : -1" << endl;
            }
            else{
                cout << "Node : " << i << " : +1" << endl;
            }
        }
    }
    
    //seems like the segmentation is consistent 

    //we should try a recursive one 
    
    

    //we need to look at the second smallest eigen value (and it's vector)

    erlcm_graph_similarity_t g_msg; 
    g_msg.no_nodes = size;
    g_msg.node_ids = (int32_t *) calloc(size, sizeof(int32_t));
    g_msg.similarity = score_matrix; 

    erlcm_graph_similarity_t_publish(s->lcm, "SLAM_GRAPH_SIMILARITY", &g_msg);
    free(g_msg.node_ids);

    for(int i=0; i<size;i++){
        delete kdTree[i];
        delete plist[ind].dataPts;
    }
    free(kdTree);
    free(plist);
    delete nnIdx;
    delete dists;

    for(int i=0; i < size; i++){
        free(score_matrix[i]);
    }
    free(score_matrix);
    //save the scores and do a segmentation 
    
    int64_t end_utime = bot_timestamp_now();

    fprintf(stderr,"\n++++ Time elapse : %f\n", (end_utime - start_utime)/1.0e3);

    free(node_list);
}


void calculatePointOverlap(state_t *s){
    if(s->tf == NULL || s->node_list == NULL || s->tf->size <=1){
        fprintf(stderr,"Not enough nodes\n");
        return;
    }

    erlcm_slam_graph_node_t **node_list = (erlcm_slam_graph_node_t **)calloc(g_list_length(s->node_list), sizeof(erlcm_slam_graph_node_t *));

    int size = g_list_length(s->node_list);
    
    //    polygon *poly_list = (polygon *) calloc(g_list_length(s->node_list), sizeof(polygon));

    GList *list = s->node_list;
    GList *elem;
    int ind = 0;


    int64_t start_utime = bot_timestamp_now();
    //ANNpointArray *dataPts = (ANNpointArray *) calloc(size, sizeof(ANNpointArray));
    ann_t *plist = (ann_t *)calloc(size, sizeof(ann_t));
        
    //while (g_list_next(list)) {
    for(elem = list; elem; elem = elem->next) {
        erlcm_slam_graph_node_t *curr_node = (erlcm_slam_graph_node_t *)(elem->data);
        node_list[ind] = curr_node;
        fprintf(stderr,"ID : %d\n", curr_node->id);

        double pBody[3] = { 0, 0, 0 };
        double pLocal[3];
        BotTrans bodyToLocal;
        bodyToLocal.trans_vec[0] = s->tf->trans[curr_node->id].pos[0];
        bodyToLocal.trans_vec[1] = s->tf->trans[curr_node->id].pos[1];
        bodyToLocal.trans_vec[2] = 0;
        double rpy[3] = { curr_node->rp[0], curr_node->rp[1], s->tf->trans[curr_node->id].t };
        bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);

        plist[ind].dataPts = annAllocPts(curr_node->pl.no, 2);

        //ANNpointArray dp = plist[ind].dataPts;
        plist[ind].no_points = curr_node->pl.no;
        
        for(int i=0; i< curr_node->pl.no; i++){
            //fprintf(stderr,"Adding : %d\n", i);
            pBody[0] = curr_node->pl.points[i].pos[0];
            pBody[1] = curr_node->pl.points[i].pos[1];
            bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
            plist[ind].dataPts[i][0] = pLocal[0];//curr_node->pl.points[i].pos[0];
            plist[ind].dataPts[i][1] = pLocal[1];//curr_node->pl.points[i].pos[1];
        }
        ind++;  
    }
    
    
    double threshold = 0.2;

    for(int i = 0; i< size; i++){
        fprintf(stderr,"Searching in : %d\n", i);
        for(int j=0; j < size; j++){
            int count = 0;
            if(i < j)
                continue;
            fprintf(stderr,"\tSearching : %d => %d\n", j, plist[j].no_points);
            
            for(int qp=0; qp < plist[j].no_points; qp++){ 
                for(int cp = 0; cp < plist[i].no_points; cp++){ 
                    if(hypot(plist[j].dataPts[qp][0] - plist[i].dataPts[cp][0],
                             plist[j].dataPts[qp][1] - plist[i].dataPts[cp][1]) < threshold){
                        count++;
                        break;
                    }
                }
            }           
            fprintf(stderr,"Count :%d,%d => %d\n", i, j, count);
        }

    }

    int64_t end_utime = bot_timestamp_now();

    fprintf(stderr,"++++ Time elapse : %f\n", (end_utime - start_utime)/1.0e3);
}

#ifdef __BOOST__
//we are still getting wierd negative values - so might need to ignore it 
void calculateSimilarity(state_t *s){
    if(s->tf == NULL || s->node_list == NULL || s->tf->size <=1){
        fprintf(stderr,"Not enough nodes\n");
        return;
    }

    erlcm_slam_graph_node_t **node_list = (erlcm_slam_graph_node_t **)calloc(g_list_length(s->node_list), sizeof(erlcm_slam_graph_node_t *));

    int size = g_list_length(s->node_list);
    
    polygon *poly_list = (polygon *) calloc(g_list_length(s->node_list), sizeof(polygon));

    GList *list = s->node_list;
    GList *elem;
    int ind = 0;
    //while (g_list_next(list)) {
    for(elem = list; elem; elem = elem->next) {
        erlcm_slam_graph_node_t *curr_node = (erlcm_slam_graph_node_t *)(elem->data);
        node_list[ind] = curr_node;
        fprintf(stderr,"ID : %d\n", curr_node->id);
        //list = g_list_next (list);   

        polygon *pl = &poly_list[ind];

        double pBody[3] = { 0, 0, 0 };
        double pLocal[3];
        BotTrans bodyToLocal;
        bodyToLocal.trans_vec[0] = s->tf->trans[curr_node->id].pos[0];
        bodyToLocal.trans_vec[1] = s->tf->trans[curr_node->id].pos[1];
        bodyToLocal.trans_vec[2] = 0;
        double rpy[3] = { curr_node->rp[0], curr_node->rp[1], s->tf->trans[curr_node->id].t };
        bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
        
        for(int i=0; i< curr_node->pl.no; i++){
            pBody[0] = curr_node->pl.points[i].pos[0];
            pBody[1] = curr_node->pl.points[i].pos[1];
            bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
            append(*pl, make_tuple(pLocal[0], pLocal[1]));
            
            //append(*pl, make_tuple(curr_node->pl.points[i].pos[0],curr_node->pl.points[i].pos[1]));
        }
        
        //this seems to be missing the last item 
        
        ind++;  
        std::cout << "New Area: " << area(*pl) << std::endl;
    }
    

    for(int i=0; i< size; i++){
        for(int j=0; j< size; j++){
            if(j ==i){
                continue;
            }
            //do the intersection
            polygon *pl_1 = &poly_list[i];
            polygon *pl_2 = &poly_list[j];

            std::cout << "Area 1: " << area(*pl_1) << std::endl;
            std::cout << "Area 2: " << area(*pl_2) << std::endl;
            std::deque<polygon> output_i;
            boost::geometry::intersection(*pl_1, *pl_2, output_i);

            std::deque<polygon> output_u;
            boost::geometry::union_(*pl_1, *pl_2, output_u);
            
            double intersection_area = 0;

            BOOST_FOREACH(polygon const& p, output_i){
                intersection_area += boost::geometry::area(p);
                //std::cout << res_ind++ << ": " << boost::geometry::area(p) << std::endl;
            }

            double union_area = 0;

            BOOST_FOREACH(polygon const& p, output_u){
                union_area += boost::geometry::area(p);
            }

            fprintf(stderr,"%d - %d\t U : %.2f I : %.2f Ratio : %.2f\n", i, j, union_area, 
                    intersection_area, intersection_area / union_area);
        }
    }
    
    
    /*
    int ind = trajectory.size() -1;
    SlamPose * slampose_curr = trajectory.at(ind);
    int last_new_ind = ind;
    while(slampose_curr->region_id == -1){     
        last_new_ind = ind;
        ind--;
        
        if(ind < 0){
            break;
        }
        
        fprintf(stderr, "Index : %d \n", ind);
        slampose_curr = trajectory.at(ind);
    }

    if(last_new_ind == trajectory.size() -1){

        //create and add a cost vector
        //std::vector<double> c_cost;
        std::vector<double> *c_cost = new std::vector<double>();     
        c_cost->push_back(1);
        cost.push_back(c_cost);
        return;
    }

    //ideally we wont need to do the second loop - as this was calculated earlier 
    //for(int m = trajectory.size() -1; m >= last_new_ind; m--){
    int m = trajectory.size() -1;
    Scan *c_scan = trajectory.at(m)->scan;
    polygon c_poly, l_poly;
    
    Pose2d value = trajectory.at(m)->pose2d_node->value();

    double pBody[3] = { 0, 0, 0 };
    double pLocal[3];
    BotTrans bodyToLocal;
    bodyToLocal.trans_vec[0] = value.x();
    bodyToLocal.trans_vec[1] = value.y();
    bodyToLocal.trans_vec[2] = 0;
    double rpy[3] = { trajectory.at(m)->rp[0], trajectory.at(m)->rp[1], value.t() };
    bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
    //points need to be clockwise - the laser scans are counter-clockwise
 
    if(0){
        bot_lcmgl_line_width(lcmgl_point_area, 1);
        bot_lcmgl_point_size(lcmgl_point_area, 3);
        //}
        bot_lcmgl_begin(lcmgl_point_area, GL_POINTS);
        bot_lcmgl_begin(lcmgl_point_area, GL_LINES);
        bot_lcmgl_color3f(lcmgl_point_area, 1,0,0);
    }
    for(int i=c_scan->numPoints -1; i>=0; i--){
        pBody[0] = c_scan->points[i].x;//scan->points[i].x;
        pBody[1] = c_scan->points[i].y;//scan->points[i].y;
        bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
        append(c_poly, make_tuple(pLocal[0], pLocal[1]));
        if(0){
            bot_lcmgl_vertex3f(lcmgl_point_area, pLocal[0], pLocal[1], pLocal[2]);
        }
        //bot_lcmgl_vertex3f(lcmgl_point_area, c_scan->points[i].x, c_scan->points[i].y, 0);
        append(l_poly,make_tuple(c_scan->points[i].x, c_scan->points[i].y));
    }
    
    double c_area = area(l_poly);
    std::cout << "New Area: " << area(c_poly) << std::endl;
    std::cout << "New Area: " << area(l_poly) << std::endl;

    boost::geometry::clear(l_poly);  

    std::vector<double> *c_cost = new std::vector<double>();     
    //std::vector<double> c_cost;// = new std::vector<double>();     
    c_cost->push_back(1);
    //dont match with self
    for(int n = trajectory.size() -2; n >= last_new_ind; n--){
        //int n = m -1;
    Scan *p_scan = trajectory.at(n)->scan;
    //do a check for the intersection of area 
    polygon p_poly;

    Pose2d value1 = trajectory.at(n)->pose2d_node->value();

    double pBody1[3] = { 0, 0, 0 };
    double pLocal1[3];
    BotTrans bodyToLocal1;
    bodyToLocal1.trans_vec[0] = value1.x();
    bodyToLocal1.trans_vec[1] = value1.y();
    bodyToLocal1.trans_vec[2] = 0;
    double rpy1[3] = { trajectory.at(n)->rp[0], trajectory.at(n)->rp[1], value1.t() };
    bot_roll_pitch_yaw_to_quat(rpy1, bodyToLocal1.rot_quat);
       
    if(0){
        bot_lcmgl_color3f(lcmgl_point_area, 0,1,0);
    }
    for(int i=p_scan->numPoints -1; i>=0; i--){
        pBody1[0] = p_scan->points[i].x;//scan->points[i].x;
        pBody1[1] = p_scan->points[i].y;//scan->points[i].y;
        bot_trans_apply_vec(&bodyToLocal1, pBody1, pLocal1);
        if(0){
            bot_lcmgl_vertex3f(lcmgl_point_area, pLocal1[0], pLocal1[1], pLocal1[2]);
        }
        append(p_poly, make_tuple(pLocal1[0], pLocal1[1]));//p_scan->points[i].x, p_scan->points[i].y));
    }
        
    if(0){
        bot_lcmgl_end(lcmgl_point_area);
        //bot_lcmgl_pop_attrib(lcmgl_point_area);
        bot_lcmgl_switch_buffer(lcmgl_point_area);
    }

    double p_area = area(p_poly);
    std::cout << "Prev Area: " << area(p_poly) << std::endl;
        
    //do the intersection
    std::deque<polygon> output;
    boost::geometry::intersection(c_poly, p_poly, output);
            
    int res_ind =0;
    //std::cout << "Intersection:" << std::endl;
    double intersection_area = 0;


    BOOST_FOREACH(polygon const& p, output){
        intersection_area += boost::geometry::area(p);
        //std::cout << res_ind++ << ": " << boost::geometry::area(p) << std::endl;
    }

    //do the union
    std::deque<polygon> output_u;
    boost::geometry::union_(c_poly, p_poly, output_u);
            
    res_ind =0;
    //std::cout << "Union:" << std::endl;
    double union_area = 0;


    BOOST_FOREACH(polygon const& p, output_u){
        union_area += boost::geometry::area(p);
        //std::cout << res_ind++ << ": " << boost::geometry::area(p) << std::endl;
    }
    double score = (intersection_area / union_area);

    //double score = (intersection_area / c_area);
    fprintf(stderr," ++++ Interesection  : %f, U_area : %f Ratio : %f\n" , 
            intersection_area , union_area, (intersection_area / union_area));

    if(union_area < intersection_area){
        fprintf(stderr, "---Error - Unoin is less than intersecion\n");
    }
    //a_score =  (intersection_area / p_area);
    boost::geometry::clear(p_poly);
        
    //c_cost->insert(0,&score);
    //c_cost->push_back(score);
    //c_cost.push_back(score);
    vector<double>::iterator it;
    it = c_cost->begin();
    c_cost->insert(it,score);
    fprintf(stderr,"Called at : %d\n", n - last_new_ind);
    //this part doesnt seem to be working 
    std::vector<double> *c_vect = cost.at(n - last_new_ind);
    c_vect->push_back(score);
        

    cost.push_back(c_cost);
    
    
    }
    boost::geometry::clear(c_poly);  

    //go through the vectors and print 
    
    //bot_lcmgl_text(lcmgl_points,t_pos,score_info);*/
}
#endif

static void on_slam_transform(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
			     const erlcm_slam_pose_transform_list_t* msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 
    if(s->tf !=NULL){
	erlcm_slam_pose_transform_list_t_destroy(s->tf);
    }
    s->tf = erlcm_slam_pose_transform_list_t_copy(msg);
    if(s->verbose){
        fprintf(stderr,"T");
    }
    //calculateSimilarity(s);
    //calculatePointOverlap(s);
    calculatePointOverlapkdTree(s);
}


static void on_pose(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
			     const bot_core_pose_t * msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 
    if(s->last_pose !=NULL){
	bot_core_pose_t_destroy(s->last_pose);
    }
    s->last_pose = bot_core_pose_t_copy(msg);
    if(s->verbose){
	fprintf(stderr,"Pose : (%f,%f)\n",msg->pos[0], msg->pos[1]);//, rpy[2], vel[0] , vel[1]);
    }

    bot_core_pose_t *pose = bot_core_pose_t_copy(msg);

    fprintf(stderr, "Size of List (at start): %d\n", g_list_length(s->pose_list));

    if(g_list_length(s->pose_list) < POSE_LIST_SIZE){
	fprintf(stderr,"Adding\n");
	s->pose_list = g_list_prepend (s->pose_list, (pose));
    }
    else{
	fprintf(stderr, "Removing and Inserting\n");
	GList* last = g_list_last (s->pose_list);
	bot_core_pose_t *last_pose = (bot_core_pose_t *) last->data;
	fprintf(stderr,"Pose : %.3f (%f,%f)\n",last_pose->utime / 1000000.0 , last_pose->pos[0], last_pose->pos[1]);
	
	//	s->pose_list = g_list_remove_link (s->pose_list, last);	
	bot_core_pose_t_destroy((bot_core_pose_t *) last->data);
	s->pose_list = g_list_delete_link (s->pose_list, last);	

	
	fprintf(stderr, "Size of List (After remove): %d\n", g_list_length(s->pose_list));
	s->pose_list = g_list_prepend (s->pose_list , (gpointer) (pose));
    }
    
    fprintf(stderr, "Size of List (at end): %d\n", g_list_length(s->pose_list));
}

//pthread
static void *track_work_thread(void *user)
{
    state_t *s = (state_t*) user;

    printf("obstacles: track_work_thread()\n");

    while(1){
	fprintf(stderr, " T - called ()\n");
	
	usleep(50000);
    }
}

void read_parameters_from_conf(state_t *s)
{
    BotParam *b_param = s->b_server; 
    double max_t_vel =  bot_param_get_double_or_fail(b_param, "robot.max_t_vel");
    double max_r_vel = bot_param_get_double_or_fail(b_param, "robot.max_r_vel");
  
    char **planar_lidar_names = bot_param_get_all_planar_lidar_names(b_param);
  
    if(planar_lidar_names) {
	for (int pind = 0; planar_lidar_names[pind] != NULL; pind++) {
	    fprintf(stderr, "Channel : %s\n", planar_lidar_names[pind]);
	}
    }
  
    g_strfreev(planar_lidar_names);
}

//doesnt do anything right now - timeout function
gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;
  
    //do the periodic stuff 
    if(s->verbose){
	fprintf(stderr, "Callback - Timeout\n");
    }
    //return true to keep running
    return TRUE;
}

void subscribe_to_channels(state_t *s)
{
    //bot_core_pose_t_subscribe(s->lcm, "POSE", on_pose ,s);

    erlcm_slam_graph_node_t_subscribe(s->lcm, "SLAM_NODE", on_slam_node, s);
    erlcm_slam_pose_transform_list_t_subscribe(s->lcm, "SLAM_NODE_TRANSFORMS", on_slam_transform, s);
}  


static void usage(char * funcName)
{
    printf("Usage: %s [options]\n"
	   "options are:\n"
	   "--help,        -h    display this message \n"
	   "--publish_all, -a    publish robot laser messages for all channels \n"
	   "--verbose,     -v    be verbose", funcName);
    exit(1);

}

int 
main(int argc, char **argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));
    state->last_pose = NULL;
    state->pose_list = NULL;
    state->node_list = NULL;

    state->tf = NULL;
    state->n_cut_threshold = 0.5;//0.65;
    state->recursive = 0;
    state->pallet = NULL;

    const char *optstring = "havn:r";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
                                  {"n_cut", required_argument, 0, 'n' },
				  { "publish_all", no_argument, 0, 'a' }, 
				  { "verbose", no_argument, 0, 'v' }, 
                                  { "recursive", no_argument, 0, 'r' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    usage(argv[0]);
	    break;

        case 'r':
            state->recursive = 1;
	    break;
        case 'n':
	    state->n_cut_threshold = strtod(optarg, 0);
            fprintf(stderr,"N Cut threshold : %f\n" , state->n_cut_threshold);
	    break;
	case 'a':
	    {
		fprintf(stderr,"Publishing all laser channels\n");
		break;
	    }
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
		state->verbose = 1;
		break;
	    }
	default:
	    {
		usage(argv[0]);
		break;
	    }
	}
    }

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
    
    state->lcmgl = bot_lcmgl_init(state->lcm,"Segmentation");

    state->b_server = bot_param_new_from_server(state->lcm, 1);

    state->frames = bot_frames_get_global (state->lcm, state->b_server);

    //pthread_create(&state->work_thread, NULL, track_work_thread, state);

    subscribe_to_channels(state);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    read_parameters_from_conf(state);

    /* heart beat*/
    //g_timeout_add_seconds (1, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


