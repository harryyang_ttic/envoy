add_definitions(
#    -ggdb3 
    #-std=gnu99
    )

set(er_topo_src
  #template.c
  main.cpp
  )

add_executable(er-topo-graph ${er_topo_src})

pods_use_pkg_config_packages(er-topo-graph lcm bot2-core bot2-param-client lcmtypes_er-lcmtypes bot2-frames eigen3 bot2-lcmgl-client bot2-frames)

target_link_libraries(er-topo-graph
  pthread
  ann
)

pods_install_executables(er-topo-graph)

add_executable(test-eigen test_eigen.cpp)

pods_use_pkg_config_packages(test-eigen eigen3)

pods_install_executables(test-eigen)