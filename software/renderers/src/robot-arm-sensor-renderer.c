//robot-arm-sensor-renderer.c

#include "robot-arm-sensor-renderer.h"
//#include "robot-arm-sensor-torque-grapher.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <bot_vis/bot_vis.h>
#include <GL/gl.h>
#include <math.h>
#include <bot_vis/gl_util.h>
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <dynamixel/dynamixel.h>
//#include <arm_ik/ik.h>

#include <lcmtypes/arm_cmd_joint_list_t.h>
//#include <lcmtypes/arm_cmd_ik_t.h>

#define GRIPPER_POSITION "Gripper-Position"
#define GRIPPER_ENABLE "Gripper-Enable"

#define X_POS "X Position"
#define Y_POS "Y Position"
#define Z_POS "Z Position"
#define ROLL "Roll"
#define PITCH "Pitch"
#define YAW "Yaw"

#define SEND_XYZ_COMMAND "Send XYZ Command"
#define SEND_COMMAND "Send to Arm"

#define NUM_JOINTS 5

#define JOINT_PREFIX "arm_config.joints"

typedef struct _renderer_joint_t{
    char *name;
    //joint dim are prob useless
    double dim[3];
    char *frame_name;
    char *relative_to;
} renderer_joint_t;

typedef struct _renderer_joint_list_t{
    int no_joints;
    renderer_joint_t *joints;
} renderer_joint_list_t;

struct _sensor_renderer_t {
    BotViewer            *viewer;
    BotFrames            *frames;
    BotGtkParamWidget     *widget;
    BotRenderer            *renderer;
    
    renderer_joint_list_t jlist;

    double g_angles[6];
    lcm_t *lcm;
    //IK *ik;

    int xyz_flag;
    
};

void draw_box(double ver[24]){
    glBegin(GL_QUADS);
  
    glVertex3d(ver[0], ver[1], ver[2]);
    glVertex3d(ver[3], ver[4], ver[5]);
    glVertex3d(ver[6], ver[7], ver[8]);
    glVertex3d(ver[9], ver[10], ver[11]);

    glVertex3d(ver[12], ver[13], ver[14]);
    glVertex3d(ver[15], ver[16], ver[17]);
    glVertex3d(ver[18], ver[19], ver[20]);
    glVertex3d(ver[21], ver[22], ver[23]);

    glVertex3d(ver[0], ver[1], ver[2]);
    glVertex3d(ver[3], ver[4], ver[5]);
    glVertex3d(ver[15], ver[16], ver[17]);
    glVertex3d(ver[12], ver[13], ver[14]);

    glVertex3d(ver[3], ver[4], ver[5]);
    glVertex3d(ver[6], ver[7], ver[8]);
    glVertex3d(ver[18], ver[19], ver[20]);
    glVertex3d(ver[15], ver[16], ver[17]);

    glVertex3d(ver[6], ver[7], ver[8]);
    glVertex3d(ver[9], ver[10], ver[11]);
    glVertex3d(ver[21], ver[22], ver[23]);
    glVertex3d(ver[18], ver[19], ver[20]);

    glVertex3d(ver[9], ver[10], ver[11]);
    glVertex3d(ver[0], ver[1], ver[2]);
    glVertex3d(ver[21], ver[22], ver[23]);
    glVertex3d(ver[12], ver[13], ver[14]);

    glEnd();
}

void draw_joint(double v1[3], double v2[3]){
    double dia = .03;


    double diff[3] = {v1[0]-v2[0],v1[1]-v2[1],v1[2]-v2[2]};
    double perp[3] = {0.0,0.0,0.0};
    //find perp vector
    //diff[0]*perp[0] + diff[1]*perp[1] + diff[2]*perp[2] = 0
    for (int j=0; j<3; j++){
        if(diff[j]==0.0){
            perp[j] = 1.0;
            break;
        }
    }
    if(perp[0]==0.0 && perp[1]==0.0 && perp[2]==0.0){
        perp[0] = 1.0;
        perp[1] = -diff[0]/diff[1];
    }
    double perp1[3] = { diff[1]*perp[2] - perp[1]*diff[2],
                        diff[2]*perp[0] - perp[2]*diff[0],
                        diff[0]*perp[1] - perp[0]*diff[1] };

    //normalize perp vectors
    double l_perp = sqrt(perp[0]*perp[0] + perp[1]*perp[1] + perp[2]*perp[2]);
    perp[0] = perp[0] / l_perp;
    perp[1] = perp[1] / l_perp;
    perp[2] = perp[2] / l_perp;
  
    l_perp = sqrt(perp1[0]*perp1[0] + perp1[1]*perp1[1] + perp1[2]*perp1[2]);
    perp1[0] = perp1[0] / l_perp;
    perp1[1] = perp1[1] / l_perp;
    perp1[2] = perp1[2] / l_perp;
  
    
    double loc[24];
    //store to loc 0
    loc[0] = v1[0] + dia*perp[0]/2;
    loc[1] = v1[1] + dia*perp[1]/2;
    loc[2] = v1[2] + dia*perp[2]/2;
  
    loc[3] = v1[0] + dia*perp1[0]/2;
    loc[4] = v1[1] + dia*perp1[1]/2;
    loc[5] = v1[2] + dia*perp1[2]/2;
  
    loc[6] = v1[0] - dia*perp[0]/2;
    loc[7] = v1[1] - dia*perp[1]/2;
    loc[8] = v1[2] - dia*perp[2]/2;
  
    loc[9] = v1[0] - dia*perp1[0]/2;
    loc[10] = v1[1] - dia*perp1[1]/2;
    loc[11] = v1[2] - dia*perp1[2]/2;
  
    loc[12] = v2[0] + dia*perp[0]/2;
    loc[13] = v2[1] + dia*perp[1]/2;
    loc[14] = v2[2] + dia*perp[2]/2;
  
    loc[15] = v2[0] + dia*perp1[0]/2;
    loc[16] = v2[1] + dia*perp1[1]/2;
    loc[17] = v2[2] + dia*perp1[2]/2;
  
    loc[18] = v2[0] - dia*perp[0]/2;
    loc[19] = v2[1] - dia*perp[1]/2;
    loc[20] = v2[2] - dia*perp[2]/2;
  
    loc[21] = v2[0] - dia*perp1[0]/2;
    loc[22] = v2[1] - dia*perp1[1]/2;
    loc[23] = v2[2] - dia*perp1[2]/2;
  
    draw_box(&loc[0]);
}

void draw_joints(double ja[6]){

    //printf("%f, %f, %f, %f, %f, %f\n",ja[0], ja[1], ja[2], ja[3], ja[4], ja[5]);
  
    // TODO: Look up lengths and starting translation in config file
    double l[6] = {.047625, .38735, .381, .06985, .038735, .111125};
  
    glColor4d(.5, .5, .5, 1);
    double base_start[3] = {0.35, -0.2, 0.4};
  
    //base
    double base_end[3] = {base_start[0] + l[0]*cos(ja[0]), base_start[1] + l[0]*sin(ja[0]), base_start[2]};
    draw_joint(base_start, base_end);
    //shoulder
    double shoulder_end[3] = {
        base_end[0] + l[1] * cos(ja[1]) * cos(ja[0]),
        base_end[1] + l[1] * cos(ja[1]) * sin(ja[0]),
        base_end[2] + l[1] * sin(ja[1])
    };
    draw_joint(base_end, shoulder_end);
    //elbow
    double ea = ja[1] + ja[2];
    double elbow_end[3] = {
        shoulder_end[0] + l[2] * cos(ea) * cos(ja[0]),
        shoulder_end[1] + l[2] * cos(ea) * sin(ja[0]),
        shoulder_end[2] + l[2] * sin(ea)
    };
    draw_joint(shoulder_end, elbow_end);
    //lateral_wrist
    double la = ja[3] + ea;
    double lw_end[3] = {
        elbow_end[0] + l[3] * cos(la) * cos(ja[0]),
        elbow_end[1] + l[3] * cos(la) * sin(ja[0]),
        elbow_end[2] + l[3] * sin(la)
    };
    draw_joint(elbow_end, lw_end);
    //twist_wrist
    double tw_end[3] = {
        lw_end[0] + l[4] * cos(la) * cos(ja[0]),
        lw_end[1] + l[4] * cos(la) * sin(ja[0]),
        lw_end[2] + l[4] * sin(la)
    };
    draw_joint(lw_end, tw_end);
    //gripper_static
    double gs_end[3] = {
        tw_end[0] + l[5] * cos(la) * cos(ja[0]),
        tw_end[1] + l[5] * cos(la) * sin(ja[0]),
        tw_end[2] + l[5] * sin(la)
    };
    draw_joint(tw_end, gs_end);
    //gripper_dynamic
    double gd_end[3] = {
        tw_end[0] + l[6] * sin(ja[4]) * cos(la+ja[5]) * sin(ja[0]), 
        tw_end[1] - l[6] * sin(ja[4]) * cos(la+ja[5]) * cos(ja[0]), 
        tw_end[2] + l[6] * cos(ja[4]) * sin(la+ja[5])
    };
    draw_joint(tw_end, gd_end);
}

static void send_arm_command(BotGtkParamWidget *pw, const char *name, 
                             sensor_renderer_t *self)
{
    if(!strcmp(name, SEND_COMMAND)){
        if(!(self->xyz_flag)){
            fprintf(stderr, "Please send XYZ command first\n");
            return;
        }

        arm_cmd_joint_t *list = (arm_cmd_joint_t *) calloc(self->jlist.no_joints, sizeof(arm_cmd_joint_t));
        
        arm_cmd_joint_list_t msg = {
            .ncommands = self->jlist.no_joints,
            .commands = list, 
        };

        for(int i=0; i<self->jlist.no_joints; i++){
            double angle = self->g_angles[i]*180/3.14159265;
            msg.commands[i].joint_index = i; 
            msg.commands[i].torque_enable = 1; 
            msg.commands[i].goal_angle = angle; 
            msg.commands[i].max_speed = 50;
            msg.commands[i].max_torque = 1023;
            printf("%d: %f\n", i, angle);
        }
        arm_cmd_joint_list_t_publish(self->lcm, "ARM_JOINT_COMMANDS", &msg);
        free(list);
    }
}

static void 
on_param_widget_changed_xyz(BotGtkParamWidget *pw, const char *name, 
                            sensor_renderer_t *self)
  
{ 
    double xyz[3] = {bot_gtk_param_widget_get_double(pw, X_POS),
                     bot_gtk_param_widget_get_double(pw, Y_POS), 
                     bot_gtk_param_widget_get_double(pw, Z_POS)
    };
    double rpy[3] = {bot_gtk_param_widget_get_double(pw, ROLL), 
                     bot_gtk_param_widget_get_double(pw, PITCH), 
                     bot_gtk_param_widget_get_double(pw, YAW)
    };

    double gripper_pos = bot_gtk_param_widget_get_double(pw, GRIPPER_POSITION);

    bool gripper_en =    bot_gtk_param_widget_get_bool(pw, GRIPPER_ENABLE);

    /*arm_cmd_ik_t msg = {
      .gripper_enable = gripper_en,
      .gripper_pos = gripper_pos,
      .xyz[0] = xyz[0],
      .xyz[1] = xyz[1],
      .xyz[2] = xyz[2],
      .ef_rpy[0] = rpy[0],
      .ef_rpy[1] = rpy[1],
      .ef_rpy[2] = rpy[2]
      };*/

    /*qs_list_t *qlist = (qs_list_t *) calloc(1, sizeof(qs_list_t));
      if(!(ik_q_gen(self->ik, xyz, rpy, gripper_pos, gripper_en, qlist))){
      fprintf(stderr, "Failed to get q's.\n");
      return;
      }


      for(int i=0; i<qlist->qs[0].num_qs; i++){
      int index = qlist->qs[0].joint_index[i];
      self->g_angles[index] = qlist->qs[0].q_vals[i];
      printf("%d: %f\n", index, self->g_angles[index]);
      }*/
    fprintf(stderr, "Not implemented\n");

    //self->lcm = bot_lcm_get_global(NULL);
    //arm_cmd_ik_t_publish(self->lcm, "ARM_IK_COMMANDS", &msg);
    //fprintf(stderr, "Message Published.\n");
    //    bot_viewer_request_redraw (self->viewer);
      
    /*self->xyz_flag = 1;
      free(qlist);*/
}

static void robot_arm_renderer_free(BotRenderer *renderer){
    free(renderer);
}

static void robot_arm_renderer_draw(BotViewer *viewer, BotRenderer *renderer){
  
    sensor_renderer_t *sr = (sensor_renderer_t*)renderer->user;

    draw_joints(&(sr->g_angles[0]));
    // ik stuff

    int num_frames = bot_frames_get_num_frames(sr->frames);
    char ** frame_names = bot_frames_get_frame_names(sr->frames);
    const char *root_name = bot_frames_get_root_name(sr->frames);
    BotTrans trans;
    BotTrans trans1;
    
    double joint_to_local[12];
    double trans_m[16], trans_m_opengl[16], trans1_m[16], trans1_m_opengl[16];


    glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT);

    char *str1 = "effector_static\0";
    char *str2 = "effector_dynamic\0";

    glEnable(GL_DEPTH_TEST);
    for (int i=1; i < sr->jlist.no_joints; i++){
        char *str = sr->jlist.joints[i].frame_name;

        if (!bot_frames_get_trans(sr->frames, sr->jlist.joints[i].frame_name,"local", &trans)) {
            fprintf(stderr,"Frame Error\n");
            return;
        }
        if (!bot_frames_get_trans(sr->frames, sr->jlist.joints[i].relative_to, "local", &trans1)) {
            fprintf(stderr,"Frame Error\n");
            return;
        }
        float i_scale = ((float)i-1) / ((float)(sr->jlist.no_joints)-2);
        float r = 1-i_scale;
        float g = 1-abs(i_scale*2-1);//(i_scale*2-1)*(i_scale*2-1);
        float b = i_scale;
        glColor4f(r,g,b,1);

        draw_joint(&(trans.trans_vec[0]), &(trans1.trans_vec[0]));
    
        glPushMatrix(); //discs representing servos
        glColor4f(.5,.5,.5,1);
      
        int n_disks = 100;
    
        if(!(strncmp(str, str1, 20)==0||strncmp(str,str2,20)==0)){
    
            bot_trans_get_mat_4x4(&trans, trans_m);
            bot_matrix_transpose_4x4d(trans_m, trans_m_opengl);
            glMultMatrixd(trans_m_opengl);
            BotTrans trans2;
            trans2.trans_vec[0] = 0;
            trans2.trans_vec[1] = 0;
            trans2.trans_vec[2] = 0;
            trans2.rot_quat[0] = 0;
            trans2.rot_quat[1] = 1;
            trans2.rot_quat[2] = 0;
            trans2.rot_quat[3] = 0;
    
            bot_trans_get_mat_4x4(&trans2, trans1_m);
            bot_matrix_transpose_4x4d(trans1_m, trans1_m_opengl);
            glMultMatrixd(trans1_m_opengl);

            for(int j=0; j<n_disks; j++){

                trans2.trans_vec[2] = -(float)j*(.04/(float)n_disks);

                bot_trans_get_mat_4x4(&trans2, trans1_m);
                bot_matrix_transpose_4x4d(trans1_m, trans1_m_opengl);
    
                bot_gl_draw_disk(0.025);

                glMultMatrixd(trans1_m_opengl);
            }
        }

    
 
        glPopMatrix();
    
    }

    glPopAttrib();

}

//obsolete?
static void cb_param_change_handler (
                                     BotGtkParamWidget *pw,
                                     const char *name,
                                     void *user){

    sensor_renderer_t *sr = (sensor_renderer_t*) user;
    BotRenderer *renderer = sr->renderer;

    if (strcmp(ENABLE_NAME, name) == 0){
        //checkbox
        int value = bot_gtk_param_widget_get_bool(pw, name);
        renderer->enabled = value;
        bot_viewer_request_redraw(sr->viewer);    
    }
    else if (strcmp(SHIFT_X_NAME, name) == 0){
        //shift over the arm by x

        int num_frames = bot_frames_get_num_frames(sr->frames);
        char ** frame_names = bot_frames_get_frame_names(sr->frames);
        const char *root_name = bot_frames_get_root_name(sr->frames);
        BotTrans trans;
        if (num_frames >= 1){
            //printf ("%s -> %s\n", root_name, frame_names[1]);
            if (bot_frames_get_trans(
                                     sr->frames,
                                     frame_names[1],
                                     root_name,
                                     &trans) != 0){
                //got the transform
                trans.trans_vec[0] = bot_gtk_param_widget_get_double(pw, name);
                bot_frames_update_frame(
                                        sr->frames,
                                        frame_names[1],
                                        root_name,
                                        &trans,
                                        bot_timestamp_now());
            }
        }
        free (frame_names);

    }
    else if (strcmp(SHIFT_Y_NAME, name) == 0){
        //shift over the arm by y

        int num_frames = bot_frames_get_num_frames(sr->frames);
        char ** frame_names = bot_frames_get_frame_names(sr->frames);
        const char *root_name = bot_frames_get_root_name(sr->frames);
        BotTrans trans;
        if (num_frames >= 1){
            //printf ("%s -> %s\n", root_name, frame_names[1]);
            if (bot_frames_get_trans(
                                     sr->frames,
                                     frame_names[1],
                                     root_name,
                                     &trans) != 0){
                //got the transform
                trans.trans_vec[1] = -bot_gtk_param_widget_get_double(pw, name);
                bot_frames_update_frame(
                                        sr->frames,
                                        frame_names[1],
                                        root_name,
                                        &trans,
                                        bot_timestamp_now());
            }
        }
        free (frame_names);
    }

}
//obsolete?
static void frames_update (
                           BotFrames *bot_frames,
                           const char *frame,
                           const char *relative_to,
                           int64_t utime,
                           void * user){

    sensor_renderer_t *sr = (sensor_renderer_t*) user;
    bot_viewer_request_redraw(sr->viewer);    
}
sensor_renderer_t * add_robot_arm_sensor_renderer_to_viewer (    
                                                             BotViewer *viewer,
                                                             BotParam *param,
                                                             int priority){
    BotRenderer *renderer = 
        (BotRenderer*)            calloc(1, sizeof(BotRenderer));
    sensor_renderer_t * sensor_renderer = 
        (sensor_renderer_t*)    calloc(1, sizeof(sensor_renderer_t));


    //printf ("in %s\n", __func__);

    //setup the values to call local functions
    renderer->draw         = robot_arm_renderer_draw;
    renderer->destroy     = robot_arm_renderer_free;
    renderer->name         = strdup("Arm Sensor Renderer");
    renderer->widget     = bot_gtk_param_widget_new();
    renderer->enabled     = 1;
    renderer->user         = sensor_renderer;
    
    sensor_renderer->viewer    = viewer;
    sensor_renderer->renderer = renderer;
    //sensor_renderer->tg        =    tg_new(param);

    sensor_renderer->xyz_flag = 0;

    lcm_t * lcm = bot_lcm_get_global(NULL);
    BotFrames *frames = bot_frames_get_global(lcm, param); 
    //IK *ik = ik_get_global(lcm);

    sensor_renderer->frames = frames;
    //sensor_renderer->ik = ik;
    sensor_renderer->lcm = lcm;

    sensor_renderer->widget = BOT_GTK_PARAM_WIDGET(renderer->widget);
    BotGtkParamWidget *pw = sensor_renderer->widget;

    bot_gtk_param_widget_add_booleans(
                                      pw,
                                      BOT_GTK_PARAM_WIDGET_CHECKBOX, 
                                      ENABLE_NAME,
                                      1,
                                      NULL);
    bot_gtk_param_widget_add_double(
                                    pw,
                                    SHIFT_X_NAME,
                                    BOT_GTK_PARAM_WIDGET_SLIDER,
                                    -10.0,
                                    10.0,
                                    .5,
                                    0);
    bot_gtk_param_widget_add_double(
                                    pw,
                                    SHIFT_Y_NAME,
                                    BOT_GTK_PARAM_WIDGET_SLIDER,
                                    -10.0,
                                    10.0,
                                    .5,
                                    0);

    g_signal_connect(    G_OBJECT(pw), "changed",
                     G_CALLBACK(cb_param_change_handler), sensor_renderer);


    bot_gtk_param_widget_add_double(pw, X_POS, BOT_GTK_PARAM_WIDGET_SLIDER, -2, 2, 0.01, 0);
    bot_gtk_param_widget_add_double(pw, Y_POS, BOT_GTK_PARAM_WIDGET_SLIDER, -2, 2, 0.01, 0);
    bot_gtk_param_widget_add_double(pw, Z_POS, BOT_GTK_PARAM_WIDGET_SLIDER, -2, 2, 0.01, 0);
    
    bot_gtk_param_widget_add_double(pw, ROLL, BOT_GTK_PARAM_WIDGET_SLIDER, -3, 3, 0.01, 0);
    bot_gtk_param_widget_add_double(pw, PITCH, BOT_GTK_PARAM_WIDGET_SLIDER, -3, 3, 0.01, 0);    
    bot_gtk_param_widget_add_double(pw, YAW, BOT_GTK_PARAM_WIDGET_SLIDER, -3, 3, 0.01, 0);

    bot_gtk_param_widget_add_double(pw, GRIPPER_POSITION, 
                                    BOT_GTK_PARAM_WIDGET_SLIDER, -150,150 , 0.1, 0);

    bot_gtk_param_widget_add_booleans(pw, 0, GRIPPER_ENABLE , 1, NULL);

    //end ik stuffs

    g_signal_connect (G_OBJECT(pw), "changed", G_CALLBACK (on_param_widget_changed_xyz), sensor_renderer);

    // bot_gtk_param_widget_add_buttons(pw, SEND_XYZ_COMMAND, NULL);

    g_signal_connect (G_OBJECT(pw), "changed", G_CALLBACK (send_arm_command), sensor_renderer);

    bot_gtk_param_widget_add_buttons(pw, SEND_COMMAND, NULL);

    bot_frames_add_update_subscriber(frames, frames_update, sensor_renderer);
    bot_viewer_add_renderer(viewer, renderer, priority);
    gtk_widget_show_all(renderer->widget);

    char **joint_names = bot_param_get_subkeys(param, JOINT_PREFIX);

    sensor_renderer->jlist.no_joints = 0;
    if (joint_names) {
        for (int pind = 0; joint_names[pind] != NULL; pind++) {
            //fprintf(stderr, "Joint Name : %s\n", joint_names[pind]);
            sensor_renderer->jlist.no_joints++;            
        }
    }
    
    sensor_renderer->jlist.joints = (renderer_joint_t *) calloc(sensor_renderer->jlist.no_joints, sizeof(renderer_joint_t));

    char key[1024];
    if (joint_names) {
        for (int pind = 0; joint_names[pind] != NULL; pind++) {
            //fprintf(stderr, "Joint Name : %s\n", joint_names[pind]);
            sensor_renderer->jlist.joints[pind].name = strdup(joint_names[pind]);
            sprintf(key, "%s.%s.joint_frame", JOINT_PREFIX, sensor_renderer->jlist.joints[pind].name);
            
            if (0 != bot_param_get_str(param, key, &sensor_renderer->jlist.joints[pind].frame_name)){
                fprintf(stderr, "%s - frame not defined\n", key);
                return NULL;
            }

            sprintf(key, "%s.%s.relative_to", JOINT_PREFIX, sensor_renderer->jlist.joints[pind].name);

            if (0 != bot_param_get_str(param, key, &sensor_renderer->jlist.joints[pind].relative_to)){
                fprintf(stderr, "%s - frame not defined\n", key);
                return NULL;
            }

            //fprintf(stderr, "%s => %s,%s\n", sensor_renderer->jlist.joints[pind].name, sensor_renderer->jlist.joints[pind].frame_name, sensor_renderer->jlist.joints[pind].relative_to);
        }
    }

    if (joint_names) {
        for (int pind = 0; joint_names[pind] != NULL; pind++) {
            free(joint_names[pind]);
        }
        free(joint_names);
    }

       

    return sensor_renderer;
}

