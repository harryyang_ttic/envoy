//robot-arm-sensor-renderer.h

#ifndef __ROBOT_ARM_SENSOR_RENDERER__
#define __ROBOT_ARM_SENSOR_RENDERER__

#include <bot_core/bot_core.h>
#include <bot_vis/bot_vis.h>
#include <bot_param/param_client.h>
#include <bot_frames/bot_frames.h>
#include <gtk/gtk.h>
//#include "../robot-arm-renderer.h"
//#include "robot-arm-sensor-torque-grapher.h"

#define ENABLE_NAME	"Enable"
#define SHIFT_X_NAME "shift x"
#define SHIFT_Y_NAME "shift y"

typedef struct _sensor_renderer_t sensor_renderer_t;

void draw_box(double ver[24]);

void draw_joint(double v1[3], double v2[3]);

sensor_renderer_t * add_robot_arm_sensor_renderer_to_viewer( 	
												BotViewer *viewer,
											BotParam *param,
												int prioirity);
//torque_grapher_t * sr_get_torque_grapher(sensor_renderer_t * sr);

#endif //__ROBOT_ARM_SENSOR_RENDERER__
