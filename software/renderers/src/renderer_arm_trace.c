/*
 * Renders a set of scrolling plots to command arm in circle trajectory
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <gdk/gdkkeysyms.h>
#include <geom_utils/geometry.h>

#include <bot_core/bot_core.h>
#include <bot_vis/gtk_util.h>
#include <bot_vis/viewer.h>
#include <bot_vis/gl_util.h>
#include <bot_vis/scrollplot2d.h>
#include <bot_vis/bot_vis.h>
#include <bot_frames/bot_frames.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot2_param.h>
#include <lcmtypes/arm_rrts_state_t.h>
#include <lcmtypes/arm_rrts_state_list_t.h>

//#include <arm_ik/ik.h>

#define RENDERER_NAME "Arm Circle Trajectory"

#define SEND_CIRCLE_COMMAND "Send Command"

#define RADIUS "Circle Radius"
#define INITIAL_X "Starting X"
#define INITIAL_Y "Starting Y"
#define INITIAL_Z "Starting Z"

#define PI 3.14159265

typedef struct _state_t state_t;

struct _state_t{
  lcm_t *lcm;
    //IK *ik;

  BotRenderer renderer;
  BotViewer *viewer;

  double goal_position[361][3]; //360 data points; x,y,z

  BotGtkParamWidget *pw;
};

static void 
on_params_changed(BotGtkParamWidget *pw, const char *name, 
        state_t *self)
{
  if(!strcmp(name, SEND_CIRCLE_COMMAND)) {
    /*    arm_cmd_circle_trace_t msg;
    msg.radius = bot_gtk_param_widget_get_double(pw, RADIUS);
    msg.initial_pos[0] = bot_gtk_param_widget_get_double(pw, INITIAL_X);
    msg.initial_pos[1] = bot_gtk_param_widget_get_double(pw, INITIAL_Y);
    msg.initial_pos[2] = bot_gtk_param_widget_get_double(pw, INITIAL_Z);

    arm_cmd_circle_trace_t_publish(self->lcm, "ARM_CIRCLE_TRACE", &msg);

    */
    
    double radius = bot_gtk_param_widget_get_double(pw, RADIUS);
    double initial_x = bot_gtk_param_widget_get_double(pw, INITIAL_X);
    double initial_y = bot_gtk_param_widget_get_double(pw, INITIAL_Y);
    double initial_z = bot_gtk_param_widget_get_double(pw, INITIAL_Z);
    double initial_pos[3] = {initial_x, initial_y, initial_z};
    fprintf(stderr, "start\n");
    for(int i = 0; i < 361; i++){
      double angle = i*PI/180;
      self->goal_position[i][0] = initial_pos[0];
      self->goal_position[i][1] = initial_pos[1] + radius*cos(angle);
      self->goal_position[i][2] = initial_pos[2] + radius*sin(angle);
    }
    fprintf(stderr, "waypoints computed\n");

    qs_list_t *qlist = (qs_list_t *) calloc(1, sizeof(qs_list_t));
    double ef_rpy[3] = {0,0,0};

    for(int i = 0; i < 4; i++){
      if(!(self->goal_position[i*90][0] == 0 && self->goal_position[i*90][1] == 0)){
	ef_rpy[2] = atan2(self->goal_position[i*90][1], self->goal_position[i*90][0]);
      }
      if(!ik_q_gen(self->ik, self->goal_position[90*i], ef_rpy, 0, true, qlist)){
	free(qlist);
	fprintf(stderr, "Entire circle boundary not in reach of arm\n");
	return;
      }
    }
    fprintf(stderr, "Prepared to trace circle\n");
    
    //generate rrts_state_list lcm message
    arm_rrts_state_list_t *msg = (arm_rrts_state_list_t *) calloc(1, sizeof(arm_rrts_state_list_t));
    msg->states = (arm_rrts_state_t *) calloc(361, sizeof(arm_rrts_state_t));

    msg->num_states = 361;

    for(int i = 0; i < 361; i++){
      ik_q_gen(self->ik, self->goal_position[i], ef_rpy, 0, true, qlist);
      msg->states[i].num_dimensions = 6;
      msg->states[i].angles = (double *) calloc(msg->states[i].num_dimensions, sizeof(double));
      for(int j = 0; j < msg->states[i].num_dimensions; j++)
	msg->states[i].angles[j] = qlist->qs[0].q_vals[j];
    }
   
    fprintf(stderr, "Publishing...");
    arm_rrts_state_list_t_publish(self->lcm, "ARM_RRTS_STATE_LIST", msg);
    free(qlist);
    for(int i = 0; i < 361; i++)
      free(msg->states[i].angles);
    free(msg->states);
    free(msg);
  
    bot_viewer_request_redraw(self->viewer);
  }
}

BotRenderer *renderer_arm_trace_new (BotViewer *viewer){
  state_t *self = 
    (state_t *) calloc(1, sizeof(state_t));
  self->viewer = viewer;
  self->renderer.name = RENDERER_NAME;
  self->renderer.user = self;
  self->renderer.enabled = 1;

  self->renderer.widget = gtk_alignment_new (0, 0.5, 1.0, 0);

  self->lcm = bot_lcm_get_global(NULL);
  self->ik = ik_get_global(self->lcm);

  self->pw = BOT_GTK_PARAM_WIDGET(bot_gtk_param_widget_new ());
  gtk_container_add(GTK_CONTAINER (self->renderer.widget), 
		     GTK_WIDGET(self->pw));
  gtk_widget_show(GTK_WIDGET (self->pw));

  bot_gtk_param_widget_add_double(self->pw, RADIUS, 
				  BOT_GTK_PARAM_WIDGET_SLIDER, 0.01, 0.5 , 0.01, .15);
  bot_gtk_param_widget_add_double(self->pw, INITIAL_X,
				  BOT_GTK_PARAM_WIDGET_SLIDER, -1, 1, 0.01, .60);
  bot_gtk_param_widget_add_double(self->pw, INITIAL_Y,
				  BOT_GTK_PARAM_WIDGET_SLIDER, -1, 1, 0.01, 0.00);
  bot_gtk_param_widget_add_double(self->pw, INITIAL_Z,
				  BOT_GTK_PARAM_WIDGET_SLIDER, -1, 1, 0.01, .20);

  g_signal_connect (G_OBJECT(self->pw), "changed", 
  		    G_CALLBACK(on_params_changed), self);

  bot_gtk_param_widget_add_buttons(self->pw, SEND_CIRCLE_COMMAND, NULL);

  return &self->renderer;
}

void setup_renderer_arm_trace_circle(BotViewer *viewer, int priority)
{
    bot_viewer_add_renderer(viewer, 
                            renderer_arm_trace_new(viewer), priority);
}
