/*
 * Renders a set of scrolling plots in the top right corner of the window
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <gdk/gdkkeysyms.h>
#include <geom_utils/geometry.h>

#include <bot_core/bot_core.h>
#include <bot_vis/gtk_util.h>
#include <bot_vis/viewer.h>
#include <bot_vis/gl_util.h>
#include <bot_vis/scrollplot2d.h>
#include <bot_vis/bot_vis.h>
#include <bot_frames/bot_frames.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot2_param.h>
#include <lcmtypes/arm_cmd_joint_list_t.h>
#include <lcmtypes/arm_cmd_ik_t.h>
#include <lcmtypes/dynamixel_cmd_address_vals_t.h>

#include <arm_forward_kinematics/forward_kinematics.h>

//#include "robot-arm-sensor-renderer.h"

#define RENDERER_NAME "Arm-Joint-Command"

#define SEND_ARM_COMMAND "Send Command"
#define SEND_XYZ_COMMAND "Send XYZ Command"

#define JOINT_PREFIX "arm_config.joints"

#define ADDRESS_VAL "Address Name"
#define SET_ADDRESS_VAL "Address Value"
#define SEND_ADDRESS_VALS "Send Values"

#define X_POS "X Position"
#define Y_POS "Y Position"
#define Z_POS "Z Position"
#define ROLL "Roll"
#define PITCH "Pitch"
#define YAW "Yaw"

typedef struct _RendererRobotArmCommands RendererRobotArmCommands;

struct _RendererRobotArmCommands {
    lcm_t *lcm;

    BotRenderer renderer;
    BotViewer *viewer;
    BotFrames *frames;
    BotEventHandler ehandler;

    BotGtkParamWidget    *pw;
    int num_joints;
    char **joint_names;
    int num_servos;
    int *num_servos_per_joint;

    double pos[2];

    double xyz_goal[3];

    

    uint64_t      max_utime;

    ForwardKinematics *fk;
};

void apply_values(arm_cmd_joint_t *joint, double joint_angle, int joint_index, double speed){
    joint->joint_index = joint_index; 
    joint->torque_enable = 1; 
    joint->goal_angle = joint_angle; 
    joint->max_speed = speed;
    joint->max_torque = 1023;
}


static void 
on_param_widget_changed (BotGtkParamWidget *pw, const char *name, 
                         RendererRobotArmCommands *self)
{
    if(!strcmp(name, SEND_ARM_COMMAND)) {
        //prob should have enable joint tick bozes also 
        
        int enable[self->num_joints];
        double joint_values[self->num_joints];
        char enable_str[1024];
        for (int i=0; i<self->num_joints; i++) {
            sprintf (enable_str, "%s-enable", self->joint_names[i]);
            enable[i] = bot_gtk_param_widget_get_bool (self->pw, enable_str);
            joint_values[i] = bot_gtk_param_widget_get_double (pw, self->joint_names[i]);
        }
        
        double angles_rad[self->num_joints-1];
        for (int i=0; i<self->num_joints-1; i++) 
            angles_rad[i] = bot_to_radians (joint_values[i]);
            

        BotTrans end_effector;
        fk_forward_kinematics (self->fk, angles_rad, &end_effector);
        fprintf (stdout, "end_effector: trans = [%.4f, %.4f, %.4f]\n",
             end_effector.trans_vec[0], end_effector.trans_vec[1], end_effector.trans_vec[2]);

        bot_frames_transform_vec (self->frames, "base", "local", end_effector.trans_vec, self->xyz_goal);



        int size = 0;
        for(int i=0; i < self->num_joints; i++){
            if(enable[i]){
                size++;
            }
        }

        double speed = 50;

        //ignoring the twist for now 
        arm_cmd_joint_t *list = (arm_cmd_joint_t *) calloc(size, sizeof(arm_cmd_joint_t));
    
        
        arm_cmd_joint_list_t msg = {
            .ncommands = size,
            .commands = list, 
        };


        int c = 0;
        //the index will be wrong for now
        for(int i=0; i < self->num_joints; i++){
            if(enable[i]){
                apply_values(&list[c], joint_values[i], i, speed);
                c++;
            }
        }
        
        arm_cmd_joint_list_t_publish(self->lcm, "ARM_JOINT_COMMANDS", &msg);

        free(list);
        bot_viewer_request_redraw (self->viewer);
    }
}

static void 
on_address_vals_changed (BotGtkParamWidget *pw, const char *name, 
                         RendererRobotArmCommands *self)
{
    if(!strcmp(name, SEND_ADDRESS_VALS)){
        
        int enable[self->num_servos];
        int idx = 0;
        char enable_str[1024];
        for (int i=0; i<self->num_joints; i++) {
            sprintf (enable_str, "%s-enable", self->joint_names[i]);
            for (int j=0; j<self->num_servos_per_joint[i]; j++) {
                enable[idx] = bot_gtk_param_widget_get_bool (self->pw, enable_str);
                idx++;
            }
        }

            

        /* 
         * int enable[7] = {bot_gtk_param_widget_get_bool(self->pw, BASE_ENABLE), 
         *                  bot_gtk_param_widget_get_bool(self->pw, SHOULDER_ENABLE),
         *                  bot_gtk_param_widget_get_bool(self->pw, SHOULDER_ENABLE),
         *                  bot_gtk_param_widget_get_bool(self->pw, ELBOW_ENABLE), 
         *                  bot_gtk_param_widget_get_bool(self->pw, WRIST_BEND_ENABLE),
         *                  bot_gtk_param_widget_get_bool(self->pw, WRIST_TWIST_ENABLE),
         *                  bot_gtk_param_widget_get_bool(self->pw, GRIPPER_ENABLE) 
         * };
         */
      
        int add = bot_gtk_param_widget_get_enum(self->pw, ADDRESS_VAL);
        int value = bot_gtk_param_widget_get_int(self->pw, SET_ADDRESS_VAL);
        if(!(add == 6 || add == 8 || add == 14 || add == 30 || add == 32 || add == 34)){
            if(value > 255)
                return;
        }

        int size = 0;
        //int id[7]; 
        int id[self->num_servos];
        for(int i=0; i < self->num_servos; i++){
            if(enable[i]){
                id[size] = i;
                size++;
            }
        }
      
        dynamixel_cmd_address_vals_t msg = {
            .nservos = size,
            .address = add,
        };

        msg.servo_id = (int32_t *)calloc(msg.nservos, sizeof(int32_t));
        msg.value = (int32_t *)calloc(msg.nservos, sizeof(int32_t));

        for(int i=0; i<size; i++){
            msg.servo_id[i] = id[i];
            msg.value[i] = value;
        }

        dynamixel_cmd_address_vals_t_publish(self->lcm, "DYNAMIXEL_COMMAND_ADDRESSES", &msg);
    
        free(msg.servo_id);
        free(msg.value);
    }
}

static int 
mouse_press (BotViewer *viewer, BotEventHandler *ehandler, const double ray_start[3], 
             const double ray_dir[3], const GdkEventButton *event)
{
    RendererRobotArmCommands *self = (RendererRobotArmCommands*) ehandler->user;

    point2d_t click_pt_local;
  
    if (0 != geom_ray_z_plane_intersect_3d(POINT3D(ray_start), 
                                           POINT3D(ray_dir), 0, &click_pt_local)) {
        bot_viewer_request_redraw(self->viewer);
        //self->active = 0;
        return 0;
    }

    /*self->dragging = 1;

      self->drag_start_local = click_pt_local;
      self->drag_finish_local = click_pt_local;

      recompute_particle_distribution(self);*/

    bot_viewer_request_redraw(self->viewer);
    return 1;
}

static void
robot_commands_draw (BotViewer *viewer, BotRenderer *renderer)
{
    RendererRobotArmCommands *self = (RendererRobotArmCommands*) renderer->user;

    glEnable (GL_DEPTH_TEST);
    glPointSize (10.0);
    glColor3f(1.0, 0.0, 0.0);
    glBegin (GL_POINTS);
    glVertex3f (self->xyz_goal[0],
                self->xyz_goal[1],
                self->xyz_goal[2]);
    glEnd ();
                

    //should render the proposed arm positions
}

static void
robot_commands_free (BotRenderer *renderer) 
{
    RendererRobotArmCommands *self = (RendererRobotArmCommands*) renderer;
    
    if (self)
        free (self);
    
}

BotRenderer *renderer_robot_arm_commands_new (BotViewer *viewer)
{
    RendererRobotArmCommands *self = 
        (RendererRobotArmCommands*) calloc (1, sizeof (RendererRobotArmCommands));
    self->viewer = viewer;
    self->renderer.draw = robot_commands_draw;
    self->renderer.destroy = robot_commands_free;
    self->renderer.name = RENDERER_NAME;
    self->renderer.user = self;
    self->renderer.enabled = 1;

    BotEventHandler *ehandler = &self->ehandler;
    ehandler->name = (char*) RENDERER_NAME;
    ehandler->enabled = 1;
    ehandler->pick_query = NULL;
    ehandler->key_press = NULL;
    ehandler->hover_query = NULL;
    ehandler->mouse_press = NULL; //mouse_press;
    ehandler->mouse_release = NULL;
    ehandler->mouse_motion = NULL;
    ehandler->user = self;

    bot_viewer_add_event_handler(viewer, &self->ehandler, 1);

    self->renderer.widget = gtk_alignment_new (0, 0.5, 1.0, 0);

    self->lcm = bot_lcm_get_global (NULL);

    BotParam *param = bot_param_get_global (self->lcm, 0);
    self->frames = bot_frames_get_global (self->lcm, param);

    self->fk = fk_new();

    
    self->pw = BOT_GTK_PARAM_WIDGET (bot_gtk_param_widget_new ());
    gtk_container_add (GTK_CONTAINER (self->renderer.widget), 
                       GTK_WIDGET(self->pw));
    gtk_widget_show (GTK_WIDGET (self->pw));



    double min_deg, max_deg, min_ticks, max_ticks, offset, deg_to_ticks;
    int sign;
    char key[1024];
    char **joint_names = bot_param_get_subkeys (param, JOINT_PREFIX);

    for (int i=0; joint_names[i]; i++) 
        self->num_joints++;

    self->joint_names = calloc (self->num_joints, sizeof (char *));
    self->num_servos_per_joint = calloc (self->num_joints, sizeof (int));

    for (int i=0; joint_names[i]; i++) {

        self->joint_names[i] = strdup (joint_names[i]);
        
        sprintf (key, "%s.%s.servos.servo_1.min_range", JOINT_PREFIX, joint_names[i]);
        bot_param_get_double (param, key, &min_ticks);
        
        sprintf (key, "%s.%s.servos.servo_1.max_range", JOINT_PREFIX, joint_names[i]);
        bot_param_get_double (param, key, &max_ticks);
        
        sprintf (key, "%s.%s.servos.servo_1.offset", JOINT_PREFIX, joint_names[i]);
        bot_param_get_double (param, key, &offset);
        
        sprintf (key, "%s.%s.servos.deg_to_ticks", JOINT_PREFIX, joint_names[i]);
        bot_param_get_double (param, key, &deg_to_ticks);

        sprintf (key, "%s.%s.servos.servo_1.sign", JOINT_PREFIX, joint_names[i]);
        bot_param_get_int (param, key, &sign);

        sprintf (key, "%s.%s.servos", JOINT_PREFIX, joint_names[i]);
        char **servo_names = bot_param_get_subkeys (param, key);
        for (int j=0; servo_names[j]; j++) {
            self->num_servos_per_joint[i]++;
            self->num_servos++;
        }

        min_deg = sign*(min_ticks - offset)/deg_to_ticks;
        max_deg = sign*(max_ticks - offset)/deg_to_ticks;

        bot_gtk_param_widget_add_double(self->pw, joint_names[i], 
                                    BOT_GTK_PARAM_WIDGET_SLIDER, bot_min(min_deg, max_deg), bot_max(min_deg, max_deg), 0.1, 0);
    

        char enable_str[1024];
        sprintf (enable_str, "%s-enable", joint_names[i]);
        
        fprintf (stdout, "Adding %s\n", enable_str);
        bot_gtk_param_widget_add_booleans(self->pw, 0,  enable_str, 1, NULL);
    }



    //{"CW Angle Limit", "CCW Angle Limit", "Highest Limit Temperature", "Lowest Limit Voltage", "Highest Limit Voltage", "Max torque", "Status return level", "Alarm LED", "Alarm Shutdown","Torque Enable","LED","CW Compliance Margin", "CCW Compliance Margin", "CW Compliance Slope", "CCW Compliance Slope", "Goal Position", "Moving Speed", "Torque Limit"};
    char **add_name = calloc(18, sizeof(char *));
    for (int i=0; i<18; i++){
        add_name[i] = calloc(30, sizeof(char));
    }
    /*
     *add_name[0] = "CW Angle Limit";
     *add_name[1] = "CCW Angle Limit";
     *add_name[2] = "Highest Limit Temperature";
     *add_name[3] = "Lowest Limit Voltage";
     *add_name[4] = "Highest Limit Voltage";
     *add_name[5] = "Max torque";
     *add_name[6] = "Status return level";
     *add_name[7] = "Alarm LED";
     *add_name[8] = "Alarm Shutdown";
     *add_name[9] = "Torque Enable";
     *add_name[10] = "LED";
     *add_name[11] = "CW Compliance Margin";
     *add_name[12] = "CCW Compliance Margin";
     *add_name[13] = "CW Compliance Slope";
     *add_name[14] = "CCW Compliance Slope";
     *add_name[15] = "Goal Position";
     *add_name[16] = "Moving Speed";
     *add_name[17] = "Torque Limit";


     int add_num[18] = {6, 8, 11, 12, 13, 14, 16, 17, 18, 24, 25, 26, 27, 28, 29, 30, 32, 34};
    */
    bot_gtk_param_widget_add_enum(self->pw, ADDRESS_VAL, BOT_GTK_PARAM_WIDGET_MENU, 6, "CW Angle Limit", 6, "CCW Angle Limit", 8, "Highest Limit Temperature", 11, "Lowest Limit Voltage", 12, "Highest Limit Voltage", 13, "Max torque", 14, "Status return level", 16, "Alarm LED", 17, "Alarm Shutdown", 18, "Torque Enable", 24, "LED", 25, "CW Compliance Margin", 26, "CCW Compliance Margin", 27, "CW Compliance Slope", 28, "CCW Compliance Slope", 29, "Goal Position", 30, "Moving Speed", 32, "Torque Limit", 34, NULL);

    bot_gtk_param_widget_add_int(self->pw, SET_ADDRESS_VAL, BOT_GTK_PARAM_WIDGET_SLIDER, 0, 1023, 1, 0);
    
    g_signal_connect (G_OBJECT (self->pw), "changed", 
                      G_CALLBACK (on_param_widget_changed), self);
    
    //bot_gtk_param_widget_add_booleans(self->pw, 0, SEND_ARM_COMMAND , 0, NULL);

    bot_gtk_param_widget_add_buttons(self->pw, SEND_ARM_COMMAND , NULL);


    g_signal_connect (G_OBJECT (self->pw), "changed", 
                      G_CALLBACK (on_address_vals_changed), self);

    bot_gtk_param_widget_add_buttons(self->pw, SEND_ADDRESS_VALS, NULL);

    
    return &self->renderer;
}

void setup_renderer_robot_arm_commands (BotViewer *viewer, int priority)
{
    bot_viewer_add_renderer(viewer, 
                            renderer_robot_arm_commands_new(viewer), priority);
}

