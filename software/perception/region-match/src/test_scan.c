#include "register.h"


int 
main(int argc, char **argv)
{

    lcm_t *lcm = bot_lcm_get_global(NULL);
    BotParam *b_server = bot_param_new_from_server(lcm, 1);
    bot_lcmgl_t *lcmgl = bot_lcmgl_init(lcm, "Scan Matcher");
    bot_lcmgl_t *lcmgl1 = bot_lcmgl_init(lcm, "Transform");

    point_t ref[800];
    for(int i = 0;i < 160;i++){
        point_t p;
        p.x = 6;
        p.y = -4 + i * (double)8 / 160;
        p.z = 0;
        ref[i] = p;
    }
    for(int i = 0;i < 240;i++){
        point_t p;
        p.x = 6 - i * (double)12 / 240;
        p.y = 4;
        p.z = 0;
        ref[160 + i] = p;
    }
    for(int i = 0;i < 160;i++){
        point_t p;
        p.x = -6;
        p.y = 4 - i * (double)8 / 160;
        p.z = 0;
        ref[400 + i] = p;
    }
    for(int i = 0;i < 240;i++){
        point_t p;
        p.x = -6 + i * (double)12 / 240;
        p.y = -4;
        p.z = 0;
        ref[560 + i] = p;
    }

  
 

    double w = (double)1 / 18 * M_PI;
    double tx = 0;
    double ty = 0;

    point_t new[800];

    for(int i = 0;i < 800;i++){
        point_t point;
        point.x = ref[i].x * cos(w) - ref[i].y * sin(w) + tx;
        point.y = ref[i].x * sin(w) + ref[i].y * cos(w) + ty;
        new[i] = point;
    }


    //Drawing
    //Reference points in red
    bot_lcmgl_color3f(lcmgl, 1, 0, 0);
    bot_lcmgl_point_size(lcmgl, 2);
    bot_lcmgl_begin(lcmgl, GL_POINTS);

    for(int i = 0;i < 800;i++){
        point_t p = ref[i];
        bot_lcmgl_vertex3f(lcmgl, p.x, p.y, 0);
    }
    bot_lcmgl_end(lcmgl);


    //New points we want to match are in blue
    bot_lcmgl_color3f(lcmgl, 0, 0, 1);
    bot_lcmgl_point_size(lcmgl, 2);
    bot_lcmgl_begin(lcmgl, GL_POINTS);

    for(int i = 0;i < 800;i++){
        point_t p = new[i];
        bot_lcmgl_vertex3f(lcmgl, p.x, p.y, 0);
    }
    bot_lcmgl_end(lcmgl);

    bot_lcmgl_switch_buffer(lcmgl);

  

    trans_t init_trans;
    init_trans.tx = 0;
    init_trans.tx = 0;
    init_trans.theta = 0;// - (double)1/ 9 * M_PI;// - (double)1 / 50 * M_PI;
    init_trans.match_error = 0;

    int64_t start = bot_timestamp_now();
    trans_t *result = scan_register(ref, new, &init_trans, 800, lcmgl1, 1);
    int64_t end = bot_timestamp_now();

    point_t fix[800];

    for(int i = 0;i < 800;i++){
        point_t point;
        double w = result->theta;
        point.x = new[i].x * cos(w) - new[i].y * sin(w) + result->tx;
        point.y = new[i].x * sin(w) + new[i].y * cos(w) + result->ty;
        fix[i] = point;
    }

    double total_dist = 0;
    for(int i = 0;i < 800;i++){
        total_dist += hypot(fix[i].x - new[i].x, fix[i].y - ref[i].y);
    }

    double pose_dist = 0;
    for(int i = 0;i < 800;i++){
        pose_dist += hypot(new[i].x - ref[i].x, new[i].y - ref[i].y);
    }

    fprintf(stderr, "Pose error: %f\n", pose_dist);
    fprintf(stderr, "Transform angle: %f, Translate x: %f, Translate y: %f, Error: %f\n", 
            result->theta, result->tx, result->ty, total_dist);
    fprintf(stderr, "Time used: %f\n", (end - start) / (double)pow(10, 6));

    free(result);
}
