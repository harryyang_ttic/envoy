#ifndef __SCAN_REGISTER_H__
#define __SCAN_REGISTER_H__


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <getopt.h>

#include <gsl/gsl_blas.h>
#include <lcm/lcm.h>
#include <lcmtypes/er_lcmtypes.h>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <GL/gl.h>
//#include <point_types/point_types.h>
//#include <lcmtypes/erlcm_xyz_point_list_t.h>
#include <full_laser/full_laser.h>


#ifdef __cplusplus
extern "C" {
#endif

  typedef struct _trans_t trans_t;
  struct _trans_t{
    double tx;
    double ty;
    double theta;
    double match_error;
  };

  void destroy_trans(trans_t *trans);


  typedef struct _point_t point_t;
  struct _point_t{
    double x;
    double y;
    double z;
  };

  void destroy_pt(point_t *pt);

  typedef struct _line_t line_t;
  struct _line_t{
    double dist;
    double direction;
  };

  void destroy_line(line_t *line);

  typedef struct _vector_t vector_t;
  struct _vector_t{
    double x;
    double y;
  };

  void destroy_vec(vector_t *v);

  typedef struct _error_func_t error_func_t;
  struct _error_func_t{
    double cx;
    double cy;
    double d;
  };

  void destroy_func(error_func_t *f);

  double calc_dist(double gradient, double intersect, double x, double y);

  double calc_gradient(point_t p1, point_t p2);

  double calc_intersect(point_t p1, double gradient);

  trans_t *calc_error(double trans_angle, line_t *new_line, point_t *new_point, 
		      line_t *proj_line, point_t *valid_p, int valid_new, int v_p, int debug, bot_lcmgl_t *lcmgl);

  trans_t *scan_register(point_t *ref, point_t *new, trans_t *init_trans, int set_size, bot_lcmgl_t *lcmgl, int debug);


#ifdef __cplusplus
}
#endif

#endif
