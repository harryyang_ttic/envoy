#include "register.h"


void destroy_trans(trans_t *trans)
{
    free(trans);
}

void destroy_pt(point_t *pt)
{
    free(pt);
}

void destroy_line(line_t *line)
{
    free(line);
}

void destroy_vec(vector_t *v)
{
    free(v);
}

void destroy_func(error_func_t *f)
{
    free(f);
}


//dist from a point to a line
double calc_dist(double gradient, double intersect, double x, double y)
{
    double a = gradient;
    int b = -1;
    double c = intersect;
    double dist = fabs(a * x + b * y + c) / (double)hypot(a, b);
    return dist;
}

//gradient
double calc_gradient(point_t p1, point_t p2)
{
    double gradient = (double)(p2.y - p1.y) / (p2.x - p1.x);
    return gradient;
}

//intersect
double calc_intersect(point_t p1, double gradient)
{
    double intersect = p1.y - gradient * p1.x;
    return intersect;
}




//Given a trans angle
trans_t *calc_error(double trans_angle, line_t *new_line, point_t *new_point, 
                    line_t *proj_line, point_t *valid_p, int valid_new, int v_p, int debug, bot_lcmgl_t *lcmgl)
{
    double A = 0;
    double B = 0;
    double C = 0;
    double D = 0;
    double E = 0;

    int func_no = 0;
    error_func_t func[valid_new];

    int outlier = 0;
    int match = 0;

    //Thresholds, need to try different values
    double alpha = (double)1/60 * M_PI;
    double Hd = 0.5;

    int iter = 0;
    
    for(int i = 0; i < valid_new;i++){
        iter++;
        double angle = new_line[i].direction;
        vector_t normal;
      
        if(cos(angle) < 0){
            normal.x = -1;
            normal.y = normal.x * tan(angle);
        }
        else{
            normal.x = 1;
            normal.y = tan(angle);
        }

        double new_x = new_point[i].x * cos(trans_angle) - new_point[i].y * sin(trans_angle);
        double new_y = new_point[i].x * sin(trans_angle) + new_point[i].y * cos(trans_angle);
        double th = atan(new_y / new_x);
        double cs = new_x / hypot(new_x, new_y);

        int index = -1;
        double min_diff = 100;
        for(int j = 0;j < v_p;j++){
            double angle = atan(valid_p[j].y / valid_p[j].x);
            double cccos = valid_p[j].x / hypot(valid_p[j].x, valid_p[j].y);
            if(fabs(angle - th) < min_diff && cccos * cs >= 0){
                min_diff = fabs(angle - atan(new_y / new_x));
                index = j;
            }
        }

        vector_t normal_ref;
        double a = proj_line[index].direction;
      
        if(cos(a) < 0){
            normal_ref.x = -1;
            normal_ref.y = normal_ref.x * tan(a);
        }
        else{
            normal_ref.x = 1;
            normal_ref.y = tan(a);
        }


        //Draw normal lines of each point
        if(debug == 1){
            bot_lcmgl_color3f(lcmgl, 0, 0, 0);
            bot_lcmgl_line_width(lcmgl, 2);
            bot_lcmgl_begin(lcmgl, GL_LINE_STRIP);
    

            for(int i = 0;i < v_p;i++){
                point_t p = valid_p[i];
                point_t p1;
                p1.x = p.x + 2 * normal_ref.x;
                p1.y = p.y + 2 * normal_ref.y;
	  
                bot_lcmgl_vertex3f(lcmgl, p.x, p.y, 0);
                bot_lcmgl_vertex3f(lcmgl, p1.x, p1.y, 0);
            }

            bot_lcmgl_end(lcmgl);
            bot_lcmgl_switch_buffer(lcmgl);
        }


        double cx = normal.x * cos(trans_angle) - normal.y * sin(trans_angle) + normal_ref.x;
        double cy = normal.x * sin(trans_angle) + normal.y * cos(trans_angle) + normal_ref.y;
        double dx = valid_p[index].x - new_x;
        double dy = valid_p[index].y - new_y;
        double d = cx * dx + cy * dy;

        /*
          vector_t normalized_n1;
          normalized_n1.x = normal.x / hypot(normal.x, normal.y);
          normalized_n1.y = normal.y / hypot(normal.x, normal.y);

          vector_t normalized_n2;
          normalized_n2.x = normal_ref.x / hypot(normal_ref.x, normal_ref.y);
          normalized_n2.y = normal_ref.y / hypot(normal_ref.x, normal_ref.y);

          double trans_x = normalized_n1.x * cos(trans_angle) - normalized_n1.y * sin(trans_angle);
          double trans_y = normalized_n1.x * sin(trans_angle) + normalized_n1.y * cos(trans_angle);
        */
        /*
          if(trans_x * normalized_n2.x + trans_y * normalized_n2.y < cos(alpha)){
          outlier++;
          continue;
          }

          if(d >= Hd){
          outlier++;
          continue;
          }
        */
        //fprintf(stderr, "%f %f %f\n", cx, cy, d);
        //fprintf(stderr, "This is %d iter\n", iter);

        A += cx * cx;
        B += cx * cy;
        C += cy * cy;
        D += cx * d;
        E += cy * d;    

        error_func_t f;
        f.cx = cx;
        f.cy = cy;
        f.d = d;
        func[func_no] = f;
        func_no++;
    }

    double ty = (B * D - A * E) / (B * B - A * C);
    double tx = (E - C * ty) / B;


    double error_sum = 0;
    for(int i = 0;i < func_no;i++){
        error_sum += pow((func[i].cx * tx + func[i].cy * ty - func[i].d), 2); 
    }


    double match_error = (double)1 / valid_new * (error_sum + outlier * Hd);
    trans_t *t = (trans_t *)calloc(1, sizeof(trans_t));
    t->tx = tx;
    t->ty = ty;
    t->theta = trans_angle;
    t->match_error = match_error;
    return t;
}



//Rotation is anti clockwise
//set size is the no of points - this seems to assume that the points are the same size - wrong 
trans_t *scan_register(point_t *ref, point_t *new, trans_t *init_trans, int set_size, bot_lcmgl_t *lcmgl, int debug)
{

    point_t proj[set_size];

    double init_theta = init_trans->theta;
    double tx = init_trans->tx;
    double ty = init_trans->ty;

    for(int i = 0;i < set_size;i++){
        proj[i].x = ref[i].x;
        proj[i].y = ref[i].y;
        proj[i].z = 0;
    }

    /*
    //Project the reference scan to the new pose using init_trans   O(n)
    for(int i = 0;i < set_size;i++){
    double prev_x = ref[i].x;
    double prev_y = ref[i].y;
    
    double x = prev_x * cos(theta) - prev_y * sin(theta);
    double y = prev_x * sin(theta) + prev_y * cos(theta);
    proj[i].x = x;
    proj[i].y = y;
    proj[i].z = 0;
    }
    */

  

    /*
      int valid_size = 0;
      point_t val_proj[set_size];

      //Discard those points on ref scan which are likely not visible from new pose
      //1. after projection, polar angles are in wrong order   O(n)
  
      double prev_max = -100;
      for(int i = 0;i < set_size;i++){
      double angle = atan(proj[i].y / proj[i].x);
      //if(angle >= prev_max){
      prev_max = angle;
      val_proj[valid_size] = proj[i];
      valid_size++;
      //}
      }

      fprintf(stderr, "Size: %d\n", valid_size);

      int v_size = 0;
      point_t v_proj[valid_size];
      //2. along the rays from new origin to the points, if there are other points close enough to rays O(n^2)
      //we might not need this, it is expensive
      for(int i = 0;i < valid_size;i++){
      for(int j = 0;j < valid_size;j++){
      point_t p1 = val_proj[i];
      point_t p2 = val_proj[j];
      point_t origin;
      origin.x = 0;
      origin.y = 0;
      origin.z = 0;
      
      if(fabs(p2.x) < fabs(p1.x) && fabs(p2.y) < fabs(p1.y)){
      double gradient = calc_gradient(p1, origin);
      double intersect = calc_intersect(p1, gradient);
      double dist = calc_dist(gradient, intersect, p2.x, p2.y);
      //if(dist < 0.001)
      //  continue;
      }
      v_proj[v_size] = val_proj[i];
      v_size++;
      break;
      }
      }
    */
    //Compute the tangent directions on each scan by fitting lines to neighborhood
    int n = 7;

    int valid_proj = 0;
    line_t proj_line[set_size];
    int v_p = 0;
    point_t valid_p[set_size];
    for(int i = 0;i < set_size;i++){
        double mean_x = 0;
        double mean_y = 0;
        double sqr_x = 0;
        double sqr_y = 0;
        double xy = 0;
        double sum_x = 0;
        double sum_y = 0;
        for(int j = -3;j <= 3;j++){
            int index = i + j;
            if(index < 0)
                index = index + (set_size - 1);
            else if(index > (set_size - 1))
                index = index - (set_size - 1);
            sum_x += proj[index].x;
            sum_y += proj[index].y;
    
        }

        mean_x = (double)1 / n * sum_x;
        mean_y = (double)1 / n * sum_y;

        for(int j = -3;j <= 3;j++){
            int index = i + j;
            if(index < 0)
                index = index + (set_size - 1);
            else if(index > (set_size - 1))
                index = index - (set_size - 1);
            sqr_x += (proj[index].x - mean_x) * (proj[index].x - mean_x);
            sqr_y += (proj[index].y - mean_y) * (proj[index].y - mean_y);
            xy += (proj[index].x - mean_x) * (proj[index].y - mean_y);
   
        }

        double angle = 0.5 * atan((double)(0 - 2) * xy / (sqr_y - sqr_x));

    

        double p = mean_x * cos(angle) + mean_y * sin(angle);
        double min_error = 0.5 * (sqr_x + sqr_y - sqrt(4 * xy * xy + (sqr_y - sqr_x) * (sqr_y - sqr_x)));


        //Exclude sample points in non-smooth regions
        //1. exclude surface nearly parallel to sensing direction or depth discontinuity
        double incidence_angle = atan(proj[i].y / proj[i].x) - angle;

        /*
        //Need to test this threshold
        if(incidence_angle >= 0.8)
        continue;
    
        //2. check if fitting error min(Efit) is too large
        //Need to test this threshold
        if(min_error >= 0.8)
        continue;
        */

        //Add this tangent line
        line_t l;
        l.dist = p;
        l.direction = angle;
        proj_line[valid_proj] = l;
        point_t pt = proj[i];
        valid_p[v_p] = pt;
        v_p++;
    }

  
    double prev_angle = -1;

    int valid_new = 0;
    line_t new_line[set_size];
    int valid_newp = 0;
    point_t new_point[set_size];
    for(int i = 0;i < set_size;i++){
        double mean_x = 0;
        double mean_y = 0;
        double sqr_x = 0;
        double sqr_y = 0;
        double xy = 0;
        double sum_x = 0;
        double sum_y = 0;
        for(int j = -3;j <= 3;j++){
            int index = i + j;
            if(index < 0)
                index = index + (set_size - 1);
            else if(index > (set_size - 1))
                index = index - (set_size - 1);
            sum_x += new[index].x;
            sum_y += new[index].y;
    
        }

        mean_x = (double)1 / n * sum_x;
        mean_y = (double)1 / n * sum_y;

        for(int j = -3;j <= 3;j++){
            int index = i + j;
            if(index < 0)
                index = index + (set_size - 1);
            else if(index > (set_size - 1))
                index = index - (set_size - 1);
            sqr_x += (new[index].x - mean_x) * (new[index].x - mean_x);
            sqr_y += (new[index].y - mean_y) * (new[index].y - mean_y);
            xy += (new[index].x - mean_x) * (new[index].y - mean_y);
        }

        double angle = 0.5 * atan((double)2 * xy / (sqr_x - sqr_y));
        double p = mean_x * cos(angle) + mean_y * sin(angle);
        double min_error = 0.5 * (sqr_x + sqr_y - sqrt(4 * xy * xy + (sqr_y - sqr_x) * (sqr_y - sqr_x)));

        /*
          if(i == 600){
          fprintf(stderr, "=========DEBUG=============\n");
          for(int j = -3;j <= 3;j++){
          int index = i + j;
          if(index < 0)
          index = index + (set_size - 1);
          else if(index > (set_size - 1))
          index = index - (set_size - 1);
          fprintf(stderr, "Index: %d, x: %f, y:%f\n", index, new[index].x, new[index].y);
          sum_x += new[index].x;
          sum_y += new[index].y;
          }
          fprintf(stderr, "Meanx: %f, Meany: %f\n", mean_x, mean_y);
          fprintf(stderr, "Min Error: %f\n", min_error);
          }
        */

        //Exclude sample points in non-smooth regions
        //1. exclude surface nearly parallel to sensing direction or depth discontinuity
        double incidence_angle = atan(new[i].y / new[i].x) - angle;


        //fprintf(stderr, "Incidence angle: %f\n", incidence_angle);
        //fprintf(stderr, "Min error: %f\n", min_error);

        //Need to test this threshold
        /*
          if(incidence_angle >= 0.8)
          continue;
    
          //2. check if fitting error min(Efit) is too large
          //Need to test this threshold
          if(min_error >= 0.8)
          continue;
        */

        //Add this tangent line
        line_t l;
        l.dist = p;
        l.direction = angle;

        if(fabs(angle - prev_angle) > 0.00001){
            prev_angle = angle;
            fprintf(stderr, "Angle: %f\n", angle);
        }
    
    
        new_line[valid_new] = l;
        valid_new++;
        new_point[valid_newp] = new[i];
        valid_newp++;
    }

    double w1 = init_theta - 0.103 * M_PI;
    double w2 = init_theta + (double)1 / 6 * M_PI - 0.103 * M_PI;
    double x1 = init_theta;
    int iter = 0;
    trans_t *t1;
    trans_t *t2;
    trans_t *chosen;

    double prev_w1 = -1000;
    double prev_w2 = -1000;

    while(1){

        iter++;

        double x2 = w1 + w2 - x1;
  
        t1 = calc_error(x1, new_line, new_point, proj_line, valid_p, valid_new, v_p, debug, lcmgl);
        t2 = calc_error(x2, new_line, new_point, proj_line, valid_p, valid_new, v_p, 0, lcmgl);
        double e1 = t1->match_error;
        double e2 = t2->match_error;

        if(e1 > e2){
            if(x1 >= x2){
                w2 = x1;
                x1 = x2;
            }
            else{
                w1 = x1;
                x1 = x2;
            }
            chosen = t2;
            fprintf(stderr, "Cost: %f\n", e2);
        }
        else{
            if(x1 >= x2)
                w1 = x2;
            else
                w2 = x2;
            chosen = t1;
            fprintf(stderr, "Cost: %f\n", e1);
        }

    
        bot_lcmgl_color3f(lcmgl, 0, 1, 0);
        bot_lcmgl_point_size(lcmgl, 2);
        bot_lcmgl_begin(lcmgl, GL_POINTS);
    

        for(int i = 0;i < set_size;i++){
            point_t p = proj[i];
            double w = chosen->theta;
            double tx = chosen->tx;
            double ty = chosen->ty;
            double x = p.x * cos(w) - p.y * sin(w) + tx;
            double y = p.x * sin(w) + p.y * cos(w) + ty;
            bot_lcmgl_vertex3f(lcmgl, x, y, 0);
        }

        bot_lcmgl_end(lcmgl);
        bot_lcmgl_switch_buffer(lcmgl);


        if(w1 > w2){
            fprintf(stderr, "ERROR in interval\n");
            return NULL;
        }

        if(x1 > w2 || x1 < w1){
            fprintf(stderr, "ERROR!\n");
            return NULL;
        }

        //fprintf(stderr, "prev w1: %f, w1: %f, prev w2: %f, w2: %f\n", prev_w1, w1, prev_w2, w2);
        if(fabs(prev_w1 - w1) < 0.000001 && fabs(prev_w2 - w2) < 0.000001)
            break;
        else{
            prev_w1 = w1;
            prev_w2 = w2;
        }

        if(iter == 1)
            break;
    

        if(iter == 100 || fabs(w2 - w1) < 0.0001)
            break;

        destroy_trans(t1);
        destroy_trans(t2);
    }

    trans_t *transform = (trans_t *)calloc(1, sizeof(trans_t));
    fprintf(stderr, "No. of iterations: %d, Interval: %f\n", iter, (w2 - w1));
    transform->theta = x1;
    if(t1->theta == x1){
        free(t2);
        return t1;
    }
    else{
        free(t1);
        return t2;
    }
}
