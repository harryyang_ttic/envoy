#include "line_match.h"


void destroy_pt(point_t *pt)
{
  free(pt);
}


void destroy_line(line_t *line)
{
  free(line);
}


double calc_dist(double gradient, double intersect, double x, double y)
{
  double a = gradient;
  int b = -1;
  double c = intersect;
  double dist = fabs(a * x + b * y + c) / (double)hypot(a, b);
  return dist;
}


line_t *extract_line_from_points(point_t *points, int start_index, int end_index)
{
    double sumx = 0;
    double sumy = 0;
    double model = points[start_index].xy[0];
    int vertical = 1;
    for(int i = start_index;i <= end_index;i++){
        sumx += points[i].xy[0];
        sumy += points[i].xy[1];
	if(points[i].xy[0] != model)
	  vertical = 0;
    }


    double meanx = (double)sumx / (end_index - start_index + 1);
    double meany = (double)sumy / (end_index - start_index + 1);
  
    //move centroid to origin
    for(int i = 0;i <= end_index - start_index;i++){
        points[i].xy[0] -= meanx;
        points[i].xy[1] -= meany;
    }
  
    //calculate sum of x^2-y^2 and sum of xy and A
    double sum1 = 0;
    double sum2 = 0;
    for(int i = 0;i <= end_index - start_index;i++){
        sum1 += ((points[i].xy[0] * points[i].xy[0]) - (points[i].xy[1] * points[i].xy[1]));
        sum2 += (points[i].xy[0] * points[i].xy[1]);
    }
    double quadratic = (double)sum1 / sum2;
  
    //calculate gradient
    double gradient1 = ((0 - quadratic) + sqrt(pow(quadratic, 2) - 4 * (-1))) / 2;
    double gradient2 = ((0 - quadratic) - sqrt(pow(quadratic, 2) - 4 * (-1))) / 2;
    double gradient;

    //Determine which gradient is maximum & which is minimum
    double dist1 = 0;
    double dist2 = 0;
    for(int i = 0;i <= end_index - start_index;i++){
        dist1 += fabs(gradient1 * points[i].xy[0] - points[i].xy[1]) / sqrt(1 + gradient1 * gradient1);
        dist2 += fabs(gradient2 * points[i].xy[0] - points[i].xy[1]) / sqrt(1 + gradient2 * gradient2);
    }

    if(dist1 <= dist2)
        gradient = gradient1;
    else
        gradient = gradient2;
    
    double theta = atan(gradient);
    if((points[end_index].xy[0] - points[start_index].xy[0]) < 0)
      theta += M_PI;
    
    //get intersect
    double intersect = meany - gradient * meanx;

    //fitting cost
    double cost = 0;
    for(int i = start_index;i <= end_index;i++){
      cost += calc_dist(gradient, intersect, points[i].xy[0], points[i].xy[1]);
    }

    //Return the line struct
    line_t *line = (line_t *)calloc(1, sizeof(line_t));

    if(vertical == 1){
      if(points[end_index].xy[1] > points[start_index].xy[1])
	line->theta = M_PI / 2;
      else
	line->theta = (double)3 / 2 * M_PI;
      return line;
    }

    line->gradient = gradient;
    line->theta = theta;
    line->cost = cost;

    return line;
}





//
void processScan(point_t *pos, line_t *lines, int *valid, int set_size)
{

  //Fit every 5 points to a straight line
  for(int i = 0;i < set_size / 5;i++){
    point_t points[5];
    for(int j = 0;j < 5;j++){
      points[j] = pos[i + j];
    }
    line_t *l = extract_line_from_points(points, 0, 4);
    lines[i].gradient = l->gradient;
    lines[i].theta = l->theta;
    lines[i].cost = l->cost;
    if(l->cost > 2.5)
      valid[i] = 0;
    else
      valid[i] = 1;
  }


  

}
