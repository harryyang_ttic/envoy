//Use velodyne info to detect doorways. It can only detect doorways that are 3-5m apart. Not a classifier on spot.

#include "line_match.h"



int unit_test_extraction()
{
  point_t points[5];
  points[0].xy[0] = 4;
  points[0].xy[1] = 4;
  points[1].xy[0] = 3;
  points[1].xy[1] = 3;
  points[2].xy[0] = 2;
  points[2].xy[1] = 2;
  points[3].xy[0] = 1;
  points[3].xy[1] = 1;
  points[4].xy[0] = 0;
  points[4].xy[1] = 0;

  line_t *l = extract_line_from_points(points, 0, 4);
  if(l->theta == (double)5 / 4 * M_PI && l->cost == 0){
    destroy_line(l);
    return 1;
  }
  else{
    destroy_line(l);
    return 0;
  }
}


int unit_test_filter()
{
  point_t points[5];
  points[0].xy[0] = 4;
  points[0].xy[1] = 4;
  points[1].xy[0] = 3;
  points[1].xy[1] = 3;
  points[2].xy[0] = 2;
  points[2].xy[1] = 2;
  points[3].xy[0] = 1;
  points[3].xy[1] = 1;
  points[4].xy[0] = 0;
  points[4].xy[1] = 0;

  line_t lines[1];
  int valid[1];
  processScan(points, lines, valid, 5);
  if(lines[0].cost == 0 && valid[0] == 1)
    return 1;
  else
    return 0;
}



int 
main(int argc, char **argv)
{
  int extract = unit_test_extraction();
  if(extract == 1)
    fprintf(stderr, "Pass the line extraction test.\n");
  else{
    fprintf(stderr, "Line extraction test failed.\n");
    return -1;
  }
  int filter = unit_test_filter();
  if(filter == 1)
    fprintf(stderr, "Pass the line extraction filter test.\n");
  else{
    fprintf(stderr, "Line extraction test failed.\n");
    return -1;
  }
}
