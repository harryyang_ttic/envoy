#ifndef __LINE_MATCH_H__
#define __LINE_MATCH_H__


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <getopt.h>

#include <gsl/gsl_blas.h>
#include <lcm/lcm.h>
#include <lcmtypes/er_lcmtypes.h>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <GL/gl.h>
#include <full_laser/full_laser.h>


#ifdef __cplusplus
extern "C" {
#endif

  typedef struct _point_t point_t;
  struct _point_t{
    double xy[2];
  };

  void destroy_pt(point_t *pt);

  typedef struct _line_t line_t;
  struct _line_t{
    double gradient;
    double theta;
    double cost;
  };

  void destroy_line(line_t *line);

  line_t *extract_line_from_points(point_t *points, int start_index, int end_index);

  void processScan(point_t *pos, line_t *lines, int *valid, int set_size);


#ifdef __cplusplus
}
#endif

#endif
