#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <bot_frames/bot_frames.h>
#include <bot_lcmgl_client/lcmgl.h>

#define POSE_LIST_SIZE 10
#define TIMEOUT_THRESHOLD 5.0 //sec
typedef struct
{
    BotParam   *b_server;
    lcm_t      *lcm;
    GMainLoop *mainloop;
    pthread_t  work_thread;

    BotFrames *frames;

    int have_kinect; 
    int have_laser; 
    
    int is_reset; 
    int64_t time_of_last_agreement; 
    erlcm_kinect_person_tracker_status_t *kinect_track;
    int64_t last_valid_kinect;
    erlcm_kinect_person_tracker_status_t *laser_track;
    int64_t last_valid_laser;
    double default_pos[3];

    int publish_all;
    int verbose; 
    int drawing;
} state_t;


static void on_laser_person(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                            const erlcm_kinect_person_tracker_status_t * msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 
    
    if(s->laser_track){
        erlcm_kinect_person_tracker_status_t_destroy(s->laser_track);
    }
    s->laser_track = erlcm_kinect_person_tracker_status_t_copy(msg);
}


static void on_kinect_person(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
			     const erlcm_kinect_person_tracker_status_t * msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 

    
    if(s->kinect_track){
        erlcm_kinect_person_tracker_status_t_destroy(s->kinect_track);
    }
    s->kinect_track = erlcm_kinect_person_tracker_status_t_copy(msg);

    //lets republish this command - for the laser to see 

    //we are publishing this out all the time 
    erlcm_kinect_person_tracker_status_t_publish(s->lcm, "PERSON_MODERATOR_SERVO", s->kinect_track);
    
}

void check_and_turn(state_t *s, double k_pos[3], double l_pos[3], int k_valid, int l_valid){
    if(s->verbose){
        fprintf(stderr,"Doing check and logic\n");
    }

    if(l_valid && k_valid){
        //check if they both agree 
        double dist = hypot(l_pos[0] - k_pos[0], l_pos[1] - k_pos[1]);
        if(s->verbose)
            fprintf(stderr,"Distance : %f\n", dist);

        if(dist < 0.5){
            s->is_reset = 0;
            if(s->verbose)
                fprintf(stderr,"Kinect and Laser both agree\n");
            s->time_of_last_agreement = bot_timestamp_now(); //this will mess up in playback
            erlcm_roi_t msg; 
            memset(&msg, 0, sizeof(erlcm_roi_t)); 
            msg.utime = bot_timestamp_now();
            msg.foviation_type = ERLCM_ROI_T_MOVE_HORIZONTAL; 
            msg.pos[0] = l_pos[0];
            msg.pos[1] = l_pos[1];
            msg.pos[2] = l_pos[2];
            msg.frame = "body";

            erlcm_roi_t_publish(s->lcm, "ROI", &msg);
        }
        else{
            if((bot_timestamp_now() - s->time_of_last_agreement)/1.0e6 > TIMEOUT_THRESHOLD){
                if(s->verbose)
                    fprintf(stderr,"Kinect and Laser dont agree - for too long - turning forward\n");
                erlcm_roi_t msg; 
                memset(&msg, 0, sizeof(erlcm_roi_t)); 
                msg.utime = bot_timestamp_now();
                msg.foviation_type = ERLCM_ROI_T_MOVE_HORIZONTAL; 
                msg.pos[0] = s->default_pos[0];
                msg.pos[1] = s->default_pos[1];
                msg.pos[2] = s->default_pos[2];
                msg.frame = "body";

                erlcm_roi_t_publish(s->lcm, "ROI", &msg);
                //tell the laser and kinect to reset 
                erlcm_person_tracking_cmd_t t_msg;
                t_msg.utime = bot_timestamp_now();
                t_msg.command = ERLCM_PERSON_TRACKING_CMD_T_CMD_STOP_TRACKING;
                t_msg.sender = ERLCM_PERSON_TRACKING_CMD_T_PERSON_MODERATOR;
                erlcm_person_tracking_cmd_t_publish(s->lcm, "PERSON_TRACKING_CMD", &t_msg);

                s->is_reset = 1;

            }
            else{
                if(s->verbose)
                    fprintf(stderr,"Kinect and Laser dont agree - waiting till threshold\n");
                erlcm_roi_t msg; 
                memset(&msg, 0, sizeof(erlcm_roi_t)); 
                msg.utime = bot_timestamp_now();
                msg.foviation_type = ERLCM_ROI_T_MOVE_HORIZONTAL; 
                msg.pos[0] = l_pos[0];
                msg.pos[1] = l_pos[1];
                msg.pos[2] = l_pos[2];
                msg.frame = "body";

                erlcm_roi_t_publish(s->lcm, "ROI", &msg);
                s->is_reset = 0;
            }
        }
        
       
    }

    else if(l_valid){

        if((bot_timestamp_now() - s->time_of_last_agreement)/1.0e6 > TIMEOUT_THRESHOLD){
            if(s->verbose)
                fprintf(stderr,"Kinect doesnt see - for too long - turning forward\n");
            erlcm_roi_t msg; 
            memset(&msg, 0, sizeof(erlcm_roi_t)); 
            msg.utime = bot_timestamp_now();
            msg.foviation_type = ERLCM_ROI_T_MOVE_HORIZONTAL; 
            msg.pos[0] = s->default_pos[0];
            msg.pos[1] = s->default_pos[1];
            msg.pos[2] = s->default_pos[2];
            msg.frame = "body";

            erlcm_roi_t_publish(s->lcm, "ROI", &msg);
            //tell the laser and kinect to reset 
            erlcm_person_tracking_cmd_t t_msg;
            t_msg.utime = bot_timestamp_now();
            t_msg.command = ERLCM_PERSON_TRACKING_CMD_T_CMD_STOP_TRACKING;
            t_msg.sender = ERLCM_PERSON_TRACKING_CMD_T_PERSON_MODERATOR;
            erlcm_person_tracking_cmd_t_publish(s->lcm, "PERSON_TRACKING_CMD", &t_msg);

            s->is_reset = 1;

        }
        else{
            if(s->verbose)
                fprintf(stderr,"Kinect doesnt see - waiting till threshold\n");
            erlcm_roi_t msg; 
            memset(&msg, 0, sizeof(erlcm_roi_t)); 
            msg.utime = bot_timestamp_now();
            msg.foviation_type = ERLCM_ROI_T_MOVE_HORIZONTAL; 
            msg.pos[0] = l_pos[0];
            msg.pos[1] = l_pos[1];
            msg.pos[2] = l_pos[2];
            msg.frame = "body";

            erlcm_roi_t_publish(s->lcm, "ROI", &msg);
            s->is_reset = 0;
        }
         
         
        /*        fprintf(stderr,"Laser sees the person - but the kinect doesn't - turn the kinect towards the person\n");
        //send out an roi message 
        erlcm_roi_t msg; 
        memset(&msg, 0, sizeof(erlcm_roi_t)); 
        msg.utime = bot_timestamp_now();
        msg.foviation_type = ERLCM_ROI_T_MOVE_HORIZONTAL; 
        msg.pos[0] = l_pos[0];
        msg.pos[1] = l_pos[1];
        msg.pos[2] = l_pos[2];
        msg.frame = "body";

        erlcm_roi_t_publish(s->lcm, "ROI", &msg);
        s->is_reset = 0;
        //some message needs to be sent to the kinect*/
    }
    else if(k_valid){
        /*erlcm_roi_t msg; 
          memset(&msg, 0, sizeof(erlcm_roi_t)); 
          msg.foviation_type = ERLCM_ROI_T_MOVE_HORIZONTAL; 
          msg.utime = bot_timestamp_now();
          msg.pos[0] = k_pos[0];
          msg.pos[1] = k_pos[1];
          msg.pos[2] = k_pos[2];
          msg.frame = "body";
          s->is_reset = 0;
          erlcm_roi_t_publish(s->lcm, "ROI", &msg);

          fprintf(stderr,"Kinect sees a person - should we init the laser??\n");*/
        if((bot_timestamp_now() - s->time_of_last_agreement)/1.0e6 > TIMEOUT_THRESHOLD){
            if(s->verbose)
                fprintf(stderr,"Laser doesnt see - for too long - turning forward\n");
            erlcm_roi_t msg; 
            memset(&msg, 0, sizeof(erlcm_roi_t)); 
            msg.utime = bot_timestamp_now();
            msg.foviation_type = ERLCM_ROI_T_MOVE_HORIZONTAL; 
            msg.pos[0] = s->default_pos[0];
            msg.pos[1] = s->default_pos[1];
            msg.pos[2] = s->default_pos[2];
            msg.frame = "body";

            erlcm_roi_t_publish(s->lcm, "ROI", &msg);
            //tell the laser and kinect to reset 
            erlcm_person_tracking_cmd_t t_msg;
            t_msg.utime = bot_timestamp_now();
            t_msg.command = ERLCM_PERSON_TRACKING_CMD_T_CMD_STOP_TRACKING;
            t_msg.sender = ERLCM_PERSON_TRACKING_CMD_T_PERSON_MODERATOR;
            erlcm_person_tracking_cmd_t_publish(s->lcm, "PERSON_TRACKING_CMD", &t_msg);

            s->is_reset = 1;

        }
        else{
            if(s->verbose)
                fprintf(stderr,"laser doesn't see - waiting till threshold\n");
            erlcm_roi_t msg; 
            memset(&msg, 0, sizeof(erlcm_roi_t)); 
            msg.utime = bot_timestamp_now();
            msg.foviation_type = ERLCM_ROI_T_MOVE_HORIZONTAL; 
            msg.pos[0] = k_pos[0];
            msg.pos[1] = k_pos[1];
            msg.pos[2] = k_pos[2];
            msg.frame = "body";

            erlcm_roi_t_publish(s->lcm, "ROI", &msg);
            s->is_reset = 0;
        }
    }
    else{
        //if((bot_timestamp_now() - s->time_of_last_agreement)/1.0e6 > TIMEOUT_THRESHOLD){
        if(s->verbose)
            fprintf(stderr,"Kinect and Laser both dont see - for too long - turning forward\n");
        erlcm_roi_t msg; 
        memset(&msg, 0, sizeof(erlcm_roi_t)); 
        msg.utime = bot_timestamp_now();
        msg.foviation_type = ERLCM_ROI_T_MOVE_HORIZONTAL; 
        msg.pos[0] = s->default_pos[0];
        msg.pos[1] = s->default_pos[1];
        msg.pos[2] = s->default_pos[2];
        msg.frame = "body";

        erlcm_roi_t_publish(s->lcm, "ROI", &msg);
        //tell the laser and kinect to reset 
        erlcm_person_tracking_cmd_t t_msg;
        t_msg.utime = bot_timestamp_now();
        t_msg.command = ERLCM_PERSON_TRACKING_CMD_T_CMD_STOP_TRACKING;
        t_msg.sender = ERLCM_PERSON_TRACKING_CMD_T_PERSON_MODERATOR;
        erlcm_person_tracking_cmd_t_publish(s->lcm, "PERSON_TRACKING_CMD", &t_msg);

        s->is_reset = 1;
        //}

    }
}

//pthread
static void *track_work_thread(void *user)
{
    state_t *s = (state_t*) user;

    printf("obstacles: track_work_thread()\n");

    while(1){
	fprintf(stderr, " T - called ()\n");
	
	usleep(50000);
    }
}

void read_parameters_from_conf(state_t *s)
{
    BotParam *b_param = s->b_server; 
    double max_t_vel =  bot_param_get_double_or_fail(b_param, "robot.max_t_vel");
    double max_r_vel = bot_param_get_double_or_fail(b_param, "robot.max_r_vel");
  
    char **planar_lidar_names = bot_param_get_all_planar_lidar_names(b_param);
  
    if(planar_lidar_names) {
	for (int pind = 0; planar_lidar_names[pind] != NULL; pind++) {
	    fprintf(stderr, "Channel : %s\n", planar_lidar_names[pind]);
	}
    }
  
    g_strfreev(planar_lidar_names);
}

//doesnt do anything right now - timeout function
gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;

    int have_k_track = 0, have_l_track = 0; 

    double xyz_l_body[3] = {.0,.0,.0};
    double xyz_k_body[3] = {.0,.0,.0};

    double body_to_local[12];
    if (!bot_frames_get_trans_mat_3x4 (s->frames, "body",
                                       "local", 
                                       body_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from KINECT to body!\n");
        return TRUE;        
    }

    if(s->is_reset){
        //check if the head is at the correct heading 
        double b_pos[3], h_pos[3] = {1.0,0,0};//, /*s->default_pos[0], s->default_pos[1], s->default_pos[2]}*/, b_pos[3], h_fixed_pos[3];
        double head_to_body[12];
        if (!bot_frames_get_trans_mat_3x4 (s->frames, "HEAD",
                                           "body", 
                                           head_to_body)) {
            fprintf (stderr, "Error getting bot_frames transformation from KINECT to body!\n");
            return TRUE;        
        }
        
        bot_vector_affine_transform_3x4_3d (head_to_body, h_pos, b_pos); 

        /*if (!bot_frames_get_trans_mat_3x4 (s->frames, "body",
          "HEAD_BASE", 
          body_to_head)) {
          fprintf (stderr, "Error getting bot_frames transformation from KINECT to body!\n");
          return TRUE;        
          }
        
          bot_vector_affine_transform_3x4_3d (body_to_head, b_pos, h_fixed_pos); 
        
          double difference = hypot(h_pos[0] - h_fixed_pos[0], h_pos[1] - h_fixed_pos[1]);*/
        
        double goal_theta = atan2(s->default_pos[1], s->default_pos[0]);
        double current_theta = atan2(b_pos[1], b_pos[0]);
        double difference = fabs(current_theta - goal_theta);
        if(s->verbose)
            fprintf(stderr, "Differnce : %f Goal : %f Current : %f\n", difference, bot_to_degrees(goal_theta), bot_to_degrees(current_theta) );
        
        //we are at the correct angle 
        if(difference < bot_to_radians(10.0)){
            if(s->verbose)
                fprintf(stderr, "At correct heading - restarting tracking\n");
            //tell the laser and kinect to reset 
            erlcm_person_tracking_cmd_t t_msg;
            t_msg.utime = bot_timestamp_now();
            t_msg.command = ERLCM_PERSON_TRACKING_CMD_T_CMD_START_TRACKING;
            t_msg.sender = ERLCM_PERSON_TRACKING_CMD_T_PERSON_MODERATOR;
            erlcm_person_tracking_cmd_t_publish(s->lcm, "PERSON_TRACKING_CMD", &t_msg);
            
            s->is_reset = 0;
            return TRUE;
        }
        else{
            if(s->verbose)
                fprintf(stderr, "Head is still to far away\n");
            //return TRUE;
            erlcm_roi_t msg; 
            memset(&msg, 0, sizeof(erlcm_roi_t)); 
            msg.utime = bot_timestamp_now();
            msg.foviation_type = ERLCM_ROI_T_MOVE_HORIZONTAL; 
            msg.pos[0] = s->default_pos[0];
            msg.pos[1] = s->default_pos[1];
            msg.pos[2] = s->default_pos[2];
            msg.frame = "body";
            
            erlcm_roi_t_publish(s->lcm, "ROI", &msg);
            s->is_reset = 1;
            return TRUE;
        }
    }

    if(s->verbose)
        fprintf(stderr, "Doing check \n");
    
    bot_lcmgl_t *lcmgl = NULL;
    if(s->drawing == 1)
        lcmgl = bot_lcmgl_init(s->lcm, "PERSON_MODERATOR");

    //check if we have recent data
    int64_t curr_time = bot_timestamp_now();
    

    if(s->kinect_track && s->laser_track){
        //we assume that things are in the body frame
        double kinect_to_body[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                      "body", s->kinect_track->utime,
                                                      kinect_to_body)) {
            fprintf (stderr, "Error getting bot_frames transformation from KINECT to body!\n");
            return TRUE;        
        }

        double laser_to_body[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                      "body", s->laser_track->utime,
                                                      laser_to_body)) {
            fprintf (stderr, "Error getting bot_frames transformation from SKIRT FRONT to body!\n");
            return TRUE;        
        }
        //check if they both agree
        if(s->verbose){
            fprintf(stderr,"Checking both\n");
        }
        if(s->kinect_track->status == ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_TRACKING && 
           (fabs(curr_time - s->kinect_track->utime)/1.0e6 < 0.5)){            
            s->last_valid_kinect = s->kinect_track->utime; 
            have_k_track = 1;
            //put this in sensor frame - and then convert to body frame 
                
            double xyz_k[3] = {s->kinect_track->x, s->kinect_track->y, 0};
                
            bot_vector_affine_transform_3x4_3d (kinect_to_body, xyz_k, xyz_k_body); 
            
            if(s->verbose)
                fprintf(stderr,"Sensor Pos: %f,%f - Body : %f,%f\n", 
                        xyz_k[0], xyz_k[1], xyz_k_body[0], xyz_k_body[1]); 
            
            if(lcmgl){                    
                double pos[3];
                bot_vector_affine_transform_3x4_3d (body_to_local, xyz_k_body, pos); 
                lcmglLineWidth (5);
                lcmglColor3f(1.0, .0, 0.0);
                lcmglCircle(pos, 0.2);
            }            
        }

        if(s->laser_track->status == ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_TRACKING &&
           (fabs(curr_time - s->laser_track->utime)/1.0e6 < 0.5)){
            s->last_valid_laser = s->laser_track->utime; 
            have_l_track = 1;
            //put this in sensor frame - and then convert to body frame 
                
            double xyz_l[3] = {s->laser_track->x, s->laser_track->y, 0};

            bot_vector_affine_transform_3x4_3d (laser_to_body, xyz_l, xyz_l_body); 
                
            if(s->verbose)
                fprintf(stderr,"Sensor Pos: %f,%f - Body : %f,%f\n", 
                        xyz_l[0], xyz_l[1], xyz_l_body[0], xyz_l_body[1]); 
            
            if(lcmgl){                    
                double pos[3];
                bot_vector_affine_transform_3x4_3d (body_to_local, xyz_l_body, pos); 
                  
                lcmglLineWidth (5);
                lcmglColor3f(0.0, 1.0, 0.0);
                lcmglCircle(pos, 0.2);
            }
        }
    }

    else if(s->kinect_track){
        double kinect_to_body[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                      "body", s->kinect_track->utime,
                                                      kinect_to_body)) {
            fprintf (stderr, "Error getting bot_frames transformation from KINECT to body!\n");
            return TRUE;        
        }
        if(s->verbose){
            fprintf(stderr,"Checking kinect\n");
        }

        if(s->kinect_track->status == ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_TRACKING &&
           (fabs(curr_time - s->kinect_track->utime)/1.0e6 < 0.5)){           

            s->last_valid_kinect = s->kinect_track->utime; 
            have_k_track = 1;
            //put this in sensor frame - and then convert to body frame 
                
            double xyz_k[3] = {s->kinect_track->x, s->kinect_track->y, 0};
            bot_vector_affine_transform_3x4_3d (kinect_to_body, xyz_k, xyz_k_body); 
                
            if(s->verbose)
                fprintf(stderr,"Sensor Pos: %f,%f - Body : %f,%f\n", 
                    xyz_k[0], xyz_k[1], xyz_k_body[0], xyz_k_body[1]); 
            
            if(lcmgl){                    
                double pos[3];
                bot_vector_affine_transform_3x4_3d (body_to_local, xyz_k_body, pos); 
                  
                lcmglLineWidth (5);
                lcmglColor3f(1.0, 0.0, 0.0);
                lcmglCircle(pos, 0.2);
                                    
            }            
        }        
    }
    else if(s->laser_track){
        double laser_to_body[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                      "body", s->laser_track->utime,
                                                      laser_to_body)) {
            fprintf (stderr, "Error getting bot_frames transformation from SKIRT FRONT to body!\n");
            return TRUE;        
        }
        
        if(s->verbose){
            fprintf(stderr,"Checking laser\n");
        }
        if(s->laser_track->status == ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_TRACKING &&
           (fabs(curr_time - s->laser_track->utime)/1.0e6 < 0.5)){           
            s->last_valid_laser = s->laser_track->utime; 
            have_l_track = 1;
            //put this in sensor frame - and then convert to body frame 
                
            double xyz_l[3] = {s->laser_track->x, s->laser_track->y, 0};
            bot_vector_affine_transform_3x4_3d (laser_to_body, xyz_l, xyz_l_body); 
            if(s->verbose)
                fprintf(stderr,"Sensor Pos: %f,%f - Body : %f,%f\n", 
                        xyz_l[0], xyz_l[1], xyz_l_body[0], xyz_l_body[1]); 
                
            if(lcmgl){                    
                double pos[3];
                bot_vector_affine_transform_3x4_3d (body_to_local, xyz_l_body, pos); 
                  
                lcmglLineWidth (5);
                lcmglColor3f(0.0, 1.0, 0.0);
                lcmglCircle(pos, 0.2);
            }
        }        
    }
    else{
        if(s->verbose){
            fprintf(stderr,"No message\n");
        }
    }

    s->have_kinect = have_k_track;
    s->have_laser = have_l_track;

    //add the logic 
    check_and_turn(s, xyz_k_body, xyz_l_body, have_k_track, have_l_track);


    if(lcmgl){  
        bot_lcmgl_switch_buffer (lcmgl);
    }
    //return true to keep running
    return TRUE;
}



void subscribe_to_channels(state_t *s)
{
    erlcm_kinect_person_tracker_status_t_subscribe(s->lcm, "LASER_PERSON_STATUS_SERVO", on_laser_person, s);  
    erlcm_kinect_person_tracker_status_t_subscribe(s->lcm, "KINECT_PERSON_STATUS", on_kinect_person, s);  
}  


static void usage(char * funcName)
{
    printf("Usage: %s [options]\n"
	   "options are:\n"
	   "--help,        -h    display this message \n"
	   "--publish_all, -a    publish robot laser messages for all channels \n"
	   "--verbose,     -v    be verbose", funcName);
    exit(1);

}

//for testing 
void init_kinect_and_laser_msg(state_t *state)
{
    state->laser_track = (erlcm_kinect_person_tracker_status_t *) calloc(1, sizeof(erlcm_kinect_person_tracker_status_t));//NULL;
        
    state->laser_track->utime = bot_timestamp_now();

    state->laser_track->status = ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_TRACKING; 
    
    state->laser_track->x = 1.0;
    state->laser_track->y = 0.5; 

    state->kinect_track = (erlcm_kinect_person_tracker_status_t *) calloc(1, sizeof(erlcm_kinect_person_tracker_status_t));//NULL;
        
    state->kinect_track->utime = bot_timestamp_now();

    state->kinect_track->status = ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_TRACKING; 
    
    state->kinect_track->x = 1.1;
    state->kinect_track->y = 0.5; 
}

int 
main(int argc, char **argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));
    state->laser_track = NULL;
    state->kinect_track = NULL;

    const char *optstring = "havtd";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "publish_all", no_argument, 0, 'a' }, 
                                  { "draw", no_argument, 0, 'd' }, 
				  { "verbose", no_argument, 0, 'v' }, 
                                  { "test", no_argument, 0, 't' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    usage(argv[0]);
	    break;
	case 'a':
	    {
		fprintf(stderr,"Publishing all laser channels\n");
		break;
	    }
        case 'd':
	    {
		fprintf(stderr,"Drawing\n");
                state->drawing = 1;
		break;
	    }
        case 't':
	    {
		fprintf(stderr,"Testing\n");
                init_kinect_and_laser_msg(state);
		break;
	    }
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
		state->verbose = 1;
		break;
	    }
	default:
	    {
		usage(argv[0]);
		break;
	    }
	}
    }

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
    state->b_server = bot_param_new_from_server(state->lcm, 1); 

    state->default_pos[0] = 1.0;//in body frame
    state->default_pos[1] = .0;//in body frame
    state->default_pos[2] = .0;//in body frame

    state->frames = bot_frames_get_global (state->lcm, state->b_server);

    //pthread_create(&state->work_thread, NULL, track_work_thread, state);

    subscribe_to_channels(state);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    read_parameters_from_conf(state);

    /* heart beat*/
    g_timeout_add (25, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


