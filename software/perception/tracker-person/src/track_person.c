#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <bot_frames/bot_frames.h>
#include <bot_lcmgl_client/lcmgl.h>

#include <gsl/gsl_fit.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_blas.h>

#ifdef __APPLE
#include <GL/gl.h>
#else
#include <GL/gl.h>
#endif

#define POSE_LIST_SIZE 10
#define NO_PARTICLES 100

typedef struct
{
    double xy[2]; 
} xy_t; 

typedef struct
{    
    xy_t center; 
    int start_ind; 
    int end_ind; 
    int is_front_laser;
    int close_filter_ind; 
    double filter_dist; 
    int allocated; 
} segment_t; 

typedef struct
{ 
    int64_t utime; 
    int list_size; 
    int size; 
    segment_t *segment; 
} segment_list_t;

typedef struct
{
    xy_t pos; 
    double weight; 
} person_track_particle_t; 

typedef struct
{
    int64_t last_utime; 
    int no_particles; 
    xy_t s_pos; 
    //this can be an aray if we dont plan to play with it 
    person_track_particle_t particle[NO_PARTICLES]; 
    int obs_ind; 
    double obs_dist; 
    int stale; 
} person_track_t; 

typedef struct
{
    int no_tracks;  //active tracks
    int size;
    person_track_t *tracks; 
} person_track_list_t;


void init_segment_list(segment_list_t *list, int size){
    list->size = 0;
    list->list_size = size;
    list->segment = (segment_t *) calloc(size, sizeof(segment_t));
}

void realloc_segment_list(segment_list_t *list, int size){
    list->list_size = size;
    list->segment = (segment_t *) realloc(list->segment, size*sizeof(segment_t));
}

void free_segment_list(segment_list_t *list){
    list->size = 0;
    list->list_size = 0;
    free(list->segment);
}


typedef struct
{
    BotParam   *b_server;
    BotFrames  *frames; 
    lcm_t      *lcm;
    GMainLoop *mainloop;
    pthread_t  work_thread;

    GList *pose_list;     

    bot_core_pose_t *last_pose;
    person_track_list_t *tracks; 
    int publish_all;
    int verbose; 
} state_t;

void init_track_list_t(person_track_list_t *list, int size){
    list->size = size; 
    list->no_tracks = 0;
    list->tracks = (person_track_t *) calloc(size, sizeof(person_track_t));
}

void realloc_track_list_t(person_track_list_t *list, int size){
    list->size = size; 
    list->tracks = (person_track_t *) realloc(list->tracks, size *sizeof(person_track_t));
}

void increase_track_list_t(person_track_list_t *list, int size){
    realloc_track_list_t(list, list->size + size);
}


void update_filters(segment_list_t *segments, state_t *s){
    //search through each filter - and find a close enough observation 
    //if there is one - update the filter based on the observation model 
    //otherwise create a new filter 
    //also kill filters and updates etc
    person_track_list_t *t  = s->tracks;

    for(int i=0; i < t->no_tracks; i++){
        //reset the observations
        t->tracks[i].obs_dist = 1000000;
        t->tracks[i].obs_ind = -1;
    }

    for(int i=0; i < t->no_tracks; i++){
        xy_t *f_pos = &(t->tracks[i].s_pos);
        
        int c_ind = -1; 
        double max_dist = 0.6;
        double close_dist = max_dist; 
        for(int j=0; j < segments->size; j++){
            double dist = hypot(f_pos->xy[0] - segments->segment[j].center.xy[0], 
                                f_pos->xy[1] - segments->segment[j].center.xy[1]);

            if(dist < close_dist){
                close_dist = dist; 
                c_ind = j; 
            }

            if(dist < segments->segment[j].filter_dist){
                segments->segment[j].filter_dist = dist;
                segments->segment[j].close_filter_ind = i;
            }
        }
        t->tracks[i].obs_ind = c_ind; 
        if(c_ind >=0){
            t->tracks[i].obs_dist = close_dist; 
        }
        else{
            t->tracks[i].obs_dist = 100000; 
        }
    }


    for(int i=0; i < t->no_tracks; i++){
        fprintf(stderr, "Track : %d => Close Observation : %d\n", i, t->tracks[i].obs_ind);
    }

    for(int j=0; j < segments->size; j++){
        fprintf(stderr, "Obs : %d => Close Track : %d\n", j, segments->segment[j].close_filter_ind);
    }

    //for filters that found observations that have it as the closest - update filter pos 
    //for now - we just update the summary location - and not worry about the particles 
    for(int i=0; i < t->no_tracks; i++){
        if(t->tracks[i].obs_ind >=0){
            //might make sense to have a list - and go through ??
            if(segments->segment[t->tracks[i].obs_ind].close_filter_ind == i){
                fprintf(stderr,"[%d] => Found close observation : %d\n", i, t->tracks[i].obs_ind);
                t->tracks[i].last_utime = segments->utime;
                //update filter position
                t->tracks[i].s_pos.xy[0] = segments->segment[t->tracks[i].obs_ind].center.xy[0];
                t->tracks[i].s_pos.xy[1] = segments->segment[t->tracks[i].obs_ind].center.xy[1];
                segments->segment[t->tracks[i].obs_ind].allocated = 1;
            }
            else{
                fprintf(stderr, "[%d] => Close obs does not have us as closest :(\n", i);  

                //check if the filter is stale - if so re-order the filters
            }
        }
        else{
            fprintf(stderr, "[%d] => No close observation :(\n", i); 
        }
    }

    //now search through the segments
    for(int j=0; j < segments->size; j++){
        if(!segments->segment[j].allocated){
            //need to update the distance - since we might have just create a new filter
            double c_dist = 10000; 
            for(int i=0; i < t->no_tracks; i++){
                xy_t *f_pos = &(t->tracks[i].s_pos);
        
                double dist = hypot(f_pos->xy[0] - segments->segment[j].center.xy[0], 
                                    f_pos->xy[1] - segments->segment[j].center.xy[1]);
                
                if(dist < c_dist){
                    c_dist = dist; 
                }
            }

            if(c_dist > 0.7){
                //create a new filter
                if(t->size < t->no_tracks + 1){
                    increase_track_list_t(t, 5);
                }
                t->tracks[t->no_tracks].s_pos.xy[0] = segments->segment[j].center.xy[0];
                t->tracks[t->no_tracks].s_pos.xy[1] = segments->segment[j].center.xy[1]; 
                t->tracks[t->no_tracks].last_utime = segments->utime;
                t->tracks[t->no_tracks].stale = 0;
                fprintf(stderr,"Creating new track => Dist %f\n", c_dist); 
                t->no_tracks++;
            }
        }
    }


    fprintf(stderr,"-----------------------------------------------\n");
    int valid_count = 0;
    for(int i=0; i < t->no_tracks; i++){      
        fprintf(stderr,"[%d] -> time since : %f\n", i, (segments->utime - t->tracks[i].last_utime) / 1.0e6);
        
        if((segments->utime - t->tracks[i].last_utime) / 1.0e6 > 0.5||
           (segments->utime - t->tracks[i].last_utime) / 1.0e6 < 0){ //if the filter hasnt been updated 
            //for 30 seconds - kill it 
            //copy over the chunk at the back 
            memcpy(&t->tracks[i], &t->tracks[i+1], (t->no_tracks - i-1) *sizeof(person_track_t));
            fprintf(stderr,"Killing filter\n");
            t->no_tracks--;
            i--; //we need to go back an index
        }
        else{
            valid_count++;
        }
    }

    bot_lcmgl_t *lcmgl = bot_lcmgl_init(s->lcm, "new_person_tracks");

    double body_to_local[12];
    if (bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                 "local", segments->utime,
                                                 body_to_local)) {
        lcmglLineWidth (3);     
        lcmglColor3f(1.0, 0.0, 0);
        for(int i=0; i < t->no_tracks; i++){    
            double b_pos[3] =  {t->tracks[i].s_pos.xy[0], t->tracks[i].s_pos.xy[1],0};
            double l_pos[3] = {.0,.0,.0};
            bot_vector_affine_transform_3x4_3d (body_to_local, b_pos, l_pos);  
            lcmglCircle(l_pos, 0.2);
        }
    }
    bot_lcmgl_switch_buffer (lcmgl);
   
}

segment_list_t * segment_laser(bot_core_planar_lidar_t *laser, state_t *s){
    double sensor_to_body[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "SKIRT_FRONT",
                                                  "body", laser->utime,
                                                  sensor_to_body)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return NULL;        
    }


    double body_to_local[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                  "local", laser->utime,
                                                  body_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return NULL;        
    }

    fprintf(stderr,"\n");
  
    int last_h_l_ind = -1;

    xy_t *local_pos = (xy_t *) calloc(laser->nranges, sizeof(xy_t));

    for(int i=0; i < laser->nranges; i++){
        double theta = laser->rad0 + 
                (double)i * laser->radstep;

        double pos[3] = {.0,.0,.0};
        pos[0] = laser->ranges[i]*cos(theta); 
        pos[1] = laser->ranges[i]*sin(theta); 

        double l_pos[3] = {.0,.0,.0};
        bot_vector_affine_transform_3x4_3d (sensor_to_body, pos, l_pos);  
        local_pos[i].xy[0] = l_pos[0];
        local_pos[i].xy[1] = l_pos[1];
    }

    double invalid_range = 30.0; 

    bot_lcmgl_t *lcmgl = bot_lcmgl_init(s->lcm, "new_person_segments");

    lcmglLineWidth (3);     

    int no_segments = 0;

    segment_list_t *list =  (segment_list_t *) calloc(1, sizeof(segment_list_t)); 

    init_segment_list(list, 5);
    list->utime = laser->utime;
    
    fprintf(stderr,"------------------------------\n");
    
    for(int i=1; i < laser->nranges; i++){
        if(laser->ranges[i] < 0.1 || laser->ranges[i] > invalid_range)
            continue;
        
        //***********This doesnt clean the invalid returns 


        //this method is good to use for the person tracking also - port 
        if((laser->ranges[i] - laser->ranges[i-1]) > 0.3){
            //fprintf(stderr,"Low to High : %f\n", laser->ranges[i] - laser->ranges[i-1]);
            if(last_h_l_ind != -1){
              
                double avg_dist = 0; 
                double size = 0;
                 
                //do some additional filtering here
                int found_close = 0;
                int lower_ind = last_h_l_ind; 
                int higher_ind = i-1;

                //*********** instead of this 

                //****** use a cleaning from the front and the back - and drop the first set of weird points

                for(int j= last_h_l_ind + 1; j < i; j++){
                    //fprintf(stderr, "\t %f\n", fabs(laser->ranges[j] - laser->ranges[j-1]));
                    if(fabs((laser->ranges[j] - laser->ranges[j-1])) > 0.05){ //laser issues - should prob discard 
                        lower_ind = j; 
                    }
                    else{
                        fprintf(stderr,"Found low stop\n");
                        break;
                    }
                }

                for(int j= i- 1; j >= lower_ind; j--){
                    //fprintf(stderr, "\t %f\n", fabs(laser->ranges[j] - laser->ranges[j-1]));
                    if(fabs((laser->ranges[j+1] - laser->ranges[j])) > 0.05){ //laser issues - should prob discard 
                        higher_ind = j; 
                    }
                    else{
                        fprintf(stderr,"Found high stop\n");
                        break;
                    }
                }


                /*for(int j= last_h_l_ind + 1; j < i; j++){
                    //fprintf(stderr, "\t %f\n", fabs(laser->ranges[j] - laser->ranges[j-1]));
                    if(fabs((laser->ranges[j] - laser->ranges[j-1])) > 0.05){ //laser issues - should prob discard 
                        if(found_close){
                            higher_ind = j-1; 
                            break;
                        }                        
                    }
                    else if(!found_close){
                        lower_ind = j-1; 
                        found_close = 1; 
                    }
                    }*/

                size = hypot(local_pos[higher_ind].xy[0] - local_pos[lower_ind].xy[0], 
                      local_pos[higher_ind].xy[1] - local_pos[lower_ind].xy[1]);
                //fprintf(stderr," Size : %f Dist : %f \n", size, avg_dist);

                fprintf(stderr, "%d-%d => %d-%d\n", last_h_l_ind, i-1, lower_ind, higher_ind); 

                //radius seems to be .105 m 

                if(higher_ind - lower_ind < 2){
                    last_h_l_ind = -1;
                    continue;
                }

                xy_t mean;
                
                mean.xy[0] = 0;
                mean.xy[1] = 0;
                
                for(int j= lower_ind; j <= higher_ind; j++){
                    mean.xy[0] += local_pos[j].xy[0];
                    mean.xy[1] += local_pos[j].xy[1];
                    avg_dist += laser->ranges[j];
                }
                
                int data_size = higher_ind - lower_ind +1; 

                if(lcmgl){
                    lcmglColor3f(0.0, 1.0, 1.0);
                    lcmglBegin(GL_LINES);
                }
                    
                for(int j= lower_ind; j <= higher_ind; j++){
                    if(lcmgl){
                        double b_pos[3] = {local_pos[j].xy[0], local_pos[j].xy[1],0};  
                        double l_pos[3] = {.0,.0,.0};
                        bot_vector_affine_transform_3x4_3d (body_to_local, b_pos, l_pos);  
                        lcmglVertex3d(l_pos[0], l_pos[1], l_pos[2]);
                    }
                }

                if(lcmgl){
                    lcmglEnd();
                }
                
                
                mean.xy[0] /= data_size;
                mean.xy[1] /= data_size;

                avg_dist /= (higher_ind - lower_ind +1);
                
                double dist_from_body = hypot(mean.xy[0], mean.xy[1]);
                
                if(size < 0.6  && size > 0.1 && dist_from_body < 5.0 && dist_from_body > 0.6 
                   && (higher_ind - lower_ind +1)>=10 ){

                    fprintf(stderr, "Possible pole Found : %d - %d\n", last_h_l_ind, i-1);

                    if(lcmgl){
                        lcmglColor3f(0.0, 1.0, 0);
                        lcmglBegin(GL_LINES);
                        }
                    
                    for(int j= lower_ind; j <= higher_ind; j++){
                        if(lcmgl){
                            double b_pos[3] = {local_pos[j].xy[0], local_pos[j].xy[1],0};  
                            double l_pos[3] = {.0,.0,.0};
                            bot_vector_affine_transform_3x4_3d (body_to_local, b_pos, l_pos);  
                            lcmglVertex3d(l_pos[0], l_pos[1], l_pos[2]);
                        }
                    }

                    if(lcmgl){
                        lcmglEnd();
                        
                    
                    if(list->size+1 == list->list_size){
                        fprintf(stderr,"Reallocing\n"); 
                        realloc_segment_list(list, list->list_size + 5);  
                    }

                    list->segment[no_segments].center.xy[0] =  mean.xy[0]; 
                    list->segment[no_segments].center.xy[1] =  mean.xy[1]; 
                    list->segment[no_segments].start_ind = lower_ind;
                    list->segment[no_segments].end_ind = higher_ind;
                    list->segment[no_segments].is_front_laser = 1; 
                    list->segment[no_segments].close_filter_ind = -1; 
                    list->segment[no_segments].filter_dist = 100000; 
                    list->segment[no_segments].allocated = 0;
                    no_segments++;
                    list->size = no_segments;
                    
                        //double c_center[3] = {x_c, y_c,0};
                        //lcmglCircle(c_center, radius);
                        
                        char seg_info[512];
                        sprintf(seg_info,"size : %f \n", size);
                        double pos[3] = {local_pos[lower_ind].xy[0], local_pos[lower_ind].xy[1],0};
                        bot_lcmgl_text(lcmgl, pos, seg_info);
                    }
                }
            }                
            last_h_l_ind = -1;
        }

        else if((laser->ranges[i] - laser->ranges[i-1]) < -0.3){
            last_h_l_ind = i;
        }
    }

    if(lcmgl)
        bot_lcmgl_switch_buffer (lcmgl);
    
    free(local_pos);

    return list;
}



static void on_laser(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
			     const bot_core_planar_lidar_t * msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 

    fprintf(stderr,".");

    bot_core_planar_lidar_t *laser = bot_core_planar_lidar_t_copy(msg);

    segment_list_t *list = segment_laser(laser, s);

    
    update_filters(list, s);

    free_segment_list(list);
    free(list);
  
    bot_core_planar_lidar_t_destroy(laser);
}

static void on_pose(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
			     const bot_core_pose_t * msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 
    if(s->last_pose !=NULL){
	bot_core_pose_t_destroy(s->last_pose);
    }
    s->last_pose = bot_core_pose_t_copy(msg);
    if(s->verbose){
	fprintf(stderr,"Pose : (%f,%f)\n",msg->pos[0], msg->pos[1]);//, rpy[2], vel[0] , vel[1]);
    }

    bot_core_pose_t *pose = bot_core_pose_t_copy(msg);

    fprintf(stderr, "Size of List (at start): %d\n", g_list_length(s->pose_list));

    if(g_list_length(s->pose_list) < POSE_LIST_SIZE){
	fprintf(stderr,"Adding\n");
	s->pose_list = g_list_prepend (s->pose_list, (pose));
    }
    else{
	fprintf(stderr, "Removing and Inserting\n");
	GList* last = g_list_last (s->pose_list);
	bot_core_pose_t *last_pose = (bot_core_pose_t *) last->data;
	fprintf(stderr,"Pose : %.3f (%f,%f)\n",last_pose->utime / 1000000.0 , last_pose->pos[0], last_pose->pos[1]);
	
	//	s->pose_list = g_list_remove_link (s->pose_list, last);	
	bot_core_pose_t_destroy((bot_core_pose_t *) last->data);
	s->pose_list = g_list_delete_link (s->pose_list, last);	

	
	fprintf(stderr, "Size of List (After remove): %d\n", g_list_length(s->pose_list));
	s->pose_list = g_list_prepend (s->pose_list , (gpointer) (pose));
    }
    
    fprintf(stderr, "Size of List (at end): %d\n", g_list_length(s->pose_list));
}

//pthread
static void *track_work_thread(void *user)
{
    state_t *s = (state_t*) user;

    printf("obstacles: track_work_thread()\n");

    while(1){
	fprintf(stderr, " T - called ()\n");
	
	usleep(50000);
    }
}

void read_parameters_from_conf(state_t *s)
{
    BotParam *b_param = s->b_server; 
    double max_t_vel =  bot_param_get_double_or_fail(b_param, "robot.max_t_vel");
    double max_r_vel = bot_param_get_double_or_fail(b_param, "robot.max_r_vel");
  
    char **planar_lidar_names = bot_param_get_all_planar_lidar_names(b_param);
  
    if(planar_lidar_names) {
	for (int pind = 0; planar_lidar_names[pind] != NULL; pind++) {
	    fprintf(stderr, "Channel : %s\n", planar_lidar_names[pind]);
	}
    }
  
    g_strfreev(planar_lidar_names);
}

//doesnt do anything right now - timeout function
gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;
  
    //do the periodic stuff 
    if(s->verbose){
	fprintf(stderr, "Callback - Timeout\n");
    }
    //return true to keep running
    return TRUE;
}

void subscribe_to_channels(state_t *s)
{
    //bot_core_pose_t_subscribe(s->lcm, "POSE", on_pose ,s);
    bot_core_planar_lidar_t_subscribe(s->lcm, "SKIRT_FRONT", on_laser, s); 
}  


static void usage(char * funcName)
{
    printf("Usage: %s [options]\n"
	   "options are:\n"
	   "--help,        -h    display this message \n"
	   "--publish_all, -a    publish robot laser messages for all channels \n"
	   "--verbose,     -v    be verbose", funcName);
    exit(1);

}

int 
main(int argc, char **argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));
    state->last_pose = NULL;
    state->pose_list = NULL;

    const char *optstring = "hav";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "publish_all", no_argument, 0, 'a' }, 
				  { "verbose", no_argument, 0, 'v' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    usage(argv[0]);
	    break;
	case 'a':
	    {
		fprintf(stderr,"Publishing all laser channels\n");
		break;
	    }
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
		state->verbose = 1;
		break;
	    }
	default:
	    {
		usage(argv[0]);
		break;
	    }
	}
    }

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
    state->b_server = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->b_server);

    //pthread_create(&state->work_thread, NULL, track_work_thread, state);

    subscribe_to_channels(state);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );

    state->tracks = calloc(1, sizeof(person_track_list_t));
    init_track_list_t(state->tracks, 5); 
  
    if (!state->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    read_parameters_from_conf(state);

    /* heart beat*/
    //    g_timeout_add_seconds (1, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


