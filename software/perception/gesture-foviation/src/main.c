#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <bot_frames/bot_frames.h>

#define POSE_LIST_SIZE 10

typedef struct
{
    BotParam   *param;
    BotFrames *bcf;
    lcm_t      *lcm;
    GMainLoop *mainloop;
    erlcm_roi_t *roi_msg; 

    double body_to_head_trans[3];

    double last_h_angle; 
    double last_v_angle; 

    int verbose; 
} state_t;

void command_head(state_t *self, double pos[3], char *frame, int type){
    //check the roi - and convert that to a heading 
    //and check the frame for the head 

    if(type == 0){
        fprintf(stderr,"No motion type specified\n");
        return;
    }

    double local_to_head[12];
    if (!bot_frames_get_trans_mat_3x4 (self->bcf, frame,
                                       "HEAD_BASE", 
                                       local_to_head)) {
        fprintf (stderr, "Error getting bot_frames transformation from local to head!\n");
        return;        
    }
    
    //BotTrans body_to_head;
    //bot_frames_get_trans (self->bcf, "local", "head", &body_to_head);
    
     
    double head_xyz[3];
    bot_vector_affine_transform_3x4_3d (local_to_head, pos, head_xyz);   
    
    if(self->verbose){
           fprintf(stderr,"Frame : %s Pos : %f,%f,%f Pos in Head co-ord : %f, %f, %f\n", frame, pos[0], pos[1], pos[2], head_xyz[0], head_xyz[1], head_xyz[2]); 
    }
    //convert to angles 
    //head command expects degrees for both axes

    //might need to check if the head is turning or not also 

    
    if(fabs(head_xyz[0]) != 0 &&  hypot(head_xyz[0], head_xyz[1]) != 0){

        //do some dampening
        
        double h_angle = bot_to_degrees(atan2(head_xyz[1], head_xyz[0])); 

        double r = hypot(head_xyz[0], head_xyz[1]);

        double v_angle = bot_to_degrees(atan2(head_xyz[2], r)); 
        if(self->verbose){
            //fprintf(stderr, "Horizontal : %f Vertical : %f\n", h_angle, 
            //      v_angle);
        }
       
        int publish = 0;

        double angle_tollerance = 2.0; //5.0

        fprintf(stderr,"Difference in angle : %f\n", fabs(self->last_h_angle - h_angle));
        
        //only publish if angle change greater than 5 degrees
        if(type == 1){// && fabs(self->last_h_angle - h_angle) > angle_tollerance){
            publish = 1;
        }

        if(type == 2){// && fabs(self->last_v_angle - v_angle) > angle_tollerance){
            publish = 1;
        }
        if(type == 3){// && ((fabs(self->last_h_angle - h_angle) > angle_tollerance)|| fabs(self->last_v_angle - v_angle) > angle_tollerance)){
            publish = 1;
        }

        if(self->verbose && publish)
            fprintf(stderr,"Publish : %d Angle change : H : %f V : %f\n", 
                    publish, 
                    fabs(self->last_h_angle - h_angle), 
                    fabs(self->last_v_angle - v_angle));
        

        if(publish){
            erlcm_servo_position_list_t msg; 
            msg.utime = bot_timestamp_now();
            msg.dof = 2; 
            if(type == 1){
                msg.command_type =  ERLCM_SERVO_POSITION_LIST_T_USE_ONE;
            }
            if(type == 2){
                msg.command_type =  ERLCM_SERVO_POSITION_LIST_T_USE_TWO;
            }
            if(type == 3){
                msg.command_type = ERLCM_SERVO_POSITION_LIST_T_USE_ONE_TWO; 
            }
            msg.values = calloc(2, sizeof(double));
            msg.values[0] = h_angle; 
            msg.values[1] = v_angle; 
            erlcm_servo_position_list_t_publish(self->lcm, "HEAD_POSITION", &msg); 
            free(msg.values);
            
            self->last_h_angle = h_angle;
            self->last_v_angle = v_angle;
        }
       

    }
}    


static void on_roi(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                   const erlcm_roi_t * msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 
    if(s->roi_msg !=NULL){
	erlcm_roi_t_destroy(s->roi_msg);
    }
    s->roi_msg = erlcm_roi_t_copy(msg);
    if(s->verbose){
	//fprintf(stderr,"ROI : %f,%f,%f\n", s->roi_msg->pos[0], s->roi_msg->pos[1], s->roi_msg->pos[2]);
    }

    if(s->roi_msg->frame == ERLCM_ROI_T_FRAME_LOCAL){
        command_head(s, s->roi_msg->pos, "local" , msg->foviation_type);              
    }
    else if(s->roi_msg->frame == ERLCM_ROI_T_FRAME_BODY){
        command_head(s, s->roi_msg->pos, "body" , msg->foviation_type);              
    }
    else{
        fprintf(stderr,"No frame specified - assuming local frame\n");
        command_head(s, s->roi_msg->pos, "local" , msg->foviation_type);  
    }
}

void read_parameters_from_conf(state_t *s)
{
    BotParam *b_param = s->param; 
    /*    double max_t_vel =  bot_param_get_double_or_fail(b_param, "robot.max_t_vel");
    double max_r_vel = bot_param_get_double_or_fail(b_param, "robot.max_r_vel");
  
    char **planar_lidar_names = bot_param_get_all_planar_lidar_names(b_param);
  
    if(planar_lidar_names) {
	for (int pind = 0; planar_lidar_names[pind] != NULL; pind++) {
	    fprintf(stderr, "Channel : %s\n", planar_lidar_names[pind]);
	}
    }
  
    g_strfreev(planar_lidar_names);*/
}

//doesnt do anything right now - timeout function
gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;
  
    //do the periodic stuff 
    if(s->verbose){
	fprintf(stderr, "Callback - Timeout\n");
    }
    
    //double pos[3] = {1.0, 0.2, 1.48}; 
    //command_head(s, pos);
    //return true to keep running
    return TRUE;
}

void subscribe_to_channels(state_t *s)
{
    erlcm_roi_t_subscribe(s->lcm, "ROI", on_roi ,s);
}  


static void usage(char * funcName)
{
    printf("Usage: %s [options]\n"
	   "options are:\n"
	   "--help,        -h    display this message \n"
	   "--publish_all, -a    publish robot laser messages for all channels \n"
	   "--verbose,     -v    be verbose", funcName);
    exit(1);

}

void frames_update_handler(BotFrames *bot_frames, const char *frame, const char * relative_to, int64_t utime, void *user)
{
    //printf("link  %s->%s was updated, user = %p\n", frame, relative_to, user);
}


int 
main(int argc, char **argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));
    state->roi_msg = NULL;
    const char *optstring = "hav";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "publish_all", no_argument, 0, 'a' }, 
				  { "verbose", no_argument, 0, 'v' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    usage(argv[0]);
	    break;
	case 'a':
	    {
		fprintf(stderr,"Publishing all laser channels\n");
		break;
	    }
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
		state->verbose = 1;
		break;
	    }
	default:
	    {
		usage(argv[0]);
		break;
	    }
	}
    }

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
    state->param = bot_param_new_from_server(state->lcm, 1);

    state->bcf = bot_frames_get_global(state->lcm, state->param);
    bot_frames_add_update_subscriber(state->bcf,frames_update_handler,NULL);

    // Get the fixed translation from body to head
    BotTrans body_to_head;
    bot_param_get_double_array (state->param, "coordinate_frames.HEAD.initial_transform.translation", &(state->body_to_head_trans[0]), 3);

    subscribe_to_channels(state);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    read_parameters_from_conf(state);

    /* heart beat*/
    //g_timeout_add_seconds (1, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    //double pos[3] = {-1.0, 1.0, 1.5}; 
    //command_head(state, pos, "local", 1);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


