//This script collect laser_annotation_t messages, compute feature values and write to files
//Requires: er-laser-annotation(laser_annotation_t messages)
//Output: A "data.txt" text file contains feature values


#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>
#include <lcmtypes/erlcm_place_classification_class_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>

#include <laser_features/laser_feature.h>

char *s1 = "doorway";
char *s2 = "conference_room";
char *s3 = "office";
char *s4 = "lab";
char *s5 = "open_area";
char *s6 = "hallway";
char *s7 = "elevator";
char *s8 = "corridor";
char *s9 = "large_meeting_room";

typedef struct
{
    lcm_t      *lcm;
    GMainLoop *mainloop;
    erlcm_laser_annotation_t *last_laser_annotation;
    int verbose;
    char *file_name;
    GHashTable *class_count;
    int skip_doorways;
    //FILE *file;
} laser_annotation;


void print_stat(gpointer key,
                  gpointer value,
                  gpointer user_data){
    char *class_name = NULL;
    int class_id = *(int *) key;
    if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_DOORWAY){
        class_name = "Doorway";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_CONFERENCE_ROOM){
        class_name = "Conference Room";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_OFFICE){
        class_name = "Office";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_LAB){
        class_name = "Lab";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_OPEN_AREA){
        class_name = "Open Area";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_HALLWAY){
        class_name = "Hallway";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_ELEVATOR){
        class_name = "Elevator";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_CORRIDOR){
        class_name = "Corridor"; //what is the difference here??
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_LARGE_MEETING_ROOM){
        class_name = "Large Meeting Room";
    }
    else{        
        class_name = "Unknown";
    }
    fprintf(stderr, "Class Name [%d] : %s Count : %d\n", *(int *) key, class_name, *(int *) value);
}

void print_stats(laser_annotation *self){
    //GHashTableIter iter;
    //gpointer key, value;
    
    //g_hash_table_iter_init (&iter, self->class_count);
    g_hash_table_foreach(self->class_count, (GHFunc) print_stat, NULL);
    
}

static void
on_laser_annotation (const lcm_recv_buf_t *rbuf, const char *channel,
             const erlcm_laser_annotation_t *msg, void *user)
{
//    fprintf(stderr, "Receive a log annotation\n");
    FILE *file;
    static int count = 0;
    count++;
    fprintf(stderr, "Count: %d\n", count);
    laser_annotation *self= (laser_annotation *)user;
    file = fopen(self->file_name, "a");
    if(self->last_laser_annotation != NULL){
	erlcm_laser_annotation_t_destroy(self->last_laser_annotation);
    }

    self->last_laser_annotation = erlcm_laser_annotation_t_copy(msg);
    bot_core_planar_lidar_t laser = self->last_laser_annotation->laser;
    char *annotation = self->last_laser_annotation->annotation;
    int label;
    
    
    //this has 6 classes - do we have training data for more classes?

//    fprintf(stderr, "Test annotation: %s", annotation);

    //these label indexes should be read from some header 
    if(!strncmp(annotation, s1, 100)){
        label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_DOORWAY;
        if(self->skip_doorways){
            return;
        }
    }
    else if(!strncmp(annotation, s2, 100)){
	label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_CONFERENCE_ROOM;
    }
    else if(!strncmp(annotation, s3, 100)){
	label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_OFFICE;
    }
    else if(!strncmp(annotation, s4, 100)){
	label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_LAB;
    }
    else if(!strncmp(annotation, s5, 100)){
	label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_OPEN_AREA;
    }
    else if(!strncmp(annotation, s6, 100)){
	label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_HALLWAY;
    }
    else if(!strncmp(annotation, s7, 100)){
	label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_ELEVATOR;
    }
    else if(!strncmp(annotation, s8, 100)){
	label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_CORRIDOR;
    }
    else if(!strncmp(annotation, s9, 100)){
	label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_LARGE_MEETING_ROOM;
    }
    else{
        fprintf(stderr, "Error - Unknown Class : %s\n", annotation);
        return;
    }

    GHashTableIter iter;
    gpointer key, value;
    
    int *class_count = (int *) g_hash_table_lookup(self->class_count, &label);
    if(!class_count){
        class_count = (int *)calloc(1,sizeof(int));
        int *id = (int *)calloc(1,sizeof(int));
        *id = label;
        *class_count = 0;
        g_hash_table_insert(self->class_count, id, class_count);
    }
    else{
        *class_count = *class_count+1;
    }
    

    //Change the feature configurations here. min_range, max_range, threshold, etc...
    feature_config_t config;
    config.min_range = 0.1;
    config.max_range = 30;
    config.cutoff = 5;
    config.gap_threshold_1 = 3;
    config.gap_threshold_2 = 0.5;
    config.deviation = 0.001;
    config.no_of_fourier_coefficient = 2;
    config.no_beam_skip = 3; //no beam skip (2 would skip 1 beam)
    raw_laser_features_t *raw_features = extract_raw_laser_features(&laser, config);

    if(self->verbose){
        print_stats(self);
        fprintf(stderr, "Saving Training data : time : %f => Annotation : %s\n", 
                laser.utime/1.0e6, annotation);
    }
    
    //write the features to file 
    polygonal_laser_features_t *polygonal_features = extract_polygonal_laser_features(&laser, config);
    fprintf(file, "%d %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f ", label, 1, raw_features->avg_difference_consecutive_beams, 2, raw_features->std_dev_difference_consecutive_beams, 3, raw_features->cutoff_avg_difference_consecutive_beams, 4, raw_features->avg_beam_length, 5, raw_features->std_dev_beam_length, 6, raw_features->no_gaps_in_scan_threshold_1, 7, raw_features->no_relative_gaps, 8, raw_features->no_gaps_to_num_beam, 9, raw_features->no_relative_gaps_to_num_beam, 10, raw_features->distance_between_two_smalled_local_minima, 11, raw_features->index_distance_between_two_smalled_local_minima, 12, raw_features->avg_ratio_consecutive_beams, 13, raw_features->avg_to_max_beam_length, 14, polygonal_features->area, 15, polygonal_features->perimeter, 16, polygonal_features->area_to_perimeter, 17, polygonal_features->PI_area_to_sqrt_perimeter, 18, polygonal_features->PI_area_to_perimeter_pow, 19, polygonal_features->sides_of_polygon);
    for(int i = 0; i <= config.no_of_fourier_coefficient * 2; i++){
//	fprintf(stderr, "Testing: %f\n", polygonal_features->fourier_transform_descriptors[1].results->real);
	fprintf(file, "%d:%f %d:%f ", 20 + i * 2, polygonal_features->fourier_transform_descriptors[0].results->real, 21 + i * 2, polygonal_features->fourier_transform_descriptors[0].results->complex);
    }
   int start = config.no_of_fourier_coefficient * 4 + 22;
   fprintf(file, "%d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f\n", start, polygonal_features->major_ellipse_axis_length_coefficient, start + 1, polygonal_features->minor_ellipse_axis_length_coefficient, start + 2, polygonal_features->major_to_minor_coefficient, start + 3, polygonal_features->central_moment_invariants[0], start + 4, polygonal_features->central_moment_invariants[1], start + 5, polygonal_features->central_moment_invariants[2], start + 6, polygonal_features->central_moment_invariants[3], start + 7, polygonal_features->central_moment_invariants[4], start + 8, polygonal_features->central_moment_invariants[5], start + 9, polygonal_features->central_moment_invariants[6], start + 10, polygonal_features->normalized_feature_of_compactness, start + 11, polygonal_features->normalized_feature_of_eccentricity, start + 12, polygonal_features->mean_centroid_to_shape_boundary, start + 13, polygonal_features->max_centroid_to_shape_boundary, start + 14, polygonal_features->mean_centroid_over_max_centroid, start + 15, polygonal_features->std_dev_centroid_to_shape_boundry, start + 16, polygonal_features->kurtosis);
//   fprintf(self->file, "\n");
   fclose(file);
}



void subscribe_to_channels(laser_annotation *self)
{
    erlcm_laser_annotation_t_subscribe(self->lcm, "LASER_ANNOTATION", on_laser_annotation, self);
}  

static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -l LOG             Specify Log File to save training data\n"
             "  -i input_lcm_file  (optinal) Specify LCM log file to read annotated laser data from\n", 
             "  -d                 Include Doorways\n", 
             "  -v                 Verbose Mode\n", 
             "  -h                 This help message\n", 
             g_path_get_basename(progname));
    exit(1);
}


int 
main(int argc, char **argv)
{
    laser_annotation *state = (laser_annotation*) calloc(1, sizeof(laser_annotation));
    const char *optstring = "i:l:vhd";

    int c;
    state->file_name = NULL;
    state->skip_doorways = 1;
    char *input_log_file = NULL;
    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'l':
            state->file_name = strdup(optarg);  //choose log file for accuracy checking
            fprintf(stderr, "Saving to LOG file %s\n", state->file_name);
            break;
        case 'i':
            input_log_file = strdup(optarg);  //choose log file for accuracy checking
            fprintf(stderr, "Reading from LOG file %s\n", input_log_file);
            break;
        case 'd':
            state->skip_doorways = 0;
            fprintf(stderr, "Training for doorways as well\n");
            break;
        case 'v':
            fprintf(stderr, "Verbose mode\n");
            state->verbose = 1;
            break;
        case 'h': //help
            usage(argv[0]);
            break;
        default:
            usage(argv[0]);
            break;
        }
    }

    fprintf(stderr, "Don't use the live mode - the handler can drop messages (because a lot of messages are sent out in bursts\n");

    if(state->file_name == NULL){
        fprintf(stderr, "No log file specified - defaulting to data.txt\n");
        //state->file = fopen("data.txt", "a");
        state->file_name = strdup("data.txt");
    }

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);

    state->class_count = g_hash_table_new(g_int_hash, g_int_equal);

    if(input_log_file){
        fprintf(stderr, "System is loading the annotation messages in log to calculate features.\n");
        lcm_eventlog_t *read_log = lcm_eventlog_create(input_log_file, "r");
        fprintf(stderr, "Log open success.\n");
  
        //Adding to annotation circular buffer
        char *annotation_channel_name = "LASER_ANNOTATION";
        int count = 0;
        for(lcm_eventlog_event_t *event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){
            int decode_status = 0;
            if(strcmp(annotation_channel_name, event->channel) == 0){
                erlcm_laser_annotation_t *msg = (erlcm_laser_annotation_t *)calloc(1, sizeof(erlcm_laser_annotation_t));
                decode_status = erlcm_laser_annotation_t_decode(event->data, 0, event->datalen, msg);
                if(decode_status < 0){
                    fprintf(stderr, "Error decoding message.\n");
                    //return -1;
                }
                count++;
                on_laser_annotation(NULL, annotation_channel_name, msg, state);
                erlcm_laser_annotation_t_destroy(msg);
            }
            lcm_eventlog_free_event(event);
        }
        print_stats(state);
        fprintf(stderr, "No of examples : %d\n", count);
        return 0;
    }

    else{
        subscribe_to_channels(state);
  
        state->mainloop = g_main_loop_new( NULL, FALSE );  
  
        if (!state->mainloop) {
            printf("Couldn't create main loop\n");
            return -1;
        }

        //add lcm to mainloop 
        bot_glib_mainloop_attach_lcm (state->lcm);

        //read_parameters_from_conf(state);

        //adding proper exiting 
        bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
        //    fprintf(stderr, "Starting Main Loop\n");

        ///////////////////////////////////////////////
        g_main_loop_run(state->mainloop);
  
        bot_glib_mainloop_detach_lcm(state->lcm);
        return 0;
    }
}


