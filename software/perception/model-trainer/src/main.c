#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>

#define BIN_SIZE 10

typedef struct
{
    lcm_t      *lcm;
    GMainLoop *mainloop;

    //GList *pose_list;     
    BotPtrCircular   *velodyne_features;

    int publish_all;
    int verbose; 
} state_t;


static void
on_segments (const lcm_recv_buf_t *rbuf, const char *channel,
             const erlcm_segment_feature_list_t *msg, void *user)
{
    state_t *s = (state_t *)user;
    g_assert(s);

    bot_ptr_circular_add (s->velodyne_features,  erlcm_segment_feature_list_t_copy(msg));

    fprintf(stderr, "Utime : %f\n", msg->utime / 1.0e6);
}

static void
on_person_id (const lcm_recv_buf_t *rbuf, const char *channel,
             const erlcm_person_id_t *msg, void *user)
{
    state_t *s= (state_t *)user;
    g_assert(s);

    //fprintf(stderr, "Buffer size : %d\n" , bot_ptr_circular_size(app->front_laser_circ));
    double min_utime = 0.2;//we dont consider if the closest laser is 0.5 s apart
    int close_ind = -1;

    for (int i=0; i<(bot_ptr_circular_size(s->velodyne_features)); i++) {
        erlcm_segment_feature_list_t *features = (erlcm_segment_feature_list_t *) bot_ptr_circular_index(s->velodyne_features, i);
        double time_diff = fabs(features->utime - msg->sensor_utime)  / 1.0e6;
        //
        if(time_diff< min_utime){
            min_utime = time_diff;
            close_ind = i;
        }
    }

    fprintf(stderr, "Time difference : %f => Ind : %d\n", min_utime, close_ind); 

    if(min_utime == 0.0){
        FILE *file; 
        file = fopen("train.txt","a+");

        erlcm_segment_feature_list_t *match = (erlcm_segment_feature_list_t *) bot_ptr_circular_index(s->velodyne_features, close_ind);
        fprintf(stderr,"Found Correct message\n");
        for(int i=0; i < match->count; i++){
            int type = -1;
            erlcm_segment_feature_t *seg = &match->list[i];
            //-1 1:0.43 3:0.12 9284:0.2
            if(seg->id == msg->person_segment_no){
                type = +1;
                fprintf(stderr, "Found Poisitive example\n");
            }
            double curvature3 = 100000000;
            if(seg->eigen_values[2] != 0){
                curvature3 = seg->eigen_values[1]/seg->eigen_values[2];
            }
            if(seg->no_points ==0)
                continue;

            //17
            fprintf(file, "%d 1:%f 2:%f 3:%f 4:%f 5:%f 6:%f 7:%f 8:%f 9:%f 10:%f 11:%f 12:%f 13:%f 14:%f 15:%f 16:%f 17:%d\n", 
                    type, seg->eigen_values[0], seg->eigen_values[1], seg->eigen_values[2], 
                    seg->first_principle_max, seg->first_principle_min, seg->second_principle_max, 
                    seg->second_principle_min, seg->third_principle_max, seg->third_principle_min, 
                    seg->first_std_dev, seg->second_std_dev, seg->third_std_dev, 
                    seg->dot_product_with_ground_normal, 
                    seg->curvature1, seg->curvature2, curvature3, 
                    seg->no_points);
        }
        fclose(file);
    }
    
    //dump the relavent features to file - in the format that libsvm expects 
}

void subscribe_to_channels(state_t *s)
{
    //bot_core_pose_t_subscribe(s->lcm, "POSE", on_pose ,s);
    erlcm_person_id_t_subscribe(s->lcm, "PERSON_SEGMENT_ANNOTATION", on_person_id, s);
    erlcm_segment_feature_list_t_subscribe(s->lcm, "VELODYNE_PERSON_SEGMENTS", on_segments, s);
}  


static void usage(char * funcName)
{
    printf("Usage: %s [options]\n"
	   "options are:\n"
	   "--help,        -h    display this message \n"
	   "--publish_all, -a    publish robot laser messages for all channels \n"
	   "--verbose,     -v    be verbose", funcName);
    exit(1);

}

void
circ_free_velodyne_data(void *user, void *p) {
    erlcm_segment_feature_list_t *np = (erlcm_segment_feature_list_t *) p; 
    erlcm_segment_feature_list_t_destroy(np);
}

int 
main(int argc, char **argv)
{
    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    const char *optstring = "hav";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "publish_all", no_argument, 0, 'a' }, 
				  { "verbose", no_argument, 0, 'v' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    usage(argv[0]);
	    break;
	case 'a':
	    {
		fprintf(stderr,"Publishing all laser channels\n");
		break;
	    }
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
		state->verbose = 1;
		break;
	    }
	default:
	    {
		usage(argv[0]);
		break;
	    }
	}
    }

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);

    state->velodyne_features = bot_ptr_circular_new (BIN_SIZE,
                                                     circ_free_velodyne_data, state);

    subscribe_to_channels(state);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    //read_parameters_from_conf(state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


