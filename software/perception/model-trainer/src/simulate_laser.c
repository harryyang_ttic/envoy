//This scripts subscribe to SKIRT_FRONT & SKIRT_REAR messages and simulate a 360-degree laser SKIRT_SIM. 
//Instead of publishing SKIRT_SIM, we might create a library for it. 

#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
#include <math.h>

#include <bot_lcmgl_client/lcmgl.h>
#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <bot_frames/bot_frames.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif 


#define NO_BINS (360) //360

typedef struct
{
    lcm_t      *lcm;
    BotParam *param;
    BotFrames *atrans;
    GMainLoop *mainloop;
    bot_core_planar_lidar_t *last_front_laser;
    bot_core_planar_lidar_t *last_rear_laser;
    float readings[NO_BINS];
    //double pos[360][3];
    bot_lcmgl_t * lcmgl;
} state_t;

int get_beam_id(double radians){
    return floor((radians + 2*M_PI)/(2*M_PI) * NO_BINS);
}

static void
on_front_laser (const lcm_recv_buf_t *rbuf, const char *channel,
                const bot_core_planar_lidar_t *msg, void *user)
{
    state_t *s = (state_t *)user;

    if(s->last_front_laser != NULL){
        bot_core_planar_lidar_t_destroy(s->last_front_laser);
    }
    s->last_front_laser = bot_core_planar_lidar_t_copy(msg);

    if(s->last_rear_laser==NULL || (s->last_rear_laser->utime - msg->utime)/1.0e6 > 0.5){
        return; 
    }
  
    fprintf(stderr, ".");

    int64_t start = bot_timestamp_now();

    for(int i=0; i < NO_BINS; i++){
        s->readings[i] = 30;
    }

    //int f_max_id = 0;
    //int f_min_id = 400;
    int wierd = 0; 
    //double min;
    for(int i = 0; i < s->last_front_laser->nranges - 1;i++){//)+=4){
        if( s->last_front_laser->ranges[i] < 0.01) 
            continue;
        //Transform from laser frame to body frame if needed
        double pose[3];
        double body[3];
        double theta = s->last_front_laser->rad0 + i * s->last_front_laser->radstep;
        //laser frame
        pose[0] = s->last_front_laser->ranges[i] * cos(theta);
        pose[1] = s->last_front_laser->ranges[i] * sin(theta);
        pose[2] = 0;
        //get it in body frame
        int trans = bot_frames_transform_vec(s->atrans, "SKIRT_FRONT", "body", pose, body);
    
        if(fabs(body[0]) < 0.5 && fabs(body[1]) < 0.3)
            continue;
        //get the heading 
        double heading = atan2(body[1], body[0]);
        double range = hypot(body[0], body[1]);
        //get the bin id 
        int id = bot_theta_to_int(heading, NO_BINS);//get_beam_id(heading);
        //  fprintf(stderr, "Front Laser : %f Body : %f ID : %d\n", bot_to_degrees(theta), 
        //          bot_to_degrees(heading), id);
        /*if(id > 135 && id < 225){
          fprintf(stderr, "Front Laser : %f Body : %f ID : %d\n", bot_to_degrees(theta), 
          bot_to_degrees(heading), id);
          wierd = 1;
          }*/

        if(s->readings[id] > range){
            s->readings[id] = range;
        }
    }


    for(int i = 0; i < s->last_rear_laser->nranges - 1;i++){//)+=4){
        if( s->last_rear_laser->ranges[i] < 0.01) 
            continue;
        //Transform from laser frame to body frame if needed
        double pose[3];
        double body[3];
        double theta = s->last_rear_laser->rad0 + i * s->last_rear_laser->radstep;
        //laser frame
        pose[0] = s->last_rear_laser->ranges[i] * cos(theta);
        pose[1] = s->last_rear_laser->ranges[i] * sin(theta);
        pose[2] = 0;
        //get it in body frame
        int trans = bot_frames_transform_vec(s->atrans, "SKIRT_REAR", "body", pose, body);
        if(fabs(body[0]) < 0.5 && fabs(body[1]) < 0.3)
            continue;
        //get the heading 
        double heading = atan2(body[1], body[0]);
        double range = hypot(body[0], body[1]);
        //get the bin id 
        int id = bot_theta_to_int(heading, NO_BINS);//get_beam_id(heading);
        /*
          fprintf(stderr, "Rear Laser : %f Body : %f ID : %d\n", bot_to_degrees(theta), 
          bot_to_degrees(heading), id);
        */
        if(s->readings[id] > range){
            s->readings[id] = range;
        }
    }

    int64_t end = bot_timestamp_now();
    
    //    fprintf(stderr, "Time taken : %f\n", (end-start)/1.0e6);

    bot_lcmgl_color3f(s->lcmgl, 0.5, 0.0, 0.5);
    bot_lcmgl_point_size(s->lcmgl, 5);
    bot_lcmgl_begin(s->lcmgl, GL_POINTS);

    int prev_valid = -1;
    for(int i=0; i < NO_BINS; i++){
        double r = s->readings[i];
	
	if(r < 30 && r > 0.1){
	  prev_valid = i;
	}
        if(r >=30 || r <= 0.1){
	  int next_valid = -1;
	  for(int j = i+1;j < NO_BINS;j++){
	    if(s->readings[j] < 30 && s->readings[j] > 0.1){
	      next_valid = j;
	      break;
	    }
	  }
	  if(prev_valid != -1 && next_valid != -1)
	    s->readings[i] = (s->readings[prev_valid] + s->readings[next_valid]) / (double)2;
        }
    }
    
    bot_core_planar_lidar_t sim_msg; 

    sim_msg.utime = msg->utime;
    sim_msg.nranges = NO_BINS;
    sim_msg.ranges = s->readings;
    sim_msg.nintensities = 0; 
    sim_msg.intensities = NULL;
    sim_msg.rad0 = 0; 
    sim_msg.radstep = 2*M_PI/ NO_BINS; 
    bot_core_planar_lidar_t_publish(s->lcm, "SKIRT_SIM", &sim_msg);

    //let laser annotation subscribe to SKIRT_SIM instead of SKIRT_FRONT
    //call the features here - and then write to file - 
    
    
    for(int i=0; i < NO_BINS; i++){
        double r = s->readings[i];
        double theta = i * 2*M_PI/ NO_BINS;
        //        s->readings[i];
        //double pose[3];
        double body[3] = {r*cos(theta), r*sin(theta), 0}, local[3];
        bot_frames_transform_vec(s->atrans, "body", "local",  body, local);
        //bot_lcmgl_vertex3f(s->lcmgl, body[0], body[1], body[2]);
        bot_lcmgl_vertex3f(s->lcmgl, local[0], local[1], local[2]);
      
    }
    bot_lcmgl_switch_buffer(s->lcmgl);
}

static void
on_rear_laser (const lcm_recv_buf_t *rbuf, const char *channel,
               const bot_core_planar_lidar_t *msg, void *user)
{
    state_t *s = (state_t *)user;

    if(s->last_rear_laser != NULL){
        bot_core_planar_lidar_t_destroy(s->last_rear_laser);
    }
    s->last_rear_laser = bot_core_planar_lidar_t_copy(msg);

    /*for(int i = 0; i < s->last_rear_laser->nranges - 1;i++){//)+=4){
    //Transform from laser frame to body frame if needed
    double pose[3];
    double body[3];
    double theta = s->last_rear_laser->rad0 + i * s->last_rear_laser->radstep;
    //laser frame
    pose[0] = s->last_rear_laser->ranges[i] * cos(theta);
    pose[1] = s->last_rear_laser->ranges[i] * sin(theta);
    pose[2] = 0;
    //get it in body frame
    int trans = bot_frames_transform_vec(s->atrans, "SKIRT_REAR", "body", pose, body);
    
    //get the heading 
    double heading = atan2(body[1], body[0]);
    double range = hypot(body[0], body[1]);
    //get the bin id 
    int id = bot_theta_to_int(heading, NO_BINS);//get_beam_id(heading);
    //
    //fprintf(stderr, "Rear Laser : %f Body : %f ID : %d\n", bot_to_degrees(theta), 
    //bot_to_degrees(heading), id);
    //
    if(s->readings[id] > range){
    s->readings[id] = range;
    }
    }*/
}

void subscribe_to_channels(state_t *s)
{
    bot_core_planar_lidar_t_subscribe(s->lcm, "SKIRT_FRONT", on_front_laser, s);
    bot_core_planar_lidar_t_subscribe(s->lcm, "SKIRT_REAR", on_rear_laser, s);
}  

int 
main(int argc, char **argv)
{
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
    state->lcmgl = bot_lcmgl_init(state->lcm, "sim-laser");
    state->param = bot_param_get_global(state->lcm, 0);
    state->atrans = bot_frames_get_global(state->lcm, state->param); 

    subscribe_to_channels(state);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }
  
    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    //read_parameters_from_conf(state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    //    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}
