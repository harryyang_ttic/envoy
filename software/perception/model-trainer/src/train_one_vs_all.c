//Trains no_class one-vs-all models for classification

#include <svm/svm.h>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
int 
main(int argc, char **argv)
{

    //input data & output label 
    FILE *file1, *file2; 
    if(argc < 3){
        fprintf(stderr, "Please provide file names\n");
        return -1;
    }
    fprintf(stderr,"Opening files : %s\n", argv[1], argv[2]);
    file1 = fopen(argv[1],"r");
    if(file1 == NULL)
        fprintf(stderr, "Error openning file1.\n");
    file2 = fopen(argv[2], "w");
    if(file2 == NULL)
        fprintf(stderr, "Error openning file2.\n");
	
	
    typedef struct svm_node svm_node_t;
    typedef struct svm_model svm_model_t;
    svm_model_t **model;
    svm_node_t *test_pt = NULL;

    int no_classes = 7;
		
    model = (svm_model_t **)calloc(no_classes, sizeof(svm_model_t *));
	
    model[0] = NULL;
    char *model_path = "m1";
    if(model_path != NULL){
        model[0] = svm_load_model(model_path);
        if(model[0] == NULL)
            fprintf(stderr,"Error loading model1\n");
        else{
            fprintf(stderr, "Model1 loaded sucessfully\n");
        }
    }

    model[1] = NULL;
    model_path = "m2";
    if(model_path != NULL){
        model[1] = svm_load_model(model_path);
        if(model[1] == NULL)
            fprintf(stderr,"Error loading model2\n");
        else{
            fprintf(stderr, "Model2 loaded sucessfully\n");
        }
    }

    model[2] = NULL;
    model_path = "m3";
    if(model_path != NULL){
        model[2] = svm_load_model(model_path);
        if(model[2] == NULL)
            fprintf(stderr,"Error loading model3\n");
        else{
            fprintf(stderr, "Model3 loaded sucessfully\n");
        }
    }

    model[3] = NULL;
    model_path = "m4";
    if(model_path != NULL){
        model[3] = svm_load_model(model_path);
        if(model[3] == NULL)
            fprintf(stderr,"Error loading model4\n");
        else{
            fprintf(stderr, "Model4 loaded sucessfully\n");
        }
    }

    model[4] = NULL;
    model_path = "m5";
    if(model_path != NULL){
        model[4] = svm_load_model(model_path);
        if(model[4] == NULL)
            fprintf(stderr,"Error loading model5\n");
        else{
            fprintf(stderr, "Model5 loaded sucessfully\n");
        }
    }

    model[5] = NULL;
    model_path = "m6";
    if(model_path != NULL){
        model[5] = svm_load_model(model_path);
        if(model[5] == NULL)
            fprintf(stderr,"Error loading model6\n");
        else{
            fprintf(stderr, "Model6 loaded sucessfully\n");
        }
    }

    model[6] = NULL;
    model_path = "m7";
    if(model_path != NULL){
        model[6] = svm_load_model(model_path);
        if(model[6] == NULL)
            fprintf(stderr,"Error loading model6\n");
        else{
            fprintf(stderr, "Model7 loaded sucessfully\n");
        }
    }

    /*	if(model != NULL){
        int svm_type = svm_get_svm_type(model);
        int nr_class = svm_get_nr_class(model);
        fprintf(stderr, "SVM type : %d SVM Class : %d\n", svm_type, nr_class);
    	}

	fprintf(stderr, "Testing:%d\n", model[0]->free_sv);
    */
    char line[5000], *ch;
    int no_features = 47;
    test_pt = (svm_node_t *)calloc(no_features, sizeof(svm_node_t));
	
    test_pt[46].index = -1;
    int correct[no_classes];
    int total[no_classes];	
    int TP[no_classes];
    int TN[no_classes];
    int FP[no_classes];
    int FN[no_classes];
    int count[no_classes][no_classes];
    for(int i = 0;i < no_classes;i++){
        for(int j = 0;j < no_classes;j++){
            count[i][j] = 0;
        }
    }
    for(int i = 0; i < no_classes;i++){
        correct[i] = 0;
        total[i] = 0;
        TP[i] = 0;
        TN[i] = 0;
        FP[i] = 0;
        FN[i] = 0;
    }
    while(fgets(line,sizeof(line),file1) != NULL){
        //		fprintf(stderr, "%s\n", line);	
        char *label = strtok(line," \t\n");
        int l = atoi(label);
        //		fprintf(stderr, "Label:%d\n", l);
        char *idx, *val, *endptr;
        int num = 0;
        while(1)
            {
                if(num == no_features - 1)
                    break;
                idx = strtok(NULL,":");
                int ind = strtod(idx, &endptr);
                val = strtok(NULL," \t");
                double v = strtod(val,&endptr);
                //			fprintf(stderr,"Strings : %s %s\n", idx, val);
                //			fprintf(stderr, "%d %f\n", ind, v);
                test_pt[ind - 1].index = ind;
                test_pt[ind - 1].value = v;
                //			fprintf(stderr, "%d\n", ind);
                num++;
            }
        double *dec_values = (double *)calloc(1, sizeof(double));
        double confidence[no_classes];
        double result[no_classes];
		
	//	double predict_label = svm_predict(model[0],test_pt);
        double max = -100;
        int max_label = -1;
        //if all 0, check = 1, if more than one value = 1, check = 2
        int count_one = 0;
        int one_index = -2;
        for(int i = 0;i < no_classes;i++){
            result[i] = svm_predict_values(model[i], test_pt, dec_values);
            confidence[i] = dec_values[0];
            if(result[i] == 1){
                count_one++;
                one_index = i;
            }
            if(confidence[i] > max){
                max = confidence[i];
                max_label = i;
            }
        }
        if(count_one == 1){
            max_label = one_index;
        }
        else if(count_one == 0){
            double min = 100;
            for(int i = 0;i < no_classes;i++){
                if(fabs(confidence[i]) < min){
                    min = fabs(confidence[i]);
                    max_label = i;
                }
            }
        }
        else if(count_one > 1){
            double max = -100;
            for(int i = 0; i < no_classes;i++){
                if(result[i] == 1 && confidence[i] > max){
                    max = confidence[i];
                    max_label = i;
                }
            }
        }
        count[l - 1][max_label]++;
        //		fprintf(stderr, "%f %f %f %f %f %f\n", confidence[0], confidence[1], confidence[2], confidence[3], confidence[4], confidence[5]);
        fprintf(file2, "%d\n", (max_label + 1));

        if((max_label + 1) == l){
            correct[l - 1]++;
            TP[l - 1]++;
            for(int j = 0; j < no_classes;j++){
                if(j != (l - 1))
                    TN[j]++;
            }
        }
        else{
            FN[l - 1]++;
            FP[max_label]++;
            for(int j = 0;j < no_classes;j++){
                if(j != (l - 1) && j != max_label)
                    TN[j]++;
            }
        }
        total[l - 1]++;
    }
    int to = 0;
    int corr = 0;
    for(int i = 0;i < no_classes;i++){
        to += total[i];
        corr += correct[i];
    }
    fprintf(stderr, "1:doorway, 2:conference_room, 3:office, 4:lab, 5:open_area, 6:hallway, 7:corridor\n");
    fprintf(stderr, "Precision = tp/(tp+fp), Recall=tp/(tp+fn), Accuracy=tp+tn/(tp+tn+fp+fn), Overall Accuracy = correct prediction/total no. of test data\n");
    for(int i = 0; i < no_classes;i++){
        fprintf(stderr, "Landmark No.: %d, TP: %d, FP: %d, FN: %d, TN: %d, Precision: %f, Recall: %f, Accuracy: %f\nClass Count: %d %d %d %d %d %d %d\n", (i + 1), TP[i], FP[i], FN[i], TN[i], TP[i] / (double)(TP[i] + FP[i]), TP[i] / (double)(TP[i] + FN[i]), (TP[i] + TN[i]) / (double)(TP[i] + TN[i] + FP[i] + FN[i]), count[i][0], count[i][1], count[i][2], count[i][3], count[i][4], count[i][5], count[i][6]);
    }
    fprintf(stderr, "Overall Accuracy: %f\n", (double)corr / to);
    fclose(file1);
    fclose(file2);
    return 1;
}
