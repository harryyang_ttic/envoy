//Subscribe to place_classification_t messages and apply HMM based on positions.
//Output: HMM_place_classification channel & HMM_place_classification_debug_t channel
//Command line: EXCEUTABLE model-file publish_option. You can add a log file with
//annotations for accuracy checking 


#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>
#include <math.h>
#include <svm/svm.h>

#include <stdlib.h>
#include <stdio.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <bot_frames/bot_frames.h>
#include <full_laser/full_laser.h>
#include "classifier_lib.h"

#define NO_CLASS 8
#define ANNOTATION_BIN_SIZE 100
#define THRESHOLD 0.2

typedef struct
{
    lcm_t      *lcm;
    GMainLoop *mainloop;
    BotParam *param;
    BotFrames *frames;
    bot_core_pose_t *last_pose;   //Most recent pose
    bot_core_pose_t *last_valid;  //Last pose that is 0.2m apart
    svm_model_t *model;
    svm_model_t *doorway_model;
    full_laser_state_t *full_laser;

    FILE *f0;  //File for observation histograms
    FILE *f1;  //File for stationary transition model
    FILE *f2; //File for moving transition model
    FILE *f3;
    double p[NO_CLASS][NO_CLASS];  //Probability from histogram
    double transition[NO_CLASS][NO_CLASS]; //Probability of transition model
    double belief[NO_CLASS];

    int publish_option;
    int num_annotations;
    int correct;
    int total;
    int doorway;
    BotPtrCircular *annotation_list;

} state_t;


double *compute_histogram(FILE *file){
    double *p = (double *) calloc(NO_CLASS * NO_CLASS, sizeof(double));
    GList **class = NULL;
    class = (GList **) calloc(NO_CLASS, sizeof(GList *));
    char str[5000];
    while(fgets(str,sizeof(str),file) != NULL){
        int len = strlen(str)- 1;
        if(str[len] == '\n')
            str[len] = 0;
        int class_id = atoi(&str[0]) - 1;
        if(class_id >= NO_CLASS){
            fprintf(stderr, "Class id exceeds no of classes.\n");
        }
        class[class_id] = g_list_prepend(class[class_id], strdup(str));
    }
	
    int d[NO_CLASS];
    for(int i = 0;i < NO_CLASS;i++){
        d[i] = (int)ceil(g_list_length(class[i]) / 100);
    }

    //Probability distribution of histograms
    int result[NO_CLASS][NO_CLASS];
    for(int i = 0;i < NO_CLASS;i++){
        for(int j = 0;j < NO_CLASS;j++){
            result[i][j] = 0;
        }
    }
    for(int i = 0;i < NO_CLASS;i++){
        int length = g_list_length(class[i]);
        for(int j = 0; j < length;j++){
            if(j % d[i] == 0){
                char *s = (char *)g_list_nth(class[i], j)->data;
                int actual_class = atoi(&s[0]) - 1;
                int predict_class = atoi(&s[2]) - 1;
                result[actual_class][predict_class]++;
            }
        }
    }
    free(class);
    for(int i = 0; i < NO_CLASS;i++){
        int count = 0;
        for(int j = 0;j < NO_CLASS;j++){
            count += result[i][j];
        }
        for(int j = 0;j < NO_CLASS;j++){
            int index = i * NO_CLASS + j;
            p[index] = (double)result[i][j] / count;
        }
    }
    fclose(file);
    return p;
}


double *compute_transition(FILE *file1){
    double *p = (double *) calloc(NO_CLASS * NO_CLASS, sizeof(double));
    int count[NO_CLASS][NO_CLASS];
    int total[NO_CLASS];
    for(int i = 0;i < NO_CLASS;i++){
        for(int j = 0;j < NO_CLASS;j++){
            count[i][j] = 0;
        }
        total[i] = 0;
    }
    char str[5000];
    //Read from model
    while(fgets(str,sizeof(str),file1) != NULL){
        //Get the start class
        int start_class = atoi(&str[0]) - 1;
        int end_class = atoi(&str[2]) - 1;
        if(start_class >= NO_CLASS || end_class >= NO_CLASS){
            fprintf(stderr, "Class id exceeds no of classes.\n");
        }
        count[start_class][end_class]++;
        total[start_class]++;
    }
    for(int i = 0;i < NO_CLASS;i++){
        for(int j = 0;j < NO_CLASS;j++){
            p[i * NO_CLASS + j] = (double)(count[i][j] + 1) / (total[i] + NO_CLASS);
        }
    }
    return p;
}


static void 
full_laser_update_handler(int64_t utime, bot_core_planar_lidar_t *msg, void *user)
{
    state_t *self = (state_t *)user;
    //Get the classification result from library
    erlcm_place_classification_t *classification = classify_place(msg, self->model, self->doorway_model);
    //Publish SVM place classification messages
    if(self->publish_option){
        fprintf(stderr, "Classified place\n");
        erlcm_place_classification_t *msg = erlcm_place_classification_t_copy(classification);
        erlcm_place_classification_t_publish(self->lcm, "PLACE_CLASSIFICATION", msg);
    }
    if(self->last_valid != NULL){
        double last_xy[3];
        double valid_xy[3];
        //transform from body to local
        int t1 = bot_frames_transform_vec(self->frames, "body", "local", self->last_pose->pos, last_xy);
        int t2 = bot_frames_transform_vec(self->frames, "body", "local", self->last_valid->pos, valid_xy);
        //update last_valid. Change distance between HMM messages here..
    
        if(hypot(valid_xy[0] - last_xy[0], valid_xy[1] - last_xy[1]) >= THRESHOLD){
            //oNLY update belief when consecutive dist >= 0.2m
            //Observation model
            char *classes[NO_CLASS];
            classes[0] = "elevator";
            classes[1] = "conference_room";
            classes[2] = "office";
            classes[3] = "lab";
            classes[4] = "open_area";
            classes[5] = "hallway";
            classes[6] = "corridor";
            classes[7] = "large_meeting_room";
            int predict_class;
            double observation_model[NO_CLASS];
            for(int i = 0;i < NO_CLASS;i++){
                if(strcmp(classification->label, classes[i]) == 0)
                    predict_class = i;
            }
            for(int i = 0;i < NO_CLASS;i++){
                observation_model[i] = self->p[i][predict_class];
            }
	
            //Transition model
            double current_belief[NO_CLASS];
            double belief_sum = 0;
            double transition_model_value[NO_CLASS];
            for(int i = 0;i < NO_CLASS;i++){
                transition_model_value[i] = 0;
                //SIGMA part
                for(int j = 0;j < NO_CLASS;j++){
                    transition_model_value[i] += self->transition[j][i] * self->belief[j];
                }
                //Update belief, Add small constant to observation model
                current_belief[i] = (observation_model[i] + 0.00001) * transition_model_value[i];
                belief_sum += current_belief[i];
            }

            //Store a copy of last belief to publish debug messages later
            double previous_belief_value[NO_CLASS];
            for(int i = 0;i < NO_CLASS;i++)
                previous_belief_value[i] = self->belief[i];


            //Normalize
            for(int i = 0;i < NO_CLASS;i++){
                self->belief[i] = (double)current_belief[i] / belief_sum;
            }


            //Publish the HMM_place_classification messages & HMM_place_classification_debug message
            erlcm_place_classification_t message;
            erlcm_HMM_place_classification_debug_t debug_msg;

            message.utime = bot_timestamp_now();
            message.pose_utime = classification->pose_utime;
            message.sensor_utime = message.pose_utime;
            message.sensor_type = classification->sensor_type;    
            message.no_class = classification->no_class;
            message.classes = (erlcm_place_classification_class_t *)calloc(message.no_class, sizeof(erlcm_place_classification_class_t));

            char *label;
            //doorway message
            if(self->doorway_model && strcmp(classification->label, "doorway") == 0){
                fprintf(stderr, "HMM found a possible doorway.\n");
                message.label = "doorway";
                for(int i = 0; i < message.no_class - 1;i++){
                    message.classes[i].name = i + 1;
                    message.classes[i].probability = self->belief[i];
                }
                //Set doorway probability to 1
                message.classes[message.no_class - 1].name = message.no_class;
                message.classes[message.no_class - 1].probability = 1;
            }

            else{
                double highest = -1;
                int index = -1;
                for(int i = 0;i < NO_CLASS;i++){
                    if(self->belief[i] > highest){
                        highest = self->belief[i];
                        index = i;
                    }
                }
                message.label = classes[index];
                for(int i = 0; i < message.no_class;i++){
                    message.classes[i].name = i + 1;
                    message.classes[i].probability = self->belief[i];
                }
            }

            //Check whether this is a correct prediction
            if(self->num_annotations){
                char *real_class;
                for(int i = 0;i < bot_ptr_circular_size(self->annotation_list);i++){
                    erlcm_annotation_t *iter = (erlcm_annotation_t *)bot_ptr_circular_index(self->annotation_list, i);
                    int64_t start_utime = iter->start_utime;
                    int64_t end_utime = iter->end_utime;
                    if(start_utime <= message.sensor_utime && end_utime >= message.sensor_utime){
                        real_class = iter->annotation;
                        break;
                    }
                }
                if(strcmp(real_class, message.label) == 0){
                    fprintf(stderr, "Correct prediction: %s\n", real_class);
                    self->correct++;
                }
                else{
                    fprintf(stderr, "Wrong prediction. Prediction by classfier: %s, Real: %s\n" ,message.label, real_class);
                }
                self->total++;
                fprintf(stderr, "Accuracy so far: %.2f\n", (double)self->correct / self->total);
            }

            debug_msg.utime = message.utime;
            debug_msg.sensor_utime = message.sensor_utime;
            debug_msg.no_class = message.no_class;
            debug_msg.label = message.label;
            debug_msg.model_type = -1;

            debug_msg.prior = (erlcm_place_classification_class_t *)calloc(debug_msg.no_class, sizeof(erlcm_place_classification_class_t));
            debug_msg.transition = (erlcm_place_classification_class_t *)calloc(debug_msg.no_class, sizeof(erlcm_place_classification_class_t));
            debug_msg.observation = (erlcm_place_classification_class_t *)calloc(debug_msg.no_class, sizeof(erlcm_place_classification_class_t));
            debug_msg.without_HMM = (erlcm_place_classification_class_t *)calloc(debug_msg.no_class, sizeof(erlcm_place_classification_class_t));

            for(int i = 0; i < message.no_class;i++){
                debug_msg.prior[i].name = i + 1;
                debug_msg.prior[i].probability = previous_belief_value[i];
                debug_msg.transition[i].name = i + 1;
                debug_msg.transition[i].probability = transition_model_value[i];
                debug_msg.observation[i].name = i + 1;
                debug_msg.observation[i].probability = observation_model[i];
                debug_msg.without_HMM[i].name = i + 1;
                debug_msg.without_HMM[i].probability = classification->classes[i].probability;
            }
            erlcm_place_classification_t_publish(self->lcm, "HMM_PLACE_CLASSIFICATION", &message);
            erlcm_HMM_place_classification_debug_t_publish(self->lcm, "HMM_PLACE_CLASSIFICATION_DEBUG", &debug_msg);
            //update last_valid
            bot_core_pose_t_destroy(self->last_valid);
            self->last_valid = bot_core_pose_t_copy(self->last_pose);
        }
    }
    //If this is the first pose message
    else if(self->last_pose)
        self->last_valid = bot_core_pose_t_copy(self->last_pose);
}


static void
on_pose(const lcm_recv_buf_t *rbuf, const char *channel,
	const bot_core_pose_t *msg, void *user){
    state_t *self = (state_t *)user;
    if(self->last_pose != NULL){
        bot_core_pose_t_destroy(self->last_pose);
    }
    self->last_pose = bot_core_pose_t_copy(msg);
}


void subscribe_to_channels(state_t *self)
{
    bot_core_pose_t_subscribe(self->lcm, "POSE", on_pose, self);
}  


void
circ_free_annotation(void *user, void *p){
    erlcm_annotation_t *np = (erlcm_annotation_t *)p;
    erlcm_annotation_t_destroy(np);
}



static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -l LOG        Specify Log File\n"
             "  -d Binary-doorway           Consider doorways\n"
             "  -p            Publish place classification messages\n"
             "  -m SVM MODEL  Specify SVM Model\n"
             "  -h            This help message\n", 
             g_path_get_basename(progname));
    exit(1);
}


int 
main(int argc, char **argv)
{

    const char *optstring = "l:d:pm:h";

    int c;
    int doorway = 0;
    char *log_file = NULL;
    char *model_path;
    int publish = 1;
    char *doorway_model;

    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'l':
            log_file = strdup(optarg);  //choose log file for accuracy checking
            fprintf(stderr, "Using LOG file \"%s\"\n", log_file);
            break;
        case 'd':
            doorway = 1;
            doorway_model = strdup(optarg);
            fprintf(stderr, "Doorway option set to :%d\n", doorway);
            break;
        case 'p':
            publish = 1;
            fprintf(stderr, "Publish option set to %d\n", publish);
            break;
        case 'm': //choose SVM model
            model_path = strdup(optarg);
            printf("Using SVM model \"%s\"\n", model_path);
            break;
        case 'h': //help
            usage(argv[0]);
            break;
        default:
            usage(argv[0]);
            break;
        }
    }

    state_t *state = (state_t *) calloc(1, sizeof(state_t));

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
    state->param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global(state->lcm, state->param);
    state->full_laser = full_laser_init(state->lcm, 360, &full_laser_update_handler, state);
    state->doorway_model = NULL;

    subscribe_to_channels(state);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }


    state->f0 = fopen("observation_model.txt", "r");
    state->f1 = fopen("pose-transition.txt", "r");
    //if(!state->f0){
    //fprintf(stderr, "Cannot open file.\n");
    //return -1;
    //}
  
    fprintf(stderr, "Using histograms directly as observation model.\n");

    //double *v = compute_histogram(state->f0);
    double correct_obs_prob = 0.9;

    for(int i = 0;i < NO_CLASS;i++){
        for(int j = 0;j < NO_CLASS;j++){
            if(i==j)
                state->p[i][j] = correct_obs_prob;//v[i * NO_CLASS + j];
            else
                state->p[i][j] = (1 - correct_obs_prob) / (NO_CLASS - 1);
        }      
    }

    double self_transition_prob = 0.5;

    //Compute the transition probability
    //double *trans = compute_transition(state->f1);
    for(int i = 0;i < NO_CLASS;i++){
        for(int j = 0;j < NO_CLASS;j++){
            //state->transition[i][j] = trans[i * NO_CLASS + j];
            if(i==j)
                state->transition[i][j] = self_transition_prob;//trans[i * NO_CLASS + j];
            else
                state->transition[i][j] = (1 - self_transition_prob) / (NO_CLASS -1);  
        }
    }

    //Initialize the belief as uniform distribution
    for(int i = 0;i < NO_CLASS;i++){
        state->belief[i] = (double)1 / NO_CLASS;
    }
    
    state->last_pose = NULL;
    state->last_valid = NULL;

    state->model = (svm_model_t *)calloc(1, sizeof(svm_model_t));
    state->model = svm_load_model(model_path);
    if(!state->model){
        fprintf(stderr, "Model load failed.\n");
        return -1;
    }
    else
        fprintf(stderr, "Model loaded success.\n");

    state->doorway_model = (svm_model_t *)calloc(1, sizeof(svm_model_t));
    state->doorway_model = NULL;
    if(doorway){
        state->doorway_model = svm_load_model(doorway_model);
        if(!state->doorway_model){
            fprintf(stderr, "Error loading binary doorway model.\n");
            return -1;
        }
    }

    if(log_file && state->doorway_model)
        fprintf(stderr, "HMM includes doorway class. Ensure your log annotations contain doorways.\n");


    //Publish message option
    state->publish_option = publish;

    state->num_annotations = 0;
    //Load log into memory for accuracy checking
    if(log_file){
        fprintf(stderr, "System is loading the annotation messages in log for accuracy checking.Please wait a while.\n");
        lcm_eventlog_t *read_log = lcm_eventlog_create(log_file, "r");
        fprintf(stderr, "Log open success.\n");
        state->annotation_list = bot_ptr_circular_new(ANNOTATION_BIN_SIZE, circ_free_annotation, state);

        state->correct = 0;
        state->total = 0;
  
        //Adding to annotation circular buffer
        char *annotation_channel_name = "LOG_ANNOTATION";
        for(lcm_eventlog_event_t *event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){
            int decode_status = 0;
            if(strcmp(annotation_channel_name, event->channel) == 0){
                erlcm_annotation_t *msg = (erlcm_annotation_t *)calloc(1, sizeof(erlcm_annotation_t));
                decode_status = erlcm_annotation_t_decode(event->data, 0, event->datalen, msg);
                if(decode_status < 0){
                    fprintf(stderr, "Error decoding message.\n");
                    return -1;
                }
      
                bot_ptr_circular_add(state->annotation_list, msg);
                state->num_annotations++;
            }
            lcm_eventlog_free_event(event);
        }
        fprintf(stderr, "No. of annotations in the log: %d\n", state->num_annotations);
        if(state->num_annotations)
            fprintf(stderr, "Finish loading annotations in the log.\n");
        else
            fprintf(stderr, "No annotation messages in the log.\n");
    }


    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    //read_parameters_from_conf(state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    //    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}
