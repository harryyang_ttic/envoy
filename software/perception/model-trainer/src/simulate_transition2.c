//This script randomly sample the log and get transition model based on distance instead of time.

#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
#include <time.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <bot_frames/bot_frames.h>

#define ANNOTATION_LIST_SIZE 200
#define POSE_LIST_SIZE 50000
#define NO_CLASS 8

typedef struct _Pose Pose;
struct _Pose{
  double xy[2];
  int64_t utime;
};

//Check each class have at least 100 simulation results
int check_num(int count[NO_CLASS]){
  for(int i = 0;i < NO_CLASS;i++){
    if(count[i] < 200)
      return 0;
  }
  return 1;
}

int check_sum(int count[NO_CLASS]){
  int sum = 0;
  for(int i = 0;i < NO_CLASS;i++)
    sum += count[i];
  if(sum >= 1000)
    return 1;
  else
    return 0;
}

int 
main(int argc, char **argv)
{
  lcm_eventlog_t *read_log;
  char *log_filename;
  FILE *file1;
  char *annotation_channel_name = "LOG_ANNOTATION";
  char *pose_channel_name = "POSE";

  //Buffer for annotations & pose
  BotPtrCircular *annotation_list;
  BotPtrCircular *pose_list;
  annotation_list = bot_ptr_circular_new(ANNOTATION_LIST_SIZE, NULL, NULL);
  pose_list = bot_ptr_circular_new(POSE_LIST_SIZE, NULL, NULL);
  int64_t end_log_utime;

  srand(time(NULL));

  //Get lcm, param and frames
  lcm_t *lcm;
  if (!(lcm = bot_lcm_get_global(NULL))) {
    fprintf (stderr, "Unable to get LCM\n");
    return 0;
  }

  BotParam *param;
  if (!(param = bot_param_get_global(lcm, 0))) {
    fprintf(stderr,"No server found : Reading from file\n");
    char config_path[2048];
    sprintf(config_path, "wheelchair.cfg");
    param = bot_param_new_from_file(config_path);

    if(!param){
      fprintf (stderr, "Unable to get BotParam instance\n");
      return 0;
    }
  }

  BotFrames *frames;
  if (!(frames = bot_frames_get_global (lcm, param))) {
    fprintf (stderr, "Unable to get BotFrames instance\n");
    return 0;
  }
  

  if(argc < 3){
    fprintf(stderr, "Provide file name.\n");
    return -1;
  }
  log_filename = argv[1];
  //open file for result writing
  file1 = fopen(argv[2], "w");
  if(!file1){
    fprintf(stderr, "Cannot open file.\n");
    return -1;
  }

  if(log_filename){
    read_log = lcm_eventlog_create(log_filename, "r");
    fprintf(stderr, "Log open success.\n");
  }

  lcm_eventlog_event_t *event = (lcm_eventlog_event_t *)calloc(1, sizeof(lcm_eventlog_event_t));
  for(event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){
    int decode_status = 0;
    //Laser annotation messages
    if(strcmp(annotation_channel_name, event->channel) == 0){
      erlcm_annotation_t *msg = (erlcm_annotation_t *)calloc(1, sizeof(erlcm_annotation_t));
      decode_status = erlcm_annotation_t_decode(event->data, 0, event->datalen, msg);
      if(decode_status < 0){
	fprintf(stderr, "Error decoding message\n");
	return -1;
      }
      fprintf(stderr, "utime: %lld, start utime: %lld, end utime: %lld, annotation: %s\n", msg->utime, msg->start_utime, msg->end_utime, msg->annotation);
      //Add to the circular buffer
      bot_ptr_circular_add(annotation_list, msg);
    }
    //Pose messages
    if(strcmp(pose_channel_name, event->channel) == 0){
      bot_core_pose_t *msg = (bot_core_pose_t *)calloc(1, sizeof(bot_core_pose_t));
      decode_status = bot_core_pose_t_decode(event->data, 0, event->datalen, msg);
      if(decode_status < 0){
	fprintf(stderr, "Error decoding message\n");
	return -1;
      }

      //Convert lcm message into a struct
      Pose *p = (Pose *)calloc(1, sizeof(Pose));
      double position[3];
      int trans = bot_frames_transform_vec(frames, "body", "local", msg->pos, position);
      if(trans == 0){
	fprintf(stderr, "Transformation failed.\n");
	return -1;
      }
      p->xy[0] = position[0];
      p->xy[1] = position[1];
      p->utime = msg->utime;

      //Add to the circular buffer
      bot_ptr_circular_add(pose_list, p);
      free(msg);

      end_log_utime = p->utime;
    }
    lcm_eventlog_free_event(event);
  }			   

  fprintf(stderr, "Annotation buffer size: %d, Pose buffer size: %d\n", bot_ptr_circular_size(annotation_list), bot_ptr_circular_size(pose_list));


  //Run simulations
  int count[NO_CLASS];
  for(int i = 0;i < NO_CLASS;i++){
    count[i] = 0;
  }

  while(1){

    if(check_num(count))
      break;

    //Get a random index in pose buffer
    int pose_length = bot_ptr_circular_size(pose_list);
    double rand_val = (double)rand() / (RAND_MAX);
    int rand_sample = (int)(rand_val * pose_length);
    Pose *pose = (Pose *)bot_ptr_circular_index(pose_list, rand_sample);
    int64_t rand_utime = pose->utime;

    
    //Get the next pose that has a distance > 0.2m
    Pose *next = NULL;
    for(int i = rand_sample; i < pose_length;i++){
      Pose *p = (Pose *)bot_ptr_circular_index(pose_list, i);
      int64_t u_time = p->utime;
      double dist = hypot(p->xy[0] - pose->xy[0], p->xy[1] - pose->xy[1]);
      //Change the dist here.
      if(dist >= 0.5){
	next = p;
	break;
      }
    }

    //If next pose exists
    if(next){
      char *this_annotation = NULL;
      char *next_annotation = NULL;
      erlcm_annotation_t *msg = (erlcm_annotation_t *)calloc(1, sizeof(erlcm_annotation_t));
      for(int i = 0;i < bot_ptr_circular_size(annotation_list);i++){
	msg = bot_ptr_circular_index(annotation_list, i);
	if(msg->start_utime <= rand_utime && msg->end_utime >= rand_utime)
	  this_annotation = msg->annotation;
	if(msg->start_utime <= next->utime && msg->end_utime >= next->utime)
	  next_annotation = msg->annotation;
      }
      free(msg);
      if(this_annotation && next_annotation){
	//Determine the start & end place in int
	char *classes[NO_CLASS];
	classes[0] = "elevator";
	classes[1] = "conference_room";
	classes[2] = "office";
	classes[3] = "lab";
	classes[4] = "open_area";
	classes[5] = "hallway";
	classes[6] = "corridor";
	classes[7] = "large_meeting_room";
	int start_place;
	int end_place;
	for(int i = 0;i < NO_CLASS;i++){
	  if(strcmp(classes[i], this_annotation) == 0)
	    start_place = i + 1;
	  if(strcmp(classes[i], next_annotation) == 0)
	    end_place = i + 1;
	}
	if(count[start_place - 1] >= 400)
	  continue;
	count[start_place - 1]++;
	fprintf(file1, "%d %d\n", start_place, end_place);
      }
    }
  }

  fclose(file1);  
}

