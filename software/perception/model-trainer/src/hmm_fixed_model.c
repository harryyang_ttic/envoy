//Subscribe to place_classification_t messages and apply HMM.
//Output: HMM_place_classification channel & HMM_place_classification_debug_t channel


#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>
#include <math.h>

#include <stdlib.h>
#include <stdio.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <svm/svm.h>

#define NO_CLASS 8
#define ANNOTATION_BIN_SIZE 100

#define self_transition 0.9
#define correct_observation 0.6

typedef struct svm_model svm_model_t;
typedef struct svm_node svm_node_t;

typedef struct _class_t{
    int id;
    double p;
} class_t;

typedef struct _belief_t{
    int no_classes;
    class_t *classes;
} belief_t;

typedef struct
{
    lcm_t      *lcm;
    GMainLoop *mainloop;

    int use_fixed_model;

    erlcm_place_classification_t *last_classification;
    bot_core_pose_t *last_pose;   //Most recent pose
    bot_core_pose_t *pose_at_last_classification;    //pose at last classification utime
    bot_core_pose_t *first_pose;   //First pose message

    svm_model_t *model;
    belief_t belief_dist;
    int no_classes;
    int *class_labels;

    double **transition_p;
    double **observation_p;
    double *belief;

    int use_kld;
    /*FILE *f0;  //File for observation histograms
    FILE *f1;  //File for stationary transition model
    FILE *f2; //File for moving transition model
    FILE *f3;

    double p[NO_CLASS][NO_CLASS];  //Probability from histogram
    double transition[NO_CLASS][NO_CLASS][2]; //Probability of transition model
    double alter_transition[NO_CLASS][NO_CLASS][3];  //Another option of transition model
    double belief[NO_CLASS];

    int transition_option;    //Determine to use three_class transition model or 2 class transition model collected from data. 
    int histogram_option; //Determine use handcraft or data collected histograms
    */
    int num_annotations;
    int correct;
    int total;
    BotPtrCircular *annotation_list;
    int doorway;

} state_t;

char* get_class_name_from_id(int class_id){
    char *class_name = NULL;
    if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_DOORWAY){
        class_name = "Doorway";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_CONFERENCE_ROOM){
        class_name = "Conference Room";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_OFFICE){
        class_name = "Office";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_LAB){
        class_name = "Lab";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_OPEN_AREA){
        class_name = "Open Area";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_HALLWAY){
        class_name = "Hallway";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_ELEVATOR){
        class_name = "Elevator";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_CORRIDOR){
        class_name = "Corridor"; //what is the difference here??
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_LARGE_MEETING_ROOM){
        class_name = "Large Meeting Room";
    }
    else{        
        class_name = "Unknown";
    }
    return class_name;
}

void init_transition_fixed(state_t *self, int no_classes){
    self->transition_p = (double **) calloc(no_classes, sizeof(double *));
    double p_of_changing = (1 - self_transition) / (no_classes - 1);
    for(int i=0; i < no_classes; i++){
        self->transition_p[i] = (double *) calloc(no_classes, sizeof(double));

        for(int j=0; j < no_classes; j++){
            if(i==j){
                self->transition_p[i][j] = self_transition;
            }
            else{
                self->transition_p[i][j] = p_of_changing;
            }
        }
    }
}

void init_observation_fixed(state_t *self, int no_classes){
    self->observation_p = (double **) calloc(no_classes, sizeof(double *));
    double false_prob = (1 - correct_observation) / (no_classes - 1);
    for(int i=0; i < no_classes; i++){
        self->observation_p[i] = (double *) calloc(no_classes, sizeof(double));

        for(int j=0; j < no_classes; j++){
            if(i==j){
                self->observation_p[i][j] = correct_observation;
            }
            else{
                self->observation_p[i][j] = false_prob;
            }
        }
    }
}


void init_belief_uniform(state_t *self, int no_classes, int *class_labels){
    self->belief_dist.no_classes = no_classes;
    self->belief_dist.classes = (class_t *) calloc(no_classes, sizeof(class_t));
    self->belief = (double *) calloc(no_classes, sizeof(double));
    double val = 1.0/ no_classes;
    for(int i=0; i< no_classes; i++){
        self->belief_dist.classes[i].id = class_labels[i];
        self->belief_dist.classes[i].p = val;
        self->belief[i] = val;
    }
}

void print_belief(state_t *self){
    fprintf(stderr, "Belief Dist\n");
    for(int i=0; i< self->belief_dist.no_classes; i++){
        fprintf(stderr, "\tClass [%d] : %.3f\n", self->belief_dist.classes[i].id, 
                self->belief_dist.classes[i].p);
    }
    fprintf(stderr, "--------------------------------\n");
}

static void
on_classification(const lcm_recv_buf_t *rbuf, const char *channel,
		  const erlcm_place_classification_t *msg, void *user)
{
    state_t *self = (state_t *)user;
    if(self->last_classification != NULL)
        erlcm_place_classification_t_destroy(self->last_classification);
  
    self->last_classification = erlcm_place_classification_t_copy(msg);

    if(msg->no_class != self->no_classes) {
        fprintf(stderr, "No of classes do not agree with the model - returning\n");
        return;
    }
 
    double *classification_prob = (double *) calloc(msg->no_class, sizeof(double));

    fprintf(stderr, "Observation Prob\n");
    for(int i = 0;i < self->no_classes;i++){
        int class_id = self->class_labels[i];
        for(int j=0; j < self->no_classes; j++){
            if(msg->classes[j].name == class_id){
                //this needs to go in the same order as the class labels 
                classification_prob[i] = self->last_classification->classes[j].probability;
                break;
            }
        }
        fprintf(stderr, "[%d] - Class [%d] => %.3f\n", i, class_id, classification_prob[i]);
    }


    double *observation_model = (double *) calloc(msg->no_class, sizeof(double));

    //the observation model needs to use the test data - otherwise the KL divergence will be wrong 
    //Use the histograms collected from data and use kl-divergence. 
    if(self->use_kld){
        double *kld = (double *) calloc(msg->no_class, sizeof(double));//[NO_CLASS];
        //Use KL_divergence to measure similarity
        for(int i = 0;i < msg->no_class;i++){
            double value = 0;
            for(int j = 0;j < msg->no_class;j++){
                double v = classification_prob[j] * log2((double)classification_prob[j] /(self->observation_p[i][j] + 0.0001));
                value += v;
            }
            kld[i] = value;
        }
        
        //compute observation model, e to the pow of -sqrt(abs(kld)).
        for(int i = 0;i < msg->no_class;i++){
            observation_model[i] = pow(M_E, (0 - fabs(kld[i])));
        }

        free(kld);
    }
    //only use histograms as the observation model, do not use SVM's probability
    else{
        /*double max_prob = 0;
        int max_class = -1;

        for(int j=0; j < self->no_classes; j++){
            if(max_prob < self->last_classification->classes[j].probability){
                max_prob = self->last_classification->classes[j].probability;
                max_class = j;
            }
        }
        for(int i = 0;i < self->no_classes;i++){
            observation_model[i] = self->observation_p[i][max_class];
            }*/

        for(int i = 0;i < self->no_classes;i++){
            int class_id = self->class_labels[i];
            for(int j=0; j < self->no_classes; j++){
                if(msg->classes[j].name == class_id){
                    observation_model[i] =  self->last_classification->classes[j].probability;
                    break;
                }
            }
        }
    }

    double *current_belief = (double *) calloc(msg->no_class, sizeof(double));
    double belief_sum = 0;
    double *transition_model_value = (double *) calloc(msg->no_class, sizeof(double));

    for(int i = 0;i < self->no_classes;i++){
        transition_model_value[i] = 0;
        for(int j=0; j < self->no_classes; j++){
            transition_model_value[i] += self->transition_p[j][i] * self->belief[j];
        }
        //Update belief, Add small constant to observation model
        current_belief[i] = (observation_model[i] + 0.00001) * transition_model_value[i];
        belief_sum += current_belief[i];
    }

    //Store a copy of last belief to publish debug messages later

    double *previous_belief_value = (double *) calloc(msg->no_class, sizeof(double));
    for(int i = 0;i < self->no_classes;i++){
        previous_belief_value[i] = self->belief[i];
    }
    
    //Normalize & update belief for next HMM. 
    for(int i = 0;i < self->no_classes;i++){
        self->belief[i] = (double)current_belief[i] / belief_sum;
        fprintf(stderr, "Current Belief %s [%d] - %.3f\n", get_class_name_from_id(self->class_labels[i]),
                self->class_labels[i], self->belief[i]);
    }

    //Publish the HMM_place_classification messages & HMM_place_classification_debug message
    /*erlcm_place_classification_t message;
    erlcm_HMM_place_classification_debug_t debug_msg;
    message.utime = bot_timestamp_now();
    char *label;
    char *classes[NO_CLASS];
    classes[0] = "elevator";
    classes[1] = "conference_room";
    classes[2] = "office";
    classes[3] = "lab";
    classes[4] = "open_area";
    classes[5] = "hallway";
    classes[6] = "corridor";
    classes[7] = "large_meeting_room";


    //If include doorway class & place classification messages predicts doorway
    if(self->doorway && strcmp(self->last_classification->label, "doorway") == 0){
        fprintf(stderr, "HMM found a possible doorway.\n");
        message.label = "doorway";
        message.pose_utime = self->last_classification->pose_utime;
        message.sensor_utime = message.pose_utime;
        message.sensor_type = self->last_classification->sensor_type;
        message.no_class = self->last_classification->no_class;
        message.classes = (erlcm_place_classification_class_t *)calloc(message.no_class, sizeof(erlcm_place_classification_class_t));
        for(int i = 0; i < message.no_class - 1;i++){
            message.classes[i].name = i + 1;
            message.classes[i].probability = self->belief[i];
        }
        //only set the doorway class probability to 1.
        message.classes[message.no_class - 1].name = message.no_class;
        message.classes[message.no_class - 1].probability = 1;
    }
    //else
    else{
        double highest = -1;
        int index = -1;
        for(int i = 0;i < NO_CLASS;i++){
            if(self->belief[i] > highest){
                highest = self->belief[i];
                index = i;
            }
        }

        message.label = classes[index];
        message.pose_utime = self->last_classification->pose_utime;
        message.sensor_utime = message.pose_utime;
        message.sensor_type = self->last_classification->sensor_type;
        message.no_class = self->last_classification->no_class;
        message.classes = (erlcm_place_classification_class_t *)calloc(message.no_class, sizeof(erlcm_place_classification_class_t));
        for(int i = 0; i < message.no_class;i++){
            message.classes[i].name = i + 1;
            message.classes[i].probability = self->belief[i];
        }
    }
    //Check whether this is a correct prediction
    if(self->num_annotations){
        char *real_class;
        for(int i = 0;i < bot_ptr_circular_size(self->annotation_list);i++){
            erlcm_annotation_t *iter = (erlcm_annotation_t *)bot_ptr_circular_index(self->annotation_list, i);
            int64_t start_utime = iter->start_utime;
            int64_t end_utime = iter->end_utime;
            if(start_utime <= message.sensor_utime && end_utime >= message.sensor_utime){
                real_class = iter->annotation;
                break;
            }
        }
        if(strcmp(real_class, message.label) == 0){
            fprintf(stderr, "Correct prediction: %s\n", real_class);
            self->correct++;
        }
        else{
            fprintf(stderr, "Wrong prediction. Prediction by classfier: %s, Real: %s\n" ,message.label, real_class);
        }
        self->total++;
        fprintf(stderr, "Accuracy so far: %.2f\n", (double)self->correct / self->total);
    }

    debug_msg.utime = message.utime;
    debug_msg.sensor_utime = message.sensor_utime;
    debug_msg.no_class = message.no_class;
    debug_msg.label = message.label;
    debug_msg.model_type = transition_model;

    debug_msg.prior = (erlcm_place_classification_class_t *)calloc(debug_msg.no_class, sizeof(erlcm_place_classification_class_t));
    debug_msg.transition = (erlcm_place_classification_class_t *)calloc(debug_msg.no_class, sizeof(erlcm_place_classification_class_t));
    debug_msg.observation = (erlcm_place_classification_class_t *)calloc(debug_msg.no_class, sizeof(erlcm_place_classification_class_t));
    debug_msg.without_HMM = (erlcm_place_classification_class_t *)calloc(debug_msg.no_class, sizeof(erlcm_place_classification_class_t));
  
    for(int i = 0; i < message.no_class;i++){
        debug_msg.prior[i].name = i + 1;
        debug_msg.prior[i].probability = previous_belief_value[i];
        debug_msg.transition[i].name = i + 1;
        debug_msg.transition[i].probability = transition_model_value[i];
        debug_msg.observation[i].name = i + 1;
        debug_msg.observation[i].probability = observation_model[i];
        debug_msg.without_HMM[i].name = i + 1;
        debug_msg.without_HMM[i].probability = self->last_classification->classes[i].probability;
    }
    erlcm_place_classification_t_publish(self->lcm, "HMM_PLACE_CLASSIFICATION", &message);
    erlcm_HMM_place_classification_debug_t_publish(self->lcm, "HMM_PLACE_CLASSIFICATION_DEBUG", &debug_msg);*/
    erlcm_place_classification_t *hmm_msg = erlcm_place_classification_t_copy(msg);
    for(int i = 0;i < self->no_classes;i++){
        int class_id = self->class_labels[i];
        for(int j = 0;j < self->no_classes;j++){
            if(hmm_msg->classes[j].name == class_id){
                hmm_msg->classes[j].probability = self->belief[i];
            }
            break;
        }
    }
    erlcm_place_classification_t_publish(self->lcm, "HMM_PLACE_CLASSIFICATION", hmm_msg);
    erlcm_place_classification_t_destroy(hmm_msg);
}


static void
on_pose(const lcm_recv_buf_t *rbuf, const char *channel,
	const bot_core_pose_t *msg, void *user){
    state_t *self = (state_t *)user;
    if(self->last_pose != NULL)
        bot_core_pose_t_destroy(self->last_pose);
    self->last_pose = bot_core_pose_t_copy(msg);

    //Update the first pose message in the log
    if(self->first_pose == NULL)
        self->first_pose = bot_core_pose_t_copy(msg);
}


void subscribe_to_channels(state_t *self)
{
    erlcm_place_classification_t_subscribe(self->lcm, "PLACE_CLASSIFICATION", on_classification, self);
    bot_core_pose_t_subscribe(self->lcm, "POSE", on_pose, self);
}  


void
circ_free_annotation(void *user, void *p){
    erlcm_annotation_t *np = (erlcm_annotation_t *)p;
    erlcm_annotation_t_destroy(np);
}


static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -o option  Specify observation histogram option. 0:KL divergence 1:histograms directly\n"
             "  -l LOG     Specify Log File\n"
             "  -d         Consider doorways.\n"
             "  -t option  Specify transition model option. 0:stationary,motion 1:stationary,slow_motion,fast_motion\n"
             "  -h         This help message\n", 
             g_path_get_basename(progname));
    exit(1);
}

//We should prob have the raw classification happen here at some point 
int 
main(int argc, char **argv)
{

    const char *optstring = "km:fo:l:pdt:h";

    state_t *state = (state_t *) calloc(1, sizeof(state_t));

    int c;
    int histogram = 1;
    int transition = 0;
    int doorway = 0;
    char *log_file = NULL;
    char *model_file = NULL;
    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'k':
            state->use_kld = 1;
            break;
        case 'o': //choose observation model
            histogram = atoi(optarg);
            printf("Using histogram option \"%d\"\n", histogram);
            break;        
        case 'm': //choose observation model
            //histogram = atoi(optarg);
            model_file = strdup(optarg);
            printf("Using model file %s\n", model_file);
            break;            
        case 'f': //choose observation model
            state->use_fixed_model = 1;
            printf("Using fixed model\n");
            break;
            /*case 'l':
            log_file = strdup(optarg);  //choose log file for accuracy checking
            fprintf(stderr, "Using LOG file \"%s\"\n", log_file);
            break;
        case 'd':
            doorway = 1;
            fprintf(stderr, "Doorway option set to :%d.\n", doorway);
            break;*/
        case 't': //choose tranistion model
            transition = atoi(optarg);  //choose transition model
            printf("Using transition option \"%d\"\n", transition);
            break;
        case 'h': //help
            usage(argv[0]);
            break;
        default:
            usage(argv[0]);
            break;
        }
    }

    if(model_file == NULL){
        fprintf(stderr, "No model file specified - please specify model file to obtain no of classes\n");
        return -1;
    }

    state->model = (svm_model_t *) calloc(1, sizeof(svm_model_t));

    state->model = svm_load_model(model_file);
    if(state->model == NULL){
        fprintf(stderr, "Error loading model.\n");
        return -1;
    }
    else
        fprintf(stderr, "Model loaded success.\n");

    state->no_classes = svm_get_nr_class(state->model);
    state->class_labels = (int *) calloc(state->no_classes, sizeof(int));

    svm_get_labels(state->model, state->class_labels);

    init_belief_uniform(state, state->no_classes, state->class_labels);
    
    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);

    subscribe_to_channels(state);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }

    if(state->use_fixed_model){
        //use standard transition and observation probabilities 
        init_transition_fixed(state, state->no_classes);
        init_observation_fixed(state, state->no_classes);
    }
    else{
        fprintf(stderr, "Only fixed model supported for now\n");
        return -1;        
    } 
        

    /*state->histogram_option = histogram;
    state->transition_option = transition;
    state->doorway = doorway;

    if(!state->transition_option){
        state->f0 = fopen("observation_model.txt", "r");
        state->f1 = fopen("stationary_model.txt", "r");
        state->f2 = fopen("move_model.txt", "r");
        if(!state->f0 || !state->f1 || !state->f2){
            fprintf(stderr, "Cannot open file.\n");
            return -1;
        }
    }
    else{
        state->f0 = fopen("observation_model.txt", "r");
        state->f1 = fopen("3_stationary.txt", "r");
        state->f2 = fopen("3_slow.txt", "r");
        state->f3 = fopen("3_fast.txt", "r");
        if(!state->f0 || !state->f1 || !state->f2 || !state->f3){
            fprintf(stderr, "Cannot open file.\n");
            return -1;
        }
    }

  
    //Compute the histogram
    if(!state->histogram_option)
        fprintf(stderr, "Using KL divergence function for observation model.\n");  
    else
        fprintf(stderr, "Using histograms directly as observation model.\n");
    

    double *v = compute_histogram(state->f0);
    for(int i = 0;i < NO_CLASS;i++){
        for(int j = 0;j < NO_CLASS;j++){
            state->p[i][j] = v[i * NO_CLASS + j];
        }      
    }

    //Compute the transition probability
    double *trans = compute_transition(state->f1, state->f2, state->f3, state->transition_option);
    if(!state->transition_option){
        for(int i = 0;i < NO_CLASS;i++){
            for(int j = 0;j < NO_CLASS;j++){
                state->transition[i][j][0] = trans[i * NO_CLASS * 2 + j * 2];
                state->transition[i][j][1] = trans[i * NO_CLASS * 2 + j * 2 + 1];
            }
        }
    }
    //(stationary, slow_motion, fast_motion) transition model
    else{
        for(int i = 0;i < NO_CLASS;i++){
            for(int j = 0;j < NO_CLASS;j++){
                state->alter_transition[i][j][0] = trans[i * NO_CLASS * 3 + j * 3];
                state->alter_transition[i][j][1] = trans[i * NO_CLASS * 3 + j * 3 + 1];
                state->alter_transition[i][j][2] = trans[i * NO_CLASS * 3 + j * 3 + 2];
            }
        }
    }
    */

    //Initialize the belief as uniform distribution
    /*for(int i = 0;i < NO_CLASS;i++){
        state->belief[i] = (double)1 / NO_CLASS;
        }*/
    
    state->last_pose = NULL;
    state->pose_at_last_classification = NULL;
    state->first_pose = NULL; 

    /*state->num_annotations = 0;
    //Load log into memory for accuracy checking
    if(log_file){
        if(state->doorway)
            fprintf(stderr, "HMM includes doorway class. Ensure your annotation log has doorways.\n");
        fprintf(stderr, "System is loading the annotation messages in log for accuracy checking.Please wait a while.\n");
        lcm_eventlog_t *read_log = lcm_eventlog_create(log_file, "r");
        fprintf(stderr, "Log open success.\n");
        state->annotation_list = bot_ptr_circular_new(ANNOTATION_BIN_SIZE, circ_free_annotation, state);

        state->correct = 0;
        state->total = 0;
  
        //Adding to annotation circular buffer
        lcm_eventlog_event_t *event = (lcm_eventlog_event_t *)calloc(1, sizeof(lcm_eventlog_event_t));
        char *annotation_channel_name = "LOG_ANNOTATION";
        for(event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){
            int decode_status = 0;
            if(strcmp(annotation_channel_name, event->channel) == 0){
                erlcm_annotation_t *msg = (erlcm_annotation_t *)calloc(1, sizeof(erlcm_annotation_t));
                decode_status = erlcm_annotation_t_decode(event->data, 0, event->datalen, msg);
                if(decode_status < 0){
                    fprintf(stderr, "Error decoding message.\n");
                    return -1;
                }
      
                bot_ptr_circular_add(state->annotation_list, msg);
                state->num_annotations++;
            }
        }
        fprintf(stderr, "No. of annotations in the log: %d\n", state->num_annotations);
        free(event);
        if(state->num_annotations)
            fprintf(stderr, "Finish loading annotations in the log.\n");
        else
            fprintf(stderr, "No annotation messages in the log.\n");
            }*/


    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    //read_parameters_from_conf(state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    //    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}
