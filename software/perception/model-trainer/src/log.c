//Play back the log and compute feature values. This avoids missing feature values due to delay in file writing.
//Note: If log does not have SKIRT_SIM message, you need to run er-simulate-360-laser

#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>

#include <laser_features/laser_feature.h>

int 
main(int argc, char **argv)
{
  lcm_eventlog_t *read_log;
  char *log_filename;
  FILE* file;
  char *laser_annotation_channel_name = "LASER_ANNOTATION";
  if(argc < 3){
    fprintf(stderr, "Provide file names.\n");
    return -1;
  }
  log_filename = argv[1];
  file = fopen(argv[2], "w");
  if(!file){
    fprintf(stderr, "Cannot open file.\n");
  }
  if(log_filename){
    read_log = lcm_eventlog_create(log_filename, "r");
  }
  lcm_eventlog_event_t *laser_annotation_event = NULL;
  for(lcm_eventlog_event_t *event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){
    int decode_status = 0;
    if(strcmp(laser_annotation_channel_name, event->channel) == 0){
//      fprintf(stderr, "Decoding event: %s\n", event->channel);
      erlcm_laser_annotation_t *msg = (erlcm_laser_annotation_t *) calloc(1, sizeof(erlcm_laser_annotation_t));
      decode_status = erlcm_laser_annotation_t_decode(event->data, 0, event->datalen, msg);
      if (decode_status < 0)
	fprintf (stderr, "Error %d decoding message\n", decode_status);
      bot_core_planar_lidar_t laser = msg->laser;
      char *annotation = msg->annotation;
      int label;
      char *s1 = "elevator";
      char *s2 = "conference_room";
      char *s3 = "office";
      char *s4 = "lab";
      char *s5 = "open_area";
      char *s6 = "hallway";
      char *s7 = "corridor";
      char *s8 = "large_meeting_room";
      //    fprintf(stderr, "Test annotation: %s", annotation);
      if(strncmp(annotation, s1, 100) == 0){
	label = 1;
      }
      else if(strncmp(annotation, s2, 100) == 0){
	label = 2;
      }
      else if(strncmp(annotation, s3, 100) == 0){
	label = 3;
      }
      else if(strncmp(annotation, s4, 100) == 0){
	label = 4;
      }
      else if(strncmp(annotation, s5, 100) == 0){
	label = 5;
      }
      else if(strncmp(annotation, s6, 100) == 0){
	label = 6;
      }
      else if(strncmp(annotation, s7, 100) == 0){
	label = 7;
      }
      else if(strncmp(annotation, s8, 100) == 0){
	label = 8;
      }

      //Change the feature configurations here. min_range, max_range, threshold, etc...
      feature_config_t config;
      config.min_range = 0.1;
      config.max_range = 30;
      config.cutoff = 5;
      config.gap_threshold_1 = 3;
      config.gap_threshold_2 = 0.5;
      config.deviation = 0.001;
      config.no_of_fourier_coefficient = 2;


      raw_laser_features_t *raw_features = extract_raw_laser_features(&laser, config);
      polygonal_laser_features_t *polygonal_features = extract_polygonal_laser_features(&laser, config);
      fprintf(file, "%d %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f ", label, 1, raw_features->avg_difference_consecutive_beams, 2, raw_features->std_dev_difference_consecutive_beams, 3, raw_features->cutoff_avg_difference_consecutive_beams, 4, raw_features->avg_beam_length, 5, raw_features->std_dev_beam_length, 6, raw_features->no_gaps_in_scan_threshold_1, 7, raw_features->no_relative_gaps, 8, raw_features->no_gaps_to_num_beam, 9, raw_features->no_relative_gaps_to_num_beam, 10, raw_features->distance_between_two_smalled_local_minima, 11, raw_features->index_distance_between_two_smalled_local_minima, 12, raw_features->avg_ratio_consecutive_beams, 13, raw_features->avg_to_max_beam_length, 14, polygonal_features->area, 15, polygonal_features->perimeter, 16, polygonal_features->area_to_perimeter, 17, polygonal_features->PI_area_to_sqrt_perimeter, 18, polygonal_features->PI_area_to_perimeter_pow, 19, polygonal_features->sides_of_polygon);
      for(int i = 0; i <= config.no_of_fourier_coefficient * 2; i++){
	//	fprintf(stderr, "Testing: %f\n", polygonal_features->fourier_transform_descriptors[1].results->real);
	fprintf(file, "%d:%f %d:%f ", 20 + i * 2, polygonal_features->fourier_transform_descriptors[0].results->real, 21 + i * 2, polygonal_features->fourier_transform_descriptors[0].results->complex);
      }
      int start = config.no_of_fourier_coefficient * 4 + 22;
      fprintf(file, "%d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f %d:%f\n", start, polygonal_features->major_ellipse_axis_length_coefficient, start + 1, polygonal_features->minor_ellipse_axis_length_coefficient, start + 2, polygonal_features->major_to_minor_coefficient, start + 3, polygonal_features->central_moment_invariants[0], start + 4, polygonal_features->central_moment_invariants[1], start + 5, polygonal_features->central_moment_invariants[2], start + 6, polygonal_features->central_moment_invariants[3], start + 7, polygonal_features->central_moment_invariants[4], start + 8, polygonal_features->central_moment_invariants[5], start + 9, polygonal_features->central_moment_invariants[6], start + 10, polygonal_features->normalized_feature_of_compactness, start + 11, polygonal_features->normalized_feature_of_eccentricity, start + 12, polygonal_features->mean_centroid_to_shape_boundary, start + 13, polygonal_features->max_centroid_to_shape_boundary, start + 14, polygonal_features->mean_centroid_over_max_centroid, start + 15, polygonal_features->std_dev_centroid_to_shape_boundry, start + 16, polygonal_features->kurtosis);
      //   fprintf(self->file, "\n");		
    }
    lcm_eventlog_free_event(event);
  }
}


