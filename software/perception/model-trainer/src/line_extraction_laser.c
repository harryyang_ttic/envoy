//Use velodyne info to detect doorways. It can only detect doorways that are 3-5m apart. Not a classifier on spot.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>

#include <gsl/gsl_blas.h>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <velodyne/velodyne.h>
#include <velodyne/velodyne_extractor.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <GL/gl.h>
#include <point_types/point_types.h>
#include <lcmtypes/erlcm_xyz_point_list_t.h>
#include <full_laser/full_laser.h>

typedef struct _state_t state_t;

struct _state_t {
  lcm_t *lcm;
  GMainLoop *mainloop;
  BotParam   *b_server;
  full_laser_state_t *full_laser;
  BotFrames *frames;
  bot_lcmgl_t *lcmgl;
  bot_core_planar_lidar_t *last_laser;
  //GList *line_segments;
};

typedef struct _xy_t xy_t;

struct _xy_t{
  double xy[2];
};

typedef struct _line_t line_t;

struct _line_t{
  double start_xy[2];
  double end_xy[2];
  double gradient;
  double intersect;
  int start_index; 
  int end_index;
};

void destroy_xy(xy_t *point){
  free(point);
}

void destroy_line(line_t *line){
  free(line);
}

//Global variable
GList *line_segments;

static void full_laser_update_handler(int64_t utime, bot_core_planar_lidar_t *msg, void *user)
{
  state_t *s = (state_t *)user;
  if(s->last_laser != NULL){
    bot_core_planar_lidar_t_destroy(s->last_laser);
  }
  s->last_laser = bot_core_planar_lidar_t_copy(msg);
}


double calc_dist(double gradient, double intersect, double x, double y)
{
  double a = gradient;
  int b = -1;
  double c = intersect;
  double dist = fabs(a * x + b * y + c) / (double)hypot(a, b);
  return dist;
}

double calc_gradient(double start[2], double end[2])
{
  double gradient = (double)(end[1] - start[1]) / (end[0] - start[0]);
  return gradient;
}

double calc_intersect(double start[2], double gradient)
{
  double intersect = start[1] - gradient * start[0];
  return intersect;
}



//Different templates for doorway
//Reference: "Approaches to mobile robot localization in indoor environments"

int findParLines(line_t *l1, line_t *l2, bot_core_planar_lidar_t *laser, BotFrames *frame)
{

  double start1_x = l1->start_xy[0];
  double start1_y = l1->start_xy[1];
  double end1_x = l1->end_xy[0];
  double end1_y = l1->end_xy[1];

  double start2_x = l2->start_xy[0];
  double start2_y = l2->start_xy[1];
  double end2_x = l2->end_xy[0];
  double end2_y = l2->end_xy[1];

  /*
  fprintf(stderr, "\n");
  fprintf(stderr, "Gradient of line1 using end points: %f\n", calc_gradient(l1->start_xy, l1->end_xy));
  fprintf(stderr, "Gradient of line2 using end points: %f\n", calc_gradient(l2->start_xy, l2->end_xy));
  fprintf(stderr, "Gradient of line1 from struct: %f\n", l1->gradient);
  fprintf(stderr, "Gradient of line2 from struct: %f\n", l2->gradient);
  fprintf(stderr, "Length of line1: %f\n", hypot(l1->start_xy[0] - l1->end_xy[0], l1->start_xy[1] - l1->end_xy[1]));
  fprintf(stderr, "Length of line2: %f\n", hypot(l2->start_xy[0] - l2->end_xy[0], l2->start_xy[1] - l2->end_xy[1]));
  fprintf(stderr, "Dist from start point of line1 to line 2: %f\n",  calc_dist(l2->gradient, l2->intersect, start1_x, start1_y));
  fprintf(stderr, "Dist from end point of line1 to line 2: %f\n",  calc_dist(l2->gradient, l2->intersect, end1_x, end1_y));
  fprintf(stderr, "Dist from start point of line2 to line1: %f\n",  calc_dist(l1->gradient, l1->intersect, start2_x, start2_y));
  fprintf(stderr, "Dist from end point of line2 to line 1: %f\n",  calc_dist(l1->gradient, l1->intersect, end2_x, end2_y));
  */

  /*
   * Some constants to determine whether the two lines can form a doorway.
   * Can play with constant to improve accuracy
   */

  //dist from start1 to second line
  double dist;
  dist = calc_dist(l2->gradient, l2->intersect, start1_x, start1_y);
  if(dist > 0.15)
    return 0;

  //end1 to second line
  dist = calc_dist(l2->gradient, l2->intersect, end1_x, end1_y);
  if(dist > 0.15)
    return 0;

  //dist from start2 to first line
  dist = calc_dist(l1->gradient, l1->intersect, start2_x, start2_y);
  if(dist > 0.15)
    return 0;

  //end2 to first line
  dist = calc_dist(l1->gradient, l1->intersect, end2_x, end2_y);
  if(dist > 0.15)
    return 0;

  //If gradient differ to much
  if(fabs(l1->gradient - l2->gradient) > 0.1)
    return 0;

  //If can continue until here, two lines are approximately at the same line, check dist
  //between points
  double dist1 = hypot(l1->start_xy[0] - l2->end_xy[0], l1->start_xy[1] - l2->end_xy[1]);
  double dist2 = hypot(l1->end_xy[0] - l2->start_xy[0], l1->end_xy[1] - l2->start_xy[1]);
  if(!(dist1 > 0.5 && dist1 < 1.5) && !(dist2 > 0.5 && dist2 < 1.5))
    return 0;
  
  //Check whether half of points between start_index & end_index are 0.2m away from the door line
  int start = -1;
  int end = -1;

  if(l1->start_index > l2->end_index){
    start = l2->end_index;
    end = l1->start_index;
  }
  else if(l2->start_index > l1->end_index){
    start = l1->end_index;
    end = l2->start_index;
  }

  fprintf(stderr, "start index: %d, end index: %d\n", start, end);

  int count = 0;
  for(int i = start;i <= end;i++){
    double xyz_sensor1[3];
    double theta1 = laser->rad0 + i * laser->radstep;
    xyz_sensor1[0] = laser->ranges[i] * cos(theta1);
    xyz_sensor1[1] = laser->ranges[i] * sin(theta1);
    xyz_sensor1[2] = 0;
    double xyz_local1[3];
    bot_frames_transform_vec(frame, "SKIRT_FRONT", "local", xyz_sensor1, xyz_local1);
    //Distance between the point & the line
    double dist = calc_dist(l1->gradient, l1->intersect, xyz_local1[0], xyz_local1[1]);
    //0.2m apart from the doorway line
    if(dist > 0.15)
      count++;
  }

  if(end - start < 20)
    return 0;

  if(count >= (end - start) / 2)
    return 1;
  else
    return 0;

}








//perform a regression fit of a straight line to a set of points xy
//reference: http://mathpages.com/home/kmath110.htm
line_t *extract_line_from_points(bot_core_planar_lidar_t *laser, int start_index, int end_index, 
				 BotFrames *frame)
{
  xy_t points[end_index - start_index + 1];
  double sumx = 0;
  double sumy = 0;
  for(int i = start_index;i <= end_index;i++){
    double xyz_sensor1[3];
    double theta1 = laser->rad0 + i * laser->radstep;
    xyz_sensor1[0] = laser->ranges[i] * cos(theta1);
    xyz_sensor1[1] = laser->ranges[i] * sin(theta1);
    xyz_sensor1[2] = 0;
    double xyz_local1[3];
    bot_frames_transform_vec(frame, "SKIRT_FRONT", "local", xyz_sensor1, xyz_local1);
    points[i - start_index].xy[0] = xyz_local1[0];
    points[i - start_index].xy[1] = xyz_local1[1];
    sumx += xyz_local1[0];
    sumy += xyz_local1[1];
  }
  double meanx = (double)sumx / (end_index - start_index + 1);
  double meany = (double)sumy / (end_index - start_index + 1);
  
  //move centroid to origin
  for(int i = 0;i <= end_index - start_index;i++){
    points[i].xy[0] -= meanx;
    points[i].xy[1] -= meany;
  }
  
  //calculate sum of x^2-y^2 and sum of xy and A
  double sum1 = 0;
  double sum2 = 0;
  for(int i = 0;i < end_index - start_index;i++){
    sum1 += ((points[i].xy[0] * points[i].xy[0]) - (points[i].xy[1] * points[i].xy[1]));
    sum2 += (points[i].xy[0] * points[i].xy[1]);
  }
  double quadratic = (double)sum1 / sum2;
  
  //calculate gradient
  double gradient1 = ((0 - quadratic) + sqrt(pow(quadratic, 2) - 4 * (-1))) / 2;
  double gradient2 = ((0 - quadratic) - sqrt(pow(quadratic, 2) - 4 * (-1))) / 2;
  double gradient;

  //Determine which gradient is maximum & which is minimum
  double dist1 = 0;
  double dist2 = 0;
  for(int i = 0;i < end_index - start_index;i++){
    dist1 += fabs(gradient1 * points[i].xy[0] - points[i].xy[1]) / sqrt(1 + gradient1 * gradient1);
    dist2 += fabs(gradient2 * points[i].xy[0] - points[i].xy[1]) / sqrt(1 + gradient2 * gradient2);
  }

  if(dist1 <= dist2)
    gradient = gradient1;
  else
    gradient = gradient2;

  //get intersect
  double intersect = meany - gradient * meanx;

  //Return the line struct
  line_t *line = (line_t *)calloc(1, sizeof(line_t));
  line->start_xy[0] = points[0].xy[0] + meanx;
  line->start_xy[1] = points[0].xy[1] + meany;
  line->end_xy[0] = points[end_index - start_index].xy[0] + meanx;
  line->end_xy[1] = points[end_index - start_index].xy[1] + meany;
  line->gradient = gradient;
  line->intersect = intersect;
  line->start_index = start_index;
  line->end_index = end_index;

  return line;
}


//split & merge algorithm to detect line segments
void split_and_merge(bot_core_planar_lidar_t *laser, int start_index, 
		     int end_index, double threshold, int count, BotFrames *frames)//, GList *line_segments)
{

  if(end_index - start_index < count)
    return;

  //Fit a line to points
  line_t *line = extract_line_from_points(laser, start_index, end_index, frames);
  
  double max_dist = -1;
  int index;
  for(int i = start_index; i <= end_index;i++){
    double xyz_sensor1[3];
    double theta1 = laser->rad0 + i * laser->radstep;
    xyz_sensor1[0] = laser->ranges[i] * cos(theta1);
    xyz_sensor1[1] = laser->ranges[i] * sin(theta1);
    xyz_sensor1[2] = 0;
    double xyz_local1[3];
    bot_frames_transform_vec(frames, "SKIRT_FRONT", "local", xyz_sensor1, xyz_local1);
    double dist = calc_dist(line->gradient, line->intersect, xyz_local1[0], xyz_local1[1]);
    if(dist > max_dist){
      max_dist = dist;
      index = i;
    }
  }

  //If distance is too large, split these points into two sets
  if(max_dist > threshold){
    free(line);
    split_and_merge(laser, start_index, index - 1, threshold, count, frames);//, line_segments);
    split_and_merge(laser, index + 1, end_index, threshold, count, frames);//, line_segments);
  }
  else{
    line_segments = g_list_prepend(line_segments, line);
    //fprintf(stderr, "Find a possible line segment.\n");
    return;
  }
}


void 
draw_potential_lines(bot_core_planar_lidar_t *laser, BotFrames *frames, bot_lcmgl_t *lcmgl)//, GList *line_segments)
{
  bot_lcmgl_color3f(lcmgl, 0, 0, 0);
  //bot_lcmgl_point_size(lcmgl, 4);
  bot_lcmgl_line_width(lcmgl, 5);
  
  split_and_merge(laser, 0, laser->nranges, 0.05, 25, frames);//, line_segments);

  fprintf(stderr, "Size of line segments: %d\n", g_list_length(line_segments));

  for(int i = 0;i < g_list_length(line_segments);i++){
    line_t *line = (line_t *)g_list_nth(line_segments, i)->data;
    bot_lcmgl_begin(lcmgl, GL_LINE_STRIP);
    bot_lcmgl_vertex3f(lcmgl, line->start_xy[0], line->start_xy[1], 0);
    bot_lcmgl_vertex3f(lcmgl, line->end_xy[0], line->end_xy[1], 0);
    bot_lcmgl_end(lcmgl);
    //free(line);

    for(int j = i + 1;j < g_list_length(line_segments);j++){
      line_t *line2 = (line_t *)g_list_nth(line_segments, j)->data;
      int par = findParLines(line, line2, laser, frames);
      if(par == 1){
	fprintf(stderr, "Find a pair of possible doorway.\n");
	bot_lcmgl_color3f(lcmgl, 0, 0, 0);
	bot_lcmgl_line_width(lcmgl, 20);
	bot_lcmgl_begin(lcmgl, GL_LINE_STRIP);
	bot_lcmgl_vertex3f(lcmgl, line->start_xy[0], line->start_xy[1], 0);
	bot_lcmgl_vertex3f(lcmgl, line->end_xy[0], line->end_xy[1], 0);
	bot_lcmgl_end(lcmgl);
	
	bot_lcmgl_begin(lcmgl, GL_LINE_STRIP);
	bot_lcmgl_vertex3f(lcmgl, line2->start_xy[0], line2->start_xy[1], 0);
	bot_lcmgl_vertex3f(lcmgl, line2->end_xy[0], line2->end_xy[1], 0);
	bot_lcmgl_end(lcmgl);
      }
    }
  }
  
  bot_lcmgl_switch_buffer(lcmgl);

  if(line_segments != NULL){
    g_list_free(line_segments);
    line_segments = NULL;
  }
}



//Try to capture some line segments
gboolean heartbeat_cb (gpointer data)
{
  state_t *s = (state_t *)data;

  line_segments = NULL;

  if(s->last_laser != NULL)
    draw_potential_lines(s->last_laser, s->frames, s->lcmgl);//, s->line_segments);

  //fprintf(stderr, "Size of lines before deletion: %d\n", g_list_length(line_segments));
  
  //return true to keep running
  return TRUE;
}

int 
main(int argc, char **argv)
{

  g_thread_init(NULL);
  setlinebuf (stdout);
  state_t *state = (state_t*) calloc(1, sizeof(state_t));

  state->lcm =  bot_lcm_get_global(NULL);
  state->b_server = bot_param_new_from_server(state->lcm, 1);
  state->frames = bot_frames_get_global (state->lcm, state->b_server);
  state->lcmgl = bot_lcmgl_init(state->lcm,"line-extraction");
  state->full_laser = full_laser_init(state->lcm, 540, &full_laser_update_handler, state);
  //state->line_segments = NULL;

  state->mainloop = g_main_loop_new( NULL, FALSE );  
  
  if (!state->mainloop) {
    printf("Couldn't create main loop\n");
    return -1;
  }

  //add lcm to mainloop 
  bot_glib_mainloop_attach_lcm (state->lcm);

  /* heart beat*/
  g_timeout_add (500, heartbeat_cb, state);

  //adding proper exiting 
  bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
  fprintf(stderr, "Starting Main Loop\n");

  ///////////////////////////////////////////////
  g_main_loop_run(state->mainloop);
  
  bot_glib_mainloop_detach_lcm(state->lcm);
}
