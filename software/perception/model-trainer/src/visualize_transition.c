//Visualize two transition models


#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>

#include <stdlib.h>
#include <stdio.h>

#define NO_CLASS 8

int 
main(int argc, char **argv)
{
  if(argc < 2){
    fprintf(stderr, "Provide file name.\n");
    return -1;
  }
  FILE *f1;
  f1 = fopen(argv[1], "r");
  if(!f1){
    fprintf(stderr, "Failed to open files\n");
    return -1;
  }
  else
    fprintf(stderr, "File open success\n");
  char str[5000];
  int count_stat[NO_CLASS][NO_CLASS];
  for(int i = 0;i < NO_CLASS;i++){
    for(int j = 0;j < NO_CLASS;j++){
      count_stat[i][j] = 0;
    }
  }
  while(fgets(str,sizeof(str),f1) != NULL){
    int first = atoi(&str[0]) - 1;
    int second = atoi(&str[2]) - 1;
    count_stat[second][first]++; 
  }
  fprintf(stderr, "Model:\n");
  for(int i = NO_CLASS - 1;i >= 0;i--){
    for(int j = 0; j < NO_CLASS;j++){
      if(j == 0)
	fprintf(stderr, "%d %d ", i + 1, count_stat[i][j]);
      else if(j != NO_CLASS - 1)
	fprintf(stderr, "%d ", count_stat[i][j]);
      else
	fprintf(stderr, "%d\n", count_stat[i][j]);
    }
  }
  for(int i = 0;i < NO_CLASS;i++){
    if(i == 0)
      fprintf(stderr, "  %d", i + 1);
    else if(i != NO_CLASS - 1)
      fprintf(stderr, " %d", i + 1);
    else
      fprintf(stderr, " %d\n", i + 1);
  }
  fclose(f1);
}
