//segment data into no_class data sets for one-vs-all training


#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>

#include <stdlib.h>
#include <stdio.h>

//read training data file and segment the data to equal sizes and splits to training and testing 

int 
main(int argc, char **argv)
{
  FILE *f1, *f2;
  if(argc < 3){
    fprintf(stderr, "Provide file names.\n");
    return -1;
  }
  f1 = fopen(argv[1], "r");
  //For training
  f2 = fopen(argv[2], "w");
  char str[5000];
  if(!f1) {
    fprintf(stderr, "No valid file\n"); 
    return 1;
  } // bail out if file not found
  else{
    fprintf(stderr, "File open successful.\n");
  }
  char *ch;
  
  while(fgets(str,sizeof(str),f1) != NULL){
    char *cp = strdup(str);
    ch = strtok(cp, " :");
    int i;
    float f;
    int index = 0;
    int count = 0;
    while(ch != NULL){
      //fprintf(stderr, "ch: %s\n", ch);
      
      if(sscanf(ch, "%f", &f) != 0){
	
	if(count == 0){
	  int v = (int)f;
	  fprintf(f2, "%d ", v);
	  //fprintf(stderr, "%d\n", v);
	  count++;
	  ch = strtok(NULL, " :");
	  continue;
	}
	if(index == 0){
	  int v = (int)f;
	  fprintf(f2, "%d:", v);
	  //fprintf(stderr, "%d\n", v);
	  index++;
	  ch = strtok(NULL, " :");
	  continue;
	}
	else if(index == 1){
	  if(f < 0)
	    f = fabs(f);
	  fprintf(f2, "%f ", f);
	  //fprintf(stderr, "%f\n", f);
	  index--;
	  ch = strtok(NULL, " :");
	}
      }
    }
    fprintf(f2, "\n");
    /*
    char *c;
    fprintf(f2, "%s ", ch);
    ch = strtok(NULL, " ");
    while(ch != NULL){
      
      c = strtok(ch, ":");
      int count = 0;
      while(c != NULL){
	if(count == 0){
	  int v = atoi(c);
	  fprintf(f2, "%d:", v);
	  count++;
	}
	else{
	  double value = atof(c);
	  if(value < 0)
	    value = fabs(value);
	  fprintf(f2, "%f ", value);
	}

	c = strtok(NULL, ":");
      }

      ch = strtok(NULL, " ");
    }
    fprintf(f2, "\n");
    */
  }
    
  fclose(f1);
  fclose(f2);
}
