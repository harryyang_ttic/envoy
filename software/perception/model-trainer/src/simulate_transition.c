//This script reads a log and simualte transition probabilty from it. 

#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
#include <time.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <bot_frames/bot_frames.h>

#define ANNOTATION_LIST_SIZE 50000
#define POSE_LIST_SIZE 50000
#define NO_CLASS 8

typedef struct _LaserAnnotation LaserAnnotation;
struct _LaserAnnotation{
  char *annotation;
  int64_t utime;
};

typedef struct _Pose Pose;
struct _Pose{
  double xy[2];
  int64_t utime;
};

//Check each class have at least 100 simulation results
int check_num(int count[NO_CLASS]){
  for(int i = 0;i < NO_CLASS;i++){
    if(count[i] < 20)
      return 0;
  }
  return 1;
}

int check_sum(int count[NO_CLASS]){
  int sum = 0;
  for(int i = 0;i < NO_CLASS;i++)
    sum += count[i];
  if(sum >= 1000)
    return 1;
  else
    return 0;
}

int 
main(int argc, char **argv)
{
  lcm_eventlog_t *read_log;
  char *log_filename;
  FILE *file1;
  FILE *file2;
  FILE *file3;
  char *laser_annotation_channel_name = "LASER_ANNOTATION";
  char *pose_channel_name = "POSE";

  //Buffer for annotations & pose
  BotPtrCircular *annotation_list;
  BotPtrCircular *pose_list;
  annotation_list = bot_ptr_circular_new(ANNOTATION_LIST_SIZE, NULL, NULL);
  pose_list = bot_ptr_circular_new(POSE_LIST_SIZE, NULL, NULL);
  int64_t end_log_utime;

  srand(time(NULL));

  //Get lcm, param and frames
  lcm_t *lcm;
  if (!(lcm = bot_lcm_get_global(NULL))) {
    fprintf (stderr, "Unable to get LCM\n");
    return 0;
  }

  BotParam *param;
  if (!(param = bot_param_get_global(lcm, 0))) {
    fprintf(stderr,"No server found : Reading from file\n");
    char config_path[2048];
    sprintf(config_path, "wheelchair.cfg");
    param = bot_param_new_from_file(config_path);

    if(!param){
      fprintf (stderr, "Unable to get BotParam instance\n");
      return 0;
    }
  }

  BotFrames *frames;
  if (!(frames = bot_frames_get_global (lcm, param))) {
    fprintf (stderr, "Unable to get BotFrames instance\n");
    return 0;
  }
  

  if(argc < 4){
    fprintf(stderr, "Provide file name.\n");
    return -1;
  }
  log_filename = argv[1];
  //open file for result writing
  file1 = fopen(argv[2], "w");
  file2 = fopen(argv[3], "w");
  file3 = fopen(argv[4], "w");
  if(!file1 || !file2 || !file3){
    fprintf(stderr, "Cannot open file.\n");
    return -1;
  }

  if(log_filename){
    read_log = lcm_eventlog_create(log_filename, "r");
    fprintf(stderr, "Log open success.\n");
  }

  int num_laser_annotation = 0;
  lcm_eventlog_event_t *event = (lcm_eventlog_event_t *)calloc(1, sizeof(lcm_eventlog_event_t));
  for(event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){
    int decode_status = 0;
    //Laser annotation messages
    if(strcmp(laser_annotation_channel_name, event->channel) == 0){
      erlcm_laser_annotation_t *msg = (erlcm_laser_annotation_t *)calloc(1, sizeof(erlcm_laser_annotation_t));
      decode_status = erlcm_laser_annotation_t_decode(event->data, 0, event->datalen, msg);
      if(decode_status < 0){
	fprintf(stderr, "Error decoding message\n");
	return -1;
      }
      
      //Convert lcm message into a struct
      LaserAnnotation *l = (LaserAnnotation *)calloc(1, sizeof(LaserAnnotation));
      l->annotation = msg->annotation;
      l->utime = msg->laser.utime;

      //Add to the circular buffer
      bot_ptr_circular_add(annotation_list, l);
      
      free(msg);
    }
    //Pose messages
    if(strcmp(pose_channel_name, event->channel) == 0){
      bot_core_pose_t *msg = (bot_core_pose_t *)calloc(1, sizeof(bot_core_pose_t));
      decode_status = bot_core_pose_t_decode(event->data, 0, event->datalen, msg);
      if(decode_status < 0){
	fprintf(stderr, "Error decoding message\n");
	return -1;
      }

      //Convert lcm message into a struct
      Pose *p = (Pose *)calloc(1, sizeof(Pose));
      double position[3];
      int trans = bot_frames_transform_vec(frames, "body", "local", msg->pos, position);
      if(trans == 0){
	fprintf(stderr, "Transformation failed.\n");
	return -1;
      }
      p->xy[0] = position[0];
      p->xy[1] = position[1];
      p->utime = msg->utime;

      //Add to the circular buffer
      bot_ptr_circular_add(pose_list, p);
      free(msg);

      end_log_utime = p->utime;
    }
    lcm_eventlog_free_event(event);
  }			   

  fprintf(stderr, "Annotation buffer size: %d, Pose buffer size: %d\n", bot_ptr_circular_size(annotation_list), bot_ptr_circular_size(pose_list));


  //Run 1000 simulations
  int count1[NO_CLASS];
  int count2[NO_CLASS];
  int count3[NO_CLASS];
  for(int i = 0;i < NO_CLASS;i++){
    count1[i] = 0;
    count2[i] = 0;
    count3[i] = 0;
  }

  while(1){

    //too hard to collect stationary data, change to change sum...
    if(check_sum(count1) && check_num(count2) && check_num(count3))
      break;

    int s1 = 0;
    int s2 = 0;
    int s3 = 0;
    for(int i = 0;i < NO_CLASS;i++){
      s1 += count1[i];
      s2 += count2[i];
      s3 += count3[i];
    }
    fprintf(stderr, "No. count1: %d, No. count2: %d, No. count3: %d\n", s1, s2, s3);

    //Get a random index in annotation buffer
    int annotation_length = bot_ptr_circular_size(annotation_list);
    double rand_val = (double)rand() / (RAND_MAX);
    int rand_sample = (int)(rand_val * annotation_length);
    LaserAnnotation *l_annotation = bot_ptr_circular_index(annotation_list, rand_sample);
    int64_t rand_utime = l_annotation->utime;

    //check_utime is to check whether dist after 0.5s > 20cm. Determine add to stationary model or dynamic model
    int64_t check_utime = rand_utime + 0.5 * pow(10, 6);

    //If check_utime exceeds end_log_utime, maybe discard this rand_utime
    if(check_utime >= end_log_utime)
      continue;

    //Search for the corresponding pose at the random timestamp
    int num = 0;
    int finish_check = 0;
    Pose *start_pose = NULL;
    Pose *end_pose = NULL;
    int stationary = 0;
    int slow_move = 0;
    int fast_move = 0;
    for(int i = bot_ptr_circular_size(pose_list) - 1;i >= 0; i--){
      Pose *p = bot_ptr_circular_index(pose_list, i);

      //Get the first pose at the rand_utime, treat it as pose at random timestamp
      if(p->utime >= rand_utime && num == 0){
	start_pose = p;
	num++;
      }

      //Check the pose 0.5s later
      if(p->utime >= check_utime && finish_check == 0 && (start_pose)){
	if(hypot(p->xy[0] - start_pose->xy[0], p->xy[1] - start_pose->xy[1]) < 0.2)
	  stationary = 1;
	else if(hypot(p->xy[0] - start_pose->xy[0], p->xy[1] - start_pose->xy[1]) >= 0.2 && hypot(p->xy[0] - start_pose->xy[0], p->xy[1] - start_pose->xy[1]) <= 0.4)
	  slow_move = 1;
	else
	  fast_move = 1;
	end_pose = p;
	break;
      }
      /*
      //Get the first pose with dist > 25cm with start_pose
      if(start_pose && !end_pose){
	if(hypot(p->xy[0] - start_pose->xy[0], p->xy[1] - start_pose->xy[1]) >= 0.2){
	  end_pose = p;
	  if(finish_check)
	    break;
	}
      }
      */
    }

    //Get the annotation at the end_pose utime
    LaserAnnotation *end_annotation;

    if(start_pose && end_pose){
      int64_t pose_end_utime = end_pose->utime;
      for(int i = rand_sample; i < bot_ptr_circular_size(annotation_list);i++){
	LaserAnnotation *la = bot_ptr_circular_index(annotation_list, i);
	if(la->utime > pose_end_utime){
	  end_annotation = la;
	}
      }
    }


    //Determine the start & end place in int
    char *classes[NO_CLASS];
    classes[0] = "elevator";
    classes[1] = "conference_room";
    classes[2] = "office";
    classes[3] = "lab";
    classes[4] = "open_area";
    classes[5] = "hallway";
    classes[6] = "corridor";
    classes[7] = "large_meeting_room";

    int start_place;
    int end_place;

    for(int i = 0;i < NO_CLASS;i++){
      if(strcmp(classes[i], l_annotation->annotation) == 0)
	start_place = i + 1;
      if(strcmp(classes[i], end_annotation->annotation) == 0)
	end_place = i + 1;
    }

    if(stationary && (count1[start_place - 1] > 400))
      continue;
    if(slow_move && (count2[start_place - 1] > 400))
      continue;
    if(fast_move && (count3[start_place - 1] > 400))
      continue;

    //Write simulation result to files
    if(start_pose && end_pose){
      if(stationary){
	fprintf(file1, "%d %d\n", start_place, end_place);
	count1[start_place - 1]++;
      }
      if(slow_move){
	fprintf(file2, "%d %d\n", start_place, end_place);
	count2[start_place - 1]++;
      }
      if(fast_move){
	fprintf(file3, "%d %d\n", start_place, end_place);
	count3[start_place - 1]++;
      }
    }
  }
  
  fclose(file1);
  fclose(file2);
  fclose(file3);
}

