//This script collect data for the observation model in HMM reading from the log contains place_classification & annotation.
//Output: a text file of the list of actual vs predicted. 

#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>

#define BIN_SIZE 1000


int 
main(int argc, char **argv)
{
  lcm_eventlog_t *read_log;
  char *log_filename;
  FILE *file;
  char *log_annotation_channel_name = "LOG_ANNOTATION";
  char *classification_channel_name = "PLACE_CLASSIFICATION";
  BotPtrCircular *annotations;

  annotations = bot_ptr_circular_new(BIN_SIZE, NULL, NULL);

  if(argc < 3){
    fprintf(stderr, "Provide file name.\n");
    return -1;
  }
  log_filename = argv[1];
  //open file for result writing
  file = fopen(argv[2], "w");
  if(!file){
    fprintf(stderr, "Cannot open file.\n");
    return -1;
  }
  if(log_filename){
    read_log = lcm_eventlog_create(log_filename, "r");
    fprintf(stderr, "Log open success.\n");
  }
  
  //Read from the log and store all log_annotation messages in the circular buffer
  lcm_eventlog_event_t *log_annotation_event = NULL;
  for(lcm_eventlog_event_t *event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){
    int decode_status = 0;
    if(strcmp(log_annotation_channel_name, event->channel) == 0){
      erlcm_annotation_t *msg = (erlcm_annotation_t *)calloc(1, sizeof(erlcm_annotation_t));
      decode_status = erlcm_annotation_t_decode(event->data, 0, event->datalen, msg);
      if(decode_status < 0){
	fprintf(stderr, "Error decoding message\n");
	return -1;
      }
      bot_ptr_circular_add(annotations, erlcm_annotation_t_copy(msg));
      free(msg);
    }
    lcm_eventlog_free_event(event);
  }
  fprintf(stderr, "Annotation list size: %d\n", bot_ptr_circular_size(annotations));

  //Reopen the log file
  lcm_eventlog_destroy(read_log);
  read_log = lcm_eventlog_create(log_filename, "r");

  lcm_eventlog_event_t *classification_event = NULL;
  for(lcm_eventlog_event_t *event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){
    int decode_status = 0;
    if(strcmp(classification_channel_name, event->channel) == 0){
      erlcm_place_classification_t *msg = (erlcm_place_classification_t *)calloc(1, sizeof(erlcm_place_classification_t));
      decode_status = erlcm_place_classification_t_decode(event->data, 0, event->datalen, msg);
      if(decode_status < 0){
	fprintf(stderr, "Error decoding message\n");
	return -1;
      }
      char *predict_class = msg->label;
      char *actual_class;
      int predict;
      int actual;
      for(int i = 0;i < bot_ptr_circular_size(annotations);i++){
	erlcm_annotation_t *iter = bot_ptr_circular_index(annotations, i);
	if(iter->start_utime <= msg->sensor_utime && iter->end_utime >= msg->sensor_utime){
	  actual_class = iter->annotation;
	  break;
	}
      }
      //Some timestamp does not have annotation
      if(actual_class){
	//A set of classes
	int no_class = 8;
	char *classes[no_class];
	classes[0] = "elevator";
	classes[1] = "conference_room";
	classes[2] = "office";
	classes[3] = "lab";
	classes[4] = "open_area";
	classes[5] = "hallway";
	classes[6] = "corridor";
	classes[7] = "large_meeting_room";
	for(int i = 0; i < no_class;i++){
	  if(strcmp(predict_class, classes[i]) == 0)
	    predict = i + 1;
	  if(strcmp(actual_class, classes[i]) == 0)
	    actual = i + 1;
	}
	fprintf(file, "%d %d\n", actual, predict);
      }
    }
    lcm_eventlog_free_event(event);
  }

  fclose(file);
}
