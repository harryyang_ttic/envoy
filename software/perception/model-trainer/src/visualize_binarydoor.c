//Visualize two transition models


#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>

#include <stdlib.h>
#include <stdio.h>

#define NO_CLASS 2

int 
main(int argc, char **argv)
{
  FILE *f1;
  if(argc < 2)
    fprintf(stderr, "provide file name.\n");
  f1 = fopen(argv[1], "r");
  if(!f1){
    fprintf(stderr, "Failed to open files\n");
    return -1;
  }
  else
    fprintf(stderr, "File open success\n");
  char str[5000];
  int count[NO_CLASS][NO_CLASS];
  for(int i = 0;i < NO_CLASS;i++){
    for(int j = 0;j < NO_CLASS;j++){
      count[i][j] = 0;
    }
  }
  while(fgets(str,sizeof(str),f1) != NULL){
    int first = atoi(&str[0]);
    int second = atoi(&str[2]);
    count[first][second]++;
  }

  int total[NO_CLASS];
  double prob[NO_CLASS][NO_CLASS];
  
  fprintf(stderr, "Total: \n");
  for(int i = 0;i < NO_CLASS;i++){
    total[i] = 0;
    for(int j = 0;j < NO_CLASS;j++){
      total[i] += count[i][j];
    }
    fprintf(stderr, "%d: %d\n", i, total[i]);
  }
  fprintf(stderr, "Predictions. [Real] [Prediction]\n");
  for(int i = 0;i < NO_CLASS;i++){
    for(int j = 0;j < NO_CLASS;j++){
      fprintf(stderr, "%d %d: %d %d\n", i, j, count[i][j], total[i]);
      prob[i][j] = (double)count[i][j]/total[i];
    }
  }

  fprintf(stderr, "Observation histogram.\n");
  for(int i = NO_CLASS - 1;i >= 0;i--){
    for(int j = 0;j < NO_CLASS;j++){
      if(j == 0)
	fprintf(stderr, "%d %.2f ", i    , prob[i][j]);
      else if(j != NO_CLASS - 1)
	fprintf(stderr, "%.2f ", prob[i][j]);
      else
	fprintf(stderr, "%.2f\n", prob[i][j]);
    }
  }
  for(int i = 0;i < NO_CLASS;i++){
    if(i == 0)
      fprintf(stderr, "    %d  ", i    );
    else if(i != NO_CLASS - 1)
      fprintf(stderr, "  %d  ", i    );
    else
      fprintf(stderr, "  %d\n", i    );
  }

  fclose(f1);
}
