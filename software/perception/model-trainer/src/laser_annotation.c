//Subscribe to log_annotation_t and planar_lidar_t messages. Once receive an annotation, publish laser_annotation_t message
//Requires: er-simulate-360-laser
//Output: publish laser_annotation_t message


#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <full_laser/full_laser.h>

#define LIDAR_LIST_SIZE 4000

typedef struct
{
    lcm_t      *lcm;
    GMainLoop *mainloop;

    GList *lidar_list;
    bot_core_planar_lidar_t *last_laser;
    bot_core_planar_lidar_t *last_nodding;
    erlcm_annotation_t *last_annotation;
    full_laser_state_t *full_laser;

    int nodding;
  
} state_t;


static void
full_laser_update_handler(int64_t utime, bot_core_planar_lidar_t *msg, void *user)
{
    state_t *s = (state_t *)user;

    if(s->last_laser != NULL){
        bot_core_planar_lidar_t_destroy(s->last_laser);
    }
    s->last_laser = bot_core_planar_lidar_t_copy(msg);
    bot_core_planar_lidar_t *laser = bot_core_planar_lidar_t_copy(msg);

    if(g_list_length(s->lidar_list) < LIDAR_LIST_SIZE){
        s->lidar_list = g_list_prepend (s->lidar_list, (laser));
    }
    else{
        GList* last = g_list_last (s->lidar_list);
        bot_core_planar_lidar_t *last_lidar = (bot_core_planar_lidar_t *) last->data;
	
        bot_core_planar_lidar_t_destroy((bot_core_planar_lidar_t *) last->data);
        s->lidar_list = g_list_delete_link (s->lidar_list, last);	

        s->lidar_list = g_list_prepend (s->lidar_list , (laser));
    }    
}

//maybe look at the pose also ?? and draw the pose of the lasers and their annotations?


static void
on_nodding (const lcm_recv_buf_t *rbuf, const char *channel,
	    const bot_core_planar_lidar_t *msg, void *user)
{
    state_t *s = (state_t *)user;
    if(s->last_nodding != NULL){
        bot_core_planar_lidar_t_destroy(s->last_nodding);
    }
    s->last_nodding = bot_core_planar_lidar_t_copy(msg);
    bot_core_planar_lidar_t *laser = bot_core_planar_lidar_t_copy(msg);

    if(g_list_length(s->lidar_list) < LIDAR_LIST_SIZE){
        s->lidar_list = g_list_prepend (s->lidar_list, (laser));
    }
    else{
        GList* last = g_list_last (s->lidar_list);
        bot_core_planar_lidar_t *last_lidar = (bot_core_planar_lidar_t *) last->data;
	
        bot_core_planar_lidar_t_destroy((bot_core_planar_lidar_t *) last->data);
        s->lidar_list = g_list_delete_link (s->lidar_list, last);	

        s->lidar_list = g_list_prepend (s->lidar_list , (laser));
    }
}

static void
on_annotation (const lcm_recv_buf_t *rbuf, const char *channel,
	       const erlcm_annotation_t *msg, void *user)
{
    state_t *s= (state_t *)user;

    if(s->last_annotation != NULL){
        erlcm_annotation_t_destroy(s->last_annotation);
    }

    s->last_annotation = erlcm_annotation_t_copy(msg);
    int64_t start_utime = s->last_annotation->start_utime;
    int64_t end_utime = s->last_annotation->end_utime;

    int last_valid_ind = 10000;
    int valid_laser_count = 0; 
    
    erlcm_laser_annotation_t message;
    for(int i = 0; i < g_list_length(s->lidar_list); i++){
        GList *element = g_list_nth(s->lidar_list, i);
        bot_core_planar_lidar_t *l = (bot_core_planar_lidar_t *)element->data;
        int64_t laser_utime = l->utime;
	
        if(laser_utime >= start_utime && laser_utime <= end_utime){
            message.laser = l[0];
            message.annotation = s->last_annotation->annotation;
            erlcm_laser_annotation_t_publish(s->lcm, "LASER_ANNOTATION", &message);
            usleep((int)1/40 * 1e6);
            valid_laser_count++;
            if(last_valid_ind > i){
                last_valid_ind = i;
            }
        }
    }

    fprintf(stderr, "Annotation : %s\n", s->last_annotation->annotation);
    if(last_valid_ind < g_list_length(s->lidar_list)){
        printf("Found valid Logs -Valid Lasers : %d  \n\t clearing up to the valid one : %d\n", valid_laser_count, last_valid_ind);
        while(g_list_length(s->lidar_list) > last_valid_ind){
            GList* last = g_list_last (s->lidar_list);
            bot_core_planar_lidar_t *last_lidar = (bot_core_planar_lidar_t *) last->data;
            //	fprintf(stderr,"Pose : %.3f (%f,%f)\n",last_pose->utime / 1000000.0 , last_pose->pos[0], last_pose->pos[1]);
	
            //	s->pose_list = g_list_remove_link (s->pose_list, last);	
            bot_core_planar_lidar_t_destroy((bot_core_planar_lidar_t *) last->data);
            s->lidar_list = g_list_delete_link (s->lidar_list, last);
        }      
    }
    fprintf(stderr,"New List size : %d\n", g_list_length(s->lidar_list));
    
}

void subscribe_to_channels(state_t *s)
{
    erlcm_annotation_t_subscribe(s->lcm, "LOG_ANNOTATION", on_annotation, s);
}  


static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -n  Use nodding laser\n"
             "  -h        This help message\n", 
             g_path_get_basename(progname));
    exit(1);
}


int 
main(int argc, char **argv)
{

    const char *optstring = "nh";

    int c;
    int nodding = 0;

    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'n':  //whether to use nodding laser
            nodding = 1;
            break;
        case 'h': //help
            usage(argv[0]);
            break;
        default:
            usage(argv[0]);
            break;
        }
    }


    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);

    subscribe_to_channels(state);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );
    //If not using nodding laser
    if(!nodding){
        state->full_laser = full_laser_init(state->lcm, 360, &full_laser_update_handler, state);
        fprintf(stderr, "Using 360 degree simulate laser.\n");
    }
    else{
        bot_core_planar_lidar_t_subscribe(state->lcm, "NODDING_LASER", on_nodding, state);
        fprintf(stderr, "subsribing to nodder laser.\n");
    }
    state->last_laser = NULL;
    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }
    state->nodding = nodding;
    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    //read_parameters_from_conf(state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    //    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


