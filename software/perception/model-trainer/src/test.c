//This script test the accuracy of svm-predict of one-vs-one classifier in libsvm


#include <svm/svm.h>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <laser_features/laser_feature.h>

int 
main(int argc, char **argv)
{
    if(argc < 2)
        fprintf(stderr, "Provide file name.\n");
    FILE *f;
    f = fopen(argv[1], "r");
    char line[5000];
    int no_features = 47;
    typedef struct svm_node svm_node_t;
    typedef struct svm_model svm_model_t;
    svm_model_t *model;
    svm_node_t *test_pt = NULL;
    model = (svm_model_t *)calloc(1, sizeof(svm_model_t));
    char *model_path = "multiclass";
    if(model_path != NULL){
        model = svm_load_model(model_path);
        if(model == NULL)
            fprintf(stderr, "Error loading model\n");
        else
            fprintf(stderr, "Model load success.\n");
    }

    test_pt = (svm_node_t *)calloc(no_features, sizeof(svm_node_t));
    test_pt[no_features - 1].index = -1;
  
    int correct = 0;
    int total = 0;
  
    while(fgets(line, sizeof(line), f) != NULL)
        {
            char *label = strtok(line," \t\n");
            int l = atoi(label);
            //		fprintf(stderr, "Label:%d\n", l);
            char *idx, *val, *endptr;
            int num = 0;
            while(1)
                {
                    if(num == no_features - 1)
                        break;
                    idx = strtok(NULL,":");
                    int ind = strtod(idx, &endptr);
                    val = strtok(NULL," \t");
                    double v = strtod(val,&endptr);
                    //			fprintf(stderr,"Strings : %s %s\n", idx, val);
                    //			fprintf(stderr, "%d %f\n", ind, v);
                    test_pt[ind - 1].index = ind;
                    test_pt[ind - 1].value = v;
                    //			fprintf(stderr, "%d\n", ind);
                    num++;
                }
            int lb = (int)svm_predict(model, test_pt);
            if(lb == l)
                correct++;
            total++;
        }
    fprintf(stderr, "Accuracy using svm_predict in libsvm is %f\n", correct/(double)total);
}
