//This script test the accuracy of svm-predict of one-vs-one classifier in libsvm


#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
#include <time.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <bot_frames/bot_frames.h>



int 
main(int argc, char **argv)
{

  if(argc < 4){
    fprintf(stderr, "Provide file name.\n");
    return -1;
  }
  FILE *f1, *f2, *f3;
  f1 = fopen(argv[1], "r");
  f2 = fopen(argv[2], "r");
  f3 = fopen(argv[3], "w");
  char line[5000];
  int count = 0;
  char *info[50000];
  while(fgets(line, sizeof(line), f1) != NULL){
    //      fprintf(stderr, "Count: %d\n", count);
    info[count] = strdup(line);
    count++;
    //   fprintf(stderr, "%s", info[0]);
  }
  fprintf(stderr, "Count: %d\n", count);
  char str[5000];
  int index = 0;
  while(fgets(str, sizeof(str), f2) != NULL){
    char *real = info[index];
    index++;
    int r = atoi(&real[0]);
    int p = atoi(&str[0]);
    fprintf(f3, "%d %d\n", r, p);
  }
}
