//Add a executable er-place-classifier to classify places
//Requires: er-simulate-360-laser(SKIRT_SIM messages)
//Publish: place_classification_t & place_classification_debug_t messages


#include <svm/svm.h>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <glib.h>
#include <bot_core/bot_core.h>
#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>
#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <laser_features/laser_feature.h>
#include <full_laser/full_laser.h>
#include <lcmtypes/erlcm_place_classification_class_t.h>

#define ANNOTATION_BIN_SIZE 100
#define THRESHOLD 0.4

//#define v

typedef struct svm_model svm_model_t;
typedef struct svm_node svm_node_t;

char *s1 = "doorway";
char *s2 = "conference_room";
char *s3 = "office";
char *s4 = "lab";
char *s5 = "open_area";
char *s6 = "hallway";
char *s7 = "elevator";
char *s8 = "corridor";
char *s9 = "large_meeting_room";

typedef struct
{
    lcm_t *lcm;
    GMainLoop *mainloop;
    svm_model_t *model;
    svm_model_t *doorway_model;  //A model for binary doorway classifier
    bot_core_planar_lidar_t *last_laser;
    full_laser_state_t *full_laser;
    GMutex *mutex; 
    int num_annotations;
    int correct;
    int total;
    int doorway;

    int no_class; 
    int *class_labels;
    int **class_counts;
}state_t;

int get_index_from_id(int id, state_t *s){
    for(int i=0; i < s->no_class; i++){
        if(id == s->class_labels[i])
            return i;
    }
    return -1;
}

int get_id_from_name(char *annotation){
    if(!strncmp(annotation, s1, 100)){
	return ERLCM_PLACE_CLASSIFICATION_CLASS_T_DOORWAY;//1;
    }
    else if(!strncmp(annotation, s2, 100)){
	return ERLCM_PLACE_CLASSIFICATION_CLASS_T_CONFERENCE_ROOM;
    }
    else if(!strncmp(annotation, s3, 100)){
	return ERLCM_PLACE_CLASSIFICATION_CLASS_T_OFFICE;
    }
    else if(!strncmp(annotation, s4, 100)){
	return ERLCM_PLACE_CLASSIFICATION_CLASS_T_LAB;
    }
    else if(!strncmp(annotation, s5, 100)){
	return ERLCM_PLACE_CLASSIFICATION_CLASS_T_OPEN_AREA;
    }
    else if(!strncmp(annotation, s6, 100)){
	return ERLCM_PLACE_CLASSIFICATION_CLASS_T_HALLWAY;
    }
    else if(!strncmp(annotation, s7, 100)){
	return ERLCM_PLACE_CLASSIFICATION_CLASS_T_ELEVATOR;
    }
    else if(!strncmp(annotation, s8, 100)){
	return ERLCM_PLACE_CLASSIFICATION_CLASS_T_CORRIDOR;
    }
    else if(!strncmp(annotation, s9, 100)){
	return ERLCM_PLACE_CLASSIFICATION_CLASS_T_LARGE_MEETING_ROOM;
    }
    else{
        fprintf(stderr, "Error - Unknown Class : %s\n", annotation);
        return -1;
    }
    
}

char* get_class_name_from_id(int class_id){
    char *class_name = NULL;
    if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_DOORWAY){
        class_name = "Doorway";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_CONFERENCE_ROOM){
        class_name = "Conference Room";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_OFFICE){
        class_name = "Office";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_LAB){
        class_name = "Lab";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_OPEN_AREA){
        class_name = "Open Area";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_HALLWAY){
        class_name = "Hallway";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_ELEVATOR){
        class_name = "Elevator";
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_CORRIDOR){
        class_name = "Corridor"; //what is the difference here??
    }
    else if(class_id == ERLCM_PLACE_CLASSIFICATION_CLASS_T_LARGE_MEETING_ROOM){
        class_name = "Large Meeting Room";
    }
    else{        
        class_name = "Unknown";
    }
    return class_name;
}


//Check whether classifier is confident with the classification
int check_classification(double *values, int no_class){
    for(int i = 0; i < no_class;i++){
        if(values[i] > THRESHOLD)
            return 1;
    }
    return 0;
}

double *calculate_features(feature_config_t config, bot_core_planar_lidar_t *laser)
{
    //calculate feature values
    int no_features = config.no_of_fourier_coefficient * 4 + 38;
    raw_laser_features_t *raw_features = extract_raw_laser_features(laser, config);
    polygonal_laser_features_t *polygonal_features = extract_polygonal_laser_features(laser, config);
    double *values = (double *) calloc(no_features, sizeof(double));
    values[0] = raw_features->avg_difference_consecutive_beams;
    values[1] = raw_features->std_dev_difference_consecutive_beams;
    values[2] = raw_features->cutoff_avg_difference_consecutive_beams;
    values[3] = raw_features->avg_beam_length;
    values[4] = raw_features->std_dev_beam_length;
    values[5] = raw_features->no_gaps_in_scan_threshold_1;
    values[6] = raw_features->no_relative_gaps;
    values[7] = raw_features->no_gaps_to_num_beam;
    values[8] = raw_features->no_relative_gaps_to_num_beam;
    values[9] = raw_features->distance_between_two_smalled_local_minima;
    values[10] = raw_features->index_distance_between_two_smalled_local_minima;
    values[11] = raw_features->avg_ratio_consecutive_beams;
    values[12] = raw_features->avg_to_max_beam_length;
    values[13] = polygonal_features->area;
    values[14] = polygonal_features->perimeter;
    values[15] = polygonal_features->area_to_perimeter;
    values[16] = polygonal_features->PI_area_to_sqrt_perimeter;
    values[17] = polygonal_features->PI_area_to_perimeter_pow;
    values[18] = polygonal_features->sides_of_polygon;
    for(int i = 0; i <= config.no_of_fourier_coefficient * 2;i++){
        values[19 + i * 2] = polygonal_features->fourier_transform_descriptors[0].results->real;
        values[20 + i * 2] = polygonal_features->fourier_transform_descriptors[0].results->complex;
    }
    int start = config.no_of_fourier_coefficient * 4 + 21;
    values[start] = polygonal_features->major_ellipse_axis_length_coefficient;
    values[start + 1] = polygonal_features->minor_ellipse_axis_length_coefficient;
    values[start + 2] = polygonal_features->major_to_minor_coefficient;
    for(int i = 0; i < 7;i++){
        values[start + 3 + i] = polygonal_features->central_moment_invariants[i];
    }
    values[start + 10] = polygonal_features->normalized_feature_of_compactness;
    values[start + 11] = polygonal_features->normalized_feature_of_eccentricity;
    values[start + 12] = polygonal_features->mean_centroid_to_shape_boundary;
    values[start + 13] = polygonal_features->max_centroid_to_shape_boundary;
    values[start + 14] = polygonal_features->mean_centroid_over_max_centroid;
    values[start + 15] = polygonal_features->std_dev_centroid_to_shape_boundry;
    values[start + 16] = polygonal_features->kurtosis;
    return values;
}



void test_result(const erlcm_laser_annotation_t *msg, state_t *s){
    bot_core_planar_lidar_t *laser = &msg->laser;
    char *annotation = msg->annotation;

    int grnd_label = -1;
    if(!strncmp(annotation, s1, 100)){
	grnd_label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_DOORWAY;//1;
    }
    else if(!strncmp(annotation, s2, 100)){
	grnd_label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_CONFERENCE_ROOM;
    }
    else if(!strncmp(annotation, s3, 100)){
        grnd_label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_OFFICE;
    }
    else if(!strncmp(annotation, s4, 100)){
	grnd_label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_LAB;
    }
    else if(!strncmp(annotation, s5, 100)){
	grnd_label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_OPEN_AREA;
    }
    else if(!strncmp(annotation, s6, 100)){
	grnd_label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_HALLWAY;
    }
    else if(!strncmp(annotation, s7, 100)){
	grnd_label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_ELEVATOR;
    }
    else if(!strncmp(annotation, s8, 100)){
	grnd_label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_CORRIDOR;
    }
    else if(!strncmp(annotation, s9, 100)){
	grnd_label = ERLCM_PLACE_CLASSIFICATION_CLASS_T_LARGE_MEETING_ROOM;
    }
    else{
        fprintf(stderr, "Error - Unknown Class : %s\n", annotation);
        return;
    }
    
    //Change the feature configurations here. min_range, max_range, etc..
    feature_config_t config;
    config.min_range = 0.1;
    config.max_range = 30;
    config.cutoff = 5;
    config.gap_threshold_1 = 3;
    config.gap_threshold_2 = 0.5;
    config.deviation = 0.001;
    config.no_of_fourier_coefficient = 2;
    config.no_beam_skip = 3;

    int no_features = config.no_of_fourier_coefficient * 4 + 38;
  
    double *feature_values = calculate_features(config, laser);
  
    svm_node_t *test_pt;
    test_pt = (svm_node_t *) calloc(no_features + 1, sizeof(svm_node_t));
    for(int i = 0;i < no_features;i++){
        test_pt[i].index = i + 1;
        test_pt[i].value = feature_values[i];
    }
    test_pt[no_features].index = -1;
    //predict label
    int label = (int)svm_predict(s->model, test_pt);
    int doorway_label;
    if(s->doorway)
        doorway_label = (int)svm_predict(s->doorway_model, test_pt);
       
    double probability[s->no_class];
    if(svm_check_probability_model(s->model) == 0){
        fprintf(stderr, "Model does not have probability information.\n");
        for(int i = 0;i < s->no_class;i++)
            probability[i] = 0;
    }

    double value = svm_predict_probability(s->model, test_pt, probability);
    free(test_pt);
    
    char* class_name = get_class_name_from_id(label);
    char* grnd_class_name = get_class_name_from_id(grnd_label);

#ifdef v
    fprintf(stderr, "Ground Truth Class : %s => Predicted : %s\n", grnd_class_name, class_name);    
    fprintf(stderr, "Prob \n");
    for(int i=0; i < s->no_class; i++){
        fprintf(stderr, "[%d] - %1.3f ", s->class_labels[i], probability[i]);
    }
#endif

    free(feature_values);
    
    int grnd_index = get_index_from_id(grnd_label, s);
    int obs_index = get_index_from_id(label, s);
    if(grnd_index < 0 || obs_index < 0){
        fprintf(stderr, "Unknown Class\n");
        return;
    }
    s->class_counts[grnd_index][obs_index]++;
}

static void
laser_annotation_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                const erlcm_laser_annotation_t *msg, void *user)
{    
    fprintf(stderr, "Called\n");
    test_result(msg, (state_t *) user);
}

static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -m MODEL  Specify SVM model\n"
             "  -d BINARY_MODEL       Add doorways\n"
             "  -h        This help message\n", 
             g_path_get_basename(progname));
    exit(1);
}


gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;

    g_mutex_lock (s->mutex);
    static int64_t last_utime = 0;

    if(s->last_laser ==NULL || (s->last_laser->utime == last_utime)){
        g_mutex_unlock (s->mutex);
        return TRUE;
    }
  
    last_utime = s->last_laser->utime;
  
    bot_core_planar_lidar_t *laser = bot_core_planar_lidar_t_copy(s->last_laser);
    g_mutex_unlock (s->mutex);

    //Change the feature configurations here. min_range, max_range, etc..
    feature_config_t config;
    config.min_range = 0.1;
    config.max_range = 30;
    config.cutoff = 5;
    config.gap_threshold_1 = 3;
    config.gap_threshold_2 = 0.5;
    config.deviation = 0.001;
    config.no_of_fourier_coefficient = 2;
    config.no_beam_skip = 3;

    int no_features = config.no_of_fourier_coefficient * 4 + 38;
  
    double *feature_values = calculate_features(config, laser);
  
    svm_node_t *test_pt;
    test_pt = (svm_node_t *) calloc(no_features + 1, sizeof(svm_node_t));
    for(int i = 0;i < no_features;i++){
        test_pt[i].index = i + 1;
        test_pt[i].value = feature_values[i];
    }
    test_pt[no_features].index = -1;
    //predict label
    int label = (int)svm_predict(s->model, test_pt);
    int doorway_label;
    if(s->doorway)
        doorway_label = (int)svm_predict(s->doorway_model, test_pt);
       
    double probability[s->no_class];
    if(svm_check_probability_model(s->model) == 0){
        fprintf(stderr, "Model does not have probability information.\n");
        for(int i = 0;i < s->no_class;i++)
            probability[i] = 0;
    }

    double value = svm_predict_probability(s->model, test_pt, probability);
    free(test_pt);
    
    char* class_name = get_class_name_from_id(label);

    fprintf(stderr, "\nPredicted : %s - ID : %d\n", class_name, label);    
    fprintf(stderr, "Prob \n");
    for(int i=0; i < s->no_class; i++){
        fprintf(stderr, "[%d] - %1.3f ", s->class_labels[i], probability[i]);
    }

    erlcm_place_classification_t msg;
    msg.utime = last_utime;
    msg.label = get_class_name_from_id(label);
    msg.pose_utime = last_utime;
    msg.sensor_utime = last_utime;
    msg.sensor_type = ERLCM_PLACE_CLASSIFICATION_T_SENSOR_TYPE_SIMULATE_360_SKIRT;

    msg.no_class = s->no_class;
    msg.classes = (erlcm_place_classification_class_t *) calloc(msg.no_class, sizeof(erlcm_place_classification_class_t));

    for(int i=0; i < s->no_class; i++){
        msg.classes[i].name = s->class_labels[i];
        msg.classes[i].probability = probability[i];
    }
        /*string label;	
	
	int64_t pose_utime;
	int8_t sensor_type;
	int64_t sensor_utime;

	int8_t no_class;
	place_classification_class_t classes[no_class];
	

	const int8_t SENSOR_TYPE_FRONT_SKIRT = 0;
	const int8_t SENSOR_TYPE_REAR_SKIRT = 1;
	const int8_t SENSOR_TYPE_SIMULATE_360_SKIRT = 2;
        */
    erlcm_place_classification_t_publish(s->lcm, "PLACE_CLASSIFICATION", &msg);

    free(feature_values);
    bot_core_planar_lidar_t_destroy(laser);
    return TRUE;
}

void classify_laser(bot_core_planar_lidar_t *laser, state_t *s){
    //Change the feature configurations here. min_range, max_range, etc..
    feature_config_t config;
    config.min_range = 0.1;
    config.max_range = 30;
    config.cutoff = 5;
    config.gap_threshold_1 = 3;
    config.gap_threshold_2 = 0.5;
    config.deviation = 0.001;
    config.no_of_fourier_coefficient = 2;
    config.no_beam_skip = 3;

    int no_features = config.no_of_fourier_coefficient * 4 + 38;
  
    double *feature_values = calculate_features(config, laser);
  
    svm_node_t *test_pt;
    test_pt = (svm_node_t *) calloc(no_features + 1, sizeof(svm_node_t));
    for(int i = 0;i < no_features;i++){
        test_pt[i].index = i + 1;
        test_pt[i].value = feature_values[i];
    }
    test_pt[no_features].index = -1;
    //predict label
    int label = (int)svm_predict(s->model, test_pt);
    int doorway_label;
    if(s->doorway)
        doorway_label = (int)svm_predict(s->doorway_model, test_pt);
       
    double probability[s->no_class];
    if(svm_check_probability_model(s->model) == 0){
        fprintf(stderr, "Model does not have probability information.\n");
        for(int i = 0;i < s->no_class;i++)
            probability[i] = 0;
    }

    double value = svm_predict_probability(s->model, test_pt, probability);
    free(test_pt);
    
    char* class_name = get_class_name_from_id(label);

#ifdef v
    fprintf(stderr, "\nPredicted : %s - ID : %d\n", class_name, label);    
    fprintf(stderr, "Prob \n");
    for(int i=0; i < s->no_class; i++){
        fprintf(stderr, "[%d] - %1.3f ", s->class_labels[i], probability[i]);
    }
#endif
    free(feature_values);
}

static void
full_laser_update_handler(int64_t utime, bot_core_planar_lidar_t *msg, void *user)
{
    state_t *s = (state_t *)user;

    g_mutex_lock (s->mutex);

    if(s->last_laser != NULL){
        bot_core_planar_lidar_t_destroy(s->last_laser);
    }
    s->last_laser = bot_core_planar_lidar_t_copy(msg);
    //classify_laser(msg, s);
    g_mutex_unlock (s->mutex);
 
}

static void
on_front_laser (const lcm_recv_buf_t *rbuf, const char *channel,
                const bot_core_planar_lidar_t *msg, void *user)
{    
    state_t *s = (state_t *)user;
    fprintf(stderr, "Called\n");
    classify_laser(msg, s);
}


int 
main(int argc, char **argv)
{

    const char *optstring = "m:l:d:hg";

    int c;
    int doorway = 0;
    char *model_path = NULL;
    char *log_file = NULL;
    char *doorway_model = NULL;
    int ground_truth = 0;

    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'm': //choose SVM model
            model_path = strdup(optarg);
            printf("Using SVM model \"%s\"\n", model_path);
            break; 
        case 'g':
            ground_truth = 1;
            break;
        case 'd':  //whether to add doorway class
            doorway = 1;
            doorway_model = strdup(optarg);
            fprintf(stderr, "Using doorway model \"%s\"\n", doorway_model);
            break;
        case 'h': //help
            usage(argv[0]);
            break;
        default:
            usage(argv[0]);
            break;
        }
    }
    if(model_path == NULL)
        usage(argv[0]);
    
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  

    //predict label, we can either use one_vs_one or one_vs_all
    state->last_laser = NULL;
  
    state->model = (svm_model_t *) calloc(1, sizeof(svm_model_t));

    state->doorway_model = (svm_model_t *)calloc(1, sizeof(svm_model_t));

    state->mutex =  g_mutex_new();//g_mutex_init ();

    state->doorway = doorway;

    if(model_path != NULL){
        state->model = svm_load_model(model_path);
        if(state->model == NULL){
            fprintf(stderr, "Error loading model.\n");
            return -1;
        }
        else
            fprintf(stderr, "Model loaded success.\n");
    }

    //this was a seperate model for detecting doorway-vs-non-doorway scans 
    //we have a template based door detector which gives better results 
    if(doorway_model != NULL){
        state->doorway_model = svm_load_model(doorway_model);
        if(state->doorway_model == NULL){
            fprintf(stderr, "Error loading model.\n");
            return -1;
        }
        else
            fprintf(stderr, "Model loaded success.\n");
    }
  
    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }

    
    state->no_class = svm_get_nr_class(state->model);
    state->class_labels = (int *) calloc(state->no_class, sizeof(int));

    state->class_counts = (int **) calloc(state->no_class, sizeof(int *));

    svm_get_labels(state->model, state->class_labels);
    for(int i=0; i < state->no_class; i++){
        fprintf(stderr, "Class => %d\n", state->class_labels[i]);
        state->class_counts[i] = (int *) calloc(state->no_class, sizeof(int));
    }
    
    if(ground_truth == 0){
        state->full_laser = full_laser_init(state->lcm, 360, &full_laser_update_handler, state);
        //bot_core_planar_lidar_t_subscribe(state->lcm, "SKIRT_FRONT", on_front_laser, state);
    }
    else{
        fprintf(stderr, "Subscribing to Laser annotation\n");
        erlcm_laser_annotation_t_subscribe(state->lcm, "LASER_ANNOTATION", laser_annotation_handler, state);
    }
    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    //read_parameters_from_conf(state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);

    g_timeout_add(200, heartbeat_cb, state);
    
    //    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}
