//segment data into no_class data sets for one-vs-all training


#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>

#include <stdlib.h>
#include <stdio.h>

//read training data file and segment the data to equal sizes and splits to training and testing 

int 
main(int argc, char **argv)
{
  FILE *f1, *train, *test;
  if(argc < 4){
    fprintf(stderr, "Provide file names.\n");
    return -1;
  }
  f1 = fopen(argv[1], "r");
  //For training
  train = fopen(argv[2], "w");
  //testing
  test = fopen(argv[3], "w");
  char str[5000];
  if(!f1) {
    fprintf(stderr, "No valid file\n"); 
    return 1;
  } // bail out if file not found
  else{
    fprintf(stderr, "File open successful.\n");
  }
  while(fgets(str,sizeof(str),f1) != NULL){
    //		fprintf(stderr, "%s\n", str);
    if(str[0] == '1'){
      fprintf(train, "%s", str);
    }
    else{
      fprintf(test, "%s", str);
    }
  }
  fclose(f1);
  fclose(train);
  fclose(test);
}
