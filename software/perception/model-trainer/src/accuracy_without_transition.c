//This script test the accuracy of classier without HMM & classifier with HMM while playing log. 


#include <svm/svm.h>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <laser_features/laser_feature.h>

#define NO_CLASS 8
#define ANNOTATION_BIN_SIZE 100
#define CLASSIFICATION_BIN_SIZE 50000

typedef struct _Classification Classification;
struct _Classification{
  char *annotation;
  int64_t utime;
};


void
circ_free_annotation(void *user, void *p){
  erlcm_annotation_t *np = (erlcm_annotation_t *)p;
  erlcm_annotation_t_destroy(np);
}


int 
main(int argc, char **argv)
{

  lcm_eventlog_t *read_log;
  char *log_filename;
  char *annotation_channel_name = "LOG_ANNOTATION";
  char *classification_channel_name = "PLACE_CLASSIFICATION";
  char *HMM_channel_name = "HMM_PLACE_CLASSIFICATION";

  //Buffer for annotations
  BotPtrCircular *annotation_list;
  annotation_list = bot_ptr_circular_new(ANNOTATION_BIN_SIZE, circ_free_annotation, NULL);
  BotPtrCircular *classification_list = bot_ptr_circular_new(CLASSIFICATION_BIN_SIZE, NULL, NULL);
  BotPtrCircular *HMM_list = bot_ptr_circular_new(CLASSIFICATION_BIN_SIZE, NULL, NULL);
  
  int64_t end_log_utime;

  if(argc < 2){
    fprintf(stderr, "Provide file name.\n");
    return -1;
  }

  log_filename = argv[1];
  if(log_filename){
    read_log = lcm_eventlog_create(log_filename, "r");
    fprintf(stderr, "Log open success.\n");
  }
  else{
    fprintf(stderr, "Failed to open log.\n");
    return -1;
  }

  int num_annotations = 0;
  int num_classifications = 0;
  int num_HMM = 0;
  lcm_eventlog_event_t *event = (lcm_eventlog_event_t *)calloc(1, sizeof(lcm_eventlog_event_t));
  //Adding to annotation circular buffer
  for(event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){
    int decode_status = 0;
    if(strcmp(annotation_channel_name, event->channel) == 0){
      erlcm_annotation_t *msg = (erlcm_annotation_t *)calloc(1, sizeof(erlcm_annotation_t));
      decode_status = erlcm_annotation_t_decode(event->data, 0, event->datalen, msg);
      if(decode_status < 0){
	fprintf(stderr, "Error decoding message.\n");
	return -1;
      }
      
      bot_ptr_circular_add(annotation_list, msg);
      num_annotations++;
    }

    //Place_classification messages
    if(strcmp(classification_channel_name, event->channel) == 0){
      erlcm_place_classification_t *msg = (erlcm_place_classification_t *)calloc(1, sizeof(erlcm_place_classification_t));
      decode_status = erlcm_place_classification_t_decode(event->data, 0, event->datalen, msg);
      if(decode_status < 0){
	fprintf(stderr, "Error decoding message.\n");
	return -1;
      }

      //convert to struct
      Classification *p = (Classification *)calloc(1, sizeof(Classification));
      p->annotation = msg->label;
      p->utime = msg->sensor_utime;
      
      bot_ptr_circular_add(classification_list, p);
      num_classifications++;
    }

    //HMM messages
    if(strcmp(HMM_channel_name, event->channel) == 0){
      erlcm_place_classification_t *msg = (erlcm_place_classification_t *)calloc(1, sizeof(erlcm_place_classification_t));
      decode_status = erlcm_place_classification_t_decode(event->data, 0, event->datalen, msg);
      if(decode_status < 0){
	fprintf(stderr, "Error decoding message.\n");
	return -1;
      }

      //convert to struct
      Classification *hmm = (Classification *)calloc(1, sizeof(Classification));
      hmm->annotation = msg->label;
      hmm->utime = msg->sensor_utime;
      
      bot_ptr_circular_add(HMM_list, hmm);
      num_HMM++;
    }
  }

  fprintf(stderr, "No. of annotations: %d, Size of annotation buffer: %d\n", num_annotations, bot_ptr_circular_size(annotation_list));
  fprintf(stderr, "No. of place_classifications: %d, Size of classification buffer: %d\n", num_classifications, bot_ptr_circular_size(classification_list));
  fprintf(stderr, "No. of HMM: %d, Size of HMM buffer: %d\n", num_HMM, bot_ptr_circular_size(HMM_list));

  char *classes[NO_CLASS];
  classes[0] = "elevator";
  classes[1] = "conference_room";
  classes[2] = "office";
  classes[3] = "lab";
  classes[4] = "open_area";
  classes[5] = "hallway";
  classes[6] = "corridor";
  classes[7] = "large_meeting_room";

  //Check the accuracy of place_classification
  if(num_classifications){
    int correct = 0;
    char *real_class = NULL;
    char *prediction = NULL;
    for(int i = 0;i < bot_ptr_circular_size(classification_list);i++){
      Classification *cls = (Classification *)bot_ptr_circular_index(classification_list, i);
      prediction = cls->annotation;
      int64_t p_utime = cls->utime;
      int64_t start_utime;
      int64_t end_utime;
      for(int j = 0;j < bot_ptr_circular_size(annotation_list);j++){
	erlcm_annotation_t *annotation = (erlcm_annotation_t *)bot_ptr_circular_index(annotation_list, j);
	start_utime = annotation->start_utime;
	end_utime = annotation->end_utime;

	//fprintf(stderr, "Start utime: %lld, End utime: %lld, Prediction utime: %lld\n", start_utime, end_utime, p_utime);
	
	if(start_utime <= p_utime && end_utime >= p_utime){
	  real_class = annotation->annotation;
	  break;
	}
      }
      //Change the range of transition time here
      if(p_utime >= start_utime + 0 * pow(10, 6) && p_utime <= end_utime - 0 * pow(10, 6)){
	if(!real_class)
	  num_classifications--;
	else if(strcmp(real_class, prediction) == 0)
	  correct++;
      }
      else{
	num_classifications--;
      }
    }
    fprintf(stderr, "No. of valid classifications: %d\n", num_classifications);
    fprintf(stderr, "Correct: %d, Total: %d, Accuracy of classifier without HMM: %.4f\n", correct, num_classifications, (double)correct/num_classifications);
  }
  if(num_HMM){
    int correct = 0;
    char *real_class;
    char *prediction;
    int m =0;
    int mistake[NO_CLASS][NO_CLASS];
    for(int i = 0;i < NO_CLASS;i++){
      for(int j = 0;j < NO_CLASS;j++){
	mistake[i][j] = 0;
      }
    }
    for(int i = 0;i < bot_ptr_circular_size(HMM_list);i++){
      Classification *cls = (Classification *)bot_ptr_circular_index(HMM_list, i);
      prediction = cls->annotation;
      int64_t p_utime = cls->utime;
      int64_t start_utime;
      int64_t end_utime;
      for(int j = 0;j < bot_ptr_circular_size(annotation_list);j++){
	erlcm_annotation_t *annotation = (erlcm_annotation_t *)bot_ptr_circular_index(annotation_list, j);
	start_utime = annotation->start_utime;
	end_utime = annotation->end_utime;
	if(start_utime <= p_utime && end_utime >= p_utime){
	  real_class = annotation->annotation;
	  break;
	}
      }
      if(p_utime >= start_utime + 0 * pow(10, 6) && p_utime <= end_utime - 0 * pow(10, 6)){
	if(!real_class)
	  num_HMM--;
	else if(strcmp(real_class, prediction) == 0)
	  correct++;
	else if(strcmp(real_class, prediction) != 0){
	  int r;
	  int p;
	  for(int j = 0; j < NO_CLASS;j++){
	    if(strcmp(classes[j], real_class) == 0)
	      r = j;
	    if(strcmp(classes[j], prediction) == 0)
	      p = j;
	  }
	  mistake[r][p]++;
	  m++;
	}	  
      }
      else
	num_HMM--;
    }
    fprintf(stderr, "No. of valid HMM classifications: %d\n", num_HMM);
    fprintf(stderr, "Correct: %d, Total: %d, Accuracy of classifier with HMM: %.4f\n", correct, num_classifications, (double)correct/num_HMM);
    int max1 = -1;
    int max1_r;
    int max1_p;
    int max2 = -1;
    int max2_r;
    int max2_p;
    int max3 = -1;
    int max3_r;
    int max3_p;
    for(int m = 0;m < NO_CLASS;m++){
      for(int n = 0;n <= m;n++)
	mistake[m][n] += mistake[n][m];
    }
    for(int m = 0;m < NO_CLASS;m++){
      for(int n = 0;n <= m;n++){
	if(m != n){
	  if(mistake[m][n] > max1){
	    max1 = mistake[m][n];
	    max1_r = m;
	    max1_p = n;
	    continue;
	  }
	  if(mistake[m][n] > max2){
	    max2 = mistake[m][n];
	    max2_r = m;
	    max2_p = n;
	    continue;
	  }
	  if(mistake[m][n] > max3){
	    max3 = mistake[m][n];
	    max3_r = m;
	    max3_p = n;
	    continue;
	  }
	}
      }
    }
    fprintf(stderr, "Num: %d %d %d, Total: %d\n", max1, max2, max3, m);
    double t1 = (double)max1/m;
    double t2 = (double)max2/m;
    double t3 = (double)max3/m;
    fprintf(stderr, "Top1 mistake! Pairs: %d %d, Percentage: %f\n", max1_r, max1_p, t1);
    fprintf(stderr, "Top2 mistake! Pairs: %d %d, Percentage: %f\n", max2_r, max2_p, t2);
    fprintf(stderr, "Top3 mistake! Pairs: %d %d, Percentage: %f\n", max3_r, max3_p, t3);
  }
}
