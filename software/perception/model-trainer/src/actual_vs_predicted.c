//This script collect data for the observation model in HMM while playing the log
//Requires: log_annotation_t:for actual class & place_classification_t:for predicted class.
//Output: a text file of the list of actual vs predicted. 

#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>

#define CLASSIFICATION_LIST_SIZE 1000

typedef struct
{
  lcm_t *lcm;
  GMainLoop *mainloop;

  FILE *file;

  GList *classification_list;
  erlcm_place_classification_t *last_classification;
  erlcm_annotation_t *last_annotation;
}state_t;


static void
on_classification (const lcm_recv_buf_t *rbuf, const char *channel,
		   const erlcm_place_classification_t *msg, void *user)
{
  state_t *s = (state_t *)user;

  if(s->last_classification != NULL){
    erlcm_place_classification_t_destroy(s->last_classification);
  }
  s->last_classification = erlcm_place_classification_t_copy(msg);
  erlcm_place_classification_t *classification = erlcm_place_classification_t_copy(msg);

  if(g_list_length(s->classification_list) < CLASSIFICATION_LIST_SIZE){
    s->classification_list = g_list_prepend (s->classification_list, (classification));
  }
  else{
    GList* last = g_list_last (s->classification_list);
    erlcm_place_classification_t *last_classification = (erlcm_place_classification_t *) last->data;
	
    erlcm_place_classification_t_destroy((erlcm_place_classification_t *) last->data);
    s->classification_list = g_list_delete_link (s->classification_list, last);	

    s->classification_list = g_list_prepend (s->classification_list , (classification));
  }
}


static void
on_annotation (const lcm_recv_buf_t *rbuf, const char *channel,
	       const erlcm_annotation_t *msg, void *user)
{
  state_t *s= (state_t *)user;

  if(s->last_annotation != NULL){
    erlcm_annotation_t_destroy(s->last_annotation);
  }

  s->file = fopen("actual_vs_predict.txt", "a");

  s->last_annotation = erlcm_annotation_t_copy(msg);
  int64_t start_utime = s->last_annotation->start_utime;
  int64_t end_utime = s->last_annotation->end_utime;

  int last_valid_ind = 10000;
  int valid_laser_count = 0; 
    
  erlcm_laser_annotation_t message;
  for(int i = 0; i < g_list_length(s->classification_list); i++){
    GList *element = g_list_nth(s->classification_list, i);
    erlcm_place_classification_t *l = (erlcm_place_classification_t *)element->data;
    int64_t classification_utime = l->pose_utime;

    if(classification_utime >= start_utime && classification_utime <= end_utime){
      char *real_class = s->last_annotation->annotation;
      char *label =l->label;
      int r = -1;
      int lb = -1;
      char *place[6];
      place[0] = "doorway";
      place[1] = "conference_room";
      place[2] = "office";
      place[3] = "lab";
      place[4] = "open_area";
      place[5] = "hallway";
      //    fprintf(stderr, "Test annotation: %s", annotation);
     
      for(int j = 0;j < 6;j++){
	if(!strcmp(real_class, place[j]))
	  r = j + 1;
      
	if(!strcmp(label, place[j]))
	  lb = j + 1;
      }
      fprintf(s->file, "%d %d\n", r, lb);

      valid_laser_count++;
      if(last_valid_ind > i){
	last_valid_ind = i;
      }
    }
  }

  if(last_valid_ind < g_list_length(s->classification_list)){
    printf("Found valid Logs -Valid Lasers : %d  \n\t clearing up to the valid one : %d\n", valid_laser_count, last_valid_ind);
    while(g_list_length(s->classification_list) > last_valid_ind){
      GList* last = g_list_last (s->classification_list);
      erlcm_place_classification_t *last_classification = (erlcm_place_classification_t *) last->data;
	
      erlcm_place_classification_t_destroy((erlcm_place_classification_t *) last->data);
      s->classification_list = g_list_delete_link (s->classification_list, last);
    }      
  }
  fprintf(stderr,"New List size : %d\n", g_list_length(s->classification_list));
  fclose(s->file);
}


void subscribe_to_channels(state_t *s)
{
  erlcm_annotation_t_subscribe(s->lcm, "LOG_ANNOTATION", on_annotation, s);
  erlcm_place_classification_t_subscribe(s->lcm, "PLACE_CLASSIFICATION", on_classification, s);
}  


int 
main(int argc, char **argv)
{
  state_t *state = (state_t*) calloc(1, sizeof(state_t));

  //this does not attach the lcm to glib mainloop - so we need to do this mannully
  state->lcm =  bot_lcm_get_global(NULL);

  subscribe_to_channels(state);
  
  state->mainloop = g_main_loop_new( NULL, FALSE );  
  
  if (!state->mainloop) {
    printf("Couldn't create main loop\n");
    return -1;
  }

  //add lcm to mainloop 
  bot_glib_mainloop_attach_lcm (state->lcm);

  //read_parameters_from_conf(state);

  //adding proper exiting 
  bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
  //    fprintf(stderr, "Starting Main Loop\n");

  ///////////////////////////////////////////////
  g_main_loop_run(state->mainloop);
  
  bot_glib_mainloop_detach_lcm(state->lcm);
}
