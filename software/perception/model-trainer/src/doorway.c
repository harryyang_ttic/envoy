//Use velodyne info to detect doorways. It can only detect doorways that are 3-5m apart. Not a classifier on spot.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>

#include <gsl/gsl_blas.h>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <velodyne/velodyne.h>
#include <velodyne/velodyne_extractor.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <GL/gl.h>
#include <point_types/point_types.h>
#include <lcmtypes/erlcm_xyz_point_list_t.h>

typedef struct _state_t state_t;

struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    BotParam   *b_server;
    velodyne_extractor_state_t *velodyne; 
    BotFrames *frames;
    bot_lcmgl_t *lcmgl;
    xyzr_point_list_t *points;
};

static void velodyne_update_handler(int64_t utime, void *user)
{
    state_t *s = (state_t *)user;
    if(s->points != NULL)
        destroy_xyzr_list(s->points);

    s->points = velodyne_extract_points_and_distance_frame(s->velodyne, "body");
}


//Try to capture some points
gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;

    if(s->points != NULL){
        //fprintf(stderr, "Size of Points : %d \n", s->points->no_points);

        double body_to_local[12];
        //Translate velydone's frame
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                      "local", s->points->utime, 
                                                      body_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from body to local!\n");
            return TRUE;        
        }

        bot_lcmgl_t *lcmgl = s->lcmgl;

        bot_lcmgl_color3f(s->lcmgl, 1, 0, 0);
        bot_lcmgl_point_size(s->lcmgl, 2);
        bot_lcmgl_begin(s->lcmgl, GL_POINTS);

        int no_points_each_column = 32;
        int no_columns = s->points->no_points / no_points_each_column;
        xyzr_point_t pts_list[no_columns][no_points_each_column];
        for(int i = 0;i < no_columns;i++){
            for(int j = 0;j < no_points_each_column;j++){
                pts_list[i][j] = s->points->points[i * no_points_each_column + j];
                double xyz[3] = {pts_list[i][j].xyz[0], pts_list[i][j].xyz[1], pts_list[i][j].xyz[2]};
                double dist = pts_list[i][j].r;
                //fprintf(stderr, "%dth point: %f %f %f. Distance: %f\n", j, xyz[0], xyz[1], xyz[2], dist);
                double theta = atan2(xyz[1], xyz[0]);
                if(fabs(theta) < bot_to_radians(110)  && dist <= 10){//&& xyz[2] >= 1.8 && xyz[2] <= 2.7){
                    double local_pos[3];
                    bot_vector_affine_transform_3x4_3d (body_to_local, xyz, local_pos);
                    //Draw points in the local frame
                    bot_lcmgl_vertex3f(s->lcmgl, local_pos[0], local_pos[1], local_pos[2]);
                }
            }
        }

        bot_lcmgl_end(s->lcmgl);
        bot_lcmgl_point_size(s->lcmgl, 4);
        bot_lcmgl_color3f(s->lcmgl, 0, 0, 1);

        //Idea: Go through each column, see whether there is a large gap in x coordinate

        //Initialize a point list to store valid points of each column
        xyzr_point_list_t valid_points;
        int valid_count = 0;
        xyzr_point_t valid_pts[no_columns];

        for(int i = 0;i < no_columns;i++){
            double depth = pts_list[i][0].xyz[0];
            int count = 0;
            int last_index = -1;
            //Check go to the ground. distinguish doorways & windows
            //int to_ground = 1;
            //int to_ground_error = 0;
      

            for(int j = 0;j < no_points_each_column;j++){
                //Is 1m difference in depth a good idea?? It can avoid false positives but might increase false negatives. 
                if(pts_list[i][j].xyz[0] - depth <= 1){
                    last_index= j;
                }
                if(pts_list[i][j].xyz[0] - depth > 1){
                    count++;
                }
                //Ensure at least 10 points below have depth difference > 1 & the height is at least 1m(distinguish from windows). This will remove all false positive of windows. But will also increase false negative of closed doorways. 
                if(count >= 10 && (pts_list[i][j].xyz[2] - pts_list[i][last_index].xyz[2]) <= -1.8){
                    /*
                      if(pts_list[i][j].xyz[0] - depth <= 1){
                      to_ground_error++;
                      }
                      if(to_ground_error == 2)
                      to_ground = 0;
                    */
                    //fprintf(stderr, "Find a possible point.\n");
                    double theta = atan2(pts_list[i][last_index].xyz[1], pts_list[i][last_index].xyz[0]);
                    //Determine the height of doorways. assume most doorways are 1.8-2.3m in height. 
                    if(pts_list[i][last_index].xyz[2] >= 1.8 && pts_list[i][last_index].xyz[2] <= 2.3
                       && fabs(theta) < bot_to_radians(110)  && pts_list[i][last_index].r <= 5){
                        /*double pos[3];
                          bot_vector_affine_transform_3x4_3d (body_to_local, pts_list[i][last_index].xyz, pos);
                          //Draw points in the local frame
                          bot_lcmgl_vertex3f(s->lcmgl, pos[0], pos[1], pos[2]);
                          break;*/
                        //if(to_ground){
                        valid_pts[valid_count] = pts_list[i][last_index];
                        valid_count++;
                        break;
                        // }
                    }
                }
            }
        }
    
        fprintf(stderr, "No of valid columns: %d\n", valid_count);
        xyzr_point_t valids[valid_count];
        for(int i = 0;i < valid_count;i++){
            valids[i] = valid_pts[i];
        }

        //Form a list of valid points
        valid_points.utime = bot_timestamp_now();
        valid_points.no_points = valid_count;
        valid_points.points = valids;

        bot_lcmgl_begin(s->lcmgl, GL_POINTS);

        //Go through valid point list and find a proper length consecutive line.
        double start_height = valid_points.points[0].xyz[2];
        int start_index = 0;
        double start_x = valid_points.points[0].xyz[0];
        double start_y = valid_points.points[0].xyz[1];
        double current_y;
        int consecutive = 0;
        int error = 0;
        for(int i = 0;i < valid_count;i++){
            xyzr_point_t point = valid_points.points[i];
            //Set a tolerance value of 0.2m. 
            //If set a constraint on x-axis, it might not detect doors that are on either sides of the wheelchair. It can only detect doors in front..
            if(point.xyz[2] - start_height <= 0.2){ //&& point.xyz[0] - start_x <= 0.2){
                consecutive++;
            }
            else{
                error++;
            }
            //If has 3 errors, reset
            if(error == 3){
                start_height = valid_points.points[i].xyz[2];
                start_index = i;
                start_x = valid_points.points[i].xyz[0];
                start_y = valid_points.points[i].xyz[1];
                consecutive = 0;
                error = 0;
            }
            //Instead of having 30 consecutive columns, determine the length might be helpful.. 0.3m??Seems 30 columns have a better performance..
            if(consecutive >= 30){
                for(int j = i - consecutive + 1;j <= i;j++){
                    xyzr_point_t p = valid_points.points[j];
                    double pos[3];
                    bot_vector_affine_transform_3x4_3d (body_to_local, p.xyz, pos);
                    //Draw points in the local frame
                    bot_lcmgl_vertex3f(s->lcmgl, pos[0], pos[1], pos[2]);
                }
                xyzr_point_t start_p = valid_points.points[i - consecutive + 1];
                xyzr_point_t end_p = valid_points.points[i];
                fprintf(stderr, "Found a doorway. Start pos: %f %f %f, End pos: %f %f %f\n", start_p.xyz[0], start_p.xyz[1], start_p.xyz[2], end_p.xyz[0], end_p.xyz[1], end_p.xyz[2]);
            }
        }
	
	bot_lcmgl_end(s->lcmgl);
	bot_lcmgl_switch_buffer(s->lcmgl);
    }
    

    //return true to keep running
    return TRUE;
}

int 
main(int argc, char **argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    state->lcm =  bot_lcm_get_global(NULL);
    state->velodyne = velodyne_extractor_init(state->lcm, &velodyne_update_handler, state);
    state->b_server = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->b_server);
    state->lcmgl = bot_lcmgl_init(state->lcm,"doorway-detector");

    state->points = NULL;

    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    /* heart beat*/
    g_timeout_add (500, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}
