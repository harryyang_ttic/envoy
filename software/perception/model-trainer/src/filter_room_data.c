
//read training data file and segment the data to equal sizes and splits to training and testing 

#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>

static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -o Output_file_name      Specify OutputFile name to save training and test data\n",
             "  -i input_data_filename   Specify the input data file to split\n",
             "  -h                 This help message\n", 
             g_path_get_basename(progname));
    exit(1);
}

int 
main(int argc, char **argv)
{
    int c;
    const char *optstring = "i:o:h";
    char *output_file = NULL;
    char *input_data_file = NULL;
    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'o':
            output_file = strdup(optarg);  //choose log file for accuracy checking
            fprintf(stderr, "Saving to Training %s_train.txt\n\tTesting : %s_test.txt\n", output_file, output_file);
            break;
        case 'i':
            input_data_file = strdup(optarg);  //choose log file for accuracy checking
            fprintf(stderr, "Reading from Data file %s\n", input_data_file);
            break;
        case 'h': //help
            usage(argv[0]);
            break;
        default:
            usage(argv[0]);
            break;
        }
    }
    if(input_data_file == NULL){
        fprintf(stderr, "No input data file specified\n");
        usage(argv[0]);
    }

    if(output_file == NULL){
        output_file = "Data";
        fprintf(stderr, "No output name specified - using default : %s\n", output_file);
    }

    GHashTable *class_glists;

    class_glists = g_hash_table_new(g_int_hash, g_int_equal);

    FILE *file; 

    file = fopen(input_data_file,"r");
    char train_name[2000]; 
    char test_name[2000]; 
    sprintf(train_name, "%s_train.txt", output_file); 
    sprintf(test_name, "%s_test.txt", output_file); 
    FILE *train = fopen(train_name,"w");
    FILE *test = fopen(test_name,"w");

    char str[5000];
    int line_count = 0;
    if(!file) {fprintf(stderr, "No valid file\n"); return 1;} // bail out if file not found
    while(fgets(str,sizeof(str),file) != NULL){
        // strip trailing '\n' if it exists

        int len = strlen(str)-1;
        if(str[len] == '\n') 
            str[len] = 0;
        int class_id = atoi(&str[0]);

        GList **glist = (GList **) g_hash_table_lookup(class_glists, &class_id);
        if(glist==NULL){
            glist = (GList **) calloc(1,sizeof(GList *));
            int *id = (int *)calloc(1,sizeof(int));
            *id = class_id;
            g_hash_table_insert(class_glists, id, glist);
        }
            
        *glist = g_list_prepend (*glist, strdup(str));
	            
        line_count++;
    }
    fclose(file);
    fprintf(stderr, "Count : %d\n", line_count);

    int min_examples = 100000;
    
    GHashTableIter iter;
    gpointer key, value;

    g_hash_table_iter_init (&iter, class_glists);
    while (g_hash_table_iter_next (&iter, &key, &value)){
        /* do something with key and value */
        fprintf(stderr, "Class ID : %d - No of examples : %d\n", *(int *) key,
                g_list_length(*(GList **) value)); 
        if(min_examples > g_list_length(*(GList **) value)){
            min_examples = g_list_length(*(GList **) value);
        }        
    }

    int no_training = min_examples / 4.0 * 3.0;
    int no_testing = min_examples - no_training;

    fprintf(stderr, "No training : %d No training : %d\n", no_training, no_testing);

    g_hash_table_iter_init (&iter, class_glists);

    while (g_hash_table_iter_next (&iter, &key, &value)){
        GList *glist = *(GList **) value;
        fprintf(stderr, "Class %d : Count : %d\n", *(int *) key , g_list_length(glist));
      
        double threshold_training = no_training / ((double) g_list_length(glist));

        double threshold_testing = threshold_training + no_testing / ((double) g_list_length(glist));

        int training_count = 0;
        int testing_count = 0; 

        for(int j=0; j < g_list_length(glist) ; j++){
            GList* current = g_list_nth(glist, j);
        
            char *curr_str = (char *) current->data;
	
            double rand_val = (double)rand()/ (RAND_MAX);// + 1.0);
            // fprintf(stderr, "Rand Value : %f - threshold : %f\n",rand_val, threshold_positive);
            if(rand_val < threshold_training){
                training_count++;
                fprintf(train, "%s\n", curr_str);
            }
            else if(rand_val < threshold_testing){
                testing_count++;
                fprintf(test, "%s\n",curr_str);
            }
        }
        fprintf(stderr, "Class : %d - Training : %d Testing : %d Total : %d\n", 
                * (int *) key, training_count, testing_count, g_list_length(glist));
    }    
   
    fclose(train);
    fclose(test);
    
}
