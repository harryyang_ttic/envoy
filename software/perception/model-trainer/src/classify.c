//Add a executable er-place-classifier to classify places
//Requires: er-simulate-360-laser(SKIRT_SIM messages)
//Publish: place_classification_t & place_classification_debug_t messages


#include <svm/svm.h>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <glib.h>
#include <bot_core/bot_core.h>
#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>
#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <laser_features/laser_feature.h>
#include <full_laser/full_laser.h>

#define ANNOTATION_BIN_SIZE 100
#define THRESHOLD 0.4

typedef struct svm_model svm_model_t;
typedef struct svm_node svm_node_t;

typedef struct
{
    lcm_t *lcm;
    GMainLoop *mainloop;
    svm_model_t *model;
    svm_model_t *doorway_model;  //A model for binary doorway classifier
    bot_core_planar_lidar_t *last_laser;
    GMutex *mutex; 
    int num_annotations;
    BotPtrCircular *annotation_list;
    int correct;
    int total;
    full_laser_state_t *full_laser;
    int doorway;
}state_t;


//Check whether classifier is confident with the classification
int check_classification(double *values, int no_class){
    for(int i = 0; i < no_class;i++){
        if(values[i] > THRESHOLD)
            return 1;
    }
    return 0;
}



double *calculate_features(feature_config_t config, bot_core_planar_lidar_t *laser)
{
    //calculate feature values
    int no_features = config.no_of_fourier_coefficient * 4 + 38;
    raw_laser_features_t *raw_features = extract_raw_laser_features(laser, config);
    polygonal_laser_features_t *polygonal_features = extract_polygonal_laser_features(laser, config);
    double *values = (double *) calloc(no_features, sizeof(double));
    values[0] = raw_features->avg_difference_consecutive_beams;
    values[1] = raw_features->std_dev_difference_consecutive_beams;
    values[2] = raw_features->cutoff_avg_difference_consecutive_beams;
    values[3] = raw_features->avg_beam_length;
    values[4] = raw_features->std_dev_beam_length;
    values[5] = raw_features->no_gaps_in_scan_threshold_1;
    values[6] = raw_features->no_relative_gaps;
    values[7] = raw_features->no_gaps_to_num_beam;
    values[8] = raw_features->no_relative_gaps_to_num_beam;
    values[9] = raw_features->distance_between_two_smalled_local_minima;
    values[10] = raw_features->index_distance_between_two_smalled_local_minima;
    values[11] = raw_features->avg_ratio_consecutive_beams;
    values[12] = raw_features->avg_to_max_beam_length;
    values[13] = polygonal_features->area;
    values[14] = polygonal_features->perimeter;
    values[15] = polygonal_features->area_to_perimeter;
    values[16] = polygonal_features->PI_area_to_sqrt_perimeter;
    values[17] = polygonal_features->PI_area_to_perimeter_pow;
    values[18] = polygonal_features->sides_of_polygon;
    for(int i = 0; i <= config.no_of_fourier_coefficient * 2;i++){
        values[19 + i * 2] = polygonal_features->fourier_transform_descriptors[0].results->real;
        values[20 + i * 2] = polygonal_features->fourier_transform_descriptors[0].results->complex;
    }
    int start = config.no_of_fourier_coefficient * 4 + 21;
    values[start] = polygonal_features->major_ellipse_axis_length_coefficient;
    values[start + 1] = polygonal_features->minor_ellipse_axis_length_coefficient;
    values[start + 2] = polygonal_features->major_to_minor_coefficient;
    for(int i = 0; i < 7;i++){
        values[start + 3 + i] = polygonal_features->central_moment_invariants[i];
    }
    values[start + 10] = polygonal_features->normalized_feature_of_compactness;
    values[start + 11] = polygonal_features->normalized_feature_of_eccentricity;
    values[start + 12] = polygonal_features->mean_centroid_to_shape_boundary;
    values[start + 13] = polygonal_features->max_centroid_to_shape_boundary;
    values[start + 14] = polygonal_features->mean_centroid_over_max_centroid;
    values[start + 15] = polygonal_features->std_dev_centroid_to_shape_boundry;
    values[start + 16] = polygonal_features->kurtosis;
    return values;
}

static void
full_laser_update_handler(int64_t utime, bot_core_planar_lidar_t *msg, void *user)
{
    state_t *s = (state_t *)user;

    g_mutex_lock (s->mutex);

    if(s->last_laser != NULL){
        bot_core_planar_lidar_t_destroy(s->last_laser);
    }
    s->last_laser = bot_core_planar_lidar_t_copy(msg);
  
    g_mutex_unlock (s->mutex);
 
}


gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;

    g_mutex_lock (s->mutex);
    static int64_t last_utime = 0;

    if(s->last_laser ==NULL || (s->last_laser->utime == last_utime)){
        g_mutex_unlock (s->mutex);
        return TRUE;
    }
  
    last_utime = s->last_laser->utime;
  
    bot_core_planar_lidar_t *laser = s->last_laser;

    //Change the feature configurations here. min_range, max_range, etc..
    feature_config_t config;
    config.min_range = 0.1;
    config.max_range = 30;
    config.cutoff = 5;
    config.gap_threshold_1 = 3;
    config.gap_threshold_2 = 0.5;
    config.deviation = 0.001;
    config.no_of_fourier_coefficient = 2;
  
    int no_features = config.no_of_fourier_coefficient * 4 + 38;
  
    double *feature_values = calculate_features(config, laser);
  
    g_mutex_unlock (s->mutex);
    
    svm_node_t *test_pt;
    test_pt = (svm_node_t *) calloc(no_features + 1, sizeof(svm_node_t));
    for(int i = 0;i < no_features;i++){
        test_pt[i].index = i + 1;
        test_pt[i].value = feature_values[i];
    }
    test_pt[no_features].index = -1;
    //predict label
    int label = (int)svm_predict(s->model, test_pt);
    int doorway_label;
    if(s->doorway)
        doorway_label = (int)svm_predict(s->doorway_model, test_pt);
    //predict probability
    int class = svm_get_nr_class(s->model);
    // fprintf(stderr, "Class Num: %d\n", class); 
    double probability[class];
    if(svm_check_probability_model(s->model) == 0){
        fprintf(stderr, "Model does not have probability information.\n");
        for(int i = 0;i < class;i++)
            probability[i] = 0;
    }

    double value = svm_predict_probability(s->model, test_pt, probability);
    free(test_pt);

    //A list of classes
    char *classes[class];
    classes[0] = "elevator";
    classes[1] = "conference_room";
    classes[2] = "office";
    classes[3] = "lab";
    classes[4] = "open_area";
    classes[5] = "hallway";
    classes[6] = "corridor";
    classes[7] = "large_meeting_room";
    char *predict;
    predict = classes[label - 1];

    //  fprintf(stderr, "Feature calculation time : %f\n", (end - start) /1.0e6);

    //Publish place_classification_t & place_classification_debug_t messages
    erlcm_place_classification_t msg1;
    erlcm_place_classification_debug_t msg2;


    //Check whether the classifier is confident with his classifications.
    //If uncertain, might be a good idea to call the binary doorway classifier & see whether it is a doorway. 
    int confidence = check_classification(probability, class);

    if(confidence || !s->doorway){

        msg1.utime = bot_timestamp_now();

        //  fprintf(stderr, "Label : %s\n", predict);
        msg1.label = predict;
        msg1.sensor_type = ERLCM_PLACE_CLASSIFICATION_T_SENSOR_TYPE_SIMULATE_360_SKIRT;
        msg1.sensor_utime = s->last_laser->utime;
        msg1.pose_utime = msg1.sensor_utime;
        msg1.no_class = class;
        msg1.classes = (erlcm_place_classification_class_t *)calloc(msg1.no_class, sizeof(erlcm_place_classification_class_t));

        for(int i = 0; i < class;i++){
            msg1.classes[i].name = i + 1;
            msg1.classes[i].probability = probability[i];
        }
    }
    else{
        msg1.utime = bot_timestamp_now();
        msg1.sensor_type = ERLCM_PLACE_CLASSIFICATION_T_SENSOR_TYPE_SIMULATE_360_SKIRT;
        msg1.sensor_utime = s->last_laser->utime;
        msg1.pose_utime = msg1.sensor_utime;
        //If this is not a doorway
        if(!doorway_label){
            //  fprintf(stderr, "Label : %s\n", predict);
            msg1.label = predict;
            msg1.no_class = class;
            msg1.classes = (erlcm_place_classification_class_t *)calloc(msg1.no_class, sizeof(erlcm_place_classification_class_t));
            for(int i = 0; i < class;i++){
                msg1.classes[i].name = i + 1;
                msg1.classes[i].probability = probability[i];
            }
        }
        else{
            fprintf(stderr, "Possible doorway.\n");
            msg1.label = "doorway";
            msg1.no_class = class + 1;
            msg1.classes = (erlcm_place_classification_class_t *)calloc(msg1.no_class, sizeof(erlcm_place_classification_class_t));
            for(int i = 0; i < class;i++){
                msg1.classes[i].name = i + 1;
                msg1.classes[i].probability = probability[i];
            }
            //Only the last class has probability 1 since it is doorway
            msg1.classes[class].name = class + 1;
            msg1.classes[class].probability = 1;
        }
    }

  
    //Check whether this is a correct prediction
    if(s->num_annotations){
        char *real_class;
        for(int i = 0;i < bot_ptr_circular_size(s->annotation_list);i++){
            erlcm_annotation_t *iter = (erlcm_annotation_t *)bot_ptr_circular_index(s->annotation_list, i);
            int64_t start_utime = iter->start_utime;
            int64_t end_utime = iter->end_utime;
            if(start_utime <= msg1.sensor_utime && end_utime >= msg1.sensor_utime){
                real_class = iter->annotation;
                break;
            }
        }
        if(strcmp(real_class, predict) == 0){
            fprintf(stderr, "Correct prediction: %s\n", real_class);
            s->correct++;
        }
        else{
            fprintf(stderr, "Wrong prediction. Prediction by classfier: %s, Real: %s\n", predict, real_class);
        }
        s->total++;
        fprintf(stderr, "Accuracy so far: %.2f\n", (double)s->correct / s->total);
    }

    //  int64_t start = bot_timestamp_now();
    //  fprintf(stderr, "Label : %s\n", msg1.label);
    msg2.utime = bot_timestamp_now();
    msg2.no_features = no_features;
    msg2.features = (erlcm_laser_feature_t *) calloc(msg2.no_features, sizeof(erlcm_laser_feature_t));
    for(int i = 0; i < no_features;i++){
        msg2.features[i].value = feature_values[i];
        if(i <= 18){
            msg2.features[i].type = i;
        }
        else if(i > 18 && i < config.no_of_fourier_coefficient * 4 + 21){
            msg2.features[i].type = 19;
        }
        else if(i >= config.no_of_fourier_coefficient * 4 + 21 && i < config.no_of_fourier_coefficient * 4 + 24){
            msg2.features[i].type = i - config.no_of_fourier_coefficient * 4 - 21 + 20;
        }
        else if(i >= config.no_of_fourier_coefficient * 4 + 24 && i < config.no_of_fourier_coefficient * 4 + 31){
            msg2.features[i].type = 23;
        }
        else{
            msg2.features[i].type = i - config.no_of_fourier_coefficient * 4 - 31 + 24;
        }
    }
    //publish
    erlcm_place_classification_t_publish(s->lcm, "PLACE_CLASSIFICATION", &msg1);
    erlcm_place_classification_debug_t_publish(s->lcm, "PLACE_CLASSIFICATION_DEBUG", &msg2);
    free(msg1.classes);
    free(msg2.features);

    //return true to keep running
    return TRUE;
}

void
circ_free_annotation(void *user, void *p){
    erlcm_annotation_t *np = (erlcm_annotation_t *)p;
    erlcm_annotation_t_destroy(np);
}


static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -m MODEL  Specify SVM model\n"
             "  -l LOG    Specify Log File\n"
             "  -d BINARY_MODEL       Add doorways\n"
             "  -h        This help message\n", 
             g_path_get_basename(progname));
    exit(1);
}


int 
main(int argc, char **argv)
{

    const char *optstring = "m:l:d:h";

    int c;
    int doorway = 0;
    char *model_path = NULL;
    char *log_file = NULL;
    char *doorway_model = NULL;

    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'm': //choose SVM model
            model_path = strdup(optarg);
            printf("Using SVM model \"%s\"\n", model_path);
            break;
        case 'l':
            log_file = strdup(optarg);  //choose log file for accuracy checking
            fprintf(stderr, "Using LOG file \"%s\"\n", log_file);
            break;
        case 'd':  //whether to add doorway class
            doorway = 1;
            doorway_model = strdup(optarg);
            fprintf(stderr, "Using doorway model \"%s\"\n", doorway_model);
            break;
        case 'h': //help
            usage(argv[0]);
            break;
        default:
            usage(argv[0]);
            break;
        }
    }

    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  

    //predict label, we can either use one_vs_one or one_vs_all
    state->last_laser = NULL;
  
    state->model = (svm_model_t *) calloc(1, sizeof(svm_model_t));

    state->doorway_model = (svm_model_t *)calloc(1, sizeof(svm_model_t));

    state->full_laser = full_laser_init(state->lcm, 360, &full_laser_update_handler, state);
  
    state->mutex =  g_mutex_new();//g_mutex_init ();

    state->doorway = doorway;

    if(model_path != NULL){
        state->model = svm_load_model(model_path);
        if(state->model == NULL){
            fprintf(stderr, "Error loading model.\n");
            return -1;
        }
        else
            fprintf(stderr, "Model loaded success.\n");
    }

    //this was a seperate model for detecting doorway-vs-non-doorway scans 
    //we have a template based door detector which gives better results 
    if(doorway_model != NULL){
        state->doorway_model = svm_load_model(doorway_model);
        if(state->doorway_model == NULL){
            fprintf(stderr, "Error loading model.\n");
            return -1;
        }
        else
            fprintf(stderr, "Model loaded success.\n");
    }
  
    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }

    if(log_file && doorway)
        fprintf(stderr, "Classifications contain doorway class. Ensure your annotation log has doorway class.\n");
  

    state->num_annotations = 0;
    //If want to check accuracy, load the annotations in log file. 
    if(log_file != NULL){
        fprintf(stderr, "System is loading the annotation messages in log for accuracy checking.Please wait a while.\n");
        lcm_eventlog_t *read_log = lcm_eventlog_create(log_file, "r");
        fprintf(stderr, "Log open success.\n");
        state->annotation_list = bot_ptr_circular_new(ANNOTATION_BIN_SIZE, circ_free_annotation, state);

        state->correct = 0;
        state->total = 0;
  
        //Adding to annotation circular buffer
        lcm_eventlog_event_t *event = (lcm_eventlog_event_t *)calloc(1, sizeof(lcm_eventlog_event_t));
        char *annotation_channel_name = "LOG_ANNOTATION";
        for(event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){
            int decode_status = 0;
            if(strcmp(annotation_channel_name, event->channel) == 0){
                erlcm_annotation_t *msg = (erlcm_annotation_t *)calloc(1, sizeof(erlcm_annotation_t));
                decode_status = erlcm_annotation_t_decode(event->data, 0, event->datalen, msg);
                if(decode_status < 0){
                    fprintf(stderr, "Error decoding message.\n");
                    return -1;
                }
      
                bot_ptr_circular_add(state->annotation_list, msg);
                state->num_annotations++;
            }
            lcm_eventlog_free_event(event);
        }
        fprintf(stderr, "No. of annotations in the log: %d\n", state->num_annotations);
        free(event);
        if(state->num_annotations)
            fprintf(stderr, "Finish loading annotations in the log.\n");
        else
            fprintf(stderr, "No annotation messages in the log.\n");
        
    }


    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    //read_parameters_from_conf(state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);

    /* heart beat*/
    g_timeout_add(200, heartbeat_cb, state);
    
    //    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}
