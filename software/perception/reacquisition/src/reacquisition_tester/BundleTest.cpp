#include <string>
#include <ctime>
#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>

#include "BundleAdjust.hpp"
#include "BundleAdjustSparse.hpp"
#include "BundleIntersection.hpp"
#include "BundleResection.hpp"
#include "GeomTypes.hpp"
#include "Utilities.hpp"
#include "IoUtilities.hpp"
#include "LensModel.hpp"

#include <bot_core/bot_core.h>
#include <bot_param/param_client.h>

#define SPARSE_BUNDLE_ADJUST


using namespace reacq;
using namespace std;

double getRand(const double iMin, const double iMax) {
    return (iMin + (double)rand()/RAND_MAX*(iMax-iMin));
}

int main (const int argc, const char **argv)
{
    int returnVal = 0;
    cout << "Setting up unit test." << endl;
    const int npts = 2000;
    const int eps = 1.e-8;

    lcm_t *lcm = bot_lcm_get_global (NULL);
    BotParam *param = bot_param_new_from_server (lcm, 0);

    LensModel lensModel;
    //lensModel.setFromParam(param, "cameras.roof_right.vision.reacquisition.calibration");
    lensModel.setFromParam(param, "bumblebee_left");

    cout << "Setting up random set of " << npts << " points." << endl;
    srand(0);
    // World points
    vector<Vector3> worldPts(npts);
    for (int i=0; i<npts; ++i) {
        worldPts[i](0) = getRand(-500,500);
        worldPts[i](1) = getRand(-500,500);
        worldPts[i](2) = getRand(0,1000);
    }

//#define MATLAB
#ifdef MATLAB

    ofstream ofs("/share/BundleTest.m", ios::out);
    ofs << "world = [ " << endl;
    for (int i=0; i<npts; ++i) {
        ofs << worldPts[i].getTranspose();
    }
    ofs << "];" << endl;

#endif //MATLAB

    cout << "Setting up cameras." << endl;

    Matrix33 rot1;
    rot1.makeIdentity();

    Vector3 trans1;
    trans1.zero();
    
    vector<Vector3> imagePts1(npts);
    for (int i=0; i<npts; ++i) {
        Utilities::projectPointToImage(worldPts[i],
                                       rot1,
                                       trans1,
                                       &lensModel,
                                       imagePts1[i]);
    }

    Matrix33 rot2;
    rot2.makeIdentity();
    
    Vector3 trans2;
    trans2.zero();
    trans2(0) = 1.;

    vector<Vector3> imagePts2(npts);
    for (int i=0; i<npts; ++i) {
        Utilities::projectPointToImage(worldPts[i],
                                       rot2,
                                       trans2,
                                       &lensModel,
                                       imagePts2[i]);
    }

    for (int i=0; i<npts; ++i) {
        cout << setprecision(5) << fixed
             << "world[" << i << "]={" << worldPts[i](0) << ","
             << worldPts[i](1) << "," << worldPts[i](2) << "};"
             << "img1={" << imagePts1[i](0) << ","
             << imagePts1[i](1) <<  "};"
             << "img2={" << imagePts2[i](0) << ","
             << imagePts2[i](1) << "}"
             << endl;
    }
    
    cout << "Setting up bundle adjust." << endl;
    Poses poses(2);
    for (int i=0; i<2; ++i) {
        poses[i] = new Pose();
        poses[i]->mOrientation.makeIdentity();
        poses[i]->mPosition.zero();
    }
    /*
    poses[0]->mOrientation = rot1;
    poses[0]->mPosition = trans1;
    poses[1]->mOrientation = rot2;
    poses[1]->mPosition = trans2;
    */

#ifdef SPARSE_BUNDLE_ADJUST
    BundleAdjustSparse bundleAdjust;
#else
    BundleAdjust bundleAdjust;
#endif
    Measurements meas(2*npts);
    for (int i=0; i<npts; ++i) {
        meas[2*i] = new Measurement();
        meas[2*i]->mImagePoint(0) = imagePts1[i](0);
        meas[2*i]->mImagePoint(1) = imagePts1[i](1);
        meas[2*i]->mModelPointIndex = i;
        meas[2*i]->mPoseIndex = 0;
        meas[2*i+1] = new Measurement();
        meas[2*i+1]->mImagePoint(0) = imagePts2[i](0);
        meas[2*i+1]->mImagePoint(1) = imagePts2[i](1);
        meas[2*i+1]->mModelPointIndex = i;
        meas[2*i+1]->mPoseIndex = 1;
    }

#ifdef MATLAB
    ofs << IoUtilities::measurementsToMatlabString("bundleMeasurements",
                                                   meas);
    ofs.close();
#endif //MATLAB
    
    ModelPoints scenePts(npts);
    Vector3 ones;
    ones(0) = 1;
    ones(1) = 1;
    ones(2) = 1;
    for (int i=0; i<npts; ++i) {
        scenePts[i] = new ModelPoint();
        scenePts[i]->mPoint = ones;

        Vector3 ray;
        lensModel.pixelToRay(imagePts1[i](0), imagePts1[i](1), ray);
        ray.normalize();
        ray = ray*10;
        //        scenePts[i]->mPoint(0) = ray(0);
        //        scenePts[i]->mPoint(1) = ray(1);
        //        scenePts[i]->mPoint(2) = ray(2);
        /*
        scenePts[i]->mPoint(0) = 1000*(double)rand()/(double)RAND_MAX;
        scenePts[i]->mPoint(1) = 1000*(double)rand()/(double)RAND_MAX;
        scenePts[i]->mPoint(2) = 1000*(double)rand()/(double)RAND_MAX;
        */
        // scenePts[i]->mPoint = worldPts[i];
        //        scenePts[i]->mPoint(0) += 0.001;
    }
    //    scenePts[0]->mPoint(0) += 1;


    cout << "Running bundle adjust." << endl;
#ifdef SPARSE_BUNDLE_ADJUST
    bundleAdjust.setRobustKernel(BundleAdjustSparse::RobustKernelTukey);
    bundleAdjust.setVerbose(true);
    BundleAdjustSparse::Result result =
        bundleAdjust.solve(meas, poses, scenePts, &lensModel);
#else
    RobustLM::Result result =
        bundleAdjust.solve(meas, poses, scenePts, &lensModel);
#endif
    cout << "Iters: " << result.mIterations << endl;

    return 1;

    cout << "Comparing measured points to actual points." << endl;

    double sumDpose = 0.;
    double dpose = 0.;

    // x_reproj_1, x_reproj_2, delta = errors(i), weight
    // same for y
    int nerrors = 0;

    cout << "----------- Bundle Adjustment Results ----------" << endl;
    Vector3 pt1, pt2;
    for (int i=0; i <npts; ++i) {
        dpose = (scenePts[i]->mPoint - worldPts[i]).getMagnitude();
        cout << "Results for point [" << i << "]" << endl;
        cout << "   * world point difference = " << dpose << endl;
        sumDpose += dpose;
        Utilities::projectPointToImage(scenePts[i]->mPoint,
                                       poses[0]->mOrientation,
                                       poses[0]->mPosition,
                                       &lensModel,
                                       pt1);
        Utilities::projectPointToImage(scenePts[i]->mPoint,
                                       poses[1]->mOrientation,
                                       poses[1]->mPosition,
                                       &lensModel,
                                       pt2);
        double dx1m = pt1(0) - imagePts1[i](0);
        double dx2m = pt2(0) - imagePts2[i](0);
        double drox1m = result.mErrors(4*i);
        double drox2m = result.mErrors(4*i+2);
        double dy1m = pt1(1) - imagePts1[i](1);
        double dy2m = pt2(1) - imagePts2[i](1);
        double droy1m = result.mErrors(4*i+1);
        double droy2m = result.mErrors(4*i+3);
//         cout << "   * computed dx_0, dx_1 = ["
//              << dx1m << ", " << dx2m
//              << "]" << endl;
//         cout << "   * computed dy_0, dy_1 = ["
//              << dy1m << ", " << dy2m
//              << "]" << endl;
        cout << "   * result object dx_0, dy_0, w(x), w(y) = ["
             << drox1m << ", " << droy1m << ", "
             << result.mWeights(4*i) << ", "
             << result.mWeights(4*i+1) << "]" << endl;
        cout << "   * result object dx_1, dy_1, w(x), w(y) = ["
             << drox2m << ", " << droy2m << ", "
             << result.mWeights(4*i+2) << ", "
             << result.mWeights(4*i+3) << "]" << endl;
        if (fabs(dx1m - drox1m) > eps) {
            cerr << "ERROR! Computed != Result object (x1)" << endl;
            nerrors++;
        }
        if (fabs(dx2m - drox2m) > eps) {
            cerr << "ERROR! Computed != Result object (x2)" << endl;
            nerrors++;
        }
        if (fabs(dy1m - droy1m) > eps) {
            cerr << "ERROR! Computed != Result object (y1)" << endl;
            nerrors++;
        }
        if (fabs(dy2m - droy2m) > eps) {
            cerr << "ERROR! Computed != Result object (y2)" << endl;
            nerrors++;
        }
    }
    if (0 == nerrors) {
        cout << "No reprojection errors found." << endl;
    } else {
        cout << "Found " << nerrors << " errors." << endl;
        returnVal = 1;
    }

    nerrors = 0;

    cout << "----------- Bundle Intersection Run and Results ----------" << endl;

    Measurements intersectMeas(2);
    BundleIntersection intersection;
    for (int i=0; i<npts; ++i) {
        intersectMeas[0] = meas[2*i];
        intersectMeas[1] = meas[2*i+1];
        RobustLM::Result res =
            intersection.solve(intersectMeas, *scenePts[i],
                               poses, &lensModel);
        cout << "Results for point [" << i << "]" << endl;
        dpose = (scenePts[i]->mPoint - worldPts[i]).getMagnitude();
        cout << "   * world point difference = " << dpose << endl;
        Utilities::projectPointToImage(scenePts[i]->mPoint,
                                       poses[0]->mOrientation,
                                       poses[0]->mPosition,
                                       &lensModel,
                                       pt1);
        Utilities::projectPointToImage(scenePts[i]->mPoint,
                                       poses[1]->mOrientation,
                                       poses[1]->mPosition,
                                       &lensModel,
                                       pt2);
        double dx1m = pt1(0) - imagePts1[i](0);
        double dx2m = pt2(0) - imagePts2[i](0);
        double drox1m = res.mErrors(0);
        double drox2m = res.mErrors(2);
        double dy1m = pt1(1) - imagePts1[i](1);
        double dy2m = pt2(1) - imagePts2[i](1);
        double droy1m = res.mErrors(1);
        double droy2m = res.mErrors(3);
        cout << "   * result object dx_0, dy_0, w(x), w(y) = ["
             << drox1m << ", " << droy1m << ", "
             << res.mWeights(0) << ", "
             << res.mWeights(1) << "]" << endl;
        cout << "   * result object dx_1, dy_1, w(x), w(y) = ["
             << drox2m << ", " << droy2m << ", "
             << res.mWeights(2) << ", "
             << res.mWeights(3) << "]" << endl;
        if (fabs(dx1m - drox1m) > eps) {
            cerr << "ERROR! Computed != Result object (x1)" << endl;
            nerrors++;
        }
        if (fabs(dx2m - drox2m) > eps) {
            cerr << "ERROR! Computed != Result object (x2)" << endl;
            nerrors++;
        }
        if (fabs(dy1m - droy1m) > eps) {
            cerr << "ERROR! Computed != Result object (y1)" << endl;
            nerrors++;
        }
        if (fabs(dy2m - droy2m) > eps) {
            cerr << "ERROR! Computed != Result object (y2)" << endl;
            nerrors++;
        }
    }

    if (0 == nerrors) {
        cout << "No reprojection errors found." << endl;
    } else {
        cout << "Found " << nerrors << " errors." << endl;
        returnVal = 1;
    }


    Matrix33 rot3;
    rot3.makeIdentity();

    Vector3 trans3;
    trans3.zero();
    trans3(0) = -1.;
    
    vector<Vector3> imagePts3(npts);
    for (int i=0; i<npts; ++i) {
        Utilities::projectPointToImage(worldPts[i],
                                       rot3,
                                       trans3,
                                       &lensModel,
                                       imagePts3[i]);
    }
    
    Measurements resecMeas(npts);
    for (int i = 0; i < npts; ++i) {
        Measurement* meas2 = new Measurement;
        meas2->mImagePoint(0) = imagePts3[i](0);
        meas2->mImagePoint(1) = imagePts3[i](1);
        meas2->mModelPointIndex = i;
        meas2->mPoseIndex = 0; //Don't care for now
        resecMeas[i] = meas2;
    }

    ModelPoints truthPts(npts);
    for (int i=0; i<npts; ++i) {
        truthPts[i] = new ModelPoint();
        truthPts[i]->mPoint = worldPts[i];
    }
    scenePts[0]->mPoint(0) += 0.001;
    Pose pose;
    pose.mOrientation.makeIdentity();
    pose.mPosition.zero();
    BundleResection resection;

    RobustLM::Result resecResult = resection.solve(resecMeas,
                                                   truthPts,
                                                   pose, &lensModel);

    cout << "----------- Bundle Resection Results ----------" << endl;
    for (int i=0; i <npts; ++i) {
        cout << "Results for point [" << i << "]" << endl;
        Utilities::projectPointToImage(truthPts[i]->mPoint,
                                       pose.mOrientation,
                                       pose.mPosition,
                                       &lensModel,
                                       pt1);
        double dx1m = pt1(0) - imagePts3[i](0);
        double drox1m = resecResult.mErrors(2*i);
        double dy1m = pt1(1) - imagePts3[i](1);
        double droy1m = resecResult.mErrors(2*i+1);
        cout << "   * result object dx, dy, w(x), w(y) = ["
             << drox1m << ", " << droy1m << ", "
             << resecResult.mWeights(2*i) << ", "
             << resecResult.mWeights(2*i+1) << "]" << endl;
        if (fabs(dx1m - drox1m) > eps) {
            cerr << "ERROR! Computed != Result object (x1)" << endl;
            nerrors++;
        }
        if (fabs(dy1m - droy1m) > eps) {
            cerr << "ERROR! Computed != Result object (y1)" << endl;
            nerrors++;
        }
    }
    if (0 == nerrors) {
        cout << "No reprojection errors found." << endl;
    } else {
        cout << "Found " << nerrors << " errors." << endl;
        returnVal = 1;
    }

    for (int i=0; i<poses.size(); ++i) {
        delete poses[i];
    }
    for (int i=0; i<resecMeas.size(); ++i) {
        delete resecMeas[i];
    }
    for (int i=0; i<meas.size(); ++i) {
        delete meas[i];
    }
    for (int i=0; i<truthPts.size(); ++i) {
        delete truthPts[i];
    }
    for (int i=0; i<scenePts.size(); ++i) {
        delete scenePts[i];
    }
    return returnVal;
    return 0;
}
