#include "FeatureBuffer.hpp"

#include <string>
#include <glib.h>
#include <iostream>

using namespace std;
using namespace reacq;

int main (const int argc, const char **argv)
{
    string featureChannel = "FRNT_IMAGE_REACQ_SIFT";
    string poseChannel = "POSE";
    cout << "Creating feature buffer." << flush;
    FeatureBuffer buf(poseChannel, 200, featureChannel, 50, 0.5, 5.);
    cout << "Done." << endl;
    buf.Start();
    cout << "Started feature buffer." << endl;

    GMainLoop *mainloop;
    mainloop = g_main_loop_new (NULL, FALSE);
    g_main_loop_run (mainloop);

    return 0;
}
