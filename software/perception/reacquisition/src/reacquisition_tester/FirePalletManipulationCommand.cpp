#include <iostream>
#include <string>

#include <lcmtypes/erlcm_reacquisition_command_list_t.h>
#include <common/globals.h>

using namespace std;

static void usage(const char *app)
{
    cout << "Usage: " << app << " channel object_id" << endl
         << endl
         << "      * channel = output channel (PALLET_MANIPULATION_COMMAND_LIST_LABELS)" << endl
         << "      * object_id = <numeric value>" << endl;
}

int main (int argc, char *argv[])
{
    if (argc < 3) {
        usage(argv[0]);
        exit(1);
    }
    string channel(argv[1]);
    int64_t objectId = atoi(argv[2]);

    lcm_t *lcm = globals_get_lcm ();

    erlcm_reacquisition_command_list_t msg;
    msg.utime = bot_timestamp_now ();
    msg.num_commands = 1;
    erlcm_reacquisition_command_t cmds[1];
    cmds[0].camera = (char*) malloc (30*sizeof(char));
    cmds[0].utime = bot_timestamp_now ();
    cmds[0].destination_id = 1;
    cmds[0].task_id = 2;
    cmds[0].pickup = false;
    cmds[0].pallet_id = objectId;
    cmds[0].roi_utime = bot_timestamp_now ();
    sprintf(cmds[0].camera, "FRNT_IMAGE");
    cmds[0].pose.utime = bot_timestamp_now ();
    cmds[0].pose.pos[0] = 0.;
    cmds[0].pose.pos[1] = 1.;
    cmds[0].pose.pos[2] = 2.;
    cmds[0].pose.vel[0] = 3.;
    cmds[0].pose.vel[1] = 4.;
    cmds[0].pose.vel[2] = 5.;
    cmds[0].pose.orientation[0] = 0.;
    cmds[0].pose.orientation[1] = 0.;
    cmds[0].pose.orientation[2] = 0.;
    cmds[0].pose.orientation[3] = 1.;
    cmds[0].pose.rotation_rate[0] = 0.;
    cmds[0].pose.rotation_rate[1] = 0.;
    cmds[0].pose.rotation_rate[2] = 0.;
    cmds[0].pose.accel[0] = 0.;
    cmds[0].pose.accel[1] = 0.;
    cmds[0].pose.accel[2] = 0.;
    cmds[0].roi.npoints = 1;
    arlcm_point2d_t pts[1];
    pts[0].x = 0.;
    pts[0].y = 0.;
    cmds[0].roi.npoints = 1;
    cmds[0].roi.points = pts;
    cmds[0].image_utime = bot_timestamp_now ();
    msg.commands = cmds;

    erlcm_reacquisition_command_list_t_publish (lcm, channel.c_str(), &msg);
    cout << "Sent message with ID [" << objectId << "] on channel ["
         << channel << "]" << endl;

    globals_release_lcm (lcm);

    return 0;
}
