#include "MatrixStatic.hpp"
#include "MatrixDynamic.hpp"
#include "GeomTypes.hpp"

#include <string>
#include <iostream>

using namespace std;
using namespace reacq;

int main (const int argc, const char **argv)
{
    //static matrix
    cout << "Unitialized static matrix:" << endl;
    Matrix33 m1;
    cout << m1;
    cout << "--------" << endl;
    cout << "Identity static matrix:" << endl;
    m1.makeIdentity();
    cout << m1;
    cout << "--------" << endl;

    MatrixDynamic m2(3,4);
    cout << "Unitialized dynamic matrix:" << endl;
    cout << m2;
    cout << "--------" << endl;
    m2(1,2) = 23.;
    m2(2,3) = 34.;
    m2(0,3) = 14.;
    cout << "Some kind of dynamic matrix:" << endl;
    cout << m2;
    cout << "--------" << endl;

    cout << "m2 * m2'" << endl;
    cout << m2 * m2.getTranspose();
    cout << "--------" << endl;

    MatrixStatic<3,3> m3;
    m3(0,0) = 3;
    m3(0,1) = -1;
    m3(0,2) = 4;
    m3(1,0) = -2;
    m3(1,1) = -6;
    m3(1,2) = -5;
    m3(2,0) = -1;
    m3(2,1) = 9;
    m3(2,2) = 2;
    cout << "m3 = " << endl;
    cout << m3;
    cout << "--------" << endl;
    MatrixStatic<3,1> b;
    b(0,0) = 1;
    b(1,0) = 1;
    b(2,0) = 9;
    cout << "b = " << endl;
    cout << b;
    cout << "b has " << b.getNumRows() << " rows and " << b.getNumCols() << " columns." << endl;
    cout << "--------" << endl;
    MatrixStatic<3,1> x = m3.solve(b);
    cout << "x = " << endl;
    cout << x;
    cout << "--------" << endl;
    cout << "m3 * x = " << endl;
    cout << m3 * x;
    cout << "--------" << endl;

    MatrixDynamic m4(3,3);
    m4(0,0) = 3;
    m4(0,1) = -1;
    m4(0,2) = 4;
    m4(1,0) = -2;
    m4(1,1) = -6;
    m4(1,2) = -5;
    m4(2,0) = -1;
    m4(2,1) = 9;
    m4(2,2) = 2;
    cout << "m4 = " << endl;
    cout << m4;
    cout << "--------" << endl;
    MatrixDynamic b2(3,1);
    b2(0,0) = 1;
    b2(1,0) = 1;
    b2(2,0) = 9;
    cout << "b2 = " << endl;
    cout << b2;
    cout << "b2 has " << b2.getNumRows() << " rows and " << b2.getNumCols() << " columns." << endl;
    cout << "--------" << endl;
    MatrixDynamic x2 = m4.solve(b2);
    cout << "x2 = " << endl;
    cout << x2;
    cout << "--------" << endl;
    cout << "m4 * x2 = " << endl;
    cout << m4 * x2;
    cout << "--------" << endl;

    return 0;
}
