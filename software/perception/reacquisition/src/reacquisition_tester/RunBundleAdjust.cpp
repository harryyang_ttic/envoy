#include <string>
#include <ctime>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

#include "BundleAdjust.hpp"
#include "IoUtilities.hpp"
#include "LensModel.hpp"

#include <bot/bot_core.h>

#include <common/arconf.h>
#include <common/geometry.h>

using namespace reacq;
using namespace std;

int main (const int argc, const char **argv)
{
    if (3 != argc) {
        cerr << "Usage: " << argv[0] << " <filename_in> <filename_out>" << endl
             << "       <filename_in>:" << endl
             << "       " << "camera_name = <name (no spaces)>" << endl
             << "       " << "num_measurements = n" << endl
             << "       " << "x1 y1 model_index1 pose_index1" << endl
             << "                 ..." << endl
             << "       " << "xn yn model_indexn pose_indexn" << endl;
        exit(1);
    }

    ifstream ifs(argv[1], ios::in);
    if (!ifs.good()) {
        cerr << "ERROR: Could not open file [" << argv[1] << "] for reading." << endl;
        exit(1);
    }

    ofstream ofs(argv[2], ios::out);
    if (!ofs.good()) {
        cerr << "ERROR: Could not open file [" << argv[2] << "] for writing." << endl;
        ofs.close();
        exit(1);
    }
    
    string camName, stmp;
    int nmeas;
    int nposes = 0;
    int npts = 0;
    ifs >> stmp >> stmp >> camName;

    BotConf *conf = globals_get_config();
    lcm_t *lcm = bot_lcm_get_global (NULL);
    BotParam *param =  bot_param_new_from_server (lcm, 0);

    LensModel lens;
    lens.setFromParam(param, camName);


    ifs >> stmp >> stmp >> nmeas;
    Measurements meas(nmeas);
    double x,y;
    for (int i=0; i<nmeas; ++i) {
        meas[i] = new Measurement();
        ifs >> x >> y >> meas[i]->mModelPointIndex >> meas[i]->mPoseIndex;
        meas[i]->mImagePoint(0) = x;
        meas[i]->mImagePoint(1) = y;
        if (meas[i]->mPoseIndex > nposes) {
            nposes = meas[i]->mPoseIndex;
        }
        if (meas[i]->mModelPointIndex > npts) {
            npts = meas[i]->mModelPointIndex;
        }
    }
    ifs.close();

    /* compensate for zero-based pose and model indexing */
    nposes++;
    npts++;

    Poses poses(nposes);
    for (int i=0; i<nposes; ++i) {
        poses[i] = new Pose();
        poses[i]->mOrientation.makeIdentity();
        poses[i]->mPosition.zero();
    }
    ModelPoints model(npts);
    Vector3 ones;
    ones(0) = 1;
    ones(1) = 1;
    ones(2) = 1;
    for (int i=0; i<npts; ++i) {
        model[i] = new ModelPoint();
        model[i]->mPoint = ones;
    }
    
    BundleAdjust adjust;
    RobustLM::Result res = adjust.solve(meas, poses, model, &lens);

    string var("bundle_adjust_result");
    ofs << var << " = struct('pts',[],'poses',[],'errors',[],'camera',[]);" << endl;
    ofs << "% x y z" << endl;
    ofs << var << ".pts = [" << endl;
    for (int i=0; i<npts; ++i) {
        ofs << model[i]->mPoint(0) << ","
            << model[i]->mPoint(1) << ","
            << model[i]->mPoint(2) << endl;
    }
    ofs << "];" << endl;
    ofs << "% r11 r12 r13 r21 r22 r23 r31 r32 r33 t1 t2 t3" << endl;
    ofs << var << ".poses = [" << endl;
    for (int i=0; i<nposes; ++i) {
        for (int j=0; j<3; ++j) {
            for (int k=0; k<3; ++k) {
                ofs << poses[i]->mOrientation(j,k) << ", ";
            }
        }
        ofs << poses[i]->mPosition(0) << ", "
            << poses[i]->mPosition(1) << ", "
            << poses[i]->mPosition(2) << "; " << endl;

    }
    ofs << "];" << endl;
    ofs << "% e1x e1y e2x e2y ... enx eny" << endl;
    ofs << var << ".errors = [";
    for (int i=0; i<res.mErrors.getNumElements(); ++i) {
        ofs << res.mErrors(i) << " ";
    }
    ofs << "];" << endl;
    ofs << var << ".camera = '" << camName << "';" << endl;
    ofs.close();

    cout << "Wrote output to " << argv[2] << endl;

    for (int i=0; i<nmeas; ++i) {
        delete meas[i];
    }
    for (int i=0; i<nposes; ++i) {
        delete poses[i];
    }
    for (int i=0; i<npts; ++i) {
        delete model[i];
    }

    globals_release_config(conf);

    return 0;

}
