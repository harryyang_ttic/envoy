#include <string>
#include <ctime>
#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>

#include <er_reacquisition/Utilities.hpp>
#include <sift/sift.hpp>

#include <bot_core/bot_core.h>

/*#include <common/globals.h>
#include <common/arconf.h>
#include <common/geometry.h>*/


using namespace reacq;
using namespace std;

int main (const int argc, const char **argv)
{
    const double smallImageScale = 0.5;
    const int    siftLevels = 3;
    const double siftPeakThresh = 15;
    const int    siftDoubleImageSize = 0;
    const double siftSigmaInit = 1.6;

    ifstream ifs("SiftTestImage.jpg", ios::in | ios::binary);
    ifs.seekg(0, ios::end);
    int nbytes = ifs.tellg();
    char jpegData[nbytes];
    ifs.seekg(0, ios::beg);
    ifs.read(jpegData, nbytes);
    ifs.close();

    for (int i=0; i<10; ++i) {
        bot_core_image_t img;
        img.utime = 0;
        img.width = 1296;
        img.height = 964;
        img.row_stride = img.width * 3;
        img.pixelformat = PIXEL_FORMAT_MJPEG;
        img.size = nbytes;
        uint8_t data[nbytes];
        memcpy(data, jpegData, nbytes);
        img.data = data;
        img.nmetadata = 1;
        bot_core_image_metadata_t metadata[1];
        metadata[0].n = 1;
        char key[10];
        metadata[0].key = key;
        sprintf(key, "test");
        uint8_t value[1];
        value[0] = 1;
        metadata[0].value = value;
        img.metadata = metadata;

        cout << "Processing new sift..." << endl;

        bot_core_image_t fullImage, resizedImage;
        bot_core_image_t rgbBuf, grayBuf;

        Utilities::allocateMultiScaleBuffers(&img,
                                             &fullImage,
                                             &resizedImage,
                                             &rgbBuf,
                                             &grayBuf,
                                             smallImageScale,
                                             smallImageScale);

        Utilities::decompressToMultiScale(&img,
                                          &fullImage,
                                          &resizedImage,
                                          &rgbBuf,
                                          &grayBuf);
    
        sift_point_feature_list_t *firstContextPts =
            sift_compute_uncompressed(resizedImage.width, resizedImage.height,
                                      siftLevels,
                                      (float*)resizedImage.data,
                                      siftPeakThresh,
                                      siftDoubleImageSize,
                                      siftSigmaInit,
                                      img.utime);

        Utilities::scaleSiftFeatures(firstContextPts,
                                     1./smallImageScale,
                                     1./smallImageScale);

        Utilities::freeAlignedImage(&rgbBuf);
        Utilities::freeAlignedImage(&grayBuf);
        Utilities::freeAlignedImage(&resizedImage);
        Utilities::freeAlignedImage(&fullImage);

    }
}
