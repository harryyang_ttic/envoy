#include <iostream>
#include <string>
#include <sstream>

#include <lcmtypes/erlcm_reacquisition_command_list_t.h>
#include <bot_core/bot_core.h>

using namespace std;

static void usage(const char *app)
{
    cout << "Usage: " << app << "      channel type object_id" << endl
         << endl
         << "      * channel = output channel (REACQUISITION_COMMAND_LIST)" << endl
         << "      * type = LOOK/STOP/REMOVE/UPDATE" << endl
         << "      * object_id = <numeric value>" << endl;
}

int main (int argc, char *argv[])
{
    if (argc < 4) {
        usage(argv[0]);
        exit(1);
    }
    string channel(argv[1]);
    string type(argv[2]);
    int64_t commandId;
    if (type == "LOOK") {
        commandId = ERLCM_REACQUISITION_COMMAND_T_COMMAND_LOOK_FOR_OBJECT;
    } else if (type == "STOP") {
        commandId = ERLCM_REACQUISITION_COMMAND_T_COMMAND_STOP_LOOKING_FOR_OBJECT;
    } else if (type == "REMOVE") {
        commandId = ERLCM_REACQUISITION_COMMAND_T_COMMAND_REMOVE_OBJECT;
    } else if (type == "UPDATE") {
        commandId = ERLCM_REACQUISITION_COMMAND_T_COMMAND_UPDATE_OBJECT;
    } else {
        cerr << "Invalid type value [" << type << "]" << endl;
        usage(argv[0]);
        exit(1);
    }
    stringstream str;
    str << argv[3];
    int64_t objectId;
    str >> objectId;

    lcm_t *lcm = bot_lcm_get_global (NULL);

    erlcm_reacquisition_command_list_t msg;
    msg.utime = bot_timestamp_now ();
    msg.num_commands = 1;
    erlcm_reacquisition_command_t cmds[1];
    cmds[0].utime = bot_timestamp_now ();
    cmds[0].object_id = objectId;
    cmds[0].command_id = commandId;
    msg.commands = cmds;

    erlcm_reacquisition_command_list_t_publish (lcm, channel.c_str(), &msg);
    cout << "Published reacquisition for id " << objectId << " on channel "
         << channel << "." << endl;

    return 0;
}
