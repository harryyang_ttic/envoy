#include <string>
#include <glib.h>
#include <iostream>
#include <getopt.h>

#include <er_reacquisition/Reacquisition.hpp>

using namespace std;
using namespace reacq;

static void
Usage(const char *app)
{
    cerr << "Usage: " << app << " [options]" << endl
         << endl
         << "  -l, --learning-channel        input model channel name (required)" << endl
         << "                                erlcm_reacquisition_segmentation_list_t" << endl
         << "  -m, --matching-channel        input command channel name (required)" << endl
         << "                                erlcm_reacquisition_command_list_t" << endl
         << "  -p, --publish-channel         output detection channel name (required)" << endl
         << "                                erlcm_reacquisition_detection_list_t" << endl
         << "  -o, --pose-buffer-capacity    pose buffer capacity for image" << endl
         << "                                buffer (required, >= 1)" << endl
         << "  -i, --image-buffer-capacity   image buffer capacity (required, >= 1)" << endl
         << "  -s, --position-threshold      position change threshold for image" << endl
         << "                                buffer (required, >= 0)" << endl
         << "  -a, --angle-threshold         angle change threshold in degrees for" << endl
         << "                                image buffer (required, >= 0)" << endl
         << "  -w, --write-models-filename   output filename for writing models (required)" << endl
         << "  -r, --read-models-filename    input filename for reading models (optional)" << endl
         << "  -f, --matlab-results-filename output matlab results filename (optional)" << endl
         << "  -h, --help                    shows this help text and exits" << endl;

}

static void
ParseOpts(int argc, const char *argv[], string &oLearningChannel,
          string &oMatchingChannel, string &oPublishChannel,
          int32_t &oPoseBufferCapacity, int32_t &oImageBufferCapacity,
          double &oPositionThreshold, double &oAngleThreshold,
          string &oWriteModelsFilename, string &oReadModelsFilename,
          string &oMatlabResultsFilename)
{
    char *optstring = (char*)"hl:m:p:o:i:s:a:w:r:f:";
    char c;
    struct option long_opts[] = {
        {"help", no_argument, 0, 'h'},
        {"learning-channel", required_argument, 0, 'm'},
        {"matching-channel", required_argument, 0, 'c'},
        {"publish-channel", required_argument, 0, 'd'},
        {"pose-buffer-capacity", required_argument, 0, 'o'},
        {"image-buffer-capacity", required_argument, 0, 'i'},
        {"position-threshold", required_argument, 0, 's'},
        {"write-models-filename", required_argument, 0, 'w'},
        {"read-models-filename", optional_argument, 0, 'r'},
        {"matlab-results-filename", optional_argument, 0, 'f'},
        {0, 0, 0, 0}};

    while ((c = getopt_long (argc, (char **)argv, optstring, long_opts, 0)) >= 0)
    {
        switch (c) 
        {
            case 'l':
                oLearningChannel = string(optarg);
                break;
            case 'm':
                oMatchingChannel = string(optarg);
                break;
            case 'p':
                oPublishChannel = string(optarg);
                break;
            case 'o':
                oPoseBufferCapacity = atoi(optarg);
                break;
            case 'i':
                oImageBufferCapacity = atoi(optarg);
                break;
            case 's':
                oPositionThreshold = atof(optarg);
                break;
            case 'a':
                oAngleThreshold = atof(optarg);
                break;
            case 'w':
                oWriteModelsFilename = string(optarg);
                break;
            case 'r':
                oReadModelsFilename = string(optarg);
                break;
            case 'f':
                oMatlabResultsFilename = string(optarg);
                break;
            case 'h':
            default:
                Usage(argv[0]);
                exit(1);
        }
    }
}


static void
VerifyOpts(const char *app, string &iLearningChannel,
           string &iMatchingChannel, string &iPublishChannel,
           int32_t &iPoseBufferCapacity, int32_t &iImageBufferCapacity,
           double &iPositionThreshold, double &iAngleThreshold,
           string &iWriteModelsFilename, string &iReadModelsFilename)
{

    if (iLearningChannel == "") {
        cerr << "ERROR: Learning channel not specified." << endl;
        Usage(app);
        exit(1);
    }
    if (iMatchingChannel == "") {
        cerr << "ERROR: Matching channel not specified." << endl;
        Usage(app);
        exit(1);
    }
    if (iPublishChannel == "") {
        cerr << "ERROR: Publish channel not specified." << endl;
        Usage(app);
        exit(1);
    }
    if (iPoseBufferCapacity <= 0) {
        cerr << "ERROR: Pose buffer capacity not specified or invalid." << endl;
        Usage(app);
        exit(1);
    }
    if (iImageBufferCapacity <= 0) {
        cerr << "ERROR: Image buffer capacity not specified or invalid." << endl;
        Usage(app);
        exit(1);
    }
    if (iPositionThreshold < 0) {
        cerr << "ERROR: Position threshold not specified or invalid." << endl;
        Usage(app);
        exit(1);
    }
    if (iAngleThreshold < 0) {
        cerr << "ERROR: Angle threshold not specified or invalid." << endl;
        Usage(app);
        exit(1);
    }
    if (iWriteModelsFilename == "") {
        cerr << "ERROR: Output models filename not specified." << endl;
        Usage(app);
        exit(1);
    }
    if (iWriteModelsFilename == iReadModelsFilename) {
        cerr << "ERROR: Output and input models filenames must be different." << endl;
        Usage(app);
        exit(1);
    }
}
        

int main (const int argc, const char **argv)
{
    string learningChannel(""), matchingChannel(""), publishChannel("");
    string writeModelsFilename(""), readModelsFilename("");
    string matlabResultsFilename("");
    int32_t poseBufferCapacity = -1;
    int32_t imageBufferCapacity = -1;
    double deltaPositionThreshold = -1., deltaThetaDegreesThreshold = -1.;

    ParseOpts(argc, argv, learningChannel, matchingChannel, publishChannel,
              poseBufferCapacity, imageBufferCapacity,
              deltaPositionThreshold, deltaThetaDegreesThreshold,
              writeModelsFilename, readModelsFilename,
              matlabResultsFilename);

    VerifyOpts(argv[0], learningChannel, matchingChannel, publishChannel,
               poseBufferCapacity, imageBufferCapacity,
               deltaPositionThreshold, deltaThetaDegreesThreshold,
               writeModelsFilename, readModelsFilename);

    Reacquisition reacquisition(learningChannel, matchingChannel, publishChannel,
                                poseBufferCapacity, imageBufferCapacity,
                                deltaPositionThreshold, deltaThetaDegreesThreshold,
                                readModelsFilename, writeModelsFilename,
                                matlabResultsFilename);
    
    GMainLoop *mainloop;
    mainloop = g_main_loop_new (NULL, FALSE);
    g_main_loop_run (mainloop);

    return 0;
}
