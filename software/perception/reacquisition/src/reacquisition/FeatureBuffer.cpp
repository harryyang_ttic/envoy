#include "FeatureBuffer.hpp"

#include <iostream>
#include <limits>
#include <lcm/lcm.h>

#ifndef M_PI
#define M_PI 3.14159265358979
#endif

namespace reacq {
    
FeatureBuffer::FeatureBuffer(const std::string &iPoseChannel,
                             int32_t iPoseCapacity,                
                             const std::string &iFeatureChannel,
                             int32_t iFeatureCapacity,
                             const double iDeltaPositionThreshold,
                             const double iDeltaThetaDegreesThreshold) :
    mPoseChannel(iPoseChannel), mPoseCapacity(iPoseCapacity),
    mFeatureChannel(iFeatureChannel), mFeatureCapacity(iFeatureCapacity),
    mDeltaPositionThreshold(iDeltaPositionThreshold),
    mDeltaThetaDegreesThreshold(iDeltaThetaDegreesThreshold),
    mRunning(false), mOriginalRows(0), mOriginalCols(0),
    mRowScale(0.), mColScale(0.)
{
    if (!g_thread_supported ()) {
        g_thread_init (NULL);
    }
    mMutex = g_mutex_new ();
    mCondition = g_cond_new ();

    mLcm = bot_lcm_get_global (NULL);
    g_assert(mLcm);
    mFeatureSub = 
        sift_point_feature_list_t_subscribe (mLcm, mFeatureChannel.c_str(),
                                              FeatureBuffer::OnFeature, this);

    mPoseSub = 
        bot_core_pose_t_subscribe (mLcm, mPoseChannel.c_str(),
                                 FeatureBuffer::OnPose, this);

    std::cout << "Created FeatureBuffer: " << std::endl
              << " * pose channel = " << iPoseChannel << std::endl
              << " * pose capacity = " << iPoseCapacity << std::endl
              << " * feature channel = " << iFeatureChannel << std::endl
              << " * feature capacity = " << iFeatureCapacity << std::endl
              << " * position threshold = " << iDeltaPositionThreshold << std::endl
              << " * angle threshold = " << iDeltaThetaDegreesThreshold << std::endl;
}


FeatureBuffer::~FeatureBuffer()
{
    if (NULL != mFeatureSub) {
        sift_point_feature_list_t_unsubscribe(mLcm, mFeatureSub);
    }

    if (NULL != mPoseSub) {
        bot_core_pose_t_unsubscribe(mLcm, mPoseSub);
    }
    
    if (NULL != mCondition) {
        g_cond_free (mCondition);
    }

    if (NULL != mMutex) {
        g_mutex_free (mMutex);
    }

}

FeatureWithPose
FeatureBuffer::GetFeatureByIndex(int iIndex)
{
    Lock();
    if (iIndex >= mFeatures.size()) {
        std::cerr << "WARNING: Trying to access feature out of array bounds." << std::endl;
        FeatureWithPose dummy;
        dummy.feature = NULL;
        dummy.pose = NULL;
        Unlock();
        return dummy;
    }
    FeatureWithPose returnVal;
    returnVal.feature = sift_point_feature_list_t_copy(mFeatures[iIndex].feature);
    returnVal.pose = bot_core_pose_t_copy(mFeatures[iIndex].pose);
    Unlock();
    return returnVal;
}

FeatureWithPose
FeatureBuffer::GetFeatureByTimestamp(int64_t iTimestamp)
{
    Lock();
    FeatureWithPose returnVal;
    int64_t minDt = std::numeric_limits<int64_t>::max();
    int iFeature = GetNumFeatures() - 1;
    int iBestFeature = -1;
    bool keepGoing = true;
    while (keepGoing) {
        int64_t dt = abs(iTimestamp - mFeatures[iFeature].feature->utime);
        if (dt == 0) {
            iBestFeature = iFeature;
            keepGoing = false;
        } else {
            if (dt < minDt) {
                minDt = dt;
                iBestFeature = iFeature;
            }
            iFeature--;
            if (iFeature < 0) {
                keepGoing = false;
            }
        }
    }
    returnVal.feature = sift_point_feature_list_t_copy(mFeatures[iBestFeature].feature);
    returnVal.pose = bot_core_pose_t_copy(mFeatures[iBestFeature].pose);
    Unlock();
    return returnVal;
}
    
void
FeatureBuffer::Clear()
{
    std::deque<FeatureWithPose>::iterator iter;
    for(iter = mFeatures.begin(); iter != mFeatures.end(); ++iter) {
        sift_point_feature_list_t_destroy(iter->feature);
        iter->feature = NULL;
        bot_core_pose_t_destroy(iter->pose);
        iter->pose = NULL;
    }
    mFeatures.clear();
}

void
FeatureBuffer::Start()
{
    Lock();
    std::cout << "FeatureBuffer::Start()" << std::endl;
    mRunning = true;
    Unlock();
}

void
FeatureBuffer::Stop()
{
    Lock();
    std::cout << "FeatureBuffer::Stop()" << std::endl;
    mRunning = false;
    Unlock();
}

void
FeatureBuffer::GetAllTimestampsAndPoses(std::vector<int64_t> &oTimes,
                                        std::vector<bot_core_pose_t*> &oPoses)
{
    Lock();
    oTimes.resize(mFeatures.size());
    oPoses.resize(mFeatures.size());
    for (int i=0; i<mFeatures.size(); ++i) {
        oTimes[i] = mFeatures[i].feature->utime;
        oPoses[i] = bot_core_pose_t_copy(mFeatures[i].pose);
    }
    Unlock();
}
    
void
FeatureBuffer::OnFeature(const lcm_recv_buf_t *iRbuf, const char *iChannel, 
                         const sift_point_feature_list_t *iMsg, void *iUserData)
{
    FeatureBuffer *featureBuffer = static_cast<FeatureBuffer*>(iUserData);
    featureBuffer->Lock();
#ifdef BUFFER_DEBUG
    std::cout << "Starting OnFeature." << std::endl;
#endif
    if (!featureBuffer->IsRunning()) {
#ifdef BUFFER_DEBUG
        std::cout << "Not running." << std::endl;
#endif        
        featureBuffer->Unlock();
        return;
    }

    if (featureBuffer->FeaturesAreFull()) {
        featureBuffer->DropFirstFeatureWithPose();
#ifdef BUFFER_DEBUG
        std::cout << "OnFeature: Buffer (" << featureBuffer->GetNumFeatures()
                  << ") full. Dropped feature." << std::endl;
#endif
    }

    if (0 == featureBuffer->GetNumPoses()) {
        std::cerr << "WARNING: Could not add new feature because pose buffer is empty." << std::endl;
        featureBuffer->Unlock();
        return;
    }

    // Find closest pose times before and after input feature
    int64_t closestTimeBefore = LONG_MAX;
    bot_core_pose_t *closestPoseBefore = NULL;
    int64_t closestTimeAfter = LONG_MAX;
    bot_core_pose_t *closestPoseAfter = NULL;
    int64_t msgTime = iMsg->utime;
    bot_core_pose_t *bestPose = NULL;

    for (size_t i=0; i<featureBuffer->GetNumPoses(); ++i) {
        bot_core_pose_t *pose = featureBuffer->PeekAtPose(i);
        int64_t dt = pose->utime - msgTime;
        if (0 == dt) {
            bestPose = bot_core_pose_t_copy(pose);
#ifdef BUFFER_DEBUG
            std::cout << "-------> Found exact pose." << std::endl;
#endif
            break;
        }
        if ((dt < closestTimeAfter) && dt > 0) {
            closestTimeAfter = dt;
            closestPoseAfter = pose;
        }
        if ((dt < closestTimeBefore) && dt < 0) {
            closestTimeBefore = -dt;
            closestPoseBefore = pose;
        }
    }

    // Compute feature pose as weighted average of closest poses 
    if (NULL == bestPose) {
        if (NULL == closestPoseBefore) {
            bestPose = bot_core_pose_t_copy(closestPoseAfter);
        } else if (NULL == closestPoseAfter) {
            bestPose = bot_core_pose_t_copy(closestPoseBefore);
        } else {
            double dt = static_cast<double>(closestPoseAfter->utime -
                                            closestPoseBefore->utime);
            double scaleBefore = static_cast<double>(closestPoseAfter->utime - msgTime) / dt;
            double scaleAfter = static_cast<double>(msgTime - closestPoseBefore->utime) / dt;
            bestPose = (bot_core_pose_t *) calloc (1, sizeof(bot_core_pose_t));
            bestPose->utime = msgTime;
            for (int i=0; i<3; ++i) {
                bestPose->pos[i] = closestPoseBefore->pos[i]*scaleBefore +
                    closestPoseAfter->pos[i]*scaleAfter;
                bestPose->vel[i] = closestPoseBefore->vel[i]*scaleBefore +
                    closestPoseAfter->vel[i]*scaleAfter;
                bestPose->rotation_rate[i] = closestPoseBefore->rotation_rate[i]*scaleBefore +
                    closestPoseAfter->rotation_rate[i]*scaleAfter;
                bestPose->accel[i] = closestPoseBefore->accel[i]*scaleBefore +
                    closestPoseAfter->accel[i]*scaleAfter;
            }
#ifdef BUFFER_DEBUG
            std::cout << "dt = " << dt
                      << ", scaleBefore = " << scaleBefore
                      << ", scaleAfter = " << scaleAfter
                      << ", closestTimeBefore = " << closestTimeBefore
                      << ", closestTimeAfter = " << closestTimeAfter
                      << std::endl;
#endif
            for (int i=0; i<4; ++i) {
                bestPose->orientation[i] = closestPoseBefore->orientation[i]*scaleBefore +
                    closestPoseAfter->orientation[i]*scaleAfter;
            }
        }
    }

    // See if the closest pose is far enough to add the new feature
    bool tooCloseFound = false;
    double negativeInputRpy[4];
    for (int i=0; i<3; ++i) {
        negativeInputRpy[i] = bestPose->orientation[i];
    }
    negativeInputRpy[3] = -bestPose->orientation[3];
    double combinedOrientation[4];
    double combinedRpy[3];
//     bot_quat_to_roll_pitch_yaw(bestPose->orientation, inputRpy);
//     std::cout << "best pose rpy = " << inputRpy[0] << ", " << inputRpy[1] << ", " << inputRpy[2] << std::endl;
//     std::cout << "featureBuffer->GetNumFeatures() = " << featureBuffer->GetNumFeatures() << std::endl;

    int i=0;
    while (!tooCloseFound && (i < featureBuffer->GetNumFeatures())) {
        FeatureWithPose feature = featureBuffer->PeekAtFeature(i);
        i++;
        double dx = bestPose->pos[0] - feature.pose->pos[0];
        double dy = bestPose->pos[1] - feature.pose->pos[1];
        // Planar assumption, so don't use z
//         double dz = bestPose->pos[2] - feature.pose->pos[2];
//         bot_quat_to_roll_pitch_yaw(feature.pose->orientation, testRpy);
//         std::cout << "Debug pose change dist: "
//              << (dx*dx + dy*dy + dz*dz) << "/" << featureBuffer->GetDeltaPositionThreshold()
//              << ", angle: " << fabs(testRpy - inputRpy)
//              << "/" << featureBuffer->GetDeltaThetaDegreesThreshold() << std::endl;
        if ((dx*dx + dy*dy) < featureBuffer->GetDeltaPositionThreshold()) {
#ifdef BUFFER_DEBUG
            std::cout << "Found distance too close: threshold = "
                      << featureBuffer->GetDeltaPositionThreshold() << ", distance = "
                      << (dx*dx + dy*dy)
                      << ", i = " << i <<  std::endl;
#endif
            bot_quat_mult(combinedOrientation, negativeInputRpy, feature.pose->orientation);
            bot_quat_to_roll_pitch_yaw(combinedOrientation, combinedRpy);
            if (fabs(combinedRpy[2]) <
                (featureBuffer->GetDeltaThetaDegreesThreshold() * M_PI / 180.)) {
#ifdef BUFFER_DEBUG
                std::cout << "and the angle is close enough: threshold = "
                          << featureBuffer->GetDeltaThetaDegreesThreshold()
                          << ", angle = "
                          << fabs(combinedRpy[2]) * 180. / M_PI
                          << ", i = " << i <<  std::endl;
#endif
                tooCloseFound = true;
            } else {
#ifdef BUFFER_DEBUG
                std::cout << ".......but the angle is far enough: threshold = "
                          << featureBuffer->GetDeltaThetaDegreesThreshold()
                          << ", angle = "
                          << fabs(combinedRpy[2]) * 180. / M_PI
                          << ", i = " << i <<  std::endl;
#endif
            }
        }
    }

    // Add new pose (and it becomes the class' responsibility), or free new pose
    if (!tooCloseFound) {
#ifdef BUFFER_DEBUG
        std::cout << "Added new feature with pose." << std::endl;
#endif
        bestPose->utime = msgTime;
        featureBuffer->AddNewFeatureWithPose(sift_point_feature_list_t_copy(iMsg), bestPose);
    } else {
#ifdef BUFFER_DEBUG
        std::cout << "Far enough not found." << std::endl;
#endif
        free(bestPose);
    }
    featureBuffer->Unlock();
}

void
FeatureBuffer::OnPose(const lcm_recv_buf_t *iRbuf, const char *iChannel, 
                    const bot_core_pose_t *iMsg, void *iUserData)
{
    FeatureBuffer *featureBuffer = static_cast<FeatureBuffer*>(iUserData);
    featureBuffer->Lock();
    if (!featureBuffer->IsRunning()) {
        featureBuffer->Unlock();
        return;
    }

    if (featureBuffer->PosesAreFull()) {
        featureBuffer->DropFirstPose();
    }

    featureBuffer->AddNewPose(bot_core_pose_t_copy(iMsg));
    featureBuffer->Unlock();
}

bot_core_pose_t *
FeatureBuffer::PeekAtPose(int iIndex)
{
    if (iIndex >= mPoses.size()) {
        return NULL;
    }
    return mPoses[iIndex];
}
        
void
FeatureBuffer::DropFirstPose()
{
    if (!mPoses.empty()) {
        bot_core_pose_t_destroy(mPoses.front());
        mPoses.pop_front();
    }
}


void
FeatureBuffer::AddNewPose(bot_core_pose_t *iPose)
{
    mPoses.push_back(iPose);
//     std::cout << "Added pose [" << mPoses.size() << "]" << std::endl;
}

void
FeatureBuffer::DropFirstFeatureWithPose()
{
    if (!mFeatures.empty()) {
        sift_point_feature_list_t_destroy(mFeatures.front().feature);
        bot_core_pose_t_destroy(mFeatures.front().pose);
        mFeatures.pop_front();
    }
}

void
FeatureBuffer::AddNewFeatureWithPose(sift_point_feature_list_t *iFeature,
                                     bot_core_pose_t *iPose)
{
    FeatureWithPose featureWithPose;
    featureWithPose.feature = iFeature;
    if (mOriginalRows && mOriginalCols) {
        if (fabs(mRowScale) < 1.e-8) {
            mRowScale = static_cast<double>(mOriginalRows) /
                featureWithPose.feature->height;
            mColScale = static_cast<double>(mOriginalCols) /
                featureWithPose.feature->width;
        }
        for (int i=0; i<featureWithPose.feature->num; ++i) {
            featureWithPose.feature->pts[i].row *= mRowScale;
            featureWithPose.feature->pts[i].col *= mColScale;
        }
    }
    featureWithPose.pose = iPose;
    mFeatures.push_back(featureWithPose);
    std::cout << "Added feature [" << featureWithPose.feature->utime << "], size = ["
              << mFeatures.size() << "] on channel ["
              << mFeatureChannel << "]" << std::endl;
}

FeatureWithPose
FeatureBuffer::PeekAtFeature(int iIndex)
{
    if (iIndex >= mFeatures.size()) {
        FeatureWithPose dummy;
        dummy.feature = NULL;
        dummy.pose = NULL;
        return dummy;
    } else {
        return mFeatures[iIndex];
    }
}
        
} //namespace reacq
