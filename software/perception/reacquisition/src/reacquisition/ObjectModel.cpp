#include "ObjectModel.hpp"

using namespace reacq;


ObjectModel::ObjectModel() : mpObject(NULL)
{
}

ObjectModel::~ObjectModel()
{
    if (NULL != mpObject) {
          om_object_t_destroy(mpObject);
    }

}
