#ifndef _AGILE_REACQ_SMARTPOINTER_H_
#define _AGILE_REACQ_SMARTPOINTER_H_

#include <glib.h>

namespace reacq {

template<typename T>
class SmartPointer {
public:
    SmartPointer() {
        mData = NULL;
        mRefCount = NULL;
        mIsArray = false;
        mMutex = NULL;
    }

    SmartPointer(const T* iData, const bool iIsArray=false) {
        mData = (T*)iData;
        mRefCount = NULL;
        mIsArray = iIsArray;
        if (!g_thread_supported ()) {
            g_thread_init (NULL);
        }
        mMutex = g_mutex_new();
        if (NULL != iData) {
            incrementRefCount();
        }
    }

    SmartPointer(const SmartPointer<T>& iPtr) {
        mData = NULL;
        mRefCount = NULL;
        mMutex = NULL;
        *this = iPtr;
    }

    ~SmartPointer() {
        decrementRefCount();
    }

    T* get() const { return mData; }
    T* get()       { return mData; }

    T* operator->() { return mData; }
    T* operator->() const { return mData; }

    bool operator==(const SmartPointer<T>& iPtr) const {
        return mData == iPtr.mData;
    }
    bool operator==(const T* iData) const {
        return mData == iData;
    }
    bool operator!=(const SmartPointer<T>& iPtr) const {
        return mData != iPtr.mData;
    }
    bool operator!=(const T* iData) const {
        return mData != iData;
    }

    T& operator[](const int iIndex) const {
        return mData[iIndex];
    }
    T& operator[](const int iIndex) {
        return mData[iIndex];
    }

    SmartPointer<T>& operator=(const SmartPointer<T>& iPtr) {
        if (iPtr.mData != mData) {
            decrementRefCount();
            mIsArray = iPtr.mIsArray;
            mData = iPtr.mData;
            mRefCount = iPtr.mRefCount;
            mMutex = iPtr.mMutex;
            incrementRefCount();
        }
        return *this;
    }

    SmartPointer<T>& operator=(const T* iData) {
        return (*this = SmartPointer<T>(iData));
    }

private:
    void incrementRefCount() {
        g_mutex_lock(mMutex);
        if (NULL == mRefCount) {
            mRefCount = new int(0);
        }
        ++(*mRefCount);
        g_mutex_unlock(mMutex);
    }

    void decrementRefCount() {
        if (NULL == mMutex) {
            return;
        }
        g_mutex_lock(mMutex);
        if ((NULL == mRefCount) || (0 == *mRefCount)) {
            return;
        }
        --(*mRefCount);
        if (0 == *mRefCount) {
            delete mRefCount;
            mRefCount = NULL;
            if (mIsArray) {
                delete [] mData;
            }
            else {
                delete mData;
            }
            mData = NULL;
            g_mutex_unlock(mMutex);
            g_mutex_free(mMutex);
            mMutex = NULL;
        }
        else {
            g_mutex_unlock(mMutex);
        }
    }


private:
    T* mData;
    int* mRefCount;
    bool mIsArray;
    GMutex* mMutex;
};

}

#endif
