#include "BundleIntersection.hpp"

using namespace std;
using namespace reacq;

BundleIntersection::Problem::Problem(Measurements &iMeasurements,
                                     Poses &iPoses,
                                     LensModel *iLensModel) :
    mMeasurements(iMeasurements),
    mPoses(iPoses),
    mLensModel(iLensModel) {
    }


MatrixDynamic
BundleIntersection::Problem::computeErrors(const MatrixDynamic& iParams) const {
    Vector3 point3d;
    point3d(0) = iParams(0);
    point3d(1) = iParams(1);
    point3d(2) = iParams(2);

    MatrixDynamic errors(2*mMeasurements.size());
    for (int i = 0; i < mPoses.size(); ++i) {
        // transform to camera coords
        Vector3 pt = mPoses[i]->mOrientation.getTranspose() *
            (point3d - mPoses[i]->mPosition);

        // project onto camera
        double pix[2];
        if (!mLensModel->rayToPixel(pt, pix[0], pix[1])) {
            pix[0] = pix[1] = 1.e10;
        }
        
        // compute difference
        errors(2*i) = pix[0] - mMeasurements[i]->mImagePoint(0);
        errors(2*i+1) = pix[1] - mMeasurements[i]->mImagePoint(1);
    }

    return errors;
}

BundleIntersection::BundleIntersection() {
}

RobustLM::Result BundleIntersection::solve(Measurements &iMeasurements,
                                           ModelPoint &ioModelPoint,
                                           Poses& iPoses,
                                           LensModel *iLensModel) {

    // push initial pose parameters into vector
    MatrixDynamic initParameters(3);
    initParameters(0) = ioModelPoint.mPoint(0);
    initParameters(1) = ioModelPoint.mPoint(1);
    initParameters(2) = ioModelPoint.mPoint(2);

    // solve problem
    Problem problem(iMeasurements, iPoses, iLensModel);
    problem.setParameters(initParameters);
//     TukeyWeighting weighting;
//     weighting.setMinSigma(0.5);
    NoWeighting weighting;
    RobustLM lm;
    lm.setVerbose(false);
    RobustLM::Result result = lm.solve(problem, weighting);

    // grab parameters from vector
    ioModelPoint.mPoint(0) = result.mParameters(0);
    ioModelPoint.mPoint(1) = result.mParameters(1);
    ioModelPoint.mPoint(2) = result.mParameters(2);

    return result;
}
