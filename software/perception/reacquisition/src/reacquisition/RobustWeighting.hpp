#ifndef _AGILE_REACQ_ROBUSTWEIGHTING_H_
#define _AGILE_REACQ_ROBUSTWEIGHTING_H_

#include "MatrixDynamic.hpp"

namespace reacq {

class RobustWeighting {
public:
    virtual double computeCost(MatrixDynamic& iErrors) const = 0;
    virtual MatrixDynamic computeWeights(MatrixDynamic& iErrors) const = 0;
    virtual void computeWeightParams(MatrixDynamic& iErrors) = 0;
};

class TukeyWeighting : public RobustWeighting {
private:
    double mSigma2;
    double mMinSigma;
    
public:
    TukeyWeighting();
    double computeCost(MatrixDynamic& iErrors) const;
    MatrixDynamic computeWeights(MatrixDynamic& iErrors) const;
    void computeWeightParams(MatrixDynamic& iErrors);

    void setMinSigma(const double iMin) {
        mMinSigma = iMin;
    }
};

class NoWeighting : public RobustWeighting {
public:
    double computeCost(MatrixDynamic& iErrors) const;
    MatrixDynamic computeWeights(MatrixDynamic& iErrors) const;
    void computeWeightParams(MatrixDynamic& iErrors);
};

}

#endif // _AGILE_REACQ_ROBUSTWEIGHTING_H_
