#include "PointGridder.hpp"

#include <algorithm>

using namespace reacq;
using namespace std;

bool PointGridder::binPoints(const vector<Vector2>& iPoints,
                             const vector<double>& iScores,
                             vector<int>& oIndices) {
    if (iPoints.size() != iScores.size()) {
        cerr << "PointGridder: Points and scores must be same size" << endl;
        return false;
    }

    // Put all points into correct bins
    mBins.clear();
    mBins.resize(mNumBinsX*mNumBinsY);
    for (int i = 0; i < iPoints.size(); ++i) {
        int binX = floor(iPoints[i](0)/mImageWidth*mNumBinsX);
        int binY = floor(iPoints[i](1)/mImageHeight*mNumBinsY);
        if ((binX > mNumBinsX) || (binY > mNumBinsY) ||
            (binX < 0) || (binY < 0)) {
            cerr << "PointGridder: point (" << iPoints[i](0) << "," <<
                iPoints[i](1) << ") outside image size (" << mImageWidth <<
                "," << mImageHeight << ")" << endl;
            return false;
        }

        PointStruct pt;
        pt.mPoint = iPoints[i];
        pt.mScore = iScores[i];
        pt.mIndex = i;

        int linearBin = binY*mNumBinsX + binX;
        mBins[linearBin].push_back(pt);
    }

    // Sort bins
    if (mDescending) {
        for (int i = 0; i < mBins.size(); ++i) {
            std::sort(mBins[i].begin(), mBins[i].end(), compareDescending);
        }
    }
    else {
        for (int i = 0; i < mBins.size(); ++i) {
            std::sort(mBins[i].begin(), mBins[i].end(), compareAscending);
        }
    }
    
    // Grab indices round-robin style
    oIndices.clear();
    while (oIndices.size() < iPoints.size()) {
        for (int i = 0; i < mBins.size(); ++i) {
            if (mBins[i].size() > 0) {
                PointStruct pt = mBins[i].front();
                oIndices.push_back(pt.mIndex);
                mBins[i].pop_front();
            }
        }
    }

    return true;
}
