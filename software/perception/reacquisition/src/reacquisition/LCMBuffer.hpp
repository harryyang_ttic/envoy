#ifndef _AGILE_REACQ_LCMBUFFER_H_
#define _AGILE_REACQ_LCMBUFFER_H_

#include "LCMData.hpp"

#include <set>
#include <string>

namespace reacq {

template<typename T>
class LCMBuffer {

public:
    class Listener {
    public:
        virtual void notify(LCMData<T>& iData) = 0;
    };

public:
    LCMBuffer() {
        mSubscription = NULL;
        setMaximumLength(100);
        mLCM = bot_lcm_get_global (NULL);

        if (!g_thread_supported()) {
            g_thread_init(NULL);
        }
        mMutex = g_mutex_new();
    }

    ~LCMBuffer() {
        stop();
        g_mutex_free(mMutex);
    }

    void setChannel(const std::string& iChannel) {
        bool receiving = isReceiving();
        if (receiving) {
            stop();
        }
        mChannel = iChannel;
        if (receiving) {
            start();
        }
    }

    std::string& getChannel() const {
        return mChannel;
    }

    void setMaximumLength(const int iLen) {
        mMaximumLength = iLen;
    }

    void lock() {
        g_mutex_lock(mMutex);
    }

    void unlock() {
        g_mutex_unlock(mMutex);
    }

    int getBufferSize() const {
        return mDataList.size();
    }

    LCMData<T>& operator()(const int iPos) {
        return mDataList[iPos];
    }

    LCMData<T>& operator()(const int iPos) const {
        return mDataList[iPos];
    }

    bool isReceiving() const {
        return (mSubscription != NULL);
    }

    void start() {
        if (NULL == mSubscription) {
            mSubscription =
                LCMData<T>::subscribe(mLCM, mChannel.c_str(), onData, this);
        }
    }

    void stop() {
        if (NULL != mSubscription) {
            LCMData<T>::unsubscribe(mLCM, mSubscription);
        }
        mSubscription = NULL;
    }

    void registerListener(Listener* iListener) {
        lock();
        mListeners.insert(iListener);
        unlock();
    }

    void unregisterListener(Listener* iListener) {
        lock();
        mListeners.erase(iListener);
        unlock();
    }

    
private:
    static void onData(const lcm_recv_buf_t *iRbuf, const char *iChannel, 
                       const T* iMsg, void *iUserData) {
        LCMBuffer<T>* self = (LCMBuffer<T>*)iUserData;
        self->lock();
        if (self->mDataList.size() >= self->mMaximumLength) {
            self->mDataList.pop_front();
        }
        self->mDataList.push_back(LCMData<T>());
        self->mDataList.back() = iMsg;
        for (typename std::set<Listener*>::const_iterator iter =
                 self->mListeners.begin();
             iter != self->mListeners.end(); ++iter) {
            (*iter)->notify(self->mDataList.back());
        }
        self->unlock();
    }

private:
    lcm_t *mLCM;
    std::deque< LCMData<T> > mDataList;
    std::string mChannel;
    int mMaximumLength;
    typename LCMData<T>::Subscription* mSubscription;

    GMutex* mMutex;
    std::set<Listener*> mListeners;
};

}

#endif // _AGILE_REACQ_LCMBUFFER_H_
