#ifndef _reacq_Quaternion_h_
#define _reacq_Quaternion_h_

#include "GeomTypes.hpp"

#include <cmath>

namespace reacq {

class Quaternion {
public:
  Quaternion() {
    setValue(0,0,0,1);
  }

  Quaternion(const double iX, const double iY, const double iZ, const double iW) {
    setValue(iX, iY, iZ, iW);
  }

  Quaternion(const Quaternion& iQuat) {
    *this = iQuat;
  }

  void setValue(const double iX, const double iY, const double iZ, const double iW) {
    mX = iX;  mY = iY;  mZ = iZ;  mW = iW;
    normalize();
    mNeedUpdate = true;
  }

  void setValue(const Vector3& iAxis, const double iAngle);

  bool setValue(const Matrix33 iRotation);

  void setValue(const double iRoll, const double iPitch, const double iYaw);

  Quaternion& operator=(const Quaternion& iQuat) {
    setValue(iQuat.mX, iQuat.mY, iQuat.mZ, iQuat.mW);
    return *this;
  }

  double getX() const { return mX; }
  double getY() const { return mY; }
  double getZ() const { return mZ; }
  double getW() const { return mW; }

  void getValue(double& oX, double& oY, double& oZ, double& oW) const {
    oX = mX;  oY = mY;  oZ = mZ;  oW = mW;
  }

  void getValue(Vector3& oAxis, double& oAngle) const;

  void getValue(double& oRoll, double& oPitch, double& oYaw) const;

  Quaternion getInverse() const {
    return Quaternion(mX, mY, mZ, -mW);
  }
  void invert() {
    mW = -mW;
    mNeedUpdate = true;
  }

  Matrix33 getRotationMatrix() const;

  Quaternion operator*(const Quaternion& iQuat) const;

  void operator*=(const Quaternion& iQuat) {
    *this = *this * iQuat;
  }

  Vector3 operator*(const Vector3& iVect) {
    if (mNeedUpdate) {
      mMatrix = getRotationMatrix();
      mNeedUpdate = false;
    }
    return mMatrix*iVect;
  }

private:
  void normalize() {
    double norm = std::sqrt(mX*mX + mY*mY + mZ*mZ + mW*mW);
    if (norm > 0) {
      norm = 1/norm;
      mX *= norm;  mY *= norm;  mZ *= norm;  mW *= norm;
    }
  }

private:
  double mW;
  double mX;
  double mY;
  double mZ;

  bool mNeedUpdate;
  Matrix33 mMatrix;
};

}

#endif
