#ifndef _AGILE_REACQ_POSE_H_
#define _AGILE_REACQ_POSE_H_

#include "GeomTypes.hpp"
#include <vector>

namespace reacq {

class Pose {

  public:

    Matrix33 mOrientation;
    Vector3 mPosition;
    MatrixStatic<6,6> mCovariance;
    
}; //class Pose

typedef std::vector<Pose*> Poses;
    
} //namespace reacq


#endif // _AGILE_REACQ_POSE_H_
