#ifndef _AGILE_REACQ_BUNDLEINTERSECTION_H_
#define _AGILE_REACQ_BUNDLEINTERSECTION_H_

#include "RobustLM.hpp"
#include "Measurement.hpp"
#include "Pose.hpp"
#include "ModelPoint.hpp"
#include "LensModel.hpp"

namespace reacq {

class BundleIntersection {
public:
    BundleIntersection();

    RobustLM::Result solve(Measurements &iMeasurements,
                           ModelPoint &iModelPoint,
                           Poses& iPoses,
                           LensModel *iLensModel);


private:
    class Problem : public RobustLM::Problem {
    public:
        Problem(Measurements &iMeasurements,
                Poses &iPoses,
                LensModel *iLensModel);
        
        MatrixDynamic computeErrors(const MatrixDynamic& iParams) const;

    private:
        Measurements mMeasurements;
        Poses mPoses;
        LensModel *mLensModel;
    };

};

}

#endif //_AGILE_REACQ_BUNDLEINTERSECTION_H_
