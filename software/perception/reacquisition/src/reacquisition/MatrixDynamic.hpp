#ifndef _reacq_MatrixDynamic_h_
#define _reacq_MatrixDynamic_h_

#include "SmartPointer.hpp"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_permutation.h>
#include <cstring>
#include <cmath>
#include <iostream>

namespace reacq {

class MatrixDynamic {


  struct View {
  private:
    View(const int iRows, const int iCols,
         const double* iData, const int iStride) {
      mRows = iRows;
      mCols = iCols;
      mData = (double*)iData;
      mStride = iStride;
    }

  public:
    View& operator=(const View& iView) {
      mRows = iView.mRows;
      mCols = iView.mCols;
      double* outPtr = mData;
      const double* inPtr = iView.mData;
      for (int i = 0; i < mRows; ++i, outPtr+=mStride, inPtr+=iView.mStride)
        memcpy(outPtr, inPtr, mCols*sizeof(double));
      return *this;
    }

    View& operator=(const MatrixDynamic& iMat) {
      double* outPtr = mData;
      const double* inPtr = iMat.mData.get();
      for (int i = 0; i < mRows; ++i, outPtr+=mStride, inPtr+=mCols)
        memcpy(outPtr, inPtr, mCols*sizeof(double));
      return *this;
    }

  private:
    friend class MatrixDynamic;

  private:
    int mRows;
    int mCols;
    int mStride;
    double* mData;
  };


public:
  MatrixDynamic() {
    mRows = mCols = -1;
    resize(1, 1);
  }

  MatrixDynamic(const int iRows, const int iCols=1) {
    mRows = mCols = -1;
    resize(iRows, iCols);
  }

  MatrixDynamic(const MatrixDynamic& iMat) {
    mRows = mCols = -1;
    resize(iMat.mRows, iMat.mCols);
    memcpy(mData.get(), iMat.mData.get(), mRows*mCols*sizeof(double));
  }

  MatrixDynamic& operator=(const MatrixDynamic& iMat) {
    if (this != &iMat) {
      resize(iMat.mRows, iMat.mCols);
      memcpy(mData.get(), iMat.mData.get(), mRows*mCols*sizeof(double));
    }
    return *this;
  }

  MatrixDynamic& operator =(const View& iView) {
    getView(mRows, mCols) = iView;
    return *this;
  }

  MatrixDynamic operator-() const {
    MatrixDynamic ret(mRows, mCols);
    for (int i = 0; i < mLength; ++i)
      ret.mData[i] = -mData[i];
    return ret;
  }

  MatrixDynamic operator+(const MatrixDynamic& iMat) const {
    MatrixDynamic ret(mRows, mCols);
    for (int i = 0; i < mLength; ++i)
      ret.mData[i] = mData[i] + iMat.mData[i];
    return ret;
  }

  void operator+=(const MatrixDynamic& iMat) {
    *this = *this + iMat;
  }

  MatrixDynamic operator-(const MatrixDynamic& iMat) const {
    MatrixDynamic ret(mRows, mCols);
    for (int i = 0; i < mLength; ++i)
      ret.mData[i] = mData[i] - iMat.mData[i];
    return ret;
  }

  void operator-=(const MatrixDynamic& iMat) {
    *this = *this - iMat;
  }

  MatrixDynamic operator*(const MatrixDynamic& iMat) const {
    MatrixDynamic ret(mRows, iMat.mCols);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1,
                   &mView.matrix, &iMat.mView.matrix,
                   0, &ret.mView.matrix);
    return ret;
  }

  void operator*=(const MatrixDynamic& iMat) {
    *this = *this * iMat;
  }

  MatrixDynamic operator*(const double iVal) const {
    MatrixDynamic ret(mRows, mCols);
    for (int i = 0; i < mLength; ++i)
      ret.mData[i] = mData[i]*iVal;
    return ret;
  }

  void operator*=(const double iVal) {
    *this = *this * iVal;
  }

  MatrixDynamic operator/(const double iVal) const {
    return (*this * (1/iVal));
  }

  void operator/=(const double iVal) {
    *this *= (1/iVal);
  }

  double& operator()(const int iRow, const int iCol=0) {
    return mData[mCols*iRow + iCol];
  }

  double operator()(const int iRow, const int iCol=0) const {
    return mData[mCols*iRow + iCol];
  }

  int getNumRows() const {
      return mRows;
  }
  int getNumCols() const {
      return mCols;
  }
  int getNumElements() const {
      return mCols*mRows;
  }
  
  double getMagnitude() const {
    double mag = 0;
    for (int i = 0; i < mLength; ++i)
      mag += mData[i]*mData[i];
    return (double)sqrt(mag);
  }

  MatrixDynamic getTranspose() const {
    MatrixDynamic ret(mCols, mRows);
    gsl_matrix_transpose_memcpy(&ret.mView.matrix, &mView.matrix);
    return ret;
  }

  void zero() {
    bzero(mData.get(), mRows*mCols*sizeof(double));
  }

  View getView(const int iRows, const int iCols,
               const int iStartRow=0, const int iStartCol=0) const {
      return View(iRows, iCols, mData.get() + iStartRow*mCols + iStartCol,
                  mCols);
  }

  bool makeIdentity() {
    if (mRows != mCols)
      return false;
    zero();
    for (int i = 0; i < mRows; ++i)
      (*this)(i,i) = 1;
    return true;
  }

  MatrixDynamic getInverse() const {
    gsl_permutation perm;
    size_t permData[mRows];
    perm.size = mRows;
    perm.data = permData;

    MatrixDynamic tmp(*this);
    int s;
    gsl_linalg_LU_decomp(&tmp.mView.matrix, &perm, &s);

    MatrixDynamic inv(mRows, mRows);
    gsl_linalg_LU_invert(&tmp.mView.matrix, &perm, &inv.mView.matrix);
    return inv;
  }

  void invert() {
    *this = getInverse();
  }

  MatrixDynamic getPseudoInverse() const {
    MatrixDynamic u, s, v;
    svd(u,s,v);
    for (int i = 0; i < s.getNumRows(); ++i) {
      s(i,i) = (s(i,i)<1e-12) ? 0 : 1/s(i,i);
    }
    return (u*s*v.getTranspose());
  }

  bool eig(MatrixDynamic& oVects, MatrixDynamic& oVals) const {
    if (mRows != mCols)
      return false;

    oVects.resize(mRows, mCols);
    oVals.resize(mRows, 1);

    MatrixDynamic temp(*this);
    gsl_vector_view vectView = gsl_vector_view_array(oVals.mData.get(), mRows);
    gsl_eigen_symmv_workspace* workspace = gsl_eigen_symmv_alloc(mRows);
    gsl_eigen_symmv(&temp.mView.matrix, &vectView.vector,
                    &oVects.mView.matrix, workspace);
    gsl_eigen_symmv_free(workspace);
    gsl_eigen_symmv_sort(&vectView.vector, &oVects.mView.matrix,
                         GSL_EIGEN_SORT_ABS_ASC);
    return true;
  }

  bool svd(MatrixDynamic& oU, MatrixDynamic& oS, MatrixDynamic& oV) const {
    oU = *this;
    oS.resize(mCols,mCols);
    oV.resize(mCols,mCols);
    MatrixDynamic work(mCols,1);
    MatrixDynamic s(mCols,1);
    gsl_vector_view workView = gsl_vector_view_array(work.mData.get(), mCols);
    gsl_vector_view sView = gsl_vector_view_array(s.mData.get(), mCols);

    gsl_linalg_SV_decomp(&oU.mView.matrix, &oV.mView.matrix,
                         &sView.vector, &workView.vector);
    oS.zero();
    for (int i = 0; i < mCols; ++i)
      oS(i,i) = s(i,0);
 
    return true;
  }

  MatrixDynamic solve(const MatrixDynamic &iB) const {
    gsl_permutation perm;
    size_t permData[mRows];
    perm.size = mRows;
    perm.data = permData;
    
    MatrixDynamic tmp(*this);
    int s;
    gsl_linalg_LU_decomp(&tmp.mView.matrix, &perm, &s);

    double bData[mRows];
    memcpy(bData, iB.mData.get(), mRows*sizeof(double));
    
    gsl_vector bVec;
    bVec.size = mRows;
    bVec.stride = 1;
    bVec.data = bData;
    bVec.block = NULL;
    bVec.owner = 0;

    double xData[mRows];
    gsl_vector xVec;
    xVec.size = mRows;
    xVec.stride = 1;
    xVec.data = xData;
    xVec.block = NULL;
    xVec.owner = 0;
    
    gsl_linalg_LU_solve(&tmp.mView.matrix, &perm, &bVec, &xVec);
    MatrixDynamic solution(mRows, 1);
    memcpy(solution.mData.get(), xVec.data, mRows*sizeof(double));
    return solution;
  }

  friend std::ostream& operator<<(std::ostream& oStr, const MatrixDynamic& iMat)
  {
      for (int i=0; i<iMat.mRows; ++i) {
          for (int j=0; j<iMat.mCols; ++j) {
              oStr << iMat(i, j) << " ";
          }
          oStr << std::endl;
      }
      return oStr;
  }

private:
  void resize(const int iRows, const int iCols) {
    if ((mRows < 0) || (mCols < 0) || (iRows*iCols != mRows*mCols)) {
      mData = new double[iRows*iCols];
    }
    mRows = iRows;
    mCols = iCols;
    mView = gsl_matrix_view_array(mData.get(), mRows, mCols);
    mLength = mRows*mCols;
  }



private:
  int mRows;
  int mCols;
  SmartPointer<double> mData;
  int mLength;
  gsl_matrix_view mView;
};

}

#endif
