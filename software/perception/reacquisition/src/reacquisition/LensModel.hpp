#ifndef _AGILE_REACQ_LENSMODEL_H_
#define _AGILE_REACQ_LENSMODEL_H_

#include <string>

#include <bot_core/bot_core.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>

#include "GeomTypes.hpp"
#include "MatrixDynamic.hpp"

namespace reacq {

class LensModel {
public:
    LensModel();
    virtual ~LensModel();

    void setName(const std::string& iName);

    void setImageSize(const double iWidth, const double iHeight);
    void setPinholeParameters(const double iF, const double iCx,
                              const double iCy);
    void setPinholeParameters(const double iFx, const double iFy,
                              const double iCx, const double iCy,
                              const double iSkew=0);

    double getWidth() const { return mImageWidth; }
    double getHeight() const { return mImageHeight; }

    Matrix33 getPinholeMatrix() const { return mK; }
    double getFocalX() const { return mK(0,0); }
    double getFocalY() const { return mK(1,1); }
    double getCenterX() const { return mK(0,2); }
    double getCenterY() const { return mK(1,2); }
    double getSkew() const { return mK(0,1); }

    std::string getName() const { return mName; }

    bool setFromParam(BotParam* iParam, const char *camera_name);


    bool pixelToRay(const Vector2& iPixel, Vector3& oRay) const;
    bool pixelToRay(const double iX, const double iY, Vector3& oRay) const;
    bool pixelToRay(const double iX, const double iY,
                    double& oX, double& oY, double& oZ) const;

    bool rayToPixel(const Vector3& iRay, Vector2& oPixel) const;
    bool rayToPixel(const Vector3& iRay, Vector3& oPixel) const;
    bool rayToPixel(const Vector3& iRay, double& oX, double& oY) const;
    bool rayToPixel(const Vector3& iRay, double& oX, double& oY,
                    double& oZ) const;
    bool rayToPixel(const double iX, const double iY, const double iZ,
                    double& oX, double& oY, double& oZ) const;

    double distortAngle(const double iAngle) const;
    double undistortAngle(const double iAngle) const;

private:
    class SplineWarp {
    public:
        SplineWarp() {
            mNumSplines = 0;
            mScaleFactor = 1;
        }
        SplineWarp(const SplineWarp& iWarp) {
            *this = iWarp;
        }
        SplineWarp(const MatrixDynamic& iCoeffs, const double iScaleFactor);
        double apply(const double iX) const;

    private:
        int mNumSplines;
        double mScaleFactor;
        MatrixDynamic mCoeffs;
    };
    

private:
    BotCamTrans *CamTrans;
    std::string mName;
    double mImageWidth;
    double mImageHeight;
    Matrix33 mK;
    Matrix33 mInvK;
    SplineWarp mDistortWarp;
    SplineWarp mUndistortWarp;
};

}

#endif
