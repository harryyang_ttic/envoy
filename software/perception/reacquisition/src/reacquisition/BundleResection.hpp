#ifndef _AGILE_REACQ_BUNDLERESECTION_H_
#define _AGILE_REACQ_BUNDLERESECTION_H_

#include "RobustLM.hpp"
#include "Measurement.hpp"
#include "Pose.hpp"
#include "ModelPoint.hpp"
#include "LensModel.hpp"

namespace reacq {

class BundleResection {
public:
    BundleResection();

    RobustLM::Result solve(Measurements &iMeasurements,
                           ModelPoints &iModelPoints,
                           Pose& iPose,
                           LensModel *iLensModel);

private:
    static void poseFromVector(const MatrixDynamic& iParameters, Pose& oPose);


private:
    class Problem : public RobustLM::Problem {
    public:
        Problem(Measurements &iMeasurements,
                ModelPoints &iModelPoints,
                LensModel *iLensModel);
        
        MatrixDynamic computeErrors(const MatrixDynamic& iParams) const;

    private:
        Measurements mMeasurements;
        ModelPoints mModelPoints;
        LensModel *mLensModel;
    };

};

}

#endif //_AGILE_REACQ_BUNDLERESECTION_H_
