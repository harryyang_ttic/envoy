#include "HomographyMatcher.hpp"
#include <math.h>

using namespace reacq;

struct _homography_ransac_t {

};

HomographyMatcher::HomographyMatcher(int iDebug,
                                     int iNumIter,
                                     double iDistThreshPix)

{
    mDebug = iDebug;
    mNumIter = iNumIter;
    mDistThreshPix = iDistThreshPix;
    mRand = g_rand_new();
    mA = gsl_matrix_alloc(12, 9);
    mV = gsl_matrix_alloc(9, 9);
    mS = gsl_vector_alloc(9);
    mWork = gsl_vector_alloc(9);
    mH = gsl_matrix_alloc(3,3);
}

HomographyMatcher::~HomographyMatcher()
{
    if (NULL != mRand) {
        g_rand_free (mRand);
    }
    if (NULL != mA) {
        gsl_matrix_free(mA);
    }
    if (NULL != mV) {
        gsl_matrix_free(mV);
    }
    if (NULL != mS) {
        gsl_vector_free(mS);
    }
    if (NULL != mWork) {
        gsl_vector_free(mWork);
    }
    if (NULL != mH) {
        gsl_matrix_free(mH);
    }


}

void
HomographyMatcher::PrettyPrintMatrix(const gsl_matrix* iMatrix,
                                     const std::string &iName)
{
    printf("%s = [\n", iName.c_str());
    for (int i=0; i<iMatrix->size1; ++i) {
        for (int j=0; j<iMatrix->size2; ++j) {
            printf(" %lf", gsl_matrix_get(iMatrix,i,j));
        }
        if (i <iMatrix->size1-1) {
            printf("\n");
        } else {
            printf("]\n");
        }
    }
}



// int
// filter_matches_homography_ransac(homography_ransac_t *self,
//                                  GArray *matches,
//                                  BotCamTrans *camtrans,
//                                  const int num_iter,
//                                  const double dist_thresh_pix,
//                                  double *homog,
//                                  int group_id)

int
HomographyMatcher::Run(double *iHomog, int iGroupId, GArray *iMatches)
//                        ,BotCamTrans *iCamtrans)
{
    if (iMatches->len < 4) {
        fprintf(stderr, "Could not run HomographyMatcher::Run() because there are fewer than 4 points.\n");
        return 1;
    }

    // Turn off default gsl error handler that aborts program on error
    // n.b. 1: Do not create a new error handler here - it's a static
    //         variable that should be created from a master thread.
    // n.b. 2: This also means that all gsl functions should be checked
    //         for errors on return (usually by looking in the source
    //         .c files since the documentation is kind of sparse)
    gsl_set_error_handler_off();
    // gsl return status - should be GSL_SUCCESS for functions that
    //                     return a status
    int status;
    double dist_thresh_pix_sq = mDistThreshPix*mDistThreshPix;
    gsl_matrix *tmp_H = gsl_matrix_alloc(3,3);

    gsl_matrix *model_rays = gsl_matrix_alloc(iMatches->len, 3);
    gsl_matrix *cur_rays = gsl_matrix_alloc(iMatches->len, 3);

    gsl_matrix *transformed_cur_rays = gsl_matrix_alloc(iMatches->len, 3);
    
    match_pair_t *match;
    double *p_model_rays = gsl_matrix_ptr(model_rays, 0, 0);
    double *p_cur_rays = gsl_matrix_ptr(cur_rays, 0, 0);
    for (int imatch=0; imatch < iMatches->len; ++imatch) {
        match = &g_array_index(iMatches, match_pair_t, imatch);
//         bot_camtrans_unproject_pixel(iCamtrans, match->x1, match->y1,
//                                      &p_model_rays[3*imatch]);
//         bot_camtrans_unproject_pixel(iCamtrans, match->x2, match->y2,
//                                      &p_cur_rays[3*imatch]);
        bot_camtrans_unproject_pixel(match->camtransModel, match->x1, match->y1,
                                     &p_model_rays[3*imatch]);
        bot_camtrans_unproject_pixel(match->camtransCurrent, match->x2, match->y2,
                                     &p_cur_rays[3*imatch]);
    }
    int ransac_index[4];
    double min_dist_sq = 1e10;
    int best_inliers = 0;
    int num_inliers;
    int num_degenerate = 0;
    for (unsigned iiter=0; iiter<mNumIter; ++iiter) {
        gsl_matrix_set_zero(mA);
        ransac_index[0] = g_rand_int_range(mRand, 0, iMatches->len);
        while ((ransac_index[1] = g_rand_int_range(mRand, 0, iMatches->len)) == ransac_index[0]) {} ;
        while (((ransac_index[2] = g_rand_int_range(mRand, 0, iMatches->len)) == ransac_index[0]) ||
               (ransac_index[2] == ransac_index[1])) {} ;
        while (((ransac_index[3] = g_rand_int_range(mRand, 0, iMatches->len)) == ransac_index[0]) ||
               (ransac_index[3] == ransac_index[1]) || (ransac_index[3] == ransac_index[2])) {} ;
        // Warp current points to model points
        for (int ia=0; ia<4; ++ia) {
            for (int itmp=0;itmp<3; ++itmp) {
                gsl_matrix_set(mA, ia*3, itmp+3,
                               -gsl_matrix_get(model_rays,ransac_index[ia], 2) *
                               gsl_matrix_get(cur_rays, ransac_index[ia], itmp));
            }
            for (int itmp=0;itmp<3; ++itmp) {
                gsl_matrix_set(mA, ia*3, itmp+6,
                               gsl_matrix_get(model_rays, ransac_index[ia], 1) *
                               gsl_matrix_get(cur_rays, ransac_index[ia], itmp));
            }
            for (int itmp=0;itmp<3; ++itmp) {
                gsl_matrix_set(mA, ia*3+1, itmp,
                               gsl_matrix_get(model_rays, ransac_index[ia], 2) *
                               gsl_matrix_get(cur_rays, ransac_index[ia], itmp));
            }
            for (int itmp=0;itmp<3; ++itmp) {
                gsl_matrix_set(mA, ia*3+1, itmp+6,
                               -gsl_matrix_get(model_rays, ransac_index[ia], 0) *
                               gsl_matrix_get(cur_rays, ransac_index[ia], itmp));
            }
            for (int itmp=0;itmp<3; ++itmp) {
                gsl_matrix_set(mA, ia*3+2, itmp,
                               -gsl_matrix_get(model_rays, ransac_index[ia], 1) *
                               gsl_matrix_get(cur_rays, ransac_index[ia], itmp));
            }
            for (int itmp=0;itmp<3; ++itmp) {
                gsl_matrix_set(mA, ia*3+2, itmp+3,
                               gsl_matrix_get(model_rays, ransac_index[ia], 0) *
                               gsl_matrix_get(cur_rays, ransac_index[ia], itmp));
            }
        }
        if (mDebug >= 4) {
            PrettyPrintMatrix(mA, "A");
        }
        status = gsl_linalg_SV_decomp(mA, mV, mS, mWork);
        if (status != GSL_SUCCESS) {
            if (mDebug >= 4) {
                printf("WARNING: filter_matches: could not perform SVD on matrix A\n");
                PrettyPrintMatrix(mA, "A");
            }
            continue;
        }
        for (int ih=0; ih<3; ++ih) {
            for (int jh=0; jh<3; ++jh) {
                gsl_matrix_set(tmp_H, ih, jh, gsl_matrix_get(mV, 3*ih+jh, 8));
            }
        }
        if (mDebug >= 4) {
            PrettyPrintMatrix(tmp_H, "H");
            PrettyPrintMatrix(mV, "V");
        }
        gsl_matrix_set_zero(transformed_cur_rays);
        status = gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1, cur_rays, tmp_H, 0, transformed_cur_rays);
        if (status != GSL_SUCCESS) {
            if (mDebug) {
                printf("SERIOUS WARNING: filter_matches: could not transform rays through homography.\n");
            }
            continue;
        }
        double h_33 = gsl_matrix_get(tmp_H, 2, 2);
        if (fabs(h_33) < 1.e-8) continue;
        double h_11_norm = gsl_matrix_get(tmp_H, 0, 0) / h_33;
        if (h_11_norm < 0) continue;
        double h_22_norm = gsl_matrix_get(tmp_H, 1, 1) / h_33;
        if (h_22_norm < 0) continue;
        
//         double projected_cur_pix[3];
        double projected_cur_pix_into_model[3];
        double dx, dy;
        double *p_transformed_cur_rays = gsl_matrix_ptr(transformed_cur_rays, 0, 0);
        double dist_sq = 0.;
        double my_dist_sq;
//         double transformed_cur_x_ctr = 0.;
//         double transformed_cur_y_ctr = 0.;
//         double original_cur_x_ctr = 0.;
//         double original_cur_y_ctr = 0.;
        num_inliers = 0;
        for (int imodel=0; imodel < iMatches->len; ++imodel) {
            match = &g_array_index(iMatches, match_pair_t, imodel);
//             bot_camtrans_project_point(iCamtrans, &p_transformed_cur_rays[3*imodel], projected_cur_pix);
//             bot_camtrans_project_point(match->camtransCurrent,
//                                        &p_transformed_cur_rays[3*imodel],
//                                        projected_cur_pix);
            bot_camtrans_project_point(match->camtransModel,
                                       &p_transformed_cur_rays[3*imodel],
                                       projected_cur_pix_into_model);
//             dx = projected_cur_pix[0] - match->x1;
//             dy = projected_cur_pix[1] - match->y1;
            dx = projected_cur_pix_into_model[0] - match->x1;
            dy = projected_cur_pix_into_model[1] - match->y1;
            my_dist_sq = dx*dx + dy*dy;
            if (my_dist_sq > dist_thresh_pix_sq) {
                dist_sq += dist_thresh_pix_sq;
            } else {
                dist_sq += my_dist_sq;
                ++num_inliers;
//                 original_cur_x_ctr += match->x2;
//                 original_cur_y_ctr += match->y2;
//                 transformed_cur_x_ctr += projected_cur_pix[0];
//                 transformed_cur_y_ctr += projected_cur_pix[1];
            }
        }

        if (num_inliers < 5) continue;

//         original_cur_x_ctr /= (double)num_inliers;
//         original_cur_y_ctr /= (double)num_inliers;
//         transformed_cur_x_ctr /= (double)num_inliers;
//         transformed_cur_y_ctr /= (double)num_inliers;
//         double transformed_cur_dist = 0.;
//         double my_dist;
//         for (int imodel=0; imodel < iMatches->len; ++imodel) {
//             match = &g_array_index(iMatches, match_pair_t, imodel);
// //             bot_camtrans_project_point(iCamtrans, &p_transformed_cur_rays[3*imodel], projected_cur_pix);
//             bot_camtrans_project_point(match->camtransModel,
//                                        &p_transformed_cur_rays[3*imodel],
//                                        projected_cur_pix_into_model);
// //             dx = match->x2 - original_cur_x_ctr - projected_cur_pix[0] + transformed_cur_x_ctr;
// //             dy = match->y2 - original_cur_y_ctr - projected_cur_pix[1] + transformed_cur_y_ctr;
//             dx = match->x2 - original_cur_x_ctr - projected_cur_pix_into_model[0] + transformed_cur_x_ctr;
//             dy = match->y2 - original_cur_y_ctr - projected_cur_pix_into_model[1] + transformed_cur_y_ctr;
//             my_dist = sqrt(dx*dx + dy*dy);
//             if (my_dist <= mDistThreshPix) {
//                 transformed_cur_dist += my_dist;
//             }
//         }
//         transformed_cur_dist /= (double)num_inliers;
//         if (transformed_cur_dist >= 200) {
//             num_degenerate++;
//             continue;
//         }
        
        if (dist_sq < min_dist_sq) {
            gsl_matrix_memcpy(mH, tmp_H);
            min_dist_sq = dist_sq;
            best_inliers = num_inliers;
        }
    } //iterations
    gsl_matrix_set_zero(transformed_cur_rays);
    gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1, cur_rays, mH, 0, transformed_cur_rays);
    if (status != GSL_SUCCESS) {
        if (mDebug >= 2) {
            printf("ERROR: filter_matches: could not transform rays through homography.\n");
        }
        return 1;
    }
    double *p_transformed_cur_rays = gsl_matrix_ptr(transformed_cur_rays, 0, 0);
//     double projected_cur_pix[3];
    double projected_cur_pix_into_model[3];
    double dx, dy;
    for (int imodel=0; imodel < iMatches->len; ++imodel) {
        match = &g_array_index(iMatches, match_pair_t, imodel);
//         bot_camtrans_project_point(iCamtrans, &p_transformed_cur_rays[3*imodel], projected_cur_pix);
        bot_camtrans_project_point(match->camtransModel,
                                   &p_transformed_cur_rays[3*imodel],
                                   projected_cur_pix_into_model);
//         dx = projected_cur_pix[0] - match->x1;
//         dy = projected_cur_pix[1] - match->y1;
        dx = projected_cur_pix_into_model[0] - match->x1;
        dy = projected_cur_pix_into_model[1] - match->y1;
        match->dist = sqrt(dx*dx + dy*dy);
        if (match->dist > mDistThreshPix) {
            match->group_id = 0;
        } else {
            match->group_id = iGroupId;
        }
    }
    if (mDebug >= 2) {
        printf("RANSAC # degenrate homographies = [%d]\n", num_degenerate);
    }
    double tmp_homog[9];
    double homog_33 = gsl_matrix_get(mH, 2, 2);
    if (fabs(homog_33) < 1.e-8) return FALSE;
    for (int ih=0; ih<3; ++ih) {
        for (int jh=0; jh<3; ++jh) {
            gsl_matrix_set(mH, ih, jh, gsl_matrix_get(mH, ih, jh) / homog_33);
            tmp_homog[3*ih+jh] = gsl_matrix_get(mH, ih, jh);
        }
    }
    double tmp_homog_inv[9];
//     if (0 != bot_matrix_inverse_3x3d(tmp_homog, iHomog)) {
    if (0 != bot_matrix_inverse_3x3d(tmp_homog, tmp_homog_inv)) {
        fprintf(stderr, "ERROR: Best ransac homography is non-invertible.\n");
        PrettyPrintMatrix(mH, "H_new_to_model");
        return 1;
    }
    for (int iinv=0; iinv<9; ++iinv) {
        iHomog[iinv] = tmp_homog_inv[iinv] / tmp_homog_inv[8];
    }
    if (mDebug >= 2) {
        printf("After %d iterations, min_dist_sq = %lf, inliers = %.1lf%% (%d/%d)\n",
               mNumIter, min_dist_sq, 100.0*best_inliers/iMatches->len,
               best_inliers, iMatches->len);
        PrettyPrintMatrix (mH, "H_best");
    }
    if (0 == best_inliers) {
        fprintf(stderr, "WARNING: No inliers found after homography filter.\n");
        return 1;
    }

    gsl_matrix_free(tmp_H);
    gsl_matrix_free(model_rays);
    gsl_matrix_free(cur_rays);
    gsl_matrix_free(transformed_cur_rays);

    return 0;

}
