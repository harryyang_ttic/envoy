#ifndef _AGILE_REACQ_OBJECTMODELMATCHER_H_
#define _AGILE_REACQ_OBJECTMODELMATCHER_H_

#include <glib.h>
#include <map>
#include <string>

#include <lcmtypes/erlcm_reacquisition_command_t.h>
#include <bot_core/bot_core.h>
#include <geom_utils/geometry.h>


namespace reacq {

class Reacquisition;
    
class ObjectModelMatcher {

class ImageData {
public:
    enum State {
        StateNew,
        StateProcessing,
        StateDone
    };

public:
    bot_core_image_t* mpImage;
    bot_core_image_t_subscription_t* mpSubscription;
    State mState;
    GMutex* mpMutex;
    GCond* mpCondition;
    std::string mCameraName;

    bot_core_image_t mFullImage;
    bot_core_image_t mSmallImage;
    bot_core_image_t mColorTemp;
    bot_core_image_t mGrayTemp;

    ImageData();
    ~ImageData();
};

public:

    ObjectModelMatcher(Reacquisition *iReacq,
                       const erlcm_reacquisition_command_t *iCmd);
    
    ~ObjectModelMatcher();

    void Stop();
    static void *InnerMatch (void *user);

private:
    static void onImage(const lcm_recv_buf_t *iRbuf,
                        const char *iChannel, 
                        const bot_core_image_t *iMsg,
                        void *iUserData);

private:

    Reacquisition *mpReacq;
    erlcm_reacquisition_command_t *mpCmd;
    bool mRunning;
    lcm_t *mpLCM;
    std::map<std::string, ImageData*> mImageDataMap;
    std::map<std::string, std::string> mChannelToCameraMap;

}; //class ObjectModelMatcher

} //namespace reacq


#endif // _AGILE_REACQ_OBJECTMODELMATCHER_H_
