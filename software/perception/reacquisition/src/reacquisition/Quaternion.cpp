#include "Quaternion.hpp"

using namespace std;
using namespace reacq;

void Quaternion::setValue(const Vector3& iAxis, const double iAngle) {
  double norm = iAxis.getMagnitude();
  if (norm > 0) {
    double factor = sin(iAngle/2)/norm;
    setValue(factor*iAxis(0), factor*iAxis(1), factor*iAxis(2), cos(iAngle/2));
  }
  else
    setValue(0,0,0,1);
}

void Quaternion::setValue(const double iRoll, const double iPitch,
                          const double iYaw) {
  double sinr2 = sin(0.5*iRoll);
  double sinp2 = sin(0.5*iPitch);
  double siny2 = sin(0.5*iYaw);

  double cosr2 = cos(0.5*iRoll);
  double cosp2 = cos(0.5*iPitch);
  double cosy2 = cos(0.5*iYaw);

  mX = sinr2 * cosp2 * cosy2 - cosr2 * sinp2 * siny2;
  mY = cosr2 * sinp2 * cosy2 + sinr2 * cosp2 * siny2;
  mZ = cosr2 * cosp2 * siny2 - sinr2 * sinp2 * cosy2;
  mW = cosr2 * cosp2 * cosy2 + sinr2 * sinp2 * siny2;

  normalize();
}


bool Quaternion::setValue(const Matrix33 iRotation) {

  double trace = iRotation(0,0) + iRotation(1,1) + iRotation(2,2) + 1;
  if (trace > 1e-8) {
    double s = 2*sqrt(trace);
    double sInv = 1/s;
    setValue(sInv*(iRotation(2,1)-iRotation(1,2)),
             sInv*(iRotation(0,2)-iRotation(2,0)),
             sInv*(iRotation(1,0)-iRotation(0,1)),
             0.25*s);
  }
  else {
    if (iRotation(0,0)>iRotation(1,1) && iRotation(0,0)>iRotation(2,2)) {
      double s = 2*sqrt(1 + iRotation(0,0) - iRotation(1,1) - iRotation(2,2));
      double sInv = 1/s;
      setValue(0.25*s, sInv*(iRotation(1,0)+iRotation(0,1)),
               sInv*(iRotation(0,2)+iRotation(2,0)),
               sInv*(iRotation(2,1)-iRotation(1,2)));
    }
    else if (iRotation(1,1) > iRotation(2,2)) {
      double s = 2*sqrt(1 + iRotation(1,1) - iRotation(0,0) - iRotation(2,2));
      double sInv = 1/s;
      setValue(sInv*(iRotation(1,0)+iRotation(0,1)), 0.25*s,
               sInv*(iRotation(2,1)+iRotation(1,2)),
               sInv*(iRotation(0,2)-iRotation(2,0)));
    }
    else {
      double s = 2*sqrt(1 + iRotation(2,2) - iRotation(0,0) - iRotation(1,1));
      double sInv = 1/s;
      setValue(sInv*(iRotation(0,2)+iRotation(2,0)),
               sInv*(iRotation(2,1)+iRotation(1,2)), 0.25*s,
               sInv*(iRotation(1,0)-iRotation(0,1)));
    }
  }

  return true;
}


void Quaternion::getValue(Vector3& oAxis, double& oAngle) const {
  oAngle = 2*acos(mW);
  oAxis(0) = mX;
  oAxis(1) = mY;
  oAxis(2) = mZ;
  oAxis = oAxis * (1/oAxis.getMagnitude());
}

void Quaternion::getValue(double& oRoll, double& oPitch, double& oYaw) const {
  oRoll = atan2(2 * (mW*mX + mY*mZ), 1 - 2 * (mX*mX + mY*mY));
  oPitch = asin(2 * (mW*mY - mZ*mX));
  oYaw = atan2(2 * (mW*mZ + mX*mY), 1 - 2 * (mY*mY + mZ*mZ));
}

Matrix33 Quaternion::getRotationMatrix() const {
  Matrix33 rot;

  double x2 = mX*mX;
  double y2 = mY*mY;
  double z2 = mZ*mZ;
  double w2 = mW*mW;
  double xy = 2*mX*mY;
  double xz = 2*mX*mZ;
  double yz = 2*mY*mZ;
  double wx = 2*mW*mX;
  double wy = 2*mW*mY;
  double wz = 2*mW*mZ;

  rot(0,0) = w2+x2-y2-z2;  rot(0,1) = xy-wz;        rot(0,2) = xz+wy;
  rot(1,0) = xy+wz;        rot(1,1) = w2-x2+y2-z2;  rot(1,2) = yz-wx;
  rot(2,0) = xz-wy;        rot(2,1) = yz+wx;        rot(2,2) = w2-x2-y2+z2;

  return rot;
}

Quaternion Quaternion::operator*(const Quaternion& iQuat) const {
  Quaternion res;

  res.mX = mW*iQuat.mX + mX*iQuat.mW + mY*iQuat.mZ - mZ*iQuat.mY;
  res.mY = mW*iQuat.mY - mX*iQuat.mZ + mY*iQuat.mW + mZ*iQuat.mX;
  res.mZ = mW*iQuat.mZ + mX*iQuat.mY - mY*iQuat.mX + mZ*iQuat.mW;
  res.mW = mW*iQuat.mW - mX*iQuat.mX - mY*iQuat.mY - mZ*iQuat.mZ;

  return res;
}
