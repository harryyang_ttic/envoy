#include "ImageBuffer.hpp"

#include <string>
#include <glib.h>
#include <iostream>

using namespace std;
using namespace reacq;

int main (const int argc, const char **argv)
{
    string imageChannel = "FRNT_IMAGE";
    string poseChannel = "POSE";
    cout << "Creating image buffer." << flush;
    ImageBuffer buf(poseChannel, 200, imageChannel, 10, 0.5, 5.);
    cout << "Done." << endl;
    buf.Start();
    cout << "Started image buffer." << endl;

    GMainLoop *mainloop;
    mainloop = g_main_loop_new (NULL, FALSE);
    g_main_loop_run (mainloop);

    return 0;
}
