#ifndef _AGILE_REACQ_REACQUISITION_H_
#define _AGILE_REACQ_REACQUISITION_H_

#include <string>
#include <map>

#include <bot_core/bot_core.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_frames/bot_frames.h>

#include "ImageBuffer.hpp"
#include "ObjectModel.hpp"
#include "ObjectModelMatcher.hpp"
#include "LensModel.hpp"

#include <lcmtypes/erlcm_reacquisition_segmentation_list_t.h>
#include <lcmtypes/erlcm_reacquisition_command_list_t.h>
#include <lcmtypes/erlcm_reacquisition_detection_list_t.h>
#include <object_model/object_client.h>

namespace reacq {

class Reacquisition {

  public:

    struct Camera {
        LensModel *mpLensModel;
        double mPose[16];
        bool mUseForMatching;
    };

    Reacquisition(const std::string &iLearningChannel,
                  const std::string &iMatchingChannel,
                  const std::string &iPublishChannel,
                  const int32_t iPoseBufferCapacity,
                  const int32_t iImageBufferCapacity,
                  const double iPositionThreshold,
                  const double iAngleThreshold,
                  const std::string &iInputModelFilename,
                  const std::string &iOutputModelFilename,
                  const std::string &iMatlabResultsFilename
                  );

    ~Reacquisition();

    inline std::string GetMatlabResultsFilename() { return mMatlabResultsFilename; }
    inline int GetMatlabIndex() { return mMatlabIndex; }
    inline void IncrementMatlabIndex() { ++mMatlabIndex; }

    void InitializeMatlabFile();
    
    void SetupBuffersAndCameras(const int32_t iPoseBufferCapacity,
                                const int32_t iImageBufferCapacity,
                                const double iPositionThreshold,
                                const double iAngleThreshold);

    
    static void OnSegmentationList(const lcm_recv_buf_t *iRbuf, const char *iChannel, 
                                   const erlcm_reacquisition_segmentation_list_t *iMsg,
                                   void *iUserData);

    static void OnCommandList(const lcm_recv_buf_t *iRbuf, const char *iChannel, 
                              const erlcm_reacquisition_command_list_t *iMsg,
                              void *iUserData);
    

    bool AddModel(ObjectModel *iModel, bool iWriteModels);
    bool AddModel(ObjectModel *iModel, bool iWriteModels, bool newObject);

    ObjectModel *FindModel(int64_t iId);

    inline const std::string GetPublishChannel() const { return mPublishChannel; }

    void GetImageBufferTimestampsAndPoses(const std::string &iCameraName,
                                          std::vector<int64_t> &oTimes,
                                          std::vector<bot_core_pose_t*> &oPoses);

    ImageWithPose GetImageBufferImageByTimestamp(const std::string &iCameraName,
                                                 int64_t iTimestamp);

    std::vector<std::string> GetCameraNames() const;

    std::string GetImageChannelName(const std::string &iCameraName);

    Camera *GetCamera(const std::string &iCameraName);
    
    bool RemoveObjectModel(int64_t iId);

    inline ObjectWorldModel *GetWorldModel() { return mWorldModel; }

    inline bool ShouldWriteMatlabResults() { return mWriteMatlabResults; }

    int GetConvexHullSegmentationList(erlcm_reacquisition_segmentation_t *in);

private:

    std::map<std::string, ImageBuffer*> mImageBuffers;
    std::map<int64_t, ObjectModel*> mModels;
    std::map<std::string, Camera*> mCameras;
    std::map<int64_t, ObjectModelMatcher*> mMatchers;

    ObjectWorldModel *mWorldModel;
    GMutex *mMutex;
    BotParam *mParam;
    lcm_t *mLcm;
    BotFrames *mFrames;
    
    std::string mLearningChannel;
    erlcm_reacquisition_segmentation_list_t_subscription_t *mLearningSub;

    std::string mMatchingChannel;
    erlcm_reacquisition_command_list_t_subscription_t *mMatchingSub;

    std::string mPublishChannel;

    std::string mOutputModelFilename;

    std::string mMatlabResultsFilename;
    bool mWriteMatlabResults;
    int mMatlabIndex;

}; // class Reacquisition

typedef std::map<std::string, ImageBuffer*>::iterator ImageIter;
typedef std::map<std::string, ImageBuffer*>::const_iterator ImageConstIter;
typedef std::map<int64_t, ObjectModel*>::iterator ObjectModelIter;
typedef std::map<int64_t, ObjectModel*>::const_iterator ObjectModelConstIter;
typedef std::map<std::string, Reacquisition::Camera*>::iterator CamerasIter;
typedef std::map<std::string, Reacquisition::Camera*>::const_iterator CamerasConstIter;
typedef std::map<int64_t, ObjectModelMatcher*>::iterator MatcherIter;
typedef std::map<int64_t, ObjectModelMatcher*>::const_iterator MatcherConstIter;
    
} // namespace reacq

#endif // _AGILE_REACQ_REACQUISITION_H_
