#include "ObjectModelBuilder.hpp"

#include <iostream>
#include <fstream>
#include <unistd.h>

#include <sys/time.h>
#include <GL/gl.h>

#include "Utilities.hpp"
#include "IoUtilities.hpp"
#include "BundleAdjust.hpp"
#include "BundleAdjustSparse.hpp"
#include "BundleIntersection.hpp"
#include "AlgorithmParameters.hpp"
#include "PointGridder.hpp"
#include "LensModel.hpp"


//TODO move to config file
#define DELTA_POSE_THRESHOLD_SQ 0.04
#define SIFT_MATCH_THRESHOLD 1.5

#define MIN_SIFT_MATCHES 30

#define SUBIMAGE_PADDING 20

#define NUM_MESSAGE_WAIT_LOOPS 500
#define MESSAGE_WAIT_TIME_USEC 10000

#define OBJECT_CULL_THRESHOLD 1.

using namespace reacq;
using namespace std;

ObjectModelBuilder::ObjectModelBuilder(Reacquisition *iReacq,
                                       const erlcm_reacquisition_segmentation_t *iSeg)
{
    mpSeg =  erlcm_reacquisition_segmentation_t_copy (iSeg);
    mpReacq = iReacq;
    cout << "Created Builder." << endl;

    if (!g_thread_supported ()) {
        g_thread_init (NULL);
    }
    cout << "Running Builder." << endl;
    g_thread_create (InnerRun, this, TRUE, NULL);
}

ObjectModelBuilder::~ObjectModelBuilder()
{
    if (NULL != mpSeg) {
        erlcm_reacquisition_segmentation_t_destroy (mpSeg);
    }
    cout << "Destroy Builder." << endl;
}


void *
ObjectModelBuilder::InnerRun (void *user)
{
    /* Init */
    ObjectModelBuilder *builder = static_cast<ObjectModelBuilder*>(user);
    ObjectModel *model = new ObjectModel();
    int iwait = 0;
    bool newObject = false;
    cerr << "Searching for object ["
         << builder->mpSeg->object_id << "] in world model..."
         << endl;
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    // Get this object from the world model if it exists
    int64_t object_id = builder->mpSeg->object_id;
    if (object_id == 0)
        object_id = Utilities::getUniqueId();

    
    while ((NUM_MESSAGE_WAIT_LOOPS > iwait) && (NULL == model->mpObject)) {
        model->mpObject = om_get_object_by_id(builder->mpReacq->GetWorldModel(),
                                              object_id);
        usleep(MESSAGE_WAIT_TIME_USEC);
        ++iwait;
    }

    if (NULL == model->mpObject) {
        cerr << "No object with id " << object_id << " found in world model. Adding new object."
             << endl;

        model->mpObject = (om_object_t *) calloc(1, sizeof(om_object_t));
        model->mpObject->utime = bot_timestamp_now();
        model->mpObject->id = object_id;
        model->mpObject->object_type = OM_OBJECT_T_UNKNOWN;
        model->mpObject->label = "none";
        newObject = true;
    }
    else
        cerr << "Found object ["
         << builder->mpSeg->object_id << "] in world model." << endl;

#ifdef MARBLE_TEST
    /* START temporary code */
    cout << "builder->mpSeg = " << builder->mpSeg << endl;
    cout << "builder->mpSeg->object_id = "
         << builder->mpSeg->object_id << endl;
    //model->mpObject->id = builder->mpSeg->object_id;

    lcm_t *lcm = bot_lcm_get_global (NULL);
    BotParam *param = bot_param_new_from_server (lcm, 0);
    BotFrames *frames = bot_frames_get_global (lcm, param);

    BotTrans body_to_local;
    bot_frames_get_trans_at_utime (frames, "body", "local", &body_to_local, mpSeg->image_utime);
    //memcpy (model->mpObject->orientation, body_to_local.rot_quat,
    //        4 * sizeof(double));


    //memcpy(model->mpObject->orientation,
    //       builder->mpSeg->pose.orientation, 4*sizeof(double));
    //wm_update_pallet(builder->mpReacq->GetWorldModel(),
    //                 model->mpObject, ARLCM_PALLET_T_REAQUISITION);
    /* END TEMPORARY CODE */
    cout << "Created ObjectModelBuilder instance." << endl;

    /* Let's not continue past here for now (debugging) */
    delete builder;
    return NULL;
#else
    Reacquisition *reacq = builder->mpReacq;
    if (NULL == reacq) {
        cerr << "ERROR: ObjectModelBuilder.cpp: Reacquisition object is null." << endl;
        delete builder;
        return NULL;
    }
    erlcm_reacquisition_segmentation_t *seg = builder->mpSeg;
    if (NULL == seg) {
        cerr << "ERROR: ObjectModelBuilder.cpp: erlcm_reacquisition_segmentation_t is null." << endl;
        delete builder;
        return NULL;
    }
    /* Find best feature times and poses, get features */
    int64_t refTime = seg->image_utime;
    string camera = seg->camera;
    vector<int64_t> imageTimes;
    vector<bot_core_pose_t*> imagePoses;
    //TODO Change GetFeatureBufferTimestampsAndPoses to return index as well
    //     Expose and use Feature and ImageBuffer Lock() and Unlock() methods
    //     to ensure that we get the desired second feature and image
    // Lock() featureBuffer here
    // TODO make a while loop to wait for at least two poses
    //      and remove the assumptions that poses[0] is before poses[1]
    //      and other assumptions too(?)
    fprintf(stderr,"Learning Segment time : %.6f\n", seg->utime/1.0e6);
    
    reacq->GetImageBufferTimestampsAndPoses(camera, imageTimes, imagePoses);
    cout << "Got [" << imageTimes.size() << "] times and ["
         << imagePoses.size() << "] poses." << endl;

    if (imageTimes.size() < 2) {
        cerr << "ERROR: Could not build model because there are only ["
             << imageTimes.size() << "] features in the buffer for camera " << camera << "." << endl;
        delete builder;
        return NULL;
    }

    // firstImage: Image in buffer with timestamp closest to segmentation image utime
    // firstPose:  Corresponding pose
    ImageWithPose firstImage =
        reacq->GetImageBufferImageByTimestamp(camera, refTime);
    bot_core_pose_t *firstPose = firstImage.pose;
    bot_core_image_t *firstImg = firstImage.image;

    cout << "First low-res feature {desired, actual} times = {"
         << refTime << "," << firstImg->utime << "}" << endl;

    // Find the (pose,image) in buffer with pose > DELTA_POSE_THRESHOLD from firstPose
    bool imageFound = false;
    int imageIndex = imageTimes.size() - 1;
    while (!imageFound && imageIndex >= 0) {
        double dPoseX = firstPose->pos[0] - imagePoses[imageIndex]->pos[0];
        double dPoseY = firstPose->pos[1] - imagePoses[imageIndex]->pos[1];
        // skipping dPoseZ //
        double dPoseSq = dPoseX * dPoseX + dPoseY * dPoseY;
        if ((dPoseSq > DELTA_POSE_THRESHOLD_SQ) &&
            (imagePoses[imageIndex]->utime < refTime)) {
            imageFound = true;
        } else {
            imageIndex--;
        }
    }

    if (!imageFound) {
        cerr << "ERROR: Could not find two good images with sufficient baseline for input message."
             << endl;
        delete builder;
        return NULL;
    }

    // secondImage: Image from buffer with pose far enough from firstImage's firstPose
    // secondPose:  Corresponding pose
    ImageWithPose secondImage =
        reacq->GetImageBufferImageByTimestamp(camera,
                                              imageTimes[imageIndex]);
    bot_core_pose_t *secondPose = secondImage.pose;
    bot_core_image_t *secondImg = secondImage.image;
    cout << "Second low-res feature {desired, actual} times = {"
         << imageTimes[imageIndex] << "," << secondImg->utime
         << "}" << endl;

    fprintf (stdout, "First pose: pos = [%.2f %.2f %.2f], quat = [%.2f %.2f %.2f %.2f]\n",
             firstPose->pos[0], firstPose->pos[1], firstPose->pos[2], firstPose->orientation[0],
             firstPose->orientation[1], firstPose->orientation[2], firstPose->orientation[3]);
    fprintf (stdout, "Second pose: pos = [%.2f %.2f %.2f], quat = [%.2f %.2f %.2f %.2f]\n",
             secondPose->pos[0], secondPose->pos[1], secondPose->pos[2], secondPose->orientation[0],
             secondPose->orientation[1], secondPose->orientation[2], secondPose->orientation[3]);


    // Unlock() featureBuffer here
    /* Get the images as soon as possible after the features */
    bot_core_image_t fullFirstImage, fullSecondImage, resizedImage;
    bot_core_image_t rgbBuf, grayBuf;
    cout << "Processing original sift features." << endl;

    // Allocate buffers that will contained resized image used for SIFT extraction
    Utilities::allocateMultiScaleBuffers(firstImg,
                                         &fullFirstImage,
                                         &resizedImage,
                                         &rgbBuf,
                                         &grayBuf,
                                         AlgorithmParameters::mSmallImageScale,
                                         AlgorithmParameters::mSmallImageScale);


    Utilities::duplicateAlignedFloatImage(&fullFirstImage, &fullSecondImage);


    // resizedImage: Scaled version of firstImg from which sift features will be extracted
    Utilities::decompressToMultiScale(firstImg,
                                      &fullFirstImage,
                                      &resizedImage,
                                      &rgbBuf,
                                      &grayBuf);

    struct timespec start;
    bot_timespec_now(&start);
    
    // Run SIFT on smaller resizedImage
    sift_point_feature_list_t *firstContextPts =
        sift_compute_uncompressed(resizedImage.width, resizedImage.height,
                                  AlgorithmParameters::mSiftLevels,
                                  (float*)resizedImage.data,
                                  AlgorithmParameters::mSiftThreshContext,
                                  AlgorithmParameters::mSiftDoubleImageSize,
                                  AlgorithmParameters::mSiftSigmaInit,
                                  firstImg->utime);

    struct timespec end;
    bot_timespec_now(&end);
    bot_timespec_subtract(&end, &start);
    int sift_time = bot_timespec_milliseconds(&end);


    // Scale the sift features back to original image size
    Utilities::scaleSiftFeatures(firstContextPts,
                                 1./AlgorithmParameters::mSmallImageScale,
                                 1./AlgorithmParameters::mSmallImageScale);

    // Repeat scaling for secondImg
    // resizedImage: Scaled version of firstImage from which sift features will be extracted
    Utilities::decompressToMultiScale(secondImg,
                                      &fullSecondImage,
                                      &resizedImage,
                                      &rgbBuf,
                                      &grayBuf);
    bot_timespec_now(&start);

    // Run sift on resizedImage
    sift_point_feature_list_t *secondContextPts =
        sift_compute_uncompressed(resizedImage.width, resizedImage.height,
                                  AlgorithmParameters::mSiftLevels,
                                  (float*)resizedImage.data,
                                  AlgorithmParameters::mSiftThreshContext,
                                  AlgorithmParameters::mSiftDoubleImageSize,
                                  AlgorithmParameters::mSiftSigmaInit,
                                  secondImg->utime);

    bot_timespec_now(&end);
    bot_timespec_subtract(&end, &start);
    sift_time += bot_timespec_milliseconds(&end);
    cout << "***** SIFT feature computation time: [" << sift_time << "] ms." << endl;

    Utilities::scaleSiftFeatures(secondContextPts,
                                 1./AlgorithmParameters::mSmallImageScale,
                                 1./AlgorithmParameters::mSmallImageScale);

    Utilities::freeAlignedImage(&rgbBuf);
    Utilities::freeAlignedImage(&grayBuf);
    Utilities::freeAlignedImage(&resizedImage);
    
    /* Convert sift_point_feature_list_t to Descriptor */
    vector<Descriptor> firstContextDesc =
        Utilities::makeDescriptorsFromFeatureList(firstContextPts);
    vector<Descriptor> secondContextDesc =
        Utilities::makeDescriptorsFromFeatureList(secondContextPts);
    LensModel *lensModel = reacq->GetCamera(camera)->mpLensModel;
    /* Match features */
    vector<PointMatch> matches;
#define MATLAB
#ifdef MATLAB
    ofstream ofs("/share/matchDescriptorDistance.m",ios::out);
    ofs << IoUtilities::pointFeaturesToMatlabString("features1",
                                                    firstContextPts);
    ofs << IoUtilities::pointFeaturesToMatlabString("features2",
                                                    secondContextPts);
    string ct = IoUtilities::lensModelToMatlabString(lensModel);
    ofs << ct << endl;
    ofs << "timestamps = [ "
        << firstContextPts->utime << ", "
        << secondContextPts->utime << " ];" << endl;
    ofs.close();
#endif

    // Feature matching between pairs
    bot_timespec_now(&start);
    Utilities::matchDescriptorsDistance(firstContextDesc, secondContextDesc,
                                        static_cast<float>(SIFT_MATCH_THRESHOLD),
                                        matches);
    bot_timespec_now(&end);
    bot_timespec_subtract(&end, &start);
    cout << "***** SIFT feature match time: [" << bot_timespec_milliseconds(&end) << "] ms." << endl;
    cout << "Found [" << matches.size() << "] scene matches." << endl;

    if (MIN_SIFT_MATCHES > matches.size()) {
        cerr << "ERROR: no low-res sift matches found." << endl;
        delete builder;
        Utilities::freeAlignedImage(&fullFirstImage);
        Utilities::freeAlignedImage(&fullSecondImage);
        return NULL;
    }

    // pare down matches to top k
//      if (false)
    if (true)
    {
        vector<Vector2> points(matches.size());
        vector<double> scores(matches.size());
        for (int i = 0; i < matches.size(); ++i) {
            points[i](0) = firstContextPts->pts[matches[i].mIndex1].col;
            points[i](1) = firstContextPts->pts[matches[i].mIndex1].row;
            scores[i] = matches[i].mScore;
        }

        vector<int> binnedIndices(matches.size());
        PointGridder gridder;
        gridder.setImageSize(fullFirstImage.width, fullFirstImage.height);
        gridder.setNumBins(16,16);
        gridder.setDescending(false);
        gridder.binPoints(points, scores, binnedIndices);

        int numPoints = std::min(AlgorithmParameters::mSiftMaxNumMatches,
                                 (int)matches.size());
        vector<PointMatch> newMatches(numPoints);
        for (int i = 0; i < numPoints; ++i) {
            newMatches[i] = matches[binnedIndices[i]];
        }
        matches = newMatches;
        cout << "Gridded points with " << matches.size() <<
            " matches remaining" << endl;
    }
    

    // Bundle Adjustment:
    //
    // Initializization

    //TODO Functionize some of the bundle adjust and intersection problems
    /* Setup problem for bundle adjustment */
    size_t numMatches = matches.size();
    Poses poses(2);
    for (int i=0; i<2; ++i) {
        poses[i] = new Pose();
        poses[i]->mOrientation.makeIdentity();
        poses[i]->mPosition.zero();
    }
    ModelPoints scenePoints(numMatches);
    Vector3 ones;
    ones(0) = 1;
    ones(1) = 1;
    ones(2) = 1;
    for (int i=0; i<numMatches; ++i) {
        scenePoints[i] = new ModelPoint();
        scenePoints[i]->mPoint = ones;
        scenePoints[i]->mDescriptor =
            Utilities::copyDescriptor(firstContextDesc[matches[i].mIndex1]);
    }

    Measurements adjustMeas(2*numMatches);
    for (int i=0; i<numMatches; ++i) {
        adjustMeas[2*i] = new Measurement();
        adjustMeas[2*i]->mImagePoint(0) =
            firstContextPts->pts[matches[i].mIndex1].col;
        adjustMeas[2*i]->mImagePoint(1) =
            firstContextPts->pts[matches[i].mIndex1].row;
        adjustMeas[2*i]->mModelPointIndex = i;
        adjustMeas[2*i]->mPoseIndex = 0;
        adjustMeas[2*i+1] = new Measurement();
        adjustMeas[2*i+1]->mImagePoint(0) =
            secondContextPts->pts[matches[i].mIndex2].col;
        adjustMeas[2*i+1]->mImagePoint(1) =
            secondContextPts->pts[matches[i].mIndex2].row;
        adjustMeas[2*i+1]->mModelPointIndex = i;
        adjustMeas[2*i+1]->mPoseIndex = 1;
    }

    // initialize bundle adjustment parameters
    if (false)
    {
        vector<Vector2> points1(numMatches), points2(numMatches);
        for (int i = 0; i < numMatches; ++i) {
            points1[i](0) = firstContextPts->pts[matches[i].mIndex1].col;
            points1[i](1) = firstContextPts->pts[matches[i].mIndex1].row;
            points2[i](0) = secondContextPts->pts[matches[i].mIndex2].col;
            points2[i](1) = secondContextPts->pts[matches[i].mIndex2].row;
        }
        BundleAdjust::initPoseFromPair(points1, points2,
                                       lensModel, 5, *poses[1]);
        // TODO: make param

        cout << "Initialized pose; translation is " <<
            poses[1]->mPosition(0) << " " << poses[1]->mPosition(1) << " " <<
            poses[1]->mPosition(2) << endl;

        for (int i = 0; i < numMatches; ++i) {
            Vector2 p1, p2;
            p1(0) = firstContextPts->pts[matches[i].mIndex1].col;
            p1(1) = firstContextPts->pts[matches[i].mIndex1].row;
            p2(0) = secondContextPts->pts[matches[i].mIndex2].col;
            p2(1) = secondContextPts->pts[matches[i].mIndex2].row;
            BundleAdjust::intersectPair(p1, p2, *poses[0], *poses[1],
                                        lensModel, scenePoints[i]->mPoint);
        }
    }

    bot_timespec_now(&start);
//     BundleAdjust bundleAdjust;
//     RobustLM::Result res = bundleAdjust.solve(adjustMeas, poses, scenePoints, lensModel);
    BundleAdjustSparse bundleAdjust;
    bundleAdjust.setRobustKernel(BundleAdjustSparse::RobustKernelTukey);
    BundleAdjustSparse::Result res = bundleAdjust.solve(adjustMeas, poses, scenePoints, lensModel);
    bot_timespec_now(&end);
    bot_timespec_subtract(&end, &start);
    cout << "***** Bundle Adjust solve time: [" << bot_timespec_milliseconds(&end) << "] ms." << endl;

    Matrix44 origPose1 = Utilities::matrixFromRotationTranslation(poses[0]->mOrientation,
                                                                  poses[0]->mPosition);
    Matrix44 origPose2 = Utilities::matrixFromRotationTranslation(poses[1]->mOrientation,
                                                                  poses[1]->mPosition);
    cout << "---------- After bundle adjustment ----------" << endl;
    cout << "poses[0] Orientation = " << endl
         << poses[0]->mOrientation << endl;
    cout << "poses[0] Position = " << endl
         << poses[0]->mPosition << endl;
    cout << "poses[1] Orientation = " << endl
         << poses[1]->mOrientation << endl;
    cout << "poses[1] Position = " << endl
         << poses[1]->mPosition << endl;

    /* Converting poses to real-world scales:
       Matrix variable names c=camera, b=body, w=world
       Steps:
         1) Transform both solved poses to local frame using known
            camera-to-body and body-to-world (from bot) poses
         2) Set the correct baseline distance between the solved
            cameras using distance between bot-measured camera poses
    */
    /* Start 1 */
    Matrix44 cb = Utilities::matrixFromArray(reacq->GetCamera(camera)->mPose);
    double tmp[16];
    bot_quat_pos_to_matrix(firstPose->orientation,
                           firstPose->pos,
                           tmp);
    Matrix44 bw1 = Utilities::matrixFromArray(tmp);
    bot_quat_pos_to_matrix(secondPose->orientation,
                           secondPose->pos,
                           tmp);
    Matrix44 bw2 = Utilities::matrixFromArray(tmp);
    Matrix44 cw1 = bw1 * cb;
    Matrix44 cw2 = bw2 * cb;
    Matrix44 f1 = Utilities::matrixFromRotationTranslation(poses[0]->mOrientation,
                                                           poses[0]->mPosition);
    Matrix44 f2 = Utilities::matrixFromRotationTranslation(poses[1]->mOrientation,
                                                           poses[1]->mPosition);
    Matrix44 t = cw1 * f1.getInverse();
    Utilities::matrixToRotationTranslation(t * f1, poses[0]->mOrientation,
                                           poses[0]->mPosition);
    Utilities::matrixToRotationTranslation(t * f2, poses[1]->mOrientation,
                                           poses[1]->mPosition);

    /* Start 2 */
    Matrix33 tmp2;
    Vector3 posw1, posw2;
    Utilities::matrixToRotationTranslation(cw1, tmp2, posw1);
    Utilities::matrixToRotationTranslation(cw2, tmp2, posw2);
    double magWorld = (posw1 - posw2).getMagnitude();
    Vector3 directionFeats = poses[1]->mPosition - poses[0]->mPosition;
    double magFeats = directionFeats.getMagnitude();
    poses[1]->mPosition = poses[0]->mPosition + directionFeats * magWorld/magFeats;

    // Construct overall transform matrix that aligns solved poses with measured body poses
    Matrix44 scaleShift;
    scaleShift.makeIdentity();
    double scaleFactor = magWorld/magFeats;
    scaleShift(0,0) = scaleShift(1,1) = scaleShift(2,2) = scaleFactor;
    for (int k = 0; k < 3; ++k) {
        scaleShift(k,3) = (1-scaleFactor)*poses[0]->mPosition(k);
    }
    Matrix44 transformToLocalFrame = scaleShift*t;
    Matrix33 rotationToLocalFrame;
    Vector3 translationToLocalFrame;
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            rotationToLocalFrame(i,j) = transformToLocalFrame(i,j);
        }
        translationToLocalFrame(i) = transformToLocalFrame(i,3);
    }

    Matrix44 finalPose1 = Utilities::matrixFromRotationTranslation(poses[0]->mOrientation,
                                                                   poses[0]->mPosition);
    Matrix44 finalPose2 = Utilities::matrixFromRotationTranslation(poses[1]->mOrientation,
                                                                   poses[1]->mPosition);
    Matrix44 tmper1 = transformToLocalFrame * origPose1;
    Matrix44 tmper2 = transformToLocalFrame * origPose2;
    cout << "diff orig 1 = " << endl << finalPose1 - tmper1;
    cout << "diff orig 2 = " << endl << finalPose2 - tmper2;
    

    // Adjust 3D scene points by the same transformation
    for (int k = 0; k < scenePoints.size(); ++k) {
        scenePoints[k]->mPoint = rotationToLocalFrame*scenePoints[k]->mPoint +
            translationToLocalFrame;
    }
    
    
    cout << "---------- Real-world poses ----------" << endl;
    cout << "poses[0] Orientation = " << endl
         << poses[0]->mOrientation << endl;
    cout << "poses[0] Position = " << endl
         << poses[0]->mPosition << endl;
    cout << "poses[1] Orientation = " << endl
         << poses[1]->mOrientation << endl;
    cout << "poses[1] Position = " << endl
         << poses[1]->mPosition << endl;
    /* Set up and run bundle intersection problem */
    cout << "----------  Bundle Intersection ----------" << endl;
    Measurements intersectMeas(2);
    BundleIntersection intersection;
    for (int i=0; i<numMatches; ++i) {
        intersectMeas[0] = adjustMeas[2*i];
        intersectMeas[1] = adjustMeas[2*i+1];
        intersection.solve(intersectMeas, *scenePoints[i],
                           poses, lensModel);
    }
#define MATLAB
#ifdef MATLAB
    {
        ofstream ofs2("/share/Intersection.m", ios::out);
        ofs2 << "model.scene = [ ..." << endl;
        for (int i=0; i<numMatches; ++i) {
            ofs2 << scenePoints[i]->mPoint(0) << ", "
                 << scenePoints[i]->mPoint(1) << ", "
                 << scenePoints[i]->mPoint(2) << "; ..." << endl;
        }
        ofs2 << "];" << endl;
        ofs2.close();
    }
#endif
    model->mContextPoints = scenePoints;
    model->mContextMeasurements = adjustMeas;
    model->mCameraPoses = poses;
    model->mpGesture = erlcm_reacquisition_segmentation_t_copy(seg);
    model->mpLensModel = lensModel;
    /* Compute SIFT and solve bundle intersection for high-res images */
    int xMin, yMin, xMax, yMax;
    int xMinPadded, yMinPadded, xMaxPadded, yMaxPadded;
    Utilities::bboxFromRoi(seg->roi, xMin, yMin, xMax, yMax);
    xMinPadded = max<int>(xMin - SUBIMAGE_PADDING, 0);
    yMinPadded = max<int>(yMin - SUBIMAGE_PADDING, 0);
    xMaxPadded = min<int>(xMax + SUBIMAGE_PADDING, fullFirstImage.width  - 1);
    yMaxPadded = min<int>(yMax + SUBIMAGE_PADDING, fullFirstImage.height - 1);
    cout << "Gesture bounding box {xmin, ymin, xmax, ymax} = "
         << "  * Original: " << xMin << ", " << yMin
         << ", " << xMax << ", " << yMax << endl
         << "  * Padded: " << xMinPadded << ", " << yMinPadded
         << ", " << xMaxPadded << ", " << yMaxPadded << endl;

    int nx = xMaxPadded - xMinPadded + 1;
    int ny = yMaxPadded - yMinPadded + 1;
    float firstSubImage[nx * ny];

    float *src = (float*)fullFirstImage.data;
    src += fullFirstImage.width * yMinPadded + xMinPadded;
    float *dest = firstSubImage;
    int nbytes = nx*sizeof(float);
    for (int row=yMinPadded;row<=yMaxPadded;row++) {
        memcpy(dest, src, nbytes);
        src += fullFirstImage.width;
        dest += nx;
    }

#undef BINDEBUG
#ifdef BINDEBUG
    {
        int nbytes2 = nx * ny * sizeof(float);
        char f = 'f'; //float
        ofstream ofs2("/share/subimage.bin", ios::out | ios::binary);
        ofs2.write((char*)&nx, sizeof(int));
        ofs2.write((char*)&ny, sizeof(int));
        ofs2.write(&f, 1);
        ofs2.write((char*)firstSubImage, nbytes2);
        ofs2.close();
    }
#endif // BINDEBUG
    
    
    sift_point_feature_list_t *firstObjectPts =
        sift_compute_uncompressed(nx, ny,
                                  AlgorithmParameters::mSiftLevels,
                                  firstSubImage,
                                  AlgorithmParameters::mSiftThreshObject,
                                  AlgorithmParameters::mSiftDoubleImageSize,
                                  AlgorithmParameters::mSiftSigmaInit,
                                  fullFirstImage.utime);

    double xoffset = static_cast<double>(xMinPadded);
    double yoffset = static_cast<double>(yMinPadded);
    for (int i=0; i<firstObjectPts->num; ++i) {
        firstObjectPts->pts[i].col += xoffset;
        firstObjectPts->pts[i].row += yoffset;
    }

    sift_point_feature_list_t *secondObjectPts =
        sift_compute_uncompressed(fullSecondImage.width, fullSecondImage.height,
                                  AlgorithmParameters::mSiftLevels,
                                  (float*)fullSecondImage.data,
                                  AlgorithmParameters::mSiftThreshObject,
                                  AlgorithmParameters::mSiftDoubleImageSize,
                                  AlgorithmParameters::mSiftSigmaInit,
                                  fullSecondImage.utime);

    vector<Descriptor> firstObjectDesc =
        Utilities::makeDescriptorsFromFeatureList(firstObjectPts);
    vector<Descriptor> secondObjectDesc =
        Utilities::makeDescriptorsFromFeatureList(secondObjectPts);
    /* Match features */
    vector<PointMatch> matchesObject;
    Utilities::matchDescriptorsDistance(firstObjectDesc, secondObjectDesc,
                                        static_cast<float>(SIFT_MATCH_THRESHOLD),
                                        matchesObject);

    numMatches = matchesObject.size();
    cout << "Found [" << numMatches << "] object matches." << endl;
    ModelPoints objectPoints(numMatches);
    for (int i=0; i<numMatches; ++i) {
        double px = firstObjectPts->pts[matchesObject[i].mIndex1].col;
        double py = firstObjectPts->pts[matchesObject[i].mIndex1].row;
        Vector3 ray;
        lensModel->pixelToRay(px, py, ray);
        ray.normalize();
        ray *= 5;  // TODO: parameter?
        Vector3 initPoint =  poses[0]->mOrientation*ray + poses[0]->mPosition;
        objectPoints[i] = new ModelPoint();
        objectPoints[i]->mPoint = initPoint;
        objectPoints[i]->mDescriptor = Utilities::copyDescriptor(firstObjectDesc[matchesObject[i].mIndex1]);
    }

    Measurements objectMeas(2*numMatches);
    for (int i=0; i<numMatches; ++i) {
        objectMeas[2*i] = new Measurement();
        objectMeas[2*i]->mImagePoint(0) =
            firstObjectPts->pts[matchesObject[i].mIndex1].col;
        objectMeas[2*i]->mImagePoint(1) =
            firstObjectPts->pts[matchesObject[i].mIndex1].row;
        objectMeas[2*i]->mModelPointIndex = i;
        objectMeas[2*i]->mPoseIndex = 0;
        objectMeas[2*i+1] = new Measurement();
        objectMeas[2*i+1]->mImagePoint(0) =
            secondObjectPts->pts[matchesObject[i].mIndex2].col;
        objectMeas[2*i+1]->mImagePoint(1) =
            secondObjectPts->pts[matchesObject[i].mIndex2].row;
        objectMeas[2*i+1]->mModelPointIndex = i;
        objectMeas[2*i+1]->mPoseIndex = 1;
    }

    /* Set up and run bundle intersection problem */
    Measurements intersectMeasObject(2);
    int numInliers = 0;

#define INLIER_THRESHOLD 5   // TODO: make this a parameter

    for (int i=0; i<numMatches; ++i) {
        intersectMeasObject[0] = objectMeas[2*i];
        intersectMeasObject[1] = objectMeas[2*i+1];
        RobustLM::Result interRes =
            intersection.solve(intersectMeasObject, *objectPoints[i],
                               poses, lensModel);
        double ex = interRes.mErrors(0);
        double ey = interRes.mErrors(1);
        double squaredError1 = ex*ex + ey*ey;
        ex = interRes.mErrors(2);
        ey = interRes.mErrors(3);
        double squaredError2 = ex*ex + ey*ey;
        if ((squaredError1 < INLIER_THRESHOLD*INLIER_THRESHOLD) &&
            (squaredError2 < INLIER_THRESHOLD*INLIER_THRESHOLD)) {
            ++numInliers;
        }
    }
    cout << "Builder: found " << numInliers << " inliers out of " <<
        numMatches << " 3D points" << endl;
#define MATLAB
#ifdef MATLAB
    {
        ofstream ofs3("/share/Intersection.m",ios::app|ios::out);
        ofs3 << "model.object = [ ..." << endl;
        for (int i=0; i<numMatches; ++i) {
            ofs3 << objectPoints[i]->mPoint(0) << ", "
                 << objectPoints[i]->mPoint(1) << ", "
                 << objectPoints[i]->mPoint(2) << "; ..." << endl;
        }
        ofs3 << "];" << endl;
        ofs3.close();
    }
#endif

    // Cull out non-object points using robust stats
    /* 1) Project points, remove points outside original gesture
       2) Find median of z values (vector sort)
       3) Remove all points outside of some threshold (1m?)
    */
    point2d_t somePoint;
    polygon2d_t gesture;
    gesture.nlists = 1;
    pointlist2d_t pix;
    pix.npoints = seg->roi.npoints;
    pix.points = (point2d_t*)calloc(seg->roi.npoints,
                                    sizeof(point2d_t));
    memcpy(pix.points, seg->roi.points,
           sizeof(point2d_t)*seg->roi.npoints);
    gesture.pointlists = &pix;
    
    vector<double> zValues;
    zValues.reserve(objectPoints.size());
    {
        ModelPoints::iterator iter = objectPoints.begin();
        while (iter != objectPoints.end()) {
            Vector3 proj;
            if (false == Utilities::projectPointToImage((*iter)->mPoint,
                                                        poses[0]->mOrientation,
                                                        poses[0]->mPosition,
                                                        lensModel,
                                                        proj)) {
                delete *iter;
                iter = objectPoints.erase(iter);
                continue;
            }
            somePoint.x = proj(0);
            somePoint.y = proj(1);
            if (!geom_point_inside_polygon_2d(&somePoint, &gesture)) {
                delete *iter;
                iter = objectPoints.erase(iter);
            } else {
                zValues.push_back(proj(2));
                ++iter;
            }
        }
    }
    cout << "Removed points outside gesture. [" << objectPoints.size()
         << "] matches remaining." << endl;
    free(pix.points);
    if (zValues.empty()) {
        cerr << "ERROR: No features remaining after gesture constraint." << endl;
        delete builder;
        return NULL;
    }
    sort(zValues.begin(), zValues.end());
    double zMedian = 0;
    int zMid = zValues.size() / 2;
    if (zValues.size() % 2) {
        zMedian = zValues[zMid];
    } else {
        zMedian = (zValues[zMid] + zValues[zMid-1])/2.;
    }
        
    /* Compute 3d object centroid and finish culling by distance from camera */
    Vector3 localCentroid;
    localCentroid.zero();
    {
        ModelPoints::iterator iter = objectPoints.begin();
        while (iter != objectPoints.end()) {
            Vector3 proj;
            Utilities::projectPointToImage((*iter)->mPoint,
                                           poses[0]->mOrientation,
                                           poses[0]->mPosition,
                                           lensModel,
                                           proj);
            if (fabs(proj(2) - zMedian) > OBJECT_CULL_THRESHOLD) {
                delete *iter;
                //TODO remove object measurements too??
                //     and have to re-index too
                iter = objectPoints.erase(iter);
            } else {
                localCentroid += (*iter)->mPoint;
                ++iter;
            }
        }
    }
    cout << "Removed points outside radius OBJECT_CULL_THRESHOLD. ["
         << objectPoints.size() << "] matches remaining." << endl;
    if (objectPoints.empty()) {
        cerr << "ERROR: No features remaining after culling points." << endl;
        delete builder;
        return NULL;
    }
        
    localCentroid /= objectPoints.size();
    model->mObjectPoints = objectPoints;
    model->mObjectMeasurements = objectMeas;

    cout << "Object local centroid: " << endl << localCentroid << endl;

#define LCMGL
#ifdef LCMGL
    IoUtilities::lcmglPublishPoints3d("REACQ_SCENE_PTS",
                                      scenePoints,
                                      1.0f, 0.0f, 0.0f, 6);

    IoUtilities::lcmglPublishPoints2d(string()+"VISION."+
                                      reacq->GetImageChannelName(lensModel->getName())+
                                      ".REACQ_SCENE",
                                      scenePoints,
                                      *poses[0],
                                      lensModel,
                                      1.0f, 0.0f, 0.0f, 4);

    IoUtilities::lcmglPublishPoints3d("REACQ_OBJECT_PTS",
                                      objectPoints,
                                      0.0f, 0.0f, 1.0f, 8);

    IoUtilities::lcmglPublishPoints2d(string()+"VISION."+
                                      reacq->GetImageChannelName(lensModel->getName())+
                                      ".REACQ_OBJECT",
                                      objectPoints,
                                      *poses[0],
                                      lensModel,
                                      0.0f, 0.0f, 1.0f, 7);

    IoUtilities::lcmglPublishGesture(string()+"VISION."+
                                     reacq->GetImageChannelName(lensModel->getName())+
                                     ".REACQ_GESTURE",
                                     seg->roi,
                                     0.0f, 0.0f, 1.0f, 5);
    
    

#endif // LCMGL
    // estimate summoning pose
    double centerPix[2]; // , centerRay[3];
    centerPix[0] = lensModel->getWidth()/2.0;
    centerPix[1] = lensModel->getHeight()/2.0;
    Vector3 summonDirection;
    lensModel->pixelToRay(centerPix[0], centerPix[1], summonDirection);
    Matrix33 rot;
    Vector3 trans;
    Utilities::matrixToRotationTranslation(cw1, rot, trans);
    summonDirection = rot*summonDirection;
    summonDirection(2) = 0.;
    summonDirection.normalize();
    // TODO: make config parameter
    Vector3 summonPosition =
        localCentroid - summonDirection * AlgorithmParameters::mSummoningDistance;
    summonPosition(2) = secondPose->pos[2];

    lcm_t *lcm = bot_lcm_get_global (NULL);
    BotParam *param = bot_param_new_from_server (lcm, 0);
    BotFrames *frames = bot_frames_get_global (lcm, param);
    
    double summonPt[] = {summonPosition(0), summonPosition(1), summonPosition(2)};
#ifdef LCMGL
    {
        string channel("REACQ_SUMMON");
        bot_lcmgl_t *lcmgl = bot_lcmgl_init (lcm, channel.c_str());
        lcmglColor3f(1.f, 0.f, 0.f);
        lcmglPointSize(20);
        lcmglBegin(GL_POINTS);
        lcmglVertex3f(static_cast<float>(localCentroid(0)),
                      static_cast<float>(localCentroid(1)),
                      static_cast<float>(localCentroid(2)));
        lcmglEnd();
        lcmglColor3f(0.f, 1.f, 0.f);
        lcmglPointSize(20);
        lcmglBegin(GL_POINTS);
        lcmglVertex3f(static_cast<float>(summonPosition(0)),
                      static_cast<float>(summonPosition(1)),
                      static_cast<float>(summonPosition(2)));
        lcmglEnd();
        lcmglLineWidth(10);
        lcmglBegin(GL_LINES);
        lcmglVertex3f(static_cast<float>(summonPosition(0)),
                      static_cast<float>(summonPosition(1)),
                      static_cast<float>(summonPosition(2)));
        Vector3 summonVector = summonPosition + summonDirection * 3;
        lcmglVertex3f(static_cast<float>(summonVector(0)),
                      static_cast<float>(summonVector(1)),
                      static_cast<float>(summonVector(2)));
        lcmglEnd();
        bot_lcmgl_switch_buffer(lcmgl);
        cout << "Sent lcmgl 3d summoning points  on [" << channel << "]" << endl;
    }
    
#endif //LCMGL
    
    double pos_global[] = {0, 0, 0};
    bot_frames_transform_vec (frames, "local", "global", summonPt, pos_global);
    double approach_theta = atan2(summonDirection(1), summonDirection(0));

    // set object origin (actually lower left corner rather than centroid)
    //Vector3 transverseDirection;
    //transverseDirection(0) = 0.5*summonDirection(1);
    //transverseDirection(1) = -summonDirection(0);
    //Vector3 objectOrigin = localCentroid - transverseDirection;
    //objectOrigin(2) = summonPosition(2);
    Vector3 objectOrigin = localCentroid;

    for (int i = 0; i < 3; ++i) {
      model->mpObject->pos[i] = objectOrigin(i);
    }
    cout << "Summon position: " << endl << summonPosition << endl;
    cout << "Approach theta: " << approach_theta << endl;
    double rpy[3];
    rpy[0] = 0;
    rpy[1] = 0;
    rpy[2] = approach_theta - M_PI/2;
    bot_roll_pitch_yaw_to_quat(rpy, model->mpObject->orientation);
    //model->mpObject->utime_updated = bot_timestamp_now();
    //model->mpObject->last_updated_by = 0; //ARLCM_PALLET_T_REAQUISITION;
    

    // Estimate the object bounding box
    Vector3 bbox_max, bbox_min;
    bbox_max(0) = -1000;
    bbox_max(1) = -1000;
    bbox_max(2) = -1000;
    bbox_min(0) = 1000;
    bbox_min(1) = 1000;
    bbox_min(2) = 1000;

    BotTrans localToObject;
    bot_trans_set_from_quat_trans (&localToObject, model->mpObject->orientation, model->mpObject->pos);
    bot_trans_invert (&localToObject);
    {
        ModelPoints::iterator iter = objectPoints.begin();
        while (iter != objectPoints.end()) {

            double xyz_local[] = {static_cast<double>((*iter)->mPoint(0)), 
                                  static_cast<double>((*iter)->mPoint(1)), static_cast<double>((*iter)->mPoint(2))};

            double xyz_body[3];
            bot_trans_apply_vec (&localToObject, xyz_local, xyz_body);

            for (int i=0; i<3; i++) {
                if (xyz_body[i] < bbox_min(i))
                    bbox_min(i) = xyz_body[i];

                if (xyz_body[i] > bbox_max(i))
                    bbox_max(i) = xyz_body[i];
            }
            ++iter;
        }
    }

    cout << "Object bbox_min: " << endl << bbox_min << endl;
    cout << "Object bbox_max: " << endl << bbox_max << endl;

    for (int i=0; i<3; i++) {
        model->mpObject->bbox_min[i] = bbox_min(i);
        model->mpObject->bbox_max[i] = bbox_max(i);
    }

    builder->mpReacq->AddModel(model, true, newObject);
    cout << "Added model for id [" << seg->object_id
         << "] to reacquisition object." << endl;
    cout << "Updated object in world model." << endl;
    /* Cleanup */
    bot_core_image_t_destroy(firstImage.image);
    bot_core_pose_t_destroy(firstImage.pose);
    bot_core_image_t_destroy(secondImage.image);
    bot_core_pose_t_destroy(secondImage.pose);
    for (unsigned i=0; i<imagePoses.size(); ++i) {
        bot_core_pose_t_destroy(imagePoses[i]);
    }
    Utilities::freeAlignedImage(&fullFirstImage);
    Utilities::freeAlignedImage(&fullSecondImage);
    // TODO cleanup verification
    delete builder;
    return NULL;
#endif
}

