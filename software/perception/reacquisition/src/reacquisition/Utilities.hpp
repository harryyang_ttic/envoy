#ifndef _AGILE_REACQ_UTILITIES_H_
#define _AGILE_REACQ_UTILITIES_H_

#include "Descriptor.hpp"
#include "PointMatch.hpp"
#include "GeomTypes.hpp"
#include "LensModel.hpp"

#include <lcmtypes/sift_point_feature_list_t.h>
#include <lcmtypes/erlcm_point2d_list_t.h>

#include <bot_core/bot_core.h>
#include <image_utils/jpeg.h>

#include <vector>

namespace reacq {

class Utilities {

public:
    static void matchDescriptorsDistance(const std::vector<Descriptor>& iA,
                                         const std::vector<Descriptor>& iB,
                                         const float iThresh,
                                         std::vector<PointMatch>& oMatches);

    static void matchDescriptorsDot(const std::vector<Descriptor>& iA,
                                    const std::vector<Descriptor>& iB,
                                    const float iThresh,
                                    std::vector<PointMatch>& oMatches);

    static std::vector<Descriptor>
    makeDescriptorsFromFeatureList(const sift_point_feature_list_t *iList);

    static Descriptor
    copyDescriptor(const Descriptor &iDesc);
    
    static void rollPitchYawToRotationMatrix(const double iRPY[3],
                                             Matrix33& oMatrix);

    static void rotationMatrixToRollPitchYaw(const Matrix33& iMatrix,
                                             double oRPY[3]);

    static void bboxFromRoi(const erlcm_point2d_list_t &iRoi,
                            int32_t &oXmin, int32_t &oYmin,
                            int32_t &oXmax, int32_t &oYmax);

    static Matrix44 matrixFromRotationTranslation(const Matrix33& iRot,
                                                  const Vector3& iPos);
    static void matrixToRotationTranslation(const Matrix44& iMatx,
                                            Matrix33& oRot,
                                            Vector3& oTrans);

    static Matrix44 matrixFromArray(const double iArray[16]);

    static bool projectPointToImage(const Vector3& iPoint,
                                    const Matrix33& iRot,
                                    const Vector3& iTrans,
                                    const LensModel* iLens,
                                    Vector3& oResult);

    static void allocateMultiScaleBuffers(const bot_core_image_t* iImage, //input image
                                          bot_core_image_t* oFullImage, //output full size gray in float
                                          bot_core_image_t* oResizedImage, //output gray resized in float
                                          bot_core_image_t* oRgbBuf, //temporary full size rgb in byte
                                          bot_core_image_t* oGrayBuf, //temporary full size gray in byte
                                          double xScale, double yScale);
    
    static void decompressToMultiScale(bot_core_image_t* iImage, //input image
                                       bot_core_image_t* oFullImage, //output full size gray in float
                                       bot_core_image_t* oResizedImage, //output gray resized in float
                                       bot_core_image_t* oRgbBuf, //temporary full size rgb in byte
                                       bot_core_image_t* oGrayBuf); //temporary full size gray in byte

    static void duplicateAlignedFloatImage(const bot_core_image_t* iImage,
                                           bot_core_image_t* oImage);

    static void freeAlignedImage(bot_core_image_t* iImage);

    static void scaleSiftFeatures(sift_point_feature_list_t *ioList,
                                  double iScaleX, double iScaleY);

    static uint64_t getUniqueId (void);
};

}

#endif // _AGILE_REACQ_UTILITIES_H_
