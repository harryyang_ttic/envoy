#ifndef _AGILE_REACQ_OBJECTMODELBUILDER_H_
#define _AGILE_REACQ_OBJECTMODELBUILDER_H_

#include <lcmtypes/erlcm_reacquisition_segmentation_t.h>
#include <sift/sift.hpp>
#include <geom_utils/geometry.h>

#include "Reacquisition.hpp"

namespace reacq {

class ObjectModelBuilder {

public:

    ObjectModelBuilder(Reacquisition *iReacq,
                       const  erlcm_reacquisition_segmentation_t *iSeg);
    
    ~ObjectModelBuilder();

    static void *InnerRun (void *user);

private:

    Reacquisition *mpReacq;
    erlcm_reacquisition_segmentation_t *mpSeg;

}; //class ObjectModelBuilder

} //namespace reacq


#endif // _AGILE_REACQ_OBJECTMODELBUILDER_H_
