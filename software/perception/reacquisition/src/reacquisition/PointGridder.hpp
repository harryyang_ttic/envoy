#ifndef _AGILE_REACQ_POINTGRIDDER_H_
#define _AGILE_REACQ_POINTGRIDDER_H_

#include <vector>
#include <deque>
#include "GeomTypes.hpp"

namespace reacq {

class PointGridder {

public:
    struct PointStruct {
        Vector2 mPoint;
        double mScore;
        int mIndex;
    };

public:
    PointGridder() {
        setImageSize(0,0);
        setNumBins(16,16);
        setDescending(false);
    }

    void setImageSize(const int iWidth, const int iHeight) {
        mImageWidth = iWidth;
        mImageHeight = iHeight;
    }

    void setNumBins(const int iNumX, const int iNumY) {
        mNumBinsX = iNumX;
        mNumBinsY = iNumY;
    }

    void setDescending(const bool iDescending) {
        mDescending = iDescending;
    }

    bool binPoints(const std::vector<Vector2>& iPoints,
                   const std::vector<double>& iScores,
                   std::vector<int>& oIndices);

    static bool compareAscending(const PointStruct& iA,
                                 const PointStruct& iB) {
        return iA.mScore < iB.mScore;
    }

    static bool compareDescending(const PointStruct& iA,
                                  const PointStruct& iB) {
        return iA.mScore > iB.mScore;
    }

private:
    int mImageWidth;
    int mImageHeight;
    int mNumBinsX;
    int mNumBinsY;
    bool mDescending;
    
    std::vector<std::deque<PointStruct> > mBins;
};

}

#endif
