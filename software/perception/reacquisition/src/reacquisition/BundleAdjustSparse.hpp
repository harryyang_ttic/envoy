#ifndef _AGILE_REACQ_BUNDLEADJUSTSPARSE_H_
#define _AGILE_REACQ_BUNDLEADJUSTSPARSE_H_

#include <algorithm>

#include "Measurement.hpp"
#include "LensModel.hpp"
#include "SmartPointer.hpp"

struct sba_crsm;

namespace reacq {

class BundleAdjustSparse {

public:
    struct Result {
        Result() {
            mIterations = 0;
            mSuccess = false;
        }
        int mIterations;
        bool mSuccess;
        MatrixDynamic mParameters;
        MatrixDynamic mErrors;
        MatrixDynamic mWeights;
        MatrixDynamic mCovariance;
    };

private:
    typedef MatrixStatic<2,6> Matrix26;
    typedef MatrixStatic<2,3> Matrix23;
    typedef MatrixStatic<6,2> Matrix62;
    typedef MatrixStatic<3,2> Matrix32;
    typedef MatrixStatic<6,6> Matrix66;
    typedef MatrixStatic<6,3> Matrix63;
    typedef MatrixStatic<6,1> Vector6;

    struct PoseData {
        Matrix33 mOrientation;
        Vector3 mPosition;
        Matrix66 mCovariance;

        Matrix66 mU;
        Matrix66 mStarU;
        Vector6 mErrorProj;
        Vector6 mRHS;
        Vector6 mDelta;

        typedef SmartPointer<PoseData> Ptr;
    };

    struct PointData {
        Vector3 mPoint;
        Matrix33 mCovariance;

        Matrix33 mV;
        Matrix33 mInvStarV; 
        Vector3 mErrorProj;
        Vector3 mRHS;
        Vector3 mDelta;

        typedef SmartPointer<PointData> Ptr;
    };

    struct MeasurementData {
        Vector2 mPixel;
        int mPoseIndex;
        int mPointIndex;

        Matrix26 mA;
        Matrix23 mB;
        Matrix63 mW;
        Matrix63 mY;
        Vector2 mError;
        Vector2 mWeight; // TODO: scalar instead?

        typedef SmartPointer<MeasurementData> Ptr;
    };


    struct RobustNop {
        RobustNop(MatrixDynamic& iE2, double iMinSigma) { }
        inline double computeWeight(double iE2)         { return 1; }
        inline double computeCost(double iE2)           { return iE2; }
    };

    struct RobustTukey {
        RobustTukey(MatrixDynamic& iE2, double iMinSigma) {
            std::vector<double> err2(iE2.getNumElements());
            for (int i = 0; i < err2.size(); ++i) {
                err2[i] = iE2(i);
            }
            sort(err2.begin(), err2.end());

            double medianVal;
            if ((err2.size() % 2) == 0) {
                medianVal = 0.5*(err2[err2.size()/2] + err2[(err2.size()-1)/2]);
            }
            else {
                medianVal = err2[(err2.size()-1)/2];
            }
            mSigma2 = 1.5*sqrt(medianVal)*4.8;
            mSigma2 = std::max(mSigma2, iMinSigma);
            mSigma2 *= mSigma2;
        }

        inline double computeWeight(double iE2) {
            return (iE2>mSigma2 ? 0 : 1-iE2/mSigma2);
        }

        inline double computeCost(double iE2) {
            if (iE2>mSigma2) {
                return 1;
            }
            else {
                double w = 1-iE2/mSigma2;
                return 1 - w*w*w;
            }
        }


    private:
        double mSigma2;
    };

public:
    enum RobustKernel {
        RobustKernelNop,
        RobustKernelTukey
    };



public: 

    BundleAdjustSparse();
    ~BundleAdjustSparse();

    // Set algorithm parameters
    void setRobustKernel(const RobustKernel iKernel) {
        mRobustKernel = iKernel;
    }
    void setMaxIterations(const int iIters) {
        mMaxIterations = iIters;
    }
    void setMinCostChangeFactor(const double iFactor) {
        mMinCostChangeFactor = iFactor;
    }
    void setMinParameterChangeFactor(const double iFactor) {
        mMinParameterChangeFactor = iFactor;
    }
    void setVerbose(const bool iVerbose) {
        mVerbose = iVerbose;
    }

    Result solve(const Measurements &iMeasurements,
                 Poses &ioPoses,
                 ModelPoints &ioModelPoints,
                 const LensModel *iLensModel);

private:
    template<typename RobustT>
    Result solveInternal();

    void createStructures(const Measurements& iMeasurements,
                          const Poses& iPoses,
                          const ModelPoints& iModelPoints,
                          const LensModel& iLensModel);

    template<typename RobustT>
    void initializeStructures(RobustT& iWeighting);

    void completeStructures();
    MatrixDynamic computeUpdates();
    void computeCovariances();

    MatrixDynamic computeError(const MatrixDynamic& iX) const;
    Vector2 computeError(const Matrix33& iOrientation, const Vector3& iPosition,
                         const Vector3& iPoint, const Vector2& iPixel) const;

    Matrix26 computeJacobianPose(const Vector3& iX,
                                 const Matrix33& iOrientation,
                                 const Vector3& iPosition,
                                 const Vector3& iPoint,
                                 const Vector2& iPixel,
                                 const Vector2& iError) const;

    Matrix23 computeJacobianPoint(const Matrix33& iOrientation,
                                  const Vector3& iPosition,
                                  const Vector3& iPoint,
                                  const Vector2& iPixel,
                                  const Vector2& iError) const;


private:
    // Algorithm parameters
    RobustKernel mRobustKernel;
    int mMaxIterations;
    double mMinCostChangeFactor;
    double mMinParameterChangeFactor;
    bool mVerbose;

    // Data for run
    LensModel mLensModel;
    std::vector<PoseData::Ptr> mPoseData;
    std::vector<PointData::Ptr> mPointData;
    std::vector<MeasurementData::Ptr> mMeasurementData;
    std::vector<std::vector<MeasurementData::Ptr> > mMeasurementMap;

    // Internal state variables
    double mLambda;
    MatrixDynamic mX;
    MatrixDynamic mS;

};

}

#endif // _AGILE_REACQ_BUNDLEADJUSTSPARSE_H_
