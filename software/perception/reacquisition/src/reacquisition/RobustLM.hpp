#ifndef _AGILE_REACQ_ROBUSTLM_H_
#define _AGILE_REACQ_ROBUSTLM_H_

#include "MatrixDynamic.hpp"
#include "RobustWeighting.hpp"

namespace reacq {

class RobustLM {

public:
    class Problem {

    private:
        MatrixDynamic mParameters;

    public:
        Problem();
        virtual ~Problem() {};

        void setParameters(const MatrixDynamic& iParams) {
            mParameters = iParams;
        }

        MatrixDynamic getParameters() const {
            return mParameters;
        }

        virtual MatrixDynamic
        computeJacobian(const MatrixDynamic& iParams) const;

        virtual MatrixDynamic
        computeErrors(const MatrixDynamic& iParams) const = 0;
    };

    class Result {
    public:
        Result() {
            mIterations = 0;
            mSuccess = false;
        }
        int mIterations;
        bool mSuccess;
        MatrixDynamic mParameters;
        MatrixDynamic mErrors;
        MatrixDynamic mWeights;
        MatrixDynamic mCovariance;
    };


public:
    RobustLM();
    ~RobustLM();

    void setMaxIterations(const int iIter) {
        mMaxIterations = iIter;
    }
    void setMinCostChangeFactor(const double iFactor) {
        mMinCostChangeFactor = iFactor;
    }
    void setMinParameterChangeFactor(const double iFactor) {
        mMinParameterChangeFactor = iFactor;
    }
    void setComputeCovariance(const bool iVal) {
        mComputeCovariance = iVal;
    }
    void setVerbose(const bool iVerbose) {
        mVerbose = iVerbose;
    }

    Result solve(Problem& iProblem);
    Result solve(Problem& iProblem, RobustWeighting& iWeighting);

private:
    int mMaxIterations;
    double mMinCostChangeFactor;
    double mMinParameterChangeFactor;
    bool mComputeCovariance;
    bool mVerbose;
};

}

#endif //_AGILE_REACQ_ROBUSTLM_H_
