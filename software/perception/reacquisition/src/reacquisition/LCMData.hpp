#ifndef _AGILE_REACQ_LCMDATA_H_
#define _AGILE_REACQ_LCMDATA_H_

namespace reacq {

template<typename T>
class LCMData;

}

#define _MakeLCMData(T)\
namespace reacq {\
\
template<>\
class LCMData<T> {\
public:\
    LCMData() {\
        mData = NULL;\
    }\
\
    LCMData(const T& iData) {\
        mData = NULL;\
        *this = iData;\
    }\
\
    LCMData(const T* iData) {\
        mData = NULL;\
        *this = iData;\
    }\
\
    LCMData(const LCMData<T>& iData) {\
        mData = NULL;\
        *this = iData;\
    }\
\
    ~LCMData() {\
        destroy();\
    }\
\
    LCMData& operator=(const T& iData) {\
        copyFrom(&iData);\
        return *this;\
    }\
\
    LCMData& operator=(const T* iData) {\
        copyFrom(iData);\
        return *this;\
    }\
\
    LCMData& operator=(const LCMData<T>& iData) {\
        copyFrom(iData.mData);\
        return *this;\
    }\
\
    T* operator()() {\
        return mData;\
    }\
\
public:\
    typedef T##_subscription_t Subscription;\
    typedef T##_handler_t HandlerFunc;\
    typedef Subscription* (*SubscribeFunc)(lcm_t*, const char*,\
                                          HandlerFunc, void*);\
    typedef int (*UnsubscribeFunc)(lcm_t*, Subscription*);\
\
public:\
    static SubscribeFunc subscribe;\
    static UnsubscribeFunc unsubscribe;\
\
private:\
    void destroy() {\
        if (NULL != mData) {\
            T##_destroy(mData);\
        }\
    }\
\
    void copyFrom(const T* iData) {\
        destroy();\
        mData = T##_copy(iData);\
    }\
\
private:\
    T* mData;\
};\
\
LCMData<T>::SubscribeFunc LCMData<T>::subscribe = T##_subscribe;\
LCMData<T>::UnsubscribeFunc LCMData<T>::unsubscribe = T##_unsubscribe;\
}\

/*
namespace reacq {

template<>
class LCMData<bot_core_pose_t> {
public:
    LCMData() {
        mData = NULL;
    }

    LCMData(const bot_core_pose_t& iData) {
        mData = NULL;
        *this = iData;
    }

    LCMData(const bot_core_pose_t* iData) {
        mData = NULL;
        *this = iData;
    }

    LCMData(const LCMData<bot_core_pose_t>& iData) {
        mData = NULL;
        *this = iData;
    }

    ~LCMData() {
        destroy();
    }

    LCMData& operator=(const bot_core_pose_t& iData) {
        copyFrom(&iData);
        return *this;
    }

    LCMData& operator=(const bot_core_pose_t* iData) {
        copyFrom(iData);
        return *this;
    }

    LCMData& operator=(const LCMData<bot_core_pose_t>& iData) {
        copyFrom(iData.mData);
        return *this;
    }

    bot_core_pose_t* operator()() {
        return mData;
    }

public:
    typedef bot_core_pose_t_subscription_t Subscription;
    typedef bot_core_pose_t_handler_t HandlerFunc;
    typedef Subscription* (*SubscribeFunc)(lcm_t*, const char*,
                                          HandlerFunc, void*);
    typedef int (*UnsubscribeFunc)(lcm_t*, Subscription*);

public:
    static SubscribeFunc subscribe;
    static UnsubscribeFunc unsubscribe;

private:
    void destroy() {
        if (NULL != mData) {
            bot_core_pose_t_destroy(mData);
        }
    }

    void copyFrom(const bot_core_pose_t* iData) {
        destroy();
        mData = bot_core_pose_t_copy(iData);
    }

private:
    bot_core_pose_t* mData;
};

LCMData<bot_core_pose_t>::SubscribeFunc LCMData<bot_core_pose_t>::subscribe = bot_core_pose_t_subscribe;
LCMData<bot_core_pose_t>::UnsubscribeFunc LCMData<bot_core_pose_t>::unsubscribe = bot_core_pose_t_unsubscribe;

    LCMData<bot_core_pose_t>::Subscription* foo;

}
*/

#endif //_AGILE_REACQ_LCMDATA_H_
