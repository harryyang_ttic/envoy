#ifndef _AGILE_REACQ_IOUTILITIES_H_
#define _AGILE_REACQ_IOUTILITIES_H_

#include "Descriptor.hpp"
#include "GeomTypes.hpp"
#include "ObjectModel.hpp"
#include "Reacquisition.hpp"
#include "LensModel.hpp"

#include <bot_core/bot_core.h>
#include <bot_lcmgl_client/lcmgl.h>

#include <lcmtypes/erlcm_reacquisition_segmentation_t.h>
#include <lcmtypes/sift_point_feature_list_t.h>
#include <lcmtypes/erlcm_point2d_list_t.h>

#include <string>
#include <fstream>
#include <vector>
#include <map>

namespace reacq {

class IoUtilities {

public:

    static std::string camtransToMatlabString(const BotCamTrans *iCamtrans);

    static std::string lensModelToMatlabString(const LensModel *iLensModel);

    static std::string pointFeaturesToMatlabString(const std::string &iVarName,
                                                   const sift_point_feature_list_t *iFeatures);

    static std::string measurementsToMatlabString(const std::string &iVarName,
                                                  const Measurements &iMeas);

    static std::string objectToMatlabString(const std::string &iVarName,
                                            const om_object_t *iObject);
    
    static std::string objectModelToMatlabString(const std::string &iVarName,
                                                 const int iIndex,
                                                 const int64_t iId,
                                                 const ObjectModel &iModel);

    static std::string pointlistToMatlabString(const std::string &iVarName,
                                               const erlcm_point2d_list_t &iPts,
                                               const int64_t iImageUtime,
                                               const std::string &iCameraName);
                                               
    
    static void loadModelPointsFromMatlabFile(std::ifstream &ifs,
                                              ModelPoints &oPts);

    static void loadObjectModelsFromMatlabFile(const std::string &iFilename,
                                               Reacquisition &iReacq);

    static void loadMeasurementsFromMatlabFile(std::ifstream &ifs,
                                               Measurements &oMeasurements);

    static void loadPosesFromMatlabFile(std::ifstream &ifs,
                                        Poses &oPoses);
    
    static void loadGestureFromMatlabFile(std::ifstream &ifs,
                                          erlcm_reacquisition_segmentation_t* &oGesture);

    static void loadObjectFromMatlabFile(std::ifstream &ifs,
                                         om_object_t* &oObject);

    /* Do not change name of bot_lcmgl_t variable name -
       it's necessary for lcmgl macros */
    static void lcmglPublishGesture(const std::string &iChannel,
                                    const erlcm_point2d_list_t &iRoi,
                                    float iRed = 1.,
                                    float iGreen = 1.,
                                    float iBlue = 1.,
                                    float iLineWidth = 1.,
                                    float iXscale = 1.,
                                    float iYscale = 1.);

    /* Do not change name of bot_lcmgl_t variable name -
       it's necessary for lcmgl macros */
    static void lcmglPublishPoints2d(const std::string &iChannel,
                                     const ModelPoints &iPoints,
                                     const Pose &iPose,
                                     const LensModel *iLensModel,
                                     float iRed = 1.,
                                     float iGreen = 1.,
                                     float iBlue = 1.,
                                     float iPointSize = 1.,
                                     float iXscale = 1.,
                                     float iYscale = 1.);

    /* Do not change name of bot_lcmgl_t variable name -
       it's necessary for lcmgl macros */
    static void lcmglPublishPoints3d(const std::string &iChannel,
                                     const ModelPoints &iPoints,
                                     float iRed = 1.,
                                     float iGreen = 1.,
                                     float iBlue = 1.,
                                     float iPointSize = 1.);
    
    static std::string measurementsToString(const Measurements &iMeasurements);
    
    static std::string modelPointsToString(const ModelPoints &iModelPoints);

    static std::string posesToString(const Poses &iPoses);
    

    static std::string
    reacquisitionSegmentationToString(const std::string &iVarName,
                                      const erlcm_reacquisition_segmentation_t *iCommand);
};

} //namespace reacq

#endif // _AGILE_REACQ_IOUTILITIES_H_
