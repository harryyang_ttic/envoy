#ifndef _reacq_GeomTypes_h_
#define _reacq_GeomTypes_h_

#include "MatrixStatic.hpp"

namespace reacq {
  typedef MatrixStatic<2,2> Matrix22;
  typedef MatrixStatic<3,3> Matrix33;
  typedef MatrixStatic<3,4> Matrix34;
  typedef MatrixStatic<4,4> Matrix44;

  typedef MatrixStatic<2,1> Vector2;
  typedef MatrixStatic<3,1> Vector3;
  typedef MatrixStatic<4,1> Vector4;
}

#endif
