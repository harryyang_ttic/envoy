#ifndef _AGILE_REACQ_FEATUREBUFFER_H_
#define _AGILE_REACQ_FEATUREBUFFER_H_

#include <string>
#include <deque>

#include <bot_core/bot_core.h>
#include <lcmtypes/sift_point_feature_list_t.h>
#include <vector>

namespace reacq {

typedef struct _FeatureWithPose {

    sift_point_feature_list_t *feature;
    bot_core_pose_t *pose;

} FeatureWithPose;
    
class FeatureBuffer {

  public:
    
    FeatureBuffer(const std::string &iPoseChannel,
                  int32_t iPoseCapacity,                
                  const std::string &iFeatureChannel,
                  int32_t iFeatureCapacity,
                  const double iDeltaPositionThreshold,
                  const double iDeltaThetaDegreesThreshold);
    
    ~FeatureBuffer();

    /* Caller is responsible for cleanup because these are passed by copy */
    FeatureWithPose GetFeatureByIndex(int iIndex);
    FeatureWithPose GetFeatureByTimestamp(int64_t iTimestamp);

    void Clear();
    
    void Start();
    
    void Stop();

    /* Caller is responsible for cleanup because these are passed by copy */
    void GetAllTimestampsAndPoses(std::vector<int64_t> &oTimes,
                                  std::vector<bot_core_pose_t*> &oPoses);


    inline void SetOriginalNumRows(int32_t iRows) { mOriginalRows = iRows; }

    inline void SetOriginalNumCols(int32_t iCols) { mOriginalCols = iCols; }

    static void OnFeature(const lcm_recv_buf_t *iRbuf, const char *iChannel, 
                          const sift_point_feature_list_t *iMsg, void *iUserData);
    
    static void OnPose(const lcm_recv_buf_t *iRbuf, const char *iChannel, 
                       const bot_core_pose_t *iMsg, void *iUserData);
    
    inline bool IsRunning() { return mRunning; }
    
    inline bool FeaturesAreFull() { return (mFeatures.size() == mFeatureCapacity); }
    
    inline bool PosesAreFull() { return (mPoses.size() == mPoseCapacity); }
    
    inline int GetNumPoses() { return mPoses.size(); }

    inline int GetNumFeatures() { return mFeatures.size(); }

    /* Start not thread-safe */
    FeatureWithPose PeekAtFeature(int iIndex);
    
    bot_core_pose_t * PeekAtPose(int iIndex);
    
    void DropFirstPose();

    void AddNewPose(bot_core_pose_t *iPose);
    
    void DropFirstFeatureWithPose();

    void AddNewFeatureWithPose(sift_point_feature_list_t *iFeature,
                             bot_core_pose_t *iPose);

    /* End not thread-safe */

    inline void Lock() { g_mutex_lock(mMutex); }
    inline void Unlock() { g_mutex_unlock(mMutex); }

    inline double GetDeltaPositionThreshold() { return mDeltaPositionThreshold; }
    inline double GetDeltaThetaDegreesThreshold() { return mDeltaThetaDegreesThreshold; }

    inline std::string GetFeatureChannelName() { return mFeatureChannel; }
    inline const std::string GetFeatureChannelName() const { return mFeatureChannel; }
      
        
  private:

    std::string mPoseChannel;
    int32_t mPoseCapacity;
    int32_t mOriginalRows, mOriginalCols;
    double mRowScale, mColScale;

    std::string mFeatureChannel;
    int32_t mFeatureCapacity;
    double mDeltaPositionThreshold;
    double mDeltaThetaDegreesThreshold;
    std::deque<FeatureWithPose> mFeatures;
    std::deque<bot_core_pose_t*> mPoses;

    lcm_t *mLcm;
    bot_core_pose_t_subscription_t *mPoseSub;
    sift_point_feature_list_t_subscription_t *mFeatureSub;

    GMutex *mMutex;
    GCond *mCondition;

    bool mRunning;

    bool mDebug;
    int mVerbose;
    
}; // class FeatureBuffer


} //namespace reacq


#endif // _AGILE_REACQ_FEATUREBUFFER_H_
