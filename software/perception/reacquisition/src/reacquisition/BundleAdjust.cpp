#include "BundleAdjust.hpp"

#include "Utilities.hpp"

using namespace reacq;
using namespace std;

BundleAdjust::BundleAdjust() {

}

BundleAdjust::Problem::Problem(Measurements &iMeasurements,
                               Poses& iPoses,
                               ModelPoints& iModelPoints,
                               LensModel *iLensModel) :
    mMeasurements(iMeasurements),
    mPoses(iPoses),
    mModelPoints(iModelPoints),
    mLensModel(iLensModel) {
    
}

MatrixDynamic
BundleAdjust::Problem::computeErrors(const MatrixDynamic& iParams) const {
    MatrixDynamic errors(2*mMeasurements.size());

    // Grab parameters from vector
    vector<Matrix33> rotations(mPoses.size());
    vector<Vector3> translations(mPoses.size());
    vector<Vector3> points3d(mModelPoints.size());
    for (int i = 0; i < mPoses.size(); ++i) {
        double rpy[3];
        rpy[0] = iParams(6*i);
        rpy[1] = iParams(6*i+1);
        rpy[2] = iParams(6*i+2);
        Utilities::rollPitchYawToRotationMatrix(rpy, rotations[i]);
        rotations[i] = rotations[i].getTranspose();
        translations[i](0) = iParams(6*i+3);
        translations[i](1) = iParams(6*i+4);
        translations[i](2) = iParams(6*i+5);
    }
    int n = mPoses.size();
    for (int i = 0; i < mModelPoints.size(); ++i) {
        points3d[i](0) = iParams(6*n + 3*i);
        points3d[i](1) = iParams(6*n + 3*i + 1);
        points3d[i](2) = iParams(6*n + 3*i + 2);
    }

    // project each point to compute error
    // TODO: use pointers in measurements rather than indices
    // this will modify points and poses in place; maybe ok
    n = mMeasurements.size();
    for (int i = 0; i < n; ++i) {
        // transform to camera coords
        int poseIndex = mMeasurements[i]->mPoseIndex;
        Vector3 pt = points3d[mMeasurements[i]->mModelPointIndex] -
            translations[poseIndex];
        pt = rotations[poseIndex]*pt;

        // project onto camera
        double pix[2];
        if (!mLensModel->rayToPixel(pt, pix[0], pix[1])) {
            pix[0] = pix[1] = 1.e10;
        }

        // compute difference
        errors(2*i) = pix[0] - mMeasurements[i]->mImagePoint(0);
        errors(2*i+1) = pix[1] - mMeasurements[i]->mImagePoint(1);
    }

    return errors;
}


RobustLM::Result BundleAdjust::solve(Measurements &iMeasurements,
                                     Poses &iPoses,
                                     ModelPoints &iModelPoints,
                                     LensModel *iLensModel) {

    // construct initial parameter vector: poses
    MatrixDynamic initParameters(6*iPoses.size() + 3*iModelPoints.size());
    int n = iPoses.size();
    for (int i = 0; i < n; ++i) {
        double rpy[3];
        Utilities::rotationMatrixToRollPitchYaw(iPoses[i]->mOrientation, rpy);
        initParameters(6*i) = rpy[0];
        initParameters(6*i+1) = rpy[1];
        initParameters(6*i+2) = rpy[2];
        initParameters(6*i+3) = iPoses[i]->mPosition(0);
        initParameters(6*i+4) = iPoses[i]->mPosition(1);
        initParameters(6*i+5) = iPoses[i]->mPosition(2);
    }

    // construct initial parameter vector: points
    for (int i = 0; i < iModelPoints.size(); ++i) {
        initParameters(6*n + 3*i) = iModelPoints[i]->mPoint(0);
        initParameters(6*n + 3*i+1) = iModelPoints[i]->mPoint(1);
        initParameters(6*n + 3*i+2) = iModelPoints[i]->mPoint(2);
    }

    // solve problem
    Problem problem(iMeasurements, iPoses, iModelPoints, iLensModel);
    problem.setParameters(initParameters);
    TukeyWeighting weighting;
    weighting.setMinSigma(0.5);
    RobustLM lm;
    RobustLM::Result result = lm.solve(problem, weighting);

    // populate outputs
    n = iPoses.size();
    for (int i = 0; i < n; ++i) {
        double rpy[3];
        rpy[0] = result.mParameters(6*i);
        rpy[1] = result.mParameters(6*i+1);
        rpy[2] = result.mParameters(6*i+2);
        Utilities::rollPitchYawToRotationMatrix(rpy, iPoses[i]->mOrientation);
        iPoses[i]->mPosition(0) = result.mParameters(6*i+3);
        iPoses[i]->mPosition(1) = result.mParameters(6*i+4);
        iPoses[i]->mPosition(2) = result.mParameters(6*i+5);
    }
    for (int i = 0; i < iModelPoints.size(); ++i) {
        iModelPoints[i]->mPoint(0) = result.mParameters(6*n + 3*i);
        iModelPoints[i]->mPoint(1) = result.mParameters(6*n + 3*i + 1);
        iModelPoints[i]->mPoint(2) = result.mParameters(6*n + 3*i + 2);
    }

    return result;
}


bool BundleAdjust::initPoseFromPair(const std::vector<Vector2>& iPoints1,
                                    const std::vector<Vector2>& iPoints2,
                                    const LensModel* iLensModel,
                                    const double iDistanceThreshold,
                                    Pose& oPose) {
    if (iPoints1.size() != iPoints2.size()) {
        cerr << "BundleAdjust: point lists differ in size" << endl;
        return false;
    }

    // Form cross products for point matches that are sufficiently far apart
    vector<Vector3> crossProducts;
    vector<Vector3> rays1;
    vector<Vector3> rays2;
    for (int i = 0; i < iPoints1.size(); ++i) {
        double distance = (iPoints1[i]-iPoints2[i]).getMagnitude();
        if (distance < iDistanceThreshold) {
            continue;
        }

        double ray1[3], ray2[3];
        if (!iLensModel->pixelToRay(iPoints1[i](0), iPoints1[i](1), ray1[0],
                                    ray1[1], ray1[2])) {
            continue;
        }
        if (!iLensModel->pixelToRay(iPoints2[i](0), iPoints2[i](1), ray2[0],
                                    ray2[1], ray2[2])) {
            continue;
        }
        
        Vector3 cross;
        cross(0) = ray1[1]*ray2[2] - ray1[2]*ray2[1];
        cross(1) = ray1[2]*ray2[0] - ray1[0]*ray2[2];
        cross(2) = ray1[0]*ray2[1] - ray1[1]*ray2[0];
        cross.normalize();
        crossProducts.push_back(cross);

        Vector3 r1, r2;
        for (int k = 0; k < 3; ++k) {
            r1(k) = ray1[k];
            r2(k) = ray2[k];
        }
        rays1.push_back(r1);
        rays2.push_back(r2);
    }

    // Set up linear system to find best direction (least squares)
    if (crossProducts.size() < 2) {
        cerr << "BundleAdjust: too few surviving cross products" << endl;
        return false;
    }
    Matrix33 matx;
    matx.zero();
    for (int i = 0; i < crossProducts.size(); ++i) {
        Vector3 cross = crossProducts[i];
        matx(0,0) += cross(0)*cross(0);
        matx(0,1) += cross(0)*cross(1);
        matx(0,2) += cross(0)*cross(2);
        matx(1,1) += cross(1)*cross(1);
        matx(1,2) += cross(1)*cross(2);
        matx(2,2) += cross(2)*cross(2);
    }
    matx(1,0) = matx(0,1);
    matx(2,0) = matx(0,2);
    matx(2,1) = matx(1,2);

    // solve system
    Matrix33 vects;
    Vector3 vals;
    matx.eig(vects,vals);
    Vector3 direction;
    for (int i = 0; i < 3; ++i) {
        direction(i) = vects(i,0);
    }
    cout << "EIGS: " << vals(0) << " " << vals(1) << " " << vals(2) << endl;

    // correct the sign
    // first point should be closer to direction than second
    int votesToFlipSign = 0;
    for (int i = 0; i < rays1.size(); ++i) {
        double dot1 = direction(0)*rays1[i](0) + direction(1)*rays1[i](1) +
            direction(2)*rays1[i](2);
        double dot2 = direction(0)*rays2[i](0) + direction(1)*rays2[i](1) +
            direction(2)*rays2[i](2);
        if (dot1 < dot2) {
            ++votesToFlipSign;
        }
    }
    if (votesToFlipSign*2 > rays1.size()) {
        direction = direction * -1;
    }

    // populate pose
    oPose.mOrientation.makeIdentity();
    oPose.mPosition = direction;

    return true;
}


bool BundleAdjust::intersectPair(const Vector2& iPoint1, const Vector2& iPoint2,
                                 const Pose& iPose1, const Pose& iPose2,
                                 const LensModel* iLensModel,
                                 Vector3& oIntersection) {
    // pixel to ray
    Vector3 r1, r2;
//     double ray1[3], ray2[3];
    if (!iLensModel->pixelToRay(iPoint1(0), iPoint1(1), r1)) {
        return false;
    }
    if (!iLensModel->pixelToRay(iPoint2(0), iPoint2(1), r2)) {
        return false;
    }

    r1 = iPose1.mOrientation*r1;
    r2 = iPose2.mOrientation*r2;

    // compute intersection
    Matrix33 I;
    I.makeIdentity();
    r1.normalize();
    r2.normalize();
    Matrix33 A = (I - r1*r1.getTranspose()) + (I - r2*r2.getTranspose());
    Vector3 b = (I - r1*r1.getTranspose())*iPose1.mPosition +
        (I - r2*r2.getTranspose())*iPose2.mPosition;
    oIntersection = A.solve(b);
    cout << oIntersection(0) << " " << oIntersection(1) << " " <<
        oIntersection(2) << endl;

    return true;
}
