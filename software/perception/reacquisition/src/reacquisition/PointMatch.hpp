#ifndef _AGILE_REACQ_POINTMATCH_H_
#define _AGILE_REACQ_POINTMATCH_H_

namespace reacq {

class PointMatch {
public:
    int mIndex1;
    int mIndex2;
    double mScore;
};
    
}

#endif // _AGILE_REACQ_POINTMATCH_H_
