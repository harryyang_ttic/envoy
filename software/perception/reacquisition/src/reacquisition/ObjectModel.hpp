#ifndef _AGILE_REACQ_OBJECTMODEL_H_
#define _AGILE_REACQ_OBJECTMODEL_H_

#include <vector>
#include <iostream>
#include <lcmtypes/erlcm_reacquisition_segmentation_t.h>
#include <lcmtypes/om_object_t.h>

#include "LensModel.hpp"
#include "GeomTypes.hpp"
#include "ModelPoint.hpp"
#include "Measurement.hpp"
#include "Pose.hpp"
#include "LensModel.hpp"

namespace reacq {

class ObjectModel {

  public:

    ModelPoints mObjectPoints;
    Measurements mObjectMeasurements;
    ModelPoints mContextPoints;
    Measurements mContextMeasurements;
    Poses mCameraPoses;
    erlcm_reacquisition_segmentation_t *mpGesture;
    LensModel *mpLensModel;
    om_object_t* mpObject;

    ObjectModel();

    //TODO populate destructor to remove all members
    ~ObjectModel();
    
    
}; //class ObjectModel

typedef std::vector<ObjectModel*> ObjectModels;
    
} //namespace reacq


#endif // _AGILE_REACQ_OBJECTMODEL_H_
