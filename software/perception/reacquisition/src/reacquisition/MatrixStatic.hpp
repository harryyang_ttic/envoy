#ifndef _reacq_MatrixStatic_h_
#define _reacq_MatrixStatic_h_

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_permutation.h>
#include <cstring>
#include <cmath>
#include <iostream>

namespace reacq {

template<int R, int C>
class MatrixStatic;

template <int R, int C>
struct MatrixStaticView {
private:
  MatrixStaticView(const double* iData, const int iStride) {
    mData = (double*)iData;
    mStride = iStride;
  }

public:
  MatrixStaticView<R,C>& operator=(const MatrixStaticView<R,C>& iView) {
    double* outPtr = mData;
    const double* inPtr = iView.mData;
    for (int i = 0; i < R; ++i, outPtr+=mStride, inPtr+=iView.mStride)
      memcpy(outPtr, inPtr, C*sizeof(double));
    return *this;
  }

  MatrixStaticView<R,C>& operator=(const MatrixStatic<R,C>& iMat) {
    double* outPtr = mData;
    const double* inPtr = iMat.mData;
    for (int i = 0; i < R; ++i, outPtr+=mStride, inPtr+=C)
      memcpy(outPtr, inPtr, C*sizeof(double));
    return *this;
  }

private:
  template <int M, int N>
  friend class MatrixStatic;

private:
  int mStride;
  double* mData;
};


template <int R, int C>
class MatrixStatic {
public:

  MatrixStatic() {
    mView = gsl_matrix_view_array(mData,R,C);
    mLength = R*C;
  }

  MatrixStatic(const MatrixStatic<R,C>& iMat) {
    mView = gsl_matrix_view_array(mData,R,C);
    mLength = R*C;
    *this = iMat;
  }

  MatrixStatic(const MatrixStaticView<R,C>& iView) {
    mView = gsl_matrix_view_array(mData,R,C);
    mLength = R*C;
    *this = iView;
  }

  ~MatrixStatic() {}

  MatrixStatic<R,C>& operator=(const MatrixStatic<R,C>& iMat) {
    if (this != &iMat) {
      memcpy(mData, iMat.mData, R*C*sizeof(double));
      mLength = iMat.mLength;
    }
    return *this;
  }

  MatrixStatic<R,C> operator-() const {
    MatrixStatic<R,C> ret;
    for (int i = 0; i < mLength; ++i)
      ret.mData[i] = -mData[i];
    return ret;
  }

  MatrixStatic<R,C> operator+(const MatrixStatic<R,C>& iMat) const {
    MatrixStatic<R,C> ret;
    for (int i = 0; i < mLength; ++i)
      ret.mData[i] = mData[i] + iMat.mData[i];
    return ret;
  }

  void operator+=(const MatrixStatic<R,C>& iMat) {
    *this = *this + iMat;
  }

  MatrixStatic<R,C> operator-(const MatrixStatic<R,C>& iMat) const {
    MatrixStatic<R,C> ret;
    for (int i = 0; i < mLength; ++i)
      ret.mData[i] = mData[i] - iMat.mData[i];
    return ret;
  }

  void operator-=(const MatrixStatic<R,C>& iMat) {
    *this = *this - iMat;
  }

  template <int C2>
  MatrixStatic<R,C2> operator*(const MatrixStatic<C,C2>& iMat) const {
    MatrixStatic<R,C2> ret;
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1,
                   &mView.matrix, &iMat.mView.matrix,
                   0, &ret.mView.matrix);
    return ret;
  }

  void operator*=(const MatrixStatic<C,C>& iMat) {
    *this = *this * iMat;
  }

  MatrixStatic<R,C> operator*(const double iVal) const {
    MatrixStatic<R,C> ret;
    for (int i = 0; i < mLength; ++i)
      ret.mData[i] = mData[i]*iVal;
    return ret;
  }

  void operator*=(const double iVal) {
    *this = *this * iVal;
  }

  MatrixStatic<R,C> operator/(const double iVal) const {
    return (*this * (1/iVal));
  }

  void operator/=(const double iVal) {
    *this *= (1/iVal);
  }

  double& operator()(const int iRow, const int iCol=0) {
    return mData[C*iRow + iCol];
  }

  double operator()(const int iRow, const int iCol=0) const {
    return mData[C*iRow + iCol];
  }

  double getMagnitude() const {
    double mag = 0;
    for (int i = 0; i < mLength; ++i)
      mag += mData[i]*mData[i];
    return (double)sqrt(mag);
  }

  void normalize() {
    double mag = getMagnitude();
    double invMag = 1.0/(mag+1e-10);
    for (int i = 0; i < mLength; ++i) {
      mData[i] *= invMag;
    }
  }


  MatrixStatic<C,R> getTranspose() const {
    MatrixStatic<C,R> ret;
    gsl_matrix_transpose_memcpy(&ret.mView.matrix, &mView.matrix);
    return ret;
  }

  double getTrace() const {
    int size = std::min(R,C);
    double trace = 0;
    for (int i = 0; i < size; ++i)
      trace += mData[i*(C+1)];
    return trace;
  }

  void zero() {
    bzero(mData, R*C*sizeof(double));
  }

  template <int M, int N>
  MatrixStaticView<M,N> getView(const int iStartRow=0,
                                const int iStartCol=0) const {
    return MatrixStaticView<M,N>(mData + iStartRow*C + iStartCol, C);
  }

  MatrixStatic<R,C>& operator=(const MatrixStaticView<R,C>& iView) {
    getView<R,C>() = iView;
    return *this;
  }


  // TODO: specialize for square matrices
  bool makeIdentity() {
    zero();
    if (R != C)
      return false;
    int n = ((R<C) ? R : C);
    for (int i = 0; i < n; ++i)
      (*this)(i,i) = 1;
    return true;
  }

  MatrixStatic<R,R> getInverse() const {
    // TODO: compile time check for square size?

    gsl_permutation perm;
    size_t permData[R];
    perm.size = R;
    perm.data = permData;

    MatrixStatic<R,R> tmp(*this);
    int s;
    gsl_linalg_LU_decomp(&tmp.mView.matrix, &perm, &s);

    MatrixStatic<R,R> inv;
    gsl_linalg_LU_invert(&tmp.mView.matrix, &perm, &inv.mView.matrix);
    return inv;
  }

  void invert() {
    *this = getInverse();
  }

  MatrixStatic<R,R> getPseudoInverse() const {
    MatrixStatic<R,R> u, s, v;
    svd(u,s,v);
    for (int i = 0; i < R; ++i) {
      s(i,i) = (s(i,i)<1e-12) ? 0 : 1/s(i,i);
    }
    return (u*s*v.getTranspose());
  }

  bool qr(MatrixStatic<R,R>& oQ, MatrixStatic<R,R>& oR) const {
    MatrixStatic<R,1> tau;
    MatrixStatic<R,R> temp(*this);
    gsl_vector_view tauView = gsl_vector_view_array(tau.mData, R);
    gsl_linalg_QR_decomp(&temp.mView.matrix, &tauView.vector);
    gsl_linalg_QR_unpack(&temp.mView.matrix, &tauView.vector, 
                         &oQ.mView.matrix, &oR.mView.matrix);
    return true;
  }

  bool eig(MatrixStatic<R,R>& oVects, MatrixStatic<R,1>& oVals) const {
    // TODO: there must be a compile-time check we can do here...
    if (R != C)
      return false;

    // TODO: try to avoid dynamic allocation of workspace here...
    MatrixStatic<R,R> temp(*this);
    gsl_vector_view vectView = gsl_vector_view_array(oVals.mData, R);
    gsl_eigen_symmv_workspace* workspace = gsl_eigen_symmv_alloc(R);
    gsl_eigen_symmv(&temp.mView.matrix, &vectView.vector,
                    &oVects.mView.matrix, workspace);
    gsl_eigen_symmv_free(workspace);
    gsl_eigen_symmv_sort(&vectView.vector, &oVects.mView.matrix,
                         GSL_EIGEN_SORT_ABS_ASC);
    return true;
  }

  bool svd(MatrixStatic<R,C>& oU, MatrixStatic<C,C>& oS,
           MatrixStatic<C,C>& oV) const {
    MatrixStatic<C,1> work;
    MatrixStatic<C,1> s;
    gsl_vector_view workView = gsl_vector_view_array(work.mData, C);
    gsl_vector_view sView = gsl_vector_view_array(s.mData, C);
    oU = *this;

    gsl_linalg_SV_decomp(&oU.mView.matrix, &oV.mView.matrix,
                         &sView.vector, &workView.vector);
    oS.zero();
    for (int i = 0; i < C; ++i)
      oS(i,i) = s(i,0);

    return true;
  }

  MatrixStatic<R,1> solve(const MatrixStatic<R,1> &iB) const {
    gsl_permutation perm;
    size_t permData[R];
    perm.size = R;
    perm.data = permData;
    
    MatrixStatic<R,R> tmp(*this);
    int s;
    gsl_linalg_LU_decomp(&tmp.mView.matrix, &perm, &s);

    double bData[R];
    memcpy(bData, iB.mData, R*sizeof(double));
    
    gsl_vector bVec;
    bVec.size = R;
    bVec.stride = 1;
    bVec.data = bData;
    bVec.block = NULL;
    bVec.owner = 0;

    double xData[R];
    gsl_vector xVec;
    xVec.size = R;
    xVec.stride = 1;
    xVec.data = xData;
    xVec.block = NULL;
    xVec.owner = 0;
    
    gsl_linalg_LU_solve(&tmp.mView.matrix, &perm, &bVec, &xVec);
    MatrixStatic<R,1> solution;
    memcpy(solution.mData, xVec.data, R*sizeof(double));
    return solution;
  }


  friend std::ostream& operator<<(std::ostream& oStr, const MatrixStatic& iMat)
  {
      for (int i=0; i<R; ++i) {
          for (int j=0; j<C; ++j) {
              oStr << iMat(i,j) << " ";
          }
          oStr << std::endl;
      }
      return oStr;
  }

  const inline int getNumRows() const { return R; }
  const inline int getNumCols() const { return C; }

  
protected:
  template<int M,int N>
  friend class MatrixStatic;

  template<int M, int N>
  friend class MatrixStaticView;

private:
  double mData[R*C];
  int mLength;
  gsl_matrix_view mView;

};


}

#endif
