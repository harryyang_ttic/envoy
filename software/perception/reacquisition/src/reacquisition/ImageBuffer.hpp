#ifndef _AGILE_REACQ_IMAGEBUFFER_H_
#define _AGILE_REACQ_IMAGEBUFFER_H_

#include <string>
#include <deque>
#include <vector>

#include <bot_core/bot_core.h>

namespace reacq {

typedef struct _ImageWithPose {

    bot_core_image_t *image;
    bot_core_pose_t *pose;

} ImageWithPose;
    
class ImageBuffer {

  public:
    
    ImageBuffer(const std::string &iPoseChannel,
                int32_t iPoseCapacity,                
                const std::string &iImageChannel,
                int32_t iImageCapacity,
                const double iDeltaPositionThreshold,
                const double iDeltaThetaDegreesThreshold);
    
    ~ImageBuffer();
        
    /* always use with Lock() and Unlock() or else
       the behavior is unpredictable */
    ImageWithPose GetImageWithoutLocking(int iIndex);
    /* Caller is responsible for cleanup because these are passed by copy */
    ImageWithPose GetImageByIndex(int iIndex);
    ImageWithPose GetImageByTimestamp(int64_t iTimestamp);

    void Clear();
    
    void Start();
    
    void Stop();

    /* Caller is responsible for cleanup because these are passed by copy */
    void GetAllTimestampsAndPoses(std::vector<int64_t> &oTimes,
                                  std::vector<bot_core_pose_t*> &oPoses);

    static void OnImage(const lcm_recv_buf_t *iRbuf, const char *iChannel, 
                        const bot_core_image_t *iMsg, void *iUserData);
    
    static void OnPose(const lcm_recv_buf_t *iRbuf, const char *iChannel, 
                       const bot_core_pose_t *iMsg, void *iUserData);
    
    inline bool IsRunning() { return mRunning; }
    
    inline bool ImagesAreFull() { return (mImages.size() == mImageCapacity); }
    
    inline bool PosesAreFull() { return (mPoses.size() == mPoseCapacity); }
    
    inline int GetNumPoses() { return mPoses.size(); }

    inline int GetNumImages() { return mImages.size(); }

    /* Start not thread-safe */
    ImageWithPose PeekAtImage(int iIndex);
    
    bot_core_pose_t * PeekAtPose(int iIndex);
    
    void DropFirstPose();

    void AddNewPose(bot_core_pose_t *iPose);
    
    void DropFirstImageWithPose();

    void AddNewImageWithPose(bot_core_image_t *iImage,
                             bot_core_pose_t *iPose);

    /* End not thread-safe */

    inline void Lock() { g_mutex_lock(mMutex); }
    inline void Unlock() { g_mutex_unlock(mMutex); }

    inline double GetDeltaPositionThreshold() { return mDeltaPositionThreshold; }
    inline double GetDeltaThetaDegreesThreshold() { return mDeltaThetaDegreesThreshold; }

    inline std::string GetImageChannelName() { return mImageChannel; }
    inline const std::string GetImageChannelName() const { return mImageChannel; }
        
  private:

    std::string mPoseChannel;
    int32_t mPoseCapacity;

    std::string mImageChannel;
    int32_t mImageCapacity;
    double mDeltaPositionThreshold;
    double mDeltaThetaDegreesThreshold;
    std::deque<ImageWithPose> mImages;
    std::deque<bot_core_pose_t*> mPoses;

    lcm_t *mLcm;
    bot_core_pose_t_subscription_t *mPoseSub;
    bot_core_image_t_subscription_t *mImageSub;

    GMutex *mMutex;
    GCond *mCondition;

    bool mRunning;

    bool mDebug;
    int mVerbose;
    
}; // class ImageBuffer


} //namespace reacq


#endif // _AGILE_REACQ_IMAGEBUFFER_H_
