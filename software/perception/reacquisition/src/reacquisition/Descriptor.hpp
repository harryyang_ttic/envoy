#ifndef _AGILE_REACQ_DESCRIPTOR_H_
#define _AGILE_REACQ_DESCRIPTOR_H_

#define DESCRIPTOR_LENGTH 128

namespace reacq {

class Descriptor {
public:
    float mData[DESCRIPTOR_LENGTH];
};
    
}

#endif // _AGILE_REACQ_DESCRIPTOR_H_
