#include "LensModel.hpp"

#include <bot_core/bot_core.h>

using namespace reacq;
using namespace std;

LensModel::LensModel() {
    mK.makeIdentity();
    mInvK.makeIdentity();
    mImageWidth = mImageHeight = 1;
}

LensModel::~LensModel() {
}

void LensModel::setName(const string& iName) {
    mName = iName;
}

void LensModel::setImageSize(const double iWidth, const double iHeight) {
    mImageWidth = iWidth;
    mImageHeight = iHeight;
}

void LensModel::setPinholeParameters(const double iF, const double iCx,
                                     const double iCy) {
    setPinholeParameters(iF, iF, iCx, iCy, 0);
}

void LensModel::setPinholeParameters(const double iFx, const double iFy,
                                     const double iCx, const double iCy,
                                     const double iSkew) {
    mK(0,0) = iFx;
    mK(1,1) = iFy;
    mK(0,2) = iCx;
    mK(1,2) = iCy;
    mK(0,1) = iSkew;
    mInvK = mK.getInverse();
}

bool LensModel::pixelToRay(const Vector2& iPixel, Vector3& oRay) const {
    return pixelToRay(iPixel(0), iPixel(1), oRay(0), oRay(1), oRay(2));
}

bool LensModel::pixelToRay(const double iX, const double iY,
                           Vector3& oRay) const {
    return pixelToRay(iX, iY, oRay(0), oRay(1), oRay(2));
}

bool LensModel::pixelToRay(const double iX, const double iY,
                           double& oX, double& oY, double& oZ) const {

    // The following undistorts the pixel and returns the projected ray
    // apply inverse pinhole matrix
    double xDistorted = iX*mInvK(0,0) + iY*mInvK(0,1) + mInvK(0,2);
    double yDistorted = iY*mInvK(1,1) + mInvK(1,2);

    // compute angle off of principal ray
    double angleDistorted = atan(hypot(xDistorted, yDistorted));

    // compute corrected angle off principal ray
    double angleUndistorted = undistortAngle(angleDistorted);

    // rotate about principal ray to align radially with distorted ray
    double sinAngleUndistorted = sin(angleUndistorted);
    // TODO: which is faster, hypot + divide + mult + mult or cos+sin+atan2?
    //    double theta = atan2(yDistorted, xDistorted);
    //    double sinTheta = sin(theta);
    //    double cosTheta = cos(theta);
    double mag = hypot(xDistorted, yDistorted);
    double invMag = (mag < 1e-8) ? 1 : 1/mag;
    double cosTheta = xDistorted*invMag;
    double sinTheta = yDistorted*invMag;
    oX = sinAngleUndistorted*cosTheta;
    oY = sinAngleUndistorted*sinTheta;
    oZ = cos(angleUndistorted);
    
    return true;

    // The following uses libbot determine pixel's ray after accounting for lens distortion
    // double ray[3];
    // int ret = bot_camtrans_unproject_pixel (CamTrans, iX, iY, ray);

    // if (ret != 0) {
    //      fprintf (stderr, "Error running LensModel:pixelToRay. bot_camtrans_unproject_pixel returned error.\n");
    //      return false;
    // }

    // double norm = sqrt (ray[0]*ray[0] + ray[1]*ray[1] + ray[2]*ray[2]);

    // oX = ray[0]/norm;
    // oY = ray[1]/norm;
    // oZ = ray[2]/norm;

    // return true;

 
}




bool LensModel::rayToPixel(const Vector3& iRay, Vector2& oPixel) const {
    double dummy;
    return rayToPixel(iRay(0), iRay(1), iRay(2), oPixel(0), oPixel(1), dummy);
}

bool LensModel::rayToPixel(const Vector3& iRay, Vector3& oPixel) const {
    return rayToPixel(iRay(0), iRay(1), iRay(2),
                      oPixel(0), oPixel(1), oPixel(2));
}

bool LensModel::rayToPixel(const Vector3& iRay, double& oX, double& oY) const {
    double dummy;
    return rayToPixel(iRay(0), iRay(1), iRay(2), oX, oY, dummy);
}

bool LensModel::rayToPixel(const Vector3& iRay, double& oX, double& oY,
                           double& oZ) const {
    return rayToPixel(iRay(0), iRay(1), iRay(2), oX, oY, oZ);
}

bool LensModel::rayToPixel(const double iX, const double iY, const double iZ,
                           double& oX, double& oY, double& oZ) const {

    // The following distorts the ray according to the lens model
    double angleUndistorted = atan2(hypot(iX, iY), iZ);
    double angleDistorted = distortAngle(angleUndistorted);
    double sinAngleDistorted = sin(angleDistorted);

    double mag = hypot(iX, iY);
    double invMag = (mag < 1e-8) ? 1 : 1/mag;
    double cosTheta = iX*invMag;
    double sinTheta = iY*invMag;
    // TODO: faster?
    //    double theta = atan2(iY, iX);
    //    double sinTheta = sin(theta);
    //    double cosTheta = cos(theta);
    double x, y, z;
    Vector3 ray;
    x = sinAngleDistorted*cosTheta;
    y = sinAngleDistorted*sinTheta;
    z = cos(angleDistorted);

    // projective divide
    x = x/z;
    y = y/z;

    // apply pinhole matrix
    oX = mK(0,0)*x + mK(0,1)*y + mK(0,2);
    oY = mK(1,1)*y + mK(1,2);
    oZ = z;

    return true;

    // double im_xyz[3];
    // double p_xyz[] = {iX, iY, iZ};
    // int ret = bot_camtrans_project_point (CamTrans, p_xyz, im_xyz);

    //  // if (ret != 0) {
    //  //     fprintf (stderr, "Error running LensModel:rayToPixel. bot_camtrans_project_point returned error\n");
    //  //     return false;
    //  // }
    
    //  double x2, y2, z2;
    //  double sinAngleUndistorted = sin(angleUndistorted);
    //  x2 = sinAngleUndistorted*cosTheta;
    //  y2 = sinAngleUndistorted*sinTheta;
    //  z2 = cos(angleUndistorted);
    
    
    //  double norm = sqrt(iX*iX + iY*iY + iZ*iZ);
    //  //double p_xyz2[] = {iX/norm, iY/norm, iZ/norm};
    //  double p_xyz2[] = {x2, y2, z2};
    //  double im_xyz2[3];
    //  p_xyz2[0] = p_xyz2[0]/p_xyz2[2];
    //  p_xyz2[1] = p_xyz2[1]/p_xyz2[2];
    //  p_xyz2[2] = p_xyz2[2]/p_xyz2[2];
    
    //  int ret2 = bot_camtrans_project_point (CamTrans, p_xyz2, im_xyz2);
    
    // if (ret != 0) {
    //      fprintf (stderr, "Error running LensModel:rayToPixel. bot_camtrans_project_point returned error\n");
    //      //return false;
    //  }
    
    // //oX = im_xyz[0];
    // //oY = im_xyz[1];
    // //oZ = im_xyz[2];
    
    //  return true;
}

bool LensModel::setFromParam(BotParam* iParam, const char *camera_name) {
    string key;

    CamTrans = bot_param_get_new_camtrans (iParam, camera_name);

    if (!CamTrans)
        return false;


    // Set image size
    //double width, height;
    //key = iRoot + ".width";
    //if (0 != bot_param_get_double(iParam, key.c_str(), &width)) return false;
    //key = iRoot + ".height";
    //if (0 != bot_param_get_double(iParam, key.c_str(), &height)) return false;
    double width = bot_camtrans_get_width (CamTrans);
    double height = bot_camtrans_get_height (CamTrans);
    setImageSize(width, height);

    // Set pinhole parameters
    //double fx, fy, skew, cx, cy;
    //key = iRoot + ".fx";
    //if (0 != bot_param_get_double(iParam, key.c_str(), &fx)) return false;
    //key = iRoot + ".fy";
    //if (0 != bot_param_get_double(iParam, key.c_str(), &fy)) return false;
    //key = iRoot + ".skew";
    //if (0 != bot_param_get_double(iParam, key.c_str(), &skew)) return false;
    //key = iRoot + ".cx";
    //if (0 != bot_param_get_double(iParam, key.c_str(), &cx)) return false;
    //key = iRoot + ".cy";
    //if (0 != bot_param_get_double(iParam, key.c_str(), &cy)) return false;
    double fx, fy, skew, cx, cy;
    fx = bot_camtrans_get_focal_length_x (CamTrans);
    fy = bot_camtrans_get_focal_length_y (CamTrans);
    cx = bot_camtrans_get_principal_x (CamTrans);
    cy = bot_camtrans_get_principal_y (CamTrans);
    skew = bot_camtrans_get_skew (CamTrans);
    setPinholeParameters(fx, fy, cx, cy, skew);

    // Set spline distortion coefficients. 
    // These were determined for and are specific to Agile Dragonfly camera.
    {
        int numSplines = 3;
        double scale = 2.915184767625;
        double coeffs[] = {0.000000000000, 0.343031430153, -0.005193595798, -0.007624150997, 0.330213683359, 0.309771785568, -0.032194497400, -0.001145710471, 0.606645261056, 0.241945659354, -0.034567451733, 0.003308900136};
        //     key = iRoot + ".distort.num_splines";
        //     if (0 != bot_param_get_int(iParam, key.c_str(), &numSplines))
        //         return false;
        //     key = iRoot + ".distort.scale_factor";
        //     if (0 != bot_param_get_double(iParam, key.c_str(), &scale))
        //         return false;
        //     double coeffs[4*numSplines];
        //     key = iRoot + ".distort.coeffs";
        //     if (4*numSplines != bot_param_get_double_array(iParam, key.c_str(),
        //                                                    coeffs, 4*numSplines))
        //         return false;
        MatrixDynamic coeffMatx(numSplines,4);
        for (int i = 0; i < numSplines; ++i) {
            for (int j = 0; j < 4; ++j) {
                coeffMatx(i,j) = coeffs[i*4 + j];
            }
        }
        mDistortWarp = SplineWarp(coeffMatx, scale);
    }

    // Set spline undistortion coefficients
    // These were determined for and are specific to Agile Dragonfly camera.
    {
         int numSplines = 3;
         double scale = 3.666416907899;
         double coeffs[] = {0.000000000000, 0.272745851091, 0.002607743765, 0.005039308844, 0.280392903701, 0.293079265154, 0.018637362324, 0.010224645409, 0.602334176587, 0.361027926029, 0.052004064105, 0.015125670970};
    //     key = iRoot + ".undistort.num_splines";
    //     if (0 != bot_param_get_int(iParam, key.c_str(), &numSplines))
    //         return false;
    //     key = iRoot + ".undistort.scale_factor";
    //     if (0 != bot_param_get_double(iParam, key.c_str(), &scale))
    //         return false;
    //     double coeffs[4*numSplines];
    //     key = iRoot + ".undistort.coeffs";
    //     if (4*numSplines != bot_param_get_double_array(iParam, key.c_str(),
    //                                                    coeffs, 4*numSplines))
    //         return false;
         MatrixDynamic coeffMatx(numSplines,4);
         for (int i = 0; i < numSplines; ++i) {
             for (int j = 0; j < 4; ++j) {
                 coeffMatx(i,j) = coeffs[i*4 + j];
             }
         }
         mUndistortWarp = SplineWarp(coeffMatx, scale);
    }

    return true;
}




LensModel::SplineWarp::SplineWarp(const MatrixDynamic& iCoeffs,
                                  const double iScaleFactor) {
    mCoeffs = iCoeffs;
    mNumSplines = mCoeffs.getNumRows();
    mScaleFactor = iScaleFactor;
}

double LensModel::SplineWarp::apply(const double iX) const {
    double xScaled = std::max(mScaleFactor*iX, 0.0);
    int xWhole = (int)floor(xScaled);
    int whichSpline = xWhole;
    whichSpline = std::min(whichSpline, mNumSplines-1);

    double x = xScaled-whichSpline;
    double x2 = x*x;
    double x3 = x2*x;

    return (mCoeffs(whichSpline,0) + mCoeffs(whichSpline,1)*x +
            mCoeffs(whichSpline,2)*x2 + mCoeffs(whichSpline,3)*x3);
}


double LensModel::distortAngle(const double iAngle) const {
    return mDistortWarp.apply(iAngle);
}

double LensModel::undistortAngle(const double iAngle) const {
    return mUndistortWarp.apply(iAngle);
}
