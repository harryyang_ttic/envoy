#include <unistd.h>

#include <fstream>
#include <sstream>
#include <vector>

#include <sift/sift.hpp>

#include "ObjectModelMatcher.hpp"
#include "BundleResection.hpp"
#include "Utilities.hpp"
#include "IoUtilities.hpp"
#include "Reacquisition.hpp"
#include "AlgorithmParameters.hpp"
#include "LensModel.hpp"



#include <geom_utils/convexhull.h>

// TODO: make these #defines into configurable parameters
#define INLIER_THRESHOLD 5
#define MIN_NUM_INLIERS 10

using namespace reacq;
using namespace std;

ObjectModelMatcher::ImageData::ImageData() {
    mpImage = NULL;
    mpSubscription = NULL;
    mState = StateNew;
    mpMutex = g_mutex_new();
    mpCondition = g_cond_new();
    mFullImage.data = NULL;
    mSmallImage.data = NULL;
    mColorTemp.data = NULL;
    mGrayTemp.data = NULL;
}

ObjectModelMatcher::ImageData::~ImageData() {
    g_cond_free(mpCondition);
    g_mutex_free(mpMutex);
    if (NULL != mFullImage.data) {
        Utilities::freeAlignedImage(&mFullImage);
    }
    if (NULL != mSmallImage.data) {
        Utilities::freeAlignedImage(&mSmallImage);
    }
    if (NULL != mColorTemp.data) {
        Utilities::freeAlignedImage(&mColorTemp);
    }
    if (NULL != mGrayTemp.data) {
        Utilities::freeAlignedImage(&mGrayTemp);
    }
}


ObjectModelMatcher::
ObjectModelMatcher(Reacquisition *iReacq,
                   const erlcm_reacquisition_command_t *iCmd)
{
    mpCmd = erlcm_reacquisition_command_t_copy(iCmd);
    mpReacq = iReacq;

    if (!g_thread_supported ()) {
        g_thread_init (NULL);
    }

    mpLCM = bot_lcm_get_global (NULL);

    // set up image data map
    vector<string> allCameraNames = mpReacq->GetCameraNames();
    vector<string> cameraNames;
    for (int i = 0; i < allCameraNames.size(); ++i) {
        Reacquisition::Camera* cam = mpReacq->GetCamera(allCameraNames[i]);
        if (cam->mUseForMatching) {
            cameraNames.push_back(allCameraNames[i]);
        }        
    }

    for (int i = 0; i < cameraNames.size(); ++i) {
        string channelName = mpReacq->GetImageChannelName(cameraNames[i]);
        mChannelToCameraMap.insert(make_pair(channelName, cameraNames[i]));

        ImageData* data = new ImageData();
        data->mCameraName = cameraNames[i];
        mImageDataMap.insert(std::make_pair(channelName, data));
    }

    // subscribe to image channels
    mRunning = false;
    map<string,ImageData*>::iterator iter = mImageDataMap.begin();
    for (; iter != mImageDataMap.end(); ++iter) {
        string channelName =
            mpReacq->GetImageChannelName(iter->second->mCameraName);
        iter->second->mpSubscription =
            bot_core_image_t_subscribe(mpLCM, channelName.c_str(),
                                     onImage, this);
        cout << "ObjectModelMatcher subscribed to channel [" << channelName << "]" << endl;
    }

    // spawn thread
    mRunning = true;
    g_thread_create (InnerMatch, this, TRUE, NULL);

    cout << "Created ObjectModelMatcher instance." << endl;
}

ObjectModelMatcher::~ObjectModelMatcher()
{
    mRunning = false;

    // NOTE: it is unsafe for this instance to be deleted without
    // calling Stop() first. (??) TODO
    // unsubscribe from all image channels
    map<string, ImageData*>::iterator iter;
    for (iter = mImageDataMap.begin();
         iter != mImageDataMap.end(); ++iter) {
        g_mutex_lock(iter->second->mpMutex);
        bot_core_image_t_unsubscribe(mpLCM,
                                   iter->second->mpSubscription);

        while (ImageData::StateProcessing == iter->second->mState) {
            g_cond_wait(iter->second->mpCondition, iter->second->mpMutex);
        }

        if (NULL != iter->second->mpImage) {
            bot_core_image_t_destroy(iter->second->mpImage);
            iter->second->mpImage = NULL;
        }
        g_mutex_unlock(iter->second->mpMutex);        
        delete iter->second;
    }

    if (NULL != mpCmd) {
        erlcm_reacquisition_command_t_destroy(mpCmd);
    }
}

void
ObjectModelMatcher::Stop()
{
    mRunning = false;
}


void *
ObjectModelMatcher::InnerMatch (void *user)
{
    ObjectModelMatcher *matcher = static_cast<ObjectModelMatcher*>(user);
    Reacquisition *reacq = matcher->mpReacq;
    ObjectModel *model = matcher->mpReacq->FindModel(matcher->mpCmd->object_id);
    if (NULL == model) {
        cerr << "ERROR: ObjectModelMatcher does not have model ["
             << matcher->mpCmd->object_id << "]" << endl;
        return NULL;
    }

    // get features from 3D model points
    vector<Descriptor> modelDescriptors(model->mContextPoints.size());
    {
        vector<Descriptor>::iterator iter = modelDescriptors.begin();
        vector<ModelPoint*>::iterator pointIter =
            model->mContextPoints.begin();
        for (; pointIter != model->mContextPoints.end();
             ++pointIter, ++iter) {
            *iter = Utilities::copyDescriptor((*pointIter)->mDescriptor);
        }
    }
    cout << "Matcher: copied " << modelDescriptors.size() <<
        " model descriptors" << endl;

    // loop until we stop running
    // TODO: how can we ensure model is always valid?
    // if we delete the model, need to stop running first? 
    while (true == matcher->mRunning) {

        // wait for new data
        // TODO: better way to do this?
        usleep(10000);
        map<string, ImageData*>::iterator iter =
            matcher->mImageDataMap.begin();
        for (; iter != matcher->mImageDataMap.end(); ++iter) {
            if (!matcher->mRunning) {
                break;
            }

            ImageData* data = iter->second;

            g_mutex_lock(data->mpMutex);
            if ((ImageData::StateDone == data->mState) ||
                (NULL == data->mpImage)) {
                g_mutex_unlock(data->mpMutex);
                continue;
            }
            data->mState = ImageData::StateProcessing;
            g_mutex_unlock(data->mpMutex);

            // run SIFT on this image
            Utilities::decompressToMultiScale(data->mpImage,
                                              &data->mFullImage,
                                              &data->mSmallImage,
                                              &data->mColorTemp,
                                              &data->mGrayTemp);
            sift_point_feature_list_t* imagePoints =
                sift_compute_uncompressed(data->mSmallImage.width,
                                          data->mSmallImage.height,
                                          AlgorithmParameters::mSiftLevels,
                                          (float*)data->mSmallImage.data,
                                          AlgorithmParameters::mSiftThreshContext,
                                          AlgorithmParameters::mSiftDoubleImageSize,
                                          AlgorithmParameters::mSiftSigmaInit,
                                          data->mpImage->utime);

            double smallImageScale = 1.0/AlgorithmParameters::mSmallImageScale;
            Utilities::scaleSiftFeatures(imagePoints, smallImageScale,
                                         smallImageScale);


            // TODO: do we want to spawn one thread for each camera?

            // copy descriptors from image features
            cout << endl << "----------------------------------------" << endl;
            cout << "Matcher: found an image to process in camera " <<
                data->mCameraName << endl;
            // TODO: copy data so that the onFeatures can proceed
            // with this channel?
            vector<Descriptor> imageDescriptors =
                Utilities::makeDescriptorsFromFeatureList(imagePoints);
            cout << "Matcher: copied " << imageDescriptors.size() <<
                " image descriptors" << endl;
            int64_t imageTimestamp = imagePoints->utime;

            // match features
            // TODO: make 1.5 a parameter
            vector<PointMatch> matches;
            Utilities::matchDescriptorsDistance(modelDescriptors,
                                                imageDescriptors,
                                                1.5f, matches);
            cout << "Matcher: found " << matches.size() <<
                " SIFT matches" << endl;
            if (matches.size() < MIN_NUM_INLIERS) {
                cout << "Matcher: too few SIFT matches; bailing..." << endl;
                sift_point_feature_list_t_destroy(imagePoints);
                g_mutex_lock(data->mpMutex);
                data->mState = ImageData::StateDone;
                g_cond_signal(data->mpCondition);
                g_mutex_unlock(data->mpMutex);
                continue;
            }
            
            // set up resection problem
            Measurements measurements(matches.size());
            for (int imatch = 0; imatch < matches.size(); ++imatch) {
                Measurement* meas = new Measurement;
                meas->mImagePoint(0) =
                    imagePoints->pts[matches[imatch].mIndex2].col;
                meas->mImagePoint(1) =
                    imagePoints->pts[matches[imatch].mIndex2].row;
                meas->mModelPointIndex = matches[imatch].mIndex1;
                meas->mPoseIndex = 0; //Don't care for now
                measurements[imatch] = meas;
            }
            LensModel *lensModel =
                reacq->GetCamera(data->mCameraName)->mpLensModel;

            sift_point_feature_list_t_destroy(imagePoints);


#define MATLAB
#ifdef MATLAB
            {
                string camfile;
                camfile += "/share/ResectionPoints_"+data->mCameraName+".m";
                ofstream ofs(camfile.c_str(),ios::out);
                ofs << "image_x_y_model_x_y_z = [" << endl;
                for (int i = 0; i < matches.size(); ++i) {
                    Measurement *meas = measurements[i];
                    ofs << " " << meas->mImagePoint(0)
                        << " " << meas->mImagePoint(1)
                        << " " << model->mContextPoints[meas->mModelPointIndex]->mPoint(0)
                        << " " << model->mContextPoints[meas->mModelPointIndex]->mPoint(1)
                        << " " << model->mContextPoints[meas->mModelPointIndex]->mPoint(2)
                        << endl;
                }
                ofs << "];" << endl;
                ofs.close();
            }
#endif
            // solve resection problem
            Pose pose;
            pose.mOrientation.makeIdentity();
            pose.mPosition.zero();
            BundleResection resection;
            RobustLM::Result resecResult =
                resection.solve(measurements,
                                model->mContextPoints,
                                pose, lensModel);
            cout << "Matcher: solved resection problem" << endl;

            // delete measurements
            for (int i = 0; i < measurements.size(); ++i) {
                delete measurements[i];
            }

            if (resecResult.mSuccess) {

                // check number of inliers
                int numInliers = 0;
                for (int i = 0; i < matches.size(); ++i) {
                    double ex = resecResult.mErrors(2*i);
                    double ey = resecResult.mErrors(2*i+1);
                    double e2 = ex*ex + ey*ey;
                    if (e2 < INLIER_THRESHOLD*INLIER_THRESHOLD) {
                        ++numInliers;
                    }
                }
                cout << "Matcher: there were " << numInliers <<
                    " inliers" << endl;
                if (numInliers < MIN_NUM_INLIERS) {
                    cout << "Matcher: too few inliers; bailing..." << endl;
                    g_mutex_lock(data->mpMutex);
                    data->mState = ImageData::StateDone;
                    g_cond_signal(data->mpCondition);
                    g_mutex_unlock(data->mpMutex);
                    continue;
                }

                // project object points onto this pose
                vector<Vector3> ppts(model->mObjectPoints.size());
                bool keepGoing = true;
                {
                    int i=0;
                    while (keepGoing && (i < model->mObjectPoints.size())) {
                        Vector3 pt3d = model->mObjectPoints[i]->mPoint;
                        if (false == Utilities::projectPointToImage(pt3d,
                                                                    pose.mOrientation,
                                                                    pose.mPosition,
                                                                    lensModel,
                                                                    ppts[i])) {
                            keepGoing = false;
                        }
                        ++i;
                    }
                }
                if (false == keepGoing) {
                    cout << "Matcher: bad projection found; bailing..."
                         << endl;
                    g_mutex_lock(data->mpMutex);
                    data->mState = ImageData::StateDone;
                    g_cond_signal(data->mpCondition);
                    g_mutex_unlock(data->mpMutex);
                    continue;
                }                    
                pointlist2d_t* projections = pointlist2d_new(ppts.size());
                for (int i=0; i<ppts.size(); ++i) {
                    projections->points[i].x = ppts[i](0);
                    projections->points[i].y = ppts[i](1);
                }
                cout << "Matcher: projected " <<
                    model->mObjectPoints.size() <<
                    " object points into this image" << endl;

                // form convex hull and find centroid
                pointlist2d_t* convexHull =
                    convexhull_graham_scan_2d (projections);
                pointlist2d_free(projections);
                double meanPt[2];
                meanPt[0] = meanPt[1] = 0;
                for (int i = 0; i < convexHull->npoints; ++i) {
                    meanPt[0] += convexHull->points[i].x;
                    meanPt[1] += convexHull->points[i].y;
                }
                meanPt[0] /= convexHull->npoints;
                meanPt[1] /= convexHull->npoints;
                cout << "Matcher: found mean of convex hull [" << meanPt[0] <<
                    ", " << meanPt[1] << "]" << endl;

                // pad hull
                // TODO: make padding amount (in pixels) a parameter
                double pad = 20;
                for (int i = 0; i < convexHull->npoints; ++i) {
                    double dir[2];
                    dir[0] = convexHull->points[i].x - meanPt[0];
                    dir[1] = convexHull->points[i].y - meanPt[1];
                    double mag = sqrt(dir[0]*dir[0] + dir[1]*dir[1]);
                    dir[0] /= mag;
                    dir[1] /= mag;

                    convexHull->points[i].x = meanPt[0] + dir[0]*(mag+pad);
                    convexHull->points[i].y = meanPt[1] + dir[1]*(mag+pad);
                }
                
                // prepare and send message
                erlcm_reacquisition_detection_list_t* msg =
                    (erlcm_reacquisition_detection_list_t*)
                    calloc(1, sizeof(erlcm_reacquisition_detection_list_t));
                msg->utime = bot_timestamp_now();
                msg->num_detections = 1;
                msg->detections = (erlcm_reacquisition_detection_t*)
                    calloc(1, sizeof(erlcm_reacquisition_detection_t));
                msg->detections->utime = msg->utime;
                msg->detections->image_utime = imageTimestamp;
                msg->detections->object_id = matcher->mpCmd->object_id;
                msg->detections->camname = strdup(data->mCameraName.c_str());
                msg->detections->roi.npoints = convexHull->npoints;
                msg->detections->roi.points = (erlcm_point2d_t*)
                    calloc(convexHull->npoints, sizeof(erlcm_point2d_t));
                for (int i = 0; i < convexHull->npoints; ++i) {
                    msg->detections->roi.points[i].x = convexHull->points[i].x;
                    msg->detections->roi.points[i].y = convexHull->points[i].y;
                }
                msg->detections->confidence = 1;  // TODO: meaningful confidence
#define LCMGL
#ifdef LCMGL
                IoUtilities::lcmglPublishGesture(string()+"VISION."+
                                                 reacq->GetImageChannelName(lensModel->getName())+
                                                 ".REACQ_GESTURE2",
                                                 msg->detections->roi,
                                                 0.f, 1.0f, 0.f, 8.f);
                
#endif // LCMGL
#define MATLAB
#ifdef MATLAB
                {
                    ostringstream camfile;
                    camfile << "/share/Gesture_" << data->mCameraName
                            << "_" << imageTimestamp << ".m";
                    ofstream ofs(camfile.str().c_str(),ios::out);
                    string gesture("gesture");
                    ofs << IoUtilities::pointlistToMatlabString(gesture,
                                                                msg->detections->roi,
                                                                imageTimestamp,
                                                                data->mCameraName);
                    ofs.close();
                }

#endif
                if (reacq->ShouldWriteMatlabResults()) {
                    ofstream ofs(reacq->GetMatlabResultsFilename().c_str(), ios::out|ios::app);
                    int matlab_index = reacq->GetMatlabIndex();
                    ostringstream line_prefix_stream;
                    line_prefix_stream << "reacquisition_results(" << matlab_index << ").";
                    string line_prefix(line_prefix_stream.str());
                    ofs << line_prefix << "utime = " << msg->utime << ";" << endl
                        << line_prefix << "object_id = " << msg->detections->object_id << ";" << endl
                        << line_prefix << "gesture_utime = " << msg->detections->image_utime << ";" << endl
                        << line_prefix << "reprojected_gesture_xy = [ ..." << endl;
                    for (int ipt=0; ipt< msg->detections->roi.npoints; ++ipt) {
                        ofs << msg->detections->roi.points[ipt].x << ", " << msg->detections->roi.points[ipt].y << "; ";
                    }
                    ofs << "];" << endl
                        << line_prefix << "confidence = " << msg->detections->confidence << ";" << endl;
                    ofs << line_prefix << "current_pose = ["
                        << pose.mPosition(0) << ","
                        << pose.mPosition(1) << ","
                        << pose.mPosition(2) << "];" << endl;
//                         << line_prefix << "task_id = " << << ";" << endl
//                         << line_prefix << "gesture_pose = " << << ";" << endl
//                         << line_prefix << "current_pose = " << << ";" << endl
//                         << line_prefix << "user_gesture_pose = " << << ";" << endl
//                         << line_prefix << "user_gesture_time = " << << ";" << endl;
                    reacq->IncrementMatlabIndex();
                    ofs.close();

//                     reacquisition_results(1).utime = 1276558840693679;
//                     reacquisition_results(1).pallet_id = 330798485361339648;
//                     reacquisition_results(1).gesture_utime = 1292181583443047;
//                     reacquisition_results(1).gesture_pose = [0.047108, -0.299357, -0.118546];
//                     reacquisition_results(1).task_id = 330798485361420288;
//                     reacquisition_results(1).reprojected_gesture_xy = [ ...
//                                                                         546.31, 57.95; 550.30, 63.21; 554.29, 73.75; 556.28, 81.65; 556.28, 113.26; 550.30, 134.33; 542.33, 152.77; 538.34, 160.67; 536.34, 163.30; 534.35, 165.93; 520.39, 181.74; 508.43, 192.27; 504.44, 192.27; 494.47, 189.64; 486.50, 187.01; 480.52, 184.37; 476.53, 181.74; 466.56, 173.84; 454.60, 163.30; 448.62, 155.40; 440.64, 139.60; 440.64, 115.89; 442.63, 110.62; 444.63, 107.99; 484.50, 76.38; 498.46, 71.11; 540.33, 57.95;];
//                     reacquisition_results(1).confidence = 1.000000;
//                     reacquisition_results(1).current_pose = [-0.671204, -0.678831, -0.116628];
//                     reacquisition_results(1).user_gesture_pose = [-0.671204, -0.678831, -0.116628];
//                     reacquisition_results(1).user_gesture_time = 1276558840693679;
                    
                }
                
                const char* chan = reacq->GetPublishChannel().c_str();
                erlcm_reacquisition_detection_list_t_publish(matcher->mpLCM,
                                                             chan, msg);
                cout << "Matcher: published gesture with " <<
                    msg->detections->roi.npoints << " points" << endl;
                
                pointlist2d_free(convexHull);
                erlcm_reacquisition_detection_list_t_destroy(msg);
            }
            g_mutex_lock(data->mpMutex);
            data->mState = ImageData::StateDone;
            g_cond_signal(data->mpCondition);
            g_mutex_unlock(data->mpMutex);
        }
    }

    delete matcher;
    return NULL;
}


void ObjectModelMatcher::onImage(const lcm_recv_buf_t *iRbuf,
                                 const char *iChannel, 
                                 const bot_core_image_t *iMsg,
                                 void *iUserData)
{
    fprintf (stdout, "    Got image on channel %s\n", iChannel);
    ObjectModelMatcher* matcher = (ObjectModelMatcher*)iUserData;
    ImageData* data = matcher->mImageDataMap[string(iChannel)];
    g_mutex_lock(data->mpMutex);
    if (!matcher->mRunning) {
        g_mutex_unlock(data->mpMutex);
        return;
    }
    if (ImageData::StateProcessing == data->mState) {
        g_mutex_unlock(data->mpMutex);
        return;
    }
    if (NULL != data->mpImage) {
        bot_core_image_t_destroy(data->mpImage);
        data->mpImage = NULL;
    }
    data->mpImage = bot_core_image_t_copy(iMsg);
    if (NULL == data->mFullImage.data) {
        double scale = AlgorithmParameters::mSmallImageScale;
        Utilities::allocateMultiScaleBuffers(data->mpImage,
                                             &data->mFullImage,
                                             &data->mSmallImage,
                                             &data->mColorTemp,
                                             &data->mGrayTemp,
                                             scale, scale);
    }
    data->mCameraName = matcher->mChannelToCameraMap[iChannel];
    data->mState = ImageData::StateNew;
    // g_cond_signal(data->mpCondition);
    g_mutex_unlock(data->mpMutex);
}
