#include "IoUtilities.hpp"

#include <sstream>
#include <iostream>
#include <iomanip>
#include <GL/gl.h>
#include "Utilities.hpp"

#include <bot_param/param_client.h>

using namespace reacq;
using namespace std;

string
IoUtilities::camtransToMatlabString(const BotCamTrans *iCamtrans)
{
    ostringstream oss;
    string name(bot_camtrans_get_name(iCamtrans));
    oss << "camera_" << name
        << "= struct("
        << "'fx', " << bot_camtrans_get_focal_length_x(iCamtrans) << ", "
        << "'fy', " << bot_camtrans_get_focal_length_y(iCamtrans) << ", "
        << "'width', " << bot_camtrans_get_image_width(iCamtrans) << ", "
        << "'height', " << bot_camtrans_get_image_height(iCamtrans) << ", "
        << "'px', " << bot_camtrans_get_principal_x(iCamtrans) << ", "
        << "'px', " << bot_camtrans_get_principal_y(iCamtrans) << ");"
        << endl;

    return oss.str();
}

string
IoUtilities::lensModelToMatlabString(const LensModel *iLensModel)
{
    ostringstream oss;
    Matrix33 p = iLensModel->getPinholeMatrix();
    oss << "camera_" << iLensModel->getName()
        << "= struct("
        << "'fx', " << iLensModel->getFocalX() << ", "
        << "'fy', " << iLensModel->getFocalY() << ", "
        << "'skew', " << iLensModel->getSkew() << ", "
        << "'width', " << iLensModel->getWidth() << ", "
        << "'height', " << iLensModel->getHeight() << ", "
        << "'matrix', [" << p(0,0) << "," << p(0,1) << "," << p(0,2)
        << ";" << p(1,0) << "," << p(1,1) << "," << p(1,2)
        << ";" << p(2,0) << "," << p(2,1) << "," << p(2,2) << "], "
        << "'cx', " << iLensModel->getCenterX() << ", "
        << "'cx', " << iLensModel->getCenterY() << ");"
        << endl;

    return oss.str();
}

string IoUtilities::pointFeaturesToMatlabString(const string &iVarName,
                                                const sift_point_feature_list_t *iFeatures)
{
    if (NULL == iFeatures) {
        return "";
    }
    int nfeats = iFeatures->num;
    int fsize = iFeatures->desc_size;
    ostringstream oss;
    
    oss << iVarName << " = zeros(" << nfeats << "," <<  fsize + 2 << ");" << endl;
    for (int i = 0; i < nfeats; ++i) {
        const float* d1 = iFeatures->pts[i].desc;
        oss << iVarName << "(" << i+1 << ",:) = [ ";
        oss << iFeatures->pts[i].col << " " << iFeatures->pts[i].row;
        for (int k = 0; k < fsize; ++k) {
            oss << " " << d1[k];
        }
        oss << " ];" << endl;
    }
    
    return oss.str();
}

string IoUtilities::measurementsToString(const Measurements &iMeasurements)
{
    ostringstream oss;
    for (int i=0; i<iMeasurements.size(); ++i) {
        Measurement *m = iMeasurements[i];
        oss << m->mImagePoint(0) << " " << m->mImagePoint(1) << " "
            << m->mModelPointIndex + 1 << " " << m->mPoseIndex + 1 << " ;" << endl;
    }

    return oss.str();
}

string IoUtilities::measurementsToMatlabString(const string &iVarName,
                                               const Measurements &iMeas)
{
    ostringstream oss;

    oss << iVarName << " = [ " << endl;
    oss << IoUtilities::measurementsToString(iMeas);
    oss << " ];" << endl;

    return oss.str();
}

string IoUtilities::objectToMatlabString(const string &iVarName,
                                         const om_object_t *iObject)
{
    ostringstream oss;

    oss << iVarName << ".utime = " << iObject->utime << " ;" << endl;
    oss << iVarName << ".id = " << iObject->id << " ;" << endl;
    oss << iVarName << ".pos = [ "
        << iObject->pos[0] << " "
        << iObject->pos[1] << " "
        << iObject->pos[2] << " ];" << endl;
    oss << iVarName << ".orientation = [ "
        << iObject->orientation[0] << " "
        << iObject->orientation[1] << " "
        << iObject->orientation[2] << " "
        << iObject->orientation[3] << " ];" << endl;
    oss << iVarName << ".bbox_min = [ "
        << iObject->bbox_min[0] << " "
        << iObject->bbox_min[1] << " "
        << iObject->bbox_min[2] << " ];" << endl;
    oss << iVarName << ".bbox_max = [ "
        << iObject->bbox_max[0] << " "
        << iObject->bbox_max[1] << " "
        << iObject->bbox_max[2] << " ];" << endl;
    oss << iVarName << ".object_type = " << iObject->object_type << " ;" << endl;
    oss << iVarName << ".label = " << iObject->label << " ;" << endl;
    
    return oss.str();
}

string IoUtilities::modelPointsToString(const ModelPoints &iModelPoints)
{
    ostringstream oss;
    for (int i=0; i<iModelPoints.size(); ++i) {
        ModelPoint *pt = iModelPoints[i];
        oss << pt->mPoint(0) << " " << pt->mPoint(1) << " " << pt->mPoint(2);
        for (int j=0; j<DESCRIPTOR_LENGTH; ++j) {
            oss << " " << pt->mDescriptor.mData[j];
        }
        oss << " ;" << endl;
    }
    

    return oss.str();
}


string IoUtilities::posesToString(const Poses &iPoses)
{
    ostringstream oss;
    for (int i=0; i<iPoses.size(); ++i) {
        Pose *p = iPoses[i];
        for (int j=0; j<3; ++j) {
            for (int k=0; k<3; ++k) {
                oss << p->mOrientation(j,k) << " ";
            }
        }
        for (int j=0; j<3; ++j) {
            oss << p->mPosition(j) << " ";
        }
        oss << " ;" << endl;
    }
    return oss.str();
}

string
IoUtilities::reacquisitionSegmentationToString(
    const string &iVarName,
    const erlcm_reacquisition_segmentation_t *iSegmentation)
{
    ostringstream oss;
    oss << iVarName << ".utime = " << iSegmentation->utime << " ;" << endl;
    oss << iVarName << ".object_id = " << iSegmentation->object_id << " ;" << endl;
    oss << iVarName << ".roi_utime = " << iSegmentation->roi_utime << " ;" << endl;
    oss << iVarName << ".camera = '" << iSegmentation->camera << "' ;" << endl;
    // oss << iVarName << ".pose.utime = " << iSegmentation->pose.utime << " ;" << endl;
    // oss << iVarName << ".pose.pos = [ "
    //     << iSegmentation->pose.pos[0] << " "
    //     << iSegmentation->pose.pos[1] << " "
    //     << iSegmentation->pose.pos[2] << " ];" << endl;
    // oss << iVarName << ".pose.vel = [ "
    //     << iSegmentation->pose.vel[0] << " "
    //     << iSegmentation->pose.vel[1] << " "
    //     << iSegmentation->pose.vel[2] << " ];" << endl;
    // oss << iVarName << ".pose.orientation = [ "
    //     << iSegmentation->pose.orientation[0] << " "
    //     << iSegmentation->pose.orientation[1] << " "
    //     << iSegmentation->pose.orientation[2] << " "
    //     << iSegmentation->pose.orientation[3] << " ];" << endl;
    // oss << iVarName << ".pose.rotation_rate = [ "
    //     << iSegmentation->pose.rotation_rate[0] << " "
    //     << iSegmentation->pose.rotation_rate[1] << " "
    //     << iSegmentation->pose.rotation_rate[2] << " ];" << endl;
    // oss << iVarName << ".pose.accel = [ "
    //     << iSegmentation->pose.accel[0] << " "
    //     << iSegmentation->pose.accel[1] << " "
    //     << iSegmentation->pose.accel[2] << " ];" << endl;
    oss << "% " << iVarName << ".roi.size() = " << iSegmentation->roi.npoints << endl;
    oss << iVarName << ".roi = [ " << endl;
    for (int i=0; i<iSegmentation->roi.npoints; ++i) {
        oss << " " << iSegmentation->roi.points[i].x
            << " " << iSegmentation->roi.points[i].y;
    }
    oss << " ];" << endl;
    oss << iVarName << ".image_utime = " << iSegmentation->image_utime << " ;" << endl;
    
    return oss.str();
}

void IoUtilities::loadModelPointsFromMatlabFile(ifstream &ifs,
                                                ModelPoints &oPts)
{
    string stmp;
    int n;
    ifs >> stmp >> stmp >> stmp >> n;
    oPts.resize(n);
    ifs >> stmp >> stmp >> stmp;
    double d;
    for (int i=0; i<n; ++i) {
        ModelPoint *pt = new ModelPoint();
        for (int j=0; j<3; ++j) {
            ifs >> d;
            pt->mPoint(j) = d;
        }
        for (int j=0; j<DESCRIPTOR_LENGTH; ++j) {
            ifs >> pt->mDescriptor.mData[j];
        }
        oPts[i] = pt;
        ifs >> stmp;
    }
    ifs >> stmp;
}

void IoUtilities::loadMeasurementsFromMatlabFile(ifstream &ifs,
                                                 Measurements &oMeasurements)
{
    string stmp;
    int n;
    ifs >> stmp >> stmp >> stmp >> n;
    oMeasurements.resize(n);
    ifs >> stmp >> stmp >> stmp;
    double d;
    for (int i=0; i<n; ++i) {
        Measurement *m = new Measurement();
        for (int j=0; j<2; ++j) {
            ifs >> d;
            m->mImagePoint(j) = d;
        }
        ifs >> m->mModelPointIndex >> m->mPoseIndex;
        m->mModelPointIndex--;
        m->mPoseIndex--;
        oMeasurements[i] = m;
        ifs >> stmp;
    }
    ifs >> stmp;
}

void IoUtilities::loadPosesFromMatlabFile(ifstream &ifs,
                                          Poses &oPoses)
{
    string stmp;
    int n;
    ifs >> stmp >> stmp >> stmp >> n;
    oPoses.resize(n);
    ifs >> stmp >> stmp >> stmp;
    double d;
    for (int i=0; i<n; ++i) {
        Pose *p = new Pose();
        for (int j=0; j<3; ++j) {        
            for (int k=0; k<3; ++k) {
                ifs >> d;
                p->mOrientation(j,k) = d;
            }
        }
        for (int j=0; j<3; ++j) {
            ifs >> d;
            p->mPosition(j) = d;
        }
        oPoses[i] = p;
        ifs >> stmp;
    }
    ifs >> stmp;
}


void IoUtilities::loadGestureFromMatlabFile(ifstream &ifs,
                                            erlcm_reacquisition_segmentation_t* &oGesture)
{
    string stmp;
    oGesture = (erlcm_reacquisition_segmentation_t*)
        calloc(1, sizeof(erlcm_reacquisition_segmentation_t));
    ifs >> stmp >> stmp >> oGesture->utime >> stmp;
    ifs >> stmp >> stmp >> oGesture->object_id >> stmp;
    ifs >> stmp >> stmp >> oGesture->roi_utime >> stmp;
    string scam;
    ifs >> stmp >> stmp >> scam >> stmp;
    oGesture->camera = strdup(scam.substr(1, scam.length()-2).c_str());
    // bot_core_pose_t pose;
    // double d0, d1, d2, d3;
    // ifs >> stmp >> stmp >> pose.utime >> stmp;
    // ifs >> stmp >> stmp >> stmp
    //     >> d0 >> d1 >> d2 >> stmp;
    // pose.pos[0] = d0; pose.pos[1] = d1; pose.pos[2] = d2;
    // ifs >> stmp >> stmp >> stmp
    //     >> d0 >> d1 >> d2 >> stmp;
    // pose.vel[0] = d0; pose.vel[1] = d1; pose.vel[2] = d2;
    // ifs >> stmp >> stmp >> stmp
    //     >> d0 >> d1 >> d2 >> d3 >> stmp;
    // pose.orientation[0] = d0; pose.orientation[1] = d1;
    // pose.orientation[2] = d2; pose.orientation[3] = d3;
    // ifs >> stmp >> stmp >> stmp
    //     >> d0 >> d1 >> d2 >> stmp;
    // pose.rotation_rate[0] = d0; pose.rotation_rate[1] = d1;
    // pose.rotation_rate[2] = d2;
    // ifs >> stmp >> stmp >> stmp
    //     >> d0 >> d1 >> d2 >> stmp;
    // pose.accel[0] = d0; pose.accel[1] = d1; pose.accel[2] = d2;
    // oGesture->pose = pose;
    ifs >> stmp >> stmp >> stmp >> oGesture->roi.npoints;
    oGesture->roi.points = (erlcm_point2d_t*)
        calloc(oGesture->roi.npoints, sizeof(erlcm_point2d_t));
    ifs >> stmp >> stmp >> stmp;
    double x, y;
    for (int i=0; i<oGesture->roi.npoints; ++i) {
        ifs >> x >> y;
        oGesture->roi.points[i].x = x;
        oGesture->roi.points[i].y = y;
    }
    ifs >> stmp;
    ifs >> stmp >> stmp >> oGesture->image_utime >> stmp;
}

void IoUtilities::loadObjectFromMatlabFile(ifstream &ifs,
                                           om_object_t* &oObject)
{
    string stmp;
    oObject = (om_object_t*) calloc(1, sizeof(om_object_t));

    ifs >> stmp >> stmp >> oObject->utime >> stmp;
    ifs >> stmp >> stmp >> oObject->id >> stmp;
    
    double d0, d1, d2, d3;
    ifs >> stmp >> stmp >> stmp >> d0 >> d1 >> d2 >> stmp;
    oObject->pos[0] = d0; oObject->pos[1] = d1; oObject->pos[2] = d2;

    ifs >> stmp >> stmp >> stmp >> d0 >> d1 >> d2 >> d3 >> stmp;
    oObject->orientation[0] = d0; oObject->orientation[1] = d1;
    oObject->orientation[2] = d2; oObject->orientation[3] = d3;

    ifs >> stmp >> stmp >> stmp >> d0 >> d1 >> d2 >> stmp;
    oObject->bbox_min[0] = d0; oObject->bbox_min[1] = d1; oObject->bbox_min[2] = d2;
    ifs >> stmp >> stmp >> stmp >> d0 >> d1 >> d2 >> stmp;
    oObject->bbox_max[0] = d0; oObject->bbox_max[1] = d1; oObject->bbox_max[2] = d2;
    int i0;
    ifs >> stmp >> stmp >> i0 >> stmp;
    oObject->object_type = (int16_t)i0;

    ifs >> stmp >> stmp >> oObject->label >> stmp;
}


void IoUtilities::loadObjectModelsFromMatlabFile(const string &iFilename,
                                                 Reacquisition &iReacq)
{
    ifstream ifs(iFilename.c_str());
    if (!ifs.good()) {
        fprintf (stderr, "loadObjectModelsFromMatlabFile: Error opening file %s\n",
                 iFilename.c_str());
        return;
    }

    string stmp;
    int nmodels;
    ifs >> stmp >> stmp >> stmp >> nmodels;
    
    lcm_t *lcm = bot_lcm_get_global (NULL);
    BotParam *param = bot_param_new_from_server (lcm, 0);

    for (int i=0; i<nmodels; ++i) {
        ObjectModel *mod = new ObjectModel();
        int64_t id;
        ifs >> stmp >> stmp >> id >> stmp;
        IoUtilities::loadPosesFromMatlabFile(ifs,
                                             mod->mCameraPoses);
        IoUtilities::loadModelPointsFromMatlabFile(ifs,
                                                   mod->mObjectPoints);
        IoUtilities::loadMeasurementsFromMatlabFile(ifs,
                                                    mod->mObjectMeasurements);
        IoUtilities::loadModelPointsFromMatlabFile(ifs,
                                                   mod->mContextPoints);
        IoUtilities::loadMeasurementsFromMatlabFile(ifs,
                                                    mod->mContextMeasurements);
        IoUtilities::loadGestureFromMatlabFile(ifs,
                                               mod->mpGesture);
        string scam;
        ifs >> stmp >> stmp >> scam >> stmp;
        string cam = scam.substr(1, scam.length()-2);
        LensModel* lensModel = new LensModel();
        lensModel->setFromParam(param, cam.c_str());
        lensModel->setName(cam);
        mod->mpLensModel = lensModel;
        IoUtilities::loadObjectFromMatlabFile(ifs, mod->mpObject);
        if (i < (nmodels - 1)) {
            iReacq.AddModel(mod, false);
        } else {
            iReacq.AddModel(mod, true);
        }
    }
    
}

string IoUtilities::objectModelToMatlabString(const string &iVarName,
                                              const int iIndex,
                                              const int64_t iId,
                                              const ObjectModel &iModel)
{
    ostringstream oss;
    ostringstream tmp;
    tmp << iVarName << "(" << iIndex << ").";
    string vind = tmp.str();
    oss << vind << "id = " << iId << " ;" << endl;
    oss << "% " << vind << "cameraPoses.size() = "
        << iModel.mCameraPoses.size() << endl;
    oss << vind << "cameraPoses = [ " << endl;
    oss << IoUtilities::posesToString(iModel.mCameraPoses);
    oss << " ];" << endl;
    oss << "% " << vind << "objectPoints.size() = "
        << iModel.mObjectPoints.size() << endl;
    oss << vind << "objectPoints = [ " << endl;
    oss << IoUtilities::modelPointsToString(iModel.mObjectPoints);
    oss << " ];" << endl;
    oss << "% " << vind << "objectMeasurements.size() = "
        << iModel.mObjectMeasurements.size() << endl;
    oss << vind << "objectMeasurements = [ " << endl;
    oss << IoUtilities::measurementsToString(iModel.mObjectMeasurements);
    oss << " ];" << endl;
    oss << "% " << vind << "contextPoints.size() = "
        << iModel.mContextPoints.size() << endl;
    oss << vind << "contextPoints = [ " << endl;
    oss << IoUtilities::modelPointsToString(iModel.mContextPoints);
    oss << " ];" << endl;
    oss << "% " << vind << "contextMeasurements.size() = "
        << iModel.mContextMeasurements.size() << endl;
    oss << vind << "contextMeasurements = [ " << endl;
    oss << IoUtilities::measurementsToString(iModel.mContextMeasurements);
    oss << " ];" << endl;
    oss << IoUtilities::reacquisitionSegmentationToString(
        vind + "gesture",
        iModel.mpGesture);
    oss << vind << "cameraName = "
        << "'" << iModel.mpLensModel->getName() << "' ;" << endl;
    string pind(vind);
    pind += "object";
    oss << IoUtilities::objectToMatlabString(pind, iModel.mpObject);
    return oss.str();
}

string IoUtilities::pointlistToMatlabString(const std::string &iVarName,
                                            const erlcm_point2d_list_t &iPts,
                                            const int64_t iImageUtime,
                                            const string &iCameraName)
{
    ostringstream oss;
    oss << iVarName << ".camera = '" << iCameraName << "';" << endl;
    oss << iVarName << ".image_utime = " << iImageUtime << ";" << endl;
    oss << iVarName << ".pts = [" << endl;
    for (int i=0; i<iPts.npoints; ++i) {
        oss << iPts.points[i].x << " " << iPts.points[i].y << endl;
    }
    oss << "];" << endl;    

    return oss.str();

}
    
                                               

void IoUtilities::lcmglPublishGesture(const string &iChannel,
                                      const erlcm_point2d_list_t &iRoi,
                                      float iRed,
                                      float iGreen,
                                      float iBlue,
                                      float iLineWidth,
                                      float iXscale,
                                      float iYscale)
{
    if (0 == iRoi.npoints) {
        return;
    }
    for (int i=0; i<iRoi.npoints; ++i) {
        if ((iRoi.points[i].x != iRoi.points[i].x) ||
            (iRoi.points[i].x != iRoi.points[i].x)) {
            cerr << "ERROR: Could not publish lcmgl gesture due to nan value" << endl
                 << "       iRoi.points[" << i << "] = {"
                 << iRoi.points[i].x << ","
                 << iRoi.points[i].y << "}" << endl;
            return;
        }
    }
    cout << "Publishing gesture on channel [" << iChannel << "]" << endl;
    lcm_t *lcm = bot_lcm_get_global (NULL);
    bot_lcmgl_t *lcmgl = bot_lcmgl_init (lcm, iChannel.c_str());
    lcmglColor3f(iRed, iGreen, iBlue);
    lcmglLineWidth(iLineWidth);
    lcmglBegin(GL_LINE_LOOP);
    for (int i=0; i<iRoi.npoints; ++i) {
        lcmglVertex2f(static_cast<float>(iRoi.points[i].x*iXscale),
                      static_cast<float>(iRoi.points[i].y*iYscale));
    }
    lcmglEnd();
    bot_lcmgl_switch_buffer(lcmgl);
    //bot_lcmgl_destroy (lcmgl);
    //globals_release_lcmgl (lcmgl);
}

void IoUtilities::lcmglPublishPoints2d(const string &iChannel,
                                       const ModelPoints &iPoints,
                                       const Pose &iPose,
                                       const LensModel *iLensModel,
                                       float iRed,
                                       float iGreen,
                                       float iBlue,
                                       float iPointSize,
                                       float iXscale,
                                       float iYscale)
{
    if (iPoints.empty() || (NULL == iLensModel)) {
        return;
    }
    cout << "Publishing 2d points on channel [" << iChannel << "]" << endl;
    lcm_t *lcm = bot_lcm_get_global (NULL);
    bot_lcmgl_t *lcmgl = bot_lcmgl_init (lcm, iChannel.c_str());
    lcmglColor3f(iRed, iGreen, iBlue);
    lcmglPointSize(iPointSize);
    lcmglBegin(GL_POINTS);
    ModelPoints::const_iterator iter = iPoints.begin();
    Vector3 proj;
    for (;iter != iPoints.end(); ++iter) {
        if (Utilities::projectPointToImage((*iter)->mPoint,
                                           iPose.mOrientation,
                                           iPose.mPosition,
                                           iLensModel,
                                           proj)) {
            lcmglVertex2f(static_cast<float>(proj(0)*iXscale),
                          static_cast<float>(proj(1)*iYscale));
        }
    }
    
    lcmglEnd();
    bot_lcmgl_switch_buffer(lcmgl);
    //globals_release_lcmgl (lcmgl);
}


void IoUtilities::lcmglPublishPoints3d(const string &iChannel,
                                       const ModelPoints &iPoints,
                                       float iRed,
                                       float iGreen,
                                       float iBlue,
                                       float iPointSize)
{
    if (iPoints.empty()) {
        return;
    }
    cout << "Publishing 3d points on channel [" << iChannel << "]" << endl;
    lcm_t *lcm = bot_lcm_get_global (NULL);
    bot_lcmgl_t *lcmgl = bot_lcmgl_init (lcm, iChannel.c_str());
    lcmglColor3f(iRed, iGreen, iBlue);
    lcmglPointSize(iPointSize);
    lcmglBegin(GL_POINTS);
    ModelPoints::const_iterator iter = iPoints.begin();
    for (;iter != iPoints.end(); ++iter) {
        lcmglVertex3f(static_cast<float>((*iter)->mPoint(0)),
                      static_cast<float>((*iter)->mPoint(1)),
                      static_cast<float>((*iter)->mPoint(2)));

    }
    
    lcmglEnd();
    bot_lcmgl_switch_buffer(lcmgl);
    //globals_release_lcmgl (lcmgl);
}
