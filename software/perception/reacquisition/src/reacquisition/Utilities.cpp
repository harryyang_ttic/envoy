#include <iostream>
#include <fstream>
#include <limits>

#include <framewave/fwImage.h>

#include <bot_core/bot_core.h>
#include <image_utils/jpeg.h>
#include <image_utils/pixels.h>
#include <camunits/pixels.h>
#include <opencv_utils/opencv_utils.hpp>
#include <opencv/cv.h>

#include "Utilities.hpp"

using namespace reacq;
using namespace std;

#define FW_DEBUG 1

void Utilities::matchDescriptorsDistance(const vector<Descriptor>& iA,
                                         const vector<Descriptor>& iB,
                                         const float iThresh,
                                         vector<PointMatch>& oMatches) {
    int nA = iA.size();
    int nB = iB.size();

    float e, dist, best1, best2;
    int bestIndex;

    oMatches.reserve(nA);

    for (int i = 0; i < nA; ++i) {
        const float* d1 = iA[i].mData;
        best1 = 1e10;
        best2 = 1e10;

        for (int j = 0; j < nB; ++j) {
            const float* d2 = iB[j].mData;

            dist = 0;

            for (int k = 0; k < DESCRIPTOR_LENGTH; ++k) {
                e = d1[k] - d2[k];
                dist += e*e;
            }

            if (dist < best1) {
                best2 = best1;
                best1 = dist;
                bestIndex = j;
            }
            else if (dist < best2) {
                best2 = dist;
            }
        }

        if (best1*iThresh < best2) {
            PointMatch match;
            match.mIndex1 = i;
            match.mIndex2 = bestIndex;
            match.mScore = best1;
            oMatches.push_back(match);
        }
    }
}

void Utilities::matchDescriptorsDot(const vector<Descriptor>& iA,
                                    const vector<Descriptor>& iB,
                                    const float iThresh,
                                    vector<PointMatch>& oMatches) {
    int nA = iA.size();
    int nB = iB.size();

    float dot, best;
    int bestIndex;

    oMatches.reserve(nA);

    for (int i = 0; i < nA; ++i) {
        const float* d1 = iA[i].mData;
        best = -1;

        for (int j = 0; j < nB; ++j) {
            const float* d2 = iB[j].mData;

            dot = 0;

            for (int k = 0; k < DESCRIPTOR_LENGTH; ++k) {
                dot += d1[k]*d2[k];
            }

            if (dot > best) {
                best = dot;
                bestIndex = j;
            }
        }

        if (best >= iThresh) {
            PointMatch match;
            match.mIndex1 = i;
            match.mIndex2 = bestIndex;
            match.mScore = best;
            oMatches.push_back(match);
        }
    }    
}

vector<Descriptor>
Utilities::makeDescriptorsFromFeatureList(const sift_point_feature_list_t *iList)
{
    vector<Descriptor> vec(iList->num);
    for (int i=0; i<iList->num; ++i) {
        memcpy(&vec[i].mData, iList->pts[i].desc, DESCRIPTOR_LENGTH*sizeof(float));
    }
    return vec;

}

Descriptor
Utilities::copyDescriptor(const Descriptor &iDesc)
{
    Descriptor out;
    memcpy(&out.mData, &iDesc.mData, DESCRIPTOR_LENGTH*sizeof(float));
    return out;
}

void Utilities::rollPitchYawToRotationMatrix(const double iRPY[3],
                                             Matrix33& oMatrix) {
    double quat[4];
    double matx[9];
    bot_roll_pitch_yaw_to_quat(iRPY, quat);
    bot_quat_to_matrix(quat, matx);
    for (int i = 0, k = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j, ++k) {
            oMatrix(i,j) = matx[k];
        }
    }
}

void Utilities::rotationMatrixToRollPitchYaw(const Matrix33& iMatrix,
                                             double oRPY[3]) {
    double quat[4];
    double matx[9];
    for (int i = 0, k = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j, ++k) {
            matx[k] = iMatrix(i,j);
        }
    }
    bot_matrix_to_quat(matx, quat);
    bot_quat_to_roll_pitch_yaw(quat, oRPY);
}

void Utilities::bboxFromRoi(const erlcm_point2d_list_t &iRoi,
                            int32_t &oXmin, int32_t &oYmin,
                            int32_t &oXmax, int32_t &oYmax)
{
    oXmin = numeric_limits<int32_t>::max();
    oYmin = numeric_limits<int32_t>::max();
    oXmax = numeric_limits<int32_t>::min();
    oYmax = numeric_limits<int32_t>::min();
    for (int32_t i=0; i<iRoi.npoints; ++i) {
        if (iRoi.points[i].x < oXmin) oXmin = iRoi.points[i].x;
        if (iRoi.points[i].y < oYmin) oYmin = iRoi.points[i].y;
        if (iRoi.points[i].x > oXmax) oXmax = iRoi.points[i].x;
        if (iRoi.points[i].y > oYmax) oYmax = iRoi.points[i].y;
    }
}

Matrix44 Utilities::matrixFromRotationTranslation(const Matrix33& iRot,
                                                  const Vector3& iTrans) {
    Matrix44 matx;
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            matx(i,j) = iRot(i,j);
        }
    }
    for (int i = 0; i < 3; ++i) {
        matx(i,3) = iTrans(i);
    }
    matx(3,0) = matx(3,1) = matx(3,2) = 0;
    matx(3,3) = 1;
    return matx;
}

void Utilities::matrixToRotationTranslation(const Matrix44& iMatx,
                                            Matrix33& oRot,
                                            Vector3& oTrans) {
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            oRot(i,j) = iMatx(i,j);
        }
    }
    for (int i = 0; i < 3; ++i) {
        oTrans(i) = iMatx(i,3);
    }
}


Matrix44 Utilities::matrixFromArray(const double iArray[16]) {
    Matrix44 matx;
    const double* ptr = iArray;
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j, ++ptr) {
            matx(i,j) = *ptr;
        }
    }
    return matx;
}

bool Utilities::projectPointToImage(const Vector3& iPoint,
                                    const Matrix33& iRot,
                                    const Vector3& iTrans,
                                    const LensModel* iLens,
                                    Vector3& oResult) {
    Vector3 pt = iRot.getTranspose()*(iPoint-iTrans);
    bool ret = iLens->rayToPixel(pt, oResult);
    oResult(2) = pt(2);
    return (ret && (oResult(2) > 0));
}

void Utilities::allocateMultiScaleBuffers(const bot_core_image_t* iImage, //input image
                                          bot_core_image_t* oFullImage, //output full size gray in float
                                          bot_core_image_t* oResizedImage, //output gray resized in float
                                          bot_core_image_t* oRgbBuf, //temporary full size rgb in byte
                                          bot_core_image_t* oGrayBuf, //temporary full size gray in byte
                                          double xScale, double yScale)
{
    *oFullImage = *iImage;
    oFullImage->data = (uint8_t*)fwiMalloc_32f_C1(iImage->width, iImage->height, &oFullImage->row_stride);
    //oFullImage->data = (uint8_t*) cvCreateImage (cvSize(iImage->width, iImage->height), IPL_DEPTH_32F, oFullImage->row_stride);
    oFullImage->size = oFullImage->row_stride * oFullImage->height;
    oFullImage->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_FLOAT_GRAY32;
    int nx = iImage->width*xScale;
    int ny = iImage->height*yScale;
    *oResizedImage = *iImage;
    oResizedImage->width = nx;
    oResizedImage->height = ny;
    oResizedImage->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_FLOAT_GRAY32;
    oResizedImage->data = (uint8_t*)fwiMalloc_32f_C1(nx, ny, &oResizedImage->row_stride);
    fprintf (stdout, "After allocation: pixel format = %d\n", oResizedImage->pixelformat);
    //oResizedImage->data = (uint8_t*) cvCreateImage (cvSize(nx, ny), IPL_DEPTH_32F, oResizedImage->row_stride);
    oResizedImage->size = oResizedImage->row_stride * oResizedImage->height;
    *oRgbBuf = *iImage;
    oRgbBuf->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB;
    oRgbBuf->data = fwiMalloc_8u_C3(iImage->width, iImage->height, &oRgbBuf->row_stride);
    //oRgbBuf->data = (uint8_t *)cvCreateImage( cvSize(iImage->width, iImage->height), IPL_DEPTH_8U, oRgbBuf->row_stride);
    oRgbBuf->size = oRgbBuf->row_stride * oRgbBuf->height;
    *oGrayBuf = *iImage;
    oGrayBuf->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY;
    oGrayBuf->data = fwiMalloc_8u_C1(iImage->width, iImage->height, &oGrayBuf->row_stride);
    //oGrayBuf->data = (uint8_t *) cvCreateImage( cvSize(iImage->width, iImage->height), IPL_DEPTH_8U, oGrayBuf->row_stride);
    oGrayBuf->size = oGrayBuf->row_stride * oGrayBuf->height;
}

void Utilities::duplicateAlignedFloatImage(const bot_core_image_t* iImage,
                                           bot_core_image_t* oImage)
{
    *oImage = *iImage;
    oImage->data = (uint8_t*)fwiMalloc_32f_C1(iImage->width, iImage->height, &oImage->row_stride);
    //oImage->data = (uint8_t*)cvCreateImage (cvSize(iImage->width, iImage->height), IPL_DEPTH_32F, oImage->row_stride);

}

void Utilities::freeAlignedImage(bot_core_image_t* iImage)
{
    fwiFree(iImage->data);
    //cvReleaseImage (iImage->data);
    iImage->data = NULL;
}


void Utilities::decompressToMultiScale(bot_core_image_t* iImage,
                                       bot_core_image_t* oFullImage,
                                       bot_core_image_t* oResizedImage,
                                       bot_core_image_t* oRgbBuf,
                                       bot_core_image_t* oGrayBuf)
{
    
    jpeg_decompress_8u_rgb (iImage->data, iImage->size,
                            oRgbBuf->data, iImage->width,
                            iImage->height, oRgbBuf->row_stride);


    FwiSize roiSize = { iImage->width, iImage->height };
    fprintf(stderr, "Size : %d, %d\n", iImage->width, iImage->height);
    //fwiRGBToGray_8u_C3C1R(oRgbBuf->data, oRgbBuf->row_stride, oGrayBuf->data, oGrayBuf->row_stride, roiSize);
    cam_pixel_convert_8u_rgb_to_8u_gray(oGrayBuf->data, oGrayBuf->row_stride,  iImage->width, iImage->height , oRgbBuf->data, oRgbBuf->row_stride);

    //fwiConvert_8u32f_C1R(oGrayBuf->data, oGrayBuf->row_stride, (float*)oFullImage->data, oFullImage->row_stride, roiSize);
    cam_pixel_convert_8u_gray_to_32f_gray ((float*)oFullImage->data, oFullImage->row_stride, iImage->width, iImage->height , oGrayBuf->data, oGrayBuf->row_stride);
    
    FwiRect srcRoi = { 0, 0, iImage->width, iImage->height };
    FwiSize dstRoiSize = { oResizedImage->width, oResizedImage->height };
    
    double xScale = static_cast<double>(oResizedImage->width) / iImage->width;
    double yScale = static_cast<double>(oResizedImage->height) / iImage->height;

    int interpMethod = FWI_INTER_LINEAR;

    
    cv::Mat fullMat = opencv_utils::get_opencv_header_for_bot_core_image_t (oFullImage);
    cv::Mat resizedMat = opencv_utils::get_opencv_header_for_bot_core_image_t (oResizedImage);
    cv::resize (fullMat, resizedMat, cvSize (oResizedImage->width, oResizedImage->height), 0, 0, CV_INTER_LINEAR);
    
    //fwiCopy_32f_C1R((float*)resizedMat.data, oResizedImage->row_stride, (float*)oResizedImage->data,
    //                oResizedImage->width*sizeof(float), dstRoiSize);
    //fwiResize_32f_C1R ((float*)oFullImage->data, roiSize, oFullImage->row_stride,
    //                   srcRoi, (float*)oResizedImage->data, oResizedImage->row_stride, dstRoiSize,
    //                   xScale, yScale, interpMethod);

    
    //fwiCopy_32f_C1R((float*)oFullImage->data, oFullImage->row_stride, (float*)oFullImage->data,
    //                oFullImage->width*sizeof(float), roiSize);

    //fwiCopy_32f_C1R((float*)oResizedImage->data, oResizedImage->row_stride, (float*)oResizedImage->data,
    //                oResizedImage->width*sizeof(float), dstRoiSize);

}
                                       
                                       
void
Utilities::scaleSiftFeatures(sift_point_feature_list_t *ioList,
                             double iScaleX, double iScaleY)
{
    for (int i=0; i<ioList->num; ++i) {
        ioList->pts[i].col *= iScaleX;
        ioList->pts[i].row *= iScaleY;
    }

}

uint64_t 
Utilities::getUniqueId (void)
{
    return (0xefffffffffffffff&(bot_timestamp_now()<<8)) + 256*rand()/RAND_MAX;
}
