#include "RobustLM.hpp"

using namespace reacq;

RobustLM::Problem::Problem() {
}

MatrixDynamic
RobustLM::Problem::computeJacobian(const MatrixDynamic& iParams) const {
    MatrixDynamic errorMinus = computeErrors(iParams);

    MatrixDynamic jac(errorMinus.getNumElements(),
                      iParams.getNumElements());

    for (int i = 0; i < iParams.getNumElements(); ++i) {
        MatrixDynamic paramPlus(iParams);
        double step = std::max(1e-6, 1e-4*iParams(i));
        paramPlus(i) += step;
        MatrixDynamic errorPlus = computeErrors(paramPlus);
        MatrixDynamic derivCol = (errorPlus - errorMinus)/step;

        for (int j = 0; j < errorMinus.getNumElements(); ++j) {
            jac(j,i) = derivCol(j);
        }
    }

    return jac;
}


RobustLM::RobustLM() {
    setMaxIterations(100);
    setMinCostChangeFactor(1e-6);
    setMinParameterChangeFactor(1e-6);
    setComputeCovariance(false);
    setVerbose(true);
}

RobustLM::~RobustLM() {
}

RobustLM::Result RobustLM::solve(Problem& iProblem) {
    NoWeighting weighting;
    return solve(iProblem, weighting);
}

RobustLM::Result RobustLM::solve(Problem& iProblem,
                                 RobustWeighting& iWeighting) {
    MatrixDynamic x = iProblem.getParameters();
    MatrixDynamic e = iProblem.computeErrors(x);
    MatrixDynamic w;

    bool success = true;
    double lambda = 1e-3;

    MatrixDynamic jacobian2;

    int iter;
    for (iter = 0; iter < mMaxIterations; ++iter) {

        // Compute weights
        iWeighting.computeWeightParams(e);
        w = iWeighting.computeWeights(e);

        // Compute error from previous step
        double prevCost = iWeighting.computeCost(e);
        if (mVerbose) {
            std::cout << "ITER " << iter << " " << prevCost << std::endl;
        }

        //
        // Set up matrix equations
        //

        // Form and weight the Jacobian
        MatrixDynamic jacobian = iProblem.computeJacobian(x);
        for (int m = 0; m < jacobian.getNumRows(); ++m) {
            for (int n = 0; n < jacobian.getNumCols(); ++n) {
                jacobian(m,n) *= w(m);
            }
        }

        // Form left-hand matrix elements
        MatrixDynamic jacobianTranspose = jacobian.getTranspose();
        jacobian2 = jacobianTranspose*jacobian;
        MatrixDynamic ident(jacobian2.getNumRows(), jacobian2.getNumCols());
        ident.makeIdentity();

        // Weight the errors
        MatrixDynamic we(w);
        for (int k = 0; k < we.getNumElements(); ++k) {
            we(k) *= e(k);
        }

        MatrixDynamic rhs = jacobianTranspose*(we);

        // Repeat until cost decreases
        double dCost = 1e10;
        MatrixDynamic xTest;
        MatrixDynamic eTest;
        while (dCost > 0) {
            MatrixDynamic lhs = jacobian2 + ident*lambda;
            MatrixDynamic delta = lhs.solve(rhs);

            // Determine error
            xTest = x - delta;
            eTest = iProblem.computeErrors(xTest);
            double currCost = iWeighting.computeCost(eTest);

            dCost = currCost - prevCost;
            if (dCost > 0) {
                lambda *= 10;
            }
        }

        lambda /= 10;
        x = xTest;
        e = eTest;

        // Check stopping conditions
        if (-dCost < prevCost*mMinCostChangeFactor) {
            break;
        }
        bool shouldStop = true;
        for (int k = 0; k < x.getNumElements(); ++k) {
            if (fabs(x(k)) > x(k)*mMinParameterChangeFactor) {
                shouldStop = false;
                break;
            }
        }
        if (shouldStop) {
            break;
        }
    }

    Result result;
    result.mParameters = x;
    result.mErrors = e;
    result.mWeights = w;
    result.mSuccess = success;
    result.mIterations = iter+1;
    if (mComputeCovariance) {
        MatrixDynamic u, s, v;
        jacobian2.svd(u,s,v);
        for (int i = 0; i < s.getNumRows(); ++i) {
            if (s(i,i) > 1e-8) {
                s(i,i) = 1/s(i,i);
            }
        }
        result.mCovariance = u*s*v.getTranspose();
    }

    return result;

}
