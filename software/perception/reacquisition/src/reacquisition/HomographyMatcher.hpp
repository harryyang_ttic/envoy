#ifndef REACQUIRE_HOMOGRAPHY_MATCHER_H
#define REACQUIRE_HOMOGRAPHY_MATCHER_H

#include <bot_core/bot_core.h>
#include <glib.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <string>

namespace reacq {

typedef struct _match_pair_t
{
    double x1; // model image point x
    double y1; // model image point y
    BotCamTrans *camtransModel;
    double x2; // current image point x
    double y2; // current image point y
    BotCamTrans *camtransCurrent;
    int group_id; // 0 = unused, 1+ = group id
    /* distance between points
       (N.B. can have multiple meanings depending on usage)
    */
    double dist;
} match_pair_t;

    
class HomographyMatcher {

public:
    
    HomographyMatcher(int iDebug,
                      int iNumIter,
                      double iDistThreshPix);

    ~HomographyMatcher();

    int Run(double *iHomog,
            int iGroupId,
            GArray *iMatches);
//             ,BotCamTrans *iCamtrans);

protected:

private:

    void PrettyPrintMatrix(const gsl_matrix* iMatrix,
                           const std::string &iName);

    int mNumIter;
    double mDistThreshPix;

    BotCamTrans *mCamtrans;

    // random number generator
    GRand *mRand;

    // debug level
    int mDebug;
    
    // SVD buffers
    gsl_matrix *mA;
    gsl_matrix *mV;
    gsl_vector *mS;
    gsl_vector *mWork;
    gsl_matrix *mH;
    
}; // class HomographyMatcher
    
} // namespace reacq

#endif //REACQUIRE_HOMOGRAPHY_MATCHER_H
