#include "BundleResection.hpp"

#include "Utilities.hpp"

using namespace std;
using namespace reacq;

BundleResection::Problem::Problem(Measurements &iMeasurements,
                                  ModelPoints &iModelPoints,
                                  LensModel *iLensModel) :
    mMeasurements(iMeasurements),
    mModelPoints(iModelPoints),
    mLensModel(iLensModel) {
    }


MatrixDynamic
BundleResection::Problem::computeErrors(const MatrixDynamic& iParams) const {
    Pose pose;
    BundleResection::poseFromVector(iParams, pose);

    MatrixDynamic errors(2*mMeasurements.size());
    Matrix33 rot = pose.mOrientation.getTranspose();
    for (int i = 0; i < mMeasurements.size(); ++i) {
        // transform to camera coords
        Vector3 pt = mModelPoints[mMeasurements[i]->mModelPointIndex]->mPoint -
            pose.mPosition;
        pt = rot*pt;

        // project onto camera
        double pix[2];
        if (!mLensModel->rayToPixel(pt, pix[0], pix[1])) {
            pix[0] = pix[1] = 1.e10;
        }
        // compute difference
        errors(2*i) = pix[0] - mMeasurements[i]->mImagePoint(0);
        errors(2*i+1) = pix[1] - mMeasurements[i]->mImagePoint(1);
    }

    return errors;
}

BundleResection::BundleResection() {
}

void BundleResection::poseFromVector(const MatrixDynamic& iParameters,
                                     Pose& oPose) {
    double rpy[3];
    rpy[0] = iParameters(0);
    rpy[1] = iParameters(1);
    rpy[2] = iParameters(2);
    Utilities::rollPitchYawToRotationMatrix(rpy, oPose.mOrientation);
    oPose.mPosition(0) = iParameters(3);
    oPose.mPosition(1) = iParameters(4);
    oPose.mPosition(2) = iParameters(5);
}

RobustLM::Result BundleResection::solve(Measurements &iMeasurements,
                                        ModelPoints &iModelPoints,
                                        Pose& iPose,
                                        LensModel *iLensModel) {

    // push initial pose parameters into vector
    MatrixDynamic initParameters(6);
    double rpy[3];
    Utilities::rotationMatrixToRollPitchYaw(iPose.mOrientation, rpy);
    initParameters(0) = rpy[0];
    initParameters(1) = rpy[1];
    initParameters(2) = rpy[2];
    initParameters(3) = iPose.mPosition(0);
    initParameters(4) = iPose.mPosition(1);
    initParameters(5) = iPose.mPosition(2);

    // solve problem
    Problem problem(iMeasurements, iModelPoints, iLensModel);
    problem.setParameters(initParameters);
    TukeyWeighting weighting;
    weighting.setMinSigma(0.5);
    RobustLM lm;
    lm.setComputeCovariance(true);
    lm.setVerbose(false);
    RobustLM::Result result = lm.solve(problem, weighting);

    // grab parameters from vector
    poseFromVector(result.mParameters, iPose);


    return result;
}
