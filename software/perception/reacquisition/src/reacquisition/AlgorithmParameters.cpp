#include "AlgorithmParameters.hpp"

using namespace reacq;

const double AlgorithmParameters::mSmallImageScale = 0.5;
const int    AlgorithmParameters::mSiftLevels = 3;
const double AlgorithmParameters::mSiftThreshObject = 0;
//const double AlgorithmParameters::mSiftThreshContext = 15;
const double AlgorithmParameters::mSiftThreshContext = 0.15;
const int    AlgorithmParameters::mSiftDoubleImageSize = 0;
const double AlgorithmParameters::mSiftSigmaInit = 1.6;
const int    AlgorithmParameters::mSiftMaxNumMatches = 300;
const double AlgorithmParameters::mSummoningDistance = 4.5;
