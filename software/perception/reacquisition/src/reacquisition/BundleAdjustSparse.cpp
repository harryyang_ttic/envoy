#include "BundleAdjustSparse.hpp"

#include "Utilities.hpp"

using namespace std;
using namespace reacq;

BundleAdjustSparse::BundleAdjustSparse() {
    setRobustKernel(RobustKernelNop);
    setMaxIterations(200);
    setMinCostChangeFactor(1e-6);
    setMinParameterChangeFactor(1e-6);
    setVerbose(true);
}

BundleAdjustSparse::~BundleAdjustSparse() {
}

BundleAdjustSparse::Result BundleAdjustSparse::
solve(const Measurements &iMeasurements,
      Poses &ioPoses,
      ModelPoints &ioModelPoints,
      const LensModel *iLensModel) {

    // initialize internal data structures and parameter vector mX
    createStructures(iMeasurements, ioPoses, ioModelPoints, *iLensModel);

    // solve
    Result result;
    switch (mRobustKernel) {
    case RobustKernelTukey:
        result = solveInternal<RobustTukey>();
        break;
    default:
        result = solveInternal<RobustNop>();
        break;
    }

    // copy back data structures
    for (int i = 0; i < mPointData.size(); ++i) {
        ioModelPoints[i]->mPoint = mPointData[i]->mPoint;
        ioModelPoints[i]->mCovariance = mPointData[i]->mCovariance;
    }
    for (int j = 0; j < mPoseData.size(); ++j) {
        ioPoses[j]->mOrientation = mPoseData[j]->mOrientation;
        ioPoses[j]->mPosition = mPoseData[j]->mPosition;
        ioPoses[j]->mCovariance = mPoseData[j]->mCovariance;
    }

    return result;
}


template<typename RobustT>
BundleAdjustSparse::Result BundleAdjustSparse::solveInternal() {

    // compute initial error vector
    MatrixDynamic error = computeError(mX);

    // initialize lambda to nominal value
    mLambda = 1e-3;

    // initialize other variables
    bool success = true;

    // iterate
    int iter;
    for (iter = 0; iter < mMaxIterations; ++iter) {

        // square the error
        MatrixDynamic error2(error.getNumElements());
        for (int k = 0; k < error.getNumElements(); ++k) {
            error2(k) = error(k)*error(k);
        }

        // create robust weighting object
        RobustT weighting(error2, 0.5);

        // compute current cost
        double costPrev = 0;
        for (int k = 0; k < error2.getNumElements(); ++k) {
            costPrev += weighting.computeCost(error2(k));
        }
        if (mVerbose) {
            cout << "iter=" << iter << " cost=" << costPrev <<
                " lambda=" << mLambda << endl;
        }

        // compute outside-lambda-loop updates
        initializeStructures(weighting);

        // initialize lambda to 0.001*max(diag(J'J)) on first iteration
        if (iter == 0) {
            mLambda = 0;
            for (int i = 0; i < mPointData.size(); ++i) {
                for (int k = 0; k < 3; ++k) {
                    mLambda = std::max(mLambda, mPointData[i]->mV(k,k));
                }
            }
            for (int j = 0; j < mPoseData.size(); ++j) {
                for (int k = 0; k < 6; ++k) {
                    mLambda = std::max(mLambda, mPoseData[j]->mU(k,k));
                }
            }
            mLambda = 1e-3*mLambda;
        }

        // lambda update loop; iterate until error does not decrease
        double costDelta = 1e10;
        MatrixDynamic xTest;
        MatrixDynamic errorTest;
        while (costDelta > 0) {

            // finish incremental update of all sparse structures
            completeStructures();

            // update parameters
            MatrixDynamic delta = computeUpdates();
            xTest = mX+delta;

            // compute tentative cost
            errorTest = computeError(xTest);
            for (int k = 0; k < errorTest.getNumElements(); ++k) {
                error2(k) = errorTest(k)*errorTest(k);
            }
            double costCurr = 0;
            for (int k = 0; k < error2.getNumElements(); ++k) {
                costCurr += weighting.computeCost(error2(k));
            }

            // if cost increases, then increase lambda
            costDelta = costCurr - costPrev;
            if (costDelta > 0) {
                mLambda *= 1.5;
            }
        }

        mLambda /= 10;
        mX = xTest;
        error = errorTest;

        // check whether cost has decreased by too small an amount
        if ((fabs(costDelta) < costPrev*mMinCostChangeFactor) ||
            (fabs(costDelta) < 1e-12)) {
            if (mVerbose) {
                cout << "Stopped due to cost change " << costDelta << endl;
            }
            break;
        }

        // check whether no parameters have changed by more than threshold
        bool shouldStop = true;
        for (int k = 0; k < mX.getNumElements(); ++k) {
            if (fabs(mX(k)) > mX(k)*mMinParameterChangeFactor) {
                shouldStop = false;
                break;
            }
        }
        if (shouldStop) {
            if (mVerbose) {
                cout << "Stopped due to param change " << endl;
            }
            break;
        }
    }

    // compute covariance
    computeCovariances();

    // copy final weights
    MatrixDynamic weights(2*mMeasurementData.size());
    for (int m = 0; m < mMeasurementData.size(); ++m) {
        weights(2*m)   = mMeasurementData[m]->mWeight(0);
        weights(2*m+1) = mMeasurementData[m]->mWeight(1);
    }

    // copy final parameters
    for (int i = 0; i < mPointData.size(); ++i) {
        for (int k = 0; k < 3; ++k) {
            mPointData[i]->mPoint(k) = mX(6*mPoseData.size() + 3*i + k);
        }
    }
    for (int j = 0; j < mPoseData.size(); ++j) {
        double rpy[] = { mX(6*j), mX(6*j+1), mX(6*j+2) };
        Utilities::rollPitchYawToRotationMatrix(rpy,
                                                mPoseData[j]->mOrientation);
        mPoseData[j]->mOrientation = mPoseData[j]->mOrientation.getTranspose();
        for (int k = 0; k < 3; ++k) {
            mPoseData[j]->mPosition(k) = mX(6*j+3+k);
        }
    }

    // populate optimization result object
    Result result;
    result.mParameters = mX;
    result.mErrors = error;
    result.mWeights = weights;
    result.mSuccess = success;
    result.mIterations = iter;

    return result;
}

void BundleAdjustSparse::createStructures(const Measurements& iMeasurements,
                                          const Poses& iPoses,
                                          const ModelPoints& iModelPoints,
                                          const LensModel& iLensModel) {
    mLensModel = iLensModel;
    int baseIndex = 6*iPoses.size();
    int numVariables = baseIndex + 3*iModelPoints.size();
    mX = MatrixDynamic(numVariables);

    // initialize parameter vector from points
    mPointData.resize(iModelPoints.size());
    for (int i = 0; i < iModelPoints.size(); ++i) {
        mPointData[i] = new PointData();
        mPointData[i]->mPoint = iModelPoints[i]->mPoint;
        for (int k = 0; k < 3; ++k) {
            mX(baseIndex + 3*i + k) = mPointData[i]->mPoint(k);
        }
    }

    // initialize parameter vector from poses
    mPoseData.resize(iPoses.size());
    for (int j = 0; j < iPoses.size(); ++j) {
        mPoseData[j] = new PoseData();
        mPoseData[j]->mOrientation = iPoses[j]->mOrientation.getTranspose();
        mPoseData[j]->mPosition = iPoses[j]->mPosition;
        double rpy[3];
        Utilities::rotationMatrixToRollPitchYaw(mPoseData[j]->mOrientation,
                                                rpy);
        for (int k = 0; k < 3; ++k) {
            mX(6*j + k) = rpy[k];
        }
        for (int k = 0; k < 3; ++k) {
            mX(6*j + 3+k) = mPoseData[j]->mPosition(k);
        }
    }

    // create measurement pointer map
    mMeasurementMap.resize(iModelPoints.size());
    for (int i = 0; i < mMeasurementMap.size(); ++i) {
        mMeasurementMap[i].resize(iPoses.size());
        for (int j = 0; j < mMeasurementMap[i].size(); ++j) {
            mMeasurementMap[i][j] = MeasurementData::Ptr(NULL);
        }
    }

    // set up measurement data
    mMeasurementData.resize(iMeasurements.size());
    for (int m = 0; m < iMeasurements.size(); ++m) {
        mMeasurementData[m] = new MeasurementData();
        int ipt = iMeasurements[m]->mModelPointIndex;
        int ipose = iMeasurements[m]->mPoseIndex;
        mMeasurementData[m]->mPixel = iMeasurements[m]->mImagePoint;
        mMeasurementData[m]->mPoseIndex = ipose;
        mMeasurementData[m]->mPointIndex = ipt;
        mMeasurementMap[ipt][ipose] = mMeasurementData[m];
    }
}

template<typename RobustT>
void BundleAdjustSparse::initializeStructures(RobustT& iWeighting) {

    // set up point-related data
    for (int i = 0; i < mPointData.size(); ++i) {

        // set 3d point
        for (int k = 0; k < 3; ++k) {
            mPointData[i]->mPoint(k) = mX(6*mPoseData.size() + 3*i + k);
        }

        // zero structures
        mPointData[i]->mV.zero();
        mPointData[i]->mErrorProj.zero();
    }


    // set up pose-related data
    for (int j = 0; j < mPoseData.size(); ++j) {

        // set rotation matrix
        double rpy[] = { mX(6*j), mX(6*j+1), mX(6*j+2) };
        Utilities::rollPitchYawToRotationMatrix(rpy,
                                                mPoseData[j]->mOrientation);

        // set position vector
        for (int k = 0; k < 3; ++k) {
            mPoseData[j]->mPosition(k) = mX(6*j+3+k);
        }

        // zero structures
        mPoseData[j]->mU.zero();
        mPoseData[j]->mErrorProj.zero();
    }


    // set up measurement-related data
    for (int m = 0; m < mMeasurementData.size(); ++m) {
        int ipose = mMeasurementData[m]->mPoseIndex;
        int ipt = mMeasurementData[m]->mPointIndex;

        // compute error for this measurement
        Vector2 error = computeError(mPoseData[ipose]->mOrientation,
                                     mPoseData[ipose]->mPosition,
                                     mPointData[ipt]->mPoint,
                                     mMeasurementData[m]->mPixel);
        mMeasurementData[m]->mError = error;

        Vector3 xOrientation;
        for (int k = 0; k < 3; ++k) {
            xOrientation(k) = mX(6*ipose + k);
        }

        // calculate derivatives
        Matrix26 A = computeJacobianPose(xOrientation,
                                         mPoseData[ipose]->mOrientation,
                                         mPoseData[ipose]->mPosition,
                                         mPointData[ipt]->mPoint,
                                         mMeasurementData[m]->mPixel,
                                         mMeasurementData[m]->mError);
        Matrix23 B = computeJacobianPoint(mPoseData[ipose]->mOrientation,
                                          mPoseData[ipose]->mPosition,
                                          mPointData[ipt]->mPoint,
                                          mMeasurementData[m]->mPixel,
                                          mMeasurementData[m]->mError);

        // compute weight
        // TODO: using both x and y weight simultaneously would be better
        Vector2 weight;
        weight(0) = iWeighting.computeWeight(error(0)*error(0));
        weight(1) = iWeighting.computeWeight(error(1)*error(1));

        // apply weight to error and jacobian
        for (int k = 0; k < 2; ++k) {
            error(k) *= weight(k);
            for (int l = 0; l < 6; ++l) {
                A(k,l) *= weight(k);
            }
            for (int l = 0; l < 3; ++l) {
                B(k,l) *= weight(k);
            }
        }

        // set measurement structures
        mMeasurementData[m]->mWeight = weight;
        mMeasurementData[m]->mA = A;
        mMeasurementData[m]->mB = B;
        Matrix62 transA = A.getTranspose();
        Matrix32 transB = B.getTranspose();
        mMeasurementData[m]->mW = transA*B;

        // aggregate sums
        mPoseData[ipose]->mU += transA*A;
        mPoseData[ipose]->mErrorProj += transA*error;
        mPointData[ipt]->mV += transB*B;
        mPointData[ipt]->mErrorProj += transB*error;
    }
}

void BundleAdjustSparse::completeStructures() {
    // add scaled identity matrices
    Matrix66 ident6;
    Matrix33 ident3;
    ident6.makeIdentity();
    ident3.makeIdentity();
    ident6 *= mLambda;
    ident3 *= mLambda;
    for (int j = 0; j < mPoseData.size(); ++j) {
        mPoseData[j]->mStarU = mPoseData[j]->mU + ident6;
        mPoseData[j]->mRHS.zero();
    }
    for (int i = 0; i < mPointData.size(); ++i) {
        mPointData[i]->mInvStarV =
            (mPointData[i]->mV + ident3).getPseudoInverse();
        mPointData[i]->mRHS.zero();
    }

    // set up squared constraint matrix for pose deltas
    mS = MatrixDynamic(6*mPoseData.size(), 6*mPoseData.size());
    mS.zero();

    for (int m = 0; m < mMeasurementData.size(); ++m) {
        int ipose = mMeasurementData[m]->mPoseIndex;
        int ipt = mMeasurementData[m]->mPointIndex;

        // compute Y
        Matrix63 W = mMeasurementData[m]->mW;
        Matrix63 Y = W * mPointData[ipt]->mInvStarV;
        mMeasurementData[m]->mY = Y;

        // update S
        for (int k = 0; k < mPoseData.size(); ++k) {
            if (ipose == k) {
                Matrix66 matx = Y * W.getTranspose();
                for (int p = 0; p < 6; ++p) {
                    for (int q = 0; q < 6; ++q) {
                        mS(6*k+p, 6*k+q) -= matx(p,q);
                    }
                }
            }
            else {
                MeasurementData::Ptr meas = mMeasurementMap[ipt][k];
                if (meas == NULL) {
                    continue;
                }
                Matrix66 matx = Y * meas->mW.getTranspose();
                for (int p = 0; p < 6; ++p) {
                    for (int q = 0; q < 6; ++q) {
                        mS(6*ipose+p, 6*meas->mPoseIndex+q) -= matx(p,q);
                    }
                }
            }
        }

        // update pose delta constraint RHS
        mPoseData[ipose]->mRHS -= Y*mPointData[ipt]->mErrorProj;
    }

    // complete S matrix
    for (int j = 0; j < mPoseData.size(); ++j) {
        for (int p = 0; p < 6; ++p) {
            for (int q = 0; q < 6; ++q) {
                mS(6*j+p, 6*j+q) += mPoseData[j]->mStarU(p,q);
            }
        }
        mPoseData[j]->mRHS += mPoseData[j]->mErrorProj;
    }
}

MatrixDynamic BundleAdjustSparse::computeUpdates() {

    // solve for pose deltas
    MatrixDynamic rhs(6*mPoseData.size());
    for (int j = 0; j < mPoseData.size(); ++j) {
        for (int k = 0; k < 6; ++k) {
            rhs(6*j+k) = mPoseData[j]->mRHS(k);
        }
    }
    MatrixDynamic poseDelta = mS.solve(rhs);
    for (int j = 0; j < mPoseData.size(); ++j) {
        for (int k = 0; k < 6; ++k) {
            mPoseData[j]->mDelta(k) = poseDelta(6*j+k);
        }
    }

    // set up structures for point delta
    for (int m = 0; m < mMeasurementData.size(); ++m) {
        PoseData::Ptr pose = mPoseData[mMeasurementData[m]->mPoseIndex];
        PointData::Ptr pt = mPointData[mMeasurementData[m]->mPointIndex];
        pt->mRHS += mMeasurementData[m]->mW.getTranspose() * pose->mDelta;
    }

    // solve for point deltas
    for (int i = 0; i < mPointData.size(); ++i) {
        PointData::Ptr pt = mPointData[i];
        pt->mRHS = pt->mErrorProj - pt->mRHS;
        pt->mDelta = pt->mInvStarV * pt->mRHS;
    }

    // form concatenated delta
    MatrixDynamic delta(mX.getNumElements());
    for (int k = 0; k < poseDelta.getNumElements(); ++k) {
        delta(k) = poseDelta(k);
    }
    for (int i = 0; i < mPointData.size(); ++i) {
        for (int k = 0; k < 3; ++k) {
            delta(6*mPoseData.size() + 3*i + k) = mPointData[i]->mDelta(k);
        }
    }

    // TODO: why do we have to negate this?
    delta = -delta;

    return delta;
}

void BundleAdjustSparse::computeCovariances() {
    //
    // compute pose covariances
    //

    // recompute S with lambda=0
    mLambda = 0;
    completeStructures();  // TODO: can probably separate this out better

    // get full pose covariance and populate individual poses
    MatrixDynamic invS = mS.getPseudoInverse();
    for (int j = 0; j < mPoseData.size(); ++j) {
        for (int p = 0; p < 6; ++p) {
            for (int q = 0; q < 6; ++q) {
                mPoseData[j]->mCovariance(p,q) = invS(6*j+p, 6*j+q);
            }
        }
    }

    // compute point covariances
    MatrixDynamic fullY(6*mPoseData.size(), 3);
    for (int i = 0; i < mPointData.size(); ++i) {
        fullY.zero();
        for (int j = 0; j < mPoseData.size(); ++j) {
            MeasurementData::Ptr meas = mMeasurementMap[i][j];
            if (meas != NULL) {
                for (int p = 0; p < 6; ++p) {
                    for (int q = 0; q < 3; ++q) {
                        fullY(6*j+p,q) = meas->mY(p,q);
                    }
                }
            }
        }
        MatrixDynamic cov = fullY.getTranspose() * invS * fullY;
        for (int p = 0; p < 3; ++p) {
            for (int q = 0; q < 3; ++q) {
                mPointData[i]->mCovariance(p,q) = cov(p,q);
            }
        }
        mPointData[i]->mCovariance += mPointData[i]->mInvStarV;
    }    
}

MatrixDynamic BundleAdjustSparse::
computeError(const MatrixDynamic& iX) const {
    MatrixDynamic e(2*mMeasurementData.size());

    // cache rotations
    vector<Matrix33> rotations(mPoseData.size());
    for (int j = 0; j < mPoseData.size(); ++j) {
        double rpy[] = { iX(6*j+0), iX(6*j+1), iX(6*j+2) };
        Utilities::rollPitchYawToRotationMatrix(rpy, rotations[j]);
    }

    for (int m = 0; m < mMeasurementData.size(); ++m) {
        int ipose = mMeasurementData[m]->mPoseIndex;
        int ipt = mMeasurementData[m]->mPointIndex;
        Vector3 pt;
        for (int k = 0; k < 3; ++k) {
            pt(k) = iX(6*mPoseData.size() + 3*ipt + k);
        }
        Vector3 trans;
        for (int k = 0; k < 3; ++k) {
            trans(k) = iX(6*ipose + 3+k);
        }
        Vector3 proj = rotations[ipose] * (pt - trans);
        Vector2 pix;
        mLensModel.rayToPixel(proj, pix);
        Vector2 diff = mMeasurementData[m]->mPixel - pix;
        e(2*m+0) = diff(0);
        e(2*m+1) = diff(1);
    }

    return e;
}

Vector2 BundleAdjustSparse::
computeError(const Matrix33& iOrientation,
             const Vector3& iPosition,
             const Vector3& iPoint,
             const Vector2& iPixel) const {
    // project point
    Vector3 pt = iOrientation * (iPoint - iPosition);
    Vector2 pix;
    mLensModel.rayToPixel(pt, pix);

    // return difference between measured and projected points
    return (iPixel - pix);
}

BundleAdjustSparse::Matrix26 BundleAdjustSparse::
computeJacobianPose(const Vector3& iX,
                    const Matrix33& iOrientation,
                    const Vector3& iPosition,
                    const Vector3& iPoint,
                    const Vector2& iPixel,
                    const Vector2& iError) const {
    Matrix26 jac;

    // first the orientation parameters
    for (int k = 0; k < 3; ++k) {
        Vector3 xPlus = iX;
        double smallValue = std::max(xPlus(k)*1e-4, 1e-6);
        xPlus(k) += smallValue;
        double rpy[] = { xPlus(0), xPlus(1), xPlus(2) };
        Matrix33 orientation;
        Utilities::rollPitchYawToRotationMatrix(rpy, orientation);
        Vector2 ePlus = computeError(orientation, iPosition, iPoint, iPixel);
        Vector2 delta = (ePlus-iError)/smallValue;
        jac(0,k) = delta(0);
        jac(1,k) = delta(1);
    }

    // next the position parameters
    for (int k = 0; k < 3; ++k) {
        Vector3 xPlus = iPosition;
        double smallValue = std::max(xPlus(k)*1e-4, 1e-6);
        xPlus(k) += smallValue;
        Vector2 ePlus = computeError(iOrientation, xPlus, iPoint, iPixel);
        Vector2 delta = (ePlus-iError)/smallValue;
        jac(0,3+k) = delta(0);
        jac(1,3+k) = delta(1);
    }
    
    return jac;
}

BundleAdjustSparse::Matrix23 BundleAdjustSparse::
computeJacobianPoint(const Matrix33& iOrientation,
                     const Vector3& iPosition,
                     const Vector3& iPoint,
                     const Vector2& iPixel,
                     const Vector2& iError) const {

    Matrix23 jac;
    for (int k = 0; k < 3; ++k) {
        Vector3 xPlus = iPoint;
        double smallValue = std::max(xPlus(k)*1e-4, 1e-6);
        xPlus(k) += smallValue;
        Vector2 ePlus = computeError(iOrientation, iPosition, xPlus, iPixel);
        Vector2 delta = (ePlus-iError)/smallValue;
        jac(0,k) = delta(0);
        jac(1,k) = delta(1);
    }

    return jac;
}
