#ifndef _AGILE_REACQ_BUNDLEADJUST_H_
#define _AGILE_REACQ_BUNDLEADJUST_H_

#include "Measurement.hpp"
#include "RobustLM.hpp"
#include "LensModel.hpp"

namespace reacq {

class BundleAdjust {

public: 

    BundleAdjust();

    RobustLM::Result solve(Measurements &iMeasurements,
                           Poses &iPoses,
                           ModelPoints &iModelPoints,
                           LensModel *iLensModel);

    static bool initPoseFromPair(const std::vector<Vector2>& iPoints1,
                                 const std::vector<Vector2>& iPoints2,
                                 const LensModel* iLensModel,
                                 const double iDistanceThreshold,
                                 Pose& oPose);

    static bool intersectPair(const Vector2& iPoint1, const Vector2& iPoint2,
                              const Pose& iPose1, const Pose& iPose2,
                              const LensModel* iLensModel,
                              Vector3& oIntersection);

private:
    
    class Problem : public RobustLM::Problem {

    public:

        Problem(Measurements &iMeasurements,
                Poses& iPoses,
                ModelPoints& iModelPoints,
                LensModel *iLensModel);
        
        MatrixDynamic computeErrors(const MatrixDynamic& iParams) const;

    private:
        Measurements mMeasurements;
        Poses mPoses;
        ModelPoints mModelPoints;
        LensModel *mLensModel;
        
    };

private:

};

}

#endif // _AGILE_REACQ_BUNDLEADJUST_H_
