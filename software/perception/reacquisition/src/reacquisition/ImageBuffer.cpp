#include "ImageBuffer.hpp"

#include <iostream>
#include <limits>

#include <image_utils/jpeg.h>
#include <image_utils/pixels.h>
#include <camunits/pixels.h>

#ifndef M_PI
#define M_PI 3.14159265358979
#endif

namespace reacq {
    
ImageBuffer::ImageBuffer(const std::string &iPoseChannel,
                         int32_t iPoseCapacity,                
                         const std::string &iImageChannel,
                         int32_t iImageCapacity,
                         const double iDeltaPositionThreshold,
                         const double iDeltaThetaDegreesThreshold) :
    mPoseChannel(iPoseChannel), mPoseCapacity(iPoseCapacity),
    mImageChannel(iImageChannel), mImageCapacity(iImageCapacity),
    mDeltaPositionThreshold(iDeltaPositionThreshold),
    mDeltaThetaDegreesThreshold(iDeltaThetaDegreesThreshold),
    mRunning(false)
{
    if (!g_thread_supported ()) {
        g_thread_init (NULL);
    }
    mMutex = g_mutex_new ();
    mCondition = g_cond_new ();

    mLcm = bot_lcm_get_global (NULL);
    g_assert(mLcm);
    mImageSub = 
        bot_core_image_t_subscribe (mLcm, mImageChannel.c_str(),
                                    ImageBuffer::OnImage, this);

    fprintf(stderr,"Subscribing to : %s\n" , mImageChannel.c_str());
    
    mPoseSub = 
        bot_core_pose_t_subscribe (mLcm, mPoseChannel.c_str(),
                                   ImageBuffer::OnPose, this);
    
    std::cout << "Created ImageBuffer: " << std::endl
              << " * pose channel = " << iPoseChannel << std::endl
              << " * pose capacity = " << iPoseCapacity << std::endl
              << " * image channel = " << iImageChannel << std::endl
              << " * image capacity = " << iImageCapacity << std::endl
              << " * position threshold = " << iDeltaPositionThreshold << std::endl
              << " * angle threshold = " << iDeltaThetaDegreesThreshold << std::endl;
}

ImageBuffer::~ImageBuffer()
{
    if (NULL != mImageSub) {
        bot_core_image_t_unsubscribe(mLcm, mImageSub);
    }

    if (NULL != mPoseSub) {
        bot_core_pose_t_unsubscribe(mLcm, mPoseSub);
    }

    if (NULL != mCondition) {
        g_cond_free (mCondition);
    }

    if (NULL != mMutex) {
        g_mutex_free (mMutex);
    }

}

ImageWithPose
ImageBuffer::GetImageByIndex(int iIndex)
{
    Lock();
    ImageWithPose returnVal = GetImageWithoutLocking(iIndex);
    Unlock();
    return returnVal;
}

ImageWithPose
ImageBuffer::GetImageByTimestamp(int64_t iTimestamp)
{
    Lock();
    ImageWithPose returnVal;
    if (0 == GetNumImages()) {
        returnVal.image = NULL;
        returnVal.pose = NULL;
        Unlock();
        return returnVal;
    }
    int64_t minDt = std::numeric_limits<int64_t>::max();
    int iImage = GetNumImages() - 1;
    int iBestImage = -1;
    bool keepGoing = true;
    while (keepGoing) {
        int64_t dt = abs(iTimestamp - mImages[iImage].image->utime);

        if (dt == 0) {
            iBestImage = iImage;
            keepGoing = false;
        } else {
            if (dt < minDt) {
                minDt = dt;
                iBestImage = iImage;
            }
            iImage--;
            if (iImage < 0) {
                keepGoing = false;
            }
        }
    }
    returnVal.image = bot_core_image_t_copy(mImages[iBestImage].image);
    returnVal.pose = bot_core_pose_t_copy(mImages[iBestImage].pose);
    Unlock();

    //fprintf (stdout, "GetImageByTimestamp: Best requested - matched = %"PRId64" - %"PRId64" = %.4f\n",
    //       iTimestamp, mImages[iBestImage].image->utime, ((float) minDt)/1E6);
    return returnVal;
}

ImageWithPose
ImageBuffer::GetImageWithoutLocking(int iIndex)
{
    if (iIndex >= mImages.size()) {
        std::cerr << "WARNING: Trying to access image out of array bounds." << std::endl;
        ImageWithPose dummy;
        dummy.image = NULL;
        dummy.pose = NULL;
        return dummy;
    }
    ImageWithPose returnVal;
    returnVal.image = bot_core_image_t_copy(mImages[iIndex].image);
    returnVal.pose = bot_core_pose_t_copy(mImages[iIndex].pose);
    return returnVal;
}
    
void
ImageBuffer::Clear()
{
    Lock();
    std::deque<ImageWithPose>::iterator iter;
    for(iter = mImages.begin(); iter != mImages.end(); ++iter) {
        bot_core_image_t_destroy(iter->image);
        iter->image = NULL;
        bot_core_pose_t_destroy(iter->pose);
        iter->pose = NULL;
    }
    mImages.clear();
    Unlock();
}

void
ImageBuffer::Start()
{
    Lock();
    std::cout << "ImageBuffer::Start()" << std::endl;
    mRunning = true;
    Unlock();
}

void
ImageBuffer::Stop()
{
    Lock();
    std::cout << "ImageBuffer::Stop()" << std::endl;
    mRunning = false;
    Unlock();
}

void
ImageBuffer::GetAllTimestampsAndPoses(std::vector<int64_t> &oTimes,
                                      std::vector<bot_core_pose_t*> &oPoses)
{
    Lock();
    oTimes.resize(mImages.size());
    oPoses.resize(mImages.size());
    for (int i=0; i<mImages.size(); ++i) {
        oTimes[i] = mImages[i].image->utime;
        oPoses[i] = bot_core_pose_t_copy(mImages[i].pose);
    }
    Unlock();
}

void
ImageBuffer::OnImage(const lcm_recv_buf_t *iRbuf, const char *iChannel, 
                     const bot_core_image_t *iMsg, void *iUserData)
{
    ImageBuffer *imgBuffer = static_cast<ImageBuffer*>(iUserData);

    fprintf (stdout, "    [%s]: There are %d images in buffer\n", iChannel, imgBuffer->GetNumImages());
    imgBuffer->Lock();
    //Sachi
    fprintf(stderr,"Image rec : %0.6f\n", iMsg->utime / 1.0e6);
    std::cout << "Starting OnImage." << std::endl;
#ifdef BUFFER_DEBUG
    std::cout << "Starting OnImage." << std::endl;
#endif
    if (!imgBuffer->IsRunning()) {
#ifdef BUFFER_DEBUG
        std::cout << "Not running." << std::endl;
#endif
        imgBuffer->Unlock();
        return;
    }

    if (0 == imgBuffer->GetNumPoses()) {
        std::cerr << "WARNING: Could not add new image because pose buffer is empty." << std::endl;
        imgBuffer->Unlock();
        return;
    }

    if (imgBuffer->ImagesAreFull()) {
        imgBuffer->DropFirstImageWithPose();
#ifdef BUFFER_DEBUG
        std::cout << "OnImage: Buffer (" << imgBuffer->GetNumImages()
                  << ") full. Dropped image." << std::endl;
#endif
    }

    // Find closest pose times before and after input image
    int64_t closestTimeBefore = -LONG_MAX;
    bot_core_pose_t *closestPoseBefore = NULL;
    int64_t closestTimeAfter = LONG_MAX;
    bot_core_pose_t *closestPoseAfter = NULL;
    int64_t msgTime = iMsg->utime;
    bot_core_pose_t *bestPose = NULL;

    for (size_t i=0; i<imgBuffer->GetNumPoses(); ++i) {
        bot_core_pose_t *pose = imgBuffer->PeekAtPose(i);
        int64_t dt = pose->utime - msgTime;
        if (0 == dt) {
            bestPose = bot_core_pose_t_copy(pose);
#ifdef BUFFER_DEBUG
            std::cout << "-------> Found exact pose." << std::endl;
#endif
            break;
        }
        if ((dt < closestTimeAfter) && (dt > 0)) {
            closestTimeAfter = dt;
            closestPoseAfter = pose;
        }
        if ((dt > closestTimeBefore) && (dt < 0)) {
            closestTimeBefore = dt;
            closestPoseBefore = pose;
        }
    }
    closestTimeBefore = -closestTimeBefore;

    // Compute image pose as weighted average of closest poses 
    if (NULL == bestPose) {
        if (NULL == closestPoseBefore) {
            bestPose = bot_core_pose_t_copy(closestPoseAfter);
        } else if (NULL == closestPoseAfter) {
            bestPose = bot_core_pose_t_copy(closestPoseBefore);
        } else {
            double ddt = static_cast<double>(closestPoseAfter->utime -
                                             closestPoseBefore->utime);
            double scaleBefore = static_cast<double>(closestPoseAfter->utime - msgTime) / ddt;
            double scaleAfter = static_cast<double>(msgTime - closestPoseBefore->utime) / ddt;
            bestPose = (bot_core_pose_t *) calloc (1, sizeof(bot_core_pose_t));
            bestPose->utime = msgTime;
            for (int i=0; i<3; ++i) {
                bestPose->pos[i] = closestPoseBefore->pos[i]*scaleBefore +
                    closestPoseAfter->pos[i]*scaleAfter;
                bestPose->vel[i] = closestPoseBefore->vel[i]*scaleBefore +
                    closestPoseAfter->vel[i]*scaleAfter;
                bestPose->rotation_rate[i] = closestPoseBefore->rotation_rate[i]*scaleBefore +
                    closestPoseAfter->rotation_rate[i]*scaleAfter;
                bestPose->accel[i] = closestPoseBefore->accel[i]*scaleBefore +
                    closestPoseAfter->accel[i]*scaleAfter;
            }
#ifdef BUFFER_DEBUG
            std::cout << "ddt = " << ddt
                      << ", scaleBefore = " << scaleBefore
                      << ", scaleAfter = " << scaleAfter
                      << ", closestTimeBefore = " << closestTimeBefore
                      << ", closestTimeAfter = " << closestTimeAfter
                      << std::endl;
#endif
            for (int i=0; i<4; ++i) {
                bestPose->orientation[i] = closestPoseBefore->orientation[i]*scaleBefore +
                    closestPoseAfter->orientation[i]*scaleAfter;
            }
        }
    }

    // See if the closest pose is far enough to add the new image
    bool tooCloseFound = false;
    double negativeInputRpy[4];
    for (int i=0; i<3; ++i) {
        negativeInputRpy[i] = bestPose->orientation[i];
    }
    negativeInputRpy[3] = -bestPose->orientation[3];
    double combinedOrientation[4];
    double combinedRpy[3];

    int i=0;
    while (!tooCloseFound && (i < imgBuffer->GetNumImages())) {
        ImageWithPose img = imgBuffer->PeekAtImage(i);
        i++;
        double dx = bestPose->pos[0] - img.pose->pos[0];
        double dy = bestPose->pos[1] - img.pose->pos[1];
        if ((dx*dx + dy*dy) < imgBuffer->GetDeltaPositionThreshold()) {
#ifdef BUFFER_DEBUG
            std::cout << "Found distance too close: threshold = "
                      << imgBuffer->GetDeltaPositionThreshold() << ", distance = "
                      << (dx*dx + dy*dy)
                      << ", i = " << i <<  std::endl;
#endif            
            bot_quat_mult(combinedOrientation, negativeInputRpy, img.pose->orientation);
            bot_quat_to_roll_pitch_yaw(combinedOrientation, combinedRpy);
            if (fabs(combinedRpy[2]) <
                (imgBuffer->GetDeltaThetaDegreesThreshold() * M_PI / 180.)) {
#ifdef BUFFER_DEBUG
                std::cout << "and the angle is close enough: threshold = "
                          << imgBuffer->GetDeltaThetaDegreesThreshold()
                          << ", angle = "
                          << fabs(combinedRpy[2]) * 180. / M_PI
                          << ", i = " << i <<  std::endl;
#endif
                tooCloseFound = true;
            } else {
#ifdef BUFFER_DEBUG
                std::cout << ".......but the angle is far enough: threshold = "
                          << imgBuffer->GetDeltaThetaDegreesThreshold()
                          << ", angle = "
                          << fabs(combinedRpy[2]) * 180. / M_PI
                          << ", i = " << i <<  std::endl;
#endif
            }
        }
    }

    // Add new pose (and it becomes the class' responsibility), or free new pose
    if (!tooCloseFound) {
#ifdef BUFFER_DEBUG
        std::cout << "Added new image with pose." << std::endl;
#endif
        bestPose->utime = msgTime;
        imgBuffer->AddNewImageWithPose(bot_core_image_t_copy(iMsg), bestPose);
    } else {
#ifdef BUFFER_DEBUG
        std::cout << "Far enough not found." << std::endl;
#endif
        free(bestPose);
    }
    imgBuffer->Unlock();
}

void
ImageBuffer::OnPose(const lcm_recv_buf_t *iRbuf, const char *iChannel, 
                    const bot_core_pose_t *iMsg, void *iUserData)
{
    ImageBuffer *imgBuffer = static_cast<ImageBuffer*>(iUserData);
    imgBuffer->Lock();
    if (!imgBuffer->IsRunning()) {
        imgBuffer->Unlock();
        return;
    }

    if (imgBuffer->PosesAreFull()) {
        imgBuffer->DropFirstPose();
    }

    imgBuffer->AddNewPose(bot_core_pose_t_copy(iMsg));
    imgBuffer->Unlock();
}

bot_core_pose_t *
ImageBuffer::PeekAtPose(int iIndex)
{
    if (iIndex >= mPoses.size()) {
        return NULL;
    }
    return mPoses[iIndex];
}
        
void
ImageBuffer::DropFirstPose()
{
    if (!mPoses.empty()) {
        bot_core_pose_t_destroy(mPoses.front());
        mPoses.pop_front();
    }
}


void
ImageBuffer::AddNewPose(bot_core_pose_t *iPose)
{
    mPoses.push_back(iPose);
#ifdef BUFFER_DEBUG
     std::cout << "Added pose [" << mPoses.size() << "]" << std::endl;
#endif
}

void
ImageBuffer::DropFirstImageWithPose()
{
    if (!mImages.empty()) {
        bot_core_image_t_destroy(mImages.front().image);
        bot_core_pose_t_destroy(mImages.front().pose);
        mImages.pop_front();
    }
}

void
ImageBuffer::AddNewImageWithPose(bot_core_image_t *iImage,
                                 bot_core_pose_t *iPose)
{
    ImageWithPose imageWithPose;
    imageWithPose.image = iImage;
    imageWithPose.pose = iPose;
    mImages.push_back(imageWithPose);
    std::cout << "Added image [" << imageWithPose.image->utime << "], size = ["
              << mImages.size() << "] on channel ["
              << mImageChannel << "]" << std::endl;
}

ImageWithPose
ImageBuffer::PeekAtImage(int iIndex)
{
    if (iIndex >= mImages.size()) {
        ImageWithPose dummy;
        dummy.image = NULL;
        dummy.pose = NULL;
        return dummy;
    } else {
        return mImages[iIndex];
    }
}
        
} //namespace reacq
