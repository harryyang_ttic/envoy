#ifndef _AGILE_REACQ_MODELPOINT_H_
#define _AGILE_REACQ_MODELPOINT_H_

#include "GeomTypes.hpp"
#include "Descriptor.hpp"
#include <vector>

namespace reacq {

class ModelPoint {

  public:

    Vector3 mPoint;
    Descriptor mDescriptor;
    Matrix33 mCovariance;


}; //class ModelPoint

typedef std::vector<ModelPoint*> ModelPoints;
    
} //namespace reacq


#endif // _AGILE_REACQ_MODELPOINT_H_
