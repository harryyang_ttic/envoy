#include "Reacquisition.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include "ObjectModelBuilder.hpp"
#include "IoUtilities.hpp"
#include <geom_utils/convexhull.h>

using namespace std;

namespace reacq {

    Reacquisition::Reacquisition(const string &iLearningChannel,
                                 const string &iMatchingChannel,
                                 const string &iPublishChannel,
                                 const int32_t iPoseBufferCapacity,
                                 const int32_t iImageBufferCapacity,
                                 const double iPositionThreshold,
                                 const double iAngleThreshold,
                                 const string &iInputModelFilename,
                                 const string &iOutputModelFilename,
                                 const string &iMatlabResultsFilename
                                 ) :
        mLearningChannel(iLearningChannel),
        mMatchingChannel(iMatchingChannel),
        mPublishChannel(iPublishChannel),
        mOutputModelFilename(iOutputModelFilename),
        mMatlabResultsFilename(iMatlabResultsFilename),
        mMatlabIndex(1)
    {
        /* Get camera publica channels */
        mLcm = bot_lcm_get_global (NULL);

         //add lcm to mainloop 
        bot_glib_mainloop_attach_lcm (mLcm);

        mParam = bot_param_new_from_server (mLcm, 0);
        if (!g_thread_supported ()) {
            g_thread_init (NULL);
        }
        mMutex = g_mutex_new ();
        
        mWorldModel = om_new();
        
        mFrames = bot_frames_get_global (mLcm, mParam);
        if ("" == mMatlabResultsFilename) {
            mWriteMatlabResults = false;
        } else {
            mWriteMatlabResults = true;
            InitializeMatlabFile();
        }
        SetupBuffersAndCameras(iPoseBufferCapacity, iImageBufferCapacity,
                               iPositionThreshold, iAngleThreshold);

        if ("" != iInputModelFilename) {
            IoUtilities::loadObjectModelsFromMatlabFile(iInputModelFilename,
                                                        *this);
        }
                                                    
    
        mLearningSub =
            erlcm_reacquisition_segmentation_list_t_subscribe(mLcm,
                                                              mLearningChannel.c_str(),
                                                              Reacquisition::OnSegmentationList,
                                                              this);
        mMatchingSub =
            erlcm_reacquisition_command_list_t_subscribe(mLcm,
                                                         mMatchingChannel.c_str(),
                                                         Reacquisition::OnCommandList,
                                                         this);

    }

   
    Reacquisition::~Reacquisition()
    {
        MatcherIter mIter;
        for (mIter = mMatchers.begin(); mIter != mMatchers.end(); ++mIter) {
            if (NULL != mIter->second) {
                delete mIter->second;
            }
        }

        ImageIter iIter;
        for (iIter = mImageBuffers.begin(); iIter != mImageBuffers.end(); ++iIter) {
            delete(iIter->second);
            iIter->second = NULL;
        }
        if (NULL != mMutex) {
            g_mutex_free (mMutex);
        }

        CamerasIter cIter;
        for (cIter = mCameras.begin(); cIter != mCameras.end(); ++cIter) {
            delete(cIter->second);
            cIter->second = NULL;
        }

        //todo call destructors on all object models and other members to pointers
        //globals_release_wm(mWorldModel);
    
        mImageBuffers.clear();
    }

    void
    Reacquisition::InitializeMatlabFile()
    {
        ofstream ofs(GetMatlabResultsFilename().c_str(), ios::out);
        ofs << "%% reacquisition results" << endl
            << "reacquisition_results = ..." << endl
            << "     struct('utime',{}, 'object_id',{}, 'reprojected_gesture_xy',{}, ..." << endl
            << "            'current_pose',{},'confidence',{},'user_gesture_pose',{}, ..." << endl
            << "            'user_gesture_utime',{});" << endl;
        ofs.close();
    }
    
    void
    Reacquisition::SetupBuffersAndCameras(const int32_t iPoseBufferCapacity,
                                          const int32_t iImageBufferCapacity,
                                          const double iPositionThreshold,
                                          const double iAngleThreshold)
    {
        char **camNames = bot_param_get_all_camera_names(mParam); 
        if (camNames)
            {
                for (int i = 0; camNames[i] && strlen(camNames[i]); i++) {
                    char key[128];
                    
                    char *poseChannel = "POSE";
                    /*snprintf (key, sizeof (key), "cameras.%s.vision.reacquisition.pose_channel",
                              camNames[i]);
                    if (0 != bot_param_get_str (mParam, key, &poseChannel)) {
                        cerr << "WARNING: Could not find reacquisition set for camera ["
                             << camNames[i] << "]" << endl;
                        continue;
                        } */
                    
                    char *imageChannel;
                    snprintf (key, sizeof (key), "cameras.%s.streams.vision.channel",
                              camNames[i]);
                    if (0 != bot_param_get_str (mParam, key, &imageChannel)) {
                        cerr << "WARNING: Incomplete reacquisition set for camera ["
                             << camNames[i] << "] - missing image_channel" << endl;
                        continue;
                    }
                    ImageBuffer *imageBuf = new ImageBuffer(string(poseChannel), iPoseBufferCapacity,
                                                            string(imageChannel), iImageBufferCapacity,
                                                            iPositionThreshold, iAngleThreshold);
                    imageBuf->Start();
                    mImageBuffers.insert(make_pair(camNames[i], imageBuf));
                    Camera *camera = new Camera();
                    string calCamera("cameras.");
                    calCamera += camNames[i];
                    LensModel* lensModel = new LensModel();
                    lensModel->setName(camNames[i]);
                    lensModel->setFromParam(mParam, camNames[i]);
                    camera->mpLensModel = lensModel;


                    char *coord_frame = bot_param_get_camera_coord_frame (mParam,
                                                                          camNames[i]);

                    int gotPose = bot_frames_get_trans_mat_4x4 (mFrames, coord_frame,
                                                                "body", camera->mPose);
                    snprintf (key, sizeof (key), "cameras.%s.use_for_detection",
                              camNames[i]);
                    int useForMatching;
                    if (0 != bot_param_get_int (mParam, key, &useForMatching)) {
                        cerr << "WARNING: did not specify whether to use channel ["
                             << camNames[i] << "] - for matching" << endl;
                        continue;
                    }
                    camera->mUseForMatching = (useForMatching > 0);
                    string useString("is");
                    if (!camera->mUseForMatching) {
                        useString = "is not";
                    }
                    cout << "Camera [" << camNames[i] << "] " << useString << " for matching." << endl;
                    if ((NULL != camera->mpLensModel) && gotPose) {
                        mCameras.insert(make_pair(camNames[i], camera));
                        cout << "FOUND camera: " << camNames[i] << endl;
                    } else {
                        cerr << "ERROR: could not setup camera: "
                             << camNames[i] << endl;
                        if (0 == gotPose) {
                            cerr << "bot_frames_get_trans_mat_3x4 failed on " <<
                                calCamera << "." << endl;
                        } else {
                            cerr << "camera is null." << endl;
                        }
                        exit(1);
                    }
                }
                bot_param_str_array_free(camNames);
            }
        else
            {
                cout << endl << "No valid camera input strings found!" << endl;
            }

    }


    /*pointlist2d_t *pix, *hull;
    if (!(pix = build_new_pixels(self, &in->gui_primitives[0], camname)))
        return;
*/

    /*static pointlist2d_t * Reacquisition::BuildNewPixels(state_t *self,
                                                         const erlcm_reacquisition_segmentation_t *in)     
    {
        pointlist2d_t *pix = pointlist2d_new(in->roi.npoints);
        
        if (!pix)
            {
                ERR("Could not create new pointlist2d.\n");
                return NULL;
            }

        double xfac = 1.0; //wm_get_xfac(self->wm, camname);
        double yfac = 1.0;// wm_get_yfac(self->wm, camname);

        for (int i = 0; i < in->roi.npoints; i++)
            {
                pix->points[i].x = in->roi.points[i].x * xfac;
                pix->points[i].y = in->roi.points[i].y * yfac;
            }

        return pix;
        }*/


    int Reacquisition::GetConvexHullSegmentationList(erlcm_reacquisition_segmentation_t *in)
    {
        // take the convex hull of the pixels
        pointlist2d_t *pix = pointlist2d_new(in->roi.npoints);
        
        if (!pix)
            {
                fprintf(stderr,"Could not create new pointlist2d.\n");
                return -1;
            }

        double xfac = 1.0; //wm_get_xfac(self->wm, camname);
        double yfac = 1.0;// wm_get_yfac(self->wm, camname);

        for (int i = 0; i < in->roi.npoints; i++)
            {
                pix->points[i].x = in->roi.points[i].x * xfac;
                pix->points[i].y = in->roi.points[i].y * yfac;
            }

        pointlist2d_t *hull = convexhull_graham_scan_2d(pix);
        if (!hull)
            {
                fprintf(stderr,"Failed to compute convex hull.\n");
                pointlist2d_free(pix);
                return -1;
            }

        if (hull->npoints < 3)
            {
                if (hull->npoints)
                    {
                        fprintf(stderr, "No points in convex hull.\n");
                    }
                else
                    {
                        fprintf(stderr, "Only %d %s in convex hull:\n", hull->npoints,
                            hull->npoints == 1 ? "point" : "points");

                        for (int i = 0; i < hull->npoints; i++)
                            fprintf(stderr, "    [%d]  %03.0f,%03.0f\n", i + 1,
                                hull->points[i].x, hull->points[i].y);
                    }

                pointlist2d_free(hull);
                pointlist2d_free(pix);
                return -1;
            }

        fprintf(stderr, "Original Size : %d New Size : %d\n", pix->npoints,  hull->npoints);

        in->roi.points = (erlcm_point2d_t *) realloc(in->roi.points, hull->npoints * sizeof(erlcm_point2d_t));
        for(int i=0; i < hull->npoints; i++){
            in->roi.points[i].x = hull->points[i].x;
            in->roi.points[i].y = hull->points[i].y;
        }

        in->roi.npoints = hull->npoints;
        
        pointlist2d_free(hull);
        pointlist2d_free(pix);
        
        return 0;
    } 


    void
    Reacquisition::OnSegmentationList(const lcm_recv_buf_t *iRbuf, const char *iChannel, 
                                      const erlcm_reacquisition_segmentation_list_t *iMsg,
                                      void *iUserData)
    {
        Reacquisition *reacq = static_cast<Reacquisition*>(iUserData);
        cout << "Received  erlcm_reacquisition_segmentation_list_t message:" << endl
             << " * utime = " << iMsg->utime << endl
             << " * num_segs = " << iMsg->num_segs << endl;
        for (int i=0; i<iMsg->num_segs; ++i) {
            cout << " * segmentations[" << i << "] =" << endl
                 << "   o object_id = " << iMsg->segmentations[i].object_id << endl
                 << "   o camera = " << iMsg->segmentations[i].camera << endl
                 << "   o # roi points = " << iMsg->segmentations[i].roi.npoints << endl;
            erlcm_reacquisition_segmentation_t *seg = erlcm_reacquisition_segmentation_t_copy(&(iMsg->segmentations[i]));

            //carry out convexhull

            g_mutex_lock(reacq->mMutex);
            int64_t id = seg->object_id;
            if (reacq->mModels.end() != reacq->mModels.find(id)) {
                cout << "ERROR: Cannot add model [" << id << "]. Id already exists." << endl;
                g_mutex_unlock(reacq->mMutex);
                erlcm_reacquisition_segmentation_t_destroy(seg);
                return;
            }
            g_mutex_unlock(reacq->mMutex);

            int sucess = reacq->GetConvexHullSegmentationList(seg);
            
            if(sucess == 0){
                new ObjectModelBuilder(reacq, seg);
            }

            erlcm_reacquisition_segmentation_t_destroy(seg);

            cout << "Done." << endl;
        }
        cout << endl;
    }

    void
    Reacquisition::OnCommandList(const lcm_recv_buf_t *iRbuf, const char *iChannel,
                                 const erlcm_reacquisition_command_list_t *iMsg,
                                 void *iUserData)
    {
        Reacquisition *reacq = static_cast<Reacquisition*>(iUserData);
    
        cout << "Processing " << iMsg->num_commands << " commands." << endl;
        for (int i=0; i<iMsg->num_commands; ++i) {
            erlcm_reacquisition_command_t *cmd = &(iMsg->commands[i]);
            cout << "Received command: " << flush;
            if (ERLCM_REACQUISITION_COMMAND_T_COMMAND_LOOK_FOR_OBJECT ==
                iMsg->commands[i].command_id) {
                cout << "Look for object." << endl;
#ifdef MARBLE_TEST_MATCH
                /* START temporary code */
                cout << "Publishing dummy found message." << flush;
                lcm_t * lcm = bot_lcm_get_global (NULL);
                erlcm_reacquisition_detection_t det[1];
                det[0].utime = bot_timestamp_now();
                if (0 == iMsg->num_commands) {
                    det[0].image_utime = iMsg->utime;
                } else {
                    det[0].image_utime = iMsg->commands[0].utime;
                }
                det[0].confidence = 1.;
                det[0].object_id = cmd->object_id;
                erlcm_point2d_list_t roi;
                roi.npoints = 4;
                erlcm_point2d_t points[4];
                points[0].x = 100;
                points[0].y = 100;
                points[1].x = 100;
                points[1].y = 200;
                points[2].x = 200;
                points[2].y = 200;
                points[3].x = 200;
                points[3].y = 100;
                roi.points = points;
                det[0].roi = roi;
                det[0].camname = strdup("roof_front");
                erlcm_reacquisition_detection_list_t det_list;
                det_list.utime = bot_timestamp_now();
                det_list.num_detections = 1;
                det_list.detections = det;
                erlcm_reacquisition_detection_list_t_publish(lcm,
                                                             reacq->GetPublishChannel().c_str(),
                                                             &det_list);
                free(det[0].camname);
                cout << "Published command." << endl;
                /* END temporary code */
#else
                g_mutex_lock(reacq->mMutex);
                MatcherConstIter iter = reacq->mMatchers.find(cmd->object_id);
                if (reacq->mMatchers.end() != iter) {
                    cout << "ERROR: Cannot start searcing for model ["
                         << cmd->object_id << "]. Id already active." << endl;
                    g_mutex_unlock(reacq->mMutex);
                    return;
                }
                ObjectModelMatcher *matcher = new ObjectModelMatcher(reacq, cmd);
                reacq->mMatchers.insert(make_pair(cmd->object_id, matcher));
                g_mutex_unlock(reacq->mMutex);
#endif
            } else if (ERLCM_REACQUISITION_COMMAND_T_COMMAND_STOP_LOOKING_FOR_OBJECT ==
                       iMsg->commands[i].command_id) {
                cout << "Stop looking for object." << endl;
                g_mutex_lock(reacq->mMutex);
                if (0 == cmd->object_id) {
                    MatcherIter iter;
                    for (iter = reacq->mMatchers.begin();
                         iter != reacq->mMatchers.end();
                         ++iter) {
                        ObjectModelMatcher *matcher = iter->second;
                        matcher->Stop();
                    }
                    reacq->mMatchers.clear();
                    g_mutex_unlock(reacq->mMutex);
                    return;
                }
                MatcherIter iter = reacq->mMatchers.find(cmd->object_id);
                if (reacq->mMatchers.end() == iter) {
                    cout << "ERROR: Cannot stop searcing for model ["
                         << cmd->object_id << "]. Id does not exist." << endl;
                    g_mutex_unlock(reacq->mMutex);
                    return;
                }
                ObjectModelMatcher *matcher = iter->second;
                matcher->Stop();
                reacq->mMatchers.erase(iter);
                g_mutex_unlock(reacq->mMutex);
                //TODO guts
            } else if (ERLCM_REACQUISITION_COMMAND_T_COMMAND_REMOVE_OBJECT ==
                       iMsg->commands[i].command_id) {
                cout << "Remove object from memory." << endl;
                //TODO guts
            } else if (ERLCM_REACQUISITION_COMMAND_T_COMMAND_UPDATE_OBJECT ==
                       iMsg->commands[i].command_id) {
                cout << "Update object model." << endl;
                //TODO guts
            } else {
                cout << endl
                     << "WARNING: Do not know how to handle command id: "
                     << iMsg->commands[i].command_id << endl;
            }
        }
    

    }

    bool
    Reacquisition::AddModel(ObjectModel *iModel, bool iWriteModels, bool newObject)
    {
        cout << "Reacquisition::AddModel()" << endl;
        g_mutex_lock(mMutex);
        int64_t id = iModel->mpObject->id;    
        if (mModels.end() != mModels.find(id)) {
            cout << "ERROR: Cannot add model [" << id << "]. Id already exists." << endl;
            g_mutex_unlock(mMutex);
            return false;
        }

        mModels.insert(make_pair(id, iModel));

        if (newObject) {
            int ret = om_add_object (GetWorldModel(), iModel->mpObject);
            if (ret < 0) {
                cout << "ERROR: Cannot add model [" << id << "] to world model," << endl;
                g_mutex_unlock (mMutex);
                return false;
            }
        } else
            om_update_object(GetWorldModel(), iModel->mpObject); 
        
        cout << "Added model [" << id << "]." << endl;
        if (iWriteModels) {
            ofstream ofs(mOutputModelFilename.c_str(), ios::out);
            ofs << "% numModels = " << mModels.size() << endl;
            ObjectModelConstIter iter;
            int index = 1;
            string modelVar("models");
            for (iter = mModels.begin(); iter != mModels.end(); ++iter, ++index) {
                ofs << IoUtilities::objectModelToMatlabString(modelVar,
                                                              index,
                                                              iter->first,
                                                              *(iter->second));
            }
            ofs.close();
            cout << "Wrote " << mModels.size() << " models to ["
                 << mOutputModelFilename << "]." << endl;
        }
        g_mutex_unlock(mMutex);
        return true;
    }

    bool
    Reacquisition::AddModel(ObjectModel *iModel, bool iWriteModels)
    {
        return AddModel (iModel, iWriteModels, false);
    }

    ObjectModel *
    Reacquisition::FindModel(int64_t iId)
    {
        cout << "Reacquisition::MatchModel()" << endl;
        g_mutex_lock(mMutex);
        ObjectModelIter iter = mModels.find(iId);
        if (mModels.end() == iter) {
            cout << "ERROR: Cannot search for model [" << iId
                 << "]. Id does not exist." << endl;
            g_mutex_unlock(mMutex);
            return NULL;
        }
        ObjectModel *ret = iter->second;
        g_mutex_unlock(mMutex);
        return ret;
    }



    void
    Reacquisition::GetImageBufferTimestampsAndPoses(const std::string &iCameraName,
                                                    std::vector<int64_t> &oTimes,
                                                    std::vector<bot_core_pose_t*> &oPoses)
    {
        ImageIter imageIter = mImageBuffers.find(iCameraName);
        int count = 0;
        if (mImageBuffers.end() == imageIter) {
            oTimes.clear();
            oPoses.clear();
            
        } else {
            ImageBuffer *imageBuf = imageIter->second;
            imageBuf->GetAllTimestampsAndPoses(oTimes, oPoses);
            count = imageBuf->GetNumImages();
        }
        fprintf (stdout, "   GetImageBuffer []: There are %d images in buffer\n\n", count);
    }

    vector<string>
    Reacquisition::GetCameraNames() const {
        vector<string> names;
        names.reserve(mCameras.size());
        CamerasConstIter iter = mCameras.begin();
        for (; iter != mCameras.end(); ++iter) {
            names.push_back(iter->first);
        }
        return names;
    }
    

    ImageWithPose
    Reacquisition::GetImageBufferImageByTimestamp(const std::string &iCameraName,
                                                  int64_t iTimestamp)
    {
        ImageIter imageIter = mImageBuffers.find(iCameraName);
        if (mImageBuffers.end() == imageIter) {
            ImageWithPose dummy;
            dummy.image = NULL;
            dummy.pose = NULL;
            return dummy;
        }
        ImageBuffer *imageBuf = imageIter->second;
        ImageWithPose returnVal =
            imageBuf->GetImageByTimestamp(iTimestamp);
        return returnVal;
    }

    Reacquisition::Camera *
    Reacquisition::GetCamera(const std::string &iCameraName)
    {
        CamerasIter iter = mCameras.find(iCameraName);
        if (mCameras.end() == iter) {
            return NULL;
        }
        return iter->second;
    }
    
    bool
    Reacquisition::RemoveObjectModel(int64_t iId)
    {
        g_mutex_lock(mMutex);
        if (0 == iId) {
            ObjectModelIter iter;
            for (iter = mModels.begin(); iter != mModels.end(); ++iter) {
                delete iter->second;
                cout << "Erased model [" << iId << "]" << endl;
            }
            mModels.clear();
            g_mutex_unlock(mMutex);
            return true;
        }
        ObjectModelIter iter = mModels.find(iId);
        if (mModels.end() == iter) {
            cerr << "MAJOR ERROR: Could not remove model [" << iId << "]" << endl;
            g_mutex_unlock(mMutex);
            return false;
        }
        delete iter->second;
        mModels.erase(iter);
        g_mutex_unlock(mMutex);
        cout << "Erased model [" << iId << "]" << endl;
        return true;
    }

    string
    Reacquisition::GetImageChannelName(const string &iCameraName)
    {
        ImageIter imageIter = mImageBuffers.find(iCameraName);
        if (mImageBuffers.end() == imageIter) {
            return "";
        }
        ImageBuffer *imageBuf = imageIter->second;
        return imageBuf->GetImageChannelName();
    }

} //namespace reacq
