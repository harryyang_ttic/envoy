#ifndef _AGILE_REACQ_ALGORITHMPARAMETERS_H_
#define _AGILE_REACQ_ALGORITHMPARAMETERS_H_

namespace reacq {

struct AlgorithmParameters {
    static const double mSmallImageScale;
    static const int    mSiftLevels;
    static const double mSiftThreshObject;
    static const double mSiftThreshContext;
    static const int    mSiftDoubleImageSize;
    static const double mSiftSigmaInit;
    static const int    mSiftMaxNumMatches;
    static const double mSummoningDistance;
};

}

#endif
