#ifndef __LASER_FEATURE_EXTRACTOR_H__
#define __LASER_FEATURE_EXTRACTOR_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>

#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>

#include <bot_core/bot_core.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <pthread.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <svm/svm.h>
#include <bot_frames/bot_frames.h>

    typedef struct svm_node svm_node_t;

    //#include <lcmtypes/er_lcmtypes.h>

    typedef struct _raw_laser_features_t{
        double avg_difference_consecutive_beams;
        double std_dev_difference_consecutive_beams;
        double cutoff_avg_difference_consecutive_beams;
        double avg_beam_length;
        double std_dev_beam_length;
        double no_gaps_in_scan_threshold_1;
        double no_relative_gaps;
        double no_gaps_to_num_beam;
        double no_relative_gaps_to_num_beam;
        //        double no_beams_on_lines;
        double distance_between_two_smalled_local_minima;
        double index_distance_between_two_smalled_local_minima;
        double avg_ratio_consecutive_beams;
        double avg_to_max_beam_length;
        int size;
    } raw_laser_features_t;

    typedef struct _complex_number_t{
        double real;
        double complex;
    } complex_number_t;

    typedef struct _complex_result_t{
        complex_number_t *results;
        int size;
    } complex_result_t;


    typedef struct _polygonal_laser_features_t{
        double area;
        double perimeter;
        double area_to_perimeter;
        double PI_area_to_sqrt_perimeter;
        double PI_area_to_perimeter_pow;
        double sides_of_polygon; //Computer and robot vision VI, p61
        //why not do some PCAs??
        complex_result_t *fourier_transform_descriptors; 
        double major_ellipse_axis_length_coefficient;
        double minor_ellipse_axis_length_coefficient;
        double major_to_minor_coefficient;
        //we can also has ellipse with respect to descriptors
        double central_moment_invariants[7];  //7 values
        double normalized_feature_of_compactness;
        double normalized_feature_of_eccentricity;
        double mean_centroid_to_shape_boundary;
        double max_centroid_to_shape_boundary;
        double mean_centroid_over_max_centroid;
        double std_dev_centroid_to_shape_boundry;
        double kurtosis;
        int size;
        //        double form_factor_of_perimeter;
    } polygonal_laser_features_t;

    typedef struct _features{
        raw_laser_features_t raw_features;
        polygonal_laser_features_t polygonal_features;
        int size;
    }features_t;

    typedef struct _feature_config_t{
        double max_range;
        double min_range;
        double cutoff;
        double gap_threshold_1;
        double gap_threshold_2;
        double deviation;
        int no_of_fourier_coefficient;
        int no_beam_skip;
    } feature_config_t;

    void raw_laser_features_t_destroy(raw_laser_features_t *rf);
    void polygonal_laser_features_t_destroy(polygonal_laser_features_t *pf);
    void features_t_destroy(features_t *ft);

    bot_core_planar_lidar_t *skip_beam(bot_core_planar_lidar_t *laser, int no_beam_skip);

    //First set of library(raw laser features)
    double calculate_feature_range_diff(bot_core_planar_lidar_t *laser, int b1, int b2, double min, double max); //seems like a help func. 
    double calculate_feature_avg_diff(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_stddev_diff(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_cutoff_diff(bot_core_planar_lidar_t *laser, double length, double min, double max);
    double calculate_feature_avg_beam_length(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_stddev_beam_length(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_mean_dist_centroid(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_std_dist_centroid(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_no_gaps(bot_core_planar_lidar_t *laser, double min, double max, double threhold);
    double calculate_feature_no_of_relative_gaps(bot_core_planar_lidar_t *laser, double min, double max, double threhold);
    double calculate_feature_difference_gap_to_num(bot_core_planar_lidar_t *laser, double min, double max, double threshold);
    double calculate_feature_relative_gap_to_num(bot_core_planar_lidar_t *laser, double min, double max, double threshold);
    double calculate_feature_dist_local_minima(bot_core_planar_lidar_t *laser, double min, double max, double deviation);
    double calculate_feature_index_diff_local_minima(bot_core_planar_lidar_t *laser, double min, double max, double deviation);
    double calculate_feature_relation_consecutive_beams(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_average_length_over_maxlen(bot_core_planar_lidar_t *laser, double min, double max);
    //Add in No. of beams on lines

    //Second set(polygonar features).
    double calculate_feature_area_by_crossproduct(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_perimeter(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_perimeter_over_area(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_PI_area_over_sqrt_perimeter(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_area_PI_perimeter(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_sides_to_polygon(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_mean_dist_centroid(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_std_dist_centroid(bot_core_planar_lidar_t *laser, double min, double max);
    complex_result_t *calculate_feature_fourier_descriptor(bot_core_planar_lidar_t *laser, double min, double max, int no_of_fourier_coefficient);
    complex_result_t calculate_feature_ith_fourier_descriptor(bot_core_planar_lidar_t *laser, double min, double max, int no_coefficient, int index);  //seems like a helper.
    //[0] is minor_axis length, [1] is major_axis_length
    //double *calculate_feature_major_minor_length_axis_coefficients(bot_core_planar_lidar_t *laser, double min, double max);
    int calculate_feature_major_minor_length_axis_coefficients(bot_core_planar_lidar_t *laser, double min, double max, double result[2]);
    double calculate_feature_major_to_minor_coefficient(bot_core_planar_lidar_t *laser, double min, double max);
    double *calculate_feature_moments_invariants(bot_core_planar_lidar_t *laser, double min, double max); //return array of size 7
    //[0] is compactness and [1] is eccentricity
    //double *calculate_feature_compactness_and_eccentricity(bot_core_planar_lidar_t *laser, double min, double max);
    int calculate_feature_compactness_and_eccentricity(bot_core_planar_lidar_t *laser, double min, double max, double result[2]);
    double calculate_feature_mean_dist_centroid(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_max_dist_centroid(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_mean_dist_centroid_over_maxdist(bot_core_planar_lidar_t *laser, double min, double max);
    double calculate_feature_sides_to_polygon(bot_core_planar_lidar_t *laser, double min, double max); //not sure what this does
    double calculate_feautre_kurtosis(bot_core_planar_lidar_t *laser, double min, double max);

    svm_node_t *calculate_svm_features(bot_core_planar_lidar_t *laser, feature_config_t config);

    //High level features
    raw_laser_features_t *extract_raw_laser_features(bot_core_planar_lidar_t *laser, feature_config_t config);
    polygonal_laser_features_t *extract_polygonal_laser_features(bot_core_planar_lidar_t *laser, feature_config_t config);
    features_t *extract_features_difference(bot_core_planar_lidar_t *l1, bot_core_planar_lidar_t *l2, feature_config_t config);

    void write_difference_features(int label, char *file_name, feature_config_t config, 
                                   bot_core_planar_lidar_t *l1, bot_core_planar_lidar_t *l2);


#ifdef __cplusplus
}
#endif

#endif
