#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <bot_core/bot_core.h>
#include <laser_features/laser_feature.h>
//#include <lcmtypes/er_lcmtypes.h>

typedef struct _TestFeature TestFeature;
struct _TestFeature {
    lcm_t *lc;
    bot_core_planar_lidar_t_subscription_t *lidar_sub;
    bot_core_planar_lidar_t *lidar_last_front;
    bot_core_planar_lidar_t *first_laser;
    GMainLoop *mainloop;
};


static void
on_skirt_front(const lcm_recv_buf_t *rbuf, 
               const char *channel, const bot_core_planar_lidar_t *msg, void *user)
{
    TestFeature *self = (TestFeature*)user;
  
    if(self->first_laser != NULL){
        if(self->lidar_last_front != NULL){
            bot_core_planar_lidar_t_destroy(self->lidar_last_front);
        }
        self->lidar_last_front = bot_core_planar_lidar_t_copy(self->first_laser);
    }

  
    if(self->first_laser != NULL){
        bot_core_planar_lidar_t_destroy(self->first_laser);
    }
  
    self->first_laser = bot_core_planar_lidar_t_copy(msg);
  
    feature_config_t fconfig;
    fconfig.max_range = 30;
    fconfig.min_range = 0.1;
    fconfig.cutoff = 3;
    fconfig.gap_threshold_1 = 0.5;
    fconfig.gap_threshold_2 = 0.3;
    fconfig.deviation = 0.001;
    fconfig.no_of_fourier_coefficient = 2;
    fconfig.no_beam_skip = 3;
  
  
    //fprintf(stderr, "Start calculating.\n");
    if(self->first_laser && self->lidar_last_front){
      /*
        int64_t start = bot_timestamp_now();
        features_t *features = extract_features_difference(self->first_laser, self->lidar_last_front, fconfig);
        fprintf(stderr, "%f %f %f %f %d\n", features->raw_features.avg_beam_length, features->raw_features.std_dev_beam_length, features->polygonal_features.area, features->polygonal_features.fourier_transform_descriptors->results[0].real, features->size);
        int64_t end = bot_timestamp_now();
        fprintf(stderr, "Time taken : %f\n", (end - start) / 1.0e6);

	double area = calculate_feature_area_by_crossproduct(self->first_laser, fconfig.min_range, fconfig.max_range);
	double area1 = calculate_feature_area_by_crossproduct(self->lidar_last_front, fconfig.min_range, fconfig.max_range);
	fprintf(stderr, "Area: %f\n", area);
	fprintf(stderr, "Area: %f\n", area1);

        features_t_destroy(features);
      */
      write_difference_features(1, "test.txt", fconfig, self->first_laser, self->lidar_last_front);
    }

    //fprintf(stderr, "End calculating\n");
}


int main()//int argc, char** argv
{
    TestFeature *self = (TestFeature*) calloc (1, sizeof (TestFeature));
    self->lc = bot_lcm_get_global(NULL);
    self->lidar_sub = bot_core_planar_lidar_t_subscribe(self->lc, "SKIRT_FRONT", on_skirt_front, self);
    self->lidar_last_front = NULL;
    self->first_laser = NULL;
    self->mainloop = g_main_loop_new( NULL, FALSE );  
  
    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (self->lc);

    //add lcm to mainloop 
    ///////////////////////////////////////////////
    g_main_loop_run(self->mainloop);
  
    bot_glib_mainloop_detach_lcm(self->lc);
	
    return 0;
}
