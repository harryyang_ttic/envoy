#include "image_feature_extractor.hpp"

CImage *
decode_image(const bot_core_image_t *img){
    CImage *c_img = NULL;
    switch (img->pixelformat) {
    case BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY:
        fprintf(stderr, "Image is gray\n");
        c_img = new CImage((char *) img->data, img->width, img->height, IT_BYTE);
        break;
    case BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB:
        c_img = new CImage((char *) img->data, img->width, img->height, IT_RGB24);
        break;
    case BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG:
        {
            int num_channels = 3;
            c_img = new CImage(img->width, img->height, IT_RGB24);
            uint8_t *data = (uint8_t *)calloc(img->height * img->width * num_channels, sizeof(uint8_t));
            jpeg_decompress_8u_rgb(img->data,
                                   img->size,
                                   data, 
                                   img->width,
                                   img->height,
                                   img->width * num_channels);
            c_img->createFromBuffer((char *) data, img->width, img->height,IT_RGB24);
            free(data);
            break;
        }
    default:
        fprintf(stderr, "Unrecognized image format\n");
        break;
    }
    return c_img;
}

CSvmNode *calculate_image_features(image_feature_extractor_t *extractor, const bot_core_image_t *img){
    CImage *c_img = decode_image(img);
    if(c_img==NULL)
        return NULL;
    double ignore_border_pixels = 15;
    CCrfh *crfh = extractor->crfh_sys->computeHistogram(*c_img, ignore_border_pixels);
    delete c_img;

    if (extractor->minHistValue>0) crfh->filter(extractor->minHistValue);
    crfh->normalize();

    CSvmNode* features = crfh->getLibSvmVector(); 
    delete crfh;
    return features;
}

image_feature_extractor_t *create_image_feature_extractor(double _min_hist_value){
    image_feature_extractor_t *extractor = (image_feature_extractor_t *) calloc(1, sizeof(image_feature_extractor_t));
    extractor->minHistValue = _min_hist_value;
    QString str = "Lx(2,20)+Ly(2,20)+Lx(4,20)+Ly(4,20)";
    extractor->crfh_sys =  new CSystem(str);
    return extractor;
}

/*image_feature_extractor_t *create_image_feature_extractor(QString str, double _min_hist_value){
    image_feature_extractor_t *extractor = (image_feature_extractor_t *) calloc(1, sizeof(image_feature_extractor_t));
    extractor->minHistValue = _min_hist_value;
    extractor->crfh_sys =  new CSystem(str);
    return extractor;
    }*/
