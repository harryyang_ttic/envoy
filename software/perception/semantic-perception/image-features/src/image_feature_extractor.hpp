#ifndef _LIBIMAGE_FEATURES_H
#define _LIBIMAGE_FEATURES_H

//#ifdef __cplusplus
//extern "C" {
//#endif

#include <CSystem.h>
#include <CImage.h>
#include <CCrfh.h> 
#include <QString>
#include <image_utils/jpeg.h>
#include <lcmtypes/bot_core_image_t.h>
    typedef struct {
        CSystem *crfh_sys;
        double minHistValue;
    } image_feature_extractor_t;

    image_feature_extractor_t *create_image_feature_extractor(double _min_hist_value = 0.0);
       
    CSvmNode *calculate_image_features(image_feature_extractor_t *extractor, const bot_core_image_t *img);
    
    void destroy_image_feature_extractor(image_feature_extractor_t *extractor);

//#ifdef __cplusplus
//}
//#endif

#endif
