#ifndef ER_SEMANTIC_PERCEPTION_RENDERERS_H_
#define ER_SEMANTIC_PERCEPTION_RENDERERS_H_
#include <bot_vis/bot_vis.h>
#include <bot_frames/bot_frames.h>

#ifdef __cplusplus
extern "C" {
#endif
    void setup_renderer_annotation(BotViewer *viewer, int priority);
    void log_annotation_add_renderer_to_viewer(BotViewer *viewer, int render_priority, lcm_t *lcm, BotParam * param);
    void setup_renderer_place_classification(BotViewer *viewer, int render_priority, lcm_t *lcm, BotParam * param);

#ifdef __cplusplus
}
#endif

#endif
