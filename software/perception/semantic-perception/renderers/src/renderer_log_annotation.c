#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include <geom_utils/geometry.h>
#include "er_semantic_renderers.h"
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <lcmtypes/perception_annotation_t.h> 
#include <lcmtypes/perception_annotation_list_t.h> 
#include <lcmtypes/bot_core_pose_t.h>

#define DIST_TO_ADD 0.05
#define RENDERER_NAME "LOG_ANNOTATION"

#define ROOM_TYPE "Room type"
#define PLAYBACK_MODE "Playback Mode" //listen to a loaded annotation file and a log being played back
#define APPEARENCE_TYPE "Appearence type"
#define PARAM_CLEAR_OBJECTS "Clear objects"
#define ANNOTATION_START "Annotation_Start"
#define ANNOTATION_END "Annotation_End"
#define PARAM_CREATE_LOG_FILE "Create/Select annotation file"

#define SET_POSE_QUEUE "Set Pose Queue"
#define MAX_POSE_QUEUE_SIZE 2000

typedef struct _RendererLogAnnotation RendererLogAnnotation;

struct _RendererLogAnnotation {
    BotRenderer renderer;
    BotEventHandler ehandler;
    BotViewer *viewer;
    lcm_t *lc;
    BotFrames  *atrans;
    BotParam   *config;

    BotGtkParamWidget *pw;

    int playback_mode;

    int numObjects;
    char **objectNames;

    int numLandmarks;
    char **landmarkNames;

    int numAppearence;
    char **appearenceNames;

    int * landmarkNums;
    int * appearenceNums;
    int activeLandmarkNum;
    int activeAppearenceNum;

    int start_pressed;               //If start or end pressed, redraw.
    int end_pressed;

    double start_position_body[3];      //start position
    double start_position_local[3];

    double end_position_body[3];       //end position
    double end_position_local[3];

    int annotation_started;  //0 not started, 1 started. reset to 0 when annotation ends
    int64_t annotation_start_utime;

    //    perception_annotation_t_subscription_t *sub;
    bot_core_pose_t_subscription_t *pose_sub;

    //    perception_annotation_t *annotation;
    bot_core_pose_t *pose_last;

    bot_core_pose_t **annotated_pose_list;
    int no_annotated_pose_list;
    gchar *log_annotation_filename;

    char *last_annotation_str; 
    int pose_history_length;
    BotPtrCircular *pose_history;

    perception_annotation_t *current_annotation;
    perception_annotation_list_t *last_annotation_list;
  
};

static void pose_destroy(void *user, void *p)
{
    if (p) {
        bot_core_pose_t *pose = (bot_core_pose_t*) p;
        bot_core_pose_t_destroy(pose);
    }
}

////////////////////////////////////////////////////////////////////////////////
// ------------------------------ Drawing Functions ------------------------- //
////////////////////////////////////////////////////////////////////////////////

static void _draw(BotViewer *viewer, BotRenderer *renderer){

    RendererLogAnnotation *self = (RendererLogAnnotation*)renderer;
		
    if(self->playback_mode){
        if(self->current_annotation){
            int64_t s_utime = self->current_annotation->start_utime;
            int64_t e_utime = self->current_annotation->end_utime;
            
            for(int i=0; i < bot_ptr_circular_size(self->pose_history); i++){
                bot_core_pose_t *pose = bot_ptr_circular_index(self->pose_history, i);
                if(pose->utime > s_utime && pose->utime < e_utime){
                    double textpos[3] = {pose->pos[0] + 0.1,
                                         pose->pos[1] + 0.1,
                                         pose->pos[2] + 0.01};
                    bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, self->current_annotation->annotation,
                                     BOT_GL_DRAW_TEXT_DROP_SHADOW);
                    break;
                }
                else{
                    break;
                }
            }
            glColor3f(0, 1, 0);
            glPointSize(8);
            glBegin (GL_POINTS);
                        
            for(int i=0; i < bot_ptr_circular_size(self->pose_history); i++){
                bot_core_pose_t *pose = bot_ptr_circular_index(self->pose_history, i);
                if(pose->utime > s_utime && pose->utime < e_utime){
                    glVertex3f (pose->pos[0],
                                pose->pos[1],
                                pose->pos[2]);
                }
                else{
                    break;
                }
            }
            glEnd();
            glLineWidth(5);
            glBegin (GL_LINE_STRIP);
            for(int i=0; i < bot_ptr_circular_size(self->pose_history); i++){
                bot_core_pose_t *pose = bot_ptr_circular_index(self->pose_history, i);
                if(pose->utime > s_utime && pose->utime < e_utime){
                    glVertex3f (pose->pos[0],
                                pose->pos[1],
                                pose->pos[2]);
                }
                else{
                    break;
                }
            }
            glEnd();
        }
        else{
            //fprintf(stderr, "No current annotation\n");
        }
    }
    
    else{
        glColor3f(0, 1, 0);
    
        if(self->start_pressed || self->end_pressed){
            glPushMatrix();
            glTranslatef(self->start_position_body[0], self->start_position_body[1], 0);
            bot_gl_draw_circle(0.1);
            glPopMatrix();

            if(!self->start_pressed){
                glColor3f(0, 0, 1);
                glPushMatrix();
                //		fprintf(stderr, "Prepare to draw end_position:%.2f %.2f\n", self->end_position_local[0], self->end_position_local[1]);
                glTranslatef(self->end_position_body[0], self->end_position_body[1], 0);
                bot_gl_draw_circle(0.1);
                glPopMatrix();
            
            }
            if(self->last_annotation_str != NULL){
                double textpos[3] = {self->start_position_body[0] + 0.1, self->start_position_body[1]+ 0.1, 0};
                bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, self->last_annotation_str,
                                 BOT_GL_DRAW_TEXT_DROP_SHADOW);
            }
        }
    

        if(self->no_annotated_pose_list > 0 && self->annotated_pose_list != NULL){
            //draw points 
            glColor3f(1, 0, 0);
            //glLineWidth(10);
            glPointSize(8);
            glBegin (GL_POINTS);
            for(int i=0; i < self->no_annotated_pose_list ; i++){
                glVertex3f (self->annotated_pose_list[i]->pos[0],
                            self->annotated_pose_list[i]->pos[1],
                            self->annotated_pose_list[i]->pos[2]);
            }
            glEnd();
            glLineWidth(5);
            glBegin (GL_LINE_STRIP);
            for(int i=0; i < self->no_annotated_pose_list ; i++){
                glVertex3f (self->annotated_pose_list[i]->pos[0],
                            self->annotated_pose_list[i]->pos[1],
                            self->annotated_pose_list[i]->pos[2]);
            }
            glEnd();
        }
    }
}

void reset_pose_list(RendererLogAnnotation *self){
    if(self->annotated_pose_list != NULL && self->no_annotated_pose_list > 0){
        for(int i=0; i < self->no_annotated_pose_list ; i++){
            bot_core_pose_t_destroy(self->annotated_pose_list[i]);
        }
        self->no_annotated_pose_list = 0;
        free(self->annotated_pose_list);
        self->annotated_pose_list = NULL;
    }
}

static void 
on_annotations(const lcm_recv_buf_t *rbuf, 
               const char *channel, const perception_annotation_list_t *msg, void *user)
{
    RendererLogAnnotation *self = (RendererLogAnnotation*)user;
    
    if (self->last_annotation_list){
        perception_annotation_list_t_destroy (self->last_annotation_list);
    }

    self->last_annotation_list = perception_annotation_list_t_copy(msg);
    /*fprintf(stdout, "Annotation list received -> Size : %d\n", self->last_annotation_list->count);
      for(int i=0; i < self->last_annotation_list->count; i++){
      perception_annotation_t *annot = &self->last_annotation_list->annotations[i];
      fprintf(stdout, "Updated current annotation %s : %f - %f\n", annot->annotation, 
      annot->start_utime / 1.0e6, 
      annot->end_utime / 1.0e6);
      }*/

    self->current_annotation = NULL;
}

static void 
on_pose(const lcm_recv_buf_t *rbuf, 
        const char *channel, const bot_core_pose_t *msg, void *user)
{
    RendererLogAnnotation *self = (RendererLogAnnotation*)user;

    if (self->pose_last)
        bot_core_pose_t_destroy (self->pose_last);

    self->pose_last = bot_core_pose_t_copy(msg);
    
    if(self->last_annotation_list){
        int64_t utime = self->pose_last->utime;
        
        if(!self->current_annotation || !(self->current_annotation->start_utime < utime && self->current_annotation->end_utime > utime)){
            self->current_annotation = NULL;
            for(int i=0; i < self->last_annotation_list->count; i++){
                perception_annotation_t *annot = &self->last_annotation_list->annotations[i];
                if(annot->start_utime < utime && annot->end_utime > utime){
                    self->current_annotation = annot;
                    fprintf(stdout, "Updated current annotation %s : %f - %f\n", annot->annotation, 
                            annot->start_utime / 1.0e6, 
                            annot->end_utime / 1.0e6);
                    break;
                }
            }
        }
        //check if there is a time period for which it's inside
    }

    //if the pose has changed significantly - we should add it to queue 
    if(bot_ptr_circular_size(self->pose_history)){
        bot_core_pose_t *l_added_pose = bot_ptr_circular_index(self->pose_history, 0);
        
        //add to the queue if we have moved enough 
        if(hypot(l_added_pose->pos[0] - self->pose_last->pos[0], l_added_pose->pos[1] - self->pose_last->pos[1]) > DIST_TO_ADD){
            bot_ptr_circular_add(self->pose_history, bot_core_pose_t_copy(self->pose_last));
        }
    }
    else{
        bot_ptr_circular_add(self->pose_history, bot_core_pose_t_copy(self->pose_last));
    }
}

static void on_param_widget_changed(BotGtkParamWidget *pw, const char *name, void *user)
{
    RendererLogAnnotation *self = (RendererLogAnnotation*) user;

    if(self->pose_history_length != bot_gtk_param_widget_get_int(self->pw, SET_POSE_QUEUE)){
        self->pose_history_length = bot_gtk_param_widget_get_int(self->pw, SET_POSE_QUEUE);
        //trash the old queue and create a new one 
        bot_ptr_circular_resize(self->pose_history, self->pose_history_length);
    }

    if(!strcmp(name, ROOM_TYPE)) {
        self->activeLandmarkNum = bot_gtk_param_widget_get_enum(pw, ROOM_TYPE);
        bot_viewer_set_status_bar_message(self->viewer, "Landmark: (%s) %d", 
                                          self->landmarkNames[self->activeLandmarkNum], 
                                          self->activeLandmarkNum);
    }
    if(!strcmp(name, PARAM_CLEAR_OBJECTS)){
        for(int i=0; i < self->numObjects; i++){
            bot_gtk_param_widget_set_bool(self->pw, 
                                          self->objectNames[i], 0);
        }
    }

    if(!strcmp(name, APPEARENCE_TYPE)) {
        self->activeAppearenceNum = bot_gtk_param_widget_get_enum(pw, APPEARENCE_TYPE);
        bot_viewer_set_status_bar_message(self->viewer, "Appearence: (%s)", self->appearenceNames[self->activeAppearenceNum]);
    }
    
    if(!strcmp(name, PLAYBACK_MODE)){
        self->playback_mode = bot_gtk_param_widget_get_bool(self->pw, PLAYBACK_MODE);
    }

    if (!strcmp(name, PARAM_CREATE_LOG_FILE)) {
      
        GtkWidget *dialog;
        dialog = gtk_file_chooser_dialog_new("Add annotations to file", NULL,
                                             GTK_FILE_CHOOSER_ACTION_SAVE,
                                             GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                             GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                                             NULL);
        if (self->log_annotation_filename)
            gtk_file_chooser_set_filename (GTK_FILE_CHOOSER(dialog),
                                           self->log_annotation_filename);
        if (gtk_dialog_run (GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
            char *filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
            if (filename != NULL) {
                if (self->log_annotation_filename)
                    g_free (self->log_annotation_filename);
                self->log_annotation_filename = g_strdup (filename);
	      
                free (filename);
            }
        }
	
        gtk_widget_destroy (dialog);
    }
	
    if(!strcmp(name, ANNOTATION_START)){
        if(!self->pose_last)
            return;

        if(self->annotation_started){//self->pose_last->utime < self->annotation_start_utime){
            fprintf(stderr, "New start called - ignoring the old start time\n");
            self->annotation_started = 0;
        }

        //ensure previous annotation has ended 
        if(!self->annotation_started){
            self->annotation_started = 1;

            //clear the pose history
            bot_ptr_circular_clear(self->pose_history);

            reset_pose_list(self);

            self->start_pressed = 1;
            self->end_pressed = 0;
            //Initialize a message
            self->annotation_start_utime = self->pose_last->utime;
            // fprintf(stderr, "Annotation start: ", self->annotation_start_utime);
	
            //Get current position in BODY frame
            for(int i = 0; i < 3; i++){
                self->start_position_body[i] = self->pose_last->pos[i];
            }

            if(self->last_annotation_str != NULL)
                free(self->last_annotation_str);

            char annotation[1024];
            sprintf(annotation, "%s,%s", self->landmarkNames[self->activeLandmarkNum], 
                    self->appearenceNames[self->activeAppearenceNum]);

            self->last_annotation_str = strdup(annotation);

            fprintf(stderr,"Clicked start - Start Annotation\n");

            //Transform from BODY frame to LOCAL frame
            int trans = bot_frames_transform_vec(self->atrans, "body", "local", self->start_position_body, self->start_position_local);
		
            if(trans == 1){
                fprintf(stderr,"Start Position Transformation Successfully.\n");       //If == 1, transform successfully
                fprintf(stderr,"Start_Position_Body_Frame:%.2f %.2f %.2f; Start_Position_Local_Frame:%.2f %.2f %.2f\n", self->start_position_body[0], self->start_position_body[1], self->start_position_body[2], self->start_position_local[0], self->start_position_local[1], self->start_position_local[2]);		
            }
            else{
                fprintf(stderr,"Start Position Transformation Failed.\n"); 
            }


            //Mark the start position
            if(self->start_pressed){
                //			fprintf(stderr, "In the if statement.\n");
                bot_viewer_request_redraw(self->viewer);
            }

        }
        else{
            fprintf(stderr, "Already started annotation.\n");
        }
    }
    else if(!strcmp(name, ANNOTATION_END)){
        if(self->pose_last->utime < self->annotation_start_utime){
            fprintf(stderr, "Annotation last utime is earlier than the start time - log rewinded? - trashing the label\n");
            self->annotation_started = 0;  
            self->start_pressed = 0;
            self->end_pressed = 0;
        } 

        //ensure annotation has started before end
        if (self->annotation_started){
            self->annotation_started = 0;
            self->start_pressed = 0;
            self->end_pressed = 1;
            perception_annotation_t msg;
            msg.start_utime = self->annotation_start_utime;
            msg.end_utime = self->pose_last->utime;
            msg.utime = bot_timestamp_now();

            //char annotation[100];
            //strcpy(annotation, self->landmarkNames[self->activeLandmarkNum]);
            char annotation[1024];
            char *channel_name = "POSE";
            sprintf(annotation, "%s,%s,%s", self->appearenceNames[self->activeAppearenceNum], 
                    channel_name, self->landmarkNames[self->activeLandmarkNum]);
            

            msg.annotation = strdup(annotation);
            msg.reference_channel = strdup (channel_name);

            for(int i = 0; i < 3; i++){
                self->end_position_body[i] = self->pose_last->pos[i];
            }

            //copy the pose list to another data structure (which should be cleared when a new annotation is happening)
            fprintf(stderr,"Clicked end - End Annotation\n");

            //Transform from BODY to LOCAL
            int trans = bot_frames_transform_vec(self->atrans, "body", "local", self->end_position_body, self->end_position_local);
            if(trans == 1){
                fprintf(stderr,"End Position Transformation Successfully.\n");       //If == 1, transform successfully
                fprintf(stderr,"End_Position_Body_Frame:%.2f %.2f %.2f; End_Position_Local_Frame:%.2f %.2f %.2f\n", self->end_position_body[0], self->end_position_body[1], self->end_position_body[2], self->end_position_local[0], self->end_position_local[1], self->end_position_local[2]);		
            }
            else{
                fprintf(stderr,"End Position Transformation Failed.\n"); 
            }
		
            // Write annotation data to text file.
            if (self->log_annotation_filename){
                FILE *fp = fopen (self->log_annotation_filename, "a");
                fprintf (fp, "%"PRId64",%"PRId64",%s", self->annotation_start_utime, self->pose_last->utime, 
                         annotation);
                
                for(int i=0; i < self->numObjects; i++){
                    if(bot_gtk_param_widget_get_bool (self->pw, self->objectNames[i])){
                        fprintf(fp,",%s", self->objectNames[i]);
                    }
                }
                fprintf(fp, "\n");
		
                fclose (fp);
            } else
                fprintf (stderr, "Error: Choose a file to save the annotation data to\n");
	

            //Publish the LOG_ANNOTAION message
            perception_annotation_t_publish(self->lc, "LOG_ANNOTATION", &msg);

            /*perception_annotation_msg_t a_msg;
              a_msg.utime = msg.utime;
              a_msg.start_time = msg.start_utime;
              a_msg.end_utime = msg.end_utime;
              a_msg.type = */

            free (msg.annotation);
            free (msg.reference_channel);

            int matched_ind = 0;
            int count = 0;
            for(int i=0; i < bot_ptr_circular_size(self->pose_history); i++){
                bot_core_pose_t *pose = bot_ptr_circular_index(self->pose_history, i);
                if(pose->utime <= msg.end_utime && pose->utime >= msg.start_utime){
                    count++;
                }      
                if(pose->utime < msg.start_utime)
                    break;
            }

            self->annotated_pose_list = (bot_core_pose_t **) calloc(count, sizeof(bot_core_pose_t *));
            fprintf(stderr, "Pose list starting to be instantiated...\n");
            for(int i=0; i < bot_ptr_circular_size(self->pose_history); i++){
                bot_core_pose_t *pose = bot_ptr_circular_index(self->pose_history, i);
                //fprintf(stderr, "%" PRId64 ", %" PRId64 ", %" PRId64 "\n", pose->utime, msg.start_utime, msg.end_utime);         //Segfault occurs when pose->utime == msg.end_utime. Still happens with <= but differently
                if(pose->utime <= msg.end_utime && pose->utime >= msg.start_utime){ 
                    self->annotated_pose_list[i] = bot_core_pose_t_copy(pose);
                    self->no_annotated_pose_list = i+1;
                }   
                if(pose->utime < msg.start_utime)
                    break;
            }

            if(self->end_pressed){
                bot_viewer_request_redraw(self->viewer);
            }
        }
        else{
            fprintf(stderr, "Haven't started annotation\n");
        }
    }
}



static void
_free (BotRenderer *renderer)
{

    RendererLogAnnotation *self = (RendererLogAnnotation *) renderer->user;
    reset_pose_list(self);
    if(self->pose_history)
        bot_ptr_circular_destroy(self->pose_history);
    free(self);
    free (renderer);
}

BotRenderer *renderer_log_annotation_new (BotViewer *viewer, int render_priority, lcm_t *lcm, BotParam * param)
{
    RendererLogAnnotation *self = (RendererLogAnnotation*) calloc (1, sizeof (RendererLogAnnotation));
    self->viewer = viewer;
    self->renderer.draw = _draw;
    self->renderer.destroy = _free;
    self->renderer.name = RENDERER_NAME;
    self->renderer.user = self;

    BotEventHandler *ehandler = &self->ehandler;
    ehandler->name = (char*) RENDERER_NAME;
    ehandler->enabled = 1;
    ehandler->pick_query = NULL;
    ehandler->key_press = NULL;
    ehandler->hover_query = NULL;
    ehandler->mouse_press = NULL;
    ehandler->mouse_release = NULL;
    ehandler->mouse_motion = NULL;
    ehandler->user = self;

    bot_viewer_add_event_handler(viewer, &self->ehandler, render_priority);

    self->lc = lcm; //globals_get_lcm_full(NULL,1);

    //Try to set up frame
    self->config = param;
    self->atrans = bot_frames_get_global(self->lc, self->config);


    //subscribe to pose_t lcm messages
    self->pose_sub = bot_core_pose_t_subscribe(self->lc, "POSE", on_pose, self);

    perception_annotation_list_t_subscribe(self->lc, "ANNOTATION_LIST", on_annotations, self);

    
    self->pw = BOT_GTK_PARAM_WIDGET(bot_gtk_param_widget_new());

    self->no_annotated_pose_list = 0;
    self->annotated_pose_list = NULL;

    //Change landmarks here
    self->numLandmarks = 11;  //one more for null
    self->landmarkNames = calloc(self->numLandmarks, sizeof(char*));
    self->landmarkNames[0] = "kitchen";
    self->landmarkNames[1] = "conferenceroom";
    self->landmarkNames[2] = "office";
    self->landmarkNames[3] = "lab";
    self->landmarkNames[4] = "lounge";
    self->landmarkNames[5] = "hallway";
    self->landmarkNames[6] = "elevatorlobby";
    self->landmarkNames[7] = "large_meeting_room";
    self->landmarkNames[8] = "classroom";
    self->landmarkNames[9] = "elevator";
    //self->landmarkNames[10] = "cubicle_office_area";
    self->landmarkNames[10] = "elevator_lobby";
    //self->landmarkNames[11] = "stairway";
    self->landmarkNums = calloc(self->numLandmarks, sizeof(int));

    self->numObjects = 14;  //one more for null
    self->objectNames = calloc(self->numObjects, sizeof(char*));
    self->objectNames[0] = "monitor";
    self->objectNames[1] = "keyboard";
    self->objectNames[2] = "laptop";
    self->objectNames[3] = "microwave";
    self->objectNames[4] = "refrigerator";
    self->objectNames[5] = "couch";
    self->objectNames[6] = "table";
    self->objectNames[7] = "chair";
    self->objectNames[8] = "cabinet";
    self->objectNames[9] = "elevator_door";
    self->objectNames[10] = "sink";
    self->objectNames[11] = "printer";
    self->objectNames[12] = "trashcan";
    self->objectNames[13] = "stairs";

    self->numAppearence = 3;  
    self->appearenceNames = calloc(self->numAppearence, sizeof(char*));
    self->appearenceNames[0] = "room";
    self->appearenceNames[1] = "hallway";
    self->appearenceNames[2] = "openarea";

    self->appearenceNums = calloc(self->numLandmarks, sizeof(int));


    self->last_annotation_str = NULL;
    for (int i = 0; i < self->numLandmarks; i++){
        self->landmarkNums[i] = i;
    }

    for (int i = 0; i < self->numAppearence; i++){
        self->appearenceNums[i] = i;
    }

    //Create or select file to which to write annotations
    bot_gtk_param_widget_add_buttons(self->pw, PARAM_CREATE_LOG_FILE, NULL);

    self->last_annotation_list = NULL;

    //start/pause log annotation
    bot_gtk_param_widget_add_enumv(self->pw, ROOM_TYPE, BOT_GTK_PARAM_WIDGET_DEFAULTS, 0, self->numLandmarks, (const char **) self->landmarkNames, self->landmarkNums);
    bot_gtk_param_widget_add_enumv(self->pw, APPEARENCE_TYPE, BOT_GTK_PARAM_WIDGET_DEFAULTS, 0, self->numAppearence, (const char **) self->appearenceNames, self->appearenceNums);
    
    bot_gtk_param_widget_add_booleans(self->pw, BOT_GTK_PARAM_WIDGET_DEFAULTS, 
                                      PLAYBACK_MODE, 0, NULL); 
    
    bot_gtk_param_widget_add_buttons(self->pw, ANNOTATION_START, NULL);
    bot_gtk_param_widget_add_buttons(self->pw, ANNOTATION_END, NULL);

    bot_gtk_param_widget_add_buttons(self->pw, PARAM_CLEAR_OBJECTS, NULL);
    
    for(int i=0; i < self->numObjects; i++){
        bot_gtk_param_widget_add_booleans (self->pw, 
                                           0,
                                           self->objectNames[i], 0, NULL);
    }

    bot_gtk_param_widget_add_int(self->pw, SET_POSE_QUEUE, BOT_GTK_PARAM_WIDGET_SLIDER, 0, MAX_POSE_QUEUE_SIZE, 1, MAX_POSE_QUEUE_SIZE);
    
    self->pose_history_length = bot_gtk_param_widget_get_int(self->pw, SET_POSE_QUEUE);
    self->pose_history = bot_ptr_circular_new(self->pose_history_length, pose_destroy, NULL);

    g_signal_connect(G_OBJECT(self->pw), "changed", G_CALLBACK(on_param_widget_changed), self);
    self->renderer.widget = GTK_WIDGET(self->pw);

    self->current_annotation = NULL;
    self->activeLandmarkNum = 0;
    self->activeAppearenceNum = 0;
    self->playback_mode = 0;
    return &self->renderer;
}

void log_annotation_add_renderer_to_viewer(BotViewer *viewer, int render_priority, lcm_t *lcm, BotParam * param)
{
    bot_viewer_add_renderer(viewer, renderer_log_annotation_new(viewer, render_priority, lcm, param), 
                            render_priority);
}
