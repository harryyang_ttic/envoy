
#include <stdio.h>
#include <glib.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#include <bot_core/bot_core.h>
#include <bot_vis/viewer.h>
#include <bot_vis/gl_util.h>
#include <bot_frames/bot_frames.h>

#include <lcmtypes/slam_graph_particle_list_t.h>
#include <lcmtypes/slam_graph_particle_t.h>
#include <lcmtypes/slam_graph_weight_list_t.h>
#include <lcmtypes/perception_annotation_t.h>

#define ERR(fmt, ...) do {                                  \
      fprintf(stderr, "["__FILE__":%d Error: ", __LINE__);  \
      fprintf(stderr, fmt, ##__VA_ARGS__);                  \
  } while (0)

#define MAX_HIST 10000
#define MAX_POSE_HIST 100
#define PARAM_CREATE_LOG_FILE "Save annotation buffer to file"
#define PARAM_RESET_BUFFER "Reset annotation buffer"
#define RENDERER_NAME "ANNOTATION"

//struct for the annotation/pose combination
typedef struct
{
  int annot_number;
  perception_annotation_t *annot;
  int num_pose;

  //The nodes at which the annotation will start and end on
  int node_start;
  int node_end;
  
  double *x;
  double *y;
  double *z;
} annotation_pose_t;


typedef struct
{
    BotRenderer renderer;
    BotViewer *viewer;
    BotFrames *frames;
    BotEventHandler ehandler;
    BotGtkParamWidget *pw;  
    BotPtrCircular *annotation_buffer;
    
    // Most likely particle/id
    slam_graph_particle_t *ml_particle;
    int  ml_particle_id;
   
    lcm_t *lcm;

    perception_annotation_t_subscription_t *sub;
  
    perception_annotation_t *annotation_last;

} RendererAnnotation;
 
static void on_particle_list_t(const lcm_recv_buf_t *rbuf, const char *channel, const slam_graph_particle_list_t *msg, void *user){

    RendererAnnotation *self = (RendererAnnotation*)user;
    self->ml_particle_id = 0;

    double maxprob = 0;
    
    slam_graph_particle_list_t *particlelist = (slam_graph_particle_list_t *) slam_graph_particle_list_t_copy(msg);
    
    slam_graph_particle_t *ml_particle;
    for(int i = 0; i < sizeof(particlelist->particle_list); i++){
      slam_graph_particle_t *particle =  &particlelist->particle_list[i];
      double prob = exp(particle->weight);
      if(prob >= maxprob){
	  maxprob = prob;
	  self->ml_particle_id = particle->id;
	  //self->ml_particle = particle;
	  ml_particle = particle;
       }
    }
    if (self->ml_particle)
      slam_graph_particle_t_destroy (self->ml_particle);
    self->ml_particle = slam_graph_particle_t_copy (ml_particle);

    slam_graph_particle_list_t_destroy(particlelist);
    
}     

static void on_annotation(const lcm_recv_buf_t *rbuf, 
              const char *channel, const perception_annotation_t *msg, void *user)
{
    RendererAnnotation *self = (RendererAnnotation*)user;

    // Allocate memory for the annotation/pose
    annotation_pose_t *annot_pose = (annotation_pose_t*) calloc (1, sizeof(annotation_pose_t));

    // Copy the annotation into the annotation/pose buffer
    annot_pose->annot = perception_annotation_t_copy (msg);
    annot_pose->annot_number = bot_ptr_circular_size(self->annotation_buffer);



    // Allocate memory for the positions
    int64_t utime_start = annot_pose->annot->start_utime;
    int64_t utime_end = annot_pose->annot->utime;
    int utime_delta = 0.1*1E6;

    annot_pose->num_pose = (utime_end - utime_start)/ utime_delta;

    annot_pose->x = (double*) calloc(1, annot_pose->num_pose * sizeof(double));
    annot_pose->y = (double*) calloc(1, annot_pose->num_pose * sizeof(double));
    annot_pose->z = (double*) calloc(1, annot_pose->num_pose * sizeof(double));
    

    //Store the post information into the buffer
    int64_t utime_draw = utime_start;
    int count = 0;
    while (utime_draw < utime_end && (count < annot_pose->num_pose)) {
      BotTrans pose;
      bot_frames_get_trans_with_utime (self->frames, "body", "local", utime_draw, &pose);
      annot_pose->x[count] = pose.trans_vec[0];
      annot_pose->y[count] = pose.trans_vec[1];
      annot_pose->z[count] = pose.trans_vec[2];
      
      count++;
      utime_draw += utime_delta;
    }
   
    int start_difference = 1000000000;    
    int end_difference = 1000000000;
 
    //ASSIGN THE MATCHING NODES WITH THE ANNOTATION

    slam_graph_particle_t *particle = (slam_graph_particle_t *) self->ml_particle;
    if(particle){
    for(int i = 0;i < self->ml_particle->no_nodes; i++){
        slam_graph_node_t *node = (slam_graph_node_t *) &particle->node_list[i];
	if(abs(utime_start - node->utime) < start_difference){
	    start_difference = abs(utime_start - node->utime);
	    annot_pose->node_start = node->id;
	}
        if(abs(utime_end - node->utime) < end_difference){
	  end_difference = abs(utime_end - node->utime);
	    annot_pose->node_end = node->id;
	}
	
    }
    }
    
               
    bot_ptr_circular_add(self->annotation_buffer,annot_pose);

    bot_viewer_request_redraw(self->viewer);
}

// called by annotation_buffer when an annotation/pose needs to be freed.
static void free_annotation_element(void *user, void *p)
{
  annotation_pose_t *annot_pose = (annotation_pose_t *) p;
  perception_annotation_t_destroy (annot_pose->annot);//*************************************
  free(annot_pose->x);
  free(annot_pose->y);
  free(annot_pose->z);
  free(annot_pose);
}

////////////////////////////////////////////////////////////////////////////////
// ------------------------------ Drawing Functions ------------------------- //
////////////////////////////////////////////////////////////////////////////////

static void 
_draw(BotViewer *viewer, BotRenderer *r)
{
    RendererAnnotation *self = (RendererAnnotation*)r;
    if (!self->annotation_buffer)
        return;

    if(bot_ptr_circular_size(self->annotation_buffer) == 0)
        return;

    glPushAttrib (GL_ENABLE_BIT);
    glEnable (GL_BLEND);
    glDisable (GL_DEPTH_TEST);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    int gl_width = (GTK_WIDGET(viewer->gl_area)->allocation.width);
    int gl_height = (GTK_WIDGET(viewer->gl_area)->allocation.height);

    // transform into window coordinates, where <0, 0> is the top left corner
    // of the window and <gl_width, gl_height> is the bottom right corner
    // of the window
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0, gl_width, 0, gl_height);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glTranslatef(0, gl_height, 0);
    glScalef(1, -1, 1);

    void *font = GLUT_BITMAP_8_BY_13;
    int line_height = 14;

    float colors[3][3] = {
        { 1.0, 1.0, 1.0 },
        { 0.7, 0.7, 0.0 },
        { 1.0, 0.0, 0.0 },
    };


    // Form the string that you'd like to print to the viewer.
    // This could just be the annotation from the message or you could include
    // the audio channel name, for example "[HEADSET_AUDIO]: The kitchen is down the hall"
    char annotation_line[256];
    annotation_pose_t *annot_pose = (annotation_pose_t *) bot_ptr_circular_index(self->annotation_buffer, 0); //********************* 
    sprintf (annotation_line, "[%s]: %s", annot_pose->annot->reference_channel, annot_pose->annot->annotation);
    double x = 10;
    double y = gl_height - 4 * line_height - 1;
    

    glColor4f(0, 0, 0, 0.7);
    glBegin(GL_QUADS);
    glVertex2f(x, y - line_height);
    glVertex2f(x + 80*12, y - line_height);
    glVertex2f(x + 80*12, y + 5 * line_height);
    glVertex2f(x, y + 5 * line_height);
    glEnd();

    // Use bot_glutBitmapString() to draw annotation string at (x, y)
    glColor3fv(colors[0]);
    glRasterPos2f(x, y);
    bot_glutBitmapString(font, (unsigned char*) annotation_line);

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glPopAttrib ();

    // Color the path and add label
    for (int i = 0; i < bot_ptr_circular_size(self->annotation_buffer); i++) {

      annotation_pose_t * annot_pose = (annotation_pose_t *) bot_ptr_circular_index(self->annotation_buffer, i);
      
      if(annot_pose->annot_number % 2){
	  glColor3f(1, 0, 0);
      }
      else{
	  glColor3f(1, .5, 0);
      } // alternates color for easier viewing of the transcriptions on path.
	
      glLineWidth(5.0);
      glBegin(GL_LINE_STRIP);
      double xyz_global[3];
      
      /*for(int j=0; j < annot_pose->num_pose; j++) {
	
	double xyz_local[] = {annot_pose->x[j], annot_pose->y[j], annot_pose->z[j]};
        bot_frames_transform_vec (self->frames, "local", "global", xyz_local, xyz_global);
     
	//glVertex3f (annot_pose->x[j], annot_pose->y[j], annot_pose->z[j]);
	glVertex3f (xyz_global[0], xyz_global[1], xyz_global[2]);
      }*/


      //Draw on the particle
      
      double finalxy[2];
      slam_graph_particle_t * particle = (slam_graph_particle_t *) self->ml_particle;
      if(particle && particle->node_list){
	for(int i = annot_pose->node_start; i <= annot_pose->node_end; i ++){
	  slam_graph_node_t node = particle->node_list[i];
	  //printf("%d",i);
	  finalxy[0] = node.xy[0];
	    finalxy[1] = node.xy[1];
	  glVertex3f(node.xy[0],node.xy[1],0);
	}
      }
      glEnd();
	
      char label_number[50];
      sprintf(label_number, "%d", annot_pose->annot_number);
	
      double textpos[3] = {finalxy[0] + 0.1, finalxy[1]+ 0.1, 0};
     
      bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, label_number, BOT_GL_DRAW_TEXT_DROP_SHADOW);

    }

}


////////////////////////////////////////////////////////////////////////////////
// ------------------------------ Up and Down ------------------------------- //
////////////////////////////////////////////////////////////////////////////////

static void on_param_widget_changed(BotGtkParamWidget *pw, const char *name, void *user)
{
    RendererAnnotation *self = (RendererAnnotation*) user;
    // Saves the annotation buffer into a text file
    if (!strcmp(name, PARAM_CREATE_LOG_FILE)) {
      
      GtkWidget *dialog;
      dialog = gtk_file_chooser_dialog_new("Save annotations to file", NULL,
					   GTK_FILE_CHOOSER_ACTION_SAVE,
					   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					   GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
					   NULL);
	if (gtk_dialog_run (GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
	    char *filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
	    
	    if (filename != NULL) {
	      
	      FILE *fp = fopen (filename, "w");
     
	      for(int i = bot_ptr_circular_size(self->annotation_buffer) - 1; i >= 0; i--){

		annotation_pose_t * annot_pose = (annotation_pose_t*) bot_ptr_circular_index(self->annotation_buffer,i);

		fprintf(fp, "[%d] %s\n", annot_pose->annot_number, annot_pose->annot->annotation);

	      }
	      
	      free (filename);
		  fclose(fp);
	    }
	}
	
	gtk_widget_destroy (dialog);
    }
    // If the reset button is pressed, the annotation buffer is cleared.
    if (!strcmp(name, PARAM_RESET_BUFFER)){
      
      bot_ptr_circular_clear(self->annotation_buffer);
    }
	
}

static void 
_destroy(BotRenderer *r)
{
    if (!r) return;

    RendererAnnotation *self = (RendererAnnotation*)r->user;
    free(self);
}

BotRenderer *renderer_annotation_new(BotViewer *viewer)
{
    RendererAnnotation *self = (RendererAnnotation*)calloc(1, sizeof(RendererAnnotation));
    self->viewer = viewer;
    
    // Create the annotation buffer which stores annotations
    self->annotation_buffer = bot_ptr_circular_new(MAX_HIST, free_annotation_element, NULL);//********************************

    // Defining functions that take care of drawing on the screen and destroying
    BotRenderer *r = &self->renderer;
    self->renderer.name = RENDERER_NAME;
    self->renderer.draw = _draw;
    self->renderer.destroy = _destroy;

    BotEventHandler *ehandler = &self->ehandler;
    ehandler->name = (char*) RENDERER_NAME;
    ehandler->enabled = 1;
    ehandler->pick_query = NULL;
    ehandler->key_press = NULL;
    ehandler->hover_query = NULL;
    ehandler->mouse_press = NULL;
    ehandler->mouse_release = NULL;
    ehandler->mouse_motion = NULL;
    ehandler->user = self;

    // Add Widget for buttons/interface
    bot_viewer_add_event_handler(viewer, &self->ehandler, 1);      
    self->pw = BOT_GTK_PARAM_WIDGET(bot_gtk_param_widget_new());

    bot_gtk_param_widget_add_buttons(self->pw, PARAM_CREATE_LOG_FILE, NULL);
    bot_gtk_param_widget_add_buttons(self->pw, PARAM_RESET_BUFFER, NULL);

    g_signal_connect(G_OBJECT(self->pw), "changed", G_CALLBACK(on_param_widget_changed), self);
    self->renderer.widget = GTK_WIDGET(self->pw);
    
    // Get the handle to LCM, which is necessary to subscribe to messages
    self->lcm = bot_lcm_get_global (NULL);

    BotParam *param = bot_param_new_from_server (self->lcm, 0);
    self->frames = bot_frames_get_global (self->lcm, param);

    // Subscribe to the annotation message 
    self->sub = perception_annotation_t_subscribe(self->lcm, "ANNOTATION", on_annotation, self);
    // Subscribe to the SLAM particle lists
    slam_graph_particle_list_t_subscribe(self->lcm, "PARTICLE_ISAM_RESULT", on_particle_list_t, self);

    return r;
}

void setup_renderer_annotation(BotViewer *viewer, int priority)
{
    BotRenderer *r = renderer_annotation_new(viewer);
    bot_viewer_add_renderer(viewer, r, priority);
}
