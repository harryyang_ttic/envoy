//Subscribe to log_annotation_t and planar_lidar_t messages. Once receive an annotation, publish laser_annotation_t message
//Requires: er-simulate-360-laser
//Output: publish laser_annotation_t message


#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <bot_param/param_client.h>
#include <bot_frames/bot_frames.h>
#include <lcm/lcm.h>

#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>
#include <lcmtypes/perception_lcmtypes.h>

#include <bot_param/param_client.h>//
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <full_laser/full_laser.h>

#define SENSOR_DECIMATION_DISTANCE 0.1
#define SENSOR_DECIMATION_HEADING_DEG 10
#define LIDAR_LIST_SIZE 4000

typedef struct
{
    lcm_t      *lcm;
    BotFrames *frames;
    GMainLoop *mainloop;

    GList *lidar_list;
    GList *annotation_list;
    bot_core_planar_lidar_t *last_laser;
    bot_core_planar_lidar_t *last_nodding;
    perception_annotation_t *last_annotation;
    bot_core_image_t *last_image;
    full_laser_state_t *full_laser;
    char *annotation_file;
    lcm_eventlog_t *write_log;
    int nodding;
    char *image_channel;

    double last_laser_xyt[3];
    double last_image_xyt[3];
  
    int room_laser_count;
    int room_image_count;
    int openarea_laser_count;
    int openarea_image_count;
    int hallway_laser_count;
    int hallway_image_count;
} state_t;

char *room = "room";
char *openarea = "openarea";
char *hallway = "hallway";

void update_count(state_t *s, perception_annotation_t *a, int laser){
    if(!strcmp(a->annotation, room)){
        if(laser){
            s->room_laser_count++;
        }
        else{
            s->room_image_count++;
        }
    }
    else if(!strcmp(a->annotation, openarea)){
        if(laser){
            s->openarea_laser_count++;
        }
        else{
            s->openarea_image_count++;
        }
    }
    else if(!strcmp(a->annotation, hallway)){
        if(laser){
            s->hallway_laser_count++;
        }
        else{
            s->hallway_image_count++;
        }
    }
}


static void
full_laser_update_handler(int64_t utime, bot_core_planar_lidar_t *msg, void *user)
{
    state_t *s = (state_t *)user;

    BotTrans body_to_local; 

    if(!bot_frames_get_trans_with_utime(s->frames, "body", 
                                        "local",
                                        utime, 
                                        &body_to_local)){
        fprintf (stderr, "Error getting bot_frames transformation from to local!\n");
        return;
    }

    double rpy[3] = {0};
    bot_quat_to_roll_pitch_yaw(body_to_local.rot_quat, rpy);

    double xyt[3] = {body_to_local.trans_vec[0], body_to_local.trans_vec[1], rpy[2]};

    double delta = hypot(xyt[0] - s->last_laser_xyt[0], xyt[1] - s->last_laser_xyt[1]);
    double delta_t = bot_mod2pi(xyt[2] - s->last_laser_xyt[2]);

    if( delta > SENSOR_DECIMATION_DISTANCE || fabs(delta_t) > bot_to_radians(SENSOR_DECIMATION_HEADING_DEG)){
        s->last_laser_xyt[0] = xyt[0];
        s->last_laser_xyt[1] = xyt[1];
        s->last_laser_xyt[2] = xyt[2];
    }
    else{
        return;
    }
    
    fprintf(stderr, "Adding annotated laser (%f) %f (deg)\n", delta, fabs(bot_to_degrees(delta_t)));

    if(s->last_laser != NULL){
        bot_core_planar_lidar_t_destroy(s->last_laser);
    }
    s->last_laser = bot_core_planar_lidar_t_copy(msg);

    int64_t start_utime;
    int64_t end_utime;

    int last_valid_ind = 10000;
    int valid_annotation_count = 0; 
    
    perception_laser_annotation_t message;

    // Check if any annotations in list populated from file match
    // current utime.
    for(int i = 0; i < g_list_length(s->annotation_list); i++){
        GList *element = g_list_nth(s->annotation_list, i);
        perception_annotation_t *a = (perception_annotation_t *) element->data;
        int64_t laser_utime = utime;
        start_utime = a->start_utime;
        end_utime = a->end_utime;
	
        // If so, publish as lcm message.
        if(laser_utime >= start_utime && laser_utime <= end_utime){
            fprintf(stdout, "Laser => Appearence Type : %s\n", a->annotation);
            message.laser = *s->last_laser;
            message.annotation = a->annotation;
            perception_laser_annotation_t_publish(s->lcm, "LASER_ANNOTATION", &message);
            //usleep((int)1/40 * 1e6);
            valid_annotation_count++;
            if(last_valid_ind > i){
                last_valid_ind = i;
            }
            update_count(s, a, 1);
            break;
        }
    }
}

static void on_image (const lcm_recv_buf_t *rbut, const char *channel, 
		      const bot_core_image_t *msg, void *user)
{
    state_t *s = (state_t *)user;
    
    BotTrans body_to_local; 

    if(!bot_frames_get_trans_with_utime(s->frames, "body", 
                                        "local",
                                        msg->utime, 
                                        &body_to_local)){
        fprintf (stderr, "Error getting bot_frames transformation from to local!\n");
        return;
    }

    double rpy[3] = {0};
    bot_quat_to_roll_pitch_yaw(body_to_local.rot_quat, rpy);

    double xyt[3] = {body_to_local.trans_vec[0], body_to_local.trans_vec[1], rpy[2]};
    
    double delta = hypot(xyt[0] - s->last_image_xyt[0], xyt[1] - s->last_image_xyt[1]);
    double delta_t = bot_mod2pi(xyt[2] - s->last_laser_xyt[2]);
    
    if( delta > SENSOR_DECIMATION_DISTANCE || fabs(delta_t) > bot_to_radians(SENSOR_DECIMATION_HEADING_DEG)){
        s->last_image_xyt[0] = xyt[0];
        s->last_image_xyt[1] = xyt[1];
        s->last_image_xyt[2] = xyt[2];
    }
    else{
        return;
    }

    fprintf(stderr, "Adding annotated image (%f) %f (deg)\n", delta, fabs(bot_to_degrees(delta_t)));

    if(s->last_image != NULL){
        bot_core_image_t_destroy(s->last_image);
    }
    s->last_image = bot_core_image_t_copy(msg);
  
    int64_t start_utime;
    int64_t end_utime;
    int64_t image_utime = s->last_image->utime;

    perception_image_annotation_t message;
    // Check if any annotations in list populated from file match
    // current utime.
    for(int i = 0; i < g_list_length(s->annotation_list); i++){
        GList *element = g_list_nth(s->annotation_list, i);
        perception_annotation_t *a = (perception_annotation_t *) element->data;
        start_utime = a->start_utime;
        end_utime = a->end_utime;
      
        // If so, publish as lcm message.
        if(image_utime >= start_utime && image_utime <= end_utime){
            message.image = *s->last_image;
            message.annotation = a->annotation;
            fprintf(stdout, "Image => Appearence Type : %s\n", a->annotation);
            perception_image_annotation_t_publish(s->lcm, "IMAGE_ANNOTATION", &message);
            update_count(s, a, 0);
        }
    }
}

      

static char** split_csv (char line[], int *no_elements)
{
    const char* tok = ",";
    char * split;
    //Get number of commas in line. How many tokens are there?
    //Right now we expect this to be four (start, end, label, channel).
    int count = 0;
    char * pch;
    pch = strchr(line, ',');
    while (pch != NULL){
        count++;
        pch = strchr(pch+1, ',');
    }
    //One more token than comma
    count++;

    split = strtok (line, tok);
    char* *tokens = calloc((count+1), (sizeof(split)));
    int j = 0; 
    while (split != NULL){
        tokens[j] = split;
        split = strtok (NULL, tok);
        j++;
    }
    *no_elements = j;
    return tokens;
}

void publish_loaded_annotations(state_t *s){
    perception_annotation_list_t *msg = (perception_annotation_list_t *) calloc(1, sizeof(perception_annotation_list_t));

    msg->count = (int) g_list_length(s->annotation_list);
    msg->annotations = (perception_annotation_t *) calloc(msg->count, sizeof(perception_annotation_t));

    //fprintf(stdout, "\nPublishing annotation list -> Size : %d\n", msg->count);
    for(int i = 0; i < g_list_length(s->annotation_list); i++){
        GList *element = g_list_nth(s->annotation_list, i);
        perception_annotation_t *a = (perception_annotation_t *) element->data;
              
        msg->annotations[i].start_utime = a->start_utime;
        msg->annotations[i].end_utime = a->end_utime;
        msg->annotations[i].annotation = strdup(a->annotation);
        msg->annotations[i].reference_channel = strdup(a->reference_channel);
        //fprintf(stdout, "[%d] %.5f - %.5f - %s\n", 
        //         i, msg->annotations[i].start_utime  / 1.0e6, msg->annotations[i].end_utime / 1.0e6, 
        //        msg->annotations[i].annotation);
    }
    perception_annotation_list_t_publish(s->lcm, "ANNOTATION_LIST", msg);
    perception_annotation_list_t_destroy(msg);
}

  
static int load_annotations(state_t *s)
{
    FILE *ann = fopen (s->annotation_file, "r+");
    if (ann != NULL){
        char line [1000];
        //Initialize an lcm message for each line of annotation log
        while (fgets (line, sizeof line, ann) != NULL){
            int no_elements = 0;
            char **tokens = split_csv(line, &no_elements);

            if(no_elements < 4){
                fprintf(stderr, "Error - not enough elements loaded\n");
            }
            perception_annotation_t *new_annotation = (perception_annotation_t*) calloc(1, sizeof(perception_annotation_t));
            new_annotation->start_utime = atoll(tokens[0]);
            new_annotation->end_utime = atoll(tokens[1]);
            new_annotation->annotation = strdup(tokens[2]); //new method has appearence type in 4th colum
            new_annotation->reference_channel = strdup(tokens[3]);
            s->annotation_list = g_list_prepend (s->annotation_list, (new_annotation));        
            char *region_type =  NULL;
            if(no_elements >=5){
                region_type = strdup(tokens[4]);
            }
            else{
                region_type = strdup("unknown");
            }
               
            fprintf(stdout, "Time : %.4f - %.4f- Apperance Class : %s - Type : %s\n", (new_annotation->start_utime) / 1.0e6, 
                    (new_annotation->end_utime) / 1.0e6, tokens[2], 
                    region_type);

            //anything after that is objects 
        }
        fprintf(stderr, "Annotations loaded from file.\n");
        publish_loaded_annotations(s);
        return 0;
    }
    else{
        fprintf(stderr, "Error: please specify an annotation file.\n");
        return -1;
    }
}


static int load_annotations_old(state_t *s)
{
    FILE *ann = fopen (s->annotation_file, "r+");
    if (ann != NULL){
        char line [150];
        //Initialize an lcm message for each line of annotation log
        while (fgets (line, sizeof line, ann) != NULL){
            int count = 0;
            char **tokens = split_csv(line, &count);
            perception_annotation_t *new_annotation = (perception_annotation_t*) calloc(1, sizeof(perception_annotation_t));
            new_annotation->start_utime = atoll(tokens[0]);
            new_annotation->end_utime = atoll(tokens[1]);
            new_annotation->annotation = strdup(tokens[2]); //new method has appearence type in 4th colum
            new_annotation->reference_channel = strdup(tokens[3]);
            s->annotation_list = g_list_prepend (s->annotation_list, (new_annotation));        

            fprintf(stdout, "Time : %f - Apperance Class : %s\n", (new_annotation->start_utime) / 1.0e6, tokens[2]);
        }
        fprintf(stderr, "Annotations loaded from file.\n");
        return 0;
    }
    else{
        fprintf(stderr, "Error: please specify an annotation file.\n");
        return -1;
    }
}

     

void laser_annotation_handler(const lcm_recv_buf_t *rbuf, const char *channel, const perception_laser_annotation_t *msg, void *user)
{  
    state_t *s = (state_t *) user; 

    //Write annotations to lcm log   
    //fprintf(stderr,"Received laser annotations\n");

    //writing to file 
    int channellen = strlen(channel);    
    int64_t mem_sz = sizeof(lcm_eventlog_event_t) + channellen + 1 + rbuf->data_size;
    
    lcm_eventlog_event_t *le = (lcm_eventlog_event_t*) malloc(mem_sz);
    memset(le, 0, mem_sz);

    le->timestamp = rbuf->recv_utime;
    le->channellen = channellen;
    le->datalen = rbuf->data_size;

    le->channel = ((char*)le) + sizeof(lcm_eventlog_event_t);
    strcpy(le->channel, channel);
    le->data = le->channel + channellen + 1;
    
    assert((char*)le->data + rbuf->data_size == (char*)le + mem_sz);
    memcpy(le->data, rbuf->data, rbuf->data_size);

    if(0 != lcm_eventlog_write_event(s->write_log, le)) {
        fprintf(stderr, "Error\n"); 
    }
    else{
        //fprintf(stderr,"Saved to file\n");
    }
    //lets write to file 
}

void image_annotation_handler(const lcm_recv_buf_t *rbuf, const char *channel, 
			      const perception_image_annotation_t *msg, void *user)
{
    state_t *s = (state_t *) user;

    //Write annotation to lcm log
    //fprintf(stderr, "Received image annotations\n");

    //writing to file
    int channellen = strlen(channel);
    int64_t mem_sz = sizeof(lcm_eventlog_event_t) + channellen + 1 + rbuf->data_size;

    lcm_eventlog_event_t *le = (lcm_eventlog_event_t*) malloc(mem_sz);
    memset(le, 0, mem_sz);

    le->timestamp = rbuf->recv_utime;
    le->channellen = channellen;
    le->datalen = rbuf->data_size;

    le->channel = ((char*)le) + sizeof(lcm_eventlog_event_t);
    strcpy(le->channel, channel);
    le->data = le->channel + channellen+1;

    assert((char*)le->data + rbuf->data_size == (char*)le + mem_sz);
    memcpy(le->data, rbuf->data, rbuf->data_size);

    if(0 != lcm_eventlog_write_event(s->write_log, le)){
        fprintf(stderr, "Error\n");
    }
    else{
        //fprintf(stderr, "Saved to file\n");
    }
}


void subscribe_to_channels(state_t *s)
{
    //perception_annotation_t_subscribe(s->lcm, "LOG_ANNOTATION", on_annotation, s);
    perception_laser_annotation_t_subscribe(s->lcm, "LASER_ANNOTATION", &laser_annotation_handler, s);
    bot_core_image_t_subscribe(s->lcm, s->image_channel, on_image, s);
    perception_image_annotation_t_subscribe(s->lcm, "IMAGE_ANNOTATION", &image_annotation_handler, s);
}  


static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -N        Use new annotation file\n"
             "  -n        Use nodding laser\n"
             "  -h        This help message\n"
	     "  -a        Annotation filename\n"
	     "  -l        Log filename\n"
	     "  -c        Image channel\n",
             g_path_get_basename(progname));
    exit(1);
}

int 
main(int argc, char **argv)
{

    const char *optstring = "Nnha:l:c:";

    int c;
    int nodding = 0;
    char * log_path = NULL;
    char * channel = strdup("DRAGONFLY_IMAGE");
    int annotations_from_file = 0;

    state_t *state = (state_t*) calloc(1, sizeof(state_t));
    int new_annotation_file = 0;
    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'n':  //whether to use nodding laser
            nodding = 1;
            break;
        case 'N':  //whether to use nodding laser
            new_annotation_file = 1;
            break;
        case 'h': //help
            usage(argv[0]);
            break;
	case 'a': //Annotation text file
	    state->annotation_file = strdup(optarg);
	    annotations_from_file = 1;
            break;    
	case 'l': //Log file
	    log_path = strdup(optarg);
	    break;
	case 'c': // Channel for images
	    channel = strdup(optarg);
	    break;
        default:
            usage(argv[0]);
            break;
        }
    }

    state->last_laser_xyt[0] = 0;
    state->last_laser_xyt[1] = 0;
    state->last_laser_xyt[2] = 0;
    
    state->last_image_xyt[0] = 0;
    state->last_image_xyt[1] = 0;
    state->last_image_xyt[2] = 0;

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);

    BotParam *param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, param);
    state->write_log = lcm_eventlog_create(log_path, "w");
    state->image_channel = channel;
    subscribe_to_channels(state);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );
    //If not using nodding laser
    if(!nodding){
        state->full_laser = full_laser_init(state->lcm, 360, &full_laser_update_handler, state);
        fprintf(stderr, "Using 360 degree simulate laser.\n");
    }
    else{
        //bot_core_planar_lidar_t_subscribe(state->lcm, "NODDING_LASER", on_nodding, state);
        fprintf(stderr, "subscribing to no laser.\n");
    }

    state->last_laser = NULL;
    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }
    state->nodding = nodding;

    if(annotations_from_file){
        load_annotations(state);
    }
    
    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    //read_parameters_from_conf(state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    //    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);

    bot_glib_mainloop_detach_lcm(state->lcm);
}


