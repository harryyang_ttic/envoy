
//read training data file and segment the data to equal sizes and splits to training and testing 

#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>

#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>

static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -o Output_file_name      Specify OutputFile name to save training and test data\n",
             "  -i input_data_filename   Specify the input data file to split\n",
             "  -h                 This help message\n", 
             g_path_get_basename(progname));
    exit(1);
}

int 
main(int argc, char **argv)
{
    int c;
    const char *optstring = "i:o:h";
    char *output_file = NULL;
    char *input_data_file = NULL;
    int remove_class_id = -1;
    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'o':
            output_file = strdup(optarg);  //choose log file for accuracy checking
            fprintf(stderr, "Saving to Training %s_train.txt\n\tTesting : %s_test.txt\n", output_file, output_file);
            break;
        case 'i':
            input_data_file = strdup(optarg);  //choose log file for accuracy checking
            fprintf(stderr, "Reading from Data file %s\n", input_data_file);
            break;
        //would be nice if we can skip a bunch of classes - maybe give it in bracket form or csv 
        case 'r':
            remove_class_id = atoi(optarg);  //choose log file for accuracy checking
            fprintf(stderr, "Removing class id %d\n", remove_class_id);
            break;
        case 'h': //help
            usage(argv[0]);
            break;
        default:
            usage(argv[0]);
            break;
        }
    }
    if(input_data_file == NULL){
        fprintf(stderr, "No input data file specified\n");
        usage(argv[0]);
    }

    if(output_file == NULL){
        output_file = "Data";
        fprintf(stderr, "No output name specified - using default : %s\n", output_file);
    }

    GHashTable *class_glists;

    class_glists = g_hash_table_new(g_int_hash, g_int_equal);

    FILE *file; 

    file = fopen(input_data_file,"r");
    FILE *train = fopen(output_file,"w");

    char str[5000000];
    int line_count = 0;
    if(!file) {fprintf(stderr, "No valid file\n"); return 1;} // bail out if file not found
    while(fgets(str,sizeof(str),file) != NULL){
        // strip trailing '\n' if it exists

        int len = strlen(str)-1;
        if(str[len] == '\n') 
            str[len] = 0;
        int class_id = atoi(&str[0]);

        if(class_id == remove_class_id){
            fprintf(stderr, "Skipping Class %d\n", remove_class_id);
            continue;
        }

        GList **glist = (GList **) g_hash_table_lookup(class_glists, &class_id);
        if(glist==NULL){
            glist = (GList **) calloc(1,sizeof(GList *));
            int *id = (int *)calloc(1,sizeof(int));
            *id = class_id;
            g_hash_table_insert(class_glists, id, glist);
        }
            
        *glist = g_list_prepend (*glist, strdup(str));
	            
        fprintf(train, "%s\n", str);            
        line_count++;
    }
    fclose(file);
    fprintf(stderr, "Count : %d\n", line_count);

    fclose(train);
}
