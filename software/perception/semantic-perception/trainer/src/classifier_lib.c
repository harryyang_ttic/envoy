//A library takes in bot_core_planar_lidar_t messages & return perception_place_classification_t messages
//If classification is uncertain, use binary doorway classifier to check whether it is a doorway

#include "classifier_lib.h"
#define THRESHOLD 0.4

//Determine whether classifier is confident
int check_confidence(double *values, int no_class){
  for(int i = 0;i < no_class;i++){
    if(values[i] > THRESHOLD)
      return 1;
  }
  return 0;
}


double *calculate_features(feature_config_t config, bot_core_planar_lidar_t *laser)
{
  //calculate feature values
  int no_features = config.no_of_fourier_coefficient * 4 + 38;
  raw_laser_features_t *raw_features = extract_raw_laser_features(laser, config);
  polygonal_laser_features_t *polygonal_features = extract_polygonal_laser_features(laser, config);
  double *values = (double *) calloc(no_features, sizeof(double));
  values[0] = raw_features->avg_difference_consecutive_beams;
  values[1] = raw_features->std_dev_difference_consecutive_beams;
  values[2] = raw_features->cutoff_avg_difference_consecutive_beams;
  values[3] = raw_features->avg_beam_length;
  values[4] = raw_features->std_dev_beam_length;
  values[5] = raw_features->no_gaps_in_scan_threshold_1;
  values[6] = raw_features->no_relative_gaps;
  values[7] = raw_features->no_gaps_to_num_beam;
  values[8] = raw_features->no_relative_gaps_to_num_beam;
  values[9] = raw_features->distance_between_two_smalled_local_minima;
  values[10] = raw_features->index_distance_between_two_smalled_local_minima;
  values[11] = raw_features->avg_ratio_consecutive_beams;
  values[12] = raw_features->avg_to_max_beam_length;
  values[13] = polygonal_features->area;
  values[14] = polygonal_features->perimeter;
  values[15] = polygonal_features->area_to_perimeter;
  values[16] = polygonal_features->PI_area_to_sqrt_perimeter;
  values[17] = polygonal_features->PI_area_to_perimeter_pow;
  values[18] = polygonal_features->sides_of_polygon;
  for(int i = 0; i <= config.no_of_fourier_coefficient * 2;i++){
    values[19 + i * 2] = polygonal_features->fourier_transform_descriptors[0].results->real;
    values[20 + i * 2] = polygonal_features->fourier_transform_descriptors[0].results->complex;
  }
  int start = config.no_of_fourier_coefficient * 4 + 21;
  values[start] = polygonal_features->major_ellipse_axis_length_coefficient;
  values[start + 1] = polygonal_features->minor_ellipse_axis_length_coefficient;
  values[start + 2] = polygonal_features->major_to_minor_coefficient;
  for(int i = 0; i < 7;i++){
    values[start + 3 + i] = polygonal_features->central_moment_invariants[i];
  }
  values[start + 10] = polygonal_features->normalized_feature_of_compactness;
  values[start + 11] = polygonal_features->normalized_feature_of_eccentricity;
  values[start + 12] = polygonal_features->mean_centroid_to_shape_boundary;
  values[start + 13] = polygonal_features->max_centroid_to_shape_boundary;
  values[start + 14] = polygonal_features->mean_centroid_over_max_centroid;
  values[start + 15] = polygonal_features->std_dev_centroid_to_shape_boundry;
  values[start + 16] = polygonal_features->kurtosis;
  return values;
}

perception_place_classification_t *classify_place(bot_core_planar_lidar_t *laser, svm_model_t *model, svm_model_t *doorway_model)
{

  //Change the feature configurations here. min_range, max_range, etc..
  feature_config_t config;
  config.min_range = 0.1;
  config.max_range = 30;
  config.cutoff = 5;
  config.gap_threshold_1 = 3;
  config.gap_threshold_2 = 0.5;
  config.deviation = 0.001;
  config.no_of_fourier_coefficient = 2;
  
  int no_features = config.no_of_fourier_coefficient * 4 + 38;
  
  double *feature_values = calculate_features(config, laser);
    
  svm_node_t *test_pt;
  test_pt = (svm_node_t *) calloc(no_features + 1, sizeof(svm_node_t));
  for(int i = 0;i < no_features;i++){
    test_pt[i].index = i + 1;
    test_pt[i].value = feature_values[i];
  }
  test_pt[no_features].index = -1;
  //predict label
  int label = (int)svm_predict(model, test_pt);

  //predict probability
  int class = svm_get_nr_class(model);
  // fprintf(stderr, "Class Num: %d\n", class); 
  double probability[class];
  if(svm_check_probability_model(model) == 0){
    fprintf(stderr, "Model does not have probability information.\n");
    for(int i = 0;i < class;i++)
	probability[i] = 0;
  }
  //probability distribution of svm is in probability[]
  double value = svm_predict_probability(model, test_pt, probability);

  //A list of classes
  char *classes[class];
  classes[0] = "elevator";
  classes[1] = "conference_room";
  classes[2] = "office";
  classes[3] = "lab";
  classes[4] = "open_area";
  classes[5] = "hallway";
  classes[6] = "corridor";
  classes[7] = "large_meeting_room";

  //Publish place_classification_t & place_classification_debug_t messages
  perception_place_classification_t *msg1 = (perception_place_classification_t *)calloc(1, sizeof(perception_place_classification_t));

  msg1->utime = bot_timestamp_now();
  msg1->sensor_type = PERCEPTION_PLACE_CLASSIFICATION_T_SENSOR_TYPE_SIMULATE_360_SKIRT;
  msg1->sensor_utime = laser->utime;
  msg1->pose_utime = msg1->sensor_utime;
  
  int confidence = check_confidence(probability, class);
  int doorway_label;
  if(doorway_model)
    doorway_label = (int)svm_predict(doorway_model, test_pt);
  //If confidence or doorway model is NULL or not a binary doorway
  if(confidence || !doorway_model || !doorway_label){
    char *predict;
    predict = classes[label - 1];
    msg1->label = predict; 
    msg1->no_class = class;
    msg1->classes = (perception_place_classification_class_t *)calloc(msg1->no_class, sizeof(perception_place_classification_class_t));

    for(int i = 0; i < class;i++){
      msg1->classes[i].name = i + 1;
      msg1->classes[i].probability = probability[i];
    }
  }
  //If not confident & doorway model is not NULL, check whether it is a doorway
  else{
    //if a doorway
    msg1->label = "doorway";
    msg1->no_class = class + 1;  //Add one more 'doorway'
    msg1->classes = (perception_place_classification_class_t *)calloc(msg1->no_class, sizeof(perception_place_classification_class_t));
    for(int i = 0;i < class;i++){
      msg1->classes[i].name = i + 1;
      msg1->classes[i].probability = probability[i];
    }
    msg1->classes[class].name = class + 1;
    msg1->classes[class].probability = 1;
  }
  

  //destroy features
  free(test_pt);  

  return msg1;
}
