

#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>

#include <stdlib.h>
#include <stdio.h>
#include <svm/svm.h>

int 
main(int argc, char **argv)
{
  FILE *train, *test;

  typedef struct svm_model svm_model_t;
  typedef struct svm_node svm_node_t;
  
  if(argc < 4){
    fprintf(stderr, "Please provide file names\n");
    return -1;
  }
  
  svm_model_t *model = (svm_model_t *)calloc(1, sizeof(svm_model_t));
  model = svm_load_model(argv[1]);
  train = fopen(argv[2],"r");
  test = fopen(argv[3],"r");

  int total_train_neg = 0;
  int total_train_pos = 0;
  int total_test_neg = 0;
  int total_test_pos = 0;
  
  int train_predict_neg_actual_neg = 0;
  int train_predict_neg_actual_pos = 0;
  int train_predict_pos_actual_neg = 0;
  int train_predict_pos_actual_pos = 0;

  int test_predict_neg_actual_neg = 0;
  int test_predict_neg_actual_pos = 0;
  int test_predict_pos_actual_neg = 0;
  int test_predict_pos_actual_pos = 0;

  char str[5000];
  int line_count = 0;
  if(!train || !test) {fprintf(stderr, "Not valid file\n"); return 1;} // bail out if file not found
  while(fgets(str,sizeof(str),train) != NULL)
    {

      char *cp = strdup(str);
      char *ch;
      ch = strtok(cp, " :");
      int i;
      float f;
      int index = 0;
      int count = 0;
      int real;
      int no_feature = 46;
      svm_node_t *test_pt;
      test_pt = (svm_node_t *)calloc(no_feature + 1, sizeof(svm_node_t));
      while(ch != NULL){
	//fprintf(stderr, "ch: %s\n", ch);
      
	if(sscanf(ch, "%f", &f) != 0){
	
	  if(count == 0){
	    int v = (int)f;
	    real = v;
	    if(real != 0 && real != 1){
	      fprintf(stderr, "ERROR. Invalid label.\n");
	      return -1;
	    }
	    count++;
	    ch = strtok(NULL, " :");
	    continue;
	  }
	  if(index % 2 == 0){
	    int v = (int)f;
	    test_pt[index / 2].index = v;
	    index++;
	    ch = strtok(NULL, " :");
	    continue;
	  }
	  if(index % 2 == 1){
	    test_pt[index / 2].value = f;
	    index++;
	    ch = strtok(NULL, " :");
	  }
	}
      }
      //fprintf(stderr, "Index: %d\n", (index - 1) / 2);
      test_pt[no_feature].index = -1;
      int label = (int)svm_predict(model, test_pt);
      if(real == 0){
	total_train_neg++;
	if(label == 0)
	  train_predict_neg_actual_neg++;
	else
	  train_predict_pos_actual_neg++;
      }
      else{
	total_train_pos++;
	if(label == 0)
	  train_predict_neg_actual_pos++;
	else
	  train_predict_pos_actual_pos++;
      }
    }


  //Test set
  while(fgets(str,sizeof(str),test) != NULL)
    {

      char *cp = strdup(str);
      char *ch;
      ch = strtok(cp, " :");
      int i;
      float f;
      int index = 0;
      int count = 0;
      int real;
      int no_feature = 46;
      svm_node_t *test_pt;
      test_pt = (svm_node_t *)calloc(no_feature + 1, sizeof(svm_node_t));
      while(ch != NULL){
	//fprintf(stderr, "ch: %s\n", ch);
      
	if(sscanf(ch, "%f", &f) != 0){
	
	  if(count == 0){
	    int v = (int)f;
	    real = v;
	    if(real != 0 && real != 1){
	      fprintf(stderr, "ERROR. Invalid label.\n");
	      return -1;
	    }
	    count++;
	    ch = strtok(NULL, " :");
	    continue;
	  }
	  if(index % 2 == 0){
	    int v = (int)f;
	    if(index / 2 < no_feature){
	      test_pt[index / 2].index = v;
	      index++;
	      //fprintf(stderr, "Index: %d\n", v);
	    }
	    ch = strtok(NULL, " :");
	    continue;
	  }
	  else if(index % 2 == 1){
	    test_pt[index / 2].value = f;
	    index++;
	    ch = strtok(NULL, " :");
	    //fprintf(stderr, "value: %f\n", f);
	  }
	}
      }
      test_pt[no_feature].index = -1;
      int label = (int)svm_predict(model, test_pt);
      if(real == 0){
	total_test_neg++;
	if(label == 0)
	  test_predict_neg_actual_neg++;
	else
	  test_predict_pos_actual_neg++;
      }
      else{
	total_test_pos++;
	if(label == 0)
	  test_predict_neg_actual_pos++;
	else
	  test_predict_pos_actual_pos++;
      }
    }
      
  fprintf(stderr, "Accuracy of training set: %.4f\n", 
	  (train_predict_neg_actual_neg + train_predict_pos_actual_pos) / (double)(total_train_neg + total_train_pos));
  fprintf(stderr, "Accuracy of test set: %.4f\n", 
	  (test_predict_neg_actual_neg + test_predict_pos_actual_pos) / (double)(total_test_neg + total_test_pos));

  fprintf(stderr, "\n");
  fprintf(stderr, "TRAINING SET:(REAL PREDICT)\n");
  fprintf(stderr, "(%d %d). Total: %d, No: %d, Percentage: %.4f\n", 0, 0, total_train_neg, train_predict_neg_actual_neg,
	  (double)train_predict_neg_actual_neg / total_train_neg);
  fprintf(stderr, "(%d %d). Total: %d, No: %d, Percentage: %.4f\n", 0, 1, total_train_neg, train_predict_pos_actual_neg,
	  (double)train_predict_pos_actual_neg / total_train_neg);
  fprintf(stderr, "(%d %d). Total: %d, No: %d, Percentage: %.4f\n", 1, 0, total_train_pos, train_predict_neg_actual_pos,
	  (double)train_predict_neg_actual_pos / total_train_pos);
  fprintf(stderr, "(%d %d). Total: %d, No: %d, Percentage: %.4f\n", 1, 1, total_train_pos, train_predict_pos_actual_pos,
	  (double)train_predict_pos_actual_pos / total_train_pos);

  fprintf(stderr, "\n");
  fprintf(stderr, "TEST SET:(REAL PREDICT)\n");
  fprintf(stderr, "(%d %d). Total: %d, No: %d, Percentage: %.4f\n", 0, 0, total_test_neg, test_predict_neg_actual_neg,
	  (double)test_predict_neg_actual_neg / total_test_neg);
  fprintf(stderr, "(%d %d). Total: %d, No: %d, Percentage: %.4f\n", 0, 1, total_test_neg, test_predict_pos_actual_neg,
	  (double)test_predict_pos_actual_neg / total_test_neg);
  fprintf(stderr, "(%d %d). Total: %d, No: %d, Percentage: %.4f\n", 1, 0, total_test_pos, test_predict_neg_actual_pos,
	  (double)test_predict_neg_actual_pos / total_test_pos);
  fprintf(stderr, "(%d %d). Total: %d, No: %d, Percentage: %.4f\n", 1, 1, total_test_pos, test_predict_pos_actual_pos,
	  (double)test_predict_pos_actual_pos / total_test_pos);
    
   
  fclose(train);
  fclose(test);
    
}
