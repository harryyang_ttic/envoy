#include <stdio.h>
#include <iostream>
#include <complex>
#include <math.h>
#include <pthread.h>

#include <string.h>

#include <gsl/gsl_blas.h>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
//#include <velodyne/velodyne.h>
//#include <velodyne/velodyne_extractor.h>
//#include <lcmtypes/erlcm_xyz_point_t.h>
//#include <lcmtypes/erlcm_seg_point_list_t.h>
#include <lcmtypes/er_lcmtypes.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include "segmenter.hpp"

#include <unistd.h>

typedef struct _state_t state_t;

struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    segmenter* seg;
    velodyne_extractor_state_t *velodyne;
    int64_t lastTime;
    bool drawHand;
    BotParam *param;
    BotFrames *frames;
    bot_lcmgl_t *lcmgl_full;
    bot_lcmgl_t *lcmgl_segments;
    bot_lcmgl_t *lcmgl_persons;
    svm_model_t *model;
};

static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -v      Set verbose to true.\n"
             "  -h      This help message.\n",
             g_path_get_basename(progname));
    exit(1);
}

static void publishSegments(lcm_t *lcm,
                std::map<int,std::vector<xyz_point_t> > &segPoints)
{
    std::map<int, std::vector<xyz_point_t> >::iterator mapIt;
    std::vector<xyz_point_t>::iterator vecIt;

    erlcm_segment_list_t msg;
    msg.utime = bot_timestamp_now();
    msg.no_segments = segPoints.size();
    msg.segments = NULL;

    msg.segments = (erlcm_seg_point_list_t*)realloc(msg.segments, msg.no_segments*sizeof(erlcm_seg_point_list_t));
    int j = 0;
    for (mapIt = segPoints.begin(); mapIt != segPoints.end(); mapIt++, j++)
    {
        
        erlcm_seg_point_list_t *seg_msg = &msg.segments[j];
        seg_msg->segment_id = (*mapIt).first;
        seg_msg->no_points = (*mapIt).second.size();

        seg_msg->points = (erlcm_xyz_point_t*)calloc(seg_msg->no_points,
                                                   sizeof(erlcm_xyz_point_t));
        int i = 0;
        for (vecIt = (*mapIt).second.begin();
             vecIt != (*mapIt).second.end(); vecIt++, i++)
        {
            seg_msg->points[i].xyz[0] = (*vecIt).xyz[0];
            seg_msg->points[i].xyz[1] = (*vecIt).xyz[1];
            seg_msg->points[i].xyz[2] = (*vecIt).xyz[2];
        }
    }
    
    erlcm_segment_list_t_publish(lcm, "PCL_SEGMENT_LIST", &msg);

    for (int k = 0; k > msg.no_segments; k++)
        free(msg.segments[k].points);

    free (msg.segments);

}

void point_source_cb(int64_t utime, void *data)
{
    state_t *s = (state_t *)data;

    xyz_point_list_t *ret = velodyne_extract_points_frame(s->velodyne, "body"); 
   
    if(ret != NULL && ret->utime > s->lastTime){
        s->lastTime = ret->utime;
        // check size
        std::map<int,segmenter::segment_t> segPoints;

        s->seg->buildGraph(ret, segPoints, NULL);

        std::cout << "No of Segments : " << segPoints.size() << std::endl;
        
        s->seg->getFeatures(segPoints, s->lcmgl_segments);

        double xyz_sensor[3] = {0}, xyz_local[3]; 
        //publish the features 
        double sensor_to_local[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, 
                                                      "VELODYNE","local", utime,
                                                      sensor_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        }
        
        bot_vector_affine_transform_3x4_3d (sensor_to_local, xyz_sensor, xyz_local); 

        //if we use local frame - need to send it the sensor's / or body position in local frame and the heading - to remove outlier points 
        
        // The size gets malloced in Get Person
        segmenter::person_feature_t pf;
        bool gotPerson = s->seg->getPerson(segPoints, pf, s->lcmgl_persons, s->model);

        s->seg->publishFeatures(utime, segPoints, s->lcm, "VELODYNE_PERSON_SEGMENTS");

        fprintf(stderr, "Got person : %d\n", gotPerson);

        std::vector<xyz_point_t> handPts;

        bool gotHand  = false;
        if (gotPerson)
        {
        //gotHand = s->seg->getHand(segPoints, pf, handPts);
        }
        
        fprintf(stderr, "Got hand : %d\n", gotHand);

        std::map<int,std::vector<xyz_point_t> > handPoints;
        if (gotHand)
        {
            handPoints[0] = handPts;
            
            if (s->drawHand == true)
                publishSegments(s->lcm, handPoints);

            erlcm_pointing_vector_t handVec;
            //s->seg->getHandVector(handPts, handVec);
            handVec.pos[0] = handPts[0].xyz[0];
            handVec.pos[1] = handPts[0].xyz[1];
            handVec.pos[2] = handPts[0].xyz[2];

            double p1[3], p2[3];
            p1[0] = handPts[0].xyz[0];
            p1[1] = handPts[0].xyz[1];
            p1[2] = handPts[0].xyz[2];

            p2[0] = handPts[handPts.size()-1].xyz[0];
            p2[1] = handPts[handPts.size()-1].xyz[1];
            p2[2] = handPts[handPts.size()-1].xyz[2];


            double norm = std::sqrt((p1[0]-p2[0])*(p1[0]-p2[0]) + (p1[1]-p2[1])*(p1[1]-p2[1]) + (p1[2]-p2[2])*(p1[2]-p2[2]));

            handVec.vec[0] = (p2[0]-p1[0]) / norm;
            handVec.vec[1] = (p2[1]-p1[1]) / norm;
            handVec.vec[2] = (p2[2]-p1[2]) / norm;
            
            
            // handVec.roll = atan((p2[1]-p1[1])/(p2[0]-p1[0]));
            // handVec.pitch = atan((p2[2]-p1[2])/(p2[1]-p1[1]));
            // handVec.yaw = atan((p2[0]-p1[0])/(p2[2]-p1[2]));

//            printf("%lf %lf %lf \t %lf %lf %lf\n",handVec.x, handVec.y, handVec.z, handVec.roll, handVec.pitch, handVec.yaw);

            // Convert to quaterion here?
            erlcm_pointing_vector_t_publish(s->lcm, "HAND_VECTOR", &handVec);
        }

        //free(sf);

        std::map<int, segmenter::segment_t >::iterator mapIt;
        int i = 0;
        for (mapIt = segPoints.begin(); mapIt != segPoints.end(); mapIt++, i++)
        {
            (*mapIt).second.points.clear();
        }
    }
    if (ret != NULL)
    {
        destroy_xyz_list(ret);
    }
    
}

int main(int argc, char **argv){
    printf("starting\n");

    int c;
    bool v = false;
    bool draw = false;
	bool standAlone = false;
    // command line options - to throtle - to ignore image publish  
    char *model_path = NULL;
    while ((c = getopt (argc, argv, "hvdsm:")) >= 0) {
    	switch (c) {
    	case 'v': //ignore images 
            v = true;
            printf("Setting verbose to true\n");
            break;
        case 'd':
            draw = true;
            printf("drawing hand\n");
            break;
      	case 'h':
            usage(argv[0]);
            break;            
        case 'm':
            model_path = strdup(optarg);
            fprintf(stderr, "Model Filename : %s\n", model_path);            
            break;
        }
    }
    state_t *state = (state_t*)calloc(1,sizeof(state_t));
    state->model = NULL;
    if(model_path != NULL){
        state->model=svm_load_model(model_path);
        if(state->model == NULL)
            fprintf(stderr,"Error loading model\n");
        else{
            fprintf(stderr, "Model loaded sucessfully\n");
        }
    }

    if(model_path != NULL)
        free(model_path);
    
    state->lcm = bot_lcm_get_global(NULL);
    state->lcmgl_full = bot_lcmgl_init(state->lcm, "velodyne_full");
    state->lcmgl_segments = bot_lcmgl_init(state->lcm, "velodyne_segments");
    state->lcmgl_persons = bot_lcmgl_init(state->lcm, "velodyne_persons");
    state->param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->param);
    state->velodyne = velodyne_extractor_init(state->lcm, point_source_cb, state);
    //state->velodyne = velodyne_extractor_init_full(state->lcm, 0, -M_PI/2, M_PI/2, point_source_cb, state);
    state->drawHand = draw;

    state->mainloop = g_main_loop_new(NULL, FALSE);

    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }

    bot_glib_mainloop_attach_lcm(state->lcm);

    /* heart beat*/
    //g_timeout_add (100, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);

    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);

    bot_glib_mainloop_detach_lcm(state->lcm);

}
