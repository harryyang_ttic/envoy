#include "segmenter.hpp"
#include "UnionFindSimple.hpp"
#include <unistd.h>

#define MIN_SIZE 100//1000//2000 //maybe this is the issue 
#define MAX_SIZE 20000
#define MIN_DIST 0.8//1.5
#define MAX_DIST 5.0//4.0//3.0
#define MAX_DIST_ADD 5.0
#define MIN_DIST_ADD 0.2
#define MAX_ANGLE M_PI/2
#define MIN_ANGLE -M_PI/2
#define MAX_HEIGHT 2.5
#define MIN_HEIGHT 0.1

#define NUM_ROWS 32
#define DEPTH_THRESHOLD 0.1//.05

#define NUM_HAND_POINTS 100

inline static double distance(double* xyz1, double*xyz2)
{
    double t = sqrt(pow(xyz1[0]-xyz2[0], 2) + pow(xyz1[1]-xyz2[1], 2));
    // Ignore the Z component since the lasers are far apart
    return t; 
}

inline static double ThreeDDistance(double* xyz1, double*xyz2)
{
    return sqrt(pow(xyz1[0]-xyz2[0], 2) 
                    + pow(xyz1[1]-xyz2[1], 2)
                    + pow(xyz1[2]-xyz2[2], 2));
}

void segmenter::buildGraph(xyz_point_list_t *xyzList, 
                           std::map<int,segmenter::segment_t> &segPoints,
                           bot_lcmgl_t *lcmgl)
{
    int64_t start = bot_timestamp_now();

    assert(xyzList->no_points%NUM_ROWS == 0);
    uint32_t num_cols = (xyzList->no_points)/NUM_ROWS;

    int *ind_map = (int *) calloc(xyzList->no_points, sizeof(int));
    int count = 0;
    int i,j;
    //prerun and find out which points need to be added 
    double dist = 0;
    double angle = 0;
    double height = 0;

    for (i = 0; i < xyzList->no_points; i++){
        dist  = hypot(xyzList->points[i].xyz[0], xyzList->points[i].xyz[1]);
        angle  = atan2(xyzList->points[i].xyz[1], xyzList->points[i].xyz[0]);
        height = xyzList->points[i].xyz[2];

        //skip over points we dont care about
        if( (dist < MIN_DIST_ADD || dist> MAX_DIST_ADD) || (angle < MIN_ANGLE || angle > MAX_ANGLE) || (height < MIN_HEIGHT || height > MAX_HEIGHT)) {
            ind_map[i] = -1;
            continue;
        }

        ind_map[i] = count;
        count++;
    }

    int64_t end_reassign= bot_timestamp_now();
    
    fprintf(stderr, "Point count - Orig : %d => Valid : %d\n", 
            xyzList->no_points, count);

    //UnionFindSimple uf(xyzList->no_points);

    UnionFindSimple *uf = new UnionFindSimple(xyzList->no_points);
    
    if(lcmgl){
        bot_lcmgl_color3f(lcmgl, 1.0, .0, 0);
    }

    // Velodyne data is ordered from [0-num_cols][0-NUM_ROWS] with the top
    // row first and the bottow row last
    for (i = 0; i < num_cols; i++)
    {
        for (j = 0; j < NUM_ROWS-1; j++)
        {
            int pos = i*NUM_ROWS+j;
            //this is wrapping around - which would not happen unless its 360 fov
            int next = (pos+NUM_ROWS)%xyzList->no_points;
            if(ind_map[pos] < 0)
                continue;

            if(lcmgl){
                bot_lcmgl_begin(lcmgl, GL_LINES);
            }

            if(ind_map[pos] >=0 &&  ind_map[pos + 1] >= 0){
                // Get the down point
                if (distance(xyzList->points[pos].xyz, xyzList->points[pos+1].xyz)< 
                    DEPTH_THRESHOLD)
                    {
                        uf->connectNodes(ind_map[pos], ind_map[pos+1]);
                        if(lcmgl){
                            bot_lcmgl_vertex3f(lcmgl, xyzList->points[pos].xyz[0], xyzList->points[pos].xyz[1], 
                                               xyzList->points[pos].xyz[2]);
                            bot_lcmgl_vertex3f(lcmgl, xyzList->points[pos+1].xyz[0], xyzList->points[pos+1].xyz[1], 
                                               xyzList->points[pos+1].xyz[2]);
                        }

                    }
            }

            if(lcmgl){
                bot_lcmgl_end(lcmgl);
                bot_lcmgl_begin(lcmgl, GL_LINES);
            }
            if(ind_map[pos] >=0 && ind_map[next] >= 0){
                if (distance(xyzList->points[pos].xyz, xyzList->points[next].xyz) < 
                    DEPTH_THRESHOLD)
                    {
                        uf->connectNodes(ind_map[pos] , ind_map[next]);
                        if(lcmgl){
                            bot_lcmgl_vertex3f(lcmgl, xyzList->points[pos].xyz[0], xyzList->points[pos].xyz[1], 
                                               xyzList->points[pos].xyz[2]);
                            bot_lcmgl_vertex3f(lcmgl, xyzList->points[next].xyz[0], xyzList->points[next].xyz[1], 
                                               xyzList->points[next].xyz[2]);
                        }
                    }
            }
            if(lcmgl){
                bot_lcmgl_end(lcmgl);
            }
        }
    }

    if(lcmgl){
        bot_lcmgl_switch_buffer(lcmgl);
    }

    for (i = 0; i < xyzList->no_points; i++){
        if(ind_map[i] < 0)
            continue;
        int rep = uf->getRepresentative(ind_map[i]);
        
        if (uf->getSetSize(rep) > MIN_SIZE && uf->getSetSize(rep) < MAX_SIZE)
            {
                segPoints[rep].points.push_back(xyzList->points[i]);
            }
    }
    
    free(ind_map);
    delete uf;
    int64_t end_final = bot_timestamp_now();
    
    fprintf(stderr, "Time to reassign : %f - Time for function : %f\n", (end_reassign - start)/1.0e6, 
    (end_final - start) /1.0e6);
}

void segmenter::getFeatures(std::map<int, segmenter::segment_t> &segPoints,
                            bot_lcmgl_t *lcmgl){
    
    std::map<int, segmenter::segment_t>::iterator mapIt;
    std::vector<xyz_point_t>::iterator vecIt;

    
    // Get mean point
    int i = 0;
    float c[4];
    for (mapIt = segPoints.begin(); mapIt != segPoints.end(); mapIt++, i++)
        {
            if(lcmgl){
                bot_lcmgl_begin(lcmgl, GL_POINTS);
                bot_color_util_rand_color(c, 0.7, 0.3);
                bot_lcmgl_color4f(lcmgl, c[0], c[1], c[2], c[3]);
            }

            (*mapIt).second.sf.meanPos[0] = 0;
            (*mapIt).second.sf.meanPos[1] = 0;
            (*mapIt).second.sf.meanPos[2] = 0;
            (*mapIt).second.sf.maxPos[0] = -100;
            (*mapIt).second.sf.maxPos[1] = -100;
            (*mapIt).second.sf.maxPos[2] = -100;
            (*mapIt).second.sf.minPos[0] = 100;
            (*mapIt).second.sf.minPos[1] = 100;
            (*mapIt).second.sf.minPos[2] = 100;

            //these are the segments 
            for (vecIt = (*mapIt).second.points.begin(); 
                 vecIt != (*mapIt).second.points.end(); vecIt++)
                {
                    (*mapIt).second.sf.meanPos[0] += (*vecIt).xyz[0];
                    (*mapIt).second.sf.meanPos[1] += (*vecIt).xyz[1];
                    (*mapIt).second.sf.meanPos[2] += (*vecIt).xyz[2];

                    for(int j=0; j < 3; j++){
                        if((*mapIt).second.sf.maxPos[j] < (*vecIt).xyz[j]){
                            (*mapIt).second.sf.maxPos[j] = (*vecIt).xyz[j];
                        }
                        if((*mapIt).second.sf.minPos[j] > (*vecIt).xyz[j]){
                            (*mapIt).second.sf.minPos[j] = (*vecIt).xyz[j];
                        }
                    }
                    if(lcmgl){
                        bot_lcmgl_vertex3f(lcmgl, (*vecIt).xyz[0], (*vecIt).xyz[1], (*vecIt).xyz[2]);
                    }
                }

            (*mapIt).second.sf.meanPos[0] /= (*mapIt).second.points.size();
            (*mapIt).second.sf.meanPos[1] /= (*mapIt).second.points.size();
            (*mapIt).second.sf.meanPos[2] /= (*mapIt).second.points.size();

            if(lcmgl){
                bot_lcmgl_end(lcmgl);
                bot_lcmgl_color3f(lcmgl, 0.0,1.0,0);
                char seg_info[1024];
                sprintf(seg_info, "Segment : %d \nMax [%.2f,%.2f,%.2f] \n [%.2f,%.2f,%.2f]\n", 
                        i, (*mapIt).second.sf.maxPos[0], (*mapIt).second.sf.maxPos[1], (*mapIt).second.sf.maxPos[2], 
                        (*mapIt).second.sf.minPos[0], (*mapIt).second.sf.minPos[1], (*mapIt).second.sf.minPos[2]);
                bot_lcmgl_text(lcmgl, (*mapIt).second.sf.meanPos, seg_info);
            }
        }

    if(lcmgl){
        bot_lcmgl_switch_buffer(lcmgl);
    }

    // Use centroid to get total error in each direction
    i = 0;
    for (mapIt = segPoints.begin(); mapIt != segPoints.end(); mapIt++, i++)
    {
        (*mapIt).second.sf.axisErr[0] = 0;
        (*mapIt).second.sf.axisErr[1] = 0;
        (*mapIt).second.sf.axisErr[2] = 0;

        double x = (*mapIt).second.sf.meanPos[0];
        double y = (*mapIt).second.sf.meanPos[1];
        double z = (*mapIt).second.sf.meanPos[2];

        for (vecIt = (*mapIt).second.points.begin(); 
             vecIt != (*mapIt).second.points.end(); vecIt++)
        {
            (*mapIt).second.sf.axisErr[0] += abs((*vecIt).xyz[0]-x);
            (*mapIt).second.sf.axisErr[1] += abs((*vecIt).xyz[1]-y);
            (*mapIt).second.sf.axisErr[2] += abs((*vecIt).xyz[2]-z);
        }
        (*mapIt).second.sf.axisErr[0] /= (*mapIt).second.points.size();
        (*mapIt).second.sf.axisErr[1] /= (*mapIt).second.points.size();
        (*mapIt).second.sf.axisErr[2] /= (*mapIt).second.points.size();
    }
}
struct temp{
    double x, y, z;
    double dist;
};

bool segmenter::getPerson(std::map<int,segmenter::segment_t> &segPoints,
                   segmenter::person_feature_t &pf, bot_lcmgl_t *lcmgl, svm_model_t *model)
{
    std::map<int,segmenter::segment_t>::iterator mapIt;
    bool ret = false;

    std::vector<int> mapErase;
    std::vector<xyz_point_t>::iterator vecIt;

    if(model != NULL){
        int svm_type=svm_get_svm_type(model);
        int nr_class=svm_get_nr_class(model);
        fprintf(stderr, "SVM type : %d SVM Class : %d\n", svm_type, nr_class);
    }
    
    float c[4];
    
    int no_features = 18;
    svm_node_t *test_pt = (svm_node_t *) calloc(no_features, sizeof(svm_node_t));

    int i = 0, skipping = 0;
    for (mapIt = segPoints.begin(); mapIt != segPoints.end(); mapIt++, i++)
    {
        double x = (*mapIt).second.sf.meanPos[0];
        double y = (*mapIt).second.sf.meanPos[1];
        double z = (*mapIt).second.sf.meanPos[2];

        double dist = sqrt(pow(x,2)+pow(y,2));
        
        // If the radius distance from the sensor is outside the range we're
        // looking for, or the are ignore that entry

        double dx = (*mapIt).second.sf.maxPos[0] - (*mapIt).second.sf.minPos[0];
        double dy = (*mapIt).second.sf.maxPos[1] - (*mapIt).second.sf.minPos[1];
        double dz = (*mapIt).second.sf.maxPos[2] - (*mapIt).second.sf.minPos[2];

        double r = hypot(dx,dy);

        //remove the segments 
        /*if (x < 0 || abs(y) > x || dist > MAX_DIST || dist < MIN_DIST ||
            (*mapIt).second.sf.axisErr[0] < (*mapIt).second.sf.axisErr[1] ||
            2*(*mapIt).second.sf.axisErr[1] < (*mapIt).second.sf.axisErr[2] ||
            2*(*mapIt).second.sf.axisErr[0] < (*mapIt).second.sf.axisErr[2])
            {*/
        //can we easily calculate the principle axis???
        
        if ((x < 0 ||
             dist > MAX_DIST) ||
            (dist < MIN_DIST))
        
        /*if((x < 0 ||
            dist > MAX_DIST) ||
           (dist < MIN_DIST) ||
           (dz > 1.9 || ((*mapIt).second.sf.maxPos[2] > 1.9 || (*mapIt).second.sf.maxPos[2] < 1.2))
           || r > 1.5)*/
            /*|| ((*mapIt).second.sf.axisErr[0] < (*mapIt).second.sf.axisErr[1] ||
                2*(*mapIt).second.sf.axisErr[1] < (*mapIt).second.sf.axisErr[2] ||
                2*(*mapIt).second.sf.axisErr[0] < (*mapIt).second.sf.axisErr[2]))*/
            {
            
                //(*mapIt).second.sf.axisErr[0] < (*mapIt).second.sf.axisErr[1] ||
                //2*(*mapIt).second.sf.axisErr[1] < (*mapIt).second.sf.axisErr[2] ||
                //2*(*mapIt).second.sf.axisErr[0] < (*mapIt).second.sf.axisErr[2])
            
                /*(*mapIt).second.sf.meanPos[0] = 0;
                (*mapIt).second.sf.meanPos[1] = 0;
                (*mapIt).second.sf.meanPos[2] = 0;
                skipping++;
                mapErase.push_back((*mapIt).first);*/
                (*mapIt).second.sf.is_valid = 0;
                continue;
            }

        else{
            (*mapIt).second.sf.is_valid = 1;
            
#ifdef HAS_PCL
            pcl::PointCloud<pcl::PointXYZ> segment_points;
            
            //these are the segments 
            for (vecIt = (*mapIt).second.points.begin(); 
                 vecIt != (*mapIt).second.points.end(); vecIt++)
                {
                    pcl::PointXYZ pt;
                    pt.x = (*vecIt).xyz[0];
                    pt.y = (*vecIt).xyz[1];
                    pt.z = (*vecIt).xyz[2];
                    segment_points.push_back(pt);
                    
                }
            
            pcl::PCA<pcl::PointXYZ>pca;//(segment_points);
            pca.setInputCloud(segment_points.makeShared());
            
            //std::cout << "PCA Mean " <<  pca.getMean() << " - EVectors" << pca.getEigenVectors() << std::endl;
            //std::cout << "Eigen Values " << pca.getEigenValues() << std::endl;
            
            Eigen::Vector4f xyz = pca.getMean();
            Eigen::MatrixXf eigen_vectors = pca.getEigenVectors();
            Eigen::VectorXf eigen_values = pca.getEigenValues();

            (*mapIt).second.sf.eigen_values[0] = eigen_values(0);
            (*mapIt).second.sf.eigen_values[1] = eigen_values(1);
            (*mapIt).second.sf.eigen_values[2] = eigen_values(2);

            (*mapIt).second.sf.xyz[0] = xyz(0);
            (*mapIt).second.sf.xyz[1] = xyz(1);
            (*mapIt).second.sf.xyz[2] = xyz(2);
            
            if(eigen_values(1) != 0)
                (*mapIt).second.sf.curvature1 = eigen_values(0)/eigen_values(1);
            else
                (*mapIt).second.sf.curvature1 = 10000000000; 

            if(eigen_values(2) != 0){
                (*mapIt).second.sf.curvature2 = eigen_values(0)/eigen_values(2);
                (*mapIt).second.sf.curvature3 = eigen_values(1)/eigen_values(2);
            }
            else{
                (*mapIt).second.sf.curvature2 = 10000000000; 
                (*mapIt).second.sf.curvature3 = 10000000000; 
            }

            std::cout << "Curvature " << (*mapIt).second.sf.curvature1 << std::endl;

            uint32_t maxPos = 0;
            double maxNorm = 0;
            
            uint32_t r = 0, g = 0, b = 0;
            Eigen::Vector3f first_principle_axis(3), second_principle_axis(3), third_principle_axis(3);
            (*mapIt).second.sf.first_principle_axis[0] = eigen_vectors(0,0);
            (*mapIt).second.sf.first_principle_axis[1] = eigen_vectors(1,0);
            (*mapIt).second.sf.first_principle_axis[2] = eigen_vectors(2,0);

            (*mapIt).second.sf.second_principle_axis[0] = eigen_vectors(0,1);
            (*mapIt).second.sf.second_principle_axis[1] = eigen_vectors(1,1);
            (*mapIt).second.sf.second_principle_axis[2] = eigen_vectors(2,1);

            (*mapIt).second.sf.third_principle_axis[0] = eigen_vectors(0,2);
            (*mapIt).second.sf.third_principle_axis[1] = eigen_vectors(1,2);
            (*mapIt).second.sf.third_principle_axis[2] = eigen_vectors(2,2);

            first_principle_axis(0) = eigen_vectors(0,0);
            first_principle_axis(1) = eigen_vectors(1,0);
            first_principle_axis(2) = eigen_vectors(2,0);
            second_principle_axis(0) = eigen_vectors(0,1);
            second_principle_axis(1) = eigen_vectors(1,1);
            second_principle_axis(2) = eigen_vectors(2,1);

            third_principle_axis(0) = eigen_vectors(0,2);
            third_principle_axis(1) = eigen_vectors(1,2);
            third_principle_axis(2) = eigen_vectors(2,2);
            
            (*mapIt).second.sf.first_principle_max = -1000;
            (*mapIt).second.sf.first_principle_min = 1000;
            (*mapIt).second.sf.second_principle_max = -1000;
            (*mapIt).second.sf.second_principle_min = 1000;
            (*mapIt).second.sf.third_principle_max = -1000;
            (*mapIt).second.sf.third_principle_min = 1000;
            
            //volume??
            (*mapIt).second.sf.size = (*mapIt).second.points.size() * pow((*mapIt).second.sf.xyz[2],2);

            double std_dev_first_1 = 0;
            double std_dev_first_2 = 0;
            double std_dev_second_1 = 0;
            double std_dev_second_2 = 0;
            double std_dev_third_1 = 0;
            double std_dev_third_2 = 0;

            //std::vector<uint32_t>::iterator it;
            std::vector<xyz_point_t>::iterator it;
            for (it = (*mapIt).second.points.begin(); it != (*mapIt).second.points.end(); it++)
                {
                    Eigen::Vector3f vec;
                    
                    vec(0) = (*it).xyz[0] - (*mapIt).second.sf.xyz[0];
                    vec(1) = (*it).xyz[1] - (*mapIt).second.sf.xyz[1];
                    vec(2) = (*it).xyz[2] - (*mapIt).second.sf.xyz[2];
                    
                    double dot = 
                        vec(0) * first_principle_axis(0) +
                        vec(1) * first_principle_axis(1) +
                        vec(2) * first_principle_axis(2);
                    std_dev_first_1 += pow(dot,2);
                    std_dev_first_2 += dot;
                    
                    if (dot > (*mapIt).second.sf.first_principle_max)
                        (*mapIt).second.sf.first_principle_max = dot;
                    else if (dot < (*mapIt).second.sf.first_principle_min)
                        (*mapIt).second.sf.first_principle_min = dot;
                    
                    double dot2 = 
                        vec(0) * second_principle_axis(0) +
                        vec(1) * second_principle_axis(1) +
                        vec(2) * second_principle_axis(2);
                    std_dev_second_1 += pow(dot2,2);
                    std_dev_second_2 += dot2;
                    
                    if (dot2 > (*mapIt).second.sf.second_principle_max)
                        (*mapIt).second.sf.second_principle_max = dot2;
                    else if (dot2 < (*mapIt).second.sf.second_principle_min)
                        (*mapIt).second.sf.second_principle_min = dot2;

                    double dot3 = 
                        vec(0) * third_principle_axis(0) +
                        vec(1) * third_principle_axis(1) +
                        vec(2) * third_principle_axis(2);
                    std_dev_third_1 += pow(dot2,2);
                    std_dev_third_2 += dot2;
                    
                    if (dot3 > (*mapIt).second.sf.third_principle_max)
                        (*mapIt).second.sf.third_principle_max = dot3;
                    else if (dot3 < (*mapIt).second.sf.third_principle_min)
                        (*mapIt).second.sf.third_principle_min = dot3;
                    
                }

            int size = (*mapIt).second.points.size();
            (*mapIt).second.sf.no_points = size;
            (*mapIt).second.sf.first_std_dev = sqrt(std_dev_first_1/size - 
                                    pow(std_dev_first_2/size,2));
            (*mapIt).second.sf.second_std_dev = sqrt(std_dev_second_1/size - 
                                     pow(std_dev_second_2/size,2));
            (*mapIt).second.sf.third_std_dev = sqrt(std_dev_third_1/size - 
                                     pow(std_dev_third_2/size,2));

            double grnd_plane_normal[3] = {0,0,1};

            double dot_normal = grnd_plane_normal[0] * first_principle_axis[0] + 
                grnd_plane_normal[1] * first_principle_axis[1] + 
                grnd_plane_normal[2] * first_principle_axis[2];

            (*mapIt).second.sf.grnd_plane_normal_product = fabs(dot_normal); 
            
            
            test_pt[0].index = 1;
            test_pt[0].value = (*mapIt).second.sf.eigen_values[0];

            test_pt[1].index = 2;
            test_pt[1].value = (*mapIt).second.sf.eigen_values[1];
            
            test_pt[2].index = 3;
            test_pt[2].value = (*mapIt).second.sf.eigen_values[2];

            test_pt[3].index = 4;
            test_pt[3].value = (*mapIt).second.sf.first_principle_max;

            test_pt[4].index = 5;
            test_pt[4].value = (*mapIt).second.sf.first_principle_min;

            test_pt[5].index = 6;
            test_pt[5].value = (*mapIt).second.sf.second_principle_max;

            test_pt[6].index = 7;
            test_pt[6].value = (*mapIt).second.sf.second_principle_min;

            test_pt[7].index = 8;
            test_pt[7].value = (*mapIt).second.sf.third_principle_max;

            test_pt[8].index = 9;
            test_pt[8].value = (*mapIt).second.sf.third_principle_min;

            test_pt[9].index = 10;
            test_pt[9].value = (*mapIt).second.sf.first_std_dev;

            test_pt[10].index = 11;
            test_pt[10].value = (*mapIt).second.sf.second_std_dev;

            test_pt[11].index = 12;
            test_pt[11].value = (*mapIt).second.sf.third_std_dev;


            test_pt[12].index = 13;
            test_pt[12].value = (*mapIt).second.sf.grnd_plane_normal_product;
            
            test_pt[13].index = 14;
            test_pt[13].value = (*mapIt).second.sf.curvature1;

            test_pt[14].index = 15;
            test_pt[14].value = (*mapIt).second.sf.curvature2;

            test_pt[15].index = 16;
            test_pt[15].value = (*mapIt).second.sf.curvature3;

            test_pt[16].index = 17;
            test_pt[16].value = (*mapIt).second.sf.no_points;

            test_pt[17].index = -1;

            double predict_label = 0;
            if(model != NULL){
                //seems to be decent - need to use height as a feature 
                predict_label = svm_predict(model,test_pt);
                fprintf(stderr,"[%d] Classification result %f\n", i, predict_label);
            }
            else{//use the handcoded clasification 
                if(((*mapIt).second.sf.curvature1 < 4.0 || (*mapIt).second.sf.curvature1 > 25.0) || (*mapIt).second.sf.curvature2 > 80.0 || /*(((*mapIt).second.sf.curvature2 < 200.0 || (*mapIt).second.sf.curvature2 > 350.0) || */ fabs(dot_normal) < 0.7){
                    //continue;
                    predict_label = 0;
                }
                else{
                    predict_label = 1;
                }
            }
            
            if(predict_label == 1){
                (*mapIt).second.sf.is_person = 1;
            }
            else{
                (*mapIt).second.sf.is_person = 0;
            }

#endif
            
            if(lcmgl){
                bot_lcmgl_begin(lcmgl, GL_POINTS);
                bot_color_util_rand_color(c, 0.7, 0.3);
                bot_lcmgl_color4f(lcmgl, c[0], c[1], c[2], c[3]);
            
                if((*mapIt).second.sf.is_person != 1){
                    continue;
                }
                
                //these are the segments 
                for (vecIt = (*mapIt).second.points.begin(); 
                     vecIt != (*mapIt).second.points.end(); vecIt++)
                    {                                        
                        bot_lcmgl_vertex3f(lcmgl, (*vecIt).xyz[0], (*vecIt).xyz[1], (*vecIt).xyz[2]);
                    }

                bot_lcmgl_end(lcmgl);
                //draw the principle axis 
                bot_lcmgl_color3f(lcmgl, 1,0,0);
                bot_lcmgl_begin(lcmgl, GL_LINES);
                bot_lcmgl_vertex3f(lcmgl, (*mapIt).second.sf.meanPos[0], (*mapIt).second.sf.meanPos[1], (*mapIt).second.sf.meanPos[2]);
                double v_pos[3] = {(*mapIt).second.sf.meanPos[0] + (*mapIt).second.sf.first_principle_axis[0], (*mapIt).second.sf.meanPos[1] + (*mapIt).second.sf.first_principle_axis[1], (*mapIt).second.sf.meanPos[2] + (*mapIt).second.sf.first_principle_axis[2]};
                bot_lcmgl_vertex3f(lcmgl, v_pos[0], v_pos[1], v_pos[2]);
                bot_lcmgl_end(lcmgl);
                bot_lcmgl_begin(lcmgl, GL_LINES);
                bot_lcmgl_color3f(lcmgl, 0,1.0,0);
                bot_lcmgl_vertex3f(lcmgl, (*mapIt).second.sf.meanPos[0], (*mapIt).second.sf.meanPos[1], (*mapIt).second.sf.meanPos[2]);
                double v_pos1[3] = {(*mapIt).second.sf.meanPos[0] + (*mapIt).second.sf.second_principle_axis[0], (*mapIt).second.sf.meanPos[1] + (*mapIt).second.sf.second_principle_axis[1], (*mapIt).second.sf.meanPos[2] + (*mapIt).second.sf.second_principle_axis[2]};
                bot_lcmgl_vertex3f(lcmgl, v_pos1[0], v_pos1[1], v_pos1[2]);
                bot_lcmgl_end(lcmgl);
                bot_lcmgl_begin(lcmgl, GL_LINES);
                bot_lcmgl_color3f(lcmgl, 0,.0,1.0);
                bot_lcmgl_vertex3f(lcmgl, (*mapIt).second.sf.meanPos[0], (*mapIt).second.sf.meanPos[1], (*mapIt).second.sf.meanPos[2]);
                double v_pos2[3] = {(*mapIt).second.sf.meanPos[0] + (*mapIt).second.sf.third_principle_axis[0], (*mapIt).second.sf.meanPos[1] + (*mapIt).second.sf.third_principle_axis[1], (*mapIt).second.sf.meanPos[2] + (*mapIt).second.sf.third_principle_axis[2]};
                bot_lcmgl_vertex3f(lcmgl, v_pos2[0], v_pos2[1], v_pos2[2]);
                bot_lcmgl_end(lcmgl);

            }
            
            
            if(lcmgl){
                bot_lcmgl_color3f(lcmgl, 1.0,0.0,0);
                char seg_info[1024];
                /*sprintf(seg_info, "Segment : %d \nMax [%.2f,%.2f,%.2f] \n [%.2f,%.2f,%.2f]\n", 
                  i, (*mapIt).second.sf.maxPos[0], (*mapIt).second.sf.maxPos[1], (*mapIt).second.sf.maxPos[2], 
                  (*mapIt).second.sf.minPos[0], (*mapIt).second.sf.minPos[1], (*mapIt).second.sf.minPos[2]);*/
                //Eigen::Vector4f eigVal = pca.getEigenValues();

                sprintf(seg_info, "Segment : %d \nCurve: %.2f, %.2f, max : %.2f,%.2f,%.2f \nmin : %.2f,%.2f,%.2f\nDev : %.2f,%.2f,%.2f", 
                        i,  
                        (*mapIt).second.sf.curvature1, (*mapIt).second.sf.curvature2, (*mapIt).second.sf.first_principle_max, (*mapIt).second.sf.second_principle_max, (*mapIt).second.sf.third_principle_max, 
                        (*mapIt).second.sf.first_principle_min, (*mapIt).second.sf.second_principle_min, (*mapIt).second.sf.third_principle_min, 
                        (*mapIt).second.sf.first_std_dev, (*mapIt).second.sf.second_std_dev,  (*mapIt).second.sf.third_std_dev);
                /*sprintf(seg_info, "Seg : [%d] PCA : mean : %f \n", //Eigen Values : %f, %f, %f", 
                  pca.getMean() );//, eigVal[0], eigVal[0], eigVal[0], eigVal[0]);
                  //pca.getEigenValues()[0], pca.getEigenValues()[1], pca.getEigenValues()[2]);*/
                bot_lcmgl_text(lcmgl, (*mapIt).second.sf.meanPos, seg_info);
            }
        }
    }     

    free(test_pt);
    
    if(lcmgl){
        bot_lcmgl_switch_buffer(lcmgl);
    }

    /*std::vector<int>::iterator eraseIt;
    for (eraseIt = mapErase.begin(); eraseIt < mapErase.end(); eraseIt++)
    {
        segPoints.erase(*eraseIt);
    }

     for (mapIt = segPoints.begin(); mapIt != segPoints.end(); mapIt++, i++)
    {
        double z = (*mapIt).second.sf.meanPos[2];

        if (!((*mapIt).second.sf.meanPos[0] == 0 &&
              (*mapIt).second.sf.meanPos[1] == 0 &&
              (*mapIt).second.sf.meanPos[2] == 0))
            {
                pf.is_person = true;
                pf.sf.meanPos[0] = (*mapIt).second.sf.meanPos[0];
                pf.sf.meanPos[1] = (*mapIt).second.sf.meanPos[1];
                pf.sf.meanPos[2] = (*mapIt).second.sf.meanPos[2];
                
                pf.sf.axisErr[0] = (*mapIt).second.sf.axisErr[0];
                pf.sf.axisErr[1] = (*mapIt).second.sf.axisErr[1];
                pf.sf.axisErr[2] = (*mapIt).second.sf.axisErr[2];
                break;
            }
            }*/
     
    if (segPoints.size() == 1)
        ret = true;

    return ret;
}

void segmenter::publishFeatures(int64_t utime, std::map<int,segmenter::segment_t> &segPoints,
                     lcm_t *lcm, const char *channel){
    if(segPoints.size() == 0)
        return;
    std::map<int,segmenter::segment_t>::iterator mapIt;
    std::vector<temp> minSet;
    std::vector<xyz_point_t>::iterator vecIt;
    double min = 10000;
    int minPos = 0, i =0;
    erlcm_segment_feature_list_t msg;
    msg.utime = utime;
    msg.count = segPoints.size();
    msg.list = (erlcm_segment_feature_t *) calloc(msg.count, sizeof(erlcm_segment_feature_t));
    for (mapIt = segPoints.begin(); mapIt != segPoints.end(); mapIt++,  i++)
    {
        erlcm_segment_feature_t *fe = &msg.list[i];
        fprintf(stderr, "Segment : %d No Points : %d Mean : %f,%f,%f => valid : %d person : %d\n", i, (*mapIt).second.points.size(), (*mapIt).second.sf.meanPos[0], 
                (*mapIt).second.sf.meanPos[1], (*mapIt).second.sf.meanPos[2], (*mapIt).second.sf.is_valid, (*mapIt).second.sf.is_person);

        fe->id = i;
        memcpy(fe->axis_error, (*mapIt).second.sf.axisErr, sizeof(double) * 3);
        memcpy(fe->mean_position, (*mapIt).second.sf.meanPos, sizeof(double) * 3);
        memcpy(fe->max_position, (*mapIt).second.sf.maxPos, sizeof(double) * 3);
        memcpy(fe->min_position, (*mapIt).second.sf.minPos, sizeof(double) * 3);
        
        memcpy(fe->first_principle_axis, (*mapIt).second.sf.first_principle_axis, sizeof(double) * 3);
        memcpy(fe->second_principle_axis, (*mapIt).second.sf.second_principle_axis, sizeof(double) * 3);
        memcpy(fe->third_principle_axis, (*mapIt).second.sf.third_principle_axis, sizeof(double) * 3);
        memcpy(fe->eigen_values, (*mapIt).second.sf.eigen_values, sizeof(double) * 3);
        fe->first_principle_max = (*mapIt).second.sf.first_principle_max;
        fe->first_principle_min = (*mapIt).second.sf.first_principle_min;
        fe->second_principle_max = (*mapIt).second.sf.second_principle_max;
        fe->second_principle_max = (*mapIt).second.sf.second_principle_min;
        fe->third_principle_max = (*mapIt).second.sf.third_principle_max;
        fe->third_principle_max = (*mapIt).second.sf.third_principle_min;
        fe->dot_product_with_ground_normal = (*mapIt).second.sf.grnd_plane_normal_product;
        fe->first_std_dev = (*mapIt).second.sf.first_std_dev;
        fe->second_std_dev = (*mapIt).second.sf.second_std_dev;
        fe->third_std_dev = (*mapIt).second.sf.third_std_dev;

        fe->curvature1 = (*mapIt).second.sf.curvature1;
        fe->curvature2 = (*mapIt).second.sf.curvature2;
        fe->curvature3 = (*mapIt).second.sf.curvature3;

        fe->size = (*mapIt).second.sf.size;
        fe->no_points = (*mapIt).second.sf.no_points;
        
        fe->point_size = (*mapIt).second.points.size();
        //something weird with calloc 
        fe->points = (erlcm_xyz_point_t *) malloc(fe->point_size * sizeof(erlcm_xyz_point_t));

        fprintf(stderr, "Size of Points : %d\n", (*mapIt).second.points.size());

        int j=0;
        //std::vector<uint32_t>::iterator it;
        std::vector<xyz_point_t>::iterator it;
        for (it = (*mapIt).second.points.begin(); it != (*mapIt).second.points.end(); it++, j++)
            {
                if(j >=  fe->point_size){
                    fprintf(stderr, "Error - more elements than allocated - %d %d\n", j , fe->point_size); 
                }
                fe->points[j].xyz[0] = (*it).xyz[0];
                fe->points[j].xyz[1] = (*it).xyz[1];
                fe->points[j].xyz[2] = (*it).xyz[2];
            }
        //if(fe->points != NULL)
        //free(fe->points);
    }
    
    //publish
    erlcm_segment_feature_list_t_publish(lcm, channel, &msg);
    for(int i=0; i < msg.count ;i++){
        free(msg.list[i].points);
    }

    free(msg.list);
}

bool segmenter::getHand(std::map<int,segmenter::segment_t> &segPoints,
             segmenter::person_feature_t &pf,
             std::vector<xyz_point_t> &handPts)
{
    assert(segPoints.size() == 1);
    bool ret = false;

    std::map<int,segmenter::segment_t> ::iterator mapIt;
    std::vector<temp> minSet;
    std::vector<xyz_point_t>::iterator vecIt;
    double min = 10000;
    int minPos = 0;
    for (mapIt = segPoints.begin(); mapIt != segPoints.end(); mapIt++)
    {
        double xyz[3];
        xyz[0] = pf.sf.meanPos[0];
        xyz[1] = pf.sf.meanPos[1];
        xyz[2] = pf.sf.meanPos[2];


        if (xyz[0] == 0 || xyz[1] == 0 || xyz[2] == 0) continue;

        for (vecIt = (*mapIt).second.points.begin(); 
            vecIt != (*mapIt).second.points.end(); vecIt++)
        {
            // If we have less than NUM_HAND_POINTS, add it regardless
            
            if (minSet.size() < NUM_HAND_POINTS)
            {
                temp t;
                t.x = (*vecIt).xyz[0];
                t.y = (*vecIt).xyz[1];
                t.z = (*vecIt).xyz[2];
//                t.dist = ThreeDDistance((*vecIt).xyz, xyz);
                t.dist = distance((*vecIt).xyz, xyz);

                minSet.push_back(t);
                if (t.dist < min) {
                    min = t.dist;
                    minPos = minSet.size();
                }
                continue;
            }
            //double dist = ThreeDDistance((*vecIt).xyz, xyz);
            double dist = distance((*vecIt).xyz, xyz);
            // If the distance is less than the max in the minSet, replace
            if (dist > min)
            {
                minSet[minPos].x = (*vecIt).xyz[0];
                minSet[minPos].y = (*vecIt).xyz[1];
                minSet[minPos].z = (*vecIt).xyz[2];
                minSet[minPos].dist = dist;

                // Find new min and minPos
                int k = 0;
                min = 10000;
                for (k=0;k<NUM_HAND_POINTS;k++)
                {
                    if (minSet[k].dist < min)
                    {
                        min = minSet[k].dist;
                        minPos = k;
                    }
                }
            }
        }
    }
    // Now have a vector (minSet) with the farthest NUM_HAND_POINTS points
        
    int i;

    for (i = 1; i < NUM_HAND_POINTS; i++)
    {
        temp t = minSet[i];
        int j;
        for (j=i-1; j>= 0 && minSet[j].dist > t.dist; j--)
        {
            memcpy(&minSet[j+1], &minSet[j], sizeof(temp));
        }
        memcpy(&minSet[j+1], &t, sizeof(temp));;
    }
        
    double minDist = 0.15;
    
    if (minSet[0].dist < minDist) 
    {
        ret = false;
//        printf("points are not far enough away\n");
    }
    else
    {
        double farthest[3];

        farthest[0] = minSet[NUM_HAND_POINTS-1].x;
        farthest[1] = minSet[NUM_HAND_POINTS-1].y;
        farthest[2] = minSet[NUM_HAND_POINTS-1].z;
        ret = true;
        for (i = 0; i < NUM_HAND_POINTS; i++)
        {
            double dist;

            xyz_point_t t;
            t.xyz[0] = minSet[i].x;
            t.xyz[1] = minSet[i].y;
            t.xyz[2] = minSet[i].z;

            if (ThreeDDistance(t.xyz, farthest) > 0.6)
            {
                ret = false;
                handPts.clear();
//                printf("Distance is too far in 3d\n");
                break;
            }

            handPts.push_back(t);
        }
    }

    return ret;
}

void segmenter::getHandVector(std::vector<xyz_point_t> &handPts, erlcm_point_t &handVec)
{
    handVec.x = handPts[0].xyz[0];
    handVec.y = handPts[0].xyz[1];
    handVec.z = handPts[0].xyz[2];

    double p1[3], p2[3];
    p1[0] = handPts[0].xyz[0];
    p1[1] = handPts[0].xyz[1];
    p1[2] = handPts[0].xyz[2];

    p2[0] = handPts[handPts.size()-1].xyz[0];
    p2[1] = handPts[handPts.size()-1].xyz[1];
    p2[2] = handPts[handPts.size()-1].xyz[2];
    double roll = atan((p2[1]-p1[1])/(p2[0]-p1[0]));
    double pitch = atan((p2[2]-p1[2])/(p2[1]-p1[1]));
    double yaw = atan((p2[0]-p1[0])/(p2[2]-p1[2]));

    handVec.roll = roll;
    handVec.pitch = pitch;
    handVec.yaw = yaw;
}



segmenter::segmenter(bool verbose)
{
    v = verbose;

    if (v) printf("starting segmenter\n");
}
/*
~segmenter()
{
    free(threshDepth);
}
*/

