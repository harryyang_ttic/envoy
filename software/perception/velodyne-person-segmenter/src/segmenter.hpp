#include <stdio.h>
#include <math.h>
#include <time.h>
#include <vector>
#include <map>

#include <velodyne/velodyne.h>
#include <velodyne/velodyne_extractor.h>
#include <lcmtypes/er_lcmtypes.h>
#include <bot_lcmgl_client/lcmgl.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif 

#define HAS_PCL

#ifdef HAS_PCL
// For Eigen
#include "pcl/common/common_headers.h"
#include "pcl/common/pca.h"
#include <pcl/pcl_base.h>
#endif

#include <bot_core/bot_core.h>
#include <lcmtypes/er_lcmtypes.h>

#include <svm/svm.h>



typedef struct svm_node svm_node_t;
typedef struct svm_model svm_model_t;

class segmenter{

    bool v;

    public:

    typedef struct _segment_feature_t {
        double meanPos[3];
        double axisErr[3];                      
        double maxPos[3];
        double minPos[3];

        double xyz[3];
        double first_principle_axis[3];
        double second_principle_axis[3];
        double third_principle_axis[3];
        double grnd_plane_normal_product; //absolute
        double eigen_values[3];


        double first_principle_max;
        double first_principle_min;
        double second_principle_max;
        double second_principle_min;
        double third_principle_max;
        double third_principle_min;

        double first_std_dev;
        double second_std_dev;
        double third_std_dev;

        double curvature1;
        double curvature2;
        double curvature3;
        
        int no_points;
        uint32_t size;
        int is_valid;
        int is_person;
    } segment_feature_t;
    
    typedef struct _segment_t{
        uint32_t id;
        segment_feature_t sf;
        std::vector<xyz_point_t> points;
    } segment_t;    

    typedef struct _person_feature_t {
        segment_feature_t sf;
        bool is_person;
    } person_feature_t;

    double getTimeDiff(timeval a, timeval b)
    {
        double first = a.tv_sec + (a.tv_usec/1000000.0);
        double second = b.tv_sec + (b.tv_usec/1000000.0);

        return (first - second)*1000;
    }
    
    void buildGraph(xyz_point_list_t *xyzList, 
                    std::map<int,segmenter::segment_t> &segPoints,
                    bot_lcmgl_t *lcmgl);

    void doNothing(//std::map<int, segmenter::segment_t> &segPoints,
                              bot_lcmgl_t *lcmgl);
    
    void getFeatures(std::map<int, segmenter::segment_t> &segPoints,
                     bot_lcmgl_t *lcmgl);
                     
    bool getPerson(std::map<int,segmenter::segment_t> &segPoints,
                   segmenter::person_feature_t &pf, bot_lcmgl_t *lcmgl,
                   svm_model_t *model);
                     
    bool getHand(std::map<int,segmenter::segment_t> &segPoints,
                 segmenter::person_feature_t &pf,
                 std::vector<xyz_point_t> &handPts);

    void getHandVector(std::vector<xyz_point_t> &handPts, erlcm_point_t &handVec);
    
    segmenter(bool verbose);
    
    void publishFeatures(int64_t utime, std::map<int,segmenter::segment_t> &segPoints,
                         lcm_t *lcm, const char *channel);
};
