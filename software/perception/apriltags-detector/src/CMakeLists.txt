cmake_minimum_required (VERSION 2.6)

link_libraries(apriltags)

#add_executable(sm-apriltags_demo apriltags_demo.cpp Serial.cpp)
#pods_use_pkg_config_packages(sm-apriltags_demo apriltags opencv)
#pods_install_executables(sm-apriltags_demo)

add_executable(er-apriltags-detector apriltags_detector.cpp) #Serial.cpp)
pods_use_pkg_config_packages(er-apriltags-detector apriltags opencv bot2-core bot2-frames bot2-lcmgl-client jpeg-utils lcmtypes_er-lcmtypes) #image_io_utils 
pods_install_executables(er-apriltags-detector)

