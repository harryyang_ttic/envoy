/**
 * @file april_tags.cpp
 * @brief Example application for April tags library
 * @author: Michael Kaess
 *
 * Opens the first available camera (typically a built in camera in a
 * laptop) and continuously detects April tags in the incoming
 * images. Detections are both visualized in the live image and shown
 * in the text console. Optionally allows selecting of a specific
 * camera in case multiple ones are present and specifying image
 * resolution as long as supported by the camera. Also includes the
 * option to send tag detections via a serial port, for example when
 * running on a Raspberry Pi that is connected to an Arduino.
 */

using namespace std;

#include <iostream>
#include <cstring>
#include <vector>
#include <sys/time.h>

const string usage = "\n"
    "Usage:\n"
    "  apriltags_demo [OPTION...] [deviceID]\n"
    "\n"
    "Options:\n"
    "  -h  -?          Show help options\n"
    "  -a              Arduino (send tag ids over serial port)\n"
    "  -d              disable graphics\n"
    "  -C <bbxhh>      Tag family (default 36h11)\n"
    "  -F <fx>         Focal length in pixels\n"
    "  -W <width>      Image width (default 640, availability depends on camera)\n"
    "  -H <height>     Image height (default 480, availability depends on camera)\n"
    "  -S <size>       Tag size (square black frame) in meters\n"
    "  -E <exposure>   Manually set camera exposure (default auto; range 0-10000)\n"
    "  -G <gain>       Manually set camera gain (default auto; range 0-255)\n"
    "  -B <brightness> Manually set the camera brightness (default 128; range 0-255)\n"
    "\n";

const string intro = "\n"
    "April tags test code\n"
    "(C) 2012-2013 Massachusetts Institute of Technology\n"
    "Michael Kaess\n"
    "\n";


#ifndef __APPLE__
#define EXPOSURE_CONTROL // only works in Linux
#endif

#include <GL/gl.h>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/features2d/features2d.hpp>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>

#include <jpeg-utils/jpeg-utils.h>

#include <lcmtypes/erlcm_apriltags_detection_list_t.h> 
#ifdef EXPOSURE_CONTROL
#include <libv4l2.h>
#include <linux/videodev2.h>
#include <fcntl.h>
#include <errno.h>
#endif

#define OBS_DIST_THRESHOLD 10.0
#define PRUNE_OBJECT_THRESHOLD 10.0 //prune objects outside 10m distance

// OpenCV library for easy access to USB camera and drawing of images
// on screen
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/imgproc/types_c.h>

// April tags detector and various families that can be selected by command line option
#include "AprilTags/TagDetector.h"
#include "AprilTags/Tag16h5.h"
#include "AprilTags/Tag25h7.h"
#include "AprilTags/Tag25h9.h"
#include "AprilTags/Tag36h9.h"
#include "AprilTags/Tag36h11.h"


// Needed for getopt / command line options processing
#include <unistd.h>
extern int optind;
extern char *optarg;

const char* window_name = "apriltags_demo";

class DetObject;
typedef pair<pair<DetObject *, DetObject *>, double> detectionPair;

// utility function to provide current system time (used below in
// determining frame rate at which images are being processed)
double tic() {
    struct timeval t;
    gettimeofday(&t, NULL);
    return ((double)t.tv_sec + ((double)t.tv_usec)/1000000.);
}


#include <cmath>

#ifndef PI
const double PI = 3.14159265358979323846;
#endif
const double TWOPI = 2.0*PI;

/**
 * Normalize angle to be within the interval [-pi,pi].
 */
inline double standardRad(double t) {
    if (t >= 0.) {
        t = fmod(t+PI, TWOPI) - PI;
    } else {
        t = fmod(t-PI, -TWOPI) + PI;
    }
    return t;
}

void wRo_to_euler(const Eigen::Matrix3d& wRo, double& yaw, double& pitch, double& roll) {
    yaw = standardRad(atan2(wRo(1,0), wRo(0,0)));
    double c = cos(yaw);
    double s = sin(yaw);
    pitch = standardRad(atan2(-wRo(2,0), wRo(0,0)*c + wRo(1,0)*s));
    roll  = standardRad(atan2(wRo(0,2)*s - wRo(1,2)*c, -wRo(0,1)*s + wRo(1,1)*c));
}

class DetObject 
{
public:
    int64_t utime; //utime at which the object was first observed
    int id; //unique apriltags id
    BotTrans object_to_local; 
    BotTrans object_to_body;

    DetObject(int64_t _utime, int _id): utime(_utime), id(_id){    
    }

    void print(){
        fprintf(stderr, "Object [%d] => (%.2f,%.2f,%.2f)\n", id, object_to_local.trans_vec[0], 
                object_to_local.trans_vec[1], object_to_local.trans_vec[2]);
    }

    double getDistanceToBody(BotTrans bodyToLocal){
        return hypot(object_to_local.trans_vec[0] - bodyToLocal.trans_vec[0], object_to_local.trans_vec[1] - bodyToLocal.trans_vec[1]);
    }
};

bool comparePairDistance (detectionPair p1, detectionPair p2) { 
    if(p1.second < p2.second)
        return true;
    return false;
}

class Detector 
{

public:

    lcm_t *lcm;
    BotFrames *frames; 
    BotParam *param;
    bot_lcmgl_t * lcmgl;
    char * coord_frame;
    GMutex *mutex;
    pthread_t  process_thread;
    bool verbose;
    char *channel;
    int64_t last_bot_utime;
    int detection_count;

    AprilTags::TagDetector* m_tagDetector;
    AprilTags::TagCodes m_tagCodes;

    bot_core_image_t *last_img;
    bot_core_planar_lidar_t *last_laser;

    vector<DetObject *> detections;

    cv::Mat img, img_gray, img_processed;
    int64_t img_utime;
    bool m_draw; // draw image and April tag detections?
    bool m_arduino; // send tag detections to serial port?

    int m_width; // image size in pixels
    int m_height;
    double m_tagSize; // April tag side length in meters of square black frame
    double m_fx; // camera focal length in pixels
    double m_fy;
    double m_px; // camera principal point
    double m_py;

    bool img_setup;
    char *sensor_name; 
    char *sensor_frame; 
    // default constructor
    Detector() :
        // default settings, most can be modified through command line options (see below)
        m_tagDetector(NULL),
        last_img(NULL),
	last_laser(NULL),
        m_tagCodes(AprilTags::tagCodes36h11),
        channel("DRAGONFLY_IMAGE"), sensor_name("dragonfly"), sensor_frame("dragonfly"), 
        m_draw(true),
        m_arduino(false),
        m_width(640),
        m_height(480),
        m_tagSize(0.172), //0.1032),
        m_fx(612.486),
        m_fy(614.032),
        m_px(637.539),
        m_py(426.127)

    {
        img_setup = false;
        detection_count = 0; 
        lcm =  bot_lcm_get_global(NULL);

        param = bot_param_new_from_server(lcm, 1);       

        frames = bot_frames_get_global (lcm, param);              
        mutex = g_mutex_new();
        m_tagDetector = new AprilTags::TagDetector(m_tagCodes);
    }

    void drawCurrentDetections(){
        drawDetections(lcmgl, detections,1);
    }

    void drawDetections(bot_lcmgl_t *d_lcmgl, vector<DetObject *> obj_detections, int red){
        double point_size=10.0;
        //fprintf(stderr, "No of new detections : %d\n", (int) obj_detections.size());

        bot_lcmgl_point_size(d_lcmgl, point_size);
        //if(red)
        if(!strcmp(sensor_name, "dragonfly")){
            bot_lcmgl_color3f(d_lcmgl, 1.0, 0, 0);
        }
        else if(!strcmp(sensor_name, "dragonfly_left")){
            bot_lcmgl_color3f(d_lcmgl, 0, 0, 1.0);
        }
        else{
            bot_lcmgl_color3f(d_lcmgl, 0, 1.0, 0);
        }

        for(int i=0; i < obj_detections.size();i++){
            bot_lcmgl_begin(lcmgl, GL_POINTS);
            bot_lcmgl_vertex3f(d_lcmgl, obj_detections[i]->object_to_local.trans_vec[0], obj_detections[i]->object_to_local.trans_vec[1], obj_detections[i]->object_to_local.trans_vec[2]); 
            bot_lcmgl_end(d_lcmgl);
            
            //draw orientation also
            double rpy[3];
            bot_quat_to_roll_pitch_yaw(obj_detections[i]->object_to_local.rot_quat, rpy);
            /*bot_lcmgl_begin(lcmgl, GL_LINES);
            bot_lcmgl_vertex3f(d_lcmgl, obj_detections[i]->object_to_local.trans_vec[0], obj_detections[i]->object_to_local.trans_vec[1], obj_detections[i]->object_to_local.trans_vec[2]); 
            bot_lcmgl_vertex3f(d_lcmgl, obj_detections[i]->object_to_local.trans_vec[0] + 0.5 * cos(rpy[2]), obj_detections[i]->object_to_local.trans_vec[1] + 0.5 * sin(rpy[2]), obj_detections[i]->object_to_local.trans_vec[2]); 
            bot_lcmgl_end(d_lcmgl);*/
        }

        bot_lcmgl_switch_buffer(d_lcmgl);
    }

    void publishCurrentObjectDetections(int64_t utime){
        publishObjectDetections(utime, detections);
    }

    void publishObjectDetections(int64_t utime, vector<DetObject *> obj_detections){
        if(obj_detections.size()==0){
            erlcm_apriltags_detection_list_t *msg = (erlcm_apriltags_detection_list_t *) 
                calloc(1,sizeof(erlcm_apriltags_detection_list_t)); 
            msg->utime = utime;
            msg->count = 0;
            msg->camera = strdup(sensor_name);
            msg->objects = NULL;
            erlcm_apriltags_detection_list_t_publish(lcm, "APRILTAGS_RAW_DETECTIONS", msg);
            erlcm_apriltags_detection_list_t_destroy(msg);
            return;
        }
        
        erlcm_apriltags_detection_list_t *msg = (erlcm_apriltags_detection_list_t *) calloc(1,sizeof(erlcm_apriltags_detection_list_t)); 
        msg->utime = utime;
        msg->count = obj_detections.size();
        msg->camera = strdup(sensor_name);
        msg->objects = (erlcm_apriltags_detection_t *) calloc(msg->count, sizeof(erlcm_apriltags_detection_t));
        for(int i=0; i < obj_detections.size();i++){             
            erlcm_apriltags_detection_t *obj = &msg->objects[i];
            obj->id = obj_detections[i]->id;

            obj->pos[0] = obj_detections[i]->object_to_body.trans_vec[0];
            obj->pos[1] = obj_detections[i]->object_to_body.trans_vec[1];
            obj->pos[2] = obj_detections[i]->object_to_body.trans_vec[2];
            
            obj->quat[0] = obj_detections[i]->object_to_body.rot_quat[0];
            obj->quat[1] = obj_detections[i]->object_to_body.rot_quat[1];
            obj->quat[2] = obj_detections[i]->object_to_body.rot_quat[2];
            obj->quat[3] = obj_detections[i]->object_to_body.rot_quat[3];
        }
        erlcm_apriltags_detection_list_t_publish(lcm, "APRILTAGS_RAW_DETECTIONS", msg);
        erlcm_apriltags_detection_list_t_destroy(msg);
        return;
    }

    // changing the tag family
    void setTagCodes(string s) {
        if (s=="16h5") {
            m_tagCodes = AprilTags::tagCodes16h5;
        } else if (s=="25h7") {
            m_tagCodes = AprilTags::tagCodes25h7;
        } else if (s=="25h9") {
            m_tagCodes = AprilTags::tagCodes25h9;
        } else if (s=="36h9") {
            m_tagCodes = AprilTags::tagCodes36h9;
        } else if (s=="36h11") {
            m_tagCodes = AprilTags::tagCodes36h11;
        } else {
            cout << "Invalid tag family specified" << endl;
            exit(1);
        }
    }

    // parse command line options to change default behavior
    void parseOptions(int argc, char* argv[]) {
        int c;
        while ((c = getopt(argc, argv, ":h?vdc:C:F:H:S:W:E:G:B:")) != -1) {
            // Each option character has to be in the string in getopt();
            // the first colon changes the error character from '?' to ':';
            // a colon after an option means that there is an extra
            // parameter to this option; 'W' is a reserved character
            switch (c) {
            case 'h':
            case '?':
                cout << intro;
                cout << usage;
                exit(0);
                break;            
            case 'd':
                m_draw = false;
                break;
            case 'v':
                verbose = true;
                break;
            case 'c':
                channel = strdup(optarg);
                break;
            case 'C':
                setTagCodes(optarg);
                break;
            case 'F':
                m_fx = atof(optarg);
                m_fy = m_fx;
                break;
            case 'H':
                m_height = atoi(optarg);
                m_py = m_height/2;
                break;
            case 'S':
                m_tagSize = atof(optarg);
                break;
            case 'W':
                m_width = atoi(optarg);
                m_px = m_width/2;
                break;
            case ':': // unknown option, from getopt
                cout << intro;
                cout << usage;
                exit(1);
                break;
            }
        }

        fprintf(stdout, "Channel : %s\n", channel);
        sensor_name = bot_param_get_sensor_name_from_lcm_channel(param, "cameras", channel);
               
        if(!sensor_name){
            fprintf(stderr, "Error - sensor name not found for channel %s\n", channel);
            exit(-1);
        }

        sensor_frame = bot_param_get_camera_coord_frame(param, sensor_name);
        fprintf(stdout, "Sensor Name : %s - frame : %s\n", sensor_name, sensor_frame);
        
        char key[1024];
        double pinhole_params[5];
        sprintf(key, "cameras.%s.intrinsic_cal.pinhole", sensor_name);
        if (5 != bot_param_get_double_array(param, key, pinhole_params, 5)){
            fprintf(stderr, "Error - calibration params not found for camera %s\n", sensor_name);
            exit(-1);
        }
            
        m_fx = pinhole_params[0];
        m_fy = pinhole_params[1];
        m_px = pinhole_params[3];
        m_py = pinhole_params[4];

        fprintf(stdout, "Camera params : (fx, fy) : %.3f,%.3f - (px, py) : %.3f,%.3f\n", m_fx, m_fy, m_px, m_py);
        
        char name[1024];
        sprintf(name, "apriltags-detections_%s", sensor_name); 
        lcmgl = bot_lcmgl_init(lcm, name);
    }
   
    void clearDetections(){
        vector<DetObject *> cleared_detections; 
        for(int i=0; i < detections.size(); i++){
            delete detections[i];
        }
        detections = cleared_detections;
    }

    void detectObjects1(int64_t utime, vector<AprilTags::TagDetection> &tag_detections){
        clearDetections();
        if(tag_detections.size() > 0){
            //transform base map to particle's map frame - according to nd2 
            BotTrans sensor_to_body_tf;

            bot_frames_get_trans_with_utime(frames, sensor_frame, 
                                            "body",
                                            utime, 
                                            &sensor_to_body_tf);  

            //transform base map to particle's map frame - according to nd2 
            BotTrans body_to_local_tf;

            bot_frames_get_trans_with_utime(frames, "body", 
                                            "local",
                                            utime, 
                                            &body_to_local_tf);  

            map<int,int>::iterator it;
            for (int i=0; i<tag_detections.size(); i++) {
                //printDetection(tag_detections[i]);

                // NOTE: for this to be accurate, it is necessary to use the
                // actual camera parameters here as well as the actual tag size
                // (m_fx, m_fy, m_px, m_py, m_tagSize)
                
                Eigen::Vector3d translation1;
                Eigen::Matrix3d rotation1;
                tag_detections[i].getRelativeTranslationRotation(m_tagSize, m_fx, m_fy, m_px, m_py,
                                                         translation1, rotation1);

                Eigen::Matrix3d F1;
                F1 <<
                    1, 0,  0,
                    0,  -1,  0,
                    0,  0,  1;
                Eigen::Matrix3d fixed_rot1 = F1*rotation1;
                double yaw1, pitch1, roll1;
                wRo_to_euler(fixed_rot1, yaw1, pitch1, roll1);

                cout << "  distance=" << translation1.norm()
                     << "m, x=" << translation1(0)
                     << ", y=" << translation1(1)
                     << ", z=" << translation1(2)
                     << ", yaw=" << yaw1
                     << ", pitch=" << pitch1
                     << ", roll=" << roll1 
                     << " - Good : " <<  tag_detections[i].good 
                     << endl;

                

                Eigen::Vector3d translation;
                Eigen::Matrix3d rotation;
                Eigen::Matrix4d T = tag_detections[i].getRelativeTransform(m_tagSize, m_fx, m_fy, m_px, m_py);
                    
                Eigen::Matrix3d F;
                F <<
                    1, 0,  0,
                    0,  -1,  0,
                    0,  0,  1;

                translation = T.col(3).head(3);
                rotation = T.block(0,0,3,3);
                double yaw, pitch, roll;
                
                Eigen::Matrix3d fixed_rot = F*rotation;
                wRo_to_euler(rotation, yaw, pitch, roll);
                //wRo_to_euler(fixed_rot, yaw, pitch, roll);

                //the object pose is given in the camera coord frame - but we need the pose in normal coord
                double rpy[3] = {roll, pitch, yaw};

                BotTrans object_to_object_c_center; //in the camera frame (at the object)

                if(0){
                    object_to_object_c_center.trans_vec[0] = 0; //-m_tagSize / 2.0;
                    object_to_object_c_center.trans_vec[1] = 0; //-m_tagSize / 2.0;
                    object_to_object_c_center.trans_vec[2] = 0;
                    
                    //rotation matrix to go from normal to camera frame (at the object)
                    double rpy_c[3] = {bot_to_radians(45), bot_to_radians(-90), bot_to_radians(45)};
                    bot_roll_pitch_yaw_to_quat(rpy_c, object_to_object_c_center.rot_quat);
                }
                else{
                    BotTrans object_to_object_c_center_1; //in the camera frame (at the object)
                      
                    object_to_object_c_center_1.trans_vec[0] = 0; //-m_tagSize / 2.0;
                    object_to_object_c_center_1.trans_vec[1] = 0; //-m_tagSize / 2.0;
                    object_to_object_c_center_1.trans_vec[2] = 0;
                      
                    //rotation matrix to go from normal to camera frame (at the object)
                    //double rpy_1[3] = {bot_to_radians(-90), 0,0};
                    double rpy_1[3] = {bot_to_radians(0), 0,0};
                    bot_roll_pitch_yaw_to_quat(rpy_1, object_to_object_c_center_1.rot_quat);

                    BotTrans object_c_center_1_to_object_c_center; //in the camera frame (at the object)

                    object_c_center_1_to_object_c_center.trans_vec[0] = 0;//-m_tagSize / 2.0;
                    object_c_center_1_to_object_c_center.trans_vec[1] = 0;//-m_tagSize / 2.0;
                    object_c_center_1_to_object_c_center.trans_vec[2] = 0; 

                    //rotation matrix to go from normal to camera frame (at the object)
                    //double rpy_2[3] = {0, bot_to_radians(90),0};
                    double rpy_2[3] = {0, bot_to_radians(0),0};
                    bot_roll_pitch_yaw_to_quat(rpy_2, object_c_center_1_to_object_c_center.rot_quat);

                
                    bot_trans_apply_trans_to(&object_c_center_1_to_object_c_center, &object_to_object_c_center_1, &object_to_object_c_center);
                }

                BotTrans object_c_center_to_object_c; 
                
                object_c_center_to_object_c.trans_vec[0] = m_tagSize / 2.0;
                object_c_center_to_object_c.trans_vec[1] = m_tagSize / 2.0;
                object_c_center_to_object_c.trans_vec[2] = 0;

                double rpy_z[3] = {0,0,0};
                bot_roll_pitch_yaw_to_quat(rpy_z, object_c_center_to_object_c.rot_quat);
                
                BotTrans object_to_object_c;
                bot_trans_apply_trans_to(&object_c_center_to_object_c, &object_to_object_c_center, &object_to_object_c);

                //this is the detection object_c frame to sensor
                BotTrans object_to_sensor_tf; 

                object_to_sensor_tf.trans_vec[0] = -translation1(1);
                object_to_sensor_tf.trans_vec[1] = -translation1(2);
                object_to_sensor_tf.trans_vec[2] = translation1(0);

                bot_roll_pitch_yaw_to_quat(rpy, object_to_sensor_tf.rot_quat);
                    
                /*BotTrans object_to_sensor_tf; 
                  bot_trans_apply_trans_to(&object_c_to_sensor, &object_to_object_c, &object_to_sensor_tf);*/
                                
                BotTrans object_to_body_tf;
                bot_trans_apply_trans_to(&sensor_to_body_tf, &object_to_sensor_tf, &object_to_body_tf);

                BotTrans object_to_local_tf;
                bot_trans_apply_trans_to(&body_to_local_tf, &object_to_body_tf, &object_to_local_tf);

                double dist = hypot(object_to_body_tf.trans_vec[0], object_to_body_tf.trans_vec[1]);
                
                if(dist > OBS_DIST_THRESHOLD)
                    continue;
                if(verbose){
                    fprintf(stdout, "%f ID : %d - %f,%f\n", utime / 1.0e6, tag_detections[i].id, object_to_body_tf.trans_vec[0], object_to_body_tf.trans_vec[1]);
                    fprintf(stdout, "Position (sensor) : %f,%f,%f - Body : %f,%f,%f -> Local : %f,%f,%f\n", 
                            translation(0), translation(1), translation(2), object_to_body_tf.trans_vec[0], 
                            object_to_body_tf.trans_vec[1], object_to_body_tf.trans_vec[2], 
                            object_to_local_tf.trans_vec[0], object_to_local_tf.trans_vec[1], 
                            object_to_local_tf.trans_vec[2]);
                }

                DetObject *n_det = new DetObject(utime, tag_detections[i].id);
                n_det->object_to_body = object_to_body_tf;
                n_det->object_to_local = object_to_local_tf;
                detections.push_back(n_det);
            }               
        }
        drawCurrentDetections();
        publishCurrentObjectDetections(utime);
    }

    void detectObjects(int64_t utime, vector<AprilTags::TagDetection> &tag_detections){
        clearDetections();
        if(tag_detections.size() > 0){
            //transform base map to particle's map frame - according to nd2 
            BotTrans sensor_to_body_tf;

            bot_frames_get_trans_with_utime(frames, sensor_frame, 
                                            "body",
                                            utime, 
                                            &sensor_to_body_tf);  

            //transform base map to particle's map frame - according to nd2 
            BotTrans body_to_local_tf;

            bot_frames_get_trans_with_utime(frames, "body", 
                                            "local",
                                            utime, 
                                            &body_to_local_tf);  

            map<int,int>::iterator it;
            for (int i=0; i<tag_detections.size(); i++) {
                //printDetection(tag_detections[i]);

                // NOTE: for this to be accurate, it is necessary to use the
                // actual camera parameters here as well as the actual tag size
                // (m_fx, m_fy, m_px, m_py, m_tagSize)
                
                Eigen::Vector3d translation1;
                Eigen::Matrix3d rotation1;
                tag_detections[i].getRelativeTranslationRotation(m_tagSize, m_fx, m_fy, m_px, m_py,
                                                         translation1, rotation1);

                Eigen::Matrix3d F1;
                F1 <<
                    1, 0,  0,
                    0,  -1,  0,
                    0,  0,  1;
                Eigen::Matrix3d fixed_rot1 = F1*rotation1;
                double yaw1, pitch1, roll1;
                wRo_to_euler(fixed_rot1, yaw1, pitch1, roll1);

                cout << "  distance=" << translation1.norm()
                     << "m, x=" << translation1(0)
                     << ", y=" << translation1(1)
                     << ", z=" << translation1(2)
                     << ", yaw=" << yaw1
                     << ", pitch=" << pitch1
                     << ", roll=" << roll1 
                     << " - Good : " <<  tag_detections[i].good 
                     << endl;

                

                Eigen::Vector3d translation;
                Eigen::Matrix3d rotation;
                Eigen::Matrix4d T = tag_detections[i].getRelativeTransform(m_tagSize, m_fx, m_fy, m_px, m_py);
                    
                Eigen::Matrix3d F;
                F <<
                    1, 0,  0,
                    0,  -1,  0,
                    0,  0,  1;

                translation = T.col(3).head(3);
                rotation = T.block(0,0,3,3);
                double yaw, pitch, roll;
                
                Eigen::Matrix3d fixed_rot = F*rotation;
                wRo_to_euler(rotation, yaw, pitch, roll);
                //wRo_to_euler(fixed_rot, yaw, pitch, roll);

                //the object pose is given in the camera coord frame - but we need the pose in normal coord
                double rpy[3] = {roll, pitch, yaw};

                BotTrans object_to_object_c_center; //in the camera frame (at the object)

                double rpy_z[3] = {0,0,0};

                BotTrans object_to_object1;
                object_to_object1.trans_vec[0] = 0;
                object_to_object1.trans_vec[1] = -m_tagSize / 2.0;
                object_to_object1.trans_vec[2] = -m_tagSize / 2.0;
                
                bot_roll_pitch_yaw_to_quat(rpy_z, object_to_object1.rot_quat);

                BotTrans object1_to_object_c_center_1; //in the camera frame (at the object)
                      
                object1_to_object_c_center_1.trans_vec[0] = 0; //-m_tagSize / 2.0;
                object1_to_object_c_center_1.trans_vec[1] = 0; //-m_tagSize / 2.0;
                object1_to_object_c_center_1.trans_vec[2] = 0;
                      
                //rotation matrix to go from normal to camera frame (at the object)
                //double rpy_1[3] = {bot_to_radians(-90), 0,0};
                double rpy_1[3] = {bot_to_radians(-90),0, 0};
                bot_roll_pitch_yaw_to_quat(rpy_1, object1_to_object_c_center_1.rot_quat);

                BotTrans object_to_object_c_center_1;
                bot_trans_apply_trans_to(&object1_to_object_c_center_1, &object_to_object1, &object_to_object_c_center_1);

                BotTrans object_c_center_1_to_object_c_center; //in the camera frame (at the object)

                object_c_center_1_to_object_c_center.trans_vec[0] = 0;//-m_tagSize / 2.0;
                object_c_center_1_to_object_c_center.trans_vec[1] = 0;//-m_tagSize / 2.0;
                object_c_center_1_to_object_c_center.trans_vec[2] = 0; 

                //rotation matrix to go from normal to camera frame (at the object)
                //double rpy_2[3] = {0, bot_to_radians(90),0};
                double rpy_2[3] = {bot_to_radians(0), bot_to_radians(-90), 0};
                bot_roll_pitch_yaw_to_quat(rpy_2, object_c_center_1_to_object_c_center.rot_quat);

                
                bot_trans_apply_trans_to(&object_c_center_1_to_object_c_center, &object_to_object_c_center_1, &object_to_object_c_center);


                BotTrans object_c_center_to_object_c; 
                
                object_c_center_to_object_c.trans_vec[0] = 0;//-m_tagSize / 2.0;
                object_c_center_to_object_c.trans_vec[1] = 0;//-m_tagSize / 2.0;
                object_c_center_to_object_c.trans_vec[2] = 0;

                
                bot_roll_pitch_yaw_to_quat(rpy_z, object_c_center_to_object_c.rot_quat);
                
                BotTrans object_to_object_c;
                bot_trans_apply_trans_to(&object_c_center_to_object_c, &object_to_object_c_center, &object_to_object_c);

                //this is the detection object_c frame to sensor
                BotTrans object_c_to_sensor; 

                object_c_to_sensor.trans_vec[0] = translation(0);
                object_c_to_sensor.trans_vec[1] = translation(1);
                object_c_to_sensor.trans_vec[2] = translation(2);

                bot_roll_pitch_yaw_to_quat(rpy, object_c_to_sensor.rot_quat);
                    
                BotTrans object_to_sensor_tf; 
                bot_trans_apply_trans_to(&object_c_to_sensor, &object_to_object_c, &object_to_sensor_tf);
                                
                BotTrans object_to_body_tf;
                bot_trans_apply_trans_to(&sensor_to_body_tf, &object_to_sensor_tf, &object_to_body_tf);

                BotTrans object_to_local_tf;
                bot_trans_apply_trans_to(&body_to_local_tf, &object_to_body_tf, &object_to_local_tf);

                double dist = hypot(object_to_body_tf.trans_vec[0], object_to_body_tf.trans_vec[1]);
                
                if(dist > OBS_DIST_THRESHOLD)
                    continue;
                if(verbose){
                    fprintf(stdout, "%f ID : %d - %f,%f\n", utime / 1.0e6, tag_detections[i].id, object_to_body_tf.trans_vec[0], object_to_body_tf.trans_vec[1]);
                    fprintf(stdout, "Position (sensor) : %f,%f,%f - Body : %f,%f,%f -> Local : %f,%f,%f\n", 
                            translation(0), translation(1), translation(2), object_to_body_tf.trans_vec[0], 
                            object_to_body_tf.trans_vec[1], object_to_body_tf.trans_vec[2], 
                            object_to_local_tf.trans_vec[0], object_to_local_tf.trans_vec[1], 
                            object_to_local_tf.trans_vec[2]);
                }

                DetObject *n_det = new DetObject(utime, tag_detections[i].id);
                n_det->object_to_body = object_to_body_tf;
                n_det->object_to_local = object_to_local_tf;
                detections.push_back(n_det);
            }               
        }
        drawCurrentDetections();
        publishCurrentObjectDetections(utime);
    }

    void printDetection(AprilTags::TagDetection& detection) const {
        //have a library of the id's published by someone else         
        //cout << "  Id: " << detection.id
        // << " (Hamming: " << detection.hammingDistance << ")";

        // recovering the relative pose of a tag:

        // NOTE: for this to be accurate, it is necessary to use the
        // actual camera parameters here as well as the actual tag size
        // (m_fx, m_fy, m_px, m_py, m_tagSize)

        Eigen::Vector3d translation;
        Eigen::Matrix3d rotation;
        detection.getRelativeTranslationRotation(m_tagSize, m_fx, m_fy, m_px, m_py,
                                                 translation, rotation);

        Eigen::Matrix3d F;
        F <<
            1, 0,  0,
            0,  -1,  0,
            0,  0,  1;
        Eigen::Matrix3d fixed_rot = F*rotation;
        double yaw, pitch, roll;
        wRo_to_euler(fixed_rot, yaw, pitch, roll);

        cout << "  distance=" << translation.norm()
             << "m, x=" << translation(0)
             << ", y=" << translation(1)
             << ", z=" << translation(2)
             << ", yaw=" << yaw
             << ", pitch=" << pitch
             << ", roll=" << roll
             << endl;
        
        // Also note that for SLAM/multi-view application it is better to
        // use reprojection error of corner points, because the noise in
        // this relative pose is very non-Gaussian; see iSAM source code
        // for suitable factors.
    }

    int 
    decode_image(const bot_core_image_t * msg, cv::Mat& img, cv::Mat& img_gray)
    {
        int ch = msg->row_stride / (msg->width); 
    
        if(ch==0)
            ch = 3;

        if(ch == 1){
            fprintf(stderr, "No color channels\n");
            return 0;
        }
        if(msg->pixelformat == BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY){
            fprintf(stderr, "Image is gray\n");
            //should handle this 
            return 0;
        }
    
        if (img.empty() || img.rows != msg->height || img.cols != msg->width)
            img.create(msg->height, msg->width, CV_8UC3);

        // extract image data
        switch (msg->pixelformat) {
        case BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB:
            fprintf(stderr, "RGB\n");
            memcpy(img.data, msg->data, sizeof(uint8_t) * msg->width * msg->height * 3);
            cv::cvtColor(img, img, CV_RGB2BGR);
            cv::cvtColor(img, img_gray, CV_BGR2GRAY);
            break;
        case BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG:
            if (ch == 3) { 
                jpeg_decompress_8u_rgb(msg->data,
                                       msg->size,
                                       img.data,
                                       msg->width,
                                       msg->height,
                                       msg->width * ch);
                cv::cvtColor(img, img, CV_RGB2BGR);
                cv::cvtColor(img, img_gray, CV_BGR2GRAY);
            } 
            break;
        default:
            fprintf(stderr, "Unrecognized image format\n");
            return 0;
            break;
        }
        return 1;
    }

    int detect_on_last_image(){
        g_mutex_lock(mutex);
        if(last_img == NULL){
            //fprintf(stderr, "Last image NULL\n");
            g_mutex_unlock(mutex);
            return 0;
        }
        
        if(img_utime == last_img->utime){
            g_mutex_unlock(mutex);
            return 0;
        }

        bot_core_image_t *copy = bot_core_image_t_copy(last_img);        
        g_mutex_unlock(mutex);

        if(!img_setup){
            img_setup = true;
            fprintf(stderr, "Setting up the projection stuff - only done once\n");
            fprintf(stderr, "Width : %d Height : %d -> Row stride : %d\n", copy->width, copy->height, copy->row_stride);
            m_width = copy->width;
            m_height = copy->height;            
        }  

        if (img.empty() || img.rows != copy->height || img.cols != copy->width) { 
            if (copy->pixelformat == BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY) { 
                std::cerr << "ERROR: Incoming image is grayscale!! Cannot perform road detection!!!" << std::endl;
                assert(0);
            } else { 
                std::cerr << "One time creation of image" << std::endl;
                img.create(copy->height, copy->width, CV_8UC3);
            }
        }
    
        int d_status = decode_image(copy, img, img_gray);    
        if(d_status == 0){
            fprintf(stderr, "Error decoding image\n");
            return 0;
        }
        //fprintf(stderr, "Decoded image\n");
        
        vector<AprilTags::TagDetection> new_detections = m_tagDetector->extractTags(img_gray);

        detectObjects(last_img->utime, new_detections);
        
        drawCurrentDetections();

        //also need to publish the poses of the detections 

        // show the current image including any detections
        if (m_draw) {
            for (int i=0; i<new_detections.size(); i++) {
                // also highlight in the image
                new_detections[i].draw(img);
            }
            imshow(window_name, img);
            //img_processed = img.clone();
        }

        fprintf(stdout, "Time gap : %.4f\n", (last_bot_utime - copy->utime)/1.0e6);
        img_utime = copy->utime; 
        bot_core_image_t_destroy(copy);
        return 1;
    }

    // The processing loop where images are retrieved, tags detected,
    // and information about detections generated
    void loop() {
        // Main lcm handle
        while(1) { 
            unsigned char c = cv::waitKey(1) & 0xff;
            lcm_handle(lcm);

            if (c == 'q') {
                break;  
            } else if ( c == 'c' ) {      
                cv::imshow("Captured Image", img);
            }            
        }

        return;
    }    

}; // Detector


int 
decode_image(const bot_core_image_t * msg, cv::Mat& img, cv::Mat& img_gray)
{
    int ch = msg->row_stride / (msg->width); 
    
    if(ch==0)
        ch = 3;

    if(ch == 1){
        fprintf(stderr, "No color channels\n");
        return 0;
    }
    if(msg->pixelformat == BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY){
        fprintf(stderr, "Image is gray\n");
        //should handle this 
        return 0;
    }
    
    if (img.empty() || img.rows != msg->height || img.cols != msg->width)
        img.create(msg->height, msg->width, CV_8UC3);

    // extract image data
    fprintf(stderr, "Image format : %d - Channels : %d\n", msg->pixelformat, ch);
    switch (msg->pixelformat) {
    case BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB:
        fprintf(stderr, "RGB\n");
        memcpy(img.data, msg->data, sizeof(uint8_t) * msg->width * msg->height * 3);
        cv::cvtColor(img, img, CV_RGB2BGR);
        cv::cvtColor(img, img_gray, CV_BGR2GRAY);
        break;
    case BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG:
        if (ch == 3) { 
            jpeg_decompress_8u_rgb(msg->data,
                                   msg->size,
                                   img.data,
                                   msg->width,
                                   msg->height,
                                   msg->width * ch);
            cv::cvtColor(img, img, CV_RGB2BGR);
            fprintf(stderr, "JPEG\n");
            cv::cvtColor(img, img_gray, CV_BGR2GRAY);
        } 
        break;
    default:
        fprintf(stderr, "Unrecognized image format\n");
        return 0;
        break;
    }
    return 1;
}

void on_laser(const lcm_recv_buf_t *rbuf, const char * channel, 
              const bot_core_planar_lidar_t * msg, void * user) {  
    Detector *detector = (Detector *) user;
    g_mutex_lock(detector->mutex);
    if(detector->last_laser != NULL){
        bot_core_planar_lidar_t_destroy(detector->last_laser);        
    }
    
    int64_t utime = bot_timestamp_now();
    //fprintf(stderr, "Laser Delta : %.3f\n", (msg->utime - utime)/1.0e6);
    if(detector->last_img){
        fprintf(stderr, "Laser To Image Delta : %.3f\n", (msg->utime - detector->last_img->utime)/1.0e6);
    }
    detector->last_laser = bot_core_planar_lidar_t_copy(msg);
    g_mutex_unlock(detector->mutex);
}

void on_image(const lcm_recv_buf_t *rbuf, const char * channel, 
              const bot_core_image_t * msg, void * user) {  
    Detector *detector = (Detector *) user;
    g_mutex_lock(detector->mutex);
    if(detector->last_img != NULL){
        bot_core_image_t_destroy(detector->last_img);        
    }
    
    int64_t utime = bot_timestamp_now();
    //fprintf(stderr, "Image Delta : %.3f\n", (msg->utime - utime)/1.0e6);
    //if(detector->last_laser){
    // fprintf(stderr, "Laser To Image Delta : %.3f\n", (msg->utime - detector->last_laser->utime)/1.0e6);
    //}
    detector->last_img = bot_core_image_t_copy(msg);
    g_mutex_unlock(detector->mutex);
}

void on_pose(const lcm_recv_buf_t *rbuf, const char * channel, 
             const bot_core_pose_t * msg, void * user) {  
    Detector *detector = (Detector *) user;
    g_mutex_lock(detector->mutex);
    detector->last_bot_utime = msg->utime;
    g_mutex_unlock(detector->mutex);
}

//not used right now 
static void *track_tags(void *user)
{
    Detector * self = (Detector *) user;

    int status = 0;
    while(1){
        int processed = self->detect_on_last_image();   
        if(processed){            
            usleep(500);
        }
        else{            
            usleep(1000);
        }
    }
}

// here is were everything begins
int main(int argc, char* argv[]) {
    Detector detector;

    // process command line options
    detector.parseOptions(argc, argv);

    // setup image source, window for drawing, serial port...
    bot_core_image_t_subscribe(detector.lcm, detector.channel, on_image, &detector);

    bot_core_pose_t_subscribe(detector.lcm, "POSE", on_pose, &detector);
    // the actual processing loop where tags are detected and visualized
    
    pthread_create(&detector.process_thread , NULL, track_tags, &detector);

    detector.loop();

    return 0;
}
