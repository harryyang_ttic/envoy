add_definitions(
    #-ggdb3 
    #-std=gnu99
    )

add_executable(nodder-to-octomap
  nodder_to_octomap.cpp
)

pods_use_pkg_config_packages(nodder-to-octomap lcmtypes_er-lcmtypes nodder_extractor occ-map bot2-frames octomap octomap-util)

pods_install_executables(nodder-to-octomap) 

#add_executable(nodder-to-bottle-poses
#  nodder_to_bottle_poses.cpp
#)

#pods_use_pkg_config_packages(nodder-to-bottle-poses lcmtypes_er-lcmtypes nodder_extractor occ-map bot2-frames octomap)# generic-segmenter)

#pods_install_executables(nodder-to-octomap) 

#add_executable(nodder-to-slam
#  nodder_slam.cpp
#)

#pods_use_pkg_config_packages(nodder-to-slam lcmtypes_er-lcmtypes nodder_extractor occ-map bot2-lcmgl-client bot2-frames)

#pods_install_executables(nodder-to-slam)