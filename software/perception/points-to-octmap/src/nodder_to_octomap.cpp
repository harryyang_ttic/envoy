#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <nodder/nodder_extractor.h>
#include <lcmtypes/erlcm_xyz_point_list_t.h>

#include <occ_map/VoxelMap.hpp>
#include <lcmtypes/occ_map_voxel_map_t.h>
#include <octomap/octomap.h>
#include <octomap/OcTree.h>
#include <octomap_utils/octomap_util.hpp>

using namespace octomap_utils;
using namespace std;
using namespace octomap;


typedef struct _state_t state_t;

struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    BotFrames *frames; 
    BotParam *param;
    OcTree *oc_tree;
    nodder_extractor_state_t *nodder; 
    int verbose; 
};

void nodder_cb(int64_t utime, void *data)
{
  state_t *s = (state_t *)data;
  //fprintf(stderr, "Called \n"); 
  
  xyz_point_list_t *ret = nodder_extract_new_points_frame(s->nodder, "local");
  //get the local pose also 
  if(ret == NULL){
      fprintf(stderr, "No points\n");
      return;
  }
  if(s->verbose)
      fprintf(stderr, "Size of Points : %d \n", ret->no_points);
  
  //find the size of the map based on the current position 
  //copy and convert 
  double body_to_local[12];
  if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                "local",  ret->utime,
                                                body_to_local)) {
      fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
      return;
  }
    
  double bot_body[3] = {0,0,0};
  double bot_local[3];

  double bot_infront[3] = {1.0,0,0};
  double bot_local_infront[3];

  bot_vector_affine_transform_3x4_3d (body_to_local, bot_body, bot_local);

  bot_vector_affine_transform_3x4_3d (body_to_local, bot_infront, bot_local_infront);
 

  point3d origin(bot_local[0], bot_local[1], bot_local[2]);

  for (size_t k = 0; k < ret->no_points; ++k){
      if(ret->points[k].xyz[2] < 0.2 || ret->points[k].xyz[2] > 1.5)
          continue;
      
      if(hypot(ret->points[k].xyz[0] - bot_local_infront[0], ret->points[k].xyz[1] - bot_local_infront[1]) > 3.0)
          continue;

      //should skip stuff inside the footprint also

      point3d endpoint(ret->points[k].xyz[0], ret->points[k].xyz[1], ret->points[k].xyz[2]);
        
      //(float) x*0.02f-1.0f, (float) y*0.02f-1.0f, (float) z*0.02f-1.0f);
      //s->oc_tree->updateNode(endpoint, true);        //occupied
      if (!s->oc_tree->insertRay(origin, endpoint)){
          cout << "ERROR while inserting ray from " << origin << " to " << endpoint << endl;
      }
  }

  /*for (int x=-20; x<20; x++) {
    for (int y=-20; y<20; y++) {
    for (int z=-20; z<20; z++) {
    point3d endpoint ((float) x*0.05f, (float) y*0.05f, (float) z*0.05f);
    s->oc_tree->updateNode(endpoint, true); // integrate 'occupied' measurement
    }
    }
    }

    // insert some measurements of free cells

    for (int x=-30; x<30; x++) {
    for (int y=-30; y<30; y++) {
    for (int z=-30; z<30; z++) {
    point3d endpoint ((float) x*0.02f-1.0f, (float) y*0.02f-1.0f, (float) z*0.02f-1.0f);
    s->oc_tree->updateNode(endpoint, false);  // integrate 'free' measurement
    }
    }
    }*/
    
  //free measurements 
  //tree.updateNode(endpoint, false);  // integrate 'free' measurement

  //point3d query (0., 0., 0.);
  //OcTreeNode* result = tree.search (query);
  //print_query_info(query, result);
  int occupied_depth = 16;
  int free_depth = 16;

  //this drawing function and writing should also be only optional 
  occ_map::FloatVoxelMap * voxmap = octomapToVoxelMap(s->oc_tree, occupied_depth, free_depth);
  const occ_map_voxel_map_t *occ_map_msg = voxmap->get_voxel_map_t(bot_timestamp_now());
  occ_map_voxel_map_t_publish(s->lcm, "VOXEL_MAP", occ_map_msg);
  s->oc_tree->writeBinary("tabletop_tree.bt");
  s->oc_tree->clear();
  delete voxmap;
  destroy_xyz_list(ret);
}  


void nodder_cb_voxel(int64_t utime, void *data)
{
  state_t *s = (state_t *)data;
  //fprintf(stderr, "Called \n"); 
  xyz_point_list_t *ret = nodder_extract_new_points_frame(s->nodder, "local");

  if(ret != NULL){
    if(s->verbose)
      fprintf(stderr, "Size of Points : %d \n", ret->no_points);

    //find the size of the map based on the current position 
    //copy and convert 
    double body_to_local[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
						  "local",  ret->utime,
						  body_to_local)) {
      fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
      return;
    }
    
    double bot_body[3] = {0,0,0};
    double bot_local[3];

    bot_vector_affine_transform_3x4_3d (body_to_local, bot_body, bot_local);

    //actually we should get the extent of the scan here 

    //actually there isnt much useful info behind the robot 

    double xyz0[3] = { -10 + bot_local[0], -10 + bot_local[1], 0 };
    double xyz1[3] = { 10 + bot_local[0], 10 + bot_local[1] , 5 };
    double mpp[3] = { .1, .1, .1 };
    occ_map::FloatVoxelMap fvm(xyz0, xyz1, mpp, 0);
        
    double ixyz[3];

    for (size_t k = 0; k < ret->no_points; ++k){
      if(fvm.isInMap(ret->points[k].xyz)){
	fvm.writeValue(ret->points[k].xyz,0.99);
      }
    }
        
    const occ_map_voxel_map_t * msg = fvm.get_voxel_map_t(bot_timestamp_now());
    occ_map_voxel_map_t_publish(s->lcm, "VOXEL_MAP",msg);
        
    destroy_xyz_list(ret);
  }  
}

int main(int argc, char **argv)
{
    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    state->lcm =  bot_lcm_get_global(NULL);

    
    state->param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->param);

    state->oc_tree = new OcTree(0.02);

    state->nodder = nodder_extractor_init_full(state->lcm, 1, -40, 40, NULL, 
					       &nodder_cb, state);

    if(state->nodder== NULL){
        fprintf(stderr,"Issue getting collector\n");
        return -1;
    }

    state->mainloop = g_main_loop_new( NULL, FALSE );  

    const char *optstring = "vh";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "verbose", no_argument, 0, 'v' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    //usage(argv[0]);
            fprintf(stderr, "No help found - please add help\n");
	    break;
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
                state->verbose = 1;
		break;
	    }
        }
    }
  
    if (!state->mainloop){
	printf("Couldn't create main loop\n");
	return -1;
    }
    
    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);
    
    /* heart beat*/
    //g_timeout_add (100, heartbeat_cb, state);
    
    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
    
    bot_glib_mainloop_detach_lcm(state->lcm);
}
