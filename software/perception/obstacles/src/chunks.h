#ifndef _CHUNK_H
#define _CHUNK_H

#include <glib.h>
#include <stdint.h>

#include "gridder.h"

typedef struct chunk chunk_t;
struct chunk
{
    double x, y;             // invariant: x = (x_min+x_max)/2
    double x_min, x_max, y_min, y_max;

    int64_t otrack_id;        // the track or group that we belong to
    int64_t segment_id;

    double weight;
    int64_t utime;           // time the chunk was observed

    gridder_data_t gd_data;

    uint32_t age_iters;  // how many iterations has it been around?
    double   age_dt;     // how many seconds has it been around?
    double   seen_dt;    // how many seconds has it been seen?
    uint32_t seen_iters; 
    double   unseen_dt;  // how many seconds since it was last seen?

    int reaping; // set when this chunk is being reaped.
    
    double vel[2];
    int    vel_samples;

    int    modality_flags;
    int    reported;
    int    reported_flags;

    int    do_stats_on_me;
    double sick_range;
    double velodyne_range;
};

void chunks_init();
chunk_t *chunk_alloc();
void chunk_free(chunk_t *c);

double chunk_distance_to(chunk_t *a, chunk_t *b);
GList *chunks_segment(gridder_t *gridder, GPtrArray *chunks, double max_dist);

#endif
