#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include <glib.h>

#include <bot_core/bot_core.h>
#include <er_common/unionfind.h>

#include "chunks.h"

static int _chunks = 0;

//////////////////////////////////////////////////////////////////
chunk_t *chunk_alloc()
{
    chunk_t *c = g_slice_alloc0(sizeof(chunk_t));

    c->x_max = -HUGE;
    c->x_min = HUGE;
    c->y_max = -HUGE;
    c->y_min = HUGE;

    c->velodyne_range = 200;
    c->sick_range = 200;

    _chunks++;

    // all other fields are initialized to zero above.
    return c;
}

//////////////////////////////////////////////////////////////////
void chunk_free(chunk_t *c)
{ 
    g_slice_free1(sizeof(chunk_t), c);
    _chunks--;
}

//////////////////////////////////////////////////////////////////
inline double chunk_distance_to(chunk_t *a, chunk_t *b)
{
    double dx = fabs(b->x - a->x);
    dx -= (a->x_max - a->x_min)/2 + (b->x_max - b->x_min)/2;
    if (dx < 0)
        dx = 0;

    double dy = fabs(b->y - a->y);
    dy -= (a->y_max - a->y_min)/2 + (b->y_max - b->y_min)/2;
    if (dy < 0)
        dy = 0;

    return bot_sq(dx) + bot_sq(dy);
}

//////////////////////////////////////////////////////////////////
GList *chunks_segment(gridder_t *gridder, GPtrArray *chunks, double max_dist)
{
    unionfind_t *uf = unionfind_create(bot_g_ptr_array_size(chunks));

    for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(chunks); cidx++) {

        chunk_t *c = g_ptr_array_index(chunks, cidx);

        // loop over the chunks that are possibly near this chunk
        gridder_iterator_t gid;        
        gridder_iterator_init(gridder, &gid, 
                              c->x - max_dist, c->x + max_dist,
                              c->y - max_dist, c->y + max_dist);

        // merge with all the nearby nodes
        chunk_t *nc;
        while ((nc = gridder_iterator_next(&gid)) != NULL) {

            double dstsq = chunk_distance_to(c, nc);

            if (dstsq < bot_sq(max_dist)) 
                unionfind_union(uf, c->segment_id, nc->segment_id);
        }
    }

    for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(chunks); cidx++) {
        chunk_t *c = g_ptr_array_index(chunks, cidx);
        c->segment_id = unionfind_representative(uf, c->segment_id);
    }

    GHashTable *groups_hashtable = g_hash_table_new(bot_pint64_hash, bot_pint64_equal);

    for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(chunks); cidx++) {
        chunk_t *c = g_ptr_array_index(chunks, cidx);

        GPtrArray *chunks = g_hash_table_lookup(groups_hashtable, &c->segment_id); 
        if (!chunks) {
            chunks = g_ptr_array_new();
            g_hash_table_insert(groups_hashtable, &c->segment_id, chunks);
        }

        g_ptr_array_add(chunks, c);
    }

    unionfind_destroy(uf);

    // Make a list of groups (each group is a list of chunks)
    GList *groups = g_hash_table_get_values(groups_hashtable);
    g_hash_table_destroy(groups_hashtable);

    return groups;
}
