#include <math.h>
#include <ctype.h>
#include <assert.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif


#include <bot_core/bot_core.h>
#include <bot_param/param_client.h>
#include <bot_frames/bot_frames.h>

#include "obstacles.h"

// Based on a Hokuyo UTM
#define DEFAULT_MAX_RANGE 30.0
#define DEFAULT_MIN_RANGE 0.1
#define DEFAULT_MIN_ANGLE -3*M_PI/4
#define DEFAULT_MAX_ANGLE 3*M_PI/4
#define DEFAULT_HZ 40

// A new return is said to match an existing hit if
// it is within MAX_AGREE_DISTANCE and
// the age of the hit is less than MAX_AGE
#define MAX_AGREE_DISTANCE 0.2 //0.15
#define MAX_AGE (2.5/DEFAULT_HZ)

#define MASK_BUCKETS 360

#define LCMGL_HZ 10.0
#define PRUNE_HZ 10.0

struct sick_info
{
    char    *channel;
    int8_t   mask[MASK_BUCKETS];
    double   mask_range[MASK_BUCKETS];
    int64_t  verify_mask;
    int64_t  id;
    double   color[4];

    double max_range;
    double min_range;

    double min_angle;
    double max_angle;

    double hz;
};

struct sick_sample
{
    int64_t utime;

    double xyz[3];
    
    gridder_data_t gd_data;

    uint32_t laser_flags;
    int total_votes;

    int handled;
};

static int64_t next_id = 1; // used to allocate laser ids (each gets its own bit)

int64_t sick_get_id(state_t *state, const char *channel)
{
    sick_state_t *sstate = state->sick_state;

    int64_t *v = g_hash_table_lookup(sstate->sick_ids, channel);
    if (v == NULL) {
        v = (int64_t*) malloc(sizeof(int64_t));
        *v = next_id;
        next_id*=2;
        g_hash_table_insert(sstate->sick_ids, strdup(channel), v);
    }

    return *v;
}

struct sick_sample *sick_sample_alloc()
{
    return g_slice_alloc0(sizeof(struct sick_sample));
}

static void sick_sample_free(struct sick_sample *ss)
{
    g_slice_free1(sizeof(struct sick_sample), ss);
}

static void sick_reset(state_t *state)
{
    sick_state_t *sstate = state->sick_state;

    gridder_reset(sstate->samples_gridder);
    double pos[3];
    double pos_body[3];
    if (!bot_frames_transform_vec (state->frames, "body", "local", pos_body, pos))
        memset(pos, 0, 3 * sizeof(double));

    gridder_set_origin(sstate->samples_gridder, pos[0], pos[1]);
    
    for (unsigned int i = 0; i < bot_g_ptr_array_size(sstate->samples); i++) {
        struct sick_sample *ss = g_ptr_array_index(sstate->samples, i);
        sick_sample_free(ss);
    }
    g_ptr_array_set_size(sstate->samples, 0);

    sstate->last_lcmgl_utime = 0;
}

static void draw_sick_masks(state_t *state)
{
    sick_state_t *sstate = state->sick_state;
    GList *sinfos = g_hash_table_get_values(sstate->sick_infos);
    bot_lcmgl_t *lcmgl = sstate->lcmgl_sick;

    if (!lcmgl) 
        return;
    lcmglEnable(GL_BLEND);

    for (GList *iter=sinfos; iter; iter=iter->next) {
        struct sick_info *sinfo = iter->data;
        
        double project_matrix[12];

        if(!bot_frames_get_trans_mat_3x4(state->frames, sinfo->channel, "local",
                                         project_matrix))
            continue;
        
        // original mask
        lcmglColor4f(sinfo->color[0], sinfo->color[1], sinfo->color[2], .4);
        lcmglBegin(GL_TRIANGLE_FAN);
        double local_xyz[3];
        double zero[] = {0,0,0};
        bot_vector_affine_transform_3x4_3d(project_matrix, zero, local_xyz);
        lcmglVertex3dv(local_xyz);

        for (double theta = sinfo->min_angle; theta <= sinfo->max_angle; theta += bot_to_radians(1)) {
            int i = bot_theta_to_int(theta, MASK_BUCKETS);
            double mask_range = fmin(sinfo->max_range, sinfo->mask[i%MASK_BUCKETS] *sinfo->mask_range[i%MASK_BUCKETS]);
            
            double cos_theta, sin_theta;
            bot_fasttrig_sincos(theta, &sin_theta, &cos_theta);
            double sensor_xyz[3] = { 
                mask_range * cos_theta, 
                mask_range * sin_theta, 
                0 
            };
            bot_vector_affine_transform_3x4_3d(project_matrix, sensor_xyz, local_xyz);
            lcmglVertex3dv(local_xyz);
        }
        lcmglEnd();
    }
    
    lcmglDisable(GL_BLEND);
    g_list_free(sinfos);
}

static struct sick_info *get_sick_info(state_t *state, const char *channel)
{
    sick_state_t *sstate = state->sick_state;

    struct sick_info *sinfo = g_hash_table_lookup(sstate->sick_infos, channel);
    if (sinfo)
        return sinfo;

    sinfo = (struct sick_info*) calloc(1, sizeof(struct sick_info));
    sinfo->channel = strdup(channel);
    sinfo->id = sick_get_id(state, channel);
  
    char cfg_path[256];
    snprintf(cfg_path, sizeof(cfg_path), "planar_lidar.%s", sinfo->channel);

    g_hash_table_insert(sstate->sick_infos, sinfo->channel, sinfo);
    char key[512];

    // get the maximum range of the lidar
    sprintf(key, "planar_lidars.%s.max_range", channel);
    if (bot_param_get_double (state->param, key, &(sinfo->max_range)))
        sinfo->max_range = DEFAULT_MAX_RANGE;
    sinfo->min_range = DEFAULT_MIN_RANGE;

    // get the angle fov
    double angle_range[2];
    sprintf(key, "planar_lidars.%s.angle_range", channel);
    if (bot_param_get_double_array(state->param, key, angle_range, 2) != 2) {
        sinfo->min_angle = DEFAULT_MIN_ANGLE;
        sinfo->max_angle = DEFAULT_MAX_ANGLE;
    }
    else {
        sinfo->min_angle = angle_range[1] * M_PI/180;
        sinfo->max_angle = angle_range[0] * M_PI/180;
    }

    // get the Hz
    sprintf (key, "planar_lidars.%s.hz", channel);
    if (bot_param_get_double (state->param, key, &(sinfo->hz)))
        sinfo->hz = DEFAULT_HZ;

    // handle a mask
    sprintf(key, "planar_lidars.%s.mask", channel);
    char **masks = bot_param_get_str_array_alloc(state->param, key);
    sprintf(key, "planar_lidars.%s.mask_range", channel);
    char **mask_ranges = bot_param_get_str_array_alloc(state->param, key);
    for (int j = 0; masks && masks[j]!=NULL && masks[j+1]!=NULL; j+=2) {
        double v0 = strtof(masks[j], NULL);
        double v1 = strtof(masks[j+1], NULL);
        
        double mask_range = sinfo->max_range;
        if (mask_ranges&&mask_ranges[j/2]) 
            mask_range = strtof(mask_ranges[j/2], NULL);
        
        // take small steps so that we won't skip a bucket due to a 
        // rounding area (hence the extra / 2.0)
        double theta_per_index = 2.0 * M_PI / MASK_BUCKETS;
        for (double t = v0; t <= v1; t += theta_per_index/2.0) {
            int idx = bot_theta_to_int(t, MASK_BUCKETS);            
            sinfo->mask[idx]=1;
            sinfo->mask_range[idx]=mask_range;
        }
    }

    // handle a mask
    //char key[512];

    sprintf(key, "planar_lidars.%s.verifiers", channel);
    char **verifiers = bot_param_get_str_array_alloc(state->param, key);
    for (int j = 0; verifiers && verifiers[j]!=NULL; j++) {
        int64_t vid = sick_get_id(state, verifiers[j]);
        //assert(vid != sinfo->id);
        sinfo->verify_mask |= vid;
    }

    if (sinfo->verify_mask == 0)
        printf("WARNING: No verifiers given for channel %s\n", channel);

    

    sprintf(key, "planar_lidars.%s.viewer_color", channel);
    int sz = bot_param_get_double_array(state->param, key, sinfo->color, 3);
    if (sz != 3) {
        printf("%s : missing or funny color!\n", key);
        sinfo->color[0] = 1;
        sinfo->color[1] = 1;
        sinfo->color[2] = 1;
    }

    return sinfo;
}

void rebuild_gridder(state_t *state)
{
    sick_state_t *sstate = state->sick_state;
    double pos[3];
    double pos_body[3];
    bot_frames_transform_vec (state->frames, "body", "local", pos_body, pos);

    gridder_reset(sstate->samples_gridder);
    gridder_set_origin(sstate->samples_gridder, pos[0], pos[1]);

    for (unsigned int i = 0; i < bot_g_ptr_array_size(sstate->samples); i++) {
        struct sick_sample *ss = g_ptr_array_index(sstate->samples, i);
        gridder_add(sstate->samples_gridder, ss->xyz[0], ss->xyz[1], ss);
    }
}

void prune_hits(state_t *state, int64_t utime)
{
    sick_state_t *sstate = state->sick_state;

    for (unsigned int i = 0; i < bot_g_ptr_array_size(sstate->samples); i++) {
        struct sick_sample *ss = g_ptr_array_index(sstate->samples, i);

        double age = (utime - ss->utime) / 1000000.0;
        if (age > MAX_AGE || ss->handled) {

            gridder_remove(sstate->samples_gridder, ss->xyz[0], ss->xyz[1], ss);
            g_ptr_array_remove_index_fast(sstate->samples, i);
            i--;
            sick_sample_free(ss);
            continue;
        }
    }        
}

void cleanup_gridder(state_t *state, int64_t utime)
{
    sick_state_t *sstate = state->sick_state;
    double pos[3];
    double pos_body[3];
    bot_frames_transform_vec (state->frames, "body", "local", pos_body, pos);

    prune_hits(state, utime);

    // rebuild the gridder if we've moved too far.
    double dist = sqrt( bot_sq(pos[0] - gridder_origin_x(sstate->samples_gridder)) +
                        bot_sq(pos[1] - gridder_origin_y(sstate->samples_gridder)) );
    if (dist > 0.25)
        rebuild_gridder(state);
}


int handle_laser(state_t *state, const char *channel, const bot_core_planar_lidar_t *ldata, int is_broom)
{
    sick_state_t *sstate = state->sick_state;

    struct sick_info *sinfo = get_sick_info(state, channel);

    double bot_pos[3];
    double pos_body[3];
    if (!bot_frames_transform_vec (state->frames, "body", "local", pos_body, bot_pos))
        return 0;

    double project_matrix[12];
    if(!bot_frames_get_trans_mat_3x4(state->frames, sinfo->channel, "local",
                                     project_matrix))
        return 0;

    double sensor_to_body[12];
    if(!bot_frames_get_trans_mat_3x4(state->frames, sinfo->channel, "body",
                                     sensor_to_body))
        return 0;
    
    if (sstate->lcmgl_sick) {
        bot_lcmgl_t *lcmgl = sstate->lcmgl_sick;
        lcmglPointSize(4);
        lcmglBegin(GL_POINTS);
        //lcmglColor3f(sinfo->color[0], sinfo->color[1], sinfo->color[2]);
    }

    GPtrArray *hits = g_ptr_array_new();

    for (int i = 0; i < ldata->nranges; i++) {
        double range = ldata->ranges[i];
        double theta = ldata->rad0 + ldata->radstep*i;
        
        // physical limitations parameters
        if (range > sinfo->max_range || range < sinfo->min_range)
            continue;

        // clamp if we suspect something's going wrong, like seeing
        // hills as obstacles
        if (range > sstate->MAX_RANGE) 
            continue;

        double cos_theta, sin_theta;
        bot_fasttrig_sincos(theta, &sin_theta, &cos_theta);
        double sensor_xyz[3] = { range * cos_theta, range * sin_theta, 0};

        double body_xyz[3];
        bot_vector_affine_transform_3x4_3d(sensor_to_body, sensor_xyz, 
                body_xyz);
        
        //checking if within footprint
        //assuming rectangle 
        if((body_xyz[0] < state->front_left[0] && body_xyz[0] > state->rear_left[0]) &&
           (body_xyz[1] < state->front_left[1] && body_xyz[1] > state->front_right[1])){
             continue;
        }
        
        //check if within footprint 
        

        double local_xyz[3];
        bot_vector_affine_transform_3x4_3d(project_matrix, sensor_xyz, 
                local_xyz);

        double height = local_xyz[2] - bot_pos[2];

        int idx = bot_theta_to_int(theta, MASK_BUCKETS);
        
        if (1)  {
            // denoise: require a nearby point to roughly agree as to range.
            int lo = bot_max(0, i - 2);
            int hi = bot_min(ldata->nranges-1, i+2);

            int okay = 0;
            double RANGE_AGREE_TOLERANCE = 1;

            for (int j = lo; j <= hi; j++)  {
                if (fabs(ldata->ranges[j] - range) < RANGE_AGREE_TOLERANCE) {
                    okay = 1;
                    break;
                }
            }

            if (!okay) {
                continue;
            }
        }
        
        if (sinfo->mask[idx] && (sinfo->mask_range[idx] > range)){
            fprintf(stderr,"Masked : %d\n", idx);
            continue;
        }


        // create a new sample
        struct sick_sample *ss = sick_sample_alloc();
        ss->utime = ldata->utime;
        memcpy(ss->xyz, local_xyz, 3*sizeof(double));
        ss->total_votes=1;


        // If the return is close we assume that it is not the ground if above quite a high slope from the vehicle
        // This code path doesn't require verifiers.

        //int above_worst_case_ground_plane = (( height - sstate->ALWAYS_ACCEPT_GROUND_CLEARANCE - 
        //                                       sstate->ALWAYS_ACCEPT_GROUND_SLOPE * range > 0)  &&
        //                                     (range < sstate->ALWAYS_ACCEPT_MAX_RANGE));
        int broom_above_worst_case_ground_plane = 1;/*(( height - sstate->BROOM_ALWAYS_ACCEPT_GROUND_CLEARANCE - 
                                                     sstate->BROOM_ALWAYS_ACCEPT_GROUND_SLOPE * range > 0)
						     &&(range < sstate->BROOM_ALWAYS_ACCEPT_MAX_RANGE));*/
          
        if (broom_above_worst_case_ground_plane) {
            /*fprintf(stderr,"handle_laser():creating new sample: h:%lf -C:%lf -S:%lfxr:%lf MR:%lf \n",
                height,
                sstate->ALWAYS_ACCEPT_GROUND_CLEARANCE,
                sstate->ALWAYS_ACCEPT_GROUND_SLOPE,
                range,sstate->ALWAYS_ACCEPT_MAX_RANGE);
            fprintf(stderr,"aboveground1 ");*/
            hit_t *h = hit_alloc();
            memcpy(h->xyz, local_xyz, 3 * sizeof(double));
            h->xyz[2] = 0;
            h->utime = ldata->utime;
            h->weight = 1.0;
            h->modality_flag = MODALITY_PLANAR_LIDAR;
            g_ptr_array_add(hits, h);
            ss->handled = 1;
       
            if (sstate->lcmgl_sick) {
                bot_lcmgl_t *lcmgl = sstate->lcmgl_sick;
                lcmglColor3f(0.9,0.3,0.3);
                lcmglVertex3dv(ss->xyz);
            }
        }

	    // For the skirts use a less steep ground plane.
        int skirt_above_worst_case_ground_plane = 1;/*(( height - sstate->SKIRT_ALWAYS_ACCEPT_GROUND_CLEARANCE - 
                                                     sstate->SKIRT_ALWAYS_ACCEPT_GROUND_SLOPE * range > 0)
						     &&(range < sstate->SKIRT_ALWAYS_ACCEPT_MAX_RANGE));*/

        //if (!is_broom && skirt_above_worst_case_ground_plane) {
        //if (skirt_above_worst_case_ground_plane) {
        
        // Use low returns for gridder but only make them valid if we see an above ground slope return as well
        if (1) {
            // does this hit confirm any previous hits?
            gridder_iterator_t gid;        
            gridder_iterator_init(sstate->samples_gridder, &gid, 
                                  ss->xyz[0] - MAX_AGREE_DISTANCE, ss->xyz[0] + MAX_AGREE_DISTANCE,
                                  ss->xyz[1] - MAX_AGREE_DISTANCE, ss->xyz[1] + MAX_AGREE_DISTANCE);
            
            struct sick_sample *tss;
            while ((tss = gridder_iterator_next(&gid)) != NULL) {
                
                double dstsq = bot_sq(tss->xyz[0] - ss->xyz[0]) + bot_sq(tss->xyz[1] - ss->xyz[1]);
                
                if (dstsq <= MAX_AGREE_DISTANCE) {
                    
                    double tss_age = (ldata->utime - tss->utime) / 1000000.0;
                    
                    // if we've just made this sample "good", pass it
                    // along
                    //if ((tss->laser_flags & sinfo->verify_mask)!=0 &&
                    //    tss_age <= MAX_AGE && !tss->handled) {
                    // add condition that hit is only made good if one return is above ground slope
                    
                    double max_age = MAX_AGE * DEFAULT_HZ / sinfo->hz;
                    
                    if (skirt_above_worst_case_ground_plane && tss_age <= max_age) {
                        //only act as a verification for the hit if we're above the ground "plane"
                        //  and not too old (JON)
                        tss->laser_flags |= sinfo->id;
                        tss->total_votes++;
                    }
                    
                    if (skirt_above_worst_case_ground_plane &&
                        (tss->laser_flags & sinfo->verify_mask) !=0 &&
                        tss_age <= max_age && !tss->handled) {

                        hit_t *h = hit_alloc();
                        memcpy(h->xyz, local_xyz, 3 * sizeof(double));
                        h->xyz[2] = 0;
                        h->utime = ldata->utime;
                        h->weight = 1.0;
                        h->modality_flag = MODALITY_PLANAR_LIDAR;
                        g_ptr_array_add(hits, h);
          
                        if (sstate->lcmgl_sick) {
                            bot_lcmgl_t *lcmgl = sstate->lcmgl_sick;
                            lcmglColor3f(0.3,0.9,0.3);
                            lcmglVertex3dv(ss->xyz);
                        }
                        tss->handled = 1;
                    }
                }
            }
            
            // add the new sample
            g_ptr_array_add(sstate->samples, ss);
            gridder_add(sstate->samples_gridder, ss->xyz[0], ss->xyz[1], ss);
        } else {
            // it's a broom, and it's not above the ground plane. We're done.
            sick_sample_free(ss);
        }
	
    }

    handle_hits(state, hits);

    if (sstate->lcmgl_sick) {
        bot_lcmgl_t *lcmgl = sstate->lcmgl_sick;
        lcmglEnd();
    }
    double dt = (ldata->utime - sstate->last_lcmgl_utime)/1000000.0;
    if (fabs(dt) > 3)
        sick_reset(state);

    if (1 || dt > 1.0 / LCMGL_HZ) {

        if (state->show_sick_masks) {
            draw_sick_masks(state);
        }

        sstate->last_lcmgl_utime = ldata->utime;

        if (sstate->lcmgl_sick) {
            bot_lcmgl_t *lcmgl = sstate->lcmgl_sick;
            bot_lcmgl_switch_buffer(lcmgl);
        }
    }

    if (1) {
        double dt = (ldata->utime - sstate->last_cleanup_utime) / 1000000.0;
        if (dt > 1.0 / PRUNE_HZ)
            cleanup_gridder(state, ldata->utime);
    }

    return 0;
}

static void
on_skirt(const lcm_recv_buf_t *rbuf, const char *channel, 
         const bot_core_planar_lidar_t *ldata, void *user)
{
    state_t *state = (state_t*) user;
    //handle_laser(state, channel, ldata, 0);
    // skirts are pointing down so treat the same as brooms
    handle_laser(state, channel, ldata, 0);
}

static void
on_broom(const lcm_recv_buf_t *rbuf, const char *channel, 
         const bot_core_planar_lidar_t *ldata, void *user)
{
    state_t *state = (state_t*) user;
    if (!strcmp(channel,"BROOM_CARRIAGE"))
        return;

    handle_laser(state, channel, ldata, 1);
}

int sick_init(state_t *state)
{
    sick_state_t *sstate = (sick_state_t*) calloc(1, sizeof(sick_state_t));
    state->sick_state = sstate;
    
    //sstate->ALWAYS_ACCEPT_GROUND_SLOPE = 0.025;
    //sstate->ALWAYS_ACCEPT_GROUND_CLEARANCE = 0.01;
    //sstate->ALWAYS_ACCEPT_MAX_RANGE = 50.0;
    //sstate->ALWAYS_ACCEPT_MAX_RANGE = 6.0;
    sstate->MAX_RANGE = DEFAULT_MAX_RANGE;

    sstate->BROOM_ALWAYS_ACCEPT_GROUND_SLOPE = 0.1;
    sstate->BROOM_ALWAYS_ACCEPT_GROUND_CLEARANCE = 0.1;
    sstate->BROOM_ALWAYS_ACCEPT_MAX_RANGE = 6.0;

    sstate->SKIRT_ALWAYS_ACCEPT_GROUND_SLOPE = 0.04;
    sstate->SKIRT_ALWAYS_ACCEPT_GROUND_CLEARANCE = 0.001;
    sstate->SKIRT_ALWAYS_ACCEPT_MAX_RANGE = 50.0;


    sstate->sick_ids = g_hash_table_new(g_str_hash, g_str_equal);
    sstate->sick_infos = g_hash_table_new(g_str_hash, g_str_equal);
    if (state->show_sick_hits||state->show_sick_masks) {
        sstate->lcmgl_sick = bot_lcmgl_init(state->lcm, "obstacles-sick");
    }

    sstate->samples = g_ptr_array_new();
    sstate->samples_gridder = gridder_create(-100, 100, -100, 100, 0.25,
                                             (int) (&((struct sick_sample*) NULL)->gd_data));

    ///////////////////////////////////////////////
    bot_core_planar_lidar_t_subscribe(state->lcm, "SKIRT_FRONT", on_skirt, state);
    bot_core_planar_lidar_t_subscribe(state->lcm, "SKIRT_REAR", on_skirt, state);
    //bot_core_planar_lidar_t_subscribe(state->lcm, "BROOM_.*", on_broom, state);
    
    return 0;
}

