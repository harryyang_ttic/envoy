#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include <glib.h>
#include <bot_core/bot_core.h>
//#include <common/color_util.h>

#include "tracks.h"
//#include "estimate_velocity.h"

#define TRAJECTORY_SAMPLES 50

static GPtrArray *otracks_free;
static int64_t next_otrack_id = 1;

void trajectory_sample_destroy(void *user, void *p)
{
    free(p);
}

//////////////////////////////////////////////////////////////////
void otracks_init()
{
    otracks_free = g_ptr_array_new();
}

//////////////////////////////////////////////////////////////////
otrack_t *otrack_alloc()
{
    int sz = bot_g_ptr_array_size(otracks_free);
    otrack_t *t;

    if (sz > 0) {
        t = g_ptr_array_index(otracks_free, sz-1);
        g_ptr_array_remove_index_fast(otracks_free, sz-1);
    } else {
        t = (otrack_t*) calloc(1, sizeof(otrack_t));
        t->chunks = g_ptr_array_new();
        t->trajectory = bot_ptr_circular_new(TRAJECTORY_SAMPLES, trajectory_sample_destroy, NULL);
    }

    t->maturity = 0;
    t->id = next_otrack_id++;
    srand(t->id);
    bot_color_util_rand_color(t->color, 0.4, 0.4);

    return t;
}

//////////////////////////////////////////////////////////////////
void otrack_free(otrack_t *t)
{
    g_ptr_array_set_size(t->chunks, 0);
    bot_ptr_circular_clear(t->trajectory);
    g_ptr_array_add(otracks_free, t);
}

/*
void otrack_add_trajectory_sample(otrack_t *t, int64_t utime, double pos[2])
{
    struct trajectory_sample *ts = (struct trajectory_sample*) 
        calloc(1, sizeof(struct trajectory_sample));

    ts->utime = utime;
    memcpy(ts->pos, pos, 2 * sizeof(double));

    bot_ptr_circular_add(t->trajectory, ts);
}

void otrack_estimate_current_position_and_velocity(otrack_t *t, int64_t utime)
{
    estimate_velocity_t ev;
    estimate_velocity_init(&ev);

    for (unsigned int i = 0; i < bot_ptr_circular_size(t->trajectory); i++) {
        struct trajectory_sample *ts = bot_ptr_circular_index(t->trajectory, i);
        double dt = (utime - ts->utime)/1000000.0;
        estimate_velocity_update(&ev, ts->pos[0], ts->pos[1], dt, exp(-3*fabs(dt)));
    }

    if (estimate_velocity_finish(&ev)) {
        t->utime = utime;
        memset(t->pos, 0, 2 * sizeof(double));
        memset(t->vel, 0, 2 * sizeof(double));
        memset(t->err, 0, 2 * sizeof(double));
        t->vmag = 0;
        return;
    }

    t->utime = utime;
    t->pos[0] = ev.x0;
    t->pos[1] = ev.y0;
    t->vel[0] = ev.vx;
    t->vel[1] = ev.vy;
    t->err[0] = ev.xchi;
    t->err[1] = ev.ychi;
    t->vmag = sqrt(sq(t->vel[0]) + sq(t->vel[1]));
}
*/
