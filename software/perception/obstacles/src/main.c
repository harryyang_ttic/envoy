/********************************************************************
  MIT DARPA Urban Challenge Team

  Module Name: obstacles

  Purpose: Obstacle detection and tracking, using lidar 

  Files:
    main.c : command-line interface
    box.c  : bounding box utilities and data structure
    estimate_velocity.c : Least-squares velocity estimation utility
    chunks.c : data structure representing several hits
    frontend_planar_lidar.c : frontend processing of planar lidar
    gridder.c : data structure for organizing objects in 2D space.
    track.c : obstacle track data structure

  Author: Edwin Olson (eolson@mit.edu)
  Maintainer: Matt Walter (mwalter@csail.mit.edu) and Luke Fletcher (lukesf@mit.edu)

********************************************************************/
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <glib.h>
#include <getopt.h>
#include <assert.h>
#include <math.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <lcm/lcm.h>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>

#include <er_common/gridmap_util.h>
#include <er_common/vote.h>

//#include <lcmtypes/er_obstacles.h>
#include <lcmtypes/erlcm_failsafe_t.h>
#include <lcmtypes/er_lcmtypes.h>

#include "obstacles.h"
#include "chunks.h"
#include "tracks.h"

// otrack at this rate, or whenever the velodyne does a complete spin
// (whichever rate is faster). we also publish at this rate.
#define OTRACK_HZ 9

#define MAX_CHUNK_DIMENSION 0.1//0.25   // max chunk height & width = 2xMAX_CHUNK_DIMENSION
#define MAX_ASSOCIATE_DISTANCE 1.2
#define MAX_CHUNK_CONNECT_DISTANCE (MAX_ASSOCIATE_DISTANCE * MAX_CHUNK_DIMENSION)

#define MIN_LCMGL_CHUNK_SIZE 0.1
// meters
#define GRIDDER_RANGE         7.0 //40.0     // width & height of gridder  = 2xGRIDDER_RANGE
// this only affects how many buckets we have to search
#define GRIDDER_RESOLUTION    0.15 // 0.25

// must be > 0
static int MIN_CHUNK_WEIGHT = 4;
static int MIN_SEEN_ITERS = 3;

// When chunks that fall outside a rectangle around the vehicle (the "persistent mask")
// haven't been seen for more than CHUNK_EXPIRE_SEC, they will be freed.
// Note: There is a separate expiration time for chunks that lie within the persistent mask.

#define CHUNK_MAX_SAMPLES 20


// convenience function to get the vehicle position in the local frame.

int frames_vehicle_pos_local(BotFrames *frames, double pos[3])
{
    double pos_body[3] = { 0, 0, 0 };
    return bot_frames_transform_vec(frames, "body", "local", pos_body, pos);
}

//////////////////////////////////////////////////////////////////
hit_t *hit_alloc()
{
    hit_t *h = g_slice_alloc(sizeof(hit_t));
    return h;
}

void hit_free(hit_t *h)
{
    g_slice_free1(sizeof(hit_t), h);
}

//////////////////////////////////////////////////////////////////
void reset(state_t *state)
{
    for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(state->chunks); cidx++) {
        chunk_t *c = g_ptr_array_index(state->chunks, cidx);
        chunk_free(c);
    }
    g_ptr_array_set_size(state->chunks, 0);

    for (unsigned int tidx = 0; tidx < bot_g_ptr_array_size(state->otracks); tidx++) {
        otrack_t *t = g_ptr_array_index(state->otracks, tidx);
        // chunks were just freed above.
        otrack_free(t);
    }
    g_ptr_array_set_size(state->otracks, 0);
    g_hash_table_remove_all(state->otracks_hashtable);

    double pos[3];

    //if (!atrans_vehicle_pos_local(state->atrans, pos))
    if(!frames_vehicle_pos_local(state->frames, pos))
        memset(pos, 0, 3*sizeof(double));

    gridder_reset(state->chunks_gridder);
    gridder_set_origin(state->chunks_gridder, pos[0], pos[1]);

    state->last_publish_utime = 0;
    state->last_otrack_utime = 0;
}

static void update_failsafe(state_t *state, int failsafe)
{
    /*
      state->failsafe = failsafe;

      printf("WARNING: Changing to failsafe %d\n", failsafe);

      sick_state_t *sstate = state->sick_state;

      switch (failsafe)
      {
      case 0:
      // normal operation
      if (sstate) {
      //sstate->ALWAYS_ACCEPT_GROUND_SLOPE = 0.35;
      //sstate->ALWAYS_ACCEPT_GROUND_CLEARANCE = 0.1;
      //sstate->ALWAYS_ACCEPT_MAX_RANGE = 6.0;
      sstate->ALWAYS_ACCEPT_GROUND_SLOPE = 0.0035;
      sstate->ALWAYS_ACCEPT_GROUND_CLEARANCE = 0.1;
      sstate->ALWAYS_ACCEPT_MAX_RANGE = 60.0;
      }

      MIN_CHUNK_WEIGHT = 4;
      break;

      case 1:
      // uh-oh
      if (sstate) {
      sstate->ALWAYS_ACCEPT_GROUND_SLOPE = 0.4;
      sstate->ALWAYS_ACCEPT_GROUND_CLEARANCE = 0.2;
      sstate->ALWAYS_ACCEPT_MAX_RANGE = 3.0;
      }

      MIN_CHUNK_WEIGHT = 8;
      break;

      case 2:
      // AAGGGH!
      if (sstate) {
      sstate->ALWAYS_ACCEPT_GROUND_SLOPE = 0.5;
      sstate->ALWAYS_ACCEPT_GROUND_CLEARANCE = 0.1;
      sstate->ALWAYS_ACCEPT_MAX_RANGE = -5.0; // (none)
      }

      MIN_CHUNK_WEIGHT = 10;
      break;

      default:
      // never see 3
      printf("WARNING: Damn you, whoever killed us.\n");
      break;
      }
    */
}

static void
on_failsafe(const lcm_recv_buf_t *rbuf, const char *channel, 
            const erlcm_failsafe_t *msg, void *user)
{
    state_t *state = (state_t*) user;
    sick_state_t *sstate = state->sick_state;
    
    if (msg->mode != state->failsafe)
        update_failsafe(state, msg->mode);
    
    double dt = (msg->utime - state->failsafe_utime)/1000000.0;
    state->failsafe = msg->mode;
    state->failsafe_utime = msg->utime;
    if (msg->timer >= state->failsafe_timer)
        state->failsafe_timer = fmin(msg->timer,50);
    else if (dt > 0)
        state->failsafe_timer = fmax(0, state->failsafe_timer - dt);

    if (sstate) {
        if (state->failsafe_timer > 60)
            sstate->MAX_RANGE = 0;
        else if (state->failsafe_timer > 30)
            sstate->MAX_RANGE = 4;
        else
            sstate->MAX_RANGE = HUGE;
    }
}


static void
on_pose(const lcm_recv_buf_t *rbuf, const char *channel, 
        const bot_core_pose_t *msg, void *user)
{
    state_t *state = (state_t*) user;

    if (state->pose)
        bot_core_pose_t_destroy(state->pose);
    state->pose = bot_core_pose_t_copy(msg);

    memcpy(state->mask_pos, msg->pos, 3 * sizeof(double));
    double rpy[3];
    bot_quat_to_roll_pitch_yaw(msg->orientation, rpy);
    bot_fasttrig_sincos(rpy[2], &state->mask_orientation_vector[1],
                        &state->mask_orientation_vector[0]);
}
 
// This function is called whenever we observe something that isn't
// ground.

void handle_hits(state_t *state, GPtrArray *hits)
{
    //  fprintf(stderr,"Handling hits\n");

    g_async_queue_push(state->hits_queue, hits);
}


// return the number with the larger magnitude
static double max_magnitude(double a, double b)
{
    if (fabs(a) > fabs(b))
        return a;
    return b;
}

static inline double min_magnitude(double a, double b)
{
    if (fabs(a) < fabs(b))
        return a;
    return b;
}

static void draw_mask(state_t *state, bot_lcmgl_t *lcmgl, double mask_forward, double mask_backward, double mask_width)
{
    lcmglEnable(GL_BLEND);
    lcmglBegin(GL_QUADS);
    double forward[2] = { mask_forward * state->mask_orientation_vector[0],
                          mask_forward * state->mask_orientation_vector[1] };
    double backward[2] = { - mask_backward * state->mask_orientation_vector[0],
                           - mask_backward * state->mask_orientation_vector[1] };
    
    double left[2] = { -mask_width/2 * state->mask_orientation_vector[1],
                       mask_width/2 * state->mask_orientation_vector[0] };
    
    lcmglVertex3d(state->mask_pos[0] + forward[0] + left[0],
                  state->mask_pos[1] + forward[1] + left[1],
                  state->mask_pos[2]);
    
    lcmglVertex3d(state->mask_pos[0] + forward[0] - left[0],
                  state->mask_pos[1] + forward[1] - left[1],
                  state->mask_pos[2]);
    
    lcmglVertex3d(state->mask_pos[0] + backward[0] - left[0],
                  state->mask_pos[1] + backward[1] - left[1],
                  state->mask_pos[2]);
    
    lcmglVertex3d(state->mask_pos[0] + backward[0] + left[0],
                  state->mask_pos[1] + backward[1] + left[1],
                  state->mask_pos[2]);
    
    lcmglEnd();
}

void report_chunk(chunk_t *c)
{
    if (!c->do_stats_on_me)
        return;

    printf("%15f\n", c->sick_range);
}

void otrack(state_t *state, int64_t utime, double dt)
{
    BotTrans body_to_local;

    if(!bot_frames_get_trans(state->frames, "body", "local", &body_to_local))
        return;
    double rpy[3];
    bot_quat_to_roll_pitch_yaw(body_to_local.rot_quat, rpy);
    double heading = rpy[2];
    const double *pos = body_to_local.trans_vec;

    /////////////////////////////////////////////////////////////////////////
    // Age chunks
    for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(state->chunks); cidx++) {
        chunk_t *c = g_ptr_array_index(state->chunks, cidx);

        c->age_dt += dt; 
        c->age_iters ++;

        // compute whether chunk falls within the persistence mask, which corresponds
        // to a rectangle centered on the body frame and aligned with bot yaw
        int persist_masked = 0;

        if (1) {
            // Efficient (?) way to calculate (x,y) position in body frame
            double dx = c->x - state->mask_pos[0];
            double dy = c->y - state->mask_pos[1];

            double body_x = dx * state->mask_orientation_vector[0] +
                dy * state->mask_orientation_vector[1];
            double body_y = - dx * state->mask_orientation_vector[1] +
                dy * state->mask_orientation_vector[0];

            persist_masked = (body_x < state->persist_mask_forward) &&
                (body_x > - state->persist_mask_backward) &&
                (fabs(body_y) < state->persist_mask_width/2);            
        }

        if (c->weight >= MIN_CHUNK_WEIGHT) {
            c->seen_dt += dt;
            c->seen_iters++;
            c->unseen_dt = 0;
        } else {
            c->unseen_dt += dt;
        }

        // kill the chunk if
        //   1) Lies the body-relative mask and hasn't been seen in more than OUTSIDE_PERSIST_MASK_EXPIRE_SEC OR
        //   2) Is inside the mask but it hasn't been seen in more than PERSIST_MASK_EXPIRE_SEC
        
        if ( (!persist_masked && c->unseen_dt > state->outside_persist_mask_expire_sec) ||
            ( persist_masked && c->unseen_dt > state->persist_mask_expire_sec) ||
            c->seen_dt == 0) {
            c->reaping = 1;
            g_ptr_array_remove_index_fast(state->chunks, cidx);
            cidx--;

            // we'll delete the chunk from its track and finally
            // free the chunk below... UNLESS it's not part of a track...
            if (c->seen_dt == 0) {
                report_chunk(c);
                chunk_free(c);
            }
        }
    }

    // delete "reaping" chunks from tracks. If a track has no more
    // chunks left, delete it.
    for (unsigned int tidx = 0; tidx < bot_g_ptr_array_size(state->otracks); tidx++) {
        otrack_t *t = g_ptr_array_index(state->otracks, tidx);

        for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(t->chunks); cidx++) {
            chunk_t *c = g_ptr_array_index(t->chunks, cidx);

            if (c->reaping) {
                g_ptr_array_remove_index_fast(t->chunks, cidx);
                cidx--;
                report_chunk(c);
                chunk_free(c);
            }
        }

        if (bot_g_ptr_array_size(t->chunks) == 0) {
            g_ptr_array_remove_index_fast(state->otracks, tidx);
            g_hash_table_remove(state->otracks_hashtable, &t->id);
            otrack_free(t);
            tidx--;
        }
    }

    /////////////////////////////////////////////////////////////////////////
    // *** Note *** Below this point, all chunks are conserved (all
    // chunks will be recycled into a otrack: either an old otrack, or
    // an existing otrack.) They all get segmented and then get
    // reassigned to otracks.

    /////////////////////////////////////////////////////////////////////////
    // populate the segmentation gridder and renumber segments (so each segment
    // is initially disconnected.
    gridder_reset(state->otracks_gridder);
    gridder_set_origin(state->otracks_gridder, pos[0], pos[1]);

    int64_t next_segment_id = 1; // 0 is special

    for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(state->chunks); cidx++) {
        chunk_t *c = g_ptr_array_index(state->chunks, cidx);
        c->segment_id = next_segment_id++;
        gridder_add(state->otracks_gridder, c->x, c->y, c);
    }

    /////////////////////////////////////////////////////////////////////////
    // segment all of the chunks
    GList *segments = chunks_segment(state->otracks_gridder, state->chunks, 
                                     MAX_CHUNK_CONNECT_DISTANCE);
    int segments_len = g_list_length(segments);
    /////////////////////////////////////////////////////////////////////////

    // Now, associate old otracks to segments.
    vote_t *votes = vote_create(segments_len);

    // for each old otrack, find the segment that matches best.  We
    // keep track of the best otrack for each segment. If two otracks
    // match a segment, we take the better one?
    int64_t segment_assoc[next_segment_id]; // the best track id for each segment
    int32_t segment_votes[next_segment_id]; // how many votes for that track id?
    memset(segment_votes, 0, sizeof(segment_votes));

    for (unsigned int tidx = 0; tidx < bot_g_ptr_array_size(state->otracks); tidx++) {
        otrack_t *t = g_ptr_array_index(state->otracks, tidx);

        vote_reset(votes);

        for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(t->chunks); cidx++) {
            chunk_t *c = g_ptr_array_index(t->chunks, cidx);

            vote_cast_vote(votes, c->segment_id);
        }
        
        int winner_nvotes;
        int64_t winner_id = vote_get_winner(votes, &winner_nvotes);
        if (winner_id >= 0) {
            assert(winner_id < next_segment_id);

            if (winner_nvotes > segment_votes[winner_id]) {
                segment_assoc[winner_id] = t->id;
                segment_votes[winner_id] = winner_nvotes;
            }
        }
    }

    vote_destroy(votes);
    votes = NULL;

    // remove the chunks from the track they *were* associated with
    for (unsigned int tidx = 0; tidx < bot_g_ptr_array_size(state->otracks); tidx++) {
        otrack_t *t = g_ptr_array_index(state->otracks, tidx);
        g_ptr_array_set_size(t->chunks, 0);
    }

    // add the chunks to the track they're *now* associated with
    for (GList *seg_iter=segments; seg_iter; seg_iter=seg_iter->next) {
        GPtrArray *chunks = seg_iter->data;
        assert(bot_g_ptr_array_size(chunks) > 0);
        chunk_t *chunk0 = g_ptr_array_index(chunks, 0);
        int64_t otrack_id = segment_assoc[chunk0->segment_id];
        otrack_t *t;
        
        if (segment_votes[chunk0->segment_id] == 0) {
            // no match: create a new otrack.
            t = otrack_alloc();
            g_ptr_array_add(state->otracks, t);
            g_hash_table_insert(state->otracks_hashtable, &t->id, t);
        } else {
            t = g_hash_table_lookup(state->otracks_hashtable, &segment_assoc[chunk0->segment_id]);
            assert(t);
            assert(t->id == otrack_id);
        }

        g_ptr_array_set_size(t->chunks, 0);

        for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(chunks); cidx++) {
            chunk_t *c = g_ptr_array_index(chunks, cidx);
            c->otrack_id = t->id;
            g_ptr_array_add(t->chunks, c);
        }
        t->maturity ++;
    }

    /////////////////////////////////////////////////////////////////////////
    // Tracking algorithm (version 23,186,991). Here's how this works:
    //
    // Each chunk has an associated velocity with it (more on this in
    // a second). A track has the average velocity of its chunks.
    // 
    // At each iteration, a number of new chunks are combined with old
    // chunks.  We cluster these chunks into a bunch of segments. We
    // just did all this above.
    //
    // We compute an instantaneous velocity estimate by comparing the
    // center of the bounding box for the old chunks versus the center
    // of the bounding box of all the chunks (both new and old). This
    // velocity under-estimate is propagated to each of the chunks in
    // the segment, which just have a low-pass filter on them.
    //
    // By storing velocities in the chunks, we make it easier to get
    // reasonable velocity estimates even after split/merge events.
    

    //    for (unsigned int sidx = 0; sidx < bot_g_ptr_array_size(segments); sidx++) {
    //        GPtrArray *chunks = g_ptr_array_index(segments, sidx);
    for (GList *seg_iter=segments; seg_iter; seg_iter=seg_iter->next) {
        GPtrArray *chunks = seg_iter->data;
        assert(bot_g_ptr_array_size(chunks) > 0);
        chunk_t *chunk0 = g_ptr_array_index(chunks, 0);
        int64_t otrack_id = chunk0->otrack_id;
        otrack_t *t = g_hash_table_lookup(state->otracks_hashtable, &otrack_id);
        assert(t);

        double all_x_min = HUGE, all_x_max = -HUGE;
        double all_y_min = HUGE, all_y_max = -HUGE;
        double old_x_min = HUGE, old_x_max = -HUGE;
        double old_y_min = HUGE, old_y_max = -HUGE;
        int all_cnt = 0, old_cnt = 0;

        for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(chunks); cidx++) {
            chunk_t *c = g_ptr_array_index(chunks, cidx);

            if (c->age_iters > 1) {
                old_x_min = fmin(old_x_min, c->x);
                old_x_max = fmax(old_x_max, c->x);
                old_y_min = fmin(old_y_min, c->y);
                old_y_max = fmax(old_y_max, c->y);
                old_cnt ++;
            } 

            all_x_min = fmin(all_x_min, c->x);
            all_x_max = fmax(all_x_max, c->x);
            all_y_min = fmin(all_y_min, c->y);
            all_y_max = fmax(all_y_max, c->y);
            all_cnt ++;
        }

        double vel[2] = {0,0};

        if (old_cnt > 0 && all_cnt > 0) {

            if (1) {
                //double dx = min_magnitude(all_x_max - old_x_max, all_x_min - old_x_min);
                //double dy = min_magnitude(all_y_max - old_y_max, all_y_min - old_y_min);
                double dx = max_magnitude(all_x_max - old_x_max, all_x_min - old_x_min);
                double dy = max_magnitude(all_y_max - old_y_max, all_y_min - old_y_min);
                vel[0] = dx / dt;
                vel[1] = dy / dt;
            } else {
                double all_cxy[] = { (all_x_max + all_x_min) / 2.0,
                                     (all_y_max + all_y_min) / 2.0 };
                double old_cxy[] = { (old_x_max + old_x_min) / 2.0,
                                     (old_y_max + old_y_min) / 2.0 };
                
                vel[0] = (all_cxy[0] - old_cxy[0]) / dt;
                vel[1] = (all_cxy[1] - old_cxy[1]) / dt;
            }
        }

        // in all other cases, velocity = 0.

        double track_vel[2] = {0,0};
        double track_pos[2] = {0,0};
        int    track_vel_cnt = 0;
        int    track_pos_cnt = 0;

        // update all the chunks.
        for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(chunks); cidx++) {
            chunk_t *c = g_ptr_array_index(chunks, cidx);

            //            double weight = 0.1;
            double weight = 1.0 / (c->vel_samples + 1);
            c->vel[0] = (1.0 - weight) * c->vel[0] + weight * vel[0];
            c->vel[1] = (1.0 - weight) * c->vel[1] + weight * vel[1];
            if (c->vel_samples < CHUNK_MAX_SAMPLES)
                c->vel_samples++;

            track_vel[0] += c->vel[0] * c->vel_samples;
            track_vel[1] += c->vel[1] * c->vel_samples;
            track_vel_cnt += c->vel_samples;

            track_pos[0] += c->x * c->vel_samples;
            track_pos[1] += c->y * c->vel_samples;
            track_pos_cnt += c->vel_samples;
        }

        assert(track_vel_cnt > 0);
        t->vel[0] = track_vel[0] / track_vel_cnt;
        t->vel[1] = track_vel[1] / track_vel_cnt;
        t->vmag = sqrt(bot_sq(t->vel[0]) + bot_sq(t->vel[1]));

        assert(track_pos_cnt > 0);
        t->pos[0] = track_pos[0] / track_pos_cnt;
        t->pos[1] = track_pos[1] / track_pos_cnt;
    }

    // render chunks via LCGL, color-coded according to otrack id.
    if (state->lcmgl_chunks) {
        bot_lcmgl_t *lcmgl = state->lcmgl_chunks;
        lcmglLineWidth(1);
        lcmglEnable(GL_BLEND);
        
        for (unsigned int tidx = 0; tidx < bot_g_ptr_array_size(state->otracks); tidx++) {
            otrack_t *t = g_ptr_array_index(state->otracks, tidx);

            lcmglColor4fv(t->color);
            //            lcmglColor4f(.5, .5, .5, .5);

            for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(t->chunks); cidx++) {
                chunk_t *c = g_ptr_array_index(t->chunks, cidx);
                
                double xyz[3];
                float dim[3];
                
                xyz[0] = c->x; 
                xyz[1] = c->y; 
                xyz[2] = pos[2];
                dim[0] = fmax(c->x_max - c->x_min, MIN_LCMGL_CHUNK_SIZE); 
                dim[1] = fmax(c->y_max - c->y_min, MIN_LCMGL_CHUNK_SIZE);
                dim[2] = c->age_iters == 1 ? 0.2 : 0.0; // t->mature ? 1.0 : 0;
                
                lcmglBox(xyz, dim);
 
                // velocity tails
                if (0) {
                    lcmglBegin(GL_LINES);
                    double alpha = 1;
                    lcmglVertex3d(c->x, c->y, pos[2]);
                    lcmglVertex3d(c->x + alpha*c->vel[0], c->y + alpha*c->vel[1], pos[2]);
                    lcmglEnd();
                }

                // weight labels
                if (0) {
                    char buf[128];
                    sprintf(buf, "%f", c->weight);
                    double p[] = { t->pos[0], t->pos[1], pos[2] };
                    bot_lcmgl_text(lcmgl, p, buf);
                }

            }

            if (0 && t->vmag > 1 && t->maturity > 3) {
                char buf[128];
                sprintf(buf, "%5.2f\n%d", t->vmag, t->maturity);
                double p[] = { t->pos[0], t->pos[1], pos[2] };
                bot_lcmgl_text(lcmgl, p, buf);
            }

            if (0) {
                lcmglColor3f(1,1,1);
                lcmglBegin(GL_LINES);
                double alpha = 1;
                lcmglVertex3d(t->pos[0], t->pos[1], pos[2]);
                lcmglVertex3d(t->pos[0] + alpha*t->vel[0], t->pos[1] + alpha*t->vel[1], pos[2]);
                lcmglEnd();
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////
    // compute statistics on how our sensors are doing.
    if (0) {
        int THETA_BUCKETS = 360;
        double min_range[THETA_BUCKETS];
        double min_reported_range[THETA_BUCKETS];
        double min_sick[THETA_BUCKETS];
        chunk_t *min_chunks[THETA_BUCKETS];

        for (int i = 0; i < THETA_BUCKETS; i++) {
            min_range[i] = 120;
            min_reported_range[i] = 120;
            min_sick[i] = 120;
            min_chunks[i] = NULL;
        }

        // put the minimum range to an obstacle for each theta.
        for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(state->chunks); cidx++) {
            chunk_t *c = g_ptr_array_index(state->chunks, cidx);
            
            double dist = sqrt(bot_sq(pos[0] - c->x) + bot_sq(pos[1] - c->y));
            
            double theta0 = bot_mod2pi_ref(M_PI, atan2(c->y - pos[1], c->x - pos[0]));
            int idx = bot_theta_to_int(theta0, THETA_BUCKETS);
            if (dist < min_range[idx]) {
                min_range[idx] = dist;
                min_chunks[idx] = c;
            }

            if (dist < min_reported_range[idx] && c->reported > 1) {
                min_reported_range[idx] = dist;
            }

            if (c->reported>0) {
                if (c->modality_flags&MODALITY_PLANAR_LIDAR)
                    min_sick[idx] = fmin(min_sick[idx], dist);
            }
        }

        for (int i = 0; i < THETA_BUCKETS; i++) {

            if (min_chunks[i]==NULL || (i>0 && min_chunks[i]==min_chunks[i-1]))
                continue;
            
            if (min_chunks[i]->reported>10) {// && fabs(min_range[i]-min_reported_range[i])>0.5) {
                min_chunks[i]->do_stats_on_me = 1;
                //                printf("%f %d\n", min_range[i], min_chunks[i]->modality_flags);
            }
        }
        
    }

    /////////////////////////////////////////////////////////////////////////
    // compute min free space.
    if (state->do_proximity) {
        // location of center of robot from body frame origin (where the center of the prox field is for angle measurements) 
        double seat[2]={-0.0,0.0};
        double ctheta,stheta, pos_at_seat[2];
        bot_fasttrig_sincos(heading,&ctheta,&stheta);
        pos_at_seat[0]=pos[0]+ctheta*seat[0]+stheta*seat[1];
        pos_at_seat[1]=pos[1]-stheta*seat[0]+ctheta*seat[1];

        int THETA_BUCKETS = 360;
        float min_range[THETA_BUCKETS];
        
        for (int i = 0; i < THETA_BUCKETS; i++)
            min_range[i] = 30;
        
        // put the minimum range to an obstacle for each theta into cull_range.
        for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(state->chunks); cidx++) {
            chunk_t *c = g_ptr_array_index(state->chunks, cidx);
            
            double radius = fmax(c->x_max - c->x_min, c->y_max - c->y_min) / 2.0;
            double dist = sqrt(bot_sq(pos_at_seat[0] - c->x) + bot_sq(pos_at_seat[1] - c->y));
            
            double theta0 = bot_mod2pi_ref(M_PI, -heading+atan2(c->y - pos_at_seat[1], c->x - pos_at_seat[0]));
            double dtheta = radius / dist;
            
            for (double theta = theta0 - dtheta; theta <= theta0 + dtheta; theta += bot_to_radians(0.5)) {
                int idx = bot_theta_to_int(theta, THETA_BUCKETS);
                min_range[idx] = fmin(min_range[idx], dist);
            }
        }

        // dialate the cull contour.
        float min_range_d[THETA_BUCKETS];
        memcpy(min_range_d, min_range, sizeof(min_range));
	
        
        // dialate the minimum ranges around the \hat{theta}
        // direction. 
        double my_min_range = 0.01;
        for (int i = 0; i < THETA_BUCKETS; i++) {
            double theta0 = (2*M_PI * i / THETA_BUCKETS);
            double dtheta = bot_to_radians(10);
            for (double theta = theta0 - dtheta; theta <= theta0 + dtheta; theta += bot_to_radians(0.5)) {
                int idx = bot_theta_to_int(theta, THETA_BUCKETS);
                min_range_d[idx] = fmax(min_range_d[idx], 
                                        fmin(min_range[i], my_min_range / fabs(theta - theta0)));
            }
        }
        // find overall min
        float overall_min_range=1000;
        float overall_min_theta=0;
        for (double theta = bot_to_radians(30) ; theta <= bot_to_radians(129); theta += bot_to_radians(0.5)) {
            int idx = bot_theta_to_int(theta, THETA_BUCKETS);
            if (min_range_d[idx]<overall_min_range)  {
                overall_min_range = min_range_d[idx];
                overall_min_theta=theta;
            }
            idx = bot_theta_to_int(-theta, THETA_BUCKETS);
            if (min_range_d[idx]<overall_min_range)  {
                overall_min_range = min_range_d[idx];
                overall_min_theta=-theta;
            }   
        }
        for (double theta = bot_to_radians(130) ; theta <= bot_to_radians(180); theta += bot_to_radians(0.5)) {
            int idx = bot_theta_to_int(theta, THETA_BUCKETS);
            if (min_range_d[idx]<overall_min_range)  {
                overall_min_range = min_range_d[idx]-1.0;
                overall_min_theta=theta;
            }
            idx = bot_theta_to_int(-theta, THETA_BUCKETS);
            if (min_range_d[idx]<overall_min_range)  {
                overall_min_range = min_range_d[idx]-1.0;
                overall_min_theta=-theta;
            } 
        }  

        if (state->lcmgl_proximity) {
            bot_lcmgl_t *lcmgl = state->lcmgl_proximity;
            // draw it
            //lcmglColor4f(0.3, 0.3, 1, .5);
            lcmglPointSize(8);
            lcmglEnable(GL_BLEND);
            lcmglBegin(GL_POINTS);
            //lcmglBegin(GL_POINTS);
            lcmglColor4f(1, 0, 0, .9);
            
            lcmglVertex3d(pos_at_seat[0] ,
                          pos_at_seat[1] ,
                          0);
            lcmglVertex3d(pos_at_seat[0] + cos(overall_min_theta+heading)*overall_min_range,
                          pos_at_seat[1] + sin(overall_min_theta+heading)*overall_min_range,
                          0);
            lcmglEnd();
        }

        if (state->verbose && (overall_min_range<3) )
            printf("overall_min:%f\n",overall_min_range);
       
        // dialate in the \hat{r} direction.
        //for (int i = 0; i < THETA_BUCKETS; i++) 
        //    min_range_d[i] += state->cull_range;
        
        memcpy(min_range, min_range_d, sizeof(min_range_d));

        if (state->lcmgl_proximity) {
            bot_lcmgl_t *lcmgl = state->lcmgl_proximity;
            // draw it
            //lcmglColor4f(0.3, 0.3, 1, .5);
            lcmglLineWidth(4);
            lcmglEnable(GL_BLEND);
            lcmglBegin(GL_LINE_LOOP);
            //lcmglBegin(GL_POINTS);
            for (int i = 0; i < THETA_BUCKETS; i++) {
                double theta = heading+2*M_PI * i / THETA_BUCKETS;
                float *rgb = bot_color_util_jet(3.0/(0.01+min_range[i]));
                lcmglColor4f(rgb[0], rgb[1], rgb[2], .5);
                
                lcmglVertex3d(pos_at_seat[0] + cos(theta)*min_range[i],
                              pos_at_seat[1] + sin(theta)*min_range[i],
                              0);
            }
            lcmglEnd();
        }
        bot_core_planar_lidar_t proximity;
        proximity.utime = bot_timestamp_now();
        proximity.rad0 = 0;
        proximity.radstep = 2*M_PI/(float)THETA_BUCKETS;
        
        proximity.nranges = THETA_BUCKETS;
        proximity.ranges = min_range;
        proximity.nintensities = 0;
        proximity.intensities = NULL;
        bot_core_planar_lidar_t_publish(state->lcm, "OBSTACLE_PROXIMITY", &proximity);
    }
    
    /////////////////////////////////////////////////////////////////////////
    // compute cull contour
    int THETA_BUCKETS = 360;
    double cull_range[THETA_BUCKETS];
    
    for (int i = 0; i < THETA_BUCKETS; i++)
        cull_range[i] = 120;
    
    // put the minimum range to an obstacle for each theta into cull_range.
    for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(state->chunks); cidx++) {
        chunk_t *c = g_ptr_array_index(state->chunks, cidx);

        double radius = fmax(c->x_max - c->x_min, c->y_max - c->y_min) / 2.0;
        double dist = sqrt(bot_sq(pos[0] - c->x) + bot_sq(pos[1] - c->y));

        double theta0 = bot_mod2pi_ref(M_PI, atan2(c->y - pos[1], c->x - pos[0]));
        double dtheta = radius / dist;

        for (double theta = theta0 - dtheta; theta <= theta0 + dtheta; theta += bot_to_radians(0.5)) {
            int idx = bot_theta_to_int(theta, THETA_BUCKETS);
            cull_range[idx] = fmin(cull_range[idx], dist);
        }
    }

    // dialate the cull contour.
    double cull_range_d[THETA_BUCKETS];
    
    memcpy(cull_range_d, cull_range, sizeof(cull_range));

    // dialate the minimum ranges around the \hat{theta}
    // direction. 
    for (int i = 0; i < THETA_BUCKETS; i++) {
        double theta0 = 2*M_PI * i / THETA_BUCKETS;
        double dtheta = bot_to_radians(10);
        for (double theta = theta0 - dtheta; theta <= theta0 + dtheta; theta += bot_to_radians(0.5)) {
            int idx = bot_theta_to_int(theta, THETA_BUCKETS);
            cull_range_d[idx] = fmax(cull_range_d[idx], 
                                     fmin(cull_range[i], state->cull_range / fabs(theta - theta0)));
        }
    }

    // dialate in the \hat{r} direction.
    for (int i = 0; i < THETA_BUCKETS; i++) 
        cull_range_d[i] += state->cull_range;
    
    memcpy(cull_range, cull_range_d, sizeof(cull_range_d));
    
    if (state->lcmgl_cull) {
        bot_lcmgl_t *lcmgl = state->lcmgl_cull;
        // draw it
        lcmglColor4f(1, 1, .25, .5);
        lcmglLineWidth(4);
        lcmglEnable(GL_BLEND);
        lcmglBegin(GL_LINE_LOOP);
        for (int i = 0; i < THETA_BUCKETS; i++) {
            double theta = 2*M_PI * i / THETA_BUCKETS;
            lcmglVertex3d(pos[0] + cos(theta)*cull_range[i],
                          pos[1] + sin(theta)*cull_range[i],
                          0);
        }
        lcmglEnd();
    }


    /////////////////////////////////////////////////////////////////////////
    // publish
    erlcm_rect_list_t rects;
    memset(&rects, 0, sizeof(erlcm_rect_list_t));
    rects.utime = utime;
    rects.xy[0] = pos[0];
    rects.xy[1] = pos[1];
    rects.num_rects = 0;
    rects.rects = (erlcm_rect_t*) calloc(bot_g_ptr_array_size(state->chunks), sizeof(erlcm_rect_t));
    
    erlcm_track_list_t track_list;
    memset(&track_list, 0, sizeof(erlcm_track_list_t));
    track_list.utime = utime;
    track_list.tracks = (erlcm_track_t*) calloc(bot_g_ptr_array_size(state->otracks), sizeof(erlcm_track_t));
    
    
    erlcm_person_status_t *person_tracks = NULL;
    int p_count = 0;

    for (unsigned int tidx = 0; tidx < bot_g_ptr_array_size(state->otracks); tidx++) {
        otrack_t *t = g_ptr_array_index(state->otracks, tidx);
        if (bot_g_ptr_array_size(t->chunks) == 0)
            continue;
        
        double theta = atan2(t->vel[1], t->vel[0]);
        box_from_chunks(t->chunks, theta, &t->box);
        //box_minimum_box_from_chunks(t->chunks, &t->box);
        
        int use_track = 0, use_rects = 1;
        int add_2_pedestrians=0;
        
        
        if (t->vmag > 0.6 && t->maturity > 10)        
            use_track = 1;
        
        // too fast likely to be noise 
        if (t->vmag > 2) {
            use_track = 0;
        }
        
        // remove long objects

        //change the above - if need to cut down tracks on other objects 
        double MAX_DIM = 1.0;//2;
        if (t->box.size[0] > MAX_DIM || t->box.size[1] > MAX_DIM)
            use_track = 0;

        double MIN_DIM = 0.2;
        if (t->box.size[0] < MIN_DIM && t->box.size[1] < MIN_DIM)
            use_track = 0;

        
            
        // pedestrians: include based on size
        const double PED_MAX_DIM = 1.0; // max size meters
        if (t->box.size[0] < PED_MAX_DIM && t->box.size[1] < PED_MAX_DIM) {
            if (t->maturity > 3) 
                add_2_pedestrians = state->do_pedestrians;
        }
        // if pedestrian is moving they can cause the object to smear in x direction.
        if (t->maturity > 6 && t->box.size[0] > PED_MAX_DIM && t->box.size[0] < 2*PED_MAX_DIM &&
            t->box.size[1] < PED_MAX_DIM && t->vmag > 0.6) {
            add_2_pedestrians = state->do_pedestrians;
        }
        
        // pedestrians: cull based on velocity 
        const double PED_MAX_VEL = 2.0;// max vel meters /sec
        if (t->vmag > PED_MAX_VEL) {
            add_2_pedestrians = 0;
        }
        
        // pedestrians: cull based on range
        if (add_2_pedestrians) {
            double range = sqrt(bot_sq(t->box.pos[0] - pos[0]) + bot_sq(t->box.pos[1] - pos[1]));          
            const double PED_MAX_RANGE= 5.0;
            if (range > PED_MAX_RANGE)
                add_2_pedestrians=0;
        }

        if (add_2_pedestrians) {
            
            use_track = 1;
            
            //lets publish as people list 
            person_tracks = (erlcm_person_status_t *) realloc(person_tracks, 
                                                               (p_count + 1) * sizeof(erlcm_person_status_t));
            person_tracks[p_count].id = p_count; 
            person_tracks[p_count].last_obs =  0;
            
            //this is not in the local frame
            
            person_tracks[p_count].pos[0] = t->box.pos[0] - pos[0];
            person_tracks[p_count].pos[1] = t->box.pos[1] - pos[1];
            person_tracks[p_count].closing_velocity = 0;  //need to calculate this 
            person_tracks[p_count].trans_vel_mag = t->vmag; 
            person_tracks[p_count].rot_vel_mag = 0;
            person_tracks[p_count].person_heading = t->box.theta; //convert the heading to local   
            
            p_count++;
        }
        
        /*if (add_2_pedestrians) { */
/*             erlcm_object_t *obj=&object_list.objects[object_list.num_objects++]; */
/*             obj->id=t->id; */
/*             obj->pos[0]=t->box.pos[0]; */
/*             obj->pos[1]=t->box.pos[1]; */
/*             obj->pos[2]=pos[2]; */
            
/*             //double rpy[3] = {0.0,0.0,t->box.theta}; */
/*             double th = t->box.theta; */
/*             // If velocities is small make pedestrians face robot */
/*             if (t->vmag>0.3) */
/*                 th = atan2(t->vel[1],t->vel[0]); */
/*             else  */
/*                 th = atan2(pos[1]-t->pos[1],pos[0]-t->pos[0]); */
/*             double rpy[3] = {0.0,0.0,th}; */
/*             bot_roll_pitch_yaw_to_quat(rpy, obj->orientation); */
            
/*             obj->bbox_min[0]=-t->box.size[0]/2.0; */
/*             obj->bbox_min[1]=-t->box.size[1]/2.0; */
/*             obj->bbox_min[2]=0.0;//t->box.size[2]/2.0; */
/*             obj->bbox_max[0]=t->box.size[0]/2.0; */
/*             obj->bbox_max[1]=t->box.size[1]/2.0; */
/*             obj->bbox_max[2]=2.0;//t->box.size[2]/2.0; */
/*             obj->type = ERLCM_OBJECT_T_TYPE_PEDESTRIAN; */
/*         } */
                
        if (state->do_tracks&&use_track) {
            erlcm_track_t *tt = &track_list.tracks[track_list.ntracks];
            tt->id = t->id;

            if (0) {
                // cull based on culling polygon
                double theta = atan2(t->box.pos[1] - pos[1], t->box.pos[0] - pos[0]);
                int idx = bot_theta_to_int(theta, THETA_BUCKETS);
                double range = sqrt(bot_sq(t->box.pos[0] - pos[0]) + bot_sq(t->box.pos[1] - pos[1]));
                
                if (range > cull_range[idx])
                    continue;
            }

            memcpy(tt->pos, t->box.pos, 2 * sizeof(double));
            memcpy(tt->vel, t->vel, 2 * sizeof(double));
            memcpy(tt->size, t->box.size, 2 * sizeof(double));
            tt->size_good = 1;
            tt->theta = t->box.theta;
            tt->confidence = 2;
            track_list.ntracks++;
        } 

        if (use_rects) {
            for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(t->chunks); cidx++) {
                chunk_t *c = g_ptr_array_index(t->chunks, cidx);

                double theta = atan2(c->y - pos[1], c->x - pos[0]);
                int idx = bot_theta_to_int(theta, THETA_BUCKETS);
                double range = sqrt(bot_sq(c->x - pos[0]) + bot_sq(c->y - pos[1]));
        
                if (range > cull_range[idx])
                    continue;
                
                if (c->seen_iters < MIN_SEEN_ITERS)
                    continue;

                /*                int static_value = gridmap_get_value(state->static_map, c->x, c->y, 0);
                                  if (static_value < 6)
                                  continue;
                */

                c->reported++;
 
                rects.rects[rects.num_rects].dxy[0] = c->x - pos[0];
                rects.rects[rects.num_rects].dxy[1] = c->y - pos[1];
                rects.rects[rects.num_rects].size[0] = c->x_max - c->x_min;
                rects.rects[rects.num_rects].size[1] = c->y_max - c->y_min;
                rects.rects[rects.num_rects].theta = 0;
                rects.num_rects++;

                if (0) {
                    double xyz[] = {c->x, c->y, pos[2]};
                    char buf[128];
                    sprintf(buf,"%d", c->seen_iters);
                    if (state->lcmgl_hits)
                        bot_lcmgl_text(state->lcmgl_hits, xyz, buf);
                }
            }
        }
    }

    erlcm_obstacle_list_t olist;
    memcpy (&olist.rects, &rects, sizeof (erlcm_rect_list_t));
    memcpy (&olist.tracks, &track_list, sizeof (erlcm_track_list_t));
    erlcm_obstacle_list_t_publish (state->lcm, "OBSTACLES", &olist);

    //fprintf(stderr,"People count : %d\n", p_count);

    if(p_count > 0){
        erlcm_people_pos_msg_t p_list;
        p_list.utime = bot_timestamp_now();

        p_list.people_pos = person_tracks; 
    
        p_list.num_people = p_count;//s->people_list.no_people;
        p_list.followed_person = -1;//remapped_id;//s->people_list.active_person_ind;
  
        erlcm_people_pos_msg_t_publish(state->lcm,"PEOPLE_LIST",&p_list);
        free(person_tracks);
    }
    free(track_list.tracks);
    free(rects.rects);

    /*
      int profile_iters = 500;
      cnt++;
      if ((cnt % 100) == 0)
      printf("profile: %5d / %5d\n", cnt, profile_iters);
      if (cnt == profile_iters)
      exit(0);
    */

    if (state->lcmgl_global_mask) {
        bot_lcmgl_t *lcmgl = state->lcmgl_global_mask;
        lcmglColor4f(0,0,.5,.3);
        draw_mask(state, lcmgl, state->global_mask_forward, state->global_mask_backward, state->global_mask_width);
    }

    // show persistence mask
    if (state->lcmgl_persist_mask) {
        bot_lcmgl_t *lcmgl = state->lcmgl_persist_mask;
        lcmglColor4f(1,0,1,.2);
        draw_mask(state, lcmgl, state->persist_mask_forward, state->persist_mask_backward, state->persist_mask_width);
    }
    
    for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(state->chunks); cidx++) {
        chunk_t *c = g_ptr_array_index(state->chunks, cidx);
        c->weight = 0;
    }

    /////////////////////////////////////////////////////////////////////////
    // cleanup
    for (GList *seg_iter=segments; seg_iter; seg_iter=seg_iter->next) {
        GPtrArray *chunks = seg_iter->data;
        g_ptr_array_free(chunks, TRUE);
    }
    g_list_free(segments);

    if (state->lcmgl_chunks) 
        bot_lcmgl_switch_buffer(state->lcmgl_chunks);
    if (state->lcmgl_proximity) 
        bot_lcmgl_switch_buffer(state->lcmgl_proximity);
    if (state->lcmgl_cull) 
        bot_lcmgl_switch_buffer(state->lcmgl_cull);
    if (state->lcmgl_persist_mask) 
        bot_lcmgl_switch_buffer(state->lcmgl_persist_mask);
    bot_lcmgl_t *mylcmgl = bot_lcmgl_init (state->lcm, "obstacles-bbox");//globals_get_lcmgl("obstacles-bbox",0);
    if (mylcmgl)
        bot_lcmgl_switch_buffer(mylcmgl);
}

static void process_hit(state_t *state, hit_t *h)
{
    if (state->verbose) 
        printf("obstacles: process_hit()\n");

    // detect log seeks
    double dt = (h->utime - state->last_utime)/1000000.0;
    state->last_utime = h->utime;
    
    if (fabs(dt) > 4.0) {
        printf("RESET: time warp of %f seconds\n", dt);
        reset(state);
        hit_free(h);
        return;
    }
    
    // update/create a chunk
    double x = h->xyz[0], y = h->xyz[1];
    
    gridder_iterator_t git;
    gridder_iterator_init(state->chunks_gridder, &git,
                          x - MAX_CHUNK_DIMENSION,
                          x + MAX_CHUNK_DIMENSION,                          
                          y - MAX_CHUNK_DIMENSION,
                          y + MAX_CHUNK_DIMENSION);
    
    chunk_t *c, *best_chunk = NULL;
    double bestdstsq = bot_sq(MAX_CHUNK_DIMENSION);
    while ((c = gridder_iterator_next(&git)) != NULL) {
        double dstsq = bot_sq(c->x - x) + bot_sq(c->y - y);
        if (dstsq < bestdstsq) {
            best_chunk = c;
            bestdstsq = dstsq;
        }
    }
    
    if (!best_chunk) {
        best_chunk = chunk_alloc(state);
        gridder_add(state->chunks_gridder, x, y, best_chunk);
        g_ptr_array_add(state->chunks, best_chunk);
    }
    
    double pos[3];

    frames_vehicle_pos_local(state->frames, pos);
            
    if ((best_chunk->modality_flags&h->modality_flag)==0) {
        best_chunk->modality_flags |= h->modality_flag;

        if (1) {
            double range = sqrt(bot_sq(h->xyz[0]-pos[0]) + bot_sq(h->xyz[1]-pos[1]));

            if (h->modality_flag==1)
                best_chunk->sick_range = fmin(best_chunk->sick_range, range);

        }
    }


    best_chunk->x_max = fmax(best_chunk->x_max, x);
    best_chunk->x_min = fmin(best_chunk->x_min, x);
    best_chunk->y_max = fmax(best_chunk->y_max, y);
    best_chunk->y_min = fmin(best_chunk->y_min, y);
    best_chunk->x = (best_chunk->x_min + best_chunk->x_max) / 2.0;
    best_chunk->y = (best_chunk->y_min + best_chunk->y_max) / 2.0;
    best_chunk->utime = h->utime;
    best_chunk->weight += h->weight;
    
    hit_free(h);
    
    // time to update tracks?
    double otrack_dt = (h->utime - state->last_otrack_utime) / 1000000.0;
    
    
    if (otrack_dt >= 1.0 / OTRACK_HZ || (otrack_dt > 0.049)) {
        state->last_otrack_utime = h->utime;
        
        // actually track.
        otrack(state, h->utime, otrack_dt);
        state->ntrack_updates++;
        
        // rebuild gridder
        gridder_reset(state->chunks_gridder);
        gridder_set_origin(state->chunks_gridder, pos[0], pos[1]);
        for (unsigned int cidx = 0; cidx < bot_g_ptr_array_size(state->chunks); cidx++) {
            chunk_t *c = g_ptr_array_index(state->chunks, cidx);
            gridder_add(state->chunks_gridder, c->x, c->y, c);
        }
        
        int64_t now = bot_timestamp_now();
        double report_dt = (now - state->last_report_utime) / 1000000.0;
        if (report_dt > 1) {
            printf("SUMMARY: nchunks=%5d ntracks=%5d tracking at %5.1f Hz, %d tracking drops\n", 
                   bot_g_ptr_array_size(state->chunks),
                   bot_g_ptr_array_size(state->otracks),
                   state->ntrack_updates / report_dt,
                   state->ntrack_drops);
            state->last_report_utime = now;
            state->ntrack_updates = 0;
        }
    }
}

static void *track_work_thread(void *user)
{
    state_t *state = (state_t*) user;

    if (state->verbose) 
        printf("obstacles: track_work_thread()\n");

    while (1) {

        GPtrArray *hits = g_async_queue_pop(state->hits_queue);
        //fprintf(stderr, "Array Size : %d\n", bot_g_ptr_array_size(hits));
        for (int hidx = 0; hidx < bot_g_ptr_array_size(hits); hidx++) {
            hit_t *h = g_ptr_array_index(hits, hidx);
            //fprintf(stderr,"Working thread\n");
            process_hit(state, h);
        }

        g_ptr_array_free(hits, TRUE);
    }

    if (state->verbose) 
        printf("obstacles: track_work_thread() done.\n");

    return NULL;
}


static void usage()
{
    fprintf (stderr, "usage: ar-obstacles [options]\n"
             "\n"
             "  -h, --help              Shows this help text and exits\n"
             "  -v, --verbose           be verbose\n"
             "  -s, --sick              Use sick. This is on by defaut.\n"
             "  -c, --cull              Cull objects N meters behind other objects. (0=don't cull)\n"
             "    , --proximity          Create free space proximity. \n"
             "  -p, --pedestrians       Publish pedestrian object. \n"
             "  -n, --notracks          Suppress moving tracks. \n"
             "  -f, --failsafe          Override failsafe level\n"
             "      --show-hits         Show all hits (this is generally a bad idea)\n"
             "      --show-chunks       Show chunks\n"
             "      --show-cull         Show the culling proximity\n"
             "      --show-global-mask  Show the global mask\n"
             "      --show-persist-mask Show the persistence mask\n" 
             "      --show-sick-hits    Show sick hits\n"
             "      --show-sick-mask Show sick mask\n"
             "      --show-proximity    Show the minimun proximity\n"
             );
}




int main(int argc, char *argv[])
{
    g_thread_init(NULL);
    setlinebuf (stdout);
    bot_fasttrig_init();
    otracks_init();

    state_t *state = (state_t*) calloc(1, sizeof(state_t));


    char *optstring = "hvsVc:f:12345678pPn";
    state->use_sick=1;
    state->do_tracks=1;
    
    //maybe we need a global file 
    
    state->lcm = bot_lcm_get_global(NULL);//lcm_create(NULL);//globals_get_lcm();

    bot_glib_mainloop_attach_lcm (state->lcm);

    struct option long_opts[] = {
        {"help", no_argument, 0, 'h'},
        {"verbose", no_argument, 0, 'v'},
        {"sick", no_argument, 0, 's'},
        {"cull", required_argument, 0, 'c'},
        {"notracks", required_argument, 0, 'n'},
        {"proximity", no_argument, 0, 'P'},
        {"pedestrians", no_argument, 0, 'p'},
        {"failsafe", required_argument, 0, 'f'},
        {"show-hits", no_argument, 0, '1'},
        {"show-chunks", no_argument, 0, '2'},
        {"show-cull", no_argument, 0, '3'},
        {"show-global-mask", no_argument, 0, '4'},
        {"show-persist-mask", no_argument, 0, '5'},
        {"show-sick-hits", no_argument, 0, '6'},
        {"show-sick-masks", no_argument, 0, '7'},
        {"show-proximity", no_argument, 0, '8'},
        {0, 0, 0, 0}};

    char c;
    int override_failsafe=0;
    while ((c = getopt_long (argc, argv, optstring, long_opts, 0)) >= 0)
        {
            switch (c) 
                {
                    break;
                case 'v':
                    state->verbose = 1;
                    break;
                case 's':
                    state->use_sick = 1;
                    break;
                case 'c':
                    state->cull_range = strtod(optarg,0);
                    break;
                case 'P':
                    state->do_proximity_faults = 1;
                    break;
                case 'p':
                    state->do_pedestrians = 1;
                    state->do_proximity = 1;
                    break;
                case 'n':
                    state->do_tracks = 0;
                    break;
                case 'f':
                    state->failsafe = atoi(optarg);
                    override_failsafe=1;
                    break;
                case '1':
                    state->lcmgl_hits =  bot_lcmgl_init (state->lcm, "obstacles-hits");
                    break;
                case '2':
                    state->lcmgl_chunks =  bot_lcmgl_init (state->lcm, "obstacles-chunks");
                    break;
                case '3':
                    state->lcmgl_cull =  bot_lcmgl_init (state->lcm, "obstacles-cull");
                    break;
                case '4':
                    state->lcmgl_global_mask = bot_lcmgl_init (state->lcm, "obstacles-global-mask");
                    break;
                case '5':
                    state->lcmgl_persist_mask = bot_lcmgl_init (state->lcm, "obstacles-persist-mask");
                    break;
                case '6':
                    state->show_sick_hits = 1;
                    break;
                case '7':
                    state->show_sick_masks = 1;
                    break;
                case '8':
                    state->lcmgl_proximity =  bot_lcmgl_init (state->lcm, "obstacles-proximity");
                    break;
                case 'h':
                default:
                    usage();
                    return 1;
                }
        }


    state->param = bot_param_new_from_server(state->lcm, 1);//globals_get_config();
    state->frames = bot_frames_get_global(state->lcm, state->param);
    state->chunks = g_ptr_array_new();
    state->chunks_gridder = gridder_create(-GRIDDER_RANGE, GRIDDER_RANGE, -GRIDDER_RANGE, GRIDDER_RANGE,
                                           GRIDDER_RESOLUTION,
                                           offsetof(chunk_t, gd_data));
    
    state->otracks = g_ptr_array_new();
    state->otracks_hashtable = g_hash_table_new(bot_pint64_hash, bot_pint64_equal);
    state->otracks_gridder = gridder_create(-GRIDDER_RANGE, GRIDDER_RANGE, -GRIDDER_RANGE, GRIDDER_RANGE,
                                            GRIDDER_RESOLUTION,
                                            offsetof(chunk_t, gd_data));
    
    //    state->static_map = gridmap_create(0, 0, 2*GRIDDER_RANGE, 2*GRIDDER_RANGE, 0.3);

    if (state->use_sick) {
        printf("Enabling SICK front end...\n");
        int res = sick_init(state);
        if (res)
            printf("\tSick ERROR %i\n", res);
    }

    state->global_mask_forward = bot_param_get_double_or_fail(state->param, "obstacles.global_mask_forward");
    state->global_mask_backward = bot_param_get_double_or_fail(state->param, "obstacles.global_mask_backward");
    state->global_mask_width = bot_param_get_double_or_fail(state->param, "obstacles.global_mask_width");

    state->persist_mask_forward = bot_param_get_double_or_fail(state->param, "obstacles.chunk_persist_mask_forward");
    state->persist_mask_backward = bot_param_get_double_or_fail(state->param, "obstacles.chunk_persist_mask_backward");
    state->persist_mask_width = bot_param_get_double_or_fail(state->param, "obstacles.chunk_persist_mask_width");
    state->persist_mask_expire_sec = bot_param_get_double_or_fail(state->param, "obstacles.chunk_persist_mask_expire_sec");
    state->outside_persist_mask_expire_sec = bot_param_get_double_or_fail(state->param, "obstacles.chunk_outside_persist_mask_expire_sec");
    state->too_close = bot_param_get_double_or_fail(state->param, "obstacles.too_close");

    // Derive footprint parameters from vehicle bounds.
    
    if ((bot_param_get_double_array(state->param, "calibration.vehicle_bounds.front_left", state->front_left, 2) != 2) ||
        (bot_param_get_double_array(state->param, "calibration.vehicle_bounds.front_right", state->front_right, 2) != 2) ||
        (bot_param_get_double_array(state->param, "calibration.vehicle_bounds.rear_left", state->rear_left, 2) != 2) ||
        (bot_param_get_double_array(state->param, "calibration.vehicle_bounds.rear_right", state->rear_right, 2) != 2)){
        fprintf(stderr,"Error Getting Robot footprint\n");
    }
    
    if (state->cull_range == 0)
        state->cull_range = HUGE;

    bot_core_pose_t_subscribe(state->lcm, "POSE", on_pose, state);

    if (!override_failsafe)
        erlcm_failsafe_t_subscribe(state->lcm, "FAILSAFE", on_failsafe, state);
    update_failsafe(state, state->failsafe);

    state->hits_queue = g_async_queue_new();
    pthread_create(&state->work_thread, NULL, track_work_thread, state);

    ///////////////////////////////////////////////
    // event loop 
    state->mainloop = g_main_loop_new( NULL, FALSE );
    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
}
 
