#include <stdio.h>
#include <math.h>
#include <assert.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <bot_core/bot_core.h>
#include <bot_lcmgl_client/lcmgl.h>

#include <geom_utils/geometry.h>
#include <geom_utils/convexhull.h>

#include "box.h"
#include "chunks.h"

double box_from_chunks(GPtrArray *chunks, double theta, struct box *box)
{
    double ct, st;
    bot_fasttrig_sincos(-theta, &st, &ct);

    double min_y, min_x, max_y, max_x;
    min_y = min_x = HUGE;
    max_y = max_x = -HUGE;

    assert(bot_g_ptr_array_size(chunks) > 0);
    for (unsigned int i = 0; i < bot_g_ptr_array_size(chunks); i++) {
        chunk_t *c = g_ptr_array_index(chunks, i);

        double ox = ct*c->x - st*c->y; // rotate!
        double oy = st*c->x + ct*c->y;

        min_x = fmin(min_x, ox);
        max_x = fmax(max_x, ox);
        min_y = fmin(min_y, oy);
        max_y = fmax(max_y, oy);
    }
    
    box->size[0] = max_x - min_x;
    box->size[1] = max_y - min_y;

    // where is the center of the box (in the projected space?)
    double cxp = (max_x + min_x)/2;
    double cyp = (max_y + min_y)/2;

    // project the center of the box back into the real coordinate system
    bot_fasttrig_sincos(theta, &st, &ct);
    box->pos[0] = ct*cxp - st*cyp;
    box->pos[1] = st*cxp + ct*cyp;
    
    box->theta = theta;

    // return area
    return box->size[0]*box->size[1];
}

double box_minimum_box_from_chunks(GPtrArray *chunks, struct box *min_box)
{

    // make covex polygon of points. 
    assert(bot_g_ptr_array_size(chunks) > 0);
    int n = bot_g_ptr_array_size(chunks);
    pointlist2d_t * all_pnts = pointlist2d_new (n);

    for (unsigned int i = 0; i < n; i++) {
        chunk_t *c = g_ptr_array_index(chunks, i);
        all_pnts->points[i].x = c->x;
        all_pnts->points[i].y = c->y;
    }
    // optimization optimizshmashum
    pointlist2d_t * perimeter = convexhull_graham_scan_2d (all_pnts);
    pointlist2d_free (all_pnts);
    if (!perimeter) {
        return box_from_chunks(chunks,0.0,min_box);
    }
    // for each edge in polygon find bounding box.
    struct box box;
    double minimum_area=HUGE;
    lcm_t *lcm = bot_lcm_get_global (NULL);
    bot_lcmgl_t *lcmgl = bot_lcmgl_init(lcm, "obstacles-bbox");
    lcmglEnable(GL_BLEND);
    lcmglBegin(GL_LINES);

    for(unsigned int i=0; i< perimeter->npoints-1; i++) {
        double theta = M_PI_2+atan2(perimeter->points[i+1].y-perimeter->points[i].y,perimeter->points[i+1].x-perimeter->points[i].x);
        double area = box_from_chunks(chunks, theta, &box);
        if (area<minimum_area) {
            *min_box = box;
            minimum_area = area;
        }
        lcmglColor4f(1, 0, 0, .9);
        lcmglVertex3d(perimeter->points[i].x,perimeter->points[i].y,0);
    } 
    lcmglEnd();
    //printf("min area:%lf\n",minimum_area);
    pointlist2d_free (perimeter);
    
    return minimum_area;
}
