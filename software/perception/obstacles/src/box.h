#ifndef _BOX_H
#define _BOX_H

#include <glib.h>

struct box
{
    double pos[2];
    double size[2];

    double theta;
};

double box_from_chunks(GPtrArray *chunks, double theta, struct box *box);

double box_minimum_box_from_chunks(GPtrArray *chunks, struct box *box);

#endif
