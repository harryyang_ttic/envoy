#ifndef _OBSTACLES_H
#define _OBSTACLES_H


#include <pthread.h>
#include <glib.h>

#include <lcm/lcm.h>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <er_common/gridmap.h>

#include <lcmtypes/bot_core.h>
#include <lcmtypes/erlcm_robot_status_t.h>

#include "gridder.h"

// Used in DGC to differentiate between Sick (planar) and Velodyne (rotating)
#define MODALITY_PLANAR_LIDAR 1

typedef struct
{
    double   xyz[3];
    double   weight;
    int64_t  utime;
    int      modality_flag;
} hit_t;


typedef struct
{
    GHashTable  *sick_infos;
    GPtrArray   *samples;
    gridder_t   *samples_gridder;
    int64_t      last_cleanup_utime;

    bot_lcmgl_t *lcmgl_sick;
    int64_t      last_lcmgl_utime;
    int          show_hits;

  //double ALWAYS_ACCEPT_GROUND_SLOPE;     // 0.2
  // double ALWAYS_ACCEPT_GROUND_CLEARANCE; // 0.1
  //double ALWAYS_ACCEPT_MAX_RANGE;        // 6
    double BROOM_ALWAYS_ACCEPT_GROUND_SLOPE;     // 0.2
    double BROOM_ALWAYS_ACCEPT_GROUND_CLEARANCE; // 0.1
    double BROOM_ALWAYS_ACCEPT_MAX_RANGE;        // 6
    double SKIRT_ALWAYS_ACCEPT_GROUND_SLOPE;     
    double SKIRT_ALWAYS_ACCEPT_GROUND_CLEARANCE; 
    double SKIRT_ALWAYS_ACCEPT_MAX_RANGE;        

    double MAX_RANGE;
    

    GHashTable *sick_ids;
} sick_state_t;

typedef struct
{
    BotParam  *param;
    lcm_t     *lcm;
    BotFrames *frames;
    GMainLoop *mainloop;
    int verbose;
    int use_sick;
    int use_velodyne;
    int show_hits;
    int show_sick_hits;
    int show_sick_masks;
    bot_lcmgl_t     *lcmgl_hits; 
    bot_lcmgl_t     *lcmgl_chunks; 
    bot_lcmgl_t     *lcmgl_cull; 
    bot_lcmgl_t     *lcmgl_proximity;
    bot_lcmgl_t     *lcmgl_global_mask;
    bot_lcmgl_t     *lcmgl_persist_mask;
    
    int        lcmgl_hit_count;

    sick_state_t     *sick_state;

    int64_t   last_utime;
    int64_t   last_publish_utime;
    int64_t   last_otrack_utime;
    int64_t   last_report_utime;

    int       ntrack_updates;
    int       ntrack_drops;
    double    cull_range;
    int       do_proximity;
    int       do_proximity_faults;
    int       do_pedestrians;
    int       do_tracks;

    gridder_t *chunks_gridder;
    GPtrArray *chunks;

    pthread_t  work_thread;
    GAsyncQueue *hits_queue;

    int       failsafe;
    double    failsafe_timer;
    int64_t   failsafe_utime;

    /////////////////////////////////////
    // below here, all variables belong to TRACK thread, exclusively.
    gridder_t *otracks_gridder;
    GPtrArray *otracks;
    GHashTable *otracks_hashtable;

    bot_core_pose_t    *pose;
    erlcm_robot_status_t    *robot_status;
    double    global_mask_forward, global_mask_backward, global_mask_width;
    double    persist_mask_forward, persist_mask_backward, persist_mask_width;
    double    persist_mask_expire_sec, outside_persist_mask_expire_sec;
    double    too_close;

    double    mask_orientation_vector[2];
    double    mask_pos[3];

    double front_left[2], front_right[2];
    double rear_left[2], rear_right[2];
   
} state_t;

// This function is called whenever we observe something that isn't
hit_t *hit_alloc();
void hit_free(hit_t *h);
void handle_hits(state_t *state, GPtrArray *hits);

//void handle_hit(state_t *state, double xyz[3], int64_t utime, double weight);

// Call this function whenever you receive a message with a new utime
void update_utime(state_t *state, int64_t utime);

void sick_setup(state_t *state);

int sick_init(state_t *state);

#endif
