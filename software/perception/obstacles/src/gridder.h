#ifndef _GRIDDER_H
#define _GRIDDER_H

typedef struct gridder gridder_t;

typedef struct gridder_iterator gridder_iterator_t;

typedef struct gridder_data gridder_data_t;
struct gridder_data
{
    void *next;
};


struct gridder_iterator
{
    const gridder_t *g;
    int lox, loy;
    int hix, hiy;
    int x, y;

    void *lastp; // the last element returned. If not null, we are cdring down a list.
};



/** The gridder NEVER owns the data in it; it merely maintains an
    auxillary data structure that can point to it. Structures managed
    by a gridder must contain a gridder_data element. Multiple
    gridders can contain the same objects by having them use different
    gridder_data elements.
**/


/** Create a new gridder. ptr_offset is the offset of the gridder_data
 * element in your structs. 
**/
gridder_t *gridder_create(double x_min, double x_max, double y_min, double y_max, 
                          double resolution,
                          int ptr_offset);

void gridder_destroy(gridder_t *g);

/** Add a new cell to the grid. Returns 0 on success, -1 if the point is outside the bounds.**/
int gridder_add(gridder_t *g, double x, double y, void *pin);
int gridder_remove(gridder_t *g, double x, double y, void *pin);

/** Initialize a new gridder that will search an area at least as
 * large as requested. The gridder data structure should not be
 * modified during iteration. **/
void gridder_iterator_init(const gridder_t *g, gridder_iterator_t *git, 
                           double x_min, double x_max, double y_min, double y_max);

/** Retrieve the next element, or NULL when done. **/
void *gridder_iterator_next(gridder_iterator_t *git);

/** Empty the gridder by deleting our references to the objects. (The
    underlying objects are not touched. **/
void gridder_reset(gridder_t *g);

/** Move the origin of the gridder. This is only well-defined when the
 * gridder is empty **/
void gridder_set_origin(gridder_t *g, double cx, double cy);

double gridder_origin_x(gridder_t *g);
double gridder_origin_y(gridder_t *g);

#endif
