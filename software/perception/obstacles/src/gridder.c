#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>
#include <string.h>
#include "gridder.h"

typedef void* ptr;

#define to_gridder_data_ptr(g, p) ((gridder_data_t*) (((char*) p) + g->ptr_offset))

struct gridder
{
    double x_max, x_min, y_max, y_min;
    double resolution;
    
    int sx, sy;
    int ptr_offset; // offset of the gridder_data_t in the pointers passed in/out
    ptr **p;
};

static ptr** ptr_array2d_create(int m, int n)
{
    // vs is the underlying storage
    ptr *vs = (ptr*) calloc(m*n, sizeof(ptr));

    // v is the array of row pointers
    ptr **v = (ptr**) calloc(m, sizeof(ptr));
    for (int i = 0; i < m; i++)
        v[i] = &vs[i*n];
    return v;
}

static void ptr_array2d_destroy(ptr **v)
{
    free(v[0]);
    free(v);
}

gridder_t *gridder_create(double x_min, double x_max, double y_min, double y_max, 
                          double resolution,
                          int ptr_offset)
{
    gridder_t *g = (gridder_t*) calloc(1, sizeof(gridder_t));

    g->x_max = x_max;
    g->x_min = x_min;
    g->y_max = y_max;
    g->y_min = y_min;
    g->resolution = resolution;

    g->sx = (x_max - x_min) / resolution + 1;
    g->sy = (y_max - y_min) / resolution + 1;

    g->ptr_offset = ptr_offset;

    g->p = ptr_array2d_create(g->sx, g->sy);

    printf("Gridder allocated %4d x %4d, offset %4d\n", g->sx, g->sy, ptr_offset);
    return g;
}

void gridder_destroy(gridder_t *g)
{
    ptr_array2d_destroy(g->p);
    free(g);
}

void gridder_reset(gridder_t *g)
{
    memset(g->p[0], 0, g->sx*g->sy * sizeof(ptr));
/*
    for (int i = 0; i < g->sx; i++)
        for (int j =0 ; j < g->sy; j++)
            assert(g->p[i][j]==NULL);
*/
}

int gridder_add(gridder_t *g, double x, double y, void *pin)
{
    int ix = (x - g->x_min) / g->resolution;
    int iy = (y - g->y_min) / g->resolution;

//    printf("%d %d\n", ix, iy);
    if (ix < 0 || ix >= g->sx || iy < 0 || iy >= g->sy) {
        gridder_data_t *gd = to_gridder_data_ptr(g, pin);
        gd->next = NULL;
        return -1;
    }

    if (1) {
        for (ptr p = g->p[ix][iy]; p != NULL; p = to_gridder_data_ptr(g, p)->next)
            assert(p != pin);
    }

    gridder_data_t *gd = to_gridder_data_ptr(g, pin);
    ptr p = g->p[ix][iy];
    gd->next = p;
    g->p[ix][iy] = pin;
    return 0;
}

int gridder_remove(gridder_t *g, double x, double y, void *pin)
{
    int ix = (x - g->x_min) / g->resolution;
    int iy = (y - g->y_min) / g->resolution;

    if (ix < 0 || ix >= g->sx || iy < 0 || iy >= g->sy) {
        gridder_data_t *gd = to_gridder_data_ptr(g, pin);
        gd->next = NULL;
        return -1;
    }

    ptr *p = &g->p[ix][iy];
        
    while (*p != NULL) {
        gridder_data_t *gd = to_gridder_data_ptr(g, *p);
        
        if (*p == pin) {
            // found it!
            *p = gd->next;
            return 0;
        }
        
        // this isn't it.
        p = &gd->next;
    }

    return -1;
}

void gridder_set_origin(gridder_t *g, double cx, double cy)
{
    double ox = gridder_origin_x(g);
    double oy = gridder_origin_y(g);

    g->x_max += (cx - ox);
    g->x_min += (cx - ox);
    g->y_max += (cy - oy);
    g->y_min += (cy - oy);
}

double gridder_origin_x(gridder_t *g)
{
    return (g->x_max + g->x_min)/2;
}

double gridder_origin_y(gridder_t *g)
{
    return (g->y_max + g->y_min)/2;
}

static int x_index(const gridder_t *g, double x)
{
    int ix = (x - g->x_min) / g->resolution;
    if (ix < 0)
        ix = 0;
    if (ix >= g->sx)
        ix = g->sx - 1;
    return ix;
}

static int y_index(const gridder_t *g, double y)
{
    int iy = (y - g->y_min) / g->resolution;
    if (iy < 0)
        iy = 0;
    if (iy >= g->sy)
        iy = g->sy - 1;

    return iy;
}

void gridder_iterator_init(const gridder_t *g, gridder_iterator_t *git, 
                           double x_min, double x_max, double y_min, double y_max)
{
    git->g = g;

    git->lox = x_index(g, x_min);
    git->loy = y_index(g, y_min);
    git->x = git->lox;
    git->y = git->loy;
    git->hix = x_index(g, x_max + g->resolution);
    git->hiy = y_index(g, y_max + g->resolution);

    git->lastp = g->p[git->x][git->y];

    assert(x_min <= x_max);
    assert(y_min <= y_max);
    assert(git->lox >=0);
    assert(git->loy >=0);
    assert(git->hix < g->sx);
    assert(git->hiy < g->sy);

}

void *gridder_iterator_next(gridder_iterator_t *git)
{
    if (git->lastp != NULL) {
        // not finished with this grid cell?
        gridder_data_t *gd = to_gridder_data_ptr(git->g, git->lastp);
        git->lastp = gd->next;

        // if there was more data, return it.
        if (git->lastp)
            return git->lastp;

        // no more data in this grid, fall through...
    }

loop:

    // finished with this bucket... move to the next grid cell.
    git->y++;
    if (git->y >= git->hiy) {
        git->y = git->loy;

        git->x++;
        if (git->x >= git->hix) {
            // we've finished the iterator.
            git->y = git->hiy; // make a future call fail too.
            return NULL;
        }
    }

    // any data in this bucket?
    ptr p = git->g->p[git->x][git->y];

    if (p == NULL)
        goto loop;

    git->lastp = p;
    return p;
}
