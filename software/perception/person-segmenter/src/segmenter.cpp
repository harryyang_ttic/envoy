#include <stdio.h>
#include <math.h>
#include <time.h>
#include <map>
#include <vector>

#include "UnionFindSimple.hpp"

#define joiningThreshold 20
#define minSize 240

class segmenter{

    uint32_t nnodes;

    bool v;
    uint16_t nvert, nhoriz;

    public:

double getTimeDiff(timeval a, timeval b)
{
    double first = a.tv_sec + (a.tv_usec/1000000.0);
    double second = b.tv_sec + (b.tv_usec/1000000.0);

    return (first - second)*1000;
}

void segmentGraph(UnionFindSimple *uf, uint16_t *depth){

    if (v) printf("segmenting graph\n");
    struct timeval tva, tvb;
    gettimeofday(&tva, NULL);

    // arbitrarily put the max at 6 meters

    int i, j;
    for(j = 0; j < nvert-1; j++)
    {
        for (i = 0; i < nhoriz-1; i++)
        {
            int pos = j*nhoriz+i;
            if (depth[pos] == 0 || depth[pos] > 2000 || depth[pos] < 750)
                continue;
            if (abs(depth[pos]-depth[pos+1]) < joiningThreshold)
            {
                uf->connectNodes(pos, pos+1);
            }
            if (abs(depth[pos]-depth[pos+nhoriz]) < joiningThreshold)
            {
                uf->connectNodes(pos, pos+nhoriz);
            }
        }
    }

    if (v) printf("done segmenting\n");
}

void joinGraph(UnionFindSimple *uf){

    // arbitrarily put the max at 6 meters

    int i, j;
    for(j = 0; j < nvert-1; j++)
    {
        for (i = 0; i < nhoriz-1; i++)
        {
            int pos = j*nhoriz+i;
            if (uf->getSetSize(pos+1) < minSize)
            {
                uf->connectNodes(pos, pos+1);
            }
            if (uf->getSetSize(pos+nhoriz) < minSize)
            {
                uf->connectNodes(pos, pos+nhoriz);
            }
        }
    }
}

struct segment_features
{
    uint16_t minX, maxX, minY, maxY, minZ, maxZ;
    uint32_t x, y, z;
    double xDev, yDev, zDev;
};

struct segment_t
{
    uint32_t id;
    segment_features sf;
    std::vector<uint32_t> pts;
};

void getSegments(UnionFindSimple *uf, std::map<uint32_t, segmenter::segment_t > &groupedSeg)
{
    int i;
    for (i = 0; i < nnodes; i++)
    {
        uint32_t t = uf->getRepresentative(i);
        if (uf->getSetSize(t) > minSize)
        {
            groupedSeg[t].pts.push_back(i);
        }
    }
}

void getSegmentFeatures(std::map<uint32_t, segmenter::segment_t > &groupedSeg,
                uint16_t *depth)
{
    std::map<uint32_t, segmenter::segment_t >::iterator mapIt;
    std::vector<uint32_t>::iterator vecIt;

    for (mapIt = groupedSeg.begin(); mapIt != groupedSeg.end(); mapIt++)
    {
        segment_features &sf = (*mapIt).second.sf;
        (*mapIt).second.id = (*mapIt).first;
        sf.minX = 640;
        sf.maxX = 0;
        sf.minY = 480;
        sf.maxY = 0;
        sf.minZ = 2096;
        sf.maxZ = 0;
        sf.x = 0;
        sf.y = 0;
        sf.z = 0;
        sf.xDev = 0;
        sf.yDev = 0;
        sf.zDev = 0;

        for (vecIt = (*mapIt).second.pts.begin();
             vecIt != (*mapIt).second.pts.end(); vecIt++)
        {
            if (sf.minX > ((*vecIt)%640))
                sf.minX = (*vecIt)%640;
            if (sf.minY > ((*vecIt)/640))
                sf.minY = (*vecIt)/640;
            if (sf.minZ > depth[*vecIt])
                sf.minZ = depth[*vecIt];

            if (sf.maxX < ((*vecIt)%640))
                sf.maxX = (*vecIt)%640;
            if (sf.maxY < ((*vecIt)/640))
                sf.minY = (*vecIt)/640;
            if (sf.maxZ < depth[*vecIt])
                sf.minZ = depth[*vecIt];

            sf.x += (*vecIt)%640;
            sf.y += (*vecIt)/640;
            sf.z += depth[(*vecIt)];
        }

        sf.x /= (*mapIt).second.pts.size();
        sf.y /= (*mapIt).second.pts.size();
        sf.z /= (*mapIt).second.pts.size();
        //features.push_back(sf);
    }

    int i = 0;
    for (mapIt = groupedSeg.begin(); mapIt != groupedSeg.end(); mapIt++, i++)
    {
        segment_features &sf = (*mapIt).second.sf;
        for (vecIt = (*mapIt).second.pts.begin();
             vecIt != (*mapIt).second.pts.end(); vecIt++)
        {
            sf.xDev += abs(((*vecIt)%640)-sf.x);
            sf.yDev += abs(((int)((*vecIt)/640))-sf.y);
            sf.zDev += abs(depth[(*vecIt)]-sf.z);
        }
        sf.xDev /= (*mapIt).second.pts.size();
        sf.yDev /= (*mapIt).second.pts.size();
        sf.zDev /= (*mapIt).second.pts.size();
    }

}

segmenter(uint16_t nv, uint16_t nh, bool verbose)
{
    v = verbose;
    nvert = nv;
    nhoriz = nh;

    if (v) printf("starting segmenter\n");
    nnodes = nvert*nhoriz;
}

~segmenter()
{
}
};

