#include <lcm/lcm.h>
#include <lcmtypes/kinect_frame_msg_t.h>
#include <bot_core/bot_core.h>

/*#include "libfreenect.h"
#include "libfreenect_sync.h"
#include <stdio.h>

#include <stdlib.h>
#include <math.h>
#include <kinect/kinect-utils.h>
*/
#include <zlib.h>
#include "jpeg-utils-ijg.h"
#include <pthread.h>
#include <errno.h>

class kinectGrabber
{
private:

    bool v;
    bool libfreenect;

    uint16_t nvert, nhoriz;

    static const uint16_t wid = 640, hei = 480;
    static const double max_edge_length = 0.05;

    uint16_t* depth_image;

    int64_t lastTime;

    pthread_mutex_t m, workMutex;
    bool newData;

    void kinect_handler(const lcm_recv_buf_t* lcm, const char* channel,
                        const kinect_frame_msg_t* msg){
        if (v) printf("reading kinect\n");
        // If there is already a new graph ready, ignore.
        // Ignores the latest, but works in practice well
        if (newData == true)
        {
            return;
        }

        pthread_mutex_lock(&m);
        if(msg->depth.compression == KINECT_DEPTH_MSG_T_COMPRESSION_NONE) {
            memcpy(depth_image, msg->depth.depth_data,hei*wid*sizeof(uint16_t));
        }  else if (msg->depth.compression ==
                    KINECT_DEPTH_MSG_T_COMPRESSION_ZLIB) {
            if (v) printf("decompressing\n");
            unsigned long dlen = msg->depth.uncompressed_size; 
            uncompress((uint8_t*)(depth_image), &dlen, msg->depth.depth_data,
                       msg->depth.depth_data_nbytes);
        }
        lastTime = msg->timestamp;
        newData = true;
        pthread_mutex_unlock(&m);

        if(v) printf("got all kinect data\n");
    }

    static void
    lcm_handler(const lcm_recv_buf_t* lcm, const char* chanel,
                const kinect_frame_msg_t* msg, void* user){
        kinectGrabber* s = static_cast<kinectGrabber*>(user);
        if (s != NULL)
            {
                s->kinect_handler(lcm, chanel, msg);
                usleep(10000);
            }
        else printf("message is null\n");
        
    }

public:

    kinectGrabber(lcm_t* l, bool verbose)
    {
        if (verbose) printf("starting kinect grabber\n");
        m = PTHREAD_MUTEX_INITIALIZER;
        workMutex = PTHREAD_MUTEX_INITIALIZER;
        v = verbose;

        nvert = hei;
        nhoriz = wid;

        newData = false;

        depth_image = (uint16_t*)malloc((hei*wid)*sizeof(uint16_t));

        kinect_frame_msg_t_subscribe(l, "KINECT_FRAME", lcm_handler, this);

        if (v) printf("finished kinect initializer\n");
    }

    ~kinectGrabber()
    {
        free(depth_image);
    }

    bool get_kinect_data(uint16_t* depth, int64_t &sensorTime)
    {
        if (v) printf("getting kinect data\n");
        pthread_mutex_lock(&m);
        if (newData == false)
        {
            if (v) printf(" no new kinect data\n");
            pthread_mutex_unlock(&m);
            return false;
        }

        memcpy(depth, depth_image, hei*wid*sizeof(uint16_t));
        sensorTime = lastTime;
        newData = false;
        pthread_mutex_unlock(&m);

        if (v) printf("Got kinect data\n");
        return true;
    }
};
