#include <stdlib.h>

#ifndef UNIONFIND_H
#define UNIONFIND_H


/** Implementation of disjoint set data structure that packs each
 * entry into a single array of 'int' for performance. *
 */

class UnionFindSimple{

    private:
    static const int SZ = 2;
    int size;

    public:

    int* data;
    int* reference;

    /** param maxid The maximum node id that will be referenced. **/
    UnionFindSimple(int maxid)
    {
        reference = (int*)malloc(maxid*SZ*sizeof(int));
        data = (int*) malloc(maxid*SZ*sizeof(int));
        size = maxid;

		int i;
        for (i = 0; i < maxid; i++) {
            // everyone is their own cluster of size 1
            reference[SZ*i]   = i;
            reference[SZ*i+1] = 1;
//            data[SZ*i+0] = i;
//            data[SZ*i+1] = 1;
        }
    }

	~UnionFindSimple(){
            free(reference);
            free(data);
	}

    int getRepresentative(int id)
    {
        while (data[SZ*id] != id)
        {
            data[SZ*id] = data[SZ*data[SZ*id]];
            id = data[SZ*id];
        }
        return id;
    }

    int getSetSize(int id)
    {
        return data[SZ*getRepresentative(id)+1];
    }

    /** returns the id of the merged node. **/
    int connectNodes(int aid, int bid)
    {
        int aroot = getRepresentative(aid);
        int broot = getRepresentative(bid);

        if (aroot == broot)
            return aroot;

        int asz = data[SZ*aroot+1];
        int bsz = data[SZ*broot+1];

        if (asz > bsz) {
            data[SZ*broot] = aroot;
            data[SZ*aroot+1] += bsz;

            return aroot;
        } else {
            data[SZ*aroot] = broot;
            data[SZ*broot+1] += asz;

            return broot;
        }
    }

    void reset()
    {
        memcpy(data, reference, size*SZ*sizeof(int));
    }

    void copy(UnionFindSimple *uf)
    {
        memcpy(uf->data, data, size*SZ*sizeof(int));
    }
};

#endif
