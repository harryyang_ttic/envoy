#include <stdio.h>
#include <pthread.h>

#include <string.h>

#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>

#include <lcmtypes/kinect_point2d_t.h>
#include <lcmtypes/erlcm_kinect_person_tracker_status_t.h>
#include <lcmtypes/erlcm_person_tracking_cmd_t.h>
#include <lcmtypes/er_lcmtypes.h>

#include "kinectGrabber.cpp"
#include "segmenter.cpp"
//#include "colorer.hpp"

static bool initialize;
static bool tracking;

static uint8_t DECI;
static uint8_t trackingLength = 8;

void* lcm_thread_handler(void *l)
{
    lcm_t* lcm = (lcm_t*)(l);
    while(1)
	{
            lcm_handle(lcm);
	}
}

static void tracking_handler(const lcm_recv_buf_t* lcm, const char* channel,
                const erlcm_person_tracking_cmd_t* msg, void* user){
{
    if (msg == NULL) return;
    // Only look for messages from the moderator
    if (msg->sender != ERLCM_PERSON_TRACKING_CMD_T_PERSON_MODERATOR)
        return;

    if (msg->command == ERLCM_PERSON_TRACKING_CMD_T_CMD_STOP_TRACKING)
    {
        tracking = false;
    }
    else if (msg->command == ERLCM_PERSON_TRACKING_CMD_T_CMD_START_TRACKING)
    {
        tracking = true;
        initialize = true;
    }
}
}

static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -v      Set verbose to true.\n"
             "  -d      Publish segmented image.\n"
             "  -l      Use libfreenect driver.\n"
             "  -u      Sub sample.\n"
             "  -h      This help message.\n",
             g_path_get_basename(progname));
    exit(1);
}

void checkResults(int rc)
{
    if (rc != 0)
    {
        printf("Pthread error, exiting out now\n");
        exit(1);
    }
}

void drawImage(lcm_t *lcm, uint16_t nvert, uint16_t nhoriz, 
            std::map<uint32_t, segmenter::segment_t > groupedSeg){

    // Created segmented Img with all zeros
    uint8_t segmentedImg[nvert*nhoriz*3];
    memset(segmentedImg, 0, nvert*nhoriz*3*sizeof(uint8_t));

    std::map<uint32_t, segmenter::segment_t >::iterator mapIt;
    for (mapIt = groupedSeg.begin(); mapIt != groupedSeg.end(); mapIt++)
    {
        uint32_t id = (*mapIt).first;
        std::vector<uint32_t>::iterator it;
        for (it = (*mapIt).second.pts.begin(); it != (*mapIt).second.pts.end(); it++)
        {
            uint32_t pos = (*it);
            segmentedImg[3*pos] = id&0xFF;
            segmentedImg[3*pos+1] = (id*3)&0xFF;
            segmentedImg[3*pos+2] = (id*5)&0xFF;
        }
    }

    bot_core_image_t image;
    image.utime = 0;
    image.width = nhoriz;
    image.height = nvert;
    image.row_stride = image.width*3;
    image.pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB;//GRAY;//RGB;
    image.size = image.height*image.row_stride;
    image.data = segmentedImg;
    image.nmetadata = 0;
    image.metadata = NULL;

    bot_core_image_t_publish(lcm,"Segmented Image",&image);
}


struct xyz_t{
    uint16_t x, y, z;
};

struct pos_t{
    std::vector<xyz_t> lastPos;
    uint8_t numLost;
};

bool lost;

bool getPerson(std::map<uint32_t, segmenter::segment_t > &groupedSeg,
                pos_t &pos)
{
    bool ret = false;

    int minPts = 8000;
    int minDevZ = 20;

    std::map<uint32_t, segmenter::segment_t >::iterator mapIt;
    std::vector<uint32_t>::iterator vecIt;

    std::vector<uint32_t> mapErase;
    int i = 0;
    for (mapIt = groupedSeg.begin(); mapIt != groupedSeg.end(); mapIt++, i++)
    {
        segmenter::segment_features &sf = (*mapIt).second.sf;


        if (
            (*mapIt).second.pts.size() < minPts ||
            abs(sf.maxY-sf.minY) < abs(sf.maxX - sf.minX) ||
            (sf.xDev)*1.2 > sf.yDev || 
            sf.zDev > 100 ||
            sf.xDev * 2.5 < sf.yDev ||
            0)
        {
            mapErase.push_back((*mapIt).first);
        }
    }

    if (groupedSeg.size() > mapErase.size())
    {
        ret = true;
    }
    else
    {
        pos.numLost++;
        xyz_t none;
        none.x = 0xFFFF;
        pos.lastPos.insert(pos.lastPos.begin(), none);
        if (pos.lastPos.size() >= trackingLength)
            pos.lastPos.pop_back();

        groupedSeg.clear();
        return ret;
    }

    std::vector<uint32_t>::iterator eraseIt;
    for (eraseIt = mapErase.begin(); eraseIt < mapErase.end(); eraseIt++)
    {
        groupedSeg.erase(*eraseIt);
    }

    pos.numLost = 0;
    // If there is only one person seen, erase all others
    i = 0;
    std::vector<xyz_t> xyzDist;
    //xyz_t &last = pos.lastPos[0];

    int j = 0;
    double lastX = 0, lastY = 0, lastZ = 0;
    for (i = 0; i < pos.lastPos.size(); i++)
    {
        if (pos.lastPos[i].x == 0xFFFF) continue;

        lastX += pos.lastPos[i].x;
        lastY += pos.lastPos[i].y;
        lastZ += pos.lastPos[i].z;
        j++;
    }
    xyz_t last;
    last.x = (uint16_t) (lastX /j);
    last.y = (uint16_t) (lastY /j);
    last.z = (uint16_t) (lastZ /j);

    double distFilter = 70;

    // If there aren't many stored, take any new ones
    if (j <= trackingLength/2)
    {
        distFilter = 1000;
        last.x = 320;
        last.y = 240;
        last.z = 1024;
    }

    
    if (groupedSeg.size() > 1)
    {
        //printf("grouped size > 1\n");
        i = 0;
        for (mapIt = groupedSeg.begin(); mapIt != groupedSeg.end(); mapIt++,i++)
        {
            segmenter::segment_features &sf = (*mapIt).second.sf;
            xyz_t dist;
            dist.x = abs(last.x - sf.x);
            dist.y = abs(last.y - sf.y);
            dist.z = abs(last.z - sf.z);
            xyzDist.push_back(dist);
        }
        
        i = 0;
        int maxPos = 0;
        mapIt = groupedSeg.begin();
        segmenter::segment_features &sf = (*mapIt).second.sf;
        double distance = sqrt(pow(xyzDist[i].x, 2) + pow(xyzDist[i].y,2));
        mapIt++;
        i++;
        for (; mapIt != groupedSeg.end(); mapIt++,i++)
        {
            sf = (*mapIt).second.sf;
            double newDistance = sqrt(pow(xyzDist[i].x, 2) + pow(xyzDist[i].y,2));
            if (newDistance < distance)
            {
                distance = newDistance;
                maxPos = i;
            }
        }

        //printf("0 distance = %lf, %d %d %d %d\n", distance, xyzDist[maxPos].x, xyzDist[maxPos].y, last.x, last.y);
        if (distance > distFilter)
        {
            printf("0 distance changed too much\n");
            groupedSeg.clear();

            pos.numLost++;
            xyz_t none;
            none.x = 0xFFFF;
            pos.lastPos.insert(pos.lastPos.begin(), none);
            if (pos.lastPos.size() >= trackingLength)
                pos.lastPos.pop_back();

            return false;
        }

        // Erase all but the closest one to the original
        mapErase.clear();
        i = 0;
        for (mapIt = groupedSeg.begin(); mapIt != groupedSeg.end(); mapIt++,i++)
        {
            if (i != maxPos)
                mapErase.push_back((*mapIt).first);
        }

        for (eraseIt = mapErase.begin(); eraseIt < mapErase.end(); eraseIt++)
        {
            groupedSeg.erase(*eraseIt);
        }
    }
    else
    {
        //printf("grouped size = 1\n");
        assert(groupedSeg.size() == 1);
        mapIt = groupedSeg.begin();
        segmenter::segment_features &sf = (*mapIt).second.sf;
        double distX = (double)last.x - (double)sf.x;
        double distY = (double)last.y - (double)sf.y;
        double distance = sqrt(pow(distX, 2) + pow(distY, 2));

        //printf("1 distance = %lf %lf %lf, %d %d %d %d\n", distance, distX, distY, sf.x, sf.y, last.x, last.y);
        
        if (distance > distFilter)
        {
            //printf("1 distance changed too much\n");
            groupedSeg.clear();

            pos.numLost++;
            xyz_t none;
            none.x = 0xFFFF;
            pos.lastPos.insert(pos.lastPos.begin(), none);
            if (pos.lastPos.size() >= trackingLength)
                pos.lastPos.pop_back();

            return false;
        }
    }
    assert(groupedSeg.size() == 1);

    mapIt = groupedSeg.begin();
    xyz_t temp;
    temp.x = (*mapIt).second.sf.x;
    temp.y = (*mapIt).second.sf.y;
    temp.z = (*mapIt).second.sf.z;

    pos.lastPos.insert(pos.lastPos.begin(), temp);
    if (pos.lastPos.size() >= trackingLength)
        pos.lastPos.pop_back();
        

    
    uint64_t eraseEnd = (*mapIt).first;
    groupedSeg[100] = (*mapIt).second;
    groupedSeg.erase(eraseEnd);
    return ret;
}

int main(int argc, char **argv){
    printf("starting\n");

    int c, rc;
    bool v = false;
    bool draw = false;
    bool libfreenect = false;
    bool coloring = false;
    tracking = false;
    initialize = false;
    DECI = 1;
    // command line options - to throtle - to ignore image publish  
    while ((c = getopt (argc, argv, "hvdpu:lc")) >= 0) {
    	switch (c) {
    	case 'v': //ignore images 
            v = true;
            printf("Setting verbose to true\n");
            break;
        case 'd':
            draw = true;
            printf("Drawing segmented images\n");
            break;
        case 'l':
            libfreenect = true;
            printf("Running with libfreenect\n");
            break;
        case 'u':
            DECI = atoi(optarg);
            printf("Subsampling by %d\n", DECI);
            break;
        case 'c':
            coloring = true;
            printf("Coloring segments\n");
            break;
      	case 'h':
        default:
            usage(argv[0]);
            break;
        }
    }

    lcm_t* lcm = lcm_create(NULL);
    pthread_mutex_t drawing_mutex = PTHREAD_MUTEX_INITIALIZER;


    int width = 640;
    int height = 480;
    uint16_t nvert = height / DECI;
    uint16_t nhoriz = width / DECI;
    uint32_t nnodes = nvert*nhoriz;

    uint16_t *depth = (uint16_t*)calloc(nnodes, sizeof(uint16_t));

    kinectGrabber kg(lcm, v);
    segmenter seg(nvert, nhoriz, v);

    UnionFindSimple uf(nnodes);

    erlcm_person_tracking_cmd_t_subscribe(lcm, "PERSON_TRACKING_CMD", tracking_handler, NULL);

    BotFrames *frames;
    BotParam *param;
            
    param = bot_param_new_from_server(lcm, 1);
    frames = bot_frames_get_global(lcm, param);

    if(v) printf("starting lcm thread\n");
    pthread_t lcm_thread;
    rc = pthread_create(&lcm_thread, NULL, lcm_thread_handler, lcm);
    checkResults(rc);

    tracking = true;
    initialize = true;

    struct timeval tva, tvb,tvc,tvd,tve;
    int i;
    double time1 = 0;
    int j = 0;
    pos_t lastPos;

    erlcm_kinect_person_tracker_status_t msg;
    int missedCount = 0;

    
    while(true)
    {
        // If we got a message saying not to track, then skip everything
        if (tracking == false){
            
            continue;
        }

        gettimeofday(&tva, NULL);
        int64_t sensorTime;
        bool newData = kg.get_kinect_data(depth, sensorTime);
        gettimeofday(&tvb, NULL);
        if (newData == true)
        {
            uf.reset();
            if (v) printf("segmenting\n");
            seg.segmentGraph(&uf, depth);
            //seg.joinGraph(&uf);

            gettimeofday(&tvc,NULL);

            std::map<uint32_t, segmenter::segment_t > groupedSeg;
            seg.getSegments(&uf, groupedSeg);

            seg.getSegmentFeatures(groupedSeg, depth);

            bool gotPerson = false;
            if (initialize == true)
            {
                lastPos.lastPos.clear();
                lastPos.numLost = 0;
                xyz_t pos;
                pos.x = 1.5;
                pos.y = 0;
                pos.z = 1;
                lastPos.lastPos.push_back(pos);
                // Set initialize to false again only after you get a person
                // That way the seed is consistent
            }
                gotPerson = getPerson(groupedSeg, lastPos);

            if (draw == true)
            {
                if(v) printf("drawing segmented image\n");
                drawImage(lcm, nvert, nhoriz, groupedSeg);

                gettimeofday(&tve,NULL);
            }
            
            if (gotPerson == true)
            {
                fprintf(stderr, "+");
                initialize = false;
                missedCount = 0;
                std::map<uint32_t, segmenter::segment_t >::iterator mapIt;
                mapIt = groupedSeg.begin();
                segmenter::segment_features &sf = (*mapIt).second.sf;

                // Convert u, v, to realworld x and y
                double fx_d = 1.0 / 5.9421434211923247e+02;
                double fy_d = 1.0 / 5.9104053696870778e+02;
                double cx_d = 3.3930780975300314e+02;
                double cy_d = 2.4273913761751615e+02;
                double depth = sf.z;
                double x = ((sf.x - cx_d) * depth * fx_d);
                double y = ((sf.y - cy_d) * depth * fy_d);


                erlcm_xyz_point_t xyz;
                xyz.xyz[0] = x/1000;
                xyz.xyz[1] = y/1000;
                xyz.xyz[2] = depth/1000;

                double sensor_to_local[12];
                if (!bot_frames_get_trans_mat_3x4_with_utime (frames,
                    "KINECT", "body", sensorTime, sensor_to_local)) {
                    fprintf (stderr, "Error getting bot_frames transformation from KINECT to local!\n");
                }

                erlcm_xyz_point_t transformed_xyz;
                memset(&transformed_xyz, 0, sizeof(erlcm_xyz_point_t));
                bot_vector_affine_transform_3x4_3d (sensor_to_local, xyz.xyz, transformed_xyz.xyz);


                if (v) fprintf(stderr,"Sensor : %f,%f => Body : %f,%f\n", 
                        xyz.xyz[0], xyz.xyz[1], 
                        transformed_xyz.xyz[0], 
                        transformed_xyz.xyz[1]);                        

                msg.utime = bot_timestamp_now();
                msg.status = ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_TRACKING;
                msg.sender = ERLCM_KINECT_PERSON_TRACKER_STATUS_T_SENDER_KINECT;
                msg.x = transformed_xyz.xyz[0];
                msg.y = transformed_xyz.xyz[1];

                erlcm_kinect_person_tracker_status_t_publish(lcm, "KINECT_PERSON_STATUS", &msg);

            }
            else if (missedCount < 8)
            {
                missedCount++;
                msg.utime = bot_timestamp_now();
                erlcm_kinect_person_tracker_status_t_publish(lcm, "KINECT_PERSON_STATUS", &msg);
            }
            else
            {
                fprintf(stderr, "-");
                msg.utime = bot_timestamp_now();
                msg.status = ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_LOST;
                msg.sender = ERLCM_KINECT_PERSON_TRACKER_STATUS_T_SENDER_KINECT;

                erlcm_kinect_person_tracker_status_t_publish(lcm, "KINECT_PERSON_STATUS", &msg);

            }
/**/
//printf ("getting data = %lf\n", seg.getTimeDiff(tvb, tva) );
//printf ("segmenting = %lf\n", seg.getTimeDiff(tvc, tvb) );
//printf ("drawing = %lf\n", seg.getTimeDiff(tve, tvc) );
/**/
        j++;
        time1 += (1000/(seg.getTimeDiff(tve,tva)));
//printf("total fps = %lf\n\n", time1/j);
        }
        else
        {
            if (v) printf("There is no new kinect Data, is it publishing?\n");
            usleep(10000);
        }
//sleep(3);
    }
}
