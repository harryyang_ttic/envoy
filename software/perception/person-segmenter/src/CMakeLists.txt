include_directories(${OPENNI_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS}
)#../../../drivers/kinect/libfreenect/wrappers/c_sync/)

set(CMAKE_BUILD_TYPE Release)

#add_library(kinect_depth SHARED main.cpp)

add_executable(er-person-segmenter main.cpp jpeg-utils-ijg.c)
#
pods_install_pkg_config_file(er-person-segmenter REQUIRES kinect-utils bot2-param-client)

pods_use_pkg_config_packages(er-person-segmenter lcm bot2-core bot2-lcmgl-client lcmtypes_kinect lcmtypes_dynamixel lcmtypes_er-lcmtypes kinect-utils bot2-param-client bot2-frames)

target_link_libraries(er-person-segmenter jpeg z ${OPENNI_LIBS})

pods_install_executables(er-person-segmenter)
