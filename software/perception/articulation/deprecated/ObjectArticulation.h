/*
 * ArticulatedObject.h
 *
 *  Created on: Sep 14, 2010
 *      Author: sturm
 */

#ifndef ARTICULATEDOBJECT_H_
#define ARTICULATEDOBJECT_H_

// #include "articulation_msgs/TrackModelSrv.h"
// #include "articulation_msgs/ArticulatedObjectSrv.h"
// #include "articulation_msgs/ArticulatedObjectMsg.h"
#include <lcmtypes/articulation.hpp>
/* #include <lcmtypes/articulation_articulated_object_msg_t.h> */
/* #include <lcmtypes/articulation_model_msg_t.h> */
/* #include <lcmtypes/articulation_model_param_msg_t.h> */
/* #include <lcmtypes/articulation_object_pose_msg_t.h> */
/* #include <lcmtypes/articulation_object_pose_track_msg_t.h> */
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include "../models/factory.h"
#include "../utils.hpp"
#include "structs.h"


class ObjectArticulation: public KinematicParams, public KinematicData {
public:
    // articulation_msgs::ArticulatedObjectMsg object_msg;
    articulation_models::ArticulatedObject object;
	KinematicGraph currentGraph;
	std::map< std::string, KinematicGraph > graphMap;
	ObjectArticulation();
	ObjectArticulation(const KinematicParams &other);
	// void SetObjectModel(const articulation_msgs::ArticulatedObjectMsg &msg,ros::NodeHandle* nh_local);
        bool SetObjectModel(const articulation_models::ArticulatedObject& object);
	// articulation_msgs::ArticulatedObjectMsg& GetObjectModel();
        articulation_models::ArticulatedObject& GetObjectModel();
	void FitModels();
	KinematicGraph getSpanningTree();
	void ComputeSpanningTree();
	void getFastGraph();
	void getGraph();
	void enumerateGraphs();
	void saveEval();
};

#endif /* ARTICULATEDOBJECT_H_ */
