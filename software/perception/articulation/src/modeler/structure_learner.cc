// Articulation Structure learner
// 1. Run Detector on log file
// articulation-detector -l ~/data/2013-07-31-drawer-apriltags/top_drawer_motion10fps -e 100
// 2. Run structure learner with '-k 20' (run every 20 frames)
// articulation-learner -k 20
// 3. Run viewer
// articulation-renderer / articulation-viewer


// lcm
#include <lcm/lcm-cpp.hpp>
#include <lcmtypes/bot2_param.h>

// libbot/lcm includes
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>

// opencv-utils includes
#include <perception_opencv_utils/opencv_utils.hpp>

// lcm log player wrapper
#include <lcm-utils/lcm-reader-util.hpp>

// lcm messages
#include <lcmtypes/articulation.hpp> 

// optargs
#include <ConciseArgs>

// visualization msgs
// #include <lcmtypes/visualization.h>
#include <lcmtypes/visualization.hpp>

#include <boost/foreach.hpp>

#include "models/factory.h"
#include "utils.hpp"
#include "structure/ArticulatedObject.hpp"

using namespace std;
using namespace articulation_models;

// State 
struct state_t {
    lcm::LCM lcm;

    BotParam   *b_server;
    BotFrames *b_frames;

    // Factory
    MultiModelFactory factory;

    // Model
    GenericModelVector models_valid;

    // Track ID map
    std::map<int, int> id2idx_map, idx2id_map;
    
    // Articulation objects, tracks
    articulation::articulated_object_msg_t object_msg;

    // Frames
    int nframe;

    state_t() : 
        b_server(NULL),
        b_frames(NULL), 
        nframe(0) {

        // Check lcm
        assert(lcm.good());

        //----------------------------------
        // Bot Param/frames init
        //----------------------------------
        b_server = bot_param_new_from_server(lcm.getUnderlyingLCM(), 1);
        b_frames = bot_frames_get_global (lcm.getUnderlyingLCM(), b_server);
    }
    
    void on_pose_tracks (const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                             const articulation::track_msg_t *msg);

    ~state_t() { 
    }

};
state_t state;


struct StructureLearnerOptions { 
    int vRUN_EVERY_K_FRAMES;

    StructureLearnerOptions () : 
        vRUN_EVERY_K_FRAMES(1) {}
};
StructureLearnerOptions options;

KinematicParams params;

struct structure_learner_params { 
	double sigma_position;
	double sigma_orientation;
	double sigmax_position;
	double sigmax_orientation;
	int eval_every;
	double eval_every_power;
	bool supress_similar;
	bool reuse_model;
	std::string restricted_graphs;
	bool restrict_graphs;
	bool reduce_dofs;
	bool search_all_models;
	std::string full_eval;
    structure_learner_params() {}
    structure_learner_params(const KinematicParams& params) { 
        sigma_position = params.sigma_position;
	sigma_orientation = params.sigma_orientation;
	sigmax_position = params.sigmax_position;
	sigmax_orientation = params.sigmax_orientation;
	eval_every = params.eval_every;
	eval_every_power = params.eval_every_power;
	supress_similar = params.supress_similar;
	reuse_model = params.reuse_model;
	restricted_graphs = params.restricted_graphs;
	restrict_graphs = params.restrict_graphs;
	reduce_dofs = params.reduce_dofs;
	search_all_models = params.search_all_models;
	full_eval = params.full_eval;
    }
    KinematicParams getKinematicParams() { 
        KinematicParams params;
        params.sigma_position = sigma_position;
	params.sigma_orientation = sigma_orientation;
	params.sigmax_position = sigmax_position;
	params.sigmax_orientation = sigmax_orientation;
	params.eval_every = eval_every;
	params.eval_every_power = eval_every_power;
	params.supress_similar = supress_similar;
	params.reuse_model = reuse_model;
	params.restricted_graphs = restricted_graphs;
	params.restrict_graphs = restrict_graphs;
	params.reduce_dofs = reduce_dofs;
	params.search_all_models = search_all_models;
	params.full_eval = full_eval;
        return params;
    }

};
structure_learner_params structure_params(params);

// void setArticulatedObjectParams(ArticulatedObject& object) { 

//     // Set PRIOR Params for Articulated Object
//     setParam(object.params, "sigma_position", structure_params.sigma_position,
//              articulation::model_param_msg_t::PRIOR);
//     setParam(object.params, "sigma_orientation", structure_params.sigma_orientation,
//              articulation::model_param_msg_t::PRIOR);

//     // setParam(object.params, "sigmax_position", structure_params.sigmax_position,
//     //          articulation::model_param_msg_t::PRIOR);
//     // setParam(object.params, "sigmax_orientation", structure_params.sigmax_orientation,
//     //          articulation::model_param_msg_t::PRIOR);

//     // setParam(object.params, "eval_every", structure_params.eval_every,
//     //          articulation::model_param_msg_t::PRIOR);

//     // setParam(object.params, "eval_every_power", structure_params.eval_every_power,
//     //          articulation::model_param_msg_t::PRIOR);

//     // setParam(object.params, "suppress_similar", structure_params.suppress_similar,
//     //          articulation::model_param_msg_t::PRIOR);

//     // setParam(object.params, "reuse_model", structure_params.reuse_model,
//     //          articulation::model_param_msg_t::PRIOR);

//     // setParam(object.params, "restricted_graphs", structure_params.restricted_graphs,
//     //          articulation::model_param_msg_t::PRIOR);

//     // setParam(object.params, "restrict_graphs", structure_params.restrict_graphs,
//     //          articulation::model_param_msg_t::PRIOR);

//     setParam(object.params, "reduce_dofs", structure_params.reduce_dofs,
//              articulation::model_param_msg_t::PRIOR);

//     // setParam(object.params, "search_all_models", structure_params.search_all_models,
//     //          articulation::model_param_msg_t::PRIOR);

//     // setParam(object.params, "full_eval", structure_params.full_eval,
//     //          articulation::model_param_msg_t::PRIOR);

//     return;
// }

// void state_t::collect_poses(const articulation::track_msg_t* msg) {


// }

void state_t::on_pose_tracks (const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                              const articulation::track_msg_t *msg) {

    std::cerr << "."; 
    //----------------------------------
    // Collect Tracks
    //----------------------------------
    // Set tracks for track-id
    if (object_msg.parts.size() <= msg->id)
        object_msg.parts.resize(msg->id+1);
    object_msg.parts[msg->id] = *msg;
    // std::cerr << "TRACK : " << msg->id << std::endl;
    if (object_msg.parts.size() < 2) { 
        std::cerr << "Insufficient track information, track size: " 
                  << object_msg.parts.size() << std::endl;
        return;
    }

    //----------------------------------
    // Run every few frames
    //----------------------------------
    nframe++;
    if (nframe % options.vRUN_EVERY_K_FRAMES != 0)
        return;
    
    //----------------------------------
    // Fit model to collected tracks
    //----------------------------------
    std::cerr << "\n============== fit_models ===========" << std::endl;
    ArticulatedObject object(params); 
    object.SetObjectModel(object_msg); 
    object.FitModels();

    articulation::articulated_object_msg_t fully_connected_object = object.GetObjectModel();
    std::cerr << "\n============== fit_models DONE ===========" << std::endl;
    std::cerr << "\n============== get_spanning_tree ===========" << std::endl; 
    // structureSelectSpanningTree
    object.ComputeSpanningTree();
    std::cerr << "\n============== get_spanning_tree DONE ===========" << std::endl; 
    // structureSelectSpanningTree
    std::cerr << "\n============== get_fast_graph ===========" << std::endl; 
    // structureSelectFastGraph
    object.getFastGraph();
    std::cerr << "\n============== get_fast_graph DONE ===========" << std::endl; 
    // structureSelectFastGraph
    std::cerr << "\n============== get_graph ===========" << std::endl; 
    // structureSelectGraph
    // object.getGraph();
    std::cerr << "\n============== get_graph DONE ===========" << std::endl; 

    //----------------------------------
    // Publish articulated object
    //----------------------------------
    articulation::articulated_object_msg_t fitted_object = object.GetObjectModel();

    // Fix issue with sizes before publishing
    fitted_object.num_parts = fitted_object.parts.size();
    fitted_object.num_params = fitted_object.params.size();
    fitted_object.num_models = fitted_object.models.size();
    fitted_object.num_markers = fitted_object.markers.size();
    lcm.publish("ARTICULATED_OBJECT", &fitted_object);

    //----------------------------------
    // Debug info
    //----------------------------------
    std::cerr << "**** ARTICULATED OBJECT PARAMS ****" << std::endl;
    for (int j=0; j<fitted_object.params.size(); j++) { 
        std::cerr << fitted_object.params[j].name << ": " << fitted_object.params[j].value << std::endl;
    }

    std::cerr << "**** MODEL PARAMS ****" << std::endl;
    for (int j=0; j<fitted_object.models.size(); j++) {
        articulation::model_msg_t& model = fitted_object.models[j];
        
        std::cerr << model.id << "**" << model.name << "**" << 
            model.id / fitted_object.parts.size() << "->" << 
            model.id % fitted_object.parts.size() << " " << std::endl;

        for (int k=0; k<model.params.size(); k++) { 
            std::cerr << model.params[k].name << ": " << model.params[k].value << std::endl;
        }
        std::cerr << std::endl;
    }
    std::cerr << "-------------- DONE -----------" << std::endl;

}



// void get_params() { 

//     structure_params.sigma_position = 
//         bot_param_get_double_or_fail(state.b_server,
//                                      "articulation_structure_learner.sigma_position");
//     structure_params.sigma_orientation = 
//         bot_param_get_double_or_fail(state.b_server,
//                                      "articulation_structure_learner.sigma_orientation");
//     structure_params.sigmax_position = 
//         bot_param_get_double_or_fail(state.b_server,
//                                      "articulation_structure_learner.sigmax_position");
//     structure_params.sigmax_orientation = 
//         bot_param_get_double_or_fail(state.b_server,
//                                      "articulation_structure_learner.sigmax_orientation");
//     structure_params.eval_every = 
//         bot_param_get_int_or_fail(state.b_server,
//                                      "articulation_structure_learner.eval_every");
//     structure_params.eval_every_power = 
//         bot_param_get_double_or_fail(state.b_server,
//                                      "articulation_structure_learner.eval_every_power");
//     structure_params.supress_similar = 
//         bot_param_get_boolean_or_fail(state.b_server,
//                                      "articulation_structure_learner.supress_similar");
//     structure_params.reuse_model = 
//         bot_param_get_boolean_or_fail(state.b_server,
//                                      "articulation_structure_learner.reuse_model");

//     char* restricted_graphs_str = 
//         bot_param_get_str_or_fail(state.b_server,
//                                   "articulation_structure_learner.restricted_graphs");
//     structure_params.restricted_graphs = std::string(restricted_graphs_str);

//     structure_params.restrict_graphs = 
//         bot_param_get_boolean_or_fail(state.b_server,
//                                      "articulation_structure_learner.restrict_graphs");
//     structure_params.reduce_dofs = 
//         bot_param_get_boolean_or_fail(state.b_server,
//                                      "articulation_structure_learner.reduce_dofs");
//     structure_params.search_all_models = 
//         bot_param_get_boolean_or_fail(state.b_server,
//                                      "articulation_structure_learner.search_all_models");

//     char* full_eval_str = 
//         bot_param_get_str_or_fail(state.b_server,
//                                   "articulation_structure_learner.full_eval");
//     structure_params.full_eval = std::string(full_eval_str);

//     return;
// }

int main(int argc, char** argv) 
{
    //----------------------------------
    // Opt args
    //----------------------------------
    ConciseArgs opt(argc, (char**)argv);
    opt.add(options.vRUN_EVERY_K_FRAMES, "k", "k-frames","Run every K frames");
    opt.parse();

    //----------------------------------
    // args output
    //----------------------------------
    std::cerr << "===========  Structure Learner ============" << std::endl;
    std::cerr << "MODES 1: articulation-structure-learner\n";
    std::cerr << "=============================================\n";

    std::cerr << "=> Note: Hit 'space' to proceed to next frame" << std::endl;
    std::cerr << "=> Note: Hit 'p' to proceed to previous frame" << std::endl;
    std::cerr << "=> Note: Hit 'n' to proceed to previous frame" << std::endl;
    std::cerr << "===============================================" << std::endl;

    //----------------------------------
    // Subscribe, and start main loop
    //----------------------------------
    state.lcm.subscribe("ARTICULATION_OBJECT_TRACKS", &state_t::on_pose_tracks, &state);

    // Not using config file for filter models
    // get_params();

    // Set PRIOR Params for Articulated Object
    setParam(state.object_msg.params, "sigma_position", structure_params.sigma_position,
             articulation::model_param_msg_t::PRIOR);
    setParam(state.object_msg.params, "sigma_orientation", structure_params.sigma_orientation,
             articulation::model_param_msg_t::PRIOR);
    setParam(state.object_msg.params, "reduce_dofs", structure_params.reduce_dofs,
             articulation::model_param_msg_t::PRIOR);

    while (state.lcm.handle() == 0);

    return 0;

}




// int main(int argc, char** argv) {
// 	ros::init(argc, argv, "structure_learner_server");
// 	nh = new ros::NodeHandle();
// 	nh_local = new ros::NodeHandle("~");

// 	params.LoadParams(*nh_local,false);

// 	model_pub = nh->advertise<articulation_msgs::ModelMsg> ("model", 0);
// 	track_pub = nh->advertise<articulation_msgs::TrackMsg> ("track", 0);
// 	marker_pub = nh->advertise<visualization_msgs::MarkerArray> ("structure_array", 0);
// 	ros::Publisher marker2_pub = nh->advertise<visualization_msgs::Marker> ("structure", 0);

// 	ros::ServiceServer fitService = nh->advertiseService("fit_models",
// 			structureFitModels);
// 	ros::ServiceServer selectServiceSpanningTree = nh->advertiseService("get_spanning_tree",
// 			structureSelectSpanningTree);
// 	ros::ServiceServer selectServiceFastGraph = nh->advertiseService("get_fast_graph",
// 			structureSelectFastGraph);
// 	ros::ServiceServer selectServiceGraph = nh->advertiseService("get_graph",
// 			structureSelectGraph);
// 	ros::ServiceServer selectServiceGraphAll = nh->advertiseService("get_graph_all",
// 			structureSelectGraphAll);
// 	ros::ServiceServer selectVisualizeGraph = nh->advertiseService("visualize_graph",
// 			visualizeGraph);

// 	ROS_INFO("Ready to fit articulation models and structure to articulated objects.");
// 	ros::spin();
// }
