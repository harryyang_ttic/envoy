#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <math.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

// LCM stuff
#include <lcm/lcm.h>
#include <lcmtypes/kinect_frame_msg_t.h>

#include "UnionFindSimple.hpp"

#include <bot_core/bot_core.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv/ml.h>
#include <opencv/cxcore.h>

#include <zlib.h>

#define NUM_EDGES (9)
#define COUNT_SORT_SIZE (6000)
#define IDA_MASK 0xFFFFFF
#define IDB_MASK 0xF
#define IDA_SHIFT 24
#define IDB_SHIFT 20
#define WEIGHT_SHIFT 0
#define WEIGHT_MASK 0xFFFFF

using namespace cv;
using namespace cv::flann;

// Global Variables
int width = 640;
int height = 480;

uint16_t* depth_img;
uint8_t* depth_uncompress_buffer;
uint8_t* segmented_img;
double* xyz;
double* threshDepth;
uint64_t* edgeSortList;
uint64_t* newEdgeSortList;
uint32_t* edges;

lcm_t* lcm = NULL;

static const uint8_t deci_s = 4;
static const uint8_t deci_r = 4;
static const double max_edge_length = 0.25;
static const double depthThreshold = 1000;
static const int minSize = 1000;

static const uint16_t nvert = height / deci_s;
static const uint16_t nhoriz = width / deci_r;
static const uint32_t npixels = width*height;
static const uint32_t nnodes = nvert*nhoriz;
static const uint64_t nedges = nnodes*(NUM_EDGES-1);
static const uint32_t NO_EDGE = ~0;

double RawDepthToMeters(uint16_t depthValue){
    if (depthValue < 2047) {
       return (1.0 / ((double)(depthValue) * -0.0030711016 + 3.3309495161 ));
    }
    return 0.0f;
}


void ConvertToXYZ(){
    int i, j;
    for (i = 0; i < nvert; i++){
        for(j = 0; j < nhoriz; j++){
            double fx_d = 1.0 / 5.9421434211923247e+02;
            double fy_d = 1.0 / 5.9104053696870778e+02;
            double cx_d = 3.3930780975300314e+02;
            double cy_d = 2.4273913761751615e+02;

            double depth = RawDepthToMeters(depth_img[i*deci_s*width+j*deci_r]);
            xyz[(i*nhoriz + j)*3] = ((i - cx_d) * depth * fx_d);
            xyz[(i*nhoriz + j)*3 + 1] = ((j - cy_d) * depth * fy_d);
            xyz[(i*nhoriz + j)*3 + 2] = (depth);
        }
    }
}    

static void
kinect_handler(const lcm_recv_buf_t* lcm, const char* channel, 
               const kinect_frame_msg_t* msg, void* user){

    // TODO check image width, height

    printf("reading\n");

    if(msg->depth.compression == KINECT_DEPTH_MSG_T_COMPRESSION_NONE) {
        depth_img = (uint16_t*) msg->depth.depth_data;
    }  else if (msg->depth.compression == KINECT_DEPTH_MSG_T_COMPRESSION_ZLIB) {
        printf("decompressing\n");
        unsigned long dlen = msg->depth.uncompressed_size;
        uncompress((uint8_t*)depth_img, &dlen, msg->depth.depth_data, msg->depth.depth_data_nbytes);
    }
 
    printf("finished here\n");
    ConvertToXYZ();
}

void segmentGraph(){
	printf("segmenting graph\n");

	int i, j, temp;

    temp = 0;
    for (i = 0; i < nnodes*NUM_EDGES; i++)                                      
        if (edges[i] > nnodes) temp++;
    printf("temp = %d\n",temp);

    for (i = 0; i < nnodes; i++){
		uint32_t aBig = edges[i*NUM_EDGES];

		for (j = 1; j < NUM_EDGES; j++){
			int idx = i*(NUM_EDGES-1)+j-1;

			int bBig = edges[i*NUM_EDGES+j];

			uint64_t ida = 0;
			ida = i & IDA_MASK;
			uint64_t idaShift = 0;
			idaShift = ida << IDA_SHIFT;
			uint64_t idb = 0;
			idb = (uint64_t)(j & IDB_MASK);
			uint64_t idbShift = 0;
			idbShift = idb << IDB_SHIFT;

			uint64_t error = idaShift | idbShift;
			if (edges[i*NUM_EDGES+j] != NO_EDGE){
                double t = ((double)(sqrt(pow(xyz[aBig]-xyz[bBig],2))));
				error |= (uint64_t(t * 1000)) & WEIGHT_MASK;
			}
            edgeSortList[idx] = error;
		}

		//Optimization, add threshold here
		threshDepth[i] = depthThreshold;
	}
    printf("added edge weights\n");

    temp = 0;
    for (i = 0; i < nedges; i ++){
        int a = (int)((((long long)(edgeSortList[i])) >> IDA_SHIFT) & IDA_MASK);
        int b = (int)((((long long)(edgeSortList[i])) >> IDB_SHIFT) & IDB_MASK);

        if(edges[a*NUM_EDGES+b] != NO_EDGE)                                     
        {
            int w = (int) ((edgeSortList[i] & WEIGHT_MASK));                    
            if (w == 0) temp++;
        }
    }   
    printf("There are %d 0 weights\n", temp);                          

    // arbitrarily put the max at 6 meters
    uint64_t* counts = (uint64_t*)calloc(COUNT_SORT_SIZE,sizeof(uint64_t));
	temp = 0;
    for (i = 0; i < nedges; i ++){
        int a = (int)((((long long)(edgeSortList[i])) >> IDA_SHIFT) & IDA_MASK);
        int b = (int)((((long long)(edgeSortList[i])) >> IDB_SHIFT) & IDB_MASK);

        if(edges[a*NUM_EDGES+b] != NO_EDGE)
        {
            int w = (int) ((edgeSortList[i] & WEIGHT_MASK));
            if (w >= COUNT_SORT_SIZE) w = COUNT_SORT_SIZE - 1;
			if (w == 0) temp++;
            counts[w]++;
        }
	}
	printf("finished first counting sort %d\n", temp);

    // accumulate
    for(int i = 1; i < COUNT_SORT_SIZE; i++){
        counts[i] += counts[i-1];
	}

	printf("finished accumulate %d\n", counts[0]);

	memset(newEdgeSortList, 0, nedges*sizeof(uint64_t));
	temp = 0;

    for(i = 0; i < nedges; i++)
    {
		int a = (int)((((long long)(edgeSortList[i])) >> IDA_SHIFT) & IDA_MASK);
        int b = (int)((((long long)(edgeSortList[i])) >> IDB_SHIFT) & IDB_MASK);
        if(edges[a*NUM_EDGES+b] != NO_EDGE)
        {
            int w = (int) (edgeSortList[i] & WEIGHT_MASK);
//			printf("a %d b %d\n", a,b);
            if (w >= COUNT_SORT_SIZE) w = COUNT_SORT_SIZE-1;
			if (w == 0) temp++;
//			printf("%d w = %d counts[w] was %d ",i,w,counts[w]);
            counts[w]--;
//			printf("now is %d\n", counts[w]);
			//if (counts[w] > nedges) printf("error %lu", counts[w]);
            newEdgeSortList[counts[w]] = edgeSortList[i];
        } 
    }
	printf("finished count sort %d\n", temp);
/*    temp = 0;
    for (i = 0; i < nedges; i ++){
        int a = (int)((((long long)(edgeSortList[i])) >> IDA_SHIFT) & IDA_MASK);
        int b = (int)((((long long)(edgeSortList[i])) >> IDB_SHIFT) & IDB_MASK);

        if(edges[a*NUM_EDGES+b] != NO_EDGE)
        {
            int w = (int) ((edgeSortList[i] & WEIGHT_MASK));
            if (w == 0) temp++;
        }
    }
    printf("There are %d 0 weights\n", temp);
*/
	free(counts);


    UnionFindSimple uf(nnodes);
	int no = 0, isedge = 0;
/*    temp = 0;
    for (i = 0; i < nnodes*NUM_EDGES; i++)                                      
        if (edges[i] > nnodes) temp++;
    printf("temp = %d\n",temp);
exit(0);
*/
	temp = 0;
    for(i = 0; i < nedges; i++)
    {
	//	usleep(1000000);
//		printf("starting for loop %d\n", i);

        int a = (int)((((long long)(newEdgeSortList[i])) >> IDA_SHIFT) & IDA_MASK);
        int b = (int)((((long long)(newEdgeSortList[i])) >> IDB_SHIFT) & IDB_MASK);
//		printf("a = %d, b = %d, %d %d\n",a,b,edges[a*NUM_EDGES],edges[a*NUM_EDGES+b]);
        if (edges[a*NUM_EDGES+b] != NO_EDGE)
        {
			isedge++;
//			printf("it's an edge\n");
            int aId = (int) uf.getRepresentative(edges[a*NUM_EDGES]);
            int bId = (int) uf.getRepresentative(edges[a*NUM_EDGES + b]);
//			printf("id's %d %d\n",aId,bId);
            int aSize = uf.getSetSize(aId);
            int bSize = uf.getSetSize(bId);
            int wdepth = (int)((newEdgeSortList[i]) & WEIGHT_MASK);

//			printf("aId = %d %d, bId = %d %d, wdepth = %d\n",aId,aSize,bId,bSize,wdepth);
            if(aId == bId){
		//		printf("The same - aId = %d, bId = %d\n", aId, bId);
                continue;
			}

            // joint if edge passes FH criteria
            if(((depthThreshold < 0) || (wdepth <= threshDepth[aId] && wdepth <= threshDepth[bId] )) )
            {
//				printf("meets depth threshold\n");
                int root = (int) uf.connectNodes(aId, bId);
				temp++;

                if(depthThreshold > 0)
                    threshDepth[root] = wdepth + depthThreshold / uf.getSetSize(root);

                //Update the sum for the root by adding the sum for the othe
                int rootE = a;
                int notRootE = b;
                if(root == aId){
                    rootE = a;
                    notRootE = b;
                }
            }
//			else printf("wdepth %lf, threshdepth a %lf, threshdepth b, %lf\n",wdepth, threshDepth[aId], threshDepth[bId]);
        }
		else{
			no++;
			
		}
    }
	printf("finished first join, temp = %d, no = %d, isedge %d\n", temp, no, isedge);
//exit(0);
           // enforce min size
    for(i = 0; i < nedges; i++)
    {
        int a = (int)((((long long)(newEdgeSortList[i])) >> IDA_SHIFT) & IDA_MASK);
        int b = (int)((((long long)(newEdgeSortList[i])) >> IDB_SHIFT) & IDB_MASK);
		//printf("%d %d\n", a, b);

        if(edges[a*NUM_EDGES+b] != NO_EDGE)
        {
            int aId = (int) uf.getRepresentative(edges[a*NUM_EDGES]);
            int bId = (int) uf.getRepresentative(edges[a*NUM_EDGES+b]);

            int sza = uf.getSetSize(aId);
            int szb = uf.getSetSize(bId);

            if(sza < minSize || szb < minSize){
                uf.connectNodes(aId, bId);
				temp++;
			}//else printf("sze big enough %d %d\n", sza, szb);
        }//else printf("		no edge\n"); 
    } 
	printf("finished min join, total joined = %d, of %d\n", temp, nnodes); 
	temp = 0;
	for (i=0; i<nnodes; i++){
		int t = uf.getRepresentative(i);
		//printf("representative = %d\n", t);
		temp +=t;
		srand(t);
		segmented_img[3*i] = rand();
		segmented_img[3*i+1] = rand();
		segmented_img[3*i+2] = rand();
	}
printf("average = temp/ nnodes %d\n", temp/ nnodes);

    printf("done segmenti(g\n");
}

void buildGraph(){
    printf("building graph\n");
    int s, r, i, j;

    // copy each point into the graph data structure
	int test = 0, t = 0;;
    for (s = 0; s < nvert; s++) {
        for (r = 0; r < nhoriz; r++) {
            int idx = NUM_EDGES*(s*nhoriz + r); //index in nodes for this entry

            // Ignore pixels around the edge, and ones without color
            if (xyz[3*(s*nhoriz+r)+2] <= 0.01 || s == 0 || s == nvert -1 || r ==0 || r == nhoriz -1) {
                for(i  = 0; i < NUM_EDGES; i++){
                    edges[idx+i] = NO_EDGE;
					test++;
					t++;
                }
            } else {
                // ----- We will look at four edges connecting to .: ------
                //               _. \. |. ./
                // ----- We also look at a self edge ----
                edges[idx] = s * nhoriz + (r);
                edges[idx+1] = (s + 0) * nhoriz + (r + 1);
                edges[idx+2] = (s + 1) * nhoriz + (r + 1);
                edges[idx+3] = (s + 1) * nhoriz + (r + 0);
                edges[idx+4] = (s + 1) * nhoriz + (r - 1);
                edges[idx+5] = (s + 0) * nhoriz + (r - 1);
                edges[idx+6] = (s - 1) * nhoriz + (r - 1);
                edges[idx+7] = (s - 1) * nhoriz + (r + 0);
                edges[idx+8] = (s - 1) * nhoriz + (r + 1);
				t+=9;
            }
        }
    }

	printf("made all edges, %d NO_EDGES, %d edges set\n", test, t);
	int test1 = 0, test2= 0;

    int temp = 0;

    for (i = 0; i < nnodes*NUM_EDGES; i++)                                      
        if (edges[i] > nnodes) temp++;                                        
    printf("number of nodes > max = %d\n",temp);

	t = 0;
    // Remove edges that are too long
    for (i = 0; i < nnodes; i++){
        for (j = 0; j < NUM_EDGES; j++){
			if (edges[i*NUM_EDGES + j] != NO_EDGE){
				double dist = abs(xyz[i*3 + 2] - xyz[edges[i*NUM_EDGES+j]*3 + 2]);
                if (dist > max_edge_length){
					edges[i*NUM_EDGES+j] = NO_EDGE;
					test2++;
				}
			}else test1++;
t++;
		}
    }
	printf("test1 = %d, test2 = %d, added = %d, total = %d, t = %d\n",test1,test2,test1+test2,nnodes*NUM_EDGES, t);
	temp = 0;
	for (i = 0; i < nnodes*NUM_EDGES; i++)
		if (edges[i] > nnodes) temp++;
	printf("temp = %d\n",temp);
	printf("finished building graph\n");
}

void  INThandler(int sig)
{
    printf("Exiting\n");

    free(depth_img);
    free(depth_uncompress_buffer);
    free(segmented_img);
    free(xyz);
    free(edges);
    free(threshDepth);
	free(edgeSortList);

    exit(0);
}

void drawImage(){
	printf("drawing image\n");
/*
	IplImage* source = cvCreateImage(cvSize(nhoriz,nvert), 16, 1);
	cvSetData(source, segmented_img, source->widthStep);
	cvNamedWindow( "Source", 1) ;
	cvShowImage("blah", source);
	
	cvReleaseImageHeader(&source);
	
*/
/*
	memset(segmented_img,0,3*nnodes);
	uint8_t r = 0, g = 0, b = 0;
	int i;
	for (i = 0; i < nnodes; i++){
		if (i < nnodes/3){
				segmented_img[i*3+1] = 0xFF;
		}
		else if (i< 2*nnodes/3){
			segmented_img[i*3+2] = 0xFF;
		}
		else{
			segmented_img[i*3] = 0xFF;
		}
	}
printf("done\n");
  */                                                                            

    bot_core_image_t image;
    image.utime = 0;
    image.width = nhoriz;
    image.height = nvert;
    image.row_stride = image.width*3;
    image.pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB;
    image.size = image.height*image.row_stride;
    image.data = segmented_img;
    image.nmetadata = 0;
    image.metadata = NULL;
    bot_core_image_t_publish(lcm,"IFM_RANGE",&image);
}

int main(int argc, char **argv){
    printf("starting\n");

    depth_img = (uint16_t*)malloc(npixels*3);
    depth_uncompress_buffer = (uint8_t*)malloc(npixels*sizeof(uint16_t));
    segmented_img = (uint8_t*)calloc(nnodes*3, sizeof(uint8_t));

    xyz = (double*)malloc(3*sizeof(double)*nnodes);
    threshDepth = (double*)calloc(nnodes, sizeof(double));
    edgeSortList = (uint64_t*)malloc(nedges* sizeof(uint64_t));
    newEdgeSortList = (uint64_t*)malloc(nedges*sizeof(uint64_t));
    edges = (uint32_t*)calloc((nnodes*NUM_EDGES), sizeof(uint32_t));

    // Install signal handler to free data.
    signal(SIGINT, INThandler);

    // Set up the kinect
    lcm = lcm_create(NULL);
    kinect_frame_msg_t_subscribe(lcm, "KINECT_FRAME", kinect_handler, NULL);

    while(1){
        printf("running\n");
        lcm_handle(lcm);
        buildGraph();
        segmentGraph();
		drawImage();
    }
}









