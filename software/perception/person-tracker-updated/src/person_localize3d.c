#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <OpenGL/gl.h>
#endif

#include <unistd.h>
/* global variables */

//LCM stuff
#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
//bot param stuff
#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_lcmgl_client/lcmgl.h>

//Carmen3d stuff
#include <lcmtypes/wheelchair3d_lcmtypes.h>

#include <er_carmen/carmen.h>
#include <interfaces/localize3d_messages.h>


#include <interfaces/map3d_interface.h>
#include <common3d_utils/math_util.h>
#include <common3d_utils/geometry.h>

#include "person_messages.h"
#include "person_interface.h"
#include "person_localizecore.h"



#define REALLOCATE_TIME 1.0  //no of seconds to wait to reallocate
#define CLEARING_RADIUS 0.3 //radius to clear around person estimate
#define SEGMENT_GAP 0.06//0.06//0.03 //gap used in deciding segments 
#define PERSON_OBS_RADIUS 0.4
#define MAX_PERSON_HISTORY 20//80

int tourguide_mode = 1;
carmen3d_localize_param_t param;
laser_offset_t front_laser_offset;
laser_offset_t rear_laser_offset;
carmen_localize_map_t map;
carmen_map_placelist_t placelist;
carmen3d_localize_particle_filter_p filter;
carmen3d_localize_summary_t summary;

carmen3d_map_t current_map;// = NULL;


carmen3d_localize_particle_filter_p *people_filter = NULL;
carmen3d_localize_summary_p people_summary = NULL;
double *time_since = NULL;
carmen_point_p people_positions = NULL;
erlcm_point_t *people_pos = NULL;
erlcm_point_t guide_pos;
erlcm_point_t guide_gpos;
double rel_vel = 0;

carmen_point_t std;    
erlcm_raw_odometry_msg_t* odom_msg;

int multi_people_tracking = 0;
int use_map = 0;
int no_people = 0;
int actual_filter_size = 5;

int new_person_detection = 0;
int first_person_detection = 0;
int summary_active = 0;
int first_person_obs = 0;
int active_filter_ind = -1;
int clear_rearlaser= 1;

int debug_mode = 0;

double lcm_count = 0;
double ipc_count = 0;

//carmen_robot_laser_message front_laser;
carmen_robot_laser_message carmen_robot_frontlaser;
carmen_robot_laser_message carmen_robot_rearlaser;
int have_front_laser = 0;
int have_rear_laser = 0;

carmen_point_t last_robot_position;
carmen_point_t person_position;
int valid_history = 0;
carmen_point_t *person_history = NULL; //person locations - relative to the wheelchair - so do not get transformed 
carmen_point_t *person_history_global = NULL; //person locations transformed - so in the current reference frame 
double *person_dist = NULL;
double *heading_history = NULL;

carmen_point_t new_person_position;
carmen_person_test_message test_message;
carmen_person_globalpos_message gpos_message;
carmen_point_t global_person_position;

carmen_point_t global_robot_pose;

enum FollowingState{
    FOLLOWING, //person is being tracked and followed - issuing navigation commands to the navigator 
    IDLE //not following (can still have a track on the person)
};

enum TrackingState{
    FOUND, //person has been found
    LOOKING, //looking for person (should look for a particular time and give an error message if we cant find the person
    NOT_LOOKING,//system is initialized and has no track on the person 
    LOST // system has lost track of the person 
};
enum FollowingState following_state = IDLE;
enum TrackingState tracking_state = NOT_LOOKING;
double started_looking_time = 0.0;

lcm_t * lcm;

//lcm message publishers
void publish_speech_msg(char* property)
{
    erlcm_speech_cmd_t msg;
    msg.utime = bot_timestamp_now();
    msg.cmd_type = "FOLLOWER";
    msg.cmd_property = property;
    erlcm_speech_cmd_t_publish(lcm, "PERSON_TRACKER", &msg);
}

void publish_speech_following_msg(char* property)
{
    erlcm_speech_cmd_t msg;
    msg.utime = bot_timestamp_now();
    msg.cmd_type = "FOLLOWER";
    msg.cmd_property = property;
    erlcm_speech_cmd_t_publish(lcm, "PERSON_TRACKER", &msg);
}

void publish_speech_tracking_msg(char* property)
{
    erlcm_speech_cmd_t msg;
    msg.utime = bot_timestamp_now();
    msg.cmd_type = "TRACKER";
    msg.cmd_property = property;
    erlcm_speech_cmd_t_publish(lcm, "PERSON_TRACKER", &msg);
}

//publishes both planar lidar (for the person navigator and robot_laser (for slam)
void robot_laser_pub(char * CHAN, carmen_robot_laser_message * robot, lcm_t * lcm)
{
    //fprintf(stderr,"F\n");
    erlcm_robot_laser_t msg;
    memset(&msg, 0, sizeof(msg));
    msg.laser.rad0 = robot->config.start_angle;
    msg.laser.radstep = robot->config.angular_resolution;
    msg.laser.nranges = robot->num_readings;
    msg.laser.ranges = robot->range;
    msg.pose.pos[0] = robot->robot_pose.x;
    msg.pose.pos[1] = robot->robot_pose.y;
    msg.pose.pos[2] = param.front_laser_offset; //this is added to the front laser - will cause an error in other laser configurations
    double rpy[3] = { 0, 0, robot->robot_pose.theta };
    bot_roll_pitch_yaw_to_quat(rpy, msg.pose.orientation);
    msg.utime = robot->timestamp * 1e6;
    msg.laser.utime = msg.utime;
    msg.pose.utime = msg.utime;
    msg.cov.x = 1;
    msg.cov.xy = 0;
    msg.cov.y = 1;
    msg.cov.yaw = .1;
    //bot_core_planar_lidar_t_publish(lcm,"HOKUYO_FORWARD",&msg.laser);
    erlcm_robot_laser_t_publish(lcm,CHAN,&msg);
    //fprintf(stderr,"L");
}

void robot_rearlaser_pub(char * CHAN, carmen_robot_laser_message * robot, lcm_t * lcm)
{
    erlcm_robot_laser_t msg;
    memset(&msg, 0, sizeof(msg));
    msg.laser.rad0 = robot->config.start_angle;
    msg.laser.radstep = robot->config.angular_resolution;
    msg.laser.nranges = robot->num_readings;
    msg.laser.ranges = robot->range;
    msg.pose.pos[0] = robot->robot_pose.x;
    msg.pose.pos[1] = robot->robot_pose.y;
    msg.pose.pos[2] = param.rear_laser_offset; //this is added to the front laser - will cause an error in other laser configurations
    double rpy[3] = { 0, 0, robot->robot_pose.theta };
    bot_roll_pitch_yaw_to_quat(rpy, msg.pose.orientation);
    msg.utime = robot->timestamp * 1e6;
    msg.laser.utime = msg.utime;
    msg.pose.utime = msg.utime;
    msg.cov.x = 1;
    msg.cov.xy = 0;
    msg.cov.y = 1;
    msg.cov.yaw = .1;
    //bot_core_planar_lidar_t_publish(lcm,CHAN,&msg.laser);
    erlcm_robot_laser_t_publish(lcm,CHAN,&msg);
    //fprintf(stderr,"l");
}

void publish_mission_control(lcm_t *lc, int type){
    erlcm_mission_control_msg_t msg;
    msg.utime = bot_timestamp_now();
    msg.type = type;
    erlcm_mission_control_msg_t_publish(lc, MISSION_CONTROL_CHANNEL, &msg);
}

//State updates based on speech commands 


//tracking commands are not being handled by this - it is now being handled by the person navigator
void update_following_state(enum FollowingState new_state){
    if(new_state==IDLE){//should stop the wheelchair
        if(following_state == FOLLOWING){
            //fprintf(stderr,"Stopping Wheelchair\n");
            //publish_mission_control(lcm, CARMEN3D_MISSION_CONTROL_MSG_T_NAVIGATOR_CLEAR_GOAL);
            //no need to say this - we already know that the chair has stopped
            //publish_speech_following_msg("STOPPED");
        }
        else{
            //fprintf(stderr,"Chair is already stopped\n");
        }

        following_state = new_state;
        //issue comand for the robot to stop - and the navigator to clear 
        //current goal - otherwise it will keep using the old goal 
    }
    else if(new_state==FOLLOWING){//check if we have a tracking of the person 
        if(tracking_state == FOUND){//if person is tracked the start following
            following_state = new_state;
            //fprintf(stderr,"Following Person\n");

            //why are we handling this in the tracker
            publish_speech_following_msg("FOLLOWING");
        }
        else{//we dont have an estimate of the person 
            following_state = IDLE;
            //fprintf(stderr,"Unable to Follow\n");
            //publish_speech_following_msg("UNABLE_TO_FOLLOW_LOST"); //there should also be additional reasons
            //- i.e. there is no path to follow the person - we should use this if navigator issues such a message
        }
    }
}

void update_tracking_state(enum TrackingState new_state){
    tracking_state = new_state;
    if(new_state==LOST){
        active_filter_ind = -1;
        publish_speech_tracking_msg("LOST");
        update_following_state(IDLE);
    }else if (new_state == FOUND){
        publish_speech_tracking_msg("FOUND");
        //we should not start following immediately - should follow only 
        //when we get a start following command
    }else if (new_state == LOOKING){
        //reset the person filer - maybe even clear up the current person filter??
        active_filter_ind = -1;
        publish_speech_tracking_msg("LOOKING"); 
        update_following_state(IDLE); //we need to update the tracking state because this looking message may be 
        //generated becaue the guide wants the chair to reaquire him in the case of the chair following a wrong target
    }  
}

void querry_tracking_state(void){
    if(tracking_state== FOUND){
        publish_speech_tracking_msg("SEE_YES");
    }
    else if((tracking_state== LOST) ||(tracking_state== NOT_LOOKING)){
        publish_speech_tracking_msg("SEE_NO");
    }
    else if(tracking_state== LOOKING){
        publish_speech_tracking_msg("SEE_LOOKING");
    }    
}

void initialize_particles(carmen_point_t mean, carmen_point_t std)
{
    carmen_localize_initialize_particles_gaussian_person(filter,
                                                         mean,
                                                         std);
}

void initialize_filter(carmen3d_localize_particle_filter_p pfilter,
                       carmen_point_t mean, carmen_point_t std)
{
    carmen_localize_initialize_particles_gaussian_person(pfilter,
                                                         mean,
                                                         std);
}

//published at the end of each laser message - for the person navigator
void publish_lcm_heading_goal(){ 

    erlcm_goal_heading_msg_t msg;
    msg.utime = bot_timestamp_now();
    if(active_filter_ind !=-1){  //We have active person filter  
        float person_x = people_summary[active_filter_ind].mean.x;
        float person_y = people_summary[active_filter_ind].mean.y;
        //check condition to start following a person
        float act_dist = hypot(person_x,person_y);
        float act_angle = atan2(person_y,person_x);
        msg.target_heading = act_angle;
        if(act_dist >= 0.75){//heading restriction removed from here
            msg.tracking_state = 0; //0 - suitable for following 
            //fprintf(stderr, "Person in followable Location \n");
        }
        else{
            msg.tracking_state = 1;
            //fprintf(stderr,"Person within range - Not following");
        }
    }
    else{
        msg.target_heading = 0.0;
        msg.tracking_state = -1;
        //fprintf(stderr,"Person Location unknown");
    }
    erlcm_goal_heading_msg_t_publish(lcm, "GOAL_HEADING", &msg);
}


void publish_lcm_goal() //called everytime we get a pose message - for the navigator 
{  
    if(active_filter_ind !=-1){  //We have active person filter  

        //Publishing goal interms of global framework
        erlcm_point_t goal;
        float person_x = people_summary[active_filter_ind].mean.x;
        float person_y = people_summary[active_filter_ind].mean.y;
    
        //check condition to start following a person
        float act_dist = hypot(person_x,person_y);
        float act_angle = atan2(person_y,person_x);
        if((act_dist >= 0.75) || act_angle >= M_PI/30){
            float theta = global_robot_pose.theta;
            goal.x = global_robot_pose.x + person_x*cos(theta) - person_y*sin(theta);
            goal.y = global_robot_pose.y + person_x*sin(theta) + person_y*cos(theta);
            goal.z = 0.0;
            goal.yaw = 0.0;
            goal.pitch = 0.0;
            goal.roll = 0.0;
      
            carmen3d_navigator_goal_msg_t msg;
            msg.goal = goal;
            msg.use_theta = 0; //whether to use theta
            msg.utime = bot_timestamp_now();
            msg.nonce = irand(10000000);//no idea where this function is implemented -random();
            msg.sender = CARMEN3D_NAVIGATOR_GOAL_MSG_T_SENDER_WAYPOINT_TOOL;
            carmen3d_navigator_goal_msg_t_publish(lcm, NAV_GOAL_CHANNEL, &msg);
            //fprintf(stderr, "Publishing Navigator Goal: x %f y %f z %f yaw %f\n", goal.x, goal.y, goal.z, goal.yaw);
        }    
        else{
            //fprintf(stderr,"Person within range - Not following");
        }
    }
    else{
        //fprintf(stderr,"Person Location unknown");
    }
}

void publish_people_message()  //people message - carmen message published relative to the robot 
//- for every robot laser message
{
    carmen3d_people_pos_msg_t people_list;
    people_list.utime = bot_timestamp_now();
    people_list.num_people = no_people;
    people_list.followed_person = active_filter_ind;
  
    people_list.people_pos = (erlcm_point_t *) malloc(no_people*sizeof(erlcm_point_t));
    carmen_test_alloc(people_list.people_pos);
    memcpy(people_list.people_pos,people_pos, no_people*sizeof(erlcm_point_t));
  
    carmen3d_people_pos_msg_t_publish(lcm,"PEOPLE_LIST",&people_list);
    free(people_list.people_pos);

    static erlcm_guide_info_t guide_msg;
    memset(&guide_msg,0,sizeof(erlcm_guide_info_t));
    guide_msg.utime = people_list.utime; //this is wrong - need to be the observation time 

    if(active_filter_ind >=0){
        guide_msg.tracking_state = 1;    
        guide_msg.pos[0] = guide_pos.x;
        guide_msg.pos[1] = guide_pos.y;
        //guide_msg.rel_v = rel_vel;
    }
    erlcm_guide_info_t_publish(lcm,"GUIDE_POS",&guide_msg);  
}

void publish_guide_msg(double vel_mag, double heading, double rel_vel)  //people message - carmen message published relative to the robot 
//- for every robot laser message
{
    carmen3d_people_pos_msg_t people_list;
    people_list.utime = bot_timestamp_now();
    people_list.num_people = no_people;
    people_list.followed_person = active_filter_ind;
  
    people_list.people_pos = (erlcm_point_t *) malloc(no_people*sizeof(erlcm_point_t));
    carmen_test_alloc(people_list.people_pos);
    memcpy(people_list.people_pos,people_pos, no_people*sizeof(erlcm_point_t));
  
    carmen3d_people_pos_msg_t_publish(lcm,"PEOPLE_LIST",&people_list);
    free(people_list.people_pos);

    static erlcm_guide_info_t guide_msg;
    memset(&guide_msg,0,sizeof(erlcm_guide_info_t));
    guide_msg.utime = people_list.utime; //this is wrong - need to be the observation time 

    if(active_filter_ind >=0){
        guide_msg.tracking_state = 1;    
        guide_msg.pos[0] = guide_pos.x;
        guide_msg.pos[1] = guide_pos.y;
        guide_msg.vel_mag = vel_mag;
        guide_msg.person_heading = heading;
        guide_msg.closing_velocity = rel_vel;
    }
    erlcm_guide_info_t_publish(lcm,"GUIDE_POS",&guide_msg);  
}

int 
initialize_people_filters(int no_filters)
{
    fprintf(stderr,"Filters Initialized \n");
    people_filter = (carmen3d_localize_particle_filter_p *)
        malloc(no_filters*sizeof(carmen3d_localize_particle_filter_p)); 
    carmen_test_alloc(people_filter);
  
    people_summary  = (carmen3d_localize_summary_p) 
        malloc(no_filters*sizeof(carmen3d_localize_summary_t));
    carmen_test_alloc(people_summary);
    
    time_since = (double *) malloc(no_filters*sizeof(double));
    carmen_test_alloc(time_since);
    return 0;
}



int create_new_person(carmen_point_t person_obs, carmen_point_t std, double time)  
//this is called once the filters that have observations have been updated so this 
//wont reallocate a filter that has an observation in this time period
{
    if(actual_filter_size <=no_people){//we dont have enough filters to allocate -increase by 5
        fprintf(stderr,"Increasing total no of filters");
        carmen3d_localize_particle_filter_p *temp_people_filter = (carmen3d_localize_particle_filter_p *)
            realloc(people_filter,(actual_filter_size+5)*sizeof(carmen3d_localize_particle_filter_p)); 
        carmen_test_alloc(temp_people_filter);
        people_filter = temp_people_filter;
    
        carmen3d_localize_summary_p temp_people_summary  = (carmen3d_localize_summary_p) 
            realloc(people_summary,(actual_filter_size+5)*sizeof(carmen3d_localize_summary_t));
        carmen_test_alloc(temp_people_summary);
        people_summary = temp_people_summary;
    
        double * temp_time_since = (double *) realloc(time_since, (actual_filter_size+5)*sizeof(double));
        carmen_test_alloc(temp_time_since);
        time_since = temp_time_since;
        actual_filter_size +=5;

    }
    people_filter[no_people] = carmen3d_localize_particle_filter_person_new(&param); //create a new filter 
    //this will be freed when that person is not being tracked
    initialize_filter(people_filter[no_people],person_obs,std);  
    carmen_localize_summarize_person(people_filter[no_people], &people_summary[no_people]);
    time_since[no_people] = time;
    no_people++;
  
    return 0;
}

int 
prune_filters(double time_obs){
    int k,current_act_people = 0;

    int valid_filter_count = 0;
  
    for(int j=0;j<no_people;j++){//cycle through the array 
        double dist_from_chair = hypot((people_summary[j].mean.x),(people_summary[j].mean.y));
    
        //have a smaller realloc time for the non-following filters - would cut down on a lot of things 
        //also use the lower number of particles - i.e. move away from ipc param daemon
    
        if(((time_obs-time_since[j]) <= REALLOCATE_TIME) && ((j== active_filter_ind) || (dist_from_chair < 2.0))){
            valid_filter_count++;
        }
    }
    //if the current no_of filters is larger than the live filters, 
    //remove the dead filters and reorder the live ones
    if(no_people>valid_filter_count){//current_act_people) &(current_act_people>=0)){  
        int i=0,j;
  
        //allocate memory for the reduced filters 
    
        fprintf(stderr,"New filters created \n");
        carmen3d_localize_particle_filter_p *new_people_filter = (carmen3d_localize_particle_filter_p *)
            malloc(valid_filter_count*sizeof(carmen3d_localize_particle_filter_p)); 
        carmen_test_alloc(new_people_filter);
  
        carmen3d_localize_summary_p new_people_summary  = (carmen3d_localize_summary_p) 
            malloc(valid_filter_count*sizeof(carmen3d_localize_summary_t));
        carmen_test_alloc(new_people_summary);
    
        double *new_time_since = (double *) malloc(valid_filter_count*sizeof(double));
        carmen_test_alloc(new_time_since); 
    

        for(j=0;j<no_people;j++){//cycle through the array 
            double dist_from_chair = hypot((people_summary[j].mean.x),(people_summary[j].mean.y));
            if(((time_obs-time_since[j]) <= REALLOCATE_TIME) && ((j== active_filter_ind) || (dist_from_chair < 2.0))){
                memcpy(&new_people_filter[i],&people_filter[j], sizeof(carmen3d_localize_particle_filter_p));
                memcpy(&new_people_summary[i],&people_summary[j],sizeof(carmen3d_localize_summary_t));
                new_time_since[i] = time_since[j];
                if(j == active_filter_ind){//if the position of the active filter in the filter list is changed update the location
                    active_filter_ind = i;
                }
                //fprintf(stderr,"\n");
                i++;	
            }
            else{//free the unused filter
                if(j == active_filter_ind){//if the position of the active filter in the filter list is changed update the location
                    active_filter_ind = -1;  //-1 reflects the fact that there is no active filter
                    update_tracking_state(LOST);
                    fprintf(stderr,"+++++++ Person Lost +++++++ **** \n");
                    publish_speech_msg("LOST");
                }

                //fprintf(stderr,"==\tPruning Filter \n");
                free(people_filter[j]->particles); //free the filter 
                for(k = 0; k < people_filter[j]->param->num_particles; k++) {
                    free(people_filter[j]->temp_weights[k]);
                }
                free(people_filter[j]->temp_weights);
            }
        }
        free(people_filter);
        free(people_summary);
        free(time_since);

        people_filter = new_people_filter;
        people_summary = new_people_summary;
        time_since = new_time_since;

        //fprintf(stderr,"Reallocated Filters %d \n",i);
        no_people = valid_filter_count;  //update the new no of filters
        actual_filter_size = no_people;
    } 
    return 0;
}


//lcm handlers 
void mode_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                  const char * channel __attribute__((unused)), 
                  const erlcm_tagged_node_t * msg,
                  void * user  __attribute__((unused)))
{
    char* type = msg->type;
    char* new_mode = msg->label;
    if(!strcmp(type,"mode")){
        //mode has changed - 
        if(!strcmp(new_mode,"navigation")){//navigation mode - stop following guide
            tourguide_mode = 0;
            fprintf(stderr,"Navigation mode\n");
        }
        if(!strcmp(new_mode,"tourguide")){//tourguide mode
            tourguide_mode = 1;
            fprintf(stderr,"Tourguide mode\n");
        }
    }
}

int 
detect_person_both_multi(){
    if((!have_rear_laser && !have_front_laser)){
        return -1;
    }
    return -1;
    //disabled for now - there are some errors here anyway - but need to change interface

    /*
      if (current_map.map != NULL){
      fprintf(stderr,"Checking with Gridmap - Multi\n");
      fprintf(stderr,"Have map => Size (%d,%d)\n", current_map.config.x_size, current_map.config.y_size);
      }

      //int front_laser indicates if this is the front laser - i.e. we remove person legs only from that 
      double time_obs = carmen_robot_frontlaser.timestamp; //carmen_get_time();  //time of the last observation

      carmen_point_t robot_pos = carmen_robot_frontlaser.robot_pose;
      int no_points = 0;
      carmen_point_p points = NULL;  //point array for the laser readings

      static carmen_point_p prev_points = NULL;
  
      //memset(prev_obs, 0, sizeof(prev_obs));
      static int prev_no_points = 0;

      carmen_feet_observations person_obs;

      if(!have_rear_laser){
      points = (carmen_point_p)malloc(carmen_robot_frontlaser.num_readings *sizeof(carmen_point_t));  
      carmen_test_alloc(points);
      //if we prune this observations - need to be done around the robot plus around the guide 
      person_obs = detect_segments_both(&carmen_robot_frontlaser, param.front_laser_offset,
      NULL, param.rear_laser_offset, 
      SEGMENT_GAP,points, &no_points); //runs segment detection - used at 0.2 
      }
      else{
      points = (carmen_point_p)malloc((carmen_robot_frontlaser.num_readings + carmen_robot_rearlaser.num_readings)*sizeof(carmen_point_t)); 
      carmen_test_alloc(points); 
      person_obs = detect_segments_both(&carmen_robot_frontlaser, param.front_laser_offset,
      &carmen_robot_rearlaser, param.rear_laser_offset, 
      SEGMENT_GAP,points, &no_points); //runs segment detection - used at 0.2 
      }

      //call the new feet detector
      carmen_feet_observations moved_obs;
      get_moving_feet_segments(&person_obs, &moved_obs, prev_points, prev_no_points);
  
      carmen_feet_observations used_person_obs;
  
      //Prune observations if they are in the same place as in the last frame - based on motion only
      //used_person_obs = prune_observations(&person_obs);
      used_person_obs = person_obs; //no pruning 
  
 
  
      int meta_count = used_person_obs.no_obs;
      carmen_feet_seg* feet_obs = used_person_obs.locations;
      int i,j;

      carmen_point_p person_det = (carmen_point_p) malloc(meta_count*sizeof(carmen_point_t)); 
      carmen_test_alloc(person_det);

      int obs_count = 0;

      if (current_map.map != NULL){
      fprintf(stderr,"Checking with the map\n");
      }
      fprintf(stderr,"Obs before pruning : %d\n", meta_count);

      //copy the observations on to points
      for (i=0;i<meta_count;i++){
      double l_leg_x = feet_obs[i].center_x;
      double l_leg_y = feet_obs[i].center_y;
      //checking with the map to detect valid persons
      if(current_map.map != NULL){
      float rtheta = global_robot_pose.theta;
      float leg_x = global_robot_pose.x + l_leg_x * cos(rtheta) - l_leg_y * sin(rtheta);
      float leg_y = global_robot_pose.y + l_leg_x * sin(rtheta) + l_leg_y*cos(rtheta);
      
      carmen_point_t leg_pt;
      memset(&leg_pt,0,sizeof(carmen_point_t));
      leg_pt.x = leg_x;
      leg_pt.y = leg_y;
      int l_x, l_y;

      int occupied = 0;

      //fprintf(stderr,"Map Zero (%f,%f), Mid Point : (%f,%f)\n", current_map.map_zero.x, current_map.map_zero.y,
      //      current_map.midpt.x, current_map.midpt.y);
	
      carmen3d_map3d_global_to_map_index_coordinates(leg_pt, current_map.midpt, current_map.map_zero,
      current_map.config.resolution, &l_x, &l_y);

      int search_size = 2;
	
      for(int j=fmax(0,l_x - search_size); j<=fmin(l_x+search_size,current_map.config.x_size-1); j++){
      for(int k=fmax(0,l_y - search_size); k<=fmin(l_y+search_size,current_map.config.x_size-1); k++){
	  if(current_map.map[j][k] >0.7){
      occupied = 1;
      break;
	  }
      }
      if(occupied){
	  break;
      }
      }
      if(!occupied){
      //fprintf(stderr,"Possible Leg\n");
      person_det[obs_count].x = l_leg_x;
      person_det[obs_count].y = l_leg_y;
      person_det[obs_count].theta = 0.0;			
      obs_count++;
      }
      }
      else{
      person_det[obs_count].x = l_leg_x; 
      person_det[obs_count].y = l_leg_y;
      person_det[obs_count].theta = 0.0;
      obs_count++;
      }
      }
  
      meta_count = obs_count;
      person_det = (carmen_point_p) realloc(person_det,meta_count*sizeof(carmen_point_t));
      fprintf(stderr,"Obs after pruning : %d\n", meta_count);
      //draw possible legs 
      bot_lcmgl_t *lcmgl = globals_get_lcmgl("leg_obs",1);

      if (1 && lcmgl){// && is_front_laser){ //turn to 1 if u want to draw
      lcmglColor3f(0, 1.0, 0);
    
      float rtheta = global_robot_pose.theta;
      double pos[3];
      for (i=0;i<meta_count;i++){
      float leg_x = global_robot_pose.x + person_det[i].x * cos(rtheta) - person_det[i].y * sin(rtheta);
      float leg_y = global_robot_pose.y + person_det[i].x * sin(rtheta) + person_det[i].y*cos(rtheta);
      pos[0] = leg_x;
      pos[1] = leg_y;
      pos[2] = 2.0; 

      lcmglColor3f(.0, 1.0, .0);

      lcmglLineWidth (5);      
      lcmglCircle(pos, 0.1);
      if(debug_mode){
      char seg_info[512];
      sprintf(seg_info,"[%d]%d-%d:%d", feet_obs[i].front_laser, feet_obs[i].start_segment, feet_obs[i].end_segment, 
      feet_obs[i].start_segment - feet_obs[i].end_segment);
      bot_lcmgl_text(lcmgl, pos, seg_info);
      } 
      }
      } 

      bot_lcmgl_switch_buffer (lcmgl);

      //prune dead filters
      prune_filters(time_obs);  

      //Filter initiation code
      //create a filter for the observation that is not near any of the current filters

      carmen_point_t robot_position;
  
      robot_position.x = carmen_robot_frontlaser.robot_pose.x;
      robot_position.y = carmen_robot_frontlaser.robot_pose.y;
      robot_position.theta = carmen_robot_frontlaser.robot_pose.theta;

      //update the filter summaries based on the robot motion 
      for(j=0;j<no_people;j++){
      carmen_localize_update_filter_center(last_robot_position,robot_position,&people_summary[j].mean);
      }

      carmen_point_p moved_person = (carmen_point_p) malloc(moved_obs.no_obs*sizeof(carmen_point_t)); 
      carmen_test_alloc(moved_person);

      lcmgl = globals_get_lcmgl("moved_leg_obs",1);

      //copy the moved observations on to points
      for (int i=0;i< moved_obs.no_obs;i++){

      moved_person[i].x = moved_obs.locations[i].center_x;
      moved_person[i].y = moved_obs.locations[i].center_y;
      moved_person[i].theta = 0.0;

      double pos[3];
      int occupied = 0;
      float rtheta = global_robot_pose.theta;
      float leg_x = global_robot_pose.x + moved_person[i].x * cos(rtheta) - moved_person[i].y * sin(rtheta);
      float leg_y = global_robot_pose.y + moved_person[i].x * sin(rtheta) + moved_person[i].y*cos(rtheta);
      
      pos[0] = leg_x;
      pos[1] = leg_y;
      pos[2] = 2.0;  

      if(current_map.map != NULL){
      //removing moved obs if obstacles in the map
      fprintf(stderr,"Moved Observation : %d Looking in gridmap\n",i);
      //check if there are obstacles nearby 

      carmen_point_t leg_pt;
      memset(&leg_pt,0,sizeof(carmen_point_t));
      leg_pt.x = leg_x;
      leg_pt.y = leg_y;
      int l_x, l_y;

      carmen3d_map3d_global_to_map_index_coordinates(leg_pt, current_map.midpt, current_map.map_zero,
      current_map.config.resolution, &l_x, &l_y);

      int search_size = 2;
	
      for(int j=fmax(0,l_x - search_size); j<=fmin(l_x+search_size,current_map.config.x_size-1); j++){
      for(int k=fmax(0,l_y - search_size); k<=fmin(l_y+search_size,current_map.config.x_size-1); k++){
	  if(current_map.map[j][k] >0.5){
      occupied = 1;
      break;
	  }
      }
      if(occupied){
	  break;
      }
      }
      }
      fprintf(stderr,"\t Occupied : %d\n", occupied);
      if(occupied){
      fprintf(stderr,"Skipping\n");
      continue;
      }
      fprintf(stderr,"Processing\n");
      lcmglLineWidth (5);
      lcmglColor3f(1.0, 1.0, .0);
      lcmglCircle(pos, 0.1);

      //check if there are other filters near by
      double min_dist = 1000;
      for(j=0;j<no_people;j++){
      double temp_dist = hypot((people_summary[j].mean.x - moved_person[i].x),(people_summary[j].mean.y-moved_person[i].y));
      if(temp_dist < min_dist){
      min_dist = temp_dist;
      }
      }
      if(min_dist > 0.8){//1.0){
      //check if the distance between the person being followed and this new location is farther than 1.0 ? (or 1.2?)
      if(active_filter_ind>=0){
      double temp_dist = hypot((people_summary[active_filter_ind].mean.x - moved_person[i].x),
      (people_summary[active_filter_ind].mean.y-moved_person[i].y));
      if(temp_dist > 1.0){	  
	  fprintf(stderr,"----------- Creating Filter\n");	     
	  create_new_person(moved_person[i],std, time_obs);   
      }
      else{
	  fprintf(stderr,"----------- Too Close to the person being followed\n");
      }
      }
      else{   
      fprintf(stderr,"----------- Creating Filter\n");	     
      create_new_person(moved_person[i],std, time_obs);
      }
      }
      }

      bot_lcmgl_switch_buffer (lcmgl);
  
      double **dist;
      dist = (double **) malloc(meta_count*sizeof(double *));
      carmen_test_alloc(dist);
      //create a distance matrix between the filters and the observations
      for (i=0;i<meta_count;i++){
      dist[i] = (double *) malloc(no_people*sizeof(double));
      carmen_test_alloc(dist[i]);
      for(j=0;j<no_people;j++){
      dist[i][j] = hypot((people_summary[j].mean.x-person_det[i].x),(people_summary[j].mean.y-person_det[i].y));
      }
      }

      //allocate each observation to the closest person filter 
      int *no_obs_count = (int *) malloc(no_people*sizeof(int)); //no of observations that each filter can see
      carmen_test_alloc(no_obs_count);
      memset(no_obs_count,0,no_people*sizeof(int));

      int original_filter_size = no_people;
  
      int *allocated_to_filter = (int *) malloc(meta_count*sizeof(int)); //no of observations that each filter can see
      carmen_test_alloc(allocated_to_filter);
      memset(allocated_to_filter,0,meta_count*sizeof(int));
  
      int **allocated_observations = (int **) malloc(no_people*sizeof(int *)); //what observations are assigned to each filter
      carmen_test_alloc(allocated_observations);
      for (j=0;j<no_people;j++){
      allocated_observations[j] = (int *) malloc(meta_count*sizeof(int)); //what observations are assigned to each filter
      carmen_test_alloc(allocated_observations[j]);
      memset(allocated_observations[j],-1,meta_count*sizeof(int));
      }

      double filter_creation_angle = M_PI/12; //M_PI/12;
      double filter_creation_dist = 3.0;//3.0;

      if(active_filter_ind >=0){//first do this for the tourguide 
      double dist_to_wheelchair = 10000.0;

      double person_angle = atan2(people_summary[active_filter_ind].mean.y, people_summary[active_filter_ind].mean.x);
      int use_front_laser = 0;

      fprintf(stderr,"Person Angle : %f\n", person_angle * 180/M_PI);
    
      //63 degrees is the angle at which the rear laser hits the front laser's 90 degree side scans

      if(person_angle < M_PI/180*63 && person_angle >= -M_PI/180*63){
      fprintf(stderr,"Using only the front laser observations");
      use_front_laser = 1;
      }
      else{
      fprintf(stderr,"Using only the rear laser observations");
      }
      int full_ind = -1;
      int ind = -1;
      for (i=0;i<meta_count;i++){ //search through
      if(dist[i][active_filter_ind] < 0.4){ //within the boundry
      allocated_to_filter[i]=1; //make all these observations out of bounds for the other filters
      double temp_dist = hypot(person_det[i].x, person_det[i].y);
      if(temp_dist < dist_to_wheelchair){
	  ind = i;
	  dist_to_wheelchair = temp_dist;
      }
      }
      }
      //if(no_obs_count[j]==0){
      if(ind==-1){
      carmen_localize_run_person_no_obs(people_filter[active_filter_ind], robot_pos);
      carmen_localize_summarize_person(people_filter[active_filter_ind], &people_summary[active_filter_ind]);
      }
      else if(ind>=0){//(no_obs_count[j]==1){
      int obs = ind;//allocated_observations[j][0];
      carmen_localize_run_person_with_obs(people_filter[active_filter_ind], robot_pos,person_det[obs]);
      carmen_localize_summarize_person(people_filter[active_filter_ind], &people_summary[active_filter_ind]);
      time_since[active_filter_ind] = time_obs; 
      }
      }

  
      for(j=0;j<no_people;j++){//for each filter search the close observations and pick the one closest to the wheelchair 
      if(j==active_filter_ind){
      continue;
      }
      double dist_to_wheelchair = 10000.0;
      int ind = -1;
      for (i=0;i<meta_count;i++){ //search through
      if(!allocated_to_filter[i] &&(dist[i][j] < 0.4)){ //within the boundry
      double temp_dist = hypot(person_det[i].x, person_det[i].y);
      if(temp_dist < dist_to_wheelchair){
	  ind = i;
	  dist_to_wheelchair = temp_dist;
      }
      }
      }
      //if(no_obs_count[j]==0){
      if(ind==-1){
      carmen_localize_run_person_no_obs(people_filter[j], robot_pos);
      carmen_localize_summarize_person(people_filter[j], &people_summary[j]);
      }
      else if(ind>=0){//(no_obs_count[j]==1){
      allocated_to_filter[ind] = 1;
      int obs = ind;//allocated_observations[j][0];
      carmen_localize_run_person_with_obs(people_filter[j], robot_pos,person_det[obs]);
      carmen_localize_summarize_person(people_filter[j], &people_summary[j]);
      time_since[j] = time_obs; 
      }


      }

      fprintf(stderr,"Current States => Tracking : %d, Following: %d\n",tracking_state, following_state);    

      if(tracking_state==LOOKING){//if we are in the tracking state we should reaquire 
      double min_dist = 4.0;
      double too_close_dist = 0.5;
      //double min_dist = 0.5;
      double follow_angle = M_PI/12;
      double temp_dist;
      double temp_angle;
      fprintf(stderr,"Looking for person\n");
      for(j=0;j<no_people;j++){
      temp_dist = hypot(people_summary[j].mean.x,people_summary[j].mean.y);
      temp_angle = atan2(people_summary[j].mean.y,people_summary[j].mean.x);
      if((fabs(temp_angle) < follow_angle) 
      && (temp_dist < min_dist) && (temp_dist > too_close_dist)){
      min_dist = temp_dist;
      active_filter_ind = j;	
      }
      }
      //selects the closest observation with-in the angle cone 
      if(active_filter_ind >=0){
      update_tracking_state(FOUND);  //person located	
      fprintf(stderr,"====== New Person Following Activated =========\n");      
      }
      else{
      fprintf(stderr,"**** No good filter found **** \n");
      }
      }
      else{
      if(active_filter_ind>=0){
      fprintf(stderr,"== Current Person Followed is %d : (%f,%f)\n", active_filter_ind, 
      people_summary[active_filter_ind].mean.x,people_summary[active_filter_ind].mean.y);
      }
      else{
      fprintf(stderr,"== No one being tracked \n");
      }
      }

      fprintf(stderr,"New filter Positions\n");
      for (j=0;j<no_people;j++){
      fprintf(stderr,"(%f,%f)\t",people_summary[j].mean.x,people_summary[j].mean.y);
      }

      if(no_people>0){    
      erlcm_point_t* test_people_pos = (erlcm_point_t *) 
      realloc(people_pos,no_people*sizeof(erlcm_point_t));
      carmen_test_alloc(test_people_pos);
      people_pos = test_people_pos;
      }

      int person_pos = 0;

      for(j=0;j<no_people;j++){
      people_pos[person_pos].x = people_summary[j].mean.x;
      people_pos[person_pos].y = people_summary[j].mean.y;
      people_pos[person_pos].z = 0.0;
      people_pos[person_pos].yaw = 0.0;
      people_pos[person_pos].pitch = 0.0;
      people_pos[person_pos].roll = people_summary[j].mean.theta;
      person_pos++;
      }

      //publishing a person removed robot and planar lidar message
      //if(is_front_laser){
      float distance_from_person = 0.0;
      static carmen_robot_laser_message* carmen_laser;
  
  
      if(carmen_laser == NULL){
      carmen_laser = (carmen_robot_laser_message *) malloc(sizeof(carmen_robot_laser_message));
      carmen_test_alloc(carmen_laser);
      }
      memcpy(carmen_laser,&carmen_robot_frontlaser, sizeof(carmen_robot_laser_message));
      if(carmen_laser->range==NULL){
      carmen_laser->range = (float *) malloc(carmen_robot_frontlaser.num_readings * sizeof(float));
      carmen_test_alloc(carmen_laser->range);
      }
  
      memcpy(carmen_laser->range, carmen_robot_frontlaser.range, 
      carmen_robot_frontlaser.num_readings * sizeof(float));
  
      double person_leg_range = 1.0;
      int removed_legs = 0;
      if(active_filter_ind >=0){  //if we are tracking a person -remove the observations attached to the person 
      for (i=0;i<used_person_obs.no_obs;i++){
      distance_from_person = hypot(feet_obs[i].center_x - people_summary[active_filter_ind].mean.x,
      feet_obs[i].center_y - people_summary[active_filter_ind].mean.y);
      if(distance_from_person < person_leg_range){
      removed_legs++;
      int p;
      fprintf(stderr,"[%d:%d]\n",feet_obs[i].start_segment, feet_obs[i].end_segment);
      for(p = fmax(feet_obs[i].start_segment-3,0); p <=fmin(feet_obs[i].end_segment+3,carmen_robot_frontlaser.num_readings-1);p++){
	  carmen_laser->range[p] = 0.001;  //also might have implication on the max range conversion done in laser driver
      } 	  
      }
      }
      fprintf(stderr,"No of Observations Removed : %d\n", removed_legs);
    
      //remove any points within 0.3 
      for(int p = 0; p < carmen_robot_frontlaser.num_readings ;p++){
      distance_from_person = hypot(points[p].x - people_summary[active_filter_ind].mean.x,
      points[p].y - people_summary[active_filter_ind].mean.y);
      if(distance_from_person < CLEARING_RADIUS){
      carmen_laser->range[p] = 0.001;  	  
      }
      }
      }
    
      robot_laser_pub("PERSON_ROBOT_LASER",carmen_laser,lcm);
  
      publish_people_message();

      if(no_people >=0){//draw person if we have a person being tracked
      lcmgl = globals_get_lcmgl("person",1);
      double pos[3];

      for(i=0;i< no_people;i++){
      float person_x = people_summary[i].mean.x;
      float person_y = people_summary[i].mean.y;
      float theta = global_robot_pose.theta;

      float goal_x = global_robot_pose.x + person_x * cos(theta) - person_y*sin(theta);
      float goal_y = global_robot_pose.y + person_x * sin(theta) + person_y*cos(theta);

      pos[0] = goal_x;
      pos[1] = goal_y;
      pos[2] = 2.0;  

      if (1 && lcmgl) {//turn to 1 if u want to draw
      lcmglLineWidth (5);
      if(i==active_filter_ind){
	  lcmglColor3f(1.0, .0, .0);
      }
      else{
	  lcmglColor3f(.0, 1.0, 1.0);
      }
      lcmglCircle(pos, 0.15);
      }
      }
      bot_lcmgl_switch_buffer (lcmgl);
      }
  
      fprintf(stderr,"No of filters %d \n", no_people);
      fprintf(stderr,"------------------------------------------------------------\n");
      free(person_det);

      for (i=0;i<meta_count;i++){
      free(dist[i]);
      }
      free(dist);

      for (j=0;j<original_filter_size;j++){
      free(allocated_observations[j]);
      }
      free(allocated_observations);
      free(no_obs_count);
      free(allocated_to_filter);
      free(used_person_obs.locations);
      free(prev_points);
      prev_points = points;
      prev_no_points = no_points;

      return 1;*/
}


int 
detect_person_both_basic(){ 
  
    if((!have_rear_laser && !have_front_laser)){
        return -1;
    }
    if (current_map.map != NULL){
        fprintf(stderr,"Checking with Gridmap\n");
        fprintf(stderr,"Have map => Size (%d,%d)\n", current_map.config.x_size, current_map.config.y_size);
    }

    double vel_x = .0, vel_y = .0;
    double u_vel_x = .0, u_vel_y = .0;
    double person_heading_average = .0;
    double person_vel_average = .0;
    double learning_rate = 0.3;

    double time_obs = carmen_robot_frontlaser.timestamp; 
    carmen_point_t robot_pos = carmen_robot_frontlaser.robot_pose;
    bot_lcmgl_t *lcmgl;
    carmen_point_t robot_position;

    static double vel[2] = {.0,.0};
    static double last_person_pos[3] = {.0,.0,.0};
    static double prev_time = 0;
    robot_position.x = carmen_robot_frontlaser.robot_pose.x;
    robot_position.y = carmen_robot_frontlaser.robot_pose.y;
    robot_position.theta = carmen_robot_frontlaser.robot_pose.theta;

    //update the filter summaries based on the robot motion 
    for(int j=0;j<no_people;j++){
        carmen_localize_update_filter_center(last_robot_position,robot_position,&people_summary[j].mean);
    }
  
    int no_points = 0;
    carmen_point_p points = NULL;  //point array for the laser readings

    static carmen_point_p prev_points = NULL;
    static int prev_no_points = 0;
  
    carmen_feet_observations person_obs;

    if(!have_rear_laser){
        points = (carmen_point_p)malloc(carmen_robot_frontlaser.num_readings *sizeof(carmen_point_t));  
        carmen_test_alloc(points);
        if(active_filter_ind >=0){
            carmen_point_t person_loc;
            person_loc.x = people_summary[active_filter_ind].mean.x;
            person_loc.y = people_summary[active_filter_ind].mean.y;
            person_loc.theta = .0;

            //these need to consider side offset as well as front offsets 
            person_obs = detect_segments_both_pruned(&carmen_robot_frontlaser, front_laser_offset,
                                                     NULL, rear_laser_offset, 
                                                     SEGMENT_GAP,points, &no_points, &person_loc, 0, 0);  
        }
        else{
      
            //these need to consider side offset as well as front offsets 
            person_obs = detect_segments_both(&carmen_robot_frontlaser, front_laser_offset,
                                              NULL, rear_laser_offset, 
                                              SEGMENT_GAP,points, &no_points, 0, 0);  

            //call the new feet detector
            carmen_feet_observations moved_obs;
            get_moving_feet_segments(&person_obs, &moved_obs, prev_points, prev_no_points);
  
            //lcmgl = globals_get_lcmgl("moved_leg_obs",1);
            lcmgl = bot_lcmgl_init(lcm, "moved_leg_obs");

            if (1 && lcmgl){// && is_front_laser){ //turn to 1 if u want to draw
                float rtheta = global_robot_pose.theta;
                double pos[3];
                for (int i=0;i<moved_obs.no_obs;i++){
                    float leg_x = global_robot_pose.x + moved_obs.locations[i].center_x * cos(rtheta) - moved_obs.locations[i].center_y * sin(rtheta);
                    float leg_y = global_robot_pose.y + moved_obs.locations[i].center_x * sin(rtheta) + moved_obs.locations[i].center_y*cos(rtheta);
                    pos[0] = leg_x;
                    pos[1] = leg_y;
                    pos[2] = 2.0;  
     
                    lcmglLineWidth (5);
                    lcmglColor3f(1.0, 1.0, .0);
                    lcmglCircle(pos, 0.1);
                } 
            } 

            bot_lcmgl_switch_buffer (lcmgl);

            carmen_point_p moved_person = (carmen_point_p) malloc(moved_obs.no_obs*sizeof(carmen_point_t)); 
            carmen_test_alloc(moved_person);
  
            //copy the observations on to points
            if(active_filter_ind==-1){
                for (int i=0;i< moved_obs.no_obs;i++){
                    moved_person[i].x = moved_obs.locations[i].center_x;
                    moved_person[i].y = moved_obs.locations[i].center_y;
                    moved_person[i].theta = 0.0;
                    //check if there are other filters near by
                    double min_dist = 1000;
                    for(int j=0;j<no_people;j++){
                        double temp_dist = hypot((people_summary[j].mean.x - moved_person[i].x),(people_summary[j].mean.y-moved_person[i].y));
                        if(temp_dist < min_dist){
                            min_dist = temp_dist;
                        }
                    }
                    if(min_dist < 0.8){
                        //fprintf(stderr,"Already tracking a person near - not adding new people\n");
                    }
                    else{   
                        //fprintf(stderr,"----------- Creating Filter\n");	     
                        create_new_person(moved_person[i],std, time_obs);
                    }
                }
            }
            free(moved_person);
        }
    }
    else{
        points = (carmen_point_p)malloc((carmen_robot_frontlaser.num_readings + carmen_robot_rearlaser.num_readings)*sizeof(carmen_point_t)); 
        carmen_test_alloc(points); 
        if(active_filter_ind >=0){
            carmen_point_t person_loc;
            person_loc.x = people_summary[active_filter_ind].mean.x;
            person_loc.y = people_summary[active_filter_ind].mean.y;
            person_loc.theta = .0;
            //these need to consider side offset as well as front offsets 
            person_obs = detect_segments_both_pruned(&carmen_robot_frontlaser, front_laser_offset,
                                                     &carmen_robot_rearlaser, rear_laser_offset, 
                                                     SEGMENT_GAP,points, &no_points, &person_loc, 0, 0); 
        }
        else{
            //these need to consider side offset as well as front offsets 
            person_obs = detect_segments_both(&carmen_robot_frontlaser, front_laser_offset,
                                              &carmen_robot_rearlaser, rear_laser_offset, 
                                              SEGMENT_GAP,points, &no_points, 0, 0); 

            //call the new feet detector
            carmen_feet_observations moved_obs;
            get_moving_feet_segments(&person_obs, &moved_obs, prev_points, prev_no_points);
  
            //lcmgl = globals_get_lcmgl("moved_leg_obs",1);
            lcmgl = bot_lcmgl_init(lcm, "moved_leg_obs");

            if (1 && lcmgl){// && is_front_laser){ //turn to 1 if u want to draw
                float rtheta = global_robot_pose.theta;
                double pos[3];
                for (int i=0;i<moved_obs.no_obs;i++){
                    float leg_x = global_robot_pose.x + moved_obs.locations[i].center_x * cos(rtheta) - moved_obs.locations[i].center_y * sin(rtheta);
                    float leg_y = global_robot_pose.y + moved_obs.locations[i].center_x * sin(rtheta) + moved_obs.locations[i].center_y*cos(rtheta);
                    pos[0] = leg_x;
                    pos[1] = leg_y;
                    pos[2] = 2.0;  
     
                    lcmglLineWidth (5);
                    lcmglColor3f(1.0, 1.0, .0);
                    lcmglCircle(pos, 0.1);
                } 
            } 

            bot_lcmgl_switch_buffer (lcmgl);

            carmen_point_p moved_person = (carmen_point_p) malloc(moved_obs.no_obs*sizeof(carmen_point_t)); 
            carmen_test_alloc(moved_person);
  
            //copy the observations on to points
            if(active_filter_ind==-1){
                for (int i=0;i< moved_obs.no_obs;i++){
                    moved_person[i].x = moved_obs.locations[i].center_x;
                    moved_person[i].y = moved_obs.locations[i].center_y;
                    moved_person[i].theta = 0.0;
                    //check if there are other filters near by
                    double min_dist = 1000;
                    for(int j=0;j<no_people;j++){
                        double temp_dist = hypot((people_summary[j].mean.x - moved_person[i].x),(people_summary[j].mean.y-moved_person[i].y));
                        if(temp_dist < min_dist){
                            min_dist = temp_dist;
                        }
                    }
                    if(min_dist < 0.8){
                        //fprintf(stderr,"Already tracking a person near - not adding new people\n");
                    }
                    else{   
                        //fprintf(stderr,"----------- Creating Filter\n");	     
                        create_new_person(moved_person[i],std, time_obs);
                    }
                }
            }
            free(moved_person);
        }
    }

    static carmen_point_t prev_velocity = {.0,.0,.0};
    carmen_localize_update_person_hostory(last_robot_position,robot_position,
                                          &prev_velocity, 1);


    carmen_feet_observations used_person_obs;
  
    //Prune observations if they are in the same place as in the last frame - based on motion only
    //used_person_obs = prune_observations(&person_obs);
    used_person_obs = person_obs; //no pruning 
  
 
  
    int full_count = used_person_obs.no_obs;
    carmen_feet_seg* feet_obs = used_person_obs.locations;
    int i,j;

    /*carmen_point_p person_det = (carmen_point_p) malloc(full_count*sizeof(carmen_point_t)); 
      carmen_test_alloc(person_det);

      carmen_point_p full_person_det = (carmen_point_p) malloc(full_count*sizeof(carmen_point_t)); 
      carmen_test_alloc(full_person_det);*/

    carmen_leg_t *person_det = (carmen_leg_t *) malloc(full_count*sizeof(carmen_leg_t)); 
    carmen_test_alloc(person_det);

    carmen_leg_t *full_person_det = (carmen_leg_t *) malloc(full_count*sizeof(carmen_leg_t)); 
    carmen_test_alloc(full_person_det);

    int meta_count = 0;

    if (current_map.map != NULL){
        //fprintf(stderr,"Checking with the map\n");
    }
    //fprintf(stderr,"Obs before pruning : %d\n", full_count);


    //copy the observations on to points
    for (i=0;i<full_count;i++){
        double l_leg_x = feet_obs[i].center_x;
        double l_leg_y = feet_obs[i].center_y;
        if(current_map.map != NULL){
            //fprintf(stderr,"Checking with the map\n");
            float rtheta = global_robot_pose.theta;
            float leg_x = global_robot_pose.x + l_leg_x * cos(rtheta) - l_leg_y * sin(rtheta);
            float leg_y = global_robot_pose.y + l_leg_x * sin(rtheta) + l_leg_y*cos(rtheta);

            carmen_point_t leg_pt;
            memset(&leg_pt,0,sizeof(carmen_point_t));
            leg_pt.x = leg_x;
            leg_pt.y = leg_y;
            int l_x, l_y;

            int occupied = 0;
	
            carmen3d_map3d_global_to_map_index_coordinates(leg_pt, current_map.midpt, current_map.map_zero,
                                                           current_map.config.resolution, &l_x, &l_y);

            int search_size = 1;
	
            for(int j=fmax(0,l_x - search_size); j<=fmin(l_x+search_size,current_map.config.x_size-1); j++){
                for(int k=fmax(0,l_y - search_size); k<=fmin(l_y+search_size,current_map.config.x_size-1); k++){
                    if(current_map.map[j][k] >0.7){
                        occupied = 1;
                        break;
                    }
                }
                if(occupied){
                    break;
                }
            }
            if(!occupied){
                person_det[meta_count].x = l_leg_x;
                person_det[meta_count].y = l_leg_y;
                person_det[meta_count].theta = 0.0;	
                person_det[meta_count].front_laser = feet_obs[i].front_laser;
                meta_count++;
            }
            full_person_det[i].x = l_leg_x;
            full_person_det[i].y = l_leg_y;
            full_person_det[i].theta = 0.0;
            full_person_det[i].front_laser = feet_obs[i].front_laser;
        }
        else{
            person_det[meta_count].x = l_leg_x; 
            person_det[meta_count].y = l_leg_y;
            person_det[meta_count].theta = 0.0;
            person_det[meta_count].front_laser = feet_obs[i].front_laser;
            meta_count++;

            full_person_det[i].x = l_leg_x;
            full_person_det[i].y = l_leg_y;
            full_person_det[i].theta = 0.0;
            full_person_det[i].front_laser = feet_obs[i].front_laser;
        }
    }
  
    person_det = (carmen_leg_t *) realloc(person_det,meta_count*sizeof(carmen_leg_t));
    //fprintf(stderr,"Obs after pruning : %d\n", meta_count);
    //draw possible legs 
    //lcmgl = globals_get_lcmgl("leg_obs",1);
    lcmgl = bot_lcmgl_init(lcm,"leg_obs");

    if (1 && lcmgl){// && is_front_laser){ //turn to 1 if u want to draw
        lcmglColor3f(0, 1.0, 0);
    
        float rtheta = global_robot_pose.theta;
        double pos[3];
        for (i=0;i<meta_count;i++){
            float leg_x = global_robot_pose.x + person_det[i].x * cos(rtheta) - person_det[i].y * sin(rtheta);
            float leg_y = global_robot_pose.y + person_det[i].x * sin(rtheta) + person_det[i].y*cos(rtheta);
            pos[0] = leg_x;
            pos[1] = leg_y;
            pos[2] = 2.0; 

            lcmglColor3f(.0, 1.0, .0);    
            lcmglLineWidth (5);      
            lcmglCircle(pos, 0.1);
            if(debug_mode){
                char seg_info[512];
                sprintf(seg_info,"[%d]%d-%d:%d", feet_obs[i].front_laser, feet_obs[i].start_segment, feet_obs[i].end_segment, 
                        feet_obs[i].start_segment - feet_obs[i].end_segment);
                bot_lcmgl_text(lcmgl, pos, seg_info);
            } 
        }
    } 

    bot_lcmgl_switch_buffer (lcmgl);  

    //prune dead filters
    prune_filters(time_obs);  

    //Filter initiation code
    //create a filter for the observation that is not near any of the current filters
  
    double **dist;
    dist = (double **) malloc(meta_count*sizeof(double *));
    carmen_test_alloc(dist);
    //create a distance matrix between the filters and the observations
    for (i=0;i<meta_count;i++){
        dist[i] = (double *) malloc(no_people*sizeof(double));
        carmen_test_alloc(dist[i]);
        for(j=0;j<no_people;j++){
            dist[i][j] = hypot((people_summary[j].mean.x-person_det[i].x),(people_summary[j].mean.y-person_det[i].y));
        }
    }

    //allocate each observation to the closest person filter 
    int *no_obs_count = (int *) malloc(no_people*sizeof(int)); //no of observations that each filter can see
    carmen_test_alloc(no_obs_count);
    memset(no_obs_count,0,no_people*sizeof(int));

    int original_filter_size = no_people;
  
    int *allocated_to_filter = (int *) malloc(meta_count*sizeof(int)); //no of observations that each filter can see
    carmen_test_alloc(allocated_to_filter);
    memset(allocated_to_filter,0,meta_count*sizeof(int));
  
    int **allocated_observations = (int **) malloc(no_people*sizeof(int *)); //what observations are assigned to each filter
    carmen_test_alloc(allocated_observations);
    for (j=0;j<no_people;j++){
        allocated_observations[j] = (int *) malloc(meta_count*sizeof(int)); //what observations are assigned to each filter
        carmen_test_alloc(allocated_observations[j]);
        memset(allocated_observations[j],-1,meta_count*sizeof(int));
    }

    double filter_creation_angle = M_PI/12; //M_PI/12;
    double filter_creation_dist = 3.0;//3.0;

    if(active_filter_ind >=0){//first do this for the tourguide 
        double dist_to_person = 10000.0;
    
        double person_angle = atan2(people_summary[active_filter_ind].mean.y, people_summary[active_filter_ind].mean.x);
        int use_front_laser = 0;
    
        //fprintf(stderr,"Person Angle : %f\n", person_angle * 180/M_PI);
    
        //63 degrees is the angle at which the rear laser hits the front laser's 90 degree side scans

        if(person_angle < M_PI/180*63 && person_angle >= -M_PI/180*63){
            //fprintf(stderr,"Using only the front laser observations");
            use_front_laser = 1;
        }
        else{
            //fprintf(stderr,"Using only the rear laser observations");
        }
    
        int ind = -1;
        int full_ind = -1;
        if(debug_mode){
            fprintf(stderr,"==============================================\n");
        }
        for (i=0;i<meta_count;i++){ //search through
            if(debug_mode){
                fprintf(stderr,"%d : %f\n",i, dist[i][active_filter_ind]);
            }
      
            if(person_det[i].front_laser != use_front_laser){
                continue;
            }
            if(dist[i][active_filter_ind] < PERSON_OBS_RADIUS){ //within the boundry
                allocated_to_filter[i]=1; //make all these observations out of bounds for the other filters
                if(debug_mode){
                    fprintf(stderr,"Found Observation : %d\n", i);
                }

                if(dist[i][active_filter_ind] < dist_to_person){
                    ind = i;
                    dist_to_person = dist[i][active_filter_ind];
                }
            }
        }
        if(ind ==-1){      
            //do a full search of these observations - through both laser observations
            for (i=0;i<meta_count;i++){ //search through	
                if(dist[i][active_filter_ind] < PERSON_OBS_RADIUS){ //within the boundry
                    allocated_to_filter[i]=1; //make all these observations out of bounds for the other filters
                    if(dist[i][active_filter_ind] < dist_to_person){
                        ind = i;
                        dist_to_person = dist[i][active_filter_ind];
                    }
                }	
            }
        }
        if(ind==-1 && (meta_count <full_count)){ //we still havent found a good observation
            //have not found a good observation yet
            //fprintf(stderr,"Looking through all observations\n");
            for (i=0;i<full_count;i++){ //search through
                if(full_person_det[i].front_laser != use_front_laser){
                    continue;
                }

                double temp_dist =  hypot((people_summary[active_filter_ind].mean.x - full_person_det[i].x),
                                          (people_summary[active_filter_ind].mean.y - full_person_det[i].y));
                /*if(temp_dist < 0.4){ //within the boundry
                  double temp_dist_1 = hypot(full_person_det[i].x, full_person_det[i].y);
                  if(temp_dist_1 < dist_to_wheelchair){
                  full_ind = i;
                  dist_to_wheelchair = temp_dist_1;
                  }
                  }*/
                if(temp_dist < PERSON_OBS_RADIUS){ //within the boundry
                    if(temp_dist < dist_to_person){
                        full_ind = i;
                        dist_to_person = temp_dist;
                    }
                }

            }
        }
    
        //fprintf(stderr,"Ind : %d, Full Ind : %d\n",ind, full_ind);

        //if(no_obs_count[j]==0){
        if(ind==-1 && full_ind==-1){
            carmen_localize_run_person_no_obs(people_filter[active_filter_ind], robot_pos);
            carmen_localize_summarize_person(people_filter[active_filter_ind], &people_summary[active_filter_ind]);
        }
        else if(ind>=0){//(no_obs_count[j]==1){
            int obs = ind;//allocated_observations[j][0];
            carmen_point_t assigned_obs;
            assigned_obs.x = person_det[obs].x;
            assigned_obs.y = person_det[obs].y;
            assigned_obs.theta = person_det[obs].theta;

            double min_leg_dist = 0.5;//used to be 0.5
            //the higher this is the more likely we will get merged to false leg observation if we get near enough

            int closest_leg = -1;

            for (i=0;i<meta_count;i++){ //search through
                if(i==ind || (use_front_laser != person_det[i].front_laser)){
                    continue;
                }
                double leg_dist = hypot(person_det[i].x - person_det[obs].x, person_det[i].y - person_det[obs].y);
                if(leg_dist < min_leg_dist){
                    closest_leg = i;
                    min_leg_dist = leg_dist;
                }
            }      
      
            if(closest_leg >=0){
                //fprintf(stderr,"Found a close leg Dist : %f\n", min_leg_dist);

                guide_pos.x = (person_det[obs].x + person_det[closest_leg].x)/2;
                guide_pos.y = (person_det[obs].y + person_det[closest_leg].y)/2;
	
            }
            else{
                guide_pos.x = person_det[obs].x;
                guide_pos.y = person_det[obs].y;
            }
      
            carmen_point_t person_position_obs;
            person_position_obs.x = guide_pos.x;
            person_position_obs.y = guide_pos.y;      
      
            carmen_localize_run_person_with_obs(people_filter[active_filter_ind], robot_pos, assigned_obs);
            //this tends to mess up the tracking sometimes - not sure if the benifits of this outweigh the cost       
            //carmen_localize_run_person_with_obs(people_filter[active_filter_ind], robot_pos, person_position_obs);
            carmen_localize_summarize_person(people_filter[active_filter_ind], &people_summary[active_filter_ind]);
            time_since[active_filter_ind] = time_obs; 

            //      lcmgl = globals_get_lcmgl("act_pos",1);
            lcmgl = bot_lcmgl_init(lcm, "act_pos");
       
            if(lcmgl){
                double pos[3];
                float rtheta = global_robot_pose.theta;
                float leg_x = global_robot_pose.x + guide_pos.x * cos(rtheta) - guide_pos.y * sin(rtheta);
                float leg_y = global_robot_pose.y + guide_pos.x * sin(rtheta) + guide_pos.y*cos(rtheta);
                guide_gpos.x = leg_x;
                guide_gpos.y = leg_y;
                pos[0] = leg_x;
                pos[1] = leg_y;
                pos[2] = 2.0; 
	  
                lcmglColor3f(1.0, 0.5, .0);

                lcmglLineWidth (5);      
                lcmglCircle(pos, 0.15);
	  
                bot_lcmgl_switch_buffer (lcmgl);
            }
        }
        else if(full_ind>=0){//(no_obs_count[j]==1){
            int obs = full_ind;//allocated_observations[j][0];
            carmen_point_t assigned_obs;
            assigned_obs.x = full_person_det[obs].x;
            assigned_obs.y = full_person_det[obs].y;
            assigned_obs.theta = full_person_det[obs].theta;

            carmen_localize_run_person_with_obs(people_filter[active_filter_ind], robot_pos, assigned_obs);
            carmen_localize_summarize_person(people_filter[active_filter_ind], &people_summary[active_filter_ind]);
            time_since[active_filter_ind] = time_obs; 
        }

        //person location has been updated    
    }

  
    for(j=0;j<no_people;j++){//for each filter search the close observations and pick the one closest to the wheelchair 
        if(j==active_filter_ind){
            continue;
        }
        double dist_to_wheelchair = 10000.0;
        int ind = -1;
        for (i=0;i<meta_count;i++){ //search through
            if(!allocated_to_filter[i] &&(dist[i][j] < 0.4)){ //within the boundry
                double temp_dist = hypot(person_det[i].x, person_det[i].y);
                if(temp_dist < dist_to_wheelchair){
                    ind = i;
                    dist_to_wheelchair = temp_dist;
                }
            }
        }
        //if(no_obs_count[j]==0){
        if(ind==-1){
            carmen_localize_run_person_no_obs(people_filter[j], robot_pos);
            carmen_localize_summarize_person(people_filter[j], &people_summary[j]);
        }
        else if(ind>=0){//(no_obs_count[j]==1){
            allocated_to_filter[ind] = 1;
            int obs = ind;//allocated_observations[j][0];
            carmen_point_t assigned_obs;
            assigned_obs.x = person_det[obs].x;
            assigned_obs.y = person_det[obs].y;
            assigned_obs.theta = person_det[obs].theta;
            carmen_localize_run_person_with_obs(people_filter[j], robot_pos, assigned_obs);
            carmen_localize_summarize_person(people_filter[j], &people_summary[j]);
            time_since[j] = time_obs; 
        }

        /*else{//find the two observations that are closest to eachother 
          int k;
          double dist1 = 100;
          int obs1 = -1;
          int obs2 = -1;
          for(k=0;k<no_obs_count[j];k++){
          int obs = allocated_observations[j][k];
          if(dist[obs][j]<dist1){
          dist1 = dist[obs][j];
          obs1 = obs;
          }
          }

          double dist2 = 100;
          double temp_dist;
      
          for(k=0;k<no_obs_count[j];k++){
          int obs = allocated_observations[j][k];
          if(obs==obs1)
          continue;
          else{
          temp_dist = hypot((person_det[obs].x - person_det[obs1].x),(person_det[obs].y - person_det[obs1].y));
          if(temp_dist < dist2){
          dist2 = temp_dist;
          obs2 = obs;
          }
          }
          }

          if(dist2<0.01){//0.4){
          carmen_point_t mid_point;
          mid_point.x = (person_det[obs1].x + person_det[obs2].x)/2;
          mid_point.y = (person_det[obs1].y + person_det[obs2].y)/2;
          mid_point.theta = (person_det[obs1].theta + person_det[obs2].theta)/2; 
	
          carmen_localize_run_person_with_obs(people_filter[j], robot_pos, mid_point);
          }
          else{
          carmen_localize_run_person_with_obs(people_filter[j], robot_pos, person_det[obs1]);
          }
          carmen_localize_summarize_person(people_filter[j], &people_summary[j]);
          time_since[j] = time_obs;
          } */   
    }

    if(active_filter_ind < 0){ //creating new filters only if there is no active filter - // we should prob change this so it creates only one filer 
        for (i=0;i<meta_count;i++){
            if(allocated_to_filter[i]==0){
                double angle = atan2(person_det[i].y,person_det[i].x);
                double dist = hypot(person_det[i].x,person_det[i].y);
	
                if (fabs(angle)< filter_creation_angle && dist < filter_creation_dist){
                    int near_filter = 0;
                    double min_dist = 1.0;
                    double temp_dist;
                    for(j=0;j<no_people;j++){
                        temp_dist = hypot((people_summary[j].mean.x-person_det[i].x),(people_summary[j].mean.y-person_det[i].y));
                        if(temp_dist< min_dist){
                            min_dist = temp_dist;
                            near_filter = 1;
                            break;
                        }
                    }
                    if(near_filter==0){//no nearby filter was created 
                        carmen_point_t assigned_obs;
                        assigned_obs.x = person_det[i].x;
                        assigned_obs.y = person_det[i].y;
                        assigned_obs.theta = person_det[i].theta;
                        create_new_person(assigned_obs,std, time_obs);
                    }
                }      
            }
        }
    }
    //fprintf(stderr,"Current States => Tracking : %d, Following: %d\n",tracking_state, following_state);    

    if(tracking_state==LOOKING){//if we are in the tracking state we should reaquire 
        double max_dist = 4.0;
        double too_close_dist = 0.5;
        //double min_dist = 0.5;
        double follow_angle = M_PI/6;
        double temp_dist;
        double temp_angle;
        fprintf(stderr,"Looking for person\n");

        //selects the closest observation with-in the angle cone 
        /*for(j=0;j<no_people;j++){
          temp_dist = hypot(people_summary[j].mean.x,people_summary[j].mean.y);
          temp_angle = atan2(people_summary[j].mean.y,people_summary[j].mean.x);
          if((fabs(temp_angle) < follow_angle) 
          && (temp_dist < max_dist) && (temp_dist > too_close_dist)){
	
	
          max_dist = temp_dist;
          active_filter_ind = j;		
          }
          }*/

        double angle_offset = 10000;

        for(j=0;j<no_people;j++){
            temp_dist = hypot(people_summary[j].mean.x,people_summary[j].mean.y);
            temp_angle = atan2(people_summary[j].mean.y,people_summary[j].mean.x);
            if((fabs(temp_angle) < follow_angle) 
               && (temp_dist < max_dist) && (temp_dist > too_close_dist)){
                if(angle_offset > fabs(temp_angle)){
                    angle_offset = fabs(temp_angle);
                    active_filter_ind = j;	
                }
            }
        }
    
        if(active_filter_ind >=0){
            for(int k=0;k<no_people;k++){
                if(k != active_filter_ind){
                    time_since[k] = .0;
                }
            }
            update_tracking_state(FOUND);  //person located	
            //fprintf(stderr,"====== New Person Following Activated =========\n");      
        }
        else{
            //fprintf(stderr,"**** No good filter found **** \n");
        }
    }
    else{
        if(active_filter_ind>=0){
            //fprintf(stderr,"== Current Person Followed is %d : (%f,%f)\n", active_filter_ind, 
            //      people_summary[active_filter_ind].mean.x,people_summary[active_filter_ind].mean.y);
        }
        else{
            //fprintf(stderr,"== No one being tracked \n");
        }
    }

    //fprintf(stderr,"New filter Positions\n");
    for (j=0;j<no_people;j++){
        //fprintf(stderr,"(%f,%f)\t",people_summary[j].mean.x,people_summary[j].mean.y);
    }

    if(no_people>0){    
        erlcm_point_t* test_people_pos = (erlcm_point_t *) 
            realloc(people_pos,no_people*sizeof(erlcm_point_t));
        carmen_test_alloc(test_people_pos);
        people_pos = test_people_pos;
    }

    int person_pos = 0;

    for(j=0;j<no_people;j++){
        people_pos[person_pos].x = people_summary[j].mean.x;
        people_pos[person_pos].y = people_summary[j].mean.y;
        people_pos[person_pos].z = 0.0;
        people_pos[person_pos].yaw = 0.0;
        people_pos[person_pos].pitch = 0.0;
        people_pos[person_pos].roll = people_summary[j].mean.theta;
        person_pos++;
    }

    //publishing a person removed robot and planar lidar message
    float distance_from_person = 0.0;
    static carmen_robot_laser_message* carmen_laser;  
  
    if(carmen_laser == NULL){
        carmen_laser = (carmen_robot_laser_message *) malloc(sizeof(carmen_robot_laser_message));
        carmen_test_alloc(carmen_laser);
    }
    memcpy(carmen_laser,&carmen_robot_frontlaser, sizeof(carmen_robot_laser_message));
    if(carmen_laser->range==NULL){
        carmen_laser->range = (float *) malloc(carmen_robot_frontlaser.num_readings * sizeof(float));
        carmen_test_alloc(carmen_laser->range);
    }
  
    memcpy(carmen_laser->range, carmen_robot_frontlaser.range, 
           carmen_robot_frontlaser.num_readings * sizeof(float));

    double person_leg_range = 1.0;
    int removed_legs = 0;
    if(active_filter_ind >=0){  //if we are tracking a person -remove the observations attached to the person 
        //fprintf(stderr,"Person Location : %f,%f \n",people_summary[active_filter_ind].mean.x, people_summary[active_filter_ind].mean.y);
        for (i=0;i<used_person_obs.no_obs;i++){
            if(!feet_obs[i].front_laser){//this if the front laser - skipping rear laser stuff
                continue;
            }
            distance_from_person = hypot(feet_obs[i].center_x - people_summary[active_filter_ind].mean.x,
                                         feet_obs[i].center_y - people_summary[active_filter_ind].mean.y);
            //fprintf(stderr,"\tDistance : %f\n", distance_from_person);
            if(distance_from_person < person_leg_range){
                removed_legs++;
                int p;
                //fprintf(stderr,"[%d:%d]\n",feet_obs[i].start_segment, feet_obs[i].end_segment);
                for(p = fmax(feet_obs[i].start_segment-3,0); p <=fmin(feet_obs[i].end_segment+3,carmen_robot_frontlaser.num_readings-1);p++){
                    carmen_laser->range[p] = 0.001;  //also might have implication on the max range conversion done in laser driver
                } 	  
            }
        }
        fprintf(stderr,"No of Observations Removed : %d\n", removed_legs);
    
        //remove any points within 0.3 
        for(int p = 0; p < carmen_robot_frontlaser.num_readings ;p++){
            distance_from_person = hypot(points[p].x - people_summary[active_filter_ind].mean.x,
                                         points[p].y - people_summary[active_filter_ind].mean.y);

            //fprintf(stderr,"\tDistance : %f\n", distance_from_person);
            if(distance_from_person < CLEARING_RADIUS){
                carmen_laser->range[p] = 0.001;  	  
            }
        }
    }
    
    robot_laser_pub("PERSON_ROBOT_LASER",carmen_laser,lcm);

    //clearing the rear laser of person observations

    if(clear_rearlaser && have_rear_laser){
        static carmen_robot_laser_message* rear_carmen_laser;
    
        if(rear_carmen_laser == NULL){
            rear_carmen_laser = (carmen_robot_laser_message *) malloc(sizeof(carmen_robot_laser_message));
            carmen_test_alloc(rear_carmen_laser);
        }
        memcpy(rear_carmen_laser,&carmen_robot_rearlaser, sizeof(carmen_robot_laser_message));
        if(rear_carmen_laser->range==NULL){
            rear_carmen_laser->range = (float *) malloc(carmen_robot_rearlaser.num_readings * sizeof(float));
            carmen_test_alloc(rear_carmen_laser->range);
        }
    
        memcpy(rear_carmen_laser->range, carmen_robot_rearlaser.range, 
               carmen_robot_rearlaser.num_readings * sizeof(float));

        double person_leg_range = 1.0;
        int removed_legs = 0;
        if(active_filter_ind >=0){  //if we are tracking a person -remove the observations attached to the person 
            for (i=0;i<used_person_obs.no_obs;i++){
                if(feet_obs[i].front_laser){//this if the rear laser - skipping front laser stuff
                    continue;
                }
                distance_from_person = hypot(feet_obs[i].center_x - people_summary[active_filter_ind].mean.x,
                                             feet_obs[i].center_y - people_summary[active_filter_ind].mean.y);
                //fprintf(stderr,"\tDistance : %f\n", distance_from_person);
                if(distance_from_person < person_leg_range){
                    removed_legs++;
                    int p;
                    //fprintf(stderr,"[%d:%d]\n",feet_obs[i].start_segment, feet_obs[i].end_segment);
                    for(p = fmax(feet_obs[i].start_segment-3,0); p <=fmin(feet_obs[i].end_segment+3,carmen_robot_rearlaser.num_readings-1);p++){
                        rear_carmen_laser->range[p] = 0.001;  //also might have implication on the max range conversion done in laser driver
                    } 	  
                }
            }
            fprintf(stderr,"No of Observations Removed : %d\n", removed_legs);
    
            //remove any points within 0.3 
            for(int p = 0; p < carmen_robot_rearlaser.num_readings ;p++){
                distance_from_person = hypot(points[p].x - people_summary[active_filter_ind].mean.x,
                                             points[p].y - people_summary[active_filter_ind].mean.y);

                //fprintf(stderr,"\tDistance : %f\n", distance_from_person);
                if(distance_from_person < CLEARING_RADIUS){
                    rear_carmen_laser->range[p] = 0.001;  	  
                }
            }
        }
    
        robot_rearlaser_pub("PERSON_REAR_ROBOT_LASER",rear_carmen_laser,lcm);
    }
  
    if(active_filter_ind>=0){//we have a valid person location
        int c_size = fmin(MAX_PERSON_HISTORY-1,valid_history);
        fprintf(stderr,"C_Size : %d\n",c_size);

        if(person_history==NULL){    
            fprintf(stderr,"Creating space for history\n");
            person_history  = (carmen_point_t *)malloc(sizeof(carmen_point_t)*MAX_PERSON_HISTORY);
            memset(person_history,0,sizeof(carmen_point_t)*MAX_PERSON_HISTORY);
            fprintf(stderr,"Done setting up space\n");
        }  
        if(person_dist==NULL){    
            person_dist  = (double *)malloc(sizeof(double)*MAX_PERSON_HISTORY);
            memset(person_dist,0,sizeof(double)*MAX_PERSON_HISTORY);
            fprintf(stderr,"Done setting up space\n");
        }      
        if(person_history_global==NULL){    
            fprintf(stderr,"Creating space for history\n");
            person_history_global  = (carmen_point_t *)malloc(sizeof(carmen_point_t)*MAX_PERSON_HISTORY);
            memset(person_history_global,0,sizeof(carmen_point_t)*MAX_PERSON_HISTORY);
            fprintf(stderr,"Done setting up space\n");
        }  
    
    
        carmen_point_t * temp_history = NULL;
        carmen_point_t * temp_history_global = NULL;
        double *temp_dist = NULL;
        if(c_size>0){
            temp_history  = (carmen_point_t *)malloc(sizeof(carmen_point_t)*c_size);
            memset(temp_history,0,sizeof(carmen_point_t)*c_size);
            //copy the old history to the back
            memcpy(temp_history, person_history, sizeof(carmen_point_t)*c_size);

            temp_dist  = (double *)malloc(sizeof(double)*c_size);
            memset(temp_dist,0,sizeof(double)*c_size);
            //copy the old history to the back
            memcpy(temp_dist, person_dist, sizeof(double)*c_size);

            temp_history_global  = (carmen_point_t *)malloc(sizeof(carmen_point_t)*c_size);
            memset(temp_history_global,0,sizeof(carmen_point_t)*c_size);
            //copy the old history to the back
            memcpy(temp_history_global, person_history_global, sizeof(carmen_point_t)*c_size);
        } 

        //this is fine - if we want relative velocity - which we might for a velocity based controller
        //but this is not really useful for obtaining the person's heading direction 
        //for that we need absolute history, where everything is transformed 

        //call the update person_history_global here - we don't want to transform the current one

        carmen_localize_update_person_hostory(last_robot_position,robot_position,
                                              temp_history_global, c_size);
    
        fprintf(stderr,"Updating Person global history\n");    

        person_history[0].x = guide_pos.x;//guide_gpos.x;//people_summary[active_filter_ind].mean.x;
        person_history[0].y = guide_pos.y;//guide_gpos.y;//people_summary[active_filter_ind].mean.y;
        person_history[0].theta = time_obs; //time is stored here

        person_history_global[0].x = guide_pos.x;//guide_gpos.x;//people_summary[active_filter_ind].mean.x;
        person_history_global[0].y = guide_pos.y;//guide_gpos.y;//people_summary[active_filter_ind].mean.y;
        person_history_global[0].theta = time_obs; //time is stored here

        person_dist[0] = hypot(guide_pos.x, guide_pos.y);

        if(valid_history<MAX_PERSON_HISTORY){
            valid_history++;
        }
    
        if(c_size>0){
            fprintf(stderr,"Coppied over old stuff\n");
            memcpy(person_history+1, temp_history, sizeof(carmen_point_t)*c_size);
            free(temp_history);
            memcpy(person_dist+1, temp_dist, sizeof(double)*c_size);
            free(temp_dist);
            fprintf(stderr,"Done\n");
            memcpy(person_history_global+1, temp_history_global, sizeof(carmen_point_t)*c_size);
            free(temp_history_global);
        }    
        fprintf(stderr,"Person History\n");

        /*carmen_point_t start_point;
          carmen_point_t end_point;
          memset(&start_point, 0, sizeof(carmen_point_t));
          memset(&end_point, 0, sizeof(carmen_point_t));
    
          for(int k=0;k<valid_history;k++){
          if(k < valid_history/2.0){
          start_point.x += person_history[k].x;
          start_point.y += person_history[k].y;
          start_point.theta += person_history[k].theta;
          }
          else{
          end_point.x += person_history[k].x;
          end_point.y += person_history[k].y;
          end_point.theta += person_history[k].theta;
          }
          }*/

        //calculating velocity relative to ground truth
        carmen_point_t start_point;
        carmen_point_t end_point;
        memset(&start_point, 0, sizeof(carmen_point_t));
        memset(&end_point, 0, sizeof(carmen_point_t));
    
        for(int k=0;k<valid_history;k++){
            if(k < valid_history/2.0){
                start_point.x += person_history_global[k].x;
                start_point.y += person_history_global[k].y;
                start_point.theta += person_history_global[k].theta;
            }
            else{
                end_point.x += person_history_global[k].x;
                end_point.y += person_history_global[k].y;
                end_point.theta += person_history_global[k].theta;
            }
        }
    

        start_point.x /= (valid_history/2.0);
        start_point.y /= (valid_history/2.0);
        start_point.theta /= (valid_history/2.0);
    
        end_point.x /= (valid_history/2.0);
        end_point.y /= (valid_history/2.0);
        end_point.theta /= (valid_history/2.0);
    
    
        vel_x = (start_point.x - end_point.x)/(start_point.theta - end_point.theta);
        vel_y = (start_point.y - end_point.y)/(start_point.theta - end_point.theta);

        fprintf(stderr,"velocity : (%f,%f)\n", vel_x, vel_y);

        u_vel_x = vel_x;
        u_vel_y = vel_y;
        if(hypot(vel_x,vel_y)<= 0.2){
            u_vel_x = prev_velocity.x;
            u_vel_y = prev_velocity.y;
        }
    
        if(lcmgl){
            //we should have a threshold to decide if the person is not moving 
            //if not moving maybe use the old qualifying velocities??

            /*if(hypot(vel_x,vel_y)> 0.2){
              prev_vx = vel_x;
              prev_vy = vel_y;
              }*/

            //would this need to be transformed to current reference??
            /*
              vel_x =  prev_vx;
              vel_y =  prev_vy;*/
      

            //double alpha = atan2(guide_pos.y, guide_pos.x);
            double rtheta = global_robot_pose.theta; //+ alpha;
            double vel_g_x = u_vel_x*cos(rtheta) - u_vel_y*sin(rtheta);
            double vel_g_y = u_vel_x*sin(rtheta) + u_vel_y*cos(rtheta);           
      
            if(hypot(vel_x, vel_y)> 0.2){
                lcmglColor3f(0.0, 1.0, 0.2);
            }
            else{
                lcmglColor3f(1.0, 0.0, 1.0);
            }
      
            lcmglBegin(GL_LINES);
            lcmglVertex3d(guide_gpos.x, guide_gpos.y, 2.0);
            lcmglVertex3d(guide_gpos.x + vel_g_x, guide_gpos.y+ vel_g_y, 2.0);
            lcmglEnd();

            //temp calculation - done here 
            double alpha = atan2(u_vel_y, u_vel_x);

            //alternative positions
            double p1_alpha =  bot_mod2pi(alpha + M_PI/2);
            double p2_alpha =  bot_mod2pi(alpha - M_PI/2);

            double distance_to_side = 1.0;
     
            double p1_pos[3] = {.0,.0,.0};
            double p2_pos[3] = {.0,.0,.0};
 
            double p1_x = distance_to_side * cos(p1_alpha);
            double p1_y = distance_to_side * sin(p1_alpha);

            p1_pos[0] = guide_gpos.x + p1_x*cos(rtheta) - p1_y*sin(rtheta);
            p1_pos[1] = guide_gpos.y + p1_x*sin(rtheta) + p1_y*cos(rtheta);


            double p2_x = distance_to_side * cos(p2_alpha);
            double p2_y = distance_to_side * sin(p2_alpha);
      
            p2_pos[0] = guide_gpos.x + p2_x*cos(rtheta) - p2_y*sin(rtheta);
            p2_pos[1] = guide_gpos.y + p2_x*sin(rtheta) + p2_y*cos(rtheta);
     


            lcmglLineWidth (8);    
            //left is red
            lcmglColor3f(1.0, 0.0, 0.0);
            lcmglCircle(p1_pos, 0.1);
            //right is 
            lcmglColor3f(1.0, 1.0, 0.0);
            lcmglCircle(p2_pos, 0.1);

            bot_lcmgl_switch_buffer (lcmgl);
        }


        if(hypot(vel_x,vel_y)> 0.2){
            prev_velocity.x = vel_x;
            prev_velocity.y = vel_y;
        }

        /*double vel_from_chair = .0;
          if(valid_history>1){
          vel_from_chair =  (person_dist[valid_history-1] - person_dist[0])/(person_history[valid_history-1].theta - person_history[0].theta);
          //vel_from_chair =  (person_history[valid_history-1].x - person_history[0].x)/(person_history[valid_history-1].theta - person_history[0].theta);
          fprintf(stderr,"Approach Velocity  : %f\n", vel_from_chair);
          rel_vel = vel_from_chair;
          }
    
          lcmgl = globals_get_lcmgl("velocity",1);
          if(lcmgl){
          double alpha = atan2(guide_pos.y, guide_pos.x);
          double rtheta = global_robot_pose.theta + alpha;
          double vel_g_x = vel_from_chair*cos(rtheta);//vel_x*cos(rtheta) - vel_y*sin(rtheta);
          double vel_g_y = vel_from_chair*sin(rtheta);//vel_x*sin(rtheta) + vel_y*cos(rtheta);

          lcmglColor3f(1.0, 0.0, 1.0);
          lcmglBegin(GL_LINES);
          lcmglVertex3d(guide_gpos.x, guide_gpos.y, 2.0);
          lcmglVertex3d(guide_gpos.x + vel_g_x*2.0, guide_gpos.y+ vel_g_y*2.0, 2.0);
          lcmglEnd();
          bot_lcmgl_switch_buffer (lcmgl);
          }*/


        //lcmgl = globals_get_lcmgl("person_hsitory",1);
        lcmgl = bot_lcmgl_init(lcm, "person_history");

        if(lcmgl){
            lcmglColor3f(1.0, .0, 0);
            //lcmglBegin(GL_LINES);

            //draw the person heading also 
      
            for(int k=0;k<valid_history;k++){

                float rtheta = global_robot_pose.theta;
                float leg_x = global_robot_pose.x + person_history_global[k].x * cos(rtheta) - person_history_global[k].y * sin(rtheta);
                float leg_y = global_robot_pose.y + person_history_global[k].x* sin(rtheta) + person_history_global[k].y*cos(rtheta);
	
                double pos[3] = {leg_x, leg_y, 0};
                //lcmglVertex3d(leg_x, leg_y, 2.0);
                lcmglColor3f(1.0, .0, .0);

                lcmglLineWidth (5);      
                lcmglCircle(pos, 0.02);
	

                //lcmglVertex3d(person_history[k].x, person_history[k].y, 2.0);
                //fprintf(stderr,"\t[%d] =>(%f,%f)\n",k, person_history[k].x, person_history[k].y);
                //*****draw person trajectory
            }
            // lcmglEnd();
            bot_lcmgl_switch_buffer (lcmgl);
        }

        /*lcmgl = globals_get_lcmgl("person_hsitory",1);
          if(lcmgl){
          lcmglColor3f(1.0, 1.0, 0);
          lcmglBegin(GL_LINES);
      
          for(int k=0;k<valid_history;k++){

          float rtheta = global_robot_pose.theta;
          float leg_x = global_robot_pose.x + person_history[k].x * cos(rtheta) - person_history[k].y * sin(rtheta);
          float leg_y = global_robot_pose.y + person_history[k].x* sin(rtheta) + person_history[k].y*cos(rtheta);
	
          lcmglVertex3d(leg_x, leg_y, 2.0);
          //lcmglVertex3d(person_history[k].x, person_history[k].y, 2.0);
          //fprintf(stderr,"\t[%d] =>(%f,%f)\n",k, person_history[k].x, person_history[k].y);
          //*****draw person trajectory
          }
          lcmglEnd();
          bot_lcmgl_switch_buffer (lcmgl);
          }*/
        //memcpy(person_history, person_history, sizeof(carmen_point_t)*c_size);
    }

    //publish_people_message();
    double relative_vel  = 0; //we do not calculate if for now - prob should do 
    double vel_mag = hypot(u_vel_x, u_vel_y);
    double vel_heading = atan2(u_vel_y,u_vel_x);
    
    person_heading_average = person_heading_average * (1 - learning_rate) + vel_heading * learning_rate;
    person_vel_average = person_vel_average * (1 - learning_rate) + vel_mag * learning_rate;

    fprintf(stderr,"Guide Msg\n");
    publish_guide_msg(vel_mag, vel_heading, relative_vel);
    //publish_guide_msg(person_vel_average, person_heading_average, relative_vel);

    //lcmgl = globals_get_lcmgl("velocity",1);
    lcmgl = bot_lcmgl_init(lcm, "velocity");
 
    if(no_people >=0){//draw person if we have a person being tracked
        //lcmgl = globals_get_lcmgl("person",1);
        lcmgl = bot_lcmgl_init(lcm, "person");

        double pos[3];

        for(i=0;i< no_people;i++){
            float person_x = .0, person_y = .0;
            float theta = global_robot_pose.theta;
            if(i==active_filter_ind){
                person_x = guide_pos.x;
                person_y = guide_pos.y;
            }
            else{
                person_x = people_summary[i].mean.x;
                person_y = people_summary[i].mean.y;
            }
            float goal_x = global_robot_pose.x + person_x * cos(theta) - person_y*sin(theta);
            float goal_y = global_robot_pose.y + person_x * sin(theta) + person_y*cos(theta);


            pos[0] = goal_x;
            pos[1] = goal_y;
            pos[2] = 2.0;  

            if (1 && lcmgl) {//turn to 1 if u want to draw
                lcmglLineWidth (5);
                if(i==active_filter_ind){
                    /*lcmglColor3f(1.0, .0, 1.0);
                      lcmglCircle(last_person_pos, 0.15);
                      vel[0] = (goal_x - last_person_pos[0])/ (time_obs - prev_time);
                      vel[1] = (goal_y - last_person_pos[1])/ (time_obs - prev_time);
                      double v_theta = atan2(vel[1],vel[0]);

                      lcmglBegin(GL_LINES);
                      lcmglVertex3d(goal_x, goal_y, pos[2]);
                      lcmglVertex3d(goal_x + 1.0 * cos(v_theta), 
                      goal_y + 1.0 * sin(v_theta), 
                      pos[2]);
                      lcmglEnd();*/
		    
                    last_person_pos[0] = goal_x;
                    last_person_pos[1] = goal_y;

                    lcmglColor3f(1.0, .0, .0);
                }
                else{
                    lcmglColor3f(.0, 1.0, 1.0);
                }
                lcmglCircle(pos, 0.15);
            }
        }
        bot_lcmgl_switch_buffer (lcmgl);
    }
  
    fprintf(stderr,"No of filters %d \n", no_people);
    fprintf(stderr,"------------------------------------------------------------\n");
    free(person_det);
    free(full_person_det);

    for (i=0;i<meta_count;i++){
        free(dist[i]);
    }
    free(dist);

    for (j=0;j<original_filter_size;j++){
        free(allocated_observations[j]);
    }
    free(allocated_observations);
    free(no_obs_count);
    prev_time = time_obs;
    free(allocated_to_filter);
    free(used_person_obs.locations);
    free(prev_points);
    prev_points = points;
    prev_no_points = no_points;

    return 1;
}

void lcm_robotlaser_to_carmen_laser(erlcm_robot_laser_t *msg, carmen_robot_laser_message *robot, double laser_offset)
{
    //convert the lcm message to a carmen message because all the functions have been 
    //already written to handle carmen messages

    robot->config.accuracy = .1;
    robot->config.laser_type = HOKUYO_UTM;
    robot->config.remission_mode = REMISSION_NONE;
    robot->config.angular_resolution = msg->laser.radstep;
    robot->config.fov = msg->laser.nranges * msg->laser.radstep;
    robot->config.maximum_range = 30; 
    robot->config.start_angle = msg->laser.rad0;
    robot->num_readings = msg->laser.nranges;
    robot->range = msg->laser.ranges;
    robot->robot_pose.x = msg->pose.pos[0];
    robot->robot_pose.y = msg->pose.pos[1];
    double rpy[3] = {0,0,0};  
    bot_quat_to_roll_pitch_yaw (msg->pose.orientation, rpy) ;
    robot->robot_pose.theta = rpy[2];
    robot->laser_pose.x = robot->robot_pose.x + laser_offset*cos(rpy[2]);
    robot->laser_pose.y = robot->robot_pose.y + laser_offset*sin(rpy[2]);
    robot->laser_pose.theta = rpy[2];
    robot->timestamp = msg->utime/1e6;
}

void lcm_planar_laser_to_carmen_laser(bot_core_planar_lidar_t *laser, carmen_robot_laser_message *robot, 
                                      double laser_offset, double laser_ang_offset)
{
    //convert the lcm message to a carmen message because all the functions have been 
    //already written to handle carmen messages

    robot->config.accuracy = .1;
    robot->config.laser_type = HOKUYO_UTM;
    robot->config.remission_mode = REMISSION_NONE;
    robot->config.angular_resolution = laser->radstep;
    robot->config.fov = laser->nranges * laser->radstep;
    robot->config.maximum_range = 30; 
    robot->config.start_angle = laser->rad0 + laser_ang_offset;
    robot->num_readings = laser->nranges;
    robot->range = laser->ranges;
    if(odom_msg!=NULL){
        robot->robot_pose.x = odom_msg->x;//global_robot_pose.x;
        robot->robot_pose.y = odom_msg->y;//global_robot_pose.y;
        robot->robot_pose.theta = carmen_normalize_theta(odom_msg->theta);//global_robot_pose.theta;
    }
    else{
        robot->robot_pose.x = .0;//global_robot_pose.x;
        robot->robot_pose.y = .0;//global_robot_pose.y;
        robot->robot_pose.theta = .0;//global_robot_pose.theta;
    }
    robot->laser_pose.x = robot->robot_pose.x + laser_offset*cos(robot->robot_pose.theta);
    robot->laser_pose.y = robot->robot_pose.y + laser_offset*sin(robot->robot_pose.theta);
    robot->laser_pose.theta = robot->robot_pose.theta;
    robot->timestamp = laser->utime/1e6;
}


static void multi_gridmap_handler(const lcm_recv_buf_t *rbuf, 
                                  const char *channel, 
                                  const erlcm_multi_gridmap_t *msg, 
                                  void *user)
{
    fprintf(stderr,"=M= : Current Floor : %d\n",msg->current_floor_ind);

    static erlcm_gridmap_t* staticmsg = NULL;
    if (staticmsg != NULL) {
        erlcm_gridmap_t_destroy(staticmsg);
    }
    staticmsg = erlcm_gridmap_t_copy(&msg->maps[msg->current_floor_ind].gridmap);
  
    /*if(current_map !=NULL){
      erlcm_gridmap_t_destroy(current_map);
      }
      current_map =  erlcm_gridmap_t_copy(msg->maps[msg->current_floor].gridmap);	*/
    if (current_map.map != NULL)
        free(current_map.map);
    if (current_map.complete_map != NULL)
        free(current_map.complete_map);
    carmen3d_map_uncompress_lcm_map(&current_map, staticmsg);
    fprintf(stderr,"Acquired gridmap\n");
}

static void mapserver_gridmap_handler(const lcm_recv_buf_t *rbuf, 
                                      const char *channel, 
                                      const erlcm_gridmap_t *msg, 
                                      void *user)
{
    static erlcm_gridmap_t* staticmsg = NULL;
    if (staticmsg != NULL) {
        erlcm_gridmap_t_destroy(staticmsg);
    }
    staticmsg = erlcm_gridmap_t_copy(msg);
  
    /*if(current_map !=NULL){
      erlcm_gridmap_t_destroy(current_map);
      }
      current_map =  erlcm_gridmap_t_copy(msg->maps[msg->current_floor].gridmap);	*/
    if (current_map.map != NULL)
        free(current_map.map);
    if (current_map.complete_map != NULL)
        free(current_map.complete_map);
    carmen3d_map_uncompress_lcm_map(&current_map, staticmsg);
    current_map.map_zero.x = .0;
    current_map.map_zero.y = .0;
    fprintf(stderr,"Acquired gridmap\n");
}


void speech_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const erlcm_speech_cmd_t * msg,
                    void * user  __attribute__((unused)))
{
    static erlcm_speech_cmd_t* staticmsg = NULL;

    if (staticmsg != NULL) {
        erlcm_speech_cmd_t_destroy(staticmsg);
    }
    staticmsg = erlcm_speech_cmd_t_copy(msg);
    char* cmd = staticmsg->cmd_type;
    char* property = staticmsg->cmd_property;

    //person tracker related speech commands
    if(strcmp(cmd,"TRACKER")==0){
        if(strcmp(property,"START_LOOKING")==0){
            //guide has asked us to look for him
            update_tracking_state(LOOKING);
            fprintf(stderr,"Starting to track\n");
        }    
        else if(strcmp(property,"STATUS")==0){
            querry_tracking_state();
        }
    }
    if(strcmp(cmd,"FOLLOWER")==0){
        if(strcmp(property,"START_FOLLOWING")==0){
            //guide has asked us to follow him 
            //this we use only if we do not have the person in sight
            if(active_filter_ind ==-1){
                update_tracking_state(LOOKING);
                fprintf(stderr,"Starting to track\n");
            }
        }  
    }
}

void lcm_frontlaser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const erlcm_robot_laser_t * msg,
                            void * user  __attribute__((unused)))
{  
    static erlcm_robot_laser_t* staticmsg = NULL;

    if (staticmsg != NULL) {
        erlcm_robot_laser_t_destroy(staticmsg);
    }
    staticmsg = erlcm_robot_laser_t_copy(msg);

    lcm_robotlaser_to_carmen_laser(staticmsg,&carmen_robot_frontlaser, param.front_laser_offset);
    have_front_laser = 1;

    fprintf(stderr,"Mode : %d\n", tourguide_mode);
  
    /*if(!tourguide_mode){
      fprintf(stderr,"Navigation Mode\n");
      active_filter_ind = -1;
      last_robot_position.x = carmen_robot_frontlaser.robot_pose.x;
      last_robot_position.y = carmen_robot_frontlaser.robot_pose.y;
      last_robot_position.theta = carmen_robot_frontlaser.robot_pose.theta;
      return;
      }*/
    if(multi_people_tracking){
        fprintf(stderr,"Multi Person tracking\n");
        detect_person_both_multi();
    }
    else{
        fprintf(stderr,"Single Person tracking\n");
        detect_person_both_basic();
    }

    last_robot_position.x = carmen_robot_frontlaser.robot_pose.x;
    last_robot_position.y = carmen_robot_frontlaser.robot_pose.y;
    last_robot_position.theta = carmen_robot_frontlaser.robot_pose.theta;
  
  
}

void lcm_rearlaser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const erlcm_robot_laser_t * msg,
                           void * user  __attribute__((unused)))
{
    static erlcm_robot_laser_t* staticmsg = NULL;

    if (staticmsg != NULL) {
        erlcm_robot_laser_t_destroy(staticmsg);
    }
    staticmsg = erlcm_robot_laser_t_copy(msg);

    //we only add update the rear laser data - we only run the localize once 
    lcm_robotlaser_to_carmen_laser(staticmsg,&carmen_robot_rearlaser, param.rear_laser_offset);
    have_rear_laser = 1;
}

void lcm_base_odometry_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                               const char * channel __attribute__((unused)), 
                               const erlcm_raw_odometry_msg_t * msg,
                               void * user  __attribute__((unused)))
{
    //static erlcm_raw_odometry_msg_t* staticmsg = NULL;
    fprintf(stderr,"ODOM\n");
    if (odom_msg != NULL) {
        erlcm_raw_odometry_msg_t_destroy(odom_msg);
    }
    odom_msg = erlcm_raw_odometry_msg_t_copy(msg);

    //lcm_odometry_to_carmen_odometry(staticmsg,&carmen_robot_latest_odometry);
}

void lcm_frontplanar_laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                                   const bot_core_planar_lidar_t * msg,
                                   void * user  __attribute__((unused)))
{
    static bot_core_planar_lidar_t* staticmsg = NULL;

    if (staticmsg != NULL) {
        bot_core_planar_lidar_t_destroy(staticmsg);
    }
    staticmsg = bot_core_planar_lidar_t_copy(msg);

    lcm_planar_laser_to_carmen_laser(staticmsg,&carmen_robot_frontlaser, param.front_laser_offset, param.front_laser_angle_offset);
    have_front_laser = 1;
  
    fprintf(stderr,"Mode : %d\n", tourguide_mode);
  
    /*if(!tourguide_mode){
      fprintf(stderr,"Navigation Mode\n");
      active_filter_ind = -1;
      last_robot_position.x = carmen_robot_frontlaser.robot_pose.x;
      last_robot_position.y = carmen_robot_frontlaser.robot_pose.y;
      last_robot_position.theta = carmen_robot_frontlaser.robot_pose.theta;
      return;
      }*/

    //detect_person_both();
    if(multi_people_tracking){
        detect_person_both_multi();
    }
    else{
        fprintf(stderr,"Single Person tracking\n");
        detect_person_both_basic();
    }
  
    last_robot_position.x = carmen_robot_frontlaser.robot_pose.x;
    last_robot_position.y = carmen_robot_frontlaser.robot_pose.y;
    last_robot_position.theta = carmen_robot_frontlaser.robot_pose.theta;
}

void lcm_rearplanar_laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                                  const bot_core_planar_lidar_t * msg,
                                  void * user  __attribute__((unused)))
{
    static bot_core_planar_lidar_t* staticmsg = NULL;

    if (staticmsg != NULL) {
        bot_core_planar_lidar_t_destroy(staticmsg);
    }
    staticmsg = bot_core_planar_lidar_t_copy(msg);

    lcm_planar_laser_to_carmen_laser(staticmsg,&carmen_robot_rearlaser, param.rear_laser_offset, param.rear_laser_angle_offset);
    have_rear_laser = 1;
}



static void pose_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                         const bot_core_pose_t * msg, void * user  __attribute__((unused))) 
{
    static bot_core_pose_t *prevUpdate = NULL;

    if (prevUpdate == NULL) {
        prevUpdate = bot_core_pose_t_copy(msg);
    }

    double rpy[3];
    bot_quat_to_roll_pitch_yaw(msg->orientation, rpy);
  
    global_robot_pose.x = msg->pos[0];
    global_robot_pose.y = msg->pos[1];
    global_robot_pose.theta = rpy[2];

    fprintf(stderr,"Pos (x,y,theta) : (%f,%f,%f)\n",
            msg->pos[0],msg->pos[1],rpy[2]);    
}

void read_parameters(carmen3d_localize_param_p param)
{
    //BotConf *c = globals_get_config();
    BotParam * c= bot_param_new_from_server(lcm, 1);

    char key[2048];
    int no_particles = 0;
    sprintf(key, "%s.no_particles", "person_tracking");

    //no particles 
    if (0 != bot_param_get_int(c, key, &no_particles)){
        fprintf(stderr,"\tError Reading Params\n");
    }else{
        fprintf(stderr,"\tNo of Particles : %d\n",no_particles);
    }
    param->num_particles = no_particles;
    
    //laser params
    double position[3];
    sprintf(key, "%s.%s.%s.position", "calibration", "planar_lidars", "SKIRT_FRONT");
    if(3 != bot_param_get_double_array(c, key, position, 3)){
        fprintf(stderr,"\tError Reading Params\n");
    }else{
        fprintf(stderr,"\tFront Laser Pos : (%f,%f,%f)\n",position[0], position[1], position[2]);
    }
    param->front_laser_offset = position[0];
    front_laser_offset.dx = position[0];
    front_laser_offset.dy = position[1];
      
    sprintf(key, "%s.%s.%s.position", "calibration", "planar_lidars", "SKIRT_REAR");
    if(3 != bot_param_get_double_array(c, key, position, 3)){
        fprintf(stderr,"\tError Reading Params\n");
    }else{
        fprintf(stderr,"\tRear Laser Pos : (%f,%f,%f)\n",position[0], position[1], position[2]);
    }
    param->rear_laser_offset = position[0];

    rear_laser_offset.dx = position[0];
    rear_laser_offset.dy = position[1];

    double rpy[3];
    sprintf(key, "%s.%s.%s.rpy", "calibration", "planar_lidars", "SKIRT_FRONT");
    if(3 != bot_param_get_double_array(c, key, rpy, 3)){
        fprintf(stderr,"\tError Reading Params\n");
    }else{
        fprintf(stderr,"\tFront Laser RPY : (%f,%f,%f)\n",rpy[0], rpy[1], rpy[2]);
    }
    front_laser_offset.dtheta = carmen_degrees_to_radians(rpy[2]);
    param->front_laser_angle_offset  = carmen_degrees_to_radians(rpy[2]);
  
    sprintf(key, "%s.%s.%s.rpy", "calibration", "planar_lidars", "SKIRT_REAR");
    if(3 != bot_param_get_double_array(c, key, rpy, 3)){
        fprintf(stderr,"\tError Reading Params\n");
    }else{
        fprintf(stderr,"\tRear Laser RPY : (%f,%f,%f)\n",rpy[0], rpy[1], rpy[2]);
    }
    rear_laser_offset.dtheta = carmen_degrees_to_radians(rpy[2]);
    param->rear_laser_angle_offset = carmen_degrees_to_radians(rpy[2]);
}

void shutdown_tracker(int x)
{
    if(x == SIGINT) {
        carmen_verbose("Disconnecting from IPC network.\n");
        exit(1);
    }
}

int main(int argc, char **argv)
{
    //carmen3d_localize_param_t param;

    int c;
    int use_planar_laser = 0;  

    while ((c = getopt (argc, argv, "lfmhdr")) >= 0) {
        switch (c) {
        case 'l':
            use_planar_laser = 1;
            break;
        case 'f':
            multi_people_tracking = 1;
            fprintf(stderr,"Tracking multiple people\n");
            break;
        case 'm':
            use_map = 1;
            break;
        case 'd':
            debug_mode = 1;
            fprintf(stderr,"Debug");      
            break;      
        case 'r':
            clear_rearlaser = 1;
            fprintf(stderr,"Clearing rearlaser");
            break;
        case 'h':
        case '?':
            fprintf (stderr, "Usage: %s [-l]\n\
                        Options:\n			\
                        -l     Use Planar Laser \n"
                     , argv[0]);
            return 1;
        }
    }

    memset(&guide_pos, 0, sizeof(erlcm_point_t));
    memset(&guide_pos, 0, sizeof(erlcm_point_t));
    memset(&guide_gpos, 0, sizeof(erlcm_point_t));
    memset(&front_laser_offset, 0, sizeof(laser_offset_t));
    memset(&rear_laser_offset, 0, sizeof(laser_offset_t));
    memset(&carmen_robot_frontlaser,0,sizeof(carmen_robot_frontlaser));
    memset(&carmen_robot_rearlaser,0,sizeof(carmen_robot_rearlaser));

    current_map.map = NULL;
    current_map.complete_map = NULL;
    /* Setup exit handler */
    signal(SIGINT, shutdown_tracker);

    lcm = bot_lcm_get_global(NULL);//globals_get_lcm();

    read_parameters(&param);

    fprintf(stderr,"Parameters No of Particles : %d \n",param.num_particles);

    global_person_position.x = 0.0;
    global_person_position.y = 0.0;
    global_person_position.theta = 0.0;

    std.x = 1.0;
    std.y = 1.0;
    std.theta = carmen_degrees_to_radians(4.0); 


    initialize_people_filters(actual_filter_size);
  
    global_robot_pose.x = 0.0;
    global_robot_pose.y = 0.0;
    global_robot_pose.theta = 0.0;  

    fprintf(stderr,"Subscribing to LCM Laser\n");

    if(use_planar_laser){
        bot_core_planar_lidar_t_subscribe(lcm,"SKIRT_FRONT", lcm_frontplanar_laser_handler, NULL);
        bot_core_planar_lidar_t_subscribe(lcm,"SKIRT_FRONT", lcm_frontplanar_laser_handler, NULL);
        bot_core_planar_lidar_t_subscribe(lcm,"SKIRT_REAR", lcm_rearplanar_laser_handler, NULL);

        erlcm_raw_odometry_msg_t_subscribe(lcm, "ODOMETRY", lcm_base_odometry_handler,NULL);
    }
    else{
        erlcm_robot_laser_t_subscribe(lcm,"REAR_ROBOT_LASER", lcm_rearlaser_handler, NULL);
        erlcm_robot_laser_t_subscribe(lcm,"ROBOT_LASER", lcm_frontlaser_handler, NULL);
    }
    bot_core_pose_t_subscribe(lcm, POSE_CHANNEL, pose_handler, NULL);
    if(use_map){
        fprintf(stderr,"Using the map to remove person observations\n");
        erlcm_multi_gridmap_t_subscribe(lcm, "MULTI_FLOOR_MAPS", multi_gridmap_handler, NULL); 
        erlcm_gridmap_t_subscribe(lcm, "MAP_SERVER", mapserver_gridmap_handler, NULL); 
    }

    //prob should handle only the tracker related stuff here
    erlcm_speech_cmd_t_subscribe(lcm, "PERSON_TRACKER", speech_handler, NULL);
  
    //for messages sent from the N810
    erlcm_speech_cmd_t_subscribe(lcm, "TABLET_FOLLOWER", speech_handler, NULL);

    erlcm_tagged_node_t_subscribe(lcm, "WHEELCHAIR_MODE", mode_handler, NULL);

    while (1)
        lcm_handle (lcm);

    return 0;
}
