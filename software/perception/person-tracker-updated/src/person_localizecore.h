#ifndef CARMEN_PERSON_LOCALIZECORE_H
#define CARMEN_PERSON_LOCALIZECORE_H

#include <er_carmen/carmen.h>
#include <interfaces/localize3dcore.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Create (allocate memory for) a new person particle filter **/
carmen3d_localize_particle_filter_p 
carmen3d_localize_particle_filter_person_new(carmen3d_localize_param_p param);


/** Creates a Gaussian distribution of particles 
 *
 *  @param filter Particle filter structure the function is applied to.
 *  @param mean mean of the Gaussian
 *  @param std std var of the Gaussian
**/
void 
carmen_localize_initialize_particles_gaussian_person(carmen3d_localize_particle_filter_p filter,
					      carmen_point_t mean, 
					      carmen_point_t std);

void carmen_localize_update_filter_center(//carmen3d_localize_particle_filter_p filter,
					    carmen_point_t old_odometry_position, 
					    carmen_point_t odometry_position, 
					    carmen_point_p filter_center);

void carmen_localize_update_person_hostory(carmen_point_t old_odometry_position, 
					   carmen_point_t odometry_position, 
					   carmen_point_p person_history, 
					   int history_length);

void carmen_localize_update_person_heading(carmen_point_t old_odometry_position, 
					   carmen_point_t odometry_position, 
					   double *heading,
					   int valid_history);
  
  /*int 
carmen_localize_run_person(carmen3d_localize_particle_filter_p filter, 
			   carmen_robot_laser_message *laser, 
			   carmen_point_p all_person_observations,
			   int no_obs, 
			   carmen_point_t filter_center
			   );*/

int
carmen_localize_run_person_with_obs(carmen3d_localize_particle_filter_p filter, 
                                    carmen_point_t robot_position, 
                                    double heading, double velocity, 
                                    carmen_point_t person_observation);
  
int
carmen_localize_run_person_no_obs(carmen3d_localize_particle_filter_p filter, 
                                  double heading, double velocity, 
                                  carmen_point_t robot_position);

void 
carmen_localize_summarize_person(carmen3d_localize_particle_filter_p filter, 
				 carmen3d_localize_summary_p summary);


#ifdef __cplusplus
}
#endif

#endif
