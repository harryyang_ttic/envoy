//#include <carmen/localize_motion.h>
#include "person_localizecore.h"

/* initialize memory necessary for holding temporary sensor weights */

static void initialize_temp_weights(carmen3d_localize_particle_filter_p filter)
{
    int i;
  
    filter->temp_weights = (float **)calloc(filter->param->num_particles,
                                            sizeof(float *));
    carmen_test_alloc(filter->temp_weights);
    for(i = 0; i < filter->param->num_particles; i++) {
        filter->temp_weights[i] = (float *)calloc(MAX_BEAMS_PER_SCAN, sizeof(float));
        carmen_test_alloc(filter->temp_weights[i]);
    }
}

/* allocate memory for a new particle filter */
carmen3d_localize_particle_filter_p 
carmen3d_localize_particle_filter_person_new(carmen3d_localize_param_p param)
{
    carmen3d_localize_particle_filter_p filter;

    /* allocate the particle filter */
    filter = 
        (carmen3d_localize_particle_filter_p)
        calloc(1, sizeof(carmen3d_localize_particle_filter_t));
    carmen_test_alloc(filter);

    /* set the parameters */
    filter->param = param;

    /* allocate the initial particle set */
    filter->particles = 
        (carmen3d_localize_particle_p)calloc(filter->param->num_particles,
                                           sizeof(carmen3d_localize_particle_t));
    carmen_test_alloc(filter->particles);

    /* initialize the temporary weights */
    initialize_temp_weights(filter);
  
    /* filter has not been initialized */
    filter->initialized = 0;
    filter->first_odometry = 1;
    filter->global_mode = 0;
    filter->distance_travelled = 0;

    filter->param->laser_skip = 0; /* will be automatically initialized later on */
  
    return filter;
}

void carmen_localize_initialize_particles_gaussians_person(carmen3d_localize_particle_filter_p filter,
                                                           int num_modes,
                                                           carmen_point_t *mean,
                                                           carmen_point_t *std)
{
    int i, j, each, start, end;
    float x, y, theta;

    each = (int)floor(filter->param->num_particles / (float)num_modes);
    for(i = 0; i < num_modes; i++) {
        start = i * each;
        if(i == num_modes - 1)
            end = filter->param->num_particles;
        else
            end = (i + 1) * each;

        for(j = start; j < end; j++) {
            x = carmen_gaussian_random(mean[i].x, std[i].x);
            y = carmen_gaussian_random(mean[i].y, std[i].y);
            theta = carmen_normalize_theta(carmen_gaussian_random(mean[i].theta, 
                                                                  std[i].theta));
            filter->particles[j].x = x;
            filter->particles[j].y = y;
            filter->particles[j].theta = theta;
            filter->particles[j].weight = 0.0;
        }
    }
    filter->initialized = 1;
    filter->first_odometry = 1;
    if (num_modes < 2)
        filter->global_mode = 0;
    else
        filter->global_mode = 1;
    filter->distance_travelled = 0;
}


void carmen_localize_initialize_particles_gaussian_person(carmen3d_localize_particle_filter_p filter,
                                                          carmen_point_t mean, 
                                                          carmen_point_t std)
{
    carmen_localize_initialize_particles_gaussians_person(filter, 1, &mean, &std);
}


void carmen_localize_update_person_hostory(carmen_point_t old_odometry_position, 
                                           carmen_point_t odometry_position, 
                                           carmen_point_p person_history, 
                                           int valid_history)
{
    int backwards;
    double delta_t, delta_theta;
    double dx, dy, odom_theta;

    double dx0,dy0, theta_old;

    theta_old = old_odometry_position.theta;
  
    //w.r.t. the global measurements 

    dx0 = (odometry_position.x - old_odometry_position.x);  
    dy0 = (odometry_position.y - old_odometry_position.y);

    //converted to the robot's previous frame of reference

    dx = dx0 * cos(theta_old) + dy0 * sin(theta_old);
    dy = -dx0 * sin(theta_old) + dy0 * cos(theta_old);

    delta_t = sqrt(dx * dx + dy * dy);
    delta_theta = carmen_normalize_theta(odometry_position.theta - 
                                         old_odometry_position.theta);
    odom_theta = atan2(dy, dx);
  
    //lamda = h * cos(alpha) + k * sin(alpha)
    //mu = -h * sin(alpha) + k * cos(alpha)

    carmen_point_t person_loc;
  
    for(int k=0;k<valid_history;k++){
        double x = person_history[k].x;
        double y = person_history[k].y;

        person_history[k].x = x* cos(delta_theta) + y* sin(delta_theta) - dx;
        person_history[k].y = -x* sin(delta_theta) + y* cos(delta_theta)- dy;   
    }
}

void carmen_localize_update_person_heading(carmen_point_t old_odometry_position, 
                                           carmen_point_t odometry_position, 
                                           double *heading,
                                           int valid_history)
{
    int backwards;
    double delta_t, delta_theta;
    double dx, dy, odom_theta;

    double dx0,dy0, theta_old;

    theta_old = old_odometry_position.theta;
  
    //w.r.t. the global measurements 

    dx0 = (odometry_position.x - old_odometry_position.x);  
    dy0 = (odometry_position.y - old_odometry_position.y);

    //converted to the robot's previous frame of reference

    dx = dx0 * cos(theta_old) + dy0 * sin(theta_old);
    dy = -dx0 * sin(theta_old) + dy0 * cos(theta_old);

    delta_t = sqrt(dx * dx + dy * dy);
    delta_theta = carmen_normalize_theta(odometry_position.theta - 
                                         old_odometry_position.theta);
    odom_theta = atan2(dy, dx);
  
    //lamda = h * cos(alpha) + k * sin(alpha)
    //mu = -h * sin(alpha) + k * cos(alpha)

    carmen_point_t person_loc;
  
    for(int k=0;k<valid_history;k++){
        heading[k] += delta_theta;
        heading[k] = carmen_normalize_theta(heading[k]);
        //normalize
    }
}


void carmen_localize_update_filter_center(carmen_point_t old_odometry_position, 
                                          carmen_point_t odometry_position, 
                                          carmen_point_p filter_center)
{
    int backwards;
    double delta_t, delta_theta;
    double dx, dy, odom_theta;

    double dx0,dy0, theta_old;

    theta_old = old_odometry_position.theta;
  
    //w.r.t. the global measurements 

    dx0 = (odometry_position.x - old_odometry_position.x);  
    dy0 = (odometry_position.y - old_odometry_position.y);

    //converted to the robot's previous frame of reference

    dx = dx0 * cos(theta_old) + dy0 * sin(theta_old);
    dy = -dx0 * sin(theta_old) + dy0 * cos(theta_old);

    delta_t = sqrt(dx * dx + dy * dy);
    delta_theta = carmen_normalize_theta(odometry_position.theta - 
                                         old_odometry_position.theta);
    odom_theta = atan2(dy, dx);
  
    //calculate whether the motion was backwards
    backwards = (dx0 * cos(odometry_position.theta) + 
                 dy0 * sin(odometry_position.theta) < 0);  //wether this was in the reverse direction
  
    //lamda = h * cos(alpha) + k * sin(alpha)
    //mu = -h * sin(alpha) + k * cos(alpha)

    carmen_point_t person_loc;
  
    person_loc.x = filter_center->x;
    person_loc.y = filter_center->y;
    person_loc.theta = 0.0;
  
    filter_center->x = person_loc.x* cos(delta_theta) + person_loc.y* sin(delta_theta) - dx;
    filter_center->y = -person_loc.x* sin(delta_theta) + person_loc.y* cos(delta_theta)- dy;   
}

void carmen_localize_update_filter_center_old(carmen_point_t old_odometry_position, 
                                              carmen_point_t odometry_position, 
                                              carmen_point_p filter_center)
{//too complex and unnecesary
    int backwards;
    double delta_t, delta_theta;
    double dx, dy, odom_theta;

    double dx0,dy0, theta_old;

    theta_old = old_odometry_position.theta;
  
    //w.r.t. the global measurements 

    dx0 = (odometry_position.x - old_odometry_position.x);  
    dy0 = (odometry_position.y - old_odometry_position.y);

    //converted to the robot's previous frame of reference

    dx = dx0 * cos(theta_old) + dy0 * sin(theta_old);
    dy = -dx0 * sin(theta_old) + dy0 * cos(theta_old);

    delta_t = sqrt(dx * dx + dy * dy);
    delta_theta = carmen_normalize_theta(odometry_position.theta - 
                                         old_odometry_position.theta);
    odom_theta = atan2(dy, dx);
  
    //calculate whether the motion was backwards
    backwards = (dx0 * cos(odometry_position.theta) + 
                 dy0 * sin(odometry_position.theta) < 0);  //wether this was in the reverse direction
  
    //lamda = h * cos(alpha) + k * sin(alpha)
    //mu = -h * sin(alpha) + k * cos(alpha)

    carmen_point_t person_loc;
  
    person_loc.x = filter_center->x;
    person_loc.y = filter_center->y;
    person_loc.theta = 0.0;
  
    filter_center->x = person_loc.x* cos(delta_theta) + person_loc.y* sin(delta_theta) - dx;
    filter_center->y = -person_loc.x* sin(delta_theta) + person_loc.y* cos(delta_theta)- dy; 
  
}


void carmen_localize_incorporate_odometry_person(carmen3d_localize_particle_filter_p filter,
                                                 double heading, double velocity, //or do we need the distance traveled??
                                                 carmen_point_t odometry_position)
{
    int i, backwards;
    double delta_t, delta_theta;
    double dx, dy, odom_theta;

    if(filter->first_odometry) {
        filter->last_odometry_position = odometry_position;
        filter->first_odometry = 0;
        return;
    }

    double dx0,dy0, theta_old;

    theta_old = filter->last_odometry_position.theta;
  
    //w.r.t. the global measurements 

    dx0 = (odometry_position.x - filter->last_odometry_position.x);  
    dy0 = (odometry_position.y - filter->last_odometry_position.y);

    //converted to the robot's previous frame of reference

    dx = dx0 * cos(theta_old) + dy0 * sin(theta_old);
    dy = -dx0 * sin(theta_old) + dy0 * cos(theta_old);

    delta_t = sqrt(dx * dx + dy * dy);
    delta_theta = carmen_normalize_theta(odometry_position.theta - 
                                         filter->last_odometry_position.theta);
    odom_theta = atan2(dy, dx);
  
    //calculate whether the motion was backwards
    backwards = (dx0 * cos(odometry_position.theta) + 
                 dy0 * sin(odometry_position.theta) < 0);  //wether this was in the reverse direction
  
    filter->distance_travelled += delta_t; //Not being used for resampling

    double x_std = 0.1;
    double y_std = 0.1;

    for(i = 0; i < filter->param->num_particles; i++) {
        double ran_x = carmen_gaussian_random(0,x_std);
        double ran_y = carmen_gaussian_random(0,y_std);

        filter->particles[i].x += ran_x;
        filter->particles[i].y += ran_y;
    }
    filter->last_odometry_position = odometry_position;
}

void carmen_localize_incorporate_odometry_person_old(carmen3d_localize_particle_filter_p filter,
                                                 carmen_point_t odometry_position)
{
    int i, backwards;
    double delta_t, delta_theta;
    double dx, dy, odom_theta;

    if(filter->first_odometry) {
        filter->last_odometry_position = odometry_position;
        filter->first_odometry = 0;
        return;
    }

    double dx0,dy0, theta_old;

    theta_old = filter->last_odometry_position.theta;
  
    //w.r.t. the global measurements 

    dx0 = (odometry_position.x - filter->last_odometry_position.x);  
    dy0 = (odometry_position.y - filter->last_odometry_position.y);

    //converted to the robot's previous frame of reference

    dx = dx0 * cos(theta_old) + dy0 * sin(theta_old);
    dy = -dx0 * sin(theta_old) + dy0 * cos(theta_old);

    delta_t = sqrt(dx * dx + dy * dy);
    delta_theta = carmen_normalize_theta(odometry_position.theta - 
                                         filter->last_odometry_position.theta);
    odom_theta = atan2(dy, dx);
  
    //calculate whether the motion was backwards
    backwards = (dx0 * cos(odometry_position.theta) + 
                 dy0 * sin(odometry_position.theta) < 0);  //wether this was in the reverse direction
  
    filter->distance_travelled += delta_t; //Not being used for resampling

    double x_std = 0.05;
    double y_std = 0.05;

    for(i = 0; i < filter->param->num_particles; i++) {
        double ran_x = carmen_gaussian_random(0,x_std);
        double ran_y = carmen_gaussian_random(0,y_std);

        filter->particles[i].x += ran_x;
        filter->particles[i].y += ran_y;
    }
    filter->last_odometry_position = odometry_position;
}


void carmen_localize_resample_person(carmen3d_localize_particle_filter_p filter)
{
    int i, which_particle;
    float weight_sum = 0.0, *cumulative_sum = NULL;
    float position, step_size, max_weight = filter->particles[0].weight;
    carmen3d_localize_particle_p temp_particles = NULL;

    /* change log weights back into probabilities */
    for(i = 0; i < filter->param->num_particles; i++)
        if(filter->particles[i].weight > max_weight)
            max_weight = filter->particles[i].weight;
    for(i = 0; i < filter->param->num_particles; i++)
        filter->particles[i].weight = 
            exp(filter->particles[i].weight - max_weight);

    /* Allocate memory necessary for resampling */
    cumulative_sum = (float *)calloc(filter->param->num_particles, sizeof(float));
    carmen_test_alloc(cumulative_sum);
    temp_particles = (carmen3d_localize_particle_p)
        calloc(filter->param->num_particles, sizeof(carmen3d_localize_particle_t));
    carmen_test_alloc(temp_particles);

    /* Sum the weights of all of the particles */
    for(i = 0; i < filter->param->num_particles; i++) {
        weight_sum += filter->particles[i].weight;
        cumulative_sum[i] = weight_sum;
    }

    /* choose random starting position for low-variance walk */
    position = carmen_uniform_random(0, weight_sum);
    step_size = weight_sum / (float)filter->param->num_particles;
    which_particle = 0;
  
    /* draw num_particles random samples */
    for(i = 0; i < filter->param->num_particles; i++) {
        position += step_size;
        if(position > weight_sum) {
            position -= weight_sum;
            which_particle = 0;
        }
        while(position > cumulative_sum[which_particle])
            which_particle++;
        memcpy(temp_particles + i, filter->particles + which_particle,
               sizeof(carmen3d_localize_particle_t));
    }

    /* Copy new particles back into the filter. */
    free(filter->particles);
    filter->particles = temp_particles;
    free(cumulative_sum);

    /* set all log weights back to zero */
    for(i = 0; i < filter->param->num_particles; i++)
        filter->particles[i].weight = 0.0;
}

int carmen_localize_incorporate_person_obs(carmen3d_localize_particle_filter_p filter,//carmen_point_t person_observation,
                                           carmen_point_t person_observation)
{
    //fprintf(stderr,"Updating person observation\n");
    double std_dev = 0.2;//0.05;//0.2;//0.05;//0.05;//0.02;//0.5  //0.02??
    int i;

    double C = log(1/sqrt(2*M_PI*pow(std_dev,2)));
    for(i = 0; i < filter->param->num_particles; i++){
        filter->particles[i].weight = 0.0;
        double distance = hypot((filter->particles[i].x-person_observation.x),
                                (filter->particles[i].y-person_observation.y));
    
        /*double prob = 1/sqrt(2*M_PI*pow(std_dev,2))*exp(-(pow(distance,2)/(2*pow(std_dev,2))));
          filter->particles[i].weight = log(prob);      */
        double log_prob = C -pow(distance,2)/(2*pow(std_dev,2));
        filter->particles[i].weight = log_prob;
    }
    return 1;
}


int
carmen_localize_run_person_with_obs(carmen3d_localize_particle_filter_p filter, 
                                    carmen_point_t robot_position, 
                                    double heading, double velocity, 
                                    carmen_point_t person_observation)
{
    //carmen_point_t robot_position;
    if(filter == NULL){
        fprintf(stderr,"ERRRRRRRR = Null filter passed\n");
        return -1;
    }

    if(!filter->initialized)
        return -1;

    // incorporate the laser position stamp 
    //robot_position.x = laser->robot_pose.x;
    //robot_position.y = laser->robot_pose.y;
    //robot_position.theta = laser->robot_pose.theta;

    //Incorporate the odometry update 
  
    //fprintf(stderr,"(%f,%f)\n",filter_center.x,filter_center.y);
    carmen_localize_incorporate_odometry_person(filter, heading, velocity, robot_position);
    //fprintf(stderr,"(%f,%f)\n",filter_center.x,filter_center.y);

    int valid_obs = 0;
    //Incorporate person observation
    valid_obs = carmen_localize_incorporate_person_obs(filter,person_observation);

    carmen_localize_resample_person(filter);
    filter->distance_travelled = 0;
    filter->initialized = 1;

    return 0;
}

int 
carmen_localize_run_person_no_obs(carmen3d_localize_particle_filter_p filter, 
                                  double heading, double velocity, 
                                  carmen_point_t robot_position)
{
    //carmen_point_t robot_position;

    if(!filter->initialized)
        return -1;

    // incorporate the laser position stamp 
    //robot_position.x = laser->robot_pose.x;
    //robot_position.y = laser->robot_pose.y;
    //robot_position.theta = laser->robot_pose.theta;

    //Incorporate the odometry update 
  
    //fprintf(stderr,"(%f,%f)\n",filter_center.x,filter_center.y);
    carmen_localize_incorporate_odometry_person(filter, heading, velocity, robot_position);
    filter->initialized = 1;
    return 0;
}

void carmen_localize_summarize_person(carmen3d_localize_particle_filter_p filter, 
                                      carmen3d_localize_summary_p summary) 
{
    float mean_x, mean_y, mean_theta_x, mean_theta_y;
    float diff_x, diff_y, diff_theta, std_x, std_y, std_theta, xy_cov;
    float *weights, max_weight = filter->particles[0].weight;
    float total_weight = 0;
    int i;

    summary->converged = !filter->global_mode;

    weights = (float *)calloc(filter->param->num_particles, sizeof(float));
    carmen_test_alloc(weights);
    for(i = 0; i < filter->param->num_particles; i++)
        if(filter->particles[i].weight > max_weight)
            max_weight = filter->particles[i].weight;
    for(i = 0; i < filter->param->num_particles; i++) {
        weights[i] = exp(filter->particles[i].weight - max_weight);
        total_weight += weights[i];
    }

    // compute mean particle pose 
    mean_x = 0;
    mean_y = 0;
    mean_theta_x = 0;
    mean_theta_y = 0;
    for(i = 0; i < filter->param->num_particles; i++) {
        mean_x += filter->particles[i].x * weights[i];
        mean_y += filter->particles[i].y * weights[i];
        mean_theta_x += cos(filter->particles[i].theta) * weights[i];
        mean_theta_y += sin(filter->particles[i].theta) * weights[i];
    }
    summary->mean.x = mean_x / total_weight;
    summary->mean.y = mean_y / total_weight;

    if(mean_theta_x == 0)
        summary->mean.theta = 0;
    else
        summary->mean.theta = atan2(mean_theta_y, mean_theta_x);
    summary->odometry_pos = filter->last_odometry_position;

    // compute std particle pose 
    std_x = 0;
    std_y = 0;
    std_theta = 0;
    xy_cov = 0;
    for(i = 0; i < filter->param->num_particles; i++) {
        diff_x = (filter->particles[i].x - summary->mean.x);
        diff_y = (filter->particles[i].y - summary->mean.y);
        diff_theta = carmen_normalize_theta(filter->particles[i].theta -
                                            summary->mean.theta);
        std_x += carmen_square(diff_x);
        std_y += carmen_square(diff_y);
        std_theta += carmen_square(diff_theta);
        xy_cov += diff_x * diff_y;
    }
    summary->std.x = sqrt(std_x / filter->param->num_particles);
    summary->std.y = sqrt(std_y / filter->param->num_particles);
    summary->std.theta = sqrt(std_theta / filter->param->num_particles);
    summary->xy_cov = sqrt(xy_cov / filter->param->num_particles);

    free(weights);
}
