#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <nodder/nodder_extractor.h>
#include <lcmtypes/erlcm_xyz_point_list_t.h>
#include <interfaces/pcl_to_lcm.hpp>
#include <interfaces/pcl_lcm_lib.hpp>
#include <pcl/point_types.h>
#include <lcmtypes/arm_rrts_cmd_t.h>
#include <lcmtypes/arm_cmd_object_manipulation_cmd_t.h>

using namespace std;


typedef struct _state_t state_t;

struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    BotFrames *frames; 
    BotParam *param;
    nodder_extractor_state_t *nodder; 
    int detect_objects;
    int segment;
    int filter;
    int best_fit;
    int cluster;
    int verbose;
    int processed;
    GMutex *mutex;
    arm_cmd_object_manipulation_cmd_t *object_request;
    xyz_point_list_t *ret;
    pthread_t  object_detection_thread;
};

void update_trans(double pos[3], double rpy[3], BotTrans &trans){
    trans.trans_vec[0] = pos[0];
    trans.trans_vec[1] = pos[1];
    trans.trans_vec[2] = pos[2];
    bot_roll_pitch_yaw_to_quat(rpy, trans.rot_quat);
}


void print_bot_trans_full(BotTrans &trans, char *name=NULL){
    double rpy_b[3] = {0};

    bot_quat_to_roll_pitch_yaw(trans.rot_quat, rpy_b);
    
    if(name){
        fprintf(stderr, "%s =>", name);
    }
    
    fprintf(stderr, " Pose : %f,%f,%f - RPY : %f,%f,%f\n", 
            name, 
            trans.trans_vec[0], 
            trans.trans_vec[1], 
            trans.trans_vec[2], 
            bot_to_degrees(rpy_b[0]),
            bot_to_degrees(rpy_b[1]),
            bot_to_degrees(rpy_b[2]));
}

void print_bot_trans_pose(BotTrans &trans, char *name=NULL){
    if(name){
        fprintf(stderr, "%s =>", name);
    }    

    fprintf(stderr, "Pose : %f,%f,%f\n", 
            trans.trans_vec[0], 
            trans.trans_vec[1], 
            trans.trans_vec[2]);
}

void print_bot_trans_rpy(BotTrans &trans, char *name=NULL ){
    double rpy_b[3] = {0};

    if(name){
        fprintf(stderr, "%s =>", name);
    } 

    bot_quat_to_roll_pitch_yaw(trans.rot_quat, rpy_b);
    
    fprintf(stderr, "RPY : %f,%f,%f\n", 
            bot_to_degrees(rpy_b[0]),
            bot_to_degrees(rpy_b[1]),
            bot_to_degrees(rpy_b[2]));
}

void publish_rrt_goal(state_t *self, BotTrans& goal){
    arm_rrts_cmd_t msg;
                
    msg.gripper_enable = 0;
    msg.gripper_pos = 0.5;//gripper_pos;

    //transform this to local frame and send it out 
    msg.xyz[0] = goal.trans_vec[0];
    msg.xyz[1] = goal.trans_vec[1];
    msg.xyz[2] = goal.trans_vec[2];
                   
    bot_quat_to_roll_pitch_yaw(goal.rot_quat, msg.ef_rpy);

    msg.iterations = 20000;
                
    arm_rrts_cmd_t_publish(self->lcm, "ARM_RRTS_COMMAND", &msg);
}

void publish_pose(state_t *self, int64_t utime, BotTrans& goal){
    bot_core_pose_t msg;                
    msg.utime = utime;

    msg.pos[0] = goal.trans_vec[0];
    msg.pos[1] = goal.trans_vec[1];
    msg.pos[2] = goal.trans_vec[2];
    
    memset(msg.vel, 0, 3 * sizeof(double));
    
    memcpy(msg.orientation, goal.rot_quat, 4 * sizeof(double));
    
    bot_core_pose_t_publish(self->lcm, "OBJ_BOTTLE_POSE", &msg);
}

void send_object_to_rrt(state_t *self, BotTrans object_to_base){
    BotTrans local_to_base; 
    bot_frames_get_trans(self->frames, "local", "base", &local_to_base);

    BotTrans base_to_local; 
    bot_frames_get_trans(self->frames, "base", "local", &base_to_local);

    double pos_to_base[3] = {0.357086,0.2 ,0.242834};

    double theta = atan2(object_to_base.trans_vec[1], object_to_base.trans_vec[0]);

    BotTrans object_to_local;
    bot_trans_apply_trans_to(&base_to_local, &object_to_base, &object_to_local);
    print_bot_trans_full(object_to_local, "Object To Local - Original");

    BotTrans pitched_obj_to_object;
    double xyz[3] = {0};
    double rpy_p[3] = {0, bot_to_radians(-180), bot_to_radians(90) + theta };
    update_trans(xyz, rpy_p, pitched_obj_to_object);

    BotTrans pitched_obj_to_local;
    bot_trans_apply_trans_to(&object_to_local, &pitched_obj_to_object, &pitched_obj_to_local);

    publish_rrt_goal(self, pitched_obj_to_local);    
}

BotTrans get_object_pose(state_t *self, BotTrans object_to_base){
    BotTrans local_to_base; 
    bot_frames_get_trans(self->frames, "local", "base", &local_to_base);

    BotTrans base_to_local; 
    bot_frames_get_trans(self->frames, "base", "local", &base_to_local);

    double pos_to_base[3] = {0.357086,0.2 ,0.242834};

    double theta = atan2(object_to_base.trans_vec[1], object_to_base.trans_vec[0]);

    BotTrans object_to_local;
    bot_trans_apply_trans_to(&base_to_local, &object_to_base, &object_to_local);
    print_bot_trans_full(object_to_local, "Object To Local - Original");

    BotTrans pitched_obj_to_object;
    double xyz[3] = {0};
    double rpy_p[3] = {0, bot_to_radians(-180), bot_to_radians(90) + theta };
    update_trans(xyz, rpy_p, pitched_obj_to_object);

    BotTrans pitched_obj_to_local;
    bot_trans_apply_trans_to(&object_to_local, &pitched_obj_to_object, &pitched_obj_to_local);

    return pitched_obj_to_local;
}



int detect_objects(state_t *s){
    g_mutex_lock(s->mutex);
    //copy this over to the state
    if(s->ret == NULL || s->processed ==1 || s->object_request==NULL){
        g_mutex_unlock(s->mutex);
        return -1;
    }
    
    xyz_point_list_t *ret = copy_xyz_list(s->ret);
    g_mutex_unlock(s->mutex);

    fprintf(stderr, "Detecting Objects\n");

    if(ret != NULL){
        if(s->verbose)
            fprintf(stderr, "Size of Points : %d \n", ret->no_points);

        //find the size of the map based on the current position 
        //copy and convert 
        double body_to_local[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                      "local",  ret->utime,
                                                      body_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            return -1;
        }

        double local_to_body[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "local",
                                                      "body",  ret->utime,
                                                      local_to_body)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            return -1;
        }
    
        double bot_body[3] = {0,0,0};
        double bot_local[3];

        double bot_infront[3] = {1.0,0,0};
        double bot_local_infront[3];

        bot_vector_affine_transform_3x4_3d (body_to_local, bot_body, bot_local);

        bot_vector_affine_transform_3x4_3d (body_to_local, bot_infront, bot_local_infront);
 
 
        //actually we should get the extent of the scan here 

        //actually there isnt much useful info behind the robot 

        /*double xyz0[3] = { -10 + bot_local[0], -10 + bot_local[1], 0 };
          double xyz1[3] = { 10 + bot_local[0], 10 + bot_local[1] , 5 };
          double mpp[3] = { .1, .1, .1 };
          occ_map::FloatVoxelMap fvm(xyz0, xyz1, mpp, 0);
        
          double ixyz[3];
        */
        //this API might not be sufficient - we would need to raytrace and insert free space 

        static const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>);
        pcl::PointCloud<pcl::PointXYZ>& cloud = *cloud_ptr;
        cloud.height   = 1;//width;
        cloud.width    = ret->no_points;//height;
        cloud.is_dense = false;
        cloud.points.resize (cloud.width * cloud.height);

        int pcl_ind = 0;
        
        double point_l[3] = {0};
        double point_b[3] = {0};

        double forward_heading = atan2(bot_local_infront[1] - bot_local[1], 
                                       bot_local_infront[0] - bot_local[0]);

        for (size_t k = 0; k < ret->no_points; ++k){
            if(ret->points[k].xyz[2] < 0.2 || ret->points[k].xyz[2] > 1.5)
                continue;
        
            if(hypot(ret->points[k].xyz[0] - bot_local_infront[0], ret->points[k].xyz[1] - bot_local_infront[1]) > 2.0)
                continue;
            
            point_l[0] = ret->points[k].xyz[0];
            point_l[1] = ret->points[k].xyz[1];

            bot_vector_affine_transform_3x4_3d (local_to_body, point_l, point_b);

            //on footprint
            
            if(fabs(point_b[0]) < 0.3 && fabs(point_b[1] < 0.3))
                continue;

            double heading = atan2(point_b[1], point_b[0]);
            if(fabs(heading) > bot_to_degrees(60))
                continue;
        
            //check the angle and throw out stuff not infront of the bot 

            //should skip stuff inside the footprint also
            cloud.points[pcl_ind].x = ret->points[k].xyz[0];
            cloud.points[pcl_ind].y = ret->points[k].xyz[1];
            cloud.points[pcl_ind].z = ret->points[k].xyz[2];
        
            pcl_ind++;
        
            //point3d endpoint(ret->points[k].xyz[0], ret->points[k].xyz[1], ret->points[k].xyz[2]);
        
            //these are the valid points - need to create a pcl pointcloud - then find tabletops and then find points on top of the table - and then fit a cylinder to it - also move this to another pod - we dont want this to be pcl dependant
        
        }

        cloud.width = pcl_ind;
        cloud.points.resize (cloud.width * cloud.height);
    
        fprintf(stderr, "Publishing points : %d\n", pcl_ind);

        //pcl::PointCloud<pcl::PointXYZ>::Ptr cpy(new pcl::PointCloud<pcl::PointXYZ>(cloud));

        //publish_pcl_points_to_lcm(cloud, s->lcm);
        //publish_pcl_points_to_collection(cloud, s->lcm, "nodder_points");
        
        object_list_t result_list; 
        result_list.count = 0;
        result_list.objects = NULL;

        //filtering is bad - messes up the tabletop detection 
        int filter_points = s->filter;//0;
        int cluster_table = s->cluster;//1;
        int fit_cylinders = 1;
        int find_best_fit = s->best_fit;
        int draw_table = 1;
        int draw_objects = 1;

        /*if(s->detect_objects)
            find_tabletop_objects(cloud_ptr, filter_points, cluster_table, 
                                  fit_cylinders, find_best_fit, 
                                  draw_table, draw_objects, 
                                  forward_heading, bot_local, 
                                  &result_list, s->lcm);*/

        if(s->segment){
            find_tabletop_objects_after_segmentation(cloud_ptr, forward_heading, bot_local,
                                                     s->lcm, &result_list);

            fprintf(stderr, "Object detector - No of objects : %d\n", result_list.count);
            //get the resulting object out and publish out as an rrt goal 
            
            if(result_list.count >0){
                fprintf(stderr, "We have a result - using the first object in the list\n");
            
                BotTrans local_to_base; 
                bot_frames_get_trans(s->frames, "local", "base", &local_to_base);
                
                BotTrans object_to_local;
                object_to_local.trans_vec[0] = result_list.objects[0].center[0];
                object_to_local.trans_vec[1] = result_list.objects[0].center[1];
                object_to_local.trans_vec[2] = result_list.objects[0].center[2];

                double rpy[3] = {0};
                bot_roll_pitch_yaw_to_quat(rpy, object_to_local.rot_quat);
                
                BotTrans object_to_base;
                bot_trans_apply_trans_to(&local_to_base, &object_to_local, &object_to_base);

                //this is correct
                fprintf(stderr, "Position of Object relative to base : %f,%f,%f\n", 
                        object_to_base.trans_vec[0], 
                        object_to_base.trans_vec[1], 
                        object_to_base.trans_vec[2]);

                //send_object_to_rrt(s, object_to_base);
                BotTrans grasp_pose_to_local = get_object_pose(s, object_to_base);
                publish_pose(s, ret->utime, grasp_pose_to_local);

                arm_cmd_object_manipulation_cmd_t_destroy(s->object_request);
                s->object_request = NULL;
            }
        }

        cloud.points.resize (0);

        destroy_xyz_list(ret);
    }
    return 0;
}

void on_request(const lcm_recv_buf_t *rbuf, const char * channel, const arm_cmd_object_manipulation_cmd_t * msg, void * user) {
  
    state_t *self = (state_t *) user;

    if(msg->type == ARM_CMD_OBJECT_MANIPULATION_CMD_T_TYPE_DETECT_OBJECT){
        if(self->object_request)
            arm_cmd_object_manipulation_cmd_t_destroy(self->object_request);
        
        self->object_request = arm_cmd_object_manipulation_cmd_t_copy(msg);
    }
}

void nodder_cb(int64_t utime, void *data)
{
    state_t *s = (state_t *)data;
    //fprintf(stderr, "Called \n"); 
    xyz_point_list_t *ret = nodder_extract_new_points_frame(s->nodder, "local");
    fprintf(stderr, "Received objects\n");
    g_mutex_lock(s->mutex);
    //copy this over to the state
    if(s->ret != NULL){
        destroy_xyz_list(s->ret);
    }
    s->ret = ret;
    s->processed = 0;
    g_mutex_unlock(s->mutex);
    //this is blocking the nodder collection 

      
}



static void *particles_thread(void *user)
{
    state_t * s = (state_t *) user;

    printf("Object segmentation thread started\n");
    int status = 0;
    while(1){
        if(s->processed){
            usleep(5000);
        }
        else{
            int status = detect_objects(s);
            g_mutex_lock(s->mutex);
            s->processed = 1;
            g_mutex_unlock(s->mutex);
        }
    }
}

int main(int argc, char **argv)
{
    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    state->lcm =  bot_lcm_get_global(NULL);

    state->param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->param);

    //state->nodder = nodder_extractor_init_full(state->lcm, 1, -90, 90, NULL, 
    //&nodder_cb, state);

    state->nodder = nodder_extractor_init(state->lcm, &nodder_cb, state);
    state->ret = NULL;
    state->mutex = g_mutex_new();

    if(state->nodder== NULL){
        fprintf(stderr,"Issue getting collector\n");
        return -1;
    }

    state->mainloop = g_main_loop_new( NULL, FALSE );  
    
    fprintf(stderr, "Setting the segment first method - this one works best\n");
    state->segment = 1;

    const char *optstring = "vhcfsdb";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "verbose", no_argument, 0, 'v' }, 
                                  { "cluster", no_argument, 0, 'c' }, 
                                  { "filter", no_argument, 0, 'f' }, 
                                  { "best_fit", no_argument, 0, 'b' }, 
                                  { "segment", no_argument, 0, 's' }, 
                                  { "detect_objects", no_argument, 0, 'o' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    //usage(argv[0]);
            fprintf(stderr, "No help found - please add help\n");
	    break;
        case 'f':
            //state->filter = 1;
            fprintf(stderr, "Deprecated - Filtering before detection\n");
	    break;       
        case 'b':
            state->best_fit = 1;
            fprintf(stderr, "Finding best Table - Not yet added to new method\n");
	    break;     
        case 'd':
            //state->detect_objects = 1;
            return -1;
            fprintf(stderr, "Deprecated - Use the -s flag Looking for objects\n");
	    break;
        case 's':
            state->segment = 1;
            fprintf(stderr, "Doing segmentation - this is set by default\n");
	    break;
        case 'c':
	    //state->cluster = 1;
            fprintf(stderr, "Deprecated - Clustering after plane detection\n");
	    break;
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
                state->verbose = 1;
		break;
	    }
        }
    }
  
    if (!state->mainloop){
	printf("Couldn't create main loop\n");
	return -1;
    }

    state->object_request = NULL;
    
    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    arm_cmd_object_manipulation_cmd_t_subscribe(state->lcm, "OBJECT_MANIPULATION_REQUEST", on_request, state);

    pthread_create(&state->object_detection_thread , NULL, particles_thread, state);
    
    /* heart beat*/
    //g_timeout_add (100, heartbeat_cb, state);
    
    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
    
    bot_glib_mainloop_detach_lcm(state->lcm);
}
