# Create executables
add_executable(er-lidar-feature-extractor test.c)

target_link_libraries (er-lidar-feature-extractor ${LCMTYPES_LIBS})

# The test program uses the shared library, use the pkg-config file
pods_use_pkg_config_packages(er-lidar-feature-extractor bot2-core laser-feature-extractor)

# make executable public
pods_install_executables(er-lidar-feature-extractor)
