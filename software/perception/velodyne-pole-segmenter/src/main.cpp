#include <stdio.h>
#include <pthread.h>

#include <string.h>

#include <gsl/gsl_blas.h>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
//#include <velodyne/velodyne.h>
//#include <velodyne/velodyne_extractor.h>
//#include <lcmtypes/erlcm_xyz_point_t.h>
//#include <lcmtypes/erlcm_seg_point_list_t.h>
#include <lcmtypes/er_lcmtypes.h>

#include "segmenter.hpp"

#include <unistd.h>

typedef struct _state_t state_t;

struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    segmenter* seg;
    BotParam *param;
    BotFrames *frames;
    velodyne_extractor_state_t *velodyne;
    int64_t lastTime;
};

static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -v      Set verbose to true.\n"
             "  -h      This help message.\n",
             g_path_get_basename(progname));
    exit(1);
}

static void publishSegments(state_t *self,
                            std::map<int,std::vector<xyz_point_t> > &segPoints, double utime)
{
    std::map<int, std::vector<xyz_point_t> >::iterator mapIt;
    std::vector<xyz_point_t>::iterator vecIt;

    erlcm_segment_list_t msg;
    msg.utime = utime;
    msg.no_segments = segPoints.size();
    msg.segments = NULL;

    double body_to_velodyne[12];
    if (!bot_frames_get_trans_mat_3x4 (self->frames, "body",
                                       "VELODYNE", //self->lrc->utime,
                                                  body_to_velodyne)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;        
    }

    msg.segments = (erlcm_seg_point_list_t*)realloc(msg.segments, msg.no_segments*sizeof(erlcm_seg_point_list_t));
    int j = 0;
    for (mapIt = segPoints.begin(); mapIt != segPoints.end(); mapIt++, j++)
    {
        erlcm_seg_point_list_t *seg_msg = &msg.segments[j];
        seg_msg->segment_id = (*mapIt).first;
        seg_msg->no_points = (*mapIt).second.size();

        seg_msg->points = (erlcm_xyz_point_t*)calloc(seg_msg->no_points,
                                                   sizeof(erlcm_xyz_point_t));
        int i = 0;
        for (vecIt = (*mapIt).second.begin();
             vecIt != (*mapIt).second.end(); vecIt++, i++)
        {
            bot_vector_affine_transform_3x4_3d (body_to_velodyne, (*vecIt).xyz, seg_msg->points[i].xyz);
        }
    }
    
    erlcm_segment_list_t_publish(self->lcm, "PCL_SEGMENT_LIST", &msg);

    for (int k = 0; k > msg.no_segments; k++)
        free(msg.segments[k].points);

    free (msg.segments);

}

static void velodyne_update_handler(int64_t utime, void *user)
{
  state_t *s = (state_t *)user;

  xyz_point_list_t *ret = velodyne_extract_points_frame_compensation(s->velodyne, "body");
  // xyz_point_list_t *ret = velodyne_extract_points_frame(s->velodyne, "body"); //velodyne_extract_points(s->velodyne); 

  if(ret != NULL && ret->utime > s->lastTime){
    s->lastTime = ret->utime;

    // check size

    std::map<int,std::vector<xyz_point_t> > segPoints;
    s->seg->buildGraph(ret, segPoints);

    segmenter::segment_feature_t *sf = (segmenter::segment_feature_t*) malloc(segPoints.size()*sizeof(segmenter::segment_feature_t));

    printf("1num segments = %d\n",segPoints.size());
    s->seg->getFeatures(segPoints, sf);

    bool gotHand = false;
    gotHand = s->seg->getPoles(segPoints, sf);
    printf("2num segments = %d\n",segPoints.size());
    publishSegments(s, segPoints, ret->utime);

    free(sf);

    printf("destroyed\n");

  }

  destroy_xyz_list(ret);
}

gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;

    xyz_point_list_t *ret = velodyne_extract_points_frame(s->velodyne, "body"); //velodyne_extract_points(s->velodyne); 

    if(ret != NULL && ret->utime > s->lastTime){
        s->lastTime = ret->utime;

        // check size

        std::map<int,std::vector<xyz_point_t> > segPoints;
        s->seg->buildGraph(ret, segPoints);

        segmenter::segment_feature_t *sf = (segmenter::segment_feature_t*) malloc(segPoints.size()*sizeof(segmenter::segment_feature_t));

        printf("1num segments = %d\n",segPoints.size());
        s->seg->getFeatures(segPoints, sf);

        bool gotHand = false;
        gotHand = s->seg->getPoles(segPoints, sf);
        printf("2num segments = %d\n",segPoints.size());
        publishSegments(s, segPoints, ret->utime);

        free(sf);

        printf("destroyed\n");

    }

    destroy_xyz_list(ret);
    //return true to keep running
    return TRUE;
}

int main(int argc, char **argv){
    printf("starting\n");

    int c;
    bool v = false;
    bool draw = false;
	bool standAlone = false;
    // command line options - to throtle - to ignore image publish  
    while ((c = getopt (argc, argv, "hvds")) >= 0) {
    	switch (c) {
    	case 'v': //ignore images 
            v = true;
            printf("Setting verbose to true\n");
            break;
      	case 'h':
            usage(argv[0]);
            break;
        }
    }

    state_t *state = (state_t*)calloc(1,sizeof(state_t));
    state->lcm = bot_lcm_get_global(NULL);
    state->param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->param);

    state->velodyne = velodyne_extractor_init(state->lcm, &velodyne_update_handler , state);

    state->mainloop = g_main_loop_new(NULL, FALSE);

    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }

    bot_glib_mainloop_attach_lcm(state->lcm);

    /* heart beat*/
    //g_timeout_add (100, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);

    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);

    bot_glib_mainloop_detach_lcm(state->lcm);

}
