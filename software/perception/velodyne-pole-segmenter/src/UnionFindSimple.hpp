#include <stdlib.h>

#ifndef UNIONFIND 
#define UNIONFIND

/** Implementation of disjoint set data structure that packs each
 * entry into a single array of 'int' for performance. *
 */
class UnionFindSimple
{
	private:
	static const int SZ = 2;

    public:

    int* data; // alternating arent ids, rank, size.

    /** param maxid The maximum node id that will be referenced. **/
    UnionFindSimple(int maxid)
    {
        data = (int*) malloc(maxid*SZ*sizeof(int));

		int i;
        for (i = 0; i < maxid; i++) {
            // everyone is their own cluster of size 1
            data[SZ*i+0] = i;
            data[SZ*i+1] = 1;
        }
    }

	~UnionFindSimple(){
		free(data);
	}

    int getRepresentative(int id)
    {
        // terminal case: a node is its own parent.
        if (data[SZ*id]==id)
            return id;

        // otherwise, recurse...
        int root = getRepresentative(data[SZ*id]);

        // short circuit the path.
        data[SZ*id] = root;

        return root;
    }

    int getSetSize(int id)
    {
        return data[SZ*getRepresentative(id)+1];
    }

    /** returns the id of the merged node. **/
    int connectNodes(int aid, int bid)
    {
        int aroot = getRepresentative(aid);
        int broot = getRepresentative(bid);

        if (aroot == broot)
            return aroot;

        int asz = data[SZ*aroot+1];
        int bsz = data[SZ*broot+1];

        if (asz > bsz) {
            data[SZ*broot] = aroot;
            data[SZ*aroot+1] += bsz;

            return aroot;
        } else {
            data[SZ*aroot] = broot;
            data[SZ*broot+1] += asz;

            return broot;
        }
    }
};
#endif
