#include <stdio.h>
#include <math.h>
#include <time.h>
#include <vector>
#include <map>

#include <velodyne/velodyne.h>
#include <velodyne/velodyne_extractor.h>


class segmenter{

    bool v;

    public:

    struct segment_feature_t {
        double minXYZ[3];
        double maxXYZ[3];
        double meanPos[3];
        double axisErr[3];
    };

    double getTimeDiff(timeval a, timeval b)
    {
        double first = a.tv_sec + (a.tv_usec/1000000.0);
        double second = b.tv_sec + (b.tv_usec/1000000.0);

        return (first - second)*1000;
    }

    void buildGraph(xyz_point_list_t *xyzList, std::map<int, 
                            std::vector<xyz_point_t> > &segPoints);

    void getFeatures(std::map<int, std::vector<xyz_point_t> > &segPoints, 
                        segmenter::segment_feature_t *sf);

    bool getPoles(std::map<int, std::vector<xyz_point_t> > &segPoints, 
                        segmenter::segment_feature_t *sf);
    void getVector(std::vector<double*> &handPts);
    segmenter(bool verbose);
};
