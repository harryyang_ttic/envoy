include_directories(${OPENNI_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS})

add_executable(er-velodyne-pole-segmenter main.cpp segmenter.cpp)
#pods_install_pkg_config_file(er-velodyne-person-segmenter REQUIRES velodyne)

pods_use_pkg_config_packages(er-velodyne-pole-segmenter lcm bot2-core bot2-lcmgl-client lcmtypes_er-lcmtypes velodyne velodyne_extractor)

target_link_libraries(er-velodyne-pole-segmenter jpeg z ${OPENNI_LIBS})

pods_install_executables(er-velodyne-pole-segmenter)
