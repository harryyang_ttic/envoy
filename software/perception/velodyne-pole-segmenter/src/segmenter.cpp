#include "segmenter.hpp"
#include "UnionFindSimple.hpp"
#include <unistd.h>

#define MIN_SIZE 50
#define MAX_SIZE 1000
#define MIN_DIST 0.5
#define MAX_DIST 4.0

#define NUM_ROWS 32
#define DEPTH_THRESHOLD .05

inline static double distance(double* xyz1, double*xyz2)
{
    double t = sqrt(pow(xyz1[0]-xyz2[0], 2) + pow(xyz1[1]-xyz2[1], 2));
    // Ignore the Z component since the lasers are far apart
    return t; 
}

inline static double ThreeDDistance(double* xyz1, double*xyz2)
{
    return sqrt(pow(xyz1[0]-xyz2[0], 2) 
                    + pow(xyz1[1]-xyz2[1], 2)
                    + pow(xyz1[2]-xyz2[2], 2));
}

void segmenter::buildGraph(xyz_point_list_t *xyzList, 
                std::map<int,std::vector<xyz_point_t> > &segPoints)
{
    assert(xyzList->no_points%NUM_ROWS == 0);
    uint32_t num_cols = (xyzList->no_points)/NUM_ROWS;

    UnionFindSimple uf(xyzList->no_points);

    int i,j;
    // Velodyne data is ordered from [0-num_cols][0-NUM_ROWS] with the top
    // row first and the bottow row last
    for (i = 0; i < num_cols; i++)
    {
        for (j = 0; j < NUM_ROWS-12; j++)
        {
            int pos = i*NUM_ROWS+j;

            // Get the down point
            if (distance(xyzList->points[pos].xyz, xyzList->points[pos+1].xyz)< 
                DEPTH_THRESHOLD)
            {
                uf.connectNodes(pos, pos+1);
            }
            int next = (pos+NUM_ROWS)%xyzList->no_points;
            if (distance(xyzList->points[pos].xyz, xyzList->points[next].xyz) < 
                DEPTH_THRESHOLD)
            {
                uf.connectNodes(pos, next);
            }
        }
    }


    for (i = 0; i < xyzList->no_points; i++)
    {
        int rep = uf.getRepresentative(i);

        if (uf.getSetSize(rep) > MIN_SIZE && uf.getSetSize(rep) < MAX_SIZE)
        {
            segPoints[rep].push_back(xyzList->points[i]);
        }
    }
}

void segmenter::getFeatures(std::map<int, std::vector<xyz_point_t> > 
                                &segPoints, segmenter::segment_feature_t* sf)
{
    std::map<int, std::vector<xyz_point_t> >::iterator mapIt;
    std::vector<xyz_point_t>::iterator vecIt;

    // Get mean point
    int i = 0;
    for (mapIt = segPoints.begin(); mapIt != segPoints.end(); mapIt++, i++)
    {
        sf[i].meanPos[0] = 0;
        sf[i].meanPos[1] = 0;
        sf[i].meanPos[2] = 0;

        sf[i].maxXYZ[0] = 0;
        sf[i].maxXYZ[1] = 0;
        sf[i].maxXYZ[2] = 0;

        sf[i].minXYZ[0] = 10;
        sf[i].minXYZ[1] = 10;
        sf[i].minXYZ[2] = 10;

        for (vecIt = (*mapIt).second.begin(); 
             vecIt != (*mapIt).second.end(); vecIt++)
        {
            sf[i].meanPos[0] += (*vecIt).xyz[0];
            sf[i].meanPos[1] += (*vecIt).xyz[1];
            sf[i].meanPos[2] += (*vecIt).xyz[2];

            if ((*vecIt).xyz[0] > sf[i].maxXYZ[0])
                sf[i].maxXYZ[0] = (*vecIt).xyz[0];
            if ((*vecIt).xyz[1] > sf[i].maxXYZ[1])
                sf[i].maxXYZ[1] = (*vecIt).xyz[1];
            if ((*vecIt).xyz[2] > sf[i].maxXYZ[2])
                sf[i].maxXYZ[2] = (*vecIt).xyz[2];

            if ((*vecIt).xyz[0] < sf[i].minXYZ[0])
                sf[i].minXYZ[0] = (*vecIt).xyz[0];
            if ((*vecIt).xyz[1] < sf[i].minXYZ[1])
                sf[i].minXYZ[1] = (*vecIt).xyz[1];
            if ((*vecIt).xyz[2] < sf[i].minXYZ[2])
                sf[i].minXYZ[2] = (*vecIt).xyz[2];
        }
        sf[i].meanPos[0] /= (*mapIt).second.size();
        sf[i].meanPos[1] /= (*mapIt).second.size();
        sf[i].meanPos[2] /= (*mapIt).second.size();
    }

    // Use centroid to get total error in each direction
    i = 0;
    for (mapIt = segPoints.begin(); mapIt != segPoints.end(); mapIt++, i++)
    {
        sf[i].axisErr[0] = 0;
        sf[i].axisErr[1] = 0;
        sf[i].axisErr[2] = 0;

        double x = sf[i].meanPos[0];
        double y = sf[i].meanPos[1];
        double z = sf[i].meanPos[2];

        for (vecIt = (*mapIt).second.begin(); 
             vecIt != (*mapIt).second.end(); vecIt++)
        {
            sf[i].axisErr[0] += abs((*vecIt).xyz[0]-x);
            sf[i].axisErr[1] += abs((*vecIt).xyz[1]-y);
            sf[i].axisErr[2] += abs((*vecIt).xyz[2]-z);
        }
        sf[i].axisErr[0] /= (*mapIt).second.size();
        sf[i].axisErr[1] /= (*mapIt).second.size();
        sf[i].axisErr[2] /= (*mapIt).second.size();
    }
}
struct temp{
    double x, y, z;
    double dist;
};

bool segmenter::getPoles(std::map<int, std::vector<xyz_point_t> > 
                                &segPoints, segmenter::segment_feature_t* sf)
{
    std::map<int, std::vector<xyz_point_t> >::iterator mapIt;
    bool ret = false;

    std::vector<int> mapErase;

    int i = 0;
    for (mapIt = segPoints.begin(); mapIt != segPoints.end(); mapIt++, i++)
    {
        double x = sf[i].meanPos[0];
        double y = sf[i].meanPos[1];
        double z = sf[i].meanPos[2];

        double dist = sqrt(pow(x,2)+pow(y,2));
        double width = sqrt(pow(sf[i].maxXYZ[0] - sf[i].minXYZ[0],2) +
                            pow(sf[i].maxXYZ[1] - sf[i].maxXYZ[1],2));
        // If the radius distance from the sensor is outside the range we're
        // looking for, or the are ignore that entry
        if (
            dist > MAX_DIST || 
            dist < MIN_DIST ||
            2*sf[i].axisErr[0] > sf[i].axisErr[2] ||
            2*sf[i].axisErr[1] > sf[i].axisErr[2] ||
            sf[i].maxXYZ[2] > 1.3 ||
            sf[i].maxXYZ[2] < 0.5 ||
            width > 0.4 ||
            0)
        {
            mapErase.push_back((*mapIt).first);
            continue;
        }
    }


    std::vector<int>::iterator eraseIt;
    for (eraseIt = mapErase.begin(); eraseIt < mapErase.end(); eraseIt++)
    {
        segPoints.erase(*eraseIt);
    }

    return ret;
}



segmenter::segmenter(bool verbose)
{
    v = verbose;

    if (v) printf("starting segmenter\n");
}
/*
~segmenter()
{
    free(threshDepth);
}
*/

