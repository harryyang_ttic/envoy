import cv2
import numpy as np
import os, fnmatch, time
import scipy.io

from collections import defaultdict, namedtuple
from utils.frame_utils import Frame, KinectFrame

def read_dataset(directory, pattern): 
    # Get directory, and filename pattern
    directory = os.path.expanduser(directory)

    if not os.path.exists(directory): 
        raise Exception("""Path %s doesn't exist""" % directory)

    # Build dictionary [full_root_path -> pattern_matched_files]
    fn_map = {}
    for root, dirs, files in os.walk(directory): 

        # Filter only filename matches 
        matches = [os.path.join(root, fn) 
                   for fn in fnmatch.filter(files, pattern)]
        if not len(matches): continue
        fn_map[root] = matches
    return fn_map

class RGBDDatasetReaderUW:
    """
    RGB-D Dataset reader 
    http://rgbd-dataset.cs.washington.edu/dataset.html
    """

    def __init__(self, directory): 

        # Initialize dataset reader
        fn_map = read_dataset(directory, '*.png')

        # Build rgbd filename map [object_type -> [(rgb_fn,depth_fn), .... ]
        self.rgbd_fn_map = defaultdict(list)
        self.mat_fn_map = {}

        # Index starts at 1
        frame_info = namedtuple('frame_info', ['index', 'rgb_fn', 'depth_fn'])
        for root,v in fn_map.iteritems(): 
            idx = 1;
            while True: 
                rgb_path = os.path.join(root, '%s_%i.png' % 
                                        (os.path.basename(root), idx))
                depth_path = os.path.join(root, '%s_%i_depth.png' % 
                                          (os.path.basename(root), idx))
                if not os.path.exists(rgb_path) or not os.path.exists(depth_path): 
                    break

                self.rgbd_fn_map[os.path.basename(root)].append(
                    frame_info(index=idx, rgb_fn=rgb_path, depth_fn=depth_path)
                )
                idx += 1

        # Convert mat files
        mat_map = read_dataset(directory, '*.mat')
        for files in mat_map.values(): 
            for fn in files: 
                self.mat_fn_map[os.path.splitext(os.path.basename(fn))[0]] = fn
        print self.mat_fn_map

        # Store relevant metadata about dataset
        self.total_frames = idx
        self.rgb = True

    # Retrieve metadata
    def get_metadata(self, mat_fn): 
        return scipy.io.loadmat(mat_fn, squeeze_me=True, struct_as_record=False)

    # Retrieve frame (would be nice to use generator instead)
    def get_frame(self, rgb_fn, depth_fn): 
        rgb, depth = cv2.imread(rgb_fn, 1), cv2.imread(depth_fn, -1)
        return rgb, depth

if __name__ == "__main__": 
    # Read dataset
    rgbd_data_uw = RGBDDatasetReaderUW('~/data/rgbd-datasets/udub/rgbd-scenes')

    # Get metadata for object (bounding boxes)
    # Note: length of metadata and frames should be the same
    matfile = rgbd_data_uw.mat_fn_map['table_1']
    bboxes = rgbd_data_uw.get_metadata(matfile)['bboxes']

    # Get files for object
    files = rgbd_data_uw.rgbd_fn_map['table_1']

    # Construct frames with (rgb, d) filenames
    runtime = []
    for fidx, (bbox, f) in enumerate(zip(bboxes, files)): 
        rgb, depth = rgbd_data_uw.get_frame(f.rgb_fn, f.depth_fn)
        t1 = time.time()
        KinectFrame(f.index, rgb, depth, skip=1)
        print [(bb.category, bb.instance, bb.top, bb.bottom, bb.left, bb.right) 
               for bb in np.array([bbox]).flatten()]
        runtime.append((time.time() - t1) * 1e3)
        if fidx % 10 == 0: print 'Processed ', fidx
    print 'Done! Processed %i items. %i bboxes' % (len(files), len(bboxes))
    print 'Average runtime: ', np.mean(runtime), 'ms'

    
