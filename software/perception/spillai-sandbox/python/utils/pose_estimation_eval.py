#!/usr/bin/python 
# TODO: Compute relative pose mean instead of absolute mean pose

from __future__ import division

import random, time, itertools, operator
import numpy as np
np.set_printoptions(precision=3, suppress=True)
import cv2
import networkx as nx
import pandas as pd

from collections import defaultdict, namedtuple
from rigid_transform import Quaternion, RigidTransform, tf_compose, tf_construct

import utils.draw_utils as draw_utils
import utils.plot_utils as plot_utils

from utils.db_utils import AttrDict
from utils.logplayer_utils import LCMLogPlayer
from utils.tracker_utils import AprilTagsFeatureTracker
from utils.pose_utils import mean_pose, mean_rpy, wrap_angle, wrap_angle2

import matplotlib as mpl
from matplotlib import rc
import matplotlib.pylab as plt

from mpltools import style
style.use('ggplot')

rc('font',**{'family':'sans-serif','sans-serif':[], 'size':16})
rc('text', usetex=True)

class PoseEstimationEvalulation: 
    def __init__(self, fn, pose_est, sampler=None): 
        self.frame_pose = draw_utils.get_frame('KINECT')
        self.player = LCMLogPlayer(fn=fn)
        self.atag = AprilTagsFeatureTracker()
        self.pose_pub = defaultdict(list)

        # Direct comparison against pose estimates, with appropriate utimes

        # Find all relevant utimes 
        all_utimes = set()
        for label, utime_poses in pose_est: 
            utimes = map(lambda (utime,poses): utime, utime_poses)
            all_utimes = all_utimes.union(set(utimes))
        all_utimes = sorted(list(all_utimes))

        # Get tag poses for each utime
        tag_pose_map = self.get_tags(all_utimes)

        # For each label, find the relative pose error, with each of the tags
        print '================================================='
        self.corr_map = dict()
        for label, utime_poses in pose_est: 
            if label < 0: continue
            print '-------------------------------------------------'
            print 'Est. Label: ', label
            rels = self.relative_pose_error(utime_poses, tag_pose_map)
            if not len(rels): continue
            
            err_data = min(rels.values(), key=lambda x: x.tvec_err)
            print 'MATCH ID', err_data.tid, 'ERR', err_data.tvec_err
            # print err_data.tvecs_off, err_data.rpys_off
            self.corr_map[label] = err_data

        viz_poses = []
        for utime_poses in tag_pose_map.values(): 
            viz_poses.extend([pose for utime,pose in utime_poses])
        draw_utils.publish_pose_list2('ARTAG_POSES', viz_poses, sensor_tf='KINECT')

        # Absolute pose, and error
        data_map = defaultdict(list)
        for label, err_data in self.corr_map.iteritems(): 
            data_map['Rel-Roll'].extend(np.unwrap(err_data.rpys_off[:,0]))
            data_map['Rel-Pitch'].extend(np.unwrap(err_data.rpys_off[:,1]))
            data_map['Rel-Yaw'].extend(np.unwrap(err_data.rpys_off[:,2]))

            data_map['Rel-X'].extend(err_data.tvecs_off[:,0])
            data_map['Rel-Y'].extend(err_data.tvecs_off[:,1])
            data_map['Rel-Z'].extend(err_data.tvecs_off[:,2])

            tag_tvecs = np.vstack(map(lambda x: x.tvec, err_data.tag_poses))
            est_tvecs = np.vstack(map(lambda x: x.tvec, err_data.est_poses))

            tag_rpys = np.vstack(map(lambda x: [x.quat.to_roll_pitch_yaw()], 
                                     err_data.tag_poses))
            est_rpys = np.vstack(map(lambda x: [x.quat.to_roll_pitch_yaw()], 
                                     err_data.est_poses))

            data_map['Abs-Tag-Roll'].extend(np.unwrap(tag_rpys[:,0]))
            data_map['Abs-Tag-Pitch'].extend(np.unwrap(tag_rpys[:,1]))
            data_map['Abs-Tag-Yaw'].extend(np.unwrap(tag_rpys[:,2]))

            data_map['Abs-Tag-X'].extend(tag_tvecs[:,0])
            data_map['Abs-Tag-Y'].extend(tag_tvecs[:,1])
            data_map['Abs-Tag-Z'].extend(tag_tvecs[:,2])

            data_map['Abs-Est-Roll'].extend(np.unwrap(np.unwrap(tag_rpys[:,0]) + 
                                            np.unwrap(err_data.rpys_off[:,0])))
            data_map['Abs-Est-Pitch'].extend(np.unwrap(np.unwrap(tag_rpys[:,1]) + 
                                                       np.unwrap(err_data.rpys_off[:,1])))
            data_map['Abs-Est-Yaw'].extend(np.unwrap(np.unwrap(tag_rpys[:,2]) + 
                                                     np.unwrap(err_data.rpys_off[:,2])))
            # data_map['Abs-Est-Roll'].extend(np.unwrap(np.unwrap(est_rpys[:,0])))
            # data_map['Abs-Est-Pitch'].extend(np.unwrap(np.unwrap(est_rpys[:,1])))
            # data_map['Abs-Est-Yaw'].extend(np.unwrap(np.unwrap(est_rpys[:,2])))



            data_map['Abs-Est-X'].extend(est_tvecs[:,0])
            data_map['Abs-Est-Y'].extend(est_tvecs[:,1])
            data_map['Abs-Est-Z'].extend(est_tvecs[:,2])

            data_map['Rel-Roll'] = map(np.rad2deg, data_map['Rel-Roll'])
            data_map['Rel-Pitch'] = map(np.rad2deg, data_map['Rel-Pitch'])
            data_map['Rel-Yaw'] = map(np.rad2deg, data_map['Rel-Yaw'])

            data_map['Abs-Tag-Roll'] = map(np.rad2deg, data_map['Abs-Tag-Roll'])
            data_map['Abs-Tag-Pitch'] = map(np.rad2deg, data_map['Abs-Tag-Pitch'])
            data_map['Abs-Tag-Yaw'] = map(np.rad2deg, data_map['Abs-Tag-Yaw'])

            data_map['Abs-Est-Roll'] = map(np.rad2deg, data_map['Abs-Est-Roll'])
            data_map['Abs-Est-Pitch'] = map(np.rad2deg, data_map['Abs-Est-Pitch'])
            data_map['Abs-Est-Yaw'] = map(np.rad2deg, data_map['Abs-Est-Yaw'])
            
        # df_data_map = pd.DataFrame(data_map)
        # df_err_data_map = pd.DataFrame(err_data_map)

        # plt.plot(df.index, df.roll, '.', label='roll')

        # ax = df_relxyz.plot()
        # ax.set_xlabel('Observations')
        # ax.set_ylabel('Error (m)')
        # ax.set_ylim([-0.2, 0.2])
        # ax.set_title('Positional Error (m) compared against April Tags')

        # ax = df_relrpy.plot()
        # ax.set_xlabel('Observations')
        # ax.set_ylabel('Error (deg)')
        # ax.set_ylim([-20, 20])
        # ax.set_title('Orientation Error (deg) compared against April Tags')

        # ============================================================
        # special.errorfill(x, y_sin, y_err, label='sin', label_fill='sin error')
        # special.errorfill(x, y_cos, y_err, label='cos', label_fill='cos error',
        #                   alpha_fill=0.1)

        f = plt.figure(1)
        f.clf()
        f.subplots_adjust(hspace=0.55)
        ax = f.add_subplot(3, 1, 1)
        xs = np.arange(len(data_map['Abs-Tag-X']))
        err = np.max(np.fabs(np.array(data_map['Abs-Tag-X']) - 
                             np.array(data_map['Abs-Est-X'])))
        ax.plot(data_map['Abs-Tag-X'], marker='o', markersize=3, linewidth=2, label='Tag')
        ax.plot(data_map['Abs-Est-X'], marker='o', markersize=3, linewidth=2, label='Ours')
        ax.fill_between(x=xs, 
                        y1=data_map['Abs-Est-X']-err, 
                        y2=data_map['Abs-Est-X']+err, 
                        alpha=0.5,
                        label='Abs-Err-X', interpolate=True, facecolor='gray')
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.4), fancybox=True, ncol=2)
        ax.set_xlim([min(xs), max(xs)])
        # ax.set_ylim([-0.5, 0.5])
        ax.set_xlabel('Observations')
        ax.set_ylabel('Position X (m)')

        ax = f.add_subplot(3, 1, 2)
        err = np.max(np.fabs(np.array(data_map['Abs-Tag-Y']) - 
                             np.array(data_map['Abs-Est-Y'])))
        ax.plot(data_map['Abs-Tag-Y'], marker='o', markersize=3, linewidth=2, label='Abs-Tag-Y')
        ax.plot(data_map['Abs-Est-Y'], marker='o', markersize=3, linewidth=2, label='Abs-Est-Y')
        ax.fill_between(x=xs, 
                        y1=data_map['Abs-Est-Y']-err, 
                        y2=data_map['Abs-Est-Y']+err, alpha=0.5,
                        label='Abs-Err-Y', interpolate=True, facecolor='gray')
        ax.set_xlim([min(xs), max(xs)])
        # ax.set_ylim([-0.5, 0.5])
        ax.set_xlabel('Observations')
        ax.set_ylabel('Position Y (m)')


        ax = f.add_subplot(3, 1, 3)
        err = np.max(np.fabs(np.array(data_map['Abs-Tag-Z']) - 
                             np.array(data_map['Abs-Est-Z'])))
        ax.plot(data_map['Abs-Tag-Z'], marker='o', markersize=3, linewidth=2, label='Abs-Tag-Z')
        ax.plot(data_map['Abs-Est-Z'], marker='o', markersize=3, linewidth=2, label='Abs-Est-Z')
        ax.fill_between(x=xs, 
                        y1=data_map['Abs-Est-Z']-err, 
                        y2=data_map['Abs-Est-Z']+err, alpha=0.5,
                        label='Abs-Err-Z', interpolate=True, facecolor='gray')
        ax.set_xlim([min(xs), max(xs)])
        # ax.set_ylim([-0.5, 0.5])
        ax.set_xlabel('Observations')
        ax.set_ylabel('Position Z (m)')
        # ax.set_title('Position (m) compared against April Tags')

        
        f = plt.figure(2)
        f.subplots_adjust(hspace=0.55)
        ax = f.add_subplot(3, 1, 1)
        err = np.max(np.fabs(np.array(data_map['Abs-Tag-Roll']) - 
                             np.array(data_map['Abs-Est-Roll'])))
        # th_err = np.fabs(np.deg2rad(np.array(data_map['Abs-Tag-Roll']) 
        #                             - np.array(data_map['Abs-Est-Roll'])))
        # err = 0.035 th_err * 1.0 / (np.pi/2 - th_err)

        ax.plot(data_map['Abs-Tag-Roll'], marker='o', markersize=3, linewidth=2, label='Tag')
        ax.plot(data_map['Abs-Est-Roll'], marker='o', markersize=3, linewidth=2, label='Ours')
        ax.fill_between(x=xs, 
                        y1=data_map['Abs-Est-Roll']-err, 
                        y2=data_map['Abs-Est-Roll']+err, alpha=0.5,
                        label='Abs-Err-Roll', interpolate=True, facecolor='gray')
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.4), fancybox=True, ncol=2)
        ax.set_xlim([min(xs), max(xs)])
        mu = data_map['Abs-Tag-Roll'][0]
        off = max(max(data_map['Abs-Tag-Roll']) - min(data_map['Abs-Tag-Roll']) + 10, 40)
        ax.set_ylim([mu-off, mu+off])
        ax.set_xlabel('Observations')
        ax.set_ylabel('Roll (deg)')

        ax = f.add_subplot(3, 1, 2)
        err = np.max(np.fabs(np.array(data_map['Abs-Tag-Pitch']) - 
                             np.array(data_map['Abs-Est-Pitch'])))
        ax.plot(data_map['Abs-Tag-Pitch'], marker='o', markersize=3, linewidth=2, label='Abs-Tag-Pitch')
        ax.plot(data_map['Abs-Est-Pitch'], marker='o', markersize=3, linewidth=2, label='Abs-Est-Pitch')
        ax.fill_between(x=xs, 
                        y1=data_map['Abs-Est-Pitch']-err, 
                        y2=data_map['Abs-Est-Pitch']+err, alpha=0.5,
                        label='Abs-Err-Pitch', interpolate=True, facecolor='gray')
        ax.set_xlim([min(xs), max(xs)])
        mu = data_map['Abs-Tag-Pitch'][0]
        off = max(max(data_map['Abs-Tag-Pitch']) - min(data_map['Abs-Tag-Pitch']) + 10, 40)
        ax.set_ylim([mu-off, mu+off])
        ax.set_xlabel('Observations')
        ax.set_ylabel('Pitch (deg)')


        ax = f.add_subplot(3, 1, 3)
        err = np.max(np.fabs(np.array(data_map['Abs-Tag-Yaw']) - 
                             np.array(data_map['Abs-Est-Yaw'])))
        ax.plot(data_map['Abs-Tag-Yaw'], marker='o', markersize=3, linewidth=2, label='Abs-Tag-Yaw')
        ax.plot(data_map['Abs-Est-Yaw'], marker='o', markersize=3, linewidth=2, label='Abs-Est-Yaw')
        ax.fill_between(x=xs, 
                        y1=data_map['Abs-Est-Yaw']-err, 
                        y2=data_map['Abs-Est-Yaw']+err, alpha=0.5,
                        label='Abs-Err-Yaw', interpolate=True, facecolor='gray')
        ax.set_xlim([min(xs), max(xs)])
        mu = data_map['Abs-Tag-Yaw'][0]
        off = max(max(data_map['Abs-Tag-Yaw']) - min(data_map['Abs-Tag-Yaw']) + 10, 40)
        ax.set_ylim([mu-off, mu+off])
        ax.set_xlabel('Observations')
        ax.set_ylabel('Yaw (deg)')
        # f.savefig('absolute-pose-comparison.png')
        # ax.set_title('Position (m) compared against April Tags')

        # # ============================================================

        # f = plt.figure(2)
        # f.clf()
        # ax = f.add_subplot(3, 2, 1)
        # xs = np.arange(len(data_map['Rel-X']))
        # err = max(np.fabs(data_map['Rel-X']))
        # errhi = max(data_map['Rel-X'])
        # errlo = min(data_map['Rel-X'])
        # ax.plot(data_map['Rel-X'], marker='o', label='Rel-X', markersize=3, linewidth=2)
        # ax.axhline(y=0, linestyle='--', color='gray')
        # ax.fill_between(x=xs, 
        #                 y1=-err, 
        #                 y2=err, alpha=0.25,
        #                 label='Rel-Err-X', interpolate=True, facecolor='gray')
        # ax.legend()
        # ax.set_xlim([min(xs), max(xs)])
        # ax.set_ylim([-0.15, 0.15])
        # ax.set_xlabel('Observations')
        # ax.set_ylabel('Position Error (m)')
        # # ax.set_title('Relative Position Error in X (m)')

        # ax = f.add_subplot(3, 2, 3)
        # err = max(np.fabs(data_map['Rel-Y']))
        # errhi = max(data_map['Rel-Y'])
        # errlo = min(data_map['Rel-Y'])
        # ax.plot(data_map['Rel-Y'], marker='o', label='Rel-Y', markersize=3, linewidth=2)
        # ax.axhline(y=0, linestyle='--', color='gray')
        # ax.fill_between(x=xs, 
        #                 y1=-err, 
        #                 y2=err, alpha=0.25,
        #                 label='Rel-Err-Y', interpolate=True, facecolor='gray')
        # ax.legend()
        # ax.set_xlim([min(xs), max(xs)])
        # ax.set_ylim([-0.15, 0.15])
        # ax.set_xlabel('Observations')
        # ax.set_ylabel('Position Error (m)')
        # # ax.set_title('Relative Position Error in Y (m)')

        # ax = f.add_subplot(3, 2, 5)
        # err = max(np.fabs(data_map['Rel-Z']))
        # errhi = max(data_map['Rel-Z'])
        # errlo = min(data_map['Rel-Z'])
        # ax.plot(data_map['Rel-Z'], marker='o', label='Rel-Z', markersize=3, linewidth=2)
        # ax.axhline(y=0, linestyle='--', color='gray')
        # ax.fill_between(x=xs, 
        #                 y1=-err, 
        #                 y2=err, alpha=0.25,
        #                 label='Rel-Err-Z', interpolate=True, facecolor='gray')
        # ax.legend()
        # ax.set_xlim([min(xs), max(xs)])
        # ax.set_ylim([-0.15, 0.15])
        # ax.set_xlabel('Observations')
        # ax.set_ylabel('Position Error (m)')
        # # ax.set_title('Relative Position Error in Z (m)')

        # ax = f.add_subplot(3, 2, 2)
        # err = max(np.fabs(data_map['Rel-Roll']))
        # errhi = max(data_map['Rel-Roll'])
        # errlo = min(data_map['Rel-Roll'])
        # ax.plot(data_map['Rel-Roll'], marker='o', label='Rel-Roll', markersize=3, linewidth=2)
        # ax.axhline(y=0, linestyle='--', color='gray')
        # ax.fill_between(x=xs, 
        #                 y1=-err, 
        #                 y2=err, alpha=0.25,
        #                 label='Rel-Err-Roll', interpolate=True, facecolor='gray')
        # ax.legend()
        # ax.set_xlim([min(xs), max(xs)])
        # ax.set_ylim([-50, 50])
        # ax.set_xlabel('Observations')
        # ax.set_ylabel('Orientation Error (deg)')
        # # ax.set_title('Relative Orientation Error (Pitch - deg)')

        # ax = f.add_subplot(3, 2, 4)
        # err = max(np.fabs(data_map['Rel-Pitch']))
        # errhi = max(data_map['Rel-Pitch'])
        # errlo = min(data_map['Rel-Pitch'])
        # ax.plot(data_map['Rel-Pitch'], marker='o', label='Rel-Pitch', markersize=3, linewidth=2)
        # ax.axhline(y=0, linestyle='--', color='gray')
        # ax.fill_between(x=xs, 
        #                 y1=-err, 
        #                 y2=err, alpha=0.25,
        #                 label='Rel-Err-Pitch', interpolate=True, facecolor='gray')
        # ax.legend()
        # ax.set_xlim([min(xs), max(xs)])
        # ax.set_ylim([-50, 50])
        # ax.set_xlabel('Observations')
        # ax.set_ylabel('Orientation Error (deg)')

        # ax = f.add_subplot(3, 2, 6)
        # err = max(np.fabs(data_map['Rel-Yaw']))
        # errhi = max(data_map['Rel-Yaw'])
        # errlo = min(data_map['Rel-Yaw'])
        # ax.plot(data_map['Rel-Yaw'], marker='o', label='Rel-Yaw', markersize=3, linewidth=2)
        # ax.axhline(y=0, linestyle='--', color='gray')
        # ax.fill_between(x=xs, 
        #                 y1=-err, 
        #                 y2=err, alpha=0.25,
        #                 label='Rel-Err-Yaw', interpolate=True, facecolor='gray')
        # ax.legend()
        # ax.set_xlim([min(xs), max(xs)])
        # ax.set_ylim([-50, 50])
        # ax.set_xlabel('Observations')
        # ax.set_ylabel('Orientation Error (deg)')
        # # f.savefig('relative-pose-error.png')


    def get_corr_pose_map(self): 
        return self.corr_map

    def debug_pose(self, pose, ch='POSE_DEBUG', reset=False): 
        if reset: self.pose_pub[ch] = []
        self.pose_pub[ch].append(pose)
        draw_utils.publish_pose_list2(ch, self.pose_pub[ch], sensor_tf='KINECT')

    # per-label relative pose error with each tag
    def relative_pose_error(self, est_utime_poses, tag_pose_map): 

        err_map = dict()
        for tid, tag_utime_poses in tag_pose_map.iteritems():
            print '------- Tag : ', tid
            tag_pose_map = dict(tag_utime_poses)
            est_pose_map = dict(est_utime_poses)

            tag_utimes = tag_pose_map.keys()
            est_utimes = est_pose_map.keys()
            overlap = sorted(set(tag_utimes).intersection(set(est_utimes)))

            if not len(overlap): 
                print 'Warning! No overlapping utimes for tid: ', tid
                continue
            # print map(lambda x: x*1e-6, list(overlap))
                        
            rel_poses = []
            tag_poses, est_poses = [], []
            init_tagp, init_estp = None, None
            for utime in overlap: 
                tagp, estp = tag_pose_map[utime], \
                             est_pose_map[utime]

                if init_tagp is None: 
                    # Initialize tag, and estimated poses
                    init_tagp, init_estp = tagp, estp

                    # Estimate only rotation, fix translation
                    init_tagp0, init_estp0 = RigidTransform(init_tagp.quat, np.array([0,0,0])), \
                                             RigidTransform(init_estp.quat, np.array([0,0,0]))

                    # Relative rotation between tags detected, and estimated pose
                    init_rel = init_estp0.inverse() * init_tagp0

                    # Rotation of tag, and init est position
                    init_estp_t = RigidTransform(init_tagp.quat, init_estp.tvec); 

                # Absolute poses
                tagp1 = init_tagp.inverse() * tagp
                estp1 = init_estp_t.inverse() * estp * init_rel

                # Determine relative pose
                # rel = tagp1.inverse() * estp1
                rel = RigidTransform(estp1.quat * tagp1.quat.inverse(), tagp1.tvec - estp1.tvec)

                # reltag = init_tagp.inverse() * tagp
                # relest = init_estp.inverse() * estp
                rel_poses.append(rel)

                tag_poses.append(init_tagp * tagp1)
                est_poses.append(init_tagp * estp1)

                # if tid == 0: 

                #     self.debug_pose(init_tagp * estp1)
                #     # self.debug_pose(init_tagp * tagp1)
                #     time.sleep(0.1)
                #     # self.debug_pose(rel.inverse())
                #     # self.debug_pose(estT * tagT.inverse() * tagT)
                
                    
                #     # self.debug_pose(relest, ch='relest')
                #     # self.debug_pose(reltag, ch='reltag')


            # Get translations
            print 'TID', tid
            tvecs_off = np.vstack([p1.tvec - p2.tvec for p1, p2 in zip(tag_poses, est_poses)])

            tvecs = np.linalg.norm(tvecs_off, axis=1)
            tvec_err = np.std(tvecs)
            print 'tvecs_off: ', tvecs, 'tvec_err: ', tvec_err



            # Get rpy error
            quats_off = map(lambda x: Quaternion(rel_poses[0].quat.inverse() * x.quat), rel_poses)
            rpys_off = np.vstack(map(lambda x: [x.to_roll_pitch_yaw()], quats_off))


            err_map[tid] = AttrDict(tid=tid, tvec_err=tvec_err, 
                                    rel_poses=rel_poses, 
                                    tvecs_off=tvecs_off, rpys_off=rpys_off, 
                                    tag_poses=tag_poses, est_poses=est_poses, utimes=list(overlap)
            )

        return err_map

    def get_tags_all(self, utimes): 
        self.player.reset()
        while True: 
            frame = self.player.get_next_frame()
            if frame is None: break
            # print 'UTIME: ', frame.utime * 1e-6, utime * 1e-6
            frame.computeNormals(0.5, 0.7, 20.0)
            for tid, pose in self.atag.get_pose_map(frame).iteritems(): 
                tag_pose_map[tid].append((frame.utime,pose)) 
        return tag_pose_map

    # Can be more optimal, run once per utime
    def get_tags(self, utimes=None): 
        if utimes is None: return get_all_tags()

        sutimes = utimes[::4]

        tag_pose_map = defaultdict(list)
        for utime in sutimes: 
            frame = self.player.get_frame_with_utime(utime)
            # print 'UTIME: ', frame.utime * 1e-6, utime * 1e-6
            frame.computeNormals(0.5, 0.7, 20.0)
            for tid, pose in self.atag.get_pose_map(frame).iteritems(): 
                tag_pose_map[tid].append((frame.utime,pose)) 
        return tag_pose_map

class AprilTagsTrackerLog: 
    def __init__(self, fn, labels=[]): 
        self.player = LCMLogPlayer(fn=fn, k_frames=40)
        self.atag = AprilTagsFeatureTracker()
        self.labels = set(labels)

    def get_tags(self, ref_label=-1): 
        self.player.reset()

        utimes_set = set()
        tag_pose_map = defaultdict(list)
        while True: 
            frame = self.player.get_next_frame()
            if frame is None: break
            # print 'UTIME: ', frame.utime * 1e-6
            frame.computeNormals(0.5, 0.7, 20.0)
            for tid, pose in self.atag.get_pose_map(frame).iteritems(): 
                if tid not in self.labels: continue
                tag_pose_map[tid].append((frame.utime,pose)) 
                utimes_set.add(frame.utime)

        # Create a RIGID reference frame for all utimes
        for utime in sorted(utimes_set): 
            tag_pose_map[ref_label].append((utime,RigidTransform.identity())) 

        tag_pose_list = zip(tag_pose_map.keys(), tag_pose_map.values())
        tag_pose_list.sort(key=lambda x: x[0])                    
        return tag_pose_list
        
