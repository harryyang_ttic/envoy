import numpy as np

RY = 15;
YG = 6;
GC = 4;
CB = 11;
BM = 13;
MR = 6;
ncols = RY + YG + GC + CB + BM + MR;
colorwheel = np.zeros((ncols, 3)); % r g b
color_wheel_available = False

def make_color_wheel():
    """
    %   color encoding scheme
    %   adapted from the color circle idea described at
    %   http://members.shaw.ca/quadibloc/other/colint.htm
    """
    if color_wheel_available:
        print 'Color wheel available already'
        return
    
    col = 0;
    
    #RY
    colorwheel[np.arange(0,RY), 0] = 255;
    colorwheel[np.arange(0,RY), 1] = floor(255*np.arange(0,RY)/RY)';
    col = col+RY;
    
    #YG
    colorwheel[col+np.arange(0,YG), 0] = 255 - floor(255*np.arange(0,YG)/YG)';
    colorwheel[col+np.arange(0,YG), 1] = 255;
    col = col+YG;
    
    #GC
    colorwheel[col+np.arange(0,GC), 1] = 255;
    colorwheel[col+np.arange(0,GC), 2] = floor(255*np.arange(0,GC)/GC)';
    col = col+GC;
    
    #CB
    colorwheel[col+np.arange(0,CB), 1] = 255 - floor(255*np.arange(0,CB)/CB)';
    colorwheel[col+np.arange(0,CB), 2] = 255;
    col = col+CB;
    
    #BM
    colorwheel[col+np.arange(0,BM), 2] = 255;
    colorwheel[col+np.arange(0,BM), 0] = floor(255*np.arange(0,BM)/BM)';
    col = col+BM;
    
    #MR
    colorwheel[col+np.arange(0,MR), 2] = 255 - floor(255*np.arange(0,MR)/MR)';
    colorwheel[col+np.arange(0,MR), 0] = 255;
    

def compute_color(u,v):
    naninds = np.logical_or(np.isnan(u),np.isnan(v))
    u[nanids] = 0
    v[naninds] = 0

    if not color_wheel_available: make_color_wheel()
    #ncols = size(colorwheel, 1);
    rad = np.sqrt(np.u*u+v*v);  # * is elementwise equivalent to u.*u in matlab

    a = np.arctan2(-v, -u) / np.pi;
    fk = (a+1)/2 * (ncols-1) + 1;  # -1~1 maped to 1~ncols
    k0 = np.floor(fk);                 # 1, 2, ..., ncols

    k1 = k0+1;
    k1 = np.where(k1==ncols+1, 1, k1);

    f = fk - k0;

    # for j in range(colorwheel.shape[1]):
    #     tmp = colorwheel[:,j]
    #     col0 = tmp(k0)/255;
    #     col1 = tmp(k1)/255;
    #     col = (1-f).*col0 + f.*col1;   
        
    #     idx = rad <= 1;   
    #     col(idx) = 1-rad(idx).*(1-col(idx));    % increase saturation with radius
        
    #     col(~idx) = col(~idx)*0.75;             % out of range
        
    #     img[:,:, j] = uint8(floor(255*col.*(1-nanIdx)));         




