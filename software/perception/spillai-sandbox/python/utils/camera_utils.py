import cv2
import numpy as np
from rigid_transform import Quaternion, RigidTransform

class Camera: 
    def __init__(self): 
        pass

class KinectCamera(Camera): 
    def __init__(self, skip=1): 

	# Camera matrix
	self.rgbK = np.array([[528.49404721, 0, 319.5],
			      [0, 528.49404721, 239.5],
			      [0, 0, 1]], dtype=np.float64)

	self.depthK = np.array([[576.09757860, 0, 319.5],
				[0, 576.09757860, 239.5],
				[0, 0, 1]], dtype=np.float64)
        
        # Cached camera params 
        self.fx = self.depthK[0,0];
        self.cx = self.depthK[0,2];
        self.cy = self.depthK[1,2];
        self.fx_inv = 1.0 / self.fx;
        self.skip = skip

        # Dense calibration map
        h,w = 480,640
        xs,ys = np.arange(0,w), np.arange(0,h);
        self.xs = (xs-self.cx)*self.fx_inv
        # self.xs = self.xs[::self.skip] # skip pixels
        self.ys = (ys-self.cy)*self.fx_inv
        # self.ys = self.ys[::self.skip] # skip pixels
        self.xs, self.ys = np.meshgrid(self.xs, self.ys);

        # Project shift offsets, and baseline
        self.shift_offset = 1079.4753;
        self.projector_depth_baseline = 0.07214;

        # Dist coeff
        self.D = np.array([0., 0., 0., 0.], dtype=np.float64)

	rval, self.irgbK = cv2.invert(self.rgbK)
	self.irgbtK = self.irgbK.T

    # 16-bit depth 
    def get_cloud(self, depth, in_m=False): 
        if not in_m: depthf = depth.astype(np.float32) * 0.001; # in m
        else: depthf = depth
        # depthf = depthf[::self.skip,::self.skip]
        xs = np.multiply(self.xs, depthf);
        ys = np.multiply(self.ys, depthf)
        X = np.dstack((xs,ys,depthf))
        return X


    # # get fundamental matrix between two poses
    # def F(self, t1, t2): 
    #     p2w1 = self.relative(t1, t2)
    #     T2w1 = p2w1.to_homogeneous_matrix()
    #     E = np.dot(np.array([[0, -T2w1[2,3], T2w1[1,3]],
    #     		      [T2w1[2,3], 0, -T2w1[0,3]], 
    #     		      [-T2w1[1,3], T2w1[0,3], 0]]), T2w1[:3,:3])
    #     return np.dot(self.irgbtK, np.dot(E, self.irgbK))

    def project(self, X, pose): 
	T = pose.to_homogeneous_matrix()
	rvec,_ = cv2.Rodrigues(T[:3,:3])
	proj,_ = cv2.projectPoints(X.reshape((1,3)), rvec,  T[:3,3], self.depthK, self.D)
	return proj.reshape(2)

# static Mat getFundamentalMat( const Mat& R1, const Mat& t1,
#                               const Mat& R2, const Mat& t2,
#                               const Mat& cameraMatrix )
# {
#     Mat_<double> R = R2*R1.t(), t = t2 - R*t1;
#     double tx = t.at<double>(0,0), ty = t.at<double>(1,0), tz = t.at<double>(2,0);
#     Mat E = (Mat_<double>(3,3) << 0, -tz, ty, tz, 0, -tx, -ty, tx, 0)*R;
#     Mat iK = cameraMatrix.inv();
#     Mat F = iK.t()*E*iK;

# #if 0
#     static bool checked = false;
#     if(!checked)
#     {
#         vector<Point3f> objpoints(100);
#         Mat O(objpoints);
#         randu(O, Scalar::all(-10), Scalar::all(10));
#         vector<Point2f> imgpoints1, imgpoints2;
#         projectPoints(Mat(objpoints), R1, t1, cameraMatrix, Mat(), imgpoints1);
#         projectPoints(Mat(objpoints), R2, t2, cameraMatrix, Mat(), imgpoints2);
#         double* f = (double*)F.data;
#         for( size_t i = 0; i < objpoints.size(); i++ )
#         {
#             Point2f p1 = imgpoints1[i], p2 = imgpoints2[i];
#             double diff = p2.x*(f[0]*p1.x + f[1]*p1.y + f[2]) +
#                  p2.y*(f[3]*p1.x + f[4]*p1.y + f[5]) +
#                 f[6]*p1.x + f[7]*p1.y + f[8];
#             CV_Assert(fabs(diff) < 1e-3);
#         }
#         checked = true;
#     }
# #endif
#     return F;
# }

