import cv2;
import networkx as nx
import numpy as np
import lcm; lc = lcm.LCM()
import itertools, time, threading, logging

import heapq

import matplotlib.pylab as plt
import utils.draw_utils as draw_utils

from sklearn import cluster as skcluster
from sklearn import hmm as skhmm

from skimage.segmentation import join_segmentations
from scipy import spatial as spspatial

from utils.distributions import PosePairDistribution
from utils.distributions import plot_pose_pair_density, plot_pose_pair_variation

# from mpltools import style
# style.use('ggplot')

class TrajectoryClustering: 
    def __init__(self, data, posepair_distance_sigma, posepair_theta_sigma, 
                 min_track_intersection=10, time_chunks=5, 
                 top_k=10, sample_inds=0, 
                 verbose=True, visualize=False, save_dir=None, viz_frame=None): 

        # Cluster index map
        # [label] -> [inds]
        self.cluster_inds = dict()

	# Init labels
	data.labels = np.zeros(data.num_feats, dtype=np.int);
	data.labels.fill(-1);

        # Labels per time chunk
        data.labels_chunks = np.zeros((data.num_feats, time_chunks), dtype=np.int)
	data.labels_chunks.fill(-1);

        # Save params
        self.verbose = verbose
        self.visualize = visualize
        self.save_dir = save_dir
        self.min_track_intersection = min_track_intersection
        self.posepair_distance_sigma = posepair_distance_sigma
        self.posepair_theta_sigma = posepair_theta_sigma

        # Time chunking
        utimes_chunks = np.array_split(data.feature_utimes, time_chunks)
        utimes_inds_chunks = np.array_split(np.arange(data.num_utimes), time_chunks)

        # Run clustering on each chunk
        Ws = []
        for cidx, utimes_inds_chunk in enumerate(utimes_inds_chunks): 
            if viz_frame: 
                viz_frame(data.feature_utimes[utimes_inds_chunk[-1]])

            # print utimes_inds_chunk
            posepair_dict, W_dist, W_normal = self.build_similarity_matrix(data, 
                                                                           utimes_inds_chunk)

            # Build weight matrix with normals, and displacements
            W = np.minimum(W_dist,W_normal) 
            Ws.append( W )
            # Plot similarity matrix
            # if self.visualize: self.viz_similarity_matrix(W, 
            #                                               save_name='similarity-matrix-%i' % cidx) 

            # Find the clustering between trajectories
            if self.verbose: print '===> Cluster the trajectories ...'
            valid_feat_labels = self.cluster(data, W )

            # Viz data
            data.viz_data(utimes_inds=utimes_inds_chunk)

            # Assign cluster labels to the full dataset
            data.labels_chunks[data.valid_feat_inds,cidx] = valid_feat_labels
            # data.labels[data.valid_feat_inds] = valid_feat_labels
            
            if self.verbose: print '--- Done clustering'
            # time.sleep(2)
        
        # Viz probabilities over time chunks
        ws = np.vstack([W[np.triu_indices(W.shape[0], k=1)] for W in Ws])

        # Run overall clustering
        # valid_feats = len(data.valid_feat_inds)
        # tmp_labels = np.ones((valid_feats, valid_feats), dtype=np.bool)
        # # print 'Tmp: ', tmp_labels, data.labels_chunks[data.valid_feat_inds,0]
        # for cidx in np.arange(time_chunks): 
        #     bit = spspatial.distance.cdist(data.labels_chunks[data.valid_feat_inds,cidx][:,np.newaxis], 
        #                                    data.labels_chunks[data.valid_feat_inds,cidx][:,np.newaxis],
        #                                    metric='hamming') == 0
        #     tmp_labels = tmp_labels & bit
        #     print 'Tmp: \n', bit.astype(int)
        # data.labels[data.valid_feat_inds] = tmp_labels

        # Run overall clustering
        # Join segmentations across all time chunks
        tmp_labels = data.labels_chunks[data.valid_feat_inds,0]
        for cidx in np.arange(time_chunks)[1:]: 
            tmp_labels = join_segmentations(tmp_labels, 
                                            data.labels_chunks[data.valid_feat_inds,cidx])
        data.labels[data.valid_feat_inds] = tmp_labels

        # Build cluster index map
        # Only map top k clusters (based on size of each cluster)
        self.map_clusters(data, top_k=top_k, sample_inds=sample_inds)

        # Visualize clustering
        self.viz_clustering(data, data.valid_feat_inds, np.arange(data.num_utimes))

        # # Re-order dictionary for ground truth
        # label_dict = dict()
        # tmp_labels = data.labels;
        # for idx,l in enumerate(tmp_labels): 
        #     if l not in label_dict and l != -1: 
        #         label_dict[l] = len(label_dict)
        #     data.labels[idx] = label_dict[l]
        # print data.labels.T

        # # Viz similarity matrix per time chunk
        # if self.visualize: 
        #     f = plt.figure(1006)	
        #     f.subplots_adjust( hspace=0.4 )
        #     # self.viz_similarity_matrix_chunks(Ws, f)
        #     # if self.save_dir: plt.save_fig(''.join([self.save_dir,'motion-similarity']))
        print 'Number of labels: ', np.unique(data.labels)

        # Infer state sequences and their probs
        # ws_prob, ws_pred = self.infer_states(Ws)

    # Build cluster index map
    def map_clusters(self, data, top_k, sample_inds): 
        
        # Count number of instances of labels
        # Avoid -1 label
        unique_labels, unique_inv = np.unique(data.labels[np.where(data.labels != -1)], 
                                              return_inverse=True)
        num_labels = len(unique_labels)
        label_count = np.bincount(unique_inv)
        label_votes = zip(unique_labels, label_count)
        print 'Total: %i labels' % num_labels

        # Pick the top labeled trajectories
        label_votes.sort(key=lambda x: x[1], reverse=True)
        labels = [lv[0] for lv in label_votes[:top_k]]
        print 'Picking top %i labels: %s' % (len(labels), label_votes[:top_k])

        # Unlabel rest of the labeled trajectories
        rest_labels = [lv[0] for lv in label_votes[top_k:]]
        for label in rest_labels: 
            inds, = np.where(data.labels == label)
            data.labels[inds] = -1

        # Find exemplars for each cluster 
	for label in labels: 
            assert(label >= 0)

            # Indices that are assigned the "label"
	    inds, = np.where(data.labels == label)
	    assert(len(inds) != 0)

            # Complain about insufficient constraints
            if len(inds) < 3: 
                print 'WARNING: Label %i cluster has insufficient data constraints!' % (label)
                continue

            # Arbitrarily pick random set of label_inds (if many available)
            if sample_inds >= 3: 
                label_inds = random.sample(inds, min(sample_inds, len(inds)));
            else: 
                label_inds = inds

            # Map cluster indices
	    self.cluster_inds[label] = np.array(label_inds, dtype=np.int32)


    def infer_states(self, Ws): 
        # Weight vector for each pose-pair
        ws = np.vstack([W[np.triu_indices(W.shape[0], k=1)] for W in Ws])

        # Probabilities and predictions vector
        probs, preds = [], []
        for idx,obs in enumerate(ws.T): 
            nelems = len(obs)
            s_r = obs >= 0.95
            # s_tr = np.bitwise_and(obs < 0.95, obs.T > 0.8)
            s_n = obs <= 0.95
            
            obs_prob = np.vstack([s_r,s_n]).T
            _,ob = np.where(obs_prob)

            # if idx == 3: print obs_prob, ob
            w_startprob = np.array(obs_prob[0])
            w_transprob = np.array([[0.85, 0.15], [0.15, 0.85]])
            w_emissionprob = np.eye(2) # np.array([[0.8, 0.2], [0.2, 0.8]])
            w_hmm = skhmm.MultinomialHMM(n_components=2, startprob=w_startprob, 
                                         transmat=w_transprob)
            w_hmm.n_symbols_ = 2
            w_hmm.emissionprob_ = w_emissionprob
            
            pred = w_hmm.predict(ob, algorithm='viterbi')
            prob = w_hmm.predict_proba(ob)
            # print pred.shape, prob.shape, ob
            # if idx == 3: print obs, ob, pred, prob, s_tr

            preds.append(pred)
            probs.append(prob[tuple(np.vstack([np.arange(len(pred)), pred]))])

        ws_prob = np.vstack(probs).T
        ws_pred = np.vstack(preds).T
        
        if 1: 
            f = plt.figure(1007)	
            f.subplots_adjust( hspace=0.4 )
            ax1 = f.add_subplot(211)
            ax1.matshow(ws, vmin=0, vmax=1)

            ax2 = f.add_subplot(212)
            cax2 = ax2.matshow(ws_pred * 1, vmin=0, vmax=1)
            f.colorbar(cax2)

        return ws_prob, ws_pred


    def build_similarity_matrix(self, data, utimes_inds): 
        # Find the overlapping set of features between trajectories
        if self.verbose: print '===> Extracting overlapping features ...'
        overlap_dict = self.extract_overlapping_features(data, 
                                                         data.valid_feat_inds, 
                                                         utimes_inds)
        if self.verbose: print '--- Done extracting overlapping features'
        

        # Construct the similarity matrix between trajectories
        # Utime slicing is encapsulated within the overlap_dict
        if self.verbose: print '===> Compute trajectory similarity ...'
        posepair_dict, W_dist, W_normal = self.compute_trajectory_similarity(data, 
                                                                             data.valid_feat_inds, 
                                                                             overlap_dict)
        if self.verbose: print '--- Done computing pose-pairs'

        # Viz similarity in viewer
        if self.visualize: self.viz_similarity(data, posepair_dict)

        # Viz similarity
        # if self.visualize: self.viz_similarity_matrix(W_normal) 

        # # Viz a pose pair distribution for debug purposes
        # if self.save_dir: 
        #     f = plt.figure(1002)	
        #     plt.subplots_adjust( hspace=0.4 )

        #     ax1 = f.add_subplot(411)
        #     # plt.title('Histogram of variation in distance for a rigidly connected pose-pair');
        #     self.viz_posepair_distribution(posepair_dict[(0,1)], ax1, face='green')
        
        #     ax2 = f.add_subplot(412)
        #     # plt.title('Histogram of variation in distance for a non-rigidly connected pose-pair');
        #     self.viz_posepair_distribution(posepair_dict[(0,10)], ax2, face='blue')

        #     ax3 = f.add_subplot(423)
        #     # plt.title('Histogram of variation in distance for a non-rigidly connected pose-pair');
        #     self.viz_posepair_distribution(posepair_dict[(0,1)], ax3, distribution='density', face='green')

        #     ax4 = f.add_subplot(224)
        #     # plt.title('Histogram of variation in distance for a non-rigidly connected pose-pair');
        #     self.viz_posepair_distribution(posepair_dict[(0,10)], ax4, distribution='density', face='blue')

        #     plt.savefig(''.join([self.save_dir,'pose-pair-histogram-ground-truth.pdf']))
            

        return posepair_dict, W_dist, W_normal;

    # Find overlapping tracks ==============================================
    def extract_overlapping_features(self, data, valid_inds, utimes_inds):
        overlap_dict = {}

        num_inds = len(valid_inds);         
        niter, total_iters = 0, num_inds*(num_inds-1)/2;
        for jind,kind in itertools.product(valid_inds, valid_inds):
            if kind <= jind: continue;
            if niter % 200 == 0 and self.verbose: print 'Processed: %i out of %i' % (niter, total_iters)
            niter += 1 

            # find nz utime indices
            tj, = np.where(data.idx[jind,utimes_inds] != -1) # <- space of utimes_inds
            tj = utimes_inds[tj] # <- original space
            tk, = np.where(data.idx[kind,utimes_inds] != -1)
            tk = utimes_inds[tk]

            # set intersection
            intersection = np.intersect1d(tj,tk);
            if intersection.size < self.min_track_intersection: continue;

            # total_length = max(max(tk), max(tj)) - min(min(tk), min(tj))
            # if intersection.size * 1.0 / total_length < self.min_track_intersection: continue
            # # print 'tk, tj: ', total_length, intersection.size, \
            # #     intersection.size * 1.0 / total_length

            # # Test if the features are within 100 L1 ball radius
            # nn_inds = np.sum(np.fabs(data.xy[jind,intersection] - data.xy[kind,intersection]), 
            # 		     axis=1) < 100;
            # if not np.all(np.all(nn_inds, axis=0),axis=0): continue;
            
            # Build overlap dictionary
            overlap_dict[tuple(sorted((jind,kind)))] = intersection;
        if self.verbose: print 'Processed: %i out of %i' % (niter, total_iters)
        return overlap_dict;

    # Compute Pose-Pairs for overlapping tracks ============================
    def compute_trajectory_similarity(self, data, valid_inds, overlap_dict): 

        # Valid inds map:  valid_inds2inds => [4,5,9,...] -> [0,1,2,...] = {4:0,5:1,9:2}
        num_valid_feats = len(valid_inds);
        valid_inds2inds = dict( zip( valid_inds, np.arange(num_valid_feats) ) )

        # Build Weight/Affinity matrix
        W_normal = np.eye(num_valid_feats);
        W_dist = np.eye(num_valid_feats);
        # W_overlapk = np.zeros_like(W_dist)

        # Compute Pose Pair Similarity
        posepair_dict = dict();

        # debug_ids = [4,5,6,7,12,13,14,15]
        for id_pair,overlap_utimes_inds in overlap_dict.iteritems():
            # Evaluate Pose Pair Distribution
            ppd = PosePairDistribution(data, id_pair, 
                                       overlap_utimes_inds, 
                                       distance_sigma=self.posepair_distance_sigma, 
                                       theta_sigma=self.posepair_theta_sigma);
            # print ppd.distance_match, ppd.distance_hist

            # if id_pair[0] in debug_ids and id_pair[1] in debug_ids and abs(id_pair[0]-id_pair[1])>3: 
            #     print 'conf: ', ppd.confidence, ppd.min_confidence, ppd.max_confidence, \
            if self.verbose: print id_pair, ppd.distance_match
            if self.verbose: print id_pair[0], id_pair[1], ppd.distance_match

            if np.isnan(ppd.distance_match) or np.isnan(ppd.normal_match): continue
            posepair_dict[id_pair] = ppd;

            # Key stored is valid_feat_id -> valid_feat_id_idx via valid_inds2inds
            r, c = valid_inds2inds[id_pair[0]], valid_inds2inds[id_pair[1]]

            W_normal[r,c] = ppd.normal_match;
            W_normal[c,r] = ppd.normal_match;

            W_dist[r,c] = ppd.distance_match;
            W_dist[c,r] = ppd.distance_match;

            # W_overlapk[r,c] = len(overlap_utimes_inds)
            # W_overlapk[c,r] = len(overlap_utimes_inds)

        # # Pick top 3 overlapping indices
        # W_overlap = np.empty(shape=W_overlapk.shape, dtype=bool)
        # for jidx, row in enumerate(W_overlapk): 
        #     inds = map(lambda (idx,count): idx, 
        #                heapq.nlargest(5, enumerate(row), lambda x: x[1]))
        #     W_overlap[jidx, inds] = True

        # W_dist[~W_overlap] = 0
        # W_normal[~W_overlap] = 0

        return posepair_dict, W_dist, W_normal
        
    # Clustering ===========================================================
    def cluster(self, data, W): 

        # Cluster labels on the valid features
        exemplars,valid_labels = skcluster.dbscan(1-W, eps=.2, min_samples=3, metric='precomputed')
        # print exemplars, valid_labels

        if self.visualize: 
            # Shuffle cluster inds such that the cluster labels are together
            num_valid_feats = len(data.valid_feat_inds);
            assert(num_valid_feats == len(valid_labels));
            valid_indlabels = zip(np.arange(num_valid_feats), valid_labels)
            valid_indlabels.sort(key=lambda x: x[1]) # sort to bring cluster labels together
            valid_inds_shuffled, valid_labels_shuffled = zip(*valid_indlabels)

            # Count number of instances of labels
            unique_labels, unique_inv = np.unique(valid_labels, return_inverse=True)
            num_labels = len(unique_labels)
            label_count = np.bincount(unique_inv)
            label_votes = zip(unique_labels, label_count)
            label_votes.sort(key=lambda x: x[1], reverse=True)
        
            # Viz
            self.viz_clustering_matrix(W, valid_inds_shuffled)

        print valid_labels
        return valid_labels
        

    # Draw segmented features ==============================================
    def viz_clustering(self, data, valid_inds, utimes_inds): 
        # Visualize clusters in descending order of size
        viz_pts, viz_normals, viz_colors = [], [], []
        viz_traj1, viz_traj2 = [], []

        # Indices of label in the full dataset (could also look at valid dataset)
        labels = np.unique(data.labels)
        num_labels = len(labels)
        for label in labels:
            # Only among valid_inds
            inds, = np.where(data.labels[valid_inds] == label)
            inds = valid_inds[inds]
            print 'LABEL: ', label, inds

            # Publish each feature ind
            for idx in inds: 
                ut_inds, = np.where(data.idx[idx,utimes_inds] != -1);
                ut_inds = utimes_inds[ut_inds]

                viz_pts.append(data.xyz[idx,ut_inds,:])

                viz_traj1.append(data.xyz[idx,ut_inds[:-1],:])
                viz_traj2.append(data.xyz[idx,ut_inds[1:],:])

                viz_normals.append(data.xyz[idx,ut_inds,:] + data.normal[idx,ut_inds,:]*0.04)

                # Color by label
                carr = draw_utils.get_color_arr(label, len(ut_inds), palette_size=num_labels,
                                             color_by='label')
                viz_colors.append(carr)

        viz_pts = np.vstack(viz_pts)
        viz_colors = np.vstack(viz_colors)
        viz_normals = np.vstack(viz_normals)
        viz_traj1, viz_traj2 = np.vstack(viz_traj1), np.vstack(viz_traj2)

        draw_utils.publish_point_cloud('CLUSTERED_PTS', viz_pts, 
                                       c=viz_colors, sensor_tf='KINECT')
        draw_utils.publish_line_segments('CLUSTERED_NORMAL', viz_pts, viz_normals, 
                                         c=viz_colors, sensor_tf='KINECT')
        draw_utils.publish_line_segments('CLUSTERED_TRAJ', viz_traj1, viz_traj2, 
                                         c=viz_colors, sensor_tf='KINECT')

    # Display shuffled matrix with clusters together
    def viz_clustering_matrix(self, W, valid_inds_shuffled): 
        nrows,ncols = W.shape

        fig = plt.figure(1001)

        ax = fig.add_subplot(111)
        cax = ax.matshow(W[np.ix_(valid_inds_shuffled,valid_inds_shuffled)], origin='lower')
        fig.colorbar(cax)

        labels = [str(idx) for idx in valid_inds_shuffled]
        ax.set_xticks(np.arange(nrows))
        ax.set_yticks(np.arange(nrows))

        ax.set_xticklabels(labels)
        ax.set_yticklabels(labels)

        plt.axis('off')
        plt.title('Clustering of trajectories via DBSCAN propagation indicating # of clusters')
        if self.save_dir: plt.savefig('affinity-prop-block-wise-clusters.pdf')

        # print 'Top 10 Cluster labels'
        # print unique(valid_labels)[:10]

    # Viz feature similarity ===============================================
    def viz_similarity(self, data, posepair_dict): 
        # debug_ids = [8,9,10,11]

        viz_pts, viz_normals, viz_colors = [], [], []
        for id_pair,ppd in posepair_dict.iteritems():

            # if not (id_pair[0] in debug_ids or id_pair[1] in debug_ids): continue
            # print id_pair, ppd.distance_match

            # Get Pair and 
            idx1, idx2 = id_pair
            c = plt.cm.jet(ppd.distance_match)
            ut_inds1, = np.where(data.idx[idx1,ppd.overlap_utimes_inds] != -1)
            ut_inds1 = ppd.overlap_utimes_inds[ut_inds1]
            ut_inds2, = np.where(data.idx[idx2,ppd.overlap_utimes_inds] != -1)
            ut_inds2 = ppd.overlap_utimes_inds[ut_inds2]

            viz_pts.append(data.xyz[idx1,ut_inds1[-1],:])
            #viz_pts.append(data.xyz[idx2,ut_inds2[-1],:])
            viz_normals.append(data.xyz[idx2,ut_inds2[-1],:])
            viz_colors.append(c);

        if not len(viz_pts): return

        viz_pts = np.vstack(viz_pts)
        viz_colors = np.vstack(viz_colors)
        viz_normals = np.vstack(viz_normals)

        draw_utils.publish_point_cloud('EDGE_DATA', viz_pts, c=viz_colors, sensor_tf='KINECT')
        draw_utils.publish_line_segments('EDGE_WEIGHTS', viz_pts, viz_normals, 
                                         c=viz_colors, sensor_tf='KINECT')

    def viz_similarity_matrix_chunks(self, Ws, f): 
        for cidx, W_dist in enumerate(Ws): 
            ax = f.add_subplot(3,3,cidx+1)
            ax.set_title('Time chunk: %s' % (str(cidx+1)))
            ax.matshow(W_dist, vmin=0, vmax=1)
            ax.set_axis_off()

    def viz_similarity_matrix(self, W, save_name='similarity-matrix'):
        nrows,ncols = W.shape

        fig = plt.figure(1000)
        fig.clf()

        ax = fig.add_subplot(111)
        cax = ax.matshow(W, vmin=0, vmax=1)
        # fig.colorbar(cax)

        labels = [str(idx) for idx in np.arange(nrows)]
        ax.set_xticks(np.arange(nrows))
        ax.set_yticks(np.arange(nrows))

        ax.set_xticklabels(labels)
        ax.set_yticklabels(labels)

        if self.save_dir: plt.savefig(''.join([self.save_dir,save_name,'.pdf']), format='pdf')

    # Plot the distribution of pair of points ==============================
    def viz_posepair_distribution(self, ppd, ax, distribution='histogram', face='green'): 
        if distribution == 'density': 
            plot_pose_pair_density(ppd, face=face)
            plt.text(.8,.85,'$\mu=%4.3f~(m)$' % (np.mean(ppd.distance_hist)), 
                     ha='center',va='center',transform=ax.transAxes)

        else: 
            plot_pose_pair_variation(ppd, face=face)
            plt.text(.8,.85,'$\mu=%4.3f, ~\sigma=%4.3f~(m)$' % (np.mean(ppd.distance_hist), 
                                                              np.std(ppd.distance_hist)), 
                     ha='center',va='center',transform=ax.transAxes)
        # plt.show()
