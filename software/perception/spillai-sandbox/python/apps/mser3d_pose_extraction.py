'''
Pose Estimation using MSER3D detector/tracker
====================
'''

import time
import numpy as np
import tables as tb
import sys, cv2, collections, os.path, time, cPickle
from optparse import OptionParser

import utils.rigid_transform as rtf
import utils.draw_utils as drawutils
import utils.pose_utils as pose_utils

import utils.io_utils as io_utils
from utils.io_utils import Feature3DTable
from fs_utils import LCMLogReader, MSER3D, GPFT

import fs_apriltags

def write_to_table(sfro, fpts): 
    poses = []
    for fpt in fpts: 
        sfro['utime'] = fpt.utime
        sfro['id'] = fpt.id
        sfro['point'] = fpt.point.reshape(-1)
        sfro['size'] = fpt.keypoint.size
        sfro['angle'] = fpt.keypoint.angle
        sfro['response'] = fpt.keypoint.response
        sfro['xyz'] = fpt.xyz().reshape(-1)
        sfro['normal'] = fpt.normal().reshape(-1)
        sfro['tangent'] = fpt.tangent().reshape(-1)
        pose = rtf.Pose.from_triad(fpt.id, 
                                   fpt.xyz().reshape(-1), 
                                   fpt.normal().reshape(-1), 
                                   fpt.tangent().reshape(-1))
        poses.append(pose); 
        # xyzs.append(fpt.xyz().reshape(-1))
        # normals.append(fpt.normal().reshape(-1) * 0.1 + fpt.xyz().reshape(-1))
        # carr.append(np.array([0.9, 0.1, 0.1, 1.0]))
        sfro.append()
        

    drawutils.publish_pose_list2('MSER3D_POSES', poses)


if __name__ == "__main__": 
    parses = OptionParser()
    parses.add_option("-l", "--log-file", dest="filename", 
                      help="Input log Filename", 
                      metavar="FILENAME")
    parses.add_option("-s", "--scale", dest="scale", 
                      help="Scale",default=1.0,type="float",
                      metavar="SCALE")

    # parses.add_option("-t", "--log-type", dest="logtype", 
    #                   help="Log type (lcm,hdf5)",default="video",
    #                   metavar="LOGTYPE")
    # parses.add_option("-s", "--simulate", dest="simulate", 
    #                   help="Simulate", 
    #                   metavar="S", default=False)
    # parses.add_option("-x", "--rewrite", dest="rewrite", 
    #                   help="Re-write DB", 
    #                   metavar="X", default=False)
    # parses.add_option("-q", "--flush-queue", dest="flush_queue", 
    #                   help="Flush every N frames", 
    #                   metavar="Q", default=25)
    # parses.add_option("-c", "--channel", dest="channel", 
    #                   help="Extract Channel", 
    #                   metavar="C", default='KINECT_FRAME')
    # parses.add_option("-n", "--num-frames", dest="frames", 
    #                   help="Number of Frames", type="int",
    #                   metavar="N", default=None)

    (options, args) = parses.parse_args()

    # Set up LCMReader =================================================    
    if options.filename is None or not os.path.exists(options.filename):
        print 'Invalid Filename: %s' % options.filename
        sys.exit(0)
    reader = LCMLogReader(filename=options.filename, scale=options.scale, sequential_read=False)
    init_time = time.time();

    # Set up output folder =============================================
    output_filename = io_utils.get_output_folder(options.filename, ext='.h5')

    # Open the db  =====================================================
    h5f = tb.openFile(output_filename, mode='a', title='%s' % options.filename)
    
    # if 'tags3d' in h5f.root:
    #     h5f.removeNode(h5f.root, name='tags3d')    

    tables = ['tags3d']
    sftables = dict()
    for table in tables: 
        # Remove table
        if table in h5f.root: h5f.removeNode(h5f.root, name=table)    
        # Create table
        sftables[table] = h5f.createTable(h5f.root, name=table, description=Feature3DTable, 
                               title='%s-data' % (table) )
        print 'Creating new Log table: %s' % (table)

    # Initialize MSER tracker ==========================================
    mser3d = MSER3D()
    gpft3d = GPFT()

    drawutils.publish_sensor_frame('KINECT_FRAME')

    while True: 
        frame = reader.getNextFrame();
        if frame is None: break

        # # Smoothen cloud
        # frame.fastBilateralFilter();

        # # Compute surface normals
        # frame.computeNormals(1., 0.01, 15.0)

        # Main MSER update call
        # fpts = mser3d.update(frame)
        # fpts = gpft3d.detect(frame)
        
        # Extract AprilTags as Feature3D
        fpts = fs_apriltags.extractFeatures(frame)

        # Extract AprilTags and set 

        print 'Detected %i features' % len(fpts)
        # for fpt in fpts: print fpt.utime, fpt.id 

        poses, xyzs, normals, carr = [], [], [], []

        # Write to table 
        tagro = sftables['tags3d'].row
        write_to_table(tagro, fpts)

        # poses = np.vstack(poses)
        # xyzs = np.vstack(xyzs)
        # normals = np.vstack(normals)
        # drawutils.publish_point_cloud('GPFT_pts', xyzs, c=np.vstack(carr))
        # drawutils.publish_line_segments('GPFT', xyzs, normals, c=np.vstack(carr))

        # Only flush every 100 frames
        if reader.frame_num % 100 == 0: 
            print 'Processed %i frames' % reader.frame_num

            # Flush each table
            for table in sftables.values(): table.flush()

    # Flush tables =====================================================
    for table in sftables.values(): table.flush()

    # Index DB =========================================================
    print 'Indexing DB ...'
    for table in sftables.values(): 
        if not table.indexed: 
            st = time.time()    
            table.cols.id.createIndex()
            table.cols.utime.createIndex()    
            print 'DB indexed in %4.3f s' % (time.time() - st)
        else:
            table.cols.id.reIndex()
            table.cols.utime.createIndex()        
            print 'DB already indexed, but reindexing'

    # Build table for utimes, and IDs ==================================
    for table_name, table in sftables.iteritems():
        utimes = sorted(list(set(table.cols.utime[:])))
        utimes_map = dict(zip(utimes, range(len(utimes))))
        print 'Total utimes: %i' % len(utimes)

        ids = sorted(list(set(table.cols.id[:])))
        ids_map = dict(zip(ids, range(len(ids))))
        print 'Total tracks: %i' % len(ids)

        # Replace with new table
        if '%s_utimes_table' % table_name in h5f.root: 
            h5f.removeNode(h5f.root, name='%s_utimes_table' % table_name)
        if '%s_ids_table' % table_name in h5f.root: 
            h5f.removeNode(h5f.root, name='%s_ids_table' % table_name)

        utimestable = h5f.createArray(h5f.root, 
                                      '%s_utimes_table' % table_name, np.array(utimes))
        idstable = h5f.createArray(h5f.root, 
                                   '%s_ids_table' % table_name, np.array(ids))
    
    # Close db        
    print 'Closing db ...'
    print 'Processed %i frames in %4.1fs' % (reader.frame_num, time.time() - init_time)
    h5f.close()
