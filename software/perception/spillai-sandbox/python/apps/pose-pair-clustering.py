# -*- coding: utf-8 -*-
# <nbformat>2</nbformat>

# <markdowncell>

# Library Imports
# 
# 
# 
# Pose Pair distance sigma ~ 1.5 cm
# 
# Pose Pair normal sigma ~ 5 degrees

# <codecell>

#!/usr/bin/env python

% load_ext autoreload

% autoreload 2



import time

import lcm



import numpy as np

import tables as tb



import cv2

import cv2.cv as cv



import matplotlib as mpl

import matplotlib.pylab as plt

import matplotlib.mlab as mlab

import matplotlib.animation as animation

import matplotlib.delaunay as triang





from mpl_toolkits.mplot3d.axes3d import Axes3D



from sklearn import manifold as skmanifold

from sklearn import cluster as skcluster

from sklearn import metrics as skmetrics

from sklearn import utils as skutils



from scipy import cluster as spcluster

from scipy import spatial as spspatial



import fastcluster as fcluster



import utils.db_utils as dbutils

import utils.lcm_utils as lcmutils

import utils.plot_utils as plotutils

import utils.draw_utils as drawutils

import utils.imshow_utils as imutils

import utils.distributions as distributions



import scipy.io as sio

import utils.fs_spectral_embedding as fsspectral



import utils.slic_utils as slic_utils



import slic.slic as slic

import slic.depthslic as depthslic



import seeds.seeds as seeds



import utils.normal_estimation as normal_estimation

import pcl_utils





from collections import namedtuple, defaultdict



plt.jet()



# ===== Parameters ==== 

MIN_TRACK_LENGTH = 10; 

MIN_TRACK_INTERSECTION = 5;

MIN_TRAJECTORY_VAR = 0.05 * 0.05;



# ===== Matplotlib ==== 

pylab.rcParams['figure.figsize'] = 8,6  # that's default image size for this interactive session

mpl.rcParams['figure.figsize'] = [8,6]

# <codecell>

# h5f.close()

# print h5f

# <codecell>

fn = '/home/spillai/data/2013_articulation_test/book_unfolding/lcmlog-2013-03-16.00';



# Open DB

h5f = tb.openFile('%s.h5' % fn, mode='r')

# <codecell>

feature_ids = h5f.root.superfeatures_ids_table.read()

feature_utimes = h5f.root.superfeatures_utimes_table.read()

feature_data = h5f.root.superfeatures_table.read()

print feature_data[:,:,0:2], feature_ids.shape, feature_utimes.shape

# <codecell>

# ===== Get opt args ====

# fn = '/home/spillai/envoy/software/perception/python-perception/posepairclustering/track_data.mat'

# track_data = sio.loadmat(fn)

# feature_ids = track_data['feature_ids'];

# feature_utimes = track_data['feature_utimes']

# feature_data = track_data['feature_data'];



# 1328308047526785

#fn = '/home/spillai/data/2012_02_03_affordance_data/lcmlog-2012-02-03.01'

#fn = '/home/spillai/data/2012_02_03_affordance_data/lcmlog-2012-02-03.02'

#fn = '/home/spillai/data/2012_07_30_door_handle_fiducial/door_handle_far_with_fiducial'

#fn = '/home/spillai/data/2012_07_30_door_handle_fiducial/door_handle_closeup_with_fiducial'

# fn = '/home/spillai/data/2013_articulation_test/book_unfolding/lcmlog-2013-03-16.00';

feats_fn = fn + '-feats.yml'

#fn = '/home/spillai/data/2013_articulation_test/book_unfolding/ground-truth-apr11-3revolute-with-noise3.yml';

# fn = '/home/spillai/data/2013_articulation_test/book_unfolding/ground-truth-apr11-3revolute.yml';

#fn = '/home/spillai/data/2013_articulation_test/book_unfolding/lcmlog-2013-03-16.00-feats.yml';



# ===== Load the data ==== 

# Imagine 3D spatio-temporal data (FEATS x UTIMES x DIMS)

print '===> Loading data: ', fn, '...'

# feature_ids = np.asarray(cv.Load(feats_fn, cv.CreateMemStorage(), 'feature_ids'));

# feature_utimes = np.asarray(cv.Load(feats_fn, cv.CreateMemStorage(), 'feature_utimes'));

# feature_data = np.asarray(cv.Load(feats_fn, cv.CreateMemStorage(), 'feature_data'));



# ===== Temporary: subset ====

#feature_ids = feature_ids[:];

#feature_utimes = feature_utimes[-150:];

#feature_data = feature_data[:, -150:, :];



[num_feats, num_utimes, num_dims] = feature_data.shape;

if not feature_ids.size == num_feats: raise AssertionError

if not feature_utimes.size == num_utimes: raise AssertionError

print 'Num features: ', num_feats

print 'Num utimes: ', num_utimes

print 'Num dims: ', num_dims

# <codecell>

# ===== Pruning ====

# sum over all UTIMES, and check nz for x value, and nz != 1

print '===> Pruning missing features ...'

valid_feat_inds, = np.where(np.sum(feature_data[:,:,0] != 0, axis=1)>MIN_TRACK_LENGTH)

print '--- Done pruning missing features: %i out of %i good' % (len(valid_feat_inds), num_feats)

# <codecell>

print '===> Sort features by length ...'

# Sum (column-wise to count length of track), and zip

feat_size_inds = zip(valid_feat_inds, np.sum(feature_data[:,:,0] != 0, axis=1).tolist());

# Sort by length 

feat_size_inds.sort(key=lambda x : x[1], reverse=True)

# Unzip and keep valid_inds

valid_feat_inds,tmp = zip(*feat_size_inds)

# Keep top 100 inds

#valid_feat_inds = valid_feat_inds[:100];



# for j in valid_feat_inds:

#     valid_feat_inds[j], np.sum(feature_data[j,:,0] != 0, axis=1);

#     d = np.linalg.norm(np.var(feature_data[j,ut_inds,2:5], axis=0)); # np.linalg.norm(np.max(feature_data[j,ut_inds,2:5]) - np.min(feature_data[j,ut_inds,2:5],axis=0));

#     #print np.linalg.norm(np.var(feature_data[j,ut_inds,2:5], axis=0))

#     if d >= MIN_TRAJECTORY_VAR: tmp_valid_feat_inds.append(j);

# print '--- Done pruning short features: %i out of %i good' % (len(tmp_valid_feat_inds), len(valid_feat_inds))

# # valid_feat_inds = tmp_valid_feat_inds;

# <codecell>

# ===== Viz data ====

plt.matshow(feature_data[valid_feat_inds,:,0] != 0, interpolation='nearest', cmap=plt.cm.jet);

plt.title('Tracks');

plt.xlabel('Time');

plt.ylabel('Features');

# <codecell>

# ===== Find overlapping tracks ====

print '===> Extracting overlapping features ...'

overlap_dict = dict(); # {}

for jind in valid_feat_inds:

    # find nz utime indices

    tj, = np.where(np.logical_and(feature_data[jind,:,0] != 0, feature_data[jind,:,4] > 0))

    if jind % 200 == 0: print 'Processing: ', jind



    for kind in valid_feat_inds: 

        if kind <= jind: continue;

           

        # find nz utime indices

        tk, = np.where(np.logical_and(feature_data[kind,:,0] != 0, feature_data[jind,:,4] > 0))



        # set intersection

        intersection = np.intersect1d(tj,tk);

        if intersection.size < MIN_TRACK_INTERSECTION: continue;



        # Test if the features are within 100 L1 ball radius

        nn_inds = np.sum(np.fabs(feature_data[jind,intersection,0:2] - feature_data[kind,intersection,0:2]), axis=1) > 100;

        if not np.all(np.all(nn_inds, axis=0),axis=0): continue;



        overlap_dict[tuple(sorted((jind,kind)))] = intersection;

print '--- Done extracting overlapping features'

# <codecell>

# ===== Compute Pose-Pairs for overlapping tracks ====

print '===> Compute pose-pairs ...'

# Feat idx to mat idx

feat_index = np.array([valid_feat_inds[j] for j in range(0,len(valid_feat_inds))])

feat_ind_map = dict();

[feat_ind_map.update({valid_feat_inds[j]:j}) for j in range(0,len(valid_feat_inds))]



# Inverse map of above

feat_ind_map_rev = dict();

[feat_ind_map_rev.update({v:k}) for k,v in feat_ind_map.iteritems()]



num_valid_feats = len(valid_feat_inds);

assert(num_valid_feats == len(feat_ind_map))



Anormal = np.zeros((num_valid_feats, num_valid_feats));

Adist = np.zeros((num_valid_feats, num_valid_feats));

np.fill_diagonal(Adist, 1)

posepair_dict = dict();

for key,val in overlap_dict.iteritems():

    ppd = distributions.PosePairDistribution(feature_data, key, val);

    posepair_dict[key] = ppd;



    r, c = feat_ind_map[key[0]], feat_ind_map[key[1]]

    

    Anormal[r,c] = ppd.normal_match;

    Anormal[c,r] = ppd.normal_match;

    

    Adist[r,c] = ppd.distance_match;

    Adist[c,r] = ppd.distance_match;

    

print '--- Done computing pose-pairs'



#D = (Anormal + Adist)*0.5;

D = Adist;

print D

# <codecell>

# # ===== Plot the distribution of pair of points ===== 

# # f = plt.figure(1)

# #plt.suptitle('Histogram of pose-pair variation for all valid tracks');

# print 'All valid tracks: ', len(posepair_dict.items())

# distributions.plot_pose_pair_variation(posepair_dict) 



# # f2 = figure(2)

# # plt.suptitle('Filtered tracks');

# # print 'Filtered tracks: ', len(filtered_posepair_dict.items())

# # distributions.plot_pose_pair_variation(filtered_posepair_dict)

# <codecell>

# ===== Draw line segments between feature points that agree with the rigid body motion distribution =====

# edges between points are colored by their distance matches

# <codecell>

# ===== Affinity Propagation  =====

sk_ap = skcluster.AffinityPropagation(damping=0.6, max_iter=500, convergence_iter=15, affinity='precomputed', verbose=True)

sk_labels = sk_ap.fit_predict(D)

sk_labels_full = np.array([-1] * len(feature_ids));

sk_labels_full[feat_index] = sk_labels



sk_lab_array = zip(np.arange(len(sk_labels)),sk_labels)

sk_lab_array.sort(key=lambda x: x[1])

inds,labs = zip(*sk_lab_array)



plt.matshow(D[np.ix_(inds,inds)], origin='lower')

plt.axis('off')

#plt.savefig('affinity-prop-block-wise-clusters.png')

print 'Clustering of trajectories via Affinity propagation indicating # of  clusters'

print unique(sk_labels)

# <codecell>

# ===== Draw segmented features =====

label_points_colors = []

num_labels = len(unique(sk_labels))

# Count number of instances of labels

un_labels,un_inv = np.unique(sk_labels, return_inverse=True)

label_count = np.bincount(un_inv)

label_votes = zip(un_labels, label_count)

label_votes.sort(key=lambda x: x[1], reverse=True)



pts_data, normals_data, colors_edge_data, colors_data = [], [], [], []

for label,votes in label_votes:

    feat_inds, = np.where(sk_labels_full == label)

    #print label, votes

    for idx in feat_inds: 

        ut_inds, = np.where(feature_data[idx,:,0] != 0);



        c = plt.cm.gist_rainbow(label*1.0/num_labels)

        label_points_colors.append(c)



        pts_data.append(feature_data[idx,ut_inds,2:5])

        normals_data.append(feature_data[idx,ut_inds,2:5] + feature_data[idx,ut_inds,5:8]*0.04)

        colors_data.append(np.tile(c, (len(ut_inds), 1)));



        c = plt.cm.gist_rainbow(0.8)

        colors_edge_data.append(np.tile(c, (len(ut_inds), 1)));

    

pts_data = np.vstack(pts_data)

colors_data = np.vstack(colors_data)

normals_data = np.vstack(normals_data)

colors_edge_data = np.vstack(colors_edge_data)

label_points_colors = np.vstack(label_points_colors)



#f = figure()

#ax = f.add_subplot(1, 1, 1)

#ax.scatter(all_points_data[:,0], all_points_data[:,1],c=all_points_colors)

#ax.set_xlim([0,3.2])

#ax.set_ylim([0,2])

#ax.set_xlabel('X')

#ax.set_ylabel('Y')

#plt.savefig('trajectory-xy-axis-clustered.png')

#ax.axis('off')

drawutils.publish_sensor_frame('KINECT_FRAME')

drawutils.publish_point_cloud('SORTED_DATA', pts_data, c=colors_data);

drawutils.publish_line_segments('SORTED_LINE', pts_data, normals_data, c=colors_edge_data)

# <codecell>

# # ===== Embedding =====

# pts_data, normals_data, colors_edge_data, colors_data = [], [], [], []

# for label,votes in label_votes:

#     feat_inds, = np.where(sk_labels_full == label)

#     #print label, votes

#     for idx in feat_inds: 

#         ut_inds, = np.where(feature_data[idx,:,0] != 0);

#         #print feature_data[idx,ut_inds,2:5]

#         se = skmanifold.SpectralEmbedding(n_components=3, affinity='rbf', gamma=0.1, eigen_solver='arpack')

#         #print feature_data[idx,ut_inds,2:5]

#         Y = se.fit_transform(feature_data[idx,ut_inds,2:5])



#         pts_data.append(Y)

#     pts_data = np.vstack(pts_data)



#     # fig = figure()

#     # ax = fig.add_subplot(1, 1, 1, projection='3d')

#     # ax.scatter(pts_data[:,0], pts_data[:,1], pts_data[:,2], s=30)

#     break

# <codecell>

# ===== Viz Pose-Pairs adjacency matrix ====

print """Plot visualizing the difference in euclidean distance matching"""

plt.jet()

plt.matshow(D, origin='lower');

plt.title('Difference of Euclidean distance matching');

plt.xlabel('Features')

plt.ylabel('Features')

plt.colorbar();

# <codecell>

# ===== Hierarchical clustering tree  ====

D_ = D;

linkageD = fcluster.linkage(D_, method='average')

# ===== Visualize hierarchical clustering tree  ====                                                

plotutils.visualize_linkage(D_, linkageD)

#plt.savefig('average-hierarhical-clustering-example.png')

# <codecell>

# # ===== Multi-dimensional scaling on affinity matrix  =====

# skmds = skmanifold.MDS(n_components=3, metric=True, verbose=1);

# skmds_embedding = skmds.fit_transform(D);

# skmdsmin = np.min(np.min(skmds_embedding, axis=0))

# skmdsmax = np.max(np.max(skmds_embedding, axis=0))

# skmds_embedding = (skmds_embedding - skmdsmin)/(skmdsmax-skmdsmin);

# fig = figure()

# ax = fig.add_subplot(1, 1, 1, projection='3d')

# ax.scatter(skmds_embedding[:,0], skmds_embedding[:,1], skmds_embedding[:,2], s=40, c=label_points_colors);

# ax.set_xlabel('X')

# ax.set_ylabel('Y')

# ax.set_zlabel('Z')

# <codecell>

# ===== Post-processed video  =====

reader = lcmutils.KinectLogReader(fn, channel='KINECT_FRAME',extract_depth=False,extract_X=False)

# reader = dbutils.KinectDBReader(db=h5f)

# <codecell>

# img, depth, X = reader.get_frame(1328308308910648)



# f = figure()

# plotutils.imshow(img)

# plt.savefig('mug-pointing-1328308308910648-img.png')



# f2 = figure()

# plt.gray()

# plotutils.imshow(depth)

# plt.savefig('mug-pointing-1328308308910648-depth.png')





# f3 = figure()

# meand = pcl_utils.integral_normal_estimation(np.ascontiguousarray(X, dtype=np.float32))

# meand[np.where(depth <= 0)] = -1



# plotutils.imshow(0.5*(meand+1))

# plt.set_cmap('gist_rainbow')

# plt.savefig('mug-pointing-1328308308910648-normals.png')

# <codecell>

#seeds_labels = seeds.seeds_s(img,300,10)

# <codecell>

# ==== Create Video ====

writer = cv2.VideoWriter(fn+'-track.avi', cv2.VideoWriter_fourcc('m','p','4','2'), 15.0, (640, 480), True)



# For each timestamp

frame = 0

num_labels = len(unique(sk_labels))

all_pts = feature_data[valid_feat_inds,:,0] != 0;

for utime_idx in range(len(feature_utimes)):

    sensor_utime = int(feature_utimes[utime_idx]);



    # print 'Sensor utime', sensor_utime

    data = reader.get_frame(sensor_utime)

    if data is None: break

    img = data.rgb



    # NaN mask for depth images

    #depth_mask = np.where(depth <= 0, 0, 255).astype(np.uint8)



    # Depth with NaN mask

    #depth = np.where(depth <=0, 0, depth)

    

    # Img with NaN mask

    #img_with_depth_mask = np.empty_like(img)

    #depth_mask = depth_mask[:,:,newaxis]

    #img_with_depth_mask = np.bitwise_and(img, np.tile(depth_mask,(1,1,3)));



    

    # # Compute gradience

    # gradx = np.abs(cv2.Sobel(depth, cv2.CV_32F, 1, 0, ksize=1))

    # grady = np.abs(cv2.Sobel(depth, cv2.CV_32F, 0, 1, ksize=1))

    # # Estimate depth error bars, and cap ~ 5cm

    # depth_err = (depth - 0.5) * (0.070-0.002) / 4.0;

    # depth_err = np.maximum(depth_err, np.ones_like(depth_err) * 0.010)

    # # Compute edge confidence map (binary)

    # depth_with_edges = np.where(np.bitwise_or(gradx >= depth_err,grady >= depth_err), 0, depth)

    # grad = np.where(np.bitwise_or(gradx >= depth_err,grady >= depth_err), 255, 0)

    

    # # Mean depth

    # # meand = normal_estimation.normal_estimation(orgX, 8)

    # st = time.time()

    # meand = pcl_utils.integral_normal_estimation(np.ascontiguousarray(orgX, dtype=np.float32))

    # print 'Normal computation: ', time.time() - st

    

    # # Build depth superpixels

    # dslic_labels = depthslic.slic_s(depth_with_edges * 1000, superpixel_size=500, compactness=10)

    # contours = depthslic.contours(img_with_depth_mask, dslic_labels, 10)



    # # Slic normals    

    # slic_normals, normal_label_map = slic_utils.slic_segment(meand, dslic_labels, depth>0)



    for idx in valid_feat_inds:

        utime_inds, = np.where(feature_data[idx,:utime_idx,0] != 0);

        utime_inds = utime_inds[-20:];

        pts = feature_data[idx,utime_inds,0:2];



        label = sk_labels_full[idx]

        c = plt.cm.jet(label*1.0/num_labels)

        for j in range(pts.shape[0]-1):

            #c = plt.cm.jet(1-j*1.0/pts.shape[0])

            cv2.line(img, (pts[j,0], pts[j,1]),

                        (pts[j+1,0], pts[j+1,1]), 

                        (c[0]*255,c[1]*255,c[2]*255), 1, cv2.LINE_AA, 0)

            #if j == pts.shape[0]-2:

            #    cv2.putText(img, '%s'%idx, (pts[j,0], pts[j,1]), 0, 0.35, (c[0]*255,c[1]*255,c[2]*255),

            #            thickness=1, lineType=cv.CV_AA)        





    # =======================================================

    # Per-cluster Feature labeling

    # and for each label

    # =======================================================

    for label,votes in label_votes:

        feat_inds, = np.where(sk_labels_full == label)

        if not len(feat_inds): continue

        if votes < 10: continue

        pts = []

        c = plt.cm.jet(label*1.0/num_labels)

        

        for idx in feat_inds:

            pt = feature_data[idx,utime_idx,0:2]

            nm = feature_data[idx,utime_idx,5:8] + 1

            #cv2.circle(img, (pt[0], pt[1]), 6, (nm[0]*128,nm[1]*128,nm[2]*128), 3);

            cv2.circle(img, (pt[0], pt[1]), 1, (c[0]*255,c[1]*255,c[2]*255), -1);

            cv2.putText(img, '%s'%idx, (pt[0], pt[1]), 0, 0.35, (c[0]*255,c[1]*255,c[2]*255),

                        thickness=1, lineType=cv2.LINE_AA)        

            pts.append(pt)

        pts = np.vstack(pts)



        # kdtree = spspatial.KDTree(pts)

        # for pt in pts:

        #     nn_dists,nn_inds = kdtree.query(pt,k=5,distance_upper_bound=100)

        #     for nn in nn_inds:

        #         pt2 = pts[nn,:]

        #         cv2.line(img, (pt[0], pt[1]), (pt2[0], pt2[1]), 

        #                 (c[0]*255,c[1]*255,c[2]*255), 1, cv.CV_AA, 0)



        # c = plt.cm.gist_rainbow(label*1.0/num_labels)



        cens,edg,tri,neig = triang.delaunay(pts[:,0].T,pts[:,1].T)

        for e in edg:

           if np.sum(np.fabs(pts[e[0]] - pts[e[1]])) > 100: continue

           cv2.line(img, (pts[e[0],0], pts[e[0],1]),

                       (pts[e[1],0], pts[e[1],1]), 

                       (c[0]*255,c[1]*255,c[2]*255), 1, cv2.LINE_AA, 0)



    #if frame > 200: break;

    if frame % 20 == 0:

        print 'Processing Frame: ', frame

    frame += 1

    writer.write(cv2.cvtColor(img, cv2.COLOR_RGB2BGR))





# plotutils.imshow(img)



writer.release()

print 'Done'

# <codecell>


# <codecell>

    # img_pts_data, img_pts_label = [],[]

    # plotted = False

    # for idx in valid_img_feat_inds:

    #     label = sk_labels_full[idx]

    #     if (label < 0): continue

    #     plotted = True

    #     pt = feature_data[idx,utime_idx,0:2];

    #     print pt

    #     c = plt.cm.gist_rainbow(label*1.0/num_labels)

    #     cv.circle(img, (pt[0], pt[1]), 3, (c[0],c[1],c[2]), 1);



    # if plotted:

    #     plotutils.imshow(img)



    #break



    #if frame > 10:break

# # ===== Spectral Embedding  =====

# st = time.time()

# sp_embedding = skmanifold.SpectralEmbedding(n_components=6, affinity='precomputed', eigen_solver='arpack')

# s_embed =  sp_embedding.fit_transform(Ysq+1e-3)

# print 'Spectral Embedding Elapsed time: %s s' % (time.time() - st)

# plt.gray()

# plt.matshow(np.abs(s_embed)); 



# Y = spspatial.distance.pdist(skmds_embedding, 'euclidean');

# Ysq = spspatial.distance.squareform(Y);

# 

# linkageY = fcluster.linkage(Ysq, method='complete')

# plotutils.visualize_linkage(Ysq, linkageY)



# Pair wise segments

# for k,v in posepair_dict.iteritems():

#     c = plt.cm.jet(v.distance_match)

#     ut_inds, = np.where(feature_data[k[0],:,0] != 0);

#     p1.append(feature_data[k[0],ut_inds,2:5])

#     p2.append(feature_data[k[0],ut_inds,2:5] + feature_data[k[0],ut_inds,5:8]*0.04)

#     carr.append(np.tile(c, (len(ut_inds), 1)));

# p1 = np.vstack(p1)

# p2 = np.vstack(p2)

# print p1.shape, p2.shape

# carr = np.vstack(carr)

# drawutils.publish_line_segments('SORTED_LINE', p1, p2, c=carr)

