# -*- coding: utf-8 -*-
# <nbformat>2</nbformat>

# <codecell>

#!/usr/bin/env python

% load_ext autoreload

% autoreload 2



import time

import lcm

import itertools



import networkx as nx

import numpy as np

import tables as tb



import cv2

import cv2.cv as cv



import matplotlib as mpl

import matplotlib.pylab as plt

import matplotlib.mlab as mlab

import matplotlib.animation as animation

import matplotlib.delaunay as triang





from mpl_toolkits.mplot3d.axes3d import Axes3D



from sklearn import decomposition as skdecomposition

from sklearn import manifold as skmanifold

from sklearn import cluster as skcluster

from sklearn import metrics as skmetrics

from sklearn import utils as skutils



from scipy import cluster as spcluster

from scipy import spatial as spspatial

from scipy import sparse as spsparse



import fastcluster as fcluster



import utils.db_utils as dbutils

import utils.lcm_utils as lcmutils

import utils.plot_utils as plotutils

import utils.draw_utils as drawutils

import utils.imshow_utils as imutils

import utils.distributions as distributions

import utils.pose_pair_estimtation as ppe

from utils.compare_manifold_methods import compare_manifold_learning_methods



import scipy.io as sio

import utils.fs_spectral_embedding as fsspectral



import utils.slic_utils as slic_utils



import slic.slic as slic

import slic.depthslic as depthslic



import seeds.seeds as seeds



import utils.normal_estimation as normal_estimation

import pcl_utils





from collections import namedtuple, defaultdict



# ===== Parameters ==== 

SAMPLE_T_TIMEFRAMES = 200; 

MIN_TRACK_INTERSECTION = SAMPLE_T_TIMEFRAMES * 0.20;



# ===== Matplotlib ==== 

pylab.rcParams['figure.figsize'] = 8,6  # that's default image size for this interactive session

mpl.rcParams['figure.figsize'] = [8,6]

# <codecell>

# Open DB

fn = '/home/spillai/data/2013_articulation_test/book_unfolding/lcmlog-2013-03-16.00';

h5f = tb.openFile('%s.h5' % fn, mode='r')

# <codecell>

print '===> Loading data ...'

feature_ids =  h5f.root.superfeatures_ids_table.read().astype(np.int64);

feature_utimes =  h5f.root.superfeatures_utimes_table.read().astype(np.int64);

feature_data =  h5f.root.superfeatures_table.read();



print '===> Prune features that dont have good features ...'

feature_utime_inds_map = dict()



feature_ids_inds = []

for ind in range(len(feature_ids)): 

    valid_xy = feature_data[ind,:,0] != 0;

    valid_depth = feature_data[ind,:,4] >= 0.1;

    valid_dot = np.dot(feature_data[ind,:,5:8], np.array([0,0,-1])) >= math.cos(60*np.pi/180);

    valid = np.logical_and(np.logical_and(valid_xy, valid_depth), valid_dot)

    if np.count_nonzero(valid) < MIN_TRACK_INTERSECTION: continue

    valid_utimes_inds, = np.where(valid)



    feature_ids_inds.append(ind)

    feature_id = feature_ids[ind]

    feature_utime_inds_map[feature_id] = valid_utimes_inds



feature_ids_inds = np.array(feature_ids_inds, dtype=np.int)



print '===> Remapping feature_ids, and feature_data ...'

feature_ids = feature_ids[feature_ids_inds]

feature_data = feature_data[feature_ids_inds]

print feature_ids.shape, feature_data.shape

print 'Features: %i | Removed: %i | Before: %i' % \

    (len(feature_ids_inds), len(feature_ids)-len(feature_ids_inds), len(feature_ids))



print '===> Create id <=> idx map ...'

feature_ids_inds, = np.where(feature_ids)

id2idx_map = dict(zip(feature_ids,feature_ids_inds))

idx2id_map = dict(zip(feature_ids_inds,feature_ids))

# <codecell>

print '====> Publish good features ...'

pts,npts,cdata,cndata = [], [], [], []

for fid,feat_data in zip(feature_ids,feature_data): 

    ut_inds = feature_utime_inds_map[fid]

    pts.append(feat_data[ut_inds,2:5])

    npts.append(feat_data[ut_inds,2:5] + feat_data[ut_inds,5:8]*0.02)

    c = plt.cm.jet(id2idx_map[fid]*1.0/len(feature_ids))

    n = plt.cm.jet(0.2)

    cdata.append(np.tile(c[:3], (len(ut_inds), 1)));

    cndata.append(np.tile(n[:3], (len(ut_inds), 1)));

pts,npts,cdata,cndata = np.vstack(pts), np.vstack(npts), np.vstack(cdata), np.vstack(cndata)



drawutils.publish_sensor_frame('KINECT_FRAME')

drawutils.publish_point_cloud('FILTERED_DATA', pts, c=cdata);

drawutils.publish_line_segments('FILTERED_NORMALS', pts, npts, c=cndata)

# <codecell>

print '===> Find overlapping utimes ...'

delaunay_edges = dict()

for fidj,ut_indsj in feature_utime_inds_map.items(): 

    for fidk,ut_indsk in feature_utime_inds_map.items():

        if fidj >= fidk: continue

        utime_int = np.intersect1d(ut_indsj,ut_indsk)

        if len(utime_int) >= MIN_TRACK_INTERSECTION: delaunay_edges[(fidj,fidk)] = utime_int

print 'Delaunay edges: %i overlapping features' % len(delaunay_edges)

# <codecell>

# ===== Viz data ====

plt.matshow(feature_data[:,:,0] != 0, interpolation='nearest', cmap=plt.cm.jet);

plt.title('Tracks');

plt.xlabel('Time');

plt.ylabel('Features');

# <codecell>

print '===> Constructing affinity matrix ...'

Adist = np.zeros((len(feature_ids), len(feature_ids)))

np.fill_diagonal(Adist, 1)



Anormal = np.zeros((len(feature_ids), len(feature_ids)))

np.fill_diagonal(Anormal, 1)



Adistsp = spsparse.lil_matrix(Adist.shape)

Anormalsp = spsparse.lil_matrix(Anormal.shape)



ppd_map = dict()

count = 0; 

for key,ut_inds in delaunay_edges.iteritems(): 

    jid,kid = key;

    jidx,kidx = id2idx_map[jid],id2idx_map[kid]

    jfeat, kfeat = feature_data[jidx,ut_inds,:], feature_data[kidx,ut_inds,:]



    ppd = distributions.PosePairDistribution(jfeat,kfeat)

    ppd_map[key] = ppd;



    # print jidx, kidx, ppd.distance_match



    Adist[jidx,kidx] = ppd.distance_match

    Adist[kidx,jidx] = ppd.distance_match



    Adistsp[jidx,kidx] = 1-ppd.distance_match

    Adistsp[kidx,jidx] = 1-ppd.distance_match



    Anormal[jidx,kidx] = ppd.normal_match

    Anormal[kidx,jidx] = ppd.normal_match



    Anormalsp[jidx,kidx] = 1-ppd.normal_match

    Anormalsp[kidx,jidx] = 1-ppd.normal_match



    # if ppd.distance_match < 0.5 and count <= 6: 

    #     figure()

    #     print jid, kid

    #     distributions.plot_pose_pair_variation(ppd)

    #     count += 1





print '===> Viz Pose-Pairs adjacency matrix ...'

plotutils.matshow(Anormal)

plt.title('Difference of Euclidean distance matching');



AdistKNN = spsparse.csgraph.dijkstra(Adistsp, directed=False)

inf_inds = np.where(AdistKNN==np.inf)

AdistKNN[inf_inds] = 1



AnormalKNN = spsparse.csgraph.dijkstra(Anormalsp, directed=False)

inf_inds = np.where(AnormalKNN==np.inf)

AnormalKNN[inf_inds] = 1



plotutils.matshow(Adist)

# <codecell>

print '===> Affinity Propagation ...'

st = time.time()

sk_ap = skcluster.AffinityPropagation(damping=0.9, max_iter=500, 

                                      convergence_iter=50, affinity='precomputed', verbose=True)

sk_labels = sk_ap.fit_predict(Anormal)



print 'Clustering of trajectories via Affinity propagation indicating # of  clusters'

print unique(sk_labels)



print '===> Count number of instances of labels ...'

label_votes = plotutils.get_label_votes(sk_labels)[:4]

top_labels,top_votes = zip(*label_votes)

Anormal_reduced = plotutils.matshow_by_top_labels(Anormal, sk_labels, top_labels)

print label_votes, Anormal_reduced



# print '===> Hierarchical clustering tree ...\n'

# linkageD = fcluster.linkage(Anormal, method='complete')

# plotutils.visualize_linkage(Anormal, linkageD)

# #plt.savefig('average-hierarhical-clustering-example.png')



print 'Affinity Propagation: %3.2f s'% (time.time() - st)

# <codecell>

print '===> Pose Pair estimation with top voted segments ...'

# Flip representation of delaunay_edges

# map utime -> list of pairs

posepairs_by_utime = defaultdict(list)

for pair, utime_inds in delaunay_edges.iteritems(): 

    for utime_ind in utime_inds: 

        posepairs_by_utime[utime_ind].append(pair)



# # Pose Pairs per utime

# posepairs_by_utime = [(utime_ind, pair_list) for utime_ind,pair_list in delaunay_edges_by_utime.iteritems()]

# # Sort by utime sequence

# posepairs_by_utime.sort(key=lambda x: x[0]);



# Sort by length                  

# pose_pair_by_utime.sort(key=lambda x: len(x[1]), reverse=True);

# <codecell>

poses = []

for label,votes in label_votes: 

    print label, votes



    # Find inds that correspond to the label

    inds, = np.where(sk_labels == label)

    ids = feature_ids[inds]



    pose_manager = ppe.PoseManager(ids=feature_ids, utimes=feature_utimes)

    # FWD Pass (Filtering)

    # utime_inds = posepairs_by_utime.keys();

    # for ut1, ut2 in zip(utime_inds[:-1],utime_inds[1:]): 

    #     print ut1, ut2



    # Add observations

    count = 0

    for utime_ind,pair_list in posepairs_by_utime.iteritems():



        for pair in pair_list: 

            id1,id2 = pair

            if id1 not in pose_manager.id2s: continue

            # if id2 not in pose_manager.id2s: continue

            ind1,ind2 = id2idx_map[id1], id2idx_map[id2]

            if sk_labels[ind1] != sk_labels[ind2]: continue

            # print id1, id2, utime_ind

            pose_manager.add_observation(utime_ind,

                                         id1, feature_data[ind1,utime_ind,2:5], feature_data[ind1,utime_ind,5:8], 

                                         id2, feature_data[ind2,utime_ind,2:5], feature_data[ind2,utime_ind,5:8])

        count += 1 

        if count > 200: break

            # break

         

    print '\n ====> Optimize: \n'

    pose_manager.optimize()



    print '===> Publishing: \n'

    for fid,pose_utimes in pose_manager.final_pose_map.iteritems(): 

        for utime,pose in pose_utimes.iteritems(): 

            poses.append(pose)

        

    print '===> Publishing: \n'

    for fid,pose_utimes in pose_manager.final_pose_map.iteritems(): 

        for utime,pose in pose_utimes.iteritems(): 

            poses.append(pose)





    # # Find

    # for utime_ind, pair_list in posepairs_by_utime.iteritems(): 

    #     print 'Update', utime_ind

    #     pose_manager.update(utime_ind)

    #     if pose_manager.id1 in pose_manager.final_pose_map[utime_ind]: 

    #         poses.append(pose_manager.final_pose_map[utime_ind][261])





    #     # # if len(p) < 100: continue;

    # posemap = pose_manager.estimate(261)

    # for pose_list in posemap.values():

    #     poses.append(pose_list)



        # print feature_data[ind1,216,2:8]

        # print utime_inds

        # poses.append(pose_manager.Oc[id1])

    # pose_manager.estimate(261)        



    print 'No. of edges: ', pose_manager.pp_graph.number_of_edges()

    # nx.write_dot(pose_manager.pp_graph, "graph-261.dot")

    # plt.show()



    break





poses = np.vstack(poses)

drawutils.publish_pose_list('POSE_PAIR_ESTIMATE', poses, sensor_tf=True)

# <codecell>

print '===> Manifold learning'



X = poses[:,:3]

color = np.arange(len(X))/(1.0*len(X))



fig = figure()

ax = fig.add_subplot(1, 1, 1, projection='3d')

ax.scatter(X[:,0], X[:,1], X[:,2], c=color, cmap=plt.cm.jet)



compare_manifold_learning_methods(X, color)





# plt.xaxis.set_major_formatter(NullFormatter())

# plt.yaxis.set_major_formatter(NullFormatter())

# <codecell>





print '===> Sparse PCA on poses'

sppca = skdecomposition.SparsePCA(n_components=2, alpha=0.01, ridge_alpha=0.01, max_iter=1000, tol=1e-08, method='lars', n_jobs=1, U_init=None, V_init=None, verbose=True, random_state=None)

sppca.fit(poses[:,:3])



print 'Error', sppca.error_, 'Comps: ', sppca.components_

sppca_poses = sppca.fit_transform(poses[:,:3])

# print sppca_poses



# <codecell>

print '===> Draw segmented features ...'

colors = np.array([x for x in 'bgrcmykbgrcmykbgrcmykbgrcmyk'])

colors = np.hstack([colors] * 20)



pts, npts, cedata, cdata = [], [], [], []

fpts, spts, cldata = [], [], []



# For top label votes

print '===> Draw top voted segments ...\n'

for label,votes in label_votes:



    # Find inds that correspond to the label

    feat_inds, = np.where(sk_labels == label)



    # c = mpl.colors.ColorConverter.colors[colors[label]]     

    c = plt.cm.jet(label*1.0/len(label_votes))

    

    for idx in feat_inds: 

        fid = feature_ids[idx]        

        # if fid != 261: continue

        ut_inds = feature_utime_inds_map[fid]



        pts.append(feature_data[idx,ut_inds,2:5])

        npts.append(feature_data[idx,ut_inds,2:5]+feature_data[idx,ut_inds,5:8]*0.02)



        fpts.append(feature_data[idx,ut_inds[:-1],2:5])

        spts.append(feature_data[idx,ut_inds[1:],2:5])



        cdata.append(np.tile(c, (len(ut_inds), 1)));

        cldata.append(np.tile(c, (len(ut_inds)-1, 1)));



        ce = plt.cm.jet(0.8)

        cedata.append(np.tile(ce, (len(ut_inds), 1)));       



pts = np.vstack(pts)

npts = np.vstack(npts)



fpts = np.vstack(fpts)

spts = np.vstack(spts)



cdata = np.vstack(cdata)

cedata = np.vstack(cedata)

cldata = np.vstack(cldata)



drawutils.publish_sensor_frame('KINECT_FRAME')

drawutils.publish_point_cloud('CLUSTERED_DATA', pts, c=cdata);

drawutils.publish_line_segments('CLUSTERED_NORMALS', pts, npts, c=cedata)

drawutils.publish_line_segments('CLUSTERED_LINE', fpts, spts, c=cldata)

# <codecell>

print '===> Draw edges between tracks ...'

fpts, spts, cdata = [], [], []

for pair,utime_inds in delaunay_edges.items(): 

    id1, id2 = pair

    ind1, ind2 = id2idx_map[id1], id2idx_map[id2]



    # Only draw edges between same labels 

    if not sk_labels[ind1] == sk_labels[ind2]: continue



    fpts.append(feature_data[ind1, utime_inds[-1], 2:5])

    spts.append(feature_data[ind2, utime_inds[-1], 2:5])



    c = plt.cm.jet(Anormal[ind1,ind2])[:3]

    cdata.append(c);



fpts = np.vstack(fpts)

spts = np.vstack(spts)



cdata = np.vstack(cdata)

drawutils.publish_line_segments('DELAUNAY', fpts, spts, c=cdata)

# <codecell>

print '===> MDS ...'

n_comps=3

mds = skmanifold.MDS(n_components=n_comps, metric=True, n_init=6, max_iter=100, verbose=1, dissimilarity='precomputed')

embed = mds.fit_transform(Anormal)

c = plt.cm.jet(sk_labels*1.0/len(label_votes))



if n_comps == 2: 

    plt.scatter(embed[:,0], embed[:,1], c=c[:,:3], s=40)

else: 

    fig = figure()

    ax = fig.add_subplot(1, 1, 1, projection='3d')

    ax.scatter(embed[:,0], embed[:,1], embed[:,2], s=30, c=c[:,:3])



# print '===> Draw segmented features ...\n'

# plotutils.matshow_bylabels(Anormal, linkageD[:,3])

# sk_labels = linkageD[:,3]

# <codecell>

# ===== Post-processed video  =====

reader = lcmutils.KinectLogReader(fn, channel='KINECT_FRAME',extract_depth=False,extract_X=False)

# reader = db_utils.KinectDBReader(db=h5f)

# <codecell>

# ==== Create Video ====

writer = cv2.VideoWriter(fn+'-track.avi', cv2.VideoWriter_fourcc('m','p','4','2'), 15.0, (640, 480), True)



# For each timestamp

frame = 0

num_labels = len(unique(sk_labels))

#all_pts = feature_data[valid_feat_inds,:,0] != 0;

for utime_idx in range(len(feature_utimes)):

    sensor_utime = int(feature_utimes[utime_idx]);



    # print 'Sensor utime', sensor_utime

    data = reader.get_frame(sensor_utime)

    if data is None: break

    img = data.rgb



    # # =======================================================

    # # Per-cluster delaunay trigln

    # # =======================================================

    # for pair,utime_inds in delaunay_edges.items(): 

    #     id1, id2 = pair

    #     ind1, ind2 = id2idx_map[id1], id2idx_map[id2]



    #     # Same label edge

    #     if not sk_labels[ind1] == sk_labels[ind2]: continue

        

    #     # Label must be top voted

    #     if not sk_labels[ind1] in top_labels: continue

    #     c = plt.cm.jet(sk_labels[ind1]*1.0/len(label_votes)) # Color by label

    #     # c = plt.cm.jet(Anormal[ind1,ind2]) # Color by edge weight



    #     pts1 = feature_data[ind1, utime_inds[-1], 0:2].astype(int)

    #     pts2 = feature_data[ind2, utime_inds[-1], 0:2].astype(int)



    #     cv2.line(img, (pts1[0],pts1[1]), (pts2[0],pts2[1]), 

    #              (c[0]*255,c[1]*255,c[2]*255), 1, cv.CV_AA, 0)

    

    # =======================================================

    # Per-cluster Feature labeling

    # and for each label

    # =======================================================

    # For top label votes

    for label,votes in label_votes:



        # Find inds that correspond to the label

        feat_inds, = np.where(sk_labels == label)



        c = plt.cm.jet(label*1.0/len(label_votes)) # Color by label

        # c = mpl.colors.ColorConverter.colors[colors[label]]     



        for idx in feat_inds:

            fid = feature_ids[idx]

            # ut_inds = feature_utime_inds_map[fid]

            pt = feature_data[idx,utime_idx,0:2].astype(int)

            cv2.circle(img, (pt[0], pt[1]), 3, (c[0]*255,c[1]*255,c[2]*255), -1, 
                       lineType=cv2.LINE_AA);



            # nm = feature_data[idx,ut_inds,5:8] + 1

            #cv2.circle(img, (pt[0], pt[1]), 6, (nm[0]*128,nm[1]*128,nm[2]*128), 3);

            # cv2.putText(img, '%s'%feat_id, (pt[0], pt[1]), 0, 0.35, (c[0]*255,c[1]*255,c[2]*255),

            #             thickness=1, lineType=cv.CV_AA)        



    #if frame > 200: break;

    if frame % 20 == 0:

        print 'Processing Frame: ', frame

    frame += 1

    writer.write(cv2.cvtColor(img, cv2.COLOR_RGB2BGR))





# plotutils.imshow(img)



writer.release()

print 'Done'

# <codecell>


# <codecell>

    # img_pts_data, img_pts_label = [],[]

    # plotted = False

    # for idx in valid_img_feat_inds:

    #     label = sk_labels_full[idx]

    #     if (label < 0): continue

    #     plotted = True

    #     pt = feature_data[idx,utime_idx,0:2];

    #     print pt

    #     c = plt.cm.gist_rainbow(label*1.0/num_labels)

    #     cv.circle(img, (pt[0], pt[1]), 3, (c[0],c[1],c[2]), 1);



    # if plotted:

    #     plotutils.imshow(img)



    #break



    #if frame > 10:break

# # ===== Spectral Embedding  =====

# st = time.time()

# sp_embedding = skmanifold.SpectralEmbedding(n_components=6, affinity='precomputed', eigen_solver='arpack')

# s_embed =  sp_embedding.fit_transform(Ysq+1e-3)

# print 'Spectral Embedding Elapsed time: %s s' % (time.time() - st)

# plt.gray()

# plt.matshow(np.abs(s_embed)); 



# Y = spspatial.distance.pdist(skmds_embedding, 'euclidean');

# Ysq = spspatial.distance.squareform(Y);

# 

# linkageY = fcluster.linkage(Ysq, method='complete')

# plotutils.visualize_linkage(Ysq, linkageY)



# Pair wise segments

# for k,v in posepair_dict.iteritems():

#     c = plt.cm.jet(v.distance_match)

#     ut_inds, = np.where(feature_data[k[0],:,0] != 0);

#     p1.append(feature_data[k[0],ut_inds,2:5])

#     p2.append(feature_data[k[0],ut_inds,2:5] + feature_data[k[0],ut_inds,5:8]*0.04)

#     carr.append(np.tile(c, (len(ut_inds), 1)));

# p1 = np.vstack(p1)

# p2 = np.vstack(p2)

# print p1.shape, p2.shape

# carr = np.vstack(carr)

# drawutils.publish_line_segments('SORTED_LINE', p1, p2, c=carr)

