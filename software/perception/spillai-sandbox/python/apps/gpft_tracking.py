'''
============================================================
Feature Trajectory Error Analysis using GPFT
============================================================
'''

# Main Idea: Track features over time, use april tags as ground truth Single
# ARtag, and only a single maximal cluster (similar to VO) Compute Homography
# from ground truth, and determine error between actual (x,y) and expected (x,y)
# python gpft_tracking.py -l ~/data/2013-09-03-artags-articulation/lcmlog-2013-09-03.01

import time
import numpy as np
import sys, cv2, collections, os.path, time
from optparse import OptionParser
from collections import defaultdict, namedtuple
import itertools

import utils.draw_utils as draw_utils
import utils.io_utils as io_utils

from utils.db_utils import AttrDict, DictDB
from utils.io_utils import Feature3DTable, Pose3DTable, Feature3DWriter, Pose3DWriter
from utils.tracker_utils import TrackerInfo, AprilTagsFeatureTracker, DenseTrajectoriesTracker
from fs_utils import LCMLogReader, publish_cloud

if __name__ == "__main__": 
    parser = OptionParser()
    parser.add_option("-l", "--log-file", dest="filename", 
                      help="Input log Filename", 
                      metavar="FILENAME")
    parser.add_option("-i", "--interval", dest="interval", 
                      help="Eval every i frames", default=1, type="int")
    parser.add_option("-s", "--scale", dest="scale", 
                      help="Scale",default=1.0,type="float",
                      metavar="SCALE")
    (options, args) = parser.parse_args()
    
    # Required options =================================================
    if not options.filename: 
        parser.error('Filename not given')

    # Trackers map =====================================================
    # trackers = AttrDict()
    trackers = [ ('apriltags', AprilTagsFeatureTracker()), ('gpft', DenseTrajectoriesTracker()) ]
    # trackers.apriltags = AprilTagsFeatureTracker(); 
    # trackers.gpft = DenseTrajectoriesTracker() 

    attrs = {'apriltags': {'col':(0,0,200), 'radius':4, 'thickness': 2}, 
             'gpft': {'col':(0,200,0), 'radius':3, 'thickness': -1}}

    # Ground Truth Estimators map ======================================
    gt_estimators = AttrDict()
    # gt_estimators.apriltags = AprilTagsFeatureTracker(data={'expected_id':0} )# writer=Pose3DWriter(), 

    # Set up LCMReader =================================================
    # Only read certain frames
    if options.filename is None or not os.path.exists(options.filename):
        print 'Invalid Filename: %s' % options.filename
        sys.exit(0)
    reader = LCMLogReader(filename=options.filename, scale=options.scale, sequential_read=False)
    frames = np.arange(0, reader.getNumFrames()-1, options.interval).astype(int)

    init_time = time.time();

    # # Open the db  =====================================================
    processed_fn = io_utils.get_processed_filename(options.filename)
    print 'PROCESSED FILENAME', processed_fn
    db = DictDB(filename=processed_fn, mode='w', ext='.h5')

    # Viz ==============================================================
    ncols = 10; cols = np.random.randint(0,256,(ncols,3)).astype(int);
    # draw_utils.publish_sensor_frame('KINECT_FRAME')

    # Main process loop ================================================
    for fno in frames: 
        # Get Frame
        frame = reader.getFrame(fno);
        if frame is None: break

        # Compute normals 
        # frame.fastBilateralFilter(10., 0.03);
        frame.computeNormals(0.5)
        # publish_cloud("KINECT_FILTERED", frame)

        # # Estimate Sensor Pose
        # for gtname, gte in gt_estimators.iteritems(): 
        #     pose = gte.get_pose(frame)
        #     if pose is None: continue
        #     print 'Estimated Pose via %s: %s' % (gtname, pose)

        #     # Write to tracker data
        #     gte.data.append((frame.utime, 1, pose))

        #     draw_utils.publish_pose_list2(''.join([gtname,'_CAMERA_POSE']), [pose])

        # Plot ground truth as white, rest as colored
        rgb = frame.getRGB()

        # Perform tracking for each tracker
        mask = None
        for tname, tracker in trackers: 
            print 'Tracker name: ', tname

            if tname == 'apriltags': 
                fpts, mask = tracker.get_features(frame, return_mask=True)
            else: 
                print 'Tname; ', tname
                assert (mask is not None)
                fpts = tracker.get_features(frame, mask)
            print 'Detected %i features from %s' % (len(fpts), tname)

            # Write to tracker data
            tracker.data.extend(fpts)

            col = attrs[tname]['col']
            radius = attrs[tname]['radius']
            thickness = attrs[tname]['thickness']
            for fpt in fpts: 
                cv2.circle(rgb, tuple(fpt.point.reshape(-1).astype(int).tolist()), 
                           radius, col, thickness, lineType=cv2.CV_AA);

            # Viz normals
            pts3d, normals3d, colors3d = [], [], []
            pts3d.extend([fpt.xyz().reshape(-1) for fpt in fpts])
            normals3d.extend([fpt.normal().reshape(-1)*0.05 for fpt in fpts])
            colors3d.extend([np.array([0.9, 0.1, 0.1, 1.0]) for fpt in fpts])
            if len(pts3d): 
                draw_utils.publish_point_cloud('FPTS_%s' % (tname), 
                                               np.vstack(pts3d), c=np.vstack(colors3d))
                draw_utils.publish_line_segments('FPTS_NORMALS_%s' % (tname), 
                                                 np.vstack(pts3d), 
                                                 np.vstack(pts3d) + np.vstack(normals3d), 
                                                 c=np.vstack(colors3d))


        cv2.imshow('detections', rgb)
        cv2.waitKey(10)

        # Only flush after the whole process
        if reader.frame_num % 100 == 0: 
            print 'Processed %i frames' % reader.frame_num

    # Write to db
    for tname, tracker in trackers:
        db.data[tname] = Feature3DWriter(data=tracker.data)
        print 'TOTAL DATA SAVED: ', len(tracker.data)
    # for gtname, gte in gt_estimators.iteritems():
    #     db.data[gtname] = Pose3DWriter(data=gte.data)
    
    # Close db        
    print 'Closing db ...'
    print 'Processed %i frames in %4.1fs' % (reader.frame_num, time.time() - init_time)
    db.flush()
    db.close()
    print 'Done!' 
