#!/usr/bin/env python
import time, sys
import os.path
from optparse import OptionParser

import numpy as np
import matplotlib as mpl
import matplotlib.pylab as plt

import utils.draw_utils as drawutils
import utils.plot_utils as plotutils
import utils.imshow_utils as cvutils
import utils.grid_crf as grid_crf

import cv2
import cv2.cv as cv
import lcm

from utils import keyboard
import utils.lcmutils as lcmutils
import slic.slic as slic
import slic.depthslic as depthslic

# ===== Matplotlib ==== 
plt.rcParams['figure.figsize'] = 8,8  # that's default image size for this interactive session
mpl.rcParams['figure.figsize'] = [12,12]

#if __name__ == '__main__':
    # parser = OptionParser()
    # parser.add_option("-f", "--filename", dest="filename", 
    #                   help="Log filename")
    # (options, args) = parser.parse_args()

#fn = '/home/spillai/data/2012_07_30_door_handle_fiducial/door_handle_far_with_fiducial'
#reader = lcmutils.KinectLogReader(fn, channel='KINECT_FRAME', fps=0, loop=False)
#img, depth, X = reader.get_next_frame()
#img, depth, X = reader.get_frame(1343674677899844)

fn = '/home/spillai/data/2013_articulation_test/book_unfolding/lcmlog-2013-03-16.00'
reader = lcmutils.KinectLogReader(fn, channel='KINECT_FRAME', fps=0, loop=False)
img, depth, X = reader.get_frame(1363459703409560)
if img is None: sys.exit(0)

# NaN mask for depth images
depth_nan_mask = np.where(depth == 0, 0, 255)

# Img with NaN mask
img_with_depth_mask = np.empty_like(img)
for j in range(3):
    img_with_depth_mask[:,:,j] = np.bitwise_and(img[:,:,j], depth_nan_mask);

# Compute gradience
gradx = np.abs(cv2.Sobel(depth, cv2.CV_32F, 1, 0, ksize=1))
grady = np.abs(cv2.Sobel(depth, cv2.CV_32F, 0, 1, ksize=1))

# Estimate depth error bars, and cap ~ 5cm
depth_err = (depth - 0.5) * (0.070-0.002) / 4.0;
depth_err = np.maximum(depth_err, np.ones_like(depth_err) * 0.010)

# Compute edge confidence map (binary)
depth_with_edges = np.where(np.bitwise_or(gradx >= depth_err,grady >= depth_err), 0, depth)
grad = np.where(np.bitwise_or(gradx >= depth_err,grady >= depth_err), 255, 0)
dgrad = np.multiply(gradx, gradx) + np.multiply(grady, grady)
figure(); plt.imshow(gradx); plt.axis('off')
figure(); plt.imshow(grady); plt.axis('off')


st = time.time()
meand = normal_estimation.normal_estimation(orgX, 4)
print 'normal estimation: ', time.time() - st


#figure();
h,w = depth.shape
#plt.imshow(meand);
#plt.axis('off')
#figure()

plt.imshow(np.ma.masked_array(depth, mask=mask))

# # plt.imshow(gradx)
# # plt.subplot(2,2,2)
# # plt.imshow(grady)
# plt.subplot(1,2,1)
# plt.axis('off')
# plt.imshow(grad)
# plt.subplot(1,2,2)
# plt.axis('off')
# plt.imshow(depth_err)

#f1 = plt.figure()
#plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0, wspace=0.01, hspace=0.01)
#plt.subplot(1,2,1)
#slic_labels = slic.slic_s(img, superpixel_size=200, compactness=30)
#contours = slic.contours(img, slic_labels, 4)

#plt.imshow(contours[:, :, :-1].copy()); plt.axis('off')

# plt.subplot(1,2,2)
# dslic_labels = depthslic.slic_s(depth_with_edges * 1000, superpixel_size=100, compactness=10)
# contours = depthslic.contours(img_with_depth_mask, dslic_labels, 10)
# plt.axis('off')
# plt.imshow(contours[:, :, :-1].copy())


# #plt.gray()
# f2 = plt.figure()
# plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0, wspace=0.01, hspace=0.01)
# # plt.subplot(2,2,1)
# # plt.imshow(gradx)
# # plt.subplot(2,2,2)
# # plt.imshow(grady)
# plt.subplot(1,2,1)
# plt.axis('off')
# plt.imshow(grad)
# plt.subplot(1,2,2)
# plt.axis('off')
# plt.imshow(depth_err)
# plt.show()

# drawutils.publish_sensor_frame('KINECT_FRAME')
# drawutils.publish_point_cloud('KINECT_POINT_CLOUD_DATA', X,c=drawutils.convert_image_to_carr(img)); 



# print options, args

