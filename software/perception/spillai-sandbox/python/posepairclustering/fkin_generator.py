#!/usr/bin/python
import scipy.io as sio
import cv2.cv as cv
import lcm
import numpy as np
import PyKDL as kdl

def kdl_T(f):
    return np.array([[f.M[0,0], f.M[0,1], f.M[0,2], f.p[0]],
                         [f.M[1,0], f.M[1,1], f.M[1,2], f.p[1]],
                         [f.M[2,0], f.M[2,1], f.M[2,2], f.p[2]],
                         [0,0,0,1]])
def kdl_R(f):
    return np.array([[f.M[0,0], f.M[0,1], f.M[0,2]],
                         [f.M[1,0], f.M[1,1], f.M[1,2]],
                         [f.M[2,0], f.M[2,1], f.M[2,2]]])

def kdl_t(f):
    return np.array([f.p[0], f.p[1],f.p[2]])

njoints = 3;
jnt_thickness = 0.02; # m
feats_per_link = 10; 
segment_feats = dict()

chain = kdl.Chain()

len0 = 1.0
jnt0 = kdl.Joint(kdl.Joint.TransY)
frame0 = kdl.Frame(kdl.Vector(len0, 0, 0))
seg0 = kdl.Segment(jnt0, frame0)
chain.addSegment(seg0)
feats = np.random.rand(feats_per_link,3)
feats[:,0] *= -float(len0);
feats[:,1:] *= jnt_thickness;
segment_feats[0] = feats;

len1 = 1.
jnt1 = kdl.Joint(kdl.Joint.RotZ)
frame1 = kdl.Frame(kdl.Vector(len1, 0, 0))
seg1 = kdl.Segment(jnt1, frame1)
chain.addSegment(seg1)
feats = np.random.rand(feats_per_link,3)
feats[:,0] *= -float(len1);
feats[:,1:] *= jnt_thickness;
segment_feats[1] = feats;

len2 = 1.0
jnt2 = kdl.Joint(kdl.Joint.RotZ)
frame2 = kdl.Frame(kdl.Vector(len2, 0, 0))
seg2 = kdl.Segment(jnt2, frame2)
chain.addSegment(seg2)
feats = np.random.rand(feats_per_link,3)
feats[:,0] *= -float(len2);
feats[:,1:] *= jnt_thickness;
segment_feats[2] = feats;

# FK
fk = kdl.ChainFkSolverPos_recursive(chain)

dtheta = 5 * np.pi / 180 / 1000.0;
utimes = np.linspace(0,10*1e6,1e2)
thetas = np.zeros((3,len(utimes)))

# Forward kinematics
jntAngles = kdl.JntArray(njoints)
jntAngles[0] = 20 * np.pi / 180.0;
jntAngles[1] = 30 * np.pi / 180.0;
jntAngles[2] = 70 * np.pi / 180.0;
for j in range(njoints): 
    thetas[j,:] = np.linspace(0,jntAngles[j], len(utimes))

# Tracked Features
total_features = feats_per_link*njoints
saved_features = np.zeros((total_features,len(utimes),8))
end_jntAngles = kdl.JntArray(njoints)    
for j in range(len(utimes)):
    utime = utimes[j]

    endeff = kdl.Frame()    
    for k in range(njoints):
        end_jntAngles[k] = thetas[k][j];

    tf_feats, rpy_feats = [], []
    for k in range(njoints):       
        fk.JntToCart(end_jntAngles, endeff, k+1)
        rpy = kdl.Rotation(endeff.M).GetRPY()

        # Get features from kth segment        
        feats = segment_feats[k];

        # Transform features to the new coordinate frame
        tf_feats.append(np.dot(kdl_R(endeff), feats.T).T + np.tile(kdl_t(endeff), (feats.shape[0],1)))

        # Get RPY for transformed features
        rpy_feats.append(np.tile(np.array([endeff.M[0,0],endeff.M[1,0],endeff.M[2,0]]), (feats.shape[0],1)))
        
    tf_feats = np.vstack(tf_feats)
    tf_feats = tf_feats + np.random.normal(0, 0.01, tf_feats.shape)    
    rpy_feats = np.vstack(rpy_feats)    

    saved_features[:,j,0] = 1;
    saved_features[:,j,1] = 1;
    saved_features[:,j,2:5] = tf_feats;
    saved_features[:,j,5:8] = rpy_feats;

print 'Saving features: '
sio.savemat('track_data.mat', {'feature_data':saved_features, 'feature_utimes':utimes, 'feature_ids':np.arange(total_features)})
#cv.Save('feature_data.yml', cv.fromarray(saved_features),'feature_data');
#cv.Save('feature_utimes.yml', cv.fromarray(utimes.reshape(-1,1)),'feature_utimes');
#cv.Save('feature_ids.yml', cv.fromarray(np.arange(total_features, dtype=float).reshape(-1,1)), 'feature_ids')
    

