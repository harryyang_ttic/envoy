# ===== Spectral embedding  =====
#sp_manifold = fsspectral.SpectralEmbedding(n_components=7, affinity='precomputed')
#sp_manifold = skmanifold.Isomap(n_neighbors=20, n_components=3, max_iter=100, neighbors_algorithm='kd_tree')
sp_manifold = skmanifold.LocallyLinearEmbedding(n_neighbors=10, n_components=8, method='ltsa')
st = time.time()
sp_embedding = sp_manifold.fit_transform(Adist)
#figure()
#plt.title('CumSum of top eigenvalues')
#plt.plot(sp_manifold.lambdas_, '-ro')

plt.title('Spectral embedding of the data to the top 4 eigenvectors')
spmin = numpy.min(numpy.min(sp_embedding[:,:],axis=0));
spmax = numpy.max(numpy.max(sp_embedding[:,:],axis=0));
plt.scatter(sp_embedding[:,2], sp_embedding[:,3], c=(sp_embedding[:,:3]-spmin)/(spmax-spmin), s=30, marker='x')

# ===== Hierarchical clustering tree  ====
#Y = spspatial.distance.pdist(sp_manifold.embedding_[:,:3])
Y = spspatial.distance.pdist(sp_embedding[:,:], 'euclidean');
Ysq = spspatial.distance.squareform(Y);
linkage = fcluster.linkage(Y, method='centroid');

#sp_tree = spcluster.hierarchy.to_tree(linkage)
#print sp_tree.

#f = spcluster.hierarchy.fcluster(linkage, .99, criterion='inconsistent')
#print f

# ===== Work on the hierarchical tree  ====
num_sp_feats = sp_embedding.shape[0];
clusternum = 2
clustdict = {i:[i] for i in xrange(len(linkage)+1)}
#print linkage, clustdict, '\n'
for i in xrange(len(linkage)-clusternum+1):
    clust1= int(linkage[i][0])
    clust2= int(linkage[i][1])

    print 'merging: ', clustdict[clust1], clustdict[clust2]
    #labels1 = np.zeros((num_sp_feats)); labels1[clustdict[clust1]] = 1;
    #labels2 = np.zeros((num_sp_feats)); labels2[clustdict[clust2]] = 1;
    #print labels1, labels2
    #sil1_score = skmetrics.silhouette_samples(sp_embedding[:,:3], labels1, metric='euclidean')
    #sil2_score = skmetrics.silhouette_samples(sp_embedding[:,:3], labels2, metric='euclidean')

    #print 'before silhouette score:', np.mean(sil1_score[clustdict[clust1]]), np.mean(sil2_score[clustdict[clust2]])
    merged = clustdict[clust1] + clustdict[clust2];
    print 'merged set: ', clustdict
    
    # Centroid of new merged cluster (needs to be 2D array)
    cmmean = np.reshape(np.mean(sp_embedding[merged,:], axis=0), (1, -1));
    print 'cmmean', cmmean

    # Compute distortion
    distortion = spspatial.distance.cdist(sp_embedding[merged,:], cmmean, 'euclidean');
    distortion = np.sum(np.min(distortion, axis=1))/len(merged)
    print 'Distortion: ', distortion
    
    #labelsm = np.zeros((num_sp_feats)); labelsm[merged] = 1;
    #silm_score = skmetrics.silhouette_samples(sp_embedding[:,:3], labelsm, metric='euclidean')
    #mean_sil_score = np.mean(silm_score[merged])
    #print 'after silhouette score:', mean_sil_score
    if distortion >= 0.1: break
    
    clustdict[max(clustdict)+1] = merged;
    del clustdict[clust1], clustdict[clust2]
    print 'clustdict size: ', len(clustdict), '\n'
    
labels = np.zeros((num_feats))
label_map = zip(range(0,len(clustdict)), clustdict.keys());
for label,idx in label_map: labels[clustdict[idx]] = label;
num_labels = len(clustdict);
print num_labels, labels

# ===== Visualize hierarchical clustering tree  ====
# plt.suptitle('Hierarchical Clustering of Pair-wise distances with complete linkage')
plotutils.visualize_linkage(Ysq, linkage)
