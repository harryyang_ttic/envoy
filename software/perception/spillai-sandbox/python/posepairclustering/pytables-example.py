#!/usr/bin/env python
import tables as tb
import numpy as np

class Frame(tb.IsDescription):
    utime = tb.Int64Col()       # sensor utime index

    class Kinect(tb.IsDescription):
        rgb = tb.UInt8Col(shape=(480,640))        # img (np.uint8)
        depth = tb.Float32Col(shape=(480,640))    # depth img (np.float32)
        orgX = tb.Float32Col(shape=(480,640,3))     # organized pcl (np.float32)    

h5f = tb.openFile('test.h5', mode='w', title='Test hdf5')
#group = h5f.createGroup('/', 'source', 'Sensor Source')
table = h5f.createTable(h5f.root, 'log', Frame, 'Readout Example')

print h5f
h5f.close()

