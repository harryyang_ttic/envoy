#!/usr/bin/env python


import os.path
from optparse import OptionParser



# import cv2.cv as cv

# import matplotlib.animation as animation
# import matplotlib.delaunay as triang

# from mpl_toolkits.mplot3d.axes3d import Axes3D

# from sklearn import manifold as skmanifold
# from sklearn import cluster as skcluster
# from sklearn import metrics as skmetrics
# from sklearn import utils as skutils

# import scipy as sp
# from scipy import cluster as spcluster
# from scipy import spatial as spspatial

# import fastcluster as fcluster

# import utils.grid_crf as grid_crf
# import utils.draw_utils as drawutils


# import utils.imshow_utils as imutils
# import utils.distributions as distributions

# import utils.slic_utils as slic_utils
# import slic.slic as slic
# import slic.depthslic as depthslic

# import scipy.io as sio
# import utils.fs_spectral_embedding as fsspectral

# import utils.slic_utils as slic_utils

# import slic.slic as slic
# import slic.depthslic as depthslic

# import seeds.seeds as seeds



from collections import namedtuple, defaultdict


# ===== Matplotlib ==== 
pylab.rcParams['figure.figsize'] = 8,6  # that's default image size for this interactive session
mpl.rcParams['figure.figsize'] = [12,12]
