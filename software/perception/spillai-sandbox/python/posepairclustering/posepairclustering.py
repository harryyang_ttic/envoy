#!/usr/bin/env python
import lcm
import numpy as np
import cv2.cv as cv

import matplotlib as mpl
import matplotlib.pylab as plt
import matplotlib.mlab as mlab
import matplotlib.animation as animation

import ..utils.draw_utils as draw
import ..utils.distributions as distributions

from collections import namedtuple, defaultdict

# ===== Parameters ==== 
MIN_TRACK_LENGTH = 50; 
MIN_TRACK_INTERSECTION = 10;
MIN_TRAJECTORY_VAR = 0.05 * 0.05;
POSEPAIR_DISTANCE_SIGMA = 0.015; # sig = 0.015 m
POSEPAIR_DISTANCE_SIGMA_NORM = 0.015 * np.sqrt(2*np.pi); 
POSEPAIR_THETA_SIGMA = 5 * np.pi / 180; # sig = 5 deg
POSEPAIR_THETA_SIGMA_NORM = POSEPAIR_THETA_SIGMA * np.sqrt(2*np.pi);

# ===== Matplotlib ==== 
plt.rcParams['figure.figsize'] = 8,8  # that's default image size for this interactive session
mpl.rcParams['figure.figsize'] = [8,8]

# ===== Pose Pair Distribution ==== 
def plot_pose_pair_variation(ppd):
    # ===== Viz histogram of pose-pair-normal distributions =====
    # plt.subplot(221);
    # n, bins, patches = plt.hist(np.hstack([v.normal for k,v in ppd.iteritems()]),
    #                             20, normed=1, facecolor='green', alpha=0.75, range=(0,1))
    # bincenters = 0.5*(bins[1:]+bins[:-1])
    # y = mlab.normpdf(bincenters, 1, POSEPAIR_THETA_SIGMA)
    # l = plt.plot(bincenters, y, 'r--', linewidth=1)
    # plt.xlim((0,1))
    # plt.grid(True);
    # plt.title('Normal variation');
    # plt.xlabel('Cosine distance (m)')
    
    # plt.subplot(222);
    # n, bins, patches = plt.hist(np.hstack([v.distance for k,v in ppd.iteritems()]),
    #                             20, normed=1, facecolor='blue', alpha=0.75, range=(0,1))
    # bincenters = 0.5*(bins[1:]+bins[:-1])
    # y = mlab.normpdf(bincenters, 1, POSEPAIR_DISTANCE_SIGMA)
    # l = plt.plot(bincenters, y, 'r--', linewidth=1)
    # plt.xlim((0,0.5))
    # plt.grid(True);
    # plt.title('Distance variation');
    # plt.xlabel('Euclidean distance (m)');

    plt.subplot(221);
    n, bins, patches = plt.hist(np.hstack([v.normal_pdf for k,v in ppd.iteritems()]),
                                40, normed=1, facecolor='green', alpha=0.75, range=(0,1))
    bincenters = 0.5*(bins[1:]+bins[:-1])
    y = mlab.normpdf(bincenters, 1, POSEPAIR_THETA_SIGMA)
    l = plt.plot(bincenters, y, 'r--', linewidth=1)
    plt.xlim((0,1))
    plt.grid(True);
    plt.title('Normal variation');
    plt.xlabel('PDF of Cosine distance')
    
    plt.subplot(222);
    n, bins, patches = plt.hist(np.hstack([v.distance_pdf for k,v in ppd.iteritems()]),
                                40, normed=1, facecolor='blue', alpha=0.75, range=(0,1))
    bincenters = 0.5*(bins[1:]+bins[:-1])
    y = mlab.normpdf(bincenters, 1, POSEPAIR_DISTANCE_SIGMA)
    l = plt.plot(bincenters, y, 'r--', linewidth=1)
    plt.xlim((0,1))
    plt.grid(True);
    plt.title('Distance variation');
    plt.xlabel('PDF of Euclidean distance');


# ===== Main instance ==== 
if __name__ == "__main__": 
    lc = lcm.LCM()

    # ===== Get opt args ====
    # fn = '/home/spillai/data/2013_articulation_test/book_unfolding/ground-truth-apr11-3revolute-with-noise.yml'
    # fn = '/home/spillai/data/2013_articulation_test/book_unfolding/ground-truth-apr11-3revolute.yml';
    fn = '/home/spillai/data/2013_articulation_test/book_unfolding/lcmlog-2013-03-16.00-feats.yml';

    # ===== Load the data ==== 
    # Imagine 3D spatio-temporal data (FEATS x UTIMES x DIMS)
    print '===> Loading data: ', fn, '...'
    feature_ids = np.asarray(cv.Load(fn, cv.CreateMemStorage(), 'feature_ids'));
    feature_utimes = np.asarray(cv.Load(fn, cv.CreateMemStorage(), 'feature_utimes'));
    feature_data = np.asarray(cv.Load(fn, cv.CreateMemStorage(), 'feature_data'));

    # ===== Temporary: subset ====
    feature_ids = feature_ids[:1000];
    feature_utimes = feature_utimes[:1000];
    feature_data = feature_data[:1000, :1000, :];

    [num_feats, num_utimes, num_dims] = feature_data.shape;
    if not feature_ids.size == num_feats: raise AssertionError
    if not feature_utimes.size == num_utimes: raise AssertionError
    print 'Num features: ', num_feats
    print 'Num utimes: ', num_utimes
    print 'Num dims: ', num_dims

    # ===== Pruning ====
    # sum over all UTIMES, and check nz for x value
    print '===> Pruning missing features ...'
    valid_feat_inds, = np.where(np.sum(feature_data[:,:,0] != 0,
                                       axis=1)>MIN_TRACK_LENGTH)
    print '--- Done missing pruning features: %i out of %i good' % (len(valid_feat_inds), num_feats)

    # print '===> Pruning short features ...'
    # # remove feats that don't move much
    # tmp_valid_feat_inds = [];
    # for j in valid_feat_inds:
    #     # find nz utime indices 
    #     ut_inds, = np.where(feature_data[j,:,0] != 0);
    #     d = np.linalg.norm(np.var(feature_data[j,ut_inds,2:5], axis=0)); # np.linalg.norm(np.max(feature_data[j,ut_inds,2:5]) - np.min(feature_data[j,ut_inds,2:5],axis=0));
    #     #print np.linalg.norm(np.var(feature_data[j,ut_inds,2:5], axis=0))
    #     if d >= MIN_TRAJECTORY_VAR: tmp_valid_feat_inds.append(j);
    # print '--- Done pruning short features: %i out of %i good' % (len(tmp_valid_feat_inds), len(valid_feat_inds))
    # valid_feat_inds = tmp_valid_feat_inds;


    # ===== Viz data ====
    plt.matshow(np.repeat(feature_data[valid_feat_inds,:,0] != 0, 20, axis=0), interpolation='nearest', cmap=plt.cm.jet);
    plt.title('Tracks');
    plt.xlabel('Time');
    plt.ylabel('Features');

    # ===== Find overlapping tracks ====
    print '===> Extracting overlapping features ...'
    overlap_dict = dict(); # {}
    for j in valid_feat_inds:
        # find nz utime indices
        tj, = np.where(feature_data[j,:,0] != 0)
        if j % 200 == 0: print 'Processing: ', j

        for k in valid_feat_inds:
            if k <= j: continue;

            # find nz utime indices
            tk, = np.where(feature_data[k,:,0] != 0)

            # set intersection
            intersection = np.intersect1d(tj,tk);
            if intersection.size < MIN_TRACK_INTERSECTION: continue;

            overlap_dict[tuple(sorted((j,k)))] = intersection;
    print '--- Done extracting overlapping features'

    # ===== Compute Pose-Pairs for overlapping tracks ====
    print '===> Compute pose-pairs ...'
    Anormal = np.zeros((num_feats, num_feats));
    Adist = np.zeros((num_feats, num_feats));
    posepair_dict = {};
    filtered_posepair_dict = {};
    for key,val in overlap_dict.iteritems():
            ppd = distributions.PosePairDistribution(feature_data, key, val, \
                                                         POSEPAIR_DISTANCE_SIGMA, POSEPAIR_DISTANCE_SIGMA_NORM, \
                                                         POSEPAIR_THETA_SIGMA, POSEPAIR_THETA_SIGMA_NORM);
            posepair_dict[key] = ppd;
            if np.mean(ppd.distance_pdf) >= 0.9:  filtered_posepair_dict[key] = ppd
            Anormal[key[0],key[1]] = ppd.normal_match;
            Adist[key[0],key[1]] = ppd.distance_match;
    print '--- Done computing pose-pairs'

    f = plt.figure(1)
    plt.suptitle('Histogram of pose-pair variation for all valid tracks');
    print 'All valid tracks: ', len(posepair_dict.items())
    plot_pose_pair_variation(posepair_dict)

    f2 = plt.figure(2)
    plt.suptitle('Filtered tracks');
    print 'Filtered tracks: ', len(filtered_posepair_dict.items())
    plot_pose_pair_variation(filtered_posepair_dict)

    posepair_normal_vec = [v.normal for k,v in filtered_posepair_dict.iteritems()];
    posepair_normal_vec = np.vstack(posepair_normal_vec)

    posepair_distance_vec = [v.distance for k,v in filtered_posepair_dict.iteritems()]
    posepair_distance_vec = np.vstack(posepair_distance_vec);
    print filtered_posepair_dict.iterkeys()

    p1, p2 = [], []
    for k in filtered_posepair_dict.iterkeys():
        ut_inds, = np.where(feature_data[k[0],:,0] != 0);
        p1.append(feature_data[k[0],ut_inds[-1],2:5])
        ut_inds, = np.where(feature_data[k[1],:,0] != 0);
        p2.append(feature_data[k[1],ut_inds[-1],2:5])
    p1 = np.vstack(p1)
    p2 = np.vstack(p2)
    draw.publish_line_segments('SORTED_LINE', p1, p2, c=.8)

    posepair_votes = defaultdict(int);
    for k in filtered_posepair_dict.iterkeys():
        posepair_votes[k[0]] += 1;
        posepair_votes[k[1]] += 1;
    sorted_posepair_votes = sorted(posepair_votes.items(), key=lambda x: x[1], reverse=True);
    sorted_inds, sorted_votes = zip(*sorted_posepair_votes);
    print sorted_posepair_votes

    #f = figure(10)
    #f.clf()
    #ax = f.gca(projection='3d')
    sorted_data = []
    sorted_colors = [];
    for idx,votes in sorted_posepair_votes:
        ut_inds, = np.where(feature_data[idx,:,0] != 0);
        c = plt.cm.jet(0.5+float(votes)/50)
        sorted_data.append(feature_data[idx,ut_inds,2:5]);
        sorted_colors.append(np.tile(c, [len(ut_inds),1]));
    sorted_data = np.vstack(sorted_data)
    sorted_colors = np.vstack(sorted_colors)

    draw.publish_sensor_frame('KINECT_FRAME')
    draw.publish_point_cloud('SORTED_DATA', sorted_data, sorted_colors);
    #draw.publish_line_segments('SORTED_LINES',sorted_data)

    #f = figure(10);
    #f.clf()
    #ax = f.gca(projection='3d')
    #ax = axes3d.Axes3D(f)
    # plt.title('Euclidean distance variation')
    # plt.subplot(131)
    # ax.scatter(posepair_distance_vec[:,0], posepair_distance_vec[:,1], posepair_distance_vec[:,2], c='r');
    # f.add_axes(ax)
    # plt.title('XY axis')
    # plt.grid(True)


    #plt.title('Scatter')
    #plt.scatter(feature_data[sorted_posepair_votes])
    # plt.subplot(132)
    # plt.plot(posepair_distance_vec[:,1], posepair_distance_vec[:,2], 'r.');
    # plt.title('YZ axis')
    # plt.grid(True)

    # plt.subplot(133)
    # plt.plot(posepair_distance_vec[:,0], posepair_distance_vec[:,2], 'r.');
    # plt.title('XZ axis')
    # plt.grid(True)

    #mvi.points3d(posepair_distance_vec[:,0], posepair_distance_vec[:,1], posepair_distance_vec[:,2], colormap='copper')
    #mvi.show()
    #plt.subplot(122);
    #plt.scatter(posepair_normal_vec[:,0], posepair_normal_vec[:,1], posepair_normal_vec[:,2], c='b', marker='^')
    #posepair_distance_hist = np.hstack(posepair_distance_hist)

    # <codecell>

    # # ===== Viz Pose-Pairs adjacency matrix ====
    print """Plot visualizing the difference in normal matching"""
    sz = [1]*1;
    plt.matshow(np.kron(Anormal, [sz]*1), interpolation='nearest', cmap=plt.cm.jet);
    plt.title('Difference of Normal matching');
    plt.xlabel('Features')
    plt.ylabel('Features')
    plt.colorbar();

    print """Plot visualizing the difference in euclidean distance matching"""
    plt.matshow(np.kron(Adist, [sz]*1), interpolation='nearest', cmap=plt.cm.jet);
    plt.title('Difference of Euclidean distance matching');
    plt.xlabel('Features')
    plt.ylabel('Features')
    plt.colorbar();

    # # ===== Sort tracks by energy statistic  ====
    # tracken_dict = dict();
    # sorted_feat_inds = []

    # #FeatureInfo = namedtuple("FeatureInfo", ["idx",""])
    # #SortedPair(first=j, second=k);

    # for j in valid_feat_inds:
    #     # find nz utime indices 
    #     ut_inds, = np.where(feature_data[j,:,0] != 0);
    #     d = np.max(feature_data[j,ut_inds,2:5]) - np.min(feature_data[j,ut_inds,2:5],axis=0);
    #     sorted_feat_inds.append((j,np.square(np.linalg.norm(d))));
    # sorted_feat_inds.sort(key=lambda x: x[1], reverse=True);
    # sorted_feat_inds = np.array(sorted_feat_inds,dtype='int')[:,0];

    # # ===== Viz Pose-Pairs adjacency matrix ====
    # # advanced slicing will also work
    # Anormal_sorted = Anormal[:,sorted_feat_inds][sorted_feat_inds]
    # Adist_sorted = Adist[:,sorted_feat_inds][sorted_feat_inds]

    # print """Plot visualizing the difference of Normal matching\
    #     after being sorted by decreasing trajectory lengths"""
    # sz = [1]*1;
    # plt.matshow(np.kron(Anormal_sorted, [sz]*1), interpolation='nearest', cmap=plt.cm.jet);
    # plt.title('Difference of Normal matching\n (sorted by decreasing trajectory lengths)');
    # plt.xlabel('Features')
    # plt.ylabel('Features')
    # plt.colorbar();

    # print """Plot visualizing the difference in euclidean distance matching\
    #         after being sorted by decreasing trajectory lengths"""
    # plt.matshow(np.kron(Adist_sorted, [sz]*1), interpolation='nearest', cmap=plt.cm.jet);
    # plt.title('Difference of Euclidean distance matching\n (sorted by decreasing trajectory lengths)');
    # plt.xlabel('Features')
    # plt.ylabel('Features')
    # plt.colorbar();

    # <codecell>

    # # ===== Temporary: subset ====
    # feature_ids = feature_ids[:1000];
    # feature_utimes = feature_utimes[:1000];
    # feature_data = feature_data[:1000, :1000, :];

    # fig = plt.figure(10); clf()
    # ax = fig.add_subplot(111, autoscale_on=True)
    # utime_text = ax.text(0.02, 0.02, '', transform=ax.transAxes)
    # plt.tight_layout()
    # ax.axis('off')
    # fig.subplots_adjust(left=0.0,right=1.0)

    # from cylcmlogreader import PyLCMLogReader;
    # player = PyLCMLogReader();
    # player.init('/home/spillai/data/2013_articulation_test/book_unfolding/lcmlog-2013-03-16.00');

    # for utime in feature_utimes:
    #     img, cloud = player.getFrame(utime)


    # # # ===== Video Animation ====
    # image = None
    # def animate(idx):
    #     global player, image
    #     if idx % 10 == 0: print 'Frame: ', idx
    #     img,cloud = player.getFrame(feature_utimes[idx])
    #     utime_text.set_text('Time: %4.2f' % feature_utimes[idx])
    #     if image is None:
    #         image = ax.imshow(img, interpolation='nearest', animated=True, label='video')
    #     else:
    #         image.set_data(img)
    #         plt.draw()
    #     return image,utime_text


    # frames = feature_utimes.size
    # anim = animation.FuncAnimation(fig, animate, frames=100, blit=True)
    # anim.save('test.mp4', fps=20)
    #plt.show()



    # if True:
    #     imcount = 0
    #     path = '/home/spillai/data/2013_articulation_test/book_unfolding/processed.avi'
    #     fourcc = cv.CV_FOURCC('i','Y','U','V')
    #     writer = cv2.VideoWriter(path, fourcc, 1.0, (640, 480), True)
    #     if writer: print 'writing'
    #     for utime in feature_utimes:
    #         img,cloud = player.getFrame(utime)
    #         imcount += 1
    #         #print img
    #         writer.write(img)
    #         if imcount > 10: break
    #     writer.release()
    #     print 'Done writing to ', path


    # print feature_utimes.size
    # feature_utimes

    # for utime in feature_utimes:
    #     img = 

    # def init_frame():
    #     """init animation"""
    #     utime_text.set_text('')
    #     return utime_text

    # def animate(utime):
    #     ""perform animation step""
    #     utime_text.set_text('time = %7.3f' % utime)
    #     return utime_text

    # <codecell>


