#include "wjbf_wrapper.hpp"

namespace fs { namespace vision { 

void depth162depth8Color(Mat& src, Mat& dest, double minv, double maxv)
{
  Mat depthmap8u;
  src-=(short)minv;
  src.convertTo(depthmap8u,CV_8U,255.0/(maxv-minv));
  applyColorMap(depthmap8u,dest,2);
}

void weighted_joint_bilateral_filter(const cv::Mat& srcImage, const cv::Mat& srcDepth,
                                     cv::Mat& depthout) {


  const int alpha = 0; 
  const int sw = 8; 
  const int r = 3; 
  const int sigs = 30; 
  const int sigc = 50; 
  const int sigc2 = 50; 
  const int pr = 2; 
  
  const double ss = sigs/10.0;
  const double sc = sigc/10.0;
  const double sc2 = sigc2;
  const int d = 2*r+1;

  Mat srcImageGray; cvtColor(srcImage,srcImageGray,CV_BGR2GRAY);
  Mat srcImagef;srcImageGray.convertTo(srcImagef,CV_32F);
  
  Mat filledDepth = srcDepth.clone();
  fillOcclusionDepth(filledDepth,0);

  Mat tp;
  transpose(filledDepth,tp);
  fillOcclusionDepth(tp,0);
  transpose(tp,filledDepth);

  Mat filledDepthf; filledDepth.convertTo(filledDepthf,CV_32F);

  // double minv,maxv;
  // minMaxLoc(filledDepth,&minv,&maxv);
  
  Mat filteredDepthf = Mat::ones(srcDepth.size(),CV_32F);
  jointBilateralFilter(filledDepthf,srcImagef,filteredDepthf,Size(d,d),sc,ss,BILATERAL_SEPARABLE);
  filteredDepthf.convertTo(depthout,CV_16U);
  jointNearestFilter(depthout, filledDepth, Size(2*pr+1,2*pr+1),depthout);

  // Mat depthShow;
  // depth162depth8Color(depthout,depthShow,minv,maxv);

  // Mat temp;
  // cvtColor(srcImageGray,temp,CV_GRAY2BGR);
  // addWeighted(temp,alpha/100.0,depthShow,1.0-alpha/100.0,0.0,depthShow);
 
}

} // namespace vision
} // namespace fs
