# =========== cmake stuff  ==============
cmake_minimum_required(VERSION 2.6.0)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-deprecated-declarations -Wno-write-strings -Wno-unused-result")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x -Wno-deprecated-declarations -Wno-write-strings -Wno-unused-result -msse4.1")

# # SSE related
# set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
# include(cmake/OptimizeForArchitecture.cmake)


# =========== Weighted Joint Bilateral Filter ==============
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/klt)
file(GLOB wjbf_h_files "${CMAKE_CURRENT_SOURCE_DIR}/*.h")
file(GLOB wjbf_c_files "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")
add_library(wjbf  SHARED 
    wjbf_wrapper.cpp 
    ${wjbf_c_files}
)
pods_use_pkg_config_packages(wjbf 
    opencv
    # ${LCMTYPES_KINECT} ${LIBBOT_PACKAGES} glib-2.0
)
set_target_properties(wjbf PROPERTIES SOVERSION 1)
pods_install_libraries(wjbf)
pods_install_headers( ${wjbf_h_files} wjbf_wrapper.hpp DESTINATION fs_perception_wrappers/wjbf)
pods_install_pkg_config_file(wjbf
  LIBS -lwjbf
  REQUIRES opencv
  VERSION 0.0.1)
