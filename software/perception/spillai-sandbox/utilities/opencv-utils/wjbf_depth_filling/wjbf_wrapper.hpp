// Weighted Joint Bilateral Filter
// Refer to 
// http://nma.web.nitech.ac.jp/fukushima/research/weightedjointbilateralfilter.html

#include <opencv2/opencv.hpp>
// #include <perception_opencv_utils/opencv_utils.hpp>

// weighted-joint-bilateral filter
#include <fs_perception_wrappers/wjbf/filter.h>
#include <fs_perception_wrappers/wjbf/util.h>

namespace fs { namespace vision { 

void depth162depth8Color(Mat& src, Mat& dest, double minv, double maxv);
void weighted_joint_bilateral_filter(const cv::Mat& srcImage, const cv::Mat& srcDepth, cv::Mat& depthout);

  
} // namespace vision
} // namespace fs

