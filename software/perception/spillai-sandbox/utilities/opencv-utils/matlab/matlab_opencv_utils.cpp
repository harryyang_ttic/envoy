#include "matlab_opencv_utils.hpp"

namespace opencv_utils { 
    mxArray* cv_to_matlab(const cv::Mat& Tin) { 
	mxArray* Tout; 
        Tout = mxCreateDoubleMatrix(Tin.rows, Tin.cols, mxREAL);
        if (Tin.type() == CV_64FC1)
            memcpy((void *)mxGetPr(Tout), (void *)Tin.data, Tin.rows * Tin.step);
        else { 
            cv::Mat_<double> tmp; 
            Tin.convertTo(tmp, CV_64FC1); 
            memcpy((void *)mxGetPr(Tout), (void *)tmp.data, tmp.rows * tmp.step);
        }
        return Tout;
    }

    cv::Mat_<double> matlab_to_cv(const char* str, const mxArray* T) { 
        cv::Mat Tout;
        if (strcmp(mxGetClassName(T), "double") == 0) {
            const long unsigned int* dims = mxGetDimensions(T); 
            assert(dims[0] > 0 && dims[1] > 0);
            Tout = cv::Mat_<double>(dims[0], dims[1]); 
            memcpy(Tout.data, (double *)mxGetData(T), sizeof(double)*dims[0] * dims[1]);
        } else { 
            assert(0);
        }
        return Tout;
    }
}
