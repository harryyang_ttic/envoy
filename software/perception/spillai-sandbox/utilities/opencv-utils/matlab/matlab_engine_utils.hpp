// TO-DO
// - Dynamic Input variables 
/// 

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "matlab_opencv_utils.hpp"
#include "engine.h"

#define  BUFSIZE 10000
#define MATLAB_ERROR(x) x != 0

static Engine* _ep = NULL;
static int _ep_refcount = 0; 
struct MatlabEngine { 
    Engine* ep; 
    char buffer[BUFSIZE+1]; 
    MatlabEngine() { 
        // * Call engOpen with a NULL string. This starts a MATLAB process 
        // * on the current host using the command "matlab".
        if (_ep)
            std::cerr << "MATLAB Engine Instance already exists!" << std::endl;
        else { 
            if (!(_ep = engOpen(""))) {
                std::cerr << "Can't start MATLAB engine" << std::endl;
            } 

            buffer[BUFSIZE] = '\0';
            engOutputBuffer(_ep, buffer, BUFSIZE);

            // std::cerr << "MatlabEngine(): prev. REFCOUNT: " << _ep_refcount << std::endl;
            ep = _ep;
        }

        _ep_refcount = (_ep) ? _ep_refcount + 1 : _ep_refcount; // refcount only if created (or exists)
    }
    ~MatlabEngine() { 
        // std::cerr << "~MatlabEngine(): prev. REFCOUNT: " << _ep_refcount << std::endl;
        _ep_refcount--;
        if (!_ep_refcount) { 
            std::cerr << "Shutting down MATLAB Engine" << std::endl;
            engClose(ep);
        }
    }
    bool live() { 
        return (ep) ? true : false; 
    }
    bool eval(const char* str) { 
        return MATLAB_ERROR(engEvalString(ep, str));
    }
    bool put(const char* str, const mxArray* T) { 
        return MATLAB_ERROR(engPutVariable(ep, str, T));
    }
    bool put(const char* str, const cv::Mat& T) { 
        mxArray* Tmat = opencv_utils::cv_to_matlab(T); // allocates array
        bool r = put(str, Tmat);
        mxDestroyArray(Tmat);
        return r;
    }
    bool get(const char* str, cv::Mat& T) { 
        mxArray* Tmat = NULL;
        if (!(Tmat = engGetVariable(ep, str))) { 
            std::cerr << "Error getting variable " << std::string(str) << std::endl;
            return false;
        }
        T = opencv_utils::matlab_to_cv(str, Tmat);
        return true;
    }

    bool keyboard() {
        std::cerr << "Entering MATLAB debug mode" << std::endl;
        char str[BUFSIZE+1];
        do { 
            printf(">> ");
            fgets(str, BUFSIZE, stdin);
        
            eval(str);
            printf("%s", buffer);

        } while (strcmp(str, "q\n") != 0);
        return true;
    }
};
                
        
