#ifndef MATLAB_OPENCV_PLOT_UTILS_H__
#define MATLAB_OPENCV_PLOT_UTILS_H__

#include <math.h>
#include <stdio.h>
#include <opencv2/opencv.hpp>

#include "engine.h"

namespace opencv_utils { 
    mxArray* cv_to_matlab(const cv::Mat& T);
    cv::Mat_<double> matlab_to_cv(const char* str, const mxArray* T);

}

#endif // MATLAB_OPENCV_PLOT_UTILS_H
