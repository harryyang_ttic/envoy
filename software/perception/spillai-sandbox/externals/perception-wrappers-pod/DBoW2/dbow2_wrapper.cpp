#include "dbow2_wrapper.hpp"

namespace fs { namespace vision { 

DBoW2::DBoW2() {
  // extended surf gives 128-dimensional vectors
  // cv::initModule_nonfree();
  const bool EXTENDED_SURF = true;
  surf = cv::SURF(200, 4, 2, EXTENDED_SURF);
}

DBoW2::~DBoW2() {
}

void DBoW2::reset() {
  features.clear();
  // features.reserve(NIMAGES);
}

void DBoW2::addFrame(const opencv_utils::Frame& frame) {
  addImage(frame.getGrayRef());
}

void DBoW2::addImage(const cv::Mat& img) {

  cv::Mat mask;
  std::vector<cv::KeyPoint> keypoints;
  std::vector<float> descriptors;
  
  surf(img, mask, keypoints, descriptors);
  features.push_back(vector<vector<float> >());

  changeStructure(descriptors, features.back(), surf.descriptorSize());
}

std::vector<Result>
DBoW2::queryFrame(const opencv_utils::Frame& frame, int results) {
  return queryImage(frame.getGrayRef(), results);
}

std::vector<Result>
DBoW2::queryImage(const cv::Mat& img, int results) {
  cv::Mat mask;
  std::vector<cv::KeyPoint> keypoints;
  std::vector<float> descriptors;
  
  surf(img, mask, keypoints, descriptors);

  std::vector<std::vector<float> > feats;
  changeStructure(descriptors, feats, surf.descriptorSize());

  QueryResults ret;
  db.query(feats, ret, results);

  std::vector<Result> res(ret.begin(), ret.end());
  // ret[0] is always the same image in this case, because we added it to the 
  // database. ret[1] is the second best match.
  cout << "Searching for Image " << ret << endl;
  
  return res;
}

void DBoW2::buildVOC() {
  // branching factor and depth levels 
  const int k = 9;
  const int L = 3;
  const WeightingType weight = TF_IDF;
  const ScoringType score = L1_NORM;

  Surf64Vocabulary voc(k, L, weight, score);

  cout << "Creating a small " << k << "^" << L << " vocabulary..." << endl;
  voc.create(features);
  cout << "... done!" << endl;

  cout << "Vocabulary information: " << endl
       << voc << endl << endl;

  // // lets do something with this vocabulary
  // cout << "Matching images against themselves (0 low, 1 high): " << endl;
  // BowVector v1, v2;
  // for(int i = 0; i < features.size(); i++)
  // {
  //   voc.transform(features[i], v1);
  //   for(int j = 0; j < features.size(); j++)
  //   {
  //     voc.transform(features[j], v2);
      
  //     double score = voc.score(v1, v2);
  //     cout << "Image " << i << " vs Image " << j << ": " << score << endl;
  //   }
  // }

  // save the vocabulary to disk
  cout << endl << "Saving vocabulary..." << endl;
  voc.save("small_voc.yml.gz");
  cout << "Done" << endl;

}

void DBoW2::buildDB() {
  cout << "Creating a small database..." << endl;

  // load the vocabulary from disk
  Surf64Vocabulary voc("small_voc.yml.gz");
  
  // Surf64Database db(false, 0); // false = do not use direct index
  db = Surf64Database (voc, true, 0); // false = do not use direct index
  // (so ignore the last param)
  // The direct index is useful if we want to retrieve the features that 
  // belong to some vocabulary node.
  // db creates a copy of the vocabulary, we may get rid of "voc" now

  // add images to the database
  for(int i = 0; i < features.size(); i++)
  {
    db.add(features[i]);
  }

  cout << "... done!" << endl;

  cout << "Database information: " << endl << db << endl;

  // // and query the database
  // cout << "Querying the database: " << endl;

  // QueryResults ret;
  // for(int i = 0; i < features.size(); i++)
  // {
  //   db.query(features[i], ret, 4);

  //   // ret[0] is always the same image in this case, because we added it to the 
  //   // database. ret[1] is the second best match.

  //   cout << "Searching for Image " << i << ". " << ret << endl;
  // }

  // cout << endl;

  // we can save the database. The created file includes the vocabulary
  // and the entries added
  cout << "Saving database..." << endl;
  db.save("small_db.yml.gz");
  cout << "... done!" << endl;
  
  // // once saved, we can load it again  
  // cout << "Retrieving database once again..." << endl;
  // Surf64Database db2("small_db.yml.gz");
  // cout << "... done! This is: " << endl << db2 << endl;
}


void DBoW2::changeStructure(const std::vector<float> &plain,
                            std::vector<std::vector<float> > &out,
                            int L) {
  out.resize(plain.size() / L);

  unsigned int j = 0;
  for(unsigned int i = 0; i < plain.size(); i += L, ++j)
  {
    out[j].resize(L);
    std::copy(plain.begin() + i, plain.begin() + i + L, out[j].begin());
  }
}


} // namespace vision
} // namespace fs
