#include "fs_dense_trajectories.hpp"

// using namespace cv;

namespace fs { namespace vision {

DenseTrajectories::DenseTrajectories() { 
  std::cerr << "dense traj. ctor" << std::endl;
  // Profiler
  profiler_.setName("DTraj", true, true);
  
  frame_num = 0;
  show_track = 1;
  track_id = 0;
  scale_num = 5;
  
  // inits
  fscales = std::vector<float>(0);
  sizes = std::vector<cv::Size>(0);

  prev_grey_pyr = std::vector<cv::Mat> (0),
      grey_pyr = std::vector<cv::Mat> (0),
      flow_pyr = std::vector<cv::Mat> (0);

  // for optical flow
  prev_poly_pyr = std::vector<cv::Mat>(0),
      poly_pyr = std::vector<cv::Mat>(0); 
    
  init_counter = 0;

#define GFTT 1
#if GFTT
  // FAST / GFTT Feature Detector
  int gftt_maxCorners=500; double gftt_qualityLevel=QUALITY;
  double gftt_minDistance=MIN_DISTANCE;
  int gftt_blockSize=11; bool gftt_useHarrisDetector=false; double gftt_k=0.06 ;
  
  detector_ = cv::Ptr<cv::FeatureDetector> (
      new cv::GFTTDetector( gftt_maxCorners, gftt_qualityLevel, gftt_minDistance,
                            gftt_blockSize, gftt_useHarrisDetector, gftt_k) );
#else
  int fast_threshold = 30; bool nonmax = true;
  detector_ = cv::Ptr<cv::FeatureDetector> (
      new cv::FastFeatureDetector( fast_threshold, nonmax, FastFeatureDetector::TYPE_9_16 ));
#endif
  
  // Create extractor
  const bool extended = false, upright = true;
  cv::initModule_nonfree();
  extractor_ = cv::Ptr<cv::DescriptorExtractor>
      ( new SurfDescriptorExtractor(400, 1, 1, extended, upright) );
  // extractor_ =  cv::Ptr<cv::DescriptorExtractor>( new BriefDescriptorExtractor(32) );
   
  // Plot colors
  if (!colors.size()) { 
    colors = std::vector<cv::Scalar>(10);
    for (int j=0; j<colors.size(); j++)
      colors[j] = opencv_utils::val_to_bgr(j * 1.0 / (colors.size()-1));
  }

  if( detector_.empty() || extractor_.empty() ) {
    std::cout << "Failed to create detector/extractor" << std::endl;
    exit(0);
  }

}

DenseTrajectories::~DenseTrajectories() { 
  std::cerr << "dense traj. dtor" << std::endl;
}

void DenseTrajectories::processFrame(opencv_utils::Frame& frame, const cv::Mat& mask) {
  const cv::Mat& rgb = frame.getRGBRef();
  profiler_.enter("processFrame()");
  frame.computeNormals(0.25);
  profiler_.leave("processFrame()");

  // Copy for depth, surface normal extraction
  frame_ = frame;
  
  profiler_.enter("processImage()");
  processImage(rgb, mask);
  profiler_.leave("processImage()");
}

void DenseTrajectories::InitImagePyramids(const cv::Size& sz) {
  // Init Pyramids
  InitPry(sz, fscales, sizes);

  // Build Pyramids
  BuildPry(sizes, CV_8UC1, prev_grey_pyr);
  BuildPry(sizes, CV_8UC1, grey_pyr);

  BuildPry(sizes, CV_32FC2, flow_pyr);
  BuildPry(sizes, CV_32FC(5), prev_poly_pyr);
  BuildPry(sizes, CV_32FC(5), poly_pyr);

  // Create scale-space tracks 
  xyScaleTracks.resize(scale_num);

}

void DenseTrajectories::BuildImagePyramids(const cv::Mat& img, std::vector<cv::Mat>& img_pyr) {

  for(int iScale = 0; iScale < scale_num; iScale++) {
    if(iScale == 0)
      img.copyTo(img_pyr[0]);
    else
      cv::resize(img_pyr[iScale-1], img_pyr[iScale],
                 img_pyr[iScale].size(), 0, 0, INTER_LINEAR);
  }
}


void
DenseTrajectories::visualize() {

  std::vector<std::pair<int, float> > plot_data;
  cv::Mat display = image.clone();
  
  // For each scale
  for (int iScale=0; iScale < scale_num; iScale++) {

    // For each track
    const std::vector<Track>& tracks = xyScaleTracks[iScale];
    for (int j=0; j<tracks.size(); j++) {      
      // draw the trajectories at the first scale
      if(show_track == 1)
        DrawTrack(tracks[j], iScale, display);

      // plot
      if (tracks[j].status != Track::LOST)
        plot_data.push_back(std::make_pair(tracks[j].id, tracks[j].match.back()));

    }
  }

  // Done with copying, plot
  if( show_track == 1 ) {
    cv::imshow( "FSDenseTrack", display);
  }

  // cv::Mat plt = opencv_utils::plot_1D("match-brief", plot_data);
  // cv::imshow("plot", plt);

  return;  
}

// void DenseTrajectories::ExtractFeatures(const std::vector<cv::Point2f>& pred_points,
//                                         const int& iScale,
//                                         std::vector<fsvision::Feature3D>& pred_features) {

//   // 1. Extract features
//   std::vector<cv::KeyPoint> pred_kpts(pred_points.size());
//   for (int j=0; j<pred_points.size(); j++) { 
//     pred_kpts[j].class_id = j;
//     pred_kpts[j].pt = pred_points[j];
//     pred_kpts[j].size = MIN_DISTANCE * 9 / (1.2 * 2); // 1.f;
//   }

//   // 1a. Extract description
//   cv::Mat pred_desc;
//   extractor_->compute(grey_pyr[iScale], pred_kpts, pred_desc);
//   assert(pred_kpts.size() == pred_desc.rows);

//   // 1b. Re-init, and set values
//   pred_features = std::vector<fsvision::Feature3D>(pred_points.size());
//   for (int j=0; j<pred_features.size(); j++)
//     pred_features[j].set_point(cv::Point2f(-1,-1));
  
//   for (int j=0; j<pred_kpts.size(); j++) {
//     int idx = pred_kpts[j].class_id;
//     pred_features[idx].set_point(pred_kpts[j].pt);
//     pred_features[idx].set_desc(pred_desc.row(j));
//   }

//   return;
// }

void DenseTrajectories::ExtractFeatures(std::vector<cv::Point2f>& pred_points,
                                        std::vector<cv::Mat>& pred_desc,
                                        std::vector<cv::Mat>& pred_mipmap,
                                        const int& iScale) { 

  // 1. Extract features
  std::vector<cv::KeyPoint> pred_kpts(pred_points.size());
  for (int j=0; j<pred_points.size(); j++) { 
    pred_kpts[j].class_id = j;
    pred_kpts[j].pt = pred_points[j];
    pred_kpts[j].size = MIN_DISTANCE; // MIN_DISTANCE * 9 / (1.2 * 2); // 1.f;
  }

  // 1a. Extract description
  cv::Mat pred_desc_;
  extractor_->compute(grey_pyr[iScale], pred_kpts, pred_desc_);
  assert(pred_kpts.size() == pred_desc_.rows);

  // 1b. Re-init, and set values
  pred_points = std::vector<cv::Point2f>(pred_points.size(), cv::Point2f(-1,-1));
  pred_desc = std::vector<cv::Mat>(pred_points.size());
  pred_mipmap = std::vector<cv::Mat>(pred_points.size());
  for (int j=0; j<pred_kpts.size(); j++) {
    int idx = pred_kpts[j].class_id;
    pred_points[idx] = pred_kpts[j].pt;
    pred_desc[idx] = pred_desc_.row(j);

    cv::Rect r(pred_points[idx].x-MIN_DISTANCE/2, pred_points[idx].y-MIN_DISTANCE/2,
               MIN_DISTANCE, MIN_DISTANCE);
    r &= cv::Rect(0, 0, grey_pyr[iScale].cols, grey_pyr[iScale].rows);
    pred_mipmap[idx] = cv::Mat(grey_pyr[iScale], r).clone();
  }

  return;
}


void DenseTrajectories::PrepareRecovery() {

  // Prepare query, and all points for NN lookup
  std::vector<cv::Point2f> query_points, rest_points;
  
  // For each scale
  for (int iScale=0; iScale < scale_num; iScale++) {
    // For each track
    const std::vector<Track>& tracks = xyScaleTracks[iScale];
    for (int j=0; j<tracks.size(); j++) {      
      if (tracks[j].status == Track::LOST)
        query_points.push_back(tracks[j].point.back() * fscales[iScale]);
      else
        rest_points.push_back(tracks[j].point.back() * fscales[iScale]);
    }
  }

  // No recovery required
  if (!rest_points.size() || !query_points.size()) return;

  profiler_.enter("k-d Tree build/search");
  // Build kd-tree on rest_points, and query 
  cv::Mat qdata = cv::Mat(query_points).reshape(1);
  cv::Mat rdata = cv::Mat(rest_points).reshape(1);  
  c_index = cv::Ptr<cv::flann::Index>(new cv::flann::Index(rdata,
                                                           cv::flann::KDTreeIndexParams(4)));

  const int KNN=5;
  cv::Mat indices(qdata.rows, KNN, CV_32SC1, -1); 
  cv::Mat dists (qdata.rows, KNN, CV_32FC1);
  c_index->knnSearch(qdata, indices, dists, KNN, cv::flann::SearchParams(64));
  profiler_.leave("k-d Tree build/search");

  std::cerr << dists << std::endl;
  return;
}

// 1. Build Image Pyramids
// 2. Detect features at each scale
// 2a Set scaled mask for detected features

// 3. Compute dense optical flow (Farneback) between prev. and curr image
// 3a Predict features based on flow, and add if predicted features
//    are close to detected features
// 3b Extract descriptors of features
// 3c Check consistency of description within a track, and add if consistent
// 3d Remove track if not consistent

// 4. Add sparse features to tracks if not predicted
static bool max_track_length(const Track& t1, const Track& t2) {
  return t1.point.size() < t2.point.size();
}

void
DenseTrajectories::processImage(const cv::Mat& frame, const cv::Mat& mask) { 

  profiler_.enter("initImages()");
  // Init: First frame
  if (image.empty()) {
    // Init Image Pyramids
    InitImagePyramids(frame.size());
  }

  // Keep copy of input
  image = frame.clone();
  // cv::Mat disp; const float sr = 2.f;
  // cv::resize(image, disp, cv::Size(image.cols*sr, image.rows*sr), 0, 0, INTER_CUBIC);
  // if (!prev_image.empty()) { 
  //   cv::Mat prev_disp;
  //   cv::resize(prev_image, prev_disp, cv::Size(image.cols*sr, image.rows*sr),
  //              0, 0, INTER_CUBIC);
  //   disp = prev_disp / 2 + disp / 2;
  // }

  // if (!prev_image.empty() && !image.empty()) {
  //   cv::imshow("img", image);
  //   cv::imshow("pimg", prev_image);
  // }
  
  // Grayscale, and blur
  cv::cvtColor(image, grey, CV_BGR2GRAY);
  cv::GaussianBlur(grey, grey, cv::Size(5,5), 0, 0, cv::BORDER_DEFAULT);
  
  // Build Pyramid
  BuildImagePyramids(grey, grey_pyr);
  // cv::buildPyramid(grey, grey_pyr, scale_num);
  profiler_.leave("initImages()");
  
  profiler_.enter("sample()");

  // Temp. masks for viz
  // cv::Mat1b det_mask_full(image.size(), 255);
  // std::vector<cv::Mat1b> det_mask_combined(scale_num);

  // Two masks:
  // 1. Detection mask: keep track of features detected and only predict
  // features in regions that have detections
  // 2. Occupancy mask: keep track of what features have been predicted,
  // and only add features in regions that don't have predictions
  std::vector<cv::Mat1b> det_mask(scale_num), occ_mask(scale_num);

  // Current Detection Features
  std::vector<std::vector<cv::Point2f> > c_points(scale_num, std::vector<cv::Point2f>(0));

  // Extract features at each scale
  for(int iScale = 0; iScale < scale_num; iScale++) {
    // 1a. Sample features from grayscale pyramid, with initial mask
    cv::Mat pyr_mask;
    
    if (!mask.empty())
      cv::resize(mask, pyr_mask, grey_pyr[iScale].size(), 0, 0, INTER_LINEAR);
    SparseSample(grey_pyr[iScale], pyr_mask, c_points[iScale]);
    
    // Fill mask with ones
    det_mask[iScale] = cv::Mat1b(grey_pyr[iScale].size(), 255);
    occ_mask[iScale] = cv::Mat1b(grey_pyr[iScale].size(), 255);

    // Set detected feature regions with zeros
    for (int j=0; j<c_points[iScale].size(); j++) { 
      cv::circle(det_mask[iScale], c_points[iScale][j], MIN_DISTANCE, 0, -1, CV_AA);

      // // Draw detection mask radius
      // if (iScale == 0) { 
      // cv::circle(disp, c_points[iScale][j] * fscales[iScale] * sr, MIN_DISTANCE*sr, cv::Scalar(150,150,150), 1, CV_AA, 0);
      // // cv::circle(disp, c_points[iScale][j] * fscales[iScale] * sr, sr, cv::Scalar(150,150,150), -1, CV_AA, 0);
      // }
    }

    // det_mask_combined[iScale] = det_mask_full.clone();
    // std::cerr << "iscale: " << iScale << " " << grey_pyr[iScale].size() << std::endl;
  }
  profiler_.leave("sample()");
  
  // Compute polynomial expansion
  profiler_.enter("farnebackpoly()");
  my::FarnebackPolyExpPyr(grey, poly_pyr, fscales, 7, 1.5);
  profiler_.leave("farnebackpoly()");
  
  // OPT1: OF Match
  // 1. Predict features with flow
  // 2. Extract feature descriptors for each prediction
  // 3. Validate feature descriptor with track
  // 4. Prune tracks if features drift
  // 5. Add new features
  if (!prev_grey.empty() && !grey.empty()) {
    assert(prev_poly_pyr.size() && poly_pyr.size() && flow_pyr.size());
    assert(poly_pyr.size() == prev_poly_pyr.size());
    assert(poly_pyr.size() == flow_pyr.size());
    
    // compute optical flow for all scales once
    profiler_.enter("farnebackOF()");
    my::calcOpticalFlowFarneback(prev_poly_pyr, poly_pyr, flow_pyr, 10, 2);
    profiler_.leave("farnebackOF()");
    
    // Given prev_pts, and flow: match features at each scale
    profiler_.enter("match()");
    for (int iScale=0; iScale < scale_num; iScale++) {
      const int& width = grey_pyr[iScale].cols;
      const int& height = grey_pyr[iScale].rows;
      std::vector<Track>& tracks = xyScaleTracks[iScale];
      
      // 1. Predict points
      std::vector<cv::Point2f> pred_points(tracks.size());
      for (int j=0; j<tracks.size(); j++) {

        // Only predict previously matched tracks
        if (tracks[j].status >= Track::LOST) continue;

        // Predict, and check bounds
        const cv::Point2f& prev_point = tracks[j].point.back();
        int x = std::min<int>(std::max<int>(cvRound(prev_point.x), 0), width-1);
        int y = std::min<int>(std::max<int>(cvRound(prev_point.y), 0), height-1);
        cv::Point2f pred_point =
            cv::Point2f(prev_point + flow_pyr[iScale].ptr<cv::Point2f>(y)[x]);

#if 1
        // 1a. Only if feature has been detected, predict
        // 1b. Set mask for new features to be added
        if (det_mask[iScale](pred_point) == 0) { 
          pred_points[j] = pred_point;
          cv::circle(occ_mask[iScale], pred_point, MIN_DISTANCE, 0, -1, CV_AA);
        }
        else
          pred_points[j] = cv::Point2f(-1,-1);


        // if (iScale == 0) {
        //    // Draw blue flow line, and prev. pt
        //   cv::circle(disp, prev_point * fscales[iScale] * sr, 1*sr, cv::Scalar(240,0,0), -1, CV_AA, 0);
        //  if (det_mask[iScale](pred_point) == 0) { 
        //     cv::line(disp, pred_point * fscales[iScale] * sr, prev_point * fscales[iScale] * sr,
        //            cv::Scalar(240,0,0),1, CV_AA, 0);
        //     cv::circle(disp, pred_point * fscales[iScale] * sr, 1*sr, cv::Scalar(240,0,0), -1, CV_AA, 0);
        //   } else {
        //     cv::line(disp, pred_point * fscales[iScale] * sr, prev_point * fscales[iScale] * sr,
        //              cv::Scalar(200,200,200),1, CV_AA, 0);
        //     cv::circle(disp, pred_point * fscales[iScale] * sr, 1*sr, cv::Scalar(0,0,240), -1, CV_AA, 0);
                       

        //   }
        // }
        
#else
        // Predict even if no detection
        pred_points[j] = pred_point;
#endif
      }

      // 2. Extract Features on each scale (both description and 3D)
      // std::vector<fsvision::Feature3D> pred_features;
      std::vector<cv::Mat> pred_desc, pred_mipmap;
      ExtractFeatures(pred_points, pred_desc, pred_mipmap, iScale);
      
      // 3. Validate predicted feature points for each track
      int track_index = 0;
      for (std::vector<Track>::iterator iTrack = tracks.begin();
           iTrack != tracks.end(); track_index++) {
        const cv::Point2f& prev_point = iTrack->point.back();
        const cv::Point2f& pred_point = pred_points[track_index];

        // 3a. Remove track if not matched (Point(-1,-1) from earlier)
        if(pred_point.x <= 0 || pred_point.x >= width ||
           pred_point.y <= 0 || pred_point.y >= height) {
          iTrack = tracks.erase(iTrack);
          // iTrack->status = Track::LOST; ++iTrack;         
          continue;
        }
       
        // 3b. Attempt to add pred. point to track
        bool added = iTrack->addPoint(pred_point, pred_desc[track_index], pred_mipmap[track_index]);

        // if (iScale == 0) {
        //   if (added) { 
        //     // Draw green flow line if added, else grey
        //     cv::circle(disp, prev_point * fscales[iScale] * sr, 1*sr, cv::Scalar(0,240,0), -1, CV_AA, 0);
        //     cv::circle(disp, pred_point * fscales[iScale] * sr, 1*sr, cv::Scalar(0,240,0), -1, CV_AA, 0);
        //     cv::line(disp, pred_point * fscales[iScale] * sr, prev_point * fscales[iScale] * sr,
        //            cv::Scalar(0,240,0),1, CV_AA, 0);
        //   } else {
        //     // Draw grey flow line if not added
        //     cv::circle(disp, pred_point * fscales[iScale] * sr, 1*sr, cv::Scalar(0,0,240), -1, CV_AA, 0);
        //     cv::circle(disp, prev_point * fscales[iScale] * sr, 1*sr, cv::Scalar(200,200,200), -1, CV_AA, 0);
        //     cv::line(disp, pred_point * fscales[iScale] * sr, prev_point * fscales[iScale] * sr,
        //              cv::Scalar(200,200,200),1, CV_AA, 0);
        //   }
        // }
        
        // 3b. Could not add pred. point to track, remove track
        if (!added) {
          // Remove track from list
          iTrack = tracks.erase(iTrack);
          // iTrack->status = Track::LOST; ++iTrack;
          continue;
        }

        ++iTrack;
      } // Done with OF for each track
    } // Done with OF for each scale

    // Prepare for recovery of lost tracks
    // PrepareRecovery();
    
    
    profiler_.leave("match()");
  }
  
  // OPT2: Add features
  // Add else statement to only add once (for test purposes)
  // else 
  {
    assert(c_points.size() == xyScaleTracks.size());

    profiler_.enter("addFeatures()");
    // 1. Add features at each scale if region not occupied
    // std::vector<std::vector<fsvision::Feature3D> > c_features(scale_num);
    std::vector<std::vector<cv::Mat> > c_desc(scale_num);
    std::vector<std::vector<cv::Mat> > c_mipmap(scale_num);
    std::vector<std::vector<cv::Point2f> > c_points_added(scale_num);

    for(int iScale = 0; iScale < scale_num; iScale++) {
      std::vector<Track>& tracks = xyScaleTracks[iScale];

      // cv::imshow("occ_mask" + std::to_string(iScale), occ_mask[iScale]);
      
      const cv::Mat1b& mask = occ_mask[iScale];
      for(int j=0; j<c_points[iScale].size(); j++) { 
        // Check occupancy
        if (mask(c_points[iScale][j]))
          c_points_added[iScale].push_back(c_points[iScale][j]);

        // // Add yellow features
        // if (iScale == 0) {
        //   if (mask(c_points[iScale][j]))
        //     cv::circle(disp, c_points[iScale][j] * fscales[iScale] * sr, 1*sr, cv::Scalar(0,240,240), -1, CV_AA, 0);
        // }



      }
    }

    // 2. Extract descriptors and add features to track
    for(int iScale = 0; iScale < scale_num; iScale++) {
      const int& width = grey_pyr[iScale].cols;
      const int& height = grey_pyr[iScale].rows;

      // 2a. Extract features from pyramid
      ExtractFeatures(c_points_added[iScale], c_desc[iScale], c_mipmap[iScale], iScale);

      // 2b. Add features to track
      std::vector<Track>& tracks = xyScaleTracks[iScale];
      for (int j=0; j<c_points_added[iScale].size(); j++) {
        // Don't add if no features are extracted (check ExtractFeatures)
        if (c_points_added[iScale][j].x < 0 || c_points_added[iScale][j].y < 0 || 
            c_points_added[iScale][j].x >= width || c_points_added[iScale][j].y >= height)
          continue;
        tracks.push_back(Track(nextTrackID(),
                               c_points_added[iScale][j], c_desc[iScale][j], c_mipmap[iScale][j]));
      }

    }
    profiler_.leave("addFeatures()");
  }

  // // Only if no new tracks are added
  // // Check if all tracks are same length
  // for (int iScale=0; iScale < scale_num; iScale++) {
  //   const std::vector<Track>& tracks = xyScaleTracks[iScale];
  //   for (int j=1; j<tracks.size(); j++) {
  //     assert(tracks[iScale][j-1].point.size() == tracks[iScale][j].point.size());
  //   }
  // }

  
  profiler_.enter("propagateData()");
  // Done with processing
  // Copy frames to prev. frames
  if (prev_image.empty()) prev_image.create(image.size(), CV_8UC3);
  image.copyTo(prev_image);

  if (prev_grey.empty()) prev_grey.create(image.size(), CV_8UC1);
  grey.copyTo(prev_grey);
  
  for(int i = 0; i < scale_num; i++) {
    grey_pyr[i].copyTo(prev_grey_pyr[i]);
    poly_pyr[i].copyTo(prev_poly_pyr[i]);
  }
  profiler_.leave("propagateData()");
  frame_num++;

  // Plot sparse features
  cv::Mat sparse = image.clone();
  for(int iScale = 0; iScale < scale_num; iScale++) { 
    for (int j=0; j<c_points[iScale].size(); j++) { 
      cv::circle(sparse, c_points[iScale][j] * fscales[iScale],
                 MIN_DISTANCE * fscales[iScale], colors[iScale%8], 1, CV_AA);
      cv::circle(sparse, c_points[iScale][j] * fscales[iScale],
                 MIN_DISTANCE * fscales[iScale] * 1.2 / 9 * 2, cv::Scalar(100,100,100), 1, CV_AA);
            
    }
  }

  // cv::imshow("disp", disp);
  cv::imshow("frame", sparse);
  // cv::imshow("mask", det_mask_full);

  // Track viz
  profiler_.enter("visualize()");
  visualize();
  profiler_.leave("visualize()");

  
  // // For feature at each scale,
  // // check if feat. fits within previous scale's neighborhood
  // cv::Mat enclosed = image.clone();
  // for(int iScale = 0; iScale < scale_num-1; iScale++) {
  //   // Fill mask with ones
  //   const cv::Mat1b& mask = det_mask_combined[iScale+1];

  //   // Check if mask is set to zero (occupied)
  //   float off = MIN_DISTANCE * fscales[iScale];
  //   std::vector<cv::Point2f> offs(4, cv::Point2f(off, off));
  //   offs[0].x *= -1; offs[0].y = 0;
  //   offs[1].x *= 1; offs[1].y = 0; 
  //   offs[2].x = 0; offs[2].y *= -1;
  //   offs[3].x = 0; offs[3].y *= 1;
    
  //   for (int j=0; j<c_points[iScale].size(); j++) {
  //     bool good = true;

  //     cv::Point2f p = c_points[iScale][j] * fscales[iScale];
      
  //     if (p.x <= off || p.y <= off || p.x + off > image.cols || p.y + off > image.rows )
  //       continue;
      
  //     for (int k=0; k<offs.size(); k++)  { 
  //       good &= !(mask(p + offs[k]));
  //       cv::circle(enclosed, (p + offs[k]),
  //                  1, // MIN_DISTANCE * fscales[iScale],
  //                  cv::Scalar(0,255,0), -1, CV_AA);        
  //     }
      
  //     cv::circle(enclosed, p, off,
  //                good ? colors[iScale%8] : colors[iScale%8] * 0.2, 1, CV_AA);
  //   }
  // }
  
  // cv::imshow("enclosed", enclosed);

  
  return;
}

inline bool is_nan(const cv::Vec3f& v) {
  return (v[0] != v[0]);
}
inline bool is_nan(const cv::Point3f& v) {
  return (v.x != v.x);
}

std::vector<fsvision::Feature3D> DenseTrajectories::getStableFeatures() {
  std::vector<fsvision::Feature3D> fpts; 

  // Get surface normals, and cloud for depth
  const cv::Mat3f& cloud = frame_.getCloudRef();
  const cv::Mat3f& normals = frame_.getNormalsRef();
  
  for(int iScale = 0; iScale < scale_num; iScale++) {
    // track feature points in each scale separately
    std::vector<Track>& tracks = xyScaleTracks[iScale];
    for (std::vector<Track>::iterator iTrack = tracks.begin();
         iTrack != tracks.end(); iTrack++) {
      if (!iTrack->point.size()) continue;
      assert(iTrack->status == Track::MATCHED);

      // Set ID, and timestamp
      fsvision::Feature3D fpt(frame_.getTimestamp(), iTrack->id);

      // Set 2D keypoint
      cv::Point2f p(iTrack->point.back() * fscales[iScale]);
      cv::KeyPoint kp(p, MIN_DISTANCE, 0, 1.f, iScale);
      fpt.set_keypoint(kp);

      // Set 3D coord, and normal if not nan
      if (!cloud.empty() && !is_nan(cloud(p))) { 
        if (!normals.empty() && !is_nan(normals(p))) {
          fpt.setFeature3D(cloud(p), normals(p)); // normal, tangent set to NaN
        } else {
          fpt.setFeature3D(cloud(p)); // normal, tangent set to NaN
        }
      }
      
      fpts.push_back(fpt);
    }
  }
  return fpts;
}


// // check whether a trajectory is valid or not
// bool IsValid(std::vector<Point2f>& track,
//              float& mean_x, float& mean_y, float& var_x, float& var_y, float& length)
// {
//   int size = track.size();
//   float norm = 1./size;
//   for(int i = 0; i < size; i++) {
//     mean_x += track[i].x;
//     mean_y += track[i].y;
//   }
//   mean_x *= norm;
//   mean_y *= norm;

//   for(int i = 0; i < size; i++) {
//     float temp_x = track[i].x - mean_x;
//     float temp_y = track[i].y - mean_y;
//     var_x += temp_x*temp_x;
//     var_y += temp_y*temp_y;
//   }
//   var_x *= norm;
//   var_y *= norm;
//   var_x = sqrt(var_x);
//   var_y = sqrt(var_y);

//   // remove static trajectory
//   if(var_x < min_var && var_y < min_var)
//     return false;
//   // remove random trajectory
//   if( var_x > max_var || var_y > max_var )
//     return false;

//   float cur_max = 0;
//   for(int i = 0; i < size-1; i++) {
//     track[i] = track[i+1] - track[i];
//     float temp = sqrt(track[i].x*track[i].x + track[i].y*track[i].y);

//     length += temp;
//     if(temp > cur_max)
//       cur_max = temp;
//   }

//   if(cur_max > max_dis && cur_max > length*0.7)
//     return false;

//   track.pop_back();
//   norm = 1./length;
//   // normalize the trajectory
//   for(int i = 0; i < size-1; i++)
//     track[i] *= norm;

//   return true;
// }

static bool
kpts_sort(const cv::KeyPoint& lkp, const cv::KeyPoint& rkp) {
  return lkp.response > rkp.response;
}


// detect new feature points in an image without overlapping to previous points
void DenseTrajectories::SparseSample(const cv::Mat& grey,
                                     const cv::Mat& mask,
                                     std::vector<Point2f>& points)
{
  // Detect
  std::vector<cv::KeyPoint> kpts;
  detector_->detect( grey, kpts, mask);
        
  // Sort by response, and set 
  points.clear();
  // std::sort(kpts.begin(), kpts.end(), kpts_sort);
  // for (int j=0; j<10 && j<kpts.size(); j++)
  //   std::cerr << kpts[j].response << " ";
  // std::cerr << std::endl;
  cv::KeyPoint::convert(kpts, points);
}


// detect new feature points in an image without overlapping to previous points
void DenseTrajectories::DenseSample(const Mat& grey, std::vector<Point2f>& points,
                                    const double quality, const int min_distance)
{
  int width = grey.cols/min_distance;
  int height = grey.rows/min_distance;

  Mat eig;
  cornerMinEigenVal(grey, eig, 3, 3);

  double maxVal = 0;
  minMaxLoc(eig, 0, &maxVal);
  const double threshold = maxVal*quality;

  std::vector<int> counters(width*height);
  int x_max = min_distance*width;
  int y_max = min_distance*height;

  for(int i = 0; i < points.size(); i++) {
    Point2f point = points[i];
    int x = cvFloor(point.x);
    int y = cvFloor(point.y);

    if(x >= x_max || y >= y_max)
      continue;
    x /= min_distance;
    y /= min_distance;
    counters[y*width+x]++;
  }

  points.clear();
  int index = 0;
  int offset = min_distance/2;
  for(int i = 0; i < height; i++)
    for(int j = 0; j < width; j++, index++) {
      if(counters[index] > 0)
        continue;

      int x = j*min_distance+offset;
      int y = i*min_distance+offset;

      if(eig.at<float>(y, x) > threshold)
        points.push_back(Point2f(float(x), float(y)));
    }
}

void DenseTrajectories::InitPry(const cv::Size& sz, std::vector<float>& scales,
                                std::vector<Size>& sizes) {
  float min_size = std::min<int>(sz.height, sz.width);

  int nlayers = 0;
  while(min_size >= PATCH_SIZE) {
    min_size /= my::scale_stride;
    nlayers++;
  }

  if(nlayers == 0) nlayers = 1; // at least 1 scale 

  scale_num = std::min<int>(scale_num, nlayers);

  scales.resize(scale_num);
  sizes.resize(scale_num);

  scales[0] = 1.;
  sizes[0] = sz; // Size(sz.width, sz.height);

  for(int i = 1; i < scale_num; i++) {
    scales[i] = scales[i-1] * my::scale_stride;
    sizes[i] = Size(cvRound(sz.width/scales[i]), cvRound(sz.height/scales[i]));
  }
}

void DenseTrajectories::BuildPry(const std::vector<Size>& sizes,
                                 const int type,
                                 std::vector<Mat>& grey_pyr) {
  int nlayers = sizes.size();
  grey_pyr.resize(nlayers);

  for(int i = 0; i < nlayers; i++)
    grey_pyr[i].create(sizes[i], type);
}


void DenseTrajectories::DrawTrack(const Track& track, const int iScale, Mat& image) {
  float scale = fscales[iScale];
  cv::Scalar col = (track.status == Track::LOST) ?
      cv::Scalar(200,200,200) : colors[iScale%8];

  for (int j=1; j<track.point.size(); j++) { 
    cv::line(image, track.point[j-1] * scale, track.point[j] * scale,
             col * (0.25  + 0.75 * (j+1.0)/float(track.point.size()+1.0)),
             1, CV_AA, 0);
  }
  // colors[track.id%8]
  cv::circle(image, track.point.back() * scale, 1, cv::Scalar(0,240,0), 1, CV_AA, 0);
}




void
DenseTrajectories::write() { 

}

} // namespace vision
} // namespace fs



// void
// DenseTrajectories::processImage(const cv::Mat& frame, const cv::Mat& mask) { 
    
//   // Init: First frame
//   if(image.empty()) {
//     // Init Image Pyramids
//     InitImagePyramids(frame.size());
    
//     // Keep copy of input
//     frame.copyTo(image);
//     cv::cvtColor(image, prev_grey, CV_BGR2GRAY);

//     // Build Pyramid
//     BuildImagePyramids(prev_grey, prev_grey_pyr); 
    
//     for(int iScale = 0; iScale < scale_num; iScale++) {
//       // Sample and extract BRIEF features
//       cv::Mat desc;
//       std::vector<cv::Point2f> points(0);
//       SparseSample(prev_grey_pyr[iScale], points, desc, QUALITY, MIN_DISTANCE);
      
//       // For each scale
//       // save the feature points
//       std::vector<Track>& tracks = xyScaleTracks[iScale];
//       for(int j=0; j<points.size(); j++)
//         tracks.push_back(Track(nextTrackID(), points[j], desc.row(j)));
//     }

//     // compute polynomial expansion
//     my::FarnebackPolyExpPyr(prev_grey, prev_poly_pyr, fscales, 7, 1.5);

//     frame_num++;
//     return;
//   }

//   // Keep copy of input image
//   init_counter++;
//   frame.copyTo(image);
//   cv::cvtColor(image, grey, CV_BGR2GRAY);

//   // compute optical flow for all scales once
//   my::FarnebackPolyExpPyr(grey, poly_pyr, fscales, 7, 1.5);
//   my::calcOpticalFlowFarneback(prev_poly_pyr, poly_pyr, flow_pyr, 10, 2);

//   // Build pyramids
//   BuildImagePyramids(grey, grey_pyr);
  
//   // 1. Predict features with flow
//   // 2. Extract feature descriptors for each prediction
//   // 3. Validate feature descriptor with track
//   // 4. Prune tracks if features drift
//   // 5. Add new features
//   for(int iScale = 0; iScale < scale_num; iScale++) {
//     int width = grey_pyr[iScale].cols;
//     int height = grey_pyr[iScale].rows;

//     // track feature points in each scale separately
//     std::vector<Track>& tracks = xyScaleTracks[iScale];

//     // 1. Predict points
//     std::vector<cv::Point2f> next_points(tracks.size());
//     for (int j=0; j<tracks.size(); j++) { 
//       const cv::Point2f& prev_point = tracks[j].point.back();
//       int x = std::min<int>(std::max<int>(cvRound(prev_point.x), 0), width-1);
//       int y = std::min<int>(std::max<int>(cvRound(prev_point.y), 0), height-1);

//       cv::Point2f point(prev_point + flow_pyr[iScale].ptr<cv::Point2f>(y)[x]);
//       // nt.y = prev_point.y + flow_pyr[iScale].ptr<float>(y)[2*x+1];

//       next_points[j] = point;
//     }
        
//     // 2. Extract BRIEF features
//     std::vector<cv::Mat> next_desc(next_points.size(), cv::Mat());
//     std::vector<cv::KeyPoint> next_kpts(next_points.size());
//     for (int j=0; j<next_points.size(); j++) { 
//       next_kpts[j].class_id = j;
//       next_kpts[j].pt = next_points[j];
//       next_kpts[j].size = 1.f;
//     }

//     // Extract description
//     cv::Mat _desc;
//     extractor_->compute(grey_pyr[iScale], next_kpts, _desc);

//     // Re-init
//     next_points = std::vector<cv::Point2f>(tracks.size(), cv::Point2f(-1,-1));
//     for (int j=0; j<next_kpts.size(); j++) {
//       int idx = next_kpts[j].class_id;
//       next_points[idx] = next_kpts[j].pt;
//       next_desc[idx] = _desc.row(j);
//     }

//     // Track feature points in each scale
//     int pts_index = 0;
//     for (std::vector<Track>::iterator iTrack = tracks.begin();
//          iTrack != tracks.end(); pts_index++) {
//       cv::Point2f prev_point = iTrack->point.back();
//       const cv::Point2f& point = next_points[pts_index];
//       if(point.x <= 0 || point.x >= width || point.y <= 0 || point.y >= height) {
//         iTrack = tracks.erase(iTrack);
//         continue;
//       }
       
//       // get the descriptors for the feature point
//       bool added = iTrack->addPoint(point, next_desc[pts_index]);
//       if (!added) {
//         iTrack = tracks.erase(iTrack);
//         continue;
//       }

//       // draw the trajectories at the first scale
//       if(show_track == 1 && iScale == 0)
//         DrawTrack(iTrack->point, iTrack->point.size()-1, iTrack->id, fscales[iScale], image);
        
//       // if the trajectory achieves the maximal length
//       if(iTrack->point.size() >= TRACK_LENGTH) {
//         // iTrack = tracks.erase(iTrack);
//         // continue;
//       }
//       ++iTrack;
//     }

//     if(init_counter != INIT_GAP)
//       continue;
     
//     // detect new feature points every initGap frames
//     std::vector<cv::Point2f> points(0);
//     for(std::vector<Track>::iterator iTrack = tracks.begin(); iTrack != tracks.end(); iTrack++)
//       points.push_back(iTrack->point.back());
      
//     // Sample and extract BRIEF features
//     cv::Mat desc;
//     SparseSample(grey_pyr[iScale], points, desc, QUALITY, MIN_DISTANCE);
      
//     // save the new feature points
//     for(int i = 0; i < points.size(); i++)
//       tracks.push_back(Track(nextTrackID(), points[i], desc.row(i)));
//   }

//   init_counter = 0;
//   // image.copyTo(prev_image);
//   grey.copyTo(prev_grey);
  
//   for(int i = 0; i < scale_num; i++) {
//     grey_pyr[i].copyTo(prev_grey_pyr[i]);
//     poly_pyr[i].copyTo(prev_poly_pyr[i]);
//   }

//   frame_num++;

//   if( show_track == 1 ) {
//     cv::imshow("frame", frame);
//     cv::imshow( "DenseTrack", image);
//   }

//   return;
// }

