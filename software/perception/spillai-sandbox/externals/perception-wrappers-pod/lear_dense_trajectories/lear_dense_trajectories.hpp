#define DENSE_SAMPLING 1

// features, frame_utils, opencv_utils include
#include <features3d/feature_types.hpp>
#include <pcl-utils/frame_utils.hpp>
#include <perception_opencv_utils/opencv_utils.hpp>

// lear dense trajectories includes
#include <fs_perception_wrappers/lear_dense_trajectories/DenseTrack.h>
#include <fs_perception_wrappers/lear_dense_trajectories/Initialize.h>
#include <fs_perception_wrappers/lear_dense_trajectories/Descriptors.h>
#include <fs_perception_wrappers/lear_dense_trajectories/OpticalFlow.h>

#include <time.h>

namespace fs { namespace vision { 
class LearDenseTrajectories { 

 public:
  cv::Mat image, prev_image, grey, prev_grey;

  std::vector<float> fscales;
  std::vector<cv::Size> sizes;

  std::vector<cv::Mat> prev_grey_pyr, grey_pyr, flow_pyr;
  std::vector<cv::Mat> prev_poly_pyr, poly_pyr; // for optical flow
  std::vector<std::list<Track> > xyScaleTracks;

  int frame_num;
  int init_counter;
  int show_track;
  int64_t track_id;
  
  // Tracker info
  TrackInfo trackInfo;
  SeqInfo seqInfo;
  
  // Descriptor info
  DescInfo hogInfo, hofInfo, mbhInfo;

public: 
  LearDenseTrajectories(); 
  virtual ~LearDenseTrajectories();
  void processFrame(const opencv_utils::Frame& frame);
  void processImage(const cv::Mat& frame, const cv::Mat& mask=cv::Mat()); 
  void draw(cv::Mat& frame);
  void write(); 

  std::vector<fsvision::Feature3D> getStableFeatures();
  
  // Track IDing 
  int64_t nextTrackID() { return ++track_id; } 
  int64_t trackID() { return track_id;  }
  bool resetTrackID() { track_id = 1; return true; }

};
    
} // namespace vision
} // namespace fs
