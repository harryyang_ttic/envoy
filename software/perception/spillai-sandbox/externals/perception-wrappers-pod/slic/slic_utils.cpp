#include "slic_utils.hpp"

namespace fs { namespace vision { 

static std::vector<cv::Vec3b> label_colors; 

int slic(const cv::Mat& img, cv::Mat& labels, 
         const int& var, const double& compactness, 
         const int& op_type, const int colorconv) { 

  //----------------------------------
  // Perform SLIC based on type
  //----------------------------------
  SLICGeneric slic; 
    
  if (op_type == SLIC_FOR_GIVEN_SUPERPIXEL_SIZE) {
    // var ~ superpixelsize
    labels = 
        slic.forGivenSuperpixelSize
        (img, var, compactness, colorconv);
  } else if (op_type == SLIC_FOR_GIVEN_NUMBER_OF_SUPERPIXELS) { 
    // var ~ K
    labels = 
        slic.forGivenNumberOfSuperpixels
        (img, var, compactness, colorconv);
  } else { 
    std::cerr << "Unknown superpixels slic op_type" << std::endl;
  }

  return slic.numlabels;
}

cv::Mat unique_label_img(const cv::Mat& labels) {
    
  // Check 
  int nLabels = 20;
  if (!label_colors.size()) { 
    label_colors = std::vector<cv::Vec3b>(20);
    label_colors[0] = cv::Vec3b(0, 0, 0);//background

    for(int label = 1; label < nLabels; ++label){
      label_colors[label] = cv::Vec3b( (rand()&255), (rand()&255), (rand()&255) );
    }
  }

  cv::Mat dst(labels.size(), CV_8UC3);
  for(int r = 0; r < dst.rows; ++r){
    for(int c = 0; c < dst.cols; ++c){
      int label = labels.at<int>(r, c);
      cv::Vec3b &pixel = dst.at<cv::Vec3b>(r, c);
      pixel = label_colors[label % nLabels];
    }
  }

  return dst;
}

cv::Mat extract_contour_mask_from_labels(const cv::Mat& labels) {
    
  cv::Mat1b dst = cv::Mat1b::zeros(labels.size());
  for(int r = 1; r < labels.rows-1; ++r){
    for(int c = 1; c < labels.cols-1; ++c){
      int l = labels.at<int>(r,c);

      bool good = true;
      for (int j=-1; j<=1; j++) { 
        for (int k=-1; k<=1; k++)
          if (labels.at<int>(r+j,c+k) != l) { 
            good = false; break; 
          }
        if (!good) break;
      }

      if (!good) dst(r,c) = 255;
    }
  }

  return dst;
}


cv::Mat draw_contours_from_labels(const cv::Mat& img, const cv::Mat& labels) {
    
  // Check
  std::cerr << img.size() << " " << labels.size() << std::endl;
  assert(img.size() == labels.size());

  cv::Mat dst = img.clone();
  for(int r = 1; r < img.rows-1; ++r){
    for(int c = 1; c < img.cols-1; ++c){
      int l = labels.at<int>(r,c);

      bool good = true;
      for (int j=-1; j<=1; j++) { 
        for (int k=-1; k<=1; k++)
          if (labels.at<int>(r+j,c+k) != l) { 
            good = false; break; 
          }
        if (!good) break;
      }

      if (!good) dst.at<cv::Vec3b>(r,c) = cv::Vec3b(0,0,255);
    }
  }
    
  // Alpha blending
  dst = dst * 0.5 + img * 0.5;

  return dst;
}

void mu_stddev_from_labels(const cv::Mat& data, const cv::Mat& labels, 
                           const int nlabels, cv::Mat& mu, cv::Mat& stddev) {
        
  if (!nlabels) return;

  int ch = data.channels();

  cv::Mat datad;
  switch (ch) { 
    case 1: 
      data.convertTo(datad, CV_64FC1);
      break;
    case 2: 
      data.convertTo(datad, CV_64FC2);
      break;
    case 3: 
      data.convertTo(datad, CV_64FC3);
      break;
    default: 
      std::cerr << "Unsupported type" << std::endl;
      assert(0);
      break;
  }

  // Create matrices
  std::vector<int> counts(nlabels, 0);
  mu = cv::Mat::zeros(nlabels, 1, datad.type()); 
  stddev = cv::Mat::zeros(nlabels, 1, datad.type()); 

  for (int y=0; y<datad.rows; y++) { 
    for (int x=0; x<datad.cols; x++) {
      int l = labels.at<int>(y,x);
      const double* valp = datad.ptr<double>(y,x);
      double* mup = mu.ptr<double>(l);
      for (int c=0; c<ch; c++)
        mup[c] += valp[c];
      counts[l] += 1;
    }
  }

  for (int j=0; j<datad.rows; j++) 
    if (counts[j]) datad.row(j) *= 1.f/counts[j];

}
    
cv::Mat apply_label_values_to_img(const cv::Mat3f& data, const cv::Mat& labels) { 
  cv::Mat3f out(labels.size(), cv::Vec3f(0,0,0));
  for (int y=0; y<labels.rows; y++) { 
    for (int x=0; x<labels.cols; x++) {
      int l = labels.at<int>(y,x);
      out(y,x) = data(l,0);
    }
  }
  return out;
}

std::map<int, std::vector<int> >
build_node_graph(const cv::Mat& labels) {

  std::map<int, std::set<int> > nn_labels_set;
  for (int i=1; i<labels.rows-1; i++) {
    for (int j=1; j<labels.cols-1; j++) {
      const int& label = labels.at<int>(i,j);
      
      for(int y = i-1; y <= i+1; y++)
        for(int x = j-1; x <= j+1; x++) {
          if (y == 0 && x == 0) continue;

          int nn = labels.at<int>(y,x);
          
          // If nn is not the same as label, then add
          if (nn != label) { 
            nn_labels_set[label].insert(nn);
            nn_labels_set[nn].insert(label);
          }

        }
    }
  }

  // Build map
  std::map<int, std::vector<int> > nn_labels;
  for (std::map<int, std::set<int> >::iterator it = nn_labels_set.begin();
       it != nn_labels_set.end(); it++) {
    std::vector<int>& vec = nn_labels[it->first];
    vec.insert(vec.end(), it->second.begin(), it->second.end());
  }
    
  return nn_labels;
}

std::map<int, float>
mean_from_labels(const cv::Mat& data, const cv::Mat& labels, const cv::Mat& mask) {
  assert(data.size() == labels.size());

  cv::Mat dataf;
  if (data.type() != CV_32F)
    data.convertTo(dataf, CV_32F);
  else
    dataf = data;

  std::map<int, float > mean;
  std::map<int, int > count;
  const int* plabel = (const int*) labels.data;
  const float* pval = (const float*) dataf.data;
  const uchar* pmask = (const uchar*) mask.data;
  for (int j=0; j<labels.cols * labels.rows; j++) {
    if (pmask && !pmask[j]) continue; // if mask exists, and is not true
    int l = plabel[j];
    if (mean.find(l) == mean.end()) { 
      mean[l] = pval[j];
      count[l] = 1; 
    } else { 
      mean[l] += pval[j];
      count[l] += 1;
    }
  }

  for (std::map<int, int>::iterator it = count.begin(); it != count.end(); it++) {
    assert(mean.find(it->first) != mean.end());
    mean[it->first] = mean[it->first] * 1.f / count[it->first];
  }

  return mean;
}


// std::map<int, float>
// mean_from_labels(const cv::Mat& data, const cv::Mat& labels, const cv::Mat& mask) {
//   assert(data.size() == labels.size());

//   cv::Mat dataf;
//   if (data.type() != CV_32F)
//     data.convertTo(dataf, CV_32F);
//   else
//     dataf = data;

//   std::map<int, float > mean;
//   std::map<int, int > count;
//   const int* plabel = (const int*) labels.data;
//   const float* pval = (const float*) dataf.data;
//   const uchar* pmask = (const uchar*) mask.data;
//   for (int j=0; j<labels.cols * labels.rows; j++) {
//     if (pmask && !pmask[j]) continue; // if mask exists, and is not true
//     int l = plabel[j];
//     if (mean.find(l) == mean.end()) { 
//       mean[l] = pval[j];
//       count[l] = 1; 
//     } else { 
//       mean[l] += pval[j];
//       count[l] += 1;
//     }
//   }

//   for (std::map<int, int>::iterator it = count.begin(); it != count.end(); it++) {
//     assert(mean.find(it->first) != mean.end());
//     mean[it->first] = mean[it->first] * 1.f / count[it->first];
//   }

//   return mean;
// }


std::map<int, float>
median_from_labels(const cv::Mat& data, const cv::Mat& labels, const cv::Mat& mask) {
  assert(data.size() == labels.size());

  cv::Mat dataf;
  if (data.type() != CV_32F)
    data.convertTo(dataf, CV_32F);
  else
    dataf = data;

  // Build vectors
  std::map<int, std::vector<float> > acc;
  const int* plabel = (const int*) labels.data;
  const float* pval = (const float*) dataf.data;
  const uchar* pmask = (const uchar*) mask.data;
  for (int j=0; j<labels.cols * labels.rows; j++) {
    if (pmask && !pmask[j]) continue; // if mask exists, and is not true
    int l = plabel[j];
    acc[l].push_back(pval[j]);
  }

  // Find median
  std::map<int, float > median;  
  for (std::map<int, std::vector<float> >::iterator it = acc.begin(); it != acc.end(); it++) {
    std::vector<float> acc_nth = it->second;
    std::nth_element(acc_nth.begin(), acc_nth.begin() + acc_nth.size() / 2, acc_nth.end()); 
    median[it->first] = acc_nth[acc_nth.size() / 2];
  }

  return median;
}

std::map<int, std::vector<float> >
attrs_from_labels(const cv::Mat& data, const cv::Mat& labels, const cv::Mat& mask) {
  // const std::vector<std::string>& attrs, 
  assert(labels.type() == CV_32S);
  // assert(data.type() == CV_32F);

  std::vector<cv::Mat> channels;
  cv::split(data, channels);

  std::map<int, std::vector<float> > comps;
  for (int j=0; j<channels.size(); j++) {
    std::map<int, float> comp = mean_from_labels(channels[j], labels, mask);
    for (std::map<int, float>::iterator it = comp.begin(); it != comp.end(); it++)
      comps[it->first].push_back(it->second);
  }

  // check
  for (std::map<int, std::vector<float> >::iterator it = comps.begin(); it != comps.end(); it++)
    assert(it->second.size() == 3);

  return comps;
}

} // namespace vision
} // namespace fs




// int seeds(const cv::Mat& img, cv::Mat& labels, 
//           const int& var, const double& compactness, 
//           const slic_type& type, const int colorconv) { 
    
//     //----------------------------------
//     // Allocate
//     //----------------------------------
//     unsigned int* imgp = new unsigned int[img.rows * img.cols];
//     opencv_utils::convert_to_argb(img, imgp);

//     //----------------------------------
//     // Perform SEEDS on the image buffer
//     //----------------------------------
//     int NR_BINS = 3; // Number of bins in each histogram channel
//     SEEDS seeds_(img.cols, img.rows, img.channels(), NR_BINS, 0);
    
//     // NOTE: the following values are defined for images from the BSD300 or BSD500 data set.
//     // If the input image size differs from 480x320, the following values might no longer be 
//     // accurate.
//     // For more info on how to select the superpixel sizes, please refer to README.TXT.
//     int seed_width = 3; int seed_height = 4; int nr_levels = 4;
//     // if (width >= height)
//     //     {
//     //         if (nr_superpixels == 600) {seed_width = 2; seed_height = 2; nr_levels = 4;}
//     //         if (nr_superpixels == 400) {seed_width = 3; seed_height = 2; nr_levels = 4;}
//     //         if (nr_superpixels == 266) {seed_width = 3; seed_height = 3; nr_levels = 4;}
//     //         if (nr_superpixels == 200) {seed_width = 3; seed_height = 4; nr_levels = 4;}
//     //         if (nr_superpixels == 150) {seed_width = 2; seed_height = 2; nr_levels = 5;}
//     //         if (nr_superpixels == 100) {seed_width = 3; seed_height = 2; nr_levels = 5;}
//     //         if (nr_superpixels == 50)  {seed_width = 3; seed_height = 4; nr_levels = 5;}
//     //         if (nr_superpixels == 25)  {seed_width = 3; seed_height = 2; nr_levels = 6;}
//     //         if (nr_superpixels == 17)  {seed_width = 3; seed_height = 3; nr_levels = 6;}
//     //         if (nr_superpixels == 12)  {seed_width = 3; seed_height = 4; nr_levels = 6;}
//     //         if (nr_superpixels == 9)  {seed_width = 2; seed_height = 2; nr_levels = 7;}
//     //         if (nr_superpixels == 6)  {seed_width = 3; seed_height = 2; nr_levels = 7;}
//     //     }
//     // else
//     //     {
//     //         if (nr_superpixels == 600) {seed_width = 2; seed_height = 2; nr_levels = 4;}
//     //         if (nr_superpixels == 400) {seed_width = 2; seed_height = 3; nr_levels = 4;}
//     //         if (nr_superpixels == 266) {seed_width = 3; seed_height = 3; nr_levels = 4;}
//     //         if (nr_superpixels == 200) {seed_width = 4; seed_height = 3; nr_levels = 4;}
//     //         if (nr_superpixels == 150) {seed_width = 2; seed_height = 2; nr_levels = 5;}
//     //         if (nr_superpixels == 100) {seed_width = 2; seed_height = 3; nr_levels = 5;}
//     //         if (nr_superpixels == 50)  {seed_width = 4; seed_height = 3; nr_levels = 5;}
//     //         if (nr_superpixels == 25)  {seed_width = 2; seed_height = 3; nr_levels = 6;}
//     //         if (nr_superpixels == 17)  {seed_width = 3; seed_height = 3; nr_levels = 6;}
//     //         if (nr_superpixels == 12)  {seed_width = 4; seed_height = 3; nr_levels = 6;}
//     //         if (nr_superpixels == 9)  {seed_width = 2; seed_height = 2; nr_levels = 7;}
//     //         if (nr_superpixels == 6)  {seed_width = 2; seed_height = 3; nr_levels = 7;}
//     //     }
//     seeds_.initialize(imgp, seed_width, seed_height, nr_levels);
//     seeds_.iterate();

//     // //----------------------------------
//     // // Perform SLIC based on type
//     // //----------------------------------
//     // if (type == SLIC_FOR_GIVEN_SUPERPIXEL_SIZE) {
//     //     // var ~ superpixelsize
//     //     slic.DoSuperpixelSegmentation_ForGivenSuperpixelSize(imgp, img.cols, img.rows, 
//     //                                                          var, compactness);
//     // } else if (type == SLIC_FOR_GIVEN_NUMBER_OF_SUPERPIXELS) { 
//     //     // var ~ K
//     //     slic.DoSuperpixelSegmentation_ForGivenNumberOfSuperpixels(imgp, img.cols, img.rows, 
//     //                                                               var, compactness);
//     // } else { 
//     //     std::cerr << "Unknown superpixels slic type" << std::endl;
//     // }

//     //----------------------------------
//     // Allocate labels
//     //----------------------------------
//     labels = cv::Mat_<int>(img.rows, img.cols, (int*) seeds_.labels[nr_levels-1]).clone();

//     //----------------------------------
//     // Clean up
//     //----------------------------------
//     if(imgp) delete [] imgp;

//     return 0;
// }
