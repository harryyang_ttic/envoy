#include "slic_generic.hpp"

namespace fs { namespace vision {

enum slic_op_type { 
  SLIC_FOR_GIVEN_SUPERPIXEL_SIZE=0, 
  SLIC_FOR_GIVEN_NUMBER_OF_SUPERPIXELS
};
  
// var is either (superpixelsize OR K=number of superpixels)
int slic(const cv::Mat& img, cv::Mat& labels, 
         const int& var, const double& compactness, 
         const int& op_type = SLIC_FOR_GIVEN_SUPERPIXEL_SIZE, 
         const int colorconv = cv::COLOR_BGR2Lab);

// // Note the colorconv default is BGR2Lab
// PyObject* 
// slic(PyObject *img, const double var, const double& compactness,
//      const int& op_type = SLIC_FOR_GIVEN_SUPERPIXEL_SIZE,
//      const int colorconv = cv::COLOR_RGB2Lab);

int slic(int* buff, int width, int height, int type, int* labels, 
         int var, double compactness, int op_type);

cv::Mat unique_label_img(const cv::Mat& labels);
cv::Mat extract_contour_mask_from_labels(const cv::Mat& labels);
cv::Mat draw_contours_from_labels(const cv::Mat& img, const cv::Mat& labels);
void mu_stddev_from_labels(const cv::Mat& data, const cv::Mat& labels, 
                           const int nlabels, cv::Mat& mu, cv::Mat& stddev);

cv::Mat apply_label_values_to_img(const cv::Mat3f& data, const cv::Mat& labels);

std::map<int, std::vector<int> >
build_node_graph(const cv::Mat& labels);

std::map<int, float>
mean_from_labels(const cv::Mat& data, const cv::Mat& labels, const cv::Mat& mask = cv::Mat());

std::map<int, float>
median_from_labels(const cv::Mat& data, const cv::Mat& labels, const cv::Mat& mask = cv::Mat());

std::map<int, std::vector<float> >
attrs_from_labels(const cv::Mat& data, const cv::Mat& labels, const cv::Mat& mask = cv::Mat()); 

} // namespace vision
} // namespace fs
