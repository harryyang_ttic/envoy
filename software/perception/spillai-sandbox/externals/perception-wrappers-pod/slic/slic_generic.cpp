// SLIC.cpp: implementation of the SLIC class.
//
// Copyright (C) Radhakrishna Achanta 2012
// All rights reserved
// Email: firstname.lastname@epfl.ch
//////////////////////////////////////////////////////////////////////
//#include "stdafx.h"
#include <cfloat>
#include <cmath>
#include <iostream>
#include <fstream>
#include "slic_generic.hpp"
// #include "cv2.hpp"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace fs { namespace vision {

SLICGeneric::SLICGeneric()
{
    mu_ch1 = std::vector<double> ();
    mu_ch2 = std::vector<double> ();
    mu_ch3 = std::vector<double> ();
    cluster_count = std::vector<double> ();

    klabel = std::vector<int>();
    numlabels = 0;
    // m_distance = EuclideanDistance<double>(); 
}

SLICGeneric::~SLICGeneric()
{
}

void 
SLICGeneric::reset() {

  mu_ch1 = std::vector<double> ();
  mu_ch2 = std::vector<double> ();
  mu_ch3 = std::vector<double> ();
  cluster_count = std::vector<double> ();


  m_mask = cv::Mat();
  m_val = cv::Mat();
  
  kseeds_val = cv::Mat();
  kseeds_pos = cv::Mat();
  klabel = std::vector<int>();
  numlabels = 0;

}

//==============================================================================
///	DetectEdges
//==============================================================================
void SLICGeneric::DetectEdges(const cv::Mat& val, std::vector<double>& edges)
{
    int width = val.cols, 
        height = val.rows;
	int sz = val.cols * val.rows;

	edges.resize(sz,0);
	for( int j = 1; j < val.rows-1; j++ ) {
            for( int k = 1; k < val.cols-1; k++ ) {
                const double* valpl = val.ptr<double>(j,k-1);
                const double* valpr = val.ptr<double>(j,k+1);

                const double* valpt = val.ptr<double>(j-1,k);
                const double* valpb = val.ptr<double>(j+1,k);
                
                // double sum_dx = m_distance(valpl,valpr), 
                //     sum_dy = m_distance(valpl,valpr);
                double sum_dx = 0, sum_dy = 0;
                for (int ch=0; ch<m_channels; ch++) { 
                    sum_dx += pow(valpl[ch]-valpr[ch],2);
                    sum_dy += pow(valpt[ch]-valpb[ch],2);
                }

                edges[j*val.cols+k] = pow(sum_dx,sum_dx) + pow(sum_dy,sum_dy);
                // edges[i] = fabs(dx) + fabs(dy); // EDITED 
            }
	}
}

//===========================================================================
///	PerturbSeeds
//===========================================================================
void SLICGeneric::PerturbSeeds(
        cv::Mat& kseedsval, 
        cv::Mat& kseedspos, 
        const std::vector<double>&                   edges)
{
	const int dx8[8] = {-1, -1,  0,  1, 1, 1, 0, -1};
	const int dy8[8] = { 0, -1, -1, -1, 0, 1, 1,  1};
	
	int numseeds = kseedsval.cols * kseedsval.rows;

	for( int n = 0; n < numseeds; n++ ) { 

            float* kseedsposp = kseedspos.ptr<float>(n);

            int ox = kseedsposp[0];//original x
            int oy = kseedsposp[1];//original y
            int oind = oy*m_width + ox;

            int storeind = oind;
            for( int i = 0; i < 8; i++ ) {
                int nx = ox+dx8[i];//new x
                int ny = oy+dy8[i];//new y

                if( nx >= 0 && nx < m_width && ny >= 0 && ny < m_height) {
                    int nind = ny*m_width + nx;
                    if( edges[nind] < edges[storeind]) {
                        storeind = nind;
                    }
                }
            }
            if(storeind != oind) {
                int x = storeind%m_width, y = storeind/m_width;
                double* kseedsvalp = kseedsval.ptr<double>(n);
                const double* valp = m_val.ptr<double>(y,x);

                kseedsposp[0] = x; kseedsposp[1] = y;

                for (int ch=0; ch<m_channels; ch++)
                    kseedsvalp[ch] = valp[ch]; 
            }
	}
}


//===========================================================================
///	GetLABXYSeeds_ForGivenStepSize
///
/// The k seed values are taken as uniform spatial pixel samples.
//===========================================================================
void SLICGeneric::GetVALXYSeeds_ForGivenStepSize(
    cv::Mat& kseedsval, 
    cv::Mat& kseedspos, 
    const int&					STEP,
    const bool&					perturbseeds,
    const vector<double>&       edgemag)
{
    const bool hexgrid = false;
    int numseeds(0);
    int n(0);

    //int xstrips = m_width/STEP;
    //int ystrips = m_height/STEP;
    int xstrips = (0.5+double(m_width)/double(STEP));
    int ystrips = (0.5+double(m_height)/double(STEP));

    int xerr = m_width  - STEP*xstrips;if(xerr < 0){xstrips--;xerr = m_width - STEP*xstrips;}
    int yerr = m_height - STEP*ystrips;if(yerr < 0){ystrips--;yerr = m_height- STEP*ystrips;}

    double xerrperstrip = double(xerr)/double(xstrips);
    double yerrperstrip = double(yerr)/double(ystrips);

    int xoff = STEP/2;
    int yoff = STEP/2;
    //-------------------------
    numseeds = xstrips*ystrips;
    //-------------------------
    kseedsval.create(numseeds, 1, m_depth);
    kseedspos.create(numseeds, 1, CV_32FC2);

    for( int y = 0; y < ystrips; y++ ) {
        int ye = y*yerrperstrip;

        for( int x = 0; x < xstrips; x++ ) {
            int xe = x*xerrperstrip;
            int seedx = (x*STEP+xoff+xe);

            if(hexgrid) { 
                seedx = x*STEP+(xoff<<(y&0x1))+xe; 
                seedx = min(m_width-1,seedx); 
            } //for hex grid sampling
            int seedy = (y*STEP+yoff+ye);

            double* kseedsvalp = kseedsval.ptr<double>(n);
            const double* valp = m_val.ptr<double>(seedy,seedx);
            if (valp[0] != valp[0]) { 
                assert(0);
            }
            for (int ch=0; ch<m_channels; ch++)
                kseedsvalp[ch] = valp[ch];

            float* kseedsposp = kseedspos.ptr<float>(n);
            kseedsposp[0] = seedx;
            kseedsposp[1] = seedy;
            n++;
        }
    }

    if(perturbseeds) {
        PerturbSeeds(kseedsval, kseedspos, edgemag);
    }
}

//===========================================================================
///	PerformSuperpixelSLIC
///
///	Performs k mean segmentation. It is fast because it looks locally, not
/// over the entire image.
//===========================================================================
void SLICGeneric::PerformSuperpixelSLIC(
        cv::Mat& kseedsval, 
        cv::Mat& kseedspos, 
        std::vector<int>&					klabels,
        const int&				STEP,
        const std::vector<double>&                   edgemag,
	const double&				M)
{
	int sz = m_width*m_height;
	const int numk = kseedsval.cols*kseedsval.rows;
	//----------------
	int offset = STEP;
        //if(STEP < 8) offset = STEP*1.5;//to prevent a crash due to a very small step size
	//----------------
	
	std::vector<double> clustersize(numk, 0);
	std::vector<double> inv(numk, 0);//to store 1/clustersize[k] values

        cv::Mat sigmaval(numk,1,m_depth);
        cv::Mat sigmapos(numk,1,CV_32FC2); // FLAG
        sigmapos = cv::Scalar::all(0);

	std::vector<double> distvec(sz, DBL_MAX);

	double invwt = 1.0/((STEP/M)*(STEP/M));


	int x1, y1, x2, y2;
	double l, a, b;
	double dist;
	double distxy;
	for( int itr = 0; itr < 10; itr++ ) {
            distvec.assign(sz, DBL_MAX);
            for( int n = 0; n < numk; n++ ) {

                const double* kseedsvalp = kseedsval.ptr<double>(n);
                const float* kseedsposp = kseedspos.ptr<float>(n);
                y1 = max(0.0,		double(kseedsposp[1]-offset));
                y2 = min((double)m_height,	double(kseedsposp[1]+offset));
                x1 = max(0.0,		double(kseedsposp[0]-offset));
                x2 = min((double)m_width,	double(kseedsposp[0]+offset));


                for( int y = y1; y < y2; y++ ) {
                    for( int x = x1; x < x2; x++ ) {
                        int i = y*m_width + x;

                        const double* valp = m_val.ptr<double>(y,x);

                        // Check mask if valid
                        if (!m_mask.at<uchar>(y,x)) continue;

                        double sum_dist = 0; 
                        for (int ch=0; ch<m_channels; ch++) {
                            sum_dist += pow(valp[ch] - kseedsvalp[ch], 2);
                        }
                        dist = sum_dist;

                        distxy = pow(x-kseedsposp[0],2) + pow(y-kseedsposp[1],2);
					
                        //------------------------------------------------------------------------
                        dist += distxy*invwt;//dist = sqrt(dist) + sqrt(distxy*invwt);//this is more exact
                        //------------------------------------------------------------------------
                        if( dist < distvec[i] ) {
                            distvec[i] = dist;
                            klabels[i]  = n;
                        }
                    }
                }
            }
            //-----------------------------------------------------------------
            // Recalculate the centroid and store in the seed values
            //-----------------------------------------------------------------
            //instead of reassigning memory on each iteration, just reset.

            sigmaval = 0;
            sigmapos = 0;
            clustersize.assign(numk, 0);
            //------------------------------------
            // edgesum.assign(numk, 0);
            //------------------------------------

            // Scoped
            {
                int ind(0);
		for( int r = 0; r < m_height; r++ ) {
                    for( int c = 0; c < m_width; c++ ) {
                        float* sigmaposp = sigmapos.ptr<float>(klabels[ind],0);

                        double* sigmavalp = sigmaval.ptr<double>(klabels[ind],0);
                        const double* valp = m_val.ptr<double>(r,c);

                        // Check mask if valid
                        if (m_mask.at<uchar>(r,c)) { 
                            for (int ch=0; ch<m_channels; ch++) { 
                                sigmavalp[ch] += valp[ch];
                            }
                            sigmaposp[0] += c;
                            sigmaposp[1] += r;

                            //------------------------------------
                            //edgesum[klabels[ind]] += edgemag[ind];
                            //------------------------------------
                            clustersize[klabels[ind]] += 1.0;
                        }

                        ind++;
                    }
                }
            }

            // Scoped
            {
                for( int k = 0; k < numk; k++ ) {
                    if( clustersize[k] <= 0 ) clustersize[k] = 1;
                    inv[k] = 1.0/clustersize[k];//computing inverse now to multiply, than divide later
                }
            }

            // Scoped
            {
                for( int k = 0; k < numk; k++ ) {

                    double* kseedsvalp = kseedsval.ptr<double>(k); 
                    float* kseedsposp = kseedspos.ptr<float>(k); 

                    const float* sigmaposp = sigmapos.ptr<float>(k);
                    const double* sigmavalp = sigmaval.ptr<double>(k);
                    
                    for (int ch=0; ch<m_channels; ch++) { 
                        kseedsvalp[ch] = sigmavalp[ch] * inv[k];
                        kseedsposp[ch] = sigmaposp[ch] * inv[k];
                    }
                    //------------------------------------
                    //edgesum[k] *= inv[k];
                    //------------------------------------
                }
            }
	}
}

//===========================================================================
///	EnforceLabelConnectivity
///
///		1. finding an adjacent label for each new component at the start
///		2. if a certain component is too small, assigning the previously found
///		    adjacent label to this component, and not incrementing the label.
//===========================================================================
void SLICGeneric::EnforceLabelConnectivity(
                                    const std::vector<int>& labels,
                                    //input labels that need to be corrected to remove stray labels
                                    std::vector<int>& nlabels,
                                    //new labels                                    // int&						numlabels,//the number of labels changes in the end if segments are removed
                                    const int& K) //the number of superpixels desired by the user
{
    //	const int dx8[8] = {-1, -1,  0,  1, 1, 1, 0, -1};
    //	const int dy8[8] = { 0, -1, -1, -1, 0, 1, 1,  1};

    const int dx4[4] = {-1,  0,  1,  0};
    const int dy4[4] = { 0, -1,  0,  1};

    int width = m_width, height = m_height;
    const int sz = width*height;
    const int SUPSZ = sz/K;
    //nlabels.resize(sz, -1);
    for( int i = 0; i < sz; i++ ) nlabels[i] = -1;
    int label(0);
    std::vector<int> xvec(sz), yvec(sz);
    int oindex(0);
    int adjlabel(0);//adjacent label
    for( int j = 0; j < height; j++ ) {
        for( int k = 0; k < width; k++ ) {
            if( 0 > nlabels[oindex] ) {
                nlabels[oindex] = label;
                //--------------------
                // Start a new segment
                //--------------------
                xvec[0] = k;
                yvec[0] = j;
                //-------------------------------------------------------
                // Quickly find an adjacent label for use later if needed
                //-------------------------------------------------------
                {
                    for( int n = 0; n < 4; n++ ) {
                        int x = xvec[0] + dx4[n];
                        int y = yvec[0] + dy4[n];
                        if( (x >= 0 && x < width) && (y >= 0 && y < height) ) {
                            int nindex = y*width + x;
                            if(nlabels[nindex] >= 0) adjlabel = nlabels[nindex];
                        }
                    }
                }

                int count(1);
                for( int c = 0; c < count; c++ ) {
                    for( int n = 0; n < 4; n++ ) {
                        int x = xvec[c] + dx4[n];
                        int y = yvec[c] + dy4[n];

                        if( (x >= 0 && x < width) && (y >= 0 && y < height) ) {
                            int nindex = y*width + x;

                            if( 0 > nlabels[nindex] && 
                                labels[oindex] == labels[nindex] ) {
                                xvec[count] = x;
                                yvec[count] = y;
                                nlabels[nindex] = label;
                                count++;
                            }
                        }

                    }
                }
                //-------------------------------------------------------
                // If segment size is less then a limit, assign an
                // adjacent label found before, and decrement label count.
                //-------------------------------------------------------
                if(count <= SUPSZ >> 2) {
                    for( int c = 0; c < count; c++ ) {
                        int ind = yvec[c]*width+xvec[c];
                        nlabels[ind] = adjlabel;
                    }
                    label--;
                }
                label++;
            }
            oindex++;
        }
    }
    numlabels = label;

}



//===========================================================================
///	DoSuperpixelSegmentation_ForGivenSuperpixelSize
///
/// The input parameter ubuff conains RGB values in a 32-bit unsigned integers
/// as follows:
///
/// [1 1 1 1 1 1 1 1]  [1 1 1 1 1 1 1 1]  [1 1 1 1 1 1 1 1]  [1 1 1 1 1 1 1 1]
///
///        Nothing              R                 G                  B
///
/// The RGB values are accessed from (and packed into) the unsigned integers
/// using bitwise operators as can be seen in the function DoRGBtoLABConversion().
///
/// compactness value depends on the input pixels values. For instance, if
/// the input is greyscale with values ranging from 0-100, then a compactness
/// value of 20.0 would give good results. A greater value will make the
/// superpixels more compact while a smaller value would make them more uneven.
///
/// The labels can be saved if needed using SaveSuperpixelLabels()
//===========================================================================
// void SLICGeneric::DoSuperpixelSegmentation_ForGivenSuperpixelSize(const cv::Mat& img, 
//                                                            const int& superpixelsize,
//                                                            const double& compactness)
cv::Mat SLICGeneric::forGivenSuperpixelSize(const cv::Mat& img, 
                                            const int& superpixelsize, 
                                            const double& compactness, 
                                            const int colorconv, 
                                            const cv::Mat& mask) {
  //------------------------------------------------
  reset();
  
  //------------------------------------------------
  const int STEP = sqrt(double(superpixelsize))+0.5;

  //--------------------------------------------------
  kseeds_val = cv::Mat();
  kseeds_pos = cv::Mat();
    
  //--------------------------------------------------
  m_width  = img.cols;
  m_height = img.rows;
  m_channels = img.channels();
  m_type = img.type();

  int sz = m_width*m_height;

  //--------------------------------------------------
  std::vector<int> labels(sz,-1);

  //--------------------------------------------------
  // m_distance.set_dimensionality(m_channels); 

  //--------------------------------------------------
  if (mask.empty())
    m_mask = cv::Mat(img.rows, img.cols, CV_8UC1, 255);
  else 
    m_mask = mask.clone();
    
  //--------------------------------------------------
  if (m_channels == 3) { 
    m_depth = CV_64FC3;
    if (colorconv == -1) { 
      img.convertTo(m_val, m_depth);
    } else {
      // assert(m_type == CV_8UC3);
      // assert(colorconv == cv::COLOR_BGR2Lab);
      cv::Mat3b col; 
      cv::cvtColor(img, col, colorconv);
      col.convertTo(m_val, m_depth);
    }
  } else { 
    m_depth = CV_64FC1;
    img.convertTo(m_val, m_depth);        
  }
    

  //--------------------------------------------------
  bool perturbseeds(false);//perturb seeds is not absolutely necessary, one can set this flag to false

  vector<double> edgemag(0);
  if(perturbseeds) 
    DetectEdges(m_val, edgemag);
  GetVALXYSeeds_ForGivenStepSize(kseeds_val, kseeds_pos, STEP, perturbseeds, edgemag);

  PerformSuperpixelSLIC(kseeds_val, kseeds_pos, labels, STEP, edgemag,compactness);
  numlabels = kseeds_val.rows * kseeds_val.cols;

  std::vector<int> nlabels(sz);
  EnforceLabelConnectivity(labels, nlabels, double(sz)/double(STEP*STEP));

  cv::Mat_<int> labelimg = cv::Mat_<int>(img.rows, img.cols, &nlabels[0]).clone();
  return labelimg;
}

//===========================================================================
///	DoSuperpixelSegmentation_ForGivenNumberOfSuperpixels
///
/// The input parameter ubuff conains RGB values in a 32-bit unsigned integers
/// as follows:
///
/// [1 1 1 1 1 1 1 1]  [1 1 1 1 1 1 1 1]  [1 1 1 1 1 1 1 1]  [1 1 1 1 1 1 1 1]
///
///        Nothing              R                 G                  B
///
/// The RGB values are accessed from (and packed into) the unsigned integers
/// using bitwise operators as can be seen in the function DoRGBtoLABConversion().
///
/// compactness value depends on the input pixels values. For instance, if
/// the input is greyscale with values ranging from 0-100, then a compactness
/// value of 20.0 would give good results. A greater value will make the
/// superpixels more compact while a smaller value would make them more uneven.
///
/// The labels can be saved if needed using SaveSuperpixelLabels()
//===========================================================================
// K - required number of superpixels 
// compactness - weight given to spatial distance
cv::Mat SLICGeneric::forGivenNumberOfSuperpixels(const cv::Mat& img, 
                                                 const int& K,
                                                 const double& compactness, 
                                                 const int colorconv, 
                                                 const cv::Mat& mask)
{
    const int superpixelsize = 0.5+double(img.cols*img.rows)/double(K);
    return forGivenSuperpixelSize(img,superpixelsize,compactness,colorconv, mask);
}

cv::Mat SLICGeneric::getMeanLocations() { 
    return kseeds_pos;
}

cv::Mat SLICGeneric::getMeanValues() { 
    return kseeds_val;
}


// // // The conversions functions above are taken from OpenCV. The following function is 
// // what we define to access the C++ code we are interested in.
// PyObject* SLICGeneric::forGivenSuperpixelSize(PyObject* pyimg, double var, double compactness, int colorconv) {

//     // Numpy init
//     //import_array();

//     // Convert
//     cv::Mat img;
//     pyopencv_to(pyimg, img); 

//     // Call method
//     cv::Mat labels = forGivenSuperpixelSize(img, var, compactness, colorconv);

//     // Return labels
//     // Py_BuildValue("NN", pyopencv_from(), pyopencv_from)
//     return pyopencv_from(labels); 
// }


} // namespace vision
} // namespace fs

