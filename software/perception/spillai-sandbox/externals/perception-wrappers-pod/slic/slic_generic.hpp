// SLIC.h: interface for the SLIC class.
//===========================================================================
// This code implements the superpixel method described in:
//
// Radhakrishna Achanta, Appu Shaji, Kevin Smith, Aurelien Lucchi, Pascal Fua, and Sabine Susstrunk,
// "SLIC Superpixels",
// EPFL Technical Report no. 149300, June 2010.
//===========================================================================
//	Copyright (c) 2012 Radhakrishna Achanta [EPFL]. All rights reserved.
//===========================================================================
//////////////////////////////////////////////////////////////////////

#if !defined(_SLICPP_H_INCLUDED_)
#define _SLICPP_H_INCLUDED_


#include <vector>
#include <string>
#include <algorithm>
#include <opencv2/opencv.hpp>
// #include<boost/python.hpp>
using namespace std;


// template<typename T>
// class DistanceFunction {
// public:
//     /// the dimensionality
//     const int D;
//     /// constructor for initialising the data dimensionality
//     DistanceFunction(const int _D) : D(_D) {
//     }
//     /// virtual destructor
//     virtual ~DistanceFunction() {};

//     /*! /brief distance operator
//      *
//      * When called, computes the distance between points p1 and p2
//      * using the implemented distance measure. This assumes that
//      * p1[D-1] and p2[D-1] point to valid memory addresses for speed
//      * since this function will often be called in a loop.
//      *
//      * @param p1 the first point
//      * @param p2 the second point
//      * @return the implemented measure of distance or similarity between
//      * p1 and p2
//      */
//     virtual T operator() (T const * const p1, T const * const p2) const = 0;
// };

// template<typename T>
// class EuclideanDistance : public DistanceFunction<T> {
// public:
//     const int method;
//     EuclideanDistance (const int _D = 1) : DistanceFunction<T>(_D), method(0) {}
//     T operator() (T const * const p1, T const * const p2) const {
//         T dist = 0;
//         for (int n = 0; n < this->D; ++n) dist += pow(p2[n]-p1[n],2);
//         return sqrt(dist);
//     }
//     static void set_dimensionality(const int _D) { D = _D; }
//     static T compute(T const * const p1, T const * const p2, const int D) {
//         return EuclideanDistance(D).operator()(p1, p2);
//     }
// };

// // 1 - (v1 dot v2)
// template<typename T>
// class CosineDistanceDistance : public DistanceFunction<T> {
// public:
//     const int method;
//     CosineDistanceDistance (const int _D = 1) : DistanceFunction<T>(_D), method(0) {}
//     T operator() (T const * const p1, T const * const p2) const {
//         T dist = 0;
//         for (int n = 0; n < this->D; ++n) dist += p2[n]*p1[n];
//         return dist;
//     }
//     static void set_dimensionality(const int _D) { D = _D; }
//     static T compute(T const * const p1, T const * const p2, const int D) {
//         return CosineDistanceDistance(D).operator()(p1, p2);
//     }
// };
namespace fs { namespace vision {

class SLICGeneric
{
 public:
  SLICGeneric();
  ~SLICGeneric();
  void reset();
  // template<typename T1>
  // void setDistanceMetric(const DistanceFunction<T1>& distance) { 
  //     m_distance = distance;
  // }

  //============================================================================
  // Superpixel segmentation for a given step size (superpixel size ~= step*step)
  //============================================================================
  cv::Mat forGivenSuperpixelSize(const cv::Mat& img,
                                 const int& superpixelsize, 
                                 const double& compactness,
                                 const int colorconv = -1, 
                                 const cv::Mat& mask = cv::Mat());
  //============================================================================
  // Superpixel segmentation for a given number of superpixels
  //============================================================================
  cv::Mat forGivenNumberOfSuperpixels(const cv::Mat& img,
                                      const int& K,//required number of superpixels
                                      const double& compactness, 
                                      const int colorconv = -1, 
                                      const cv::Mat& mask = cv::Mat());
  //10-20 is a good value for CIELAB space

  cv::Mat getMeanLocations();
  cv::Mat getMeanValues();

  // PyObject* forGivenSuperpixelSize(PyObject* pyimg, 
  //                                  double var, double compactness, int colorconv);

 private:
  //============================================================================
  // The main SLIC algorithm for generating superpixels
  //============================================================================
  void PerformSuperpixelSLIC(
      cv::Mat& val, 
      cv::Mat& pos, 
      std::vector<int>& klabels,
      const int& STEP,
      const std::vector<double>& edgemag,
      const double& m = 10.0);
  //============================================================================
  // Pick seeds for superpixels when step size of superpixels is given.
  //============================================================================
  void GetVALXYSeeds_ForGivenStepSize(
      cv::Mat& val, 
      cv::Mat& pos, 
      const int& STEP,
      const bool& perturbseeds,
      const std::vector<double>& edgemag);
  //============================================================================
  // Move the superpixel seeds to low gradient positions to avoid putting seeds
  // at region boundaries.
  //============================================================================
  void PerturbSeeds(
      cv::Mat& kseedsval, 
      cv::Mat& kseedspos, 
      const std::vector<double>& edges);
  //============================================================================
  // Detect color edges, to help PerturbSeeds()
  //============================================================================
  void DetectEdges(const cv::Mat& val, std::vector<double>& edges);
  //============================================================================
  // Post-processing of SLIC segmentation, to avoid stray labels.
  //============================================================================
  void EnforceLabelConnectivity(
      const std::vector<int>& labels,
      std::vector<int>&	nlabels,//input labels that need to be corrected to remove stray labels
      const int& K); //the number of superpixels desired by the user


 private:
  int m_width;
  int m_height;
  int m_type;
  int m_depth;
  int m_channels;

  cv::Mat m_mask;
  cv::Mat m_val;
  // DistanceFunction* m_distance;

 public: 
  enum colorspace { SLIC_RGB=0, SLIC_HSV, SLIC_LAB };        
  // cv::Mat_<Vec3d> mu_ch;

  cv::Mat kseeds_val;
  cv::Mat kseeds_pos;

  std::vector<double> mu_ch1, mu_ch2, mu_ch3; 
  std::vector<double> cluster_count; 
    
  std::vector<int> klabel;
  int numlabels;
    
};

} // namespace vision
} // namespace fs

#endif // !defined(_SLICPP_H_INCLUDED_)
