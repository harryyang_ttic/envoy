// Birchfield's KLT implementation wrapper
// Refer to 
// http://www.ces.clemson.edu/~stb/klt/user/chpt8.html

// frame_utils include
#include <opencv2/opencv.hpp>
#include <pcl-utils/frame_utils.hpp>
#include <perception_opencv_utils/opencv_utils.hpp>

// birchfield_klt include
#include <fs_perception_wrappers/birchfield_klt/klt.h>

// feature_types
#include <features3d/feature_types.hpp>

namespace fs { namespace vision { 

class BirchfieldKLT {

 public:
  BirchfieldKLT(const int nFeatures=800, const int affineConsistencyCheck=-1,
                const int window_width=9, const int mindist=10);
  ~BirchfieldKLT();
  void processFrame(const opencv_utils::Frame& frame);
  void processImage(const cv::Mat& img);
  std::vector<fsvision::Feature3D> getStableFeatures();

 private:
  KLT_TrackingContext tc;
  KLT_FeatureList fl;
  cv::Mat pgray;

  // For IDing purposes
  int64_t track_id_;
  std::vector<int64_t> ids;
  // std::vector<int> valid;
  int64_t nextTrackID() { return ++track_id_; } 
  int64_t trackID() { return track_id_;  }
  bool resetTrackIDs() { track_id_ = 1; return true; }
  void maintainIDs();
};
  
} // namespace vision
} // namespace fs
