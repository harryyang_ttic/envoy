#include "klt_wrapper.hpp"

namespace fs { namespace vision { 

BirchfieldKLT::BirchfieldKLT(const int nFeatures, const int affineConsistencyCheck,
                             const int window_width, const int mindist) {

  // Setup KLT Tracking context
  tc = KLTCreateTrackingContext();
  tc->mindist = mindist;
  tc->window_width  = window_width;
  tc->window_height = window_width;
  tc->sequentialMode = TRUE;
  tc->writeInternalImages = FALSE;
  tc->affineConsistencyCheck = affineConsistencyCheck;  /* set this to 2 to turn on affine consistency check */
  tc->smoothBeforeSelecting = TRUE;
  
  // Pyramid (search range param)
  KLTChangeTCPyramid(tc, 15);
  KLTUpdateTCBorder(tc);

  // Create feature list with desired num of features
  fl = KLTCreateFeatureList(nFeatures);

  std::cerr << "PYRAMIDS: " << tc->nPyramidLevels << std::endl;
  
  // Empty pgray image
  pgray = cv::Mat();

  // Init ids
  resetTrackIDs();
  ids = std::vector<int64_t>(nFeatures, -1);
  for (int j=0; j<nFeatures; j++)
    ids[j] = nextTrackID();
  // valid = std::vector<int>(nFeatures, -1);
}

BirchfieldKLT::~BirchfieldKLT() {
  KLTFreeFeatureList(fl);
  KLTFreeTrackingContext(tc);
}

void BirchfieldKLT::maintainIDs() {
  assert(fl->nFeatures == ids.size());
  for (int indx = 0 ; indx < fl->nFeatures ; indx++) {
    // Only change IDs when you lose track
    if (fl->feature[indx]->val < 0)  {
      ids[indx] = nextTrackID();
    }
  }
}

void BirchfieldKLT::processFrame(const opencv_utils::Frame& frame) {
  const cv::Mat& gray = frame.getGrayRef();
  processImage(gray);
}
  
void BirchfieldKLT::processImage(const cv::Mat& img) {
  cv::Mat gray;
  if (img.channels() == 3)
    cv::cvtColor(img, gray, cv::COLOR_BGR2GRAY);
  else if (img.channels() == 1)
    gray = img;
  else
    assert(0);

  if (pgray.empty()) { 
    // Select good features
    KLTSelectGoodFeatures(tc, (uchar*) gray.data, gray.cols, gray.rows, fl);
    maintainIDs(); 
  } else {
    // Track features
    KLTTrackFeatures(tc, (uchar*) pgray.data, (uchar*) gray.data, gray.cols, gray.rows, fl);
    maintainIDs();
    KLTReplaceLostFeatures(tc, (uchar*) gray.data, gray.cols, gray.rows, fl);
  }
  // Copy image for next iteration
  pgray = gray.clone();
  return;
}

std::vector<fsvision::Feature3D> BirchfieldKLT::getStableFeatures() {
  std::vector<fsvision::Feature3D> fpts; 

  /* For each feature, do ... */
  float xloc, yloc;
  fsvision::Feature3D fpt;
  assert(fl->nFeatures == ids.size());
  for (int indx = 0 ; indx < fl->nFeatures ; indx++) {
    /* Only track features that are not lost */
    if (fl->feature[indx]->val >= 0)  {
      xloc = fl->feature[indx]->x;
      yloc = fl->feature[indx]->y;
      fpt.set_point(cv::Point2f(xloc,yloc));
      fpt.set_id(ids[indx]);
      fpts.push_back(fpt);
    }
  }
  return fpts;
}

} // namespace vision
} // namespace fs
