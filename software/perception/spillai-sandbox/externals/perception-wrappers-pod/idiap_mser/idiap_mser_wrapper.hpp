// IDIAP MSER implementation wrapper
// Refer to 
// http://www.idiap.ch/scientific-research/resources/mser

// idiap_mser include
#include <fs_perception_wrappers/idiap_mser/mser.h>

// idiap mser
#include "mser.h"

namespace fs { namespace vision { 

class IDIAPMSERWrapper {

 public:
  IDIAPMSERWrapper(int delta = 2, double minArea = 0.0001, double maxArea = 0.5, double maxVariation = 0.5,
		 double minDiversity = 0.33, bool eight = false);
  ~IDIAPMSERWrapper();
  // std::vector<idiap::MSER::Region>
  // void processFrame(const opencv_utils::Frame& frame);
  void processImage(const cv::Mat& img);
  void drawRegions(const cv::Mat& img, std::vector<idiap::MSER::Region>& regions);
  void drawEllipse(const idiap::MSER::Region & region,
                   int width, int height, int depth,
                   uint8_t* bits, const uint8_t * color);

 private:
  idiap::MSER mser;
};
  
} // namespace vision
} // namespace fs
