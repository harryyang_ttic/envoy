#include "idiap_mser_wrapper.hpp"

namespace fs { namespace vision { 

IDIAPMSERWrapper::IDIAPMSERWrapper(int delta, double minArea, double maxArea, double maxVariation,
                     double minDiversity, bool eight) {
  mser = idiap::MSER(delta, minArea, maxArea, maxVariation, minDiversity, eight);
}

IDIAPMSERWrapper::~IDIAPMSERWrapper() {
}


void IDIAPMSERWrapper::processImage(const cv::Mat& gray) {
  std::vector<idiap::MSER::Region> regions;
  mser.processImage(gray, regions);
  // drawRegions(frame.getRGBRef(), regions);
  return;
}

  
// // std::vector<idiap::MSER::Region>
// void IDIAPMSERWrapper::processFrame(const opencv_utils::Frame& frame) {
//   const cv::Mat& gray = frame.getGrayRef();

//   std::vector<idiap::MSER::Region> regions;
//   mser.update(gray, regions);
//   drawRegions(frame.getRGBRef(), regions);
//   return;
//   // return regions;
// }


void IDIAPMSERWrapper::drawEllipse(const idiap::MSER::Region & region,
                                   int width, int height, int depth,
                                   uint8_t* bits, const uint8_t * color) {
  // Centroid (mean)
  const double x = region.moments_[0] / region.area_;
  const double y = region.moments_[1] / region.area_;
	
  // Covariance matrix [a b; b c]
  const double a = region.moments_[2] / region.area_ - x * x;
  const double b = region.moments_[3] / region.area_ - x * y;
  const double c = region.moments_[4] / region.area_ - y * y;
	
  // Eigenvalues of the covariance matrix
  const double d  = a + c;
  const double e  = a - c;
  const double f  = sqrt(4.0 * b * b + e * e);
  const double e0 = (d + f) / 2.0; // First eigenvalue
  const double e1 = (d - f) / 2.0; // Second eigenvalue
	
  // Desired norm of the eigenvectors
  const double e0sq = sqrt(e0);
  const double e1sq = sqrt(e1);
	
  // Eigenvectors
  double v0x = e0sq;
  double v0y = 0.0;
  double v1x = 0.0;
  double v1y = e1sq;
	
  if (b) {
    v0x = e0 - c;
    v0y = b;
    v1x = e1 - c;
    v1y = b;
		
    // Normalize the eigenvectors
    const double n0 = e0sq / sqrt(v0x * v0x + v0y * v0y);
    v0x *= n0;
    v0y *= n0;
		
    const double n1 = e1sq / sqrt(v1x * v1x + v1y * v1y);
    v1x *= n1;
    v1y *= n1;
  }
	
  for (double t = 0.0; t < 2.0 * M_PI; t += 0.001) {
    int x2 = x + (cos(t) * v0x + sin(t) * v1x) * 2.0 + 0.5;
    int y2 = y + (cos(t) * v0y + sin(t) * v1y) * 2.0 + 0.5;
		
    if ((x2 >= 0) && (x2 < width) && (y2 >= 0) && (y2 < height))
      for (int i = 0; i < std::min(depth, 3); ++i)
        bits[(y2 * width + x2) * depth + i] = color[i];
  }
}


void IDIAPMSERWrapper::drawRegions(const cv::Mat& img, std::vector<idiap::MSER::Region>& regions) {

  cv::Mat disp = img.clone();
  	
  // Draw ellipses in the original image
  const uint8_t colors[3] = {255, 255, 255};
	
  for (int j = 0; j < regions.size(); ++j)
    drawEllipse(regions[j], img.cols, img.rows, disp.channels(), (uint8_t*)disp.data, colors);

  cv::imshow("IDIAP MSER", disp);
  cv::waitKey(1);
  return ;
  
}

} // namespace vision
} // namespace fs
