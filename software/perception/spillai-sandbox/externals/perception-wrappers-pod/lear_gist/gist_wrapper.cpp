#include "gist_wrapper.hpp"

namespace fs { namespace vision { 

GIST::GIST() {
  nblocks = 4, n_scale = 3;
  int orientations_per_scale_[] = {8, 8, 3};
  orientations_per_scale = std::vector<int>(orientations_per_scale_,
                                            orientations_per_scale_ + 3 * sizeof(int));
  
  /* compute descriptor size */
  descsize=0;
  for(int i=0;i<n_scale;i++) 
    descsize+=nblocks*nblocks*orientations_per_scale[i];
  descsize*=3; /* color */

  // /* print descriptor */
  // for(int i=0;i<descsize;i++) 
  //   printf("%.4f ",desc[i]);
}

GIST::~GIST() {
}

cv::Mat GIST::processFrame(const opencv_utils::Frame& frame) {
  return processImage(frame.getRGBRef());
}

cv::Mat GIST::processImage(const cv::Mat& img) {
  assert(img.channels() == 3);

  std::vector<cv::Mat> ch;
  cv::split(img, ch);

  color_image_t* im = color_image_new(img.cols, img.rows);
  int npixels = img.cols * img.rows;
  memcpy(im->c1, ch[2].data, npixels);
  memcpy(im->c2, ch[1].data, npixels);
  memcpy(im->c3, ch[0].data, npixels);

  for (int j=0; j<3; j++)
    std::cerr << orientations_per_scale[j] << " ";
  float* descp = color_gist_scaletab(im, nblocks, n_scale, &orientations_per_scale[0]);
  color_image_delete(im);
  
  std::vector<float> desc(descp, descp + sizeof(float) * descsize);
  return cv::Mat(desc);
}

} // namespace vision
} // namespace fs
