#ifndef FS_TYPES_HPP_
#define FS_TYPES_HPP_

#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include "utils/template.h"
#include "utils/container.h"
#include "utils/conversion.h"
#include "utils/pair.h"

// opencv includes
#include <opencv2/opencv.hpp>

namespace fs { namespace python {

// python::tuple ret=python::make_tuple(se3.position,se3.orientation);
// return Py_BuildValue("(ddd)", v[0], v[1], v[2]);
// return incref(ret.ptr());

struct Vec3f_to_mat {
  static PyObject* convert(const cv::Vec3f& v){
    NDArrayConverter cvt;
    PyObject* ret = cvt.toNDArray(cv::Mat(v));
    return ret;
  }
};


struct Point_to_mat {
  static PyObject* convert(const cv::Point& v){
    NDArrayConverter cvt;
    PyObject* ret = cvt.toNDArray(cv::Mat(v));
    return ret;
  }
};

struct Point2f_to_mat {
  static PyObject* convert(const cv::Point2f& v){
    NDArrayConverter cvt;
    PyObject* ret = cvt.toNDArray(cv::Mat(v));
    return ret;
  }
};

struct Point3f_to_mat {
  static PyObject* convert(const cv::Point3f& v){
    NDArrayConverter cvt;
    PyObject* ret = cvt.toNDArray(cv::Mat(v));
    return ret;
  }
};

// Convert to type T (cv::Mat)
template <typename T>
struct Mat_to_pyobject {
  static PyObject* convert(const T& mat){
    NDArrayConverter cvt;
    PyObject* ret = cvt.toNDArray(mat);
    return ret;
  }
};


template <typename T>
struct Mat_python_converter
{
  Mat_python_converter()
  {
    // Register from converter
    boost::python::converter::registry::push_back(
        &convertible,
        &construct,
        boost::python::type_id<T>());

    // Register to converter
    py::to_python_converter<T, Mat_to_pyobject<T> >();
  }

  // Convert from type T to PyObject (numpy array)
  // Determine if obj_ptr can be converted in a cv::Mat
  static void* convertible(PyObject* obj_ptr)
  {
    // if (!PyString_Check(obj_ptr)) return 0;
    return obj_ptr;
  }

  // Convert obj_ptr into a cv::Mat
  static void construct(PyObject* obj_ptr,
                        boost::python::converter::rvalue_from_python_stage1_data* data)
  {
    using namespace boost::python;
    typedef converter::rvalue_from_python_storage< T > storage_t;

    storage_t* the_storage = reinterpret_cast<storage_t*>( data );
    void* memory_chunk = the_storage->storage.bytes;

    NDArrayConverter cvt;
    T* newvec = new (memory_chunk) T(cvt.toMat(obj_ptr));
    data->convertible = memory_chunk;

    return;
  }
};

// void py_init();
bool init_and_export_converters();

} // namespace python
} // namespace fs

#endif // FS_TYPES_HPP_
