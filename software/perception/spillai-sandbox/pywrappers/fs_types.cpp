#include "fs_types.hpp"

namespace fs { namespace python {

static void py_init() {
  Py_Initialize();
  import_array();
}

static bool export_type_conversions_once = false;
bool init_and_export_converters() {

  if (export_type_conversions_once)
    return false;
  
  std::cerr << "PYTHON TYPE CONVERTERS exported" << std::endl;
  export_type_conversions_once = true;

  // Py_Init and array import
  py_init();
  
  expose_template_type<int>();
  expose_template_type<float>();
  expose_template_type<double>();

  // vectors
  expose_template_type< std::vector<int> >();
  expose_template_type< std::vector<float> >();
  expose_template_type< std::vector<double> >();

  expose_template_type< std::vector<cv::Mat> >();
  expose_template_type< std::vector<cv::Mat_<float> > >();

  expose_template_type<std::map<int, std::vector<int> > >();
  expose_template_type<std::map<int, std::vector<float> > >();

  expose_template_type< std::vector<cv::Point> >();
  expose_template_type< std::vector<cv::Point2f> >();
  expose_template_type< std::vector<cv::KeyPoint> >();
  
  py::to_python_converter<cv::Point, Point_to_mat>();
  py::to_python_converter<cv::Point2f, Point2f_to_mat>();
  py::to_python_converter<cv::Point3f, Point3f_to_mat>();
  py::to_python_converter<cv::Vec3f, Vec3f_to_mat>();

  // register the to-from-python converter for each of the types
  Mat_python_converter< cv::Mat >();
  
  Mat_python_converter< cv::Mat_<int> >();
  Mat_python_converter< cv::Mat_<float> >();
  Mat_python_converter< cv::Mat_<double> >();
  
  Mat_python_converter< cv::Mat1w >();
  Mat_python_converter< cv::Mat1b >();
  Mat_python_converter< cv::Mat3b >();
  Mat_python_converter< cv::Mat3f >();

  return true;
}

} // namespace python
} // namespace fs

