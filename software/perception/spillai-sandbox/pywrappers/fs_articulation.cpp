// Wrapper for Articulation module
// lcm
#include <lcm/lcm-cpp.hpp>
#include <lcmtypes/bot2_param.h>

#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

// fs_types
#include "fs_types.hpp"

#include <articulation/factory.h>
#include <articulation/utils.hpp>
#include <articulation/structs.h>
#include <articulation/ArticulatedObject.hpp>

using namespace articulation_models;
// using namespace articulation;

namespace py = boost::python;

namespace fs { namespace vision { 
class ArticulationLearner {
 public:
  ArticulationLearner() : b_server(NULL), ref_id(-1) {}
  ArticulationLearner(const std::string& msg_buf, int ref_id=-1) :
      b_server(NULL) {

    // Init server
    InitParams();
    
    articulation::track_list_msg_t msg;
    msg.decode((void*) msg_buf.c_str(), 0, msg_buf.size());

    // Test validity of decode
    lcm.publish("ARTICULATION_OBJECT_TRACKS", &msg);
    
    // Object msg is updated
    articulation::articulated_object_msg_t object_msg;
    object_msg.parts.resize(msg.num_tracks);
    for (int j=0; j<msg.num_tracks; j++) {
      std::cerr << "TRACK : " << msg.tracks[j].id << " => " << j << std::endl;
      object_msg.parts[j] = msg.tracks[j];
    }

    // Setting object model
    object = ArticulatedObject(params); 
    object.SetObjectModel(object_msg); 
  }

  std::string project(const std::string& msg_buf) {
    articulation::track_list_msg_t msg;
    msg.decode((void*) msg_buf.c_str(), 0, msg_buf.size());

  }
  
  std::string fit() {
    // Fit the object model
    object.FitModels();

    articulation::articulated_object_msg_t fully_connected_object = object.GetObjectModel();
    std::cerr << "\n============== fit_models DONE ===========" << std::endl;
    std::cerr << "\n============== get_spanning_tree ===========" << std::endl; 
    // structureSelectSpanningTree
    object.ComputeSpanningTree();
    std::cerr << "\n============== get_spanning_tree DONE ===========" << std::endl; 
    // structureSelectSpanningTree
    std::cerr << "\n============== get_fast_graph ===========" << std::endl; 
    // structureSelectFastGraph
    object.getFastGraph();
    std::cerr << "\n============== get_fast_graph DONE ===========" << std::endl; 
    // structureSelectFastGraph
    std::cerr << "\n============== get_graph ===========" << std::endl; 
    // structureSelectGraph
    object.getGraph();
    std::cerr << "\n============== get_graph DONE ===========" << std::endl; 

    //----------------------------------
    // Publish articulated object
    //----------------------------------
    articulation::articulated_object_msg_t fitted_object = object.GetObjectModel();

    // Fix issue with sizes before publishing
    fitted_object.num_parts = fitted_object.parts.size();
    fitted_object.num_params = fitted_object.params.size();
    fitted_object.num_models = fitted_object.models.size();
    for (int j=0; j<fitted_object.num_models; j++) {
      fitted_object.models[j].track.num_channels = fitted_object.models[j].track.channels.size();
      for (int k=0; k<fitted_object.models[j].track.channels.size(); k++)
        fitted_object.models[j].track.channels[k].num_values = fitted_object.models[j].track.channels[k].values.size();
      fitted_object.models[j].track.num_poses = fitted_object.models[j].track.pose.size();
      fitted_object.models[j].track.num_poses_projected = fitted_object.models[j].track.pose_projected.size();
      fitted_object.models[j].track.num_poses_resampled = fitted_object.models[j].track.pose_resampled.size();
    }
    fitted_object.num_markers = fitted_object.markers.size();

    // lcm.publish("ARTICULATED_OBJECT", &fitted_object);

    // Return the fitted object
    int sz = fitted_object.getEncodedSize();
    char *buf = (char*) malloc (sz);
    fitted_object.encode(buf, 0, sz);

    std::string ret;
    ret.assign(buf, sz);
    
    free(buf);
    
    return ret;
  }

  void InitParams() {
    // Not using config file for filter models
    b_server = bot_param_new_from_server(lcm.getUnderlyingLCM(), 1);
    params.LoadParams(b_server);
  }
  
 private:
  ArticulatedObject object; // (params); 
  KinematicParams params;
  int ref_id;
  
  lcm::LCM lcm;
  BotParam   *b_server;
};

} // namespace vision
} // namespace fs

namespace fs { namespace python { 

BOOST_PYTHON_MODULE(fs_articulation)
{
  // Py Init and Main types export
  fs::python::init_and_export_converters();

  // ArticulationLearner
  py::class_<fs::vision::ArticulationLearner>("ArticulationLearner")
      .def(py::init< std::string, py::optional<int> >
           (py::args("msg", "ref_id")))
      .def("fit", &fs::vision::ArticulationLearner::fit)
      ;
}

} // namespace python
} // namespace fs
