// Wrapper for particular pcl functions
// The cython pcl module is incomplete

#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

// fs_types
#include "fs_types.hpp"

// Eigen
#include <Eigen/Dense>
#include <opencv2/core/eigen.hpp>

// pcl-utils
#include <pcl-utils/pcl_utils.hpp>

#include <pcl/io/pcd_io.h>
#include <pcl/octree/octree.h>
#include <pcl/correspondence.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/correspondence_rejection_surface_normal.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>

namespace py = boost::python;

namespace fs { namespace python { 

Eigen::Matrix4f icp(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud_in,
                    pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud_out) {
  pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
  icp.setInputCloud(cloud_in);
  icp.setInputTarget(cloud_out);
  pcl::PointCloud<pcl::PointXYZ>::Ptr Final(new pcl::PointCloud<pcl::PointXYZ>());
  icp.align(*Final);
  Eigen::Matrix4f T = icp.getFinalTransformation();
  return T;
}

// cv::Mat1f svd_tf() {
//   Eigen::Matrix4f transformation;
//   pcl::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> svd;
//   svd.estimateRigidTransformation(src, trgt, corres, trans);
// }


// py::tuple correspondence_rejection_normal_SAC(const cv::Mat_<float>& from_cloud,
//                                               const cv::Mat_<float>& to_cloud,
//                                               const cv::Mat_<float>& from_normals,
//                                               const cv::Mat_<float>& to_normals,
//                                               float inlier_threshold=0.05,
//                                               int max_iterations=20) {

//   pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> reg;
//   typedef pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ>
//       PointToPoint;
//   boost::shared_ptr<PointToPoint> tf_svd(new PointToPoint(&reg));
//   reg.setTransformationEstimation(tf_svd);
  
//   // Convert correspondences
//   pcl::CorrespondencesPtr corrs(new pcl::Correspondences());
//   for (int j=0; j<from_cloud.rows; j++)
//     corrs->push_back(pcl::Correspondence(j, j, 0.f));
  
//   // Set correspondences, and refine params
//   // reg.setRefineModel(true);
//   reg.setInputCorrespondences(corrs);

//   // Convert to pcl
//   pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_from_cloud(new pcl::PointCloud<pcl::PointXYZ>());
//   pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_to_cloud(new pcl::PointCloud<pcl::PointXYZ>()); 

//   pcl_utils::convert_mat_to_pcl<pcl::PointXYZ>(from_cloud, *pcl_from_cloud);
//   pcl_utils::convert_mat_to_pcl<pcl::PointXYZ>(to_cloud, *pcl_to_cloud); 
  
//   // Convert to pcl
//   pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_from_normals
//       (new pcl::PointCloud<pcl::PointXYZ>());
//   pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_to_normals
//       (new pcl::PointCloud<pcl::PointXYZ>()); 

//   pcl_utils::convert_mat_to_pcl<pcl::PointXYZ>(from_normals, *pcl_from_normals);
//   pcl_utils::convert_mat_to_pcl<pcl::PointXYZ>(to_normals, *pcl_to_normals); 

//   // Rejector 1
//   boost::shared_ptr<pcl::registration::CorrespondenceRejectorSurfaceNormal> rej_normals
//       (new pcl::registration::CorrespondenceRejectorSurfaceNormal);
//   rej_normals->setThreshold(M_PI/4.f);
//   rej_normals->initializeDataContainer<pcl::PointXYZ, pcl::PointXYZ>();
//   rej_normals->setInputSource<pcl::PointXYZ>(pcl_from_cloud);
//   rej_normals->setInputTarget<pcl::PointXYZ>(pcl_to_cloud);
//   reg.addCorrespondenceRejector(rej_normals);
  
//   // Rejector 2
//   typedef pcl::registration::CorrespondenceRejectorSampleConsensus<pcl::PointXYZ>
//       CorrespondenceRejectionSAC;
//   boost::shared_ptr<CorrespondenceRejectionSAC> rej_points
//       (new CorrespondenceRejectionSAC() );

//   // Set input, and target clouds
//   rej_points->setInputSource<pcl::PointXYZ>(pcl_from_cloud);
//   rej_points->setInputTarget<pcl::PointXYZ>(pcl_to_cloud);

//   // Set params
//   rej_points->setInlierThreshold(inlier_threshold);
//   rej_points->setMaxIterations(max_iterations);
//   reg.addCorrespondenceRejector(rej_points);
  
//   // Get inliers, and refine model
//   pcl::CorrespondencesPtr inliers_(new pcl::Correspondences());
//   reg.getCorrespondences(*inliers_);

//   // Convert Inliers
//   Eigen::Matrix4f T_ = sac.getBestTransformation();
//   std::vector<int> inliers(inliers_->size());
//   for (int j=0; j<inliers_->size(); j++)
//     inliers[j] = (*inliers_)[j].index_query; 

//   // Convert tf
//   cv::Mat T;
//   cv::eigen2cv(T_, T);

//   // Return tf and inliers
//   return py::make_tuple(T, inliers);
// }

std::vector<int>
change_detection(const cv::Mat_<float>& from_cloud, const cv::Mat_<float>& to_cloud,
                 const float resolution, const int noise_filter,
                 const bool return_inverse) {

  // Octree resolution - side length of octree voxels
  std::cerr << "Resolution: " << resolution << std::endl;
  std::cerr << "Noise filter: " << noise_filter << std::endl;
  
  // Convert to pcl
  pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_from_cloud(new pcl::PointCloud<pcl::PointXYZ>());
  pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_to_cloud(new pcl::PointCloud<pcl::PointXYZ>()); 

  pcl_utils::convert_mat_to_pcl<pcl::PointXYZ>(from_cloud, *pcl_from_cloud);
  pcl_utils::convert_mat_to_pcl<pcl::PointXYZ>(to_cloud, *pcl_to_cloud); 
  
  // Instantiate octree-based point cloud change detection class
  pcl::octree::OctreePointCloudChangeDetector<pcl::PointXYZ> octree (resolution);

  // Add points from cloudA to octree
  octree.setInputCloud (pcl_from_cloud);
  octree.addPointsFromInputCloud ();
  
  // Switch octree buffers: This resets octree but keeps previous tree structure in memory.
  octree.switchBuffers ();

  // Add points from cloudB to octree
  octree.setInputCloud (pcl_to_cloud);
  octree.addPointsFromInputCloud ();
  
  // Get vector of point indices from octree voxels which did not exist in previous buffer
  std::vector<int> newPointIdxVector;
  octree.getPointIndicesFromNewVoxels (newPointIdxVector); // , noise_filter);

  if (return_inverse) {
    std::vector<int> all_inds(pcl_to_cloud->points.size(), 0);

    // Set original inds
    for (int j=0; j<all_inds.size(); j++)
      all_inds[j] = j;
    
    // Set values at locations for masking
    for (int j=0; j<newPointIdxVector.size(); j++)
      all_inds[newPointIdxVector[j]] = -1;

    for (std::vector<int>::iterator it = all_inds.begin(); it != all_inds.end(); )
      if (*it == -1)
        it = all_inds.erase(it);
      else
        it++;
    newPointIdxVector = all_inds;
  }
  
  // // Output points
  // std::cout << "Output from getPointIndicesFromNewVoxels:" << std::endl;
  // for (size_t i = 0; i < newPointIdxVector.size (); ++i)
  //   std::cout << i << "# Index:" << newPointIdxVector[i]
  //             << "  Point:" << pcl_to_cloud->points[newPointIdxVector[i]].x << " "
  //             << pcl_to_cloud->points[newPointIdxVector[i]].y << " "
  //             << pcl_to_cloud->points[newPointIdxVector[i]].z << std::endl;

  // index to_cloud
  return newPointIdxVector;
}


py::tuple
correspondence_rejection_SAC(const cv::Mat_<float>& from_cloud,
                             const cv::Mat_<float>& to_cloud,
                             const cv::Mat_<float>& from_cloud_dense=cv::Mat_<float>(),
                             const cv::Mat_<float>& to_cloud_dense=cv::Mat_<float>(), 
                             float inlier_threshold=0.05,
                             int max_iterations=20) {

  pcl::registration::CorrespondenceRejectorSampleConsensus<pcl::PointXYZ> sac;

  // Convert to pcl
  pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_from_cloud(new pcl::PointCloud<pcl::PointXYZ>());
  pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_to_cloud(new pcl::PointCloud<pcl::PointXYZ>()); 

  pcl_utils::convert_mat_to_pcl<pcl::PointXYZ>(from_cloud, *pcl_from_cloud);
  pcl_utils::convert_mat_to_pcl<pcl::PointXYZ>(to_cloud, *pcl_to_cloud); 

  // Convert correspondences
  pcl::CorrespondencesPtr corrs(new pcl::Correspondences());
  for (int j=0; j<from_cloud.rows; j++) {
    corrs->push_back(pcl::Correspondence(j, j, 0.f));
    // cv::norm(from_cloud.row(j) - to_cloud.row(j))));
  }

  // Set input, and target clouds
  sac.setInputCloud(pcl_from_cloud);
  sac.setTargetCloud(pcl_to_cloud);

  // Set params
  sac.setInlierThreshold(inlier_threshold);
  sac.setMaxIterations(max_iterations);

  // Set correspondences, and refine params
  sac.setRefineModel(true);
  sac.setInputCorrespondences(corrs);

  // Get inliers, and refine model
  pcl::CorrespondencesPtr inliers_(new pcl::Correspondences());
  sac.getCorrespondences(*inliers_);

  // Convert Inliers
  Eigen::Matrix4f T_ = sac.getBestTransformation();
  std::cerr << "tf: " << T_ << std::endl;

  std::vector<int> inliers(inliers_->size());
  for (int j=0; j<inliers_->size(); j++)
    inliers[j] = (*inliers_)[j].index_query; 

  if (!from_cloud_dense.empty() && !to_cloud_dense.empty()) {
    std::cerr << "ICP with dense point clouds " << std::endl;
    // Convert to pcl
    pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_from_cloud_dense
        (new pcl::PointCloud<pcl::PointXYZ>());
    pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_to_cloud_dense
        (new pcl::PointCloud<pcl::PointXYZ>()); 

    pcl_utils::convert_mat_to_pcl<pcl::PointXYZ>(from_cloud_dense, *pcl_from_cloud_dense);
    pcl_utils::convert_mat_to_pcl<pcl::PointXYZ>(to_cloud_dense, *pcl_to_cloud_dense); 

    // std::cerr << "ICP aligning  " << pcl_from_cloud_dense->points.size() << " "
    //           << pcl_to_cloud_dense->points.size() << std::endl;
    T_ = icp(pcl_from_cloud_dense, pcl_to_cloud_dense);
    std::cerr << "tf icp: " << T_ << std::endl;
  }
  
  
  // Convert tf
  cv::Mat T;
  cv::eigen2cv(T_, T);

  // Return tf and inliers
  return py::make_tuple(T, inliers);
}

cv::Mat3f compute_normals_wrapper(const cv::Mat3f& cloud,
                                  float depth_change_factor=0.5, float smoothing_size=10.f) {
  cv::Mat3f normals;
  pcl_utils::compute_normals(cloud, normals, depth_change_factor, smoothing_size);
  return normals;
}

// BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(compute_normals_overloads,
//                                        fs::python::compute_normals_wrapper, 1, 3);

BOOST_PYTHON_MODULE(fs_pcl_utils)
{
  // Py Init and Main types export
  fs::python::init_and_export_converters();

  py::def("compute_normals",
          &fs::python::compute_normals_wrapper, 
          py::args("cloud", "depth_change_factor", "smoothing_size"));

  py::def("CorrespondenceRejectionSAC",
          &fs::python::correspondence_rejection_SAC, 
          (py::arg("source"), py::arg("target"),
           py::arg("source_dense"), py::arg("target_dense"),
           py::arg("inlier_threshold")=0.05, py::arg("max_iterations")=20));

  py::def("change_detection",
          &fs::python::change_detection, 
          (py::arg("source"), py::arg("target"),
           py::arg("resolution")=0.01, py::arg("noise_filter")=7,
           py::arg("return_inverse")=false));

}


} // namespace python
} // namespace fs
