#ifndef NUMPYMAT_H_
#define NUMPYMAT_H_

#include <iostream>

#include <boost/python.hpp>
#include <numpy/arrayobject.h>
#include <complex>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>

using namespace cv;

namespace{
int failmsg(const char *fmt, ...);
}
bool numpy_to_mat(const PyObject* o, cv::Mat& m, const char* name = "<unknown>", bool allowND=true);

#endif // NUMPYMAT_H_


