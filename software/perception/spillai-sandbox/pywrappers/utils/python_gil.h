// Copyright(c) 2008 Tri Tech Information Systems Inc. 
// Distributed under the Boost Software License, Version 1.0.
//     (See accompanying file ../../LICENSE_1_0.txt or copy at
//           http://www.boost.org/LICENSE_1_0.txt)
//     

#ifndef __UTIL_PYTHON_GIL_H__
#define __UTIL_PYTHON_GIL_H__

class PythonGIL
{
    PyGILState_STATE m_gstate;

public:
    PythonGIL()
    {
        m_gstate = PyGILState_Ensure();
    }

    ~PythonGIL()
    {
        PyGILState_Release( m_gstate );
    }
};

#endif
