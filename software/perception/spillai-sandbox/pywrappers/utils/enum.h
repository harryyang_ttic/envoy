// Copyright(c) 2008 Tri Tech Information Systems Inc. 
// Distributed under the Boost Software License, Version 1.0.
//     (See accompanying file ../../LICENSE_1_0.txt or copy at
//           http://www.boost.org/LICENSE_1_0.txt)
//     

#ifndef INCLUDE_PYTHON_ENUM_H
#define INCLUDE_PYTHON_ENUM_H
#include <boost/python.hpp>

template<typename T>
struct exported_enum_ : public boost::python::enum_<T>
{
    exported_enum_(char const * name, char const * prefix):
        boost::python::enum_<T>(name),
        m_prefix(prefix)
    {}

    exported_enum_<T>& value(char const * name, T t)
    {
        boost::python::enum_<T>::value(name,t);
        boost::python::api::setattr(current, boost::python::object(m_prefix + name), t);
        return *this;
    }    


    private:
        std::string m_prefix;        
        boost::python::scope current;
};
#endif

