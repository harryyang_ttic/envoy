add_definitions(
    -std=gnu99)

#build the viewer

add_executable(fs-viewer 
    udp_util.c
    main.cpp
    )

include_directories(
    ${GTK2_INCLUDE_DIRS}
    ${GLUT_INCLUDE_DIR})

target_link_libraries(fs-viewer
    ${GTK2_LDFLAGS}
    ${GLUT_LIBRARIES})

pods_use_pkg_config_packages(fs-viewer
  #    arm-interface-renderer
  bot2-vis 
  bot2-lcmgl-renderer 
  bot2-frames-renderers 
  bot2-frames
  er-renderers
  # laser-util-renderer
  image-util-renderer
  lcmtypes_er-lcmtypes
  #    map3d_interface
  #    er-path-util
  #    velodyne-renderer
  #    occ-map-renderers
  #    nodder-renderer
  kinect-renderer
  collections_renderer  
  #    rrtstar-arm-renderer
  #    octomap-renderer
  articulation-renderer
  articulation-structure-learner  
    #er-semantic-perception-renderers
    # renderer_visualization
    #kinect-skeleton-renderer
    #object-model-renderer
    )

pods_install_executables(fs-viewer)
