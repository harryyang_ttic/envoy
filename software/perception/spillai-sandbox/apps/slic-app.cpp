// opencv-utils includes
#include <perception_opencv_utils/opencv_utils.hpp>

// lcm log player wrapper
#include <lcm-utils/lcm-reader-util.hpp>

// slic include
#include <fs_perception_wrappers/slic/slic_generic.hpp>
#include <fs_perception_wrappers/slic/slic_utils.hpp>

// threads
#include <fs-utils/thread_safe_grabber.hpp>

// optargs
#include <ConciseArgs>

using namespace std;
using namespace cv;

//----------------------------------
// State representation
//----------------------------------
struct state_t {
  lcm::LCM lcm;
  cv::VideoWriter writer;
  ThreadSafeGrabber<kinect::frame_msg_t> grabber;

  void run(const kinect::frame_msg_t& msg);
  
  state_t() {
    grabber.run = boost::bind(&state_t::run, this, _1);
    grabber.subscribe("KINECT_FRAME");
  }

  ~state_t() { 
  }

  void* ptr() { 
    return (void*) this;
  }
    
  void process(const cv::Mat& img);

  void on_image_frame (const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                       const bot_core::image_t *msg);

  void on_kinect_image_frame (const lcm::ReceiveBuffer* rbuf, 
                              const std::string& chan,
                              const kinect::frame_msg_t *msg);

};
state_t state;

struct SLICOptions { 
  bool vLIVE_MODE;
  bool vCREATE_VIDEO;
  bool vDEBUG;
  int vPLOT;
  float vMAX_FPS;
  float vSCALE;
  float vSUPERPIXEL_SIZE, vCOMPACTNESS, vSUPERPIXEL_COUNT;
  std::string vLOG_FILENAME;
  std::string vCHANNEL;
  std::string vTYPE;

  SLICOptions () : 
      vLOG_FILENAME(""), vCHANNEL("KINECT_FRAME"), vTYPE("SLIC"), 
      vMAX_FPS(30.f) , vSCALE(1.f), vSUPERPIXEL_SIZE(300), vSUPERPIXEL_COUNT(0) /*500*/, vCOMPACTNESS(10), 
      vDEBUG(false), vLIVE_MODE(false), vCREATE_VIDEO(false), vPLOT(1) {}
};
SLICOptions options;

// LCM Log player wrapper
LCMLogReaderOptions poptions;
LCMLogReader player;



static void on_kinect_image_frame(const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                                  const kinect::frame_msg_t *msg) {
  state.on_kinect_image_frame(rbuf, chan, msg);
}

void state_t::on_image_frame (const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                              const bot_core::image_t *msg) {

  //----------------------------------
  // Unpack 
  //----------------------------------
  double t1 = bot_timestamp_now();
  cv::Mat img = cv::Mat(msg->height, msg->width, CV_8UC3, (void*) &msg->data[0]).clone();
  printf("===> UNPACK TIME: %4.2f ms\n", (bot_timestamp_now() - t1) * 1e-3); 
  std::cerr << "rgb: " << img.size() << std::endl;

  cv::resize(img, img, cv::Size(int(img.cols * options.vSCALE), int(img.rows * options.vSCALE)), 
             0, 0, cv::INTER_NEAREST);

  //----------------------------------
  // Process
  //----------------------------------
  process(img);

}

void state_t::run(const kinect::frame_msg_t& msg) {
  //----------------------------------
  // Unpack Point cloud
  //----------------------------------
  cv::Mat img;
  double t1 = bot_timestamp_now();
  opencv_utils::unpack_kinect_frame(&msg, img, options.vSCALE);
  printf("===> UNPACK TIME: %4.2f ms\n", (bot_timestamp_now() - t1) * 1e-3); 
  std::cerr << "rgb: " << img.size() << std::endl;

  //----------------------------------
  // Process
  //----------------------------------
  process(img);
}

void state_t::on_kinect_image_frame (const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                                     const kinect::frame_msg_t *msg) {
  run(*msg);
}

void state_t::process(const cv::Mat& img) { 

  double t1 = bot_timestamp_now();

  //--------------------------------------------
  // Gaussian blur
  //--------------------------------------------
  cv::GaussianBlur(img, img, cv::Size(3,3), 0, 0, cv::BORDER_DEFAULT);

  //----------------------------------
  // Main SLIC call
  //----------------------------------
  int numlabels;
  cv::Mat labels;
  if (options.vTYPE == "SLIC") { 
    numlabels = fs::vision::slic(img, labels, 
                               options.vSUPERPIXEL_SIZE, options.vCOMPACTNESS, 
                                 fs::vision::slic_op_type::SLIC_FOR_GIVEN_SUPERPIXEL_SIZE, 
                               cv::COLOR_BGR2Lab);
  } else 
    std::cerr << "Uknown segmetation type" << std::endl;

  //----------------------------------
  // Draw labels
  //----------------------------------
  cv::Mat out_img = fs::vision::draw_contours_from_labels(img, labels);
  cv::imshow("rgb_img", out_img);
  printf("===> TOTAL TIME: %4.2f ms\n", (bot_timestamp_now() - t1) * 1e-3); 
  cv::waitKey(10);

  //----------------------------------
  // Create video writer
  //----------------------------------
  if (options.vCREATE_VIDEO && options.vLOG_FILENAME != "") {
    if (!writer.isOpened()) { 
      writer.open(options.vLOG_FILENAME + "-video.avi", VideoWriter::fourcc('M','P','4','2'),
                  30, out_img.size(), 1);
      std::cerr << "===> Video writer created" << std::endl;
    }
    writer << out_img;
    std::cerr << "writing" << std::endl;
  }
  return;
}

int main( int argc, char** argv )
{

  //----------------------------------
  // Opt args
  //----------------------------------
  ConciseArgs opt(argc, (char**)argv);
  opt.add(options.vLOG_FILENAME, "l", "log-file","Log file name");
  opt.add(options.vCREATE_VIDEO, "v", "create-video","Create video");
  opt.add(options.vCHANNEL, "c", "channel","Kinect Channel");
  opt.add(options.vTYPE, "a", "algorithm","Superpixel segmentation type");
  opt.add(options.vMAX_FPS, "r", "max-rate","Max FPS");
  opt.add(options.vSCALE, "s", "scale","Scale");    
  opt.add(options.vPLOT, "p", "plot","Plot Viz");
  opt.add(options.vSUPERPIXEL_SIZE, "z", "size","Superpixel size");
  opt.add(options.vSUPERPIXEL_COUNT, "n", "count","Superpixel count");
  opt.add(options.vCOMPACTNESS, "t", "compact","Superpixel compactness");
  opt.parse();

  //----------------------------------
  // args output
  //----------------------------------
  std::cerr << "===========  SLIC app ============" << std::endl;
  std::cerr << "MODES 1: slic -l log-file -v <create video> -r max-rate -t SLIC/SEEDS\n";
  std::cerr << "=============================================\n";
  opt.usage();
  std::cerr << "===============================================" << std::endl;

    
  // Handle special args cases
  if (options.vLOG_FILENAME.empty()) { 
    options.vLIVE_MODE = true;
  } else {
    options.vLIVE_MODE = false;
    options.vCREATE_VIDEO = false;

    //----------------------------------
    // Setup Player Options
    //----------------------------------
    poptions.fn = options.vLOG_FILENAME;
    poptions.ch = options.vCHANNEL;
    poptions.fps = options.vMAX_FPS;

    poptions.lcm = &state.lcm;
    poptions.handler = &on_kinect_image_frame;
    poptions.user_data = state.ptr();
  }

  //----------------------------------
  // Subscribe, and start main loop
  //----------------------------------
  if (options.vLIVE_MODE) {
    // state.lcm.subscribe(options.vCHANNEL, &state_t::on_kinect_image_frame, &state ); 
    // state.lcm.subscribe(options.vCHANNEL, &state_t::on_image_frame, &state ); 
    while (state.lcm.handle() == 0);
  } else { 
    player.init(poptions); 
  }

  return 0;
}
