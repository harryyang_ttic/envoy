// dense-trajectories include
#include <fs_perception_wrappers/lear_dense_trajectories/lear_dense_trajectories.hpp>
#include <fs_perception_wrappers/fs_dense_trajectories/fs_dense_trajectories.hpp>

// lcm
#include <lcm/lcm-cpp.hpp>
#include <lcmtypes/bot2_param.h>

// libbot/lcm includes
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>

// libbot cpp
#include <lcmtypes/bot_core.hpp>

// opencv-utils includes
#include <perception_opencv_utils/opencv_utils.hpp>

// lcm log player wrapper
#include <lcm-utils/lcm-reader-util.hpp>

// optargs
#include <ConciseArgs>

// threads
#include <fs-utils/thread_safe_grabber.hpp>

#include <thread>
#include <unistd.h>
// // April tags detector and various families that can be selected by command line option
// #include <AprilTags/TagDetector.h>
// #include <AprilTags/Tag16h5.h>
// #include <AprilTags/Tag25h7.h>
// #include <AprilTags/Tag25h9.h>
// #include <AprilTags/Tag36h9.h>
// #include <AprilTags/Tag36h11.h>

using namespace std;
using namespace cv;


//----------------------------------
// State representation
//----------------------------------
struct state_t {
  lcm::LCM lcm;
  cv::VideoWriter writer;
  ThreadSafeGrabber<kinect::frame_msg_t> grabber;
  // AprilTags::TagDetector* m_tagDetector;
  // AprilTags::TagCodes m_tagCodes;
  fs::vision::DenseTrajectories dtraj; 

  void run(const kinect::frame_msg_t& msg);
  void process(const cv::Mat& img);

  state_t() 
      // m_tagDetector(NULL),
      // m_tagCodes(AprilTags::tagCodes36h11)
  {
    //----------------------------------
    // Setup tag detector
    //----------------------------------
    // m_tagDetector = new AprilTags::TagDetector(m_tagCodes);
  }

  ~state_t() { 
    // if (m_tagDetector) delete m_tagDetector;
  }

  void* ptr() { 
    return (void*) this;
  }

  void on_image_frame (const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                       const bot_core::image_t *msg);

  void on_kinect_image_frame (const lcm::ReceiveBuffer* rbuf, 
                              const std::string& chan,
                              const kinect::frame_msg_t *msg);
};
state_t state;

struct DenseTrajectoriesOptions { 
    bool vLIVE_MODE;
    bool vCREATE_VIDEO;
    bool vDEBUG;
    int vSTART_FRAME, vEND_FRAME, vPLOT;
    float vMAX_FPS;
    float vSCALE;
    std::string vLOG_FILENAME;
    std::string vFEATS_FILENAME;
    std::string vCHANNEL;

    DenseTrajectoriesOptions () : 
        vLOG_FILENAME(""), vFEATS_FILENAME(""), vCHANNEL("KINECT_FRAME"), 
        vMAX_FPS(30.f) , vSTART_FRAME(0), vEND_FRAME(-1), vSCALE(1.f), 
        vDEBUG(false), vLIVE_MODE(false), vCREATE_VIDEO(false), vPLOT(1) {}
};
DenseTrajectoriesOptions options;

// LCM Log player wrapper
LCMLogReaderOptions poptions;
LCMLogReader player;


void state_t::on_image_frame (const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                              const bot_core::image_t *msg) {

    //----------------------------------
    // Unpack 
    //----------------------------------
    double t1 = bot_timestamp_now();
    cv::Mat img = cv::Mat(msg->height, msg->width, CV_8UC3, (void*) &msg->data[0]).clone();
    printf("===> UNPACK TIME: %4.2f ms\n", (bot_timestamp_now() - t1) * 1e-3); 
    std::cerr << "rgb: " << img.size() << std::endl;

    cv::resize(img, img, cv::Size(int(img.cols * options.vSCALE), int(img.rows * options.vSCALE)), 
               0, 0, cv::INTER_NEAREST);

    //----------------------------------
    // Process
    //----------------------------------
    process(img);

}


static void on_kinect_image_frame(const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                                   const kinect::frame_msg_t *msg) {
    state.on_kinect_image_frame(rbuf, chan, msg);
}


void state_t::on_kinect_image_frame (const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                                     const kinect::frame_msg_t *msg) {
  run(*msg);
}

void state_t::run(const kinect::frame_msg_t& msg) {
  
  printf("===> process\n");   

  //----------------------------------
  // Unpack Point cloud
  //----------------------------------
  cv::Mat img;
  double t1 = bot_timestamp_now();
  opencv_utils::unpack_kinect_frame(&msg, img, options.vSCALE);
  printf("===> UNPACK TIME: %4.2f ms\n", (bot_timestamp_now() - t1) * 1e-3); 
  std::cerr << "rgb: " << img.size() << std::endl;
  
  //----------------------------------
  // Process
  //----------------------------------
  process(img);
  
}


void state_t::process(const cv::Mat& img) { 


    //----------------------------------
    // Mask out only regions that require detection
    //----------------------------------
    // detect April tags (requires a gray scale image)
  cv::Mat mask = cv::Mat::zeros(img.size(), CV_8UC1); mask = 255;

  // cv::Mat gray;
  // cv::cvtColor(img, gray, COLOR_BGR2GRAY);
  // vector<AprilTags::TagDetection> detections = m_tagDetector->extractTags(gray);

  // // print out each detection
  // cout << detections.size() << " tags detected:" << endl;
  // for (int i=0; i<detections.size(); i++) {

  //   const AprilTags::TagDetection& detection = detections[i];
    
  //   // use corner points detected by line intersection
  //   std::pair<float, float> p1 = detection.p[0];
  //   std::pair<float, float> p2 = detection.p[1];
  //   std::pair<float, float> p3 = detection.p[2];
  //   std::pair<float, float> p4 = detection.p[3];

  //   std::vector<cv::Point> pts(4);
  //   pts[0].x = p1.first, pts[0].y = p1.second;
  //   pts[1].x = p2.first, pts[1].y = p2.second;
  //   pts[2].x = p3.first, pts[2].y = p3.second;
  //   pts[3].x = p4.first, pts[3].y = p4.second;
  //   std::vector<std::vector<cv::Point> > ppts(1, pts);
  //   cv::drawContours( mask, ppts, 0, cv::Scalar(255), CV_FILLED, 8 );        
  // }
  // cv::imshow("mask", mask);

  //----------------------------------
  // Main dense trajectories call
  //----------------------------------
  double t1 = bot_timestamp_now();

  dtraj.processImage(img, mask);
  printf("===> TOTAL TIME: %4.2f ms\n", (bot_timestamp_now() - t1) * 1e-3); 
  cv::waitKey(10);

  // //----------------------------------
  // // Create video writer
  // //----------------------------------
  // if (options.vCREATE_VIDEO && options.vLOG_FILENAME != "") {
  //   if (!writer.isOpened()) { 
  //     writer.open(options.vLOG_FILENAME + "-video.avi", VideoWriter::fourcc('M','P','4','2'),
  //                 30, out_img.size(), 1);
  //     std::cerr << "===> Video writer created" << std::endl;
  //   }


  //   writer << out_img;
  //   std::cerr << "writing" << std::endl;
  // }

  // opencv_utils::write("dense-trajectories-test.avi");
  return;
}

int main( int argc, char** argv )
{

    //----------------------------------
    // Opt args
    //----------------------------------
    ConciseArgs opt(argc, (char**)argv);
    opt.add(options.vLOG_FILENAME, "l", "log-file","Log file name");
    opt.add(options.vFEATS_FILENAME, "f", "feats-file","Feats file name");
    opt.add(options.vCREATE_VIDEO, "v", "create-video","Create video");
    opt.add(options.vCHANNEL, "c", "channel","Kinect Channel");
    opt.add(options.vMAX_FPS, "r", "max-rate","Max FPS");
    opt.add(options.vSCALE, "s", "scale","Scale");    
    opt.add(options.vPLOT, "p", "plot","Plot Viz");
    opt.parse();

    // Handle special args cases
    if (options.vLIVE_MODE) { 
        options.vCREATE_VIDEO = false;
    }

    //----------------------------------
    // args output
    //----------------------------------
    std::cerr << "===========  Dense Trajectories ============" << std::endl;
    std::cerr << "MODES 1: dense-trajectories -l log-file -f feats-file -v <create video> -r max-rate\n";
    std::cerr << "=============================================\n";
    if (options.vLOG_FILENAME != "") { 
        std::cerr << "=> LOG FILENAME : " << options.vLOG_FILENAME << std::endl;    
    } else { 
        std::cerr << "=> LIVE MODE" << std::endl; options.vLIVE_MODE = true;
        std::cerr << "=> FEATS FILENAME : " << options.vFEATS_FILENAME << std::endl;    
    }
    std::cerr << "=> CREATE VIDEO : " << options.vCREATE_VIDEO << std::endl;
    std::cerr << "=> CHANNEL : " << options.vCHANNEL << std::endl;
    std::cerr << "=> MAX FPS : " << options.vMAX_FPS << std::endl;
    std::cerr << "=> SCALE : " << options.vSCALE << std::endl;
    std::cerr << "=> PLOT : " << options.vPLOT << std::endl;
    // std::cerr << "=> DEBUG : " << options.vDEBUG << std::endl;
    std::cerr << "=> Note: Hit 'space' to proceed to next frame" << std::endl;
    std::cerr << "=> Note: Hit 'p' to proceed to previous frame" << std::endl;
    std::cerr << "=> Note: Hit 'n' to proceed to previous frame" << std::endl;
    std::cerr << "===============================================" << std::endl;

    //----------------------------------
    // Setup Player Options
    //----------------------------------
    poptions.fn = options.vLOG_FILENAME;
    poptions.ch = options.vCHANNEL;
    poptions.fps = options.vMAX_FPS;
    poptions.start_frame = options.vSTART_FRAME;
    poptions.end_frame = options.vEND_FRAME;

    poptions.lcm = &state.lcm;
    poptions.handler = &on_kinect_image_frame;
    poptions.user_data = state.ptr();

    //----------------------------------
    // Subscribe, and start main loop
    //----------------------------------
    if (options.vLIVE_MODE) { 
      // state.lcm.subscribe(options.vCHANNEL, &state_t::on_kinect_image_frame, &state );
      // state.lcm.subscribe(options.vCHANNEL, &state_t::on_image_frame, &state );

      // Set grabber to subscribe to KINECT_FRAME, and set callback
      state.grabber.run = boost::bind(&state_t::run, &state, _1);
      state.grabber.subscribe("KINECT_FRAME");

      while (state.lcm.handle() == 0);
    } else { 
        player.init(poptions); 
    }

    //----------------------------------
    // Save KLT features (for log playback mode)
    //----------------------------------
    // if (options.vFEATS_FILENAME != "") dtraj.write(); 

    return 0;
}
