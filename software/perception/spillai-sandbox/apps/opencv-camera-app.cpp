// lcm
#include <lcm/lcm-cpp.hpp>

// libbot/lcm includes
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>

// libbot cpp
#include <lcmtypes/bot_core.hpp>

// threads
#include <thread>
#include <unistd.h>

// opencv-utils includes
#include <perception_opencv_utils/opencv_utils.hpp>

// optargs
#include <ConciseArgs>

using namespace std;
using namespace cv;


struct OpenCVCameraOptions { 
  bool vDEBUG;
  int vDEVICE_ID;
  int vWIDTH;
  std::string vCHANNEL;

    OpenCVCameraOptions () : 
        vDEVICE_ID(0), vDEBUG(true), vWIDTH(640), vCHANNEL("CAMERA") {}
};
OpenCVCameraOptions options;

struct state_t {
    lcm::LCM lcm;

    std::thread grabth;
    std::thread displayth;

    std::mutex m;
    std::condition_variable grabcond;

    cv::VideoCapture cap;
    std::vector<cv::Mat> img_queue;
    cv::Mat img;
    
    bool new_frame;
    int64_t frame;

    state_t(const int vDEVICE_ID) 
    {
        // Init
        init(vDEVICE_ID);


        // Grab & Display threads
        grabth = std::thread(&state_t::grab, this);
        displayth = std::thread(&state_t::display, this);


    }

    ~state_t() { 
        grabth.join();
        displayth.join();
    }

    void* ptr() { 
        return (void*) this;
    }
    
    void grab() { 
        // try { 
            while (1) {
                // std::this_thread::interruption_point();

                if (!cap.grab()) { 
                    usleep(10*1e3); // 10 ms
                    continue;
                }
            
                // Try locking
                if (m.try_lock()) { 

                    // Retrieve image
                    cap.retrieve(img);

                    new_frame = true;
                
                    // Unlock mutex
                    m.unlock();

                    // Notify display thread
                    grabcond.notify_one();
                }

            }
        // } catch (std::thread_interrupted& ) { 
        //     std::cerr << "grab()" << std::endl;
        //     std::cerr << "\t Thread stopped" << std::endl;
        //     cap.release();
        //     return;            
        // }

    }
    
    void init(const int id) { 
        // std::lock_guard<std::mutex> lock(m);
        std::cerr << "Initializing: " << id << std::endl;

        new_frame = true;
        frame = 0; 

        if (!cap.open(id))
            throw std::runtime_error(std::string("Failed to init camera: " + id));

        cap.set(CAP_PROP_FRAME_WIDTH, options.vWIDTH);
        cap.set(CAP_PROP_FRAME_HEIGHT, int(options.vWIDTH * 3/4));
        
        // Capture
        cap >> img;
        return;
    }
    
    void display() { 
        
        while (1) { 
            // Lock access
            std::unique_lock<mutex> lk(m);

            while (!new_frame) { 
                // Wait for grab done notification
                grabcond.wait(lk);
            }

            new_frame = false;

            if (options.vDEBUG) { 
                cv::Mat _img = img.clone();
                cv::imshow("img", _img);
                cv::waitKey(10);
            }
            
            cv::cvtColor(img, img, cv::COLOR_BGR2RGB);

            int compressed_size = img.cols * img.rows * img.channels();
              
            cv::Mat3b img_(img.size());
            int compression_status = jpeg_compress_8u_rgb
                (img.data, img.cols, img.rows,
                 img.cols * img.channels(),
                 img_.data, &compressed_size, 95);
            
            // Publish image
            int ch = img_.channels();
            bot_core::image_t bot_img;
            bot_img.utime = bot_timestamp_now();
            bot_img.width = img_.cols;
            bot_img.height = img_.rows;
            bot_img.row_stride = img_.cols * ch; 
            bot_img.pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG;
            bot_img.row_stride = img_.cols * ch; 
            bot_img.size = compressed_size;
            uint8_t* imgp = img_.ptr<uint8_t>(0);
            bot_img.data = std::vector<uint8_t>(imgp, imgp + bot_img.size);
            bot_img.nmetadata = 0;

            lcm.publish(options.vCHANNEL, &bot_img);

        }

    }


};

int main( int argc, char** argv )
{

    //----------------------------------
    // Opt args
    //----------------------------------
    ConciseArgs opt(argc, (char**)argv);
    opt.add(options.vDEVICE_ID, "i", "device-id","Device ID");
    opt.add(options.vDEBUG, "d", "debug","Debug");
    opt.add(options.vWIDTH, "w", "width","Width (w x h)");
    opt.add(options.vCHANNEL, "c", "channel","Channel");
    opt.parse();

    //----------------------------------
    // args output
    //----------------------------------
    std::cerr << "===========  OpenCV Camera APP ============" << std::endl;
    std::cerr << "MODES 1: opencv-camera-app -i <device_id> -d <debug 0/1>\n";
    std::cerr << "=============================================\n";
    std::cerr << "=> DEVICE_ID : " << options.vDEVICE_ID << std::endl;
    std::cerr << "=> DEBUG : " << options.vDEBUG << std::endl;
    std::cerr << "=> CHANNEL : " << options.vCHANNEL << std::endl;
    std::cerr << "===============================================" << std::endl;


    //----------------------------------
    // State representation
    //----------------------------------
    state_t state(options.vDEVICE_ID);

    return 0;
}
