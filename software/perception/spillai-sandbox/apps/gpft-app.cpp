// gpft
#include <gpft/general_purpose_feature_tracker2.hpp>

// lcm
#include <lcm/lcm-cpp.hpp>
#include <lcmtypes/bot2_param.h>

// libbot/lcm includes
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>

// libbot cpp
#include <lcmtypes/bot_core.hpp>

// opencv-utils includes
#include <perception_opencv_utils/opencv_utils.hpp>

// lcm log player wrapper
#include <lcm-utils/lcm-reader-util.hpp>

// optargs
#include <ConciseArgs>

// Profiler
#include <fs-utils/profiler.hpp>

// pcl-utils includes
#include <pcl-utils/pcl_utils.hpp>

// pose utils
#include <pcl-utils/pose_utils.hpp>

// frame utils
#include <pcl-utils/frame_utils.hpp>

// vis-utils includes
#include <vis-utils/vis_utils.hpp>


// April tags detector and various families that can be selected by command line option
#include <AprilTags/TagDetector.h>
#include <AprilTags/Tag16h5.h>
#include <AprilTags/Tag25h7.h>
#include <AprilTags/Tag25h9.h>
#include <AprilTags/Tag36h9.h>
#include <AprilTags/Tag36h11.h>

// threads
#include <fs-utils/thread_safe_grabber.hpp>



// using namespace std;
// using namespace cv;


//----------------------------------
// State representation
//----------------------------------
struct state_t {
  lcm::LCM lcm;
  // ThreadSafeGrabber<kinect::frame_msg_t> grabber;
  
  BotParam   *b_server;
  BotFrames *b_frames;

  
  AprilTags::TagDetector* m_tagDetector;
  AprilTags::TagCodes m_tagCodes;
  

  cv::VideoWriter writer;

  fsvision::Profiler profiler;

  fsvision::GeneralPurposeFeatureTracker gpft;
  // fsvision::KeyPointTracker kptracker;

  void run(const kinect::frame_msg_t& msg);
  
  state_t()
      : b_server(NULL),
        b_frames(NULL),
                        
        m_tagDetector(NULL),
        m_tagCodes(AprilTags::tagCodes36h11)

  {
    // grabber.run = boost::bind(&state_t::run, this, _1);
    // grabber.subscribe("KINECT_FRAME");

    //----------------------------------
    // Bot Param/frames init
    //----------------------------------
    b_server = bot_param_new_from_server(lcm.getUnderlyingLCM(), 1);
    b_frames = bot_frames_get_global (lcm.getUnderlyingLCM(), b_server);
    vs::reset_collections_t reset;
    lcm.publish("RESET_COLLECTIONS", &reset);

    //----------------------------------
    // Setup tag detector
    //----------------------------------
    m_tagDetector = new AprilTags::TagDetector(m_tagCodes);

    
    profiler.setName("GPFT");

    // kptracker.setDistanceThreshold(0.05);
    // kptracker.setAngleThreshold(30.f * CV_PI / 180);
  }

  ~state_t() { 
  }

  void* ptr() { 
    return (void*) this;
  }
    
  void process(const cv::Mat& img);

  void on_kinect_image_frame (const lcm::ReceiveBuffer* rbuf, 
                              const std::string& chan,
                              const kinect::frame_msg_t *msg);
  void extract_and_mask_tags(opencv_utils::Frame& frame);
  pose_utils::pose_t transform_to_local_frame(const pose_utils::pose_t& tag_pose);
};
state_t state;
 
struct GPFTOptions { 
    bool vLIVE_MODE;
    bool vCREATE_VIDEO;
    bool vDEBUG;
    float vMAX_FPS;
    float vSCALE;
    std::string vLOG_FILENAME;
    std::string vCHANNEL;

    GPFTOptions () : 
        vLOG_FILENAME(""), vCHANNEL("KINECT_FRAME"), 
        vMAX_FPS(30.f) , vSCALE(1.f), 
        vDEBUG(false), vLIVE_MODE(false), vCREATE_VIDEO(false) {}
};
GPFTOptions options;

// LCM Log player wrapper
LCMLogReaderOptions poptions;
LCMLogReader player;



pose_utils::pose_t
state_t::transform_to_local_frame(const pose_utils::pose_t& tag_pose) { 
    BotTrans sensor_frame;
    bot_frames_get_trans_with_utime (b_frames, "KINECT", "local", tag_pose.utime, &sensor_frame);

    pose_utils::pose_t sensor_pose(sensor_frame);
    pose_utils::pose_t tag_pose_tf = sensor_pose * tag_pose;
    tag_pose_tf.id = tag_pose.id;
    tag_pose_tf.utime = tag_pose.utime;
    return tag_pose_tf;
}


static pose_utils::pose_t compute_tag_pose(opencv_utils::Frame& frame, 
                                           AprilTags::TagDetection& detection) { 

    // use corner points detected by line intersection
    std::pair<float, float> p1 = detection.p[0];
    std::pair<float, float> p2 = detection.p[1];
    std::pair<float, float> p3 = detection.p[2];
    std::pair<float, float> p4 = detection.p[3];

    cv::Mat3f cloud = frame.getCloudRef();
    cv::Vec3f p = cloud(p3.second,p3.first);
    cv::Vec3f v1 = cv::Vec3f(cloud(p4.second,p4.first)) - cv::Vec3f(cloud(p3.second,p3.first));
    v1 *= 1.0 / cv::norm(v1);
    cv::Vec3f v2 = cv::Vec3f(cloud(p2.second,p2.first)) - cv::Vec3f(cloud(p3.second,p3.first));
    v2 *= 1.0 / cv::norm(v2);

    pose_utils::pose_t tag_pose(frame.getTimestamp(), p, v1, v2);
    tag_pose.id = detection.id;
    // std::cerr << "id: " << detection.id << std::endl;

    return tag_pose;
}

// Image gets modified
void 
state_t::extract_and_mask_tags(opencv_utils::Frame& frame) {
  // detect April tags (requires a gray scale image)
  std::vector<AprilTags::TagDetection> detections = m_tagDetector->extractTags(frame.getGrayRef());


  std::vector<std::vector<cv::Point> > mask_pts;
  std::vector<pose_utils::pose_t> poses(detections.size());
  for (int j=0; j<detections.size(); j++) {
    AprilTags::TagDetection& detection = detections[j];

    std::vector<cv::Point> pts(4);
    for (int k=0; k<4; k++)
      pts[k] = cv::Point(detection.p[k].first, detection.p[k].second);

    // Scale up points before masking
    pts = opencv_utils::scale_contour(pts, 1.8);

    // Add to mask
    mask_pts.push_back(pts);
    
    // Compute Tag Pose
    poses[j] = compute_tag_pose(frame, detection);
  }

  // cv::Mat3b inpainted = frame.getRGBRef().clone();
  // cv::fillPoly(inpainted, mask_pts, cv::Scalar(0,0,255), CV_AA, 0);
  // cv::addWeighted( frame.getRGBRef(), 0.5, inpainted, 0.5, 0, inpainted );
  // cv::imshow("inpainted", inpainted);

  cv::Mat1b mask(frame.getRGBRef().size()); mask = 0;
  cv::fillPoly(mask, mask_pts, cv::Scalar(255), CV_AA, 0);
  // cv::imshow("mask-ply", mask);
  
  // Invert
  cv::bitwise_not(mask, mask);
  frame.setImageMask(mask);
  
  //----------------------------------
  // Publish poses
  //----------------------------------
  vs::obj_collection_t objs_msg; 
  objs_msg.id = 10010; 
  objs_msg.name = "APRILTAGS"; 
  objs_msg.type = VS_OBJ_COLLECTION_T_AXIS3D; 
  objs_msg.reset = true; 
  objs_msg.objs.resize(poses.size());
  
  // Transform Pose to Sensor Frame
  for (int j=0; j<poses.size(); j++) { 
    pose_utils::pose_t tag_pose_tf = transform_to_local_frame(poses[j]);
    double rpy[3]; bot_quat_to_roll_pitch_yaw (tag_pose_tf.orientation, rpy);

    // Populate Pose 
    objs_msg.objs[j].id = tag_pose_tf.id; 
    objs_msg.objs[j].x = tag_pose_tf.pos[0], objs_msg.objs[j].y = tag_pose_tf.pos[1], 
        objs_msg.objs[j].z = tag_pose_tf.pos[2];
    objs_msg.objs[j].roll = rpy[0], objs_msg.objs[j].pitch = rpy[1], 
        objs_msg.objs[j].yaw = rpy[2];
  }
  objs_msg.nobjs = objs_msg.objs.size(); 
  lcm.publish("OBJ_COLLECTION", &objs_msg);
  
  
}



static void on_kinect_image_frame(const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                                   const kinect::frame_msg_t *msg) {
    state.on_kinect_image_frame(rbuf, chan, msg);
}


void state_t::on_kinect_image_frame (const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                                   const kinect::frame_msg_t *msg) {

  run(*msg);
}

void state_t::run(const kinect::frame_msg_t& msg) {

    profiler.enter("on_kinect");

    //----------------------------------
    // Publish Camera frame
    //----------------------------------
    int64_t sensor_id = vis_utils::publish_camera_frame(msg.timestamp);

    //----------------------------------
    // Unpack Point cloud
    //----------------------------------
    profiler.enter("unpack()");
    opencv_utils::Frame frame(&msg, options.vSCALE);
    profiler.leave("unpack()");

    // Prune at depth and set mask 
    // profiler.enter("PruneDepth()");
    // frame.pruneDepth(10.f);
    // profiler.leave("PruneDepth()");

    
    // Compute normals (and mask)
    // profiler.enter("computeNormals()");
    // frame.computeNormals(0.25, 0.01, 10.f);
    // profiler.leave("computeNormals()");

    // //----------------------------------
    // // Extract tags if any
    // //----------------------------------
    // profiler.enter("extractTags()");
    // extract_and_mask_tags(frame);
    // profiler.leave("extractTags()");

    //----------------------------------
    // Main GPFT computation    // Provide depth+normals mask
    //----------------------------------
    gpft.processFrame(frame);

    const int min_track_size = 1;
    std::vector<fsvision::Feature3D> fpts = gpft.getAllFeatures(min_track_size);

    // //----------------------------------
    // // KeyPoint tracking
    // //----------------------------------
    // kptracker.update(frame, fpts);
    // fpts = kptracker.getStableFeatures(10);

    //----------------------------------
    // KeyPoint viz
    //----------------------------------
    cv::Mat3b out; 
    cv::Mat mask; // = frame.getCombinedMaskRef();
    frame.getRGBRef().copyTo(out); 

    // Colors
    std::vector<cv::Vec3b> colors(30);
    opencv_utils::fillColors( colors );
    
    fsvision::Feature3D pfpt;
    for (auto& fpt: fpts) {
      const cv::KeyPoint& kp = fpt.get_keypoint();
      cv::Point2f tvec = cv::Point2f(kp.size, kp.response) * 5;

      // cv::line(out, kp.pt + tvec, kp.pt, cv::Scalar(0,0,255), 1, CV_AA);
      cv::Scalar col = colors[fpt.get_id()%colors.size()];
      // cv::Scalar col = colors[fpt.get_id()%colors.size()];
      // cv::circle(out, kp.pt, 5, cv::Scalar(0,128,0), 1, CV_AA );

      // Draw correspondences between frames
      if (pfpt.get_id() == fpt.get_id()) {
        cv::line(out, kp.pt, pfpt.get_point(), col, 1, CV_AA);
      } else {
        cv::circle(out, kp.pt, 2, col, 1, CV_AA );
      }
      // copy 
      pfpt = fpt;

      // cv::addText(out, cv::format("%3.2f",gpft.track_manager_.track_add_rate_), 
      //         cv::Point(20,20), cv::fontQt("Times")); 
      // cv::addText(out, cv::format("%i",gpft.track_manager_.tracklet_map_.size()),
      //         cv::Point(20,40), cv::fontQt("Times")); 
      
    }
    cv::imshow("GPFT", out);
  
    // cv::waitKey(1);
    profiler.leave("on_kinect");
}

void state_t::process(const cv::Mat& img) { 

    double t1 = bot_timestamp_now();


    // //----------------------------------
    // // Create video writer
    // //----------------------------------
    // if (options.vCREATE_VIDEO && options.vLOG_FILENAME != "") {
    //     if (!writer.isOpened()) { 
    //         writer.open(options.vLOG_FILENAME + "-video.avi", VideoWriter::fourcc('M','P','4','2'),
    //                            30, out_img.size(), 1);
    //         std::cerr << "===> Video writer created" << std::endl;
    //     }
    //     writer << out_img;
    //     std::cerr << "writing" << std::endl;
    // }
    return;
}

void  INThandler(int sig) {
    printf("Exiting\n");
    exit(0);
}

int main( int argc, char** argv )
{

  
    //----------------------------------
    // Opt args
    //----------------------------------
    ConciseArgs opt(argc, (char**)argv);
    opt.add(options.vLOG_FILENAME, "l", "log-file","Log file name");
    opt.add(options.vCREATE_VIDEO, "v", "create-video","Create video");
    opt.add(options.vCHANNEL, "c", "channel","Kinect Channel");
    opt.add(options.vMAX_FPS, "r", "max-rate","Max FPS");
    opt.add(options.vSCALE, "s", "scale","Scale");    
    opt.parse();

    // Handle special args cases
    if (options.vLIVE_MODE) { 
        options.vCREATE_VIDEO = false;
    }

    //----------------------------------
    // args output
    //----------------------------------
    std::cerr << "===========  GPFT app ============" << std::endl;
    std::cerr << "MODES 1: gpft -l log-file -v <create video> -r max-rate\n";
    std::cerr << "=============================================\n";
    if (options.vLOG_FILENAME != "") { 
        std::cerr << "=> LOG FILENAME : " << options.vLOG_FILENAME << std::endl;    
    } else { 
        std::cerr << "=> LIVE MODE" << std::endl; options.vLIVE_MODE = true;
    }
    std::cerr << "=> CREATE VIDEO : " << options.vCREATE_VIDEO << std::endl;
    std::cerr << "=> CHANNEL : " << options.vCHANNEL << std::endl;
    std::cerr << "=> MAX FPS : " << options.vMAX_FPS << std::endl;
    std::cerr << "=> SCALE : " << options.vSCALE << std::endl;

    std::cerr << "===============================================" << std::endl;

    //----------------------------------
    // Setup Player Options
    //----------------------------------
    poptions.fn = options.vLOG_FILENAME;
    poptions.ch = options.vCHANNEL;
    poptions.fps = options.vMAX_FPS;
    poptions.lcm = &state.lcm;
    poptions.handler = on_kinect_image_frame;
    poptions.user_data = state.ptr();

    //----------------------------------
    // GPFT Inits
    //----------------------------------
    // state.gpft.setScale(options.vSCALE);
    cv::namedWindow("GPFT");
    cv::moveWindow("GPFT", 2100, 400);

    cv::namedWindow("match");
    cv::moveWindow("match", 2740, 400);
    
    //----------------------------------
    // Subscribe, and start main loop
    //----------------------------------
    signal(SIGINT, INThandler);    
    if (options.vLIVE_MODE) {
      state.lcm.subscribe(options.vCHANNEL, &state_t::on_kinect_image_frame, &state ); 
      do {
        unsigned char c = cv::waitKey(10) & 0xff;
        if (c == 'q') break; 
      } while (state.lcm.handle() == 0);
      
    } else { 
        player.init(poptions); 
    }

    return 0;
}
