// April tags detector and various families that can be selected by command line option
#include <AprilTags/TagDetector.h>
#include <AprilTags/Tag16h5.h>
#include <AprilTags/Tag25h7.h>
#include <AprilTags/Tag25h9.h>
#include <AprilTags/Tag36h9.h>
#include <AprilTags/Tag36h11.h>

// lcm
#include <lcm/lcm.h>
#include <lcmtypes/bot2_param.h>

// libbot/lcm includes
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>

// opencv-utils includes
#include <perception_opencv_utils/opencv_utils.hpp>

// frame utils
#include <pcl-utils/frame_utils.hpp>
#include <pcl-utils/pose_utils.hpp>

// lcm log player wrapper
#include <lcm-utils/lcm-reader-util.hpp>

// optargs
#include <ConciseArgs>

// frame utils
#include <features3d/feature_types.hpp>
#include <pcl-utils/frame_utils.hpp>

// Profiler
#include <fs-utils/profiler.hpp>

// vis-utils includes
#include <vis-utils/vis_utils.hpp>

using namespace std;
using namespace cv;


#ifndef PI
const double PI = 3.14159265358979323846;
#endif
const double TWOPI = 2.0*PI;

/**
 * Normalize angle to be within the interval [-pi,pi].
 */
inline double standardRad(double t) {
  if (t >= 0.) {
    t = fmod(t+PI, TWOPI) - PI;
  } else {
    t = fmod(t-PI, -TWOPI) + PI;
  }
  return t;
}

void wRo_to_euler(const Eigen::Matrix3d& wRo, double& yaw, double& pitch, double& roll) {
    yaw = standardRad(atan2(wRo(1,0), wRo(0,0)));
    double c = cos(yaw);
    double s = sin(yaw);
    pitch = standardRad(atan2(-wRo(2,0), wRo(0,0)*c + wRo(1,0)*s));
    roll  = standardRad(atan2(wRo(0,2)*s - wRo(1,2)*c, -wRo(0,1)*s + wRo(1,1)*c));
}

struct state_t {
  lcm::LCM lcm;

  BotParam   *b_server;
  BotFrames *b_frames;


  cv::VideoWriter writer;

  fsvision::Profiler profiler;
  AprilTags::TagDetector* m_tagDetector;
  AprilTags::TagCodes m_tagCodes;

  int m_width; // image size in pixels
  int m_height;
  double m_tagSize; // April tag side length in meters of square black frame
  double m_fx; // camera focal length in pixels
  double m_fy;
  double m_px; // camera principal point
  double m_py;


  state_t() : 

              m_tagDetector(NULL),
              m_tagCodes(AprilTags::tagCodes36h11), 

              m_width(640),
              m_height(480),
              m_tagSize(0.166),
              m_fx(528.49404721),
              m_fy(528.49404721),
              m_px(m_width/2-0.5),
              m_py(m_height/2-0.5) 
  {
    profiler.setName("AprilTags-APP");

    //----------------------------------
    // Bot Param/frames init
    //----------------------------------
    b_server = bot_param_new_from_server(lcm.getUnderlyingLCM(), 1);
    b_frames = bot_frames_get_global (lcm.getUnderlyingLCM(), b_server);

  }

    ~state_t() { 
        if (m_tagDetector) delete m_tagDetector;
    }

    void* ptr() { 
        return (void*) this;
    }

  std::vector<fsvision::Feature3D>
  apriltags_extractFeatures(const opencv_utils::Frame& frame);

  pose_utils::pose_t
  transform_to_local_frame(const pose_utils::pose_t& tag_pose);

  
    void on_kinect_image_frame (const lcm::ReceiveBuffer* rbuf, 
                              const std::string& chan,
                              const kinect::frame_msg_t *msg);

    void print_detection(AprilTags::TagDetection& detection) {
        cout << "  Id: " << detection.id
             << " (Hamming: " << detection.hammingDistance << ")";

        // recovering the relative pose of a tag:

        // NOTE: for this to be accurate, it is necessary to use the
        // actual camera parameters here as well as the actual tag size
        // (m_fx, m_fy, m_px, m_py, m_tagSize)

        Eigen::Vector3d translation;
        Eigen::Matrix3d rotation;
        detection.getRelativeTranslationRotation(m_tagSize, m_fx, m_fy, m_px, m_py,
                                                 translation, rotation);

        Eigen::Matrix3d F;
        F <<
            1, 0,  0,
            0,  -1,  0,
            0,  0,  1;
        Eigen::Matrix3d fixed_rot = F*rotation;
        double yaw, pitch, roll;
        wRo_to_euler(fixed_rot, yaw, pitch, roll);

        cout << "  distance=" << translation.norm()
             << "m, x=" << translation(0)
             << ", y=" << translation(1)
             << ", z=" << translation(2)
             << ", yaw=" << yaw
             << ", pitch=" << pitch
             << ", roll=" << roll
             << endl;

        // Also note that for SLAM/multi-view application it is better to
        // use reprojection error of corner points, because the noise in
        // this relative pose is very non-Gaussian; see iSAM source code
        // for suitable factors.
    }

};
state_t state;

struct AprilTagsOptions { 
    bool vLIVE_MODE;
    bool vCREATE_VIDEO;
    bool vDEBUG;
    int vPLOT;
    float vMAX_FPS;
    float vSCALE;
    std::string vLOG_FILENAME;
    std::string vCHANNEL;

    AprilTagsOptions () : 
        vLOG_FILENAME(""), vCHANNEL("KINECT_FRAME"), 
        vMAX_FPS(30.f) , vSCALE(1.f), vDEBUG(true), 
        vLIVE_MODE(false), vCREATE_VIDEO(false), vPLOT(1) {}
};
AprilTagsOptions options;

// LCM Log player wrapper
LCMLogReaderOptions poptions;
LCMLogReader player;

static inline bool in_bounds(const cv::Mat& cloud, const cv::Point2f& p) {
  return (p.x >= 0 && p.x < cloud.cols &&
          p.y >= 0 && p.y < cloud.rows);
}


inline cv::Vec3f mean_val(const cv::Mat3f& normals, const cv::Point2f& p) {
  // Ensure keypoint is valid
  assert(p.x >= 0 && p.x < normals.cols &&
         p.y >= 0 && p.y < normals.rows);

  const int sz = 5; 
  int pxmin = std::max(0, int(p.x - sz/2));
  int pymin = std::max(0, int(p.y - sz/2));
  int pxmax = std::min(normals.cols-1, int(p.x + sz/2)); 
  int pymax = std::min(normals.rows-1, int(p.y + sz/2)); 

  int count = 0; 
  cv::Vec3f normal(0.f,0.f,0.f);
  for (int y=pymin; y<=pymax; y++) {
    for (int x=pxmin; x<=pxmax; x++) {
      const cv::Point3f& n = normals(y,x);
      if (n.x != n.x) continue;
      normal[0] += n.x, normal[1] += n.y, normal[2] += n.z; 
      count++;
    }
  }
  if (count == 0)
    return cv::Vec3f(std::numeric_limits<float>::quiet_NaN(),
                     std::numeric_limits<float>::quiet_NaN(), 
                     std::numeric_limits<float>::quiet_NaN());
  normal *= 1.f / count; 
  return normal;
  
}

std::vector<fsvision::Feature3D>
state_t::apriltags_extractFeatures(const opencv_utils::Frame& frame) {

  profiler.enter("ExtractFeatures()");
  cv::Mat img = frame.getRGBRef().clone();
  const cv::Mat& gray = frame.getGrayRef();
  cv::Mat3f cloud = frame.getCloudRef();
  // cv::Mat3f _normals = frame.getNormalsRef().clone();
  // cv::Mat3f normals; 
  // // cv::GaussianBlur(normals, normals, cv::Size(9,9), 0, 0);
  // cv::bilateralFilter(_normals, normals, 7, 7/2, 7*2);
  // cv::imshow("normals", normals);
  // cv::GaussianBlur(normals, normals, cv::Size(9,9), 0, 0);
  // cv::medianBlur(normals, normals, 15);
  
  // Detect April tags (requires a gray scale image)
  std::vector<AprilTags::TagDetection> detections = m_tagDetector->extractTags(gray);
  std::vector<fsvision::Feature3D> ret;

  int64_t now = frame.getTimestamp();
  // std::cerr << "now: " << now << std::endl;
  for (int j=0; j<detections.size(); j++) {

    std::vector<fsvision::Feature3D> tags;
    for (int k=0; k<4; k++) { 
      // Propagate timestamp, and ID
      cv::Point2f p(detections[j].p[k].first, detections[j].p[k].second);
      cv::Point2f p2(detections[j].p[(k+1)%4].first, detections[j].p[(k+1)%4].second);
      cv::Point2f p3(detections[j].p[(k+2)%4].first, detections[j].p[(k+2)%4].second);

      // If none in bounds, return 
      if (!in_bounds(cloud, p)) continue;
      
      // Initialize feature3d, and propagate keypoint data
      fsvision::Feature3D fpt(now, detections[j].id * 4 + k);
      fpt.set_point(p);
    
      // Handle NaN downstream
      cv::Point3f xyz(mean_val(cloud, p));
      if (xyz.x != xyz.x) continue;

      // cv::Point3f t1(cloud(p2)-cloud(p)), t2(cloud(p3)-cloud(p));
      cv::Point3f t1(mean_val(cloud, p2)-mean_val(cloud, p)),
          t2(mean_val(cloud, p3)-mean_val(cloud, p));
      float t1norm = cv::norm(t1), t2norm = cv::norm(t2);
      // std::cerr << "t1norm: " << t2 << " " << t1 << std::endl;
      if (t1norm == 0 || t2norm == 0 || t1.x != t1.x || t2.x != t2.x) continue;
      t1 *= 1.f / t1norm, t2 *= 1.f / t2norm;

      // cv::Point3f normal(normals(p)); // = mean_val(normals, p);
      cv::Point3f normal = t1.cross(t2);
      float nnorm = cv::norm(normal);
      if (nnorm == 0 || normal.x != normal.x) continue;
      normal *= 1.f / nnorm;
      // std::cerr << "normal: " << normal << std::endl;
      if (normal.x != normal.x) continue;
      fpt.setFeature3D(xyz, normal, t1);

     
      if (detections[j].id == 0 && k == 0)
        std::cerr << detections[j].id * 4 + k
                  << " Normal: " << normal << " tangent: " << t1 << std::endl;

        
        // float ratio = kpts[j].size / kpts[j].response;
        // float xx = cv::norm(evec.first), yy = cv::norm(evec.second);
        // float xx = feat_size*feat_size, yy=feat_size*feat_size; 
        // fpt.setCovariance3D(xx*xx, 0., 0., yy, 0., tnorm*tnorm*ratio*ratio);
        // std::cerr << "covs: " << fpt.covs() << std::endl;

        
        // std::cerr << "cp: " << xyz << " cnormal: " << normal
        //           << " ctp: " << tangent << " dot: " << tangent.dot(normal) << std::endl;
      // } else {
      //   fpt.setFeature3D(xyz, normal);
        // std::cerr << "cp: " << xyz << " cnormal: " << normal << std::endl;
      // }

      // Push Feature3D (xyz/normal/tangent could be NaN)
      tags.push_back(fpt);
    }
    // if (tags.size() == 4)
    ret.insert(ret.end(), tags.begin(), tags.end());
  }
  std::cerr << "features: " << ret.size() << std::endl;
  profiler.leave("ExtractFeatures()");

    // show the current image including any detections
  if (options.vDEBUG) {
    for (int i=0; i<detections.size(); i++) {
      // also highlight in the image
      detections[i].draw(img);
    }
    cv::imshow("AprilTags", img); 
    // cv::Mat3b cdepth = opencv_utils::color_depth_map(depth);
    // cv::imshow("Depth", cdepth); 

    // cv::imshow("cloud", cloud);
  }
    
  return ret;
}


pose_utils::pose_t
state_t::transform_to_local_frame(const pose_utils::pose_t& tag_pose) { 
    BotTrans sensor_frame;
    bot_frames_get_trans_with_utime (b_frames, "KINECT", "local", tag_pose.utime, &sensor_frame);

    pose_utils::pose_t sensor_pose(sensor_frame);
    pose_utils::pose_t tag_pose_tf = sensor_pose * tag_pose;
    tag_pose_tf.id = tag_pose.id;
    tag_pose_tf.utime = tag_pose.utime;
    return tag_pose_tf;
}


static void on_kinect_image_frame(const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                                   const kinect::frame_msg_t *msg) {
    state.on_kinect_image_frame(rbuf, chan, msg);
}

void state_t::on_kinect_image_frame (const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                                   const kinect::frame_msg_t *msg) {

    double t = (double)getTickCount();

    profiler.enter("on_kinect_image_frame");
    //----------------------------------
    // Unpack Point cloud
    //----------------------------------
    cv::Mat img, gray;
    cv::Mat_<uint16_t> depth;
    cv::Mat_<Vec3f> cloud;
    // opencv_utils::unpack_kinect_frame(msg, img, options.vSCALE);
    opencv_utils::Frame frame(msg, options.vSCALE);
    frame.fastBilateralFilter(50.f, 0.05); // sigma_s, sigma_r
    // frame.medianFilter(11); // sigma_s, sigma_r
    vis_utils::publish_cloud("KINECT_DEBUG", frame.getCloudRef(), frame.getRGBRef());
    std::vector<fsvision::Feature3D> fpts = apriltags_extractFeatures(frame);

    //----------------------------------
    // Publish poses
    //----------------------------------
    vs::obj_collection_t objs_msg; 
    objs_msg.id = 10000; 
    objs_msg.name = "POSE_EST"; 
    objs_msg.type = VS_OBJ_COLLECTION_T_AXIS3D; 
    objs_msg.reset = true; 
    objs_msg.objs.resize(fpts.size());

    for (int j=0; j<fpts.size(); j++) {
      assert(fpts[j].xyz_valid() && fpts[j].normal_valid() && fpts[j].xyz_valid());
      
      const cv::Vec3f& pos = fpts[j].xyz();
      const cv::Vec3f& nvec = fpts[j].normal();
      const cv::Vec3f& tvec = fpts[j].tangent();
      const cv::Vec6f& fcov = fpts[j].covs();

      // std::cerr << pos << " " << nvec << " " << tvec << std::endl;
      // if (pos[0] != pos[0] || tvec[0] != tvec[0] || nvec[0] != nvec[0])
      //   continue;

      // Compute pose from observation
      pose_utils::pose_t pt_pose(fpts[j].get_utime(), pos, nvec, tvec);
      if (fpts[j].get_id() == 0) { 
        std::cerr << "pt_pose: " << fpts[j].get_id() << " " << pt_pose << std::endl;
      }
      
      // Transform Pose to Sensor Frame
      pose_utils::pose_t tag_pose_tf = transform_to_local_frame(pt_pose);
      double rpy[3]; bot_quat_to_roll_pitch_yaw (tag_pose_tf.orientation, rpy);

      // Populate Pose 
      objs_msg.objs[j].id = j; // fpts[j].get_id();
      objs_msg.objs[j].x = tag_pose_tf.pos[0], objs_msg.objs[j].y = tag_pose_tf.pos[1], 
          objs_msg.objs[j].z = tag_pose_tf.pos[2];
      objs_msg.objs[j].roll = rpy[0], objs_msg.objs[j].pitch = rpy[1], 
          objs_msg.objs[j].yaw = rpy[2];
    }

    // Visualize poses
    objs_msg.nobjs = objs_msg.objs.size(); 
    lcm.publish("OBJ_COLLECTION", &objs_msg);

    
    
    // img = frame.getRGBRef();
    // // opencv_utils::unpack_kinect_frame(msg, img, depth, options.vSCALE);
    // printf("===> UNPACK TIME: %4.2f ms\n", ((double)getTickCount() - t)/getTickFrequency() * 1e3);     
    // // std::cerr << "depth: " << depth.size() << std::endl;
    // // std::cerr << "cloud: " << cloud.size() << std::endl;

    // //--------------------------------------------
    // // Gaussian blur, and gray
    // //--------------------------------------------
    // // cv::GaussianBlur(img, img, cv::Size(3,3), 0, 0, cv::BORDER_DEFAULT);
    // cv::cvtColor(img, gray, COLOR_BGR2GRAY);


    // // detect April tags (requires a gray scale image)
    // vector<AprilTags::TagDetection> detections = m_tagDetector->extractTags(gray);

    // // print out each detection
    // cout << detections.size() << " tags detected:" << endl;
    // for (int i=0; i<detections.size(); i++) {
    //     print_detection(detections[i]);
    // }

    // // show the current image including any detections
    // if (options.vDEBUG) {
    //     for (int i=0; i<detections.size(); i++) {
    //         // also highlight in the image
    //         detections[i].draw(img);
    //     }
    //     opencv_utils::imshow("AprilTags", img); 
    //     // cv::Mat3b cdepth = opencv_utils::color_depth_map(depth);
    //     // cv::imshow("Depth", cdepth); 

    //     // cv::imshow("cloud", cloud);
    // }

    // print out the frame rate at which image frames are being processed
    // printf("===> TOTAL TIME: %4.2f ms\n", ((double)getTickCount() - t)/getTickFrequency() * 1e3);     
    cv::waitKey(20);

    // //----------------------------------
    // // Create video writer
    // //----------------------------------
    // if (options.vCREATE_VIDEO && options.vLOG_FILENAME != "") {
    //     if (!state->writer.isOpened()) { 
    //         state->writer.open(options.vLOG_FILENAME + "-video.avi", VideoWriter::fourcc('M','P','4','2'),
    //                            30, out_img.size(), 1);
    //         std::cerr << "===> Video writer created" << std::endl;
    //     }
    //     state->writer << out_img;
    //     std::cerr << "writing" << std::endl;
    // }
    profiler.leave("on_kinect_image_frame");
    return;
}

int main( int argc, char** argv )
{
    //----------------------------------
    // Opt args
    //----------------------------------
    ConciseArgs opt(argc, (char**)argv);
    opt.add(options.vLOG_FILENAME, "l", "log-file","Log file name");
    opt.add(options.vCREATE_VIDEO, "v", "create-video","Create video");
    opt.add(options.vCHANNEL, "c", "channel","Kinect Channel");
    opt.add(options.vMAX_FPS, "r", "max-rate","Max FPS");
    opt.add(options.vSCALE, "s", "scale","Scale");    
    opt.add(options.vPLOT, "p", "plot","Plot Viz");
    opt.parse();

    // Handle special args cases
    if (options.vLIVE_MODE) { 
        options.vCREATE_VIDEO = false;
    }

    //----------------------------------
    // args output
    //----------------------------------
    std::cerr << "===========  AprilTags APP ============" << std::endl;
    std::cerr << "MODES 1: apriltags-app -l log-file -v <create video> -r max-rate\n";
    std::cerr << "=============================================\n";
    if (options.vLOG_FILENAME != "") { 
        std::cerr << "=> LOG FILENAME : " << options.vLOG_FILENAME << std::endl;    
    } else { 
        std::cerr << "=> LIVE MODE" << std::endl; options.vLIVE_MODE = true;
    }
    std::cerr << "=> CREATE VIDEO : " << options.vCREATE_VIDEO << std::endl;
    std::cerr << "=> CHANNEL : " << options.vCHANNEL << std::endl;
    std::cerr << "=> MAX FPS : " << options.vMAX_FPS << std::endl;
    std::cerr << "=> SCALE : " << options.vSCALE << std::endl;
    std::cerr << "=> PLOT : " << options.vPLOT << std::endl;

    std::cerr << "=> Note: Hit 'space' to proceed to next frame" << std::endl;
    std::cerr << "=> Note: Hit 'p' to proceed to previous frame" << std::endl;
    std::cerr << "=> Note: Hit 'n' to proceed to previous frame" << std::endl;
    std::cerr << "===============================================" << std::endl;

    //----------------------------------
    // Setup Player Options
    //----------------------------------
    poptions.fn = options.vLOG_FILENAME;
    poptions.ch = options.vCHANNEL;
    poptions.fps = options.vMAX_FPS;
    poptions.lcm = &state.lcm;
    poptions.handler = on_kinect_image_frame;
    poptions.user_data = state.ptr();

    //----------------------------------
    // Setup tag detector
    //----------------------------------
    state.m_tagDetector = new AprilTags::TagDetector(state.m_tagCodes);

    //----------------------------------
    // Subscribe, and start main loop
    //----------------------------------
    if (options.vLIVE_MODE) {
      state.lcm.subscribe(options.vCHANNEL, &state_t::on_kinect_image_frame, &state );
      while (state.lcm.handle() == 0);
    } else { 
        player.init(poptions); 
    }

    return 0;
}
