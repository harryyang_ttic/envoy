// lcm
#include <lcm/lcm-cpp.hpp>
#include <lcmtypes/bot2_param.h>

// libbot/lcm includes
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>

// libbot cpp
#include <lcmtypes/bot_core.hpp>

// opencv-utils includes
#include <perception_opencv_utils/opencv_utils.hpp>

// lcm log player wrapper
#include <lcm-utils/lcm-reader-util.hpp>

// optargs
#include <ConciseArgs>

// Profiler
#include <fs-utils/profiler.hpp>

// pcl-utils includes
#include <pcl-utils/pcl_utils.hpp>

// pose utils
#include <pcl-utils/pose_utils.hpp>

// frame utils
#include <pcl-utils/frame_utils.hpp>

// vis-utils includes
#include <vis-utils/vis_utils.hpp>

// mser3d
#include <mser3d/mser3d.hpp>
#include <features3d/feature_types.hpp>

// April tags detector and various families that can be selected by command line option
#include <AprilTags/TagDetector.h>
#include <AprilTags/Tag16h5.h>
#include <AprilTags/Tag25h7.h>
#include <AprilTags/Tag25h9.h>
#include <AprilTags/Tag36h9.h>
#include <AprilTags/Tag36h11.h>


// using namespace std;
// using namespace cv;

//----------------------------------
// State representation
//----------------------------------
struct state_t {
  lcm::LCM lcm;

  BotParam   *b_server;
  BotFrames *b_frames;

  AprilTags::TagDetector* m_tagDetector;
  AprilTags::TagCodes m_tagCodes;

  
  cv::VideoWriter writer;

  fsvision::Profiler profiler;

  fsvision::MSER3D mser3D;
  
  state_t()
      : b_server(NULL),
        b_frames(NULL),
                
        m_tagDetector(NULL),
        m_tagCodes(AprilTags::tagCodes36h11)

  {

    //----------------------------------
    // Bot Param/frames init
    //----------------------------------
    b_server = bot_param_new_from_server(lcm.getUnderlyingLCM(), 1);
    b_frames = bot_frames_get_global (lcm.getUnderlyingLCM(), b_server);
    vs::reset_collections_t reset;
    lcm.publish("RESET_COLLECTIONS", &reset);

    //----------------------------------
    // Setup tag detector
    //----------------------------------
    m_tagDetector = new AprilTags::TagDetector(m_tagCodes);

    profiler.setName("MSER3D-APP");

  }

  ~state_t() { 
  }

  void* ptr() { 
    return (void*) this;
  }
    
  void process(const cv::Mat& img);

  void on_kinect_image_frame (const lcm::ReceiveBuffer* rbuf, 
                              const std::string& chan,
                              const kinect::frame_msg_t *msg);

  void extract_and_mask_tags(opencv_utils::Frame& frame);
  pose_utils::pose_t transform_to_local_frame(const pose_utils::pose_t& tag_pose);

  
};
state_t state;

struct MSER3DOptions { 
    bool vLIVE_MODE;
    bool vCREATE_VIDEO;
    bool vDEBUG;
    float vMAX_FPS;
    float vSCALE;
    std::string vLOG_FILENAME;
    std::string vCHANNEL;

    MSER3DOptions () : 
        vLOG_FILENAME(""), vCHANNEL("KINECT_FRAME"), 
        vMAX_FPS(30.f) , vSCALE(1.f), 
        vDEBUG(false), vLIVE_MODE(false), vCREATE_VIDEO(false) {}
};
MSER3DOptions options;

// LCM Log player wrapper
LCMLogReaderOptions poptions;
LCMLogReader player;

pose_utils::pose_t
state_t::transform_to_local_frame(const pose_utils::pose_t& tag_pose) { 
    BotTrans sensor_frame;
    bot_frames_get_trans_with_utime (b_frames, "KINECT", "local", tag_pose.utime, &sensor_frame);

    pose_utils::pose_t sensor_pose(sensor_frame);
    pose_utils::pose_t tag_pose_tf = sensor_pose * tag_pose;
    tag_pose_tf.id = tag_pose.id;
    tag_pose_tf.utime = tag_pose.utime;
    return tag_pose_tf;
}


static pose_utils::pose_t compute_tag_pose(opencv_utils::Frame& frame, 
                                           AprilTags::TagDetection& detection) { 

    // use corner points detected by line intersection
    std::pair<float, float> p1 = detection.p[0];
    std::pair<float, float> p2 = detection.p[1];
    std::pair<float, float> p3 = detection.p[2];
    std::pair<float, float> p4 = detection.p[3];

    cv::Mat3f cloud = frame.getCloudRef();
    cv::Vec3f p = cloud(p3.second,p3.first);
    cv::Vec3f v1 = cv::Vec3f(cloud(p4.second,p4.first)) - cv::Vec3f(cloud(p3.second,p3.first));
    v1 *= 1.0 / cv::norm(v1);
    cv::Vec3f v2 = cv::Vec3f(cloud(p2.second,p2.first)) - cv::Vec3f(cloud(p3.second,p3.first));
    v2 *= 1.0 / cv::norm(v2);

    pose_utils::pose_t tag_pose(frame.getTimestamp(), p, v1, v2);
    tag_pose.utime = frame.getTimestamp();
    tag_pose.id = detection.id;
    // std::cerr << "id: " << detection.id << std::endl;

    return tag_pose;
}

// Compute the mean of the contour
static inline cv::Point2f
mean_contour(const std::vector<cv::Point>& pts) {
  cv::Point2f mu(0.f, 0.f);
  for (auto& pt : pts) 
    mu.x += pt.x, mu.y += pt.y;
  if (pts.size()) mu.x /= pts.size(), mu.y /= pts.size();
  return mu;
}

static inline cv::Point2f
mean_contour(const std::vector<cv::KeyPoint>& kpts) {
  cv::Point2f mu(0.f, 0.f);
  for (auto& kpt : kpts)
    mu.x += kpt.pt.x, mu.y += kpt.pt.y;
  if (kpts.size()) mu.x /= kpts.size(), mu.y /= kpts.size();
  return mu;
}



// Shrink contour for stable depth features
static inline std::vector<cv::Point>
scale_contour(const std::vector<cv::Point>& pts, float scale=1.2) {
  cv::Point2f mu = mean_contour(pts);

  std::vector<cv::Point> out_pts(pts.size());
  for (int j=0; j<pts.size(); j++) {
    cv::Point2f v(cv::Point2f(pts[j]) - mu);
    out_pts[j] = cv::Point(mu + v * scale); 
  }
  
  return out_pts;
}

// Image gets modified
void 
state_t::extract_and_mask_tags(opencv_utils::Frame& frame) {
  // detect April tags (requires a gray scale image)
  std::vector<AprilTags::TagDetection> detections = m_tagDetector->extractTags(frame.getGrayRef());


  std::vector<std::vector<cv::Point> > mask_pts;
  std::vector<pose_utils::pose_t> poses(detections.size());
  for (int j=0; j<detections.size(); j++) {
    AprilTags::TagDetection& detection = detections[j];

    std::vector<cv::Point> pts(4);
    for (int k=0; k<4; k++)
      pts[k] = cv::Point(detection.p[k].first, detection.p[k].second);

    // Scale up points
    pts = scale_contour(pts, 1.2);

    mask_pts.push_back(pts);
    
    // Compute Tag Pose
    poses[j] = compute_tag_pose(frame, detection);
  }

  
  cv::Mat3b inpainted = frame.getRGBRef().clone();
  cv::fillPoly(inpainted, mask_pts, cv::Scalar(0,0,255), CV_AA, 0);
  cv::addWeighted( frame.getRGBRef(), 0.5, inpainted, 0.5, 0, inpainted );
  cv::imshow("inpainted", inpainted);
  
  cv::Mat1b mask = cv::Mat1b::zeros(frame.getRGBRef().size());
  cv::fillPoly(mask, mask_pts, cv::Scalar(255), CV_AA, 0);
  cv::dilate(mask, mask, 9);
  
  // cv::Mat inpainted; 
  // cv::inpaint(frame.getRGBRef(), mask, inpainted, 9, cv::INPAINT_TELEA);
  // frame.setRGB(inpainted);
  // cv::imshow("inpainted", inpainted);
  // cv::waitKey(10);
  
  //----------------------------------
  // Publish poses
  //----------------------------------
  vs::obj_collection_t objs_msg; 
  objs_msg.id = 10010; 
  objs_msg.name = "APRILTAGS"; 
  objs_msg.type = VS_OBJ_COLLECTION_T_AXIS3D; 
  objs_msg.reset = true; 
  objs_msg.objs.resize(poses.size());
  
  // Transform Pose to Sensor Frame
  for (int j=0; j<poses.size(); j++) { 
    pose_utils::pose_t tag_pose_tf = transform_to_local_frame(poses[j]);
    double rpy[3]; bot_quat_to_roll_pitch_yaw (tag_pose_tf.orientation, rpy);

    // Populate Pose 
    objs_msg.objs[j].id = tag_pose_tf.id; 
    objs_msg.objs[j].x = tag_pose_tf.pos[0], objs_msg.objs[j].y = tag_pose_tf.pos[1], 
        objs_msg.objs[j].z = tag_pose_tf.pos[2];
    objs_msg.objs[j].roll = rpy[0], objs_msg.objs[j].pitch = rpy[1], 
        objs_msg.objs[j].yaw = rpy[2];
  }
  objs_msg.nobjs = objs_msg.objs.size(); 
  lcm.publish("OBJ_COLLECTION", &objs_msg);
  
  
}
    

static void on_kinect_image_frame(const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                                   const kinect::frame_msg_t *msg) {
    state.on_kinect_image_frame(rbuf, chan, msg);
}

void state_t::on_kinect_image_frame (const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                                   const kinect::frame_msg_t *msg) {

    profiler.enter("on_kinect");

    //----------------------------------
    // Publish Camera frame
    //----------------------------------
    int64_t sensor_id = vis_utils::publish_camera_frame(msg->timestamp);

    //----------------------------------
    // Unpack Point cloud
    //----------------------------------
    profiler.enter("unpack()");
    opencv_utils::Frame frame(msg, options.vSCALE);
    profiler.leave("unpack()");

    // opencv_utils::imshow("cloud", frame.getCloudRef());
    
    // Fast Bilateral filter (and mask)
    profiler.enter("fastBilateralFilter()");
    frame.fastBilateralFilter();
    profiler.leave("fastBilateralFilter()");

    // // Prune at depth and set mask 
    // profiler.enter("PruneDepth()");
    // frame.pruneDepth(10.f);
    // profiler.leave("PruneDepth()");
    
    // Compute normals (and mask)
    profiler.enter("computeNormals()");
    frame.computeNormals(1.f, 0.01, 15.f);
    cv::imshow("compute-normals", (frame.getNormalsRef() + 1) * 0.5);
    profiler.leave("computeNormals()");
    
    // Plot frame
    // frame.plot();q

    //----------------------------------
    // Extract tags if any
    //----------------------------------
    profiler.enter("extractTags()");
    // extract_and_mask_tags(frame);
    profiler.leave("extractTags()");

    //----------------------------------
    // Main MSER3D computation    // Provide depth+normals mask
    //----------------------------------
    std::vector<fsvision::Feature3D> fpts = mser3D.update(frame);

    //----------------------------------
    // KeyPoint viz
    //----------------------------------
    cv::Mat3f vimage = frame.getRGBRef().clone(); 
    cv::Mat mask = frame.getCombinedMaskRef();
    // frame.getRGBRef().copyTo(vimage, mask); 
    // vnormals = 0.5 * (vnormals + 1); 
    
    fsvision::Feature3D pfpt;
    for (auto& fpt: fpts) {
      const cv::KeyPoint& kp = fpt.get_keypoint();
      cv::Point2f tvec = cv::Point2f(kp.size, kp.response) * fsvision::feat_size;

      cv::line(vimage, kp.pt + tvec, kp.pt, cv::Scalar(0,0,255), 1, CV_AA);
      cv::circle(vimage, kp.pt, 1, cv::Scalar(0,255,255) / 255.0, 1, CV_AA );
      cv::circle(vimage, kp.pt, fsvision::feat_size, cv::Scalar(0,128,0) / 255.0, 1, CV_AA );
      // Draw correspondences between frames
      if (pfpt.get_id() == fpt.get_id()) {
        cv::line(vimage, kp.pt, pfpt.get_point(), cv::Scalar(0,200,0), 1, CV_AA);
      }
      // copy 
      pfpt = fpt;
      // putText(vimage, cv::format("%i",fpt.get_id()), kp.pt, 0, 0.3,
      //         cv::Scalar(0,255,0) / 255.0, 1,CV_AA);

    }
    cv::imshow("normals", vimage);


    profiler.enter("visualize");
#if 1
    //----------------------------------
    // Publish poses
    //----------------------------------
    vs::obj_collection_t objs_msg; 
    objs_msg.id = 10000; 
    objs_msg.name = "POSE_EST"; 
    objs_msg.type = VS_OBJ_COLLECTION_T_AXIS3D; 
    objs_msg.reset = true; 
    objs_msg.objs.resize(fpts.size());

    //----------------------------------
    // Publish ellipsoids
    //----------------------------------
    vs::cov_collection_t covs_msg; 
    covs_msg.id = 10001; 
    covs_msg.name = "ELLIPSOIDS"; 
    covs_msg.type = VS_COV_COLLECTION_T_ELLIPSOID; 
    covs_msg.reset = true;    
    covs_msg.covs.resize(fpts.size());

    //----------------------------------
    // Publish text
    //----------------------------------
    vs::text_collection_t texts_msg; 
    texts_msg.id = 10002; 
    texts_msg.name = "POSE IDs"; 
    texts_msg.type = VS_COV_COLLECTION_T_ELLIPSOID; 
    texts_msg.reset = true;    
    texts_msg.texts.resize(fpts.size());

    //----------------------------------
    // Publish surface ellipses 
    //----------------------------------
    vs::point3d_list_collection_t ellipses_msg; 
    ellipses_msg.id = 10003; 
    ellipses_msg.name = "ELLIPSE_SURFACE"; 
    ellipses_msg.type = VS_POINT3D_LIST_COLLECTION_T_TRIANGLE_FAN; 
    ellipses_msg.reset = true;    
    ellipses_msg.point_lists.resize(fpts.size());

    //----------------------------------
    // Publish corr. lines 
    //----------------------------------
    vs::point3d_list_collection_t lines_msg; 
    lines_msg.id = 10004; 
    lines_msg.name = "FEATURE_TRAJECTORY"; 
    lines_msg.type = VS_POINT3D_LIST_COLLECTION_T_LINES; 
    lines_msg.reset = true;    
    lines_msg.point_lists.resize(1);
    
    //----------------------------------
    // Get cloud, normals
    //----------------------------------
    cv::Mat3f cloud = frame.getCloudRef();
    cv::Mat3f normals = frame.getNormalsRef();
    
    //--------------------------------------------
    // Visualize
    //--------------------------------------------
    int pid = -1;
    pose_utils::pose_t pobj;

    vs::point3d_t pt; pt.y = 0.f, pt.z = 0.f, pt.x = 0.f;
    vs::color_t col; col.r = 0.f, col.g = 0.f, col.b = 1.f;
    
    for (int j=0; j<fpts.size(); j++) {
      assert(fpts[j].xyz_valid() && fpts[j].normal_valid() && fpts[j].xyz_valid());
      
      const cv::Vec3f& pos = fpts[j].xyz();
      const cv::Vec3f& nvec = fpts[j].normal();
      const cv::Vec3f& tvec = fpts[j].tangent();
      const cv::Vec6f& fcov = fpts[j].covs();

      // std::cerr << pos << " " << nvec << " " << tvec << std::endl;
      // if (pos[0] != pos[0] || tvec[0] != tvec[0] || nvec[0] != nvec[0])
      //   continue;

      // Compute pose from observation
      pose_utils::pose_t pt_pose(fpts[j].get_utime(), pos, nvec, tvec);
      
      // Transform Pose to Sensor Frame
      pose_utils::pose_t tag_pose_tf = transform_to_local_frame(pt_pose);
      double rpy[3]; bot_quat_to_roll_pitch_yaw (tag_pose_tf.orientation, rpy);

      // Populate Pose 
      objs_msg.objs[j].id = j; // fpts[j].get_id();
      objs_msg.objs[j].x = tag_pose_tf.pos[0], objs_msg.objs[j].y = tag_pose_tf.pos[1], 
          objs_msg.objs[j].z = tag_pose_tf.pos[2];
      objs_msg.objs[j].roll = rpy[0], objs_msg.objs[j].pitch = rpy[1], 
          objs_msg.objs[j].yaw = rpy[2];


      if (pid == fpts[j].get_id()) {
        pt.x = pobj.pos[0], pt.y = pobj.pos[1], pt.z = pobj.pos[2];
        lines_msg.point_lists[0].points.push_back(pt);

        pt.x = pt_pose.pos[0], pt.y = pt_pose.pos[1], pt.z = pt_pose.pos[2];
        lines_msg.point_lists[0].points.push_back(pt);

        lines_msg.point_lists[0].colors.push_back(col);
        lines_msg.point_lists[0].colors.push_back(col);
      }
      
      // copy
      pobj = pt_pose;
      pid = fpts[j].get_id();
      
#if 0
      // Add ellipsoids
      covs_msg.covs[j].id = j;
      covs_msg.covs[j].collection = 10000; 
      covs_msg.covs[j].element_id = j;
      covs_msg.covs[j].n = 6;
      covs_msg.covs[j].entries.resize(covs_msg.covs[j].n, 0);

      covs_msg.covs[j].entries[0] = fcov[0];
      covs_msg.covs[j].entries[3] = fcov[3];
      covs_msg.covs[j].entries[5] = fcov[5]; 

      // Add Text
      texts_msg.texts[j].id = j;
      texts_msg.texts[j].collection_id = 10000; 
      texts_msg.texts[j].object_id = j;
      texts_msg.texts[j].text = std::to_string(fpts[j].get_id());
      
      // Surface ellipses
      ellipses_msg.point_lists[j].id = j;
      ellipses_msg.point_lists[j].collection = 10000;
      ellipses_msg.point_lists[j].element_id = j;

      // Populate points
      vs::point3d_t pt;      
      pt.y = 0.f, pt.z = 0.f, pt.x = 0.f;
      ellipses_msg.point_lists[j].points.push_back(pt);

      vs::color_t col; 
      col.r = 1.f, col.g = 0.f, col.b = 0.f;
      ellipses_msg.point_lists[j].colors.push_back(col);
      
      for (int th=0; th<=360; th+=10) {
        float a = sqrt(fcov[0])/3.f, b = sqrt(fcov[3])/3.f;
        pt.y = a * cos(th * CV_PI/180), pt.z = b * sin(th * CV_PI/180), pt.x = 0.f; 
        ellipses_msg.point_lists[j].points.push_back(pt);

        ellipses_msg.point_lists[j].colors.push_back(col);
      }
      ellipses_msg.point_lists[j].npoints = ellipses_msg.point_lists[j].points.size();
      ellipses_msg.point_lists[j].ncolors = ellipses_msg.point_lists[j].colors.size();
      ellipses_msg.point_lists[j].nnormals = 0;
      ellipses_msg.point_lists[j].npointids = 0;
#endif
    }

// Visualize poses
    objs_msg.nobjs = objs_msg.objs.size(); 
    lcm.publish("OBJ_COLLECTION", &objs_msg);

    // Visualize trajectories 
    lines_msg.nlists = lines_msg.point_lists.size();

    lines_msg.point_lists[0].id = 1;
    lines_msg.point_lists[0].collection = sensor_id;
    lines_msg.point_lists[0].element_id = 1;
    
    lines_msg.point_lists[0].npoints = lines_msg.point_lists[0].points.size();
    lines_msg.point_lists[0].ncolors = lines_msg.point_lists[0].colors.size();
    lines_msg.point_lists[0].nnormals = 0;
    lines_msg.point_lists[0].npointids = 0;
    
    lcm.publish("POINTS_COLLECTION", &lines_msg);

#if 0
    // Visualize ellipsoids
    covs_msg.ncovs = covs_msg.covs.size(); 
    lcm.publish("COV_COLLECTION", &covs_msg);

    // Visualize pose ids
    texts_msg.n = texts_msg.texts.size(); 
    lcm.publish("TEXT_COLLECTION", &texts_msg);

    // Visualize surface ellipses 
    ellipses_msg.nlists = ellipses_msg.point_lists.size();
    lcm.publish("POINTS_COLLECTION", &ellipses_msg);
#endif    

#endif
    profiler.leave("visualize");
    // Set timestamp
    profiler.leave("on_kinect");
    cv::waitKey(5);

}

void state_t::process(const cv::Mat& img) { 

    double t1 = bot_timestamp_now();


    // //----------------------------------
    // // Create video writer
    // //----------------------------------
    // if (options.vCREATE_VIDEO && options.vLOG_FILENAME != "") {
    //     if (!writer.isOpened()) { 
    //         writer.open(options.vLOG_FILENAME + "-video.avi", VideoWriter::fourcc('M','P','4','2'),
    //                            30, out_img.size(), 1);
    //         std::cerr << "===> Video writer created" << std::endl;
    //     }
    //     writer << out_img;
    //     std::cerr << "writing" << std::endl;
    // }
    return;
}

int main( int argc, char** argv )
{

    //----------------------------------
    // Opt args
    //----------------------------------
    ConciseArgs opt(argc, (char**)argv);
    opt.add(options.vLOG_FILENAME, "l", "log-file","Log file name");
    opt.add(options.vCREATE_VIDEO, "v", "create-video","Create video");
    opt.add(options.vCHANNEL, "c", "channel","Kinect Channel");
    opt.add(options.vMAX_FPS, "r", "max-rate","Max FPS");
    opt.add(options.vSCALE, "s", "scale","Scale");    
    opt.parse();

    // Handle special args cases
    if (options.vLIVE_MODE) { 
        options.vCREATE_VIDEO = false;
    }

    //----------------------------------
    // args output
    //----------------------------------
    std::cerr << "===========  MSER3D app ============" << std::endl;
    std::cerr << "MODES 1: mser3d-app -l log-file -v <create video> -r max-rate\n";
    std::cerr << "=============================================\n";
    if (options.vLOG_FILENAME != "") { 
        std::cerr << "=> LOG FILENAME : " << options.vLOG_FILENAME << std::endl;    
    } else { 
        std::cerr << "=> LIVE MODE" << std::endl; options.vLIVE_MODE = true;
    }
    std::cerr << "=> CREATE VIDEO : " << options.vCREATE_VIDEO << std::endl;
    std::cerr << "=> CHANNEL : " << options.vCHANNEL << std::endl;
    std::cerr << "=> MAX FPS : " << options.vMAX_FPS << std::endl;
    std::cerr << "=> SCALE : " << options.vSCALE << std::endl;

    std::cerr << "===============================================" << std::endl;

    //----------------------------------
    // Setup Player Options
    //----------------------------------
    poptions.fn = options.vLOG_FILENAME;
    poptions.ch = options.vCHANNEL;
    poptions.fps = options.vMAX_FPS;
    poptions.lcm = &state.lcm;
    poptions.handler = on_kinect_image_frame;
    poptions.user_data = state.ptr();

    //----------------------------------
    // Subscribe, and start main loop
    //----------------------------------
    if (options.vLIVE_MODE) {
        state.lcm.subscribe(options.vCHANNEL, &state_t::on_kinect_image_frame, &state ); 
        while (state.lcm.handle() == 0);
    } else { 
        player.init(poptions); 
    }

    return 0;
}
