import cv2
import numpy as np
import matplotlib as mpl
import matplotlib.pylab as plt

import fs_slic as slic
import utils.slic_utils as slic_utils

import utils.lcmutils as lcmutils
import utils.plot_utils as plotutils

fn = '/home/spillai/data/2013_articulation_test/book_unfolding/lcmlog-2013-03-24.00'
reader = lcmutils.KinectLogReader(fn, channel='KINECT_FRAME')

frame = reader.get_frame(1364159429904421)
img, depth, orgX = frame.rgb, frame.depth, frame.X

plotutils.imshow(img)

# Build image (LAB) superpixels
slic_labels = slic.slic_s(img, superpixel_size=300, 
                          compactness=10, im_type=cv2.CV_8UC3, op_type=cv2.COLOR_RGB2LAB)

segment, label_mean = slic_utils.slic_segment(img, slic_labels)
plt.imshow(segment)
plt.show()
# contours = slic.contours(img, slic_labels, 10)

