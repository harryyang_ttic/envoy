// 
// Author: Sudeep Pillai (spillai@csail.mit.edu) Aug 25, 2013
// Updates: Aug 04, 2013
// 

#include "mser3d.hpp"

namespace fsvision
{

#define VIZ 1

MSER3D::MSER3D( int _pyramid_levels, int _delta, int _min_area, int _max_area,
                  double _max_variation, double _min_diversity,
                  int _max_evolution, double _area_threshold,
                double _min_margin, int _edge_blur_size )
    : pyramid_levels(_pyramid_levels),
      delta(_delta), min_area(_min_area), max_area(_max_area), max_variation(_max_variation),
      min_diversity(_min_diversity), max_evolution(_max_evolution), area_threshold(_area_threshold), 
      min_margin(_min_margin), edge_blur_size(_edge_blur_size) { 

  // Setup MSER
  mser = opencv_utils::MSER(delta, min_area, max_area, max_variation,
                            min_diversity, max_evolution, area_threshold,
                            min_margin, edge_blur_size);
  
  // Setup profiler
  profiler.setName("MSER3D");

  // Setup tracker
  kptracker.setDistanceThreshold(0.05);
  kptracker.setAngleThreshold(30.f * CV_PI / 180);

}

// Sample from contours 
void
contour_from_pts(const std::vector<cv::Point>& hull, std::vector<cv::Point>& contour,
                              int quantize = 10) {

  int rem = 0; 
  for (int j=0; j<hull.size(); ++j) {
    int k=(j+1) % hull.size();
    cv::Point2f v = cv::Point2f(hull[k] - hull[j]);
    float vnorm = cv::norm(v);
    if (vnorm < 1) continue; // at least 1 pixel
    v.x /= vnorm, v.y /= vnorm;
    for (int i=0; i<vnorm; i+=quantize) {
      cv::Point p(v.x * i, v.y * i);
      contour.push_back(hull[j] + p);
      rem = vnorm - i;
    }
  }

}

// Compute mean of contour
static inline cv::Point2f
mean_contour(const std::vector<cv::Point>& pts) {
  cv::Point2f mu(0.f, 0.f);
  for (auto& pt : pts) 
    mu.x += pt.x, mu.y += pt.y;
  if (pts.size()) mu.x /= pts.size(), mu.y /= pts.size();
  return mu;
}

static inline cv::Point2f
mean_contour(const std::vector<cv::KeyPoint>& kpts) {
  cv::Point2f mu(0.f, 0.f);
  for (auto& kpt : kpts)
    mu.x += kpt.pt.x, mu.y += kpt.pt.y;
  if (kpts.size()) mu.x /= kpts.size(), mu.y /= kpts.size();
  return mu;
}


// Shrink contour for stable depth features
static inline std::vector<cv::Point>
shrink_contour(const std::vector<cv::Point>& pts, float scale=0.8) {
  cv::Point2f mu = mean_contour(pts);

  std::vector<cv::Point> out_pts(pts.size());
  for (int j=0; j<pts.size(); j++) {
    cv::Point2f v(cv::Point2f(pts[j]) - mu);
    out_pts[j] = cv::Point(mu + v * scale); 
  }
  
  return out_pts;
}


static std::vector<cv::KeyPoint>
interestPointsFromEllipse(const cv::RotatedRect& rect,
                       const int octave) {
  
  std::vector<cv::KeyPoint> kpts;
  float vnorm = (rect.size.width + rect.size.height) / 2;
  cv::Point2f v(cos(rect.angle * CV_PI/180), sin(rect.angle * CV_PI/180));
  kpts.push_back(cv::KeyPoint(cv::Point2f(rect.center), v.x, rect.angle * CV_PI/180, v.y, octave, -1));
  return kpts;
}



static std::vector<cv::KeyPoint>
interestPointsFromHull(const std::vector<cv::Point>& pts,
                       const int octave, 
                       const int class_id, 
                       const float min_angle_response = 10.f * CV_PI/180,
                       const float shrink_ratio = 0.8) {
  
  std::vector<cv::KeyPoint> kpts;
  float angle_th = cos(min_angle_response);
  std::vector<cv::Point> spts = shrink_contour(pts, shrink_ratio);
  cv::Point2f mu = mean_contour(spts);
  
  assert(spts.size() == pts.size());
  for (int j=0; j<spts.size(); j++) {
    cv::Point2f p(spts[j]);
    cv::Point2f pnext(spts[(j+1)%spts.size()]);
    cv::Point2f v(pnext - p);
    
    float vnorm = cv::norm(v);
    v *= 1.0 / vnorm;
    
    kpts.push_back(cv::KeyPoint(cv::Point2f(p), 
                                v.x, atan2(v.y,v.x) * 180 / CV_PI, v.y,
                                octave, class_id)); 
  }

  return kpts;
}

inline bool is_nan(const cv::Vec3f& v) {
  return (v[0] != v[0]);
}

static inline
cv::Vec3f compute_surface_variation(const opencv_utils::Frame& frame,
                                const std::vector<cv::Point>& pts,
                                int samples = 30) {

  const int skip = std::min(20, std::max(1, int(pts.size() / samples)));
  
  cv::Vec3f var = 0;
  cv::Vec3f mu = 0;
  int count = 0; 

  // Compute mean
  const cv::Mat3f& normals = frame.getNormalsRef();
  for (int j=0; j<pts.size(); j+=skip) {
    if (is_nan(normals(pts[j]))) continue;
    mu += normals(pts[j]);
    count++;
  }

  // If no depth registered, return high var
  if (count == 0)
    return cv::Vec3f(1.f,1.f,1.f);
  
  mu *= 1.f / count;

  // COmpute var
  for (int j=0; j<pts.size(); j+=skip) {
    if (is_nan(normals(pts[j]))) continue;
    cv::Vec3f v = (normals(pts[j]) - mu);
    var += cv::Vec3f(v[0]*v[0], v[1]*v[1], v[2]*v[2]);
  }

  var *= 1.f / count;
  return var;
}

static void
constructFeatures(const opencv_utils::Frame& frame,
                  const std::vector<cv::KeyPoint>& kpts,
                  std::vector<fsvision::Feature3D>& fpts) {

  // Get frame elements
  const cv::Mat3f& cloud = frame.getCloudRef();
  const cv::Mat3f& normals = frame.getNormalsRef();
  
  // Propagate timestamp, and ID
  assert(fpts.size() == 0);
  int64_t now = frame.getTimestamp();
  for (int j=0; j<kpts.size(); j++) {
    cv::Point2f p(kpts[j].pt);

    // Ensure keypoint is valid
    if (p.x <  0 || p.x >= cloud.cols-1 ||
        p.y <  0 || p.y >= cloud.rows-1)
      continue;
    
    // Initialize feature3d, and propagate keypoint data
    Feature3D fpt(now, -1);
    fpt.set_keypoint(kpts[j]);
    
    // Handle NaN downstream
    cv::Point3f xyz(cloud(p));
    cv::Point3f normal(normals(p));

    // 2D KeyPoint vector
    cv::Point2f tp = cv::Point2f(p) + cv::Point2f(kpts[j].size, kpts[j].response) * feat_size;

    // stdr::cerr << "kp: " << kpts[j] << std::endl;
    // std::cerr << "p: " << p << " tp: " << tp << std::endl;
    
    if (tp.x >= 0 && tp.x < cloud.cols-1 &&
        tp.y >= 0 && tp.y < cloud.rows-1) {
      cv::Point3f tangent(cloud(tp) - cloud(p));
      float tnorm = cv::norm(tangent);
      tangent *= 1.f / tnorm;
      fpt.setFeature3D(xyz, normal, tangent);

      float ratio = kpts[j].size / kpts[j].response;
      // float xx = cv::norm(evec.first), yy = cv::norm(evec.second);
      float xx = feat_size*feat_size, yy=feat_size*feat_size; 
      fpt.setCovariance3D(xx*xx, 0., 0., yy, 0., tnorm*tnorm*ratio*ratio);
      // std::cerr << "covs: " << fpt.covs() << std::endl;
      
      // std::cerr << "cp: " << xyz << " cnormal: " << normal
      //           << " ctp: " << tangent << " dot: " << tangent.dot(normal) << std::endl;
    } else {
      fpt.setFeature3D(xyz, normal);
      // std::cerr << "cp: " << xyz << " cnormal: " << normal << std::endl;
    }

    // Push Feature3D (xyz/normal/tangent could be NaN)
    fpts.push_back(fpt);
    
  }

}

cv::Mat MSER3D::maxEdgeResponse(const cv::Mat& src) {
  // Sobel detection with color images =================================
  /// Generate grad_x and grad_y
  int scale = 1;
  int delta = 0;
  int ddepth = CV_32F;
  cv::Mat grad, grad_x, grad_y, abs_grad_x, abs_grad_y;
  
  /// Gradient X
  cv::Sobel( src, grad_x, ddepth, 1, 0, 3, scale, delta, cv::BORDER_DEFAULT );   
  cv::convertScaleAbs( grad_x, abs_grad_x );

  /// Gradient Y  
  cv::Sobel( src, grad_y, ddepth, 0, 1, 3, scale, delta, cv::BORDER_DEFAULT );   
  cv::convertScaleAbs( grad_y, abs_grad_y );

  /// Total Gradient (approximate)
  cv::addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad );

  std::vector<cv::Mat> channels; 
  cv::split(grad, channels);
  assert(channels.size() == 3);

  // Find max of R,G,B channels
  cv::Mat max_grad; 
  cv::max(channels[0], channels[1], max_grad); 
  cv::max(max_grad, channels[2], max_grad);

  // Noise factor
  max_grad = max_grad - 15.f;

  // // Erode
  // cv::erode(max_grad, max_grad, cv::Mat(), cv::Point(-1,-1), 1);
  // cv::dilate(max_grad, max_grad, cv::Mat(), cv::Point(-1,-1), 1);

  // // Blur response
  // cv::blur(max_grad, max_grad, cv::Size(3,3));
  
  return max_grad;

}

float contour_confidence(const cv::Mat3f& normals,
                         const std::vector<cv::Point>& pts) {

  // Default 
  if (!pts.size()) return 0.f;

  std::vector<cv::Point> sample_pts = shrink_contour(pts, 0.7);
  
  int votes = 0;  
  for (int j=0; j<sample_pts.size(); j++) {
    const cv::Point& p1 = sample_pts[j];
    const cv::Point& p2 = sample_pts[(j+1)%sample_pts.size()];
    // Query edge map and evaluate the pts
    if(p1.x >=0 && p1.x <= normals.cols && p1.y >= 0 && p1.y < normals.rows &&
       p2.x >=0 && p2.x <= normals.cols && p2.y >= 0 && p2.y < normals.rows) {
      cv::Vec3f n1(normals(p1)), n2(normals(p2));
      if (!is_nan(n1) && !is_nan(n2) && n1.dot(n2) > 0.9)
        votes++;
    }
  }
  return votes * 1.f / sample_pts.size();
}

static int find_level(const std::vector<cv::Vec4i>& hierarchy, int idx) {
  if (hierarchy[idx][3] < 0)
    return 1;
  return find_level(hierarchy, hierarchy[idx][3]) + 1;
}

// Detect MSERs at multiple scales (all keypoints at all scales are reported)
void MSER3D::detectImpl(const opencv_utils::Frame& frame,
                        std::vector<fsvision::Feature3D>& ms_fpts) {

  profiler.enter("detectImpl");

  cv::Mat src;
  cv::Mat out = frame.getRGBRef().clone();

  // Work with surface normals =========================================
  // cv::Mat mask = frame.getCombinedMaskRef();
  cv::Mat mask = cv::Mat(); // frame.getCombinedMaskRef();
  cv::Mat3f image; frame.getNormalsRef().copyTo(image, mask);
  src = 0.5 * (image + 1); // needed for mser (range 0-1.f)

  // MSER mask =========================================================
  cv::Mat1b mser_mask(frame.size());
  
  // Work with images  =================================================
  // cv::Mat mask;
  // const cv::Mat3f& normals = frame.getNormalsRef();
  // cv::Mat3b image; frame.getRGBRef().copyTo(image);
  // // cv::cvtColor(image, image, cv::COLOR_BGR2Lab);
  // cv::GaussianBlur(image, src, cv::Size(5,5), 0); 
  // // cv::bilateralFilter(image, src, 5, 5*2, 5/2);
  // cv::imshow("src", src);
  // // cv::medianBlur(image, src, 3);
  
  // Convert to 3 channel 8-bit image ==================================
  // normals[j] = ((normals[j] + 1) * 128).astype(np.uint8)
  // cv::Mat src; image.convertTo(src, CV_8UC3, 128);
  // image.convertTo(src, CV_32FC3, 128); (look at chisquare in mser)

  // Viz
  std::vector<cv::Scalar> colors(10);
  opencv_utils::fillColors(colors);
  
  // Create mask =======================================================
  cv::Mat src_mask = mask;
  cv::Mat dilated_mask;
  if( !mask.empty() )
  {
    dilate( mask, dilated_mask, cv::Mat() );
    cv::Mat mask255( mask.size(), CV_8UC1, cv::Scalar(0) );
    mask255.setTo( cv::Scalar(255), dilated_mask != 0 );
    dilated_mask = mask255;
  }
  
  // Detect at multiple levels =========================================
  for( int l = 0, multiplier = 1;
       l <= pyramid_levels; ++l, multiplier *= 2 ) {
    
    std::stringstream ss; 
    ss << "Octave: " << l << " Image: " << src.size();
    profiler.enter(ss.str());

    // Reinit mser with appropriate params
    mser = opencv_utils::MSER(delta, min_area / (multiplier * multiplier),
                              (src.rows * src.cols) * 0.75, max_variation,
                              min_diversity, max_evolution, area_threshold,
                              min_margin, edge_blur_size);

    // Sobel detection with color images =================================
    // cv::Mat gray3;
    // cv::Mat out3 = out.clone();
    // cv::Mat gray = maxEdgeResponse(src);
    cv::Mat gray = frame.getGrayRef().clone();
    if (l == 0)
      cv::imshow("response", gray);
    // cv::cvtColor(gray, gray3, cv::COLOR_GRAY2BGR);

    // Detect on current level of the pyramid
    cv::Mat dxy;

    std::vector<cv::Vec4i> hierarchy;
    std::vector<std::vector<cv::Point> > mser_pts_vec;
    mser( src, mser_pts_vec, hierarchy, dxy, src_mask);
    
    // Scale up the detections (after fitting the convex hulls)
    int idx=0, class_idx=0; 
    for (auto& mser_pts : mser_pts_vec) {
      idx++;

      // // Compute variation in surface normal space
      // cv::Vec3f var = compute_surface_variation(frame, mser_pts);
      // float var_th = 0.2; 
      // // std::cerr << "surface_var: " << var << std::endl;
      // if (var[0] > var_th || var[1] > var_th || var[2] > var_th) continue;

      cv::Scalar white( 255, 255, 255 );
// #if 0
//       // Viz mser pts
//       for (int j=0; j<mser_pts.size(); j+=5) {
//         cv::Point pt = mser_pts[j];
//         pt.x *= multiplier, pt.y *= multiplier;
//         if (l==0)
//           cv::circle(out, pt, true, hierarchy[idx][3] < 0 ? white : colors[idx%10], 1, CV_AA, 0); 
//       }

//       // for(idx = 0 ; idx < hierarchy.size(); idx++) {
//       std::cerr << "hierarchy: " << idx << " : " << hierarchy[idx] << std::endl;

//       idx++;      
// #endif

      // Build convex hull of points
      std::vector<cv::Point> hull_pts;
      cv::convexHull(mser_pts, hull_pts);
      if (hull_pts.size() > 2) {

        int level = find_level(hierarchy, idx);
        // std::cerr << "level: " << idx << " " << level << std::endl;
        cv::polylines(out, hull_pts, true, cv::Scalar(0, level*1.f/6*255, level*1.f/6*255), 1, CV_AA, 0);
        // hierarchy[idx-1][3] < 0 ? cv::Scalar(0,255,255) : cv::Scalar(255,0,0), 1, CV_AA, 0);
      }
      
//       // Prune and scale hull points
//       for (std::vector<cv::Point>::iterator it = hull_pts.begin();
//            it != hull_pts.end(); ) {
//         it->x *= multiplier, it->y *= multiplier;
//         if (it->x < 0 || it->x >= image.cols || it->y < 0 || it->y >= image.rows) {
//           it = hull_pts.erase(it);
//         } else
//           it++;           
//       }

//       // Get samples and approx. contour
//       std::vector<cv::Point> sample_pts, approx_pts; 
//       contour_from_pts(hull_pts, sample_pts, 10);

//       // // Fit Ellipse to it
//       // cv::RotatedRect ell = fitEllipse( sample_pts );
//       // ell.size.width *= 0.25, ell.size.height *= 0.25;
//       // cv::ellipse( out, ell, cv::Scalar(0,255,255), 1 );
//       // std::vector<cv::KeyPoint> ipts = interestPointsFromEllipse(ell, l); 
      
//       // Approximate polygon
//       cv::approxPolyDP(cv::Mat(sample_pts), approx_pts, 3, true);

//       // Compute Contour Confidence
//       float conf = contour_confidence(normals, sample_pts);
// #if 0
//       for (auto& pt : sample_pts) { 
//         // Query edge map and evaluate the pts
//         if(pt.x >=0 && pt.x <= normals.cols && pt.y >= 0 && pt.y < normals.rows) {
//           cv::Vec3f n(normals(pt));
//           if (!is_nan(n))
//             cv::circle(out, pt, 2, (n+cv::Vec3f(1.f,1.f,1.f))*128.f, CV_FILLED, CV_AA);
//         }
//       }
// #endif

//       // Only progress with 0.9 confidence
//       if (conf < 0.9) continue;
      
//       // Construct Mask

      
//       // Get interest points from hull
//       float min_angle_response = 0.f * CV_PI / 180;
//       float shrink_ratio = 1;
//       std::vector<cv::KeyPoint> ipts = interestPointsFromHull(approx_pts, l, class_idx++,
//                                                               min_angle_response, 
//                                                               shrink_ratio);
      
//       // Construct features from keypoints
//       std::vector<fsvision::Feature3D> fpts; 
//       constructFeatures(frame, ipts, fpts);

//       // Insert into ms_fpts
//       ms_fpts.insert(ms_fpts.end(), fpts.begin(), fpts.end());
      
#if VIZ

      // // viz shrunk hull
      // std::vector<std::vector<cv::Point> > test_pts;

      // std::vector<cv::Point> ipts_(ipts.size());
      // for (int j=0; j<ipts.size(); j++)
      //   ipts_[j] = ipts[j].pt;
      
      // test_pts.push_back(shrink_contour(hull_pts, 0.8));
      
      // // viz hull pts
      // for (auto& test_pt: test_pts)
      //   for (auto& pt : test_pt) {
      //     cv::circle(out, pt, 2, cv::Scalar(0,255,255), CV_FILLED, CV_AA);
      //     // cv::circle(out, pt, 2, opencv_utils::val_to_bgr(std::min(1.f,conf*3)), CV_FILLED, CV_AA);
      //   }
      // cv::circle(out, mean_contour(sample_pts), 2, cv::Scalar(0,0,255), CV_FILLED, CV_AA);
#endif
      
    }
    std::cerr << "KeyPoints detected at octave: " << mser_pts_vec.size() << std::endl;
      
    // Downsample
    if( l < pyramid_levels ) {
      cv::Mat dst;
      cv::pyrDown( src, dst );
      src = dst;
      if( !mask.empty() )
        cv::resize( dilated_mask, src_mask, src.size(), 0, 0, cv::INTER_NEAREST );
    }

    // opencv_utils::imshow(std::string("out"+std::to_string(l)), out3);
    // opencv_utils::imshow(std::string("pyr"+std::to_string(l)), gray3);      
    profiler.leave(ss.str());

    // Only upto 160x120
    if (src.cols < 160 || src.rows < 120)
      break;

  }

#if VIZ
  cv::imshow("ellipses", out);
#endif
  
  profiler.leave("detectImpl");
  std::cerr << "Total KeyPoints detected: " << ms_fpts.size() << std::endl;
  
  return;
}

struct IntraFrameMatch {
  IntraFrameMatch(const opencv_utils::Frame& frame,
                  const std::vector<fsvision::Feature3D>& _fpts, const float _angle_th ) :
      fpts(_fpts), angle_th(_angle_th) {
    
    // Cluster keypoints based on distance ===============================
    // Add points that are within radius ball within each other
    // Find k-nearest neighbors and compute surface normal

    double t1 = bot_timestamp_now();
    // Viz
    cv::Mat img = frame.getRGBRef().clone();
    const cv::Mat3f& cloud = frame.getCloudRef();

    // Convert points
    std::vector<cv::Point3f> points;
    fsvision::Feature3D::convert(fpts, points);
    cv::Mat data = cv::Mat(points).reshape(1);

    // Build index
    cv::flann::Index index(data, cv::flann::KDTreeIndexParams(4));
    
    // Query
    cv::Mat indices(points.size(), 10, CV_32SC1);
    cv::Mat dists(points.size(), 10, CV_32FC1);
    index.knnSearch(data, indices, dists, 10,
                        cv::flann::SearchParams(64));

    std::cerr << "build index: " << (bot_timestamp_now() - t1) * 1e-3 << " ms" << std::endl;
    
    // Connectivity
    neigh = cv::Mat1b::zeros(points.size(), points.size());
    cv::Mat1b visited = cv::Mat1b::zeros(neigh.size());
    
    // Cache nn
    t1 = bot_timestamp_now();
    for (int j=0; j<indices.rows; j++) {
      int* p = indices.ptr<int>(j);
      float* d = dists.ptr<float>(j);

      const fsvision::Feature3D& fj = fpts[j];
      const cv::KeyPoint& kpj = fj.get_keypoint();
      cv::Point2f tvecj = cv::Point2f(kpj.size,kpj.response);
      
      for (int k=0; k<indices.cols; k++) {
        // Skip random indices; Don't add self to nn cache
        int nidx = p[k];
        if (j==nidx || nidx < 0 || nidx >= fpts.size())
          continue;

        // Skip if the pair has been visited
        if (visited(j,nidx)) continue;
        visited(j,nidx) = 1, visited(nidx,j) = 1;
        
        const fsvision::Feature3D& fk = fpts[nidx];
        const cv::KeyPoint& kpk = fk.get_keypoint();
        cv::Point2f tveck = cv::Point2f(kpk.size,kpk.response);
        
        // 3D Matching
        // 1. Angle between normals/tangents within threshold
        if ((fj.normal().dot(fk.normal()) < cos(angle_th))  ||
            (fj.tangent().dot(fk.tangent()) < cos(angle_th)))
          continue;
        
        // 2D Matching
        // 1. Within-ellipse of each other
        // Here we look at the distance between the centers of two
        // ellipses (d_vec), and ensure that this distance is less
        // than the distance from the origin of the ellipse to the arc
        // Here size is ||major||/2, and response is ||minor||/2
        cv::Point2f dv = kpk.pt - kpj.pt;
        if (cv::norm(dv) >= feat_size)
          continue;
        
        // std::cerr << "d_vec: " << d_vec
        // << " d_ell: " << d_ell << (d_vec > d_ell ? " BAD" : " GOOD")
        // << " j: " << keypoints[j].angle
        // << " k: " << keypoints[nidx].angle << std::endl;
        
        // Viz matches (draw green lines between matches)
        cv::line(img, kpj.pt, kpk.pt, cv::Scalar(0,255,0), 1, CV_AA);
        cv::circle(img, kpk.pt, 1, cv::Scalar(255,255,0), CV_FILLED);
        neigh(j,nidx) = 1;
        neigh(nidx,j) = 1; 
      }
      cv::circle(img, kpj.pt, 1, cv::Scalar(255,255,0), CV_FILLED);

    }
    // std::cerr << "match : " << (bot_timestamp_now() - t1) * 1e-3 << " ms" << std::endl;
    // cv::imshow("intraframematch", img);
  }
  
  inline bool operator() (const int& a, const int& b) {
    // Expect the cache to be set, otherwise complain
    assert(neigh(a,b) == neigh(b,a));
    return (neigh(a,b) > 0);
  }

  const float angle_th;
  std::vector<fsvision::Feature3D> fpts;
  cv::Mat1b neigh; // cached connectivity

};

void 
MSER3D::nonmaxSupression(const opencv_utils::Frame& frame,
                         const std::vector<fsvision::Feature3D>& ms_fpts,
                         std::vector<fsvision::Feature3D>& clustered_fpts) {
  
  // Index keypoints and provide the data to IntraFrameMatch struct
  std::vector<int> ms_inds(ms_fpts.size());
  for (int j=0; j<ms_inds.size(); j++) ms_inds[j] = j;

  std::vector<cv::Scalar> colors(10);
  opencv_utils::fillColors(colors);
  
  std::vector<int> labels;

  profiler.enter("partition");
  int nclasses = opencv_utils::partition(ms_inds, labels,
                                         IntraFrameMatch(frame, ms_fpts, 30. * CV_PI/180) );
  profiler.leave("partition");
  std::cerr << "NCLASSES: " << nclasses << " out of " << ms_inds.size() << std::endl;
  // std::cerr << cv::Mat(labels) << std::endl;

  // Return if no eq. classes found
  if (!nclasses) return;

#if VIZ
  cv::Mat out = frame.getRGBRef().clone();
#endif
  
  profiler.enter("compute_mu_stats");
  // Map output keypoints based on label
  std::map<int, std::vector<fsvision::Feature3D> > fp_map;
  for (int j=0; j<labels.size(); j++) {
    int l = labels[j];
    const fsvision::Feature3D& fp = ms_fpts[j];
#if VIZ
    const cv::KeyPoint& kp = fp.get_keypoint();
    cv::Point2f tvec = cv::Point2f(kp.size, kp.response) * feat_size;
    cv::line(out, kp.pt + tvec, kp.pt, cv::Scalar(0,0,255), 1, CV_AA);
    cv::circle( out, kp.pt, feat_size, colors[l%10], 1, CV_AA );
#endif
    fp_map[l].push_back(fp);
  }
  // std::cerr << cv::Mat(labels) << std::endl;

#if VIZ
  cv::imshow("out", out);
#endif

  // Keypoints
  for (std::map<int, std::vector<fsvision::Feature3D> >::iterator it = fp_map.begin();
       it != fp_map.end(); it++) {
    const std::vector<fsvision::Feature3D>& fpts = it->second;
    assert(fpts.size());

    // Remove keypoints with only 1 detection across scales
    // if (kpts.size() < 2) continue;
    
    cv::Point2f mu_pt(0,0);
    float mu_angle = 0.f, mu_size = 0.f, mu_response = 0.f;
    int n = fpts.size();
    std::vector<float> angles;
    std::vector<cv::Point3f> tangents;
    std::vector<cv::Point3f> normals;
    std::vector<cv::Point3f> xyzs;

    for (auto& fpt: fpts) {
      if (fpt.xyz_valid()) xyzs.push_back(fpt.xyz());
      if (fpt.normal_valid()) normals.push_back(fpt.normal());
      if (fpt.tangent_valid()) tangents.push_back(fpt.tangent());
      
      const cv::KeyPoint& kpt = fpt.get_keypoint();
      // mu_xyz += fpt.xyz();
      mu_pt += kpt.pt;
      // mu_angle += fabs(cos(kpt.angle * CV_PI / 180));
      mu_size += kpt.size;
      mu_response += kpt.response;
      angles.push_back(kpt.angle);
    }
    std::nth_element(angles.begin(), angles.begin() + n/2, angles.end());
    mu_pt *= 1.f / fpts.size(); // , mu_pt.y *= 1.f / fpts.size();
    mu_angle *= 1.f / fpts.size();
    mu_size *= 1.f / fpts.size();
    mu_response *= 1.f / fpts.size();

    // Compute mean, covar on position
    cv::Mat covar, mean;
    cv::calcCovarMatrix(cv::Mat(xyzs).reshape(1), covar, mean,
                        CV_COVAR_NORMAL | CV_COVAR_ROWS, CV_32FC1);

    // Add min stddev. 
    covar = cv::max(cv::Mat1f(covar.size(), 0.03*0.03), covar);
    const float* pmean = mean.ptr<float>(0);
    const float* pcov = covar.ptr<float>(0);

    // Compute mean keypoint
    Feature3D mu_fpt(frame.getTimestamp(), -1);
    mu_fpt.set_keypoint(cv::KeyPoint(mu_pt, mu_size, angles[n/2], mu_response,
                                     fpts.size(), it->first));

    // Compute mean normal, tangent
    cv::Point3f mu_xyz(pmean[0], pmean[1], pmean[2]);
    cv::Point3f mu_normal = std::accumulate(normals.begin(), normals.end(), cv::Point3f(0,0,0));
    cv::Point3f mu_tangent = std::accumulate(tangents.begin(), tangents.end(),
                                             cv::Point3f(0,0,0));
    mu_normal *= 1.f / normals.size();
    mu_tangent *= 1.f / tangents.size();

    // Set position
    mu_fpt.setFeature3D(mu_xyz, mu_normal, mu_tangent);

    // Set covariances
    mu_fpt.setCovariance3D(pcov[0], pcov[1], pcov[2], pcov[4], pcov[5], pcov[8]);
    
    clustered_fpts.push_back(mu_fpt); 
  }
  profiler.leave("compute_mu_stats");

#if VIZ
  // Viz
  cv::Mat3f image = 0.5 * (frame.getNormalsRef().clone() + 1);
  for (auto& fpt: clustered_fpts) {
    const cv::KeyPoint& kp = fpt.get_keypoint();

    cv::Point2f tvec = cv::Point2f(kp.size, kp.response) * feat_size;    
    cv::line(image, kp.pt + tvec, kp.pt, cv::Scalar(0,0,1), 1, CV_AA);
    // cv::line(image, kp.pt + nvec, kp.pt, cv::Scalar(0,1,0), 1, CV_AA);
    cv::circle(image, kp.pt, feat_size, cv::Scalar(0,255,0) / 255.0, 1, CV_AA );
  }
  cv::imshow("non-max-suppression", image);    
#endif
  return;
}

void MSER3D::detect( const opencv_utils::Frame& frame,
                     std::vector<fsvision::Feature3D>& fpts) {

  profiler.enter("detect");

  // Multi-scale keypoint detection ====================================
  std::vector<fsvision::Feature3D> ms_fpts;
  detectImpl(frame, ms_fpts);
  if (!ms_fpts.size()) return;
  
  std::vector<cv::Scalar> colors(10);
  opencv_utils::fillColors(colors);

#if VIZ
  // Plot ellipses
  cv::Mat out = frame.getRGBRef().clone();
  for (auto& fpt : ms_fpts) {
    const cv::KeyPoint& kp = fpt.get_keypoint();

    cv::Scalar col = (fpt.xyz_valid() && fpt.normal_valid() && fpt.tangent_valid()) ?
        colors[kp.class_id%10] : cv::Scalar(0,0,0);
    
    cv::line(out, kp.pt, kp.pt + cv::Point2f(kp.size, kp.response) * 10, col, 1, CV_AA);
    cv::circle(out, kp.pt, 5, col, 1, CV_AA);
  }
  cv::imshow("ms_ellipses", out);
#endif

  return;
  
  // Temporarily remove features without depth  =======================
  for (std::vector<fsvision::Feature3D>::iterator it = ms_fpts.begin();
       it != ms_fpts.end(); ) {
    if (it->xyz_valid() && it->normal_valid() && it->tangent_valid())
      it++;
    else
      it = ms_fpts.erase(it);
  }

  // Non-max suppression by clustering  ===============================
  // Cluster keypoints that are within radius ball, and angle diff
  profiler.enter("non-max suppression");
  nonmaxSupression(frame, ms_fpts, fpts);
  profiler.leave("non-max suppression");

  profiler.leave("detect");
}


// Simple KeyPoint Tracking
std::vector<fsvision::Feature3D>
MSER3D::update( const opencv_utils::Frame& frame) {

  std::cerr << "Processing: " << frame.getTimestamp() << std::endl;

  // KeyPoint Detection
  std::vector<fsvision::Feature3D> fpts;
  detect(frame, fpts);

  // KeyPoint Tracker Update
  kptracker.update(frame, fpts);

  return kptracker.getStableFeatures(10);
}
        
        // // find ellipse (it seems cvfitellipse2 have error or sth?)
        // RotatedRect box = fitEllipse( r );

        // box.angle=(float)CV_PI/2-box.angle;
        // ellipse( ellipses, box, Scalar(196,255,255), 2 );


        // const vector<Point>& contour = new_pts[j];
        // for ( int k = 0; k < contour.size(); k++ ) {
        //   Point pt = r[j];
        //   img.at<Vec3b>(pt) = bcolors[i%9];
        // }

// void Mser3DFeatureDetector::detectImpl( const Mat& image, std::vector<KeyPoint>& keypoints, const Mat& mask ) const
// {
//     std::vector<std::vector<Point> > msers;

//     (*this)(image, msers, mask);

//     std::vector<std::vector<Point> >::const_iterator contour_it = msers.begin();
//     Rect r(0, 0, image.cols, image.rows);
//     for( ; contour_it != msers.end(); ++contour_it )
//     {
//         // TODO check transformation from MSER region to KeyPoint
//         RotatedRect rect = fitEllipse(Mat(*contour_it));
//         float diam = std::sqrt(rect.size.height*rect.size.width);

//         if( diam > std::numeric_limits<float>::epsilon() && r.contains(rect.center) )
//             keypoints.push_back( KeyPoint(rect.center, diam) );
//     }

// }

}



// void PyramidAdaptedFeatureDetector::detectImpl( const Mat& image, std::vector<KeyPoint>& keypoints, const Mat& mask ) const
// {
//     Mat src = image;
//     Mat src_mask = mask;

//     Mat dilated_mask;
//     if( !mask.empty() )
//     {
//         dilate( mask, dilated_mask, Mat() );
//         Mat mask255( mask.size(), CV_8UC1, Scalar(0) );
//         mask255.setTo( Scalar(255), dilated_mask != 0 );
//         dilated_mask = mask255;
//     }

//     for( int l = 0, multiplier = 1; l <= maxLevel; ++l, multiplier *= 2 )
//     {
//         // Detect on current level of the pyramid
//         std::vector<KeyPoint> new_pts;
//         detector->detect( src, new_pts, src_mask );
//         std::vector<KeyPoint>::iterator it = new_pts.begin(),
//                                    end = new_pts.end();
//         for( ; it != end; ++it)
//         {
//             it->pt.x *= multiplier;
//             it->pt.y *= multiplier;
//             it->size *= multiplier;
//             it->octave = l;
//         }
//         keypoints.insert( keypoints.end(), new_pts.begin(), new_pts.end() );

//         // Downsample
//         if( l < maxLevel )
//         {
//             Mat dst;
//             pyrDown( src, dst );
//             src = dst;

//             if( !mask.empty() )
//                 resize( dilated_mask, src_mask, src.size(), 0, 0, INTER_AREA );
//         }
//     }

//     if( !mask.empty() )
//         KeyPointsFilter::runByPixelsMask( keypoints, mask );
// }

// #if 0
//       // Subpixel accuracy
//       std::vector<cv::Point2f> pts_subpixel;
//       cv::KeyPoint::convert(ipts, pts_subpixel);

//       // Compute subpixel accurate features
//       // Based on the surface normal derivative
//       // obtained via MSER
//       cv::Size SubPix_winsize(5,5);
//       if (pts_subpixel.size()) { 
//         cv::TermCriteria SubPix_termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,10,0.001);
//         cv::cornerSubPix( der, pts_subpixel, SubPix_winsize, 
//                           cv::Size(-1,-1), SubPix_termcrit);
//       }
      
//       // Copy subpix points to keypoints
//       for (int j=0; j<ipts.size(); j++)
//         ipts[j].pt = pts_subpixel[j];

//       // Viz
//       for (int j=0; j<pts_subpixel.size(); j++) {
//         // Compute subpixel on the dxy image
//         cv::circle(der3, ipts[j].pt, 3, cv::Scalar(0,200,255), 1, CV_AA, 0);
//         cv::circle(der3, pts_subpixel[j], 1, cv::Scalar(0,200,0), 1, CV_AA, 0);
//         cv::rectangle(der3,
//                       ipts[j].pt - cv::Point2f(5,5),
//                       ipts[j].pt + cv::Point2f(5,5),
//                       cv::Scalar(0, 200, 200), 1, CV_AA);
//       }
// #endif




  // // Sobel detection with color images =================================
  // /// Generate grad_x and grad_y
  // int scale = 1;
  // int delta = 0;
  // int ddepth = CV_32F;
  // cv::Mat grad, grad_x, grad_y, abs_grad_x, abs_grad_y;
  
  // cv::Mat lab;
  // cv::cvtColor(src, lab, CV_BGR2Lab);
 
  // /// Gradient X
  // cv::Sobel( lab, grad_x, ddepth, 1, 0, 3, scale, delta, cv::BORDER_DEFAULT );   
  // cv::convertScaleAbs( grad_x, abs_grad_x );

  // /// Gradient Y  
  // cv::Sobel( lab, grad_y, ddepth, 0, 1, 3, scale, delta, cv::BORDER_DEFAULT );   
  // cv::convertScaleAbs( grad_y, abs_grad_y );

  // /// Total Gradient (approximate)
  // cv::addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad );

  // std::vector<cv::Mat> channels; 
  // cv::split(grad, channels);
  // assert(channels.size() == 3);

  // // Find max of R,G,B channels
  // cv::Mat max_grad; 
  // cv::max(channels[0], channels[1], max_grad); 
  // cv::max(max_grad, channels[2], max_grad);

  // cv::imshow("max_grad", max_grad);


  // // Find the edges
  // cv::Mat edges; 
  // cv::Canny(max_grad, edges, 230, 255, 3, true); 

  // std::vector<std::vector<cv::Point> > contours, filtered_contours;
  // std::vector<cv::Vec4i> hierarchy;
  // findContours( edges, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE );
  // // cv::drawContours(out, contours, -1, cv::Scalar(0,255,0), 2, CV_AA);

  // std::vector<std::vector<cv::Point> > approx_contours(contours.size());
  // for (int j=0; j<contours.size(); j++) { 
  //   cv::approxPolyDP(cv::Mat(contours[j]), approx_contours[j], 3, true);
  // }
  // cv::polylines(out, approx_contours, true, cv::Scalar(0,255,255), 1, CV_AA, 0);  
  // cv::imshow("grad", out);

  // return;
  // // cv::Mat dst = cv::Mat::zeros(image.size(), CV_8UC3);
  // // cv::Mat filtered_edges = Mat::zeros(image.size(), CV_8UC1);
  // // if( !contours.empty() && !hierarchy.empty() ) {
  // //   // iterate through all the top-level contours,
  // //   // draw each connected component with its own random color
  // //   int idx = 0;
  // //   for( ; idx >= 0; idx = hierarchy[idx][0] ) {
  // //     Scalar color( (rand()&255), (rand()&255), (rand()&255) );

  // //     bool found = false;
  // //     std::vector<cv::Point>& contoursj = contours[idx];
  // //     for (int j=0; j<contoursj.size(); j++)

  // //       if (imgb_th(contoursj[j].y,contoursj[j].x)[2] > 0) { 
  // //         filtered_contours.push_back(contoursj);
  // //         found = true;
  // //         break;
  // //       }
  // //     if (found) { 
  // //       drawContours( dst, contours, idx, color, CV_FILLED, 8, hierarchy );                
  // //       drawContours( filtered_edges, contours, idx, cv::Scalar(255), 1, 8, hierarchy );                
  // //     }
                
  // //   }
  // // }
  // // std::cerr << "filtered contours: " << filtered_contours.size() << std::endl;
  // // cv::imshow("filtered_edges", filtered_edges);


// inline std::pair<cv::Point2f, cv::Point2f>
// MSER3D::ellipse_midpoints(const cv::RotatedRect& rrect) {

//   // Get Rrect pts
//   std::vector<cv::Point2f> pts(4);
//   rrect.points(&pts[0]);

//   std::vector<cv::Point2f> mdpts(4);
//   for (int j=0; j<mdpts.size(); j++)
//     mdpts[j] = 0.5 * pts[j] + 0.5 * pts[(j+1)%pts.size()];

//   float maxx = 0, maxy = 0; 
//   cv::Point2f vsx, vsy;
//   for (int j=0; j<mdpts.size(); j++) { 
//     if (mdpts[j].x >= rrect.center.x) {
//       if (mdpts[j].x - rrect.center.x > maxx) { 
//         maxx = mdpts[j].x - rrect.center.x;
//         vsx = mdpts[j];
//       }
//     }
      
//     if (mdpts[j].y >= rrect.center.y) {
//       if (mdpts[j].y - rrect.center.y > maxy) { 
//         maxy = mdpts[j].y - rrect.center.y;
//         vsy = mdpts[j];
//       }
//     }
//   }
//   // assert(maxx != 0 && maxy != 0);

//   cv::Point2f v1 = vsx - rrect.center;
//   float v1norm = cv::norm(v1);

//   cv::Point2f v2 = vsy - rrect.center;
//   float v2norm = cv::norm(v2);

//   if (v1norm > v2norm) 
//     return std::make_pair(v1, v2);
//   else
//     return std::make_pair(v2, v1);  
// }



// static inline float
// contourContinuity(opencv_utils::Frame& frame, std::vector<cv::Point>& pts) {
//   std::vector<cv::Point> spts = shrink_contour(pts, 0.8);
  
//   std::vector<cv::Point>samples;
//   contour_from_pts(spts, samples);

//   if (samples.size() < 2) return 0.f;
  
//   const cv::Mat1b& mask = frame.getCombinedMaskRef();
//   int count = 0;
//   float sum = 0;

//   for (int j=0; j<samples.size()-1; j++) {
//     if (samples[j].x > 0 && samples[j].x < mask.cols-1 &&
//         samples[j].y > 0 && samples[j].y < mask.rows-1) {
//       sum += (mask(int(samples[j].y), int(samples[j].x)) ? 1 : 0);
//       count++;
//     } else {
//       return 0;
//     }
//   }
//   return sum / count; 
// }

// static std::vector<cv::KeyPoint>
// interestPointsFromRotatedRect(const cv::RotatedRect& rrect, 
//                               const int octave, 
//                               const int class_id) {
  
//   std::vector<cv::KeyPoint> kpts;
//   kpts.push_back(cv::KeyPoint(rrect.center, rrect.size.width/3.f, rrect.angle,
//                               rrect.size.height/3.f, octave, class_id));
//   return kpts;
// }



  // // Sobel detection with depth images =================================
  // const cv::Mat3f& cloud = frame.getCloudRef();
  // std::vector<cv::Mat> channels; 
  // cv::split(cloud, channels);
  
  // /// Generate grad_x and grad_y
  // int scale = 1;
  // int delta = 0;
  // int ddepth = CV_32F;
  // cv::Mat grad, grad_x, grad_y, abs_grad_x, abs_grad_y;
  
  // /// Gradient X
  // cv::Sobel( channels[2], grad_x, ddepth, 1, 0, 3, scale, delta, cv::BORDER_DEFAULT );
  // // cv::convertScaleAbs( grad_x, abs_grad_x );

  // /// Gradient Y  
  // cv::Sobel( channels[2], grad_y, ddepth, 0, 1, 3, scale, delta, cv::BORDER_DEFAULT );   
  // // cv::convertScaleAbs( grad_y, abs_grad_y );

  // /// Total Gradient (approximate)
  // cv::addWeighted( grad_x, 0.5, grad_y, 0.5, 0, grad );

  // // std::vector<cv::Mat> channels; 
  // // cv::split(grad, channels);
  // // assert(channels.size() == 3);

  // // Find max of R,G,B channels
  // cv::Mat max_grad; 
  // // cv::max(channels[0], channels[1], max_grad); 
  // // cv::max(max_grad, channels[2], max_grad);

  // opencv_utils::imshow("cloud", cloud);
  // opencv_utils::imshow("cloud_grad", grad);
