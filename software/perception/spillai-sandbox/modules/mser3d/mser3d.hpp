#ifndef __FSVISION_OPENCV_FEATURES_3D_HPP__
#define __FSVISION_OPENCV_FEATURES_3D_HPP__

// opencv 
#include <opencv2/opencv.hpp>

// opencv-utils includes
#include <perception_opencv_utils/opencv_utils.hpp>

// frame-utils includes
#include <pcl-utils/frame_utils.hpp>

// Profiler
#include <fs-utils/profiler.hpp>

// Types
#include <features3d/feature_types.hpp>

// Tracker
#include <features3d/keypoint_tracker.hpp>

// Boost Geometry
// #include <boost/geometry.hpp>
// #include <boost/geometry/geometries/register/point.hpp>
// #include <boost/geometry/geometries/register/ring.hpp>

// // BOOST_GEOMETRY_REGISTER_POINT_2D(cv::Point, int, boost::geometry::cs::cartesian, x, y)
// // BOOST_GEOMETRY_REGISTER_RING(std::vector<cv::Point>)


namespace fsvision {
static const float feat_size = 10;
// Maximal Stable Extremal Regions class for surface normals
class MSER3D {

 protected:
  fsvision::Profiler profiler;
  fsvision::KeyPointTracker kptracker;
  
  int pyramid_levels;
  int delta, min_area, max_area, edge_blur_size, max_evolution;
  double max_variation, min_diversity, area_threshold, min_margin;
  
  opencv_utils::MSER mser;
  
 public:
  MSER3D( int _pyramid_levels=0, int _delta=2, int _min_area=100, int _max_area=320*240,
          double _max_variation=0.5, double _min_diversity=0.5, // .25
          int _max_evolution=200, double _area_threshold=1.01, 
          double _min_margin=0.003, int _edge_blur_size=3 );
  // MSER3D( int _pyramid_levels=1, int _delta=5, int _min_area=50, int _max_area=160*120,
  //         double _max_variation=0.25, double _min_diversity=.2,
  //         int _max_evolution=200, double _area_threshold=1.01, 
  //         double _min_margin=0.003, int _edge_blur_size=11 );


  cv::Mat maxEdgeResponse(const cv::Mat& src);
  
  void detectImpl(const opencv_utils::Frame& frame,
                  std::vector<fsvision::Feature3D>& fpts);
  
  void detect( const opencv_utils::Frame& frame,
               std::vector<fsvision::Feature3D>& fpts);

  std::vector<fsvision::Feature3D> update( const opencv_utils::Frame& frame); 
  
  // void detect( const cv::Mat& image, std::vector<cv::KeyPoint>& msers,
  //              const cv::Mat& mask=cv::Mat() );

  void
  nonmaxSupression(const opencv_utils::Frame& frame,
                   const std::vector<fsvision::Feature3D>& ms_fpts,
                   std::vector<fsvision::Feature3D>& clustered_fpts);
  
  // static inline std::pair<cv::Point2f, cv::Point2f>
  // ellipse_midpoints(const cv::RotatedRect& rrect);

  
};

}

#endif
