// 
// Author: Sudeep Pillai (spillai@csail.mit.edu)
// Updates: Dec 20, 2013
//

#include "general_purpose_feature_tracker2.hpp"
#define VIZ 1
namespace fsvision { 
//============================================
// GeneralPurposeFeatureTracker class
//============================================
GeneralPurposeFeatureTracker::GeneralPurposeFeatureTracker (
    const bool use_gftt, const bool enable_subpixel_refinement,
    const int num_feats, const int min_add_radius, const int feat_block_size, 
    const int feature_match_threshold, const int feature_distance_threshold,
    const int allowed_skips, const int allowed_predictions) { 

  //--------------------------------------------
  // Params
  //--------------------------------------------
  gpft_use_gftt_ = use_gftt;
  gpft_enable_subpixel_refinement_ = enable_subpixel_refinement;
  gpft_num_feats_ = num_feats, gpft_min_add_radius_ = min_add_radius,
      gpft_feat_block_size_ = feat_block_size,
      gpft_feature_match_threshold_ = feature_match_threshold,
      gpft_feature_distance_threshold_ = feature_distance_threshold,
      gpft_allowed_skips_ = allowed_skips,
      gpft_allowed_predictions_ = allowed_predictions;
  gpft_feature_distance_thresholdsq_ = std::pow(gpft_feature_distance_threshold_,2);

  std::cerr << "Params: " << std::endl;
  std::cerr << "use_gftt: " << gpft_use_gftt_ << std::endl;
  std::cerr << "enable_subpixel_refinement: " << gpft_enable_subpixel_refinement_ << std::endl;
  std::cerr << "num_feats: " << gpft_num_feats_ << std::endl;
  std::cerr << "min_add_radius: " << gpft_min_add_radius_ << std::endl;
  std::cerr << "feat_block_size: " << gpft_feat_block_size_ << std::endl;
  std::cerr << "feature_match_threshold: " << gpft_feature_match_threshold_ << std::endl;
  std::cerr << "feature_distance_threshold: " << gpft_feature_distance_threshold_ << std::endl;
  std::cerr << "allowed_skips: " << gpft_allowed_skips_ << std::endl;
  std::cerr << "allowed_predictions: " << gpft_allowed_predictions_ << std::endl;

  //--------------------------------------------
  // Setup Track Manager
  //--------------------------------------------
  track_manager_.setParams(gpft_num_feats_, gpft_allowed_predictions_, gpft_allowed_skips_);

  //--------------------------------------------
  // Profiler naming
  //--------------------------------------------
  profiler_.setName("GPFT");
  
  //--------------------------------------------
  // FAST / GFTT Feature Detector
  //--------------------------------------------
  int gftt_maxCorners=gpft_num_feats_; 
  int gftt_blockSize=gpft_feat_block_size_;
  double gftt_qualityLevel=0.04; double gftt_minDistance=5;
  bool gftt_useHarrisDetector=false; double gftt_k=0.04 ;

  int fast_threshold = 20; bool nonmax = true;
  
  if (gpft_use_gftt_) { 
    cv::Ptr<cv::FeatureDetector> detector(
        new cv::GFTTDetector( gftt_maxCorners, gftt_qualityLevel, gftt_minDistance,
                              gftt_blockSize, gftt_useHarrisDetector, gftt_k) );
    detector_ = cv::Ptr<cv::PyramidAdaptedFeatureDetector>
        (new cv::PyramidAdaptedFeatureDetector(detector, 0) );
  }
  else  { 
    cv::Ptr<cv::FeatureDetector> detector(
        new cv::FastFeatureDetector( fast_threshold, nonmax ));
    detector_ = cv::Ptr<cv::PyramidAdaptedFeatureDetector>
        (new cv::PyramidAdaptedFeatureDetector(detector, 0) );
  }
 
  //--------------------------------------------
  // Feature Extractor
  //--------------------------------------------
  extractor_ = cv::DescriptorExtractor::create("BRIEF");

  //--------------------------------------------
  // Descriptor Matcher
  //--------------------------------------------
  // BruteForce-Hamming, BruteForce-HammingLUT, BruteForce, 
  // FlannBased, BruteForce-SL2, BruteForce-Hamming(2)
  matcher_ = cv::DescriptorMatcher::create( "BruteForce-Hamming");

  if( extractor_.empty() || matcher_.empty()  ) {
    cout << "Failed to create detector/descriptor/extractor" << endl;
    exit(0);
  }

}

GeneralPurposeFeatureTracker::~GeneralPurposeFeatureTracker () { 
}

// Main assumption is that the rate at which frames are processed is constant
// i.e constant frame rate
void
GeneralPurposeFeatureTracker::processFrame(opencv_utils::Frame& frame, cv::Mat mask) {
  // Profiler
  profiler_.enter("processFrame()");

  // Profiler, and debugging
  std::stringstream ss;
  profiler_.enter("detectAndAddFeatures()");

  //--------------------------------------------
  // Detect features (at multiple scales) w/ mask
  // 1. First cleanup old tracks
  //    Also prune long tracks
  //--------------------------------------------
  track_manager_.cleanup();
  //--------------------------------------------
  // 2. Retrieve previous stable features
  //--------------------------------------------
  std::vector<Feature3D> p_tpts;
  track_manager_.getStableFeatures(p_tpts);
  std:: cerr << "getstable: " << p_tpts.size() << std::endl;
  
  //--------------------------------------------
  // Create Mask 
  // If no previous pts, mask is just ones
  // Combined Depth/Cloud/Previous pts mask
  //--------------------------------------------
  // const cv::Mat1b& mask = cv::Mat(); // frame_.getImageMaskRef();

  //--------------------------------------------
  // 3. Detect features (at multiple scales) w/o mask
  // Determine correspondences 
  // a. Prop. corr from p_tpts to c_tpts
  //    c_tpts are appropriately ID'd
  //    Note: Here, the descriptors are extracted within
  // b. Mask out regions that are occupied by c_tpts
  // c. Detect features with above mask
  // d. Extract descriptors for new features
  // e. Add new features (id=-1) to c_tpts 
  //    track_manager takes care of IDing
  //--------------------------------------------
  std::vector<Feature3D> c_tpts;
  detectFeatures(c_tpts, mask);
  ss << "Detected Features: " << c_tpts.size() << std::endl;
  
  //--------------------------------------------
  // 4. Feature description, Depth/Surface normal pruning
  //--------------------------------------------
  describeFeatures(c_tpts);
  ss << "Described Features: " << c_tpts.size() << std::endl;

  // //--------------------------------------------
  // // 5. Match Features
  // // Find correspondences with expected locations mask
  // // Appropriately ID c_tpts, if ID -1, then mark for adding
  // //--------------------------------------------
  // if (p_tpts.size() && c_tpts.size()) { 
  //   matchFeatures(p_tpts, c_tpts);
  // } 

  //--------------------------------------------
  // 6. Add features
  //--------------------------------------------
  addFeatures(c_tpts);
  
  // Profiler
  profiler_.leave("detectAndAddFeatures()");
  profiler_.debug("detectAndAddFeatures() "+ss.str());

  //--------------------------------------------
  // 7. Viz
  //--------------------------------------------
  visualize();
  
  // Profiler
  profiler_.leave("processFrame()");
# if VIZ
  // cv::waitKey(1);
#endif

  // Push
  frame_queue_.push(frame); 
}
  

void
GeneralPurposeFeatureTracker::detectFeatures (std::vector<fsvision::Feature3D>& tpts,
                                              const cv::Mat& mask) { 

  profiler_.enter("detectFeatures(kpts,scale,mask)");
  std::stringstream ss;

  // Get Frame
  opencv_utils::Frame frame_;
  if (!frame_queue_.empty())
    frame_queue_.get_latest(frame_);
  else
    return;
  
  //--------------------------------------------
  // Gaussian / Box Blur 
  // TODO add mode for doing box blur
  //--------------------------------------------
  cv::Mat gray;
  const cv::Mat& _gray = frame_.getGrayRef();
  cv::GaussianBlur(_gray, gray, cv::Size(5,5), 0, 0, cv::BORDER_DEFAULT);

  // Set gray image (for later use)
  frame_.setGray(gray); 
  
  //--------------------------------------------
  // Detect keypoints
  // TODO mask from previous tracked features
  //--------------------------------------------
  profiler_.enter("KEYPOINT DETECTION");
  std::vector<cv::KeyPoint> kpts;
  detector_->detect( gray, kpts, mask );
  profiler_.leave("KEYPOINT DETECTION");
        
  // //--------------------------------------------
  // // Subpixel accuracy
  // // TODO Subpixel window size
  // //--------------------------------------------
  // // profiler_.enter("SUBPIXEL-REFINEMENT");
  // if (gpft_enable_subpixel_refinement_) {
  //   cv::TermCriteria SubPix_termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,10,0.05);
  //   cv::Size SubPix_winsize(5,5);
  //   std::vector<cv::Point2f> sp_pts;
  //   cv::KeyPoint::convert(kpts, sp_pts);
  //   cv::cornerSubPix( gray, sp_pts, SubPix_winsize, 
  //                     cv::Size(-1,-1), SubPix_termcrit);
  //   cv::KeyPoint::convert(sp_pts, kpts);
  // }
  // // profiler_.leave("SUBPIXEL-REFINEMENT");

# if VIZ
  {
    cv::Mat display;
    frame_.getRGBRef().copyTo(display, frame_.getImageMaskRef());

    std::vector<cv::Vec3b> colors(6);
    opencv_utils::fillColors( colors );

    for (int j=0; j<kpts.size(); j++) {
      cv::Scalar col = colors[kpts[j].octave];
      circle(display, kpts[j].pt, gpft_feat_block_size_/2, col, 1, CV_AA);
      // circle(display, kpts[j].pt, 1, cv::Scalar(0,240,0), 1, CV_AA);
    }
    cv::imshow("pts", display);
    cv::imshow("mask", mask);
  }
#endif

  // Init pts
  int64_t now = frame_.getTimestamp();
  tpts = std::vector<fsvision::Feature3D>(kpts.size());
  for (int j=0; j<kpts.size(); j++) {
    tpts[j] = Feature3D(now, -1);
    tpts[j].set_keypoint(kpts[j]);
    tpts[j].set_status(Feature::DETECTED);
  }
  
  profiler_.leave("detectFeatures(kpts,scale,mask)");
  // profiler_.debug("detectFeatures(kpts,scale,mask) "+ss.str());
  return;
}


void
GeneralPurposeFeatureTracker::addFeatures (std::vector<Feature3D>& tpts) { 
  //--------------------------------------------
  // Profiler
  //--------------------------------------------
  // profiler_.enter("addFeatures(tpts)");

  //--------------------------------------------
  // Add Features and describe them
  //--------------------------------------------
  track_manager_.addFeatures(tpts);

  // profiler_.leave("addFeatures(tpts)");
}


void
GeneralPurposeFeatureTracker::describeFeatures (std::vector<Feature3D>& tpts) {
  if (!tpts.size()) return;

  // Get Frame
  opencv_utils::Frame frame_;
  if (!frame_queue_.empty())
    frame_queue_.get_latest(frame_);
  else
    return;

  profiler_.enter("describeFeatures(kpts,desc)");
  std::stringstream ss;

  //--------------------------------------------
  // Prune invalid/poor Depth/Normal Features
  //--------------------------------------------
  setDepthFeatures(tpts); 
        
  //--------------------------------------------
  // Convert tpts to kpts
  // Retain index of tpts, kpts[j].class_id  is
  // preserved
  //--------------------------------------------
  std::vector<cv::KeyPoint> kpts;
  Feature3D::convert(tpts, kpts); // index -> class_id
  ss << "Converting : " << kpts.size() << std::endl;

  //--------------------------------------------
  // Extract descriptors
  //--------------------------------------------
  cv::Mat desc;
  extractor_->compute(frame_.getGrayRef(), kpts, desc);
  // std::vector<cv::Mat> desc;
  // describeFeatures(frame_, kpts, desc);
  ss << "Keypoints: " << kpts.size() << " desc: " << desc.size() << std::endl;
  // assert(kpts.size() != 0);

  //--------------------------------------------
  // Save desc
  //--------------------------------------------
  // assert(tpts.size() == desc.rows);
  std::map<int64_t, cv::Mat> id_desc;
  for (int j=0; j<kpts.size(); j++ ) { 
    int idx = kpts[j].class_id;
    id_desc[idx] = desc.row(j);
  }

  // ss << "IDs descs: " << id_desc.size() << std::endl;
  // ss << "tpts size: " << tpts.size() << std::endl;

  //--------------------------------------------
  // Remove points if no descriptions
  //--------------------------------------------
  int idx = 0;
  for (std::vector<Feature3D>::iterator it = tpts.begin(); it != tpts.end(); idx++) { 
    if (id_desc.find(idx) == id_desc.end()) { 
      it = tpts.erase(it);
    } else { 
      it->set_desc(id_desc[idx]);
      it++;
    }            
  }

  // ss << "Describing : " << tpts.size() << std::endl;

  profiler_.leave("describeFeatures(kpts,desc)");
  // profiler_.debug("describeFeatures(kpts,desc) " + ss.str());
  return;
}

inline bool is_nan(const cv::Vec3f& v) {
  return (v[0] != v[0]);
}
inline bool is_nan(const cv::Point3f& v) {
  return (v.x != v.x);
}

void
GeneralPurposeFeatureTracker::setDepthFeatures (std::vector<Feature3D>& tpts) {

  // Get Frame
  opencv_utils::Frame frame_;
  if (!frame_queue_.empty())
    frame_queue_.get_latest(frame_);
  else
    return;

  // profiler_.enter("prune Depth Features()");
  std::stringstream ss;
  ss << "# PTS BEFORE: " << tpts.size() << std::endl;

  //--------------------------------------------
  // Patch normal variation
  //--------------------------------------------
  // const cv::Mat_<uint16_t>& depth = frame_.getDepthRef();
  const cv::Mat3f& cloud = frame_.getCloudRef();
  const cv::Mat3f& normals = frame_.getNormalsRef();

  // std::vector<cv::Point2f> pts(tpts.size());
  // for (int j=0; j<tpts.size(); j++) 
  //   pts[j] = tpts[j].get_point();
  // std::vector<cv::Point3f> pts3d = opencv_utils::get_xyz(depth, pts);
  
  for (int j=0; j<tpts.size(); j++) {
    // // Set Feature if not nan
    // if (is_nan(pts3d[j])) continue;
    // tpts[j].setFeature3D(pts3d[j]);
    const cv::Point2f& p = tpts[j].get_point();
    
    // Set normal if not nan
    if (!cloud.empty() && !is_nan(cloud(p))) { 
      if (!normals.empty() && !is_nan(normals(p))) {
        tpts[j].setFeature3D(cloud(p), normals(p)); // normal, tangent set to NaN
      } else {
        tpts[j].setFeature3D(cloud(p)); // normal, tangent set to NaN
      }
    }
  }
  ss << "# PTS AFTER: " << tpts.size() << std::endl;
  // profiler_.leave("prune Depth Features()");      
  // profiler_.debug("prune Depth Features()" + ss.str());      
}


void GeneralPurposeFeatureTracker::visualize() {
  
  // Get Frame
  opencv_utils::Frame frame_;
  if (!frame_queue_.empty())
    frame_queue_.get_latest(frame_);
  else
    return;
  
  const int min_track_size = 1;
  std::vector<fsvision::Feature3D> fpts = getAllFeatures(min_track_size);

  //----------------------------------
  // KeyPoint viz
  //----------------------------------
  cv::Mat3b out; 
  frame_.getRGBRef().copyTo(out); 

  // Colors
  std::vector<cv::Vec3b> colors(30);
  opencv_utils::fillColors( colors );
    
  fsvision::Feature3D pfpt;
  for (auto& fpt: fpts) {
    const cv::KeyPoint& kp = fpt.get_keypoint();
    cv::Point2f tvec = cv::Point2f(kp.size, kp.response) * 5;

    // cv::line(out, kp.pt + tvec, kp.pt, cv::Scalar(0,0,255), 1, CV_AA);
    cv::Scalar col = colors[fpt.get_id()%colors.size()];
    // cv::Scalar col = colors[fpt.get_id()%colors.size()];
    // cv::circle(out, kp.pt, 5, cv::Scalar(0,128,0), 1, CV_AA );

    // Draw correspondences between frames
    if (pfpt.get_id() == fpt.get_id()) {
      cv::line(out, kp.pt, pfpt.get_point(), col, 1, CV_AA);
    } else {
      cv::circle(out, kp.pt, 2, col, 1, CV_AA );
    }
    // copy 
    pfpt = fpt;

    // cv::addText(out, cv::format("%3.2f",gpft.track_manager_.track_add_rate_), 
    //         cv::Point(20,20), cv::fontQt("Times")); 
    // cv::addText(out, cv::format("%i",gpft.track_manager_.tracklet_map_.size()),
    //         cv::Point(20,40), cv::fontQt("Times")); 
      
  }
  cv::imshow("GPFT", out);
}

std::vector<fsvision::Feature3D>
GeneralPurposeFeatureTracker::getAllFeatures(const int min_track_size) { 

  // Retrieve all features
  std::vector<fsvision::Feature3D> fpts;
  track_manager_.getAllFeatures(fpts, min_track_size);
        
  return fpts;
}

// void GeneralPurposeFeatureTracker::flowSparse (const cv::Mat& pgray, 
//                                                const cv::Mat& gray, 
//                                                std::vector<cv::Point2f>& pts_in, 
//                                                std::vector<cv::Point2f>& pts_out, 
//                                                std::vector<uchar>& status, 
//                                                std::vector<float>& err) { 

//   //--------------------------------------------
//   // Profiler
//   //--------------------------------------------
//   profiler_.enter("flowSparse()");

// #if GPU_ENABLED
//   gpframe_.upload(pgray);
//   if (gframe_.empty() || gpframe_.empty()) return;
// #endif
  
//   // Set status
//   status.resize(pts_in.size(), 0);

//   // Set error status
//   err.resize(pts_in.size(), 10000.0);

//   // Convert
//   const int width = gray.cols;
//   const int height = gray.rows;

//   // Sparse optical flow
//   float deriv_lambda = 0.1; float min_eig = 1e-3; cv::Size LK_winsize(5,5);
//   cv::TermCriteria LK_termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,10,0.1);
//   cv::calcOpticalFlowPyrLK(
//       pgray, 
//       gray, 
//       pts_in, 
//       pts_out, 
//       status, 
//       err, 
//       LK_winsize,3,LK_termcrit); // , 0, 0.001, deriv_lambda);

//   // Predicted  points
//   for (int j=0; j<pts_out.size(); j++) {
//     if ( status[j] && // check status
//          pts_out[j].x >= 0 && pts_out[j].x < width && // check bounds
//          pts_out[j].y >= 0 && pts_out[j].y < height &&
//          cv::norm(pts_out[j]-pts_in[j]) < std::sqrt(gpft_feature_distance_threshold_)) {
//       status[j] = 1;
//     } else {
//       status[j] = 0;
//       pts_out[j] = cv::Point2f(-1,-1);
//     }
//   }

// #if VIZ
//   // Viz before removing invalid tpts
//   {
//     cv::Mat display = frame_.getRGBRef().clone();
//     assert(pts_in.size() == pts_out.size());
//     for (int j=0; j<pts_in.size(); j++) {
//       cv::Scalar col = status[j] ? cv::Scalar(0,255,255) : cv::Scalar(0,0,0);
//       if (!status[j]) continue;
//       cv::line(display,
//                pts_in[j],
//                pts_out[j],
//                col, 1, CV_AA);
//       circle(display, pts_in[j], 1, col, 1, CV_AA);
//       circle(display, pts_out[j], 1, col, 1, CV_AA);
//     }
//     cv::imshow("flow ", display);
//   }
// #endif
        
//   //--------------------------------------------
//   // Profiler
//   //--------------------------------------------
//   profiler_.leave("flowSparse()");
        
// }


}
