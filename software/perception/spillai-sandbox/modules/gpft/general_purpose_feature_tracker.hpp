// 
// Author: Sudeep Pillai (spillai@csail.mit.edu) Mar 16, 2013
// Updates: Aug 04, 2013
// 

#ifndef GENERAL_PURPOSE_FEATURE_TRACKER_HPP_
#define GENERAL_PURPOSE_FEATURE_TRACKER_HPP_

// Standard includes
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <deque>
#include <map>
#include <set> 

// non-free opencv includes
#include <opencv2/nonfree/nonfree.hpp>

// opencv-utils includes
#include <perception_opencv_utils/opencv_utils.hpp>

// frame-utils includes
#include <pcl-utils/frame_utils.hpp>

// Profiler
#include <fs-utils/profiler.hpp>
#include <fs-utils/thread_safe_queue.hpp>

// Types
#include <features3d/feature_types.hpp>
#include <features3d/keypoint_tracker.hpp>

#define GPU_ENABLED 0
#define USE_GFTT 1

#if GPU_ENABLED
#include <opencv2/cuda.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudafeatures2d.hpp>
#endif

// #include "subdivision2d.hpp"

// #define DEBUG_CAPS 1

// const int GPFT_MAX_TRACK_TIMESTAMP_DELAY = 1.f; // in seconds
// const float GPFT_MIN_FLOW_MATCH_DISTANCE = 3.f;
// const float GPFT_MIN_DESC_MATCH_DISTANCE = 100.f;
// const float GPFT_MAX_TRACK_TIMESPAN = 0.5f;
// const float GPFT_ADD_POINT_DISTANCE_THRESHOLD = 10.f;
// const float GPFT_ADD_POINT_DISTANCE_THRESHOLD_SQ = std::pow(GPFT_ADD_POINT_DISTANCE_THRESHOLD,2);
// const float GPFT_POINT_SPACING_THRESHOLD = 20.f;

// struct node_info { 
//     int idx;
//     float dist_normal;
//     float dist_euclidean;
// };


namespace fsvision { 

// #define FEATURE_MATCH_THRESHOLD 20 // 20 for BRIEF, 100 for FREAK,
// #define FEATURE_DISTANCE_THRESHOLD 15
// #define FEATURE_DISTANCE_THRESHOLD_SQR FEATURE_DISTANCE_THRESHOLD*FEATURE_DISTANCE_THRESHOLD
// #define ALLOWED_SKIPS 5
// #define ALLOWED_PREDICTIONS 0

class GeneralPurposeFeatureTracker { 
  enum TrackingMode { TRACK2D, TRACK3D, TRACK3D_WITH_NORMALS };
 private: 
  TrackingMode mode_;
 public:

  //--------------------------------------------
  // Constants/Variables
  //--------------------------------------------
  bool gpft_enable_subpixel_refinement_, gpft_use_gftt_;
  int gpft_num_feats_, gpft_min_add_radius_, gpft_feat_block_size_,
    gpft_feature_match_threshold_, gpft_feature_distance_threshold_, 
    gpft_allowed_skips_, gpft_allowed_predictions_;
  int gpft_feature_distance_thresholdsq_;
  
  //--------------------------------------------
  // Feature Extractor, Descriptor, and Matcher
  //--------------------------------------------
  // cv::Ptr<cv::FeatureDetector> detector;
  cv::Ptr<cv::DescriptorExtractor> extractor_; 
  cv::Ptr<cv::DescriptorMatcher> matcher_; 
  cv::Ptr<cv::BOWTrainer> vocab_;

#if GPU_ENABLED
  // #if USE_GFTT
  cv::Ptr<cv::cuda::CornersDetector> detector_;
  // #else
  cv::Ptr<cv::cuda::FAST_CUDA> fast_detector_;
  // #endif
#else
  cv::Ptr<cv::PyramidAdaptedFeatureDetector> detector_;
#endif
  //--------------------------------------------
  // KeyPoint Tracker 
  //--------------------------------------------
  // fsvision::KeyPointTracker kptracker_;
  TrackManager<fsvision::Feature3D> track_manager_;
  Profiler profiler_;

  //--------------------------------------------
  // Image history for optical flow
  //--------------------------------------------
  // float scale_factor_;
  // Frame frame_, pframe_;
  FixedLengthQueue<opencv_utils::Frame, 2> frame_queue_;
  opencv_utils::Frame frame_, pframe_;

#if GPU_ENABLED
  cv::cuda::GpuMat gframe_, gpframe_;
#endif
  
 public:
  GeneralPurposeFeatureTracker (const bool use_gftt=false, const bool enable_subpixel_refinement_=false,
                                const int num_feats=1500, const int min_add_radius=10,
                                const int feat_block_size=7, 
                                const int feature_match_threshold=50, const int feature_distance_threshold=15,
                                const int allowed_skips=5, const int allowed_predictions=5);
  ~GeneralPurposeFeatureTracker ();

  void processFrame(opencv_utils::Frame& frame, cv::Mat mask = cv::Mat());
  // void processImage(cv::Mat& img, const cv::Mat& mask = cv::Mat());
  
  std::vector<Feature3D> getAllFeatures(const int min_track_size=TRACK_SIZE_LIMIT);
  std::vector<Feature3D> getStableFeatures();
  
  void medianFlow (const cv::Mat& pgray, 
    const cv::Mat& gray, 
    std::vector<Feature3D>& pts_in, 
    std::vector<Feature3D>& pts_out, 
    std::vector<uchar>& status);
  void medianFlowSparse (const cv::Mat& pgray, 
    const cv::Mat& gray, 
    std::vector<Feature3D>& pts_in, 
    std::vector<Feature3D>& pts_out, 
    std::vector<uchar>& status, 
    std::vector<float>& err);
  void flowSparse (const cv::Mat& pgray, 
    const cv::Mat& gray, 
    std::vector<Feature3D>& pts_in, 
    std::vector<Feature3D>& pts_out, 
    std::vector<uchar>& status, 
    std::vector<float>& err);

  void flowSparse (const cv::Mat& pgray, 
    const cv::Mat& gray, 
    std::vector<cv::Point2f>& pts_in, 
    std::vector<cv::Point2f>& pts_out, 
    std::vector<uchar>& status, 
    std::vector<float>& err);
        
  void addFeatures (std::vector<Feature3D>& tpts);

  void corrFeaturesSparse(std::vector<Feature3D>& p_tpts, 
    std::vector<Feature3D>& c_tpts,
    const cv::Mat& mask = cv::Mat());
  
  void corrFeatures (std::vector<Feature3D>& p_kpts, 
    std::vector<Feature3D>& c_tpts);
  
  void matchFeatures (std::vector<Feature3D>& p_kpts, 
    std::vector<Feature3D>& c_tpts);

  
  void detectFeatures(std::vector<fsvision::Feature3D>& tpts, 
    const cv::Mat& mask = cv::Mat());
  // void describeFeatures (const opencv_utils::Frame& frame,
  //   std::vector<cv::KeyPoint>& kpts, std::vector<cv::Mat>& desc);
  void describeFeatures (std::vector<Feature3D>& tpts);
  void setDepthFeatures (std::vector<Feature3D>& tpts);

  void pruneFeatures(std::vector<Feature3D>& p_tpts, 
    std::vector<Feature3D>& c_tpts);

  void visualize();
};


}

#endif // GENERAL_PURPOSE_FEATURE_TRACKER_HPP_
