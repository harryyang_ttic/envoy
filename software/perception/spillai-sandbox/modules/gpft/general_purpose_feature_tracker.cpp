// 
// Author: Sudeep Pillai (spillai@csail.mit.edu) Mar 16, 2013
// Updates: Aug 04, 2013
// 
#include "general_purpose_feature_tracker.hpp"
#define VIZ 1
namespace fsvision { 
//============================================
// GeneralPurposeFeatureTracker class
//============================================
GeneralPurposeFeatureTracker::GeneralPurposeFeatureTracker (
    const bool use_gftt, const bool enable_subpixel_refinement,
    const int num_feats, const int min_add_radius, const int feat_block_size, 
    const int feature_match_threshold, const int feature_distance_threshold,
    const int allowed_skips, const int allowed_predictions) { 

  //--------------------------------------------
  // Params
  //--------------------------------------------
  gpft_use_gftt_ = use_gftt;
  gpft_enable_subpixel_refinement_ = enable_subpixel_refinement;
  gpft_num_feats_ = num_feats, gpft_min_add_radius_ = min_add_radius,
      gpft_feat_block_size_ = feat_block_size,
      gpft_feature_match_threshold_ = feature_match_threshold,
      gpft_feature_distance_threshold_ = feature_distance_threshold,
      gpft_allowed_skips_ = allowed_skips,
      gpft_allowed_predictions_ = allowed_predictions;
  gpft_feature_distance_thresholdsq_ = std::pow(gpft_feature_distance_threshold_,2);

  std::cerr << "Params: " << std::endl;
  std::cerr << "use_gftt: " << gpft_use_gftt_ << std::endl;
  std::cerr << "enable_subpixel_refinement: " << gpft_enable_subpixel_refinement_ << std::endl;
  std::cerr << "num_feats: " << gpft_num_feats_ << std::endl;
  std::cerr << "min_add_radius: " << gpft_min_add_radius_ << std::endl;
  std::cerr << "feat_block_size: " << gpft_feat_block_size_ << std::endl;
  std::cerr << "feature_match_threshold: " << gpft_feature_match_threshold_ << std::endl;
  std::cerr << "feature_distance_threshold: " << gpft_feature_distance_threshold_ << std::endl;
  std::cerr << "allowed_skips: " << gpft_allowed_skips_ << std::endl;
  std::cerr << "allowed_predictions: " << gpft_allowed_predictions_ << std::endl;

  //--------------------------------------------
  // Setup Track Manager
  //--------------------------------------------
  track_manager_.setParams(gpft_num_feats_, gpft_allowed_predictions_, gpft_allowed_skips_);

  //--------------------------------------------
  // Profiler naming
  //--------------------------------------------
  profiler_.setName("GPFT");
  
  //--------------------------------------------
  // FAST / GFTT Feature Detector
  //--------------------------------------------
  int gftt_maxCorners=gpft_num_feats_; double gftt_qualityLevel=0.04; double gftt_minDistance=5;
  int gftt_blockSize=gpft_feat_block_size_; bool gftt_useHarrisDetector=false; double gftt_k=0.04 ;
  int fast_threshold = 20; bool nonmax = true;
  
#if GPU_ENABLED // WITH GPU
  cv::cuda::printShortCudaDeviceInfo(cv::cuda::getDevice());

  if (gpft_use_gftt_) 
    detector_ = cv::cuda::createGoodFeaturesToTrackDetector
        ( CV_8UC1, gftt_maxCorners, gftt_qualityLevel, gftt_minDistance,
          gftt_blockSize, gftt_useHarrisDetector, gftt_k);
  else 
    fast_detector_ = cv::Ptr<cv::cuda::FAST_CUDA>
        (new cv::cuda::FAST_CUDA( fast_threshold, nonmax ) );
  
#else // NO GPU

  if (gpft_use_gftt_) { 
    cv::Ptr<cv::FeatureDetector> detector(
        new cv::GFTTDetector( gftt_maxCorners, gftt_qualityLevel, gftt_minDistance,
                              gftt_blockSize, gftt_useHarrisDetector, gftt_k) );
    detector_ = cv::Ptr<cv::PyramidAdaptedFeatureDetector>
        (new cv::PyramidAdaptedFeatureDetector(detector, 0) );
  }
  else  { 
    cv::Ptr<cv::FeatureDetector> detector(
        new cv::FastFeatureDetector( fast_threshold, nonmax ));
    detector_ = cv::Ptr<cv::PyramidAdaptedFeatureDetector>
        (new cv::PyramidAdaptedFeatureDetector(detector, 0) );
  }
#endif
 
  //--------------------------------------------
  // Feature Extractor
  //--------------------------------------------
  extractor_ = cv::DescriptorExtractor::create("BRIEF");

  //--------------------------------------------
  // Descriptor Matcher
  //--------------------------------------------
  // BruteForce-Hamming, BruteForce-HammingLUT, BruteForce, 
  // FlannBased, BruteForce-SL2, BruteForce-Hamming(2)
  matcher_ = cv::DescriptorMatcher::create( "BruteForce-Hamming");

  if( extractor_.empty() || matcher_.empty()  ) {
    cout << "Failed to create detector/descriptor/extractor" << endl;
    exit(0);
  }

  // cv::initModule_nonfree();
  // cv::SURF* detector = new cv::SURF(5e-3);

}

// GeneralPurposeFeatureTracker::GeneralPurposeFeatureTracker (const GeneralPurposeFeatureTracker& gpft) { 
//     std::cerr << "GeneralPurposeFeatureTracker copy: " << std::endl;
// }

GeneralPurposeFeatureTracker::~GeneralPurposeFeatureTracker () { 
}

std::vector<fsvision::Feature3D>
GeneralPurposeFeatureTracker::getAllFeatures(const int min_track_size) { 

  // Retrieve all features
  std::vector<fsvision::Feature3D> fpts;
  track_manager_.getAllFeatures(fpts, min_track_size);
        
  return fpts;
}


std::vector<fsvision::Feature3D>
GeneralPurposeFeatureTracker::getStableFeatures() { 

  // Retrieve stable features
  std::vector<fsvision::Feature3D> fpts;
  track_manager_.getStableFeatures(fpts);
        
  return fpts;
}


void
GeneralPurposeFeatureTracker::processFrame(opencv_utils::Frame& frame, cv::Mat mask) {
  int64_t now = frame_.getTimestamp();
  
  // Profiler
  profiler_.enter("processFrame()");

  //--------------------------------------------
  // Set frame
  //--------------------------------------------
  frame_queue_.push(frame);
  frame_ = frame;
  
  // Profiler, and debugging
  std::stringstream ss;
  profiler_.enter("detectAndAddFeatures()");

  //--------------------------------------------
  // Detect features (at multiple scales) w/ mask
  // 1. First cleanup old tracks
  //    Also prune long tracks
  //--------------------------------------------
  track_manager_.cleanup(now);
  //--------------------------------------------
  // 2. Retrieve previous stable features
  //--------------------------------------------
  std::vector<Feature3D> p_tpts;
  track_manager_.getStableFeatures(p_tpts);
  std:: cerr << "getstable: " << p_tpts.size() << std::endl;
  
  //--------------------------------------------
  // Create Mask 
  // If no previous pts, mask is just ones
  // Combined Depth/Cloud/Previous pts mask
  //--------------------------------------------
  // const cv::Mat1b& mask = cv::Mat(); // frame_.getImageMaskRef();

  //--------------------------------------------
  // 3. Detect features (at multiple scales) w/o mask
  // Determine correspondences 
  // a. Prop. corr from p_tpts to c_tpts
  //    c_tpts are appropriately ID'd
  //    Note: Here, the descriptors are extracted within
  // b. Mask out regions that are occupied by c_tpts
  // c. Detect features with above mask
  // d. Extract descriptors for new features
  // e. Add new features (id=-1) to c_tpts 
  //    track_manager takes care of IDing
  //--------------------------------------------
  std::vector<Feature3D> c_tpts;
  detectFeatures(c_tpts, mask);

  //--------------------------------------------
  // 4. Feature description, Depth/Surface normal pruning
  //--------------------------------------------
  describeFeatures(c_tpts);

  //--------------------------------------------
  // 5. Match Features
  // Find correspondences with expected locations mask
  // Appropriately ID c_tpts, if ID -1, then mark for adding
  //--------------------------------------------
  if (p_tpts.size() && c_tpts.size()) { 
    matchFeatures(p_tpts, c_tpts);
  } 

  //--------------------------------------------
  // 6. Add features
  //--------------------------------------------
  addFeatures(c_tpts);
  
  // Profiler
  profiler_.leave("detectAndAddFeatures()");
  profiler_.debug("detectAndAddFeatures() "+ss.str());

  // Copy frames 
  pframe_ = frame_;

  //--------------------------------------------
  // 7. Viz
  //--------------------------------------------
  visualize();
  
  // Profiler
  profiler_.leave("processFrame()");
# if VIZ
  // cv::waitKey(1);
#endif
}


static bool
kpts_sort(const cv::KeyPoint& lkp, const cv::KeyPoint& rkp) {
  return lkp.response > rkp.response;
}

void
GeneralPurposeFeatureTracker::detectFeatures (std::vector<fsvision::Feature3D>& tpts, 
                                              const cv::Mat& mask) { 

  profiler_.enter("detectFeatures(kpts,scale,mask)");
  std::stringstream ss;

  //--------------------------------------------
  // Gaussian / Box Blur 
  // TODO add mode for doing box blur
  //--------------------------------------------
  cv::Mat gray;
  const cv::Mat& _gray = frame_.getGrayRef();
  
  // #if GPU_ENABLED  
  // option 1: 
  // bilateralFilter(_gray, gray, 5, 10, 2.5, cv::BORDER_DEFAULT );

  // option 2:
  cv::GaussianBlur(_gray, gray, cv::Size(5,5), 0, 0, cv::BORDER_DEFAULT);

  // options 3: 
  // cv::blur(_gray, gray, cv::Size(11,11), cv::Point(-1,-1), cv::BORDER_DEFAULT);
  // #else
  // cv::GaussianBlur(_gray, gray, cv::Size(5,5), 0, 0, cv::BORDER_DEFAULT);
  // #endif

#if GPU_ENABLED
  gframe_.upload(gray);
#endif
  
  //--------------------------------------------
  // Set gray image (for later use)
  //--------------------------------------------
  frame_.setGray(gray); 
  
  //--------------------------------------------
  // Detect keypoints
  // TODO mask from previous tracked features
  //--------------------------------------------
  // profiler_.enter("KEYPOINT DETECTION");
  std::vector<cv::KeyPoint> kpts;
#if GPU_ENABLED

#if USE_GFTT
  cv::cuda::GpuMat gkpts;
  detector_->detect( gframe_, gkpts);

  std::vector<cv::Point2f> pts(gkpts.cols);
  cv::Mat pts_mat(1, gkpts.cols, CV_32FC2, (void*) &pts[0]);
  gkpts.download(pts_mat);

  cv::KeyPoint::convert(pts, kpts);

#else
  (*fast_detector_)(gframe_, cv::cuda::GpuMat(), kpts);
#endif
  
#else
  detector_->detect( gray, kpts, mask );
#endif
  // profiler_.leave("KEYPOINT DETECTION");
        
  //--------------------------------------------
  // Subpixel accuracy
  // TODO Subpixel window size
  //--------------------------------------------
  // profiler_.enter("SUBPIXEL-REFINEMENT");
  if (gpft_enable_subpixel_refinement_) {
    cv::TermCriteria SubPix_termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,10,0.05);
    cv::Size SubPix_winsize(5,5);
    std::vector<cv::Point2f> sp_pts;
    cv::KeyPoint::convert(kpts, sp_pts);
    cv::cornerSubPix( gray, sp_pts, SubPix_winsize, 
                      cv::Size(-1,-1), SubPix_termcrit);
    cv::KeyPoint::convert(sp_pts, kpts);
  }
  // profiler_.leave("SUBPIXEL-REFINEMENT");

# if 1 // VIZ
  {
    cv::Mat display;
    frame_.getRGBRef().copyTo(display, frame_.getImageMaskRef());

    std::vector<cv::Vec3b> colors(6);
    opencv_utils::fillColors( colors );

    for (int j=0; j<kpts.size(); j++) {
      cv::Scalar col = colors[kpts[j].octave];
      circle(display, kpts[j].pt, gpft_feat_block_size_/2, col, 1, CV_AA);
      // circle(display, kpts[j].pt, 1, cv::Scalar(0,240,0), 1, CV_AA);
    }
    cv::imshow("pts", display);
    cv::imshow("mask", mask);
  }
#endif

  // Propagate Feature pts
  int64_t now = frame_.getTimestamp();
  tpts = std::vector<fsvision::Feature3D>(kpts.size());
  for (int j=0; j<kpts.size(); j++) {
    tpts[j] = Feature3D(now, -1);
    tpts[j].set_keypoint(kpts[j]);
  }
  
  profiler_.leave("detectFeatures(kpts,scale,mask)");
  // profiler_.debug("detectFeatures(kpts,scale,mask) "+ss.str());
  return;
}

// void
// GeneralPurposeFeatureTracker::describeFeatures (const opencv_utils::Frame& frame,
//                                                 std::vector<cv::KeyPoint>& kpts,
//                                                 std::vector<cv::Mat>& desc) {
//   if (!kpts.size()) return;

// #define PATCH_SIZE 11
  
//   profiler_.enter("describeFeatures(kpts,desc)");
//   std::stringstream ss;
//   double tic = bot_timestamp_now();

//   desc = std::vector<cv::Mat>(kpts.size());
//   const cv::Mat& gray = frame_.getGrayRef();
//   for (int j=0; j<kpts.size(); j++) {
//     cv::Rect r(kpts[j].pt.x, kpts[j].pt.y, PATCH_SIZE, PATCH_SIZE);
//     r &= cv::Rect(0, 0, gray.cols, gray.rows);
//     desc[j] = cv::Mat(gray, r).clone();
//   }
  
//   ss << "FEATURE EXTRACTION: " << (bot_timestamp_now() - tic) * 1e-3 
//      << " ms" << std::endl; 
//   profiler_.leave("describeFeatures(kpts,desc)");
//   profiler_.debug("describeFeatures(kpts,desc) " + ss.str());
//   return;
// }


inline bool is_nan(const cv::Vec3f& v) {
  return (v[0] != v[0]);
}
inline bool is_nan(const cv::Point3f& v) {
  return (v.x != v.x);
}

// static inline
// std::pair<cv::Vec3f, cv::Vec3f>
// compute_surface_variation(const opencv_utils::Frame& frame,
//                           const cv::Point2f& p,
//                           int skip = 2) {
  
//   cv::Vec3f var = 0;
//   cv::Vec3f mu = 0;
//   int count = 0; 

//   // Compute mean
//   const cv::Mat3f& normals = frame.getNormalsRef();

//   // Patch
//   cv::Rect rect(p, cv::Size(5,5));
//   rect &= cv::Rect(0,0,normals.cols,normals.rows);
  
//   for (int y=rect.tl().y; y<=rect.br().y; y+=skip) {
//     for (int x=rect.tl().x; x<=rect.br().x; x+=skip) {
//       if (is_nan(normals(y,x))) continue;
//       mu += normals(y,x);
//       count++;
//     }
//   }

//   // If no depth registered, return high var
//   // or if very few depth registered
//   if (count == 0 || count < 0.5 * rect.width * rect.height)
//     return std::make_pair(cv::Vec3f(0.f,0.f,0.f), cv::Vec3f(1.f,1.f,1.f));
  
//   mu *= 1.f / count;

//   // COmpute var
//   for (int y=rect.tl().y; y<=rect.br().y; y+=skip) {
//     for (int x=rect.tl().x; x<=rect.br().x; x+=skip) {
//       if (is_nan(normals(y,x))) continue;
//       cv::Vec3f v = (normals(y,x) - mu);
//       var += cv::Vec3f(v[0]*v[0], v[1]*v[1], v[2]*v[2]);
//     }
//   }

//   var *= 1.f / count;
//   return std::make_pair(mu, var);
// }


void
GeneralPurposeFeatureTracker::setDepthFeatures (std::vector<Feature3D>& tpts) {

  // profiler_.enter("prune Depth Features()");
  std::stringstream ss;
  ss << "# PTS BEFORE: " << tpts.size() << std::endl;

  //--------------------------------------------
  // Patch normal variation
  //--------------------------------------------
  // const cv::Mat_<uint16_t>& depth = frame_.getDepthRef();
  const cv::Mat3f& cloud = frame_.getCloudRef();
  const cv::Mat3f& normals = frame_.getNormalsRef();

  // std::vector<cv::Point2f> pts(tpts.size());
  // for (int j=0; j<tpts.size(); j++) 
  //   pts[j] = tpts[j].get_point();
  // std::vector<cv::Point3f> pts3d = opencv_utils::get_xyz(depth, pts);
  
  for (int j=0; j<tpts.size(); j++) {
    // // Set Feature if not nan
    // if (is_nan(pts3d[j])) continue;
    // tpts[j].setFeature3D(pts3d[j]);
    const cv::Point2f& p = tpts[j].get_point();
    
    // Set normal if not nan
    if (!cloud.empty() && !is_nan(cloud(p))) { 
      if (!normals.empty() && !is_nan(normals(p))) {
        tpts[j].setFeature3D(cloud(p), normals(p)); // normal, tangent set to NaN
      } else {
        tpts[j].setFeature3D(cloud(p)); // normal, tangent set to NaN
      }
    }
  }
  ss << "# PTS AFTER: " << tpts.size() << std::endl;
  // profiler_.leave("prune Depth Features()");      
  // profiler_.debug("prune Depth Features()" + ss.str());      
}


void
GeneralPurposeFeatureTracker::describeFeatures (std::vector<Feature3D>& tpts) {
  if (!tpts.size()) return;
  
  profiler_.enter("describeFeatures(kpts,desc)");
  std::stringstream ss;

  //--------------------------------------------
  // Prune invalid/poor Depth/Normal Features
  //--------------------------------------------
  setDepthFeatures(tpts); 
        
  //--------------------------------------------
  // Convert tpts to kpts
  // Retain index of tpts, kpts[j].class_id  is
  // preserved
  //--------------------------------------------
  std::vector<cv::KeyPoint> kpts;
  Feature3D::convert(tpts, kpts); // index -> class_id
  ss << "Converting : " << kpts.size() << std::endl;

  //--------------------------------------------
  // Extract descriptors
  //--------------------------------------------
  cv::Mat desc;
  extractor_->compute(frame_.getGrayRef(), kpts, desc);
  // std::vector<cv::Mat> desc;
  // describeFeatures(frame_, kpts, desc);
  ss << "Keypoints: " << kpts.size() << " desc: " << desc.size() << std::endl;
  // assert(kpts.size() != 0);

  //--------------------------------------------
  // Save desc
  //--------------------------------------------
  // assert(tpts.size() == desc.rows);
  std::map<int64_t, cv::Mat> id_desc;
  for (int j=0; j<kpts.size(); j++ ) { 
    int idx = kpts[j].class_id;
    id_desc[idx] = desc.row(j);
  }

  // ss << "IDs descs: " << id_desc.size() << std::endl;
  // ss << "tpts size: " << tpts.size() << std::endl;

  //--------------------------------------------
  // Remove points if no descriptions
  //--------------------------------------------
  int idx = 0;
  for (std::vector<Feature3D>::iterator it = tpts.begin(); it != tpts.end(); idx++) { 
    if (id_desc.find(idx) == id_desc.end()) { 
      it = tpts.erase(it);
    } else { 
      it->set_desc(id_desc[idx]);
      it++;
    }            
  }

  // ss << "Describing : " << tpts.size() << std::endl;

  profiler_.leave("describeFeatures(kpts,desc)");
  // profiler_.debug("describeFeatures(kpts,desc) " + ss.str());
  return;
}

void
GeneralPurposeFeatureTracker::pruneFeatures (std::vector<Feature3D>& p_tpts, 
                                             std::vector<Feature3D>& c_tpts) { 
  profiler_.enter("pruneFeatures()");
  std::stringstream ss;
  ss << " COMPARING p_tpts: " << p_tpts.size() << " c_tpts: " << c_tpts.size()
     << std::endl;

  int nbef = c_tpts.size(); 

  // Build map
  std::map<int64_t, cv::Mat> p_map, c_map;
  for (int j=0; j<p_tpts.size(); j++)
    p_map[p_tpts[j].get_id()] = p_tpts[j].get_desc();
  for (int j=0; j<c_tpts.size(); j++)
    c_map[c_tpts[j].get_id()] = c_tpts[j].get_desc();

  for (std::vector<Feature3D>::iterator it = c_tpts.begin(); it != c_tpts.end(); ) { 
    int64_t id = it->get_id();
    assert(c_map.find(id) != c_map.end());
    if (p_map.find(id) == p_map.end()) { 
      it = c_tpts.erase(it);
      continue;
    }
    const cv::Mat& p_desc = p_map[id];
    const cv::Mat& c_desc = c_map[id];

    std::vector<cv::DMatch> matches;
    matcher_->match(c_desc, p_desc, matches);
    cv::DMatch& match = matches[0];
    if (match.distance >= 0.5) { 
      it = c_tpts.erase(it);
    } else { 
      ss <<  "match: " << match.distance << std::endl;
      it++;
    }
  }

  ss << " PRUNED: " << nbef - c_tpts.size() 
     << " REMAINING: " << c_tpts.size()
     << std::endl;

  profiler_.leave("pruneFeatures()");
  profiler_.debug("pruneFeatures() "+ss.str());
  return;
}


void
GeneralPurposeFeatureTracker::addFeatures (std::vector<Feature3D>& tpts) { 
  //--------------------------------------------
  // Profiler
  //--------------------------------------------
  // profiler_.enter("addFeatures(tpts)");

  //--------------------------------------------
  // Add Features and describe them
  //--------------------------------------------
  track_manager_.addFeatures(tpts);

  // profiler_.leave("addFeatures(tpts)");
}



void GeneralPurposeFeatureTracker::flowSparse (const cv::Mat& pgray, 
                                               const cv::Mat& gray, 
                                               std::vector<Feature3D>& tpts_in, 
                                               std::vector<Feature3D>& tpts_out, 
                                               std::vector<uchar>& status, 
                                               std::vector<float>& err) { 

  //--------------------------------------------
  // Profiler
  //--------------------------------------------
  profiler_.enter("flowSparse()");

  // Copy tpts_out
  tpts_out = tpts_in;

  // Set status
  status.resize(tpts_in.size(), 0);

  // Set error status
  err.resize(tpts_in.size(), 10000.0);

  // Convert
  const int width = gray.cols;
  const int height = gray.rows;

  std::vector<cv::Point2f> pts_in;
  Feature3D::convert(tpts_in, pts_in);

  // Sparse optical flow
  std::vector<cv::Point2f> pts_out;
  std::vector<uchar> pred_p_status;
  std::vector<float> pred_p_err;

  float deriv_lambda = 0.1; float min_eig = 1e-3; cv::Size LK_winsize(5,5);
  cv::TermCriteria LK_termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,10,0.1);
  cv::calcOpticalFlowPyrLK(
      pgray, 
      gray, 
      pts_in, 
      pts_out, 
      pred_p_status, 
      pred_p_err, 
      LK_winsize,3,LK_termcrit); // , 0, 0.001, deriv_lambda);

  // Predicted  points
  for (int j=0; j<pts_out.size(); j++) {
    if ( pred_p_status[j] && // check status
         pred_p_err[j] < gpft_min_add_radius_ && 
         pts_out[j].x >= 0 && pts_out[j].x < width && // check bounds
         pts_out[j].y >= 0 && pts_out[j].y < height ) { 
      status[j] = 1;
      tpts_out[j].set_point(pts_out[j]);
    } else {
      tpts_out[j].set_point(cv::Point2f(-1,-1));
    }
  }

#if VIZ
  // Viz before removing invalid tpts
  {
    cv::Mat display = frame_.getRGBRef().clone();
    assert(tpts_in.size() == tpts_out.size());
    for (int j=0; j<tpts_in.size(); j++) {
      cv::Scalar col = tpts_out[j].point_valid() ? cv::Scalar(0,255,255) : cv::Scalar(0,0,0);
      cv::line(display,
               tpts_in[j].get_point(),
               tpts_out[j].get_point(),
               col, 1, CV_AA);
      circle(display, tpts_in[j].get_point(), 1, col, 1, CV_AA);
      circle(display, tpts_out[j].get_point(), 1, col, 1, CV_AA);
    }
    cv::imshow("flow ", display);
  }
#endif
        
  // Remove invalid tpts
  for (std::vector<Feature3D>::iterator it = tpts_out.begin();
       it != tpts_out.end(); ) {
    if (!it->point_valid())
      it = tpts_out.erase(it);
    else
      it++;
  }

  //--------------------------------------------
  // Profiler
  //--------------------------------------------
  profiler_.leave("flowSparse()");


        
}


void GeneralPurposeFeatureTracker::flowSparse (const cv::Mat& pgray, 
                                               const cv::Mat& gray, 
                                               std::vector<cv::Point2f>& pts_in, 
                                               std::vector<cv::Point2f>& pts_out, 
                                               std::vector<uchar>& status, 
                                               std::vector<float>& err) { 

  //--------------------------------------------
  // Profiler
  //--------------------------------------------
  profiler_.enter("flowSparse()");

#if GPU_ENABLED
  gpframe_.upload(pgray);
  if (gframe_.empty() || gpframe_.empty()) return;
#endif
  
  // Set status
  status.resize(pts_in.size(), 0);

  // Set error status
  err.resize(pts_in.size(), 10000.0);

  // Convert
  const int width = gray.cols;
  const int height = gray.rows;

  // Sparse optical flow
  float deriv_lambda = 0.1; float min_eig = 1e-3; cv::Size LK_winsize(5,5);
  cv::TermCriteria LK_termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,10,0.1);
  cv::calcOpticalFlowPyrLK(
      pgray, 
      gray, 
      pts_in, 
      pts_out, 
      status, 
      err, 
      LK_winsize,3,LK_termcrit); // , 0, 0.001, deriv_lambda);

  // Predicted  points
  for (int j=0; j<pts_out.size(); j++) {
    if ( status[j] && // check status
         pts_out[j].x >= 0 && pts_out[j].x < width && // check bounds
         pts_out[j].y >= 0 && pts_out[j].y < height &&
         cv::norm(pts_out[j]-pts_in[j]) < std::sqrt(gpft_feature_distance_threshold_)) {
      status[j] = 1;
    } else {
      status[j] = 0;
      pts_out[j] = cv::Point2f(-1,-1);
    }
  }

#if VIZ
  // Viz before removing invalid tpts
  {
    cv::Mat display = frame_.getRGBRef().clone();
    assert(pts_in.size() == pts_out.size());
    for (int j=0; j<pts_in.size(); j++) {
      cv::Scalar col = status[j] ? cv::Scalar(0,255,255) : cv::Scalar(0,0,0);
      if (!status[j]) continue;
      cv::line(display,
               pts_in[j],
               pts_out[j],
               col, 1, CV_AA);
      circle(display, pts_in[j], 1, col, 1, CV_AA);
      circle(display, pts_out[j], 1, col, 1, CV_AA);
    }
    cv::imshow("flow ", display);
  }
#endif
        
  //--------------------------------------------
  // Profiler
  //--------------------------------------------
  profiler_.leave("flowSparse()");
        
}



// Ideally this does the main correspondence matching between 
// sets of points with different cardinality
// First step however is to propagate via sparse optical flow
// discarding the detected points
void 
GeneralPurposeFeatureTracker::corrFeaturesSparse(std::vector<Feature3D>& p_tpts, 
                                                 std::vector<Feature3D>& c_tpts,
                                                 const cv::Mat& c_mask) { 
  assert(frame_.ready() && pframe_.ready());

  // opencv_utils::imshow("frame", frame_.getGrayRef());
  // opencv_utils::imshow("pframe", pframe_.getGrayRef());
        
  //--------------------------------------------
  // Profiler
  //--------------------------------------------
  profiler_.enter("corrFeaturesSparse()");
  std::stringstream ss;

  //--------------------------------------------
  // Forward propagation (Sparse LK)
  //--------------------------------------------
  std::vector<Feature3D> pred_c_tpts;
  std::vector<uchar> pred_c_status;
  std::vector<float> pred_c_err;

  // Sparse median flow (forward)
  flowSparse ( 
      pframe_.getGrayRef(), 
      frame_.getGrayRef(), 
      p_tpts, 
      c_tpts, 
      pred_c_status, 
      pred_c_err);

  //--------------------------------------------
  // Persist utime to current tpts
  //--------------------------------------------
  // Remove tpts that don't fall within expected feature mask
  for (std::vector<Feature3D>::iterator it = c_tpts.begin(); it != c_tpts.end(); ) {
    assert(it->point_valid());
    cv::Point2f p = it->get_point();
    if (!c_mask.empty() && !c_mask.at<uchar>(p.y, p.x))
      it = c_tpts.erase(it);
    else {
      // Update timestamp
      it->set_utime(frame_.getTimestamp());
      it++;
    }
  }

  // for (auto& pt: p_tpts)
  //   std::cerr << "pt: " << pt.get_id() << " ";
  // std::cerr << std::endl;
  
  // //--------------------------------------------
  // // Subpixel refinement to predicted points
  // //--------------------------------------------
  // profiler_.enter("SUB PIXEL REFINEMENT"); 
  // std::vector<cv::Point2f> c_pts;
  // Feature3D::convert(c_tpts, c_pts);

  // cv::TermCriteria SubPix_termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,10,0.05);
  // cv::Size SubPix_winsize(3,3);
  // cv::cornerSubPix( frame_.getGrayRef(), c_pts, SubPix_winsize, 
  //                   cv::Size(-1,-1), SubPix_termcrit);
  // profiler_.leave("SUB PIXEL REFINEMENT"); 

  // // Copy back (IDs preserved)
  // Feature3D::convert(c_pts, c_tpts);

  // --------------------------------------------
  // Match and prune features
  // that don't match prev. description
  // --------------------------------------------
  // assert(c_tpts.size() == p_tpts.size());
  pruneFeatures(p_tpts, c_tpts);

  //--------------------------------------------
  // Profiler
  //--------------------------------------------
  profiler_.leave("corrFeaturesSparse()");
        
}

void 
GeneralPurposeFeatureTracker::corrFeatures (std::vector<Feature3D>& p_tpts, 
                                            std::vector<Feature3D>& c_tpts) { 
  //--------------------------------------------
  // Profiler
  //--------------------------------------------
  profiler_.enter("corrFeatures()");
  std::stringstream ss;

  //--------------------------------------------
  // Forward propagation (Dense Farneback)
  //--------------------------------------------
  std::vector<uchar> pred_c_status;
  std::vector<Feature3D> pred_c_tpts;

  // Dense median flow (forward)
  medianFlow(pframe_.getGrayRef(), 
             frame_.getGrayRef(), 
             p_tpts, 
             pred_c_tpts, 
             pred_c_status);

  //--------------------------------------------
  // Copy predicted to current tpts
  //--------------------------------------------
  for (int j=0; j<pred_c_tpts.size(); j++) {
    Feature3D f = pred_c_tpts[j];

    // Update timestamp
    f.set_utime(frame_.getTimestamp());
            
    // Only add if status is good
    if (pred_c_status[j])
      c_tpts.push_back(f);
  }


  //--------------------------------------------
  // Subpixel refinement to predicted points
  //--------------------------------------------
  // profiler_.enter("SUB PIXEL REFINEMENT"); 
  std::vector<cv::Point2f> c_pts;
  Feature3D::convert(c_tpts, c_pts);

  cv::TermCriteria SubPix_termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,10,0.05);
  cv::Size SubPix_winsize(3,3);
  cv::cornerSubPix( frame_.getGrayRef(), c_pts, SubPix_winsize, 
                    cv::Size(-1,-1), SubPix_termcrit);
  // profiler_.leave("SUB PIXEL REFINEMENT"); 

  // Copy back (IDs preserved)
  Feature3D::convert(c_pts, c_tpts);

  // // --------------------------------------------
  // // Feature description
  // // --------------------------------------------
  // // assert(c_tpts.size() == p_tpts.size());
  // describeFeatures(c_tpts);

  // --------------------------------------------
  // Match and prune features
  // that don't match prev. description
  // --------------------------------------------
  // assert(c_tpts.size() == p_tpts.size());
  pruneFeatures(p_tpts, c_tpts);

  //--------------------------------------------
  // Profiler
  //--------------------------------------------
  profiler_.leave("corrFeatures()");

}

static inline bool match_sort(const std::pair<int, cv::DMatch>& lhs,
                              const std::pair<int, cv::DMatch>& rhs) {
  return (lhs.second.distance < rhs.second.distance);
}

// static inline bool match_sort(const std::pair<int, float>& lhs,
//                               const std::pair<int, float>& rhs) {
//   return (lhs.second > rhs.second);
// }

static bool
tpts_sort(const Feature3D* lkp, const Feature3D* rkp) {
  return lkp->point_.response > rkp->point_.response;
}


// Ideally this does the main correspondence matching between 
// sets of points with different cardinality
// First step however is to propagate via sparse optical flow
// discarding the detected points
void 
GeneralPurposeFeatureTracker::matchFeatures(std::vector<Feature3D>& p_tpts, 
                                            std::vector<Feature3D>& c_tpts) {
  //--------------------------------------------
  // Checks
  //--------------------------------------------
  assert(frame_.ready() && pframe_.ready());
  if (!p_tpts.size() || !c_tpts.size()) return;

  //--------------------------------------------
  // Profiler
  //--------------------------------------------
  profiler_.enter("matchFeatures()");
  std::stringstream ss;

  //--------------------------------------------
  // Build spatial kd-tree for each octave
  //--------------------------------------------
  // pts at each octave
  std::map<int, std::vector<Feature3D*> > p_tpts_map;
  for (int j=0; j<p_tpts.size(); j++) {
    const int octave = p_tpts[j].get_keypoint().octave;
    p_tpts_map[octave].push_back(&(p_tpts[j]));
  }

  // accumulate all the c_tpts at octave
  std::map<int, std::vector<Feature3D*> > c_tpts_map;
  for (int j=0; j<c_tpts.size(); j++) {
    const int octave = c_tpts[j].get_keypoint().octave;
    c_tpts_map[octave].push_back(&(c_tpts[j]));
  }
  
  const int KNN = 2;

  cv::Mat display;
  frame_.getRGBRef().copyTo(display, frame_.getImageMaskRef());

  std::vector<cv::Vec3b> colors(6);
  opencv_utils::fillColors( colors );

  // At each octave, perform spatial matching
  for (std::map<int, std::vector<Feature3D*> >::iterator it = p_tpts_map.begin();
       it != p_tpts_map.end(); it++) {

    // Aggregate features from each octave
    const int& octave = it->first;
    std::vector<Feature3D*>& p_tptsj = p_tpts_map[it->first];
    if (!p_tptsj.size()) continue;
    // std::sort(p_tptsj.begin(), p_tptsj.end(), tpts_sort);
    std::vector<cv::Point2f> p_pts(p_tptsj.size());
    for (int j=0; j<p_tptsj.size(); j++) p_pts[j] = p_tptsj[j]->get_point();

    std::vector<Feature3D*>& c_tptsj = c_tpts_map[it->first];
    if (!c_tptsj.size()) continue;
    // std::sort(c_tptsj.begin(), c_tptsj.end(), tpts_sort);
    std::vector<cv::Point2f> c_pts(c_tptsj.size());
    for (int j=0; j<c_tptsj.size(); j++) c_pts[j] = c_tptsj[j]->get_point();

    profiler_.enter("flowSparse");
    // Propagate features
    std::vector<uchar> pred_status;
    std::vector<float> pred_err;
    std::vector<cv::Point2f> pred_pts; 
    flowSparse(pframe_.getGrayRef(), frame_.getGrayRef(),
               p_pts, pred_pts, pred_status, pred_err);
    profiler_.leave("flowSparse");

    profiler_.enter("k-d Tree build/search");
    // Build curr. data for matching spatially, build Index, and query prev. data
    // Query, and data is 3xN; dists are squared
    cv::Mat cdata = cv::Mat(c_pts).reshape(1);
    cv::Mat pdata = cv::Mat(pred_pts).reshape(1);
    cv::flann::Index index(cdata, cv::flann::KDTreeIndexParams(4));
    cv::Mat indices; // (c_pts.size(), KNN, CV_32SC1, -1); 
    cv::Mat dists; // (c_pts.size(), KNN, CV_32FC1);
    index.knnSearch(pdata, indices, dists, KNN, cv::flann::SearchParams(64));
    profiler_.leave("k-d Tree build/search");
    
    // Color pts based on octave
    cv::Scalar col = colors[octave];

    // Match Conflict confidences
    std::map<int, float> match_confidence;

    profiler_.enter("match");
    // For each p_pt
    for (int j=0; j<indices.rows; j++) {

      // If BAD prediction
      if (!pred_status[j]) continue;
      
      circle(display, p_pts[j], gpft_feat_block_size_/2, col, 1, CV_AA);
      // circle(display, pred_pts[j], FEAT_BLOCK_SIZE/2, cv::Scalar(0,200,0), 1, CV_AA);

      // Find valid neighbors
      std::vector<std::pair<int, cv::DMatch> > matches;

      for (int k=0; k<indices.cols; k++) {
        int nidx = indices.at<int>(j,k);
        if (dists.at<float>(j,k) >= gpft_feature_distance_thresholdsq_) continue;
        if (nidx < 0 || nidx >= c_pts.size()) continue;

        std::vector<cv::DMatch> match;
        assert(j >= 0 && j <= p_tptsj.size());
        assert(nidx >= 0 && nidx <= c_tptsj.size());

        // cv::Mat res; 
        // cv::matchTemplate(p_tptsj[j]->get_desc(), c_tptsj[nidx]->get_desc(),
        //                   res, cv::TM_CCORR_NORMED);
        
        matcher_->match(p_tptsj[j]->get_desc().clone(),
                        c_tptsj[nidx]->get_desc().clone(), match);
        assert(match.size() == 1);
        matches.push_back(std::make_pair(nidx,match[0]));
      }

      // Continue if no matches
      if (!matches.size()) continue;      

      // Sort by matches (threshold match)
      std::sort(matches.begin(), matches.end(), match_sort);

      int cidx = matches[0].first; float cdist = matches[0].second.distance;
      std::cerr << "Match: " << cdist << std::endl;
      circle(display, c_pts[cidx], gpft_feat_block_size_/2-1, col * 0.5, 1, CV_AA);
      if (cdist >= gpft_feature_match_threshold_) continue;
      
      assert(c_tptsj[cidx]->get_keypoint().octave == p_tptsj[j]->get_keypoint().octave);
      circle(display, c_pts[cidx], gpft_feat_block_size_/2+1, col * 0.5, 1, CV_AA);
      
      // Propagate ID
      if (c_tptsj[cidx]->get_id() == -1) { 
        match_confidence[cidx] = cdist;
        c_tptsj[cidx]->set_id(p_tptsj[j]->get_id());
        c_tptsj[cidx]->set_status(Feature::MATCHED);
      } else {
        // Conflicting matches (p1 -> c, p2 -> c): pick best match
        assert(match_confidence.find(cidx) != match_confidence.end());
        if (match_confidence[p_tptsj[j]->get_id()] < cdist) {
          match_confidence[cidx] = cdist;
          c_tptsj[cidx]->set_id(p_tptsj[j]->get_id());
          c_tptsj[cidx]->set_status(Feature::MATCHED);
        }        
      }

      // cv::Scalar col = colors[p_tptsj[j]->get_keypoint().octave];
      cv::line(display, p_pts[j], c_pts[cidx], col, 1, CV_AA);
    }

    if (gpft_allowed_predictions_) { 
      // For all the failed matches, or failed detections that leads to failed matches
      // Propagate via optical flow instead and keep track of number of predictions
      // Too many predictions lead to drift, so avoid it if too many
      int64_t now = frame_.getTimestamp();
      
      // Propagate Feature pts
      std::vector<fsvision::Feature3D> pred_tptsj;

      // Find matched IDs
      std::set<int64_t> matched_ids; 
      for (auto& ctptj : c_tptsj) matched_ids.insert(ctptj->get_id());

      // Add predicted tpts
      for (int k=0; k<p_tptsj.size(); k++) {
        if (matched_ids.find(p_tptsj[k]->get_id()) != matched_ids.end()) continue;
        if (cv::norm(pred_pts[k] - p_tptsj[k]->get_point()) >= gpft_feature_distance_threshold_)
          continue;
        Feature3D fpt = *p_tptsj[k];
        fpt.set_utime(now);
        fpt.set_point(pred_pts[k]);
        fpt.set_status(Feature::PREDICTED); // increment prediction
        pred_tptsj.push_back(fpt);
      }
      c_tpts.insert(c_tpts.end(), pred_tptsj.begin(), pred_tptsj.end());
    }
    
  }
  
  profiler_.leave("match");
  
  // Remove excess features if track_manager not ready
  // if (!track_manager_.ready(gpft_num_feats_)) { 
    for (std::vector<Feature3D>::iterator it = c_tpts.begin(); it != c_tpts.end(); ) {
      if (it->get_id() == -1) 
        it = c_tpts.erase(it);
      else 
        it++;
    }
    // }
  
  cv::imshow("match", display);

  
  //--------------------------------------------
  // Profiler
  //--------------------------------------------
  profiler_.leave("matchFeatures()");



  
}

void GeneralPurposeFeatureTracker::visualize() { 
  const int min_track_size = 1;
  std::vector<fsvision::Feature3D> fpts = getAllFeatures(min_track_size);

  //----------------------------------
  // KeyPoint viz
  //----------------------------------
  cv::Mat3b out; 
  frame_.getRGBRef().copyTo(out); 

  // Colors
  std::vector<cv::Vec3b> colors(30);
  opencv_utils::fillColors( colors );
    
  fsvision::Feature3D pfpt;
  for (auto& fpt: fpts) {
    const cv::KeyPoint& kp = fpt.get_keypoint();
    cv::Point2f tvec = cv::Point2f(kp.size, kp.response) * 5;

    // cv::line(out, kp.pt + tvec, kp.pt, cv::Scalar(0,0,255), 1, CV_AA);
    cv::Scalar col = colors[fpt.get_id()%colors.size()];
    // cv::Scalar col = colors[fpt.get_id()%colors.size()];
    // cv::circle(out, kp.pt, 5, cv::Scalar(0,128,0), 1, CV_AA );

    // Draw correspondences between frames
    if (pfpt.get_id() == fpt.get_id()) {
      cv::line(out, kp.pt, pfpt.get_point(), col, 1, CV_AA);
    } else {
      cv::circle(out, kp.pt, 2, col, 1, CV_AA );
    }
    // copy 
    pfpt = fpt;

    // cv::addText(out, cv::format("%3.2f",gpft.track_manager_.track_add_rate_), 
    //         cv::Point(20,20), cv::fontQt("Times")); 
    // cv::addText(out, cv::format("%i",gpft.track_manager_.tracklet_map_.size()),
    //         cv::Point(20,40), cv::fontQt("Times")); 
      
  }
  cv::imshow("GPFT", out);
}




void GeneralPurposeFeatureTracker::medianFlowSparse (const cv::Mat& pgray, 
                                                     const cv::Mat& gray, 
                                                     std::vector<Feature3D>& tpts_in, 
                                                     std::vector<Feature3D>& tpts_out, 
                                                     std::vector<uchar>& status, 
                                                     std::vector<float>& err) { 

  //--------------------------------------------
  // Profiler
  //--------------------------------------------
  profiler_.enter("medianFlowSparse()");
  std::stringstream ss; 

  // Copy tpts_out
  tpts_out = tpts_in;
        
  // Set status
  status.resize(tpts_in.size(), 0);

  // Set error status
  err.resize(tpts_in.size(), 10000.0);

  // Compute median (sample from neighbors)
  const int width = gray.cols;
  const int height = gray.rows;
  const int bsize = 9; // only odd
  const int bskip = 2; 
  const int bhalf = bsize/2;
  const int samples = pow(bsize - (bskip-1)*2, 2);

  // Sampled points
  std::vector<cv::Point2f> pts_in_sampled(tpts_in.size() * samples);
  for (int j=0; j<tpts_in.size(); j++) { 
    int idx = j*samples;
    cv::Point2f pt = tpts_in[j].get_point();
    for(int y = std::max<int>(0, pt.y-bhalf), k=0; y <= std::min<int>(height-1, pt.y+bhalf); y+=bskip) {
      for(int x = std::max<int>(0,pt.x-bhalf); x <= std::min<int>(width-1, pt.x+bhalf); x+=bskip, k++) { 
        pts_in_sampled[idx+k] = cv::Point2f(x,y);
      }
    }
  }

  //--------------------------------------------
  // Median sparse optical flow
  //--------------------------------------------
  std::vector<cv::Point2f> pts_out_sampled;
  std::vector<uchar> pred_p_status;
  std::vector<float> pred_p_err;
        
  float deriv_lambda = 0.1; float min_eig = 1e-3; cv::Size LK_winsize(5,5);
  cv::TermCriteria LK_termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,10,0.1);
  cv::calcOpticalFlowPyrLK(
      pgray, 
      gray, 
      pts_in_sampled, 
      pts_out_sampled, 
      pred_p_status, 
      pred_p_err, 
      LK_winsize,3,LK_termcrit); // , 0, 0.001, deriv_lambda);


  // Predicted Sampled points
  for (int j=0; j<tpts_in.size(); j++) { 
    std::vector<float> xs(samples), ys(samples);
    int idx = j*samples;

    float conf = 0; 
    for (int k=0; k<samples; k++) { 
      xs[k] = pts_out_sampled[idx+k].x - pts_in_sampled[idx+k].x;
      ys[k] = pts_out_sampled[idx+k].y - pts_in_sampled[idx+k].y;
      conf += pred_p_status[j];
    }
    conf *= 1.f / samples;

    // Median computation
    std::nth_element(xs.begin(), xs.begin() + xs.size()/2, xs.end()); 
    std::nth_element(ys.begin(), ys.begin() + ys.size()/2, ys.end()); 
            
    // Median err?
    std::nth_element(pred_p_err.begin(), 
                     pred_p_err.begin() + pred_p_err.size()/2, 
                     pred_p_err.end());
    err[j] = pred_p_err[pred_p_err.size()/2];

    // Apply offset, and propagate ID
    cv::Point2f offset(xs[xs.size()/2], ys[ys.size()/2]);
    if( conf > 0.5 && // check status confidences
        tpts_out[j].get_point().x >= 0 && tpts_out[j].get_point().x < width && 
        tpts_out[j].get_point().y >= 0 && tpts_out[j].get_point().y < height ) {
      tpts_out[j].set_point(tpts_in[j].get_point() + offset);              
    } else {
      tpts_out[j].set_point(cv::Point2f(-1,-1));
    }
  }

#if VIZ
  // Viz before removing invalid tpts
  {
    cv::Mat display = frame_.getRGBRef().clone();
    assert(tpts_in.size() == tpts_out.size());
    for (int j=0; j<tpts_in.size(); j++) {
      cv::Scalar col = tpts_out[j].point_valid() ? cv::Scalar(0,255,255) : cv::Scalar(0,0,0);
      cv::line(display,
               tpts_in[j].get_point(),
               tpts_out[j].get_point(),
               col, 1, CV_AA);
      circle(display, tpts_in[j].get_point(), 1, cv::Scalar(0,0,240), 1, CV_AA);
      circle(display, tpts_out[j].get_point(), 1, cv::Scalar(0,240,0), 1, CV_AA);
    }
    cv::imshow("flow ", display);
  }


  // Viz before removing invalid tpts
  {
    cv::Mat display = frame_.getRGBRef().clone();
    for (int j=0; j<pts_in_sampled.size(); j++) {
      cv::Scalar col = pred_p_status[j] ? cv::Scalar(0,255,255) : cv::Scalar(0,0,0);
      cv::line(display, pts_in_sampled[j], pts_out_sampled[j],
               col, 1, CV_AA);
      // circle(display, tpts_in[j].get_point(), 1, cv::Scalar(0,0,240), 1, CV_AA);
      // circle(display, tpts_out[j].get_point(), 1, cv::Scalar(0,240,0), 1, CV_AA);
    }
    cv::imshow("flow-all ", display);
  }
#endif
        
  // Remove invalid tpts
  for (std::vector<Feature3D>::iterator it = tpts_out.begin();
       it != tpts_out.end(); ) {
    if (!it->point_valid())
      it = tpts_out.erase(it);
    else
      it++;
  }

        
  //--------------------------------------------
  // Profiler
  //--------------------------------------------
  profiler_.leave("medianFlowSparse()");
  profiler_.debug("medianFlowSparse()" + ss.str());
       
}

void GeneralPurposeFeatureTracker::medianFlow(const cv::Mat& pgray, 
                                              const cv::Mat& gray, 
                                              std::vector<Feature3D>& tpts_in, 
                                              std::vector<Feature3D>& tpts_out, 
                                              std::vector<uchar>& status) { 

  //--------------------------------------------
  // Profiler
  //--------------------------------------------
  profiler_.enter("medianFlow()");
  std::stringstream ss; 

  // Copy pts_out
  tpts_out = tpts_in;

  // Set status
  status.resize(tpts_in.size(), 0);

  // Compute median (sample from neighbors)
  const int width = gray.cols;
  const int height = gray.rows;
  const int bsize = 3; // only odd
  const int bskip = 1; 
  const int bhalf = bsize/2;
  const int samples = pow(bsize - (bskip-1)*2, 2);

  //--------------------------------------------
  // Main dense OF (farneback)
  //--------------------------------------------
  cv::Mat_<cv::Point2f> flow;

  float pyrScale = sqrt(2)/2.0; int numLevels = 3; int winSize = 5;
  int numIters = 2; int polyN = 7; float polySigma = 1.5; 
  int flags = cv::OPTFLOW_FARNEBACK_GAUSSIAN;

  profiler_.enter("OF FARNEBACK:");
  cv::calcOpticalFlowFarneback( 
      pgray,
      gray, 
      flow, 
      pyrScale, numLevels, winSize,
      numIters, polyN, polySigma, flags);
  profiler_.leave("OF FARNEBACK:");

#if VIZ
  cv::Mat out;
  opencv_utils::drawOpticalFlow(flow, out);
  cv::imshow("flow", out);
#endif
  
  // Predicted samples
  for (int j=0; j<tpts_in.size(); j++) { 
    std::vector<double> xs, ys;
    int x = cvFloor(tpts_in[j].get_point().x);
    int y = cvFloor(tpts_in[j].get_point().y);
    for(int m = x-bhalf; m <= x+bhalf; m+=bskip)
      for(int n = y-bhalf; n <= y+bhalf; n+=bskip) {
        int p = std::min<int>(std::max<int>(m, 0), width-1);
        int q = std::min<int>(std::max<int>(n, 0), height-1);
        cv::Point2f f = flow(q,p);
        xs.push_back(f.x);
        ys.push_back(f.y);
      }

    // Median computation
    std::nth_element(xs.begin(), xs.begin() + xs.size()/2, xs.end()); 
    std::nth_element(ys.begin(), ys.begin() + ys.size()/2, ys.end()); 

    // Apply offset, and propagate ID
    cv::Point2f offset(xs[xs.size()/2], ys[ys.size()/2]);
    tpts_out[j].set_point(tpts_in[j].get_point() + flow(tpts_in[j].get_point())); // offset;

    if( tpts_out[j].get_point().x >= 0 && 
        tpts_out[j].get_point().x < width && 
        tpts_out[j].get_point().y >= 0 && 
        tpts_out[j].get_point().y < height )
      status[j] = 1;
  }

  //--------------------------------------------
  // Profiler
  //--------------------------------------------
  profiler_.leave("medianFlow()");

  return;
}




}


  // //--------------------------------------------
  // // Add Features if previous frame didn't exist
  // // 1. Detect features with ones mask
  // // 2. Extract descriptors for the features
  // // 3. Add new features (id=-1) to c_tpts
  // //    track_manager takes care of IDing
  // //--------------------------------------------
  // else if (!pframe_.ready()) { 

  //   // cv::Mat img = frame_.getRGBRef().clone();
  //   // for (int j=0; j<c_tpts.size(); j++) {
  //   //   if (c_tpts[j].xyz_valid() && c_tpts[j].normal_valid())
  //   //     circle(img, c_tpts[j].get_point(), MIN_ADD_RADIUS, 
  //   //            cv::Scalar(0,200,0), 1, CV_AA);
  //   //   else
  //   //     circle(img, c_tpts[j].get_point(), MIN_ADD_RADIUS, 
  //   //            cv::Scalar(200,0,0), 1, CV_AA);
              
  //   // }
  //   // cv::imshow("first_new_pts", img);
  //   // cv::imshow("first_mask", mask);
            
  // } 
  // else {                        
  //   // Shouldn't happen
  //   assert(0); 
  // }


    
    // // Update mask with possible feature locations
    // cv::Mat c_mask = cv::Mat(mask.size(), CV_8UC1); c_mask = 0; 
    // for (int j=0; j<c_kpts.size(); j++)
    //   circle(c_mask, c_kpts[j].pt, FEAT_BLOCK_SIZE/2, 
    //          cv::Scalar(255), CV_FILLED, CV_AA);

//     // Ask track manager if we need more tracks
//     // Otherwise, skip
//     if (track_manager_.ready()) { 
//       // Mask out regions that are certainly occuppied
//       // TODO constant (MIN_ADD_RADIUS)
//       for (int j=0; j<c_tpts.size(); j++)
//         circle(mask, c_tpts[j].get_point(), MIN_ADD_RADIUS, 
//                cv::Scalar(0), CV_FILLED, CV_AA);

//       // Detect with feature occupancy mask
//       c_kpts.clear();
//       detectFeatures(c_kpts, mask); 
//       ss << "NEWLY ADDED POINTS: " << c_kpts.size() << std::endl;
//       // assert(c_kpts.size());
            
//       // Assemble new_c_tpts
//       std::vector<Feature3D> new_c_tpts(c_kpts.size());
//       for (int j=0; j<c_kpts.size(); j++) { 
//         const int32_t id = -1;
//         Feature3D f3d(now, id); f3d.set_keypoint(c_kpts[j]);
//         new_c_tpts[j] = f3d;
//       }

//       // Add to c_tpts
//       ss << "ADD NEW FEATS BEF: " << c_tpts.size() << std::endl;
//       c_tpts.insert(c_tpts.end(), new_c_tpts.begin(), new_c_tpts.end());
//       ss << "ADD NEW FEATS AFT: " << c_tpts.size() << std::endl;

// #if VIZ
//       cv::Mat img = frame_.getRGBRef().clone();
//       for (int j=0; j<new_c_tpts.size(); j++)
//         circle(img, new_c_tpts[j].get_point(), MIN_ADD_RADIUS, 
//                cv::Scalar(0,200,0), CV_FILLED, CV_AA);
//       cv::imshow("add_pts", img);
//       cv::imshow("mask", mask);
// #endif
                
//     }

  // //--------------------------------------------
  // // Find sobel on each x, y
  // //--------------------------------------------
  // cv::Mat1b dx, dy, zero;
  // cv::Sobel(gray, dx, gray.depth(), 1, 0, 3);
  // cv::Sobel(gray, dy, gray.depth(), 0, 1, 3);
  // cv::Mat1f dxf, dyf;
  // dx.convertTo(dxf, CV_32F);   dy.convertTo(dyf, CV_32F);

  // cv::Mat1f zerof = cv::Mat1f::zeros(dx.size());
  // cv::magnitude(dxf, dyf, zerof);
  // zerof.convertTo(zero, CV_8U);
  
  // cv::Mat3b grad;
  // cv::Mat grad_[] = {dx, dy, zero};
  // cv::merge(grad_, 3, grad);
  // cv::imshow("gradxy", grad);
