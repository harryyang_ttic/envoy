// 
// Author: Sudeep Pillai (spillai@csail.mit.edu) 
// Updates: Aug 07, 2013
// 

#ifndef DEPTH_FEATURES_HPP_
#define DEPTH_FEATURES_HPP_

// Standard includes
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <deque>
#include <map>
#include <set> 

// opencv includes
#include <opencv2/opencv.hpp>

// non-free opencv includes
#include <opencv2/nonfree/nonfree.hpp>

// opencv-utils includes
#include <perception_opencv_utils/opencv_utils.hpp>

// GPFT includes
#include <fs-utils/profiler.hpp>

namespace fsvision { 

    class DepthFeatures { 
    public:

        //--------------------------------------------
        // Constants/Variables
        //--------------------------------------------
        bool enable_subpixel_refinement_;

        //--------------------------------------------
        // Feature Extractor, Descriptor, and Matcher
        //--------------------------------------------
        // cv::Ptr<cv::FeatureDetector> detector;
        cv::Ptr<cv::PyramidAdaptedFeatureDetector> detector_;
        cv::Ptr<cv::DescriptorExtractor> extractor_; 
        cv::Ptr<cv::DescriptorMatcher> matcher_; 
        cv::Ptr<cv::BOWTrainer> vocab_;

        //--------------------------------------------
        // Profiler
        //--------------------------------------------
        Profiler profiler_;

        //--------------------------------------------
        // Image history for optical flow
        //--------------------------------------------
        float scale_factor_;
        // Frame frame_, pframe_;

    public:
        DepthFeatures ();
        ~DepthFeatures ();

        // // void setTrackingMode (const TrackingMode& mode);
        void setScale(const float scale_factor);
        // void setTimestamp(const int64_t utime);
        // void setImage(const cv::Mat& img); 
        // void setDepthImage(const cv::Mat& depth);
        // void setCloud(const cv::Mat& cloud);
        // void update();
        void update (const cv::Mat& img, const cv::Mat& depth);
        // void updateInit();

        // void plot();

        // void medianFlow (const cv::Mat& pgray, 
        //                  const cv::Mat& gray, 
        //                  std::vector<Feature2D>& pts_in, 
        //                  std::vector<Feature2D>& pts_out, 
        //                  std::vector<uchar>& status);
        // void medianFlowSparse (const cv::Mat& pgray, 
        //                        const cv::Mat& gray, 
        //                        std::vector<Feature2D>& pts_in, 
        //                        std::vector<Feature2D>& pts_out, 
        //                        std::vector<uchar>& status, 
        //                        std::vector<float>& err);
        // void flowSparse (const cv::Mat& pgray, 
        //                  const cv::Mat& gray, 
        //                  std::vector<Feature2D>& pts_in, 
        //                  std::vector<Feature2D>& pts_out, 
        //                  std::vector<uchar>& status, 
        //                  std::vector<float>& err);
        
        // void addFeatures (std::vector<Feature2D>& tpts);

        // void corrFeaturesSparse(std::vector<Feature2D>& p_tpts, 
        //                         std::vector<Feature2D>& c_tpts);
        // void corrFeatures (std::vector<Feature2D>& p_kpts, 
        //                    std::vector<Feature2D>& c_tpts);

        // void detectAndAddFeatures();
        // void detectFeatures(std::vector<cv::KeyPoint>& kpts, 
        //                     const float scale_factor = 1.f, 
        //                     const cv::Mat& mask = cv::Mat());
        // void describeFeatures (std::vector<cv::KeyPoint>& kpts, 
        //                        cv::Mat& desc);
        // void describeFeatures (std::vector<Feature2D>& tpts);

        // void pruneFeatures(std::vector<Feature2D>& p_tpts, 
        //                    std::vector<Feature2D>& c_tpts);
            
    };


}

#endif // GENERAL_PURPOSE_FEATURE_TRACKER_HPP_
