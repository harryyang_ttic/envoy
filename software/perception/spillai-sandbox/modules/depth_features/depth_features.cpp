// 
// Author: Sudeep Pillai (spillai@csail.mit.edu) 
// Updates: Aug 07, 2013
// 

#include "depth_features.hpp"

namespace fsvision { 
    //============================================
    // DepthFeatures class
    //============================================
    DepthFeatures::DepthFeatures () { 
        std::cerr << "DepthFeatures ctor: " << std::endl;

        //--------------------------------------------
        // Profiler naming
        //--------------------------------------------
        profiler_.setName("DepthFeatures");

        //--------------------------------------------
        // FAST / GFTT Feature Detector
        //--------------------------------------------
        // // FAST
        // int fast_threshold = 50; bool nonmax = true; 
        // cv::FastFeatureDetector* detector = new cv::FastFeatureDetector( fast_threshold, nonmax );

        // GFTT
        int gftt_maxCorners=400; double gftt_qualityLevel=0.04; double gftt_minDistance=5;
        int gftt_blockSize=5; bool gftt_useHarrisDetector=false; double gftt_k=0.04 ;
        cv::GFTTDetector* detector = new cv::GFTTDetector( gftt_maxCorners, gftt_qualityLevel, gftt_minDistance,
                                                            gftt_blockSize, gftt_useHarrisDetector, gftt_k);


        // // MSER
        // int mser_delta=5; int mser_min_area=60; int mser_max_area=14400;
        // double mser_max_variation=0.25; double mser_min_diversity=.2;
        // int mser_max_evolution=200; double mser_area_threshold=1.01;
        // double mser_min_margin=0.003; int mser_edge_blur_size=5;
        // cv::MSER* _detector = new cv::MSER( mser_delta, mser_min_area, mser_max_area,
        //                                     mser_max_variation, mser_min_diversity,
        //                                     mser_max_evolution, mser_area_threshold,
        //                                     mser_min_margin, mser_edge_blur_size );

        // cv::SURF* _detector = new cv::SURF(5e-3);

        // Pyramid Detector
        // TODO: Pyramid scales might fail when doing subpixel
        // - Take care of pyramid scales internally

        detector_ = new cv::PyramidAdaptedFeatureDetector(detector, 1 );
       
        //--------------------------------------------
        // Feature Extractor
        //--------------------------------------------
        extractor_ = cv::DescriptorExtractor::create("SURF");
        cv::initModule_nonfree();


        //--------------------------------------------
        // Descriptor Matcher
        // // Find one best match for each query descriptor (if mask is empty).
        // CV_WRAP void match( const Mat& queryDescriptors, const Mat& trainDescriptors,
        //             CV_OUT vector<DMatch>& matches, const Mat& mask=Mat() ) const;
        // // Find k best matches for each query descriptor (in increasing order of distances).
        // // compactResult is used when mask is not empty. If compactResult is false matches
        // // vector will have the same size as queryDescriptors rows. If compactResult is true
        // // matches vector will not contain matches for fully masked out query descriptors.
       // CV_WRAP void knnMatch( const Mat& queryDescriptors, const Mat& trainDescriptors,
        //                CV_OUT vector<vector<DMatch> >& matches, int k,
        //                const Mat& mask=Mat(), bool compactResult=false ) const;
        // // Find best matches for each query descriptor which have distance less than
        // // maxDistance (in increasing order of distances).
        // void radiusMatch( const Mat& queryDescriptors, const Mat& trainDescriptors,
        //                   vector<vector<DMatch> >& matches, float maxDistance,
        //                   const Mat& mask=Mat(), bool compactResult=false ) const;
        //--------------------------------------------
        // BruteForce-Hamming, BruteForce-HammingLUT, BruteForce, 
        // FlannBased, BruteForce-SL2, BruteForce-Hamming(2)
        // matcher_ = cv::DescriptorMatcher::create( "BruteForce-Hamming" ); 
        matcher_ = cv::DescriptorMatcher::create( "BruteForce" ); 
        
        if( detector_.empty() || 
            extractor_.empty() || 
            matcher_.empty()  )  {
            std::cout << "Failed to create detector/descriptor/extractor" << std::endl;
        }

        // mode = TRACK3D_WITH_NORMALS;

        //--------------------------------------------
        // Set global parameters and constants 
        // - mode, subpixel refinement, block sizes
        //--------------------------------------------


        enable_subpixel_refinement_ = true;
    }

    // GeneralPurposeFeatureTracker::GeneralPurposeFeatureTracker (const GeneralPurposeFeatureTracker& gpft) { 
    //     std::cerr << "GeneralPurposeFeatureTracker copy: " << std::endl;
    // }

    DepthFeatures::~DepthFeatures () { 
        std::cerr << "DepthFeatures dtor: " << std::endl;
    }

    void 
    DepthFeatures::setScale(const float scale_factor) { 

    }

    void 
    DepthFeatures::update(const cv::Mat& img, const cv::Mat& depth) { 

        // Create NaN mask
        cv::Mat_<float> zero = cv::Mat_<float>::zeros(depth.size());
        cv::Mat mask = (depth == 0);

        // Convert to 32-bit floating point
        cv::Mat depthf;
        depth.convertTo(depthf, CV_32F, 1.f/1000);

        // Gaussian blur
        // cv::GaussianBlur(depthf, depthf, cv::Size(9,9), 0);

        // Sobel at scales
        cv::Mat sobel3, sobel5;
        cv::Sobel(depthf, sobel3, depthf.depth(), 1, 1, 3);
        cv::Sobel(depthf, sobel5, depthf.depth(), 1, 1, 7);

        // cv::Mat sob1[] = {sobelx, sobely, zero} ;

        // cv::Mat_<cv::Vec3f> sobel;
        // cv::merge(sob1,3,sobel); 
        // cv::imshow("Sobel", sobel);


        cv::Mat_<cv::Vec6f> depthev;
        cv::cornerEigenValsAndVecs(depthf, depthev, 10, 5);

        std::vector<cv::Mat> ch;
        cv::split(depthev, ch);


        cv::Mat lambdas[] = {ch[0], ch[1], zero} ;
        cv::Mat ev1[] = {ch[2], ch[3], zero} ;
        cv::Mat ev2[] = {ch[4], ch[5], zero} ;

        cv::Mat_<cv::Vec3f> evec1;
        cv::merge(ev2,3,evec1); 

        // // Convert to 8-bit
        // cv::Mat1b depth8; 
        // convertScaleAbs(depth, depth8, 30.f/1000);

        // cv::Mat resp8;
        // cornerHarris(depthf, resp8, 10, 3, 0.001, cv::BORDER_DEFAULT);

        cv::Mat g1, g2;
        cv::GaussianBlur(depthf, g1, cv::Size(9,9), 0);
        cv::GaussianBlur(depthf, g2, cv::Size(15,15), 0);

        // cv::imshow("mask", mask);
        // cv::imshow("img", img);
        cv::imshow("depth", depthf);

        cv::imshow("evec1", evec1);

        // for (int j=0; j<6; j++) { 
        //     std::stringstream ss; 
        //     ss << "depth" << j ;
        //     cv::imshow(ss.str(), ch[j]);
        // }
        // cv::imshow("depth8", depth8);
        // cv::imshow("resp8", resp8);
        // cv::imshow("g1", g1);
        // cv::imshow("g2", g2);
        cv::imshow("dog", sobel5-sobel3);

    }

}
