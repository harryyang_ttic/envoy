// lcm
#include <lcm/lcm-cpp.hpp>
#include <lcmtypes/bot2_param.h>

// libbot/lcm includes
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>

// libbot cpp
#include <lcmtypes/bot_core.hpp>

// opencv-utils includes
#include <perception_opencv_utils/opencv_utils.hpp>

// lcm log player wrapper
#include <lcm-utils/lcm-reader-util.hpp>

// optargs
#include <ConciseArgs>

// Profiler
#include <fs-utils/profiler.hpp>

#include "depth_features.hpp"

#include <slic/slic_utils.hpp>

// pcl-utils includes
#include <pcl-utils/pcl_utils.hpp>

// vis-utils includes
#include <vis-utils/vis_utils.hpp>

// // pcl includes
// #include <pcl/common/common_headers.h>
// #include <pcl/io/io.h>
// #include <pcl/io/pcd_io.h>
// #include <sure/sure_estimator.h>
// #include <pcl/common/transforms.h>
// #include <pcl/keypoints/iss_3d.h>

// #include <pcl/keypoints/keypoint.h>
// #include <pcl/features/normal_3d.h>
// #include <pcl/features/integral_image_normal.h>
// #include <pcl/keypoints/harris_3d.h>
// #include <pcl/keypoints/harris_6d.h>
// #include <pcl/search/organized.h>
// #include <pcl/keypoints/susan.h>
// #include <pcl/keypoints/uniform_sampling.h>

// #include <pcl/point_types.h>
// #include <pcl/io/pcd_io.h>
// #include <pcl/search/organized.h>
// #include <pcl/search/kdtree.h>
// #include <pcl/features/normal_3d_omp.h>
// #include <pcl/filters/conditional_removal.h>
// #include <pcl/segmentation/extract_clusters.h>

// #include <pcl/features/don.h>


// #include <pcl/range_image/range_image.h>
// #include <pcl/range_image/range_image_planar.h>

// #include <pcl/visualization/pcl_visualizer.h>
// #include <pcl/visualization/cloud_viewer.h>
// #include <pcl/visualization/range_image_visualizer.h>
// #include <pcl/features/range_image_border_extractor.h>
// #include <pcl/keypoints/narf_keypoint.h>
// #include <pcl/keypoints/harris_3d.h>


using namespace std;
using namespace cv;

//----------------------------------
// State representation
//----------------------------------
struct state_t {
    lcm::LCM lcm;
    cv::VideoWriter writer;
    fsvision::Profiler profiler;

    fsvision::DepthFeatures dfeatures_;

    state_t() {
        profiler.setName("Depth Features");
    }

    ~state_t() { 
    }

    void* ptr() { 
        return (void*) this;
    }
    
    void process(const cv::Mat& img);

    void on_kinect_image_frame (const lcm::ReceiveBuffer* rbuf, 
                                const std::string& chan,
                                const kinect::frame_msg_t *msg);

};
state_t state;

struct DepthFeaturesOptions { 
    bool vLIVE_MODE;
    bool vCREATE_VIDEO;
    bool vDEBUG;
    float vMAX_FPS;
    float vSCALE;
    std::string vLOG_FILENAME;
    std::string vCHANNEL;

    DepthFeaturesOptions () : 
        vLOG_FILENAME(""), vCHANNEL("KINECT_FRAME"), 
        vMAX_FPS(30.f) , vSCALE(1.f), 
        vDEBUG(false), vLIVE_MODE(false), vCREATE_VIDEO(false) {}
};
DepthFeaturesOptions options;

// LCM Log player wrapper
LCMLogReaderOptions poptions;
LCMLogReader player;


static void on_kinect_image_frame(const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                                   const kinect::frame_msg_t *msg) {
    state.on_kinect_image_frame(rbuf, chan, msg);
}

void state_t::on_kinect_image_frame (const lcm::ReceiveBuffer* rbuf, const std::string& chan,
                                   const kinect::frame_msg_t *msg) {

    profiler.enter("on_kinect");

    //----------------------------------
    // Unpack Point cloud
    //----------------------------------
    cv::Mat img;

    cv::Mat_<uint16_t> depth;
    opencv_utils::unpack_kinect_frame_with_depth(msg, img, depth, options.vSCALE);
    cv::Mat1b depth_mask = (depth != 0);

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud;
    pcl_utils::unpack_kinect_frame_with_cloud(msg, img, cloud, options.vSCALE);

    pcl::PointCloud<pcl::Normal>::Ptr normals;
    pcl_utils::compute_normals(*cloud, *normals);

    //----------------------------------
    // Compute depth/normals mask
    //----------------------------------
    cv::Vec3f normals_NaN = cv::Vec3f(0,0,1);

    // Convert normals to cv::Vec3f
    cv::Mat_<cv::Vec3f> m_normals;
    pcl_utils::convert_pcl_to_normalsmat(*normals, m_normals, normals_NaN);

    // Build Normals mask
    cv::Mat1b normals_mask(m_normals.size());
    for (int j=0; j<m_normals.cols * m_normals.rows; j++)  { 
        const cv::Vec3f& v = m_normals.at<cv::Vec3f>(j);
        normals_mask.at<uchar>(j) = ((v == v) && v != normals_NaN) ? 255 : 0;
    }
    
    // For visualization purposes
    m_normals += cv::Mat_<cv::Vec3f>::ones(m_normals.size());
    cv::imshow("normals", m_normals);

    // Bitwise OR: Normals + Depth mask
    cv::Mat1b dn_mask; cv::bitwise_or(normals_mask, depth_mask, dn_mask);

    //----------------------------------
    // Main SLIC computation
    // Provide depth+normals mask
    //----------------------------------
    int superpixel_size = 300; double compactness = 0.5; 
    SLICGeneric slic;
    cv::Mat normlabels = slic.forGivenSuperpixelSize(m_normals, 
                                                     superpixel_size, 
                                                     compactness, -1, dn_mask);

    cv::Mat dst = fsvision::draw_contours_from_labels(img, normlabels);
    cv::imshow("dlabels", dst);

    //----------------------------------
    // Get Mean locations/normals
    //----------------------------------
    cv::Mat mu_xy = slic.getMeanLocations();
    cv::Mat mu_normals = slic.getMeanValues();
    
    // std::cerr << "mu_xy" << mu_xy << std::endl;
    // std::cerr << "mu_normals" << mu_normals << std::endl;

    // cv::Mat mu_normals_img = fsvision::apply_label_values_to_img(mu_normals, normlabels);
    // // mu_normals_img += cv::Mat_<cv::Vec3f>::ones(mu_normals_img.size());
    // cv::imshow("mu_normals", mu_normals_img);

    // int scale1 = 3, scale2 = 7;

    // pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud;
    // pcl_utils::unpack_kinect_frame_with_cloud(msg, img, cloud);
    // vis_utils::publish_cloud("CLOUD", cloud);


    // pcl::search::OrganizedNeighbor
    //     <pcl::PointXYZRGB>::Ptr tree(new pcl::search::OrganizedNeighbor<pcl::PointXYZRGB>());
    // tree->setInputCloud(cloud);

    // Compute normals using both small and large scales at each point
    // pcl::NormalEstimationOMP<pcl::PointXYZRGB, pcl::PointNormal> ne;
    // ne.setInputCloud (cloud);
    // ne.setSearchMethod (tree);
    // ne.setViewPoint (std::numeric_limits<float>::max (), 
    //                  std::numeric_limits<float>::max (), 
    //                  std::numeric_limits<float>::max ());  

    // // calculate normals with the small scale
    // cout << "Calculating normals for scale..." << scale1 << endl;
    // pcl::PointCloud<pcl::PointNormal>::Ptr normals_small_scale (new pcl::PointCloud<pcl::PointNormal>);

    // ne.setRadiusSearch (scale1);
    // ne.compute (*normals_small_scale);
    
    // // calculate normals with the large scale
    // cout << "Calculating normals for scale..." << scale2 << endl;
    // pcl::PointCloud<pcl::PointNormal>::Ptr normals_large_scale (new pcl::PointCloud<pcl::PointNormal>);

    // ne.setRadiusSearch (scale2);
    // ne.compute (*normals_large_scale);

    // // Create output cloud for DoN results
    // PointCloud<pcl::PointNormal>::Ptr doncloud (new pcl::PointCloud<pcl::PointNormal>);
    // copyPointCloud<pcl::PointXYZRGB, pcl::PointNormal>(*cloud, *doncloud);

    // std::cout << "Calculating DoN... " << std::endl;
    // // Create DoN operator
    // pcl::DifferenceOfNormalsEstimation<pcl::PointXYZRGB, pcl::PointNormal, pcl::PointNormal> don;
    // don.setInputCloud (cloud);
    // don.setNormalScaleLarge (normals_large_scale);
    // don.setNormalScaleSmall (normals_small_scale);

    // if (!don.initCompute ())
    //     {
    //         std::cerr << "Error: Could not intialize DoN feature operator" << std::endl;
    //         exit (EXIT_FAILURE);
    //     }

    // // Compute DoN
    // don.computeFeature (*doncloud);

    // // pcl::HarrisKeypoint3D<pcl::PointXYZRGB,pcl::PointXYZI> harris3D; 
    // pcl::UniformSampling<pcl::PointXYZRGB> keypoint3D;

    // keypoint3D.setSearchMethod(tree);
    // // keypoint3D.setNormals(normals);
    // keypoint3D.setInputCloud(cloud); 

    // // // Harris
    // // keypoint3D.setNonMaxSupression(true); 
    // // keypoint3D.setRadius (0.15); 
    // // keypoint3D.setRadiusSearch (0.05); 
    // // keypoint3D.setMethod(pcl::HarrisKeypoint3D<pcl::PointXYZRGB,pcl::PointXYZI>::LOWE); 

    // keypoint3D.setRadiusSearch(0.2);
    // // keypoint3D.setNonMaxSupression(true);

    // pcl::PointCloud<pcl::PointXYZRGB>::Ptr keypoints (new pcl::PointCloud<pcl::PointXYZRGB, int>); 
    // keypoint3D.compute(*keypoints); 
    // cerr << "Computed " << keypoints->points.size () << " Keypoints  " << std:: endl; 

    // // get a copy of the features for visualization
    // pcl::PointCloud<pcl::PointXYZRGB>::Ptr features_cloud = 
    //     pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>());
    // features_cloud->points.resize(keypoints->points.size());
    // for (int j=0; j<keypoints->points.size(); j++) {
    //     features_cloud->points[j].x = keypoints->points[j].x, 
    //         features_cloud->points[j].y = keypoints->points[j].y, 
    //         features_cloud->points[j].z = keypoints->points[j].z;
    //     features_cloud->points[j].r = 0, 
    //         features_cloud->points[j].g = 255.f, 
    //         features_cloud->points[j].b = 0;
    // }
    // vis_utils::publish_cloud("FEATURES", features_cloud);


    // // SURE 3D Feature base class
    // sure::SURE_Estimator<pcl::PointXYZRGB> sure;
    // sure::Configuration config;

    // // set input cloud
    // sure.setInputCloud(cloud);

    // // Ignore background detections
    // config.setAdditionalPointsOnDepthBorders(true);
    // config.setIgnoreBackgroundDetections(true);

    // // Adjust the size of the features (in meter)
    // config.setSize(0.01f);

    // // Adjust the sampling rate
    // config.setSamplingRate(0.04f);

    // // Adjust the normal sampling rate
    // config.setNormalSamplingRate(0.1f);

    // // Adjust the influence radius for calculating normals
    // // config.setNormalsScale(0.02f);

    // // Adjust the minimum Cornerness to reduce number of features on edges
    // config.setMinimumCornerness(0.15f);

    // // config.setEntropyCalculationMode(sure::NORMALS);
    // config.setEntropyCalculationMode(sure::CROSSPRODUCTS_ALL_NORMALS_PAIRWISE);

    // // set altered configuration
    // sure.setConfig(config);

    // // calculate features
    // sure.calculateFeatures();

    // // get a copy of the features for visualization
    // pcl::PointCloud<pcl::InterestPoint>::Ptr features = sure.getInterestPoints();
    // pcl::PointCloud<pcl::PointXYZRGB>::Ptr features_cloud = 
    //     pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>());
    // features_cloud->points.resize(features->points.size());
    // for (int j=0; j<features->points.size(); j++) {
    //     features_cloud->points[j].x = features->points[j].x, 
    //         features_cloud->points[j].y = features->points[j].y, 
    //         features_cloud->points[j].z = features->points[j].z;
    //     features_cloud->points[j].r = 0, 
    //         features_cloud->points[j].g = 255.f, 
    //         features_cloud->points[j].b = 0;
    // }
    // vis_utils::publish_cloud("FEATURES", features_cloud);

    // std::vector<cv::Point2f> surepts;
    // pcl_utils::project_points(features, surepts);

    // cv::Mat display = img.clone();
    // for (int j=0; j<surepts.size(); j++) {
    //     circle(display, surepts[j], 1, cv::Scalar(0,240,0), 1, CV_AA);
    // }
    // cv::imshow("display", display);

    // Main update
    // dfeatures_.update(img, depth);

    // Set timestamp
    profiler.leave("on_kinect");
    cv::waitKey(10);

}

void state_t::process(const cv::Mat& img) { 

    double t1 = bot_timestamp_now();


    // //----------------------------------
    // // Create video writer
    // //----------------------------------
    // if (options.vCREATE_VIDEO && options.vLOG_FILENAME != "") {
    //     if (!writer.isOpened()) { 
    //         writer.open(options.vLOG_FILENAME + "-video.avi", VideoWriter::fourcc('M','P','4','2'),
    //                            30, out_img.size(), 1);
    //         std::cerr << "===> Video writer created" << std::endl;
    //     }
    //     writer << out_img;
    //     std::cerr << "writing" << std::endl;
    // }
    return;
}

int main( int argc, char** argv )
{

    //----------------------------------
    // Opt args
    //----------------------------------
    ConciseArgs opt(argc, (char**)argv);
    opt.add(options.vLOG_FILENAME, "l", "log-file","Log file name");
    opt.add(options.vCREATE_VIDEO, "v", "create-video","Create video");
    opt.add(options.vCHANNEL, "c", "channel","Kinect Channel");
    opt.add(options.vMAX_FPS, "r", "max-rate","Max FPS");
    opt.add(options.vSCALE, "s", "scale","Scale");    
    opt.parse();

    // Handle special args cases
    if (options.vLIVE_MODE) { 
        options.vCREATE_VIDEO = false;
    }

    //----------------------------------
    // args output
    //----------------------------------
    std::cerr << "===========  DepthFeatures app ============" << std::endl;
    std::cerr << "MODES 1: depth-features-app -l log-file -v <create video> -r max-rate\n";
    std::cerr << "=============================================\n";
    if (options.vLOG_FILENAME != "") { 
        std::cerr << "=> LOG FILENAME : " << options.vLOG_FILENAME << std::endl;    
    } else { 
        std::cerr << "=> LIVE MODE" << std::endl; options.vLIVE_MODE = true;
    }
    std::cerr << "=> CREATE VIDEO : " << options.vCREATE_VIDEO << std::endl;
    std::cerr << "=> CHANNEL : " << options.vCHANNEL << std::endl;
    std::cerr << "=> MAX FPS : " << options.vMAX_FPS << std::endl;
    std::cerr << "=> SCALE : " << options.vSCALE << std::endl;

    std::cerr << "===============================================" << std::endl;

    //----------------------------------
    // Setup Player Options
    //----------------------------------
    poptions.fn = options.vLOG_FILENAME;
    poptions.ch = options.vCHANNEL;
    poptions.fps = options.vMAX_FPS;
    poptions.lcm = &state.lcm;
    poptions.handler = on_kinect_image_frame;
    poptions.user_data = state.ptr();

    //----------------------------------
    // Subscribe, and start main loop
    //----------------------------------
    if (options.vLIVE_MODE) {
        state.lcm.subscribe(options.vCHANNEL, &state_t::on_kinect_image_frame, &state ); 
        while (state.lcm.handle() == 0);
    } else { 
        player.init(poptions); 
    }

    return 0;
}
