#include "slic_tracker.h"

int slic_size = 225;
int slic_dim = std::sqrt(slic_size);
int slic_k = 30;
int slic_m = 40;
int slic_rgb_dist = 30;

const double SCALE = 4.0;

#define HSV 1
// ColorClassifier CC(HSV);

void normal_estimation(pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, pcl::PointCloud<pcl::Normal>::Ptr& cloud_normals) { 
    // Normal estimation
    pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> ne;
    ne.setInputCloud(cloud);

    pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree_n (new pcl::search::KdTree<pcl::PointXYZRGB>());
    ne.setSearchMethod(tree_n);
    ne.setRadiusSearch(0.2);
    ne.compute(*cloud_normals);
    // std::cout << "Estimated the normals" << std::endl;

    return;
}

void publish_point_cloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud) { 
    return;
}

cv::Mat3b rgb_img;
static void on_kinect_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                                   const kinect_frame_msg_t *msg, 
                                   void *user_data ) {
    // Check msg type
    state_t *state = (state_t*) user_data;
    if (msg->depth.depth_data_format != KINECT_DEPTH_MSG_T_DEPTH_MM) { 
        std::cerr << "Warning: kinect data format is not KINECT_DEPTH_MSG_T_DEPTH_MM" << std::endl 
                  << "Program may not function properly" << std::endl;
    }
    assert (msg->depth.depth_data_format == KINECT_DEPTH_MSG_T_DEPTH_MM);

    // Unpack Point cloud
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB> ());
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal> ());

    double t1 = bot_timestamp_now();
    cv::Mat3b img(msg->image.height, msg->image.width);
    state->pc_lcm->unpack_kinect_frame(msg, img.data, cloud);
    cv::resize(img, rgb_img, cv::Size(int(img.cols / SCALE), int(img.rows / SCALE)), 0, 0, cv::INTER_NEAREST);
    double t2 = bot_timestamp_now();
    std::cerr << "unpack time: " << (t2-t1)*1e-3 << std::endl;

    std::cerr << "cloud: " << cloud->points.size() << std::endl;
    std::cerr << "rgb: " << rgb_img.size() << std::endl;
    
    cvtColor(rgb_img, rgb_img, CV_BGR2RGB);

    //----------------------------------
    // SLIC superpixel segmentation
    //----------------------------------
    SLIC_info slic_info = slic_superpixel_segmentation(rgb_img);
    const SLIC& slic = slic_info.slic;
    
    //----------------------------------
    // Delaunay triangulation on SLIC centroids
    //----------------------------------
    cv::Subdiv2D subdiv(cv::Rect(0, 0, rgb_img.cols , rgb_img.rows));
    for (int j=0; j<slic.kseeds_x.size(); j++)  { 
        int x = slic.kseeds_x[j];
        int y = slic.kseeds_y[j];
        int n = slic.klabel[y*rgb_img.cols + x];

        subdiv.insert(cv::Point2f(x,y));
        cv::circle(rgb_img, cv::Point2f(x,y), 1, cv::Scalar(0,0,0), CV_FILLED);        
        cv::putText(rgb_img, cv::format("%i", n),cv::Point2f(x,y), 0, 0.25, cv::Scalar(255,255,255),1);
    }

    //----------------------------------
    // Delaunay triangulation on SLIC centroids
    //----------------------------------
    std::vector<cv::Vec6f> triangleList;
    subdiv.getTriangleList(triangleList);
    cv::vector<cv::Point> pt(3);

    cv::Scalar active_facet_color(0, 0, 255), delaunay_color(255,255,255);
    std::vector<cv::Vec3i> faces;
    for( size_t i = 0; i < triangleList.size(); i++ ) {
        cv::Vec6f t = triangleList[i];
        pt[0] = cv::Point(cvRound(t[0]), cvRound(t[1]));
        pt[1] = cv::Point(cvRound(t[2]), cvRound(t[3]));
        pt[2] = cv::Point(cvRound(t[4]), cvRound(t[5]));
        line(rgb_img, pt[0], pt[1], delaunay_color, 1, CV_AA, 0);
        line(rgb_img, pt[1], pt[2], delaunay_color, 1, CV_AA, 0);
        line(rgb_img, pt[2], pt[0], delaunay_color, 1, CV_AA, 0);

        int p1 = 0, p2 = 0, p3 = 0, curr_edge = 0;
        // subdiv.locate(cv::Point2f(t[0], t[1]), curr_edge, p1);
        // subdiv.locate(cv::Point2f(t[2], t[3]), curr_edge, p2);
        // subdiv.locate(cv::Point2f(t[4], t[5]), curr_edge, p3);
        faces.push_back(cv::Vec3i(p1, p2, p3));         
    }
    opencv_utils::imshow("rgb", rgb_img);


    // //----------------------------------
    // // Normal estimation
    // //----------------------------------
    // normal_estimation(cloud, cloud_normals);

    // //----------------------------------
    // // Averaged Surface normals
    // //----------------------------------
    // std::vector<cv::Point3f> normals_orig = std::vector<cv::Point3f>(slic.numlabels, cv::Point3f(0,0,0));
    // std::vector<cv::Point3f> normals_vec = std::vector<cv::Point3f>(slic.numlabels, cv::Point3f(0,0,0));
    // for (int j=0; j<cloud->points.size(); j++)  { 
    //     int n = slic.klabel[j];
    //     normals_orig[n] += cv::Point3f(cloud->points[j].x, cloud->points[j].y, cloud->points[j].z);
    //     normals_vec[n] += cv::Point3f(cloud_normals->points[j].normal[0], 
    //                                   cloud_normals->points[j].normal[1], 
    //                                   cloud_normals->points[j].normal[2]);
    // }
    // for (int j=0; j<normals_orig.size(); j++)  { 
    //     normals_orig[j].x /= slic.cluster_count[j], 
    //         normals_orig[j].y /= slic.cluster_count[j], 
    //         normals_orig[j].z /= slic.cluster_count[j];
    //     normals_vec[j].x /= slic.cluster_count[j],
    //         normals_vec[j].y /= slic.cluster_count[j],
    //         normals_vec[j].z /= slic.cluster_count[j];
    // }

    

    // //----------------------------------
    // // Publish point cloud to viewer
    // //----------------------------------
    // erlcm_xyzrgb_point_list_t pt_list;
    // pt_list.utime = bot_timestamp_now();
    // pt_list.no_points = cloud->points.size();
    // pt_list.points = (erlcm_xyzrgb_point_t*) malloc(cloud->points.size() * sizeof(erlcm_xyzrgb_point_t));
    // pt_list.no_faces = 0; // faces.size();
    // pt_list.faces = 0; // (erlcm_tri_t*) malloc(cloud->points.size() * sizeof(erlcm_tri_t));
    // for (int j=0; j<cloud->points.size(); j++) { 
    //     erlcm_xyzrgb_point_t pt; 
    //     pt.xyz[0] = cloud->points[j].x, pt.xyz[1] = cloud->points[j].y, pt.xyz[2] = cloud->points[j].z;
    //     // std::cerr << (int)cloud->points[j].b << " " << (int)cloud->points[j].g << " " << (int)cloud->points[j].r << " " << std::endl;
    //     pt.rgba = 0;
    //     pt.rgba |= 100; 
    //     pt.rgba |= (int)cloud->points[j].b << 8; 
    //     pt.rgba |= (int)cloud->points[j].g << 16; 
    //     pt.rgba |= (int)cloud->points[j].r << 24;
    //     pt_list.points[j] = pt;
    // }
    // // for (int j=0; j<faces.size(); j++) { 
    // //     pt_list.faces[j].tri[0] = faces[j][0];
    // //     pt_list.faces[j].tri[1] = faces[j][1];
    // //     pt_list.faces[j].tri[2] = faces[j][2];
        
    // // }
    // erlcm_xyzrgb_point_list_t_publish(state->lcm, "PCL_XYZRGB_LIST", &pt_list);
    // free(pt_list.points);

    // // publish_point_cloud(cloud);

    // //----------------------------------
    // // Publish slic normals to viewer
    // //----------------------------------
    // erlcm_normal_point_list_t normal_pt_list;
    // normal_pt_list.utime = msg->timestamp;
    // normal_pt_list.no_points = normals_orig.size();
    // normal_pt_list.points = (erlcm_normal_point_t*) malloc(normals_orig.size() * sizeof(erlcm_normal_point_t));
    // for (int j=0; j<normals_orig.size(); j++) { 
    //     erlcm_normal_point_t pt; 
    //     pt.xyz[0] = normals_orig[j].x, pt.xyz[1] = normals_orig[j].y, pt.xyz[2] = normals_orig[j].z;
    //     pt.normals[0] = normals_vec[j].x, pt.normals[1] = normals_vec[j].y, pt.normals[2] = normals_vec[j].z;
    //     // pt.xyz[0] = cloud->points[j].x, pt.xyz[1] = cloud->points[j].y, pt.xyz[2] = cloud->points[j].z;
    //     // pt.normals[0] = cloud_normals->points[j].normal[0], 
    //     //     pt.normals[1] = cloud_normals->points[j].normal[1], 
    //     //     pt.normals[2] = cloud_normals->points[j].normal[2];
    //     normal_pt_list.points[j] = pt;
    // }
    // erlcm_normal_point_list_t_publish(state->lcm, "PCL_NORMAL_LIST", &normal_pt_list);
    // free(normal_pt_list.points);
    

    // // Publish point cloud to viewer
    // erlcm_normal_point_list_t normal_pt_list;
    // normal_pt_list.utime = msg->timestamp;
    // normal_pt_list.no_points = cloud_normals->points.size();
    // normal_pt_list.points = (erlcm_normal_point_t*) malloc(cloud_normals->points.size() * sizeof(erlcm_normal_point_t));
    // for (int j=0; j<cloud->points.size(); j++) { 
    //     erlcm_normal_point_t pt; 
    //     pt.xyz[0] = normals_orig[j].x, pt.xyz[1] = normals_orig[j].y, pt.xyz[2] = normals_orig[j].z;
    //     pt.normals[0] = normals_vec[j].x, pt.normals[1] = normals_vec[j].y, pt.normals[2] = normals_vec[j].z;
    //     // pt.xyz[0] = cloud->points[j].x, pt.xyz[1] = cloud->points[j].y, pt.xyz[2] = cloud->points[j].z;
    //     // pt.normals[0] = cloud_normals->points[j].normal[0], 
    //     //     pt.normals[1] = cloud_normals->points[j].normal[1], 
    //     //     pt.normals[2] = cloud_normals->points[j].normal[2];
    //     normal_pt_list.points[j] = pt;
    // }
    // erlcm_normal_point_list_t_publish(state->lcm, "PCL_NORMAL_LIST", &normal_pt_list);
    // free(normal_pt_list.points);

}

SLIC_info slic_superpixel_segmentation(const cv::Mat& img) { 
    double t1 = bot_timestamp_now();
    unsigned int* imgp = new unsigned int[img.rows * img.cols];
    opencv_utils::convert_to_argb(img, imgp);
    double t2 = bot_timestamp_now();
    std::cerr << " time: " << (t2-t1)*1e-3 << std::endl;

    //----------------------------------
    // Perform SLIC on the image buffer
    //----------------------------------
    SLIC_info slic_info;
    SLIC& slic = slic_info.slic;
    t1 = bot_timestamp_now();
    slic.DoSuperpixelSegmentation_ForGivenSuperpixelSize(imgp, img.cols, img.rows, slic_size, slic_m);
    t2 = bot_timestamp_now();
    std::cerr << " superpixel time: " << (t2-t1)*1e-3 << std::endl;

    // Alternately one can also use the function DoSuperpixelSegmentation_ForGivenStepSize() for a desired superpixel size
    //----------------------------------
    // Draw boundaries around segments
    //----------------------------------
    int* klabels = &slic.klabel[0];
    std::cerr << "NUM: " << slic.klabel.size() << " " << slic.numlabels << std::endl;
    t1 = bot_timestamp_now();
    slic.DrawContoursAroundSegments(imgp, klabels, img.cols, img.rows, 0xff0000);
    t2 = bot_timestamp_now();
    std::cerr << " draw time: " << (t2-t1)*1e-3 << std::endl;

    //----------------------------------
    // Convert to Mat
    //----------------------------------
    cv::Mat slic_img(img.rows,img.cols, img.type());
    opencv_utils::convert_to_mat(imgp, slic_img);

    //----------------------------------
    // Centroids
    //----------------------------------
    for (int j=0; j<slic.kseeds_x.size(); j++)  { 
        int x = slic.kseeds_x[j];
        int y = slic.kseeds_y[j];
        int n = slic.klabel[y*img.cols + x];

        cv::circle(slic_img, cv::Point2f(x,y), 1, cv::Scalar(0,0,0), CV_FILLED);        
        cv::putText(slic_img, cv::format("%i", n),cv::Point2f(x,y), 0, 0.25, cv::Scalar(255,255,255),1);
    }

    //----------------------------------
    // Compute mean color
    //----------------------------------
    slic.ComputeMeanSuperPixelColor(img, SLIC_RGB);

    //----------------------------------
    // Plot the slic img with mean color features
    //----------------------------------
    cv::Mat3b mean_slic_img(slic_img.rows, slic_img.cols);
    for (int y=0; y<mean_slic_img.rows; y++) { 
        for (int x=0; x<mean_slic_img.cols; x++) { 
            int idx = y*mean_slic_img.cols + x;
            int n = klabels[idx];
            mean_slic_img(y,x) = cv::Vec3b(slic.mu_ch1[n], slic.mu_ch2[n], slic.mu_ch3[n]);
        }
    }
    opencv_utils::imshow("Mean slic img", mean_slic_img);

//     // ------------------------------
//     // Slic color histogram
//     // ------------------------------

//     // === Find min=max depth === 
//     double hue_max = 180, hue_min = 0;
//     minMaxLoc(Mat(mu_b), &hue_min, &hue_max, 0, 0);
//     const int hue_bins = ((hue_max - hue_min) / 5) + 1;

//     // === Heat map based on normed hue ===
//     Mat3b mean_hue_img = Mat3b::zeros(slic_img.rows, slic_img.cols);
//     std::vector<double> mu_hue_norm(slic.numlabels, 0);
//     for (int y=0; y<mean_hue_img.rows; y++) { 
//         for (int x=0; x<mean_hue_img.cols; x++) { 
//             int idx = y*mean_hue_img.cols + x;
//             int n = klabels[idx];
//             double hue = double(mu_b[n]);
//             mu_hue_norm[n] = (hue - hue_min) * 1.f / (hue_max - hue_min);
//             mean_hue_img(y,x) = opencv_utils::hsv_to_bgrvec(Point3f(mu_hue_norm[n] * 120, 255, 255));
//         }
//     }
//     opencv_utils::imshow("slic img normed green", mean_hue_img);

//     // std::cerr << "Hue: " << hue_min << " " << hue_max << std::endl;
//     // std::cerr << "Hue bins: " << hue_bins << std::endl;

//     // === Hue histogram img (only superpixels) ===
//     Mat_<float> hue_hist_img(1, slic.numlabels);
//     for (int j=0; j<hue_hist_img.cols; j++) { 
//         int x = slic.kseeds_x[j];
//         int y = slic.kseeds_y[j];
//         if ( x <= 0 || y <= 0 || x > img.cols  || y > img.rows )
//             continue;        
//         int n = slic.klabels[y*img.cols+x];
        
//         hue_hist_img.at<float>(n) = mu_b[n];
//     }

//     // std::cerr << "Hue Hist Img: " << hue_hist_img << std::endl;
//     // std::cerr << "Hue Hist Img mask: " << bg_hist_mask << std::endl;

//     // === Compute normalized histogram for background given mask and depth values ===
//     Mat bg_hue_hist = opencv_utils::compute_1D_histogram(hue_hist_img, bg_hist_mask, 
//                                                          "BG hue Histogram", 0, 180, hue_bins); //0-1 normalized
//     Mat fg_hue_hist = opencv_utils::compute_1D_histogram(hue_hist_img, fg_hist_mask, 
//                                                          "FG hue Histogram", 0, 180, hue_bins); //0-1 normalized

//     // std::cerr << "BG Hue hist: " << bg_hue_hist << std::endl << std::endl;
//     // std::cerr << "FG Hue hist: " << fg_hue_hist << std::endl << std::endl;

//     // === Compute backprojection with all depth values (mean superpixel depth values) ===
//     float hue_ranges[] = {0, 180.f};
//     Mat backProject_hue;
//     const float* hranges[] = { hue_ranges };
//     calcBackProject(&hue_hist_img, 1, 0, bg_hue_hist, backProject_hue, hranges); 
//     threshold(backProject_hue, backProject_hue, 0.2, 1, THRESH_BINARY_INV);

//     // std::cerr << "Backproject hue: " << backProject_hue << std::endl;

//     // === Visualize backprojection === 
//     Mat1b backProject_hue_img = Mat1b::zeros(slic_img.rows, slic_img.cols);
//     for (int y=0; y<backProject_hue_img.rows; y++) { 
//         for (int x=0; x<backProject_hue_img.cols; x++) { 
//             int idx = y*backProject_hue_img.cols + x;
//             int n = klabels[idx];
//             backProject_hue_img(y,x) = uchar(backProject_hue.at<float>(n) * 255.f);
//         }
//     }

//     // opencv_utils::imshow("backProject hue", backProject_hue);
//     // opencv_utils::imshow("backProject hue img", backProject_hue_img);

//     //----------------------------------
//     // Plot the slic img with mean depth
//     //----------------------------------
//     Mat3b mean_slic_depth_img = Mat3b::zeros(slic_img.rows, slic_img.cols);
//     Mat_<double> mean_slic_depth = Mat_<double>::zeros(slic_img.rows, slic_img.cols);
//     std::vector<double> mu_depth(slic.numlabels, 0);
//     std::vector<int> depth_count(slic.numlabels, 0);
//     for (int y=0; y<mean_slic_depth_img.rows; y++) { 
//         for (int x=0; x<mean_slic_depth_img.cols; x++) { 
//             int idx = y*mean_slic_depth_img.cols + x;
//             int n = klabels[idx];
//             double depth = frame.kinect.getDepth(rect.y + y, rect.x + x);
//             if (depth >= 0.1 && depth < 5) { 
//                 mu_depth[n] += depth; depth_count[n]++;
//             }
//         }
//     }

//     // === Normalize ===
//     for (int j=0; j<mu_depth.size(); j++) mu_depth[j] *= 1.f / depth_count[j];
//     // std::cerr << "mu_depth_norm: " << Mat(mu_depth) << std::endl;
    
//     // === Find min=max depth === 
//     // Only allow upto 1cm bin resolution
//     double depth_max = 1, depth_min = .1;
//     minMaxLoc(Mat(mu_depth), &depth_min, &depth_max, 0, 0);
//     depth_min -= 0.1; depth_max += 0.1;
//     const int depth_bins = ((depth_max - depth_min) / 0.02) + 1;

//     // std::cerr << "Depth: " << depth_min << " " << depth_max << std::endl;
//     // std::cerr << "Depth bins: " << depth_bins << std::endl;

//     // === Heat map based on depth ===
//     std::vector<double> mu_depth_norm(slic.numlabels, 0);
//     for (int y=0; y<mean_slic_depth_img.rows; y++) { 
//         for (int x=0; x<mean_slic_depth_img.cols; x++) { 
//             int idx = y*mean_slic_depth_img.cols + x;
//             int n = klabels[idx];
//             double depth = mu_depth[n];
//             mu_depth_norm[n] = (depth - depth_min) * 1.f / (depth_max - depth_min);
//             if (depth > 0.1f && depth < 5) { 
//                 mean_slic_depth(y,x) = mu_depth_norm[n];
//                 mean_slic_depth_img(y,x) = opencv_utils::hsv_to_bgrvec(Point3f(mu_depth_norm[n] * 120, 255, 255));
//             }
//         }
//     }
//     opencv_utils::imshow("slic img depth", mean_slic_depth_img);

//     //----------------------------------
//     // Slic Mean depth histogram
//     //----------------------------------
//     Mat_<float> depth_hist_img(1, slic.numlabels);
//     for (int j=0; j<depth_hist_img.cols; j++) 
//         depth_hist_img.at<float>(j) = mu_depth_norm[j];

//     // std::cerr << "Depth Hist Img: " << depth_hist_img << std::endl;
//     // std::cerr << "Depth Hist Img mask: " << bg_depth_hist_mask << std::endl;
    
//     // === Compute normalized histogram for background given mask and depth values ===
//     Mat bg_depth_hist = opencv_utils::compute_1D_histogram(depth_hist_img, bg_hist_mask, 
//                                                            "BG depth Histogram", 0, 1, depth_bins); //0-1 normalized
//     Mat fg_depth_hist = opencv_utils::compute_1D_histogram(depth_hist_img, fg_hist_mask, 
//                                                            "FG depth Histogram", 0, 1, depth_bins); //0-1 normalized

//     // std::cerr << "BG Depth hist: " << bg_depth_hist << std::endl << std::endl;
//     // std::cerr << "FG Depth hist: " << fg_depth_hist << std::endl << std::endl;
    
//     // === Compute backprojection with all depth values (mean superpixel depth values) ===
//     float depth_ranges[] = {0, 1};
//     const float* ranges[] = { depth_ranges };
//     Mat backProject_depth;
//     calcBackProject(&depth_hist_img, 1, 0, bg_depth_hist, backProject_depth, ranges);
//     threshold(backProject_depth, backProject_depth, 0.2, 1, THRESH_BINARY_INV);
//     // std::cerr << "Backproject depth: " << backProject_depth << std::endl;
    
//     // === Visualize backprojection === 
//     Mat1b backProject_depth_img = Mat1b::zeros(slic_img.rows, slic_img.cols);
//     for (int y=0; y<backProject_depth_img.rows; y++) { 
//         for (int x=0; x<backProject_depth_img.cols; x++) { 
//             int idx = y*backProject_depth_img.cols + x;
//             int n = klabels[idx];
//             backProject_depth_img(y,x) = backProject_depth.at<float>(n) * 255;
//         }
//     }

//     //----------------------------------
//     // Pick blob with least moment of inertia about the center
//     //----------------------------------
//     pick_low_mi_blob(backProject_depth_img, slic_info.mask);

//     // imshow("backProject depth", backProject_depth);
//     opencv_utils::imshow("backProject depth img", backProject_depth_img);
//     opencv_utils::imshow("slic_info.mask", slic_info.mask);


//     //----------------------------------
//     // Visualize object RGB values
//     //----------------------------------
//     Mat slic_obj_img = Mat::zeros(slic_img.size(), CV_8UC3);
//     for (int j=0; j<slic_info.slic.numlabels; j++) { 
//         int x = slic_info.slic.kseeds_x[j];
//         int y = slic_info.slic.kseeds_y[j];
//         if (x <= 0 || y <= 0 || x > slic_img.cols  || y > slic_img.rows )
//             continue;        
//         int n = slic_info.slic.klabels[y*slic_img.cols+x];

//         // if (slic_info.mask.at<uchar>(y,x))
//         //     std::cerr<<"* "<<n<<": "
//         //              <<"("<<slic_info.mu_b[n]<<","<<slic_info.mu_g[n]<<","<<slic_info.mu_r[n]<<")"
//         //              <<std::endl;
//         // else 
//         //     std::cerr<<"  "<<n<<": "
//         //              <<"("<<slic_info.mu_b[n]<<","<<slic_info.mu_g[n]<<","<<slic_info.mu_r[n]<<")"
//         //              <<std::endl;

// #if HSV
//         Scalar s = opencv_utils::hsv_to_bgr(Point3f(slic_info.mu_b[n], slic_info.mu_g[n], slic_info.mu_r[n]));
// #else
//         Scalar s(slic_info.mu_b[n], slic_info.mu_g[n], slic_info.mu_r[n]);
// #endif
//         circle(slic_obj_img, Point(x,y), 3, s,CV_FILLED);
//         cv::putText(slic_obj_img, cv::format("%i", n),Point2f(x,y), 0, 0.25, s,1);
//     }
//     opencv_utils::imshow("SLIC Object RGB", slic_obj_img);

    //----------------------------------
    // Clean up
    //----------------------------------
    if(imgp) delete [] imgp;
 
    opencv_utils::imshow("slic", slic_img);
    return slic_info;
}


int 
main(int argc, char **argv)
{

    const char *optstring = "dfnsha";
    int simulate = 1;
    int draw = 0;
    int c;

    g_thread_init(NULL);
    setlinebuf (stdout);
    state = (state_t*) calloc(1, sizeof(state_t));

    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
            /* case 'd': */
            /*     draw = 1; */
            /*     break; */
            /* case 'f': */
            /*     simulate = 0; */
            /*     break; */
            /* case 'n': */
            /*     simulate = 2; */
            /*     break; */
            /* case 'a': */
            /*     state->run_always = 1; */
            /*     break; */
            /* case 's': */
            /*     simulate = 1; */
            /*     break; */
            /* case 'h': //help */
            /*     usage(argv[0]); */
            /*     break; */
            default:
                // usage(argv[0]);
                break;
        }
    }


    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
    state->b_server = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->b_server);

    state->pc_lcm = new pointcloud_lcm(state->lcm);
    state->pc_lcm->set_kinect_decimate(SCALE); 

    // Vis Config:
    state->pc_vis = new pointcloud_vis(state->lcm);
    if(draw)
        state->lcmgl = bot_lcmgl_init(state->lcm,"pcl-experiments");


    // LCM-related
    kinect_frame_msg_t_subscribe(state->lcm, "KINECT_FRAME", on_kinect_image_frame, (void*) state );
    state->mainloop = g_main_loop_new( NULL, FALSE );  

    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    /* heart beat*/
    // g_timeout_add(25, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


