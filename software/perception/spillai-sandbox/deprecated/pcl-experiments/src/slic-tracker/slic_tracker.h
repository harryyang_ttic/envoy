// Standard includes
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>

// libbot/lcm includes
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>

#include <lcm/lcm.h>
#include <lcmtypes/bot2_param.h>
#include <lcmtypes/erlcm_xyzrgb_point_list_t.h>
#include <lcmtypes/erlcm/xyzrgb_point_list_t.hpp>
#include <lcmtypes/erlcm_normal_point_list_t.h>
#include <lcmtypes/erlcm/normal_point_list_t.hpp>

// pcl includes
#include <pcl/io/pcd_io.h>
#include <pcl/PointIndices.h>
#include <pcl/registration/icp.h>

#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/segmentation/extract_clusters.h>

// pcl includes for kinect
#include <pcl_tools/pointcloud_lcm.hpp>
#include <pcl_tools/pointcloud_vis.hpp>

// opencv includes
#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/legacy/compat.hpp"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/highgui/highgui.hpp"

// opencv-utils includes
#include <perception_opencv_utils/calib_utils.hpp>
#include <perception_opencv_utils/imshow_utils.hpp>
#include <perception_opencv_utils/math_utils.hpp>
#include <perception_opencv_utils/plot_utils.hpp>
#include <perception_opencv_utils/color_utils.hpp>
#include <perception_opencv_utils/imgproc_utils.hpp>

#include "SLIC.h"

typedef struct
{
    lcm_t *lcm;
    GMainLoop *mainloop;
    BotParam   *b_server;
    BotFrames *frames;
    bot_lcmgl_t *lcmgl;

    pointcloud_lcm* pc_lcm;
    pointcloud_vis* pc_vis;

    /* GMutex *mutex; */
    /* GList *door_list; */
    /* bot_core_planar_lidar_t *last_laser; */
    /* full_laser_state_t *full_laser; */
    /* int run_always; */
    /* int draw; */

}state_t;
state_t* state = NULL;

struct SLIC_info { 
    SLIC slic;
    cv::Mat mask;
    std::vector<double> mu_ch1, mu_ch2, mu_ch3, cluster_count;

    SLIC_info() { 
        mask = cv::Mat();
    }

    SLIC_info(const SLIC& _slic, const cv::Mat& _mask) { 
        slic = _slic;
        mask = _mask.clone();
    }
};

SLIC_info slic_superpixel_segmentation(const cv::Mat& img);
