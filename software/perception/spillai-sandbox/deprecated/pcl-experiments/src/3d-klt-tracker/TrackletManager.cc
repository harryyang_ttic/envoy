#include "TrackletManager.hpp"
// 
// opencv-utils includes
#include <perception_opencv_utils/calib_utils.hpp>
#include <perception_opencv_utils/imshow_utils.hpp>
#include <perception_opencv_utils/math_utils.hpp>
#include <perception_opencv_utils/plot_utils.hpp>
#include <perception_opencv_utils/color_utils.hpp>
#include <perception_opencv_utils/imgproc_utils.hpp>

namespace spvision { 
    //============================================
    // TrackManager class
    //============================================
    TrackManager::TrackManager () { 

    }

    TrackManager::~TrackManager () { 

    }

    void TrackManager::reset() { 
        tracklets.clear();
        track_confidence.clear();
        past_utimes.clear();
        reset_trackletID();
    }


    //--------------------------------------------
    // Only process frames that are newer
    // Ignore frames that are old
    // Track Clean up before update
    //--------------------------------------------
    bool TrackManager::cleanup(int64_t utime, float MAX_TRACK_TIMESTAMP_DELAY) { 
        std::vector<int64_t> erase_ids; 

        //----------------------------------
        // If utime is older than latest of all utimes (full reset)
        //----------------------------------
        int64_t latest_of_all_utimes = 0;
        for (TrackMapIt it = tracklets.begin(); it != tracklets.end(); it++) { 
            Track& track = it->second;
            assert(track.size());
            int64_t latest_utime = track.end()[-1].utime; 
            latest_of_all_utimes = std::max(latest_of_all_utimes, latest_utime);
        }
        if (utime < latest_of_all_utimes) 
            for (TrackMapIt it = tracklets.begin(); it != tracklets.end(); it++) 
                erase_ids.push_back(it->first); 
        bool reset = (tracklets.size() && erase_ids.size() == tracklets.size());
        if (reset) { tracklets.clear(); return true; }
        

        //----------------------------------
        // clean up tracks that are old
        //----------------------------------
        for (TrackMapIt it = tracklets.begin(); it != tracklets.end(); it++) { 
            Track& track = it->second;
            assert(track.size());
            int64_t latest_utime = track.end()[-1].utime; 
            // If update frame is old (then reset)
            if (utime < latest_utime) erase_ids.push_back(it->first); 
            // If update frame is ahead of last track update by GPFT_MAX_TRACK_TIMESTAMP_DELAY seconds
            if (utime - latest_utime >= MAX_TRACK_TIMESTAMP_DELAY * 1e6) erase_ids.push_back(it->first);

        }
        for (int j=0; j<erase_ids.size(); j++) { 
            // std::cerr << "ERASING (RESET!) " << erase_ids[j] << std::endl;
            tracklets.erase(erase_ids[j]); 
        }
        if (erase_ids.size()) std::cerr << "TRACKS ERASED: " << erase_ids.size() << std::endl;
        return false; 
    }

    //--------------------------------------------
    // Prune so that length is only GPFT_MAX_TRACK_TIMESPAN seconds long
    //--------------------------------------------
    void TrackManager::prune(float TIMESPAN) { 
        for (TrackMapIt it = tracklets.begin(); it != tracklets.end(); it++) { 
            Track& track = it->second;
            if (!track.size()) continue;
            int64_t latest_utime = track.end()[-1].utime;
            int64_t first_utime = track.begin()[0].utime;
            if (latest_utime - first_utime > TIMESPAN * 1e6) { 
                int idx = track.size()-1; 
                for (; idx>=0; idx--) 
                    if (latest_utime - track[idx].utime > TIMESPAN * 1e6)
                        break;
                track.erase(track.begin(), track.begin() + idx);
            }
        }

    }

        //--------------------------------------------
        // Prepare SuperFeatures from existing tracklets
        //--------------------------------------------
    void TrackManager::getLatestFeatures(std::vector<SuperFeature>& p_sfpts, 
                                                 std::vector<cv::Point2f>& p_pts) { 
        for (TrackMapIt it = tracklets.begin(); it != tracklets.end(); it++) { 
            Track& track = it->second;
            assert(track.size());
            assert(it->first == track.end()[-1].id);
            assert(past_utimes.size());
            // Add functionality for adding keypoints only if within some window
            int64_t last_utime = past_utimes.front();
            // if (track.end()[-1].utime != last_utime) continue;
            p_sfpts.push_back(track.end()[-1]); 
            p_pts.push_back(track.end()[-1].point2D); 
        }
        return;
    }

    //--------------------------------------------
    // Now the rest unprocessed current points are new
    // Add new features
    //--------------------------------------------
    void TrackManager::addSuperFeatures(int64_t utime, std::list<SuperFeature>& c_sfpts_list, 
                                           float ADD_3D_POINT_THRESHOLD) { 
        int count_added = 0, count_updated = 0;

        // Set all to invalid unless new observations are added
        for (TrackMapIt it = tracklets.begin(); it != tracklets.end(); it++) { 
            Track& track = it->second;
            track.valid = false;
        }
        // Add new observations
        for (std::list<SuperFeature>::iterator it = c_sfpts_list.begin(); 
             it != c_sfpts_list.end(); it++) { 
            if (tracklets[it->id].size()) { 
                if (cv::norm(it->point3D - tracklets[it->id].back().point3D) > ADD_3D_POINT_THRESHOLD)
                    continue;
            }
            it->id = (it->id >= 0) ? it->id : nextTrackletID(); 
            (it->id >= 0) ? count_updated++ : count_added++;                    
            tracklets[it->id].push_back(*it);
            tracklets[it->id].valid = true;
            updateConfidence(it->id);
        }

        printf("Total: %i, Added: %i, Updated: %i points\n", 
               c_sfpts_list.size(), count_added, count_updated); 
        printf("Tracklets : %i\n",tracklets.size());

        past_utimes.push_front(utime);
        return;
    }

    void TrackManager::plot(int64_t utime, const cv::Mat& img) { 
        if (img.empty()) return;

        double sc = 1.f;
        cv::Mat display = img.clone();
        cv::Mat dalpha = 0.5 * display;
    
        {   int64_t max_track_length = 1, min_track_length = std::numeric_limits<int>::max();
            for (std::map<int64_t, int64_t>::iterator it = track_confidence.begin(); 
                 it != track_confidence.end(); it++) { 
                const int64_t feat_id = it->first; 
                max_track_length = std::max(it->second, max_track_length);
                min_track_length = std::min(it->second, min_track_length);
            }
            float conf_norm = 255.f / (max_track_length - min_track_length);

            cv::Vec3b h(0,200,200);
            float r = 5; // spvision::GPFT_ADD_POINT_DISTANCE_THRESHOLD;
            for (TrackMapIt it = tracklets.begin(); it != tracklets.end(); it++) { 
                const int64_t feat_id = it->first; 
                const Track& track = it->second; 
            
                if (!track.valid) continue;
                if (track.end()[-1].utime != utime) continue;

                // std::cerr << "FeatID: " << feat_id << " " << track.end()[-1].point2D << std::endl;
                cv::Point2f pt = track.end()[-1].point2D;
                float theta = track.end()[-1].angle * CV_PI / 180;

                // Draw trace of tracklet
                for (int k=track.size()-2, j=0; k>=0; k--, j++)
                    cv::line(dalpha, sc * track[k].point2D, sc * track[k+1].point2D, 
                             CV_RGB(0,0,200), 1, CV_AA, 0);

                double conf = (track_confidence[it->first] - min_track_length) * conf_norm;
                h = cv::Vec3b(conf,conf,conf);

                // Draw the keypoint circumference
                circle(dalpha, sc * pt, r, cv::Scalar(h[0],h[1],h[2]), 1,CV_AA);

                // // Draw the keypoint angle
                // cv::line(dalpha, sc * pt, sc * (pt + cv::Point2f(r*cos(theta), r*sin(theta))), 
                //          cv::Scalar(h[0],h[1],h[2]), 2, CV_AA, 0);

                // Draw the center 
                circle(dalpha, sc * pt, 1, cv::Scalar(200,200,200), 1,CV_AA);
                
                // Draw the ID
                putText(dalpha,cv::format("%i",feat_id), sc * pt, 0, 0.3, cv::Scalar(0,200,0),1,CV_AA);
            }
            addWeighted(display, 0.2, dalpha, 0.5, 0, display);

        }
        opencv_utils::imshow("Tracklets", display);
        return;
           
    }


    //============================================
    // TrackletManager class
    //============================================
    TrackletManager::TrackletManager () { 

    }

    TrackletManager::~TrackletManager () { 

    }

    void TrackletManager::init(const int64_t TRACK_ID) { 
        std::cerr << "########### INIT TRACK " << std::endl;
        tracklets[TRACK_ID] = spvision::Tracklet();
        tracklets[TRACK_ID].valid = false; 

        tracklets[TRACK_ID].id = TRACK_ID;
        tracklets[TRACK_ID].cluster_id = -1;

        tracklets[TRACK_ID].tracks = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
        tracklets[TRACK_ID].tracks->height = 1;

        tracklets[TRACK_ID].tracks_refined = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
        tracklets[TRACK_ID].tracks_refined->height = 1;

        tracklets[TRACK_ID].normals = pcl::PointCloud<pcl::Normal>::Ptr (new pcl::PointCloud<pcl::Normal>);
        tracklets[TRACK_ID].normals->height = 1;

        tracklets[TRACK_ID].tangents = pcl::PointCloud<pcl::Normal>::Ptr (new pcl::PointCloud<pcl::Normal>);
        tracklets[TRACK_ID].tangents->height = 1;

        tracklets[TRACK_ID].tracks_full = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
        tracklets[TRACK_ID].tracks_full->height = 1;

        tracklets[TRACK_ID].normals_full = pcl::PointCloud<pcl::Normal>::Ptr(new pcl::PointCloud<pcl::Normal>);
        tracklets[TRACK_ID].normals_full->height = 1;

        tracklets[TRACK_ID].latest_track_utime = 0; 
        tracklets[TRACK_ID].latest_track_sample_utime = 0; 
        tracklets[TRACK_ID].latest_track_processed_utime = 0; 
        return;
    }

    //----------------------------------
    // Main update function
    // - Ensure that the update times are newer than previous updates
    // - Check update times in cleanup
    // - Cleanup should be called before the update is called with the older timestamp
    //----------------------------------
    void TrackletManager::update(int64_t utime, const UniquePoseMap& tracks_msg) { 

        for (UniquePoseMapConstIt it = tracks_msg.begin(); it != tracks_msg.end(); it++) { 
           
            //----------------------------------
            // Track ID, Pose
            //----------------------------------
            int64_t TRACK_ID = it->first; 
            const bot_core_pose_t& pose_msg = it->second;

            //----------------------------------
            // Ensure only single track with unique ID
            //----------------------------------
            bool ID_FOUND = (tracklets.find(TRACK_ID) != tracklets.end());
            if (!ID_FOUND) init(TRACK_ID); 

            // ----------------------------------
            // Ensure that the utime is newer that latest track utime
            // ----------------------------------
            assert (utime > tracklets[TRACK_ID].latest_track_utime);

            //----------------------------------
            // Sampled track 
            //----------------------------------
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud = tracklets[TRACK_ID].tracks;

            //----------------------------------
            // Sampled track refined (tangent projected)
            //----------------------------------
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud_refined = tracklets[TRACK_ID].tracks_refined;

            //----------------------------------
            // Sampled track normals
            //----------------------------------
            pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_normals = tracklets[TRACK_ID].normals;
            pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_normals_full = tracklets[TRACK_ID].normals_full;
        
            //----------------------------------
            // Sampled track tangents
            //----------------------------------
            pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_tangents = tracklets[TRACK_ID].tangents;

            //----------------------------------
            // Full track (for tangent computation)
            //----------------------------------
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud_full = tracklets[TRACK_ID].tracks_full; 

            //-------------------------------------------------------------
            // Fill up point cloud (points, normals, tangent computation
            //-------------------------------------------------------------
            // utime: 
            // Define utime range as TIME_WINDOW_WIDTH (time window width)
            // For each track, find the last TIME_WINDOW_WIDTH samples, 
            // and sample TIME_SLICES times uniformly
            // 
            // pt, normals: 
            // populated upto TIME_SLICES size
            // 
            // pt_refined: 
            // projected on to tangent vector
            //-------------------------------------------------------------
            pcl::Normal normal_init; 
            normal_init.normal[0] = 0, normal_init.normal[1] = 0, normal_init.normal[2] = 1;

            std::deque<int64_t>& utimes = tracklets[TRACK_ID].utimes; 
            std::deque<int64_t>& utimes_slices = tracklets[TRACK_ID].utimes_slices; 

            //----------------------------------
            // First fill up all utimes 
            //----------------------------------
            // Populate XYZ
            pcl::PointXYZRGB pt; 
            pt.x = pose_msg.pos[0], pt.y = pose_msg.pos[1], pt.z = pose_msg.pos[2];

            // HACK??
            pcl::Normal normal; 
            normal.normal[0] = pose_msg.orientation[0], normal.normal[1] = pose_msg.orientation[1], 
                normal.normal[2] = pose_msg.orientation[2]; 

            // Populate track_cloud_full 
            track_cloud_full->points.push_back(pt);
            track_cloud_full->width = track_cloud_full->size();

            // Populate track_cloud_normals_full 
            track_cloud_normals_full->points.push_back(normal);
            track_cloud_normals_full->width = track_cloud_normals_full->size();

            // Fill up utimes
            utimes.push_back(utime); 

            // Check utimes and cloud size
            assert(track_cloud_full->points.size() == utimes.size()); 
            assert(track_cloud_normals_full->points.size() == utimes.size()); 

            // Update latest_track_utime
            tracklets[TRACK_ID].latest_track_utime = utime; 

            //----------------------------------
            // Fill up sample utimes
            //----------------------------------
            if (utime - tracklets[TRACK_ID].latest_track_sample_utime >= TIME_SLICE_RATE_SEC * 1e6) { 

                // Populate sampled track cloud 
                track_cloud->points.push_back(pt); 
                track_cloud->width = track_cloud->points.size();

                track_cloud_refined->points.push_back(pt); 
                track_cloud_refined->width = track_cloud_refined->points.size();

                track_cloud_normals->points.push_back(normal); 
                track_cloud_normals->width = track_cloud_normals->points.size();
            
                track_cloud_tangents->points.push_back(normal_init); 
                track_cloud_tangents->width = track_cloud->points.size();

                // Fill up sample utimes
                utimes_slices.push_back(utime); 
                
                // Check utimes and cloud size
                assert(track_cloud->points.size() == utimes_slices.size());
                assert(track_cloud_refined->points.size() == utimes_slices.size());
                assert(track_cloud_normals->points.size() == utimes_slices.size());
                assert(track_cloud_tangents->points.size() == utimes_slices.size());

                // Update latest_track_sample_utime
                tracklets[TRACK_ID].latest_track_sample_utime = utime; 
            }

            //----------------------------------
            // Ensure validity
            //----------------------------------
            tracklets[TRACK_ID].valid = (track_cloud->points.size() > MIN_FIT_LENGTH);
            // std::cerr << "TRACKLET VALID: " << TRACK_ID << " " << track_cloud->points.size() << std::endl;
            // std::cerr << "# TRACKS: " << track_cloud_full->points.size() << std::endl;
            // std::cerr << "# SLICES: " << utime_slices_inds.size() << std::endl;
        }
            return;
    }

    //--------------------------------------------
    // Only process frames that are newer
    // Ignore frames that are old
    // Track Clean up before update
    //--------------------------------------------
    bool TrackletManager::cleanup(int64_t utime, float MAX_TRACK_TIMESTAMP_DELAY_S) { 
        std::vector<int64_t> erase_ids; 
        
        //----------------------------------
        // If utime is older than latest of all utimes (full reset)
        //----------------------------------
        int64_t latest_of_all_utimes = 0;
        for (TrackletMapIt it = tracklets.begin(); it != tracklets.end(); it++) { 
            Tracklet& tracklet = it->second;
            int64_t latest_utime = tracklet.utimes.back(); 
            latest_of_all_utimes = std::max(latest_of_all_utimes, latest_utime);
        }
        if (utime < latest_of_all_utimes) 
            for (TrackletMapIt it = tracklets.begin(); it != tracklets.end(); it++) 
                erase_ids.push_back(it->first); 
        bool perform_reset = (tracklets.size() && erase_ids.size() == tracklets.size());
        if (perform_reset) { reset(); return true; }

        //----------------------------------
        // clean up tracks that are old
        //----------------------------------
        for (TrackletMapIt it = tracklets.begin(); 
             it != tracklets.end(); it++) { 

            int64_t TRACK_ID = it->first; 
            Tracklet& tracklet = it->second; 
            std::deque<int64_t>& utimes = tracklet.utimes; 
            std::deque<int64_t>& utimes_slices = tracklet.utimes_slices; 

            int64_t latest_utime = utimes.back(); 
            // if no updates for TIME_SLICES * TIME_SLICE_RATE_S * 2
            if (utime < latest_utime) erase_ids.push_back(TRACK_ID);
            if (utime - latest_utime >= MAX_TRACK_TIMESTAMP_DELAY_S * 1e6) erase_ids.push_back(TRACK_ID); 

        }
        for (int j=0; j<erase_ids.size(); j++) { 
            // std::cerr << "ERASING (RESET!) " << erase_ids[j] << std::endl;
            tracklets.erase(erase_ids[j]); 
        }
        if (erase_ids.size()) std::cerr << "TRACKS ERASED: " << erase_ids.size() << std::endl;
        return false; 
    }
    
    //--------------------------------------------
    // Prune so that length is only GPFT_MAX_TRACK_TIMESPAN seconds long
    //--------------------------------------------
    void TrackletManager::prune(float TIMESPAN_S) { 
        //----------------------------------
        // clean up tracks that are old
        //----------------------------------
        for (TrackletMapIt it = tracklets.begin(); 
             it != tracklets.end(); it++) { 
            
            int64_t TRACK_ID = it->first; 
            Tracklet& tracklet = it->second; 
            std::deque<int64_t>& utimes = tracklet.utimes; 
            std::deque<int64_t>& utimes_slices = tracklet.utimes_slices; 

            int64_t latest_utime = utimes.back(); 
            int64_t first_utime = utimes.front(); 
            
            //----------------------------------
            // Sampled track , Sampled track refined (tangent projected), 
            // Sampled track normals, Sampled track tangents, 
            // Full track (for tangent computation), 
            //----------------------------------
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud = tracklet.tracks;
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud_refined = tracklet.tracks_refined;
            pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_normals = tracklet.normals;
            pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_normals_full = tracklet.normals_full;
            pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_tangents = tracklet.tangents;
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud_full = tracklet.tracks_full; 
            
            int size_full_bef = track_cloud_full->points.size(); 
            int size_sample_bef = track_cloud->points.size(); 
            
            // if latest track utime lags 
            int64_t utime_th = latest_utime - TIMESPAN_S * 1e6; 

            //----------------------------------
            // Ensure the threshold is met
            //----------------------------------
            while (utimes.size() && (utimes.front() < utime_th)) { 
                utimes.pop_front(); 
                track_cloud_full->points.erase(track_cloud_full->points.begin(), track_cloud_full->points.begin() + 1); 
                track_cloud_normals_full->points.erase(track_cloud_normals_full->points.begin(), 
                                                       track_cloud_normals_full->points.begin() + 1); 
                assert(utimes.size() == track_cloud_full->points.size()); 
                assert(utimes.size() == track_cloud_normals_full->points.size()); 
            }
            while (utimes_slices.size() && (utimes_slices.front() < utime_th)) { 
                utimes_slices.pop_front(); 
                track_cloud->points.erase(track_cloud->points.begin(), track_cloud->points.begin() + 1);                 
                track_cloud_normals->points.erase(track_cloud_normals->points.begin(), track_cloud_normals->points.begin() + 1); 
                track_cloud_tangents->points.erase(track_cloud_tangents->points.begin(), track_cloud_tangents->points.begin() + 1); 
                track_cloud_refined->points.erase(track_cloud_refined->points.begin(), track_cloud_refined->points.begin() + 1); 
                assert(utimes_slices.size() == track_cloud->points.size()); 
            }

            //----------------------------------
            // Remove excessive samples
            //----------------------------------
            if (utimes_slices.size() > TIME_SLICES) { 
                int64_t utime_erase = utimes_slices[utimes_slices.size()-TIME_SLICES]; 
                while (utimes.size() && (utimes.front() < utime_erase)) { 
                    utimes.pop_front(); 
                    track_cloud_full->points.erase(track_cloud_full->points.begin(), track_cloud_full->points.begin() + 1); 
                    track_cloud_normals_full->points.erase(track_cloud_normals_full->points.begin(), 
                                                           track_cloud_normals_full->points.begin() + 1); 
                    assert(utimes.size() == track_cloud_full->points.size()); 
                    assert(utimes.size() == track_cloud_normals_full->points.size()); 
                }
                while (utimes_slices.size() && (utimes_slices.front() < utime_erase)) { 
                    utimes_slices.pop_front(); 
                    track_cloud->points.erase(track_cloud->points.begin(), track_cloud->points.begin() + 1);                 
                    track_cloud_normals->points.erase(track_cloud_normals->points.begin(), 
                                                      track_cloud_normals->points.begin() + 1); 
                    track_cloud_tangents->points.erase(track_cloud_tangents->points.begin(), 
                                                       track_cloud_tangents->points.begin() + 1); 
                    track_cloud_refined->points.erase(track_cloud_refined->points.begin(), 
                                                      track_cloud_refined->points.begin() + 1); 

                    assert(utimes_slices.size() == track_cloud->points.size()); 
                    assert(utimes_slices.size() == track_cloud_normals->points.size()); 
                    assert(utimes_slices.size() == track_cloud_tangents->points.size()); 
                    assert(utimes_slices.size() == track_cloud_refined->points.size()); 
                }
            }
            // std::cerr << "LAST UTIME: " << track_list_pcl[TRACK_ID].latest_track_utime << std::endl;
            // std::cerr << "LAST SAMPLE UTIME: " << track_list_pcl[TRACK_ID].latest_track_sample_utime << std::endl;
            // std::cerr << "UTIMES: ";
            // for (int k=0; k<utimes.size(); k++) std::cerr << utimes[k] << " "; 
            // std::cerr << std::endl;
            // std::cerr << "UTIMES SLICES: ";
            // for (int k=0; k<utimes.size()-1; k++) std::cerr << utimes[k+1]-utimes[k] << " "; 
            // std::cerr << std::endl;

            int size_full_aft = track_cloud_full->points.size(); 
            int size_sample_aft = track_cloud->points.size(); 

            // ensure that it is only of size TIME_SLICES

            // std::cerr << "PRUNING: " << TRACK_ID << " (" << size_full_bef << " -> " << size_full_aft << ") ("
            //           << size_sample_bef << " -> " << size_sample_aft << ") "
            //           << std::endl;

        }
        return;
    }

    void TrackletManager::reset() { 
        tracklets.clear();
    }

    void TrackletManager::recomputeUtimeMap() { 
    for (TrackletMapIt it = tracklets.begin(); 
         it != tracklets.end(); it++) { 

        int64_t TRACK_ID = it->first; 
        Tracklet& tracklet = it->second; 
        std::deque<int64_t>& utimes = tracklet.utimes; 
        std::deque<int64_t>& utimes_slices = tracklet.utimes_slices; 

        //----------------------------------
        // Create utimes map
        //----------------------------------
        std::map<int64_t, int>& utimes_map = it->second.utimes_map; 
        utimes_map.clear(); 
        for (int k=0; k<utimes.size(); k++) utimes_map[utimes[k]] = k;
        
        std::map<int64_t, int>& utimes_slices_map = it->second.utimes_slices_map; 
        utimes_slices_map.clear(); 
        for (int k=0; k<utimes_slices.size(); k++) utimes_slices_map[utimes_slices[k]] = k;
         
        // //----------------------------------
        // // Keep utime ref (for canonical pose ref)
        // //----------------------------------
        // it->second.utime_ref = utimes_slices.front(); 
    }
    return;
    }
}
