#include "MRPTTracker.hpp"

using namespace spvision;
MRPTTracker::MRPTTracker() : VERBOSE_MODE(false), _inited(false) { 
    std::cerr << "INITIALIZING MRPTTRACKER: " << std::endl;
    init();
}

MRPTTracker::MRPTTracker(const MRPTTracker& tr) { 
}

MRPTTracker::~MRPTTracker() { 
}

void
MRPTTracker::setParams(char* str, double value) { 
    
}


void 
MRPTTracker::initTracker() { 
    // "CFeatureTracker_KL" is by far the most robust implementation for now:
    tracker = CGenericFeatureTrackerAutoPtr( new CFeatureTracker_KL );
    // tracker = CGenericFeatureTrackerAutoPtr( new CFeatureTracker_FAST );
    // tracker = CGenericFeatureTrackerAutoPtr( new CFeatureTracker_PatchMatch );
    // tracker = new CFeatureTracker_PatchMatch();

}

void 
MRPTTracker::setDefaultParams() { 
    // Set of parameters common to any tracker implementation:
    // -------------------------------------------------------------
    // To see all the existing params and documentation, see mrpt::vision::CGenericFeatureTracker
    tracker->extra_params["remove_lost_features"]         = 1;   // automatically remove out-of-image and badly tracked features

    tracker->extra_params["add_new_features"]             = 1;   // track, AND ALSO, add new features
    tracker->extra_params["add_new_feat_min_separation"]  = 5;
    tracker->extra_params["minimum_KLT_response_to_add"]  = 3;
    tracker->extra_params["add_new_feat_max_features"]    = 3000;
    tracker->extra_params["add_new_feat_patch_size"]      = 5;

    tracker->extra_params["update_patches_every"]		= 5;  // Don't update patches.

    tracker->extra_params["check_KLT_response_every"]	= 5;	// Re-check the KLT-response to assure features are in good points.
    tracker->extra_params["minimum_KLT_response"]	    = 5;

    // Specific params for "CFeatureTracker_KL"
    // ------------------------------------------------------
    tracker->extra_params["window_width"]  = 15;
    tracker->extra_params["window_height"] = 15;
    tracker->extra_params["LK_levels"] = 3;
    //tracker->extra_params["LK_max_iters"] = 10;
    //tracker->extra_params["LK_epsilon"] = 0.1;
    //tracker->extra_params["LK_max_tracking_error"] = 150;

    return; 
}

void
MRPTTracker::reinit() { 
    pcimg = CImage();
    pimg = cv::Mat();

    _inited = false; 
    
    simple_feats = TSimpleFeatureList();
    tracklet_manager.reset();
}

void
MRPTTracker::init() { 
    
    initTracker(); 
    setDefaultParams(); 

    tracklet_manager.reset();
    _inited = true; 
}

// template <typename T>
void
MRPTTracker::update(int64_t utime, cv::Mat& img, 
                    pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, 
                    pcl::PointCloud<pcl::Normal>::Ptr& normals,
                    pcl::search::KdTree<pcl::PointXYZRGB>::Ptr& tree) { 

    double tic;
    std::cerr << "MRPT update: " << std::endl;
    if (img.empty()) return; 

    //--------------------------------------------
    // Only process frames that are newer
    // Ignore frames that are old
    // Track Clean up before update
    //--------------------------------------------
    tic = bot_timestamp_now();
    if (tracklet_manager.cleanup(utime, MRPT_MAX_TRACK_TIMESTAMP_DELAY)) reinit();
    printf("%s===> TRACK CLEANUP: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 


    //--------------------------------------------
    // Prune so that length is only GPFT_MAX_TRACK_TIMESPAN seconds long
    //--------------------------------------------
    tic = bot_timestamp_now();
    tracklet_manager.prune(MRPT_MAX_TRACK_TIMESPAN);
    printf("%s===> TRACK CLEANUP: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 


    //--------------------------------------------
    // Gaussian blur
    //--------------------------------------------
    tic = bot_timestamp_now();
    cv::Mat gray; 
    // cv::medianBlur(img, img, 5);
    cv::GaussianBlur(img, img, cv::Size(3,3), 0, 0, cv::BORDER_DEFAULT);
    cv::cvtColor(img, gray, CV_BGR2GRAY);
    printf("%s===> GAUSSIAN BLUR: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    //--------------------------------------------
    // Prepare SuperFeatures from existing tracklets
    //--------------------------------------------
    std::vector<SuperFeature> p_sfpts; 
    std::vector<cv::Point2f> p_pts;
    tracklet_manager.getLatestFeatures(p_sfpts, p_pts);
    printf("Existing Features: %i\n", p_sfpts.size()); 

    //--------------------------------------------
    // Detect Keypoints
    //--------------------------------------------
    tic = bot_timestamp_now();
    CImage cimg; 
    convert_to_cimg(img, cimg);
    // Previous and Current image available
    if (!pimg.empty() && !img.empty())
        // This single call makes: detection, tracking, recalculation of KLT_response, etc.
        tracker->trackFeatures(pcimg, cimg, simple_feats);
    // Convert to Features2D/Features3D
    std::list<SuperFeature> c_sfpts = convertToFeatures(utime, simple_feats, cloud, normals, tree); 
    printf("Updated  Features: %i\n", c_sfpts.size()); 
    printf("%s===> KEYPOINT DETECTION AND TRACKING: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    //--------------------------------------------
    // Add new features
    //--------------------------------------------
    tic = bot_timestamp_now();
    tracklet_manager.addSuperFeatures(utime, c_sfpts, MRPT_ADD_3D_POINT_DISTANCE_THRESHOLD);
    printf("%s===> ADD SUPER FEATURES: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    //--------------------------------------------
    // Plot features
    //--------------------------------------------
    tic = bot_timestamp_now();
    // tracklet_manager.plot(utime, img);
    printf("%s===> PLOT FEATURES: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    //--------------------------------------------
    // Store frame, utime management
    //--------------------------------------------
    pcimg = cimg; 
    pimg = img.clone();

    return; 
}

void 
MRPTTracker::normalEstimation(std::vector<SuperFeature>& feats, 
                              const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, 
                              const std::vector<int>& _inds) { 

    // Normal estimation
    pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> ne;
    // ne.setNumberOfThreads(2);
    ne.setInputCloud(cloud);
    
    boost::shared_ptr<vector<int> > inds (new vector<int> ());
    inds->insert(inds->begin(), _inds.begin(), _inds.end());
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
    pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree_n (new pcl::search::KdTree<pcl::PointXYZRGB>());
    ne.setSearchMethod(tree_n);
    ne.setRadiusSearch(0.2);
    // ne.setKSearch(3);
    ne.setIndices(inds); 
    ne.compute(*cloud_normals);

    assert(cloud_normals->points.size() == feats.size());
    for (int j=0; j<feats.size(); j++) { 
        feats[j].normal = cv::Vec3f(cloud_normals->points[j].normal[0],
                                cloud_normals->points[j].normal[1],
                                cloud_normals->points[j].normal[2]);
        // if (j < 10) 
        //     std::cerr << feats[j].normal << std::endl;
    }
    std::cout << "Estimated the normals: " << cloud_normals->points.size() << std::endl;

    return;
}


std::list<SuperFeature>
MRPTTracker::convertToFeatures(int64_t utime, const TSimpleFeatureList& _feats, 
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, 
                               pcl::PointCloud<pcl::Normal>::Ptr& normals, 
                               pcl::search::KdTree<pcl::PointXYZRGB>::Ptr& tree) { 

    std::list<SuperFeature> feats; 
    if (!_feats.size()) return feats; 

    if (!tree) 
        std::cerr << "kD-TREE NOT INITIALIZED" << std::endl;

    // // Estimate normals from the features
    // // Normal estimation
    // pcl::PointCloud<pcl::PointXYZ>::Ptr feat_cloud(new pcl::PointCloud<pcl::PointXYZ> ());
    // pcl::PointCloud<pcl::Normal>::Ptr feat_normals (new pcl::PointCloud<pcl::Normal> ());
    // pcl::search::KdTree<pcl::PointXYZ>::Ptr feat_tree(new pcl::search::KdTree<pcl::PointXYZ>());
    // pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
    // feat_cloud->width    = _feats.size();
    // feat_cloud->height   = 1;
    // feat_cloud->is_dense = false;
    // feat_cloud->points.resize (_feats.size());

    // // Search 
    // boost::shared_ptr<vector<int> > search_inds (new vector<int> ()); // cloud_decimated->points.size()));
    // for (int j=0; j<_feats.size(); j++) { 
    //     int idx = int(_feats[j].pt.y)*cloud->width + int(_feats[j].pt.x);
    //     if (idx < 0 || idx >= cloud->points.size()) { 
    //         continue;
    //     }
    //     pcl::PointXYZRGB pt = cloud->points[idx];
    //     if (pt.z < 0) continue;
    //     search_inds->push_back(j);
    //     feat_cloud->points[j].x = pt.x, feat_cloud->points[j].y = pt.y, feat_cloud->points[j].z = pt.z; 
    // }
    
    // // Set input cloud
    // ne.setInputCloud(feat_cloud);
    // ne.setIndices(search_inds);
    // ne.setSearchMethod(feat_tree);
    // ne.setKSearch(9);
    // ne.compute(*feat_normals);

    // std::cerr << "_FEATS: " << _feats.size() << " " 
    //           << feat_cloud->points.size() << " " << feat_normals->points.size() << std::endl;
    std::vector<int> inds(_feats.size()); 
    for (int j=0; j<_feats.size(); j++) { 
        SuperFeature feat; 
        int idx = int(_feats[j].pt.y)*cloud->width + int(_feats[j].pt.x);
        if (idx < 0 || idx >= cloud->points.size()) { 
            continue;
        }
        inds[feats.size()] = idx;

        pcl::PointXYZRGB pt = cloud->points[idx];
        if (pt.z != pt.z) continue; // Check if disparity is NaN
        
        // Search for the feature in the decimated point cloud, and retrieve its normal
        std::vector<int> kinds; 
        std::vector<float> kdists; 
        tree->nearestKSearch(pt, 1, kinds, kdists); 

        feat.utime = utime;
        feat.id = _feats[j].ID;
        feat.response = _feats[j].response;
        feat.point2D.x = _feats[j].pt.x;
        feat.point2D.y = _feats[j].pt.y;        
        feat.point3D.x = pt.x, feat.point3D.y = pt.y, feat.point3D.z = pt.z;
        
        // feat.normal = cv::Vec3f(feat_normals->points[j].normal[0], 
        //                         feat_normals->points[j].normal[1], 
        //                         feat_normals->points[j].normal[2]);
        if (kinds.size() && kinds[0] >= 0 && kinds[0] < normals->points.size()) { 
            feat.normal = cv::Vec3f(normals->points[kinds[0]].normal[0], 
                                    normals->points[kinds[0]].normal[1], 
                                    normals->points[kinds[0]].normal[2]);
        } else { 
            std::cerr << "kinds: " << kinds[0] << std::endl;
            feat.normal = cv::Vec3f(0,0,1); // no normal computation
        }
        feats.push_back(feat); 
    }
    std::cerr << "cloud_decimated: " << normals->points.size() << std::endl;
    inds.resize(feats.size());

    std::cerr << "Normal FEATS: " << inds.size() << std::endl;
    // normalEstimation(feats, cloud, inds);

    return feats; 
}
