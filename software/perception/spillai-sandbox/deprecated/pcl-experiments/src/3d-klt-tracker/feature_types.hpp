#ifndef SPVISION_FEATURE_TYPES_HPP_
#define SPVISION_FEATURE_TYPES_HPP_

// Standard includes
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <deque>
#include <map>
#include <set> 
#include <numeric>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/glext.h>

#include <lcm/lcm.h>
#include <lcmtypes/bot2_param.h>
#include <bot_core/bot_core.h>

#include <lcmtypes/articulation_object_pose_msg_t.h>
#include <lcmtypes/articulation_object_pose_list_msg_t.h>
#include <lcmtypes/articulation_object_pose_track_msg_t.h>
#include <lcmtypes/articulation_object_pose_track_list_msg_t.h>
#include <lcmtypes/articulation_articulated_object_msg_t.h>

#include <lcmtypes/vs_obj_collection_t.h>
#include <lcmtypes/vs_obj_t.h>
#include <lcmtypes/vs_link_collection_t.h>
#include <lcmtypes/vs_link_t.h>
#include <lcmtypes/vs_cov_collection_t.h>
#include <lcmtypes/vs_cov_t.h>
#include <lcmtypes/vs_point3d_list_collection_t.h>
#include <lcmtypes/vs_point3d_list_t.h>
#include <lcmtypes/vs_reset_collections_t.h>
#include <lcmtypes/vs_collection_config_t.h>
#include <lcmtypes/vs_text_collection_t.h>
#include <lcmtypes/vs_text_t.h>

// pcl includes
#include <pcl/pcl_macros.h>
#include <pcl/io/pcd_io.h>
#include <pcl/PointIndices.h>
#include <pcl/registration/icp.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>

#include <pcl/filters/extract_indices.h>
#include <pcl/filters/random_sample.h>

#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>

#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>

#include <pcl/features/normal_3d.h>
#include <pcl/features/3dsc.h>
#include <pcl/features/impl/3dsc.hpp>
#include <pcl/features/usc.h>
#include <pcl/features/pfh.h>
#include <pcl/features/moment_invariants.h>

#include <pcl/registration/transformation_estimation.h>
#include <pcl/registration/transformation_estimation_svd.h>
#include <pcl/registration/transformation_estimation_lm.h>

// opencv includes
#include <opencv2/opencv.hpp>

// 

/* #include <perception_matlab_utils/matlab_engine_utils.hpp> */
/* #include <perception_matlab_utils/matlab_opencv_utils.hpp> */

static char* esc_red = "\033[0;31m";
static char* esc_green = "\033[0;32m";
static char* esc_brown = "\033[0;33m";
static char* esc_blue = "\033[0;34m";
static char* esc_magenta = "\033[0;35m";
static char* esc_cyan = "\033[0;36m";
static char* esc_lightgray = "\033[0;37m";
static char* esc_yellow = "\033[1;33m";
static char* esc_def = "\033\[0m";



namespace spvision { 

    struct Feature { 
        int64_t id; 
        int64_t utime; 
        Feature () : id(-1), utime(0) {}
        Feature (int64_t _utime, int64_t _id) : utime(_utime), id(_id) {}
        virtual ~Feature () {}
    };

    struct Feature2D : public Feature { 
        float response; 
        cv::Point2f point; 
        Feature2D () : Feature(), response(0.f), point(cv::Point2f(0,0)) { }
        Feature2D (int64_t _utime, int64_t _id, const cv::Point2f& _point) : Feature(_utime, _id) { 
            point = _point;
            response = 0.f;
        }
        Feature2D (int64_t _utime, int64_t _id, const cv::Point2f& _point, 
                   float _response) : Feature(_utime, _id) { 
            point = _point;
            response = _response; 
        }
        virtual ~Feature2D () {}
    };
    
    struct Feature3D : public Feature { 
        cv::Point3f point; 
        cv::Vec3f normal;
        Feature3D () : Feature(), point(cv::Point3f(0,0,0)), normal(cv::Vec3f(0,0,1)) { }
        Feature3D (int64_t _utime, int64_t _id, 
                   const cv::Point3f& _point, const cv::Vec3f& _normal) : Feature(_utime, _id) { 
            point = _point; 
            normal = _normal;
        }
        ~Feature3D () {}
    };

    struct SuperFeature : public Feature { 
        bool normal_valid;
        float response, angle;
        cv::Mat description;
        cv::Point2f point2D; // change to keypoint
        cv::Point3f point3D; 
        cv::Vec3f normal;
        SuperFeature () : Feature(), response(0.f), angle(0.f), point2D(cv::Point2f(0,0)), 
            point3D(cv::Point3f(0,0,0)), normal(cv::Vec3f(0,0,1)), normal_valid(false) { }
        SuperFeature (int64_t _utime, int64_t _id, 
                      float _response, 
                      float _angle, 
                      const cv::Point2f& _point2D, 
                      const cv::Point3f& _point3D, 
                      const cv::Vec3f& _normal, 
                      const cv::Mat& _description) : Feature(_utime, _id) { 
            response = _response;
            angle = _angle;
            point2D = _point2D; 
            point3D = _point3D; 
            normal = _normal;
            description = _description;
        }
        virtual ~SuperFeature () {}
    };

    struct Track2D : public std::vector<Feature2D> { 
        bool valid;
        Track2D () : std::vector<Feature2D>() {
            valid = true;
        }
    };

    struct Track3D : public std::vector<Feature3D> { 
        bool valid;
        Track3D () : std::vector<Feature3D>() {
            valid = true;
        }
    };

    struct Track : public std::vector<SuperFeature> { 
        bool valid;
        Track () : std::vector<SuperFeature>() {
            valid = true;
        }
    };

    typedef std::map<int64_t, Track2D> Track2DMap; 
    typedef std::map<int64_t, Track2D>::iterator Track2DMapIt; 

    typedef std::map<int64_t, Track3D> Track3DMap; 
    typedef std::map<int64_t, Track3D>::iterator Track3DMapIt; 

    typedef std::map<int64_t, Track> TrackMap; 
    typedef std::map<int64_t, Track>::iterator TrackMapIt; 


    // Point cloud track specifics
    struct Tracklet { 
        bool valid; 
        int id, cluster_id;
        std::vector<float> desc;
        std::vector<float> desc2; 

        std::deque<int64_t> utimes; 
        std::map<int64_t, int> utimes_map; 
        std::deque<int64_t> utimes_slices; 
        std::map<int64_t, int> utimes_slices_map;

        pcl::PointCloud<pcl::PointXYZRGB>::Ptr tracks; // euclidean position
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr tracks_full; // euclidean position full track
        int64_t latest_track_utime; 

        pcl::PointCloud<pcl::Normal>::Ptr normals_full; // full normal of the depth cloud
        pcl::PointCloud<pcl::Normal>::Ptr normals; // normal of the depth cloud

        // mean position obtained from projecting last MIN_FIT_LENGTH
        // points on to the plane whose normal is the tangent vector
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr tracks_refined; // projected mean position 
        pcl::PointCloud<pcl::Normal>::Ptr tangents; // tangent vector of the motion
        int64_t latest_track_sample_utime; 
        int64_t latest_track_processed_utime; 

        // pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree;

        Tracklet () : id(-1), cluster_id(-1), valid(false), 
            latest_track_utime(0), latest_track_sample_utime(0) {} // starts off with red
    };
    typedef std::vector<Tracklet> TrackletList;
    typedef std::map<int64_t, Tracklet> TrackletMap;
    typedef std::map<int64_t, Tracklet>::iterator TrackletMapIt;

    typedef std::map<int64_t, bot_core_pose_t> UniquePoseMap;
    typedef std::map<int64_t, bot_core_pose_t>::iterator UniquePoseMapIt;
    typedef std::map<int64_t, bot_core_pose_t>::const_iterator UniquePoseMapConstIt;

    typedef std::pair<int64_t, int64_t> id_pair;

    typedef std::map<int64_t, std::set<id_pair> > PosePairMap;
    typedef std::map<int64_t, std::set<id_pair> >::iterator PosePairMapIt;
    

static inline std::pair<int, int> make_sorted_pair(int a, int b) { 
    assert(a!=b);
    if (a < b)
        return std::make_pair(a, b);
    else
        return std::make_pair(b, a);
}

}
#endif /*SPVISION_FEATURE_TYPES_HPP_*/
