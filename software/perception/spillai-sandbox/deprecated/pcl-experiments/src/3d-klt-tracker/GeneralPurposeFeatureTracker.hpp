// 
// Author: Sudeep Pillai (spillai@csail.mit.edu) Mar 16, 2013
// 

#ifndef GENERAL_PURPOSE_FEATURE_TRACKER_HPP_
#define GENERAL_PURPOSE_FEATURE_TRACKER_HPP_

// Standard includes
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <deque>
#include <map>
#include <set> 

// pcl includes
#include <pcl/io/pcd_io.h>
#include <pcl/PointIndices.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/random_sample.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/integral_image_normal.h>

// libbot/lcm includes
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>

// Feature includes
#include "feature_types.hpp"
#include "subdivision2d.hpp"

#define DEBUG_CAPS 1

namespace spvision { 

    const int GPFT_MAX_TRACK_TIMESTAMP_DELAY = 1.f; // in seconds
    const float GPFT_MIN_FLOW_MATCH_DISTANCE = 3.f;
    const float GPFT_MIN_DESC_MATCH_DISTANCE = 100.f;
    const float GPFT_MAX_TRACK_TIMESPAN = 0.5f;
    const float GPFT_ADD_POINT_DISTANCE_THRESHOLD = 10.f;
    const float GPFT_ADD_POINT_DISTANCE_THRESHOLD_SQ = std::pow(GPFT_ADD_POINT_DISTANCE_THRESHOLD,2);
    const float GPFT_POINT_SPACING_THRESHOLD = 20.f;

    struct node_info { 
        int idx;
        float dist_normal;
        float dist_euclidean;
    };

    class GeneralPurposeFeatureTracker { 
        enum TrackingMode { TRACK2D, TRACK3D, TRACK3D_WITH_NORMALS };
    private: 
        TrackingMode mode;
    public:

        //--------------------------------------------
        // Constants/Variables
        //--------------------------------------------
        bool enable_subpixel_refinement;

        //--------------------------------------------
        // Feature Extractor, Descriptor, and Matcher
        //--------------------------------------------
        // cv::Ptr<cv::FeatureDetector> detector;
        cv::Ptr<cv::PyramidAdaptedFeatureDetector> detector;
        cv::Ptr<cv::DescriptorExtractor> extractor; 
        cv::Ptr<cv::DescriptorMatcher> matcher; 
        cv::Ptr<cv::BOWTrainer> vocab;

        //--------------------------------------------
        // Tracklet manager
        //--------------------------------------------
        TrackManager tracklet_manager;

        //--------------------------------------------
        // Image history for optical flow
        //--------------------------------------------
        cv::Mat pimg;

    public:
        GeneralPurposeFeatureTracker ();
        ~GeneralPurposeFeatureTracker ();

        void setTrackingMode (const TrackingMode& _mode);
        void update(int64_t utime, const cv::Mat& img);
        void update(int64_t utime, const cv::Mat& img, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud,
                    pcl::PointCloud<pcl::Normal>::Ptr& normals,
                    pcl::search::KdTree<pcl::PointXYZRGB>::Ptr& tree, float DEPTH_SCALE);

        // void addFeatures(const std::vector<cv::KeyPoint>& kpts, 
        //                  const cv::Mat& desc, 
        //                  const std::vector<int>& ids = std::vector<int>());
        bool ready();
        void reset();
    };

}

#endif // GENERAL_PURPOSE_FEATURE_TRACKER_HPP_



    //--------------------------------------------
    // Feature Detector
    // virtual void detectImpl( const Mat& image, vector<KeyPoint>& keypoints, const Mat& mask=Mat() ) const;
    //--------------------------------------------
    // // FAST
    // int fast_threshold = 10; bool nonmax = true; 
    // obj_matcher.detector = new FastFeatureDetector( fast_threshold, nonmax );

    // // GFTT
    // int gftt_maxCorners=1000; double gftt_qualityLevel=0.01; double gftt_minDistance=1;
    // int gftt_blockSize=3; bool gftt_useHarrisDetector=false; double gftt_k=0.04 ;
    // obj_matcher.detector = new GFTTDetector( gftt_maxCorners, gftt_qualityLevel, gftt_minDistance,
    //                                          gftt_blockSize, gftt_useHarrisDetector, gftt_k);

    // // BRISK
    // int brisk_thresh=10; int brisk_octaves=3; float brisk_patternScale=1.0f;
    // obj_matcher.detector = new BRISK(brisk_thresh, brisk_octaves, brisk_patternScale);

    // // ORB
    // int orb_nfeatures = 500; float orb_scaleFactor = 1.2f; int orb_nlevels = 8; int orb_edgeThreshold = 31;
    //     int orb_firstLevel = 0; int orb_WTA_K=2; int orb_scoreType=ORB::HARRIS_SCORE; int orb_patchSize=31;
    // obj_matcher.detector = new ORB(orb_nfeatures, orb_scaleFactor, orb_nlevels, orb_edgeThreshold, 
    //     orb_firstLevel, orb_WTA_K, orb_scoreType, orb_patchSize );

    // // FREAK
    // bool freak_orientationNormalized = true;
    // bool freak_scaleNormalized = true;
    // float freak_patternScale = 22.0f;
    // int freak_nOctaves = 4;
    // std::vector<int> freak_selectedPairs = std::vector<int>();
    // obj_matcher.detector = new FREAK( freak_orientationNormalized,
    //                                   freak_scaleNormalized,
    //                                   freak_patternScale,
    //                                   freak_nOctaves,
    //                                   freak_selectedPairs);

    // // MSER
    // int mser_delta=5; int mser_min_area=60; int mser_max_area=14400;
    // double mser_max_variation=0.25; double mser_min_diversity=.2;
    // int mser_max_evolution=200; double mser_area_threshold=1.01;
    // double mser_min_margin=0.003; int mser_edge_blur_size=5;
    // obj_matcher.detector = new MSER( mser_delta, mser_min_area, mser_max_area,
    //                                  mser_max_variation, mser_min_diversity,
    //                                  mser_max_evolution, mser_area_threshold,
    //                                  mser_min_margin, mser_edge_blur_size );

    // // Grid-adapted
    // int maxTotalKeypoints=1000, gridRows=4, gridCols=4;
    // obj_detector = GridAdaptedFeatureDetector( detector, maxTotalKeypoints, gridRows, gridCols );

    // // Pyramid-adapted
    // int maxLevel=2;
    // PyramidAdaptedFeatureDetector( detector, maxLevel );

    //--------------------------------------------
    // Feature Extractor
    // virtual void computeImpl( const Mat& image, vector<KeyPoint>& keypoints, Mat& descriptors ) const;
    //--------------------------------------------

    // // OpponentColorDescriptorExtractor
    // obj_matcher.extractor = new OpponentColorDescriptorExtractor();

    // // BRIEF
    // int brief_bytes = 32;
    // obj_matcher.extractor = BriefDescriptorExtractor( brief_bytes );

    //--------------------------------------------
    // Descriptor Matcher
    // // Find one best match for each query descriptor (if mask is empty).
    // CV_WRAP void match( const Mat& queryDescriptors, const Mat& trainDescriptors,
    //             CV_OUT vector<DMatch>& matches, const Mat& mask=Mat() ) const;
    // // Find k best matches for each query descriptor (in increasing order of distances).
    // // compactResult is used when mask is not empty. If compactResult is false matches
    // // vector will have the same size as queryDescriptors rows. If compactResult is true
    // // matches vector will not contain matches for fully masked out query descriptors.
    // CV_WRAP void knnMatch( const Mat& queryDescriptors, const Mat& trainDescriptors,
    //                CV_OUT vector<vector<DMatch> >& matches, int k,
    //                const Mat& mask=Mat(), bool compactResult=false ) const;
    // // Find best matches for each query descriptor which have distance less than
    // // maxDistance (in increasing order of distances).
    // void radiusMatch( const Mat& queryDescriptors, const Mat& trainDescriptors,
    //                   vector<vector<DMatch> >& matches, float maxDistance,
    //                   const Mat& mask=Mat(), bool compactResult=false ) const;
    //--------------------------------------------
    // // Brute-force matcher
    // int bfmatcher_normType, bool bfmatcher_crossCheck=false
    // obj_matcher.matcher = new BFMatcher( bfmatcher_normType, bfmatcher_crossCheck );

    // // Flann Matcher
    // cv::Ptr<flann::IndexParams> flann_indexParams=new flann::KDTreeIndexParams();
    // cv::Ptr<flann::SearchParams> flann_searchParams=new flann::SearchParams();
    // obj_matcher.matcher = new FlannBasedMatcher( flann_indexParams, flann_searchParams);

    // //--------------------------------------------
    // // Vocab builder
    // virtual Mat cluster() const;
    // virtual Mat cluster( const Mat& descriptors ) const;
    // //--------------------------------------------
    // int bow_clusterCount; TermCriteria bow_termcrit=TermCriteria();
    // int bow_attempts=3; int bow_flags=KMEANS_PP_CENTERS;
    // BOWKMeansTrainer( bow_clusterCount, bow_termcrit, bow_attempts, bow_flags );
    
    
    // BOWImgDescriptorExtractor bow_desc_extractor(extractor, matcher); 
    // bow_desc_extractor.compute(img, kpts, 
    
