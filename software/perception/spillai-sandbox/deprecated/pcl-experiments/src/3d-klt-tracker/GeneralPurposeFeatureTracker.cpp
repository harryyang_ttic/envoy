#include "TrackletManager.hpp"
#include "GeneralPurposeFeatureTracker.hpp"

// opencv-utils includes
#include <perception_opencv_utils/calib_utils.hpp>
#include <perception_opencv_utils/imshow_utils.hpp>
#include <perception_opencv_utils/math_utils.hpp>
#include <perception_opencv_utils/plot_utils.hpp>
#include <perception_opencv_utils/color_utils.hpp>
#include <perception_opencv_utils/imgproc_utils.hpp>
#include <opencv2/nonfree/nonfree.hpp>

namespace spvision { 


    static void sparseMedianFlow(cv::flann::Index& pindex, 
                                 cv::vector<cv::Point2f>& ppts_in, 
                                 cv::vector<cv::Point2f>& cpts_in, 
                                 cv::vector<cv::Point2f>& cpts_out, 
                                 cv::vector<bool>& status) { 

        assert(ppts_in.size() == cpts_in.size());
        status = std::vector<bool>(ppts_in.size(), false);
        cpts_out = std::vector<cv::Point2f>(ppts_in.size());        
        const int knn = 5; 

        for (int j=0; j<ppts_in.size(); j++) { 
            cv::Mat cpts_mat = cv::Mat(cpts_in[j]).reshape(1).t();
            cv::Mat_<int> knn_inds(1,knn); knn_inds = -1;
            cv::Mat_<float> knn_dists;

            pindex.knnSearch(cpts_mat, knn_inds, knn_dists, knn, cv::flann::SearchParams());
            
            std::vector<float> flowx, flowy; 
            for (int k=0; k<knn; k++) {
                int idx = knn_inds.at<int>(k);
                if (idx < 0) break;
                if (knn_dists.at<float>(k) > 16 * GPFT_ADD_POINT_DISTANCE_THRESHOLD_SQ) break;
                // std::cerr << "c: " << cpts_in[idx] << "=>" 
                //           << "pt: " << ppts_in[idx] << "= " 
                //           << "kd: " << knn_dists.at<float>(k) << " " << GPFT_ADD_POINT_DISTANCE_THRESHOLD_SQ << std::endl;
                cv::Point2f vec = cpts_in[idx] - ppts_in[idx];
                if (cv::norm(vec) > 4 * GPFT_ADD_POINT_DISTANCE_THRESHOLD) continue;
                flowx.push_back(vec.x);
                flowy.push_back(vec.y);
            }

            if (!flowx.size()) continue;
            std::nth_element(flowx.begin(), flowx.begin() + flowx.size()/2, flowx.end());
            std::nth_element(flowy.begin(), flowy.begin() + flowy.size()/2, flowy.end());
            cpts_out[j] = ppts_in[j] + cv::Point2f(flowx[flowx.size()/2], flowy[flowy.size()/2]);

            // if (cv::norm(cpts_out[j] - cpts_in[j]) > GPFT_ADD_POINT_DISTANCE_THRESHOLD) { 
            //     std::cerr << "cpts_out: " << cpts_out[j] << " in: " << cpts_in[j] << std::endl;
            //     std::cerr << "flowx: " << cv::Mat(flowx) << " flowy: " << cv::Mat(flowy) << std::endl;
            // }

            status[j] = true;
        }
        // cpts_in = cpts_out;
        return;
    }

    //============================================
    // GeneralPurposeFeatureTracker class
    //============================================

    static inline float depth_rbf_func(float x, float mu, float sigma2) { 
        if (x>mu)
            return std::exp(-std::pow(x-mu,2)/(2*sigma2));
        else 
            return 1.f;
    }

    // ascending
    static bool sort_by_node_info_depth(const node_info& lhs, 
                                        const node_info& rhs) { 
        return lhs.dist_euclidean < rhs.dist_euclidean;
    }

    // descending
    static bool sort_by_node_info_normal(const node_info& lhs, 
                                         const node_info& rhs) { 
        return lhs.dist_normal > rhs.dist_normal;
    }

    GeneralPurposeFeatureTracker::GeneralPurposeFeatureTracker () { 
        std::cerr << "GeneralPurposeFeatureTracker ctor: " << std::endl;
        //--------------------------------------------
        // Feature Detector
        // virtual void detectImpl( const Mat& image, vector<KeyPoint>& keypoints, const Mat& mask=Mat() ) const;
        //--------------------------------------------
        // detector = new cv::PyramidAdaptedFeatureDetector( cv::FeatureDetector::create("GFTT"), 3 );

        // // FAST
        // int fast_threshold = 10; bool nonmax = true; 
        // cv::FastFeatureDetector* _detector = new cv::FastFeatureDetector( fast_threshold, nonmax );

        // GFTT
        int gftt_maxCorners=400; double gftt_qualityLevel=0.02; double gftt_minDistance=10;
        int gftt_blockSize=5; bool gftt_useHarrisDetector=false; double gftt_k=0.04 ;
        cv::GFTTDetector* _detector = new cv::GFTTDetector( gftt_maxCorners, gftt_qualityLevel, gftt_minDistance,
                                                            gftt_blockSize, gftt_useHarrisDetector, gftt_k);


        // // MSER
        // int mser_delta=5; int mser_min_area=60; int mser_max_area=14400;
        // double mser_max_variation=0.25; double mser_min_diversity=.2;
        // int mser_max_evolution=200; double mser_area_threshold=1.01;
        // double mser_min_margin=0.003; int mser_edge_blur_size=5;
        // cv::MSER* _detector = new cv::MSER( mser_delta, mser_min_area, mser_max_area,
        //                                     mser_max_variation, mser_min_diversity,
        //                                     mser_max_evolution, mser_area_threshold,
        //                                     mser_min_margin, mser_edge_blur_size );

        // cv::SURF* _detector = new cv::SURF(5e-3);

        // Pyramid Detector
        detector = new cv::PyramidAdaptedFeatureDetector(_detector, 3 );
       
        //--------------------------------------------
        // Feature Extractor
        // virtual void computeImpl( const Mat& image, vector<KeyPoint>& keypoints, Mat& descriptors ) const;
        //--------------------------------------------
        extractor = cv::DescriptorExtractor::create("FREAK");

        //--------------------------------------------
        // Descriptor Matcher
        // // Find one best match for each query descriptor (if mask is empty).
        // CV_WRAP void match( const Mat& queryDescriptors, const Mat& trainDescriptors,
        //             CV_OUT vector<DMatch>& matches, const Mat& mask=Mat() ) const;
        // // Find k best matches for each query descriptor (in increasing order of distances).
        // // compactResult is used when mask is not empty. If compactResult is false matches
        // // vector will have the same size as queryDescriptors rows. If compactResult is true
        // // matches vector will not contain matches for fully masked out query descriptors.
       // CV_WRAP void knnMatch( const Mat& queryDescriptors, const Mat& trainDescriptors,
        //                CV_OUT vector<vector<DMatch> >& matches, int k,
        //                const Mat& mask=Mat(), bool compactResult=false ) const;
        // // Find best matches for each query descriptor which have distance less than
        // // maxDistance (in increasing order of distances).
        // void radiusMatch( const Mat& queryDescriptors, const Mat& trainDescriptors,
        //                   vector<vector<DMatch> >& matches, float maxDistance,
        //                   const Mat& mask=Mat(), bool compactResult=false ) const;
        //--------------------------------------------
        // BruteForce-Hamming, BruteForce-HammingLUT, BruteForce, 
        // FlannBased, BruteForce-SL2, BruteForce-Hamming(2)
        matcher = cv::DescriptorMatcher::create( "BruteForce-Hamming" ); 
        // matcher = cv::DescriptorMatcher::create( "BruteForce" ); 
        
        if( detector.empty() || extractor.empty() || matcher.empty()  )  {
            cout << "Can not create detector or descriptor exstractor or descriptor matcher of given types" << endl;
        }

        mode = TRACK3D_WITH_NORMALS;
        enable_subpixel_refinement = true;
    }

    // GeneralPurposeFeatureTracker::GeneralPurposeFeatureTracker (const GeneralPurposeFeatureTracker& gpft) { 
    //     std::cerr << "GeneralPurposeFeatureTracker copy: " << std::endl;
    // }

    GeneralPurposeFeatureTracker::~GeneralPurposeFeatureTracker () { 
        std::cerr << "GeneralPurposeFeatureTracker dtor: " << std::endl;
    }

    void 
    GeneralPurposeFeatureTracker::setTrackingMode (const TrackingMode& _mode) { 
        mode = _mode;
    }

    void GeneralPurposeFeatureTracker::reset() {
        pimg = cv::Mat();
        tracklet_manager.reset();
    }

    void
    GeneralPurposeFeatureTracker::update (int64_t utime, const cv::Mat& img) { 
        assert(mode == TRACK2D);
        std::cerr << "GeneralPurposeFeatureTracker update track2d: " << std::endl;
    }

    void
    GeneralPurposeFeatureTracker::update (int64_t utime, const cv::Mat& img, 
                                          pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud,
                                          pcl::PointCloud<pcl::Normal>::Ptr& normals,
                                          pcl::search::KdTree<pcl::PointXYZRGB>::Ptr& tree, 
                                          float DEPTH_SCALE) { 
        assert(mode == TRACK3D || mode == TRACK3D_WITH_NORMALS);
        double tic;
        std::cerr << "GeneralPurposeFeatureTracker update track3d: " << std::endl;
        if (img.empty()) return;

        //--------------------------------------------
        // Only process frames that are newer
        // Ignore frames that are old
        // Track Clean up before update
        //--------------------------------------------
        tic = bot_timestamp_now();
        tracklet_manager.cleanup(utime, GPFT_MAX_TRACK_TIMESTAMP_DELAY);
        printf("%s===> TRACK CLEANUP: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

        //--------------------------------------------
        // Prune so that length is only GPFT_MAX_TRACK_TIMESPAN seconds long
        //--------------------------------------------
        tic = bot_timestamp_now();
        tracklet_manager.prune(GPFT_MAX_TRACK_TIMESPAN);
        printf("%s===> TRACK CLEANUP: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

        //--------------------------------------------
        // Gaussian blur
        //--------------------------------------------
        tic = bot_timestamp_now();

#if 1
        std::vector<cv::Mat> channels;
        cv::split(img, channels);
        cv::Mat gray = channels[1];         
#else
        cv::Mat gray;
        cv::cvtColor(img, gray, CV_BGR2GRAY);
#endif

        // cv::blur(gray, gray, cv::Size(11,11), cv::Point(-1,-1), cv::BORDER_DEFAULT);
        cv::GaussianBlur(gray, gray, cv::Size(11,11), 0, 0, cv::BORDER_DEFAULT);
        // cv::bilateralFilter(img, img, 5);
        printf("%s===> GAUSSIAN BLUR: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

        //--------------------------------------------
        // Prepare SuperFeatures from existing tracklets
        //--------------------------------------------
        std::vector<SuperFeature> p_sfpts; 
        std::vector<cv::Point2f> p_pts;
        tracklet_manager.getLatestFeatures(p_sfpts, p_pts);
        printf("Existing Features: %i\n", p_sfpts.size()); 

        //--------------------------------------------
        // Detect keypoints
        //--------------------------------------------
        tic = bot_timestamp_now();
        std::vector<cv::KeyPoint> c_kpts;
        detector->detect( gray, c_kpts );
        printf("%s===> KEYPOINT DETECTION: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 
        tic = bot_timestamp_now();
        if (enable_subpixel_refinement)
        {
            cv::TermCriteria SubPix_termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,5,0.05);
            cv::Size SubPix_winsize(11,11);
            std::vector<cv::Point2f> c_pts_;
            cv::KeyPoint::convert(c_kpts, c_pts_);
            cv::cornerSubPix( gray, c_pts_, SubPix_winsize, cv::Size(-1,-1), SubPix_termcrit);
            cv::KeyPoint::convert(c_pts_, c_kpts);
        }
        printf("%s===> SUB-PIXEL REFINEMENT: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

        {
            cv::Mat display = img.clone();
            for (int j=0; j<c_kpts.size(); j++) 
                circle(display, c_kpts[j].pt, 1, cv::Scalar(200), 1, CV_AA);
            opencv_utils::imshow("pts", display);
        }

        //--------------------------------------------
        // Extract descriptors
        //--------------------------------------------
        tic = bot_timestamp_now();
        cv::Mat c_desc;
        extractor->compute( gray, c_kpts, c_desc );
        printf("%s===> DESC EXTRACTION: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

        printf("Extracted: %i pts\n", c_kpts.size()); 
        if (!c_kpts.size()) return;

        //--------------------------------------------
        // Prepare SuperFeatures from detected features 
        // ( ID is not resolved yet (set to -1) )
        //--------------------------------------------
        tic = bot_timestamp_now();
        std::vector<SuperFeature> c_sfpts; 
        std::vector<cv::Point2f> c_pts; 
        SuperFeature sf; sf.id = -1;
        assert(c_kpts.size() == c_desc.rows);
        for (int j=0; j<c_kpts.size(); j++) { 
            int idx = int(c_kpts[j].pt.y)*cloud->width + int(c_kpts[j].pt.x);
            if (idx < 0 || idx >= cloud->points.size()) continue;

            pcl::PointXYZRGB pt = cloud->points[idx];
            if (pt.z != pt.z) continue; // Check if disparity is NaN

            // std::cerr << "pt: " << c_kpts[j].pt << " angle:" << c_kpts[j].angle 
            //           << " resp:" << c_kpts[j].response << " sz: " << c_kpts[j].size << std::endl;
            // Populate Point2D, Point3D, response, angles, ID
            sf.utime = utime;
            sf.response = c_kpts[j].response;
            sf.angle = c_kpts[j].angle;
            sf.description = c_desc.row(j).clone();
            sf.point2D.x = c_kpts[j].pt.x;
            sf.point2D.y = c_kpts[j].pt.y;
            sf.point3D.x = pt.x, sf.point3D.y = pt.y, sf.point3D.z = pt.z;
            sf.normal = cv::Vec3f(0,0,1);
            sf.normal_valid = false;

            // Normal: Search for the feature in the decimated point cloud, and retrieve its normal
            if (tree && normals) { 
                std::vector<int> kinds; 
                std::vector<float> kdists; 
                tree->nearestKSearch(pt, 1, kinds, kdists); 
                if (kinds.size() && kinds[0] >= 0 && kinds[0] < normals->points.size() && 
                    !(normals->points[kinds[0]].normal[0] != normals->points[kinds[0]].normal[0] || 
                      normals->points[kinds[0]].normal[1] != normals->points[kinds[0]].normal[1] || 
                      normals->points[kinds[0]].normal[2] != normals->points[kinds[0]].normal[2])) { 
                    
                    sf.normal = cv::Vec3f(normals->points[kinds[0]].normal[0], 
                                          normals->points[kinds[0]].normal[1], 
                                          normals->points[kinds[0]].normal[2]);
                    // std::cerr << "normal: " << sf.normal << std::endl;
                    sf.normal_valid = true;
                }
            } 
            
            c_sfpts.push_back(sf);
            c_pts.push_back(sf.point2D);
        }
        printf("Current Features: %i | Dropped Features: %i\n", c_sfpts.size(), c_kpts.size() - c_sfpts.size()); 
        printf("%s===> PREPARE SUPERFEATURES: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

        /*

        tic = bot_timestamp_now();
        //--------------------------------------------
        // Build KdTree for existing features
        //--------------------------------------------
        cv::Mat p_pts_mat = cv::Mat(p_pts).reshape(1);
        cv::flann::LinearIndexParams p_pts_tree_params; // using 4 randomized kdtrees        
        cv::flann::Index p_knn_index(p_pts_mat, p_pts_tree_params);

        //--------------------------------------------
        // Build KdTree for new features
        //--------------------------------------------
        cv::Mat c_pts_mat = cv::Mat(c_pts).reshape(1);
        cv::flann::LinearIndexParams c_pts_tree_params; // using 4 randomized kdtrees        
        cv::flann::Index c_knn_index(c_pts_mat, c_pts_tree_params);
        printf("%s===> BUILD KD TREE: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

        //--------------------------------------------
        // Build Delaunay triangulation for connected components
        //--------------------------------------------
        tic = bot_timestamp_now();
        spvision::Subdiv2D subdiv(cv::Rect(0, 0, img.cols, img.rows));
        std::map<int, int> delvtx_idx; 
        for (int j=0; j<c_pts.size(); j++) 
            delvtx_idx.insert(std::make_pair(subdiv.insert(c_pts[j]), j));

        std::vector<cv::Vec2i> edge_list;
        std::map<std::pair<int, int>, float> edge_pair;
        std::map<std::pair<int, int>, float> depth_edge_pair;
        std::map<std::pair<int, int>, float> normal_edge_pair;
        std::map<int, std::vector<node_info> > vtx_map;
        { 
            cv::Mat display = img.clone();
            int v1, v2, v1idx, v2idx;
            cv::Point2f p1, p2;
            subdiv.getEdgeList(edge_list); 

            for (int j=0; j<edge_list.size(); j++) { 
                int v1 = edge_list[j][0];
                int v2 = edge_list[j][1];

                p1 = subdiv.vtx[v1].pt;
                p2 = subdiv.vtx[v2].pt;

                assert(delvtx_idx.find(v1) != delvtx_idx.end());
                assert(delvtx_idx.find(v2) != delvtx_idx.end());

                v1idx = delvtx_idx[v1]; 
                v2idx = delvtx_idx[v2];

                // std::cerr << "dist " << norm << ": " << c_sfpts[v1idx].point3D << " " << c_sfpts[v2idx].point3D << std::endl;
                node_info ni; 
                ni.dist_euclidean = cv::norm(c_sfpts[v1idx].point3D - c_sfpts[v2idx].point3D);
                // ni.dist_euclidean = depth_rbf_func(ni.dist_euclidean, 0.1, 0.01);
                ni.dist_normal = (c_sfpts[v1idx].normal.dot(c_sfpts[v2idx].normal) + 1) * 0.5f;

                ni.idx = v2idx;
                vtx_map[v1idx].push_back(ni);
                ni.idx = v1idx;
                vtx_map[v2idx].push_back(ni);

                edge_pair[make_sorted_pair(v1idx,v2idx)] = ni.dist_normal; //  * ni.dist_euclidean;
                // depth_edge_pair[make_sorted_pair(v1idx,v2idx)] = ni.dist_euclidean;
                normal_edge_pair[make_sorted_pair(v1idx,v2idx)] = ni.dist_normal;
            }
        }

        {
            // RBF on difference of normals, and distance from expected pt (look at neighbors and extrapolate);
            for (std::map<int, std::vector<node_info> >::iterator it1 = vtx_map.begin(); 
                 it1 != vtx_map.end(); it1++) { 
                int v1idx = it1->first; 
                std::vector<node_info>& map = it1->second;
                if (!map.size()) continue;

                std::vector<node_info> sorted_depth(map.begin(), map.end());
                std::sort(sorted_depth.begin(), sorted_depth.end(), sort_by_node_info_depth);

                // std::vector<node_info> sorted_normals(map.begin(), map.end());
                // std::sort(sorted_normals.begin(), sorted_normals.end(), sort_by_node_info_normal);

                int vmedidx = sorted_depth[sorted_depth.size()/2].idx;
                int vmedidx2 = sorted_depth[sorted_depth.size()/4].idx;
                float norm = cv::norm(c_sfpts[v1idx].point3D - c_sfpts[vmedidx].point3D);
                float norm2 = cv::norm(c_sfpts[v1idx].point3D - c_sfpts[vmedidx2].point3D);
                for (int j=0; j<sorted_depth.size(); j++) { 
                    int v2idx = sorted_depth[j].idx;
                    std::pair<int, int> pair = make_sorted_pair(v1idx, v2idx);
                    float rbf = depth_rbf_func(sorted_depth[j].dist_euclidean, norm, 4*norm*norm);
                    if (edge_pair.find(pair) == edge_pair.end())
                        edge_pair[pair] = rbf;
                    else 
                        edge_pair[pair] *= rbf;

                    if (depth_edge_pair.find(pair) == depth_edge_pair.end())
                        depth_edge_pair[pair] = rbf;
                    else 
                        depth_edge_pair[pair] *= rbf;
                }

                // vmedidx = sorted_normals[sorted_normals.size()/2].idx;
                // vmedidx2 = sorted_normals[sorted_normals.size()/4].idx;
                // norm = (c_sfpts[v1idx].normal.dot(c_sfpts[vmedidx].normal) + 1) * 0.5f;
                // norm2 = (c_sfpts[v1idx].normal.dot(c_sfpts[vmedidx2].normal) + 1) * 0.5f;

                // if (edge_pair.find(pair) == edge_pair.end())
                //     edge_pair[pair] = norm;
                // else 
                //     edge_pair[pair] *= norm;

                // if (normal_edge_pair.find(pair) == normal_edge_pair.end())
                //     normal_edge_pair[pair] = rbf;
                // else 
                //     normal_edge_pair[pair] *= rbf;
            }
        }

        //--------------------------------------------
        // Depth + normals 
        //--------------------------------------------
        { 
            cv::Mat display = img.clone();
            for (std::map<std::pair<int, int>, float>::iterator it = edge_pair.begin(); it != edge_pair.end(); it++) { 
                int v1idx = it->first.first; 
                int v2idx = it->first.second; 

                cv::Point2f p1 = c_sfpts[v1idx].point2D;
                cv::Point2f p2 = c_sfpts[v2idx].point2D;

                // float hue = (1.f - it->second) * 150;
                // cv::Scalar h(opencv_utils::hsv_to_bgrvec(cv::Vec3b(hue, 255, 255)));
                float s = it->second;
                cv::Scalar h(0, 255*s, 255*s);
                cv::line(display, p1, p2, h, 1, CV_AA, 0);
            }
            for (int j=0; j<c_pts.size(); j++) 
                circle(display, c_pts[j], 1, cv::Scalar(0,200,200), -1, CV_AA);
            opencv_utils::imshow("Delaunay depth+normals", display);
        }


        //--------------------------------------------
        // Depth edge belief 
        //--------------------------------------------
        { 
            cv::Mat display = img.clone();
            for (std::map<std::pair<int, int>, float>::iterator it = depth_edge_pair.begin(); 
                 it != depth_edge_pair.end(); it++) { 
                int v1idx = it->first.first; 
                int v2idx = it->first.second; 

                cv::Point2f p1 = c_sfpts[v1idx].point2D;
                cv::Point2f p2 = c_sfpts[v2idx].point2D;

                // float hue = (1.f - it->second) * 150;
                // cv::Scalar h(opencv_utils::hsv_to_bgrvec(cv::Vec3b(hue, 255, 255)));
                float s = it->second;
                cv::Scalar h(0, 255*s, 255*s);
                cv::line(display, p1, p2, h, 1, CV_AA, 0);
            }
            for (int j=0; j<c_pts.size(); j++) 
                circle(display, c_pts[j], 2, cv::Scalar(c_sfpts[j].point3D.z * 128.f,
                                                        c_sfpts[j].point3D.z * 128.f,
                                                        c_sfpts[j].point3D.z * 128.f), -1, CV_AA);
            opencv_utils::imshow("Delaunay depth", display);
        }


        //--------------------------------------------
        // Normal edge belief
        //--------------------------------------------
        { 
            cv::Mat display = img.clone();
            for (std::map<std::pair<int, int>, float>::iterator it = normal_edge_pair.begin(); 
                 it != normal_edge_pair.end(); it++) { 
                int v1idx = it->first.first; 
                int v2idx = it->first.second; 

                cv::Point2f p1 = c_sfpts[v1idx].point2D;
                cv::Point2f p2 = c_sfpts[v2idx].point2D;

                // float hue = (1.f - it->second) * 150;
                // cv::Scalar h(opencv_utils::hsv_to_bgrvec(cv::Vec3b(hue, 255, 255)));
                float s = it->second;
                cv::Scalar h(0, 255*s, 255*s);
                if (c_sfpts[v1idx].normal_valid && c_sfpts[v2idx].normal_valid) 
                    cv::line(display, p1, p2, h, 1, CV_AA, 0);
            }
            for (int j=0; j<c_pts.size(); j++) { 
                circle(display, c_pts[j], 2, 
                       cv::Scalar((c_sfpts[j].normal[0] + 1.f) * 128.f, 
                                  (c_sfpts[j].normal[1] + 1.f) * 128.f, 
                                  (c_sfpts[j].normal[2] + 1.f) * 128.f), -1, CV_AA);
            }
            opencv_utils::imshow("Delaunay normals", display);
        }

        // //--------------------------------------------
        // // Find Delaunay Triangle centers
        // //--------------------------------------------
        // {
        //     cv::Mat display = img.clone();
        //     cv::Mat display1 = img.clone();
        //     cv::Mat display2 = img.clone();
        //     cv::Mat display3 = img.clone();

        //     std::vector<cv::Vec2i> edge_indices;
        //     std::vector<cv::Vec3i> tri_indices;
        //     subdiv.getEdgeList(edge_indices);
        //     subdiv.getTriangleList(tri_indices);

        //     //--------------------------------------------
        //     // Draw DT
        //     //--------------------------------------------
        //     for (int j=0; j<edge_indices.size(); j++) { 
        //         int v1 = edge_indices[j][0];
        //         int v2 = edge_indices[j][1];

        //         cv::Point2f p1 = subdiv.vtx[v1].pt;
        //         cv::Point2f p2 = subdiv.vtx[v2].pt;

        //         cv::Scalar h(50,50,50);
        //         cv::line(display, p1, p2, h, 1, CV_AA, 0);
        //     }

        //     //--------------------------------------------
        //     // Find Delaunay Triangle centers
        //     // Build shared_edges
        //     //--------------------------------------------
        //     std::map<std::pair<int, int>, std::list<int> > shared_edges; 
        //     for( size_t i = 0; i < tri_indices.size(); i++ ) {
        //         int v1 = tri_indices[i][0], v2 = tri_indices[i][1], v3 = tri_indices[i][2];

        //         std::pair<int, int> pair1 = make_sorted_pair(v1, v2);
        //         std::pair<int, int> pair2 = make_sorted_pair(v2, v3);
        //         std::pair<int, int> pair3 = make_sorted_pair(v1, v3);

        //         shared_edges[pair1].push_back(i);
        //         shared_edges[pair2].push_back(i);
        //         shared_edges[pair3].push_back(i);

        //         cv::Point2f p1 = subdiv.vtx[v1].pt;
        //         cv::Point2f p2 = subdiv.vtx[v2].pt;
        //         cv::Point2f p3 = subdiv.vtx[v3].pt;

        //         circle(display, (p1 + p2 + p3) * (1.f/3), 3, Scalar(), -1, CV_AA, 0);
        //     }

        //     //--------------------------------------------
        //     // Median depth and normal within each tri
        //     //--------------------------------------------
        //     std::vector<std::vector<cv::Point2f> > tri_vertices(tri_indices.size(), std::vector<cv::Point2f>(3));
        //     for (int j=0; j<tri_indices.size(); j++) 
        //         for (int k=0; k<3; k++)
        //             tri_vertices[j][k] = subdiv.vtx[tri_indices[j][k]].pt;
                
        //     assert(cloud->height * cloud->width == cloud->points.size());

        //     std::vector<bool> processed(normals->points.size(), false);
        //     std::vector<float> tri_depths(tri_indices.size(), 0);
        //     std::vector<cv::Vec3f> tri_normals(tri_indices.size(), cv::Vec3f(0,0,1));

        //     int skip = int(DEPTH_SCALE);

        //     cv::vector<cv::Scalar> pt_colors(tri_indices.size());
        //     for (int j=0; j<pt_colors.size(); j++) 
        //         pt_colors[j] = cv::Scalar(rand()%256,rand()%256, rand()%256);

        //     for (int j=0; j<tri_indices.size(); j++) {
        //         std::vector<cv::Point2f>& vertices = tri_vertices[j];
        //         std::vector<float> depths, normalsx, normalsy, normalsz;
        //         cv::Point pt;

        //         for (int y=0, idx=0; y<cloud->height; y+=skip) { 
        //             for (int x=0; x<cloud->width; x+=skip, idx++) { 
        //                 if (processed[idx]) continue;
        //                 pt.x = x; 
        //                 pt.y = y;
        //                 if (pt.x < 0 || pt.y < 0 || pt.x >= img.cols || pt.y >= img.rows) continue;
        //                 int _idx = pt.y * cloud->width + pt.x;
        //                 assert(_idx >=0 && _idx < cloud->width * cloud->height);
        //                 if (cv::pointPolygonTest(cv::Mat(vertices), pt, false) > 0) { 
        //                     depths.push_back(cloud->points[_idx].z);
        //                     normalsx.push_back(normals->points[idx].normal[0]);
        //                     normalsy.push_back(normals->points[idx].normal[1]);
        //                     normalsz.push_back(normals->points[idx].normal[2]);
        //                     processed[idx] = true;
        //                     circle(display, pt, 3, cv::Scalar((normals->points[idx].normal[0] + 1.f) * 128, 
        //                                                       (normals->points[idx].normal[1] + 1.f) * 128, 
        //                                                       (normals->points[idx].normal[2] + 1.f) * 128), -1, CV_AA, 0);
        //                 }
        //             }
        //         }
        //         std::nth_element(depths.begin(), depths.begin() + depths.size() / 4, depths.end());
        //         std::nth_element(normalsx.begin(), normalsx.begin() + normalsx.size() / 2, normalsx.end());
        //         std::nth_element(normalsy.begin(), normalsy.begin() + normalsy.size() / 2, normalsy.end());
        //         std::nth_element(normalsz.begin(), normalsz.begin() + normalsz.size() / 2, normalsz.end());                        
        //         if (depths.size())
        //             tri_depths[j] = depths[depths.size()/4];

        //         if (normalsx.size())
        //             tri_normals[j] = cv::Vec3f(normalsx[normalsx.size()/2], 
        //                                        normalsy[normalsy.size()/2], 
        //                                        normalsz[normalsz.size()/2]);
        //     }

        //     std::map<std::pair<int, int>, float> edge_pair;
        //     std::map<std::pair<int, int>, float> depth_edge_pair;
        //     std::map<std::pair<int, int>, float> normal_edge_pair;

        //     //--------------------------------------------
        //     // Depth and Normal similarity between tris
        //     //--------------------------------------------
        //     for (std::map<std::pair<int, int>, std::list<int> >::iterator it = shared_edges.begin(); 
        //          it != shared_edges.end(); it++) { 
        //         const std::pair<int, int>& pair = it->first;
        //         std::vector<int> tris(it->second.begin(), it->second.end());
        //         if (tris.size() != 2) continue;
        //         int tidx1 = tris[0], tidx2 = tris[1];
        //         assert(tidx1>=0 && tidx1 < tri_indices.size());
        //         int t1v1 = tri_indices[tidx1][0], t1v2 = tri_indices[tidx1][1], t1v3 = tri_indices[tidx1][2];
        //         int t2v1 = tri_indices[tidx2][0], t2v2 = tri_indices[tidx2][1], t2v3 = tri_indices[tidx2][2];

        //         cv::Point2f t1p1 = subdiv.vtx[t1v1].pt, t2p1 = subdiv.vtx[t2v1].pt;
        //         cv::Point2f t1p2 = subdiv.vtx[t1v2].pt, t2p2 = subdiv.vtx[t2v2].pt;;
        //         cv::Point2f t1p3 = subdiv.vtx[t1v3].pt, t2p3 = subdiv.vtx[t2v3].pt;;

        //         float norm_dist = fabs(tri_normals[tidx1].dot(tri_normals[tidx2])); 
        //         float depth_dist = depth_rbf_func(fabs(tri_depths[tidx1] - tri_depths[tidx2]), 0.02, 0.0009); 
        //         float dist = norm_dist * depth_dist;
        //         edge_pair[pair] = dist;
        //         depth_edge_pair[pair] = depth_dist;
        //         normal_edge_pair[pair] = norm_dist;

        //         cv::Scalar h(0, 255*dist, 255*dist);
        //         cv::line(display1, (t1p1 + t1p2 + t1p3) * (1.f/3), (t2p1 + t2p2 + t2p3) * (1.f/3), h, 1, CV_AA, 0);

        //         h = cv::Scalar(0, 255*depth_dist, 255*depth_dist);
        //         cv::line(display2, (t1p1 + t1p2 + t1p3) * (1.f/3), (t2p1 + t2p2 + t2p3) * (1.f/3), h, 1, CV_AA, 0);

        //         h = cv::Scalar(0, 255*norm_dist, 255*norm_dist);
        //         cv::line(display3, (t1p1 + t1p2 + t1p3) * (1.f/3), (t2p1 + t2p2 + t2p3) * (1.f/3), h, 1, CV_AA, 0);
        //     }
        //     opencv_utils::imshow("Delaunay triangulation", display);            
        //     opencv_utils::imshow("Delaunay depth+normal", display1);            
        //     opencv_utils::imshow("Delaunay depth", display2);            
        //     opencv_utils::imshow("Delaunay normal", display3);            
        // }

        printf("%s===> BUILD DELAUNAY TRIANGULATION: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

        std::set<int64_t> assigned_ids; 
#define OPTICALFLOW 0
#if OPTICALFLOW
        //--------------------------------------------
        // LK Sparse Optical Flow
        //--------------------------------------------
        tic = bot_timestamp_now();
        std::vector<cv::Point2f> pred_c_pts;
        std::vector<uchar> pred_c_status;
        std::vector<float> pred_c_err;
        
        // Refined after median flow
        std::vector<bool> pred_c_pts_refined_status;
        std::vector<cv::Point2f> pred_c_pts_refined; 

        if (p_pts.size()) { 
            assert(!pimg.empty());

            // Compute LK optical flow
            float deriv_lambda = 0.1;
            float min_eig = 1e-3;
            cv::Size LK_winsize(7,7);
            
            cv::TermCriteria LK_termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,40,0.01);
            cv::calcOpticalFlowPyrLK(pimg, img, p_pts, pred_c_pts, pred_c_status, pred_c_err, LK_winsize,
                                     1, LK_termcrit); // , 0, 0.001, deriv_lambda);
            printf("%s===> OPTICAL FLOW: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

            // subpixel refinement
            cv::TermCriteria SubPix_termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,5,0.05);
            cv::Size SubPix_winsize(11,11);
            tic = bot_timestamp_now();
            cv::cornerSubPix( gray, pred_c_pts, SubPix_winsize, cv::Size(-1,-1), SubPix_termcrit);
            printf("%s===> OF: SUB PIXEL REFINEMENT: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

            // Median filter
            tic = bot_timestamp_now();
            sparseMedianFlow(p_knn_index, p_pts, pred_c_pts, pred_c_pts_refined, pred_c_pts_refined_status);
            printf("%s===> SPARSE OPTICAL FLOW: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 
            { cv::Mat display = pimg.clone() / 2;
                // Pre-median flow
                for (int j=0; j<p_pts.size(); j++) { 
                    cv::Scalar c = (pred_c_status[j]) ? cv::Scalar(100,100,100) : cv::Scalar(50,50,50);
                    cv::line(display, p_pts[j], pred_c_pts[j], c, 1, CV_AA, 0);
                }
                // Median flow
                for (int j=0; j<p_pts.size(); j++) {
                    cv::Scalar c = (pred_c_pts_refined_status[j]) ? cv::Scalar(0,0,200) : cv::Scalar(0,0,50);

                    float norm = cv::norm(p_pts[j] - pred_c_pts_refined[j]);
                    cv::Point pt = (p_pts[j] + pred_c_pts_refined[j]) * 0.5;
                    // cv::putText(display,cv::format("%3.2f", norm), pt, 0, 0.3, c,1,CV_AA);
                    cv::line(display, p_pts[j], pred_c_pts_refined[j], c, 1, CV_AA, 0);
                }
                opencv_utils::imshow("green: refined (median flow), white: (optical flow pred)", display);
            }
        }     


        assert(pred_c_pts_refined.size() == p_pts.size());
        assert(pred_c_pts_refined.size() == p_sfpts.size());
        { 
            int count = 0; 
            const int rnn_max_results = 1;
            for (int j=0; j<pred_c_pts_refined.size(); j++) { 
                if (!pred_c_pts_refined_status[j]) continue;
            
                // Search new points to see if within threshold of old points
                cv::Mat_<int> rnn_inds(1,rnn_max_results); rnn_inds = -1;
                cv::Mat_<float> rnn_dists;
            
                cv::Mat pred_pts_mat = cv::Mat(pred_c_pts_refined[j]).reshape(1).t();
                // std::cerr << "pred_pts_mat: " << pred_pts_mat << std::endl;
            
                int RNN = std::min(c_knn_index.radiusSearch(pred_pts_mat, rnn_inds, rnn_dists, 
                                                            GPFT_ADD_POINT_DISTANCE_THRESHOLD_SQ, 
                                                            rnn_max_results, cv::flann::SearchParams()), rnn_max_results);
                // std::cerr << "rnn_inds: " << rnn_inds << " rnn_dists: " << rnn_dists << " RNN: " << RNN << std::endl;
                if (!RNN) continue;
            
                // Build descriptors for each point in (current) within add_point_distance_threshold
                bool matched = false;
                cv::Mat rnn_desc(RNN, extractor->descriptorSize(), extractor->descriptorType());
                for (int k=0; k<RNN; k++) { 
                    assert(rnn_inds.at<int>(k)>=0 && rnn_inds.at<int>(k)<c_sfpts.size());
                    // std::cerr << "cpt: [" << k << "]: " << c_sfpts[rnn_inds.at<int>(k)].point2D << " d: " << rnn_dists.at<float>(k) << std::endl;

                    if (rnn_dists.at<float>(k) > GPFT_MIN_FLOW_MATCH_DISTANCE) continue;
                    rnn_desc.row(k) = c_sfpts[rnn_inds.at<int>(k)].description;
                    matched = true;
                }
                
                if (!matched) continue;
                
                // Find the nearest neighbors by match distance
                std::vector<std::vector<cv::DMatch> > matches_;
                matcher->knnMatch(p_sfpts[j].description, rnn_desc, matches_, RNN);
                assert(p_sfpts[j].description.rows == 1);
                assert(matches_.size() == 1);
                std::vector<cv::DMatch>& matches = matches_[0];
                // assert(matches.size() == knn);
                // std::cerr << "Matches: " << std::endl;
                // for (int m=0; m<matches.size(); m++) 
                //     std::cerr << matches[m].distance << " " << matches[m].trainIdx << "<=" << matches[m].queryIdx << std::endl;
            
                for (int m=0; m<matches.size(); m++) { 
                    // Pick only top match
                    int k_idx = matches[m].trainIdx;
                    assert(k_idx >=0 && k_idx < RNN);
                    int c_idx = rnn_inds.at<int>(k_idx);
                    assert(c_idx >=0 && c_idx < c_sfpts.size());
                
                    // Only for those not processed yet
                    if (c_sfpts[c_idx].id>=0) continue;
                    if (matches[m].distance < GPFT_MIN_DESC_MATCH_DISTANCE) continue;
                
                    // std::cerr << "TOP MATCH: " << matches[0].distance << ": " 
                    //           << c_sfpts[c_idx].point2D << "<=" << pred_c_pts_refined[j] << std::endl;
                    assigned_ids.insert(p_sfpts[j].id);
                    assert(c_sfpts[c_idx].id == -1);
                    c_sfpts[c_idx].id = p_sfpts[j].id; // unique id for every j
                    // if (!(p_sfpts[j].description.empty() || c_sfpts[j].description.empty())) { 
                    // std::cerr << "pdesc: " << p_sfpts[j].description << std::endl;
                    // std::cerr << "cdesc: " << c_sfpts[j].description << std::endl;
                    // }
                    count++;
                    break; // Pick only top match
                }
            }
            printf("Matched Features: %i | Unmatched Features: %i\n", count, pred_c_pts_refined.size() - count); 
            printf("%s===> OPTICAL FLOW MATCH WITH EXISTING TRACKS: %4.2f ms%s\n", 
                   esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 
        }
#endif

        //--------------------------------------------
        // Compare previous keypoints with current
        //--------------------------------------------
        tic = bot_timestamp_now();
        // { cv::Mat display = img.clone();
        //     for (int j=0; j<c_sfpts.size(); j++) 
        //         circle(display, c_sfpts[j].point2D, 1, cv::Scalar(200,200,200), 1, CV_AA);
        //     for (int j=0; j<p_sfpts.size(); j++) 
        //         circle(display, p_sfpts[j].point2D, 1, cv::Scalar(0,200,0), 1, CV_AA);
        //     opencv_utils::imshow("green: prev, white: curr", display);
        // }
        {
            // First add points that are close to the previous list
            int count = 0; 
            const int rnn_max_results = 5;
            for (int j=0; j<p_sfpts.size(); j++) { 
                if (assigned_ids.find(p_sfpts[j].id) != assigned_ids.end()) continue;
 
                // Search new points to see if within threshold of old points
                cv::Mat_<int> rnn_inds(1,rnn_max_results); rnn_inds = -1;
                cv::Mat_<float> rnn_dists;

                cv::Mat p_sfpts_mat = cv::Mat(p_sfpts[j].point2D).reshape(1).t();
                // std::cerr << "p_sfpts_mat: " << p_sfpts_mat << std::endl;

                int RNN = std::min(c_knn_index.radiusSearch(p_sfpts_mat, rnn_inds, rnn_dists, 
                                                            GPFT_ADD_POINT_DISTANCE_THRESHOLD_SQ, 
                                                            rnn_max_results, cv::flann::SearchParams()), rnn_max_results);
                // std::cerr << "feats: " << c_pts_mat.size() << " rnn_inds: " << rnn_inds << 
                //     " rnn_dists: " << rnn_dists << " RNN: " << RNN << std::endl;
                if (!RNN) continue;

                // Build descriptors for each point in (current) within add_point_distance_threshold
                cv::Mat rnn_desc(RNN, extractor->descriptorSize(), extractor->descriptorType());
                for (int k=0; k<RNN; k++) { 
                    assert(rnn_inds.at<int>(k)>=0 && rnn_inds.at<int>(k)<c_sfpts.size());
                    // std::cerr << "cpt: [" << k << "]: " << c_sfpts[rnn_inds.at<int>(k)].point2D << " d: " << rnn_dists.at<float>(k) << std::endl;
                    rnn_desc.row(k) = c_sfpts[rnn_inds.at<int>(k)].description;
                }

                // Find the nearest neighbors by match distance
                std::vector<std::vector<cv::DMatch> > matches_;
                matcher->knnMatch(p_sfpts[j].description, rnn_desc, matches_, RNN);
                assert(p_sfpts[j].description.rows == 1);
                assert(matches_.size() == 1);
                std::vector<cv::DMatch>& matches = matches_[0];
                // assert(matches.size() == knn);
                // std::cerr << "Matches: " << std::endl;
                // for (int m=0; m<matches.size(); m++) 
                //     std::cerr << matches[m].distance << " " << matches[m].trainIdx << "<=" << matches[m].queryIdx << std::endl;
                    
                
                for (int m=0; m<matches.size(); m++) { 
                    // Pick only top match
                    int k_idx = matches[m].trainIdx;
                    assert(k_idx >=0 && k_idx < RNN);
                    int c_idx = rnn_inds.at<int>(k_idx);
                    assert(c_idx >=0 && c_idx < c_sfpts.size());

                    // Only for those not processed yet
                    if (c_sfpts[c_idx].id>=0) continue;
                    if (matches[m].distance < GPFT_MIN_DESC_MATCH_DISTANCE) continue;
                    // std::cerr << "TOP MATCH: " << matches[0].distance << ": " 
                    //           << c_sfpts[c_idx].point2D << "<=" << p_sfpts[j].point2D << std::endl;

                    assert(c_sfpts[c_idx].id == -1);
                    c_sfpts[c_idx].id = p_sfpts[j].id; // unique id for every j
                    // if (!(p_sfpts[j].description.empty() || c_sfpts[j].description.empty())) { 
                    // std::cerr << "pdesc: " << p_sfpts[j].description << std::endl;
                    // std::cerr << "cdesc: " << c_sfpts[j].description << std::endl;
                    // }
                    count++;
                    break; // Pick only top match
                }
            }
            printf("Matched Features: %i | Dropped Features: %i\n", count, p_sfpts.size() - count); 
            printf("%s===> DESCRIPTOR MATCH WITH EXISTING TRACKS: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 
        }

        //--------------------------------------------
        // Remove unprocessed current points that are
        // too close to the previous points (if available)
        //--------------------------------------------
        tic = bot_timestamp_now();
        std::list<SuperFeature> c_sfpts_list(c_sfpts.begin(), c_sfpts.end());
        { 
            if (p_pts.size()) { 
                int total = c_sfpts_list.size();
                for (std::list<SuperFeature>::iterator it = c_sfpts_list.begin(); it != c_sfpts_list.end(); it++) { 
                    if (it->id >= 0) continue;

                    // Search previous points to see if within threshold of current points
                    cv::Mat rnn_inds, rnn_dists;
                    cv::Mat c_sfpts_mat = cv::Mat(it->point2D).reshape(1).t();
                    // std::cerr << "p_sfpts_mat: " << p_sfpts_mat << std::endl;

                    int rnn_max_results = 1; 
                    int RNN = std::min(p_knn_index.radiusSearch(c_sfpts_mat, rnn_inds, rnn_dists, 
                                                                GPFT_POINT_SPACING_THRESHOLD, 
                                                                rnn_max_results, cv::flann::SearchParams()), rnn_max_results);
                    // std::cerr << "feats: " << c_pts_mat.size() << " rnn_inds: " << rnn_inds << 
                    //     " rnn_dists: " << rnn_dists << " RNN: " << RNN << std::endl;
                    if (!RNN) continue;
                    it = c_sfpts_list.erase(it); it--;
                }
                printf("===> Total: %i, Remaining: %i, Removed: %i\n", total, c_sfpts_list.size(), total - c_sfpts_list.size()); 
            }
        }
        printf("%s===> REMOVE UNPROCESSED POINTS: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 
            
        //--------------------------------------------
        // Now the rest unprocessed current points are new
        // Add new features
        //--------------------------------------------
        tic = bot_timestamp_now();
        tracklet_manager.addSuperFeatures(utime, c_sfpts_list);
        printf("%s===> ADD SUPER FEATURES: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

#if DEBUG_CAPS
        tic = bot_timestamp_now();
        double sc = 1.f;
        cv::Mat display = img.clone();
        cv::Mat dalpha = 0.5 * display;
        cv::Mat dalpha2 = dalpha.clone();
        
        {   int64_t max_track_length = 1, min_track_length = std::numeric_limits<int>::max();
            for (std::map<int64_t, int64_t>::iterator it = tracklet_manager.track_confidence.begin(); 
                 it != tracklet_manager.track_confidence.end(); it++) { 
                const int64_t feat_id = it->first; 
                max_track_length = std::max(it->second, max_track_length);
                min_track_length = std::min(it->second, min_track_length);
            }
            float conf_norm = 255.f / (max_track_length - min_track_length);

            cv::Vec3b h(0,200,200);
            float r = spvision::GPFT_ADD_POINT_DISTANCE_THRESHOLD;
            for (TrackMapIt it = tracklet_manager.tracklets.begin(); 
                 it != tracklet_manager.tracklets.end(); it++) { 
                const int64_t feat_id = it->first; 
                const Track& track = it->second; 
                if (track.end()[-1].utime != utime) continue;

                // std::cerr << "FeatID: " << feat_id << " " << track.end()[-1].point2D << std::endl;
                cv::Point2f pt = track.end()[-1].point2D;
                float theta = track.end()[-1].angle * CV_PI / 180;

                // Draw trace of tracklet
                for (int k=track.size()-2, j=0; k>=0; k--, j++)
                    cv::line(dalpha, sc * track[k].point2D, sc * track[k+1].point2D, 
                             CV_RGB(0,0,200), 1, CV_AA, 0);

                double conf = (tracklet_manager.track_confidence[it->first] - min_track_length) * conf_norm;
                h = cv::Vec3b(conf,conf,conf);

                // Draw the keypoint circumference
                circle(dalpha, sc * pt, r, cv::Scalar(h[0],h[1],h[2]), 1,CV_AA);

                // // Draw the keypoint angle
                // cv::line(dalpha, sc * pt, sc * (pt + cv::Point2f(r*cos(theta), r*sin(theta))), 
                //          cv::Scalar(h[0],h[1],h[2]), 2, CV_AA, 0);

                // Draw the center 
                circle(dalpha, sc * pt, 1, cv::Scalar(200,200,200), 1,CV_AA);
                
                // Draw the ID
                putText(dalpha,cv::format("%i",feat_id), sc * pt, 0, 0.3, cv::Scalar(0,200,0),1,CV_AA);
            }
            addWeighted(display, 0.2, dalpha, 0.5, 0, display);

#if OPTICALFLOW
            assert(p_pts.size() == pred_c_pts.size());
            for (int j=0; j<pred_c_pts.size(); j++) {  
                if (!pred_c_status[j]) continue;
                
                // Draw the keypoint angle
                // std::cerr << cv::norm(pred_c_pts[j]-p_pts[j]) << " " << pred_c_pts[j] << "<=" << p_pts[j] << std::endl;
                cv::Vec3b _h = (cv::norm(pred_c_pts[j]-p_pts[j]) < 2 * spvision::GPFT_ADD_POINT_DISTANCE_THRESHOLD) ? h : cv::Vec3b(0,0,100);
                cv::line(dalpha2, sc * p_pts[j], sc * pred_c_pts[j], cv::Scalar(_h[0],_h[1],_h[2]), 1, CV_AA, 0);

                // Draw the predicted keypoint from LK Optical flow
                circle(dalpha2, sc * pred_c_pts[j], 1, cv::Scalar(h[0]/2,h[1]/2,h[2]/2), 1, CV_AA);
            }            
            if (!pimg.empty())
                addWeighted(pimg, 0.5, dalpha2, 0.5, 0, dalpha2);
            else 
                addWeighted(display, 0.5, dalpha2, 0.5, 0, dalpha2);
            opencv_utils::imshow("LK Tracklets", dalpha2);
#endif
        }

        opencv_utils::imshow("Debug Tracklets", display);

        printf("%s===> PLOT KLT: %4.2f ms%s\n", esc_red, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

#endif


        //--------------------------------------------
        // Plot features
        //--------------------------------------------
        tic = bot_timestamp_now();
        tracklet_manager.plot(utime, img);
        printf("%s===> PLOT FEATURES: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 
*/
        //--------------------------------------------
        // Store frame, utime management
        //--------------------------------------------
        pimg = img.clone();

        return;
    }

    // // Display caps
    // GeneralPurposeFeatureTracker::plot_correspondences (const cv::Mat& img, 
    //                                       pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud,
    //                                       pcl::PointCloud<pcl::Normal>::Ptr& normals,
    //                                       pcl::search::KdTree<pcl::PointXYZRGB>::Ptr& tree) { 
    //     assert(mode == TRACK3D || mode == TRACK3D_WITH_NORMALS);


}





// int layers = 3;
// cv::Mat flow; 

// cv::Mat gray, pgray;
// cv::cvtColor(img, gray, CV_BGR2GRAY);
// cv::cvtColor(p_img, pgray, CV_BGR2GRAY);

// cv::calcOpticalFlowSF(p_img, in, flow, layers, LK_winsize.width, 1); 
// cv::imshow("simple flow", flow);

// cv::Ptr<cv::DenseOpticalFlow> pdof = cv::createOptFlow_DualTVL1();
// pdof->calc(pgray, gray, flow);
// std::cerr << flow.size() << " "<< flow.type() << " " << flow.depth() << std::endl;


            // for (std::map<int, int>::iterator it = delvtx_idx.begin(); 
            //      it != delvtx_idx.end(); it++) { 
            //     v1 = it->first; 
                
            //     e1 = -1;
            //     p1 = subdiv.getVertex(v1, &e1);
            //     e2 = subdiv.getEdge(e1, cv::Subdiv2D::PREV_AROUND_DST);
            //     e3 = subdiv.getEdge(e2, cv::Subdiv2D::PREV_AROUND_DST);

            //     v2 = subdiv.edgeDst(e1, NULL);
            //     v3 = subdiv.edgeDst(e2, NULL);
            //     v4 = subdiv.edgeDst(e3, NULL);

            //     assert(v4 == v1);

            //     assert(delvtx_idx.find(v1) != delvtx_idx.end());
            //     assert(delvtx_idx.find(v2) != delvtx_idx.end());
            //     v1idx = delvtx_idx[v1]; 
            //     v2idx = delvtx_idx[v2];
            //     assert(v1idx >=0 && v1idx < c_pts.size());
            //     assert(v2idx >=0 && v2idx < c_pts.size());

            //     p2 = subdiv.getVertex(v2, &e1);
