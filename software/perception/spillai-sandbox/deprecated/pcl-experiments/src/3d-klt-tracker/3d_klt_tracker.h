// Standard includes
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <deque>
#include <map>
#include <set> 

// #include "mrpt_tracker.h"
#include "MRPTTracker.hpp"
#include "FOVISTracker.hpp"
#include "GeneralPurposeFeatureTracker.hpp"

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/glext.h>

// libbot/lcm includes
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <ConciseArgs>

#include <lcm/lcm.h>
#include <lcmtypes/bot2_param.h>
#include "lcmtypes/er_lcmtypes.h" 
// #include <lcmtypes/erlcm_tracklet_list_t.h>

// pcl includes
#include <pcl/io/pcd_io.h>
#include <pcl/PointIndices.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/random_sample.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/surface/mls.h>

// pcl includes for kinect
#include <pointcloud_tools/pointcloud_lcm.hpp>
#include <pointcloud_tools/pointcloud_vis.hpp>

// opencv includes
// #include <opencv2/opencv.hpp>

// opencv includes
#include <opencv2/opencv.hpp>


#include <lcmtypes/vs_obj_collection_t.h>
#include <lcmtypes/vs_obj_t.h>
#include <lcmtypes/vs_link_collection_t.h>
#include <lcmtypes/vs_link_t.h>
#include <lcmtypes/vs_cov_collection_t.h>
#include <lcmtypes/vs_cov_t.h>
#include <lcmtypes/vs_point3d_list_collection_t.h>
#include <lcmtypes/vs_point3d_list_t.h>
#include <lcmtypes/vs_reset_collections_t.h>
#include <lcmtypes/vs_collection_config_t.h>
#include <lcmtypes/vs_text_collection_t.h>
#include <lcmtypes/vs_text_t.h>

// #include "Descriptors.h"
// #include "Initialize.h"
// #include "DenseTrack.h"

// Shape Context Histogram Comparison Method
// CV_COMP_CORREL, CV_COMP_CHISQR, 
// CV_COMP_INTERSECT, CV_COMP_BHATTACHARYYA
const int SC3D_HIST_COMP_METHOD = CV_COMP_CORREL; 

// Decimation of RGB, and point cloud
const double SCALE = 1.0;

// Decimation of depth features for surface normal computation
const double DEPTH_SCALE = 1.0; // DEPTH_SCALE * SCALE would be total scale for points


// Add new 2d features if no points within x pixels
const float ADD_POINT_DISTANCE_THRESHOLD = 3.f;

// Minimum depth eps threshold for track association
const float EPS_DEPTH_CONTINUITY_THRESHOLD = 0.1f; // 10cm

// Minimum time association between features in a tracklet
const float DELTAT_ASSOCIATION = .3f;

const int MIN_TRACK_SIZE = 1;
// Fix this???
cv::TermCriteria termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03);
cv::Size subPixWinSize(10,10), winSize(31,31);

/* IplImageWrapper image, prev_image, grey, prev_grey; */
/* IplImagePyramid grey_pyramid, prev_grey_pyramid, eig_pyramid; */
float* fscales = 0; // float scale values
int show_track = 1; // set show_track = 1, if you want to visualize the trajectories
int frameNum = 0;

// std::vector<std::list<Track> > xyScaleTracks;
int init_counter = 0; // indicate when to detect new feature points


/* struct FeatureTrackerOptions {  */
/*     bool vDEBUG; */
/*     float vSCALE; */
/*     std::string detector_type; */
/*     std::string extractor_type; */
/*     std::string matcher_type; */
/*     std::string vCHANNEL; */

/*     FeatureTrackerOptions () :  */
/*         vCHANNEL(std::string("CAMERALEFT")), vAFFORDANCE_CHANNEL(std::string("CAMERALEFT_MASKZIPPED")),  */
/*         vSCALE(1.f), vAFFORDANCE_ID(64), vDEBUG(false) {} */
/* }; */
/* FeatureTrackerOptions options; */


typedef struct
{
    lcm_t *lcm;
    GMainLoop *mainloop;
    BotParam   *b_server;
    BotFrames *frames;

    pointcloud_lcm* pc_lcm;
    pointcloud_vis* pc_vis;

}state_t;
state_t* state = NULL;
cv::Mat3b rgb_img;


struct Frame { 
    int64_t utime; 
    cv::Mat img;
    Frame() { utime = 0; img = cv::Mat(); }
    Frame(int64_t _utime, const cv::Mat& _img) { 
        utime = _utime; img = _img.clone(); 
    }
};

int OBJECT_ID = 1; 
static int nextObjectID() { 
    return ++OBJECT_ID;
}
static int objectID() { 
    return OBJECT_ID;
}

std::deque<Frame> frame_queue;
std::deque<std::vector<cv::Point2f> > feats_queue;
std::deque<spvision::Track3D > feats_3d_queue;

/* std::map<int, spvision::Track2D > tracklet; */
/* std::map<int, spvision::Track3D > tracklet_3d; */

cv::Mat_<uint16_t> depth_img;
pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud;
pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_decimated;
pcl::search::KdTree<pcl::PointXYZRGB>::Ptr cloud_decimated_tree;
pcl::PointCloud<pcl::Normal>::Ptr cloud_normals;

pcl::PointCloud<pcl::PointNormal>::Ptr cloud_smoothed_;
pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_smoothed;
pcl::PointCloud<pcl::Normal>::Ptr cloud_smoothed_normals;

spvision::Track3D compute_3d_features(const std::vector<cv::Point2f>& feats);
void perform_klt();
void plot_klt();
void prune_tracks();
void cluster_tracks_with_shape_contexts();
void cluster_tracks_with_pca();

int64_t earliest_utime = std::numeric_limits<int64_t>::max();

std::map<int64_t, std::vector<spvision::SuperFeature> > full_klt_features;

/* namespace densetraj { */
/* class DenseTracker {  */
/*     TrackerInfo tracker; */
/*     DescInfo hogInfo; */
/*     DescInfo hofInfo; */
/*     DescInfo mbhInfo; */
/*     IplImageWrapper image, prev_image, grey, prev_grey; */
/*     IplImagePyramid grey_pyramid, prev_grey_pyramid, eig_pyramid; */
/*     std::vector<std::list<Track> > xyScaleTracks; */
/*     int i; */
/*  public: */
/*     DenseTracker () {  */
/*         InitTrackerInfo(&tracker, track_length, init_gap); */
/*         InitDescInfo(&hogInfo, 8, 0, 1, patch_size, nxy_cell, nt_cell); */
/*         InitDescInfo(&hofInfo, 9, 1, 1, patch_size, nxy_cell, nt_cell); */
/*         InitDescInfo(&mbhInfo, 8, 0, 1, patch_size, nxy_cell, nt_cell); */
/*     } */

/*     void init(IplImage* frame) {  */
/*         // initailize all the buffers */
/*         image = IplImageWrapper( cvGetSize(frame), 8, 3 ); */
/*         image->origin = frame->origin; */
/*         prev_image= IplImageWrapper( cvGetSize(frame), 8, 3 ); */
/*         prev_image->origin = frame->origin; */
/*         grey = IplImageWrapper( cvGetSize(frame), 8, 1 ); */
/*         grey_pyramid = IplImagePyramid( cvGetSize(frame), 8, 1, scale_stride ); */
/*         prev_grey = IplImageWrapper( cvGetSize(frame), 8, 1 ); */
/*         prev_grey_pyramid = IplImagePyramid( cvGetSize(frame), 8, 1, scale_stride ); */
/*         eig_pyramid = IplImagePyramid( cvGetSize(frame), 32, 1, scale_stride ); */

/*         cvCopy( frame, image, 0 ); */
/*         cvCvtColor( image, grey, CV_BGR2GRAY ); */
/*         grey_pyramid.rebuild( grey ); */

/*         // how many scale we can have */
/*         scale_num = std::min<std::size_t>(scale_num, grey_pyramid.numOfLevels()); */
/*         fscales = (float*)cvAlloc(scale_num*sizeof(float)); */
/*         xyScaleTracks.resize(scale_num); */

/*         for( int ixyScale = 0; ixyScale < scale_num; ++ixyScale ) { */
/*             std::list<Track>& tracks = xyScaleTracks[ixyScale]; */
/*             fscales[ixyScale] = pow(scale_stride, ixyScale); */

/*             // find good features at each scale separately */
/*             IplImage *grey_temp = 0, *eig_temp = 0; */
/*             std::size_t temp_level = (std::size_t)ixyScale; */
/*             grey_temp = cvCloneImage(grey_pyramid.getImage(temp_level)); */
/*             eig_temp = cvCloneImage(eig_pyramid.getImage(temp_level)); */
/*             std::vector<CvPoint2D32f> points(0); */
/*             cvDenseSample(grey_temp, eig_temp, points, quality, min_distance); */

/*             // save the feature points */
/*             for( i = 0; i < points.size(); i++ ) { */
/*                 Track track(tracker.trackLength); */
/*                 PointDesc point(hogInfo, hofInfo, mbhInfo, points[i]); */
/*                 track.addPointDesc(point); */
/*                 tracks.push_back(track); */
/*             } */

/*             cvReleaseImage( &grey_temp ); */
/*             cvReleaseImage( &eig_temp ); */
/*         } */
/*     } */

/*     void update(cv::Mat& img) {  */
/*         if (img.empty()) return; */
/*         if (img.channels() != 3)  */
/*             std::cerr << "Image not color! " << std::endl; */


/*         IplImage _frame(img);  */
/*         IplImage* frame = &_frame; */
/*         // if( frameNum >= start_frame && frameNum <= end_frame ) { */

/*         // Init */
/*         if (!image) init(frame);  */

/*         // build the image pyramid for the current frame */
/*         cvCopy( frame, image, 0 ); */
/*         cvCvtColor( image, grey, CV_BGR2GRAY ); */
/*         grey_pyramid.rebuild(grey); */

/*         if( frameNum > 0 ) { */
/*             init_counter++; */
/*             for( int ixyScale = 0; ixyScale < scale_num; ++ixyScale ) { */
/*                 // track feature points in each scale separately */
/*                 std::vector<CvPoint2D32f> points_in(0); */
/*                 std::list<Track>& tracks = xyScaleTracks[ixyScale]; */
/*                 for (std::list<Track>::iterator iTrack = tracks.begin(); iTrack != tracks.end(); ++iTrack) { */
/*                     CvPoint2D32f point = iTrack->pointDescs.back().point; */
/*                     points_in.push_back(point); // collect all the feature points */
/*                 } */
/*                 int count = points_in.size(); */
/*                 IplImage *prev_grey_temp = 0, *grey_temp = 0; */
/*                 std::size_t temp_level = ixyScale; */
/*                 prev_grey_temp = cvCloneImage(prev_grey_pyramid.getImage(temp_level)); */
/*                 grey_temp = cvCloneImage(grey_pyramid.getImage(temp_level)); */

/*                 cv::Mat prev_grey_mat = cv::cvarrToMat(prev_grey_temp); */
/*                 cv::Mat grey_mat = cv::cvarrToMat(grey_temp); */

/*                 std::vector<int> status(count); */
/*                 std::vector<CvPoint2D32f> points_out(count); */

/*                 // compute the optical flow */
/*                 IplImage* flow = cvCreateImage(cvGetSize(grey_temp), IPL_DEPTH_32F, 2); */
/*                 cv::Mat flow_mat = cv::cvarrToMat(flow); */
/*                 cv::calcOpticalFlowFarneback( prev_grey_mat, grey_mat, flow_mat, */
/*                                               std::sqrt(2)/2.0, 5, 10, 2, 7, 1.5, cv::OPTFLOW_FARNEBACK_GAUSSIAN ); */
/*                 // track feature points by median filtering */
/*                 OpticalFlowTracker(flow, points_in, points_out, status); */

/*                 int width = grey_temp->width; */
/*                 int height = grey_temp->height; */
/*                 // compute the integral histograms */
/*                 DescMat* hogMat = InitDescMat(height, width, hogInfo.nBins); */
/*                 HogComp(prev_grey_temp, hogMat, hogInfo); */

/*                 DescMat* hofMat = InitDescMat(height, width, hofInfo.nBins); */
/*                 HofComp(flow, hofMat, hofInfo); */

/*                 DescMat* mbhMatX = InitDescMat(height, width, mbhInfo.nBins); */
/*                 DescMat* mbhMatY = InitDescMat(height, width, mbhInfo.nBins); */
/*                 MbhComp(flow, mbhMatX, mbhMatY, mbhInfo); */

/*                 i = 0; */
/*                 for (std::list<Track>::iterator iTrack = tracks.begin(); iTrack != tracks.end(); ++i) { */
/*                     if( status[i] == 1 ) { // if the feature point is successfully tracked */
/*                         PointDesc& pointDesc = iTrack->pointDescs.back(); */
/*                         CvPoint2D32f prev_point = points_in[i]; */
/*                         // get the descriptors for the feature point */
/*                         CvScalar rect = getRect(prev_point, cvSize(width, height), hogInfo); */
/*                         pointDesc.hog = getDesc(hogMat, rect, hogInfo); */
/*                         pointDesc.hof = getDesc(hofMat, rect, hofInfo); */
/*                         pointDesc.mbhX = getDesc(mbhMatX, rect, mbhInfo); */
/*                         pointDesc.mbhY = getDesc(mbhMatY, rect, mbhInfo); */

/*                         PointDesc point(hogInfo, hofInfo, mbhInfo, points_out[i]); */
/*                         iTrack->addPointDesc(point); */

/*                         // draw this track */
/*                         if( show_track == 1 ) { */
/*                             std::list<PointDesc>& descs = iTrack->pointDescs; */
/*                             std::list<PointDesc>::iterator iDesc = descs.begin(); */
/*                             float length = descs.size(); */
/*                             CvPoint2D32f point0 = iDesc->point; */
/*                             point0.x *= fscales[ixyScale]; // map the point to first scale */
/*                             point0.y *= fscales[ixyScale]; */

/*                             float j = 0; */
/*                             for (iDesc++; iDesc != descs.end(); ++iDesc, ++j) { */
/*                                 CvPoint2D32f point1 = iDesc->point; */
/*                                 point1.x *= fscales[ixyScale]; */
/*                                 point1.y *= fscales[ixyScale]; */

/*                                 cvLine(image, cvPointFrom32f(point0), cvPointFrom32f(point1), */
/*                                        CV_RGB(0,cvFloor(255.0*(j+1.0)/length),0), 2, 8,0); */
/*                                 point0 = point1; */
/*                             } */
/*                             cvCircle(image, cvPointFrom32f(point0), 2, CV_RGB(255,0,0), -1, 8,0); */
/*                         } */
/*                         ++iTrack; */
/*                     } */
/*                     else // remove the track, if we lose feature point */
/*                         iTrack = tracks.erase(iTrack); */
/*                 } */
/*                 ReleDescMat(hogMat); */
/*                 ReleDescMat(hofMat); */
/*                 ReleDescMat(mbhMatX); */
/*                 ReleDescMat(mbhMatY); */
/*                 cvReleaseImage( &prev_grey_temp ); */
/*                 cvReleaseImage( &grey_temp ); */
/*                 cvReleaseImage( &flow ); */
/*             } */

/*             for( int ixyScale = 0; ixyScale < scale_num; ++ixyScale ) { */
/*                 std::list<Track>& tracks = xyScaleTracks[ixyScale]; // output the features for each scale */
/*                 for( std::list<Track>::iterator iTrack = tracks.begin(); iTrack != tracks.end(); ) { */
/*                     if( iTrack->pointDescs.size() >= tracker.trackLength+1 ) { // if the trajectory achieves the length we want */
/*                         std::vector<CvPoint2D32f> trajectory(tracker.trackLength+1); */
/*                         std::list<PointDesc>& descs = iTrack->pointDescs; */
/*                         std::list<PointDesc>::iterator iDesc = descs.begin(); */

/*                         for (int count = 0; count <= tracker.trackLength; ++iDesc, ++count) { */
/*                             trajectory[count].x = iDesc->point.x*fscales[ixyScale]; */
/*                             trajectory[count].y = iDesc->point.y*fscales[ixyScale]; */
/*                         } */
/*                         float mean_x(0), mean_y(0), var_x(0), var_y(0), length(0); */
/*                         if( isValid(trajectory, mean_x, mean_y, var_x, var_y, length) == 1 ) { */
/*                             /\* printf("%d\t", frameNum); *\/ */
/*                             /\* printf("%f\t%f\t", mean_x, mean_y); *\/ */
/*                             /\* printf("%f\t%f\t", var_x, var_y); *\/ */
/*                             /\* printf("%f\t", length); *\/ */
/*                             /\* printf("%f\t", fscales[ixyScale]); *\/ */

/*                             /\* for (int count = 0; count < tracker.trackLength; ++count) *\/ */
/*                             /\*     printf("%f\t%f\t", trajectory[count].x,trajectory[count].y ); *\/ */

/*                             iDesc = descs.begin(); */
/*                             int t_stride = cvFloor(tracker.trackLength/hogInfo.ntCells); */
/*                             for( int n = 0; n < hogInfo.ntCells; n++ ) { */
/*                                 std::vector<float> vec(hogInfo.dim); */
/*                                 for( int t = 0; t < t_stride; t++, iDesc++ ) */
/*                                     for( int m = 0; m < hogInfo.dim; m++ ) */
/*                                         vec[m] += iDesc->hog[m]; */
/*                                 /\* for( int m = 0; m < hogInfo.dim; m++ ) *\/ */
/*                                 /\*     printf("%f\t", vec[m]/float(t_stride)); *\/ */
/*                             } */

/*                             iDesc = descs.begin(); */
/*                             t_stride = cvFloor(tracker.trackLength/hofInfo.ntCells); */
/*                             for( int n = 0; n < hofInfo.ntCells; n++ ) { */
/*                                 std::vector<float> vec(hofInfo.dim); */
/*                                 for( int t = 0; t < t_stride; t++, iDesc++ ) */
/*                                     for( int m = 0; m < hofInfo.dim; m++ ) */
/*                                         vec[m] += iDesc->hof[m]; */
/*                                 /\* for( int m = 0; m < hofInfo.dim; m++ ) *\/ */
/*                                 /\*     printf("%f\t", vec[m]/float(t_stride)); *\/ */
/*                             } */

/*                             iDesc = descs.begin(); */
/*                             t_stride = cvFloor(tracker.trackLength/mbhInfo.ntCells); */
/*                             for( int n = 0; n < mbhInfo.ntCells; n++ ) { */
/*                                 std::vector<float> vec(mbhInfo.dim); */
/*                                 for( int t = 0; t < t_stride; t++, iDesc++ ) */
/*                                     for( int m = 0; m < mbhInfo.dim; m++ ) */
/*                                         vec[m] += iDesc->mbhX[m]; */
/*                                 /\* for( int m = 0; m < mbhInfo.dim; m++ ) *\/ */
/*                                 /\*     printf("%f\t", vec[m]/float(t_stride)); *\/ */
/*                             } */

/*                             iDesc = descs.begin(); */
/*                             t_stride = cvFloor(tracker.trackLength/mbhInfo.ntCells); */
/*                             for( int n = 0; n < mbhInfo.ntCells; n++ ) { */
/*                                 std::vector<float> vec(mbhInfo.dim); */
/*                                 for( int t = 0; t < t_stride; t++, iDesc++ ) */
/*                                     for( int m = 0; m < mbhInfo.dim; m++ ) */
/*                                         vec[m] += iDesc->mbhY[m]; */
/*                                 /\* for( int m = 0; m < mbhInfo.dim; m++ ) *\/ */
/*                                 /\*     printf("%f\t", vec[m]/float(t_stride)); *\/ */
/*                             } */

/*                             /\* printf("\n"); *\/ */
/*                         } */
/*                         iTrack = tracks.erase(iTrack); */
/*                     } */
/*                     else */
/*                         iTrack++; */
/*                 } */
/*             } */

/*             if( init_counter == tracker.initGap ) { // detect new feature points every initGap frames */
/*                 init_counter = 0; */
/*                 for (int ixyScale = 0; ixyScale < scale_num; ++ixyScale) { */
/*                     std::list<Track>& tracks = xyScaleTracks[ixyScale]; */
/*                     std::vector<CvPoint2D32f> points_in(0); */
/*                     std::vector<CvPoint2D32f> points_out(0); */
/*                     for(std::list<Track>::iterator iTrack = tracks.begin(); iTrack != tracks.end(); iTrack++, i++) { */
/*                         std::list<PointDesc>& descs = iTrack->pointDescs; */
/*                         CvPoint2D32f point = descs.back().point; // the last point in the track */
/*                         points_in.push_back(point); */
/*                     } */

/*                     IplImage *grey_temp = 0, *eig_temp = 0; */
/*                     std::size_t temp_level = (std::size_t)ixyScale; */
/*                     grey_temp = cvCloneImage(grey_pyramid.getImage(temp_level)); */
/*                     eig_temp = cvCloneImage(eig_pyramid.getImage(temp_level)); */

/*                     cvDenseSample(grey_temp, eig_temp, points_in, points_out, quality, min_distance); */
/*                     // save the new feature points */
/*                     for( i = 0; i < points_out.size(); i++) { */
/*                         Track track(tracker.trackLength); */
/*                         PointDesc point(hogInfo, hofInfo, mbhInfo, points_out[i]); */
/*                         track.addPointDesc(point); */
/*                         tracks.push_back(track); */
/*                     } */
/*                     cvReleaseImage( &grey_temp ); */
/*                     cvReleaseImage( &eig_temp ); */
/*                 } */
/*             } */
/*         } */

/*         cvCopy( frame, prev_image, 0 ); */
/*         cvCvtColor( prev_image, prev_grey, CV_BGR2GRAY ); */
/*         prev_grey_pyramid.rebuild(prev_grey); */
/*         // } */

/*         cv::Mat imgmat(image, true); */
/*         cv::imshow("DenseTrack", imgmat); */
/*         cv::waitKey(10); */
/*         frameNum++; */

/*     } */

/* }; */
/* } */
/* /\* typedef std::map<int64_t, densetraj::DenseTracker*> TrackerMap; *\/ */
/* /\* typedef std::map<int64_t, densetraj::DenseTracker*>::iterator TrackerMapIt; *\/ */
/* /\* TrackerMap trackerMap; *\/ */

// typedef std::map<int64_t, spvision::MRPTTracker*> TrackerMap;
//typedef std::map<int64_t, spvision::MRPTTracker*>::iterator TrackerMapIt;
// TrackerMap trackerMap;
spvision::MRPTTracker MRPTT;

// General purpose Feature Tracker
spvision::GeneralPurposeFeatureTracker GPFT;


/* void reset() {  */
/*     frame_queue.clear();  */
/*     feats_queue.clear(); */
/*     feats_3d_queue.clear(); */
/*     tracklet.clear(); */
/*     tracklet_3d.clear(); */
/* } */
