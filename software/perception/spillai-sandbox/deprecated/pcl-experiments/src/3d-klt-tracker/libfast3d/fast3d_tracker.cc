#include "fast3d_tracker.hpp"

using namespace spvision;
FAST3DTracker::FAST3DTracker() : VERBOSE_MODE(false), _inited(false) { 
    std::cerr << "INITIALIZING FAST3DTRACKER: " << std::endl;
    init();
}

FAST3DTracker::FAST3DTracker(const FAST3DTracker& tr) { 
}

FAST3DTracker::~FAST3DTracker() { 
}

void
FAST3DTracker::setParams(char* str, double value) { 
}


void 
FAST3DTracker::initTracker() { 
    // // get the RGB camera parameters of our device

    // const int width = 640, height = 480; 
    // memset(&rgb_params, 0, sizeof(fovis::CameraIntrinsicsParameters));
    // rgb_params.width = width;
    // rgb_params.height = height;

    // // TODO read these values from the camera somehow, instead of hard-coding it
    // // Unfortunately, the OpenNI API doesn't seem to expose them.
    // rgb_params.fx = 528.49404721; 
    // rgb_params.fy = rgb_params.fx;
    // rgb_params.cx = width / 2.0;
    // rgb_params.cy = height / 2.0;

    // depth_image = new fovis::DepthImage(rgb_params, width, height); 
    // rect = new fovis::Rectification (rgb_params); // cap->getRgbParameters());

    // fovis::Fast3DTrackerOptions options = 
    //     fovis::Fast3DTracker::getDefaultOptions();
   
    // If we wanted to play around with the different VO parameters, we could set
    // them here in the "options" variable.
    // tracker = new fovis::Fast3DTracker(rect, options); 
}

void 
FAST3DTracker::setDefaultParams() { 
    // // Set of parameters common to any tracker implementation:
    // // -------------------------------------------------------------
    // // To see all the existing params and documentation, see mrpt::vision::CGenericFeatureTracker
    // tracker->extra_params["remove_lost_features"]         = 1;   // automatically remove out-of-image and badly tracked features

    // tracker->extra_params["add_new_features"]             = 1;   // track, AND ALSO, add new features
    // tracker->extra_params["add_new_feat_min_separation"]  = 16;
    // tracker->extra_params["minimum_KLT_response_to_add"]  = 10;
    // tracker->extra_params["add_new_feat_max_features"]    = 500;
    // tracker->extra_params["add_new_feat_patch_size"]      = 11;

    // tracker->extra_params["update_patches_every"]		= 0;  // Don't update patches.

    // tracker->extra_params["check_KLT_response_every"]	= 5;	// Re-check the KLT-response to assure features are in good points.
    // tracker->extra_params["minimum_KLT_response"]	    = 5;

    // // Specific params for "CFeatureTracker_KL"
    // // ------------------------------------------------------
    // tracker->extra_params["window_width"]  = 5;
    // tracker->extra_params["window_height"] = 5;
    // //tracker->extra_params["LK_levels"] = 3;
    // //tracker->extra_params["LK_max_iters"] = 10;
    // //tracker->extra_params["LK_epsilon"] = 0.1;
    // //tracker->extra_params["LK_max_tracking_error"] = 150;
   return; 
}

void
FAST3DTracker::reinit() { 
    pimg = cv::Mat();
    _inited = false; 
}

void
FAST3DTracker::init() { 
    initTracker(); 
    setDefaultParams(); 
    _inited = true; 
}

// template <typename T>
void
FAST3DTracker::update(int64_t utime, cv::Mat& img, cv::Mat& depth, 
                    pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, 
                    pcl::PointCloud<pcl::Normal>::Ptr& normals,
                    pcl::search::KdTree<pcl::PointXYZRGB>::Ptr& tree) { 

    if (img.empty()) return; 

    // Track Clean up before update
    std::vector<int64_t> erase_ids; 
    for (TrackMapIt it = tracklet.begin(); 
         it != tracklet.end(); it++) { 
        Track& track = it->second;
        int64_t latest_utime = track.end()[-1].utime; 
        if (utime < latest_utime) 
            erase_ids.push_back(it->first); 
    }
    for (int j=0; j<erase_ids.size(); j++) { 
        std::cerr << "ERASING (RESET!)" << erase_ids[j] << std::endl;
        tracklet.erase(erase_ids[j]); 
        // pimg = cv::Mat(); img = cv::Mat(); 
        reinit(); 
    }

    // Current image available
    depth_image->setDepthImage((float*)depth.data); 
    tracker->processFrame(img.data, depth_image);

    // // Convert to Features2D/Features3D
    // std::vector<SuperFeature> feats = convertToFeatures(utime, simple_feats, cloud, normals, tree); 
    // std::cerr << "feats size: " << feats.size() << std::endl;

    // // add new points with id to the tracklet (update utime)
    // std::set<int64_t> observed_ids; 
    // for (int j=0; j<feats.size(); j++) { 
    //     SuperFeature& feat = feats[j]; 
    //     observed_ids.insert(feat.id);

    //     // std::cerr << "featID: " << feat.id << std::endl;
    //     feat.utime = utime; 
    //     tracklet[feat.id].push_back(feat);
    // }
    // std::cerr << "===> Tracklets: " << tracklet.size() << std::endl;

    // // prune so that length is only MAX_TRACK_TIMESPAN seconds long
    // for (TrackMapIt it = tracklet.begin(); 
    //      it != tracklet.end(); it++) { 
    //     Track& track = it->second;
    //     if (!track.size()) continue;
    //     int64_t latest_utime = track.end()[-1].utime;
    //     int64_t first_utime = track.begin()[0].utime;
    //     // assert(latest_utime > first_utime);
    //     if (latest_utime - first_utime > MAX_TRACK_TIMESPAN * 1e6) { 
    //         int idx = track.size()-1; 
    //         for (; idx>=0; idx--) 
    //             if (latest_utime - track[idx].utime > MAX_TRACK_TIMESPAN * 1e6)
    //                 break;
    //         track.erase(track.begin(), track.begin() + idx + 1);
    //     }
    // }

    // // // Remove all tracks whose most recent feature is lagging by more 
    // // // than MIN_TRACK_TIMESTAMP_DELAY seconds from the most recent timestamp
    // // // Should take care of resetting as well
    // // for (TrackMapIt it = tracklet.begin(); 
    // //      it != tracklet.end(); it++) { 
    // //     Track& track = it->second;
    // //     if (!track.size()) continue;

    // //     int64_t latest_utime = track.end()[-1].utime;
    // //     if (utime - latest_utime > MAX_TRACK_TIMESTAMP_DELAY * 1e6) { 
    // //         tracklet.erase(it);
    // //     } 
    // // }

    // // Mark as invalid if not observed
    // for (TrackMapIt it = tracklet.begin(); 
    //      it != tracklet.end(); it++) { 
    //     Track& track = it->second;
    //     if (observed_ids.find(it->first) == observed_ids.end())
    //         track.valid = false;
    // }

    // // Mark as valid if observed
    // for (std::set<int64_t>::iterator it = observed_ids.begin(); it != observed_ids.end(); it++) { 
    //     if (tracklet.find(*it) != tracklet.end())
    //         tracklet[*it].valid = true;
    // }

    pimg = img.clone();
    return; 
}

void 
FAST3DTracker::normalEstimation(std::vector<SuperFeature>& feats, 
                              const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, 
                              const std::vector<int>& _inds) { 

    // // Normal estimation
    // pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> ne;
    // // ne.setNumberOfThreads(2);
    // ne.setInputCloud(cloud);
    
    // boost::shared_ptr<vector<int> > inds (new vector<int> ());
    // inds->insert(inds->begin(), _inds.begin(), _inds.end());
    // pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
    // pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree_n (new pcl::search::KdTree<pcl::PointXYZRGB>());
    // ne.setSearchMethod(tree_n);
    // ne.setRadiusSearch(0.2);
    // // ne.setKSearch(3);
    // ne.setIndices(inds); 
    // ne.compute(*cloud_normals);

    // assert(cloud_normals->points.size() == feats.size());
    // for (int j=0; j<feats.size(); j++) { 
    //     feats[j].normal = cv::Vec3f(cloud_normals->points[j].normal[0],
    //                             cloud_normals->points[j].normal[1],
    //                             cloud_normals->points[j].normal[2]);
    //     // if (j < 10) 
    //     //     std::cerr << feats[j].normal << std::endl;
    // }
    // std::cout << "Estimated the normals: " << cloud_normals->points.size() << std::endl;

    return;
}


// std::vector<SuperFeature>
// FAST3DTracker::convertToFeatures(int64_t utime, const TSimpleFeatureList& _feats, 
//                                pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, 
//                                pcl::PointCloud<pcl::Normal>::Ptr& normals, 
//                                pcl::search::KdTree<pcl::PointXYZRGB>::Ptr& tree) { 

//     std::vector<SuperFeature> feats; 
//     if (!_feats.size()) return feats; 

//     if (!tree) 
//         std::cerr << "kD-TREE NOT INITIALIZED" << std::endl;

//     std::cerr << "_FEATS: " << _feats.size() << std::endl;
//     std::vector<int> inds(_feats.size()); 
//     for (int j=0; j<_feats.size(); j++) { 
//         SuperFeature feat; 
//         int idx = int(_feats[j].pt.y)*cloud->width + int(_feats[j].pt.x);
//         if (idx < 0 || idx > cloud->points.size()) { 
//             continue;
//         }
//         inds[feats.size()] = idx;

//         pcl::PointXYZRGB pt = cloud->points[idx];
//         if (pt.z < 0) continue;
        
//         // Search for the feature in the decimated point cloud, and retrieve its normal
//         std::vector<int> kinds; 
//         std::vector<float> kdists; 
//         tree->nearestKSearch(pt, 1, kinds, kdists); 

//         feat.utime = utime;
//         feat.id = _feats[j].ID;
//         feat.response = _feats[j].response;
//         feat.point2D.x = _feats[j].pt.x;
//         feat.point2D.y = _feats[j].pt.y;        
//         feat.point3D.x = pt.x, feat.point3D.y = pt.y, feat.point3D.z = pt.z;
//         if (kinds.size() && kinds[0] >= 0 && kinds[0] < normals->points.size()) { 
//             feat.normal = cv::Vec3f(normals->points[kinds[0]].normal[0], 
//                                     normals->points[kinds[0]].normal[1], 
//                                     normals->points[kinds[0]].normal[2]);
//         } else { 
//             std::cerr << "kinds: " << kinds[0] << std::endl;
//             feat.normal = cv::Vec3f(0,0,1); // no normal computation
//         }
//         feats.push_back(feat); 
//     }
//     std::cerr << "cloud_decimated: " << normals->points.size() << std::endl;
//     inds.resize(feats.size());

//     std::cerr << "Normal FEATS: " << inds.size() << std::endl;
//     // normalEstimation(feats, cloud, inds);

//     return feats; 
// }
