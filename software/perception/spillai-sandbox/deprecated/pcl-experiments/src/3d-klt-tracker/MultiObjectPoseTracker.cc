#include "MultiObjectPoseTracker.hpp"
#include "spectralClustering.hpp"

// 
// opencv-utils includes
#include <perception_opencv_utils/calib_utils.hpp>
#include <perception_opencv_utils/imshow_utils.hpp>
#include <perception_opencv_utils/math_utils.hpp>
#include <perception_opencv_utils/plot_utils.hpp>
#include <perception_opencv_utils/color_utils.hpp>
#include <perception_opencv_utils/imgproc_utils.hpp>

namespace spvision { 

    struct PoseClusteringAlgorithm { 
        MultiObjectPoseTracker* MOPT;
        bool valid; 

        std::vector<int64_t> track_id_map; 
        cv::Mat_<float> desc_vec_mat; 
        cv::Mat_<float> adj_mat;

        pcl::search::KdTree<pcl::PointXYZRGB>::Ptr knn_tree; // geodesic ??
        cv::Mat_<uchar> knn_mask; 


        pcl::PointCloud<pcl::PointXY>::Ptr endeff_track;
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr endeff_track_cloud;  
        pcl::PointCloud<pcl::Normal>::Ptr endeff_track_cloud_tangents; 

        std::vector<std::vector<int> > knn_inds; 
        std::vector<std::vector<float> > knn_dists; 

        std::vector<std::vector<int> > rnn_inds; 
        std::vector<std::vector<float> > rnn_dists; 

        std::vector<int> labels; 
        PoseClusteringAlgorithm(MultiObjectPoseTracker* _MOPT) : valid(false), MOPT(_MOPT) {}
        ~PoseClusteringAlgorithm() {}
        void set_tracklets() { 
            int NUM_VALID_TRACKLETS = 0; 
            for (TrackletMapIt it = MOPT->tracklet_manager.tracklets.begin(); 
                 it != MOPT->tracklet_manager.tracklets.end(); it++)
                if (it->second.valid && 
                    cv::sum(cv::Mat(it->second.desc))[0] > 0 && 
                    it->second.utimes.back() == MOPT->utime_now) 
                    NUM_VALID_TRACKLETS++; 
            std::cerr << " NUMBER OF VALID TRACKS: " << NUM_VALID_TRACKLETS << std::endl;
            if (NUM_VALID_TRACKLETS < 2) return; 

            valid = true; 

            //----------------------------------
            // Descriptors for NUM_VALID_TRACKLETS points
            //----------------------------------
            track_id_map = std::vector<int64_t> (NUM_VALID_TRACKLETS); 
            desc_vec_mat = cv::Mat_<float>::zeros(NUM_VALID_TRACKLETS, DESC_SIZE); 
            endeff_track_cloud = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
            endeff_track_cloud_tangents = pcl::PointCloud<pcl::Normal>::Ptr(new pcl::PointCloud<pcl::Normal>);

            std::cerr << "Desc Vec Mat: " << NUM_VALID_TRACKLETS << "x" << DESC_SIZE << std::endl; 
            {
                int idx = 0; 
                for (TrackletMapIt it = MOPT->tracklet_manager.tracklets.begin(); it != MOPT->tracklet_manager.tracklets.end(); it++) { 
                    int64_t TRACK_ID = it->first;
                    Tracklet& tracklet = it->second; 

                    if (cv::sum(cv::Mat(it->second.desc))[0] == 0) continue;
                    if (!(it->second.valid)) continue;
                    if (tracklet.utimes.back() != MOPT->utime_now) continue;


                    //----------------------------------
                    // Sampled track , Sampled track refined (tangent projected), 
                    // Sampled track normals, Sampled track tangents, 
                    // Full track (for tangent computation), 
                    //----------------------------------
                    pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud = tracklet.tracks;
                    pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_tangents = tracklet.tangents;


                    //----------------------------------
                    // Track IDs for the tracks
                    //----------------------------------
                    track_id_map[idx] = TRACK_ID; 

                    //----------------------------------
                    // Fill descriptors
                    //----------------------------------
                    assert(it->second.desc.size() == DESC_SIZE); 
                    for (int k=0; k<tracklet.desc.size(); k++)         
                        desc_vec_mat(idx,k) = tracklet.desc[k];


                    // //----------------------------------
                    // // Endeffector kdtree for knn mask
                    // //----------------------------------
                    // opencv_utils::tic();
                    // track_compute_endeffectors(); 
                    // opencv_utils::toc("COMPUTE: ENDEFFECTORS");

                    //----------------------------------
                    // Last point needs to be from the current timestamp
                    //----------------------------------
                    std::deque<int64_t>& utimes = tracklet.utimes; 

                    //----------------------------------
                    // K nearest neighbor search for last point in trajectory (index)
                    //----------------------------------
                    pcl::PointXYZRGB last_pt; 
                    last_pt.x = track_cloud->points.end()[-1].x, 
                        last_pt.y = track_cloud->points.end()[-1].y, 
                        last_pt.z = track_cloud->points.end()[-1].z;
                    endeff_track_cloud->points.push_back(last_pt);
        
                    pcl::Normal last_tgt; 
                    last_tgt.normal[0] = track_cloud_tangents->points.end()[-1].normal[0], 
                        last_tgt.normal[1] = track_cloud_tangents->points.end()[-1].normal[1],
                        last_tgt.normal[2] = track_cloud_tangents->points.end()[-1].normal[2];
                    endeff_track_cloud_tangents->points.push_back(last_tgt);

                    idx++;
                }
                std::cerr << "idx: " << idx << std::endl;

            }
            /*
            //----------------------------------
            // PCA on shape context
            //----------------------------------
            cv::Mat sc_mean;
            cv::PCA sc_pca(desc_vec_mat, sc_mean, CV_PCA_DATA_AS_ROW, 3); 
            cv::Mat sc_reduced(desc_vec_mat.rows, 3, desc_vec_mat.type());
            sc_pca.project(desc_vec_mat, sc_reduced);
            cv::normalize(sc_reduced, sc_reduced, 0.f, 1.f, cv::NORM_MINMAX);

            cv::Mat_<float> c1 = sc_reduced.col(0); 
            std::vector<float> c1vec(c1.data, c1.data + sc_reduced.rows);
            std::nth_element(c1vec.begin(), c1vec.begin() + c1vec.size()/2, c1vec.end()); 

            cv::Mat_<float> c2 = sc_reduced.col(1); 
            std::vector<float> c2vec(c2.data, c2.data + sc_reduced.rows);
            std::nth_element(c2vec.begin(), c2vec.begin() + c2vec.size()/2, c2vec.end()); 

            cv::Mat_<float> c3 = sc_reduced.col(2); 
            std::vector<float> c3vec(c3.data, c3.data + sc_reduced.rows);
            std::nth_element(c3vec.begin(), c3vec.begin() + c3vec.size()/2, c3vec.end()); 
        
            sc_reduced.col(0) -= c1vec[c1vec.size()/2];
            sc_reduced.col(1) -= c2vec[c2vec.size()/2];
            sc_reduced.col(2) -= c3vec[c3vec.size()/2];

            // std::cerr << "desc_vec: " << desc_vec_mat << std::endl;
            std::cerr << "reduced_vec: " << sc_reduced << std::endl;

            for (int j=0; j<desc_vec_mat.rows; j++) { 
            cv::Mat coeffs = sc_reduced.row(j);
           
            int64_t TRACK_ID = track_id_map[j];
            TrackPointCloud& tracklet = track_list[TRACK_ID]; 
            
            //----------------------------------
            // Sampled track (Display shape context heatmap)
            //----------------------------------
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud = tracklet.tracks;            
            track_cloud->points.end()[-1].r = coeffs.at<float>(0), 
            track_cloud->points.end()[-1].g = coeffs.at<float>(1), 
            track_cloud->points.end()[-1].b = coeffs.at<float>(2);
            // std::cerr << track_cloud->points.end()[-1] << std::endl;

            }
            */
        }

        bool build_knn_tree() { 
            if (!valid) return false; 
            //----------------------------------
            // Build k-nn sparse graph (only in x,y), no depth
            //----------------------------------
            knn_tree = pcl::search::KdTree<pcl::PointXYZRGB>::Ptr(new pcl::search::KdTree<pcl::PointXYZRGB> ());
            endeff_track_cloud->height = 1;
            endeff_track_cloud->width = endeff_track_cloud->points.size();

            // endeff_track = pcl::PointCloud<pcl::PointXY>::Ptr(new pcl::PointCloud<pcl::PointXY>());
            // endeff_track->height = endeff_track_cloud->height;
            // endeff_track->width = endeff_track_cloud->width;
            // endeff_track->points.resize(endeff_track_cloud->points.size());
            // for (int j=0; j<endeff_track_cloud->points.size(); j++) 
            //     endeff_track->points[j].x = endeff_track_cloud->points[j].x, 
            //         endeff_track->points[j].y = endeff_track_cloud->points[j].y;         
            knn_tree->setInputCloud(endeff_track_cloud);
            return true; 
        }

        bool build_knn_mask() { 
            if (!valid) return false;
            //----------------------------------
            // Build k-nn mask 
            //----------------------------------
            const int N = desc_vec_mat.rows; 
            knn_mask = cv::Mat_<uchar>::zeros(N, N);

            knn_inds = std::vector<std::vector<int> > (N); 
            knn_dists = std::vector<std::vector<float> > (N); 

            for (int j=0; j<endeff_track_cloud->points.size(); j++) { 
                int KNN = 10;
                std::vector<int> inds;
                std::vector<float> dists;
                if ( knn_tree->nearestKSearch (endeff_track_cloud->points[j], KNN, inds, dists) > 0 ) { 
                    // Find median dist and only add neighbors within eps ball of 2 * median dist radius
                    float median_dist = dists[dists.size()/2];
                    for (int k=0; k<dists.size(); k++) 
                        if (dists[k] < 4 * median_dist) { 
                            knn_inds[j].push_back(inds[k]);
                            knn_dists[j].push_back(dists[k]);
                        }
                    // std::cerr << "knn: " << cv::Mat_<int>(inds) << std::endl;
                }
                for (int k=0; k<knn_inds[j].size(); k++) 
                    knn_mask(j,knn_inds[j][k]) = 1, knn_mask(knn_inds[j][k],j) = 1;
            }
            return true;
            // std::cerr << "knn_mask: " << cv::Mat_<int>(knn_mask) << std::endl;
        }

        bool build_rnn_inds() { 
            if (!valid) return false;
            //----------------------------------
            // Build radius-bubble matrix 
            //----------------------------------
            const int N = desc_vec_mat.rows;
            rnn_inds = std::vector<std::vector<int> > (N); 
            rnn_dists = std::vector<std::vector<float> > (N); 
            assert(endeff_track_cloud->points.size() == N);
            for (int j=0; j<endeff_track_cloud->points.size(); j++) { 
                float radius_search = varRNN_RADIUS;
                std::vector<int> inds;
                std::vector<float> dists;
                if ( knn_tree->radiusSearch (endeff_track_cloud->points[j], radius_search, inds, dists) > 0 ) { 
                    rnn_inds[j] = inds;
                    rnn_dists[j] = dists;
                    // std::cerr << "knn: " << cv::Mat_<int>(inds) << std::endl;
                }
            }
            return true; 
        }
    
        bool build_adjacency() { 
            //----------------------------------
            // Spectral Clustering using histogram intersection
            //----------------------------------
            // opencv_utils::tic();
            HistogramSimilarity<float> dist_func(DESC_SIZE, HIST_COMP_METHOD); 

            // ----------------------------------
            // Sparse k-nn graph
            // ----------------------------------
            if (!knn_mask.empty())
                adjacencyMatrix(desc_vec_mat, adj_mat, dist_func, knn_mask);
            else 
                return false; 

            // std::cerr << esc_green << "DESC VEC MAT: " << std::endl;
            // for (int j=0; j<desc_vec_mat.rows; j++) {
            //     std::cerr << track_id_map[j] << ": " << j << ": ";
            //     for (int k=0; k<desc_vec_mat.cols; k++) 
            //         std::cerr << desc_vec_mat(j,k) << ", "; 
            //     std::cerr << std::endl;
            // }
            // std::cerr << esc_def << std::endl;

            vs_point3d_list_collection_t viz_track_shape_cloud_msg;
            viz_track_shape_cloud_msg.id = 1010; 
            viz_track_shape_cloud_msg.name = "POSE_EST | TRACK_CLOUD_SHAPE_SIMILARITY"; 
            viz_track_shape_cloud_msg.type = VS_POINT3D_LIST_COLLECTION_T_LINES; 
            viz_track_shape_cloud_msg.reset = true; 

            std::vector<vs_point3d_list_t> viz_list_(1); 
            vs_point3d_list_t& viz_list = viz_list_[0];

            // Viz List
            viz_list.nnormals = 0; 
            viz_list.normals = NULL; 
            viz_list.npointids = 0; 
            viz_list.pointids = NULL; 

            uint64_t time_id = (uint64_t)bot_timestamp_now(); 
            viz_list.id = time_id; 
            viz_list.collection = 100; 
            viz_list.element_id = 1;

            std::vector<vs_point3d_t> edges; 
            std::vector<vs_color_t> edges_c;

            const int N = endeff_track_cloud->points.size(); 
            vs_point3d_t pt; 
            vs_color_t col;
       
            int idx=0; 
            for (int j=0; j<desc_vec_mat.rows; j++) { 
                for (int k=j+1; k<desc_vec_mat.rows; k++) { 
                    if (!knn_mask(j,k)) continue;
                    float d = adj_mat(j,k);
                    cv::Vec3f color(d, 0, 0);

                    pt.x = endeff_track_cloud->points[j].x;
                    pt.y = endeff_track_cloud->points[j].y;
                    pt.z = endeff_track_cloud->points[j].z;
                    col.r = color[0];
                    col.g = color[1];
                    col.b = color[2];
                    edges.push_back(pt);
                    edges_c.push_back(col);

                    pt.x = endeff_track_cloud->points[k].x;
                    pt.y = endeff_track_cloud->points[k].y;
                    pt.z = endeff_track_cloud->points[k].z;
                    col.r = color[0];
                    col.g = color[1];
                    col.b = color[2];
                    edges.push_back(pt);
                    edges_c.push_back(col);
                }
            }
            viz_list.npoints = edges.size();
            viz_list.points = &edges[0];

            viz_list.ncolors = edges_c.size(); 
            viz_list.colors = &edges_c[0];

            // Finish up viz
            viz_track_shape_cloud_msg.nlists = viz_list_.size();
            viz_track_shape_cloud_msg.point_lists = &viz_list_[0];

            vs_point3d_list_collection_t_publish(MOPT->lcm, "POINTS_COLLECTION", &viz_track_shape_cloud_msg);

            // // Matlab agglomerative clustering
            // matlab.put("adj_mat", adj_mat); 
            // std::stringstream ss; 
            // ss << "adj_mat = 1-adj_mat; "
            //    << "adj_mat = adj_mat .* (ones(size(adj_mat)) - eye(size(adj_mat)));"
            //    << "adj_mat_vec = squareform(adj_mat, 'tovector');"
            //    << "Z = linkage(adj_mat_vec, 'average');"
            //    << "c = cluster(Z, 'maxclust', 4); "
            //    << "dendrogram(Z, 'colorThreshold', 'default'); "; 
            // matlab.eval(ss.str().c_str());
            // matlab.keyboard();
            return true; 
        }

        bool ransac_with_priors() { 

            // Pick the most dominant edge weight, 
            // find its cluster based on ransac
            for (int j=0; desc_vec_mat.rows; j++) { 
                const int64_t TRACK_ID = track_id_map[j];

            }



        }

        bool perform_rnn_spectral_clustering() { 

            //----------------------------------
            // Spectral clustering
            // Main recursive clustering routine
            //----------------------------------
            // - Run until all the points have been clustered
            // - Randomly pick a point, find points within radius of say 1.f, 
            // build dense adjacency matrix, find the best cluster within this set of points.
            // Cluster would be the smaller cut with higher normalized energy, i.e. the 2 clusters
            // would have the same assoc. energy, but the smaller cluster would have a higher 
            // normalized assoc. energy. 
            // - Set this high energy cluster as labeled, and redo clustering

            const int N = desc_vec_mat.rows; 
            assert(N == track_id_map.size()); 

            double tic = bot_timestamp_now();
            int label_id = 1;

            std::set<int> pending;
            for (int j=0; j<N; j++) pending.insert(j);

            labels = std::vector<int>(N,0);
            while (pending.size() && pending.size() > .1 * labels.size()) { 

#if 0
                std::set<int>::iterator it = pending.begin(); 
                int idx = *it; 
#else
                // Sort by energy statistic/length
                std::vector<std::pair<int, int> > _pending;
                for (std::set<int>::iterator it1 = pending.begin(); it1 != pending.end(); it1++) { 
                    int idx = *it1;
                    int64_t TRACK_ID = track_id_map[idx];
                    TrackletMapIt itp = MOPT->tracklet_manager.tracklets.find(TRACK_ID); 
                    assert (itp != MOPT->tracklet_manager.tracklets.end());

                    _pending.push_back(std::make_pair(idx, int(MOPT->tracklet_manager.tracklets[TRACK_ID].tracks_full->points.size())));
                }
                std::nth_element(_pending.begin(), _pending.begin(), _pending.end(), sort_pending_by_length_desc);

                int idx = _pending.begin()[0].first; 
                int track_length = _pending.begin()[0].second; 
#endif

                std:: cerr << "********> idx: " << idx /*<< " track length: " << track_length */
                           << " pending: " << pending.size() << std::endl;

                if (labels[idx]) continue;
        
                // nearest neighbors to pt 
                const std::vector<int>& _rnn = rnn_inds[idx];
                const std::vector<float>& _rnnd = rnn_dists[idx];

                // nearest neighbors - labeled points
                std::vector<int> rnn; 
                std::vector<float> rnnd;
                for (int j=0; j<_rnn.size(); j++)
                    if (!labels[_rnn[j]]) 
                        rnn.push_back(_rnn[j]), rnnd.push_back(_rnnd[j]);
        

                // continue if all have been labeled
                if (rnn.size() < 2) { pending.erase(idx); std::cerr << "no rnn: " << std::endl; continue;}

                double max_dist = *(std::max_element(rnnd.begin(), rnnd.end()));

                // adjacency matrix of all points within the radius
                cv::Mat_<float> rnn_adj_mat(rnn.size(), rnn.size());
                for (int k=0; k<rnn.size(); k++) 
                    for (int l=0; l<rnn.size(); l++) 
                        rnn_adj_mat(k,l) = adj_mat(rnn[k],rnn[l]); //  * gaussian(0.5f * (rnnd[k]+rnnd[l]), 0.f, .5 * max_dist);

                int K = 2;
                cv::Mat_<int> rnn_labels(rnn_adj_mat.rows, 1);

                // auto-tuned 0-way cut (K=1) or 1-way cut (K=2)
                cut_info NcutInfo = spectralClustering(rnn_adj_mat, rnn_labels, K, LAPLACIAN_SHI_MALIK);
        
                if(!NcutInfo.size()) {std::cerr << "nocutinfo: " << rnn.size() << std::endl; continue;}
                // std::cerr << "rnn: " << cv::Mat(rnn) << " " << rnn.size() << std::endl;
                // std::cerr << "rnnd: " << cv::Mat(rnnd) << " " << rnn.size() << std::endl;
                // std::cerr << "rnninds: " << rnn.size() << " " << rnn_labels << std::endl;
                // std::cerr << "inds: " << NcutInfo[0].cutInds.size() << " " << cv::Mat(NcutInfo[0].cutInds) << std::endl;

                const std::vector<int>& cutinds = NcutInfo[0].cutInds;
                // std::cerr << "pending bef: " << cv::Mat(std::vector<int>(pending.begin(), pending.end())) << std::endl;
                // std::cerr << "cutinds: " << cv::Mat(cutinds) << std::endl;
                // std::cerr << "cutidx: ";
                // for (int k=0; k<cutinds.size(); k++) 
                //     std::cerr << rnn[cutinds[k]] << "; ";
                // std::cerr << std::endl;
                // std::cerr << "score: " << NcutInfo[0].score << std::endl;
                for (int k=0; k<cutinds.size(); k++) { 
                    int _idx = rnn[cutinds[k]];
                    assert(!labels[_idx]);
                    labels[_idx] = label_id;

                    assert(_idx >=0 && idx < track_id_map.size()); 
                    int64_t TRACK_ID = track_id_map[_idx]; 
                    assert(MOPT->tracklet_manager.tracklets.find(TRACK_ID) != MOPT->tracklet_manager.tracklets.end()); 
                    assert(MOPT->tracklet_manager.tracklets[TRACK_ID].valid); 
                    MOPT->tracklet_manager.tracklets[TRACK_ID].cluster_id = label_id; 

                    std::cerr << "LABEL=> " << TRACK_ID << ": " << label_id << std::endl;
                    std::set<int>::iterator _it = pending.find(_idx);
                    assert(_it != pending.end());
                    pending.erase(_it);
                }
                // std::cerr << "pending aft: " << cv::Mat(std::vector<int>(pending.begin(), pending.end())) << std::endl;
                label_id++;
            }
            printf("%s===> SPECTRAL_CLUSTERING: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 
            // std::cerr << "LABELS: " << cv::Mat(labels) << std::endl;


        }

        bool perform_scgp_spectral_clustering() { 

            //----------------------------------
            // Spectral clustering
            // Main recursive clustering routine
            //----------------------------------
            // - Run until all the points have been clustered
            // - Randomly pick a point, find points within radius of say 1.f, 
            // build dense adjacency matrix, find the best cluster within this set of points.
            // Cluster would be the smaller cut with higher normalized energy, i.e. the 2 clusters
            // would have the same assoc. energy, but the smaller cluster would have a higher 
            // normalized assoc. energy. 
            // - Set this high energy cluster as labeled, and redo clustering

            const int N = desc_vec_mat.rows; 
            assert(N == track_id_map.size()); 

            double tic = bot_timestamp_now();
            int label_id = 1;

            std::set<int> pending;
            for (int j=0; j<N; j++) pending.insert(j);

            labels = std::vector<int>(N,0);
            while (pending.size() && pending.size() > .1 * labels.size()) { 

#if 0
                std::set<int>::iterator it = pending.begin(); 
                int idx = *it; 
#else
                // Sort by energy statistic/length
                std::vector<std::pair<int, int> > _pending;
                for (std::set<int>::iterator it1 = pending.begin(); it1 != pending.end(); it1++) { 
                    int idx = *it1;
                    int64_t TRACK_ID = track_id_map[idx];
                    TrackletMapIt itp = MOPT->tracklet_manager.tracklets.find(TRACK_ID); 
                    assert (itp != MOPT->tracklet_manager.tracklets.end());

                    _pending.push_back(std::make_pair(idx, int(MOPT->tracklet_manager.tracklets[TRACK_ID].tracks_full->points.size())));
                }
                std::nth_element(_pending.begin(), _pending.begin(), _pending.end(), sort_pending_by_length_desc);

                int idx = _pending.begin()[0].first; 
                int track_length = _pending.begin()[0].second; 
#endif

                std:: cerr << "********> idx: " << idx /*<< " track length: " << track_length */
                           << " pending: " << pending.size() << std::endl;

                cv::Mat_<float> pending_adj_mat(pending.size(), pending.size());
                std::vector<int> pending_inds(pending.begin(), pending.end());
                for (int j=0; j<pending_inds.size(); j++) 
                    for (int k=0; k<pending_inds.size(); k++) { 
                        assert(!labels[pending_inds[j]]);
                        pending_adj_mat(j,k) = adj_mat(pending_inds[j], pending_inds[k]);
                    }

                int K = 2;
                cv::Mat_<int> pending_labels(pending_adj_mat.rows, 1);

                // auto-tuned 0-way cut (K=1) or 1-way cut (K=2)
                cut_info NcutInfo = spectralClustering(pending_adj_mat, pending_labels, K, LAPLACIAN_SHI_MALIK);
        
                if(!NcutInfo.size()) {std::cerr << "nocutinfo: " << std::endl; continue;}
                std::cerr << "inds: " << NcutInfo[0].cutInds.size() << " " << cv::Mat(NcutInfo[0].cutInds) << std::endl;

                const std::vector<int>& cutinds = NcutInfo[0].cutInds;
                std::cerr << "pending bef: " << cv::Mat(std::vector<int>(pending.begin(), pending.end())) << std::endl;
                std::cerr << "cutinds: " << cv::Mat(cutinds) << std::endl;
                std::cerr << "cutidx: ";
                for (int k=0; k<cutinds.size(); k++) 
                    std::cerr << pending_inds[cutinds[k]] << "; ";
                std::cerr << std::endl;
                std::cerr << "score: " << NcutInfo[0].score << std::endl;
                for (int k=0; k<cutinds.size(); k++) { 
                    int _idx = pending_inds[cutinds[k]];
                    assert(!labels[_idx]);
                    labels[_idx] = label_id;

                    assert(_idx >=0 && idx < track_id_map.size()); 
                    int64_t TRACK_ID = track_id_map[_idx]; 
                    assert(MOPT->tracklet_manager.tracklets.find(TRACK_ID) != MOPT->tracklet_manager.tracklets.end()); 
                    assert(MOPT->tracklet_manager.tracklets[TRACK_ID].valid); 
                    MOPT->tracklet_manager.tracklets[TRACK_ID].cluster_id = label_id; 
                    std::cerr << "LABEL=> " << TRACK_ID << ": " << label_id << std::endl;


                    std::set<int>::iterator _it = pending.find(_idx);
                    assert(_it != pending.end());
                    pending.erase(_it);
                }
                std::cerr << "pending aft: " << cv::Mat(std::vector<int>(pending.begin(), pending.end())) << std::endl;
                label_id++;
            }
            printf("%s===> SPECTRAL_CLUSTERING: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 
            // std::cerr << "LABELS: " << cv::Mat(labels) << std::endl;


        }


        void perform_pose_estimation_from_clusters(TrackletMap& track_list, BotTrans& sensor_pose) { 
            // ID to [vector of idx] map
            std::map<int, std::vector<int64_t> > id_map;
            for (TrackletMapIt it = track_list.begin(); it != track_list.end(); it++) { 
                if (!it->second.valid) continue;
                int64_t TRACK_ID = it->first; 
                id_map[track_list[TRACK_ID].cluster_id].push_back(TRACK_ID);
            }
        
    
            // Pose to be published
            std::vector<bot_core_pose_t> pose_list_msg;
    
            // For each cluster compute the pose of each track
            for (std::map<int, std::vector<int64_t> >::iterator it=id_map.begin(); 
                 it != id_map.end(); it++) { 
                const int label = it->first;
                if (label<=0) continue; 
                std::cerr << "_______ label: " << label << std::endl;
                const std::vector<int64_t>& track_ids = it->second;

                // Pair-specific pose (from pair ids) => map id_pair to (map utime to pose)
                std::map<id_pair, std::map<int64_t, bot_core_pose_t> > pair_map;

                // Cannot infer pose from 1 track
                if (track_ids.size() < 2) continue;

#if 0
                // Initialized to identity
                // bot_core_pose_t Oc;
                // Oc.pos[0] = Xmu[0], Oc.pos[1] = Xmu[1], Oc.pos[2] = Xmu[2];
                // Oc.orientation[0] = 1, Oc.orientation[1] = 0,
                //     Oc.orientation[2] = 0, Oc.orientation[3] = 0;
                // pose_list_msg.push_back(Oc);

                // Least-squares pose estimation
                std::map<int64_t, pcl::PointCloud<pcl::PointXYZRGB> > utime_pcls;
                std::map<int64_t, std::vector<bool> > utime_valid;
                for (int64_t j=0; j<track_ids.size(); j++) {

                    const int64_t TRACK_IDj = track_ids[j];
                    Tracklet& trackj = track_list[TRACK_IDj];

                    if (!trackj.valid) continue;
                    std::map<int64_t, int>& utime_slices_mapj = trackj.utimes_slices_map;

                    int overlap = 0;
                    for (std::map<int64_t, int>::iterator itj = utime_slices_mapj.begin();
                         itj != utime_slices_mapj.end(); itj++) {
                        const int64_t utimej = itj->first;
                        const int jidx = itj->second;
                        assert(jidx >= 0 && jidx < trackj.utimes_slices.size());
                        if (utime_valid.find(utimej) == utime_valid.end())
                            utime_valid[utimej] = std::vector<bool>(track_ids.size(), false);

                        if (utime_pcls.find(utimej) == utime_pcls.end())
                            utime_pcls[utimej] = pcl::PointCloud<pcl::PointXYZRGB>(track_ids.size(), 1);
                        
                        utime_valid[utimej][j] = true;
                        utime_pcls[utimej][j] = trackj.tracks->points[jidx];
                    }
                }

                if (!utime_pcls.size()) { std::cerr << "Not enough utime slices: " << std::endl; continue; }

                // Find correspondences
                std::vector<Eigen::Matrix4f> tfs(utime_pcls.size());
                assert(utime_pcls.size() == utime_valid.size());
                { int idx = 1;

                    tfs[0] = Eigen::Matrix4f::Identity();

                    for (std::map<int64_t, std::vector<bool> >::iterator it1 = utime_valid.begin();
                         it1 != utime_valid.end(); it1++) {
                        std::map<int64_t, std::vector<bool> >::iterator it2 = it1; it2++;
                        if (it2 == utime_valid.end()) continue;
                        assert(it1->second.size() == it2->second.size());

                        pcl::PointCloud<pcl::PointXYZRGB>& pcl1 = utime_pcls[it1->first];
                        pcl::PointCloud<pcl::PointXYZRGB>& pcl2 = utime_pcls[it2->first];
                        assert(pcl1->size() == pcl2->size());

                        std::vector<bool>& valid1 = it1->second;
                        std::vector<bool>& valid2 = it2->second;

                        std::vector<int> inds;
                        for (int j=0; j<valid1.size(); j++)
                            if (valid1[j] && valid2[j])
                                inds.push_back(j);

                        pcl::registration::TransformationEstimationSVD
                            <pcl::PointXYZRGB, pcl::PointXYZRGB > tf_est_SVD;
                        Eigen::Matrix4f tf;
                        tf_est_SVD.estimateRigidTransformation(pcl1, inds, pcl2, inds, tf);
                        tfs[idx++] = tf;
                        std::cerr << "tf: " << it1->first << "=>" << it2->first << " " << tf << std::endl;


                        // Find the corresponding idx in the track

                
                        // for (int j=0; j<track_ids.size(); j++) {
                        //     const int64_t TRACK_IDj = track_ids[j];
                        //     TrackPointCloud& trackj = track_list[TRACK_IDj];
                    
                        //     if (!trackj.valid) continue;
                        //     std::map<int64_t, int>& utime_slices_mapj = trackj.utimes_slices_map;
                    
                        //     int jidx = utime_slices_mapj[it1->first];
                        //     if (it1 == utime_valid.begin()) {

                        //     } else {

                        //     }

                    }
                }
#else
                // Per-pair pair_map construction
                for (int64_t j=0; j<track_ids.size(); j++) {
                    const int64_t TRACK_IDj = track_ids[j];
                    Tracklet& trackj = track_list[TRACK_IDj];

                    if (!trackj.valid) continue;
                    std::map<int64_t, int>& utime_slices_mapj = trackj.utimes_slices_map; 
                    // std::cerr << "trackj[" << TRACK_IDj << "] " << trackj.tracks->points.size() << std::endl;
                    // for (int l=trackj.tangents->size()-1; l>=0; l--) { 
                    //     printf("%f ", trackj.utime_slices[l] * 1e-3);; 
                    // }
                
                    for (int k=j+1; k<track_ids.size(); k++) { 
                        const int TRACK_IDk = track_ids[k];
                        Tracklet& trackk = track_list[TRACK_IDk];

                        // if (!(TRACK_IDj == DEBUG_ID || TRACK_IDk == DEBUG_ID)) continue;
                        if (!trackk.valid) continue;
                        std::map<int64_t, int>& utime_slices_mapk = trackk.utimes_slices_map; 
                        // std::cerr << "trackk[" << TRACK_IDk << "] " << trackk.tracks->points.size() << std::endl;

                        // id_pair pair = make_sorted_pair(TRACK_IDj,TRACK_IDk);
                        id_pair pair_jk = std::make_pair(TRACK_IDj, TRACK_IDk); 
                        id_pair pair_kj = std::make_pair(TRACK_IDk, TRACK_IDj); 
                        bool flip = (TRACK_IDk < TRACK_IDj);

                        // this is only temporary (also, tangential vector is computed, so first point is neglected)
                        // relax constraint
                        if (pair_map.find(pair_jk) == pair_map.end()) pair_map[pair_jk] = std::map<int64_t, bot_core_pose_t>(); 
                        if (pair_map.find(pair_kj) == pair_map.end()) pair_map[pair_kj] = std::map<int64_t, bot_core_pose_t>(); 
                        // for (int l=trackk.tangents->size()-1; l>=0; l--) { 
                        //     printf("%f ", trackk.utime_slices[l] * 1e-3); 
                        // }
                        // std::cerr << std::endl;

                        int overlap = 0; 
#define USE_NORMALS 1
                        for (std::map<int64_t, int>::iterator itj = utime_slices_mapj.begin(); 
                             itj != utime_slices_mapj.end(); itj++) { 
                            const int64_t utimej = itj->first;
                            if (utime_slices_mapk.find(utimej) == utime_slices_mapk.end()) continue;
                            overlap++; 
                            int jidx = utime_slices_mapj[utimej]; 
                            int kidx = utime_slices_mapk[utimej]; 
                            // printf("%ld(%i,%i) ", utimej,jidx,kidx); 
                            assert(jidx >= 0 && jidx < trackj.utimes_slices.size());
                            assert(kidx >= 0 && kidx < trackk.utimes_slices.size());

                            // if (trackj.tracks_refined->points.size() < MIN_FIT_LENGTH && 
                            //     trackk.tracks_refined->points.size() < MIN_FIT_LENGTH)
                            //     continue;

                            // Choose to use the refined points or the track points

                            cv::Vec3f p1(trackj.tracks->points[jidx].x, 
                                         trackj.tracks->points[jidx].y, 
                                         trackj.tracks->points[jidx].z);

#if USE_NORMALS                        
                            cv::Vec3f t1(trackj.normals->points[jidx].normal[0],
                                         trackj.normals->points[jidx].normal[1],
                                         trackj.normals->points[jidx].normal[2]);
#else
                            cv::Vec3f t1(trackj.tangents->points[jidx].normal[0],
                                         trackj.tangents->points[jidx].normal[1],
                                         trackj.tangents->points[jidx].normal[2]);
#endif
                        

                            cv::Vec3f p2(trackk.tracks->points[kidx].x, 
                                         trackk.tracks->points[kidx].y, 
                                         trackk.tracks->points[kidx].z);
#if USE_NORMALS
                            cv::Vec3f t2(trackk.normals->points[kidx].normal[0],
                                         trackk.normals->points[kidx].normal[1],
                                         trackk.normals->points[kidx].normal[2]);
#else
                            cv::Vec3f t2(trackk.tangents->points[kidx].normal[0],
                                         trackk.tangents->points[kidx].normal[1],
                                         trackk.tangents->points[kidx].normal[2]);
#endif

                            if (t1[0] != t1[0] || t2[0] != t2[0] || 
                                t1[1] != t1[1] || t2[1] != t2[1] || 
                                t1[2] != t1[2] || t2[2] != t2[2])
                                continue;

                            t1 /= cv::norm(t1);
                            t2 /= cv::norm(t2);
                    
                            cv::Vec3f v1 = p1-p2;
                            float v1norm = cv::norm(v1);
                            if (v1norm < MIN_NORM) continue;
                            v1 /= v1norm;
                            cv::Vec3f v2 = -v1;

                            cv::Vec3f z1 = t1.cross(v1);
                            float z1norm = cv::norm(z1);
                            if (z1norm < MIN_NORM) continue;
                            z1 /= z1norm;

                            cv::Vec3f z2 = t2.cross(v2);
                            float z2norm = cv::norm(z2);
                            if (z2norm < MIN_NORM) continue;
                            z2 /= z2norm;

                            cv::Vec3f y1 = z1.cross(t1);
                            float y1norm = cv::norm(y1);
                            if (y1norm < MIN_NORM) continue;
                            y1 /= y1norm;

                            cv::Vec3f y2 = z2.cross(t2);
                            float y2norm = cv::norm(y2);
                            if (y2norm < MIN_NORM) continue;
                            y2 /= y2norm;

                            double T1[9], T2[9];
                            T1[0] = t1[0], T1[1] = y1[0], T1[2] = z1[0];
                            T1[3] = t1[1], T1[4] = y1[1], T1[5] = z1[1];
                            T1[6] = t1[2], T1[7] = y1[2], T1[8] = z1[2];

                            T2[0] = t2[0], T2[1] = y2[0], T2[2] = z2[0];
                            T2[3] = t2[1], T2[4] = y2[1], T2[5] = z2[1];
                            T2[6] = t2[2], T2[7] = y2[2], T2[8] = z2[2];

                            /* std::cerr << "T1 " << std::endl; */
                            /* for (int m=0; m<9; m++)  */
                            /*     std::cerr << T1[m] << " "; */
                            /* std::cerr << std::endl; */

                            /* std::cerr << "T2 " << std::endl; */
                            /* for (int m=0; m<9; m++)  */
                            /*     std::cerr << T2[m] << " "; */
                            /* std::cerr << std::endl; */

                            // std::cerr << "v1: " << v1norm << " " << v1 << std::endl;

                            // std::cerr << "p1: " << p1 << std::endl;
                            // std::cerr << "t1: " << t1 << std::endl;

                            // std::cerr << "p2: " << p2 << std::endl;
                            // std::cerr << "t2: " << t2 << std::endl;
                        
                            // std::cerr << "v1: " << v1 << std::endl;
                            // std::cerr << "v2: " << v2 << std::endl;
                        
                            // std::cerr << "z1: " << z1 << std::endl;
                            // std::cerr << "z2: " << z2 << std::endl;
                        
                            // std::cerr << "y1: " << y1 << std::endl;
                            // std::cerr << "y2: " << y2 << std::endl;

                            // Position, Orientation needs to be UKF mean
                            bot_core_pose_t pose_jk_w; 
                            pose_jk_w.utime = 0;
                            pose_jk_w.pos[0] = p1[0], pose_jk_w.pos[1] = p1[1], pose_jk_w.pos[2] = p1[2]; 
                            bot_matrix_to_quat2(T1, pose_jk_w.orientation);

                            // if (TRACK_IDj == DEBUG_ID) 
                            //     pose_list_msg.push_back(pose_jk_w);
                    
                            bot_core_pose_t pose_kj_w; 
                            pose_kj_w.utime = 0;
                            pose_kj_w.pos[0] = p2[0], pose_kj_w.pos[1] = p2[1], pose_kj_w.pos[2] = p2[2]; 
                            bot_matrix_to_quat2(T2, pose_kj_w.orientation);                    
                            // if (TRACK_IDk == DEBUG_ID)
                            //     pose_list_msg.push_back(pose_kj_w);

                            assert(pair_map.find(pair_jk) != pair_map.end());
                            assert(pair_map.find(pair_kj) != pair_map.end());
                            pair_map[pair_jk][utimej] = pose_jk_w; 
                            pair_map[pair_kj][utimej] = pose_kj_w; 
                            /* if (flip) */
                            /*     pair_map[pair][utimej] = pose_jk_w; */
                            /* else */
                            /*     pair_map[pair][utimej] = pose_kj_w; */

                            // shouldn't be nan
                            if ((pose_jk_w.orientation[0] != pose_jk_w.orientation[0]) || 
                                (pose_kj_w.orientation[0] != pose_kj_w.orientation[0])) { 

                                std::cerr << "pose_jk_w: " << pose_jk_w.pos[0] << " " 
                                          << pose_jk_w.pos[1] << " " 
                                          << pose_jk_w.pos[2] << " " 
                                          << pose_jk_w.orientation[0] << " " 
                                          << pose_jk_w.orientation[1] << " " 
                                          << pose_jk_w.orientation[2] << " " 
                                          << pose_jk_w.orientation[3] << " " << std::endl;

                                std::cerr << "pose_kj_w: " << pose_kj_w.pos[0] << " " 
                                          << pose_kj_w.pos[1] << " " 
                                          << pose_kj_w.pos[2] << " " 
                                          << pose_kj_w.orientation[0] << " " 
                                          << pose_kj_w.orientation[1] << " " 
                                          << pose_kj_w.orientation[2] << " " 
                                          << pose_kj_w.orientation[3] << " " << std::endl;
                        
                            }

                            //     // break; // only for last point in the trajectory
                        }
                        // printf("\nOVERLAP: %i\n", overlap);
                
                    }
                    // break; // please remove (only for test purposes)
                }

                Eigen::Matrix<double, object_state::DOF, object_state::DOF> cov;
                cov.setZero();
        
                // set diagonal of cov block of pos part
                setDiagonal(cov, &object_state::pos, 1);

                // fill entire orient-pos-cov. block
                MTK::subblock(cov, &object_state::orient, &object_state::pos).fill(0.1);

                // Per pair of trajectories, one obs. is calculated
                std::map<int64_t, std::map<int64_t, std::vector<object_state> > > observations;
                // Per-pair pose estimation

                for (std::map<id_pair, std::map<int64_t, bot_core_pose_t> >::iterator pair_it = pair_map.begin(); 
                     pair_it != pair_map.end(); pair_it++) { 
                    const id_pair& pair_jk = pair_it->first; 
                    int64_t TRACK_IDj = pair_jk.first; 
                    int64_t TRACK_IDk = pair_jk.second; 

                    id_pair pair_kj = std::make_pair(TRACK_IDk, TRACK_IDj);
                    assert(pair_map.find(pair_kj) != pair_map.end());

                    Tracklet& trackj = track_list[TRACK_IDj];
                    if (!trackj.valid) continue;
                    std::map<int64_t, int>& utime_slices_mapj = trackj.utimes_slices_map; 

                    Tracklet& trackk = track_list[TRACK_IDk];
                    if (!trackk.valid) continue;
                    std::map<int64_t, int>& utime_slices_mapk = trackk.utimes_slices_map; 

                    // if (!(TRACK_IDj == DEBUG_ID || TRACK_IDk == DEBUG_ID)) continue;

                    if (!pair_map[pair_jk].size()) { 
                        // std::cerr << "*********** No pairs: " << std::endl;
                        continue;
                    } else  { 
                        /* std::cerr << "pair_map[" << pair_jk.first << "," << pair_jk.second << "]: "  */
                        /*           << pair_map[pair_jk].size() << std::endl; */
                    }
                    // Recall that TOcj0_w and Tj0_w are fixed rotational transform
                    // So, we can apply the relative transformation between j1_j0 to Ocj0 and get Ocj1

                    // Pose of the pair (j,k)
                    double dTj0_k[16], dTk0_j[16];

                    // This is the first frame for which this pose-pair is valid/available
                    int64_t ref_time = pair_map[pair_jk].begin()->first;
                    assert(utime_slices_mapj.find(ref_time) != utime_slices_mapj.end()); 
                    assert(utime_slices_mapk.find(ref_time) != utime_slices_mapk.end()); 

                    // Find the corresponding idx in the track
                    int jidx = utime_slices_mapj[ref_time]; 
                    int kidx = utime_slices_mapk[ref_time]; 

                    // Initial pose-pair (j,k), used to compute relative transformations
                    const bot_core_pose_t& pose_j0_k = pair_map[pair_jk].begin()->second;
                    const bot_core_pose_t& pose_k0_j = pair_map[pair_kj].begin()->second;
                    bot_core_pose_to_mat(pose_j0_k, dTj0_k);
                    bot_core_pose_to_mat(pose_k0_j, dTk0_j);
                    cv::Mat_<double> Tj0_k(4,4,dTj0_k); 
                    cv::Mat_<double> Tk0_j(4,4,dTk0_j); 

                    cv::Mat_<double> Tk0_jrb = rigid_body_invert(Tj0_k);
                    cv::Mat_<double> Tj0_krb = rigid_body_invert(Tk0_j);

                    // Initial pose of j (pos of trackj @ t=0, orientation = eye(3))
                    bot_core_pose_t Ocj;
                    Ocj.pos[0] = trackj.tracks->points[jidx].x, Ocj.pos[1] = trackj.tracks->points[jidx].y, 
                        Ocj.pos[2] = trackj.tracks->points[jidx].z;
                
                    bool align_to_normal = true; 
                    if (align_to_normal) { 
                        if (!make_coordinate_tf(trackj.normals->points[jidx], Ocj)) continue; 
                    } else {                      
                        Ocj.orientation[0] = 1, Ocj.orientation[1] = 0,  Ocj.orientation[2] = 0, Ocj.orientation[3] = 0;
                    }

                    // Initial pose of j (pos of trackk @ t=0, orientation = eye(3))
                    bot_core_pose_t Ock;
                    Ock.pos[0] = trackk.tracks->points[kidx].x, Ock.pos[1] = trackk.tracks->points[kidx].y, 
                        Ock.pos[2] = trackk.tracks->points[kidx].z;
                    if (align_to_normal) { 
                        if (!make_coordinate_tf(trackk.normals->points[kidx], Ock)) continue; 
                    } else {                      
                        Ock.orientation[0] = 1, Ock.orientation[1] = 0,  Ock.orientation[2] = 0, Ock.orientation[3] = 0;
                    }

                    // Intial pose in matrix form
                    double dTOcj0_k[16], dTOck0_j[16];
                    bot_core_pose_to_mat(Ocj, dTOcj0_k);
                    bot_core_pose_to_mat(Ock, dTOck0_j);
                    cv::Mat_<double> TOcj0_k(4,4,dTOcj0_k);
                    cv::Mat_<double> TOck0_j(4,4,dTOck0_j);

                    // For all the overlapping pose_pairs between the tracks (j,k)
                    for (std::map<int64_t, bot_core_pose_t>::iterator time_it = pair_map[pair_jk].begin(); 
                         time_it != pair_map[pair_jk].end(); time_it++) { 
                        const int64_t utimej = time_it->first; 
                        assert(pair_map.find(pair_kj) != pair_map.end() && 
                               pair_map[pair_kj].find(utimej) != pair_map[pair_kj].end());

                        // Find the corresponding idx in the track
                        int jidx = utime_slices_mapj[utimej]; 
                        int kidx = utime_slices_mapk[utimej]; 
                    
                        // j-k pose-pair @ T = t
                        bot_core_pose_t pose_j1_k = time_it->second; 
                        bot_core_pose_t pose_k1_j = pair_map[pair_kj][utimej];

                        // Pose to mat
                        double dTj1_k[16], dTk1_j[16];
                        bot_core_pose_to_mat(pose_j1_k, dTj1_k);
                        bot_core_pose_to_mat(pose_k1_j, dTk1_j);

                        // Inverted pose-pair
                        cv::Mat_<double> Tj1_k(4,4,dTj1_k); 
                        cv::Mat_<double> Tk1_j(4,4,dTk1_j); 
                    
                        // Compute relative transformation
                        cv::Mat_<double> Tj1_j0 = Tj1_k * Tk0_jrb;
                        cv::Mat_<double> TOcj1_k = Tj1_j0  * TOcj0_k;

                        cv::Mat_<double> Tk1_k0 = Tk1_j * Tj0_krb; // small naming convention issue
                        cv::Mat_<double> TOck1_j = Tk1_k0  * TOck0_j;
                    
                        // Convert to bot core
                        bot_core_pose_t pose_Ocj1_k;
                        double* dTOcj1_k = (double*)TOcj1_k.data;
                        mat_to_bot_core_pose(dTOcj1_k, pose_Ocj1_k);

                        bot_core_pose_t pose_Ock1_j;
                        double* dTOck1_j = (double*)TOck1_j.data;
                        mat_to_bot_core_pose(dTOck1_j, pose_Ock1_j);

                        // if (TRACK_IDj == DEBUG_ID) { 
                        // printf("(%i,%i) \n", TRACK_IDj,TRACK_IDk);                         
                        
                        /* pose_Ocj1_k.pos[0] = trackj.tracks->points[jidx].x,  */
                        /*     pose_Ocj1_k.pos[1] = trackj.tracks->points[jidx].y,  */
                        /*     pose_Ocj1_k.pos[2] = trackj.tracks->points[jidx].z;  */
                        // pose_list_msg.push_back(pose_Ocj1_k); 
                        // }
                        // if (TRACK_IDk == DEBUG_ID) { 
                        /* pose_Ock1_j.pos[0] = trackk.tracks->points[kidx].x, 
                           pose_Ock1_j.pos[1] = trackk.tracks->points[kidx].y,  */
                        /*     pose_Ock1_j.pos[2] = trackk.tracks->points[kidx].z;  */
                        // pose_list_msg.push_back(pose_Ock1_j);
                        // }

                        object_state stj;
                        stj.pos << pose_Ocj1_k.pos[0],pose_Ocj1_k.pos[1],pose_Ocj1_k.pos[2];
                        stj.vel << 0.f,0.f,0.f;
                        stj.orient = Pose(pose_Ocj1_k.orientation[0], pose_Ocj1_k.orientation[1],
                                          pose_Ocj1_k.orientation[2],pose_Ocj1_k.orientation[3]);

                        object_state stk;
                        stk.pos << pose_Ock1_j.pos[0],pose_Ock1_j.pos[1],pose_Ock1_j.pos[2];
                        stk.vel << 0.f,0.f,0.f;
                        stk.orient = Pose(pose_Ock1_j.orientation[0], pose_Ock1_j.orientation[1],
                                          pose_Ock1_j.orientation[2],pose_Ock1_j.orientation[3]);

                        if ((stj.pos[0] != stj.pos[0]) || (stj.pos[1] != stj.pos[1]) || 
                            (stj.pos[2] != stj.pos[2])) { 
                            std::cerr << "pose_j1_k: " << pose_j1_k.pos[0] << " " 
                                      << pose_j1_k.pos[1] << " " 
                                      << pose_j1_k.pos[2] << " " 
                                      << pose_j1_k.orientation[0] << " " 
                                      << pose_j1_k.orientation[1] << " " 
                                      << pose_j1_k.orientation[2] << " " 
                                      << pose_j1_k.orientation[3] << " " << std::endl;

                            std::cerr << "object_state: " << stj << std::endl;
                            std::cerr << "Tj1_k: " << Tj1_k << std::endl;
                            // std::cerr << "Tj1_j0: " << Tj1_j0 << std::endl;
                            std::cerr << "TOcj0_k: " << TOcj0_k << std::endl;
                            assert(0); // this shouldn't happen
                        } else {
                            // This is observation for a specific track j; which 
                            // accumulated over all k, should be mean pose
                            // i.e. if all Ocj0's are initialized as canonical [I 0], 
                            // then the relative transformation is consistent across
                            // all pose pairs
                            observations[TRACK_IDj][utimej].push_back(stj);
                            observations[TRACK_IDk][utimej].push_back(stk);
                        }
                        // break; // only for last point in the trajectory
                    }
                    // break; // please remove (only for test purposes)
                }

                std::vector<object_state> refined_states(observations.size());
        
                ukfom::ukf<object_state>::cov init_cov = 0.01 * ukfom::ukf<object_state>::cov::Identity();
                ukfom::ukf<object_state>::cov process_noise_covar = 0.01 * ukfom::ukf<object_state>::cov::Identity();
                // ukfom::ukf<object_state>* kf; // (object_state(), init_cov); //   = NULL; 


#if 1
                // Per ID mean
                std::map<int64_t, std::map<int64_t, object_state> > mean_map; 
                for (std::map<int64_t, std::map<int64_t, std::vector<object_state> > >::iterator it = observations.begin(); 
                     it != observations.end(); it++) { 
                    int64_t TRACK_IDj = it->first; 
                    std::map<int64_t, std::vector<object_state> >& obs_time_map = it->second; 
                    if (!obs_time_map.size()) continue;

                    object_state track_mean; 
                    Eigen::Matrix<double, object_state::DOF, object_state::DOF> track_pcov;
                    MTK::mean_and_covariance(track_mean, track_pcov, obs_time_map.begin()->second); 
                    ukfom::ukf<object_state> kf(track_mean, init_cov);
            
                    for (std::map<int64_t, std::vector<object_state> >::iterator time_it = obs_time_map.begin(); 
                         time_it != obs_time_map.end(); time_it++) { 
                        if (!time_it->second.size()) continue;

                        const int64_t utime = it->first; 
                        std::vector<object_state>& obs = time_it->second;

                        object_state mean; 
                        Vec3 omega; omega << 0.f,0.f,0.f;
                        Eigen::Matrix<double, object_state::DOF, object_state::DOF> pcov;
                        MTK::mean_and_covariance(mean, pcov, obs); 

                        if ((mean.pos[0] != mean.pos[0]) || (mean.pos[1] != mean.pos[1]) || 
                            (mean.pos[2] != mean.pos[2])) { 

                            for (int k=0; k<obs.size(); k++) 
                                std::cerr << "obs: " << obs[k] << std::endl;

                            std::cerr << "Mean: " << mean << std::endl;
                            std::cerr << "Cov: " << pcov << std::endl;
                        }
                        // std::cerr << mean.pos[0] << " " << mean.pos[1] << " " << mean.pos[2] << " " 
                        //           << mean.orient.w() << " " << mean.orient.x() << " " << mean.orient.y() << " " 
                        //           << mean.orient.z() << std::endl;

                        // // std::cerr << "init_cov: " << pcov << std::endl;

                        // if (j > 0) { 

                        //     // std::cerr << "process noise covar: " << process_noise_covar << std::endl;
                        //     kf.predict(boost::bind(pose_process_model, _1, omega), process_noise_covar); 

                        //     // std::cerr << "pose_meas._noise_covar noise covar: " << std::endl << pose_measurement_noise_cov() << std::endl;
                        //     // std::cerr << "object_state: " << mean << std::endl;
                        //     kf.update(mean, pose_measurement_model, pose_measurement_noise_cov());
                        // }
                
                        // // update state
                        // // refined_states[j] = kf.mu();
                        // object_state mean2 = kf.mu();
                        // // std::cerr << "KF: cov: " << kf.sigma().block(0,0,3,3) << std::endl;
                        // Matrix3x3 cov = kf.sigma().block(0,0,3,3); 


                        // publish
                        bot_core_pose_t pose_Oc1_w;
                        pose_Oc1_w.pos[0] = mean.pos[0], 
                            pose_Oc1_w.pos[1] = mean.pos[1], 
                            pose_Oc1_w.pos[2] = mean.pos[2];
                        pose_Oc1_w.orientation[0] = mean.orient.w(), 
                            pose_Oc1_w.orientation[1] = mean.orient.x(), 
                            pose_Oc1_w.orientation[2] = mean.orient.y(), 
                            pose_Oc1_w.orientation[3] = mean.orient.z();
                        pose_list_msg.push_back(pose_Oc1_w);

                    }

                    if ((track_mean.pos[0] != track_mean.pos[0]) || (track_mean.pos[1] != track_mean.pos[1]) || 
                        (track_mean.pos[2] != track_mean.pos[2])) { 

                        // for (int k=0; k<obs.size(); k++) 
                        //     std::cerr << "obs: " << obs[k] << std::endl;

                        std::cerr << "Track_Mean: " << track_mean << std::endl;
                        // std::cerr << "Cov: " << pcov << std::endl;
                    }

                    /* // publish */
                    /* bot_core_pose_t pose_Oc1_w; */
                    /* pose_Oc1_w.pos[0] = track_mean.pos[0],  */
                    /*     pose_Oc1_w.pos[1] = track_mean.pos[1],  */
                    /*     pose_Oc1_w.pos[2] = track_mean.pos[2]; */
                    /* pose_Oc1_w.orientation[0] = track_mean.orient.w(),  */
                    /*     pose_Oc1_w.orientation[1] = track_mean.orient.x(),  */
                    /*     pose_Oc1_w.orientation[2] = track_mean.orient.y(),  */
                    /*     pose_Oc1_w.orientation[3] = track_mean.orient.z(); */
                    /* pose_list_msg.push_back(pose_Oc1_w); */


                }

                // delete kf;
   
#else
                // Per cluster mean
                object_state mean; 
                Eigen::Matrix<double, object_state::DOF, object_state::DOF> pcov;
                MTK::mean_and_covariance(mean, pcov, observations[0]); 
                ukfom::ukf<object_state> kf(mean, init_cov);

                for (int j=0; j<observations.size(); j++) { 
                    if (!observations[j].size()) continue;
                    std::vector<object_state>& obs = observations[j];
            
                    object_state mean; 
                    Vec3 omega; omega << 0.f,0.f,0.f;
                    Eigen::Matrix<double, object_state::DOF, object_state::DOF> pcov;
                    MTK::mean_and_covariance(mean, pcov, obs); 

                    if ((mean.pos[0] != mean.pos[0]) || (mean.pos[1] != mean.pos[1]) || 
                        (mean.pos[2] != mean.pos[2])) { 

                        for (int k=0; k<obs.size(); k++) 
                            std::cerr << "obs: " << obs[k] << std::endl;

                        std::cerr << "Mean: " << mean << std::endl;
                        std::cerr << "Cov: " << pcov << std::endl;
                    }
                    // std::cerr << mean.pos[0] << " " << mean.pos[1] << " " << mean.pos[2] << " " 
                    //           << mean.orient.w() << " " << mean.orient.x() << " " << mean.orient.y() << " " 
                    //           << mean.orient.z() << std::endl;

            
                    // // std::cerr << "init_cov: " << pcov << std::endl;
                    if (j > 0) { 

                        // std::cerr << "process noise covar: " << process_noise_covar << std::endl;
                        kf.predict(boost::bind(pose_process_model, _1, omega), process_noise_covar); 

                        // std::cerr << "pose_meas._noise_covar noise covar: " << std::endl << pose_measurement_noise_cov() << std::endl;
                        // std::cerr << "object_state: " << mean << std::endl;
                        kf.update(mean, pose_measurement_model, pose_measurement_noise_cov());
                    }
                
                    // update state
                    // refined_states[j] = kf.mu();
                    object_state mean2 = kf.mu();
                    // std::cerr << "KF: cov: " << kf.sigma().block(0,0,3,3) << std::endl;
                    Matrix3x3 cov = kf.sigma().block(0,0,3,3); 

                    // publish
                    bot_core_pose_t pose_Oc1_w;
                    pose_Oc1_w.pos[0] = mean2.pos[0], pose_Oc1_w.pos[1] = mean2.pos[1], pose_Oc1_w.pos[2] = mean2.pos[2];
                    pose_Oc1_w.orientation[0] = mean2.orient.w(), pose_Oc1_w.orientation[1] = mean2.orient.x(), 
                        pose_Oc1_w.orientation[2] = mean2.orient.y(), pose_Oc1_w.orientation[3] = mean2.orient.z();

                    pose_list_msg.push_back(pose_Oc1_w);
                }
                // delete kf;
#endif
#endif
                // break; // only one cluster
            } // for each cluster

            // std::cerr << "****************** " << pose_list_msg.size() << std::endl;
            if (!pose_list_msg.size()) return;

            // pose_list_msg.erase(pose_list_msg.begin() + 1, pose_list_msg.end());
            vs_obj_collection_t objs_msg; 
            objs_msg.id = 10000; 
            objs_msg.name = "POSE_EST | POSE_LIST"; 
            objs_msg.type = VS_OBJ_COLLECTION_T_AXIS3D; 
            objs_msg.reset = true; 
            vs_obj_t poses[pose_list_msg.size()];

            for (int j=0; j<pose_list_msg.size(); j++) { 
                double dTs_w[16];
                bot_trans_get_mat_4x4(&sensor_pose, dTs_w);
                cv::Mat_<double> Ts_w(4,4,dTs_w);

                double dTobs[16];
                bot_core_pose_t obs = pose_list_msg[j];
                bot_core_pose_to_mat(obs, dTobs);
                cv::Mat_<double> Tobs_s(4,4,dTobs);
                cv::Mat_<double> Tobs_w = Ts_w * Tobs_s; // 
                double* dTobs_w = (double*)Tobs_w.data;

                bot_core_pose_t obs_s; 
                mat_to_bot_core_pose(dTobs_w, obs_s);

                poses[j].id = j; 
                poses[j].x = obs_s.pos[0];
                poses[j].y = obs_s.pos[1];
                poses[j].z = obs_s.pos[2];

                double rpy[3]; 
                bot_quat_to_roll_pitch_yaw(obs_s.orientation, rpy);
                poses[j].roll = rpy[0], poses[j].pitch = rpy[1], poses[j].yaw = rpy[2]; 

                // std::cerr << "poses[j]: " << pose_list_msg[j].orientation[0] << " " << pose_list_msg[j].orientation[1]
                //           << " " << pose_list_msg[j].orientation[2] << " " << pose_list_msg[j].orientation[3] << std::endl;
                // std::cerr << "poses[j]: " << poses[j].x << " " << poses[j].y 
                //           << " " << poses[j].z << std::endl;
                // std::cerr << "poses[j]: " << poses[j].roll << " " << poses[j].yaw 
                //           << " " << poses[j].pitch << std::endl;
            }

            objs_msg.nobjs = pose_list_msg.size(); 
            objs_msg.objs = &poses[0];
            vs_obj_collection_t_publish(MOPT->lcm, "OBJ_COLLECTION", &objs_msg);
    
            // vs_collection_config_t config_msg; 
            // config_msg.collection_id = 400; 
            // config_msg.n = 1; 
            // vs_property_t props[config_msg.n];
            // props[0].name = "frame";
            // props[0].value = "KINECT";    
            // config_msg.properties = props;
            // vs_collection_config_t_publish(state->lcm, "COLLECTION_CONFIG", &config_msg);
            return;

        }
    };



    MultiObjectPoseTracker::MultiObjectPoseTracker() { 
        std::cerr << "INITIALIZING MultiObjectPoseTracker: " << std::endl;
        // init();
    }

    MultiObjectPoseTracker::MultiObjectPoseTracker(const MultiObjectPoseTracker& tr) { 
    }

    MultiObjectPoseTracker::~MultiObjectPoseTracker() { 
    }

    void MultiObjectPoseTracker::reinit() { 
        tracklet_manager.reset();
    }

    // void MultiObjectPoseTracker::update(int64_t utime, const erlcm_tracklet_list_t* tracks_msg) { 
    void MultiObjectPoseTracker::update(int64_t utime, const UniquePoseMap& track_features) { 
        
        //--------------------------------------------
        // utime_now
        //--------------------------------------------
        utime_now = utime;

        double tic;
        std::cerr << "MOPT update: " << std::endl;
        // if (!tracks_msg->num_tracks) return; 
        if (!track_features.size()) return;
     
        //--------------------------------------------
        // Only process frames that are newer
        // Ignore frames that are old
        // Track Clean up before update
        //--------------------------------------------
        tic = bot_timestamp_now();
        if (tracklet_manager.cleanup(utime, MOPT_MAX_TRACK_TIMESTAMP_DELAY_SEC)) reinit();
        posepair_manager.cleanup(utime);
        printf("%s===> TRACK CLEANUP: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

        //--------------------------------------------
        // Update
        //--------------------------------------------
        tic = bot_timestamp_now();
        tracklet_manager.update(utime, track_features);
        posepair_manager.update(utime, track_features);
        printf("%s===> TRACK UPDATE: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

        //--------------------------------------------
        // Prune so that length is only MAX_TRACK_TIMESPAN seconds long
        //--------------------------------------------
        tic = bot_timestamp_now();
        tracklet_manager.prune(MOPT_MAX_TRACK_TIMESPAN_SEC);
        posepair_manager.prune(MOPT_MAX_TRACK_TIMESPAN_SEC);
        printf("%s===> TRACK PRUNE: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

        //--------------------------------------------
        // Recompute utime map
        //--------------------------------------------
        tic = bot_timestamp_now();
        tracklet_manager.recomputeUtimeMap();
        printf("%s===> TRACK RECOMPUTE MAP: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

        // //--------------------------------------------
        // // Plot features
        // //--------------------------------------------
        // tic = bot_timestamp_now();
        // tracklet_manager.plot(utime, img);
        // printf("%s===> PLOT FEATURES: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

        return; 
    }


    void 
    MultiObjectPoseTracker::computeTrackTangents() { 
        for (std::map<int64_t, spvision::Tracklet>::iterator it = tracklet_manager.tracklets.begin(); 
             it != tracklet_manager.tracklets.end(); it++) { 
        
            int64_t TRACK_ID = it->first; 
            spvision::Tracklet& tracklet = it->second; 
            std::deque<int64_t>& utimes = tracklet.utimes; 
            std::deque<int64_t>& utimes_slices = tracklet.utimes_slices; 

            if (!tracklet.valid) { /*std::cerr << "TRACKLET NOT VALID" << std::endl; */ continue; }

            //----------------------------------
            // Sampled track , Sampled track refined (tangent projected), 
            // Sampled track normals, Sampled track tangents, 
            // Full track (for tangent computation), 
            //----------------------------------
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud = tracklet.tracks;
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud_refined = tracklet.tracks_refined;
            pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_normals = tracklet.normals;
            pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_tangents = tracklet.tangents;
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud_full = tracklet.tracks_full; 

            //----------------------------------
            // utimes map
            //----------------------------------
            std::map<int64_t, int>& utimes_map = tracklet_manager.tracklets[TRACK_ID].utimes_map; 
            std::map<int64_t, int>& utimes_slices_map = tracklet_manager.tracklets[TRACK_ID].utimes_slices_map; 

            //----------------------------------
            // Tangent, and refined track computation
            //----------------------------------
            int64_t last_processed_utime = tracklet.latest_track_processed_utime; 
            for (int k=0; k<utimes_slices.size(); k++) { 
                assert(utimes_map.find(utimes_slices[k]) != utimes_map.end());
                assert(utimes_slices_map.find(utimes_slices[k]) != utimes_slices_map.end());

                int64_t utime_slice = utimes_slices[k]; 
                int idx = utimes_map[utime_slice];
                int slice_idx = utimes_slices_map[utime_slice]; 

                // if (TRACK_ID == DEBUG_ID) { 
                //     std::cerr << "*********************" << std::endl;
                //     std::cerr << "LAST PROCESSED : " << utime_slice << "  " << tracklet.latest_track_processed_utime << ": " << k << std::endl;
                // }

                // Only compute on new points
                if (utime_slice < tracklet.latest_track_processed_utime) continue;

                boost::shared_ptr<vector<int> > fit_inds (new vector<int> ());
                int lidx = std::max(0, idx - MIN_FIT_LENGTH/2), 
                    ridx = std::min(int(track_cloud_full->points.size()-1), idx + MIN_FIT_LENGTH/2); 
                if (lidx == 0) 
                    ridx = std::min(int(track_cloud_full->points.size()-1), MIN_FIT_LENGTH-1);
                else if (ridx == track_cloud_full->points.size()-1) 
                    lidx = ridx - (MIN_FIT_LENGTH - 1); 
                for (int m=lidx; m<=ridx; m++) fit_inds->push_back(m);

                // if (TRACK_ID == DEBUG_ID)
                //     std::cerr << track_cloud->points.size() << " fit_inds: " << slice_idx << ":" << idx << " " << cv::Mat(*fit_inds) << std::endl;
                // Create the filtering object
                pcl::PointCloud<pcl::PointXYZRGB>::Ptr segment (new pcl::PointCloud<pcl::PointXYZRGB>);
                pcl::ExtractIndices<pcl::PointXYZRGB> extract;
                extract.setInputCloud(track_cloud_full);
                extract.setIndices (fit_inds);
                extract.setNegative (false);
                extract.filter (*segment);
                // assert(segment->points.size() == MIN_FIT_LENGTH);
            
                pcl::console::VERBOSITY_LEVEL vblvl = pcl::console::getVerbosityLevel();
                pcl::console::setVerbosityLevel(pcl::console::L_ALWAYS);

                // 3D Line Coefficients
                pcl::ModelCoefficients::Ptr coeff (new pcl::ModelCoefficients ());
                pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());

                // Create the segmentation object
                pcl::SACSegmentation<pcl::PointXYZRGB> seg;
                seg.setOptimizeCoefficients (true);
                seg.setModelType (pcl::SACMODEL_LINE);
                seg.setMethodType (pcl::SAC_RANSAC);
                seg.setMaxIterations (5);
                seg.setDistanceThreshold (0.1); 

                // Fit 3D Line
                seg.setInputCloud (segment);
                seg.segment (*inliers, *coeff);
            
                // std::cerr << "#inliers: " << inliers->indices.size() << " coeff: " << coeff->values[3] << " " 
                //           << coeff->values[4] << " " 
                //           << coeff->values[5] << std::endl;

                // Test direction and correct
                cv::Vec3f pp1(track_cloud_full->points[fit_inds->begin()[0]].x, 
                              track_cloud_full->points[fit_inds->begin()[0]].y, 
                              track_cloud_full->points[fit_inds->begin()[0]].z);
                cv::Vec3f p1(track_cloud_full->points[fit_inds->end()[-1]].x, 
                             track_cloud_full->points[fit_inds->end()[-1]].y, 
                             track_cloud_full->points[fit_inds->end()[-1]].z);
                cv::Vec3f t1 = (p1-pp1)/cv::norm(p1-pp1);
            
                // Shouldn't ever happen
                assert (!(coeff->values[3] == 0 && coeff->values[4] == 0 && coeff->values[5] == 0));

                float dot = t1[0]*coeff->values[3] + t1[1]*coeff->values[4] + t1[2]*coeff->values[5]; 
                if (dot < 0) {
                    track_cloud_tangents->points[slice_idx].normal[0] = -coeff->values[3], 
                        track_cloud_tangents->points[slice_idx].normal[1] = -coeff->values[4], 
                        track_cloud_tangents->points[slice_idx].normal[2] = -coeff->values[5];
                } else {
                    track_cloud_tangents->points[slice_idx].normal[0] = coeff->values[3], 
                        track_cloud_tangents->points[slice_idx].normal[1] = coeff->values[4], 
                        track_cloud_tangents->points[slice_idx].normal[2] = coeff->values[5];
                }

                // Once first tangent is computed, then fill in the previous 
                // tangents with the same value
                // if (k==MIN_FIT_LENGTH-1) { 
                //     for (int m=0; m<MIN_FIT_LENGTH-1; m++) 
                //         track_cloud_tangents->points[m] = track_cloud_tangents->points[k];
                // }

                // Project MIN_FIT_LENGTH along tangent vector to the plane whose 
                // normal is the tangent vector
                // if q is a point to be projected, p1 is the tangent point (above), 
                // and t1 is the tangent vector then: 
                // q_proj = q + ((q-p1).*t1) *t1;

                cv::Vec3f mean_q(0.f,0.f,0.f);
                for (int m=0; m<fit_inds->size(); m++) { 
                    cv::Vec3f q(track_cloud_full->points[fit_inds->begin()[m]].x, 
                                track_cloud_full->points[fit_inds->begin()[m]].y, 
                                track_cloud_full->points[fit_inds->begin()[m]].z);
                    cv::Vec3f q_proj = q + (fabs(t1.dot(q-p1))*t1);
                    mean_q += q_proj;
                }
                mean_q *= 1.f / fit_inds->size();

                // Populate refined tracks
                // std::cerr << "track cloud refined: " << slice_idx << std::endl;
                track_cloud_refined->points[slice_idx].x = mean_q[0], 
                    track_cloud_refined->points[slice_idx].y = mean_q[1], 
                    track_cloud_refined->points[slice_idx].z = mean_q[2];
            
                // Fill 0-MIN_FIT_LENGTH indices for the refined points
                // if (k==MIN_FIT_LENGTH-1) { 
                //     for (int m=0; m<MIN_FIT_LENGTH-1; m++) 
                //         track_cloud_refined->points[m] = track_cloud_refined->points[k];
                // }

                // Update processed utime (if null, that means the previous samples need to be recomputed)
                if (!tracklet.latest_track_processed_utime) { 
                    // if (TRACK_ID == DEBUG_ID)
                    //     std::cerr << "#################### REDO processing" << std::endl;
                    tracklet.latest_track_processed_utime = utimes_slices[0]; 
                } else 
                    tracklet.latest_track_processed_utime = utime_slice; 

            } // end for each pose (within a particular track)
        }
    }


    void 
    MultiObjectPoseTracker::computeTrackNormalsDescription() { 
        for (std::map<int64_t, spvision::Tracklet>::iterator it = tracklet_manager.tracklets.begin(); 
             it != tracklet_manager.tracklets.end(); it++) { 
        
            int64_t TRACK_ID = it->first; 
            spvision::Tracklet& tracklet = it->second; 
            std::deque<int64_t>& utimes = tracklet.utimes; 
            std::deque<int64_t>& utimes_slices = tracklet.utimes_slices; 
            if (!tracklet.valid) { continue; } // atleast min_fit_length

            //----------------------------------
            // Sampled track , Sampled track refined (tangent projected), 
            // Sampled track normals, Sampled track tangents, 
            // Full track (for tangent computation), 
            //----------------------------------
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud = tracklet.tracks;
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud_refined = tracklet.tracks_refined;
            pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_normals = tracklet.normals;
            pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_tangents = tracklet.tangents;
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud_full = tracklet.tracks_full; 

            //----------------------------------
            // utimes map
            //----------------------------------
            std::map<int64_t, int>& utimes_slices_map = tracklet_manager.tracklets[TRACK_ID].utimes_slices_map; 

            //----------------------------------
            // Normal, and L2 distance 
            //----------------------------------
            std::vector<float> desc(NDESC_BINS, 0); 
            const float desc_norm = PI / NDESC_BINS; 

            cv::Vec3f p0(track_cloud->points[0].x, 
                         track_cloud->points[0].y, 
                         track_cloud->points[0].z);

            cv::Vec3f n0(track_cloud_normals->points[0].normal[0], 
                         track_cloud_normals->points[0].normal[1], 
                         track_cloud_normals->points[0].normal[2]);
        

            // std::cerr << "dot: ";
            int dotcount = 0;
            for (int k=0; k<utimes_slices.size(); k++) { 
                assert(utimes_slices_map.find(utimes_slices[k]) != utimes_slices_map.end());

                int64_t utime_slice = utimes_slices[k]; 
                int slice_idx = utimes_slices_map[utime_slice]; 
                assert(k == slice_idx);

                // if (TRACK_ID == DEBUG_ID) { 
                //     std::cerr << "*********************" << std::endl;
                //     std::cerr << "LAST PROCESSED : " << utime_slice << "  " << tracklet.latest_track_processed_utime << ": " << k << std::endl;
                // }

                cv::Vec3f p1(track_cloud->points[slice_idx].x, 
                             track_cloud->points[slice_idx].y, 
                             track_cloud->points[slice_idx].z);

                cv::Vec3f n1(track_cloud_normals->points[slice_idx].normal[0], 
                             track_cloud_normals->points[slice_idx].normal[1], 
                             track_cloud_normals->points[slice_idx].normal[2]);

                float dot = n1.dot(n0);
                //std::cerr << dot << " ";
                for (int j=0; j<NDESC_BINS; j++) 
                    if ((dot > cos(j*desc_norm-PI/2)) && (dot <= cos((j+1)*desc_norm-PI/2)))
                        desc[j]++, dotcount++;
            }
            //----------------------------------
            // Descriptor
            //----------------------------------
            if (dotcount) { 
                for (int j=0; j<desc.size(); j++) 
                    desc[j] *= 1.f / dotcount;
            }
            tracklet.desc2 = desc;

            // std::cerr << "desc: " << TRACK_ID << ": " << cv::Mat(desc) << std::endl;
        }
        return;
    }


    static bool matching_utimes_count_sort(const std::pair<std::pair<int64_t, int64_t>, std::vector<int64_t> >& lhs, 
                                           const std::pair<std::pair<int64_t, int64_t>, std::vector<int64_t> >& rhs) { 
        return lhs.second.size() > rhs.second.size();
    }

    void 
    MultiObjectPoseTracker::computeInterpolatedTrackNormals() { 

        // Find matching utimes between tracks
        std::map<std::pair<int64_t, int64_t>, std::vector<int64_t> > matching_utimes_count;
        for (std::map<int64_t, spvision::Tracklet>::iterator itj = tracklet_manager.tracklets.begin(); 
             itj != tracklet_manager.tracklets.end(); itj++) { 
        
            int64_t TRACK_IDj = itj->first; 
            spvision::Tracklet& trackletj = itj->second; 
            std::set<int64_t> utimes_slices_setj = 
                std::set<int64_t>(trackletj.utimes_slices.begin(), trackletj.utimes_slices.end());; 

            for (std::map<int64_t, spvision::Tracklet>::iterator itk = tracklet_manager.tracklets.begin(); 
                 itk != tracklet_manager.tracklets.end(); itk++) { 
                if (itj == itk) continue;

                int64_t TRACK_IDk = itk->first; 
                std::pair<int64_t, int64_t> pair = make_sorted_pair(TRACK_IDj, TRACK_IDk);

                if (matching_utimes_count.find(pair) != matching_utimes_count.end()) continue;

                spvision::Tracklet& trackletk = itk->second; 
                std::set<int64_t> utimes_slices_setk = 
                    std::set<int64_t>(trackletk.utimes_slices.begin(), trackletk.utimes_slices.end());; 
                    
                std::vector<int64_t> matching_utimes(utimes_slices_setj.size() + utimes_slices_setk.size());
                std::vector<int64_t>::iterator it = std::set_intersection(utimes_slices_setj.begin(), 
                                                                          utimes_slices_setj.end(), 
                                                                          utimes_slices_setk.begin(), 
                                                                          utimes_slices_setk.end(), 
                                                                          matching_utimes.begin());
                matching_utimes.resize(it-matching_utimes.begin());
                matching_utimes_count.insert(std::make_pair(pair, matching_utimes));
            }
        }

        // convert to vector to sort
        std::vector<std::pair<id_pair, std::vector<int64_t> > > matching_utimes_count_vec;
        for (std::map<id_pair, std::vector<int64_t> >::iterator it = matching_utimes_count.begin(); 
             it != matching_utimes_count.end(); it++)
            matching_utimes_count_vec.push_back(std::make_pair(it->first, it->second));
        std::sort(matching_utimes_count_vec.begin(), matching_utimes_count_vec.end(), matching_utimes_count_sort);
        
        // Prune (only threshold upto 50% of top matching counts)
        if (!matching_utimes_count_vec.size()) return;
        int top_matching_counts = matching_utimes_count_vec[0].second.size();
        {int idx; 
            for (idx=0; idx<matching_utimes_count_vec.size(); idx++)
                if (matching_utimes_count_vec[idx].second.size() < top_matching_counts * 0.5)
                    break;
            matching_utimes_count_vec.erase(matching_utimes_count_vec.begin() + idx, matching_utimes_count_vec.end());
        }

        std::cerr << "matching: " 
                  << matching_utimes_count_vec.end()[-1].second.size() << " " 
                  << matching_utimes_count_vec.begin()[0].second.size() << std::endl;
       

        // // For each pair, find pose transformation between first and last overlapping pair
        // // Find spherical interpolation between frames
        // for (int j=0; j<matching_utimes_count_vec.size(); j++) { 
        //     const std::pair<id_pair, std::vector<int64_t> >& match = matching_utimes_count_vec[j];
        //     id_pair& pair = match.first;
        //     std::vector<int64_t>& matched_utimes = match.second;
            
        //     if (!matched_utimes.size()) return;
        //     int utimet0 = matched_utimes.begin()[0];
        //     int utimetT = matched_utimes.end()[-1];

        //     Tracklet& trackj = tracklet_manager.tracklets[pair.first];
        //     Tracklet& trackk = tracklet_manager.tracklets[pair.second];

        //     std::map<int64_t, int>& utime_slices_mapj = trackj.utimes_slices_map; 
        //     std::map<int64_t, int>& utime_slices_mapk = trackk.utimes_slices_map; 

        //     assert(utime_slices_mapj.find(utimet0) != utime_slices_mapj.end());
        //     assert(utime_slices_mapj.find(utimetT) != utime_slices_mapj.end());

        //     assert(utime_slices_mapk.find(utimet0) != utime_slices_mapk.end());
        //     assert(utime_slices_mapk.find(utimetT) != utime_slices_mapk.end());

        //     // Find the corresponding idx in the track
        //     int jidx = utime_slices_mapj[utimet0]; 
        //     int kidx = utime_slices_mapk[utimet0]; 

        //     assert(jidx >= 0 && jidx < trackj.utimes_slices.size());
        //     assert(kidx >= 0 && kidx < trackk.utimes_slices.size());
                
        //     }
        // }

        return;
    }


    void 
    MultiObjectPoseTracker::computeTrackDescription() { 
        for (std::map<int64_t, spvision::Tracklet>::iterator it = tracklet_manager.tracklets.begin(); 
             it != tracklet_manager.tracklets.end(); it++) { 
        
            int64_t TRACK_ID = it->first; 
            spvision::Tracklet& tracklet = it->second; 
            std::deque<int64_t>& utimes = tracklet.utimes; 
            std::deque<int64_t>& utimes_slices = tracklet.utimes_slices; 

            if (!tracklet.valid) { /*std::cerr << "TRACKLET NOT VALID" << std::endl; */continue; }

            //----------------------------------
            // Sampled track , Sampled track refined (tangent projected), 
            // Sampled track normals, Sampled track tangents, 
            // Full track (for tangent computation), 
            //----------------------------------
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud = tracklet.tracks;
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud_refined = tracklet.tracks_refined;
            pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_normals = tracklet.normals;
            pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_tangents = tracklet.tangents;
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud_full = tracklet.tracks_full; 

            //----------------------------------
            // Setup the shape context computation
            //----------------------------------
            pcl::PointCloud<pcl::Normal>::Ptr input_normals(new pcl::PointCloud<pcl::Normal>(track_cloud_normals->size(), 1, 
                                                                                             pcl::Normal(0,0,1)));
            pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree_sc; //  = tracklet.tree; 
            tree_sc = pcl::search::KdTree<pcl::PointXYZRGB>::Ptr(new pcl::search::KdTree<pcl::PointXYZRGB> ());

#define SHAPECONTEXT 1
#define PFH 0

#if SHAPECONTEXT
            pcl::ShapeContext3DEstimation<pcl::PointXYZRGB, pcl::Normal, pcl::ShapeContext> descriptor;
#else
            pcl::PFHEstimation<pcl::PointXYZRGB, pcl::Normal, pcl::PFHSignature125> descriptor;
#endif
            // Provide the point cloud
            descriptor.setInputCloud (track_cloud);
            // Provide normals
            descriptor.setInputNormals (track_cloud_normals);
            // descriptor.setInputNormals (input_normals);
            // Use the same KdTree from the normal estimation
            descriptor.setSearchMethod (tree_sc);

#if SHAPECONTEXT
            pcl::PointCloud<pcl::ShapeContext>::Ptr desc_features (new pcl::PointCloud<pcl::ShapeContext>);
#else
            pcl::PointCloud<pcl::PFHSignature125>::Ptr desc_features (new pcl::PointCloud<pcl::PFHSignature125>);
        
#endif

#if SHAPECONTEXT
            descriptor.setPointDensityRadius (0.05);
            descriptor.setMinimalRadius (0.001);
            descriptor.setAzimuthBins (SC_AZIMUTH_BINS);
            descriptor.setElevationBins (SC_ELEVATION_BINS);
            descriptor.setRadiusBins (SC_RADIUS_BINS);
#endif
            descriptor.setRadiusSearch (1.f);
        
            //----------------------------------
            // Set the indices
            //----------------------------------
            boost::shared_ptr<vector<int> > indices (new vector<int> ());
            indices->push_back (track_cloud->points.size()-1);
            descriptor.setIndices(indices);

            // Actually compute the shape contexts
            // opencv_utils::tic();
            descriptor.compute (*desc_features);
            // opencv_utils::toc("Shape context compute");

            // Display and retrieve the shape context descriptor vector
            // std::cout << shape_context_features->points.end()[-1] << std::endl;
#if SHAPECONTEXT
            std::vector<float> desc; 
            if (!desc_features->points.size())
                tracklet.valid = false; 
            else { 
                desc = desc_features->points.end()[-1].descriptor;
                assert(desc.size() == DESC_SIZE);
            }
#else
            float* hist = desc_features->points.end()[-1].histogram;
            std::vector<float> desc(hist, hist + 125);
#endif
        
            //----------------------------------
            // Descriptor
            //----------------------------------
            tracklet.desc = desc;
        }
        return;
    }

    bool 
    MultiObjectPoseTracker::constructTrackletGraph() { 
        PoseClusteringAlgorithm alg(this); 
        alg.set_tracklets(); 
        alg.build_knn_tree(); 
        alg.build_knn_mask(); 
        // alg.build_rnn_inds(); 
        alg.build_adjacency(); 

        alg.ransac_with_priors();

        // alg.perform_scgp_spectral_clustering(); 


        double tic = bot_timestamp_now();
        // alg.perform_pose_estimation_from_clusters(tracklet_manager.tracklets,sensor_frame); 
        printf("POSE_ESTIMATION: %4.2f ms\n", (bot_timestamp_now() - tic) * 1e-3); 

        return true;
    }

    void
    MultiObjectPoseTracker::viz() { 
        if (!tracklet_manager.tracklets.size()) return; 
        const int NUM_TRACKS = tracklet_manager.tracklets.size(); 

        //----------------------------------
        // Publish camera frame of reference for drawing
        //----------------------------------
        vs_obj_collection_t objs_msg; 
        objs_msg.id = 100; 
        objs_msg.name = "KINECT_POSE"; 
        objs_msg.type = VS_OBJ_COLLECTION_T_AXIS3D; 
        objs_msg.reset = true; 

        BotTrans sensor_frame;
        bot_frames_get_trans_with_utime (frames, "KINECT", "local", utime_now, &sensor_frame);
        double rpy[3]; bot_quat_to_roll_pitch_yaw(sensor_frame.rot_quat, rpy);
        
        vs_obj_t poses[1]; poses[0].id = 1; 
        poses[0].x = sensor_frame.trans_vec[0], poses[0].y = sensor_frame.trans_vec[1], poses[0].z = sensor_frame.trans_vec[2]; 
        poses[0].roll = rpy[0], poses[0].pitch = rpy[1], poses[0].yaw = rpy[2]; 

        objs_msg.nobjs = 1; 
        objs_msg.objs = &poses[0];
        vs_obj_collection_t_publish(lcm, "OBJ_COLLECTION", &objs_msg);

        //----------------------------------
        // Viz inits
        //----------------------------------
        vs_point3d_list_collection_t viz_track_cloud_msg;
        viz_track_cloud_msg.id = 1000; 
        viz_track_cloud_msg.name = "POSE_EST | TRACK_CLOUD"; 
        viz_track_cloud_msg.type = VS_POINT3D_LIST_COLLECTION_T_LINE_STRIP; 
        viz_track_cloud_msg.reset = true; 

        vs_point3d_list_collection_t viz_track_cloud_normals_msg;
        viz_track_cloud_normals_msg.id = 1001; 
        viz_track_cloud_normals_msg.name = "POSE_EST | TRACK_CLOUD_NORMALS"; 
        viz_track_cloud_normals_msg.type = VS_POINT3D_LIST_COLLECTION_T_LINES; 
        viz_track_cloud_normals_msg.reset = true; 

        vs_point3d_list_collection_t viz_track_cloud_tangents_msg;
        viz_track_cloud_tangents_msg.id = 1002; 
        viz_track_cloud_tangents_msg.name = "POSE_EST | TRACK_CLOUD_TANGENTS"; 
        viz_track_cloud_tangents_msg.type = VS_POINT3D_LIST_COLLECTION_T_LINES; 
        viz_track_cloud_tangents_msg.reset = true; 

        vs_point3d_list_collection_t viz_track_cloud_full_msg;
        viz_track_cloud_full_msg.id = 1003; 
        viz_track_cloud_full_msg.name = "POSE_EST | TRACK_CLOUD_FULL"; 
        viz_track_cloud_full_msg.type = VS_POINT3D_LIST_COLLECTION_T_POINT; 
        viz_track_cloud_full_msg.reset = true; 

        vs_point3d_list_collection_t viz_track_cloud_refined_msg;
        viz_track_cloud_refined_msg.id = 1004; 
        viz_track_cloud_refined_msg.name = "POSE_EST | TRACK_CLOUD_REFINED"; 
        viz_track_cloud_refined_msg.type = VS_POINT3D_LIST_COLLECTION_T_POINT; 
        viz_track_cloud_refined_msg.reset = true; 

        vs_point3d_list_collection_t viz_track_shape_cloud_msg;
        viz_track_shape_cloud_msg.id = 1006; 
        viz_track_shape_cloud_msg.name = "POSE_EST | TRACK_CLOUD_SHAPE_LABELED"; 
        viz_track_shape_cloud_msg.type = VS_POINT3D_LIST_COLLECTION_T_POINT; 
        viz_track_shape_cloud_msg.reset = true; 

        vs_text_collection_t viz_text_msg; 
        viz_text_msg.id = 1007; 
        viz_text_msg.name = "POSE_EST | TRACKLETS_ID";
        viz_text_msg.type = 0; 
        viz_text_msg.reset = true;

        erlcm_normal_point_list_t viz_track_cloud_tangents_msg2;
        viz_track_cloud_tangents_msg2.utime = bot_timestamp_now();

        int nidx=0;
        std::vector<vs_point3d_list_t> viz_track_cloud(NUM_TRACKS); 
        std::vector<vs_point3d_list_t> viz_track_cloud_normals(NUM_TRACKS); 
        std::vector<vs_point3d_list_t> viz_track_cloud_tangents(NUM_TRACKS); 
        std::vector<vs_point3d_list_t> viz_track_cloud_full(NUM_TRACKS); 
        std::vector<vs_point3d_list_t> viz_track_cloud_refined(NUM_TRACKS); 
        std::vector<vs_point3d_list_t> viz_track_shape_cloud(NUM_TRACKS); 
        std::vector<vs_text_t> viz_texts;  
        std::vector<erlcm_normal_point_t> viz_track_cloud_tangent;

        { int idx = 0; 
            for (std::map<int64_t, spvision::Tracklet>::iterator it = tracklet_manager.tracklets.begin(); 
                 it != tracklet_manager.tracklets.end(); it++) { 
                const int TRACK_ID = it->first;
                spvision::Tracklet& tracklet = it->second; 

                //----------------------------------
                // Sampled track , Sampled track refined (tangent projected), 
                // Sampled track normals, Sampled track tangents, 
                // Full track (for tangent computation), 
                //----------------------------------
                pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud = tracklet.tracks;
                pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud_refined = tracklet.tracks_refined;
                pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_normals = tracklet.normals;
                pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_tangents = tracklet.tangents;
                pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud_full = tracklet.tracks_full; 

                populate_viz_cloud(viz_track_cloud[idx], track_cloud, "blue", TRACK_ID);
                populate_viz_cloud(viz_track_shape_cloud[idx], track_cloud, NULL, TRACK_ID);
                populate_viz_cloud(viz_track_cloud_full[idx], track_cloud_full, "red", TRACK_ID);
                populate_viz_cloud(viz_track_cloud_refined[idx], track_cloud_refined, "green", TRACK_ID);
                populate_viz_tangents(viz_track_cloud_tangents[idx], track_cloud, track_cloud_tangents, "blue"); 
                populate_viz_tangents(viz_track_cloud_normals[idx], track_cloud, track_cloud_normals, "green"); 
                // if (it->first == DEBUG_ID)
                // populate_viz_tangents(viz_track_cloud_tangent, track_cloud, track_cloud_tangents); 
                // populate_viz_tangents(viz_track_cloud_tangents, track_cloud, track_cloud_tangents); 


                // Text (ID)
                viz_texts.resize(idx+1); 
                viz_texts[idx].id = TRACK_ID; // bot_timestamp_now(); 
                viz_texts[idx].collection_id = 1006;
                viz_texts[idx].object_id = TRACK_ID; 

                char str[8];
                sprintf(str, "ID:%i", tracklet.cluster_id); 
                viz_texts[idx].text = str;
                idx++;
            }
        }

        // Finish up viz
        viz_track_cloud_msg.nlists = NUM_TRACKS;
        viz_track_cloud_msg.point_lists = &viz_track_cloud[0];

        viz_track_shape_cloud_msg.nlists = NUM_TRACKS;
        viz_track_shape_cloud_msg.point_lists = &viz_track_shape_cloud[0];

        viz_track_cloud_normals_msg.nlists = NUM_TRACKS;
        viz_track_cloud_normals_msg.point_lists = &viz_track_cloud_normals[0];

        viz_track_cloud_tangents_msg.nlists = NUM_TRACKS;
        viz_track_cloud_tangents_msg.point_lists = &viz_track_cloud_tangents[0];

        viz_track_cloud_full_msg.nlists = NUM_TRACKS;
        viz_track_cloud_full_msg.point_lists = &viz_track_cloud_full[0];

        viz_track_cloud_refined_msg.nlists = NUM_TRACKS;
        viz_track_cloud_refined_msg.point_lists = &viz_track_cloud_refined[0];

        viz_track_cloud_tangents_msg2.no_points = viz_track_cloud_tangent.size();
        viz_track_cloud_tangents_msg2.points = &viz_track_cloud_tangent[0]; 

        viz_text_msg.n = viz_texts.size(); // texts.size(); 
        viz_text_msg.texts = &viz_texts[0];

        vs_point3d_list_collection_t_publish(lcm, "POINTS_COLLECTION", &viz_track_cloud_msg);
        vs_point3d_list_collection_t_publish(lcm, "POINTS_COLLECTION", &viz_track_shape_cloud_msg);
        vs_point3d_list_collection_t_publish(lcm, "POINTS_COLLECTION", &viz_track_cloud_normals_msg);
        vs_point3d_list_collection_t_publish(lcm, "POINTS_COLLECTION", &viz_track_cloud_tangents_msg);
        vs_point3d_list_collection_t_publish(lcm, "POINTS_COLLECTION", &viz_track_cloud_full_msg);
        vs_point3d_list_collection_t_publish(lcm, "POINTS_COLLECTION", &viz_track_cloud_refined_msg);
        vs_text_collection_t_publish(lcm, "TEXT_COLLECTION", &viz_text_msg);
        erlcm_normal_point_list_t_publish(lcm, "PCL_NORMAL_LIST", &viz_track_cloud_tangents_msg2);


        //----------------------------------
        // Viz track cleanup
        //----------------------------------
        for (int j=0; j<viz_track_cloud_msg.nlists; j++) {
            delete [] viz_track_cloud_msg.point_lists[j].points; 
            delete [] viz_track_shape_cloud_msg.point_lists[j].points; 
            delete [] viz_track_cloud_full_msg.point_lists[j].points; 
            delete [] viz_track_cloud_normals_msg.point_lists[j].points; 
            delete [] viz_track_cloud_tangents_msg.point_lists[j].points; 
            delete [] viz_track_cloud_refined_msg.point_lists[j].points; 

            delete [] viz_track_cloud_msg.point_lists[j].colors; 
            delete [] viz_track_shape_cloud_msg.point_lists[j].colors; 
            delete [] viz_track_cloud_full_msg.point_lists[j].colors; 
            delete [] viz_track_cloud_normals_msg.point_lists[j].colors; 
            delete [] viz_track_cloud_tangents_msg.point_lists[j].colors; 
            delete [] viz_track_cloud_refined_msg.point_lists[j].colors; 
        }
        // #endif


    }

    void 
    MultiObjectPoseTracker::vizPose() { 
        if (!tracklet_manager.tracklets.size()) return; 
        const int NUM_TRACKS = tracklet_manager.tracklets.size(); 

        //----------------------------------
        // Viz inits
        //----------------------------------
        vs_point3d_list_collection_t viz_track_cloud_full_labeled_msg;
        viz_track_cloud_full_labeled_msg.id = 1050; 
        viz_track_cloud_full_labeled_msg.name = "POSE_EST | TRACK_CLOUD_FULL_LABELED"; 
        viz_track_cloud_full_labeled_msg.type = VS_POINT3D_LIST_COLLECTION_T_POINT; 
        viz_track_cloud_full_labeled_msg.reset = true; 

        std::vector<vs_point3d_list_t> viz_track_cloud_full_labeled(NUM_TRACKS); 

        //----------------------------------
        // Viz pose from clusters
        //----------------------------------
        std::vector<cv::Vec3f> track_colors(20);
        // opencv_utils::fillColors(track_colors); 
        for (int j=0; j<track_colors.size(); j++) { 
            track_colors[j] = cv::Vec3f((float)rand() / (float)RAND_MAX, 
                                        (float)rand() / (float)RAND_MAX, 
                                        (float)rand() / (float)RAND_MAX);
        }
        track_colors[0] = cv::Vec3f(0,0,0);
        { int idx = 0; 
            for (spvision::TrackletMapIt it = tracklet_manager.tracklets.begin(); it != tracklet_manager.tracklets.end(); it++) { 
                int64_t TRACK_ID = it->first; 
                spvision::Tracklet& tracklet = it->second; 
                if (!tracklet.valid) continue;
        
                if (tracklet.cluster_id < 0) { 
                    std::cerr << "CLUSTER_ID ERR *********" << std::endl;
                    tracklet.cluster_id = 0; 
                    // continue;
                }
                if (tracklet.cluster_id == 0) { 
                    // std::cerr << "tracklet.cluster_id: " << tracklet.cluster_id << ": " 
                    //           << cv::Vec3f(track_colors[tracklet.cluster_id]) << std::endl;
                
                    cv::Vec3f black(0,0,0);
                    populate_viz_cloud(viz_track_cloud_full_labeled[idx++], tracklet_manager.tracklets[TRACK_ID].tracks_full, 
                                       black, TRACK_ID);
                    continue;

                }

                // std::cerr << "TRACK_ID: " << TRACK_ID << ": " << tracklet.cluster_id << ": " << cv::Mat(track_colors[tracklet.cluster_id]) << std::endl;
                populate_viz_cloud(viz_track_cloud_full_labeled[idx++], tracklet_manager.tracklets[TRACK_ID].tracks_full, 
                                   track_colors[tracklet.cluster_id], TRACK_ID);
            }
        }


        viz_track_cloud_full_labeled_msg.nlists = NUM_TRACKS;
        viz_track_cloud_full_labeled_msg.point_lists = &viz_track_cloud_full_labeled[0];

        //----------------------------------
        // Viz track cleanup
        //----------------------------------
        for (int j=0; j<viz_track_cloud_full_labeled_msg.nlists; j++) {
            delete [] viz_track_cloud_full_labeled_msg.point_lists[j].points; 
            delete [] viz_track_cloud_full_labeled_msg.point_lists[j].colors; 
        }

    }
}
