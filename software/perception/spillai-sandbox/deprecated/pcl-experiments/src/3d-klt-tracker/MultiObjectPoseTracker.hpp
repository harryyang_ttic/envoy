// 
// Author: Sudeep Pillai (spillai@csail.mit.edu) Mar 24, 2013
// 

#ifndef MULTI_OBJECT_POSE_TRACKER_HPP_
#define MULTI_OBJECT_POSE_TRACKER_HPP_
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <deque>
#include <map>
#include <set> 
#include <numeric>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/glext.h>

// libbot/lcm includes
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>

#include <lcm/lcm.h>
#include <lcmtypes/bot2_param.h>

#include <lcmtypes/articulation_object_pose_msg_t.h>
#include <lcmtypes/articulation_object_pose_list_msg_t.h>
#include <lcmtypes/articulation_object_pose_track_msg_t.h>
#include <lcmtypes/articulation_object_pose_track_list_msg_t.h>
#include <lcmtypes/articulation_articulated_object_msg_t.h>

#include <lcmtypes/vs_obj_collection_t.h>
#include <lcmtypes/vs_obj_t.h>
#include <lcmtypes/vs_link_collection_t.h>
#include <lcmtypes/vs_link_t.h>
#include <lcmtypes/vs_cov_collection_t.h>
#include <lcmtypes/vs_cov_t.h>
#include <lcmtypes/vs_point3d_list_collection_t.h>
#include <lcmtypes/vs_point3d_list_t.h>
#include <lcmtypes/vs_reset_collections_t.h>
#include <lcmtypes/vs_collection_config_t.h>
#include <lcmtypes/vs_text_collection_t.h>
#include <lcmtypes/vs_text_t.h>

// pcl includes
// #include <pcl/pcl_macros.h>
// #include <pcl/io/pcd_io.h>
// #include <pcl/PointIndices.h>
#include <pcl/registration/icp.h>
#include <pcl/ModelCoefficients.h>
// #include <pcl/point_types.h>

#include <pcl/filters/extract_indices.h>
#include <pcl/filters/random_sample.h>

#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>

#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>

#include <pcl/features/normal_3d.h>
#include <pcl/features/3dsc.h>
#include <pcl/features/impl/3dsc.hpp>
#include <pcl/features/usc.h>
#include <pcl/features/pfh.h>
#include <pcl/features/moment_invariants.h>

#include <pcl/registration/transformation_estimation.h>
#include <pcl/registration/transformation_estimation_svd.h>
#include <pcl/registration/transformation_estimation_lm.h>

// opencv includes
#include <opencv2/opencv.hpp>

#include <perception_matlab_utils/matlab_engine_utils.hpp>
#include <perception_matlab_utils/matlab_opencv_utils.hpp>

// More utilities
// #include "sc3d.h"
#include "feature_types.hpp"
#include "TrackletManager.hpp"
#include "PosePairManager.hpp"

const double pose_noise = 0.01f;
const double vel_est_noise = 0.01f;
const double omega_est_noise = 0.01f;
const double acc_noise = 0.f;
const double dt = 0.01;
const double gravity = 9.8 * 0.001;
static double varRNN_RADIUS = 1.f;
static const float MIN_NORM = 1e-6; 
// Shape Context Histogram Comparison Method
// CV_COMP_CORREL, CV_COMP_CHISQR, 
// CV_COMP_INTERSECT, CV_COMP_BHATTACHARYYA

// MTK's pose and orientation definition:
#include <mtk/types/pose.hpp>
#include <mtk/types/SOn.hpp>

#include <mtk/build_manifold.hpp>
#include <mtk/mean_and_covar.hpp>
#include <ukfom/ukf.hpp>
#include <ukfom/mtkwrap.hpp>

typedef MTK::vect<3, double> Vec3;
typedef MTK::SO3<double> Pose;
typedef Eigen::Matrix<double,3,3> Matrix3x3;

// Define a Compund manifold
MTK_BUILD_MANIFOLD(object_state_, /* Name of the manifold */
	((Vec3, pos))      /* each sub-variable in form ((type, name)) */
	((Pose, orient))    /* no comma between variables! */
	((Vec3, vel))      /* each sub-variable in form ((type, name)) */
)	                   /* semicolon is optional */

typedef ukfom::mtkwrap<object_state_> object_state;
typedef Eigen::Matrix<double, object_state::DOF, object_state::DOF> Matrix9x9;

struct IndexInfo { 
    int slice_idx; 
    int full_idx;
    IndexInfo () : slice_idx(-1), full_idx(-1) {}
    IndexInfo (int _full_idx, int _slice_idx) : slice_idx(_slice_idx), full_idx(_full_idx) {};
};


#if 1
int 
static bot_matrix_to_quat2(const double rot[9], double quat[4]) { 

    float tr = rot[0] + rot[4] + rot[8];

    if (tr > 0) { 
        float S = sqrt(tr+1.0) * 2; // S=4*qw 
        quat[0] = 0.25 * S;
        quat[1] = (rot[7] - rot[5]) / S;
        quat[2] = (rot[2] - rot[6]) / S; 
        quat[3] = (rot[3] - rot[1]) / S; 
    } else if ((rot[0] > rot[4])&(rot[0] > rot[8])) { 
        float S = sqrt(1.0 + rot[0] - rot[4] - rot[8]) * 2; // S=4*qx 
        quat[0] = (rot[7] - rot[5]) / S;
        quat[1] = 0.25 * S;
        quat[2] = (rot[1] + rot[3]) / S; 
        quat[3] = (rot[2] + rot[6]) / S; 
    } else if (rot[4] > rot[8]) { 
        float S = sqrt(1.0 + rot[4] - rot[0] - rot[8]) * 2; // S=4*qy
        quat[0] = (rot[2] - rot[6]) / S;
        quat[1] = (rot[1] + rot[3]) / S; 
        quat[2] = 0.25 * S;
        quat[3] = (rot[5] + rot[7]) / S; 
    } else { 
        float S = sqrt(1.0 + rot[8] - rot[0] - rot[4]) * 2; // S=4*qz
        quat[0] = (rot[3] - rot[1]) / S;
        quat[1] = (rot[2] + rot[6]) / S;
        quat[2] = (rot[5] + rot[7]) / S;
        quat[3] = 0.25 * S;
    }

    return 0;
}
#else
int 
static bot_matrix_to_quat2(const double rot[9], double quat[4])
{
    quat[0] = 0.5*sqrt(fabs(rot[0]+rot[4]+rot[8]+1));

  if (quat[0] != quat[0]) { 
      printf("ERRR!!!!!!!!!!!!!!!!!!!!!!!!!! quat[0] - %4.3f (%4.3f)\n", quat[0], rot[0]+rot[4]+rot[8]+1);
      assert(0);
  }

  if (fabs(quat[0]) > 1e-8) {
      double w4 = 1.0/(4.0*quat[0]);
    quat[1] = (rot[7]-rot[5]) * w4;
    quat[2] = (rot[2]-rot[6]) * w4;
    quat[3] = (rot[3]-rot[1]) * w4;
  }
  else {
    quat[1] = sqrt(fabs(-0.5*(rot[4]+rot[8])));
    quat[2] = sqrt(fabs(-0.5*(rot[0]+rot[8])));
    quat[3] = sqrt(fabs(-0.5*(rot[0]+rot[4])));
  }

  /*LSF: I may be missing something but this didn't work for me until I divided by the magnitude instead:
    double norm = quat[0]*quat[0] + quat[1]*quat[1] + quat[2]*quat[2] +
                     quat[3]*quat[3];
  if (fabs(norm) < 1e-10)
  return -1; */
  double norm = sqrt(quat[0]*quat[0] + quat[1]*quat[1] + quat[2]*quat[2] +
                     quat[3]*quat[3]);

  norm = 1/norm;
  quat[0] *= norm;
  quat[1] *= norm;
  quat[2] *= norm;
  quat[3] *= norm;

  return 0;
}
#endif

static void bot_core_pose_to_mat(const bot_core_pose_t& msg, double* T) { 

    double R[9];
    bot_quat_to_matrix(msg.orientation, R);

    T[3] = msg.pos[0], T[7] = msg.pos[1], T[11] = msg.pos[2];

    T[0] = R[0], T[1] = R[1], T[2] = R[2];
    T[4] = R[3], T[5] = R[4], T[6] = R[5];
    T[8] = R[6], T[9] = R[7], T[10] = R[8];
    T[12] = 0, T[13] = 0, T[14] = 0, T[15] = 1;
    return;
}

static 
void mat_to_bot_core_pose(double* T, bot_core_pose_t& msg) { 

    double R[9];
    R[0] = T[0], R[1] = T[1], R[2] = T[2];
    R[3] = T[4], R[4] = T[5], R[5] = T[6];
    R[6] = T[8], R[7] = T[9], R[8] = T[10];

    bot_matrix_to_quat2(R, msg.orientation);
    msg.pos[0] = T[3] , msg.pos[1] = T[7], msg.pos[2] = T[11];
    return;
}

static 
cv::Mat_<double> rigid_body_invert(const cv::Mat_<double>& T) { 
    cv::Mat_<double> Tinv = cv::Mat_<double>::eye(4,4);
        
    cv::Mat Rinv = Tinv(cv::Rect(0,0,3,3));
    cv::Mat tinv = Tinv(cv::Rect(3,0,1,3));

    cv::Mat Rt = T(cv::Rect(0,0,3,3)).clone().t();
    Rt.copyTo(Rinv);

    cv::Mat t = T(cv::Rect(3,0,1,3));
    cv::Mat(-Rt*t).copyTo(tinv);

    // std::cerr << "T: " << T << " " << Rinv << " " << tinv << std::endl << " tinv:" << Tinv << std::endl;
    return Tinv;
}

static
bool make_coordinate_tf(pcl::Normal& normal, bot_core_pose_t& pose) { 
    // x aligns with the normal
    cv::Vec3f t1(normal.normal[0], 
                 normal.normal[1], 
                 normal.normal[2]);

    // cooked up y
    cv::Vec3f ytilda(0,1,0);
                    
    // compute z
    cv::Vec3f z1 = t1.cross(ytilda); 
    float z1norm = cv::norm(z1);
    if (z1norm < MIN_NORM) return false;
    z1 /= z1norm;

    // new y
    cv::Vec3f y1 = z1.cross(t1); 
    float y1norm = cv::norm(y1);
    if (z1norm < MIN_NORM) return false;
    y1 /= y1norm;

    double R[9]; 
    R[0] = t1[0], R[1] = y1[0], R[2] = z1[0];
    R[3] = t1[1], R[4] = y1[1], R[5] = z1[1];
    R[6] = t1[2], R[7] = y1[2], R[8] = z1[2];
    bot_matrix_to_quat2(R, pose.orientation);
    return true;
}

static 
float  l2_dist_float_vec(const std::vector<float>& v1, const std::vector<float>& v2) { 
    assert(v1.size() == v2.size());
    float norm;
    for (int j=0; j<v1.size(); j++) { 
        float v = v1[j] - v2[j];
        norm += v*v;
    }
    return std::sqrt(norm);
}

    // MatlabEngine matlab;

// Params for sc3d_test
const float MOPT_MAX_TRACK_TIMESTAMP_DELAY_SEC = spvision::TIME_SLICES * spvision::TIME_SLICE_RATE_SEC * 2; 
const float MOPT_MAX_TRACK_TIMESPAN_SEC = spvision::TIME_SLICES * spvision::TIME_SLICE_RATE_SEC;

// std::vector<int64_t> track_list_pcl_ids; // (NUM_TRACKS); 

// // For k-d tree construction
// std::vector<int64_t> endeff_track_ids; 
// pcl::PointCloud<pcl::PointXYZRGB>::Ptr endeff_track_cloud;  
// pcl::PointCloud<pcl::Normal>::Ptr endeff_track_cloud_tangents; 


static bool sort_pending_by_length_desc(const std::pair<int, int>& lhs, 
                                        const std::pair<int, int>& rhs) { 
    return lhs.second > rhs.second; 
}

namespace spvision { 
class MultiObjectPoseTracker { 
private: 

public: 

    //--------------------------------------------
    // PosePair manager
    //--------------------------------------------
    PosePairManager posepair_manager;

    //--------------------------------------------
    // Tracklet manager
    //--------------------------------------------
    TrackletManager tracklet_manager;
    BotTrans sensor_frame;
    BotFrames*  frames;
    lcm_t* lcm;

    int64_t utime_now;

public: 
    MultiObjectPoseTracker();
    MultiObjectPoseTracker(const MultiObjectPoseTracker& tracker);
    ~MultiObjectPoseTracker();       
    MultiObjectPoseTracker& operator=(const MultiObjectPoseTracker& tracker) { return *this; }

    void setState(lcm_t* _lcm, BotFrames* _frames) { 
        lcm = _lcm, frames = _frames; 
        bot_frames_get_trans (frames, "KINECT", "local", &sensor_frame);
    }

    int numTracks() { return tracklet_manager.tracklets.size(); }
    // void update(int64_t utime, const erlcm_tracklet_list_t* tracks_msg);
    void update(int64_t utime, const spvision::UniquePoseMap& track_features);
    void computeTrackTangents();       
    void computeInterpolatedTrackNormals();
    void computeTrackNormalsDescription();
    void computeTrackDescription();
    bool constructTrackletGraph();
    void viz(); 
    void vizPose();

    // bool inited() { return _inited; };
    // void init(); 
     
    void reinit();

    // void initTracker();

    // void setParams(char* str, double value); 
    // void setDefaultParams(); 

    // // template <typename T>
    // // normals size and tree size should be same
    // void update(int64_t utime, cv::Mat& img, 
    //             pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, 
    //             pcl::PointCloud<pcl::Normal>::Ptr& normals,
    //             pcl::search::KdTree<pcl::PointXYZRGB>::Ptr& tree);

    // std::list<SuperFeature>
    // convertToFeatures(int64_t utime, const TSimpleFeatureList& _feats, 
    //                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud,                     
    //                   pcl::PointCloud<pcl::Normal>::Ptr& normals, 
    //                   pcl::search::KdTree<pcl::PointXYZRGB>::Ptr& tree);

    // void
    // normalEstimation(std::vector<SuperFeature>& feats, 
    //                  const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, 
    //                  const std::vector<int>& inds);           

    // void setVerbose(bool mode) { VERBOSE_MODE = mode; }
}; 

}
#endif // MULTI_OBJECT_POSE_TRACKER_HPP_
