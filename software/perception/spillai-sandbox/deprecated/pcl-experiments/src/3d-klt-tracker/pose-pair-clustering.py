#!/usr/bin/env python
import numpy as np
import cv as cv
import cv2.cv as cv2
import matplotlib as mpl
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from collections import namedtuple

# tuple keys for dict
#from namedcollections import namedtuple
# ===== Parameters ==== 
MIN_TRACK_LENGTH = 10; 
MIN_TRACK_INTERSECTION = 10;
MIN_TRAJECTORY_DISTANCE_SQ = np.square(0.05);
POSEPAIR_DISTANCE_SIGMA = 0.015; # sig = 0.015 m
POSEPAIR_DISTANCE_SIGMA_NORM = 0.015 * np.sqrt(2*np.pi); 
POSEPAIR_THETA_SIGMA = 10 * np.pi / 180; # sig = 5 deg
POSEPAIR_THETA_SIGMA_NORM = 0.015 * np.sqrt(2*np.pi);

# ===== Matplotlib ==== 
mpl.rcParams['figure.figsize'] = [8,8]

class PosePairDistribution:
    def __init__(self, data, key, val):
        self.idx1, self.idx2 = key;
        self.overlap_utimes = val;
        tj = data[key[0], val, :];
        tk = data[key[1], val, :];

        self.distance_hist = np.sum(np.square(tj[:,2:5:]-tk[:,2:5]), axis=1); # x,y,z (2,3,4)
        self.zm_distance_hist = self.distance_hist - np.mean(self.distance_hist)        
        self.distance_pdf = POSEPAIR_DISTANCE_SIGMA_NORM * \
            mlab.normpdf(self.zm_distance_hist, 0, POSEPAIR_DISTANCE_SIGMA); 
        self.distance_match = np.mean(self.distance_pdf);
        
        self.normal_hist = np.arccos(np.sum(np.multiply(tj[:,-3:],tk[:,-3:]), axis=1)); # normals (-3:)
        self.zm_normal_hist = self.normal_hist - np.mean(self.normal_hist);
        self.normal_pdf = POSEPAIR_THETA_SIGMA_NORM * \
            mlab.normpdf(self.zm_normal_hist, 0, POSEPAIR_THETA_SIGMA);
        self.normal_match = np.mean(self.normal_pdf);    

# ===== Get opt args ==== 
fn = '/home/spillai/data/2013_articulation_test/book_unfolding/lcmlog-2013-03-16.00-feats.yml';

# ===== Load the data ==== 
print '===> Loading data: ', fn, '...'
# Imagine 3D spatio-temporal data (FEATS x UTIMES x DIMS)
feature_ids = np.asarray(cv2.Load(fn, cv.CreateMemStorage(), 'feature_ids'));
feature_utimes = np.asarray(cv2.Load(fn, cv.CreateMemStorage(), 'feature_utimes'));
feature_data = np.asarray(cv2.Load(fn, cv.CreateMemStorage(), 'feature_data'));

# ===== Temporary: subset ==== 
feature_ids = feature_ids[:200];
feature_utimes = feature_utimes[:100];
feature_data = feature_data[:200, :100, :];

[num_feats, num_utimes, num_dims] = feature_data.shape;
if not feature_ids.size == num_feats: raise AssertionError
if not feature_utimes.size == num_utimes: raise AssertionError
print 'Num features: ', num_feats
print 'Num utimes: ', num_utimes
print 'Num dims: ', num_dims

# ===== Pruning ====
# sum over all UTIMES, and check nz for x value
print '===> Pruning features ...'
valid_feat_inds, = np.where(np.sum(feature_data[:,:,0] != 0,
                                   axis=1)>MIN_TRACK_LENGTH)

# # remove feats that don't move much
# tmp_valid_feat_inds = [];
# for j in valid_feat_inds:
#     # find nz utime indices 
#     ut_inds, = np.where(feature_data[j,:,0] != 0);
#     d = np.square(np.linalg.norm(np.max(feature_data[j,ut_inds,2:5]) - np.min(feature_data[j,ut_inds,2:5],axis=0)));
#     if d >= MIN_TRAJECTORY_DISTANCE_SQ: tmp_valid_feat_inds.append(j);
# valid_feat_inds = tmp_valid_feat_inds;
print '--- Done pruning features'

# ===== Viz data ====
# np.kron(a, [[1,1],[1,1]]
# np.repeat(a, 5, axis=1);
plt.matshow(np.repeat(feature_data[:,:,0] != 0, 1, axis=0), interpolation='nearest', cmap=plt.cm.jet);
plt.title('Tracks');
plt.xlabel('Time');
plt.ylabel('Features');

# ===== Find overlapping tracks ====
print '===> Extracting overlapping features ...'
overlap_dict = dict(); # {}
for j in valid_feat_inds:
    # find nz utime indices
    tj, = np.where(feature_data[j,:,0] != 0)
    if j % 200 == 0: print 'Processing: ', j

    for k in valid_feat_inds:
        if k <= j: continue;
           
        # find nz utime indices
        tk, = np.where(feature_data[k,:,0] != 0);

        # set intersection
        intersection = np.intersect1d(tj,tk);
        if intersection.size < MIN_TRACK_INTERSECTION: continue;

        overlap_dict[tuple(sorted((j,k)))] = intersection;
print '--- Done extracting overlapping features'

# ===== Compute Pose-Pairs for overlapping tracks ====
print '===> Compute pose-pairs ...'
Anormal = np.zeros((num_feats, num_feats));
Adist = np.zeros((num_feats, num_feats));
posepair_dict = {};
for key,val in overlap_dict.iteritems():
        ppd = PosePairDistribution(feature_data, key, val);
        posepair_dict[key] = ppd;
        Anormal[key[0],key[1]] = ppd.normal_match;
        Adist[key[0],key[1]] = ppd.distance_match;
print '--- Done computing pose-pairs'

# # ===== Viz histogram of pose-pair-normal distributions =====
posepair_normal_hist = [v.zm_normal_hist for k,v in posepair_dict.iteritems()]
posepair_normal_hist = np.hstack(posepair_normal_hist);
posepair_distance_hist = [v.zm_distance_hist for k,v in posepair_dict.iteritems()]
posepair_distance_hist = np.hstack(posepair_distance_hist)

fig = plt.figure()
ax = fig.add_subplot(121)
n, bins, patches = ax.hist(posepair_normal_hist, 100, normed=1, facecolor='green', alpha=0.75)
bincenters = 0.5*(bins[1:]+bins[:-1])
y = mlab.normpdf(bincenters, 0, POSEPAIR_THETA_SIGMA)

ax = fig.add_subplot(122)
n, bins, patches = ax.hist(posepair_distance_hist, 100, normed=1, facecolor='blue', alpha=0.75)
bincenters = 0.5*(bins[1:]+bins[:-1])
y = mlab.normpdf(bincenters, 0, POSEPAIR_DISTANCE_SIGMA)

# # ===== Viz Pose-Pairs adjacency matrix ====
# print """Plot visualizing the difference in normal matching"""
# sz = [1]*1;
# plt.matshow(np.kron(Anormal, [sz]*1), interpolation='nearest', cmap=plt.cm.jet);
# plt.title('Difference of Normal matching');
# plt.xlabel('Features')
# plt.ylabel('Features')
# plt.colorbar();

# print """Plot visualizing the difference in euclidean distance matching"""
# plt.matshow(np.kron(Adist, [sz]*1), interpolation='nearest', cmap=plt.cm.jet);
# plt.title('Difference of Euclidean distance matching');
# plt.xlabel('Features')
# plt.ylabel('Features')
# plt.colorbar();

# # ===== Sort tracks by energy statistic  ====
# tracken_dict = dict();
# sorted_feat_inds = []

# #FeatureInfo = namedtuple("FeatureInfo", ["idx",""])
# #SortedPair(first=j, second=k);

# for j in valid_feat_inds:
#     # find nz utime indices 
#     ut_inds, = np.where(feature_data[j,:,0] != 0);
#     d = np.max(feature_data[j,ut_inds,2:5]) - np.min(feature_data[j,ut_inds,2:5],axis=0);
#     sorted_feat_inds.append((j,np.square(np.linalg.norm(d))));
# sorted_feat_inds.sort(key=lambda x: x[1], reverse=True);
# sorted_feat_inds = np.array(sorted_feat_inds,dtype='int')[:,0];

# # ===== Viz Pose-Pairs adjacency matrix ====
# # advanced slicing will also work
# Anormal_sorted = Anormal[:,sorted_feat_inds][sorted_feat_inds]
# Adist_sorted = Adist[:,sorted_feat_inds][sorted_feat_inds]

# print """Plot visualizing the difference of Normal matching\
#     after being sorted by decreasing trajectory lengths"""
# sz = [1]*1;
# plt.matshow(np.kron(Anormal_sorted, [sz]*1), interpolation='nearest', cmap=plt.cm.jet);
# plt.title('Difference of Normal matching\n (sorted by decreasing trajectory lengths)');
# plt.xlabel('Features')
# plt.ylabel('Features')
# plt.colorbar();

# print """Plot visualizing the difference in euclidean distance matching\
#         after being sorted by decreasing trajectory lengths"""
# plt.matshow(np.kron(Adist_sorted, [sz]*1), interpolation='nearest', cmap=plt.cm.jet);
# plt.title('Difference of Euclidean distance matching\n (sorted by decreasing trajectory lengths)');
# plt.xlabel('Features')
# plt.ylabel('Features')
# plt.colorbar();


