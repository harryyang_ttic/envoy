// MRPT
#include <mrpt/vision.h> 	// For feature detection, etc.
#include <mrpt/gui.h>		// For visualization windows
#include <mrpt/base.h>

using namespace mrpt;
/* using namespace mrpt::hwdrivers; */
using namespace mrpt::utils;
/* using namespace mrpt::slam; */
using namespace mrpt::vision;
using namespace mrpt::poses;


mrpt::gui::CDisplayWindow3DPtr win;  // This is global such as an exception within the main program do not abruptly closes the window
CGenericFeatureTrackerAutoPtr  tracker;
TSimpleFeatureList  trackedFeats;

bool 		hasResolution = false;
TCamera		cameraParams; // For now, will only hold the image resolution on the arrive of the first frame.

// INITS
unsigned int	step_num = 0;

bool  SHOW_FEAT_IDS = true;
bool  SHOW_RESPONSES = true;
bool  SHOW_FEAT_TRACKS = true;

// const double SAVE_VIDEO_FPS = 30; // If DO_SAVE_VIDEO=true, the FPS of the video file
// const char*  SAVE_VIDEO_CODEC = "XVID"; // "XVID", "PIM1", "MJPG"
	
bool  DO_HIST_EQUALIZE_IN_GRAYSCALE = false;
// string VIDEO_OUTPUT_FILE = "./tracking_video.avi";

const double MAX_FPS = 5000; // 5.0;  // Hz (to slow down visualization).


// --------------------------------
// The main loop
// --------------------------------
CImage		previous_image;

TSequenceFeatureObservations    feat_track_history;
bool							save_tracked_history = true; // Dump feat_track_history to a file at the end

TCameraPoseID 					curCamPoseId = 0;
mrpt::opengl::COpenGLViewportPtr gl_view;

// Aux data for drawing the recent track of features:
static const size_t FEATS_TRACK_LEN = 10;
std::map<TFeatureID,std::list<TPixelCoord> >  feat_tracks;


void init_mprt() { 
    // win = mrpt::gui::CDisplayWindow3D::Create("Tracked features",800,600);

	// "CFeatureTracker_KL" is by far the most robust implementation for now:
	tracker = CGenericFeatureTrackerAutoPtr( new CFeatureTracker_KL );
	//tracker = CGenericFeatureTrackerAutoPtr( new CFeatureTracker_FAST );
	//tracker = CGenericFeatureTrackerAutoPtr( new CFeatureTracker_PatchMatch );

	tracker->enableTimeLogger(true); // Do time profiling.

	// Set of parameters common to any tracker implementation:
	// -------------------------------------------------------------
	// To see all the existing params and documentation, see mrpt::vision::CGenericFeatureTracker
	tracker->extra_params["remove_lost_features"]         = 1;   // automatically remove out-of-image and badly tracked features

	tracker->extra_params["add_new_features"]             = 1;   // track, AND ALSO, add new features
	tracker->extra_params["add_new_feat_min_separation"]  = 16;
	tracker->extra_params["minimum_KLT_response_to_add"]  = 10;
	tracker->extra_params["add_new_feat_max_features"]    = 350;
	tracker->extra_params["add_new_feat_patch_size"]      = 11;

	tracker->extra_params["update_patches_every"]		= 0;  // Don't update patches.

	tracker->extra_params["check_KLT_response_every"]	= 5;	// Re-check the KLT-response to assure features are in good points.
	tracker->extra_params["minimum_KLT_response"]	    = 5;

	// Specific params for "CFeatureTracker_KL"
	// ------------------------------------------------------
	tracker->extra_params["window_width"]  = 5;
	tracker->extra_params["window_height"] = 5;
	//tracker->extra_params["LK_levels"] = 3;
	//tracker->extra_params["LK_max_iters"] = 10;
	//tracker->extra_params["LK_epsilon"] = 0.1;
	//tracker->extra_params["LK_max_tracking_error"] = 150;

	/* cout << endl << "TO END THE PROGRAM: Close the window.\n"; */

	/* mrpt::opengl::COpenGLViewportPtr gl_view; */
	/* { */
	/* 	mrpt::opengl::COpenGLScenePtr scene = win->get3DSceneAndLock(); */
	/* 	gl_view = scene->getViewport("main"); */
	/* 	win->unlockAccess3DScene(); */
	/* } */
}
