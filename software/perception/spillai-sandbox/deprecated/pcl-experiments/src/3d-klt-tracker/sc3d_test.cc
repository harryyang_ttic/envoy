#include "sc3d_test.hpp"
#include <bot_lcmgl_client/lcmgl.h>

using namespace spvision;
const std::string PROCESS_NAME("SC3D_TEST");

object_state pose_process_model(const object_state& s, const Vec3& w) { 
    object_state s2;

    // apply rotation
    Vec3 scaled_axis = w * dt;
    MTK::SO3<double> rot = MTK::SO3<double>::exp(scaled_axis);
    s2.orient = s.orient * rot;
    
    // s2.vel = s.vel + (s.orient * a) * dt;
    s2.vel = s.vel; // constant velocity model
    
    // translate
    s2.pos = s.pos + s.vel * dt;

    return s2;
}

static object_state pose_measurement_model(const object_state& s) { 
    return s;
}

// Matrix3x3 gps_measurement_noise_cov(const object_state& s) { 
//     return gps_noise * Matrix3x3::Identity();
// }

static Matrix9x9 pose_measurement_noise_cov() { 
    return pose_noise * ukfom::ukf<object_state>::cov::Identity();
}

ukfom::ukf<object_state>::cov process_noise_cov() {
    ukfom::ukf<object_state>::cov cov = ukfom::ukf<object_state>::cov::Zero();
    setDiagonal ( cov , &object_state::pos , omega_est_noise * dt ) ;
    setDiagonal ( cov , &object_state::orient , omega_est_noise * dt ) ;
    setDiagonal ( cov , &object_state::vel , acc_noise * dt ) ;
    return cov;
}

static void 
on_tracks (const lcm_recv_buf_t *rbuf, const char *channel,
           const erlcm_tracklet_list_t *tracks_msg, void *user_data ) { 
    
    std::cerr << "on_object_tracks" << std::endl;
    double tic, t1 = bot_timestamp_now();
    const int NUM_TRACKS_IN = tracks_msg->num_tracks;
    if (!(NUM_TRACKS_IN + MOPT.numTracks())) return;
    std::cerr << "IN Num Tracks: " << tracks_msg->num_tracks << std::endl;

    //--------------------------------------------
    // Only process frames that are latest
    //--------------------------------------------
     int64_t utime = 0; 
     UniquePoseMap track_features; 
     for (int j=0; j<tracks_msg->num_tracks; j++) { 
         int64_t track_id = tracks_msg->tracks[j].track_id;
         bot_core_pose_t& pose = tracks_msg->tracks[j].poses[tracks_msg->tracks[j].num_poses-1];
         if (!tracks_msg->tracks[j].num_poses) continue;
         if (utime) assert(utime == pose.utime);
         else utime = pose.utime;
         track_features[track_id] = pose;
     }

     // //----------------------------------
     // // Track List update (and init if needed)
     // //----------------------------------
     // tic = bot_timestamp_now();
     // // perform_alg_inits();
     // printf("%s===> INITS: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    //----------------------------------
    // Update tracks in streaming form 
    //----------------------------------
    tic = bot_timestamp_now();
    MOPT.update(utime, track_features);
    printf("%s===> UPDATE: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    // //----------------------------------
    // // Tangent computation using LS fit over window
    // //----------------------------------
    // tic = bot_timestamp_now();
    // MOPT.computeTrackTangents(); 
    // printf("%s===> COMPUTE: TANGENTS: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    std::cerr << "NUM TRACKLETS: " << MOPT.numTracks() << std::endl;
    if (!MOPT.numTracks()) return; 
    const int NUM_TRACKS = MOPT.numTracks(); 


    // //----------------------------------
    // // Compute Interpolated Normals
    // //----------------------------------
    // tic = bot_timestamp_now();
    // MOPT.computeInterpolatedTrackNormals();
    // printf("%s===> COMPUTE: INTERPOLATED NORMALS: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 


    // //----------------------------------
    // // Shape Contexts for trajectory matching 
    // //----------------------------------
    // tic = bot_timestamp_now();
    // MOPT.computeTrackDescription(); 
    // printf("%s===> COMPUTE: SHAPE CONTEXT: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    // //----------------------------------
    // // Normal Descriptor
    // //----------------------------------
    // tic = bot_timestamp_now();
    // MOPT.computeTrackNormalsDescription(); 
    // printf("%s===> COMPUTE: NORMAL DESC: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    // //----------------------------------
    // // Compute Tracklet graph and pose
    // //----------------------------------
    // tic = bot_timestamp_now();
    // MOPT.constructTrackletGraph();
    // printf("%s===> CONSTRUCT TRACKLET GRAPH: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    //----------------------------------
    // Viz (track_cloud)
    //----------------------------------
    tic = bot_timestamp_now();
    MOPT.viz(); 
    printf("%s===> VIZ: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    // //----------------------------------
    // // Viz Pose
    // //----------------------------------
    // tic = bot_timestamp_now();
    // MOPT.vizPose(); 
    // printf("%s===> VIZ POSE: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    printf("%s===> TOTAL TIME: %4.2f ms%s\n", esc_red, (bot_timestamp_now() - t1) * 1e-3, esc_def); 
    return;
}

int 
main(int argc, char **argv)
{

    const char *optstring = "dfnsha";
    int c;

    g_thread_init(NULL);
    setlinebuf (stdout);
    state = new state_t; 

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
    state->p_server = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->p_server);

    MOPT.setState(state->lcm, state->frames);

    // LCM-related
    state->mainloop = g_main_loop_new( NULL, FALSE );  

    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }

    // articulation_object_pose_track_list_msg_t_subscribe
    // (state->lcm, "ARTICULATION_OBJECT_POSE_TRACK_LIST", on_object_tracks, NULL);
    erlcm_tracklet_list_t_subscribe(state->lcm, "TRACKLETS", on_tracks, NULL);

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}

//     //----------------------------------
//     // Compute pose from clusters
//     //----------------------------------
//     // pose_from_clusters(tracks_msg, labels);

//     //----------------------------------
//     // Publish point cloud to viewer
//     //----------------------------------
//     erlcm_xyzrgb_point_list_t pt_list;
//     pt_list.utime = bot_timestamp_now();
//     pt_list.no_points = 0; 
//     pt_list.points = NULL; 
//     pt_list.no_faces = 0; 
//     pt_list.faces = 0; 
//     idx = 0;

//     cv::Mat3b track_colors_mat(track_colors); 
//     cv::cvtColor(track_colors_mat, track_colors_mat, CV_HSV2RGB);
//     for (int j=0; j<tracks_msg->num_tracks; j++) { 
//         pt_list.no_points += tracks_msg->tracks[j].num_poses;
//         pt_list.points = (erlcm_xyzrgb_point_t*) 
//             realloc(pt_list.points, pt_list.no_points * sizeof(erlcm_xyzrgb_point_t));
        
//         const int label = labels(j,0);
//         int R = int(track_colors_mat(0,label)[0]), 
//             G = int(track_colors_mat(0,label)[1]), 
//             B = int(track_colors_mat(0,label)[2]); 

//         for (int k=0; k<tracks_msg->tracks[j].num_poses; k++) {
//             erlcm_xyzrgb_point_t pt; 
//             pt.xyz[0] = tracks_msg->tracks[j].pose[k].pos[0], 
//             pt.xyz[1] = tracks_msg->tracks[j].pose[k].pos[1], 
//             pt.xyz[2] = tracks_msg->tracks[j].pose[k].pos[2];

//             pt.rgba = 0;
//             pt.rgba |= 100; 
//             pt.rgba |= (int)B << 8; 
//             pt.rgba |= (int)G << 16; 
//             pt.rgba |= (int)R << 24;
//             pt_list.points[idx++] = pt;
//         }
//     }
//     erlcm_xyzrgb_point_list_t_publish(state->lcm, "PCL_XYZRGB_LIST", &pt_list);
//     free(pt_list.points);

//     //----------------------------------
//     // Publish track normals to viewer
//     //----------------------------------
//     erlcm_normal_point_list_t normal_pt_list;
//     normal_pt_list.utime = bot_timestamp_now();
//     normal_pt_list.no_points = 0;
//     normal_pt_list.points = NULL; // (erlcm_normal_point_t*) malloc(all_tracks.size() * 
//     //                       sizeof(erlcm_normal_point_t));
//     idx=0;
//     for (int j=0; j<tracks_msg->num_tracks; j++) { 
//         normal_pt_list.no_points += tracks_msg->tracks[j].num_poses;
//         normal_pt_list.points = (erlcm_normal_point_t*) 
//             realloc(normal_pt_list.points, normal_pt_list.no_points * sizeof(erlcm_normal_point_t));
        
//         for (int k=0; k<tracks_msg->tracks[j].num_poses; k++) {
//             erlcm_normal_point_t pt; 

//             double T[16];
//             bot_quat_pos_to_matrix(tracks_msg->tracks[j].pose[k].orientation, 
//                                    tracks_msg->tracks[j].pose[k].pos, T);
            
//             pt.xyz[0] = tracks_msg->tracks[j].pose[k].pos[0], 
//             pt.xyz[1] = tracks_msg->tracks[j].pose[k].pos[1], 
//             pt.xyz[2] = tracks_msg->tracks[j].pose[k].pos[2];
//             // std::cerr << "T: " << cv::Mat_<double>(4, 4, T) << std::endl;
//             pt.normals[0] = T[4], 
//                 pt.normals[1] = T[5], 
//                 pt.normals[2] = T[6];
//             normal_pt_list.points[idx++] = pt;
//         }
//     }
//     erlcm_normal_point_list_t_publish(state->lcm, "PCL_NORMAL_LIST", &normal_pt_list);
//     free(normal_pt_list.points);

//     //----------------------------------
//     // Publish point cloud to viewer
//     //----------------------------------
//     erlcm_segment_list_t seg_list;
//     seg_list.utime = bot_timestamp_now();
//     seg_list.no_segments = desc_idx.size();
//     seg_list.segments = (erlcm_seg_point_list_t*) malloc(desc_idx.size() * sizeof(erlcm_seg_point_list_t));
//     for (int j=0; j<tracks_msg->num_tracks; j++) { 
//         int id = desc_idx[j];
//         int label = labels.at<int>(j);

//         erlcm_seg_point_list_t& seg = seg_list.segments[j];
//         seg.no_points = tracks_msg->tracks[j].num_poses;
//         seg.segment_id = label;
//         seg.points = (erlcm_xyz_point_t*) malloc(tracks_msg->tracks[j].num_poses * sizeof(erlcm_xyz_point_t));
//         seg.coefficients[0] = seg.coefficients[1] = seg.coefficients[2] = seg.coefficients[3] = 0;
//         seg.centroid[0] = seg.centroid[1] = seg.centroid[2] = 0;
//         for (int k=0; k<tracks_msg->tracks[j].num_poses; k++) {
//             erlcm_xyz_point_t pt;
//             pt.xyz[0] = tracks_msg->tracks[j].pose[k].pos[0], 
//                 pt.xyz[1] = tracks_msg->tracks[j].pose[k].pos[1], 
//                 pt.xyz[2] = tracks_msg->tracks[j].pose[k].pos[2]; 
//             seg.points[k] = pt;
//         }
//     }
//     erlcm_segment_list_t_publish(state->lcm, "PCL_SEGMENT_LIST", &seg_list);
//     for (int j=0; j<seg_list.no_segments; j++)
//         free(seg_list.segments[j].points);
//     free(seg_list.segments);



#if 0
                double dTj0_w[16];
                const bot_core_pose_t& pose_j0_w = pair_map[pair][ref_time];
                bot_core_pose_to_mat(pose_j0_w, dTj0_w);
                cv::Mat_<double> Tj0_w(4,4,dTj0_w); 
                cv::Mat_<double> Tw_j0 = rigid_body_invert(Tj0_w);

                double dTOc0_w[16];
                bot_core_pose_to_mat(Oc, dTOc0_w);
                cv::Mat_<double> TOc0_w(4,4,dTOc0_w);

                if (observations.size() != track_list[idxj].tangents->size())
                    observations.resize(track_list[idxj].tangents->size());

                for (int l=0; l<track_list[idxj].tangents->size(); l+=10) { 
                    double dTj1_w[16]; 

                    if (pair_map.find(pair) == pair_map.end()) 
                        assert(0);

                    const bot_core_pose_t& pose_j1_w = pair_map[pair][l];

                    bot_core_pose_to_mat(pose_j1_w, dTj1_w);

                    cv::Mat_<double> Tj1_w(4,4,dTj1_w); 
                    cv::Mat_<double> Tj1_j0 = Tj1_w * Tw_j0;
                    cv::Mat_<double> TOc1_w = Tj1_j0 * TOc0_w;

                    // publish
                    bot_core_pose_t pose_Oc1_w;
                    double* dTOc1_w = (double*)TOc1_w.data;
                    mat_to_bot_core_pose(dTOc1_w, pose_Oc1_w);
                    //pose_list_msg.push_back(pose_Oc1_w);

                    object_state st;
                    Vec3 pos; st.pos << pose_Oc1_w.pos[0],pose_Oc1_w.pos[1],pose_Oc1_w.pos[2];
                    Vec3 vel; if (l<1) vel<<0.f,0.f,0.f; 
                    st.vel << 0.f,0.f,0.f;
                    st.orient = Pose(pose_Oc1_w.orientation[0], pose_Oc1_w.orientation[1],
                                     pose_Oc1_w.orientation[2],pose_Oc1_w.orientation[3]);

                    
                    if ((st.pos[0] != st.pos[0]) || (st.pos[1] != st.pos[1]) || 
                        (st.pos[2] != st.pos[2])) { 
                        std::cerr << "pose_j1_w: " << pose_j1_w.pos[0] << " " 
                                  << pose_j1_w.pos[1] << " " 
                                  << pose_j1_w.pos[2] << " " 
                                  << pose_j1_w.orientation[0] << " " 
                                  << pose_j1_w.orientation[1] << " " 
                                  << pose_j1_w.orientation[2] << " " 
                                  << pose_j1_w.orientation[3] << " " << std::endl;

                        std::cerr << "object_state: " << st << std::endl;
                        std::cerr << "Tj1_w: " << Tj1_w << std::endl;
                        std::cerr << "Tj1_j0: " << Tj1_j0 << std::endl;
                        std::cerr << "TOc0_w: " << TOc0_w << std::endl;
                        assert(0); // this shouldn't happen
                    } else 
                        observations[l].push_back(st);
                    // break; // only for last point in the trajectory
                }
            }
            // break; // please remove (only for test purposes)
        }

        std::vector<object_state> refined_states(observations.size());
        
        ukfom::ukf<object_state>::cov init_cov = 0.01 * ukfom::ukf<object_state>::cov::Identity();
        ukfom::ukf<object_state>::cov process_noise_covar = 0.01 * ukfom::ukf<object_state>::cov::Identity();
        // ukfom::ukf<object_state>* kf; // (object_state(), init_cov); //   = NULL; 

        object_state mean; 
        Eigen::Matrix<double, object_state::DOF, object_state::DOF> pcov;
        MTK::mean_and_covariance(mean, pcov, observations[0]); 
        ukfom::ukf<object_state> kf(mean, init_cov);

        for (int j=0; j<observations.size(); j++) { 
            if (!observations[j].size()) continue;
            std::vector<object_state>& obs = observations[j];
            
            object_state mean; 
            Vec3 omega; omega << 0.f,0.f,0.f;
            Eigen::Matrix<double, object_state::DOF, object_state::DOF> pcov;
            MTK::mean_and_covariance(mean, pcov, obs); 

            if ((mean.pos[0] != mean.pos[0]) || (mean.pos[1] != mean.pos[1]) || 
                (mean.pos[2] != mean.pos[2])) { 

                for (int k=0; k<obs.size(); k++) 
                    std::cerr << "obs: " << obs[k] << std::endl;

                std::cerr << "Mean: " << mean << std::endl;
                std::cerr << "Cov: " << pcov << std::endl;
            }
            // std::cerr << mean.pos[0] << " " << mean.pos[1] << " " << mean.pos[2] << " " 
            //           << mean.orient.w() << " " << mean.orient.x() << " " << mean.orient.y() << " " 
            //           << mean.orient.z() << std::endl;

            
            // // std::cerr << "init_cov: " << pcov << std::endl;
            if (j > 0) { 

                // std::cerr << "process noise covar: " << process_noise_covar << std::endl;
                kf.predict(boost::bind(pose_process_model, _1, omega), process_noise_covar); 

                // std::cerr << "pose_meas._noise_covar noise covar: " << std::endl << pose_measurement_noise_cov() << std::endl;
                // std::cerr << "object_state: " << mean << std::endl;
                kf.update(mean, pose_measurement_model, pose_measurement_noise_cov());
            }
                
            // update state
            // refined_states[j] = kf.mu();
            object_state mean2 = kf.mu();
            // std::cerr << "KF: cov: " << kf.sigma().block(0,0,3,3) << std::endl;
            Matrix3x3 cov = kf.sigma().block(0,0,3,3); 

            // publish
            bot_core_pose_t pose_Oc1_w;
            pose_Oc1_w.pos[0] = mean2.pos[0], pose_Oc1_w.pos[1] = mean2.pos[1], pose_Oc1_w.pos[2] = mean2.pos[2];
            pose_Oc1_w.orientation[0] = mean2.orient.w(), pose_Oc1_w.orientation[1] = mean2.orient.x(), 
                pose_Oc1_w.orientation[2] = mean2.orient.y(), pose_Oc1_w.orientation[3] = mean2.orient.z();

            pose_list_msg.push_back(pose_Oc1_w);
        }
        // delete kf;
#endif





// #if 0
//     //----------------------------------
//     // Viz
//     //----------------------------------
//     cv::Mat3b adjviz;
//     cv::Mat1b normadj; cv::normalize(adj_mat, normadj, 0, 255, cv::NORM_MINMAX, CV_8UC1);
//     cv::cvtColor(normadj, adjviz, CV_GRAY2BGR);
//     cv::cvtColor(adjviz, adjviz, CV_BGR2HSV);
//     std::vector<cv::Vec3b> track_colors(label_id+1);
//     opencv_utils::fillColors(track_colors); 
//     for (int k=0; k<adj_mat.rows; k++) 
//         for (int l=0; l<3; l++)
//             adjviz(k, std::min(k + l, adj_mat.cols)) = track_colors[labels[k]];
//     cv::cvtColor(adjviz, adjviz, CV_HSV2BGR);
//     opencv_utils::imshow("Adjacency matrix", adjviz);
//     cv::waitKey(500);    

// #endif


// void populate_viz_cloud(vs_point3d_list_t& viz_list, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud, 
//                         cv::Vec3b& color, int64_t object_id = bot_timestamp_now()) { 
//     cv::Vec3f _color = color * 1.f / 255.f; 
//     populate_viz_cloud(viz_list, track_cloud, _color, object_id);
//     return;
// }


//----------------------------------
// Use 3D Shape Contexts to cluster tracklets
//----------------------------------
// #if 0
// static void 
// on_object_tracks (const lcm_recv_buf_t *rbuf, const char *channel,
//                   const articulation_object_pose_track_list_msg_t *tracks_msg, void *user_data ) { 
    
//     std::cerr << "on_object_tracks" << std::endl;

//     int idx = 0;
//     int num_tracks = tracks_msg->num_tracks;
//     if (!num_tracks) return;

//     //----------------------------------
//     // Build desc mat
//     //----------------------------------
//     std::vector<int> shuff_inds(num_tracks); 

//     // Shuffle points around for test purposes
//     for (int j=0; j<tracks_msg->num_tracks; j++) shuff_inds[j] = j;
//     // std::random_shuffle(shuff_inds.begin(), shuff_inds.end());
//     bool once = false;

//     //----------------------------------
//     // Build tracks
//     //----------------------------------
//     opencv_utils::tic();
//     TrackListPointCloud track_list_pcl(num_tracks);
//     pcl::PointCloud<pcl::PointXYZRGB>::Ptr endeff_track_cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
//     pcl::PointCloud<pcl::Normal>::Ptr endeff_track_cloud_tangents (new pcl::PointCloud<pcl::Normal>);
//     for (int _j=0; _j<tracks_msg->num_tracks; _j++) { 
//         int j = shuff_inds[_j];
//         track_list_pcl[j].id = j;
        
//         //----------------------------------
//         // Full track 
//         //----------------------------------
//         pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud = track_list_pcl[j].tracks;
//         track_cloud = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
//         track_cloud->height = 1;
//         track_cloud->width = tracks_msg->tracks[j].num_poses;
//         track_cloud->points.resize(tracks_msg->tracks[j].num_poses);

//         //----------------------------------
//         // Full track refined (tangent projected)
//         //----------------------------------
//         pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud_refined = track_list_pcl[j].tracks_refined;
//         track_cloud_refined = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
//         track_cloud_refined->height = 1;
//         track_cloud_refined->width = tracks_msg->tracks[j].num_poses;
//         track_cloud_refined->points.resize(tracks_msg->tracks[j].num_poses);

//         //----------------------------------
//         // Full track normals
//         //----------------------------------
//         pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_normals = track_list_pcl[j].normals;
//         track_cloud_normals = pcl::PointCloud<pcl::Normal>::Ptr (new pcl::PointCloud<pcl::Normal>);
//         track_cloud_normals->height = 1;
//         track_cloud_normals->width = tracks_msg->tracks[j].num_poses;
//         track_cloud_normals->points.resize(tracks_msg->tracks[j].num_poses);


//         //----------------------------------
//         // Full track tangents
//         //----------------------------------
//         pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_tangents = track_list_pcl[j].tangents;
//         track_cloud_tangents = pcl::PointCloud<pcl::Normal>::Ptr (new pcl::PointCloud<pcl::Normal>);
//         track_cloud_tangents->height = 1;
//         track_cloud_tangents->width = tracks_msg->tracks[j].num_poses;
//         track_cloud_tangents->points.resize(tracks_msg->tracks[j].num_poses);


//         //----------------------------------
//         // Fill up point cloud (points, normals, tangent computation
//         //----------------------------------
//         for (int k=0; k<tracks_msg->tracks[j].num_poses; k++) { 
//             if (!once) { 
//                 std::cerr << "====================== > poses: " << tracks_msg->tracks[j].num_poses << std::endl;
//                 once = true;
//             }
    
//             // Populate XYZ
//             articulation_object_pose_msg_t& pose_msg = tracks_msg->tracks[j].pose[k];
//             track_cloud->points[k].x = pose_msg.pos[0], 
//                 track_cloud->points[k].y = pose_msg.pos[1], 
//                 track_cloud->points[k].z = pose_msg.pos[2] + 1;

//             // Populate Normals
//             double T[16];
//             bot_quat_pos_to_matrix(pose_msg.orientation, pose_msg.pos, T);
//             track_cloud_normals->points[k].normal[0] = T[0], 
//                 track_cloud_normals->points[k].normal[1] = T[3], 
//                 track_cloud_normals->points[k].normal[2] = T[6];            

//             // Compute tangents
//             // Minimum length for fitting
//             if (k<MIN_FIT_LENGTH-1) continue;

//             boost::shared_ptr<vector<int> > fit_inds (new vector<int> ());
//             for (int m=k-MIN_FIT_LENGTH+1; m<=k; m++) 
//                 fit_inds->push_back(m);
            
//             // std::cerr << "fit_inds: " << k << " " << cv::Mat(*fit_inds) << std::endl;
//             // Create the filtering object
//             pcl::PointCloud<pcl::PointXYZRGB>::Ptr segment (new pcl::PointCloud<pcl::PointXYZRGB>);
//             pcl::ExtractIndices<pcl::PointXYZRGB> extract;
//             extract.setInputCloud(track_cloud);
//             extract.setIndices (fit_inds);
//             extract.setNegative (false);
//             extract.filter (*segment);
//             assert(segment->points.size() == MIN_FIT_LENGTH);

//             // 3D Line Coefficients
//             pcl::ModelCoefficients::Ptr coeff (new pcl::ModelCoefficients ());
//             pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());

//             // Create the segmentation object
//             pcl::SACSegmentation<pcl::PointXYZRGB> seg;
//             seg.setOptimizeCoefficients (true);
//             seg.setModelType (pcl::SACMODEL_LINE);
//             seg.setMethodType (pcl::SAC_RANSAC);
//             seg.setMaxIterations (20);
//             seg.setDistanceThreshold (0.02); 

//             // Fit 3D Line
//             seg.setInputCloud (segment);
//             seg.segment (*inliers, *coeff);
            
//             // Test direction and correct
//             cv::Vec3f pp1(track_cloud->points[fit_inds->begin()[0]].x, 
//                          track_cloud->points[fit_inds->begin()[0]].y, 
//                          track_cloud->points[fit_inds->begin()[0]].z);
//             cv::Vec3f p1(track_cloud->points[fit_inds->end()[-1]].x, 
//                           track_cloud->points[fit_inds->end()[-1]].y, 
//                           track_cloud->points[fit_inds->end()[-1]].z);
//             cv::Vec3f t1 = (p1-pp1)/cv::norm(p1-pp1);

//             float dot = t1[0]*coeff->values[3] + t1[1]*coeff->values[4] + t1[2]*coeff->values[5]; 
//             if (dot < 0) {
//                 track_cloud_tangents->points[k].normal[0] = -coeff->values[3], 
//                     track_cloud_tangents->points[k].normal[1] = -coeff->values[4], 
//                     track_cloud_tangents->points[k].normal[2] = -coeff->values[5];
//             } else {
//                 track_cloud_tangents->points[k].normal[0] = coeff->values[3], 
//                     track_cloud_tangents->points[k].normal[1] = coeff->values[4], 
//                     track_cloud_tangents->points[k].normal[2] = coeff->values[5];
//             }
//             // Once first tangent is computed, then fill in the previous 
//             // tangents with the same value
//             if (k==MIN_FIT_LENGTH-1) { 
//                 for (int m=0; m<MIN_FIT_LENGTH-1; m++) 
//                     track_cloud_tangents->points[m] = track_cloud_tangents->points[k];
//             }

//             // Project MIN_FIT_LENGTH along tangent vector to the plane whose 
//             // normal is the tangent vector
//             // if q is a point to be projected, p1 is the tangent point (above), 
//             // and t1 is the tangent vector then: 
//             // q_proj = q + ((q-p1).*t1) *t1;

//             cv::Vec3f mean_q(0.f,0.f,0.f);
//             for (int m=0; m<fit_inds->size(); m++) { 
//                 cv::Vec3f q(track_cloud->points[fit_inds->begin()[m]].x, 
//                         track_cloud->points[fit_inds->begin()[m]].y, 
//                         track_cloud->points[fit_inds->begin()[m]].z);
//                 cv::Vec3f q_proj = q + (fabs(t1.dot(q-p1))*t1);
//                 mean_q += q_proj;
//             }
//             mean_q *= 1.f / fit_inds->size();

//             // Populate refined tracks
//             track_cloud_refined->points[k].x = mean_q[0], 
//                 track_cloud_refined->points[k].y = mean_q[1], 
//                 track_cloud_refined->points[k].z = mean_q[2];
            
//             // Fill 0-MIN_FIT_LENGTH indices for the refined points
//             if (k==MIN_FIT_LENGTH-1) { 
//                 for (int m=0; m<MIN_FIT_LENGTH-1; m++) 
//                     track_cloud_refined->points[m] = track_cloud_refined->points[k];
//             }
            
//         } // end for each pose (within a particular track)


        
//         //----------------------------------
//         // Creating the kdtree object for the search method of the extraction
//         //----------------------------------
        
//         pcl::search::KdTree<pcl::PointXYZRGB>::Ptr& tree_sc = track_list_pcl[j].tree; 
//         tree_sc = pcl::search::KdTree<pcl::PointXYZRGB>::Ptr(new pcl::search::KdTree<pcl::PointXYZRGB> ());
//         // Setup the shape context computation
// #define SHAPECONTEXT 1
// #define PFH 0
//         // #define MOMENTS 1
// #if SHAPECONTEXT
//         pcl::ShapeContext3DEstimation<pcl::PointXYZRGB, pcl::Normal, pcl::ShapeContext> descriptor;
// #else
//         pcl::PFHEstimation<pcl::PointXYZRGB, pcl::Normal, pcl::PFHSignature125> descriptor;
// #endif
//         // Provide the point cloud
//         descriptor.setInputCloud (track_cloud);
//         // Provide normals
//         descriptor.setInputNormals (track_cloud_normals);
//         // Use the same KdTree from the normal estimation
//         descriptor.setSearchMethod (tree_sc);

// #if SHAPECONTEXT
//         pcl::PointCloud<pcl::ShapeContext>::Ptr desc_features (new pcl::PointCloud<pcl::ShapeContext>);
// #else
//         pcl::PointCloud<pcl::PFHSignature125>::Ptr desc_features (new pcl::PointCloud<pcl::PFHSignature125>);
        
// #endif
        
//         // The minimal radius is generally set to approx. 1/10 of the search radius, 
//         // while the pt. density radius is generally set to 1/5
//         cv::Point3f p1(tracks_msg->tracks[j].pose[0].pos[0],
//                        tracks_msg->tracks[j].pose[0].pos[1], 
//                        tracks_msg->tracks[j].pose[0].pos[2]);
        
//         int last = tracks_msg->tracks[j].num_poses-1;
//         cv::Point3f p2(tracks_msg->tracks[j].pose[last].pos[0],
//                        tracks_msg->tracks[j].pose[last].pos[1], 
//                        tracks_msg->tracks[j].pose[last].pos[2]);
        
//         // std::cerr << "search_radius: " << opencv_utils::l2_dist(p1, p2) << std::endl;
//         // sc.setRadiusSearch (std::max(.1, opencv_utils::l2_dist(p1, p2) + 2*1e-3));
// #if SHAPECONTEXT
//         descriptor.setPointDensityRadius (0.05);
//         descriptor.setMinimalRadius (0.05);
//         descriptor.setAzimuthBins (8);
//         descriptor.setElevationBins (8);
//         descriptor.setRadiusBins (16);
// #endif
//         descriptor.setRadiusSearch (1.f);
        
//         //----------------------------------
//         // Set the indices
//         //----------------------------------
//         boost::shared_ptr<vector<int> > indices (new vector<int> ());
//         indices->push_back (track_cloud->points.size()-1);
//         descriptor.setIndices(indices);

//         // Actually compute the shape contexts
//         // opencv_utils::tic();
//         descriptor.compute (*desc_features);
//         // opencv_utils::toc("Shape context compute");

//         // Display and retrieve the shape context descriptor vector
//         // std::cout << shape_context_features->points.end()[-1] << std::endl;
// #if SHAPECONTEXT
//         std::vector<float> desc = desc_features->points.end()[-1].descriptor;
// #else
//         float* hist = desc_features->points.end()[-1].histogram;
//         std::vector<float> desc(hist, hist + 125);
// #endif
        
//         // pcl::MomentInvariantsEstimation<pcl::PointXYZRGB, pcl::MomentInvariants> descriptor2;
//         // std::vector<float> desc2(3);
//         // descriptor2.computePointMomentInvariants(*track_cloud, desc2[0], desc2[1], desc2[2]);
//         // std::cerr << "=========> desc2: " << cv::Mat(desc2) << std::endl;

//         //----------------------------------
//         // K nearest neighbor search for last point in trajectory (index)
//         //----------------------------------
//         pcl::PointXYZRGB last_pt; 
//         last_pt.x = track_cloud->points.end()[-1].x, 
//             last_pt.y = track_cloud->points.end()[-1].y, 
//             last_pt.z = track_cloud->points.end()[-1].z;
//         endeff_track_cloud->points.push_back(last_pt);
        
//         pcl::Normal last_tgt; 
//         last_tgt.normal[0] = track_cloud_tangents->points.end()[-1].normal[0], 
//             last_tgt.normal[1] = track_cloud_tangents->points.end()[-1].normal[1],
//             last_tgt.normal[2] = track_cloud_tangents->points.end()[-1].normal[2];
//         endeff_track_cloud_tangents->points.push_back(last_tgt);
        

//         //----------------------------------
//         // Descriptor
//         //----------------------------------
//         track_list_pcl[j].desc = desc;

//         //----------------------------------
//         // Make time slices and build tangent descriptor
//         //----------------------------------
//         track_list_pcl[j].desc2 = std::vector<cv::Vec3f>(TIME_SLICES);

//         const int skip = floor(track_cloud_tangents->points.size() / TIME_SLICES);
//         for (int k=0,count=0; k<track_cloud_tangents->points.size() && count<TIME_SLICES; k+=skip) { 
//             track_list_pcl[j].desc2[count++] = cv::Vec3f(track_cloud_tangents->points[k].normal[0], 
//                                                        track_cloud_tangents->points[k].normal[1], 
//                                                        track_cloud_tangents->points[k].normal[2]);

//             // std::cerr << j << ":" << k <<  " " << cv::Vec3f(track_cloud_tangents->points[k].normal[0], 
//             //                        track_cloud_tangents->points[k].normal[1], 
//             //                        track_cloud_tangents->points[k].normal[2]) << std::endl;
//         }

//     } // end for each track
//     opencv_utils::toc("BUILD TRACKS/TANGENTS/NORMALS/SHAPE CONTEXT DESC.");
// #else 


// void track_compute_endeffectors() { 
//     if (!endeff_track_cloud) 
//         endeff_track_cloud = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
//     else 
//         endeff_track_cloud->points.clear(); 
//     if (!endeff_track_cloud_tangents)
//         endeff_track_cloud_tangents = pcl::PointCloud<pcl::Normal>::Ptr(new pcl::PointCloud<pcl::Normal>);
//     else 
//         endeff_track_cloud_tangents->points.clear(); 

//     endeff_track_ids.clear(); 

//     for (std::map<int64_t, spvision::Tracklet>::iterator it = track_list_pcl.begin(); 
//          it != track_list_pcl.end(); it++) { 
        
//         int64_t TRACK_ID = it->first; 
//         spvision::Tracklet& tracklet = it->second; 
//         if (!tracklet.valid) { /*std::cerr << "TRACKLET NOT VALID" << std::endl; */continue; }

//         //----------------------------------
//         // Sampled track , Sampled track refined (tangent projected), 
//         // Sampled track normals, Sampled track tangents, 
//         // Full track (for tangent computation), 
//         //----------------------------------
//         pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud = tracklet.tracks;
//         pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_tangents = tracklet.tangents;

//         //----------------------------------
//         // K nearest neighbor search for last point in trajectory (index)
//         //----------------------------------
//         pcl::PointXYZRGB last_pt; 
//         last_pt.x = track_cloud->points.end()[-1].x, 
//             last_pt.y = track_cloud->points.end()[-1].y, 
//             last_pt.z = track_cloud->points.end()[-1].z;
//         // endeff_track_cloud->points.push_back(last_pt);
        
//         pcl::Normal last_tgt; 
//         last_tgt.normal[0] = track_cloud_tangents->points.end()[-1].normal[0], 
//             last_tgt.normal[1] = track_cloud_tangents->points.end()[-1].normal[1],
//             last_tgt.normal[2] = track_cloud_tangents->points.end()[-1].normal[2];
//         // endeff_track_cloud_tangents->points.push_back(last_tgt);
//         // endeff_track_ids.push_back(TRACK_ID);
//     }
//     return;

//         // //----------------------------------
//         // // Make time slices and build tangent descriptor
//         // //----------------------------------
//         // std::vector<cv::Vec3f>& desc2 = track_list_pcl[TRACK_ID].desc2;
//         // desc2 = std::vector<cv::Vec3f>(TIME_SLICES);

//         // for (int k=0; k<track_cloud_tangents->points.size(); k++) { 
//         //     desc2[k] = cv::Vec3f(track_cloud_tangents->points[k].normal[0], 
//         //                          track_cloud_tangents->points[k].normal[1], 
//         //                          track_cloud_tangents->points[k].normal[2]);
            
//         //     // std::cerr << j << ":" << k <<  " " << cv::Vec3f(track_cloud_tangents->points[k].normal[0], 
//         //     //                        track_cloud_tangents->points[k].normal[1], 
//         //     //                        track_cloud_tangents->points[k].normal[2]) << std::endl;
//         // }
// }
