// More utilities
// #include "sc3d.h"
// #include "feature_types.h"
// #include "TrackletManager.hpp"
#include "MultiObjectPoseTracker.hpp"
#include <ConciseArgs>

namespace spvision { 

struct state_t
{
    lcm_t *lcm;
    GMainLoop *mainloop;
    BotParam   *p_server;
    BotFrames *frames;
};
state_t* state = NULL;

spvision::MultiObjectPoseTracker MOPT;
}
