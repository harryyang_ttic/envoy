#ifndef SPVISION_LCM_LOG_PLAYER_HPP_
#define SPVISION_LCM_LOG_PLAYER_HPP_

// lcm
#include <lcm/lcm.h>
#include <lcmtypes/bot2_param.h>
#include "lcmtypes/er_lcmtypes.h" 

// libbot includes
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <ConciseArgs>

// opencv includes
#include <opencv2/opencv.hpp>

// pcl includes
#include <pcl/io/pcd_io.h>
#include <pcl/PointIndices.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/random_sample.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/surface/mls.h>

// pcl includes for kinect
#include <pointcloud_tools/pointcloud_lcm.hpp>
#include <pointcloud_tools/pointcloud_vis.hpp>

// numpy
// #include <numpy/ndarrayobject.h>

// default handler receiving images
static void _on_kinect_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                                       const kinect_frame_msg_t *msg, 
                                       void *user_data ) {
    double tic; 

    // Check msg type
    // state_t *state = (state_t*) user_data;
    if (msg->depth.depth_data_format != KINECT_DEPTH_MSG_T_DEPTH_MM) { 
        std::cerr << "Warning: kinect data format is not KINECT_DEPTH_MSG_T_DEPTH_MM" << std::endl 
                  << "Program may not function properly" << std::endl;
    }
    assert (msg->depth.depth_data_format == KINECT_DEPTH_MSG_T_DEPTH_MM);

    //----------------------------------
    // Unpack Point cloud
    //----------------------------------
    double t1 = bot_timestamp_now();
    cv::Mat3b img(msg->image.height, msg->image.width);
    cv::Mat_<float> depth_img = cv::Mat_<float>(img.size());

    // state->pc_lcm->unpack_kinect_frame(msg, img.data, depth_img.data, cloud);
    // cv::resize(img, rgb_img, cv::Size(int(img.cols / SCALE), int(img.rows / SCALE)));
    // printf("%s===> UNPACK TIME: %4.2f ms%s\n", esc_yellow, (bot_timestamp_now() - t1) * 1e-3, esc_def); 
    // std::cerr << "cloud: " << cloud->points.size() << std::endl;
    // std::cerr << "rgb: " << rgb_img.size() << std::endl;

    //--------------------------------------------
    // Convert Color
    //--------------------------------------------
    // cvtColor(rgb_img, rgb_img, CV_RGB2BGR);

    return;
}


struct logplayer_state_t {
    lcm_t* lcm;
    pointcloud_lcm* pc_lcm;
    pointcloud_vis* pc_vis;
};

struct LCMLogReaderOptions { 
    std::string fn;    // Filename
    std::string ch;    // Specific channel subscribe
    float fps;

    lcm_t* lcm;    
    kinect_frame_msg_t_handler_t handler;
    
    void* user_data;    // user data

    LCMLogReaderOptions () { 
        fn = "";
        ch = "";
        
        lcm = NULL;
        user_data = NULL;
    }    
}; 

struct LCMLogReader { 
    LCMLogReaderOptions options;

    logplayer_state_t* state; // state
    lcm_eventlog_t* log; // Log Container
    lcm_eventlog_event_t* event; // event 
    int64_t last_utime;
    double usleep_interval;

    std::map<int64_t, int64_t> utime_map; // sensor to log utime map

    cv::Mat img, depth_img, cloud; 

    LCMLogReader () {
        log = NULL;
        event = NULL;
        last_utime = 0;
    }

    ~LCMLogReader() { 
        if (log) { // delete log 
        }
        if (state) 
            delete state;
    }

    inline int64_t find_closest_utime(int64_t utime) { 
        printf("find_closest_utime: %lli\n",utime);
        std::map<int64_t, int64_t>::iterator it = utime_map.lower_bound(utime);
        if (it == utime_map.begin() || it->first == utime) { 
            printf("closest upper: %lli\n", it->first);
            return it->second;
        }
        std::map<int64_t, int64_t>::iterator it2 = it;
        it2--;
        if (it2 == utime_map.end() || ((utime - it2->first)  < (it->first - utime))) { 
            printf("closest lower: %lli\n", it2->first);
            return it2->second;
        }
        printf("closest upper: %lli\n",it->first);
        return it->second;
    }

    void init_index() { 
        std::cerr << "Init indexing" << std::endl;
        // read all events and create map
        // lcm_eventlog_event_t* ev = lcm_eventlog_read_next_event (log);

        int count = 0; 
        for (event = lcm_eventlog_read_next_event(log); event != NULL; 
             event = lcm_eventlog_read_next_event(log)) { 
            if (options.ch.length() && strcmp(options.ch.c_str(), event->channel) == 0) { 
                // Decode msg
                kinect_frame_msg_t msg; 
                memset (&msg, 0, sizeof (kinect_frame_msg_t));
                kinect_frame_msg_t_decode(event->data, 0, event->datalen, &msg);
                utime_map[int64_t(msg.timestamp)] = int64_t(event->timestamp);

                // debug
                count++;
                if (count %100 == 0) { 
                    std::cerr << "Indexed " << count << "frames:  " 
                              << msg.timestamp << "->" << event->timestamp << std::endl;
                }

                kinect_frame_msg_t_decode_cleanup (&msg);
                lcm_eventlog_free_event(event);
            }
        } 
        // Seek back to first timestamp
        int ret = lcm_eventlog_seek_to_timestamp(log, int64_t(utime_map.begin()->second));
        std::cerr << "Done indexing" << std::endl;
    }

    // for python wrapper
    void init (char* _fn) { 
        std::string fn(_fn);
        LCMLogReaderOptions options;

        //----------------------------------
        // Log player options
        //----------------------------------
        options.lcm =  bot_lcm_get_global(NULL);
        options.fn = fn;
        options.ch = "KINECT_FRAME";
        options.fps = 1000.f;
        options.handler = _on_kinect_image_frame;
        options.user_data = NULL;

        //----------------------------------
        // State
        //----------------------------------
        state = new logplayer_state_t();
        bool flip_coords = false;
        state->lcm = options.lcm;
        state->pc_lcm = new pointcloud_lcm(state->lcm, false);
        state->pc_lcm->set_decimate(1.f);  // no SCALE (ing)
        state->pc_vis = new pointcloud_vis(state->lcm);

        init(options, false);

        return;
    }

    // for c++ calls
    void init (const LCMLogReaderOptions& _options, bool sequential_read = true) { 
        // log player options
        options = _options;

        // open log
        log = lcm_eventlog_create (options.fn.c_str(), "r");
        if (!log) {
            fprintf (stderr, "Unable to open source logfile %s\n", options.fn.c_str());
            return;
        }

        std::cerr << "===========  LCM LOG PLAYER ============" << std::endl;
        std::cerr << "=> LOG FILENAME : " << options.fn << std::endl;
        std::cerr << "=> CHANNEL : " << options.ch << std::endl;
        std::cerr << "=> FPS : " << options.fps << std::endl;
        std::cerr << "=> LCM PTR : " << (options.lcm != 0) << std::endl;
        std::cerr << "=> USER_DATA PTR : " << (options.user_data != 0) << std::endl;
        // std::cerr << "=> Note: Hit 'space' to proceed to next frame" << std::endl;
        // std::cerr << "=> Note: Hit 'p' to proceed to previous frame" << std::endl;
        // std::cerr << "=> Note: Hit 'n' to proceed to previous frame" << std::endl;
        std::cerr << "===============================================" << std::endl;

        if (!options.user_data) { std::cerr << "WARNING USER DATA NOT SET!!!" << std::endl; } 

        // Set usleep interval
        usleep_interval = 1.f / options.fps * 1e6;

        if (sequential_read)
            getNextFrame(); // start
        else { 
            init_index();
        }
    }

    bool good() { return (log != NULL && options.ch != ""); }

    bool getNextFrame() { 
        assert(good());
        assert(options.handler);

        // get next event from log
        event = lcm_eventlog_read_next_event (log);
        if (event != NULL) { 
            if (options.ch.length() && strcmp(options.ch.c_str(), event->channel) == 0) { 
                // Decode msg
                kinect_frame_msg_t msg; 
                memset (&msg, 0, sizeof (kinect_frame_msg_t));
                kinect_frame_msg_t_decode(event->data, 0, event->datalen, &msg);

                // HACK recv_buf_t NULL??
                options.handler(NULL, event->channel, &msg, options.user_data);
                // on_kinect_image_frame(NULL, event->channel, &msg, options.user_data);

                kinect_frame_msg_t_decode_cleanup (&msg);
                lcm_eventlog_free_event(event);
            }
        } else { 
            return false;
        }

        
        // Get next frame
        if (options.fps > 0.f) { 
            usleep(usleep_interval);
            getNextFrame();
        } else  { 
            while(1) { 
                unsigned char c = cv::waitKey(10) & 0xff;
                if (c == 'q') break;  
                else if ( c == ' ' ) getNextFrame();
            }
        }
        return true;
    }

    bool getFrame(int64_t sensor_utime, uint8_t** img_data, uint32_t* sz, float** cloud_data) { 
        assert(good());
        assert(options.handler);

        // printf("getFrame: %lli\n", sensor_utime);
        int64_t event_utime = find_closest_utime(sensor_utime);
        // printf("seek to : %lli\n", event_utime);
        // get next event from log
        int ret = lcm_eventlog_seek_to_timestamp(log, event_utime);
        if (ret == 0) { 

            // run until th
            for (event = lcm_eventlog_read_next_event(log); event != NULL; 
                 event = lcm_eventlog_read_next_event(log)) { 
                if (options.ch.length() && strcmp(options.ch.c_str(), event->channel) == 0) { 
                    // Decode msg
                    kinect_frame_msg_t msg; 
                    memset (&msg, 0, sizeof (kinect_frame_msg_t));
                    kinect_frame_msg_t_decode(event->data, 0, event->datalen, &msg);


                    // Check msg type
                    if (msg.depth.depth_data_format != KINECT_DEPTH_MSG_T_DEPTH_MM) { 
                        std::cerr << "Warning: kinect data format is not KINECT_DEPTH_MSG_T_DEPTH_MM" << std::endl 
                                  << "Program may not function properly" << std::endl;
                    }
                    assert (msg.depth.depth_data_format == KINECT_DEPTH_MSG_T_DEPTH_MM);

                    //----------------------------------
                    // Unpack Data
                    //----------------------------------
                    double t1 = bot_timestamp_now();
                    if (img.empty())
                        img.create(msg.image.height, msg.image.width, CV_8UC3);
                    // if (depth_img.empty())
                    //     depth_img.create(msg.image.height, msg.image.width, CV_32FC1);
                    if (cloud.empty())
                        cloud.create(msg.image.height, msg.image.width, CV_32FC3);

                    pcl::PointCloud<pcl::PointXYZRGB>::Ptr p = 
                        pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>());
                    state->pc_lcm->unpack_kinect_frame(&msg, img.data, p);
                    printf("===> UNPACK TIME: %4.2f ms\n", (bot_timestamp_now() - t1) * 1e-3);                     
                    //----------------------------------
                    // Unpack Point cloud
                    //----------------------------------
                    float* cptr = (float*)cloud.data;
                    for (int j=0; j<p->points.size(); j++) 
                        cptr[j*3] = p->points[j].x, cptr[j*3+1] = p->points[j].y, cptr[j*3+2] = p->points[j].z; 

                    // printf("match frame: %lli %lli\n",msg.timestamp, sensor_utime);
                    // printf("msg: %i %i\n", msg.image.height, msg.image.width); 
                    assert(msg.timestamp == sensor_utime);
                           
                    *img_data = (uchar*)img.data;
                    *cloud_data = (float*)cloud.data;
                    // printf("data: %p %i\n", data, img.data); 
                    sz[0] = img.rows, sz[1] = img.cols, sz[2] = img.channels();
            
                    kinect_frame_msg_t_decode_cleanup (&msg);
                    lcm_eventlog_free_event(event);
                    break;
                }
            }
            return true;
        }
        return false;
    }
};

#endif // SPVISION_LCM_LOG_PLAYER_HPP_

