// 3D KLT tracker
// Author: Sudeep Pillai (spillai@csail.mit.edu)
// TODO: 
// http://lear.inrialpes.fr/people/wang/dense_trajectories
// http://www.irisa.fr/vista/Papers/2009_bmvc_wang.pdf
// http://www.vision.jhu.edu/manifoldlearning.htm

#include "3d_klt_tracker.h"
using namespace spvision;

// int TRACKLET_ID = 1; 
// int nextTrackletID() { 
//     return ++TRACKLET_ID;
// }
// int trackletID() { 
//     return TRACKLET_ID;
// }

void perform_klt() { 
    std::cerr << "------------------------------------------>" << std::endl;
    std::cerr << "Frame queue: " << frame_queue.size() << std::endl;
    if (frame_queue.size() < 2)
        return;

    uint64_t new_utime = frame_queue[0].utime;
    uint64_t prev_utime = frame_queue[1].utime;
    printf("time: %f s, %f s\n", new_utime * 1e-6, prev_utime * 1e-6); 

    cv::Mat new_frame = frame_queue[0].img; 
    cv::Mat prev_frame = frame_queue[1].img;

    // Compute LK optical flow
    std::vector<uchar> status;
    std::vector<float> err;

    std::vector<int> tracklet_ids;
    std::vector<cv::Point2f> orig_feats;
    std::vector<cv::Point2f> prop_feats;

    if (!tracklet.size()) { 
        std::cerr << "===> No tracklets yet!: Adding tracklets" << std::endl;
        // add new points (computed from prev frame) with id to the tracklet
        for (int j=0; j<feats_queue[1].size(); j++) { 
            tracklet[nextTrackletID()].push_back(Feature2D(prev_utime, feats_queue[1][j]));

            // update Feature3D utime
            feats_3d_queue[1][j].utime = prev_utime;
            tracklet_3d[trackletID()].push_back(feats_3d_queue[1][j]);
        }
        std::cerr << "===> Tracklets: " << trackletID() << std::endl;

        // all points are considered as orig feats
        for (std::map<int, Track2D >::const_iterator it = tracklet.begin(); 
             it != tracklet.end(); it++) { 
            tracklet_ids.push_back(it->first);
            orig_feats.push_back(it->second.end()[-1].point);
        }
    } else { 
        // old tracked points + new points that aren't close to the old tracked points

        // old tracked points
        for (std::map<int, Track2D >::iterator it = tracklet.begin(); 
             it != tracklet.end(); it++) { 

            // only add points whose tracks are valid
            if (!it->second.valid)
                continue;

            // if time diff between last feature added and current feature 
            // is > than DELTAT_ASSOC., don't add to the tracklet
            const Feature2D& last_feat = it->second.end()[-1];
            if (prev_utime - last_feat.utime > DELTAT_ASSOCIATION*1e6) { 
                it->second.valid = false;
                continue;
            }
            orig_feats.push_back(it->second.end()[-1].point);
        }

        // Build kd tree for old points
        cv::Mat_<float> orig_feats_mat(orig_feats.size(), 2);
        for (int j=0; j<orig_feats.size(); j++) 
            orig_feats_mat.at<float>(j,0) = orig_feats[j].x, 
                orig_feats_mat.at<float>(j,1) = orig_feats[j].y;

        cv::Mat_<float> feats_queue_mat(feats_queue[1].size(), 2);
        for (int j=0; j<feats_queue[1].size(); j++) 
            feats_queue_mat.at<float>(j,0) = feats_queue[1][j].x, 
                feats_queue_mat.at<float>(j,1) = feats_queue[1][j].y;

        cv::flann::LinearIndexParams flannIndexParams; // using 4 randomized kdtrees        
        cv::flann::Index knn_index(orig_feats_mat, flannIndexParams);
        
        // Search new points to see if within threshold of old points
        int knn = 1;
        cv::Mat knn_inds;
        cv::Mat knn_dists;
        knn_index.knnSearch(feats_queue_mat, knn_inds, knn_dists, knn, cv::flann::SearchParams());
        std::cerr << "feats: " << feats_queue[1].size() << " knn_inds: " << knn_inds.size() << 
            " knn_dists: " << knn_dists.size() << std::endl;

        // If new points are not in distance threshold eps ball, add the point to the
        // tracklet with new ID
        for (int j=0; j<feats_queue[1].size(); j++) { 
            if (knn_dists.at<float>(j) > ADD_POINT_DISTANCE_THRESHOLD) { 
                tracklet[nextTrackletID()].push_back(Feature2D(prev_utime, feats_queue[1][j]));

                feats_3d_queue[1][j].utime = prev_utime;
                tracklet_3d[trackletID()].push_back(feats_3d_queue[1][j]);
            }
        }

        // old+new tracked points
        tracklet_ids.clear();
        orig_feats.clear();
        for (std::map<int, Track2D >::const_iterator it = tracklet.begin(); 
             it != tracklet.end(); it++) { 
            // Only add valid tracklets
            if (!it->second.valid)
                continue;
            tracklet_ids.push_back(it->first);
            orig_feats.push_back(it->second.end()[-1].point);
        }
    }

    if (!orig_feats.size()) return;

    std::cerr << "===> ORIG tracklets: " << orig_feats.size() << std::endl;
    std::cerr << "===> ID tracklets: " << tracklet_ids.size() << std::endl;
    calcOpticalFlowPyrLK(prev_frame, new_frame, orig_feats, prop_feats, status, err, winSize,
                         3, termcrit, 0, 0.001);

    // Compute propagated features in 3D+Normal
    Track3D prop_feats_3d = compute_3d_features(prop_feats);

    // Add propagated points
    for (int j=0; j<orig_feats.size(); j++) { 
        if (!status[j])
            continue;
        const int& id = tracklet_ids[j];

        if(tracklet_3d[id].size()) { 
            const Feature3D& prev_pt = tracklet_3d[id].back();
            const Feature3D& add_pt = prop_feats_3d[j];

            // if eps depth is high (chuck)
            if (fabs(prev_pt.point.z - add_pt.point.z) > EPS_DEPTH_CONTINUITY_THRESHOLD)
                continue;
            
            // std::cerr << "prev: " << prev_pt.point.x << " " << prev_pt.point.y << " " << prev_pt.point.z << std::endl;
            // std::cerr << "add: " << add_pt.point.x << " " << add_pt.point.y << " " << add_pt.point.z << std::endl;
        }

        // Ensure that tracklet ID is already added
        assert(tracklet.find(id) != tracklet.end());
        // Ensure that tracklet is valid (shouldn't be invalid)
        assert(tracklet[id].valid);
        tracklet[id].push_back(Feature2D(new_utime, prop_feats[j]));       

        prop_feats_3d[j].utime = new_utime;
        tracklet_3d[id].push_back(prop_feats_3d[j]);
    }
    

}

void prune_tracks() { 
    if (!tracklet_3d.size() || !tracklet.size())
        return;

    // Keep only MAX_TRACK_TIMESPAN seconds of track info. 
    int64_t most_recent_utime = 0; 
    for (std::map<int, Track2D >::iterator it = tracklet.begin(); 
         it != tracklet.end(); it++) { 
        Track2D& track2d = it->second;
        if (!track2d.size()) continue;

        int64_t latest_utime = track2d.end()[-1].utime;
        int64_t first_utime = track2d.begin()[0].utime;
        if (latest_utime - first_utime > MAX_TRACK_TIMESPAN * 1e6) { 
            int idx = track2d.size()-1; 
            for (; idx>=0; idx--) 
                if (latest_utime - track2d[idx].utime > MAX_TRACK_TIMESPAN * 1e6)
                    break;
            track2d.erase(track2d.begin(), track2d.begin() + idx + 1);
        }
        most_recent_utime = (latest_utime > most_recent_utime) ? latest_utime : most_recent_utime;
    }

    for (std::map<int, Track3D >::iterator it = tracklet_3d.begin(); 
         it != tracklet_3d.end(); it++) { 
        Track3D& track3d = it->second;
        if (!track3d.size()) continue;

        int64_t latest_utime = track3d.end()[-1].utime;
        int64_t first_utime = track3d.begin()[0].utime;
        if (latest_utime - first_utime > MAX_TRACK_TIMESPAN * 1e6) { 
            int idx = track3d.size()-1; 
            for (; idx>=0; idx--) 
                if (latest_utime - track3d[idx].utime > MAX_TRACK_TIMESPAN * 1e6)
                    break;
            track3d.erase(track3d.begin(), track3d.begin() + idx + 1);
        }
    }


    // Remove all tracks whose most recent feature is lagging by more than MIN_TRACK_TIMESTAMP_DELAY seconds from the most recent timestamp
    // Should take care of resetting as well
    for (std::map<int, Track2D >::iterator it = tracklet.begin(); 
         it != tracklet.end(); it++) { 
        Track2D& track2d = it->second;
        if (!track2d.size()) continue;

        int64_t latest_utime = track2d.end()[-1].utime;
        if (most_recent_utime - latest_utime > MAX_TRACK_TIMESTAMP_DELAY * 1e6) { 
            std::map<int, Track3D>::iterator it3d = tracklet_3d.find(it->first);
            assert(it3d != tracklet_3d.end());
            tracklet_3d.erase(it3d);
            tracklet.erase(it);
        } 
    }

}



// Sort by descending tracklet length 
bool tracklet_sort_by_length(const std::pair<int, Track2D >& lhs, 
                             const std::pair<int, Track2D >& rhs) { 
    return lhs.second.size() > rhs.second.size();
}

void plot_klt() { 
    if (rgb_img.empty())
        return;

    // Sort tracks by length
    std::vector<std::pair<int, Track2D > > sorted_tracklet;
    for (std::map<int, Track2D >::iterator it = tracklet.begin(); 
         it != tracklet.end(); it++) { 
        if (!it->second.valid)
            continue;
        sorted_tracklet.push_back(std::make_pair(it->first, it->second));
    }

    std::sort(sorted_tracklet.begin(), sorted_tracklet.end(), tracklet_sort_by_length);

    // Color tracks
    std::vector<cv::Scalar> track_colors(std::min(int(sorted_tracklet.size()), 20));
    if (!track_colors.size()) return;
    opencv_utils::fillColors(track_colors);
    for (int j=0; j<sorted_tracklet.size(); j++) { 
        const Track2D& track = sorted_tracklet[j].second;
        if (!track.size())
            continue;

        circle(rgb_img, track.end()[-1].point, 1, CV_RGB(0,200,0), CV_FILLED);
        for (int k=0; k<track.size()-1; k++)
            cv::line(rgb_img, track[k].point, track[k+1].point, CV_RGB(0,0,200), 1, CV_AA, 0);
        
    }

    std::stringstream ss;
    ss << "Tracklets: " << tracklet.size();
    opencv_utils::putText(ss.str(), rgb_img);
    opencv_utils::imshow("Tracklets", rgb_img);


    Track3D all_tracks;
    for (std::map<int, Track3D >::const_iterator it = tracklet_3d.begin(); 
         it != tracklet_3d.end(); it++) { 
        const Track3D& track = it->second;
        if (!track.valid)
            continue;
        all_tracks.insert(all_tracks.end(), track.begin(), track.end());
    }


    // //----------------------------------
    // // Publish point cloud to viewer
    // //----------------------------------
    // erlcm_xyzrgb_point_list_t pt_list;
    // pt_list.utime = bot_timestamp_now();
    // pt_list.no_points = all_tracks.size();
    // pt_list.points = (erlcm_xyzrgb_point_t*) malloc(all_tracks.size() * sizeof(erlcm_xyzrgb_point_t));
    // pt_list.no_faces = 0; 
    // pt_list.faces = 0; 
    // for (int j=0; j<all_tracks.size(); j++) { 
    //     erlcm_xyzrgb_point_t pt; 
    //     pt.xyz[0] = all_tracks[j].point.x, 
    //         pt.xyz[1] = all_tracks[j].point.y, 
    //         pt.xyz[2] = all_tracks[j].point.z;
    //     pt.rgba = 0;
    //     pt.rgba |= 100; 
    //     pt.rgba |= (int)all_tracks[j].point.b << 8; 
    //     pt.rgba |= (int)all_tracks[j].point.g << 16; 
    //     pt.rgba |= (int)all_tracks[j].point.r << 24;
    //     pt_list.points[j] = pt;
    // }
    // erlcm_xyzrgb_point_list_t_publish(state->lcm, "PCL_XYZRGB_LIST", &pt_list);
    // free(pt_list.points);


    //----------------------------------
    // Publish track normals to viewer
    //----------------------------------
    erlcm_normal_point_list_t normal_pt_list;
    normal_pt_list.utime = bot_timestamp_now();
    normal_pt_list.no_points = all_tracks.size();
    normal_pt_list.points = (erlcm_normal_point_t*) malloc(all_tracks.size() * 
                                                           sizeof(erlcm_normal_point_t));
    for (int j=0; j<all_tracks.size(); j++) { 
        erlcm_normal_point_t pt; 
        pt.xyz[0] = all_tracks[j].point.x, 
            pt.xyz[1] = all_tracks[j].point.y, 
            pt.xyz[2] = all_tracks[j].point.z;
        pt.normals[0] = all_tracks[j].normal[0], 
            pt.normals[1] = all_tracks[j].normal[1], 
            pt.normals[2] = all_tracks[j].normal[2];
        normal_pt_list.points[j] = pt;
    }
    erlcm_normal_point_list_t_publish(state->lcm, "PCL_NORMAL_LIST", &normal_pt_list);
    free(normal_pt_list.points);

    // //----------------------------------
    // // Publish slic normals to viewer
    // //----------------------------------
    // erlcm_normal_point_list_t normal_pt_list;
    // normal_pt_list.utime = bot_timestamp_now();
    // normal_pt_list.no_points = cloud_normals->points.size();
    // normal_pt_list.points = (erlcm_normal_point_t*) malloc(cloud_normals->points.size() * sizeof(erlcm_normal_point_t));
    // for (int j=0; j<cloud_normals->points.size(); j++) { 
    //     erlcm_normal_point_t pt; 
    //     // pt.xyz[0] = normals_orig[j].x, pt.xyz[1] = normals_orig[j].y, pt.xyz[2] = normals_orig[j].z;
    //     // pt.normals[0] = normals_vec[j].x, pt.normals[1] = normals_vec[j].y, pt.normals[2] = normals_vec[j].z;
    //     pt.xyz[0] = cloud_decimated->points[j].x, 
    //         pt.xyz[1] = cloud_decimated->points[j].y, 
    //         pt.xyz[2] = cloud_decimated->points[j].z;
    //     pt.normals[0] = cloud_normals->points[j].normal[0], 
    //         pt.normals[1] = cloud_normals->points[j].normal[1], 
    //         pt.normals[2] = cloud_normals->points[j].normal[2];
    //     normal_pt_list.points[j] = pt;
    // }
    // erlcm_normal_point_list_t_publish(state->lcm, "PCL_NORMAL_LIST", &normal_pt_list);
    // free(normal_pt_list.points);

}


void normal_estimation(pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, pcl::PointCloud<pcl::Normal>::Ptr& cloud_normals) { 
    // Normal estimation
    pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> ne;
    
    // Decimate point cloud by DEPTH_SCALE
    cloud_decimated->width    =(int) (cloud->width/ (double) DEPTH_SCALE) ;
    cloud_decimated->height   =(int) (cloud->height/ (double) DEPTH_SCALE);
    cloud_decimated->is_dense = false;
    cloud_decimated->points.resize (cloud_decimated->width * cloud_decimated->height);
    for(int v=0, j=0; v<cloud->height; v+=DEPTH_SCALE)
      for(int u=0; u<cloud->width; u+=DEPTH_SCALE ) 
          cloud_decimated->points[j++] = cloud->points[v*cloud->width+u];

    std::cerr << " CLOUD: " << cloud->points.size() 
              << " DECIM: " << cloud_decimated->points.size() << std::endl;

    ne.setInputCloud(cloud_decimated);

    pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree_n (new pcl::search::KdTree<pcl::PointXYZRGB>());
    ne.setSearchMethod(tree_n);
    ne.setRadiusSearch(0.2);
    ne.compute(*cloud_normals);
    // std::cout << "Estimated the normals" << std::endl;

    return;
}

Track3D compute_3d_features(const std::vector<cv::Point2f>& feats) { 
    
    // Decimated points in 3D for lookup
    cv::Mat_<float> feats_mat(cloud_decimated->points.size(), 3);
    for (int j=0; j<cloud_decimated->points.size(); j++)
        feats_mat.at<float>(j,0) = cloud_decimated->points[j].x, 
            feats_mat.at<float>(j,1) = cloud_decimated->points[j].y, 
            feats_mat.at<float>(j,2) = cloud_decimated->points[j].z;

    // Find k-nearest neighbors and compute surface normal
    cv::flann::LinearIndexParams flannIndexParams; // using 4 randomized kdtrees        
    cv::flann::Index knn_index(feats_mat, flannIndexParams);


    // Keypoint Features in 3D
    Track3D feats_3d; feats_3d.resize(feats.size());
    for (int j=0; j<feats.size(); j++) { 
        int idx = int(feats[j].y)*cloud->width + int(feats[j].x);
        if (idx < 0 || idx > cloud->points.size()) { 
            pcl::PointXYZRGB pt; pt.x = pt.y = pt.z = 0;
            pcl::Normal nm; nm.normal[0] = 1, nm.normal[1] = nm.normal[2] = 0;
            feats_3d[j] = Feature3D(pt, nm);
            continue;
        }

        pcl::PointXYZRGB pt = cloud->points[idx]; 

        // Search new points to see if within threshold of old points
        int knn = 1;
        cv::Mat knn_inds;
        cv::Mat knn_dists;
        cv::Mat_<float> pt_mat(1, 3);
        pt_mat.at<float>(0,0) = pt.x, pt_mat.at<float>(0,1) = pt.y, pt_mat.at<float>(0,2) = pt.z;
        knn_index.knnSearch(pt_mat, knn_inds, knn_dists, knn, cv::flann::SearchParams());

        // Assign the same surface normal as the closest normal (from decimated cloud)
        int k_ind = knn_inds.at<int>(0,0);
        pcl::Normal nm = cloud_normals->points[k_ind];

        feats_3d[j] = Feature3D(pt, nm);

        // std::cerr << "feats: " << pt_mat.size() << " knn_inds: " << knn_inds.size() << 
        //     " knn_dists: " << knn_dists.size() << std::endl;
    }

        
    
    return feats_3d;
}

void publish_tracks() { 
    erlcm_tracklet_list_t tracklet_list_msg;
    tracklet_list_msg.num_tracks = tracklet_3d.size();
    tracklet_list_msg.tracks = (erlcm_tracklet_t*)
        malloc(sizeof(erlcm_tracklet_t) * tracklet_3d.size());

    int j = 0; 
    for (std::map<int, Track3D >::const_iterator it = tracklet_3d.begin(); 
         it != tracklet_3d.end(); it++, j++) { 
        const Track3D& track = it->second;
        if (!track.valid) continue;

        tracklet_list_msg.tracks[j].id = it->first; // track id
        tracklet_list_msg.tracks[j].num_poses = track.size();
        tracklet_list_msg.tracks[j].poses = (bot_core_pose_t*)
            malloc(sizeof(bot_core_pose_t) * track.size());        

        for (int k=0; k<track.size(); k++) { 
            tracklet_list_msg.tracks[j].poses[k].utime = track[k].utime;

            tracklet_list_msg.tracks[j].poses[k].pos[0] = track[k].point.x;
            tracklet_list_msg.tracks[j].poses[k].pos[1] = track[k].point.y;
            tracklet_list_msg.tracks[j].poses[k].pos[2] = track[k].point.z;
                
            tracklet_list_msg.tracks[j].poses[k].orientation[0] = 1;
            tracklet_list_msg.tracks[j].poses[k].orientation[1] = 0;
            tracklet_list_msg.tracks[j].poses[k].orientation[2] = 0;
            tracklet_list_msg.tracks[j].poses[k].orientation[3] = 0;
        }
    }
    erlcm_tracklet_list_t_publish(state->lcm, "TRACKLETS", &tracklet_list_msg);

    for (int j=0; j<tracklet_list_msg.num_tracks; j++) 
        free(tracklet_list_msg.tracks[j].poses);
    free(tracklet_list_msg.tracks);
    return;
    
}

void vis_tracks() { 
    BotTrans sensor_frame;
    bot_frames_get_trans (state->frames, "KINECT", "local", &sensor_frame);
    double rpy[3]; bot_quat_to_roll_pitch_yaw(sensor_frame.rot_quat, rpy);
        
    vs_obj_collection_t objs_msg; 
    objs_msg.id = 100; 
    objs_msg.name = "KINECT_POSE"; 
    objs_msg.type = VS_OBJ_COLLECTION_T_AXIS3D; 
    objs_msg.reset = true; 
    vs_obj_t poses[1]; 
    poses[0].id = 1; 
    poses[0].x = sensor_frame.trans_vec[0], 
        poses[0].y = sensor_frame.trans_vec[1], poses[0].z = sensor_frame.trans_vec[2]; 
    poses[0].roll = rpy[0], poses[0].pitch = rpy[1], poses[0].yaw = rpy[2]; 
    // poses[0].roll = 0, poses[0].pitch = 0, poses[0].yaw = 0; 
    objs_msg.nobjs = 1; 
    objs_msg.objs = &poses[0];
    vs_obj_collection_t_publish(state->lcm, "OBJ_COLLECTION", &objs_msg);

    vs_point3d_list_collection_t tracklets_msg;
    tracklets_msg.id = 400; 
    tracklets_msg.name = "TRACKLETS_3D"; 
    tracklets_msg.type = VS_POINT3D_LIST_COLLECTION_T_POINT; 
    tracklets_msg.reset = true; 
    std::vector<vs_point3d_list_t> track_list; // [tracklet_3d.size()];

    if (!tracklet_3d.size()) return ;
    int j = 0; 
    for (std::map<int, Track3D >::const_iterator it = tracklet_3d.begin(); 
         it != tracklet_3d.end(); it++, j++) {
        const Track3D& track = it->second;
        if (!track.valid) continue;
        track_list.resize(j+1);

        track_list[j].nnormals = 0; 
        track_list[j].normals = NULL; 
        track_list[j].npointids = 0; 
        track_list[j].pointids = NULL; 

        uint64_t time_id = (uint64_t)bot_timestamp_now(); 
        track_list[j].id = time_id; 
        track_list[j].collection = 100; 
        track_list[j].element_id = 1;

        track_list[j].npoints = track.size();
        track_list[j].points = new vs_point3d_t[track.size()];

        track_list[j].ncolors = track.size(); 
        track_list[j].colors = new vs_color_t[track.size()];

        for (int k=0; k<track.size(); k++) { 
            track_list[j].points[k].x = track[k].point.x;
            track_list[j].points[k].y = track[k].point.y;
            track_list[j].points[k].z = track[k].point.z;
            track_list[j].colors[k].r = .0;
            track_list[j].colors[k].g = .0;
            track_list[j].colors[k].b = .3;
        }
    }
    tracklets_msg.nlists = track_list.size();
    tracklets_msg.point_lists = &track_list[0]; 
    vs_point3d_list_collection_t_publish(state->lcm, "POINTS_COLLECTION", &tracklets_msg);

    for (int j=0; j<tracklets_msg.nlists; j++) {
        delete [] tracklets_msg.point_lists[j].points; 
        delete [] tracklets_msg.point_lists[j].colors; 
    }
}

void reset_all_trackers() { 
    for (TrackerMapIt it = trackerMap.begin(); it != trackerMap.end(); it++) { 
        it->second->reinit();
    }
    return; 
}

static void on_kinect_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                                   const kinect_frame_msg_t *msg, 
                                   void *user_data ) {
    // Check msg type
    state_t *state = (state_t*) user_data;
    if (msg->depth.depth_data_format != KINECT_DEPTH_MSG_T_DEPTH_MM) { 
        std::cerr << "Warning: kinect data format is not KINECT_DEPTH_MSG_T_DEPTH_MM" << std::endl 
                  << "Program may not function properly" << std::endl;
    }
    assert (msg->depth.depth_data_format == KINECT_DEPTH_MSG_T_DEPTH_MM);

    //----------------------------------
    // Reset if Log repeats
    //----------------------------------
    if (earliest_utime >= msg->timestamp) { 
        reset(); 
        earliest_utime = msg->timestamp; 
        std::cerr << "******************************************************************************" << std::endl;
        std::cerr << "RESETTING!" << std::endl;
        reset_all_trackers(); 
        std::cerr << "******************************************************************************" << std::endl;
    }
        std::cerr << "******************************************************************************" << std::endl;

    //----------------------------------
    // Unpack Point cloud
    //----------------------------------
    double t1 = bot_timestamp_now();
    cv::Mat3b img(msg->image.height, msg->image.width);
    state->pc_lcm->unpack_kinect_frame(msg, img.data, cloud);
    cv::resize(img, rgb_img, cv::Size(int(img.cols / SCALE), int(img.rows / SCALE)));
    double t2 = bot_timestamp_now();
    std::cerr << "unpack time: " << (t2-t1)*1e-3 << std::endl;
    std::cerr << "cloud: " << cloud->points.size() << std::endl;
    std::cerr << "rgb: " << rgb_img.size() << std::endl;
    
    cvtColor(rgb_img, rgb_img, CV_RGB2BGR);

    // //----------------------------------
    // // Normal estimation
    // //----------------------------------
    // opencv_utils::tic(); 
    // normal_estimation(cloud, cloud_normals);
    // opencv_utils::toc("NORMAL_ESTIMATION");

    // Push to frame queue
    cv::Mat gray;
    cvtColor(rgb_img, gray, CV_BGR2GRAY);

    // // Compute features
    // std::vector<cv::Point2f> feats;
    // cv::goodFeaturesToTrack(gray, feats, MAX_COUNT, 0.01, 10, cv::Mat(), 3, 0, 0.04);
    // cv::cornerSubPix(gray, feats, subPixWinSize, cv::Size(-1,-1), termcrit);

    // Update frame queue and feats queue
    frame_queue.push_front(Frame(msg->timestamp, rgb_img));
    // feats_queue.push_front(feats);

    // // Compute 3D features
    // Track3D feats_3d = compute_3d_features(feats);
    // feats_3d_queue.push_front(feats_3d);

    // Frame queue size is constant
    if (frame_queue.size() > MAX_FRAME_QUEUE_SIZE) { 
        frame_queue.pop_back();
        // feats_queue.pop_back();
        // feats_3d_queue.pop_back();
        // assert(frame_queue.size() == feats_queue.size());
    }

    // KLT tracker
    opencv_utils::tic();
    perform_klt();
    opencv_utils::toc("PERFORM_KLT");


    // Prune tracks
    opencv_utils::tic();
    prune_tracks();
    opencv_utils::toc("PRUNE_TRACKS");

    // // Cluster tracks based on principal components
    // opencv_utils::tic();
    // cluster_tracks_with_shape_contexts();
    // // cluster_tracks_with_pca();
    // opencv_utils::toc("CLUSTER_TRACKS");
 
    plot_klt();
    plot_klt2();
    publish_tracks();
    vis_tracks(); // sends the tracks to collections renderer

}

void* lcm_thread_handler(void *l)
{
    lcm_t* lcm = (lcm_t*)(l);
    while(1)
	{
            lcm_handle(lcm);
	}
}



int 
main(int argc, char **argv)
{
    g_thread_init(NULL);
    setlinebuf (stdout);
    state = new state_t;

    ConciseArgs opt(argc, (char**)argv);
    opt.parse();

    //----------------------------------
    // LCM-related states
    //----------------------------------
    state->lcm =  bot_lcm_get_global(NULL);
    state->b_server = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->b_server);

    bool flip_coords = false;
    state->pc_lcm = new pointcloud_lcm(state->lcm, false);
    state->pc_lcm->set_kinect_decimate(1.f);  // no SCALE (ing)

    // Vis Config:
    state->pc_vis = new pointcloud_vis(state->lcm);

    //----------------------------------
    // PCL
    //----------------------------------
    cloud = pcl::PointCloud<pcl::PointXYZRGB>::Ptr (new pcl::PointCloud<pcl::PointXYZRGB> ());
    cloud_decimated = pcl::PointCloud<pcl::PointXYZRGB>::Ptr (new pcl::PointCloud<pcl::PointXYZRGB> ());
    cloud_normals = pcl::PointCloud<pcl::Normal>::Ptr (new pcl::PointCloud<pcl::Normal> ());

    //----------------------------------
    // Subscribe, and start main loop
    //----------------------------------
    kinect_frame_msg_t_subscribe(state->lcm, "KINECT_FRAME", on_kinect_image_frame, (void*) state );
    state->mainloop = g_main_loop_new( NULL, FALSE );  

    // pthread_t lcm_thread;
    // rc = pthread_create(&lcm_thread, NULL, lcm_thread_handler, lcm);

    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


