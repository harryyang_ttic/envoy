// #include <boost/numpy.hpp>
#include <boost/python.hpp>
#include <opencv2/opencv.hpp>
#include "lcmlogreader.hpp"

namespace bp = boost::python;
// namespace bn = boost::numpy;

// bn::ndarray lcmlogreader(...) { 

//     // get_image;
//     Py_intptr_t shape[2] = { img.rows, img.cols };
//     bn::ndarray result = bp::zeros(&shape[0], &shape[1], bn::dtype::get_built_in<double>());
//     std::copy(v.begin(), v.end(), reinterpret_cast<double*>(result.get_data()));

//     return result;
// }

using namespace bp;
BOOST_PYTHON_MODULE(py_lcmlogreader)
{
    // bp::initialize();

    void        (LCMLogReader::*d1)(std::string)             = &LCMLogReader::init;
    void (LCMLogReader::*d2)(const LCMLogReaderOptions& _options, bool sequential_read) = &LCMLogReader::init;
    bool (LCMLogReader::*d3)(int64_t sensor_utime) = &LCMLogReader::getFrame;

    class_<LCMLogReader>("LCMLogReader")
        .def("init", d1)
        .def("getFrame", &LCMLogReader::getFrame)
        ;
};
