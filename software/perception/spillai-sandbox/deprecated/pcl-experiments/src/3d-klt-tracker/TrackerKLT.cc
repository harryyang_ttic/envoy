#include "TrackerKLT.hpp"

using namespace spvision;
TrackerKLT::TrackerKLT() : VERBOSE_MODE(false), _inited(false) { 
    std::cerr << "INITIALIZING TRACKER KLT: " << std::endl;
    init();
}

TrackerKLT::TrackerKLT(const TrackerKLT& tr) { 
}

TrackerKLT::~TrackerKLT() { 
}

void
TrackerKLT::setParams(char* str, double value) { 
    
}


void 
TrackerKLT::initTracker() { 
    // "CFeatureTracker_KL" is by far the most robust implementation for now:
    // tracker = CGenericFeatureTrackerAutoPtr( new CFeatureTracker_KL );
    // tracker = CGenericFeatureTrackerAutoPtr( new CFeatureTracker_FAST );
    // tracker = CGenericFeatureTrackerAutoPtr( new CFeatureTracker_PatchMatch );
}

void 
TrackerKLT::setDefaultParams() { 
    // Set of parameters common to any tracker implementation: (ported implementation from MRPT)
    // -------------------------------------------------------------
    params.clear(); 

    params["remove_lost_features"]         = 1;   // automatically remove out-of-image and badly tracked features

    params["add_new_features"]             = 1;   // track, AND ALSO, add new features
    params["add_new_feat_min_separation"]  = 16;
    params["minimum_KLT_response_to_add"]  = 10;
    params["add_new_feat_max_features"]    = 500;
    params["add_new_feat_patch_size"]      = 11;

    params["update_patches_every"]		= 0;  // Don't update patches.
    params["check_KLT_response_every"]	= 5;	// Re-check the KLT-response to assure features are in good points.
    params["minimum_KLT_response"]	    = 5;

    // Specific params for "CFeatureTracker_KL"
    // ------------------------------------------------------
    params["window_width"]  = 5;
    params["window_height"] = 5;
    //params["LK_levels"] = 3;
    //params["LK_max_iters"] = 10;
    //params["LK_epsilon"] = 0.1;
    //params["LK_max_tracking_error"] = 150;

    return; 
}

void
TrackerKLT::reinit() { 
    pimg = cv::Mat();
    features = std::vector<cv::Point2f>(); 
    _inited = false; 
}

void
TrackerKLT::init() { 
    initTracker(); 
    setDefaultParams(); 
    _inited = true; 
}

void 
TrackerKLT::trackFeatures(cv::Mat& old_img, cv::Mat& new_img, std::vector<cv::Point2f>& featureList) { 

  
    const unsigned int 	window_width = params["window_width"];
    const unsigned int 	window_height = params["window_height"];

    const int 	LK_levels    = params["LK_levels"];
    const int 	LK_max_iters = params["LK_max_iters"];
    const int 	LK_epsilon   = params["LK_epsilon"];
    const float LK_max_tracking_error = params["LK_max_tracking_error"];

    // Both images must be of the same size
    ASSERT_( old_img.getWidth() == new_img.getWidth() && old_img.getHeight() == new_img.getHeight() );

    const size_t  img_width  = old_img.getWidth();
    const size_t  img_height = old_img.getHeight();

    const size_t nFeatures	= featureList.size();					// Number of features

    assert(old_img.channels() == 1 && new_img.channels() == 1);

    std::vector<uchar> status;
    std::vector<float> err;

    std::vector<int> tracklet_ids;
    std::vector<cv::Point2f> prop_feats;

    std::cerr << "===> ORIG tracklets: " << orig_feats.size() << std::endl;
    std::cerr << "===> ID tracklets: " << tracklet_ids.size() << std::endl;
    cv::calcOpticalFlowPyrLK(old_img, new_img, featuresList, prop_feats, status, err, cv::Size(window_width,window_height),
                             KL_levels, cv::TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,LK_max_iters,KL_epsilon), 0);

    assert(featuresList.size() == prop_feats.size()); 
    for (int j=0; j<featuresList.size(); j++) { 
        const bool err_large = err[j]>LK_max_tracking_error; 
        if (status[j]==1 && 
            !err_large && 
            prop_feats[j].x >= 0 && prop_feats[j].y >= 0 &&
            prop_feats[j].x < img_width && prop_feats[j].y < img_height) { 
            
        }

    }


}


// template <typename T>
void
TrackerKLT::update(int64_t utime, cv::Mat& img, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud) { 
    if (img.empty()) return; 

    // Previous and Current image available
    if (!pimg.empty() && !img.empty())
        // This single call makes: detection, tracking, recalculation of KLT_response, etc.
        trackFeatures(pcimg, cimg, simple_feats);

    // Convert to Features2D/Features3D
    std::vector<SuperFeature> feats = convertToFeatures(utime, simple_feats, cloud); 

    // add new points with id to the tracklet (update utime)
    std::set<int64_t> observed_ids; 
    for (int j=0; j<feats.size(); j++) { 
        SuperFeature& feat = feats[j]; 
        observed_ids.insert(feat.id);

        // std::cerr << "featID: " << feat.id << std::endl;
        feat.utime = utime; 
        tracklet[feat.id].push_back(feat);
    }
    std::cerr << "===> Tracklets: " << tracklet.size() << std::endl;

    // prune so that length is only MAX_TRACK_TIMESPAN seconds long
    for (TrackMapIt it = tracklet.begin(); 
         it != tracklet.end(); it++) { 
        Track& track = it->second;
        if (!track.size()) continue;
        int64_t latest_utime = track.end()[-1].utime;
        int64_t first_utime = track.begin()[0].utime;
        // assert(latest_utime > first_utime);
        if (latest_utime - first_utime > MAX_TRACK_TIMESPAN * 1e6) { 
            int idx = track.size()-1; 
            for (; idx>=0; idx--) 
                if (latest_utime - track[idx].utime > MAX_TRACK_TIMESPAN * 1e6)
                    break;
            track.erase(track.begin(), track.begin() + idx + 1);
        }
    }

    // // Remove all tracks whose most recent feature is lagging by more 
    // // than MIN_TRACK_TIMESTAMP_DELAY seconds from the most recent timestamp
    // // Should take care of resetting as well
    // for (TrackMapIt it = tracklet.begin(); 
    //      it != tracklet.end(); it++) { 
    //     Track& track = it->second;
    //     if (!track.size()) continue;

    //     int64_t latest_utime = track.end()[-1].utime;
    //     if (utime - latest_utime > MAX_TRACK_TIMESTAMP_DELAY * 1e6) { 
    //         tracklet.erase(it);
    //     } 
    // }

    // Mark as invalid if not observed
    for (TrackMapIt it = tracklet.begin(); 
         it != tracklet.end(); it++) { 
        Track& track = it->second;
        if (observed_ids.find(it->first) == observed_ids.end())
            track.valid = false;
    }

    // Mark as valid if observed
    for (std::set<int64_t>::iterator it = observed_ids.begin(); it != observed_ids.end(); it++) { 
        if (tracklet.find(*it) != tracklet.end())
            tracklet[*it].valid = true;
    }
        

    pcimg = cimg; 
    pimg = img.clone();
    return; 
}

std::vector<SuperFeature>
TrackerKLT::convertToFeatures(int64_t utime, const TSimpleFeatureList& _feats, 
                                 pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud) { 
    std::vector<SuperFeature> feats(_feats.size()); 
    for (int j=0; j<_feats.size(); j++) { 
        int idx = int(_feats[j].pt.y)*cloud->width + int(_feats[j].pt.x);
        if (idx < 0 || idx > cloud->points.size()) { 
            feats[j] = SuperFeature();
            continue;
        }

        pcl::PointXYZRGB pt = cloud->points[idx];
        feats[j].utime = utime;
        feats[j].id = _feats[j].ID;
        feats[j].response = _feats[j].response;
        feats[j].point2D.x = _feats[j].pt.x;
        feats[j].point2D.y = _feats[j].pt.y;        
        feats[j].point3D.x = pt.x, feats[j].point3D.y = pt.y, feats[j].point3D.z = pt.z;
        feats[j].normal = cv::Vec3f(1,0,0); // no normal computation
    }
    return feats; 
}


// std::vector<Feature2D> 
// TrackerKLT::convertTo2DFeatures(int64_t utime, const TSimpleFeatureList& _feats) { 
//     std::vector<Feature2D> feats(_feats.size()); 
//     for (int j=0; j<_feats.size(); j++) { 
//         feats[j].utime = utime;
//         feats[j].id = _feats[j].ID;
//         feats[j].response = _feats[j].response; 
//         feats[j].point.x = _feats[j].pt.x; 
//         feats[j].point.y = _feats[j].pt.y;         
//     }
//     return feats; 
// }
 
// // template <typename T>
// std::vector<Feature3D>
// TrackerKLT::convertTo3DFeatures(int64_t utime, const TSimpleFeatureList& _feats, 
//                                  pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud) { 
//     std::vector<Feature3D> feats(_feats.size()); 
//     for (int j=0; j<_feats.size(); j++) { 
//         int idx = int(_feats[j].pt.y)*cloud->width + int(_feats[j].pt.x);
//         if (idx < 0 || idx > cloud->points.size()) { 
//             feats[j] = Feature3D();
//             continue;
//         }

//         pcl::PointXYZRGB pt = cloud->points[idx];
//         feats[j].utime = utime;
//         feats[j].id = _feats[j].ID;
//         feats[j].point.x = pt.x, feats[j].point.y = pt.y, feats[j].point.z = pt.z;
//         feats[j].normal = cv::Vec3f(1,0,0); // no normal computation
//     }
//     return feats; 
// }
