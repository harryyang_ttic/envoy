// Simultaneous Object Pose and Segmentation via pose-pairs
// Author: Sudeep Pillai (spillai@csail.mit.edu)
// TODO: 
// http://lear.inrialpes.fr/people/wang/dense_trajectories
// http://www.irisa.fr/vista/Papers/2009_bmvc_wang.pdf
// http://www.vision.jhu.edu/manifoldlearning.htm
#include "pose-pair-estimator.hpp"

// opencv-utils includes
#include <perception_opencv_utils/calib_utils.hpp>
#include <perception_opencv_utils/imshow_utils.hpp>
#include <perception_opencv_utils/math_utils.hpp>
#include <perception_opencv_utils/plot_utils.hpp>
#include <perception_opencv_utils/color_utils.hpp>
#include <perception_opencv_utils/imgproc_utils.hpp>

using namespace spvision;
const std::string PROCESS_NAME("POSE PAIR ESTIMATOR");

struct PosePairEstimatorOptions { 
    bool vLIVE_MODE;
    bool vDEBUG;
    float vMAX_FPS;
    std::string vLOG_FILENAME;
    std::string vCHANNEL;

    PosePairEstimatorOptions () : 
        vLOG_FILENAME(""), vCHANNEL("KINECT_FRAME"), vMAX_FPS(30.f) , vDEBUG(false) {}
};
PosePairEstimatorOptions options;

static void 
on_tracks (const lcm_recv_buf_t *rbuf, const char *channel,
           const erlcm_tracklet_list_t *tracks_msg, void *user_data ) { 
    
    std::cerr << "on_object_tracks" << std::endl;
    double tic, t1 = bot_timestamp_now();
    const int NUM_TRACKS_IN = tracks_msg->num_tracks;
    if (!(NUM_TRACKS_IN + MOPT.numTracks())) return;
    std::cerr << "IN Num Tracks: " << tracks_msg->num_tracks << std::endl;

    //--------------------------------------------
    // Only process frames that are latest
    //--------------------------------------------
     int64_t utime = 0; 
     UniquePoseMap track_features; 
     for (int j=0; j<tracks_msg->num_tracks; j++) { 
         int64_t track_id = tracks_msg->tracks[j].track_id;
         bot_core_pose_t& pose = tracks_msg->tracks[j].poses[tracks_msg->tracks[j].num_poses-1];
         if (!tracks_msg->tracks[j].num_poses) continue;
         if (utime) assert(utime == pose.utime);
         else utime = pose.utime;
         track_features[track_id] = pose;
     }

    //----------------------------------
    // Update tracks in streaming form 
    //----------------------------------
    tic = bot_timestamp_now();
    MOPT.update(utime, track_features);
    printf("%s===> UPDATE: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    std::cerr << "NUM TRACKLETS: " << MOPT.numTracks() << std::endl;
    if (!MOPT.numTracks()) return; 
    const int NUM_TRACKS = MOPT.numTracks(); 

    //----------------------------------
    // Viz (track_cloud)
    //----------------------------------
    tic = bot_timestamp_now();
    MOPT.viz(); 
    printf("%s===> VIZ: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    printf("%s===> TOTAL TIME: %4.2f ms%s\n", esc_red, (bot_timestamp_now() - t1) * 1e-3, esc_def); 
    return;
}

int 
main(int argc, char **argv)
{
    state = new state_t; 


    ConciseArgs opt(argc, (char**)argv);
    opt.add(options.vLOG_FILENAME, "f", "log-file","Log file name");
    opt.add(options.vCHANNEL, "c", "channel","Kinect Channel");
    opt.add(options.vMAX_FPS, "r", "max-rate","Max FPS");
    opt.parse();
  
    std::cerr << "===========  POSE PAIR ESTIMATOR ============" << std::endl;
    std::cerr << "MODES 1: pose-pair-estimator -f log-file -r max-rate\n";
    std::cerr << "=============================================\n";
    if (options.vLOG_FILENAME != "") { 
        std::cerr << "=> LOG FILENAME : " << options.vLOG_FILENAME << std::endl;    
    } else { 
        std::cerr << "=> FAILED TO FIND : " << options.vLOG_FILENAME << ". Exiting!" << std::endl;    
        exit(0);
    }
    // std::cerr << "=> CHANNEL : " << options.vCHANNEL << std::endl;
    // std::cerr << "=> MAX FPS : " << options.vMAX_FPS << std::endl;
    // std::cerr << "=> DEBUG : " << options.vDEBUG << std::endl;
    // std::cerr << "=> Note: Hit 'space' to proceed to next frame" << std::endl;
    // std::cerr << "=> Note: Hit 'p' to proceed to previous frame" << std::endl;
    // std::cerr << "=> Note: Hit 'n' to proceed to previous frame" << std::endl;
    std::cerr << "===============================================" << std::endl;

    state->lcm =  bot_lcm_get_global(NULL);
    state->p_server = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->p_server);

    MOPT.setState(state->lcm, state->frames);

    //----------------------------------
    // Read features file
    //----------------------------------
    double tic = bot_timestamp_now();
    std::string fn = options.vLOG_FILENAME + "-feats.yml"; 
    std::cerr << "Reading KLT Features from " << fn << std::endl;
    cv::FileStorage fs(fn.c_str(), cv::FileStorage::READ);

    const int N = (int)fs["num_feats"];
    const int T = (int)fs["num_frames"];
    const int D = (int)fs["num_dims"];

    cv::Mat ids, utimes;
    cv::SparseMat feats; 
    fs["feature_ids"] >> ids;
    fs["feature_utimes"] >> utimes;
    fs["feature_data"] >> feats;
    fs.release();

    std::cerr << "Done Reading feats yml file" << std::endl;
    std::cerr << "ids: " << ids.size() << std::endl;
    std::cerr << "utimes: " << utimes.size() << std::endl;
    std::cerr << "feats: " << feats.size() << std::endl;
    printf("%s===> IO: %4.2f ms%s\n", esc_green, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    //----------------------------------
    // Viz sparsity of data
    //----------------------------------
    cv::Mat1b dfeats = cv::Mat1b::zeros(N,T); 
    for (int j=0; j<N; j++) 
        for (int k=0; k<T; k++)
            if (feats.ptr(j, k, 0, false)) dfeats(j,k) = 1; 
    cv::imshow("sparsity", dfeats * 255);

    //----------------------------------
    // 1. Pruning 
    //----------------------------------
    // Min # of features / track
    const int MIN_FEATURES_PER_TRACKLET = 5; 
    std::vector<bool> valid_ids(N, false);
    for (int j=0; j<N; j++) valid_ids[j] = (cv::sum(dfeats.row(j))[0] >= MIN_FEATURES_PER_TRACKLET);

    //----------------------------------
    // Viz sparsity of data
    //----------------------------------
    cv::Mat3b dfeats2 = cv::Mat3b::zeros(N,T); 
    for (int j=0; j<N; j++) { 
        cv::Vec3b color = valid_ids[j] ? cv::Vec3b(0,255,0) : cv::Vec3b(0,0,255);
        for (int k=0; k<T; k++)
            dfeats2(j,k) = (dfeats(j,k)) ? color : cv::Vec3b(0,0,0); 
    }
    cv::imshow("pruned sparsity", dfeats2 * 255);


    while(1) { 
        unsigned char c = cv::waitKey(10) & 0xff;
        if (c == 'q') break;  
        cv::waitKey(1);
    }

    return 0;
}
