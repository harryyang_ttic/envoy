// 3D KLT tracker
// Author: Sudeep Pillai (spillai@csail.mit.edu)
// TODO: 
// http://lear.inrialpes.fr/people/wang/dense_trajectories
// http://www.irisa.fr/vista/Papers/2009_bmvc_wang.pdf
// http://www.vision.jhu.edu/manifoldlearning.htm

#include "3d_klt_tracker.h"

// lcm log player wrapper
#include "lcmlogreader.hpp"

// Run: 
// 3d-klt-tracker -f ~/data/2013_articulation_test/book_unfolding/lcmlog-2013-03-16.00 -p0 -r 1000

// opencv-utils includes
#include <perception_opencv_utils/calib_utils.hpp>
#include <perception_opencv_utils/imshow_utils.hpp>
#include <perception_opencv_utils/math_utils.hpp>
#include <perception_opencv_utils/plot_utils.hpp>
#include <perception_opencv_utils/color_utils.hpp>
#include <perception_opencv_utils/imgproc_utils.hpp>


using namespace spvision;

static void on_kinect_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                                   const kinect_frame_msg_t *msg, 
                                   void *user_data );


std::string locate_folder(const std::string& fn) { 
    size_t found = fn.find_last_of("/");
    return fn.substr(0, found+1);
}

#define PERFORM_GPFT 0

// LCM Log player wrapper
LCMLogReaderOptions poptions;
LCMLogReader player;

struct KLTTrackerOptions { 
    bool vLIVE_MODE;
    bool vDEBUG;
    int vPLOT;
    float vMAX_FPS;
    std::string vLOG_FILENAME;
    std::string vFEATS_FILENAME;
    std::string vCHANNEL;

    KLTTrackerOptions () : 
        vLOG_FILENAME(""), vFEATS_FILENAME(""), vCHANNEL("KINECT_FRAME"), 
        vMAX_FPS(30.f) , vDEBUG(false), vLIVE_MODE(false), vPLOT(1) {}
};
KLTTrackerOptions options;

// Single object tracking
void perform_fast_tracking() { 
    // std::cerr << "------------------------------------------>" << std::endl;
    std::cerr << "Frame queue: " << frame_queue.size() << std::endl;
    if (!frame_queue.size()) return;

    uint64_t utime = frame_queue.front().utime;
    cv::Mat frame = frame_queue.front().img; 
    printf("time: %f s\n", utime * 1e-6); 

#if PERFORM_GPFT
    GPFT.update(utime, frame, cloud, cloud_normals, cloud_decimated_tree, DEPTH_SCALE);
#else 
    MRPTT.update(utime, frame, cloud, cloud_normals, cloud_decimated_tree);
#endif
    return; 
}

void populate_viz_tangents(vs_point3d_list_t& viz_list,
                           const Track& track, char* str) { 
    
    cv::Vec3f color = select_color(str); 
    viz_list.nnormals = 0; 
    viz_list.normals = NULL; 
    viz_list.npointids = 0; 
    viz_list.pointids = NULL; 

    // This is set from 3d_klt_tracker (hack!!)
    uint64_t time_id = (uint64_t)bot_timestamp_now(); 
    viz_list.id = time_id; 
    viz_list.collection = 100; 
    viz_list.element_id = 1;

    viz_list.npoints = track.size() * 2;
    viz_list.points = new vs_point3d_t[track.size() * 2];

    viz_list.ncolors = track.size() * 2; 
    viz_list.colors = new vs_color_t[track.size() * 2];

    for (int k=0; k<track.size(); k++) { 
        int idx = 2*k;
        viz_list.points[idx].x = track[k].point3D.x;
        viz_list.points[idx].y = track[k].point3D.y;
        viz_list.points[idx].z = track[k].point3D.z;
        viz_list.colors[idx].r = color[0];
        viz_list.colors[idx].g = color[1];
        viz_list.colors[idx].b = color[2];

        viz_list.points[idx+1].x = track[k].point3D.x + 0.025 * track[k].normal[0];
        viz_list.points[idx+1].y = track[k].point3D.y + 0.025 * track[k].normal[1];
        viz_list.points[idx+1].z = track[k].point3D.z + 0.025 * track[k].normal[2];
        viz_list.colors[idx+1].r = color[0];
        viz_list.colors[idx+1].g = color[1];
        viz_list.colors[idx+1].b = color[2];
    }
    return;
}

// void moving_least_squares(pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud) { 
//     // Create a KD-Tree
//     pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB>);

//     // Output has the PointNormal type in order to store the normals calculated by MLS
//     // cloud_smoothed

//     // Init object (second point type is for the normals, even if unused)
//     pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointNormal> mls;
 
//     mls.setComputeNormals (true);

//     // Set parameters
//     mls.setInputCloud (cloud);
//     mls.setPolynomialFit (true);
//     mls.setSearchMethod (tree);
//     mls.setSearchRadius (0.08);

//     // Reconstruct
//     mls.process (*cloud_smoothed_);

//     // compute cloud_smoothed, cloud_smoothed_normals
//     cloud_smoothed->points.resize(cloud_smoothed_->points.size());
//     cloud_smoothed_normals->points.resize(cloud_smoothed_->points.size());
//     float u2f = 1.f / 255.f;
//     for (int j=0; j<cloud_smoothed_->points.size(); j++) { 
//         cloud_smoothed->points[j].x = cloud_smoothed_->points[j].x,
//             cloud_smoothed->points[j].y = cloud_smoothed_->points[j].y,
//             cloud_smoothed->points[j].z = cloud_smoothed_->points[j].z;

//         cloud_smoothed->points[j].r = cloud->points[j].r * u2f,
//             cloud_smoothed->points[j].g = cloud->points[j].g * u2f,
//             cloud_smoothed->points[j].b = cloud->points[j].b * u2f;
        
//         cloud_smoothed_normals->points[j].normal[0] = cloud_smoothed_->points[j].normal[0];
//         cloud_smoothed_normals->points[j].normal[1] = cloud_smoothed_->points[j].normal[1];
//         cloud_smoothed_normals->points[j].normal[2] = cloud_smoothed_->points[j].normal[2];
//     }
//     return;
// }

void normal_estimation(pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, pcl::PointCloud<pcl::Normal>::Ptr& cloud_normals) { 
    // Normal estimation
#define INTEGRAL_IMAGE_NORMAL 1
#if INTEGRAL_IMAGE_NORMAL
    pcl::IntegralImageNormalEstimation<pcl::PointXYZRGB, pcl::Normal> ne;
#else 
    pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> ne;
#endif

#if 0
    pcl::VoxelGrid<pcl::PointXYZRGB> sor;
    sor.setInputCloud (cloud->makeShared ());
    sor.setLeafSize (0.05f, 0.05f, 0.05f); //was 0.01
    sor.filter (*cloud_decimated);
#else    
    // Decimate point cloud by DEPTH_SCALE
    cloud_decimated->width    =(int) (cloud->width/ (double) DEPTH_SCALE) ;
    cloud_decimated->height   =(int) (cloud->height/ (double) DEPTH_SCALE);
    cloud_decimated->is_dense = true;
    cloud_decimated->points.resize (cloud_decimated->width * cloud_decimated->height);

    boost::shared_ptr<vector<int> > search_inds (new vector<int> ()); // cloud_decimated->points.size()));
    for(int v=0, j=0; v<cloud->height; v+=DEPTH_SCALE)
        for(int u=0; u<cloud->width; u+=DEPTH_SCALE ) { 
            int idx = v*cloud->width+u; 
            search_inds->push_back(j);
            cloud_decimated->points[j++] = cloud->points[idx];
        }
#endif
            
#if INTEGRAL_IMAGE_NORMAL
    // options: COVARIANCE_MATRIX, AVERAGE_3D_GRADIENT, AVERAGE_DEPTH_CHANGE, SIMPLE_3D_GRADIENT
    ne.setNormalEstimationMethod (ne.AVERAGE_3D_GRADIENT);
    // ne.setDepthDependentSmoothing(true);
    ne.setMaxDepthChangeFactor(0.5f);
    ne.setNormalSmoothingSize(10.0f);    
    // options: BORDER_POLICY_MIRROR, BORDER_POLICY_IGNORE
    ne.setBorderPolicy(ne.BORDER_POLICY_MIRROR);
    ne.setInputCloud(cloud_decimated);
    ne.compute(*cloud_normals);
    
    boost::shared_ptr<vector<int> > indices (new vector<int> ());
    for (int j=0; j<cloud_normals->points.size(); j++) 
        if (!((cloud_normals->points[j].normal[0] != cloud_normals->points[j].normal[0]) || 
              (cloud_normals->points[j].normal[1] != cloud_normals->points[j].normal[1]) || 
              (cloud_normals->points[j].normal[2] != cloud_normals->points[j].normal[2])))
            indices->push_back(j);
        else
            cloud_normals->points[j].normal[0] = 0, 
                cloud_normals->points[j].normal[1] = 0, 
                cloud_normals->points[j].normal[2] = 1;

    // Setup kd-tree
    // cloud_decimated_tree->setInputCloud(cloud_decimated, indices);
    cloud_decimated_tree->setInputCloud(cloud_decimated);
#else
    ne.setInputCloud(cloud_decimated);
    // ne.setIndices(search_inds);
    ne.setSearchMethod(cloud_decimated_tree);
    // ne.setRadiusSearch(0.05);
    ne.setKSearch(9);
    ne.compute(*cloud_normals);
#endif
    std::cout << "Estimated the decimated normals" << std::endl;

    return;
}

void publish_tracks(int64_t utime) { 

    erlcm_tracklet_list_t tracklet_list_msg;
    std::vector<erlcm_tracklet_t> tracks;
    int j = 0; 

    bool send_last_frame = true;

    int count = 0; 
    // for each track
#if PERFORM_GPFT
    for (TrackMapIt it = GPFT.tracklet_manager.tracklets.begin(); 
         it != GPFT.tracklet_manager.tracklets.end(); it++) { 
#else 
    for (TrackMapIt it = MRPTT.tracklet_manager.tracklets.begin(); 
         it != MRPTT.tracklet_manager.tracklets.end(); it++) { 
#endif
        const int64_t feat_id = it->first; 
        const Track& track = it->second; 
        if (!track.valid) continue;
        if (track.end()[-1].utime != utime) continue; // only send current frame

        // if (track.size() < MIN_TRACK_SIZE) continue;
        tracks.resize(tracks.size() + 1);

        tracks[j].object_id = j; 
        tracks[j].track_id = feat_id; // track_id
        
        tracks[j].num_poses = 1; // track.size();
        tracks[j].poses = new bot_core_pose_t[1];

        tracks[j].poses[0].utime = track.end()[-1].utime;

        tracks[j].poses[0].pos[0] = track.end()[-1].point3D.x;
        tracks[j].poses[0].pos[1] = track.end()[-1].point3D.y;
        tracks[j].poses[0].pos[2] = track.end()[-1].point3D.z;

        // HACK??
        tracks[j].poses[0].orientation[0] = track.end()[-1].normal[0];
        tracks[j].poses[0].orientation[1] = track.end()[-1].normal[1];
        tracks[j].poses[0].orientation[2] = track.end()[-1].normal[2];
        tracks[j].poses[0].orientation[3] = 0;

        // Full KLT features in playback mode
        full_klt_features[feat_id].push_back(track.end()[-1]);

        count++;

        j++; 
    }
    tracks.resize(j);
    tracklet_list_msg.num_tracks = tracks.size();
    tracklet_list_msg.tracks = &tracks[0];
    erlcm_tracklet_list_t_publish(state->lcm, "TRACKLETS", &tracklet_list_msg);

    for (int j=0; j<tracks.size(); j++) 
        delete [] tracks[j].poses;

    std::cerr << "Publish TRACKLETS: " << tracks.size() << " points: " << count << std::endl;
    return;
    
}

void vis_tracks() { 
    BotTrans sensor_frame;
    bot_frames_get_trans (state->frames, "KINECT", "local", &sensor_frame);
    double rpy[3]; bot_quat_to_roll_pitch_yaw(sensor_frame.rot_quat, rpy);
        
    //----------------------------------
    // Viz SENSOR POSE
    //----------------------------------
    vs_obj_collection_t objs_msg; 
    objs_msg.id = 100; 
    objs_msg.name = "KINECT_POSE | KLT"; 
    objs_msg.type = VS_OBJ_COLLECTION_T_AXIS3D; 
    objs_msg.reset = true; 
    vs_obj_t poses[1]; 
    poses[0].id = 1; 
    poses[0].x = sensor_frame.trans_vec[0], 
        poses[0].y = sensor_frame.trans_vec[1], poses[0].z = sensor_frame.trans_vec[2]; 
    poses[0].roll = rpy[0], poses[0].pitch = rpy[1], poses[0].yaw = rpy[2]; 
    // poses[0].roll = 0, poses[0].pitch = 0, poses[0].yaw = 0; 
    objs_msg.nobjs = 1; 
    objs_msg.objs = &poses[0];
    vs_obj_collection_t_publish(state->lcm, "OBJ_COLLECTION", &objs_msg);

    // //----------------------------------
    // // Viz latest track
    // //----------------------------------
    // vs_obj_collection_t tracklet_tip_msg; 
    // tracklet_tip_msg.id = 101; 
    // tracklet_tip_msg.name = "TRACKLET_LATEST | KLT"; 
    // tracklet_tip_msg.type = VS_OBJ_COLLECTION_T_AXIS3D; 
    // tracklet_tip_msg.reset = true; 
    // std::vector<vs_obj_t> end_eff_poses;

    //----------------------------------
    // Viz text
    //----------------------------------
    vs_text_collection_t viz_text_msg; 
    viz_text_msg.id = 200; 
    viz_text_msg.name = "KLT | TRACKLETS_ID";
    viz_text_msg.type = 0; 
    viz_text_msg.reset = true;
    std::vector<vs_text_t> viz_texts; 

    //----------------------------------
    // Viz points
    //----------------------------------
    vs_point3d_list_collection_t tracklets_msg;
    tracklets_msg.id = 400; 
    tracklets_msg.name = "KLT | TRACKLETS_3D"; 
    tracklets_msg.type = VS_POINT3D_LIST_COLLECTION_T_POINT; 
    tracklets_msg.reset = true; 

    //----------------------------------
    // Viz points
    //----------------------------------
    vs_point3d_list_collection_t tracklets_strip_msg;
    tracklets_strip_msg.id = 401; 
    tracklets_strip_msg.name = "KLT | TRACKLETS_3D_STRIP"; 
    tracklets_strip_msg.type = VS_POINT3D_LIST_COLLECTION_T_LINE_STRIP; 
    tracklets_strip_msg.reset = true; 

    //----------------------------------
    // Viz normals
    //----------------------------------
    vs_point3d_list_collection_t viz_track_normals_msg;
    viz_track_normals_msg.id = 402; 
    viz_track_normals_msg.name = "KLT | TRACKLETS_NORMALS"; 
    viz_track_normals_msg.type = VS_POINT3D_LIST_COLLECTION_T_LINES; 
    viz_track_normals_msg.reset = true; 

    std::vector<vs_point3d_list_t> tracks; 
    std::vector<vs_point3d_list_t> viz_track_normals;

    // std::vector<cv::Scalar> track_colors(std::min(int(trackerMap.size()), 20));
    std::vector<cv::Scalar> track_colors(5);
    if (!track_colors.size()) return; 
    opencv_utils::fillColors(track_colors);
    {
    int j = 0, object_idx = 0; 
    // for (TrackerMapIt oit = trackerMap.begin(); 
    //      oit != trackerMap.end(); oit++, object_idx++) { 
    //     const int64_t object_id = oit->first; 
    //     TrackMap& tracklet = oit->second->tracklet; 

        // for each track

#if PERFORM_GPFT
        for (TrackMapIt it = GPFT.tracklet_manager.tracklets.begin(); 
             it != GPFT.tracklet_manager.tracklets.end(); it++) { 
#else 
        for (TrackMapIt it = MRPTT.tracklet_manager.tracklets.begin(); 
             it != MRPTT.tracklet_manager.tracklets.end(); it++) { 
#endif
            const int64_t feat_id = it->first; 
            const Track& track = it->second; 

            if (!track.size()) continue;
            tracks.resize(tracks.size() + 1);
            viz_track_normals.resize(viz_track_normals.size() + 1);
            viz_texts.resize(tracks.size()); 
            // end_eff_poses.resize(tracks.size());

            tracks[j].nnormals = 0; 
            tracks[j].normals = NULL; 
            tracks[j].npointids = 0; 
            tracks[j].pointids = NULL; 

            uint64_t time_id = (uint64_t)bot_timestamp_now(); 
            tracks[j].id = time_id; 
            tracks[j].collection = 100; 
            tracks[j].element_id = 1;

            tracks[j].npoints = track.size();
            tracks[j].points = new vs_point3d_t[track.size()];

            tracks[j].ncolors = track.size(); 
            tracks[j].colors = new vs_color_t[track.size()];

            // // Pose
            // end_eff_poses[j].id = time_id; 
            // end_eff_poses[j].x = track.end()[-1].point3D.x, 
            //     end_eff_poses[j].y = track.end()[-1].point3D.y, end_eff_poses[j].z = track.end()[-1].point3D.z; 
            // end_eff_poses[j].roll = rpy[0], end_eff_poses[j].pitch = rpy[1], end_eff_poses[j].yaw = rpy[2]; 

            // Text (ID)
            viz_texts[j].id = time_id; 
            viz_texts[j].collection_id = 400;
            viz_texts[j].object_id = time_id; 

            // std::cerr << "time: " << texts[j].id << " " << std::endl;
            // std::cerr << "collection: " << texts[j].collection_id << " " << std::endl;
            // std::cerr << "object: " << texts[j].object_id << " " << std::endl;

            char str[8];
            sprintf(str, "%li", feat_id); 
            viz_texts[j].text = str;
            // std::cerr << "str: " << str << " " << std::endl;

            // Populate tangents
            populate_viz_tangents(viz_track_normals[j], track, "blue");

            for (int k=0; k<track.size(); k++) { 
                tracks[j].points[k].x = track[k].point3D.x;
                tracks[j].points[k].y = track[k].point3D.y;
                tracks[j].points[k].z = track[k].point3D.z;
                tracks[j].colors[k].r = .9; // track_colors[object_idx][0] * 1.f / 255.f;
                tracks[j].colors[k].g = .1; // track_colors[object_idx][1] * 1.f / 255.f;
                tracks[j].colors[k].b = .1; // track_colors[object_idx][2] * 1.f / 255.f;
            }
            j++;
        }
    }
    tracklets_msg.nlists = tracks.size();
    tracklets_msg.point_lists = &tracks[0]; 

    tracklets_strip_msg.nlists = tracks.size();
    tracklets_strip_msg.point_lists = &tracks[0]; 

    viz_track_normals_msg.nlists = viz_track_normals.size();
    viz_track_normals_msg.point_lists = &viz_track_normals[0]; 

    viz_text_msg.n = viz_texts.size(); // texts.size(); 
    viz_text_msg.texts = &viz_texts[0];

    // tracklet_tip_msg.nobjs = end_eff_poses.size(); 
    // tracklet_tip_msg.objs = &end_eff_poses[0];
    // vs_obj_collection_t_publish(state->lcm, "OBJ_COLLECTION", &tracklet_tip_msg);

    vs_point3d_list_collection_t_publish(state->lcm, "POINTS_COLLECTION", &tracklets_msg);
    vs_point3d_list_collection_t_publish(state->lcm, "POINTS_COLLECTION", &tracklets_strip_msg);
    vs_point3d_list_collection_t_publish(state->lcm, "POINTS_COLLECTION", &viz_track_normals_msg);
    vs_text_collection_t_publish(state->lcm, "TEXT_COLLECTION", &viz_text_msg);

    //----------------------------------
    // Viz decimated pcl
    //----------------------------------
    vs_point3d_list_collection_t viz_decimated_cloud_msg;
    viz_decimated_cloud_msg.id = 410; 
    viz_decimated_cloud_msg.name = "KLT | DECIMATED_TRACK_CLOUD"; 
    viz_decimated_cloud_msg.type = VS_POINT3D_LIST_COLLECTION_T_POINT; 
    viz_decimated_cloud_msg.reset = true; 

    std::vector<vs_point3d_list_t> viz_decimated_cloud(1); 
    populate_viz_cloud(viz_decimated_cloud[0], cloud_decimated, "blue");
    viz_decimated_cloud_msg.nlists = viz_decimated_cloud.size();
    viz_decimated_cloud_msg.point_lists = &viz_decimated_cloud[0];
    vs_point3d_list_collection_t_publish(state->lcm, "POINTS_COLLECTION", &viz_decimated_cloud_msg);

    //----------------------------------
    // Viz normals
    //----------------------------------
    vs_point3d_list_collection_t viz_cloud_normals_msg;
    viz_cloud_normals_msg.id = 411; 
    viz_cloud_normals_msg.name = "KLT | DECIMATED_CLOUD_NORMALS"; 
    viz_cloud_normals_msg.type = VS_POINT3D_LIST_COLLECTION_T_LINES; 
    viz_cloud_normals_msg.reset = true; 

    std::vector<vs_point3d_list_t> viz_cloud_normals(1); 
    populate_viz_tangents(viz_cloud_normals[0], cloud_decimated, cloud_normals, "blue");
    viz_cloud_normals_msg.nlists = viz_cloud_normals.size();
    viz_cloud_normals_msg.point_lists = &viz_cloud_normals[0];
    vs_point3d_list_collection_t_publish(state->lcm, "POINTS_COLLECTION", &viz_cloud_normals_msg);

    // //----------------------------------
    // // Viz smoothed
    // //----------------------------------
    // vs_point3d_list_collection_t viz_cloud_smoothed_msg;
    // viz_cloud_smoothed_msg.id = 412; 
    // viz_cloud_smoothed_msg.name = "KLT | DECIMATED_CLOUD_SMOOTHED"; 
    // viz_cloud_smoothed_msg.type = VS_POINT3D_LIST_COLLECTION_T_POINT; 
    // viz_cloud_smoothed_msg.reset = true; 

    // std::vector<vs_point3d_list_t> viz_cloud_smoothed(1); 
    // populate_viz_cloud(viz_cloud_smoothed[0], cloud, NULL);
    // viz_cloud_smoothed_msg.nlists = viz_cloud_smoothed.size();
    // viz_cloud_smoothed_msg.point_lists = &viz_cloud_smoothed[0];
    // vs_point3d_list_collection_t_publish(state->lcm, "POINTS_COLLECTION", &viz_cloud_smoothed_msg);

    // //----------------------------------
    // // Viz smoothed normals
    // //----------------------------------
    // vs_point3d_list_collection_t viz_cloud_smoothed_normals_msg;
    // viz_cloud_smoothed_normals_msg.id = 413; 
    // viz_cloud_smoothed_normals_msg.name = "KLT | DECIMATED_CLOUD_SMOOTHED_NORMALS"; 
    // viz_cloud_smoothed_normals_msg.type = VS_POINT3D_LIST_COLLECTION_T_LINES; 
    // viz_cloud_smoothed_normals_msg.reset = true; 

    // std::vector<vs_point3d_list_t> viz_cloud_smoothed_normals(1); 
    // populate_viz_tangents(viz_cloud_smoothed_normals[0], cloud_smoothed, cloud_smoothed_normals, "green");
    // viz_cloud_smoothed_normals_msg.nlists = viz_cloud_smoothed_normals.size();
    // viz_cloud_smoothed_normals_msg.point_lists = &viz_cloud_smoothed_normals[0];
    // vs_point3d_list_collection_t_publish(state->lcm, "POINTS_COLLECTION", &viz_cloud_smoothed_normals_msg);

    for (int j=0; j<viz_decimated_cloud_msg.nlists; j++) {
        delete [] viz_decimated_cloud_msg.point_lists[j].points; 
        delete [] viz_decimated_cloud_msg.point_lists[j].colors; 
    }
    // for (int j=0; j<viz_cloud_smoothed_msg.nlists; j++) {
    //     delete [] viz_cloud_smoothed_msg.point_lists[j].points; 
    //     delete [] viz_cloud_smoothed_msg.point_lists[j].colors; 
    // }
    // for (int j=0; j<viz_cloud_smoothed_normals_msg.nlists; j++) {
    //     delete [] viz_cloud_smoothed_normals_msg.point_lists[j].points; 
    //     delete [] viz_cloud_smoothed_normals_msg.point_lists[j].colors; 
    // }
    for (int j=0; j<viz_cloud_normals_msg.nlists; j++) {
        delete [] viz_cloud_normals_msg.point_lists[j].points; 
        delete [] viz_cloud_normals_msg.point_lists[j].colors; 
    }
    for (int j=0; j<viz_track_normals_msg.nlists; j++) {
        delete [] viz_track_normals_msg.point_lists[j].points; 
        delete [] viz_track_normals_msg.point_lists[j].colors; 
    }
    for (int j=0; j<tracklets_msg.nlists; j++) {
        delete [] tracklets_msg.point_lists[j].points; 
        delete [] tracklets_msg.point_lists[j].colors; 
    }
}

void reset_all_trackers() { 
    MRPTT = spvision::MRPTTracker();
    GPFT.reset();
    return; 
}

static void on_kinect_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                                   const kinect_frame_msg_t *msg, 
                                   void *user_data ) {
    double tic; 

    // Check msg type
    state_t *state = (state_t*) user_data;
    if (msg->depth.depth_data_format != KINECT_DEPTH_MSG_T_DEPTH_MM) { 
        std::cerr << "Warning: kinect data format is not KINECT_DEPTH_MSG_T_DEPTH_MM" << std::endl 
                  << "Program may not function properly" << std::endl;
    }
    assert (msg->depth.depth_data_format == KINECT_DEPTH_MSG_T_DEPTH_MM);

    //----------------------------------
    // Reset if Log repeats
    //----------------------------------
    if (earliest_utime >= msg->timestamp) { 
        // reset(); 
        earliest_utime = msg->timestamp; 
        std::cerr << "******************************************************************************" << std::endl;
        std::cerr << "RESETTING!" << std::endl;
        reset_all_trackers(); 
        std::cerr << "******************************************************************************" << std::endl;
    }
        std::cerr << "******************************************************************************" << std::endl;

    //----------------------------------
    // Unpack Point cloud
    //----------------------------------
    double t1 = bot_timestamp_now();
    cv::Mat3b img(msg->image.height, msg->image.width);
    if (depth_img.empty()) depth_img = cv::Mat_<float>(img.size());

    state->pc_lcm->unpack_kinect_frame(msg, img.data, cloud);
    cv::resize(img, rgb_img, cv::Size(int(img.cols / SCALE), int(img.rows / SCALE)), 0, 0, cv::INTER_NEAREST);
    printf("%s===> UNPACK TIME: %4.2f ms%s\n", esc_yellow, (bot_timestamp_now() - t1) * 1e-3, esc_def); 
    std::cerr << "cloud: " << cloud->points.size() << std::endl;
    std::cerr << "rgb: " << rgb_img.size() << std::endl;

    //--------------------------------------------
    // Convert Color
    //--------------------------------------------
    cvtColor(rgb_img, rgb_img, CV_RGB2BGR);

    // //----------------------------------
    // // Moving Least Squares
    // //----------------------------------
    // double tic = bot_timestamp_now();
    // moving_least_squares(cloud);
    // printf("%s===> MOVING_LEAST_SQUARES: %4.2f ms%s\n", esc_yellow, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    //----------------------------------
    // Normal estimation
    //----------------------------------
    tic = bot_timestamp_now();
    normal_estimation(cloud, cloud_normals);
    printf("%s===> NORMAL_ESTIMATION: %4.2f ms%s\n", esc_yellow, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    // Push to frame queue
    cv::Mat gray;
    cvtColor(rgb_img, gray, CV_BGR2GRAY);

    // Update frame queue and feats queue
    frame_queue.push_front(Frame(msg->timestamp, rgb_img));

    // Frame queue size is constant
    if (frame_queue.size() > MRPT_MAX_FRAME_QUEUE_SIZE) { 
        frame_queue.pop_back();
    }

    //--------------------------------------------
    // Main Tracking 
    //--------------------------------------------
    // General purpose tracker
    tic = bot_timestamp_now();
    perform_fast_tracking();
    printf("%s===> PERFORM_TRACKING: %4.2f ms%s\n", esc_yellow, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    //--------------------------------------------
    // Publish Tracks
    //--------------------------------------------
    tic = bot_timestamp_now();
    publish_tracks(msg->timestamp);
    printf("%s===> PUBLISH TRACKS: %4.2f ms%s\n", esc_red, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    //--------------------------------------------
    // Viz Tracks
    //--------------------------------------------
    tic = bot_timestamp_now();
    if (options.vPLOT) vis_tracks(); // sends the tracks to collections renderer
    printf("%s===> VIZ_TRACKS: %4.2f ms%s\n", esc_red, (bot_timestamp_now() - tic) * 1e-3, esc_def); 

    printf("%s===> TOTAL TIME: %4.2f ms%s\n", esc_red, (bot_timestamp_now() - t1) * 1e-3, esc_def); 
    cv::waitKey(1);
}

void* lcm_thread_handler(void *l)
{
    lcm_t* lcm = (lcm_t*)(l);
    while(1)
	{
            lcm_handle(lcm);
	}
}

 void save_klt_features() { 


     std::set<double> ids_set; 
     std::map<double, int> ids_map; 

     std::set<double> utimes_set;
     std::map<double, int> utimes_map;
    //--------------------------------------------
    // Determine ids, and utimes
    //--------------------------------------------
     { int idx = 0; 
         for (std::map<int64_t, std::vector<spvision::SuperFeature> >::iterator it = full_klt_features.begin(); 
              it != full_klt_features.end(); it++) { 
             const std::vector<spvision::SuperFeature>& track = it->second;

             ids_set.insert(double(it->first));
             ids_map[it->first] = idx++;
             for (int j=0; j<track.size(); j++) 
                 utimes_set.insert(track[j].utime);
         }
     }

    //--------------------------------------------
    // Determine ids, and utimes
    //--------------------------------------------
     { int idx = 0;
         for (std::set<double>::iterator it = utimes_set.begin(); 
              it != utimes_set.end(); it++) 
             utimes_map[*it] = idx++;
     }

     const int N = ids_map.size();
     const int T = utimes_map.size(); 
     const int D = 8; // 8 feats: u, v, x, y, z, nx, ny, nz
     if (!utimes_map.size() || !ids_map.size()) return;

    //--------------------------------------------
    // Fill data
    //--------------------------------------------
     int sz[] = {N, T, D};
     cv::SparseMat feats(3, &sz[0], CV_32F);

     int idx_[3];
     for (std::map<int64_t, std::vector<spvision::SuperFeature> >::iterator it = full_klt_features.begin(); 
          it != full_klt_features.end(); it++) { 
         const double feat_id = it->first;

         assert(ids_map.find(feat_id) != ids_map.end());
         int id_idx = ids_map[feat_id];
         assert(id_idx >=0 && id_idx < N);
         idx_[0] = id_idx; 
         
         const std::vector<spvision::SuperFeature>& track = it->second;
         for (int j=0; j<track.size(); j++) { 
             double utime = track[j].utime;
             assert(track[j].id == feat_id);
             assert(utimes_map.find(utime) != utimes_map.end());
             int utime_idx = utimes_map[utime];
             assert(utime_idx >=0 && utime_idx < T);

             // indices
             idx_[1] = utime_idx; idx_[2] = 0;
            
             // u,v
             feats.ref<float>(idx_) = track[j].point2D.x; idx_[2]++;
             feats.ref<float>(idx_) = track[j].point2D.y; idx_[2]++;

             // x,y,z
             feats.ref<float>(idx_) = track[j].point3D.x; idx_[2]++;
             feats.ref<float>(idx_) = track[j].point3D.y; idx_[2]++;
             feats.ref<float>(idx_) = track[j].point3D.z; idx_[2]++;

             // nx,ny,nz
             feats.ref<float>(idx_) = track[j].normal[0]; idx_[2]++;
             feats.ref<float>(idx_) = track[j].normal[1]; idx_[2]++;
             feats.ref<float>(idx_) = track[j].normal[2];

         }
     }

     std::string fn = (options.vLIVE_MODE) ? options.vFEATS_FILENAME : options.vLOG_FILENAME + "-feats.yml";
     std::cerr << "Saving KLT Features to " << fn << std::endl;

     FileStorage fs(fn, FileStorage::WRITE);
     fs << "num_feats" << N;
     fs << "num_frames" << T;
     fs << "num_dims" << D;

     std::vector<double> ids(ids_set.begin(), ids_set.end());
     std::vector<double> utimes(utimes_set.begin(), utimes_set.end());
     cv::Mat_<double> ids_mat(ids);
     cv::Mat_<double> utimes_mat(utimes);

     fs << "feature_ids" << ids_mat;
     fs << "feature_utimes" << utimes_mat;

     cv::Mat dfeats; feats.copyTo(dfeats);
     fs << "feature_data" << dfeats;
     fs.release();

     return;
 }

void  INThandler(int sig) {
    printf("Exiting\n");

    //----------------------------------
    // Save KLT features (for log playback mode)
    //----------------------------------
    if (options.vFEATS_FILENAME != "") { 
        std::cerr << "saving klt features: " << std::endl;
        save_klt_features();
    }

    exit(0);
}

 
int 
main(int argc, char **argv)
{
    g_thread_init(NULL);
    setlinebuf (stdout);
    state = new state_t;

    ConciseArgs opt(argc, (char**)argv);
    opt.add(options.vLOG_FILENAME, "l", "log-file","Log file name");
    opt.add(options.vFEATS_FILENAME, "f", "feats-file","Feats file name");
    opt.add(options.vCHANNEL, "c", "channel","Kinect Channel");
    opt.add(options.vMAX_FPS, "r", "max-rate","Max FPS");
    opt.add(options.vPLOT, "p", "plot","Plot Viz");
    opt.parse();
  
    std::cerr << "===========  3D KLT Tracker ============" << std::endl;
    std::cerr << "MODES 1: 3d-klt-tracker -l log-file -f feats-file -r max-rate\n";
    std::cerr << "=============================================\n";
    if (options.vLOG_FILENAME != "") { 
        std::cerr << "=> LOG FILENAME : " << options.vLOG_FILENAME << std::endl;    
    } else { 
        std::cerr << "=> LIVE MODE" << std::endl; options.vLIVE_MODE = true;
        std::cerr << "=> FEATS FILENAME : " << options.vFEATS_FILENAME << std::endl;    
    }
    std::cerr << "=> CHANNEL : " << options.vCHANNEL << std::endl;
    std::cerr << "=> MAX FPS : " << options.vMAX_FPS << std::endl;
    std::cerr << "=> PLOT : " << options.vPLOT << std::endl;
    // std::cerr << "=> DEBUG : " << options.vDEBUG << std::endl;
    std::cerr << "=> Note: Hit 'space' to proceed to next frame" << std::endl;
    std::cerr << "=> Note: Hit 'p' to proceed to previous frame" << std::endl;
    std::cerr << "=> Note: Hit 'n' to proceed to previous frame" << std::endl;
    std::cerr << "===============================================" << std::endl;
  
    // Install signal handler to free data.
    struct sigaction sigINThandler; 
    sigINThandler.sa_handler = INThandler;
    sigemptyset(&sigINThandler.sa_mask);
    sigINThandler.sa_flags = 0;
    sigaction(SIGINT, &sigINThandler, NULL);
    // sigaction(SIGKILL, &sigINThandler, NULL);
    // sigaction(SIGTERM, &sigINThandler, NULL);
    // sigaction(SIGHUP, &sigINThandler, NULL);

    //----------------------------------
    // LCM-related states
    //----------------------------------
    state->lcm =  bot_lcm_get_global(NULL);
    state->b_server = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->b_server);

    bool flip_coords = false;
    state->pc_lcm = new pointcloud_lcm(state->lcm, false);
    state->pc_lcm->set_decimate(1.f);  // no SCALE (ing)

    // Vis Config:
    state->pc_vis = new pointcloud_vis(state->lcm);

    //----------------------------------
    // Setup Player Options
    //----------------------------------
    poptions.fn = options.vLOG_FILENAME;
    poptions.ch = options.vCHANNEL;
    poptions.fps = options.vMAX_FPS;
    poptions.lcm = state->lcm;
    poptions.handler = on_kinect_image_frame;
    poptions.user_data = (void*) state;

    //----------------------------------
    // PCL
    //----------------------------------
    cloud = pcl::PointCloud<pcl::PointXYZRGB>::Ptr (new pcl::PointCloud<pcl::PointXYZRGB> ());

    cloud_decimated = pcl::PointCloud<pcl::PointXYZRGB>::Ptr (new pcl::PointCloud<pcl::PointXYZRGB> ());
    cloud_decimated_tree =  pcl::search::KdTree<pcl::PointXYZRGB>::Ptr(new pcl::search::KdTree<pcl::PointXYZRGB>());
    cloud_normals = pcl::PointCloud<pcl::Normal>::Ptr (new pcl::PointCloud<pcl::Normal> ());


    cloud_smoothed_ = pcl::PointCloud<pcl::PointNormal>::Ptr (new pcl::PointCloud<pcl::PointNormal> ());
    cloud_smoothed = pcl::PointCloud<pcl::PointXYZRGB>::Ptr (new pcl::PointCloud<pcl::PointXYZRGB> ());
    cloud_smoothed_normals = pcl::PointCloud<pcl::Normal>::Ptr (new pcl::PointCloud<pcl::Normal> ());

    //----------------------------------
    // Subscribe, and start main loop
    //----------------------------------
    if (options.vLIVE_MODE) { 
        kinect_frame_msg_t_subscribe(state->lcm, options.vCHANNEL.c_str(), on_kinect_image_frame, (void*) state );

        while (1) lcm_handle(state->lcm);

        // state->mainloop = g_main_loop_new( NULL, FALSE );  
        // if (!state->mainloop) {
        //     printf("Couldn't create main loop\n");
        //     return -1;
        // }
        // //add lcm to mainloop 
        // bot_glib_mainloop_attach_lcm (state->lcm);

    } else { 
        player.init(poptions); 
    }

    if (options.vLIVE_MODE) { 
        //adding proper exiting 
        // bot_signal_pipe_glib_quit_on_kill (state->mainloop);
        // g_main_loop_run(state->mainloop);
    } 

    //----------------------------------
    // Save KLT features (for log playback mode)
    //----------------------------------
    if (options.vFEATS_FILENAME != "") save_klt_features();
  
    // bot_glib_mainloop_detach_lcm(state->lcm);
}






// void perform_fast_ncc_tracking() { 
//     std::cerr << "------------------------------------------>" << std::endl;
//     std::cerr << "Frame queue: " << frame_queue.size() << std::endl;
//     if (frame_queue.size() < 2) return;

//     uint64_t new_utime = frame_queue[0].utime;
//     uint64_t prev_utime = frame_queue[1].utime;
//     printf("time: %f s, %f s\n", new_utime * 1e-6, prev_utime * 1e-6); 

//     cv::Mat new_frame = frame_queue[0].img; 
//     cv::Mat prev_frame = frame_queue[1].img;

//     IplImage _theIplImg = IplImage(new_frame); 
//     IplImage* theIplImg = cvCloneImage(&_theIplImg);
//     CImage theImg;  // The grabbed image:
//     theImg.setFromIplImage(theIplImg);

//     // Take the resolution upon first valid frame.
//     if (!hasResolution) {
//         hasResolution = true;
//         // cameraParams.scaleToResolution()...
//         cameraParams.ncols = theImg.getWidth();
//         cameraParams.nrows = theImg.getHeight();
//     }


//     // Do tracking:
//     if (step_num>=1) {   // we need "previous_image" to be valid.
//         // This single call makes: detection, tracking, recalculation of KLT_response, etc.
//         tracker->trackFeatures(previous_image, theImg, trackedFeats);
//     }

//     // Save the image for the next step:
//     previous_image = theImg;

//     // Save history of feature observations:
//     // tracker->getProfiler().enter("Save history");

//     std::cerr << "FEATS2: " << trackedFeats.size() << std::endl; 
//     for (size_t i=0;i<trackedFeats.size();++i) {
//         TSimpleFeature &f = trackedFeats[i];
        
//         const TPixelCoordf pxRaw(f.pt.x,f.pt.y);
//         TPixelCoordf  pxUndist;
//         //mrpt::vision::pinhole::undistort_point(pxRaw,pxUndist, cameraParams);
//         pxUndist = pxRaw;
        
//         feat_track_history.push_back( TFeatureObservation(f.ID,curCamPoseId, pxUndist ) );
//     }
//     curCamPoseId++;
//     // tracker->getProfiler().leave("Save history");

//     // now that we're done with the image, we can directly write onto it
//     //  for the display
//     // ----------------------------------------------------------------
//     if (DO_HIST_EQUALIZE_IN_GRAYSCALE && !theImg.isColor())
//         theImg.equalizeHistInPlace();
//     // Convert to color so we can draw color marks, etc.
//     theImg.colorImageInPlace();

//     double extra_tim_to_wait=0;

//     {	// FPS:
//         static CTicTac tictac;
//         const double T = tictac.Tac();
//         tictac.Tic();
//         const double fps = 1.0/(std::max(1e-5,T));
//         //theImg.filledRectangle(1,1,175,25,TColor(0,0,0));

//         const int current_adapt_thres = tracker->getDetectorAdaptiveThreshold();

//         theImg.selectTextFont("6x13B");
//         theImg.textOut(3,3,format("FPS: %.03f Hz", fps ),TColor(200,200,0) );
//         theImg.textOut(3,22,format("# feats: %u - Adaptive threshold: %i", (unsigned int)trackedFeats.size(), current_adapt_thres ),TColor(200,200,0) );

//         theImg.textOut(3,41,
//                        format("# raw feats: %u - Removed: %u",
//                               (unsigned int)tracker->last_execution_extra_info.raw_FAST_feats_detected,
//                               (unsigned int)tracker->last_execution_extra_info.num_deleted_feats ),
//                        TColor(200,200,0) );

//         extra_tim_to_wait = 1.0/MAX_FPS - 1.0/fps;
//     }

//     // Draw feature tracks
//     if (SHOW_FEAT_TRACKS)
//         {
//             // Update new feature coords:
//             // tracker->getProfiler().enter("drawFeatureTracks");

//             std::set<TFeatureID> observed_IDs;

//             std::cerr << trackedFeats.size() << std::endl;

//             for (size_t i=0;i<trackedFeats.size();++i)
//                 {
//                     const TSimpleFeature &ft = trackedFeats[i];
//                     std::list<TPixelCoord> & seq = feat_tracks[ft.ID];

//                     observed_IDs.insert(ft.ID);

//                     if (seq.size()>=FEATS_TRACK_LEN) seq.erase(seq.begin());
//                     seq.push_back(ft.pt);

//                     // Draw:
//                     if (seq.size()>1)
//                         {
//                             const std::list<TPixelCoord>::const_iterator it_end = seq.end();

//                             std::list<TPixelCoord>::const_iterator it      = seq.begin();
//                             std::list<TPixelCoord>::const_iterator it_prev = it++;

//                             for (;it!=it_end;++it)
//                                 {
//                                     theImg.line(it_prev->x,it_prev->y,it->x,it->y, TColor(190,190,190) );
//                                     it_prev = it;
//                                 }
//                         }
//                 }

//             // tracker->getProfiler().leave("drawFeatureTracks");

//             // Purge old data:
//             for (std::map<TFeatureID,std::list<TPixelCoord> >::iterator it=feat_tracks.begin();it!=feat_tracks.end(); )
//                 {
//                     if (observed_IDs.find(it->first)==observed_IDs.end())
//                         {
//                             std::map<TFeatureID,std::list<TPixelCoord> >::iterator next_it = it;
//                             next_it++;
//                             feat_tracks.erase(it);
//                             it = next_it;
//                         }
//                     else ++it;
//                 }
//         }

//     // Draw Tracked feats:
//     {
//         theImg.selectTextFont("5x7");
//         // tracker->getProfiler().enter("drawFeatures");
//         theImg.drawFeatures(trackedFeats, TColor(0,0,255), SHOW_FEAT_IDS, SHOW_RESPONSES);
//         // tracker->getProfiler().leave("drawFeatures");
//     }

//     IplImage* ipl_img = theImg.getAs<IplImage>();
//     cv::Mat img_mat(ipl_img);
//     opencv_utils::imshow("MRPT Tracker", img_mat);
//     // // Update window:
//     // win->get3DSceneAndLock();
//     // gl_view->setImageView(theImg);
//     // win->unlockAccess3DScene();
//     // win->repaint();


//     // if (extra_tim_to_wait>0)
//     //     mrpt::system::sleep(1000*extra_tim_to_wait);

//     step_num++;

//     return;
// }

