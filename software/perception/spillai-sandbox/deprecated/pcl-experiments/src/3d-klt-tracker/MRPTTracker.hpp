// MRPT
#include <mrpt/vision.h> 	// For feature detection, etc.
#include <mrpt/gui.h>		// For visualization windows
#include <mrpt/base.h>

// pcl includes
// #include <pcl/io/pcd_io.h>
// #include <pcl/PointIndices.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/random_sample.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
// #include <pcl/segmentation/extract_clusters.h>
//#include <pcl/features/normal_3d.h>
//#include <pcl/features/normal_3d_omp.h>

// opencv includes
//#include <opencv2/opencv.hpp>

// opencv includes
#include <opencv2/opencv.hpp>
 
// Feature types
#include "feature_types.hpp"
#include "TrackletManager.hpp"

using namespace mrpt;
using namespace mrpt::utils;
using namespace mrpt::vision;
using namespace mrpt::poses;

static void convert_to_cimg(cv::Mat& img, CImage& cimg) { 
    IplImage _ipl_img = IplImage(img); 
    IplImage* ipl_img = cvCloneImage(&_ipl_img);
    cimg.setFromIplImage(ipl_img);
    return;
}

namespace spvision { 
    // Max number of features being tracked
    const int MRPT_MAX_COUNT = 500;
    const int MRPT_MAX_FRAME_QUEUE_SIZE = 2; 
    const int MRPT_MAX_TRACK_TIMESTAMP_DELAY = 1.f; // in seconds
    // const float MRPT_MIN_FLOW_MATCH_DISTANCE = 3.f;
    // const float MRPT_MIN_DESC_MATCH_DISTANCE = 100.f;
    const float MRPT_MAX_TRACK_TIMESPAN = 2.f;

    const float MRPT_ADD_3D_POINT_DISTANCE_THRESHOLD = 0.05f; // in m
    const float MRPT_ADD_3D_POINT_DISTANCE_THRESHOLD_SQ = std::pow(MRPT_ADD_3D_POINT_DISTANCE_THRESHOLD,2);

    const float MRPT_ADD_POINT_DISTANCE_THRESHOLD = 10.f;
    const float MRPT_ADD_POINT_DISTANCE_THRESHOLD_SQ = std::pow(MRPT_ADD_POINT_DISTANCE_THRESHOLD,2);
    const float MRPT_POINT_SPACING_THRESHOLD = 20.f;

   class MRPTTracker { 
    private: 
       CGenericFeatureTrackerAutoPtr  tracker;

       // This needs to be updated if the tracks are updated via timestamps
       TSimpleFeatureList  simple_feats;
       bool VERBOSE_MODE; 

       CImage pcimg; 
       cv::Mat pimg; 
       bool _inited; 

       void convertToTracks(); 
   public: 
       // TrackMap tracklet;

       //--------------------------------------------
       // Tracklet manager
       //--------------------------------------------
       TrackManager tracklet_manager;


    public: 
       MRPTTracker();
       MRPTTracker(const MRPTTracker& tracker);
       ~MRPTTracker();       
       MRPTTracker& operator=(const MRPTTracker& tracker) { return *this; }
       bool inited() { return _inited; };

       void init(); 
       
       void reinit();

       void initTracker();

       void setParams(char* str, double value); 
       void setDefaultParams(); 

       // template <typename T>
       // normals size and tree size should be same
       void update(int64_t utime, cv::Mat& img, 
                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, 
                   pcl::PointCloud<pcl::Normal>::Ptr& normals,
                   pcl::search::KdTree<pcl::PointXYZRGB>::Ptr& tree);

       std::list<SuperFeature>
       convertToFeatures(int64_t utime, const TSimpleFeatureList& _feats, 
                         pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud,                     
                         pcl::PointCloud<pcl::Normal>::Ptr& normals, 
                         pcl::search::KdTree<pcl::PointXYZRGB>::Ptr& tree);

       void
       normalEstimation(std::vector<SuperFeature>& feats, 
                        const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, 
                        const std::vector<int>& inds);           

       void setVerbose(bool mode) { VERBOSE_MODE = mode; }
   }; 

}
