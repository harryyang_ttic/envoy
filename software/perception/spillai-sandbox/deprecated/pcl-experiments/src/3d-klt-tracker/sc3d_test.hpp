// More utilities
// #include "sc3d.h"
// #include "feature_types.h"
// #include "TrackletManager.hpp"
#include "MultiObjectPoseTracker.hpp"

namespace spvision { 

// MIN_FIT_LENGTH needs to be even
#define DEBUG_ID 2

struct state_t
{
    lcm_t *lcm;
    GMainLoop *mainloop;
    BotParam   *p_server, *dynamic_p_server;
    BotFrames *frames;
    std::map<std::string, bot_lcmgl_t*> lcmgl;

};
state_t* state = NULL;

spvision::MultiObjectPoseTracker MOPT;


}
