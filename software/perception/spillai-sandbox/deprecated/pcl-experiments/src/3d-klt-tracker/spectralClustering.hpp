/* 
 *  Software License Agreement (BSD License)
 *
 *  Copyright (c) 2012, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 *  File:    spectralClustering.hpp
 *  Author:  Hilton Bristow
 *  Created: Aug 24, 2012
 */

#ifndef SPECTRALCLUSTERING_HPP_
#define SPECTRALCLUSTERING_HPP_

#include <limits>
#include <cstdlib>
#include <algorithm>
#include <opencv2/opencv.hpp>

#define DEBUG 0
const float varTANGENT_COSINE_DISTANCE = 0.866f;

struct sp_cluster_info { 
    float score; 
    int k;
    sp_cluster_info () {}
    sp_cluster_info (const int _k, const float _score) : k(_k), score(_score) {}
};

static bool l2_pair_sort(const std::pair<int, float>& lhs, 
                         const std::pair<int, float>& rhs) { 
    return lhs.second > rhs.second;
}

struct kmeans_info { 
    int label; 
    float score;
    std::vector<int> cutInds;   
    cv::Mat_<float> center;   
    kmeans_info () : label(0), score(0) {}
    kmeans_info (int _label, const std::vector<int>& _cutInds) : label(_label), cutInds(_cutInds), score(0) {}
};

typedef std::vector<kmeans_info> cut_info;


cut_info graph_cut_energy(const cv::Mat_<int>& labels, const cv::Mat_<float>& centers) { 
    const int N = labels.rows;
    const int K = centers.rows;
    std::vector<std::vector<int> > lab_inds(K);
    for (int j=0; j<N; j++) { 
        int label = labels.at<int>(j);
        lab_inds[label].push_back(j);
    }

    cut_info info(K);
    for (int j=0; j<K; j++) { 
        int label = j;
        info[j].label = label;
        info[j].score = cv::norm(centers.row(label)) ; // info[0].score is the score for the 0th label
        info[j].cutInds = lab_inds[label];
        info[j].center = centers.row(label).clone();
    }

    return info;
}

bool sort_cut_size(const kmeans_info& lhs, 
                   const kmeans_info& rhs) { 
    /* std::cerr << "comparing " << lhs.first.score << " < " << rhs.first.score <<  " => " ; */
    /* std::cerr << lhs.first.k << " < " << rhs.first.k << std::endl; */
    return lhs.cutInds.size() < rhs.cutInds.size();
}

bool sort_cut_energy(const kmeans_info& lhs, 
                       const kmeans_info& rhs) { 
    /* std::cerr << "comparing " << lhs.first.score << " < " << rhs.first.score <<  " => " ; */
    /* std::cerr << lhs.first.k << " < " << rhs.first.k << std::endl; */
    return lhs.score > rhs.score;
}

// bool kmeans_label_sort(const kmeans_info& lhs, 
//                        const kmeans_info& rhs) { 
//     /* std::cerr << "comparing " << lhs.first.score << " < " << rhs.first.score <<  " => " ; */
//     /* std::cerr << lhs.first.k << " < " << rhs.first.k << std::endl; */
//     return lhs.score > rhs.score;
// }

// struct cluster_info { 
//     int label;
//     std::vector<int> cutInds;
//     int countp, countn;
//     float cutK, assocK, NcutK;
//     cluster_info () : label(0), countp(0), countn(0), cutK(0), assocK(0), NcutK(0) {}
//     // cluster_info (const double _energy, 
//     //               const std::vector<float> _cutK, 
//     //               const std::vector<float> _assocK, 
//     //               const std::vector<float> _NcutK) : cutK(_cutK), assocK(_assocK), NcutK(_NcutK) {}
// };

// typedef std::vector<cluster_info> cut_info;

// bool sort_cut_energy(const cluster_info& lhs, 
//                        const cluster_info& rhs) { 
//     /* std::cerr << "comparing " << lhs.first.score << " < " << rhs.first.score <<  " => " ; */
//     /* std::cerr << lhs.first.k << " < " << rhs.first.k << std::endl; */
//     return lhs.countp < rhs.countp;
// }


// struct cut_info : std::vector<cluster_info> { 
//     double energy;
//     // std::vector<std::vector<int> > cutinds;
//     // std::vector<int> countp, countn;
//     // std::vector<float> cutK, assocK, NcutK;
//     cut_info () {}
//     // cut_info (const double _energy, 
//     //           const std::vector<float> _cutK, 
//     //           const std::vector<float> _assocK, 
//     //           const std::vector<float> _NcutK) : energy(_energy), cutK(_cutK), assocK(_assocK), NcutK(_NcutK) {}
// };
// ---------------------------------------------------------------------------
// DISTANCE FUNCTIONS
// ---------------------------------------------------------------------------

/*! /class DistanceFunction
 *  /brief A functor interface which defines a distance or similarity function
 *
 *	This class provides a standard functor interface for distance and
 *	similarity functions (which are both considered to be members of
 *	comparison classes). Subclasses of this function can then be used in
 *	any function that requires as input a DistanceFunction.
 *
 *	The DistanceFunction has a single public data member, D, the dimensionality
 *	of the data, and an operator() which performs the distance computation of
 *	two input data arrays.
 *
 *	Subclasses are also encouraged to provide a static method with the signature
 *	T compute(T const * const p1, T const * const p1, const uint8_t D, ...), which
 *	constructs an instance and calls the operator() for once-off distance
 *	computations.
 *
 */
template<typename T>
class DistanceFunction {
public:
	/// the dimensionality
	const int D;
	/// constructor for initialising the data dimensionality
	DistanceFunction(const int _D) : D(_D) {
        }
	/// virtual destructor
	virtual ~DistanceFunction() {};

	/*! /brief distance operator
	 *
	 * When called, computes the distance between points p1 and p2
	 * using the implemented distance measure. This assumes that
	 * p1[D-1] and p2[D-1] point to valid memory addresses for speed
	 * since this function will often be called in a loop.
	 *
	 * @param p1 the first point
	 * @param p2 the second point
	 * @return the implemented measure of distance or similarity between
	 * p1 and p2
	 */
	virtual T operator() (T const * const p1, T const * const p2) const = 0;
};

/*! /class EuclideanEpsilonSimilarity
 *  /brief Euclidean similarity function for spectral clustering
 *
 *  This class implements the DistanceFunction interface using Euclidean
 *  distance. Since spectral clustering requires similarity functions,
 *  this uses the binary epsilon method, such that:
 *
 *  sqrt(sum( (p2 - p1)^2 )) < eps ? 1 : 0;
 */
template<typename T>
class EuclideanEpsilonSimilarity : public DistanceFunction<T> {
public:
	const T eps;
	EuclideanEpsilonSimilarity(const int _D, const T _eps) : DistanceFunction<T>(_D), eps(_eps) {}
	T operator() (T const * const p1, T const * const p2) const {
		T dist = 0;
		for (int n = 0; n < this->D; ++n) dist += pow(p2[n]-p1[n],2);
		return sqrt(dist) < eps ? 1 : 0;
	}
	static T compute(T const * const p1, T const * const p2, const int D, const T eps) {
		return EuclideanEpsilonSimilarity(D, eps).operator()(p1, p2);
	}
	static EuclideanEpsilonSimilarity TwoD(T _eps) { return EuclideanEpsilonSimilarity(2, _eps); }
	static EuclideanEpsilonSimilarity ThreeD(T _eps) { return EuclideanEpsilonSimilarity(3, _eps); }

};


/*! /class HistogramSimilarity
 *  /brief HistogramSimilarity similarity function for spectral clustering
 *
 *  This class implements the DistanceFunction interface using Euclidean
 *  distance. Since spectral clustering requires similarity functions,
 *  this uses the binary epsilon method, such that:
 *
 *  sqrt(sum( (p2 - p1)^2 )) < eps ? 1 : 0;
 */
template<typename T>
class EuclideanSimilarity : public DistanceFunction<T> {
public:
        const int method;
        EuclideanSimilarity (const int _D) : DistanceFunction<T>(_D), method(0) {}
	T operator() (T const * const p1, T const * const p2) const {
            T dist = 0;
            for (int n = 0; n < this->D; ++n) dist += pow(p2[n]-p1[n],2);
            return sqrt(dist);
	}
	static T compute(T const * const p1, T const * const p2, const int D, const T eps) {
		return EuclideanSimilarity(D, eps).operator()(p1, p2);
	}
};

template<typename T>
class HistogramSimilarity : public DistanceFunction<T> {
public:
        const int method;
        HistogramSimilarity (const int _D, const int _method) : DistanceFunction<T>(_D), method(_method) {}
	T operator() (T const * const p1, T const * const p2) const {
            cv::Mat_<T> p1_mat(1, this->D, (float*)p1, this->D); 
            cv::Mat_<T> p2_mat(1, this->D, (float*)p2, this->D); 
            T dist = cv::compareHist(p1_mat, p2_mat, this->method);
            return dist;
	}
	static T compute(T const * const p1, T const * const p2, const int D, const T eps) {
            return HistogramSimilarity(D, eps).operator()(p1, p2);
	}
};


// template<typename T>
// class CosineTangentDistanceSimilarity : public DistanceFunction<T> {
// public:
//         const int method;
//         CosineTangentDistanceSimilarity (const int _D, const int _method) : DistanceFunction<T>(_D), method(_method) {}
// 	double operator2() (T const * const p1, T const * const p2) const {
// 	}
// 	static double compute(T const * const p1, T const * const p2, const int D, const T eps) {
//             return CosineTangentDistanceSimilarity(D, eps).operator2()(p1, p2);
// 	}
// };



double gaussian(double x, double mu, double sigma) { 
    double off = x-mu;
    double val = ( 1.f / (sigma * sqrt(2 * M_PI)) ) * exp(-off*off/(2*sigma*sigma)); 
    // std::cerr << "x: " << x << " mu: " << mu << " sigma: " << sigma << " off: " << off << " val: " << val << std::endl;
    return val;
}

// ---------------------------------------------------------------------------
// SPECTRAL CLUSTERING
// ---------------------------------------------------------------------------

/*! /brief compute the pariwise similarity matrix for a set of points
 *
 * Given a set of datapoints, data, where observations are rows and variables
 * are columns, and a similarity measure implementing the DistanceFunction
 * interface, this returns the adjacency matrix for the data.
 *
 * This method is intended to construct input for the spectralClustering
 * method, and thus asserts that the distance is symmetric.
 *
 * @param data the input datapoints
 * @param adjacency the output adjacency matrix
 * @param distance the distance function
 */
template<typename T>
void adjacencyMatrix(const cv::Mat_<T>& data, cv::Mat_<T>& adjacency, DistanceFunction<T>& distance, 
                     const cv::Mat_<uchar>& mask = cv::Mat_<uchar>()) {

	// assert that the distance function is symmetric by random sampling
	unsigned int D = data.size().width;
	unsigned int N = data.size().height;
	CV_Assert(distance.D == D);
	for (unsigned int n = 0; n < std::min((unsigned int)5,N); ++n) {
		unsigned int i = rand() % N;
		unsigned int j = rand() % N;
		CV_Assert(distance(data[i], data[j]) == distance(data[j], data[i]));
	}

	// construct the upper triangular part of the adjacency matrix
	adjacency.create(N, N);
	for (unsigned int i = 0; i < N; ++i) {
		for (unsigned int j = i; j < N; ++j) {
                    float d = distance(data[i], data[j]); // gaussian(distance(data[i], data[j]), 1.f, 0.1);
                    if (mask.empty())
			adjacency(i,j) = d;
                    else  
                        adjacency(i,j) = mask(i,j) > 0 ? d : 0.f;
		}
	}
	// replicate to produce symmetry
	completeSymm(adjacency);
}

double cosine_distance(const std::vector<cv::Vec3f>& p1, const std::vector<cv::Vec3f>& p2) { 
    assert((p1.size() == p2.size()) && (p1.size() != 0));
    double dist = 0.f;
    // std::cerr << "p1.p2: " ;
    for (int n=0; n<p1.size(); ++n) { 
        // std::cerr << p2[n] << " " << p1[n] << std::endl;
        dist += (p2[n].dot(p1[n]) > varTANGENT_COSINE_DISTANCE);
        // dist += p2[n].dot(p1[n]);
    }
    // std::cerr << std::endl;
    // std::cerr << "[score: " << dist/p1.size() << "]" << std::endl;
    return dist/p1.size(); 
}

template<typename T>
void adjacencyMatrix(const std::vector<std::vector<cv::Vec3f> >& data, cv::Mat_<T>& adjacency,
                     const cv::Mat_<uchar>& mask = cv::Mat_<uchar>()) {

	// assert that the distance function is symmetric by random sampling
	// unsigned int D = data.size().width;
	unsigned int N = data.size();
	// CV_Assert(distance.D == D);
	for (unsigned int n = 0; n < std::min((unsigned int)5,N); ++n) {
		unsigned int i = rand() % N;
		unsigned int j = rand() % N;
		CV_Assert(cosine_distance(data[i], data[j]) == cosine_distance(data[j], data[i]));
	}

	// construct the upper triangular part of the adjacency matrix
	adjacency.create(N, N);
	for (unsigned int i = 0; i < N; ++i) {
		for (unsigned int j = i; j < N; ++j) {
                    // std::cerr << i << "x" << j << std::endl;
                    if (mask.empty())
			adjacency(i,j) = cosine_distance(data[i], data[j]);
                    else  
                        adjacency(i,j) = mask(i,j) > 0 ? cosine_distance(data[i], data[j]) : 0.0f;
		}
	}
	// replicate to produce symmetry
	completeSymm(adjacency);
}

// the normalization schemes available
enum {
	LAPLACIAN_UNNORMALIZED = 0,
	LAPLACIAN_SHI_MALIK    = 1,
	LAPLACIAN_NG_WEISS     = 2
};

/*! /brief compute the graph laplacian from an adjacency matrix
 *
 * Given an adjacency matrix and a normalization method, compute the
 * graph laplacian. The normalization method can be any one of:
 * 	- LAPLACIAN_UNNORMALIZED
 * 		- no normalization applied
 * 	- LAPLACIAN_SHI_MALIK
 * 		- normalization according to Shi and Malik, "Normalized Cuts
 * 		  and Image Segmentation", PAMI 2000
 * 	- LAPLACIAN_NG_WEISS
 * 		- normalization according to Ng, Jordan and Weiss,
 * 		  "On Spectral Clustering: Analysis and an Algorithm", NIPS 2002
 *
 * @param W the adjacency matrix
 * @param L the graph laplacian
 * @param normalization the normalization method
 */
template<typename T>
void graphLaplacian(const cv::Mat_<T>& W, cv::Mat_<T>& L, const int normalization) {

	// compute the degree matrix
	unsigned int N = W.rows;
	cv::Mat_<T> D;
	cv::reduce(W, D, 0, CV_REDUCE_SUM);

	// compute the laplacian
	switch (normalization) {
	case LAPLACIAN_UNNORMALIZED: {
		L = cv::Mat::diag(D) - W;
		break;
	} case LAPLACIAN_NG_WEISS: {
		cv::Mat_<T> I = cv::Mat_<T>::eye(N, N);
		cv::pow(D, -0.5, D);
		D = cv::Mat::diag(D);
		L = I - (D * W * D);
		break;
	} case LAPLACIAN_SHI_MALIK: {
		cv::Mat_<T> I = cv::Mat_<T>::eye(N, N);
		L = I - cv::Mat::diag(1/D) * W;
		break;
	} default:
		CV_Error(CV_StsBadArg, "Unknown graph laplacian normalization method supplied");
		break;
	}
}

/*! @brief perform spectral clustering
 *
 * Given the supplied adjacency matrix, the number of clusters and a graph laplacian
 * normalization method, compute the clusters on the smallest k eigenvectors of
 * the graph laplacian.
 *
 * The adjacency matrix can be computed from raw data using the adjacencyMatrix()
 * method. The current similarity measure implemented is binary epsilon similarity
 * using euclidean distance (EuclideanEpsilonDistance). If you wish to use a different
 * similarity measure, simply provide your own class derived from DistanceFunction.
 *
 * @param A the adjacency matrix (N x N)
 * @param idx the output matrix of cluster membership (N x 1)
 * @param K the number of desired clusters for the kmeans step
 * @param normalize the graph laplacian normalization method
 */
void print_mat(const char* str, const cv::Mat_<float>& mat) { 
    printf("%s:\n",str);
    for (int i=0; i<mat.rows; i++) { 
        // if (i % 5 == 0) printf("\n");
        for (int j=0; j<mat.cols; j++) { 
            // if (j % 5 == 0) printf("  ");
            printf("%3.2f; ", mat(i,j));
        }
        printf("\n");
    }
    return;
}

// template<typename T>
// cut_info graph_cut_energy(const cv::Mat_<T>& A, const int K, const cv::Mat_<int>& labels) { 

//     const int N = A.rows; 
//     std::vector<std::vector<uchar> > lab_mask(K, std::vector<uchar>(N,0));
//     std::vector<std::vector<int> > lab_inds(K);
//     for (int j=0; j<N; j++) {  
//         // specific cluster vertex inds
//         int label = labels.at<int>(j);
//         lab_mask[label][j] = 1;
//         lab_inds[label].push_back(j);
//     }    

//     double energy = 0;
    
//     cut_info info(K);
//     for (int j=0; j<K; j++) { 
//         double cutAjV_j = 0;
//         double assocAjV = 0;
//         const std::vector<uchar>& Aj = lab_mask[j];
//         for (int y=0; y<N; y++) { // for all rows (Aj)
            
//             if (Aj[y]) { // limited to set Aj
//                 // First cut count
//                 info[j].countp++;

//                 // For all cols (V)
//                 for (int x=0; x<N; x++) 
//                     assocAjV += A(y,x);

//                 // For all cols (V-Aj)
//                 for (int x=0; x<N; x++) 
//                     if (!Aj[x])
//                         cutAjV_j += A(y,x);

//             } 
//         }
        
//         info[j].label = j;
//         info[j].cutInds = lab_inds[j];

//         info[j].countn = N-info[j].countp;
//         info[j].cutK = cutAjV_j;
//         info[j].assocK = assocAjV;
        
//         double enK = cutAjV_j / assocAjV;
//         info[j].NcutK = enK;

//         energy += enK; 
//         // std::cerr << cutAjV_j << " " << assocAjV << " " << cutAjV_j / assocAjV << " " << energy << std::endl;
//     }
//     return info;
// }


/*! @brief perform spectral clustering
 *
 * Given the supplied adjacency matrix, the number of clusters and a graph laplacian
 * normalization method, compute the clusters on the smallest k eigenvectors of
 * the graph laplacian.
 *
 * The adjacency matrix can be computed from raw data using the adjacencyMatrix()
 * method. The current similarity measure implemented is binary epsilon similarity
 * using euclidean distance (EuclideanEpsilonDistance). If you wish to use a different
 * similarity measure, simply provide your own class derived from DistanceFunction.
 *
 * @param A the adjacency matrix (N x N)
 * @param idx the output matrix of cluster membership (N x 1)
 * @param K the number of desired clusters for the kmeans step
 * @param normalize the graph laplacian normalization method
 */
template<typename T>
cut_info spectralClustering(const cv::Mat_<T>& A, cv::Mat_<int>& idx, unsigned int _K, int normalize=LAPLACIAN_UNNORMALIZED) {

    // number of components (1-way or 0-way split)
    int K = _K;

    const unsigned int N = A.rows;
    // compute the Laplacian of the adjacency matrix
    cv::Mat_<T> L;
    graphLaplacian(A, L, normalize);

    // calculate the eigenvalues and eigenvectors
    cv::Mat_<T> V, D;
    cv::eigen(L, D, V);

#if DEBUG
    std::cerr << "Adjacency Matrix: " << std::endl;
    for (int i=0; i<A.rows; i++) { 
        if (i % 5 == 0) printf("\n");
        for (int j=0; j<A.cols; j++) { 
            if (j % 5 == 0) printf("  ");
            printf("%3.2f; ", A(i,j));
        }
        printf("\n");
    }
#endif
#if DEBUG
    std::cerr << "EVals: " << std::endl;
    for (int i=0; i<D.rows; i++) { 
        for (int j=0; j<D.cols; j++) { 
            printf("  ");
            printf("%6.5f; ", D(i,j));
        }
        printf("\n");
    }

    std::cerr << "EVecs: " << std::endl;
    for (int i=0; i<V.rows; i++) { 
        for (int j=0; j<V.cols; j++) { 
            printf(" ");
            printf("%3.2f; ", V(i,j));
        }
        printf("\n");
    }
#endif

#if DEBUG
    std::cerr << "====================== > K: " << K << " N: " << N << std::endl;
#endif

    if (V.rows <= N-2) { 
        std::cerr << "V: " << V << std::endl;
        return cut_info();
    }
    double score = 0.f;
    
    int n;
    for (n=0; n<N-1; n++)
        if (D(n) < 0.5f)
            break;
    
    cv::Mat_<T> centers;
    cut_info NcutInfo;

    if (n==N-1) { 
        K = 1;
        idx = cv::Mat_<int>::zeros(N, 1);
        score = 0.f; 
        n = N-2;
        std::cerr << "====================== > ONE COMP K:" << K << " N: " << N << std::endl;
    } else { 
        assert(!idx.empty());
        // this is the index that is preferred (rnn search
        // first rnn is the point itself
        idx = 1; idx.at<int>(0) = 0; 
    }
    
    // // Compute EigenVal gaps
    // cv::Mat_<T> dD(N-1,1);
    // for (int j=0; j<N-1; j++) 
    //     dD(j) = D(j) - D(j+1);
    // print_mat("dD", dD);

    //std::cerr << "L: " << std::endl;
        
    // keep only the k-principal components
    // std::cerr << "n: " << n << " k:" << N-n-1 << std::endl;
    V = V.rowRange(n, N-1);
        
    // std::cerr << "*********** K:" << K << " N:" << V.size() << std::endl;
    // if using the Ng, Jordan and Weiss laplacian, normalize the eigenvectors
    if (normalize == LAPLACIAN_NG_WEISS) {
        cv::Mat_<T> norm;
        cv::pow(V, 2, norm);
        cv::reduce(norm, norm, 0, CV_REDUCE_SUM);
        for (unsigned int k = 0; k < V.rows; ++k) V.row(k) = V.row(k) / norm;
    }

    // cluster the results
    cv::Mat_<float> Vf;
    cv::TermCriteria term(1, 100, 1e-9);
    V = V.t();
    V.convertTo(Vf, CV_32F);
#if DEBUG
    print_mat("Vf", Vf);
    // std::cerr << 1.f/D(N-2, 0) << " " << 1.f/D(N-3, 0) << std::endl;
#endif


    // Compute Mean and covar. of the eigenvector if single eigenvector
    if (K == 1 && N > 2) { 
        cv::Mat mean;
        cv::Scalar stddev;
        cv::meanStdDev(Vf, mean, stddev);
        if (stddev[0] > .1f) {// if the variance really isn't close to zero
            K = 2;
            assert(!idx.empty());
            idx = 1; idx.at<int>(0) = 0;
        }
        // std::cerr << "std_dev: " << stddev << std::endl;
    }

    // Inspect graph energies
    // Sort the cut energies in descending order
    // L2 distance of the k-means centers of the points in reduced space
    // Here the first row corresponds to the query point **
    // Hence, initialize k-means to the original query point
    score = cv::kmeans(Vf, K, idx, term, 1, cv::KMEANS_USE_INITIAL_LABELS, centers);
    NcutInfo = graph_cut_energy(idx, centers);

    // Sort based on compactness score
    std::sort(NcutInfo.begin(), NcutInfo.end(), sort_cut_energy);
    std::cerr << "SL"<<K<<"-Labels (" << score << "): " << idx << std::endl;
    for (int k=0; k<NcutInfo.size(); k++) { 
        printf("SORTED score: %4.3f;   label: %i\n", 
               NcutInfo[k].score, NcutInfo[k].label);
        // print_mat("center", NcutInfo[k].center);
    }


#if DEBUG
    // Viz adjacency matrix
    cv::Mat3b adjviz;
    cv::Mat normadj; cv::normalize(A, normadj, 0, 255, cv::NORM_MINMAX, CV_8UC1);
    cv::cvtColor(normadj, adjviz, CV_GRAY2BGR);
    cv::cvtColor(adjviz, adjviz, CV_BGR2HSV);

    std::vector<cv::Vec3b> track_colors(K);
    opencv_utils::fillColors(track_colors); 

    for (int k=0; k<NcutInfo.size(); k++) // each cluster
        for (int l=0; l<NcutInfo[k].cutInds.size(); l++)
            for (int m=0; m<5; m++)
                adjviz(NcutInfo[k].cutInds[l], std::min(NcutInfo[k].cutInds[l] + m, A.cols-1)) = track_colors[k];
    cv::cvtColor(adjviz, adjviz, CV_HSV2BGR);
        
    std::stringstream ss; ss << "Spectral Adjacency matrix call";
    opencv_utils::imshow(ss.str(), adjviz);
#endif

    // ::cerr << K << "-means Score: " << score << std::endl;
    return NcutInfo;
}

// template<typename T>
// sp_cluster_info autotunedSpectralClustering(const cv::Mat_<T>& A, cv::Mat_<int>& idx, 
//                                             const cv::Mat_<uchar>& knn_mask = cv::Mat_<uchar>(), 
//                                             int normalize=LAPLACIAN_UNNORMALIZED, bool votedTuning = true);

// template<typename T>
// sp_cluster_info autotunedSpectralClustering(const cv::Mat_<T>& A, cv::Mat_<int>& idx, 
//                                             const cv::Mat_<uchar>& knn_mask, 
//                                             int normalize, bool votedTuning) {

// 	// compute the Laplacian of the adjacency matrix
// 	cv::Mat_<T> L;
// 	graphLaplacian(A, L, normalize);

// 	// calculate the eigenvalues and eigenvectors
// 	cv::Mat_<T> V, D;
// 	cv::eigen(L, D, V);

// #if 1
//         for (int i=0; i<A.rows; i++) { 
//             if (i % 5 == 0) printf("\n");
//             for (int j=0; j<A.cols; j++) { 
//                 if (j % 5 == 0) printf("  ");
//                 printf("%3.2f; ", A(i,j));
//             }
//             printf("\n");
//         }
            
//         std::cerr << "EVals: " << std::endl;
//         for (int i=0; i<D.rows; i++) { 
//             for (int j=0; j<D.cols; j++) { 
//                 printf("  ");
//                 printf("%4.3f; ", D(i,j));
//             }
//             printf("\n");
//         }

//         std::cerr << "EVecs: " << std::endl;
//         for (int i=0; i<V.rows; i++) { 
//             for (int j=0; j<V.cols; j++) { 
//                 printf(" ");
//                 printf("%3.2f; ", V(i,j));
//             }
//             printf("\n");
//         }
// #endif

//         // autotuned spectral clustering
//         bool done = false;
//         const unsigned int N = V.rows;
//         std::vector<kmeans_info> labels_vec;
//         std::vector<cut_info> ncut_info_vec;

//         cv::Mat_<float> vote_mat = cv::Mat_<float>::zeros(N,N);
//         for (int K=1; K<3; K++) { 
//             // if (!votedTuning) { 
// #if DEBUG
//             std::cerr << "====================== > K: " << K << std::endl;
// #endif
//             // }
//             // keep only the k-principal components
//             if (N < K) continue;
//             cv::Mat_<T> kV = V.rowRange(N-K, N-1);

//             // if using the Ng, Jordan and Weiss laplacian, normalize the eigenvectors
//             if (normalize == LAPLACIAN_NG_WEISS) {
// 		cv::Mat_<T> norm;
// 		cv::pow(kV, 2, norm);
// 		cv::reduce(norm, norm, 0, CV_REDUCE_SUM);
// 		for (unsigned int k = 0; k < K; ++k) kV.row(k) = kV.row(k) / norm;
//             }

//             // cluster the results
//             cv::Mat_<float> Vf;
//             kV = kV.t();
//             kV.convertTo(Vf, CV_32F);
//             print_mat("Vf", Vf);
//             // std::cerr << "Vf: " << Vf << std::endl;

//             cv::Mat_<float> adj_mat;
//             EuclideanSimilarity<float> dist_func(Vf.cols);
//             adjacencyMatrix(Vf, adj_mat, dist_func, knn_mask);
//             cv::max(adj_mat, 0, adj_mat);

//             // k-means
//             cv::Mat_<float> centers;
//             cv::Mat_<int> km_labels(N,1);
//             cv::TermCriteria term(1, 100, 1e-9);
//             const int attempts = 5;
//             double score = cv::kmeans(Vf, K, km_labels, term, attempts, cv::KMEANS_PP_CENTERS, centers);

//             // if (!votedTuning) { 
// #if DEBUG
//             std::cerr << "KM Labels (" << score << "): " << K << " " << km_labels << std::endl;
// #endif
//             // }

// //             // EM
// //             cv::Mat_<float> LL;
// //             cv::Mat_<int> em_labels(N,1);
// //             cv::EM em_model(K, cv::EM::COV_MAT_DIAGONAL, cv::TermCriteria());
// //             em_model.train( Vf, LL, em_labels );
// //             float avgLL = cv::mean(LL)[0];
// //             double ll = -2 * avgLL;
// //             double penalty = (K+K) * std::log(N);
// //             double bic = ll + penalty;

// // #if DEBUG
// //             printf("avgLL: %4.3f, BIC: %4.3f, penalty: %4.3f\n",ll, bic, penalty);
// //             // print_mat("LL: ", LL);
// //             std::cerr << "EM Labels (avgLL: " << avgLL << "): " << K << " " << em_labels << std::endl;
// // #endif

//             // // Build k-means vote per point
//             // assert(km_labels.rows == vote_mat.rows);
//             // for (int k=0; k<vote_mat.rows; k++) 
//             //     for (int l=0; l<vote_mat.cols; l++) 
//             //         if (km_labels(k,0) == km_labels(l,0))
//             //             vote_mat(k,l) += 1.f;
            
//             // Viz adjacency matrix
//             cv::Mat3b adjviz;
//             cv::Mat normadj; cv::normalize(adj_mat, normadj, 0, 255, cv::NORM_MINMAX, CV_8UC1);
//             cv::cvtColor(normadj, adjviz, CV_GRAY2BGR);
//             cv::cvtColor(adjviz, adjviz, CV_BGR2HSV);

//             std::vector<cv::Vec3b> track_colors(K);
//             opencv_utils::fillColors(track_colors); 
//             for (int k=0; k<adj_mat.rows; k++) 
//                 for (int l=0; l<5; l++)
//                     adjviz(k, std::max(k + l, adj_mat.cols)) = track_colors[km_labels(k,0)];
//             cv::cvtColor(adjviz, adjviz, CV_HSV2BGR);
            
//             // Compute graph cut energies
//             cut_info NcutInfo = graph_cut_energy(A, K, km_labels);
//             // double bic = avgLL - 0.5 * (K + K*K/2) * std::log(N);

//             // if (!votedTuning) { 
// #if DEBUG
//             printf("NcutK: %4.3f\n",NcutInfo.energy/K);

//             for (int j=0; j<NcutInfo.NcutK.size(); j++)
//                 printf("cutK: %3.2f;   assocK: %3.2f;   enK: %3.2f;   +ve: %i;   -ve: %i\n", 
//                        NcutInfo.cutK[j], NcutInfo.assocK[j], NcutInfo.NcutK[j], NcutInfo.countp[j], NcutInfo.countn[j]);
// #endif            
//             // }

//             if (!done) { 
//                 ncut_info_vec.push_back(NcutInfo);
//                 labels_vec.push_back(kmeans_info(K, NcutInfo.energy, km_labels));
//             }
//             // // if (labels_vec.size() > 1) {
//             // //     double dEn = (labels_vec.end()[-1].score - labels_vec.end()[-2].score) / labels_vec.end()[-2].score;
//             // //     printf("dEn: %4.3f\n",dEn);
//             // //     if (dEn < 0.1f) { 
//             // //         std::cerr << "=======> DONE: K=" << K << std::endl; 
//             // //         done = true;
//             // //         // break;
//             // //     }
//             // // }
//             // if (ncut_info_vec.size() > 1 && !done) { 
//             //     double prev_min, curr_min;
//             //     std::vector<float> prev_assocK = ncut_info_vec.end()[-2].assocK;
//             //     std::vector<float> curr_assocK = ncut_info_vec.end()[-1].assocK;
//             //     cv::minMaxLoc(prev_assocK, &prev_min, 0);
//             //     cv::minMaxLoc(curr_assocK, &curr_min, 0);
//             //     if (curr_min < prev_min) { 
//             //         std::cerr << "=======> DONE: K=" << K << std::endl; 
//             //         done = true;
//             //     }
//             // }               

//             std::stringstream ss; ss << "Spectral Adjacency matrix K=" << K;
//             opencv_utils::imshow(ss.str(), adjviz);
//         }

//         int tunedK = labels_vec.end()[-1].k;
//         double tunedScore = labels_vec.end()[-1].score;

//         // if (votedTuning) { 
//         //     // k-means on the voted matrix
//         //     cv::Mat_<int> vote_labels(N,1);
//         //     // print_mat("Vote MAT", vote_mat);
//         //     for (int k=0; k<N; k++) { 
//         //         double maxVal; 
//         //         cv::minMaxLoc(vote_mat.row(k), 0, &maxVal);
//         //         cv::threshold(vote_mat, vote_mat, maxVal-0.5, maxVal, cv::THRESH_TOZERO);
//         //     }
//         //     cv::normalize(vote_mat, vote_mat, 0.f, 1.f, cv::NORM_MINMAX);
//         //     autotunedSpectralClustering(vote_mat, vote_labels, cv::Mat(), LAPLACIAN_SHI_MALIK, false);
//         //     // std::cerr << "VOTE_LABELS: " << vote_labels << std::endl;

//         //     // Viz adjacency matrix
//         //     cv::Mat3b adjviz;
//         //     cv::Mat normadj; cv::normalize(vote_mat, normadj, 0, 255, cv::NORM_MINMAX, CV_8UC1);
//         //     cv::cvtColor(normadj, adjviz, CV_GRAY2BGR);
//         //     cv::cvtColor(adjviz, adjviz, CV_BGR2HSV);

//         //     std::vector<cv::Vec3b> track_colors(tunedK);
//         //     opencv_utils::fillColors(track_colors); 
//         //     for (int k=0; k<vote_mat.rows; k++) 
//         //         for (int l=0; l<5; l++)
//         //             adjviz(k, k + l) = track_colors[vote_labels(k,0)];
//         //     cv::cvtColor(adjviz, adjviz, CV_HSV2BGR);
        
//         //     std::stringstream ss; ss << "Voted Spectral Adjacency matrix";
//         //     opencv_utils::imshow(ss.str(), adjviz);
//         // }
        
//         // assert(labels_vec.size()); // ensure labels_vec is not empty
//         idx = labels_vec.end()[-1].labels;
//         return sp_cluster_info(tunedK, tunedScore);
// }


#endif /* SPECTRALCLUSTERING_HPP_ */


        //     // Compute distortion
        //     std::map<int, std::vector<int> > lab_inds;
        //     for (int j=0; j<N; j++) {  
        //         int label = km_labels.at<int>(j);
        //         lab_inds[label].push_back(j);
        //     }
        //             // per cluster variance/mean
        //     std::map<int, cv::Mat_<float> > covs_map, inv_covs_map;
        //     std::map<int, cv::Mat_<float> > means_map, exp_map;

        //     // std::map<int, double> weights_map;
        //     double distortion = 1e9;
        //     for (std::map<int, std::vector<int> >::iterator it = lab_inds.begin(); it != lab_inds.end(); it++) { 
        //         const int label = it->first;
        //         const std::vector<int> jinds = it->second;
        //         cv::Mat_<float> samples_k(jinds.size(), Vf.cols);

        //         // std::cerr << "jinds: " << cv::Mat_<int>(jinds) << std::endl;
        //         for (int j=0; j<jinds.size(); j++) 
        //             Vf.row(jinds[j]).copyTo(samples_k.row(j));

        //         // printf("label: (%i) ",label);
        //         // print_mat("samples", samples_k);
        //         cv::Mat_<double> covar_k, inv_covar_k, mean_k;
        //         calcCovarMatrix(samples_k, covar_k, mean_k, CV_COVAR_NORMAL | CV_COVAR_SCALE | CV_COVAR_ROWS);
        //         covar_k = cv::Mat_<double>::eye(covar_k.size()) * .2f;
        //         cv::invert(covar_k, inv_covar_k);
               
        //         double dist = 0;
        //         for (int j=0; j<N; j++) { 
        //             cv::Mat_<double> v1; Vf.row(j).convertTo(v1, CV_64F);
        //             // std::cerr << "v1: " << v1 << std::endl;
        //             // std::cerr << "mean_k: " << mean_k << std::endl;
        //             double d = cv::Mahalanobis(v1, mean_k, inv_covar_k);
        //             // std::cerr << "d: " << d << std::endl;
        //             dist += d;
        //         }
        //         dist *= 1.f / N;

        //         if (dist < distortion)
        //             distortion = dist;
        //     }

        //     std::cerr << "score: " 
        //               << distortion << " " << (float)-Vf.cols/2 << " " 
        //               << std::pow(distortion, -(float)Vf.cols/2) << std::endl;
        //     double distortion2 = std::pow(distortion, -(float)Vf.cols/2);

        //     std::cerr << "KM Labels (" << score << "): " << K << " " << km_labels << std::endl;
        //     distortion_meas.push_back(kmeans_info(K, distortion2, km_labels));
        // }
        // std::sort(distortion_meas.begin(), distortion_meas.end(), kmeans_label_sort);
        // for (int j=0; j<distortion_meas.size(); j++) 
        //     std::cerr << "distortion_meas[" << j << "] " << distortion_meas[j].k << " " << distortion_meas[j].score << std::endl;


            

        // for (K=1; K<D.rows; K++)
        //     if (D(D.rows-K) > .02f) { 
        //         K--; break;
        //     }
        // if (K==0) K=1;
        // K = 4;
        // // determine max
        // cv::Point altK(1,1);
        // double max_gap = 1e6; 
        // for (K=1; K<D.rows; K++) 
        //     dD(K-1) = D(K-1) - D(K);
        // cv::minMaxLoc(dD, 0, &max_gap, 0, &altK);
        // // std::cerr << "alt K " << max_gap << " " << altK.x << " " << altK.y << std::endl;

        // bool found = false;
        // for (K=1; K<D.rows; K++) { 
        //     if (dD(K-1) > gap_thresh) { 
        //         found = true;
        //         K--;
        //         break;
        //     }
        // }        
        // if (found) { 
        //     K = dD.rows-K;
        //     std::cerr << "Found K : " << K << std::endl;
        // } else {
        //     K = dD.rows-altK.y;
        //     std::cerr << "alt K : " << K << std::endl;
        // }


            // // EM
            // cv::Mat_<int> em_labels;
            // cv::Mat_<float> LL;
            // cv::EM em_model(K, cv::EM::COV_MAT_DIAGONAL, cv::TermCriteria());
            // em_model.train( Vf, LL, em_labels );
            // // std::cerr << "EM Labels (" << score << "): " << K << " " << km_labels << std::endl;
            // // print_mat("LL: ", LL);

            // float avgLL = cv::mean(LL)[0];
            // double ll = -2 * avgLL;
            // double penalty = (K+K) * std::log(N);
            // double bic = ll + penalty;



// double compute_BIC(const cv::Mat_<float>& Vf, const int K, const cv::Mat_<int>& km_labels) { 

//     // L_ik = log(weight_k) - 0.5 * log(|det(cov_k)|) - 0.5 *(x_i - mean_k)' cov_k^(-1) (x_i - mean_k)]
//     // q = arg(max_k(L_ik))
//     // probs_ik = exp(L_ik - L_iq) / (1 + sum_j!=q (exp(L_ij - L_iq))
//     // see Alex Smola's blog http://blog.smola.org/page/2 for
//     // details on the log-sum-exp trick

//     // Based on - http://people.csail.mit.edu/fergus/research/tutorial_em_matlabcode.zip 
//     const int N = Vf.rows; 
//     assert(km_labels.rows == Vf.rows);
//     assert(K == Vf.cols+1);

//     // Compute LL
//     std::map<int, std::vector<int> > lab_inds;
//     for (int j=0; j<N; j++) {  
//         int label = km_labels.at<int>(j);
//         lab_inds[label].push_back(j);
//     }
        
//     // per cluster variance/mean
//     std::map<int, cv::Mat_<float> > covs_map, inv_covs_map;
//     std::map<int, cv::Mat_<float> > means_map, exp_map;

//     // std::map<int, double> weights_map;
//     for (std::map<int, std::vector<int> >::iterator it = lab_inds.begin(); it != lab_inds.end(); it++) { 
//         const int label = it->first;
//         const std::vector<int> jinds = it->second;
//         cv::Mat_<float> samples_k(jinds.size(), Vf.cols);

//         // std::cerr << "jinds: " << cv::Mat_<int>(jinds) << std::endl;
//         for (int j=0; j<jinds.size(); j++) 
//             Vf.row(jinds[j]).copyTo(samples_k.row(j));

//         printf("label: (%i) ",label);
//         print_mat("samples", samples_k);
//         cv::Mat covar_k, inv_covar_k, mean_k;
//         calcCovarMatrix(samples_k, covar_k, mean_k, CV_COVAR_NORMAL | CV_COVAR_SCALE | CV_COVAR_ROWS);
//         covar_k = cv::Mat_<float>::eye(covar_k.size()) * .2f;
//         cv::invert(covar_k, inv_covar_k);

//         // std::cerr << "covDET: " << cv::determinant(covar_k) << std::endl;
//         print_mat("covar_k", covar_k); 
//         print_mat("inv_covar_k", inv_covar_k); 
//         print_mat("mean_k", mean_k); 
                
//         covs_map[label] = covar_k;
//         inv_covs_map[label] = inv_covar_k;
//         means_map[label] = mean_k;

//         cv::Mat_<float> Xc = Vf - cv::repeat(means_map[label], N, 1);
//         cv::Mat_<float> Xct; cv::transpose(Xc, Xct);
//         cv::Mat_<float> exp_mat; cv::multiply(Xct, inv_covar_k * Xct, exp_mat);
//         cv::transpose(exp_mat, exp_mat); // should be [NxK] after this step

//         cv::Mat_<float> sumexp_mat(exp_mat.rows, 1);
//         for (int j=0; j<exp_mat.rows; j++) 
//             sumexp_mat.row(j) = cv::sum(exp_mat.row(j))[0];
//         exp_map[label] = sumexp_mat;
        
//         // weights_map[label] = std::log(std::max((double)1.f / K, DBL_MIN));

//         // std::cerr << "weights_map: (" << label << ") " << weights_map[label] << std::endl;
//     }

//     printf("LL:\n");
//     cv::Mat_<float> LL(N, lab_inds.size());

//     // for (int j=0; j<LL.rows; j++) { 

//     //     // const int label = km_labels.at<int>(j);
//     //     for (int k=0; k<LL.cols; k++) { 
//     //         const int label = k;
//     //         // cv::Mat_<float> Xc = Vf.row(j) - means_map[label];


//     //         // std::cerr << "det: (" << int(covs_map.find(label) != covs_map.end()) << ") " << std::endl;
//     //         double det = cv::determinant(covs_map[label]); 
//     //         // std::cerr << "det: (" << label << ") " << det << std::endl;
//     //         if (det < 1e-2) det = 1e-2;
//     //         // cv::Mat_<float> out = -0.5 * Xc * inv_covs_map[label] * Xct;
//     //         // assert(out.rows == 1 && out.cols == 1);

//     //         // LL(j,label) += out.at<float>(0); 
//     //         LL(j,label) = std::exp( -0.5 * exponent.row(j)) ;

//     //         // LL(j,label) += -0.5 * std::log(1.f / (2*M_PI) * det); 
//     //         LL(j,label) = 1/(2*M_PI) * std::sqrt(det);

//     //         // LL(j,label) += weights_map[label]; // check DBL_MIN 
//     //         LL(j,label) = 1.f / K;
//     //     }
//     //         // printf("%4.3f; %4.3f; %4.3f\n", weights_map[label], -0.5 * std::log(fabs(det)), out.at<float>(0));
//     // }

//     for (int k=0; k<LL.cols; k++) { 
//         const int label = k;
//         cv::Mat_<float> exponent;
//         cv::exp( -0.5 * exp_map[label], exponent);
//         print_mat("exp", exponent);
//         double det = cv::determinant(covs_map[label]); 
//         LL.col(label) = (1.f / K) * (1/(2*M_PI) * std::sqrt(det)) * (exponent);
//     }
//     print_mat("LL", LL);

//     // // Max LogProb
//     // cv::Mat_<float> maxLL(LL.rows, 1);
//     // for (int j=0; j<LL.rows; j++) { 
//     //     double maxv;
//     //     cv::minMaxLoc(LL.row(j), 0, &maxv);
//     //     LL.row(j) -= maxv;
//     //     maxLL.at<float>(j) = maxv;
//     // }

//     // // expLL
//     // cv::Mat_<float> expLL;
//     // cv::exp(LL, expLL);

//     // sumexpLL
//     cv::Mat_<float> sumLL(LL.rows, 1);
//     for (int j=0; j<sumLL.rows; j++) 
//         sumLL.at<float>(j) = cv::sum(LL.row(j))[0];
//     cv::Mat_<float> logsumLL; cv::log(sumLL, logsumLL);
//     float avgLL = cv::mean(logsumLL)[0];

//     double bic = avgLL - 0.5 * (K + K) * std::log(N);
//     printf("avgLL: %4.3f, BIC: %4.3f\n",avgLL, bic);
//     // std::cerr << K << "-BIC: " << bic << std::endl;
//     return bic;
// }
