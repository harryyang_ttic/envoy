#include "PosePairManager.hpp"
// 
// opencv-utils includes
#include <perception_opencv_utils/calib_utils.hpp>
#include <perception_opencv_utils/imshow_utils.hpp>
#include <perception_opencv_utils/math_utils.hpp>
#include <perception_opencv_utils/plot_utils.hpp>
#include <perception_opencv_utils/color_utils.hpp>
#include <perception_opencv_utils/imgproc_utils.hpp>

namespace spvision { 
    //============================================
    // PosePairManager class
    //============================================
    PosePairManager::PosePairManager () { 

    }

    PosePairManager::~PosePairManager () { 

    }



    //--------------------------------------------
    // Only process frames that are newer
    // Ignore frames that are old
    // Track Clean up before update
    //--------------------------------------------
    bool PosePairManager::cleanup(int64_t utime) { 
        if (!posepairs.size()) return false;

        std::vector<int64_t> erase_utimes; 
        //----------------------------------
        // If utime is older than latest of all utimes (full reset)
        //----------------------------------
        bool perform_reset = false;
        for (PosePairMapIt it = posepairs.begin(); it != posepairs.end(); it++)
            if (it->first > utime) { perform_reset = true; break; }
        if (perform_reset) { reset(); std::cerr << "RESETTING!!!!!!!!!!!!" << std::endl; return true; }
        return false; 
    }

    // Keep track of which features have pairs at time = utime
    void PosePairManager::update(int64_t utime, const UniquePoseMap& tracks_msg) { 
        if (!tracks_msg.size()) return;

        std::vector<int64_t> ids(tracks_msg.size());
        pcl::PointCloud<pcl::PointXY>::Ptr cloud (new pcl::PointCloud<pcl::PointXY>);
        cloud->points.resize(tracks_msg.size());
        cloud->width = tracks_msg.size();
        cloud->height = 1;

        // Setup cloud
        int idx = 0;
        for (UniquePoseMapConstIt it = tracks_msg.begin(); it != tracks_msg.end(); it++) { 
            const int64_t TRACK_ID = it->first; 
            const bot_core_pose_t& pose_msg = it->second;

            // Populate XY
            pcl::PointXY pt; 
            pt.x = pose_msg.pos[0], pt.y = pose_msg.pos[1]; 
            cloud->points[idx] = pt;

            ids[idx++] = TRACK_ID;
        }

        pcl::KdTreeFLANN<pcl::PointXY> kdtree;
        kdtree.setInputCloud (cloud);

        // K nearest neighbor search
        int K = 9;
        pcl::PointXYZ qpt;
        std::vector<int> inds(K);
        std::vector<float> dists(K);

        // Init pose_pair_map
        assert(posepairs.find(utime) == posepairs.end());
        posepairs[utime] = std::set<id_pair>();
        std::set<id_pair>& pairs = posepairs[utime];

        // Make pairs
        for (int j=0; j<cloud->points.size(); j++) { 
            int64_t TRACK_IDj = ids[j];
            assert(tracks_msg.find(TRACK_IDj) != tracks_msg.end()); 
            if ( kdtree.nearestKSearch (cloud->points[j], K, inds, dists) > 0 ) {
                for (int k=0; k<inds.size(); k++) { 
                    int64_t TRACK_IDk = ids[inds[k]];
                    if (inds[k] == j) continue; // not interested in the same point
                    id_pair pair = make_sorted_pair(TRACK_IDj, TRACK_IDk);
                    if (pairs.find(pair) == pairs.end()) pairs.insert(pair);
                }
            }
        }

        return;
    }

    //--------------------------------------------
    // Prune so that length is only GPFT_MAX_TRACK_TIMESPAN seconds long
    //--------------------------------------------
    void PosePairManager::prune(float TIMESPAN_S) { 
        if (!posepairs.size()) return;
        
        int64_t latest_utime = posepairs.rbegin()->first;

        //----------------------------------
        // clean up tracks that are old
        //----------------------------------
        std::cerr << "prune: bef: " << posepairs.size() << std::endl;
        std::vector<int64_t> erase_utimes;
        for (PosePairMapIt it = posepairs.begin(); it != posepairs.end(); it++) { 
            // if latest track utime lags 
            int64_t utime_th = latest_utime - TIMESPAN_S * 1e6; 
            if (it->first < utime_th) erase_utimes.push_back(it->first);
        }
        for (int j=0; j<erase_utimes.size(); j++) 
            posepairs.erase(erase_utimes[j]);
        std::cerr << "prune: aft: " << posepairs.size() << std::endl;
        return;
    }

    void PosePairManager::reset() { 
        posepairs.clear();
    }

}
