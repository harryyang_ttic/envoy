// 
// Author: Sudeep Pillai (spillai@csail.mit.edu) Mar 16, 2013
// 

#ifndef POSEPAIR_MANAGER_HPP_
#define POSEPAIR_MANAGER_HPP_

// Feature includes
#include "feature_types.hpp"
#include "lcmtypes/er_lcmtypes.h" 

// libbot/lcm includes
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>

namespace spvision { 
    class PosePairManager { 
    public: 
        //--------------------------------------------
        // PosePair map
        //--------------------------------------------
        PosePairMap posepairs;
    public: 
        PosePairManager ();
        ~PosePairManager (); 
        //void init(const int64_t TRACK_ID);
        void update(int64_t utime, const UniquePoseMap& tracks_msg);
        bool cleanup(int64_t utime);
        void prune(float TIMESPAN); 
        //void recomputeUtimeMap();
        void reset(); 
    };
}
#endif
