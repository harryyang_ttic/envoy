// 
// Author: Sudeep Pillai (spillai@csail.mit.edu) Mar 16, 2013
// 

#ifndef TRACKLET_MANAGER_HPP_
#define TRACKLET_MANAGER_HPP_

// Feature includes
#include "feature_types.hpp"
#include "lcmtypes/er_lcmtypes.h" 

// libbot/lcm includes
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>

#define DEBUG_CAPS 1

// Move these
static int STATIC_TRACKLET_ID = 1; 
static int nextTrackletID() { 
    return ++STATIC_TRACKLET_ID;
}
static int trackletID() { 
    return STATIC_TRACKLET_ID;
}

static bool reset_trackletID() { 
    STATIC_TRACKLET_ID = 1;
    return true;
}


static 
cv::Vec3f select_color(char* str) { 
    cv::Vec3f rgb;
    if (strcmp(str, "red") == 0)
        rgb[0] = .5f, rgb[1] = 0.f, rgb[2] = 0.f;
    else if (strcmp(str, "blue") == 0)
        rgb[0] = 0.f, rgb[1] = 0.f, rgb[2] = .5f;
    else if (strcmp(str, "green") == 0)
        rgb[0] = 0.f, rgb[1] = .5f, rgb[2] = 0.f;
    else if (strcmp(str, "yellow") == 0)
        rgb[0] = 0.5f, rgb[1] = .5f, rgb[2] = 0.f;
    else if (strcmp(str, "magenta") == 0)
        rgb[0] = 0.5f, rgb[1] = 0.f, rgb[2] = 0.5f;
    else if (strcmp(str, "cyan") == 0)
        rgb[0] = 0.f, rgb[1] = .5f, rgb[2] = 0.5f;
    else if (strcmp(str, "purple") == 0)
        rgb[0] = 0.25f, rgb[1] = 0.f, rgb[2] = 0.5f;
    // else if (strcmp(str, "green") == 0)
    //     rgb[0] = 0.f, rgb[1] = .5f, rgb[2] = 0.f;
    // else if (strcmp(str, "green") == 0)
    //     rgb[0] = 0.f, rgb[1] = .5f, rgb[2] = 0.f;
    else 
        rgb[0] = 0.3f, rgb[1] = 0.3f, rgb[2] = 0.3f;
    return rgb;
}

static void populate_viz_tangents(std::vector<erlcm_normal_point_t>& viz_list, 
                           pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud_refined, 
                           pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_tangents) { 

    assert(track_cloud_refined->points.size() == track_cloud_tangents->points.size());

    erlcm_normal_point_t pt; 
    for (int j=0; j<track_cloud_tangents->points.size(); j++) { 
        pt.xyz[0] = track_cloud_refined->points[j].x, 
            pt.xyz[1] = track_cloud_refined->points[j].y, 
            pt.xyz[2] = track_cloud_refined->points[j].z;
        pt.normals[0] = track_cloud_tangents->points[j].normal[0], 
            pt.normals[1] = track_cloud_tangents->points[j].normal[1], 
            pt.normals[2] = track_cloud_tangents->points[j].normal[2];
        viz_list.push_back(pt); 
    }
}


static void populate_viz_tangents(vs_point3d_list_t& viz_list,
                           pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud, 
                           pcl::PointCloud<pcl::Normal>::Ptr& track_cloud_tangents, char* str = NULL) { 
    
    assert(track_cloud->points.size() == track_cloud_tangents->points.size());

    cv::Vec3f color = select_color(str); 
    viz_list.nnormals = 0; 
    viz_list.normals = NULL; 
    viz_list.npointids = 0; 
    viz_list.pointids = NULL; 

    // This is set from 3d_klt_tracker (hack!!)
    uint64_t time_id = (uint64_t)bot_timestamp_now(); 
    viz_list.id = time_id; 
    viz_list.collection = 100; 
    viz_list.element_id = 1;

    viz_list.npoints = track_cloud->points.size() * 2;
    viz_list.points = new vs_point3d_t[track_cloud->points.size() * 2];

    viz_list.ncolors = track_cloud->points.size() * 2; 
    viz_list.colors = new vs_color_t[track_cloud->points.size() * 2];

    for (int k=0; k<track_cloud->points.size(); k++) { 
        int idx = 2*k;
        viz_list.points[idx].x = track_cloud->points[k].x;
        viz_list.points[idx].y = track_cloud->points[k].y;
        viz_list.points[idx].z = track_cloud->points[k].z;
        viz_list.colors[idx].r = color[0];
        viz_list.colors[idx].g = color[1];
        viz_list.colors[idx].b = color[2];

        viz_list.points[idx+1].x = track_cloud->points[k].x + 0.025 * track_cloud_tangents->points[k].normal[0];
        viz_list.points[idx+1].y = track_cloud->points[k].y + 0.025 * track_cloud_tangents->points[k].normal[1];
        viz_list.points[idx+1].z = track_cloud->points[k].z + 0.025 * track_cloud_tangents->points[k].normal[2];

        viz_list.colors[idx+1].r = color[0];
        viz_list.colors[idx+1].g = color[1];
        viz_list.colors[idx+1].b = color[2];
    }
    return;
}

static 
void populate_viz_cloud(vs_point3d_list_t& viz_list, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud, 
                        char* str, int64_t object_id = bot_timestamp_now()) { 

    cv::Vec3f color(0,0,0);
    if (str) color = select_color(str); 

    viz_list.nnormals = 0; 
    viz_list.normals = NULL; 
    viz_list.npointids = 0; 
    viz_list.pointids = NULL; 

    // This is set from 3d_klt_tracker (hack!!)
    viz_list.id = bot_timestamp_now(); 
    viz_list.collection = 100; 
    viz_list.element_id = 1; 

    viz_list.npoints = track_cloud->points.size();
    viz_list.points = new vs_point3d_t[track_cloud->points.size()];

    viz_list.ncolors = track_cloud->points.size(); 
    viz_list.colors = new vs_color_t[track_cloud->points.size()];

    for (int k=0; k<track_cloud->points.size(); k++) { 
        viz_list.points[k].x = track_cloud->points[k].x;
        viz_list.points[k].y = track_cloud->points[k].y;
        viz_list.points[k].z = track_cloud->points[k].z;

        if (color[0] != 0 && color[1] != 0 && color[2] != 0) { 
            viz_list.colors[k].r = color[0];
            viz_list.colors[k].g = color[1];
            viz_list.colors[k].b = color[2];
        } else { 
            viz_list.colors[k].r = track_cloud->points[k].r * 1.f / 255.f;
            viz_list.colors[k].g = track_cloud->points[k].g * 1.f / 255.f;
            viz_list.colors[k].b = track_cloud->points[k].b * 1.f / 255.f;
            // if (k < 10)
            // std::cerr << "rgb: " 
            //           << viz_list.colors[k].r << " " 
            //           << viz_list.colors[k].r << " " 
            //           << viz_list.colors[k].b << std::endl;
        }
    }
}

static 
void populate_viz_cloud(vs_point3d_list_t& viz_list, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud, 
                        cv::Vec3f& color, int64_t object_id = bot_timestamp_now()) { 
    viz_list.nnormals = 0; 
    viz_list.normals = NULL; 
    viz_list.npointids = 0; 
    viz_list.pointids = NULL; 

    // This is set from 3d_klt_tracker (hack!!)
    viz_list.id = bot_timestamp_now(); 
    viz_list.collection = 100; 
    viz_list.element_id = 1;

    viz_list.npoints = track_cloud->points.size();
    viz_list.points = new vs_point3d_t[track_cloud->points.size()];

    viz_list.ncolors = track_cloud->points.size(); 
    viz_list.colors = new vs_color_t[track_cloud->points.size()];

    for (int k=0; k<track_cloud->points.size(); k++) { 
        viz_list.points[k].x = track_cloud->points[k].x;
        viz_list.points[k].y = track_cloud->points[k].y;
        viz_list.points[k].z = track_cloud->points[k].z;
        viz_list.colors[k].r = color[0];
        viz_list.colors[k].g = color[1];
        viz_list.colors[k].b = color[2];
    }
}


namespace spvision { 

    //============================================
    // TrackManager class
    //============================================
    class TrackManager { 
    public: 
        //--------------------------------------------
        // Tracklet map
        //--------------------------------------------
        TrackMap tracklets;
        std::map<int64_t, int64_t> track_confidence; 
        std::deque<int64_t> past_utimes;        

    public: 
        TrackManager ();
        ~TrackManager (); 

        void reset(); 
        bool cleanup(int64_t utime, float MAX_TRACK_TIMESTAMP_DELAY);
        void prune(float TIMESPAN); 

        void getLatestFeatures(std::vector<SuperFeature>& p_sfpts, 
                               std::vector<cv::Point2f>& p_pts);

        // void getLatestSuperFeatures(std::vector<SuperFeature>& p_sfpts);
        // void getLatestFeatures(std::vector<cv::Point2f>& p_pts);

        void addSuperFeatures(int64_t utime, std::list<SuperFeature>& c_sfpts_list, 
                              float ADD_3D_POINT_THRESHOLD = 1.f);

        void plot(int64_t utime, const cv::Mat& img);

        inline void updateConfidence(int64_t id) { 
            if (track_confidence.find(id) == track_confidence.end())
                track_confidence[id] = 1;
            else 
                track_confidence[id]++;
        }
    };


    //============================================
    // TrackletManager class
    //============================================
    static const int MIN_FIT_LENGTH = 4;
    static const int TIME_SLICES = 20; 
    static const double TIME_WINDOW_WIDTH_SEC = 1.f;
    static const double TIME_SLICE_RATE_SEC = TIME_WINDOW_WIDTH_SEC / (TIME_SLICES-1); 

    static const int SC_AZIMUTH_BINS = 16; 
    static const int SC_ELEVATION_BINS = 16; 
    static const int SC_RADIUS_BINS = 4; 
    static const int SC_DESC_SIZE = SC_AZIMUTH_BINS * SC_ELEVATION_BINS * SC_RADIUS_BINS;
    static const int NDESC_BINS = 20;

// #if SHAPECONTEXT
    static int DESC_SIZE = SC_DESC_SIZE;
// #else
    // static int DESC_SIZE = NDESC_BINS;
// #endif

    static const int HIST_COMP_METHOD = CV_COMP_INTERSECT; // CV_COMP_CORREL; 

    class TrackletManager { 
    public: 
        //--------------------------------------------
        // Tracklet map
        //--------------------------------------------
        TrackletMap tracklets;

    public: 
        TrackletManager ();
        ~TrackletManager (); 
        void init(const int64_t TRACK_ID);
        // void update(int64_t utime, const erlcm_tracklet_list_t* tracks_msg); 
        void update(int64_t utime, const UniquePoseMap& track_features); 
        bool cleanup(int64_t utime, float MAX_TRACK_TIMESTAMP_DELAY_SEC);
        void prune(float TIMESPAN_SEC); 
        void recomputeUtimeMap();
        void reset(); 
        // void getLatestFeatures(std::vector<SuperFeature>& p_sfpts, 
        //                        std::vector<cv::Point2f>& p_pts);

        // void getLatestSuperFeatures(std::vector<SuperFeature>& p_sfpts);
        // void getLatestFeatures(std::vector<cv::Point2f>& p_pts);

        // void addSuperFeatures(int64_t utime, std::list<SuperFeature>& c_sfpts_list, 
        //                       float ADD_3D_POINT_THRESHOLD = 1.f);

        // void plot(int64_t utime, const cv::Mat& img);
    };
}
#endif
