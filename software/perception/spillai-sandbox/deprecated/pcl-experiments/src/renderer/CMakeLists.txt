find_package(PkgConfig REQUIRED)
find_package(OpenGL REQUIRED)
set(GLUT_LIBRARIES -lglut)

pkg_check_modules(BOT2_VIS bot2-vis)
if(NOT BOT2_VIS_FOUND)
    message("bot2-vis not found.  Not building libbot2 renderer")
    return()
endif(NOT BOT2_VIS_FOUND)

add_definitions(-Wall -std=gnu99)

add_library(pcl-experiments-renderer SHARED 
    pcl_experiments_renderer.cc
    )

target_link_libraries(pcl-experiments-renderer
    ${OPENGL_LIBRARIES}
    ${GTK2_LDFLAGS}
    ${LCMTYPES_LIBS})

pods_use_pkg_config_packages(pcl-experiments-renderer
    bot2-vis
    bot2-frames
    bot2-core
    kinect-utils
    lcm  lcmtypes_er-lcmtypes
    lcmtypes_kinect)

pods_install_headers(pcl_experiments_renderer.h 
    DESTINATION pcl-experiments)

pods_install_libraries(pcl-experiments-renderer)

pods_install_pkg_config_file(pcl-experiments-renderer
        CFLAGS
        LIBS -lpcl-experiments-renderer 
        REQUIRES bot2-vis bot2-frames 
        VERSION 0.0.1)


add_executable(pcl-experiments-viewer main.c)

target_link_libraries(pcl-experiments-viewer
    ${GLUT_LIBRARIES})

pods_use_pkg_config_packages(pcl-experiments-viewer
    glib-2.0
    bot2-vis
    bot2-frames
    lcm  lcmtypes_er-lcmtypes
    pcl-experiments-renderer
    )

pods_install_libraries(pcl-experiments-renderer)

pods_install_executables(pcl-experiments-viewer)
