#include "pcl_experiments_renderer.h"
#include <lcm/lcm.h>

#include <bot_core/bot_core.h>
#include <bot_vis/bot_vis.h>
#include <bot_frames/bot_frames.h>


#include <lcmtypes/erlcm_tracklet_t.h>
#include <lcmtypes/erlcm_tracklet_list_t.h>

#include <deque>
#include <iostream>
#include <limits>
#include <algorithm>
#include <map>

std::deque<bot_core_rigid_transform_t*> tf_msgs;
typedef struct _PCLExpRenderer {
    BotRenderer renderer;
    BotGtkParamWidget *pw;
    BotViewer   *viewer;
    lcm_t     *lcm;
    BotFrames *frames;
    char * pcl_frame;

    erlcm_tracklet_list_t* tracklet_msg;

} PCLExpRenderer;

static void 
on_tracklets (const lcm_recv_buf_t *rbuf, const char *channel,
              const erlcm_tracklet_list_t *tracklet_msg, void *user )
{
    PCLExpRenderer *self = (PCLExpRenderer*) user;
    if (self->tracklet_msg)
        erlcm_tracklet_list_t_destroy(self->tracklet_msg);

    self->tracklet_msg = erlcm_tracklet_list_t_copy(tracklet_msg);
    bot_viewer_request_redraw(self->viewer);
}

static void on_param_widget_changed (BotGtkParamWidget *pw, const char *name, void *user)
{
    PCLExpRenderer *self = (PCLExpRenderer*) user;

    if (!&self->renderer)
    	return;
    
    bot_viewer_request_redraw(self->viewer);
}

inline void draw_axis(float size, float opacity, float width) { 
    glLineWidth(width);

    //x-axis
    glBegin(GL_LINES);
    glColor4f(1, 0, 0, 0.5 * opacity);
    glVertex3f(size, 0, 0);
    glVertex3f(0, 0, 0);
    glEnd();

    //y-axis
    glBegin(GL_LINES);
    glColor4f(0, 1, 0, 0.5 * opacity);
    glVertex3f(0, size, 0);
    glVertex3f(0, 0, 0);
    glEnd();

    //z-axis
    glBegin(GL_LINES);
    glColor4f(0, 0, 1, 0.5 * opacity);
    glVertex3f(0, 0, size);
    glVertex3f(0, 0, 0);
    glEnd();

    return;
}

static void _draw(BotViewer *viewer, BotRenderer *renderer)
{

    PCLExpRenderer *self = (PCLExpRenderer*) renderer->user;
    if (!(self->tracklet_msg)) return;
    glPushAttrib (GL_DEPTH_BUFFER_BIT | GL_POINT_BIT | GL_CURRENT_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glPushMatrix();
    if (self->frames==NULL || !bot_frames_have_trans(self->frames, self->pcl_frame, 
                                                     bot_frames_get_root_name(self->frames))){
        assert(0);
      // rotate so that X is forward and Z is up
      glRotatef(-90, 1, 0, 0);
    }
    else{
      //project to current frame
      double sensor_to_local_m[16];
      bot_frames_get_trans_mat_4x4(self->frames, self->pcl_frame, 
                                   bot_frames_get_root_name(self->frames),
                                   sensor_to_local_m);

      // opengl expects column-major matrices
      double sensor_to_local_m_opengl[16];
      bot_matrix_transpose_4x4d(sensor_to_local_m, sensor_to_local_m_opengl);
      glMultMatrixd(sensor_to_local_m_opengl);
    }
    
    // Main draw for tracks
    float size = .02f; 
    for (int j=0; j<self->tracklet_msg->num_tracks; j++) {
        const erlcm_tracklet_t& tracklet = self->tracklet_msg->tracks[j];
        for (int k=0; k<tracklet.num_poses; k++) { 
            if (!tracklet.num_poses) continue;
            
            double feat_to_sensor_m[16];
            double feat_to_sensor_m_opengl[16];
            bot_quat_pos_to_matrix(tracklet.poses[k].orientation, tracklet.poses[k].pos, feat_to_sensor_m);
            bot_matrix_transpose_4x4d(feat_to_sensor_m, feat_to_sensor_m_opengl);

            float opacity = (k+1) * 1.f / tracklet.num_poses;

            // Push to the pose of the track (if any)
            glPushMatrix(); // feat_to_sensor
            glMultMatrixd(feat_to_sensor_m_opengl);

            // Draw the position
            glPointSize(2.f);
            glBegin(GL_POINTS);
            glColor4f(0.2, 0.2, 0.2, opacity);
            glVertex3f(0,0,0);
            glEnd();

            // Draw the last pose thicker
            // if (k == tracklet.num_poses - 1 ) 
            //     draw_axis(size, opacity, 2);
            // else 
            //     draw_axis(size, opacity, 1);
            glPopMatrix(); // feat_to_sensor
        }

        glBegin(GL_LINE_STRIP);
        glColor4f(0.1, 0.1, 0.1, 0.2);
        for (int k=0; k<tracklet.num_poses; k++) 
            glVertex3f(tracklet.poses[k].pos[0], 
                       tracklet.poses[k].pos[1], 
                       tracklet.poses[k].pos[2]);
        glEnd();
      
    }


    
    glPopMatrix(); //sensor_to_local
    glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glPopAttrib();
  return;
}

static void _free(BotRenderer *renderer)
{
    PCLExpRenderer *self = (PCLExpRenderer*) renderer;
    if (self->tracklet_msg)
        erlcm_tracklet_list_t_destroy(self->tracklet_msg);        
    if(self->pcl_frame)
        free(self->pcl_frame);
    free(self);
}

void 
pcl_experiments_add_renderer_to_viewer(BotViewer* viewer, int priority, lcm_t* lcm, BotFrames * frames, const char * pcl_frame)
{
    PCLExpRenderer *self = new PCLExpRenderer;

    self->frames = frames;
    if (self->frames!=NULL)
      self->pcl_frame = strdup(pcl_frame);

    BotRenderer *renderer = &self->renderer;

    self->lcm = lcm;
    self->viewer = viewer;
    self->pw = BOT_GTK_PARAM_WIDGET(bot_gtk_param_widget_new());

    renderer->draw = _draw;
    renderer->destroy = _free;
    renderer->name = (char*)"PCL Experiments Renderer";
    renderer->widget = GTK_WIDGET(self->pw);
    renderer->enabled = 1;
    renderer->user = self;
    self->tracklet_msg = NULL;
    // bot_gtk_param_widget_add_booleans(self->pw, 
    //                                   BOT_GTK_PARAM_WIDGET_CHECKBOX, 
    //                                   PARAM_NAME_CLOUD_SHOW, 0, NULL);

    g_signal_connect (G_OBJECT (self->pw), "changed",
                      G_CALLBACK (on_param_widget_changed), self);
    erlcm_tracklet_list_t_subscribe(self->lcm, "TRACKLETS", on_tracklets, (void*) self);
    bot_viewer_add_renderer(viewer, renderer, priority);
}
