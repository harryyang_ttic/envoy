#include <opencv2/opencv.hpp>

#include "Descriptors.h"

// pcl includes
#include <pcl/io/pcd_io.h>
#include <pcl/PointIndices.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/random_sample.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/surface/mls.h>

// opencv-utils includes
#include <perception_opencv_utils/opencv_utils.hpp>

class DenseTrajectories { 

public:
  cv::Ptr<cv::PyramidAdaptedFeatureDetector> detector;
  cv::Ptr<cv::FeatureDetector> _detector;
    // cv::FastFeatureDetector* _detector;

    IplImageWrapper image, prev_image, grey, prev_grey;
    IplImagePyramid grey_pyramid, prev_grey_pyramid, eig_pyramid;

    float* fscales; // float scale values
    int show_track; // set show_track = 1, if you want to visualize the trajectories

    // Frame no: 
    int frameNum; 
    int init_counter;
    
    // Tracks
    std::vector<std::list<Track> > xyScaleTracks;    

    // Tracker info
    TrackerInfo tracker;

    // Descriptor info
    DescInfo hogInfo, hofInfo, mbhInfo;

    // PCL normal estimation
    // pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_decimated;
    // pcl::search::KdTree<pcl::PointXYZRGB>::Ptr cloud_decimated_tree;
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals;


public: 
    DenseTrajectories(); 
    virtual ~DenseTrajectories();

    cv::Mat1b getValidNormalsMask(const cv::Mat& frame_, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud);
    void update(const cv::Mat& frame, cv::Mat& mask, cv::Mat& out); 
    void update(const cv::Mat& frame_, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, cv::Mat& out);
  // void update(const unsigned int* ubuff, const int width, const int height, int ch); 
    void write(); 
    void draw(cv::Mat& frame);

    void normal_estimation(pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, 
                           pcl::PointCloud<pcl::Normal>::Ptr& cloud_normals);

};
    
