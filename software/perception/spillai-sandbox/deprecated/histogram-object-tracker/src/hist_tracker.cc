#include "hist_tracker.h"

// Ideas : 
// - Use watershed instead of simple histogram tracking
// - The simple histogram tracking can be used as a seed for the watershed algorithm.


// For Contour matching, make sure buildContourIDMaps() is enabled, 
// detectContours2 is the main function
// waitKey(0) can be useful

using namespace cv;

Mat tmp;

Vec3b color_tab[] =
    {
        Vec3b(0, 0, 255),
        Vec3b(0,255,0),
        Vec3b(255,100,100),
        Vec3b(255,0,255),
        Vec3b(0,255,255)
    };


void simpleMatching( Ptr<DescriptorMatcher>& descriptorMatcher,
                     const Mat& query, const Mat& train,
                     vector<DMatch>& matches12 )
{
    vector<DMatch> matches;
    descriptorMatcher->match( query, train, matches12 );

}

static void crossCheckMatching( Ptr<DescriptorMatcher>& descriptorMatcher,
                         const Mat& query, const Mat& train,
                         vector<DMatch>& filteredMatches12, int knn=1 )
{
    filteredMatches12.clear();
    vector<vector<DMatch> > matches12, matches21;
    descriptorMatcher->knnMatch( query, train, matches12, knn );
    descriptorMatcher->knnMatch( train, query, matches21, knn );
    for( size_t m = 0; m < matches12.size(); m++ )
    {
        bool findCrossCheck = false;
        for( size_t fk = 0; fk < matches12[m].size(); fk++ )
        {
            DMatch forward = matches12[m][fk];

            for( size_t bk = 0; bk < matches21[forward.trainIdx].size(); bk++ )
            {
                DMatch backward = matches21[forward.trainIdx][bk];
                if( backward.trainIdx == forward.queryIdx )
                {
                    filteredMatches12.push_back(forward);
                    findCrossCheck = true;
                    break;
                }
            }
            if( findCrossCheck ) break;
        }
    }
}

HistTracker::HistTracker() {

    // Inits
    debug_mode = 1;
    mode = VISION_MODE;
    paused = false;
    backprojMode = false;
    selectObject = false;
    trackObject = 0;
    showHist = true;

    pt_msg_max = 0;

    hsize = 16;
    bin_dev = 2;
    hranges[0] = 0, hranges[1] = 180;
    phranges = hranges;

    vmin = 10, vmax = 256, smin = 30, bproj_thresh = 50;

    // Setup windows
    if (debug_mode)
        setupWindows();

    // Images
    histimg = Mat::zeros(200, 320, CV_8UC3), 
        good_histimg = Mat::zeros(200, 320, CV_8UC3), 
        live_histimg = Mat::zeros(200, 320, CV_8UC3);
    rlive_histimg = Mat::zeros(200, 320, CV_8UC3);
    llive_histimg = Mat::zeros(200, 320, CV_8UC3);
    backproj = Mat3b::zeros(HEIGHT/SCALE, WIDTH/SCALE);
    belief = Mat3b::zeros(HEIGHT/SCALE, WIDTH/SCALE);
    belief_comp = Mat3b::zeros(HEIGHT/SCALE, WIDTH/SCALE);

    // setup Hist dims
    binW = histimg.cols / hsize;
    buf = Mat(1, hsize, CV_8UC3);
    for( int i = 0; i < hsize; i++ )
        buf.at<Vec3b>(i) = Vec3b(saturate_cast<uchar>(i*180./hsize), 255, 255);
    cvtColor(buf, buf, CV_HSV2BGR);

    //////////////////////////////////////////////////////////////////
    // Feature detector inits
    std::cout << "< Creating detector, descriptor extractor and descriptor matcher ..." << std::endl;

    // _detector = FeatureDetector::create( "ORB" );
    // _descriptorExtractor = DescriptorExtractor::create( "ORB" );
    // _descriptorMatcher = DescriptorMatcher::create( "BruteForce-HammingLUT" );

    // _detector = FeatureDetector::create( "SIFT" );
    // _descriptorExtractor = DescriptorExtractor::create( "SIFT" );
    // _descriptorMatcher = DescriptorMatcher::create( "FlannBased" );

    // _detector = FeatureDetector::create( "SURF" );
    // _descriptorExtractor = DescriptorExtractor::create( "SURF" );
    // _descriptorMatcher = DescriptorMatcher::create( "FlannBased" );

    // SURF
    // int minHessian = 400;
    // _detector = SurfFeatureDetector( minHessian );

    // BRIEF
    const int DESIRED_FTRS = 500;
     _descriptorExtractor = BriefDescriptorExtractor(32);
     _detector = new GridAdaptedFeatureDetector(new FastFeatureDetector(10, true), DESIRED_FTRS, 4, 4);

    // For IDing purposes
    buildColoredIDs();
    desc_built = false;
}

HistTracker::~HistTracker() {
}

void HistTracker::useCurrentPointLoc() { 
    trackObject = -1;    
}

void HistTracker::run() { 
    frame_fullres = kinect->getRGB().clone();

    // Resize Image
    resize(frame_fullres, frame, Size(WIDTH/SCALE,HEIGHT/SCALE));

    image = frame.clone();

    if (!paused) 
        track(image);

    if( selectObject && selection.width > 0 && selection.height > 0 ) {
        Mat roi(frame_fullres, selection);
        bitwise_not(roi, roi);
    }

    // if (debug_mode)
    //     showWindows();
    return;
}

void HistTracker::updatePointingRegion(const Mat1b& limg, const Mat1b& rimg) { 
    if (!frame.data)
        return;
    
    Mat3b _pregion = frame.clone();
    if (limg.rows != image.rows) { 
        resize(limg, lselection_mask, Size(image.cols, image.rows));
        resize(rimg, rselection_mask, Size(image.cols, image.rows));
    } else { 
        lselection_mask = limg;
        rselection_mask = rimg;
    }

    Mat _hsv, _hue, _hist, rlive_hist, llive_hist;
    blur(_pregion, _pregion, Size(3,3), Point(-1,-1));
    cvtColor(_pregion, _hsv, CV_BGR2HSV);

    int ch[] = {0, 0};
    _hue.create(_hsv.size(), _hsv.depth());
    mixChannels(&_hsv, 1, &_hue, 1, ch, 1);

    llive_histimg = Scalar::all(0);
    rlive_histimg = Scalar::all(0);
    calcHist(&_hue, 1, 0, lselection_mask, llive_hist, 1, &hsize, &phranges);
    normalize(llive_hist, llive_hist, 0, 255, CV_MINMAX);

    calcHist(&_hue, 1, 0, rselection_mask, rlive_hist, 1, &hsize, &phranges);
    normalize(rlive_hist, rlive_hist, 0, 255, CV_MINMAX);

    uchar* pregion = _pregion.data;
    uchar* pimgr = rselection_mask.data;
    uchar* pimgl = lselection_mask.data;
    for (int i=0; i<lselection_mask.rows; i++, 
             pimgr+=rselection_mask.step, 
             pimgl+=lselection_mask.step, 
             pregion+=_pregion.step) { 
        for (int j=0; j<lselection_mask.cols; j++) { 
            if (pimgr[j]) // bgr
                pregion[3*j] = 0, pregion[3*j+1] = 0, pregion[3*j+2] = 155;
            if (pimgl[j]) 
                pregion[3*j] = 155, pregion[3*j+1] = 0, pregion[3*j+2] = 0;
        }
    }

    // Draw Live Histogram
    for( int i = 0; i < hsize; i++ ) {
        int lval = saturate_cast<int>(llive_hist.at<float>(i)*llive_histimg.rows/255);
        rectangle( llive_histimg, Point(i*binW,llive_histimg.rows),
                   Point((i+1)*binW,llive_histimg.rows - lval),
                   Scalar(buf.at<Vec3b>(i)), CV_FILLED, 8 );

        int rval = saturate_cast<int>(rlive_hist.at<float>(i)*rlive_histimg.rows/255);
        rectangle( rlive_histimg, Point(i*binW,rlive_histimg.rows),
                   Point((i+1)*binW,rlive_histimg.rows - rval),
                   Scalar(buf.at<Vec3b>(i)), CV_FILLED, 8 );
    }


    rectangle( _pregion, Point(pt_msg.u/SCALE-2,pt_msg.v/SCALE-2),
                   Point(pt_msg.u/SCALE+2,pt_msg.v/SCALE+2),
               Scalar(255, 255, 255), CV_FILLED, 8 );    
    imshow("Pointing Region", _pregion);

    // switch hands based on kinect perspective
    //imshow( "Left Live Histogram", rlive_histimg );
    imshow( "Live Histogram", llive_histimg );
    //imshow("Mask", rselection_mask);
    return;
}

static void onMouse( int event, int x, int y, int, void* user_data )
{
    HistTracker* hst = (HistTracker*) user_data;

    if( hst->selectObject )
    {
        hst->selection.x = MIN(x, hst->origin.x);
        hst->selection.y = MIN(y, hst->origin.y);
        hst->selection.width = std::abs(x - hst->origin.x);
        hst->selection.height = std::abs(y - hst->origin.y);

        hst->selection &= Rect(0, 0, hst->frame_fullres.cols, hst->frame_fullres.rows);
    }

    switch( event )
    {
    case CV_EVENT_LBUTTONDOWN:
        hst->origin = Point(x,y); 
        hst->selection = Rect(x,y,0,0);
        hst->selectObject = true;
        break;
    case CV_EVENT_LBUTTONUP:
        hst->selectObject = false;
        if( hst->selection.width > 0 && hst->selection.height > 0 )
            hst->trackObject = -1;
        break;
    }
    return;
}


void HistTracker::setupWindows() { 
    namedWindow( "Track" );
    namedWindow( "Good Histogram" );
    namedWindow( "Object Tracker" );
    namedWindow( "Belief" );
    namedWindow( "Live Histogram" );

#ifdef DEBUG
    //namedWindow( "belief_mask" );
#endif

    cvMoveWindow( "Object Tracker", 0, 0);
    cvMoveWindow( "Track", WIDTH/SCALE+10, 0);
    cvMoveWindow( "Belief", 0, HEIGHT/SCALE+60);
    cvMoveWindow( "Good Histogram", 0, HEIGHT/SCALE+60+200+60);
    cvMoveWindow( "Live Histogram", WIDTH/SCALE+10, HEIGHT/SCALE+60);
    cvMoveWindow( "belief_mask", WIDTH/SCALE+10, HEIGHT/SCALE+60);

    // Setup mouse callbacks
    setMouseCallback( "Object Tracker", onMouse, (void*)this );
}

void HistTracker::showWindows() { 
    if (!image.data || !belief.data)
        return;

    //imshow( "Histogram", histimg );
    //imshow( "Live Histogram", live_histimg );
    imshow( "Good Histogram", good_histimg );
    imshow( "Object Tracker", frame_fullres );
    imshow( "Belief", belief );

}


void HistTracker::track(Mat& img) { 
    // double t1 = timestamp_us();

    // Blur the image to reduce noise
    if (SCALE == 1)
        blur(img, img, Size(5,5), Point(-1,-1));
    else 
        blur(img, img, Size(3,3), Point(-1,-1));

    // Convert to HSV space
    cvtColor(img, hsv, CV_BGR2HSV);

    //int _vmin = vmin, _vmax = vmax;

    if (!mask.data) { 
        mask = Mat1b(hsv.size());
        mask = Scalar::all(255);
    }
    // inRange(hsv, Scalar(0, smin, MIN(_vmin,_vmax)),
    //         Scalar(180, 256, MAX(_vmin, _vmax)), mask);
    //imshow("mask", mask);

    // Split the channels to hue
    int ch[] = {0, 0};
    if (!hue.data)
        hue.create(hsv.size(), hsv.depth());
    mixChannels(&hsv, 1, &hue, 1, ch, 1);

    Rect scaled_selection(selection.x/SCALE, selection.y/SCALE, 
                          selection.width/SCALE, selection.height/SCALE);

    if( trackObject ) {
        if( trackObject < 0 ) {
            if( mode == VISION_MODE) { 
                Mat roi(hue, scaled_selection), maskroi(mask, scaled_selection);
                calcHist(&roi, 1, 0, maskroi, hist, 1, &hsize, &phranges);
                trackWindow = scaled_selection;
            } else if (mode == POINTING_MODE) { 
                calcHist(&hue, 1, 0, lselection_mask, hist, 1, &hsize, &phranges);
                selection = Rect(pt_msg.u/SCALE-4,pt_msg.v/SCALE-4, 8, 8);
                selection &= Rect(0, 0, img.cols, img.rows);
                trackWindow = selection;
                imshow("lselection_mask", lselection_mask);
            } else { 
                calcHist(&hue, 1, 0, selection_mask, hist, 1, &hsize, &phranges);

            }
            normalize(hist, hist, 0, 255, CV_MINMAX);
                    
            trackObject = 1;


            histimg = Scalar::all(0);
            good_histimg = Scalar::all(0);

            // Find top hist bin and only set new hist with smaller variance
            Point max_idx;
            minMaxLoc(hist, 0, 0, 0, &max_idx);
            good_hist = hist.clone();

            // Prune other colors away from max_idx
            for( int i = 0; i < hsize; i++ )
                if (abs(max_idx.y-i)>bin_dev)
                    good_hist.at<float>(i) = 0.f ;            

            // Draw Hist
            for( int i = 0; i < hsize; i++ ) {
                int val = saturate_cast<int>(hist.at<float>(i)*histimg.rows/255);
                rectangle( histimg, Point(i*binW,histimg.rows),
                           Point((i+1)*binW,histimg.rows - val), 
                           Scalar(buf.at<Vec3b>(i)), -1, 8 );
            }
            //Draw Good Hist
            for( int i = 0; i < hsize; i++ ) {
                int val = saturate_cast<int>(good_hist.at<float>(i)*good_histimg.rows/255);
                rectangle( good_histimg, Point(i*binW,good_histimg.rows),
                           Point((i+1)*binW,good_histimg.rows - val),
                           Scalar(buf.at<Vec3b>(i)), -1, 8 );
            }

        } else { 
            // calcHist(&hue, 1, 0, selection_mask, hist, 1, &hsize, &phranges);

            // //Draw Live Hist
            // for( int i = 0; i < hsize; i++ ) {
            //     int val = saturate_cast<int>(live_hist.at<float>(i)*good_histimg.rows/255);
            //     rectangle( live_histimg, Point(i*binW,live_histimg.rows),
            //                Point((i+1)*binW,live_histimg.rows - val),
            //                Scalar(buf.at<Vec3b>(i)), -1, 8 );
            // }
        }

        calcBackProject(&hue, 1, 0, good_hist, backproj, &phranges);
        backproj &= mask;

        // Threshold the back projection 
        threshold(backproj, backproj, bproj_thresh, 255, THRESH_BINARY);

        // Use this as the belief
        belief = backproj;

        if (trackWindow.height > 0 && trackWindow.width > 0) { 
            trackBox = CamShift(backproj, trackWindow,
                                TermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 10, 1 ));

            if (trackBox.size.height > 0 && trackBox.size.width > 0) { 
                ellipse( img, trackBox, Scalar(0,0,255), 3, CV_AA );
                Size2f s2(trackBox.size.width*SCALE, trackBox.size.height*SCALE);
                RotatedRect rs(trackBox.center * SCALE, s2, trackBox.angle);
                ellipse( frame_fullres, rs, Scalar(0,0,255), 3, CV_AA );
            }
        }

        if (!(trackWindow.height > 0 && trackWindow.width > 0)) { 
            int cols = backproj.cols, rows = backproj.rows, r = (MIN(cols, rows) + 5)/6;
            //std::cerr << "before: " << rows << " " << cols << " " << r << std::endl;
            // printf("before %i %i %i\n", last_tracked_pt.x, last_tracked_pt.y, r);
            int x1 = std::max(last_tracked_pt.x - r, 0);
            int y1 = std::max(last_tracked_pt.y - r, 0);
            int x2 = std::min(last_tracked_pt.x + r, cols);
            int y2 = std::min(last_tracked_pt.y + r, rows);
            trackWindow = Rect(x1, y1, x2, y2);
        }

        // // Track previous point
        // // printf("reset %i %i\n", trackWindow.x + trackWindow.width/2, trackWindow.y + trackWindow.height/2);
        // last_tracked_pt = Point(trackWindow.x + trackWindow.width/2, trackWindow.y + trackWindow.height/2);


        //if( backprojMode )
        //    cvtColor( backproj, image, CV_GRAY2BGR );

    } else if( trackObject < 0 )
        paused = false;

    // double t2 = timestamp_us();
    // printf("track: %f milliseconds\n", (t2 - t1)*1e-3);

    return;
}

static void Point3f_convert(const std::vector<Point3f>& keypoints3d, std::vector<Point3f>& points3f,
                     const vector<int>& keypointIndexes)
{
    if( keypointIndexes.empty() ) {
        points3f = keypoints3d;
    } else {
        points3f.resize( keypointIndexes.size() );
        for( size_t i = 0; i < keypointIndexes.size(); i++ ) {
            int idx = keypointIndexes[i];
            if( idx >= 0 )
                points3f[i] = keypoints3d[idx];
            else {
                //CV_Error( CV_StsBadArg, "keypointIndexes has element < 0. TODO: process this case" );
                //points3f[i] = Point3f(-1, -1);
            }
        }
    }
}

static void drawCorrespondences(const Mat& im1, 
                                const std::vector<Point2f>& kp1, 
                                const Mat& im2, 
                                const std::vector<Point2f>& kp2, 
                                std::vector<DMatch>& filteredMatches, 
                                const Mat_<uchar>& status, 
                                Mat& output) { 

    if (output.empty())
        if (im1.rows >=0 && im1.cols >=0 && im2.rows >=0 && im2.cols >=0 && im1.rows == im2.rows) { 
            output.create(im1.rows, im1.cols + im2.cols, CV_8UC3);
        }

    Mat roi(output, Rect(0,0,im1.cols,im1.rows));
    im1.copyTo(roi);
    roi = Mat(output, Rect(im1.cols,0,im1.cols,im1.rows));
    im2.copyTo(roi);

    // Outlier count
    int inliers = 0;
    uchar* pstatus = status.data;
    for (int j=0; j<kp2.size(); j++)  
        if (pstatus[j]) inliers++;


    // Image1
    for (int j=0; j<kp1.size(); j++) 
        circle(output, Point(kp1[j].x, kp1[j].y), 2, Scalar(0,255,0), CV_FILLED);

    // Display filtered points
    pstatus = status.data;
    for (int j=0; j<kp2.size(); j++)
        if (pstatus[j]) { 
            circle(output, Point(im1.cols + kp2[j].x, kp2[j].y), 
                   2, Scalar(0,255,0), CV_FILLED);
            line(output,kp1[j], Point(im1.cols + kp2[j].x, kp2[j].y) ,Scalar(255, 0, 0), 1);
        }
    
    cv::putText(output,
                cv::format("Filtered: %i/%i", inliers, kp2.size()),
                Point(10,output.rows-20), 0, 0.35, Scalar(0,255,0),1);    

    imshow("Matches" , output);
    
    return;    
}
                    
                    

bool HistTracker::detectObject(const Mat1b& _mask, 
                               std::vector<std::pair<Point3f, Point3f> >& corrs, 
                               float& median_depth) { 
    if (!desc_built) 
        return false;

    //const Mat3b& rgb_blur = kinect->getBlurredGray();
    const Mat1b& gray_blur = kinect->getBlurredGray();

    Mat1b mask;
    if (_mask.rows != gray_blur.rows || _mask.cols != gray_blur.cols)
        resize(_mask, mask, gray_blur.size());
    else
        mask = _mask;

    // Detect keypoints
    vector<KeyPoint> cur_keypoints;
    std::vector<Point3f> cur_keypoints3d;
    _detector->detect( gray_blur, cur_keypoints, mask );

    // Prune keypoints based on depth
    // Assume that an object has a specific depth,
    // and prune keypoints whose depth isn't 
    // within some threshold of the object depth
    median_depth = pruneKeyPointsWithDepth(cur_keypoints, cur_keypoints3d);

    // Purely for display purposes
    Mat output = kinect->getRGB().clone();

    // Display SURF points
    for (int j=0; j<cur_keypoints.size(); j++) 
        circle(output, Point(cur_keypoints[j].pt.x, cur_keypoints[j].pt.y), 2, 255 );

    // Compute Descriptors
    Mat cur_descriptors;
    _descriptorExtractor.compute( gray_blur, cur_keypoints, cur_descriptors );

    //printf("points: %i\n",cur_keypoints.size());

    // Match descriptors
    vector<DMatch> filteredMatches;
    _descriptorMatcher.match( cur_descriptors, _descriptors, filteredMatches );
    //simpleMatching( _descriptorMatcher, cur_descriptors, _descriptors, filteredMatches );
    //crossCheckMatching( _descriptorMatcher, cur_descriptors, _descriptors, filteredMatches );

    if (!filteredMatches.size())
        return false;


    // Get filtered results
    vector<int> queryIdxs( filteredMatches.size() ), trainIdxs( filteredMatches.size() );
    for( size_t i = 0; i < filteredMatches.size(); i++ )
    {
        queryIdxs[i] = filteredMatches[i].queryIdx;
        trainIdxs[i] = filteredMatches[i].trainIdx;
    }

    // Computing homography
    //cout << "< Computing homography (RANSAC)..." << endl;
    vector<Point2f> points1; KeyPoint::convert(_keypoints, points1, trainIdxs);
    vector<Point2f> points2; KeyPoint::convert(cur_keypoints, points2, queryIdxs);

    vector<Point3f> points1_3d; Point3f_convert(_keypoints3d, points1_3d, trainIdxs);
    vector<Point3f> points2_3d; Point3f_convert(cur_keypoints3d, points2_3d, queryIdxs);

    
    // Build Color map
    std::map<int, int> color_map;
    for (int j=0; j<points2.size(); j++)
        color_map.insert(std::make_pair(queryIdxs[j], trainIdxs[j]));

    if (points1.size() != points2.size() || points1.size() < 4 || points2.size() < 4)
        return false;

    double ransacReprojThreshold = 2;
    Mat_<uchar> status;
    Mat_<double> H = findHomography( Mat(points1), Mat(points2), status, CV_RANSAC, ransacReprojThreshold );
    //std::cerr << status.rows << " " << status.cols << std::endl;
    //print_matrix("H", H);
    //cout << ">" << endl;

    Mat img_matches;
    drawCorrespondences(tmp, points1, output, points2, filteredMatches, status, img_matches);

    // Outlier count
    int inliers = 0;
    uchar* pstatus = status.data;
    for (int j=0; j<points1_3d.size(); j++)  
        if (pstatus[j]) { 
            corrs.push_back(std::make_pair(points1_3d[j], points2_3d[j]));
            inliers++;
        }

    imshow("Track", output);

    return (inliers > 4) ;
}

static bool depth_keypoint_sort(const std::pair<float, KeyPoint>& lhs, 
                                const std::pair<float, KeyPoint>& rhs) { 
    return (lhs.first < rhs.first);
}

float HistTracker::pruneKeyPointsWithDepth(std::vector<KeyPoint>& keypoints, 
                                          std::vector<Point3f>& keypoints3d, 
                                          double threshold) { 

    if (!keypoints.size())
        return 0;

    std::vector<std::pair<float, KeyPoint> > depths;
    for (std::vector<KeyPoint>::iterator it = keypoints.begin(); 
         it!=keypoints.end(); it++) { 
        
        double depth = kinect->getDepth(it->pt.y,it->pt.x);
        depths.push_back(std::make_pair(depth, *it));
    }

    size_t n = depths.size() / 2;
    std::nth_element(depths.begin(), depths.begin() + n, depths.end(), depth_keypoint_sort);

    keypoints.clear();
    float depthm = depths[n].first;
    for (int j=0; j<depths.size(); j++)
        if (fabs(depthm-depths[j].first) < threshold) { 
            keypoints.push_back(depths[j].second);

            Vec2f dm = kinect->getDisparity(depths[j].second.pt.y, depths[j].second.pt.x);
            keypoints3d.push_back(Point3f(dm[0]*depths[j].first, dm[1]*depths[j].first,depths[j].first));
        }
    
    return depthm;
}


bool HistTracker::buildDescriptors(const Mat1b& _mask) { 
    if (_mask.empty()) { 
        std::cerr << "Mask not yet defined" << std::endl;
        return false;
    }

    // Purely for display purposes
    Mat output = kinect->getRGB().clone();

    //const Mat3b& rgb_blur = kinect->getBlurredRGB();
    const Mat1b& gray_blur = kinect->getBlurredGray();

    Mat1b mask;
    if (_mask.rows != gray_blur.rows || _mask.cols != gray_blur.cols)
        resize(_mask, mask, gray_blur.size());
    else
        mask = _mask;
    
    bitwise_and(mask, gray_blur, mask);
    imshow("bitwise mask", mask);

    // Detect keypoints
    std::cout << "===========BUILD DESC.===============" << std::endl;
    std::cout << "Extracting keypoints from image..." << std::endl;

    // SURF keypoints on the mask
    std::vector<KeyPoint> cur_keypoints;
    std::vector<Point3f> cur_keypoints3d;
    _detector->detect( gray_blur, cur_keypoints, mask );

    // Prune keypoints based on depth
    // Assume that an object has a specific depth,
    // and prune keypoints whose depth isn't 
    // within some threshold of the object depth
    pruneKeyPointsWithDepth(cur_keypoints, cur_keypoints3d);

    // Compute Descriptors
    Mat cur_descriptors;
    std::cout << "Computing descriptors for keypoints from image..." << std::endl;
    double t1 = timestamp_us();    
    _descriptorExtractor.compute( gray_blur, cur_keypoints, cur_descriptors );
    double t2 = timestamp_us();
    std::cerr << "time: " << (t2 - t1)*1e-3 << " ms" << std::endl;
    std::cout << "===========BUILD DONE.===============" << std::endl;

    if (!cur_descriptors.rows) { 
        std::cout << "Didn't add any new points" << std::endl;        
        return false;
    }
    tmp = output.clone();

    // Create full descriptors
    Mat full_descriptors(cur_descriptors.rows+_descriptors.rows, cur_descriptors.cols, cur_descriptors.type());
    size_t size = 0;
    if (!_descriptors.empty()) { 
        std::copy(_descriptors.begin<float>(), _descriptors.end<float>(), full_descriptors.begin<float>());
        size = _descriptors.end<float>() - _descriptors.begin<float>();
    }
    std::copy(cur_descriptors.begin<float>(), cur_descriptors.end<float>(), full_descriptors.begin<float>()+size);
    _descriptors = full_descriptors.clone();

    // ROI OFFSET and Display keypoints
    for (int j=0; j<cur_keypoints.size(); j++) { 
        // Draw surf features onto output
        circle(output, Point(cur_keypoints[j].pt.x, cur_keypoints[j].pt.y), 2, id_colors[j%id_colors.size()], CV_FILLED);
        //ellipse(output, Point(cur_keypoints[j].pt.x, cur_keypoints[j].pt.y),
        //      Size(cur_keypoints[j].size, 1), cur_keypoints[j].angle * 180 / PI, 0, 360, id_colors[j%18]);
    }

    // Add keypoints
    _keypoints.insert(_keypoints.end(), cur_keypoints.begin(), cur_keypoints.end());
    _keypoints3d.insert(_keypoints3d.end(), cur_keypoints3d.begin(), cur_keypoints3d.end());

    cv::putText(output,
                cv::format("Desc size:%i", full_descriptors.rows),
                Point(10,output.rows-20), 0, 0.35, Scalar(0,255,0,255),1);    


    imshow("Descriptors", output);


    desc_built = true;

    return true;
}
