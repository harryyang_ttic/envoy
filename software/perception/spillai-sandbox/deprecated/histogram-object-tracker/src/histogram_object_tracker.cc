// Ideas : 
// - Canny edge/Water shed detection on depth image
// - Use pointed region to build histogram (hit space for confirming)
// - Segment object in 3D

// ===================================================================
/*

- Use DOT (Dominant Orientation Templates for Real-Time Detection of Texture-Less Objects
- Multimodal approach - Mutimodal templates fro real-time detection of texture-less 
  objects in heavily cluttered scenes
- May be integrate a hybrid tracker on top of DOT
- i.e. DOT running in one thread (possibly using 3d data, refer to 
  www.ros.org/presentations/2010-06-holzer-dot.pdf)
- HybridTracker running on a separate thread with updates for the object
  given every few frames (updates can run at 10fps; should work fine)


http://campar.in.tum.de/Main/StefanHinterstoisser
Binary gradient grid (BiGG) - Bradski similar to DOT

http://www.ros.org/wiki/fast_template_detector
larks
*/

#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <GL/gl.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>

#include "hist_tracker.h"
#include "hybridtracker.hpp"
#include "tprintf.h"

// LCM includes
#include <kinect-skeleton-tracker/kinect_skeleton_publisher_openni.h>

#include <pcl/keypoints/sift_keypoint.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <lcmtypes/kinect_image_msg_t.h>

#include <pthread.h>
#include <bot_param/param_client.h>

using namespace cv;

Scalar color_tab_[] = {
    Scalar(0, 0, 255),
    Scalar(0,255,0),
    Scalar(255,100,100),
    Scalar(255,0,255),
    Scalar(0,255,255)
};

int img_count = 0;
struct MouseEvent {
    MouseEvent() { event = -1; buttonState = 0; }
    Point pt;
    int event;
    int buttonState;
};
MouseEvent mouse;

Rect selection;
Point origin;
bool selectObject = false;
int trackObject = 0;

static const uint8_t deci_r = 4;
static const uint16_t nvert = HEIGHT / deci_r;
static const uint16_t nhoriz = WIDTH / deci_r;
static const uint32_t npixels = WIDTH*HEIGHT;
static const uint32_t nnodes = nvert*nhoriz;

// Last Point cloud
pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud_pcl, canonical_obj_pcl;
Point3f mean_canonical_obj_pcl;
Eigen::Matrix4f updated_tf = Eigen::Matrix4f::Identity();

kinect_data kinect;
skeleton_info skel_info;

// Tracker
HistTracker tracker;
HybridTrackerParams params;

// lcmgl
bot_lcmgl_t *lcmgl_basis_tfs;

// Threads
pthread_t opencv_th, lcm_th, histtr_th, hybridtr_th, fuse_th;
pthread_barrier_t barr;

pthread_cond_t draw_cond;
pthread_mutex_t draw_mutex;

// Threading 
// lcm_th    X ----> | lcm_th ----> | lcm_th ----> | lcm_th ---->
// histtr_th X ---------------> |
// hybrid_th X -------> | 
// fuse_th   X                  | ----------> 

static void fillColors( vector<Scalar>& colors )
{
    cv::RNG rng = theRNG();

    for( size_t ci = 0; ci < colors.size(); ci++ )
        colors[ci] = Scalar( rng(256), rng(256), rng(256) );
}

static void usage(const char* progname)
{
  fprintf (stderr, "Usage: %s [options]\n"
                   "\n"
                   "Options:\n"
                   "  -l URL    Specify LCM URL\n"
                   "  -h        This help message\n", 
                   g_path_get_basename(progname));
  exit(1);
}

static bool depth_pcl_sort(const std::pair<float, pcl::PointXYZRGB>& lhs, 
                           const std::pair<float, pcl::PointXYZRGB>& rhs) { 
    return (lhs.first < rhs.first);
}

void prunePCLWithDepth(pcl::PointCloud<pcl::PointXYZRGB>::Ptr& pcl, float threshold = 0.1) { 

    if (!pcl->points.size())
        return;
    std::vector<std::pair<float, pcl::PointXYZRGB> > depths;
    for (int j=0; j<pcl->points.size(); j++) { 
        depths.push_back(std::make_pair(pcl->points[j].z, pcl->points[j]));
    }

    if (!depths.size())
        return;

    size_t n = depths.size() / 2;
    std::nth_element(depths.begin(), depths.begin() + n, depths.end(), depth_pcl_sort);

    pcl->points.clear();
    float depthm = depths[n].first;
    for (int j=0; j<depths.size(); j++)
        if (fabs(depthm-depths[j].first) < threshold)
            pcl->points.push_back(depths[j].second);

    pcl->width = pcl->points.size();
    pcl->height = 1;
    return;
}

void populatePCL(pcl::PointCloud<pcl::PointXYZRGB>::Ptr& pcl){
    pcl->width = nhoriz;
    pcl->height = nvert;
    pcl->points.resize(nvert*nhoriz);

    //bool user_label_mask = (kinect_info.user_labels_img_debug.data != 0);

    // Convert to XYZ
    int i, j, idx, f_idx, c_idx, u, v;
    // cv::Vec2f* disparity_map = (cv::Vec2f*)kinect.disparity_map.data;
    // double* depth_map = (double*)kinect.depth.data;
    unsigned char* rgbp = kinect.rgb.data;
    
    for (i = 0; i < nvert; i++) {
        v = i*deci_r;
        unsigned char* rgbpv = rgbp + v*kinect.rgb.step;
        // cv::Vec2f* dispv = disparity_map + v*kinect.disparity_map.cols;
        // double* depthv = depth_map + v*kinect.depth.cols;

        for(j = 0; j < nhoriz; j++) {
            idx = i*nhoriz + j;
            u = j*deci_r;
            //f_idx = v*WIDTH+u;

            uint8_t r, g, b;
            //c_idx = v*WIDTH*3 + u*3;
            // if (user_label_mask && kinect_info.user_labels_img.data[v*WIDTH + u]) { 
            //     r = kinect_info.user_labels_img_debug.data[c_idx + 2];
            //     g = kinect_info.user_labels_img_debug.data[c_idx + 1];
            //     b = kinect_info.user_labels_img_debug.data[c_idx + 0];
            // } else { 
            // Vec3b s = kinect.getScalarRGB(v, u);
            r = rgbpv[3*u+2]; // s[2];
            g = rgbpv[3*u+1]; // s[1];
            b = rgbpv[3*u+0]; // s[0];
                //            }
            
            Vec3f xyz = kinect.getXYZ(v,u);
            //Vec3f xyz(depthv[u] / 1000.0 /* dispv[u][0]*/, depthv[u] / 1000.0 /* dispv[u][1]*/, depthv[u] / 1000.0);
            pcl->points[idx].x = xyz[0], pcl->points[idx].y = xyz[1], pcl->points[idx].z = xyz[2];
            pcl->points[idx].r = r, pcl->points[idx].g = g, pcl->points[idx].b = b;
        }
    }

    return;
} 


void extractPCL(pcl::PointCloud<pcl::PointXYZRGB>::Ptr& object_pcl, const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& pcl, const Mat1b& _belief, float median_depth = 0) {

    if (!_belief.data || !pcl->points.size())
        return;

    Mat1b belief;
    if (belief.rows != nvert || belief.cols != nhoriz) 
        resize(_belief, belief, Size(nhoriz,nvert));
    else
        belief = _belief;

    int idx, u, v, k=0;
    const uchar* pb = belief.data;
    for (int i=0; i<nvert; i++) { 
        for (int j=0; j<nhoriz; j++) { 
            idx = i*nhoriz + j;
            if (pb[idx]) { 
                if (median_depth == 0)
                    object_pcl->points.push_back(pcl->points[idx]);
                else {  
                    if (fabs(pcl->points[idx].z-median_depth) < .1)
                        object_pcl->points.push_back(pcl->points[idx]);
                }
            }
        }
    }

    object_pcl->width = object_pcl->points.size(); 
    object_pcl->height = 1;

    return;                
}

void publishPCL(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& pcl) { 
    if (!pcl->points.size())
        return;

    kinect_point_list_t point_cloud;
    point_cloud.points = (kinect_point_t*) malloc(sizeof(kinect_point_t) * pcl->points.size());
    memset(point_cloud.points, 0, sizeof(kinect_point_t) * pcl->points.size());
    point_cloud.num_points = pcl->points.size();

    kinect_point_t* points = point_cloud.points;
    int idx=0, f_idx=0;
    for (int j=0; j<pcl->points.size(); j++) {
            points[j].x = pcl->points[j].x;
            points[j].y = pcl->points[j].y;
            points[j].z = pcl->points[j].z;
            points[j].r = pcl->points[j].r;
            points[j].g = pcl->points[j].g;
            points[j].b = pcl->points[j].b;
    }

    kinect_point_list_t_publish(kinect.lcm, "KINECT_POINT_CLOUD", &point_cloud);
    free(point_cloud.points);
}

inline Vec3b color_picker(int i) { 
    switch (i) { 
    case 0:
        return Vec3b(155, 0, 0);
    case 1:
        return Vec3b(0, 155, 0);
    case 2:
        return Vec3b(0, 0, 155);
    case 3:
        return Vec3b(155, 155, 0);
    case 4:
        return Vec3b(0, 155, 155);
    case 5:
        return Vec3b(155, 0, 155);
    default: 
        return Vec3b(155, 0, 0);
    }
}

void publishPCL(const std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr>& pcls) { 

    int num_points = 0;
    for (int i=0; i<pcls.size(); i++) 
        num_points += pcls[i]->size();

    if (!num_points)
        return;

    kinect_point_list_t point_cloud;
    point_cloud.num_points = num_points;
    point_cloud.points = (kinect_point_t*) malloc(sizeof(kinect_point_t) * num_points );
    memset(point_cloud.points, 0, sizeof(kinect_point_t) * num_points );

    kinect_point_t* points = point_cloud.points;
    for (int i=0, idx=0; i<pcls.size(); i++) { 
        const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& pcli = pcls[i];

        Vec3b c = color_picker(i);
        for (int j=0; j<pcli->points.size(); j++, idx++) {
            points[idx].x = pcli->points[j].x;
            points[idx].y = pcli->points[j].y;
            points[idx].z = pcli->points[j].z;

            points[idx].r = c[0];
            points[idx].g = c[1];
            points[idx].b = c[2];
        }
    }

    kinect_point_list_t_publish(kinect.lcm, "KINECT_POINT_CLOUD", &point_cloud);
    free(point_cloud.points);
}

#define TF_WINDOW_SIZE 10
std::deque<Eigen::Matrix4f> tf_window;

void convert_4x4_to_opengl(const Eigen::Matrix4f& tf, double* dtf) { 
    for (int i=0; i<4; i++) 
        for (int j=0; j<4; j++) { 
            dtf[j*4 + i] = tf(i,j);
            std::cerr << dtf[j*4 + i] << " ";
        }
    std::cerr << std::endl;
    return;
}

void computeTransformationBasis(Eigen::Matrix4f& tf) { 

    if (tf_window.size() > TF_WINDOW_SIZE)
        tf_window.pop_back();
    tf_window.push_front(tf);

    if (tf_window.size() < 2)
        return;
    
    const Eigen::Matrix4f& tf_final = tf_window.front();
    const Eigen::Matrix4f& tf_init = tf_window.back();
    
    bot_lcmgl_t* lcmgl = lcmgl_basis_tfs;


    lcmglEnable(GL_BLEND);
    //lcmglBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    lcmglEnable(GL_DEPTH_TEST);

    lcmglPushMatrix();
    lcmglTranslatef(0.,0, 1); // only to draw the point cloud over the grid
    lcmglScalef(-1.,1.,1.); // to mirror the image of the kinect image
    lcmglRotatef(90, 1, 0, 0);

    lcmglColor4f(.3,.3,.3,0.4f);
    lcmglLineWidth(5);
    lcmglBegin(GL_LINE_STRIP);
    for (int j=0; j<tf_window.size(); j++) {
        // center
        const Eigen::Matrix4f& tfj = tf_window[j];
        lcmglVertex3d(tfj(0,3), tfj(1,3), tfj(2,3));
    }
    lcmglEnd();  

    // x-axis
    lcmglColor4f(.9,0,0,0.4f);
    lcmglBegin(GL_LINE_STRIP);
    for (int j=0; j<tf_window.size(); j++) { 
        const Eigen::Matrix4f& tfj = tf_window[j];

        Eigen::Vector4f t;
        t << 0.1f, 0.f, 0.f, 1.f;
        Eigen::Vector4f T = tfj * t;
        lcmglVertex3d(T(0), T(1), T(2));
    }
    lcmglEnd();

    // y-axis
    lcmglColor4f(0,.9,0,0.4f);
    lcmglBegin(GL_LINE_STRIP);
    for (int j=0; j<tf_window.size(); j++) { 
        const Eigen::Matrix4f& tfj = tf_window[j];

        Eigen::Vector4f t;
        t << 0.f, 0.1f, 0.f, 1.f;
        Eigen::Vector4f T = tfj * t;
        lcmglVertex3d(T(0), T(1), T(2));
    }
    lcmglEnd();

    // z-axis
    lcmglColor4f(0,0,0.9,0.4f);
    lcmglBegin(GL_LINE_STRIP);
    for (int j=0; j<tf_window.size(); j++) { 
        const Eigen::Matrix4f& tfj = tf_window[j];

        Eigen::Vector4f t;
        t << 0.f, 0.f, 0.1f, 1.f;
        Eigen::Vector4f T = tfj * t;
        lcmglVertex3d(T(0), T(1), T(2));
    }
    lcmglEnd();

    lcmglPopMatrix();

    lcmglDisable(GL_BLEND);
    lcmglDisable(GL_DEPTH_TEST);

    bot_lcmgl_switch_buffer(lcmgl_basis_tfs);


    return;

}

void computeICP(Eigen::Matrix4f& icp_tf, Eigen::Matrix4f& guess_tf, 
                pcl::PointCloud<pcl::PointXYZRGB>::Ptr& final, 
                pcl::PointCloud<pcl::PointXYZRGB>::Ptr& live , 
                pcl::PointCloud<pcl::PointXYZRGB>::Ptr& ref) { 

    if (!ref->points.size() || !live->points.size())
        return;

    pcl::IterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;
    //icp.setMaximumIterations (10);
    icp.setTransformationEpsilon (1e-5);
    //icp.setMaxCorrespondenceDistance (0.05);
    //std::cerr << "Ref: " << ref->points.size() << std::endl;
    icp.setInputCloud(ref);
    icp.setInputTarget(live);

    double t1 = timestamp_us();
    icp.align(*final, guess_tf); 
    double t2 = timestamp_us();

    icp_tf = icp.getFinalTransformation();
    //std::cout << icp_tf << std::endl;
    //std::cout << "TF eps: " << icp.getTransformationEpsilon () << std::endl;    
    //std::cout << "fitness eps: " << icp.getEuclideanFitnessEpsilon () << std::endl;    
    //std::cout << "fitness score: " << icp.getFitnessScore () << std::endl;    

    if (!icp.hasConverged())
        return;

    // Update step
    guess_tf = icp_tf;

    Eigen::Matrix4f tf_g_to_can = Eigen::Matrix4f::Identity();
    tf_g_to_can(0,3) = mean_canonical_obj_pcl.x;
    tf_g_to_can(1,3) = mean_canonical_obj_pcl.y;
    tf_g_to_can(2,3) = mean_canonical_obj_pcl.z;

    Eigen::Matrix4f tf_g_to_live = icp_tf * tf_g_to_can;

#define TRANSFORMATION_BASIS 0
#if TRANSFORMATION_BASIS
    computeTransformationBasis(tf_g_to_live);
#endif

    double rot[9];
    rot[0] = tf_g_to_live(0,0), rot[1] = tf_g_to_live(0,1), rot[2] = tf_g_to_live(0,2), 
        rot[3] = tf_g_to_live(1,0), rot[4] = tf_g_to_live(1,1), rot[5] = tf_g_to_live(1,2), 
        rot[6] = tf_g_to_live(2,0), rot[7] = tf_g_to_live(2,1), rot[8] = tf_g_to_live(2,2);

    bot_core_rigid_transform_t tf;
    bot_matrix_to_quat(rot, tf.quat);
    tf.utime = kinect.timestamp;
    tf.trans[0] = tf_g_to_live(0,3);
    tf.trans[1] = tf_g_to_live(1,3);
    tf.trans[2] = tf_g_to_live(2,3);
    bot_core_rigid_transform_t_publish (kinect.lcm, "GLOBAL_TO_LOCAL", &tf);

    printf("icp: %f milliseconds\n", (t2 - t1)*1e-3);



    return;

}

inline float l2_dist(const pcl::PointXYZ& p1, const pcl::PointXYZ& p2) { 
    return sqrt( (p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y) + (p1.z-p2.z)*(p1.z-p2.z) );
}

static bool sort_source_depth(const std::pair<Point3f, Point3f>& lhs, 
                              const std::pair<Point3f, Point3f>& rhs) { 
    return (lhs.first.z < rhs.first.z);
}

void transformationEstimation(std::vector<std::pair<Point3f, Point3f> >& corrs) { 
    if (!corrs.size()) { 
        //std::cerr << " No Corrs: " << std::endl;
        return;
    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr source (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr target (new pcl::PointCloud<pcl::PointXYZ>);

    boost::shared_ptr<pcl::Correspondences> correspondences (new pcl::Correspondences);
    //boost::shared_ptr<pcl::Correspondences> correspondences_result_rej_sac (new pcl::Correspondences);

    // Sort correspondences based on their depth
    // Prune out ones that are not enclosed within the object
    // Assuming the features found on the object have the same depth
    std::sort(corrs.begin(), corrs.end(), sort_source_depth);

    size_t n = corrs.size() / 2;
    std::nth_element(corrs.begin(), corrs.begin() + n, corrs.end(), sort_source_depth);

    int idx = 0;
    float mpz = corrs[n].first.z;
    float cx = 0, cy = 0, cz = 0;
    for (std::vector<std::pair<Point3f,Point3f> >::iterator it = corrs.begin(); 
         it!=corrs.end(); ) { 
        if (fabs(mpz-it->first.z) > 0.1) // 10 cm
            it = corrs.erase(it);
        else { 
            cx += it->first.x, cy += it->first.y, cz += it->first.z;
            
            pcl::PointXYZ ps(it->first.x, it->first.y, it->first.z);
            pcl::PointXYZ pt(it->second.x, it->second.y, it->second.z);
            
            source->points.push_back(ps);
            target->points.push_back(pt);

            correspondences->push_back(pcl::Correspondence(idx, idx, l2_dist(ps, pt)));        
            it++; idx++;
        }
    }

    if (corrs.size() < 4) { 
        std::cerr << " not enough 3d corrs" << std::endl;
        return;
    }
    cx /= corrs.size(), cy /= corrs.size(), cz /= corrs.size();

    source->width = corrs.size(); 
    source->height = 1; 

    target->width = corrs.size(); 
    target->height = 1; 

    // pcl::registration::CorrespondenceRejectorSampleConsensus<pcl::PointXYZ> corr_rej_sac;
    // corr_rej_sac.setInputCloud (source);
    // corr_rej_sac.setTargetCloud (target);
    // corr_rej_sac.setInlierThreshold (.1);
    // corr_rej_sac.setMaxIterations (20);
    // corr_rej_sac.setInputCorrespondences (correspondences);
    // corr_rej_sac.getCorrespondences (*correspondences_result_rej_sac);
    // Eigen::Matrix4f transform_res_from_SAC = corr_rej_sac.getBestTransformation ();


    Eigen::Matrix4f transform_res_from_SVD;
    pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> trans_est_svd;
    trans_est_svd.estimateRigidTransformation(*source, *target,
                                              *correspondences,
                                              transform_res_from_SVD);

    Eigen::Matrix4f tf_g_to_can = Eigen::Matrix4f::Identity();
    tf_g_to_can(0,3) = cx;
    tf_g_to_can(1,3) = cy;
    tf_g_to_can(2,3) = cz;

    Eigen::Matrix4f tf_g_to_live = transform_res_from_SVD * tf_g_to_can;

    //std::cerr << tf_g_to_live << std::endl;
    double rot[9];
    rot[0] = tf_g_to_live(0,0), rot[1] = tf_g_to_live(0,1), rot[2] = tf_g_to_live(0,2), 
        rot[3] = tf_g_to_live(1,0), rot[4] = tf_g_to_live(1,1), rot[5] = tf_g_to_live(1,2), 
        rot[6] = tf_g_to_live(2,0), rot[7] = tf_g_to_live(2,1), rot[8] = tf_g_to_live(2,2);

    bot_core_rigid_transform_t tf;
#if 1
    bot_matrix_to_quat(rot, tf.quat);
#else
    tf.quat[0] = 1;
    tf.quat[1] = 0; 
    tf.quat[2] = 0; 
    tf.quat[3] = 0;
#endif
    tf.utime = kinect.timestamp;
    tf.trans[0] = tf_g_to_live(0,3);
    tf.trans[1] = tf_g_to_live(1,3);
    tf.trans[2] = tf_g_to_live(2,3);
 
    bot_core_rigid_transform_t_publish (kinect.lcm, "GLOBAL_TO_LOCAL", &tf);

    // std::cerr << "RigidT: " << std::endl;
    // std::cout << transform_res_from_SVD << std::endl;
    return;

}

void compute(const Mat1b& belief) { 
    if (!tracker.running())
        return;

    float median_depth = 0;
    std::vector<std::pair<Point3f, Point3f> > corrs;
    tracker.detectObject(belief, corrs, median_depth);
    transformationEstimation(corrs);

    // ICP
    // Convert kinect msg to pcl point cloud
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr object_pcl(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr final_pcl(new pcl::PointCloud<pcl::PointXYZRGB>);
    extractPCL(object_pcl, point_cloud_pcl, belief, median_depth);

    Eigen::Matrix4f icp_tf;
    computeICP(icp_tf, updated_tf, final_pcl, object_pcl, canonical_obj_pcl);

    publishPCL(final_pcl);

    return;
}

void buildSkeletonMap(const kinect_skeleton_msg_t *skeleton_msg) { 

    skel_info.joint_p.clear();
    for (int i=0; i<skeleton_msg->num_links; i++) { 
        const kinect_link_msg_t& link = skeleton_msg->links[i];
        skel_info.joint_p.insert(std::make_pair(link.joint_id, 
                                                std::make_pair(Point3f(link.source.x, 
                                                                       link.source.y, 
                                                                       link.source.z), 
                                                               Point3f(link.dest.x, 
                                                                       link.dest.y, 
                                                                       link.dest.z))));
    }
    return;
}

void computePointingVectors() { 

    skel_info.le = skel_info.joint_p[XN_SKEL_LEFT_HAND].first;
    skel_info.lh = skel_info.joint_p[XN_SKEL_LEFT_HAND].second;
    skel_info.lvec = cv::Point3f(skel_info.lh.x-skel_info.le.x, 
                                 skel_info.lh.y-skel_info.le.y, 
                                 skel_info.lh.z-skel_info.le.z);
    float norm = sqrt(skel_info.lvec.x * skel_info.lvec.x + 
                      skel_info.lvec.y * skel_info.lvec.y + 
                      skel_info.lvec.z * skel_info.lvec.z);
    skel_info.lvec.x /= norm, skel_info.lvec.y /= norm, skel_info.lvec.z /= norm;

    skel_info.re = skel_info.joint_p[XN_SKEL_RIGHT_HAND].first;
    skel_info.rh = skel_info.joint_p[XN_SKEL_RIGHT_HAND].second;
    skel_info.rvec = cv::Point3f(skel_info.rh.x-skel_info.re.x, 
                                 skel_info.rh.y-skel_info.re.y, 
                                 skel_info.rh.z-skel_info.re.z);
    norm = sqrt(skel_info.rvec.x * skel_info.rvec.x + 
                skel_info.rvec.y * skel_info.rvec.y + 
                skel_info.rvec.z * skel_info.rvec.z);
    skel_info.rvec.x /= norm, skel_info.rvec.y /= norm, skel_info.rvec.z /= norm;

    return;
}

void trackPointingRegion() { 

    if (!point_cloud_pcl->points.size())
        return;

    float x, y, z;
    Mat1b maskr = Mat1b::zeros(nvert, nhoriz);
    Mat1b maskl = maskr.clone();

    uchar* pmaskr = maskr.data;
    uchar* pmaskl = maskl.data;

    tracker.pt_msg_max = 0;

    int minx = WIDTH, miny = HEIGHT, maxx = 0, maxy = 0;
    int idx, u, v, f_idx;
    int r, g, b;
    for (int i=0; i<nvert; i++) { 
        for (int j=0; j<nhoriz; j++) { 
            idx = i*nhoriz + j;
            
            x = point_cloud_pcl->points[idx].x;
            y = point_cloud_pcl->points[idx].y;
            z = point_cloud_pcl->points[idx].z;

            if (z == 0) continue;

            r = point_cloud_pcl->points[idx].r;
            g = point_cloud_pcl->points[idx].g;
            b = point_cloud_pcl->points[idx].b;

            // Check if point is within pointed region
            float rdx = x - skel_info.rh.x, rdy = y - skel_info.rh.y, rdz = z - skel_info.rh.z;
            float rn =  sqrt(rdx * rdx + rdy * rdy + rdz * rdz); 
            float rrad = (rdx / rn) * skel_info.rvec.x + (rdy / rn) * skel_info.rvec.y + (rdz / rn) * skel_info.rvec.z;

            float ldx = x - skel_info.lh.x, ldy = y - skel_info.lh.y, ldz = z - skel_info.lh.z;
            float ln =  sqrt(ldx * ldx + ldy * ldy + ldz * ldz); 
            float lrad = (ldx / ln) * skel_info.lvec.x + (ldy / ln) * skel_info.lvec.y + (ldz / ln) * skel_info.lvec.z;             

            if (rrad > 0.96 && fabs(rdz) < .1) { // 10 deg cone (red)
                // point_cloud_pcl->points[idx].r = 155;
                // point_cloud_pcl->points[idx].g = 0;
                // point_cloud_pcl->points[idx].b = 0;

                // pmaskr[idx] = 255;
            }
            
            if (lrad > 0.866) { 
                if (lrad > 0.96/* && fabs(ldz) < .1*/) { // 10 deg cone (blue)
                    // point_cloud_pcl->points[idx].r = 0;
                    // point_cloud_pcl->points[idx].g = 0;
                    // point_cloud_pcl->points[idx].b = 155;

                    minx = std::min(minx, int(x)); 
                    miny = std::min(miny, int(y)); 
                    maxx = std::max(maxx, int(x)); 
                    maxy = std::max(maxy, int(y)); 

                    pmaskl[idx] = 255;
                    
                    if (lrad > tracker.pt_msg_max) { 
                        tracker.pt_msg.u = j * deci_r;                
                        tracker.pt_msg.v = i * deci_r;
                        tracker.pt_msg_max = lrad;
                    }                        

                } else { 
                    pmaskr[idx] = 255;
                }
            }
        }
    }
    
    tracker.updatePointingRegion(maskl, maskr);
    return;
}


static bool contours_sort(const std::vector<Point>& lhs, const std::vector<Point>& rhs) { 
    return lhs.size() > rhs.size();
}

inline void fill_mask(Mat1b& mask, const RotatedRect& rr) { 
    mask = 0;
    if (rr.size.height >=0 && rr.size.width >=0)
        ellipse( mask, rr, 255, CV_FILLED );
    return;
}


static void 
on_skeleton_frame (const lcm_recv_buf_t *rbuf, const char *channel,
		   const kinect_skeleton_msg_t *skeleton_msg, void *user_data )
{
    if(kinect.skeleton_msg)
        kinect_skeleton_msg_t_destroy(kinect.skeleton_msg);
    kinect.skeleton_msg = kinect_skeleton_msg_t_copy(skeleton_msg);

    //tracker.drawSkeleton(kinect_info);

    // Map for skeleton joints - skel_info
    buildSkeletonMap(skeleton_msg);

    // Left and right pointing vector - skel_info.le, lh, lvec, re, rh, rvec    
    computePointingVectors();

    // Use the above information to find points within a cone of 10 deg
    // Also colors the point cloud
    trackPointingRegion();

    return;
}

static void
on_segmented_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                    const bot_core_image_t *seg_msg, void *user_data ) { 

    // Segmented image
    Mat3b seg = Mat(seg_msg->height, seg_msg->width, CV_8UC3);
    memcpy(seg.data, seg_msg->data, seg_msg->width * seg_msg->height * 3);

    // Grayscale segmented image (remove and fix the above memcpy to do this in one pass)
    Mat1b segu; 
    seg.convertTo(segu, CV_8UC1);

    // Get the binary belief image and resize it to the segmented image
    Mat1b belief = tracker.belief;
    if (seg.rows != belief.rows || seg.cols != belief.cols) 
        resize(belief, belief, Size(seg.cols,seg.rows));

    // Blob segment
    RNG rng(12345);
    vector < vector<Point> > contours;
    vector<Vec4i> hierarchy;
    findContours( belief, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
    // findContours( segu, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

    // Mat1b mask = Mat1b::zeros(seg.size());
    // for( int i = 0; i<contours.size() && i<2; i++ ) {
    //     Scalar gscale = Scalar( rng.uniform(0, 255) );
    //     drawContours( mask, contours, i, gscale, CV_FILLED, 8, hierarchy, 0, Point() );
    // }
    
    // std::vector<int> votes(contours.size(), 0);
    // for (int y=0; y<segu.rows; y++) { 
    //     for (int x=0; x<segu.cols; x++) { 
    //         if (!belief(y,x))
    //             continue;
            
    //         for (int j=0; j<contours.size(); j++) 
    //             if (pointPolygonTest(contours[j], Point(x,y), false) > 0) { 
    //                 votes[j]++;
    //                 break;
    //             }
    
    //     }
    // }

    // Descending sort blob
    std::sort(contours.begin(), contours.end(), contours_sort);

    // Get the moments
    vector<Moments> mu(contours.size() );
    for( int i = 0; i < contours.size(); i++ )
        mu[i] = moments( contours[i], false );

    //  Get the mass centers:
    vector<Point2f> mc( contours.size() );
    for( int i = 0; i < contours.size(); i++ )
        mc[i] = Point2f( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00 );

    Mat1b mask = Mat1b::zeros(seg.size());
    for( int i = 0; i<contours.size() && i<2; i++ ) {
        // if (i > 1 && contours[i].size() * 1.f / contours[i-1].size() < .7f) 
        //     break;

        //Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
        // drawContours( seg, contours, i, color, CV_FILLED, 8, hierarchy, 0, Point() );

        //Scalar gscale = Scalar( rng.uniform(0, 255) );
        // drawContours( mask, contours, i, gscale, CV_FILLED, 8, hierarchy, 0, Point() );

        Vec3b c = seg(mc[i].y, mc[i].x);
            
        for (int y=0; y<seg.rows; y++) 
            for (int x=0; x<seg.cols; x++) 
                if (seg(y,x)[0] == c[0] && seg(y,x)[1] == c[1] && seg(y,x)[2] == c[2])
                    mask(y,x) = 1;

        floodFill(mask, Point(mc[i].x,mc[i].y), 255);
        threshold(mask, mask, 2, 255, THRESH_BINARY);

        break;
    }

    // // Resize to the right scale
    // resize(mask, tracker.belief_comp, Size(nhoriz, nvert));
    tracker.belief_comp = mask;

    compute(tracker.belief_comp);

    imshow("M1 + M2", mask);
    imshow("Segmented Image", seg);

    return;
}

int envoy_decompress_bot_core_image_t(const bot_core_image_t * jpeg_image, bot_core_image_t *uncomp_image)
{
  //copy metadata
  uncomp_image->utime = jpeg_image->utime;
  if (uncomp_image->metadata != NULL)
    bot_core_image_metadata_t_destroy(uncomp_image->metadata);
  uncomp_image->nmetadata = jpeg_image->nmetadata;
  if (jpeg_image->nmetadata > 0) {
    uncomp_image->metadata = bot_core_image_metadata_t_copy(jpeg_image->metadata);
  }
  
  uncomp_image->width = jpeg_image->width;
  uncomp_image->height = jpeg_image->height;
  
  uncomp_image->row_stride = jpeg_image->width * 3;//jpeg_image->row_stride;

  int depth=0;
  if (uncomp_image->width>0)
      depth= uncomp_image->row_stride / (double)uncomp_image->width;

  int ret;

  if(uncomp_image->data == NULL){
      int uncomp_size = (uncomp_image->width) * (uncomp_image->height) * depth;
      uncomp_image->data = (unsigned char *) realloc(uncomp_image->data, uncomp_size * sizeof(uint8_t));
  }

  ret = jpeg_decompress_8u_rgb(jpeg_image->data, 
                               jpeg_image->size, uncomp_image->data, 
                               uncomp_image->width, 
                               uncomp_image->height, 
                               uncomp_image->row_stride);
  
  uncomp_image->row_stride = uncomp_image->width * depth;
  uncomp_image->size = uncomp_image->width *uncomp_image->height* depth;
  if (depth == 1) {
    uncomp_image->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY;
  }
  else if (depth == 3) {
    uncomp_image->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB; //TODO: lets just assume its rgb... dunno what else to do :-/
  }
  return ret;
}

static void on_kinect_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                                   const kinect_frame_msg_t *msg, 
                                   void *user_data ) {
    
    printf("on_kinect_image_frame\n");

    if (msg->depth.depth_data_format != KINECT_DEPTH_MSG_T_DEPTH_MM) { 
        std::cerr << "Warning: kinect data format is not KINECT_DEPTH_MSG_T_DEPTH_MM" << std::endl 
                  << "Program may not function properly" << std::endl;
    }
    assert (msg->depth.depth_data_format == KINECT_DEPTH_MSG_T_DEPTH_MM);

    double t1 = timestamp_us();    
    // Update the kinect_data struct with depth and rgb information
    kinect.update(msg);
    double t2 = timestamp_us();

    // Mat img = kinect.getRGB().clone();
    // Mat gray = kinect.getGray().clone();

    // if (selectObject && selection.width > 0 && selection.height > 0) {
    //     Mat roi(img, selection);
    //     bitwise_not(roi, roi);
    // }

    // // Copy point cloud
    // populatePCL(point_cloud_pcl);
    // double t3 = timestamp_us();

    // compute(tracker.belief);
    // double t5 = timestamp_us();

    // std::cout << "======================================" << std::endl;
    // std::cerr << "update time: " << (t2 - t1)*1e-3 << " ms" << std::endl;
    // std::cerr << "populate time: " << (t3 - t2)*1e-3 << " ms" << std::endl;
    // std::cerr << "HT run time: " << (t4 - t3)*1e-3 << " ms" << std::endl;
    // std::cerr << "feature run time: " << (t5 - t4)*1e-3 << " ms" << std::endl;

    // std::cerr << "TOTAL: " << (t5 - t1)*1e-3 << " ms" << std::endl;
    return;
}

void  INThandler(int sig)
{

    lcm_destroy(kinect.lcm);
    pthread_exit(NULL);
    
    printf("Exiting\n");
    exit(0);
}

static void onMouse(int event, int x, int y, int flags, void* userdata) {
    MouseEvent* data = (MouseEvent*)userdata;
    if (selectObject) {
        selection.x = MIN(x, origin.x);
        selection.y = MIN(y, origin.y);
        selection.width = std::abs(x - origin.x);
        selection.height = std::abs(y - origin.y);
        selection &= Rect(0, 0, WIDTH, HEIGHT);
    }

    switch (event) {
    case CV_EVENT_LBUTTONDOWN:
        origin = Point(x, y);
        selection = Rect(x, y, 0, 0);
        selectObject = true;
        break;
    case CV_EVENT_LBUTTONUP:
        selectObject = false;
        trackObject = -1;
        break;
    }
    return;
}

void* lcm_thread(void* data) { 
    kinect_data* ki = (kinect_data*) data;
    while (1) { 
        lcm_handle(ki->lcm);
        printf("lcm handler\n");
    }
}


void* fuse_thread(void* data) { 
    HistTracker* ht = (HistTracker*) data;

    double t1 = timestamp_us();
    double t2 = timestamp_us();
    while (1) { 
        
        // tprintf("fuse: sync pt\n");

        // Synchronization point
        int rc = pthread_barrier_wait(&barr);
        if(rc != 0 && rc != PTHREAD_BARRIER_SERIAL_THREAD) {
            tprintf("Could not wait on barrier\n");
            continue;
        }

        t1 = timestamp_us();

        pthread_mutex_lock(&draw_mutex);

        // tprintf("fuse thread begin\n");
        compute(tracker.belief);
        // tprintf("fuse thread done\n");

        pthread_cond_broadcast(&draw_cond); 
        pthread_mutex_unlock(&draw_mutex);

        t2 = timestamp_us();
        std::cout << "======================================" << std::endl;
        std::cerr << "UPDATE time: " << (t2 - t1)*1e-3 << " ms" << std::endl;
    }    

}
void* histtr_thread(void* data) { 
    HistTracker* ht = (HistTracker*) data;
    while (1) { 
        pthread_cond_wait(&ht->kinect->rgb_cond, &ht->kinect->rgb_mutex);
        // tprintf("histtracker started\n");
        double t1 = timestamp_us();

        Mat img = ht->kinect->getRGB().clone();
        Mat gray = ht->kinect->getGray().clone();
        pthread_mutex_unlock(&ht->kinect->rgb_mutex);

        // Update tracker with rgb image
        // Also tracks the object (can check via tracker.running())
        tracker.run();
        double t4 = timestamp_us();
        
        // Find feature descriptors from hist mask
        fill_mask(tracker.belief, tracker.trackBox);

        // tprintf("histtr: sync pt\n");
        double t2 = timestamp_us();
        std::cerr << "hist time: " << (t2 - t1)*1e-3 << " ms" << std::endl;

        // Synchronization point
        int rc = pthread_barrier_wait(&barr);
        if(rc != 0 && rc != PTHREAD_BARRIER_SERIAL_THREAD) {
            tprintf("Could not wait on barrier\n");
            continue;
        }
        // tprintf("histtr: sync done \n");

        // tprintf("hist tracker thread done\n");
    }
}

void* hybridtr_thread(void* data) { 
    HistTracker* ht = (HistTracker*) data;
    while (1) { 
        pthread_cond_wait(&ht->kinect->depth_cond, &ht->kinect->depth_mutex);
        // tprintf("hybridtracker started\n");

        double t1 = timestamp_us();
        populatePCL(point_cloud_pcl);

        pthread_mutex_unlock(&ht->kinect->depth_mutex);


        double t2 = timestamp_us();
        std::cerr << "hybrid time: " << (t2 - t1)*1e-3 << " ms" << std::endl;
        // tprintf("hybridtr: sync pt\n");

        // Synchronization point
        int rc = pthread_barrier_wait(&barr);
        if(rc != 0 && rc != PTHREAD_BARRIER_SERIAL_THREAD) {
            tprintf("Could not wait on barrier\n");
            continue;
        }

        // tprintf("hybridtr: sync done \n");

        // tprintf("hybrid tracker thread done\n");
    }
}

int main(int argc, char** argv)
{

    int c;
    char *lcm_url = NULL;

    // command line options - to publish on a specific url  
    while ((c = getopt (argc, argv, "vhir:jq:zl:")) >= 0) {
        switch (c) {
        case 'l':
            lcm_url = strdup(optarg);
            printf("Using LCM URL \"%s\"\n", lcm_url);
            break;
        case 'h':
        case '?':
            usage(argv[0]);
        }
    }

    point_cloud_pcl = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
    canonical_obj_pcl = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);

    // LCM-related
    kinect.lcm = lcm_create(lcm_url);
    if(!kinect.lcm) {
        printf("Unable to initialize LCM\n");
        return 1;
    } 

    // Initialize the tracker with kinect data pointer
    tracker.useKinectData(&kinect);

    // motion model params
    params.motion_model = CvMotionModel::LOW_PASS_FILTER;
    //params.motion_model = CvMotionModel::EM;
    params.low_pass_gain = 0.1;
    // mean shift params
    params.ms_tracker_weight = 0.4;
    params.ms_params.tracking_type = CvMeanShiftTrackerParams::HS;
    // feature tracking params
    params.ft_tracker_weight = 0.6;
    params.ft_params.feature_type = CvFeatureTrackerParams::OPTICAL_FLOW;
    params.ft_params.window_size = 0;

    // Subscribe to the kinect image
    kinect_frame_msg_t_subscribe(kinect.lcm, "KINECT_FRAME", on_kinect_image_frame, NULL);
    kinect_skeleton_msg_t_subscribe(kinect.lcm, "KINECT_SKELETON_FRAME", on_skeleton_frame, NULL);
    bot_core_image_t_subscribe(kinect.lcm, "SEGMENTED_FRAME", on_segmented_frame, NULL);

    // bot_glib_mainloop_attach_lcm (kinect.lcm);
    lcmgl_basis_tfs = bot_lcmgl_init (kinect.lcm, "TF_BASIS");

    // Install signal handler to free data.
    signal(SIGINT, INThandler);

    // Barrier count 3
    pthread_barrier_init(&barr, NULL, 3);

    // pthread_create(&opencv_th, NULL, &opencv_thread, (void*) &kinect);
    pthread_create(&histtr_th, NULL, &histtr_thread, &tracker);
    pthread_create(&hybridtr_th, NULL, &hybridtr_thread, &tracker);
    pthread_create(&fuse_th, NULL, &fuse_thread, &tracker);
    pthread_create(&lcm_th, NULL, &lcm_thread, &kinect);
 
    while(1) { 
        pthread_cond_wait(&draw_cond, &draw_mutex); 
        tracker.showWindows();
        unsigned char c = cv::waitKey(10) & 0xff;
        pthread_mutex_unlock(&draw_mutex);

        
        if (c == 'q') { 
            break;
        } else if (c == 't') { 
            std::cerr << "Using current pointed region" << std::endl;
            tracker.useCurrentPointLoc();
            // assert (tracker.mode == HistTracker::VISION_MODE);
        } else if (c == 'f') { // Extract features and do icp
            // Feature extraction & copy depth img for 3d extraction
            tracker.buildDescriptors(tracker.belief);
        } else if (c == 'e') { // Extract all points within colormatch and do icp
            std::cerr << "Captured canonical object pose" << std::endl;    

            fill_mask(tracker.belief, tracker.trackBox);
            extractPCL(canonical_obj_pcl, point_cloud_pcl, tracker.belief);

            prunePCLWithDepth(canonical_obj_pcl);

            float cx=0, cy=0, cz=0;
            for (int j=0; j<canonical_obj_pcl->points.size(); j++) { 
                cx += canonical_obj_pcl->points[j].x, 
                    cy += canonical_obj_pcl->points[j].y, 
                    cz += canonical_obj_pcl->points[j].z;
            }
            mean_canonical_obj_pcl = Point3f(cx/canonical_obj_pcl->points.size(),
                                             cy/canonical_obj_pcl->points.size(),
                                             cz/canonical_obj_pcl->points.size());
        } else if (c == 'i') { 
            updated_tf = Eigen::Matrix4f::Identity();
        } else if (c== 's') { 
            Mat gray = kinect.getGray().clone();
            
            std::stringstream ss; 
            ss << "gray" << img_count++ << ".png";
            cv::imwrite(ss.str(), gray);
            std::cerr << "Saving " << ss.str() << std::endl;
        }
    }

    return 0;
}
