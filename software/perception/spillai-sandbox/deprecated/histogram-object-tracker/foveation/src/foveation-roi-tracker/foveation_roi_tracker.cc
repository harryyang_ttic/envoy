// LCM includes
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <pthread.h>
#include <limits.h>
#include <iostream>
#include <algorithm>
#include <vector>
#include <complex>

#include <gsl/gsl_blas.h>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <velodyne/velodyne.h>
#include <velodyne/velodyne_extractor.h>

#include <bot_lcmgl_client/lcmgl.h>
#include <GL/gl.h>

#include <lcmtypes/kinect_point_t.h>
#include <lcmtypes/kinect_point_list_t.h>
#include <lcmtypes/erlcm_roi_t.h>
#include <lcmtypes/erlcm_pointing_vector_t.h>

// static void usage(const char* progname)
// {
//   fprintf (stderr, "Usage: %s [options]\n"
//                    "\n"
//                    "Options:\n"
//                    "  -l URL    Specify LCM URL\n"
//                    "  -h        This help message\n", 
//                    g_path_get_basename(progname));
//   exit(1);
// }

// void  INThandler(int sig)
// {
//     lcm_destroy(lcm);
//     printf("Exiting\n");
//     exit(0);
// }


typedef struct _state_t state_t;

struct point { 
    float x, y, z;
    point() {}
    point(float _x, float _y, float _z): x(_x), y(_y), z(_z) {}
    point(const point& p) { 
        x = p.x;
        y = p.y; 
        z = p.z;
    }
    const point operator +=(const point& p) { 
        x += p.x;
        y += p.y; 
        z += p.z;
    }
    const point operator -=(const point& p) { 
        x -= p.x;
        y -= p.y; 
        z -= p.z;
    }

};

point operator + (const point& p1, const point& p2) { 
    return point(p1.x + p2.x, p1.y + p2.y, p1.z + p2.z);
}

point operator - (const point& p1, const point& p2) { 
    return point(p1.x - p2.x, p1.y - p2.y, p1.z - p2.z);
}


struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    velodyne_extractor_state_t *velodyne; 
};
state_t * state = NULL;

// lcmgl
bot_lcmgl_t *lcmgl_bb;

struct proj_pt { 
    point p;
    float d;
    proj_pt(point _p, float _d)
        : p(_p), d(_d) {}
};

point pt_pos(1.76963,-0.230064,1.54611), pt_vec(1/sqrt(2),1/sqrt(2), 0);
// cv::Point3f pt_pos(0,0,0), pt_vec(1,0,0);
bool pointing_vec_init = true;

const float pointing_cone_fov = 0.2618; // 15 degree
const float pointing_cone_cos_fov = 0.9659; 

static void 
on_pointing_vector (const lcm_recv_buf_t *rbuf, const char *channel,
        const erlcm_pointing_vector_t *msg, void *user_data )
{
    pointing_vec_init = true;
    pt_pos.x = msg->pos[0], 
        pt_pos.y = msg->pos[1], 
        pt_pos.z = msg->pos[2];
    
    pt_vec.x = msg->vec[0], 
        pt_vec.y = msg->vec[1], 
        pt_vec.z = msg->vec[2];

    // std::cerr << "P: " << pt_pos.x << " "<< pt_pos.y << " "<< pt_pos.z << std::endl;
    // std::cerr << "V: " << pt_vec.x << " "<< pt_vec.y << " "<< pt_vec.z << std::endl << std::endl;;

    return;
}

std::pair<point, point> mu_var(const std::vector<point>& pts) { 
    point sum(0,0,0);
    for (int j=0; j<pts.size(); j++) {
        sum.x += pts[j].x;
        sum.y += pts[j].y;
        sum.z += pts[j].z;
    }
    
    int n = pts.size();
    point mu(sum.x/n, sum.y/n, sum.z/n);
    
    point sum2(0,0,0);
    for (int j=0; j<pts.size(); j++) { 
        sum2.x += (pts[j].x - mu.x) * (pts[j].x - mu.x);
        sum2.y += (pts[j].y - mu.y) * (pts[j].y - mu.y);
        sum2.z += (pts[j].z - mu.z) * (pts[j].z - mu.z);
    }
 
    point var(sum2.x/(n-1), sum2.y/(n-1), sum2.z/(n-1));
    return std::make_pair(mu, var);
}

std::pair<float, float> mu_var(const std::vector<float>& pts) { 
    float sum = 0;
    for (int j=0; j<pts.size(); j++)
        sum += pts[j];

    int n = pts.size();
    float mu = sum/n;

    float sum2 = 0;
    for (int j=0; j<pts.size(); j++)
        sum2 += (pts[j] - mu) * (pts[j] - mu);
 
    float var = sum2/(n-1);
    return std::make_pair(mu, var);
}

struct proj_pt_mu_var { 
    float depth_mu;
    float depth_var;
    
    point off_mu;
    point off_var;
};
    
proj_pt_mu_var mu_var(const std::vector<proj_pt>& pts) { 
    proj_pt_mu_var mv;
    float sum = 0;
    for (int j=0; j<pts.size(); j++)
        sum += pts[j].d;

    int n = pts.size();
    mv.depth_mu = sum/n;

    float sum2 = 0;
    for (int j=0; j<pts.size(); j++)
        sum2 += (pts[j].d - mv.depth_mu) * (pts[j].d - mv.depth_mu);
    mv.depth_var = sum2/(n-1);
 
    // pt on plane
    point sump(0,0,0); 
    for (int j=0; j<pts.size(); j++)
        sump += pts[j].p;

    mv.off_mu = point(sump.x/n,sump.y/n,sump.z/n);

    point sum2p(0,0,0);
    for (int j=0; j<pts.size(); j++) { 
        point pt(pts[j].p - mv.off_mu);
        sum2p += point(pt.x*pt.x,pt.y*pt.y,pt.z*pt.z);
    }
    mv.off_var = point(sum2p.x/(n-1),sum2p.y/(n-1),sum2p.z/(n-1));

    return mv;
}

bool pts_sort_x(const point& lhs, const point& rhs) { 
    return lhs.x < rhs.x;
}
bool pts_sort_y(const point& lhs, const point& rhs) { 
    return lhs.y < rhs.y;
}
bool pts_sort_z(const point& lhs, const point& rhs) { 
    return lhs.z < rhs.z;
}

// void publish_ellipsoid(std::vector<cv::Point3f> pts) { 

//     std::vector<cv::Point2f> xys;

//     std::pair<cv::Point3f, cv::Point3f> muvar = mu_var(pts);
//     cv::Point3f mu = muvar.first;
//     cv::Point3f var = muvar.second;

//     cv::Point3f median(mu);
//     std::nth_element(pts.begin(), pts.begin() + pts.size()/2, pts.end(), pts_sort_x);
//     median.x = pts[pts.size()/2].x;
//     std::nth_element(pts.begin(), pts.begin() + pts.size()/2, pts.end(), pts_sort_y);
//     median.y = pts[pts.size()/2].y;
//     std::nth_element(pts.begin(), pts.begin() + pts.size()/2, pts.end(), pts_sort_z);
//     median.z = pts[pts.size()/2].z;

//     erlcm_roi_t bb;
//     //memset(&bb,0, sizeof(erlcm_roi_t));
//     bb.utime = bot_timestamp_now(); // ???

//     // char test[1024];
//     // sprintf(test, "local");
//     // std::string test1("local");
//     bb.frame = "local";


//     bb.pos[0] = median.x;
//     bb.pos[1] = median.y;
//     bb.pos[2] = median.z;

//     double rpy[3];
//     rpy[0] = 0;
//     rpy[1] = atan2(pt_vec.z, pt_vec.y); 
//     rpy[2] = atan2(pt_vec.y, pt_vec.x);

//     double quat[4];
//     bot_roll_pitch_yaw_to_quat(rpy, quat);

//     for (int j=0; j<4; j++) 
//         bb.orientation[j] = quat[j];

//     // bb.orientation[0] = 1;
//     // bb.orientation[1] = 0;
//     // bb.orientation[2] = 0;
//     // bb.orientation[3] = 0;

//     bb.dim[0] = 2*std::sqrt(var.x);
//     bb.dim[1] = 2*std::sqrt(var.y);
//     bb.dim[2] = 2*std::sqrt(var.z);

//     bb.foviation_type = ERLCM_ROI_T_MOVE_HORIZONTAL;

//     erlcm_roi_t_publish(state->lcm, "ROI", &bb);
   
    
//     return;
// }

void draw_bb(erlcm_roi_t& bb) { 
    
    bot_lcmgl_t* lcmgl = lcmgl_bb;

    double curr_quat_m[16];
    bot_quat_pos_to_matrix(bb.orientation, bb.pos, curr_quat_m);

    double curr_quat_m_opengl[16];
    bot_matrix_transpose_4x4d(curr_quat_m, curr_quat_m_opengl);

    lcmglPushMatrix();
    lcmglMultMatrixd(curr_quat_m_opengl);

    float x1 = -bb.dim[0] / 2;
    float y1 = -bb.dim[1] / 2;
    float z1 = -bb.dim[2] / 2;

    float x2 = bb.dim[0] / 2;
    float y2 = bb.dim[1] / 2;
    float z2 = bb.dim[2] / 2;

    lcmglColor3f(.6,.6,0);
    lcmglLineWidth(3);
    lcmglBegin(GL_LINE_LOOP);
    lcmglVertex3f(x1, y1, z1);

    lcmglVertex3f(x1, y2, z1);
    lcmglVertex3f(x1, y2, z2);
    lcmglVertex3f(x1, y2, z1);

    lcmglVertex3f(x2, y2, z1);
    lcmglVertex3f(x2, y2, z2);
    lcmglVertex3f(x2, y2, z1);

    lcmglVertex3f(x2, y1, z1);
    lcmglVertex3f(x2, y1, z2);
    lcmglVertex3f(x2, y1, z1);

    lcmglVertex3f(x1, y1, z1);
    lcmglVertex3f(x1, y1, z2);
    lcmglVertex3f(x1, y2, z2);
    lcmglVertex3f(x2, y2, z2);
    lcmglVertex3f(x2, y1, z2);
    lcmglVertex3f(x1, y1, z2);
    lcmglEnd();

    lcmglPopMatrix();

    bot_lcmgl_switch_buffer(lcmgl_bb);
}

bool proj_pt_sort(const proj_pt& lhs, const proj_pt& rhs) { 
    return lhs.d < rhs.d;
}

void publish_ellipsoid2(std::vector<point> pts) { 
    if (!pts.size())
        return;

    std::vector<proj_pt> proj; 
    
    // Compute projection of pts to the pointing vector
    // z value is the depth, x and y are projection onto plane
    point sum(0,0,0);
    for (int j=0; j<pts.size(); j++) { 
        float d = (pts[j].x-pt_pos.x) * pt_vec.x + (pts[j].y-pt_pos.y) * pt_vec.y + (pts[j].z-pt_pos.z) * pt_vec.z;
        point p((pts[j].x-pt_pos.x)-d*pt_vec.x, (pts[j].y-pt_pos.y)-d*pt_vec.y,(pts[j].z-pt_pos.z)-d*pt_vec.z);
        proj.push_back(proj_pt(p,d));
        sum += pts[j];
    }

    // Median by depth
    std::sort(proj.begin(), proj.end(), proj_pt_sort);
    float median = proj[proj.size()/2].d;

    // Within 1m from median
    for (std::vector<proj_pt>::iterator it=proj.begin(); it!=proj.end(); ) 
        if (fabs(median-it->d)>.5)
            it = proj.erase(it);
        else 
            it++;

    proj_pt_mu_var mv = mu_var(proj);
    float depth = std::sqrt(mv.depth_var); // 1-std dev
    float max_depth = median + depth;

    erlcm_roi_t bb;
    bb.utime = bot_timestamp_now(); // ???
    bb.frame = "local"; // ERLCM_ROI_T_FRAME_LOCAL;

    // mediod (not really)
    point C(pt_pos.x + median * pt_vec.x, pt_pos.y + median * pt_vec.y, pt_pos.z + median * pt_vec.z);

    bb.pos[0] = C.x;
    bb.pos[1] = C.y;
    bb.pos[2] = C.z;

    double rpy[3];
    rpy[0] = 0;
    rpy[1] = atan2(pt_vec.z, pt_vec.y); 
    rpy[2] = atan2(pt_vec.y, pt_vec.x);

    double quat[4];
    bot_roll_pitch_yaw_to_quat(rpy, quat);

    for (int j=0; j<4; j++) 
        bb.orientation[j] = quat[j];

    // bb.orientation[0] = 1;
    // bb.orientation[1] = 0;
    // bb.orientation[2] = 0;
    // bb.orientation[3] = 0;

    // d = | (x0 - x1) x (x0 - x2) | / | x2 - x1 | ; where x2 and x1 lie on the line and x0 is the pt

    bb.dim[0] = 6 * depth;
    bb.dim[1] = 6 * std::sqrt(mv.off_var.x); // 2 * max_depth * tan(pointing_cone_fov);
    bb.dim[2] = 6 * std::sqrt(mv.off_var.y); // 2 * max_depth * tan(pointing_cone_fov);

    bb.foviation_type = ERLCM_ROI_T_MOVE_HORIZONTAL;

    erlcm_roi_t_publish(state->lcm, "ROI", &bb);
    draw_bb(bb);

    return;

}

void trackPointingRegion(const xyz_point_list_t* pcl) { 
    
    // kinect_point_list_t point_cloud;
    // point_cloud.points = (kinect_point_t*) malloc(sizeof(kinect_point_t) * pcl->no_points);
    // memset(point_cloud.points, 0, sizeof(kinect_point_t) * pcl->no_points);
    // point_cloud.num_points = pcl->no_points;

    // std::vector<float> xs, ys, zs;
    std::vector<point> xyzs;

    float x, y, z;
    // kinect_point_t* points = point_cloud.points;

    for (int j=0; j<pcl->no_points; j++) { 
        x = pcl->points[j].xyz[0]; 
        y = pcl->points[j].xyz[1]; 
        z = pcl->points[j].xyz[2]; 
        
        // // x-y plane
        // points[j].x = x; 
        // points[j].y = y; 
        // points[j].z = z; 

        // points[j].r = 0; 
        // points[j].g = 0; 
        // points[j].b = 128; 

        // Check if point is within pointed region
        float dx = x - pt_pos.x, dy = y - pt_pos.y, dz = z - pt_pos.z;
        float nm =  sqrt(dx * dx + dy * dy + dz * dz); 
        float rad = (dx / nm) * pt_vec.x + (dy / nm) * pt_vec.y + 
            (dz / nm) * pt_vec.z;

        //if (rad > 0.866) { 
            if (rad > pointing_cone_cos_fov) { // 15 deg cone (blue)
                xyzs.push_back(point(x,y,z));
                // points[j].r = 128; 
                // points[j].g = 0; 
                // points[j].b = 0; 
                //  } else { 
                // points[j].r = 0; 
                // points[j].g = 128; 
                // points[j].b = 0; 
            }
            //}
    }
    
    publish_ellipsoid2(xyzs);
    // kinect_point_list_t_publish(state->lcm, "KINECT_POINT_CLOUD", &point_cloud);
    // free(point_cloud.points); 

    return;
}

//doesnt do anything right now - timeout function
//gboolean heartbeat_cb (gpointer data)
void point_source_cb(int64_t utime, void *data)
{
    state_t *s = (state_t *)data;

    // Use the above information to find points within a cone of 10 deg
    // Also colors the point cloud
    xyz_point_list_t *ret = velodyne_extract_points(s->velodyne);
    if(ret != NULL && ret->no_points && pointing_vec_init){
        trackPointingRegion(ret);
        destroy_xyz_list(ret);
    } 

    //return true to keep running
    //return TRUE;
}


// static void 
// on_skeleton_frame (const lcm_recv_buf_t *rbuf, const char *channel,
// 		   const kinect_skeleton_msg_t *skeleton_msg, void *user_data )
// {
//     if(skeleton_msg)
//         kinect_skeleton_msg_t_destroy(skeleton_msg);
//     skeleton_msg = kinect_skeleton_msg_t_copy(skeleton_msg);


//     return;
// }

int main(int argc, char** argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state = (state_t*) calloc(1, sizeof(state_t));

    state->lcm =  bot_lcm_get_global(NULL);
    state->velodyne = velodyne_extractor_init(state->lcm, point_source_cb, state);
    state->mainloop = g_main_loop_new( NULL, FALSE );  

    if (!state->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    erlcm_pointing_vector_t_subscribe(state->lcm, "HAND_VECTOR", on_pointing_vector, NULL);
    lcmgl_bb = bot_lcmgl_init (state->lcm, "ROI");

    /* heart beat*/
    //g_timeout_add (100, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);

    // int c;
    // char *lcm_url = NULL;

    // // command line options - to publish on a specific url  
    // while ((c = getopt (argc, argv, "vhir:jq:zl:")) >= 0) {
    //     switch (c) {
    //     case 'l':
    //         lcm_url = strdup(optarg);
    //         printf("Using LCM URL \"%s\"\n", lcm_url);
    //         break;
    //     case 'h':
    //     case '?':
    //         usage(argv[0]);
    //     }
    // }

    // // LCM-related
    // lcm = lcm_create(lcm_url);
    // if(!lcm) {
    //     printf("Unable to initialize LCM\n");
    //     return 1;
    // } 

    // Subscribe pointing vector
    // kinect_skeleton_msg_t_subscribe(state->lcm, "KINECT_SKELETON_FRAME", on_skeleton_frame, NULL);
    // kinect_frame_msg_t_subscribe(kinect.lcm, "KINECT_FRAME", on_kinect_image_frame, NULL);

    // // Install signal handler to free data
    // signal(SIGINT, INThandler);

    // while(1) { 
    //     lcm_handle(lcm);
    // }

    // lcm_destroy(lcm);

    return 0;
}
