import numpy as np
cimport numpy as np

np.import_array()

cdef extern from "pcl-utils.hpp":
    cdef cppclass IntegralNormalEstimation:
        IntegralNormalEstimation()
        # depth, width, height, scale, depth_change_factor, smoothing_size, returned surface normals
        void apply(float*, int, int, int, float, float, float *)


def integral_normal_estimation(np.ndarray[np.float32_t, ndim=3] depth, scale=1, depth_change_factor=0.5, smoothing_size=10.0):
    """DepthSLIC Superpixels for fixed superpixel size.

    Parameters
    ----------
    img : numpy array, dtype=float32
        Original depth image (float image)
        Needs to be C-Contiguous.
    scale: int, default=1
        Desired scale (to downsample, scale>1)
    depth_change_factor: refer pcl api
    smoothing_size: refer pcl api        

    Returns
    -------
    normals : numpy array

    """

    if (depth.ndim != 3):
         raise ValueError("Point Cloud needs to be 3D.")
    if np.isfortran(depth):
        raise ValueError("The input is not C-contiguous")
    cdef int h = depth.shape[0]
    cdef int w = depth.shape[1]
    cdef int d = depth.shape[2]    

    cdef np.npy_intp shape[3]
    shape[0] = h
    shape[1] = w
    shape[2] = 3

    # cdef float * normals
    cdef np.ndarray[np.float32_t, ndim=3] normal = np.PyArray_SimpleNew(3, shape, np.NPY_FLOAT32)
    
    cdef IntegralNormalEstimation* ne = new IntegralNormalEstimation()
    ne.apply(<float *>depth.data, w, h, scale, depth_change_factor, smoothing_size, <float*>normal.data)
    
    return normal
