// pcl includes
#include <pcl/io/pcd_io.h>
#include <pcl/PointIndices.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/random_sample.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/surface/mls.h>

class IntegralNormalEstimation { 
    float* normals_; 

public: 
    IntegralNormalEstimation(); 
    virtual ~IntegralNormalEstimation();
    void apply(const float* ubuff, const int width, const int height, const int scale, 
               const float depth_change_factor, float smoothing_size, float* normals); 
};
    
