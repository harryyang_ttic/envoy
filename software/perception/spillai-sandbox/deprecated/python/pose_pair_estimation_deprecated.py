#!/usr/bin/python 
# TODO: Compute relative pose mean instead of absolute mean pose

from __future__ import division

import time
import numpy as np
import pandas as pd
import networkx as nx
import random
import itertools

import transformations as tf
import rigid_transform as rtf

import draw_utils

from collections import defaultdict

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

def tf_compress(T): 
    return rtf.RigidTransform.from_homogenous_matrix(T)

def tf_uncompress(v): 
    return v.to_homogeneous_matrix()

def tf_construct(v1,v2): 
    """Align vx along v1, and construct [vx,vy,vz] as follows: 
    vx = v1
    vz = v1 x v2
    vy = vz x vx
    """

    v1 /= np.linalg.norm(v1)
    v2 /= np.linalg.norm(v2)

    vz = np.cross(v1, v2); 
    vz /= np.linalg.norm(vz)

    vy = np.cross(vz, v1)
    vy /= np.linalg.norm(vy)

    R = np.vstack([v1, vy, vz]).T

    return R

def tf_T(R, t): 
    """ Construct [R t; 0 1] transformation matrix from R and t """
    T = np.eye(4)    
    T[:3,:3] = R;
    T[:3,3] = t;
    return T

def pose_pair_construct(p1,n1,p2,n2): 
    """ Construct pose-pair from (pt1,normal1), and (pt2,normal2) """
    v1 = p1-p2; v1 /= np.sqrt(np.dot(v1,v1))
    v2 = -v1

    R1, R2 = tf_construct(n1,v1), tf_construct(n2,v2)
    T1, T2 = tf_T(R1, p1), tf_T(R2, p2)    

    return rtf.RigidTransform.from_homogenous_matrix(T1), \
        rtf.RigidTransform.from_homogenous_matrix(T2)

def mean_pose(poses): 
    """HACK: Compute the mean pose of set of tfs"""
    tvecs = [pose.tvec.reshape(-1) for pose in poses]
    mu_pos = np.mean(np.vstack(tvecs), axis=0)

    rpys = [np.fmod(np.array(pose.quat.to_roll_pitch_yaw()),
                    2*np.pi) for pose in poses]
    mu_rpy = np.mean(np.vstack(rpys), axis=0);
    mu_quat = rtf.Quaternion.from_roll_pitch_yaw(mu_rpy[0], mu_rpy[1], mu_rpy[2])

    # print mu_pos, np.vstack(tvecs)
    
    return rtf.RigidTransform(mu_quat, mu_pos)

class PosePairEstimation: 
    def __init__(self, data, evaluate_top=10): 

        self.MAX_CLUSTER_SIZE = 5;
        self.SAMPLE_SIZE = 3;

        # label, utime - > pose
        self.pose_map = defaultdict(
            lambda: defaultdict()
        )

        # Build cluster index map
        self.identify_cluster_inds(data, evaluate_top=evaluate_top)

        # Estimate pose given data
        self.estimate(data)


    # def evaluate(self, gt_data): 
    #     for label,v in self.pose_map.iteritems(): 
    #         pred_p = v.values()[0]
    #         rel_p = []

    #         # Find rel distance between labels and gt
    #         for fidx in gt_data.valid_feat_inds:
    #             # Valid utimes
    #             ut_inds, = np.where(gt_data.idx[fidx,:] != -1)
    #             idx = self.idx[fidx,ut_inds[0]]
    #             rel_p.append((np.linalg.norm(gt_data.poses[idx].tvec-pred_p), fidx))

    #         # Sort by rel distance 
    #         rel_p.sort(lambda x: x[0])

    #         # Pick the closest gt as reference
    #         closest_fidx = rel_p[0][1]

    #         # Compare poses
    #         pred_utimes, pred_vals = v.keys(), v.values()
    #         ut_inds, = np.where(gt_data.idx[closest_fidx,:] != -1)
    #         utimes = np.intersect1d(gt_data.utimes[ut_inds], pred_utimes)
                

    def get_pose_list(self): 
        pose_list = []
        for label,v in self.pose_map.iteritems(): 
            vals = [(utime,pose) for utime,pose in v.iteritems()]
            vals.sort(key=lambda x: x[0])
            pose_list.append((label,[pose for utime,pose in vals]))
        pose_list.sort(key=lambda x: x[0])                    
        return pose_list

    def viz_data(self): 
        viz_poses = []
        for label,v in self.pose_map.iteritems(): 
            for pose in v.values(): 
                viz_poses.append(pose)
        draw_utils.publish_pose_list2('POSE_LIST_OPT', viz_poses, sensor_tf=False)       

    def estimate(self, data): 
        # Eval cluster 
        for label,label_inds in self.cluster_ind_map.iteritems(): 

            # Build overlapping utimes
            utimes = []
            for ind in label_inds: 
                ut_inds, = np.where(data.idx[ind,:] != -1);
                utimes.extend(ut_inds)
            utimes = sorted(set(utimes))

            # Build pose pair manager
            ppm = PoseManager(ids=data.feature_ids[label_inds], 
                              utimes=utimes)

            # For each utime, add observation
            for ut_idx, utime in enumerate(utimes): 
                
                # Look at each feature, and add if available
                linds, = np.where(data.idx[label_inds,ut_idx] != -1)
                linds = label_inds[linds]
                
                for ind1,ind2 in itertools.product(linds,linds):
                    if ind1 == ind2: continue
                    ppm.add_observation(ut_idx, 
                                        data.feature_ids[ind1], 
                                        data.xyz[ind1,ut_idx], data.normal[ind1,ut_idx],
                                        data.feature_ids[ind2], 
                                        data.xyz[ind2,ut_idx], data.normal[ind1,ut_idx]
                    )
            
            # Optimize pose
            ppm.optimize()

            # Pose map
            for utime_ind,pose in ppm.final_pose_map.iteritems(): 
                self.pose_map[label][utimes[utime_ind]] = pose
            

    def identify_cluster_inds(self, data, evaluate_top): 
        # Build cluster index map
        self.cluster_ind_map = {}

        # # First identify unique labels
	# labels = np.unique(data.labels)
        # print 'Pose Pair Estimation of %i labels' % len(labels)

        # Count number of instances of labels
        unique_labels, unique_inv = np.unique(data.labels, return_inverse=True)
        num_labels = len(unique_labels)
        label_count = np.bincount(unique_inv)
        label_votes = zip(unique_labels, label_count)

        # Pick the top labeled trajectories
        label_votes.sort(key=lambda x: x[1], reverse=True)
        labels = [lv[0] for lv in label_votes[:evaluate_top]]

        print 'Pose Pair Estimation of top %i labels' % len(labels)

        # Find exemplars for each cluster 
	for label in labels: 
	    if label < 0: continue; # don't do anything for unlabeled cases

            # Indices that have the label == label
	    inds, = np.where(data.labels == label)
	    assert(len(inds) != 0)

            # Arbitrarily pick random set of label_inds
            label_inds = random.sample(inds, min(self.MAX_CLUSTER_SIZE, len(inds)));

            # For querying relative poses
	    self.cluster_ind_map[label] = np.array(label_inds, dtype=np.int32)
	    print 'Label %i %s' % (label,label_inds)


class PoseCluster:
    def __init__(self): 
        # Store pose map
        self.cluster_id = -1
        self.pose_map = dict()
        self.exemplar = None

    def add_observations(self, poses): 
        try: 
            assert(isinstance(poses, list))
        except AssertionError: 
            print 'Cannot handle instances other than LIST: [rigid_transform.Pose . . ]'
        for pose in poses: 
            self.add_observation(pose)
        
    def add_observation(self, pose): 
        try: 
            assert(isinstance(pose, rtf.Pose))
        except AssertionError: 
            print 'Cannot handle instances other than rigid_transform.Pose'

        print 'Adding observation pose: %i' % (pose.id)
        self.pose_map[pose.id] = pose
        
    def constructed(self): 
        return self.exemplar is not None

    def construct(self): 
        for pose in self.pose_map.values(): 
            if np.isnan(pose.quat.q).any() or np.isnan(pose.tvec).any(): continue
            print 'Constructing cluster: %s with %i' % (self.pose_map.keys(), pose.id)
            self.exemplar = pose
            self.cluster_id = self.exemplar.id * 10000 # HACK
            break

    def relative(self, p_i_w_t): 
        if p_i_w_t.id not in self.pose_map.keys(): 
            print 'Cannot determine relative pose: Unknown ID: %i' % p_i_w_t.id
            return None
        p_i_w_0_inv = self.pose_map[p_i_w_t.id].inverse()
        p_i_w_t_0 = p_i_w_t * p_i_w_0_inv
        return p_i_w_t_0


    # def mean_pose(self, poses): 
    #     """HACK: Compute the mean pose of set of tfs"""
    #     # tvecs = [pose.tvec for pose in poses]
    #     # mu_pos = np.mean(np.vstack(tvecs), axis=0)
        
    #     # rpys = [np.fmod(np.array(pose.to_roll_pitch_yaw())+2*np.pi,
    #     #                 2*np.pi) for pose in poses]
    #     # mu_rpy = np.mean(np.vstack(rpys), axis=0);
    #     # mu_quat = Quaternion.from_roll_pitch_yaw(mu_rpy[0], mu_rpy[1], mu_rpy[2])

    #     return rtf.RigidTransform(poses[0].quat, poses.tvec)
    #     # return rtf.RigidTransform(mu_quat, mu_pos)


    # def estimate(self, poses): 
    #     # For each pose, find relative tf, 
    #     # and apply each of the relative tfs to the 
    #     # exemplar, and compute the exemplars mean pose

    #     assert(self.exemplar is not None)

    #     relatives = []
    #     for pose in poses: 
    #         # print 'Estimating pose: %i' % (pose.id)
    #         # Here j represents candidate within cluster
    #         p_j_w_t_0 = self.relative(pose)
    #         # print 'Relative: ', p_j_w_t_0
    #         if p_j_w_t_0 is None: continue
    #         relatives.append(p_j_w_t_0)

    #     if not len(relatives): return None
    #     return rtf.Pose.from_rigid_transform(self.exemplar.id, 
    #                                          self.mean_pose(relatives) * self.exemplar)
        

class PosePairNode: 
    def __init__(self, utime, i, j, pp_ij, pp_ji): 
        self.i = i
        self.j = j
        self.utime = utime

        self.pp_ij_w = pp_ij
        self.pp_ji_w = pp_ji

        # print 'PPN: ', 'i', self.i, 'j', self.j, 'utime', self.utime, 'pp_ij', self.pp_ij_w, 'pp_ji', self.pp_ji_w

class PoseManager: 
    def __init__(self, utimes, ids): 

        self.next_idx = 0

        # print 'Initing', 'utimes', utimes, 'ids', ids

        # Graph matrix for idx map
        self.pp_graph_utime_map = defaultdict(
            lambda: defaultdict()
        )
        self.pp_graph_pair_map = defaultdict(
            lambda: defaultdict()
        )

        # Intranodes per utime
        self.intranodes = defaultdict(set)
        
        # Pose-Pair graph
        self.pp_graph = nx.Graph()

        # Initial Pose map of track i @ t=t 
        self.initial_pose_map = defaultdict(
                lambda: rtf.RigidTransform([1,0,0,0],[0,0,0])
        ) # (id-pair : Pose-pair)

        # Final Pose map of track i @ t=t 
        self.final_pose_map = defaultdict(
            lambda: rtf.RigidTransform([1,0,0,0],[0,0,0])
        ) # (id-pair : Pose-pair)

        # Final pose obs (id,utime)
        self.final_poseobs_map = defaultdict(list)

        # Per-id Pose utimes
        # self.id_utimes = defaultdict(set)
        self.utimes = set()

        # self.initial_utime = dict() # 
        # self.final_utime = dict() # 

        # Set to indicate initialized or not
        # self.inited = dict()

        # self.id2s = [261] # , 351, 602, 470, 411, 347, 346, 366, 341, 382, 400]

        self.debug = 0

    def next_index(self): 
        self.next_idx += 1
        return self.next_idx

    def init(self, fidi, pp_ij_w_t): 
        # Get normal and pt from pose-pair
        T_ij_w_t = pp_ij_w_t.to_homogeneous_matrix()
        ni,pi = T_ij_w_t[0,:3].reshape(-1), T_ij_w_t[:3,3].reshape(-1)

        # Init. Pose Map
        Ri = tf_construct(ni, [0.,0.,-1.]) # Cooked up z (gravity vector)
        Ti = tf_T(Ri, pi)
        return rtf.RigidTransform.from_homogenous_matrix(Ti)

    def add_observation(self, utime, fidi, pi, ni, fidj, pj, nj): 
        # print 'Adding OBS: ', utime, fidi, fidj

        # Construct Pose-Pair
        pp_ij_w_t, pp_ji_w_t = pose_pair_construct(pi, ni, pj, nj)

        # Add obs. to graph
        nidx = self.next_index()

        # Look at previously added nodes, both inter/intra
        intranodes = self.pp_graph_utime_map[utime].keys()
        internodes = self.pp_graph_pair_map[(fidi,fidj)].keys()

        self.pp_graph_utime_map[utime][(fidi,fidj)] = nidx
        self.pp_graph_pair_map[(fidi,fidj)][utime] = nidx

        # Add pose-pair observation
        self.pp_graph.add_node(nidx, 
                               data=PosePairNode(utime=utime, i=fidi, j=fidj, 
                                                 pp_ij=pp_ij_w_t, pp_ji=pp_ji_w_t))

        # Find utime and putime, and establish edge between pose-pairs
        # Note: Edge weight is 1 for inter-utime edges, and 2 for intra-utime edges
        # putime = utime-1
        # if putime in self.pp_graph_utime_map and (fidi,fidj) in self.pp_graph_utime_map[putime]: 
        #     pnidx = self.pp_graph_utime_map[putime][(fidi,fidj)]
        #     self.pp_graph.add_edge(nidx,pnidx,weight=1)
        #     # print 'Adding internode edge'

        # # Connect all (fidi,fidj) for all utimes
        # if (fidi,fidj) in self.pp_graph_pair_map: 
        #     for putime,pnidx in self.pp_graph_pair_map[(fidi,fidj)].iteritems(): 
        #         self.pp_graph.add_edge(nidx,pnidx,weight=1)
        #         # print 'Adding internode edge'

        # Only connect previous (fidi,fidj)
        utimes = self.pp_graph_pair_map[(fidi,fidj)].keys()
        pidx = utimes.index(utime)-1        
        if pidx >= 0:
            putime = utimes[pidx]
            pnidx = self.pp_graph_utime_map[putime][(fidi,fidj)]
            self.pp_graph.add_edge(pnidx,nidx,weight=1)
            # print 'Adding internode edge %i %i (%i<->%i)' % (fidi, fidj, pnidx, nidx)
            
               
        # Find all pairs within same utime, and establish edges
        # print 'intranodes: ', intranodes
        for pair in random.sample(intranodes, min(3, len(intranodes))): 
        # for pair in intranodes: 
            pnidx = self.pp_graph_utime_map[utime][pair]
            self.pp_graph.add_edge(pnidx,nidx,weight=2)
            # print 'Adding intranode edge %i %i (%i<->%i)' % (fidi, fidj, pnidx, nidx)

        # INitial and final utimes
        # if fidi not in self.initial_utime: self.initial_utime[fidi] = utime
        # self.final_utime[fidi] = utime

        # Add to fidi pose utimes
        # self.id_utimes[fidi].add(utime)
        self.utimes.add(utime)

        # self.initial_pose_map[fidi][utime] = pp_ij_w_t

    def compute_relative_pose(self, path): 
        relpaths = zip(path[:-1],path[1:])
        # print 'Relative pose: ', path, relpaths

        for (nidx,pidx) in relpaths: 
            p_ppn = self.pp_graph.node[pidx]['data']
            n_ppn = self.pp_graph.node[nidx]['data']

            # During relative pose estimation, 
            # poses with same utime don't matter
            if p_ppn.utime == n_ppn.utime: continue

            # print 'Relative pose est: b/w utimes', p_ppn.utime, n_ppn.utime

            # [T_{ij}^w]_{t=t} = [T_i]_{t=t}^{t=0} * [T_{ij}^w]_{t=0}
            # [T_i]_{t=t}^{t=0} = [T_{ij}^w]_{t=t} * [T_w^{ij}]^{t=0}
            # [T_i^w]_{t=t} = ([T_{ij}^w]_{t=t} * [T_w^{ij}]^{t=0}) * [T_{ij}^w]_{t=0}
            pp_ij_w_p = p_ppn.pp_ij_w
            pp_ij_w_n = n_ppn.pp_ij_w

            # [T_i]_{t=t}^{t=0}: Compute relative tf of pose @ t=t w.r.t pose @ t=0
            # Relative transformation of posepair/fidi
            # [T_i]_{t=t}^{t=0} = [T_{ij}^w]_{t=t} * [T_w^{ij}]^{t=0}
            pp_ij_w_n_p = pp_ij_w_n * pp_ij_w_p.inverse()

        return pp_ij_w_n_p


    # Randomly select k-paths and estimate new pose for utime given pose at putime
    # Deal with outliers as well
    def update_pose(self, putime, utime): 

        # print 'Update pose with putime pose: ', self.final_pose_map[putime]
        p_i_w_p = self.final_pose_map[putime]

        ppairs = self.pp_graph_utime_map[putime].values()
        npairs = self.pp_graph_utime_map[utime].values()

        p_i_w_t_obs = []
        count = 0
        for pidx,nidx in itertools.product(ppairs,npairs):
            st = time.time()
            try:
                path = nx.shortest_path(self.pp_graph, source=nidx, target=pidx)
            except: 
                print 'Path not found', pidx, nidx
                continue;

            pp_ij_w_n_p = self.compute_relative_pose(path)
            p_i_w_t_obs.append(pp_ij_w_n_p * p_i_w_p)
           
            # print 'Time taken for shortest path: %3.2f s' % (time.time() - st)
            # print 'Path between utime: %i and putime: %i ' % (utime, putime), path

            count += 1
            if count > 5: break

        if len(p_i_w_t_obs): 
            # Final pose of fidi
            # [T_i^w]_{t=t} = ([T_{ij}^w]_{t=t} * [T_w^{ij}]^{t=0}) * [T_{ij}^w]_{t=0}
            mu_p_i_w_t = mean_pose(p_i_w_t_obs)
            self.final_pose_map[utime] = mu_p_i_w_t
            # print 'Update mean pose of fid: %i b/w t=%i and t=%i' % (fid,putime,utime)
        else: 
            print 'Failed to find mean pose'

    def optimize(self): 
        # Per label optimize
        utimes = list(self.utimes)
        relutimes = zip(utimes[:-1],utimes[1:])        

        inited = False
        for putime,utime in relutimes: 
            # If not inited
            if not inited: 
                # Initial pose map
                ninds = self.pp_graph_utime_map[putime].values()

                tvecs = []
                for nidx in ninds: 
                    tvecs.append(self.pp_graph.node[nidx]['data'].pp_ij_w.tvec)
                    tvecs.append(self.pp_graph.node[nidx]['data'].pp_ji_w.tvec)

                mu_pos = np.mean(np.vstack(tvecs), axis=0)
                
                p_i_w_0 = rtf.RigidTransform([1,0,0,0],mu_pos)
                print 'Init: ', p_i_w_0

                self.initial_pose_map[putime] = p_i_w_0
                self.final_pose_map[putime] = p_i_w_0

                # pp_ij_w_0[-4:] = np.array([0.,0.,0.,1.])
                # p_i_w_0 = self.init(fidi, pp_ij_w_0)

                print 'INIT: Track intialized at t=%i' % (putime)
                inited = True

            # Convenience function for  updating utime pose from putime for fidi
            self.update_pose(putime, utime)
            
            # if self.debug > 10: break
            self.debug += 1 

            
            

        # # For each ID
        # for fidi,utimes_set in self.id_utimes.iteritems(): 
        #     utimes = list(utimes_set)
        #     relutimes = zip(utimes[:-1],utimes[1:])        

        #     inited = False
        #     for putime,utime in relutimes: 
        #         # If not inited
        #         if not inited: 
        #             # Initial pose map
        #             pp_ij_w_0 = self.initial_pose_map[fidi][putime]
        #             # pp_ij_w_0[-4:] = np.array([0.,0.,0.,1.])
        #             p_i_w_0 = self.init(fidi, pp_ij_w_0)
        #             print 'Init: ', p_i_w_0
        #             self.final_pose_map[fidi][putime] = p_i_w_0
        #             print 'INIT: Track %i intialized at t=%i' % (fidi, putime)
        #             inited = True

        #         # Convenience function for  updating utime pose from putime for fidi
        #         self.update_pose(fidi, putime, utime)
        #         # if self.debug > 100: break
        #         self.debug += 1 


    # def update(self, utime): 
    #     pairs = self.pp_graph_utime_map[utime].keys()
    #     for (fidi,fidj) in pairs: 
        
    #         if fidi != self.id1: continue
    #         # if fidj not in self.id2s: continue

    #         nidx = self.pp_graph_utime_map[utime][(fidi,fidj)]
    #         # print self.pp_graph.node[nidx], nidx
    #         pp_ij_w_n = self.pp_graph.node[nidx]['data'].pp_ij_w

    #         utimes = self.pp_graph_pair_map[(fidi,fidj)].keys()
    #         pidx = utimes.index(utime)-1
    #         if pidx < 0: 
    #             # Initialize if no putime
    #             if fidi not in self.inited: 
    #                 self.init(utime, fidi, pp_ij_w_n)
    #                 self.inited[fidi] = nidx
    #         else: 
    #             # Find shortest path to putime (fidi,fidj)
    #             putime = utimes[pidx]

    #             # Find previous node idx
    #             pnidx = self.pp_graph_utime_map[putime][(fidi,fidj)]
    #             pp_ij_w_p = self.pp_graph.node[pnidx]['data'].pp_ij_w

    #             path = nx.shortest_path(self.pp_graph, source=pnidx, target=nidx)
    #             relpaths = zip(path[:-1],path[1:])

    #             T_i_w_p = tf_uncompress(self.final_pose_map[fidi][putime])

    #             T_ij_w_n_0 = np.eye(4)
    #             for (pidx,nidx) in relpaths: 
    #                 T_ij_w_p = tf_uncompress(pp_ij_w_p)
    #                 T_ij_w_n = tf_uncompress(pp_ij_w_n)

    #                 T_ij_w_n_p = np.dot(T_ij_w_n, tf.inverse_matrix(T_ij_w_p))
    #                 T_ij_w_n_0 = np.dot(T_ij_w_n_p, T_ij_w_n_0)
                
    #             T_i_w_n = np.dot(T_ij_w_n_0, T_i_w_p)
    #             p_i_w_n = tf_compress(T_i_w_n)
                
    #             self.final_poseobs_map[utime][fidi].append(p_i_w_n)

    #     # Compute mean pose for each track from pose pair observations        
    #     for fidi,poses in self.final_poseobs_map[utime].iteritems(): 
    #         print 'Update: track %i @ %i' % (fidi, utime)
    #         if not len(poses): continue
    #         self.final_pose_map[fidi][utime] = mean_pose(np.vstack(poses))

    # def fwd_update(self, utime): 
    #     # For all pairs @ t = utime
    #     # Compute the relative transformation between utime, and previously seen putime
    #     # Propagate final pose from putime to utime, and add as obs.
    #     pairs = self.pp_graph_utime_map[utime].keys()
    #     for (fidi,fidj) in pairs: 
        
    #         if fidi != self.id1: continue
    #         if fidj not in self.id2s: continue
            

    #         # Find utime and putime
    #         utimes = self.pp_graph_pair_map[(fidi,fidj)].keys()
    #         pidx = utimes.index(utime)-1
    #         if pidx < 0: continue
    #         putime = utimes[pidx]

    #         # Only continue if final pose is computed
    #         if fidi not in self.final_pose_map[putime]: continue

    #         print 'utime,putime', utime,putime

    #         # Pose pair for putime
    #         nidx = self.pp_graph_pair_map[(fidi,fidj)][putime]
    #         pp_ij_w_0 = self.pp_graph.node[nidx]['data'].pp_ij_w
    #         T_ij_w_0 = tf_uncompress(pp_ij_w_0)

    #         # Pose paair for utime
    #         nidx = self.pp_graph_pair_map[(fidi,fidj)][utime]
    #         pp_ij_w_t = self.pp_graph.node[nidx]['data'].pp_ij_w
    #         T_ij_w_t = tf_uncompress(pp_ij_w_t)

    #         # [T_{ij}^w]_{t=t} = [T_i]_{t=t}^{t=0} * [T_{ij}^w]_{t=0}
    #         # [T_i]_{t=t}^{t=0} = [T_{ij}^w]_{t=t} * [T_w^{ij}]^{t=0}
    #         # [T_i^w]_{t=t} = ([T_{ij}^w]_{t=t} * [T_w^{ij}]^{t=0}) * [T_{ij}^w]_{t=0}

    #         # [T_i]_{t=t}^{t=0}: Compute relative tf of pose @ t=t w.r.t pose @ t=0

    #         # Relative transformation of posepair/fidi
    #         # [T_i]_{t=t}^{t=0} = [T_{ij}^w]_{t=t} * [T_w^{ij}]^{t=0}
    #         T_i_w_t_0 = np.dot(T_ij_w_t, tf.inverse_matrix(T_ij_w_0)) 

    #         # Pose of fidi, due to fidj @ putime
    #         print 'putime: ', self.final_pose_map[putime][fidi]
    #         T_i_w_0 = tf_uncompress(self.final_pose_map[putime][fidi]) 

    #         # Final pose of fidi
    #         # [T_i^w]_{t=t} = ([T_{ij}^w]_{t=t} * [T_w^{ij}]^{t=0}) * [T_{ij}^w]_{t=0}
    #         T_i_w_t = np.dot(T_i_w_t_0, T_i_w_0)

    #         # Final pose 
    #         p_i_w_t = tf_compress(T_i_w_t)

    #         # List of final poses
    #         self.final_poseobs_map[utime][(fidi,fidj)] = p_i_w_t
    #         # self.final_pose_map[fidi][utime].append(p_i_w_t)

    #     # Prepare pose obs. for each track
    #     pairs = self.final_poseobs_map[utime].keys()
    #     # print 'FWD update: ', utime, pairs
    #     track_poses = defaultdict(list)
    #     for (fidi,fidj) in pairs: 
    #         track_poses[fidi].append(self.final_poseobs_map[utime][(fidi,fidj)])

    #     # Compute mean pose for each track from pose pair observations        
    #     for fidi,poses in track_poses.iteritems(): 
    #         print 'FWD update: track %i' % fidi
    #         if not len(poses): continue
    #         self.final_pose_map[utime][fidi] = mean_pose(np.vstack(poses))


        # # For newly introduced pose-pairs
        # pairs = self.pp_graph_utime_map[utime].keys()
        # for (fidi,fidj) in pairs: 
        #     # Find utime and putime
        #     utimes = self.pp_graph_pair_map[(fidi,fidj)].keys()
        #     pidx = utimes.index(utime)-1
        #     if pidx < 0: continue
        #     putime = utimes[pidx]


    # def estimate(self, fidi):         
    #     poses = defaultdict(list)
    #     if fidi not in self.pose_map: 
    #         return poses

    #     # # Initial pose of fid1
    #     # T_i_w_0 = tf_uncompress(self.Oc[fidi])

    #     # print 'Estimate'
    #     # fig = plt.figure(1)
    #     # ax = fig.add_subplot(111, projection='3d')
    #     # coeffs = [1,1,1]

    #     # # Radii corr. to the coeffs
    #     # rx,ry,rz = [1/np.sqrt(coeff) for coeff in coeffs]

    #     # # Set of all spherical angles:
    #     # u = np.linspace(0, 2 * np.pi, 100)
    #     # v = np.linspace(0, np.pi, 100)

    #     # # Cartesian coordinates that correspond to the spherical angles:
    #     # # (this is the equation of an ellipsoid):
    #     # x = rx * np.outer(np.cos(u), np.sin(v))
    #     # y = ry * np.outer(np.sin(u), np.sin(v))
    #     # z = rz * np.outer(np.ones_like(u), np.cos(v))

    #     # # Plot:
    #     # ax.plot_wireframe(x, y, z, rstride=4, cstride=4,  alpha=0.1, color='black')

    #     # Find mean/cov of the pose list 
    #     for utime_ind,pose_list in self.pose_map[fidi].iteritems():
    #         if not len(pose_list): continue

    #         # Find mean of pose_list
    #         # TODO: mu_pp_i_w_t_0  = mean(pose_list)
    #         # np.mean(np.vstack(pose_list)[:,:3], axis=0)

    #         # print 'Pose list: ', len(pose_list)
    #         # Find mean of pose_list
    #         qpts = []
    #         for pp_i_w_t in pose_list: 
    #             T_i_w_t = tf_uncompress(pp_i_w_t)

    #             qpts.append(T_i_w_t[:3,:3].ravel())

    #             # poses[utime_ind] = pp_i_w_t # final
    #             poses[utime_ind].append(pp_i_w_t)

    #         # if len(qpts) > 8: 
    #         #     qpts = np.vstack(qpts)
    #         #     print qpts[:,6:9]
    #         #     ax.plot(qpts[:,0], qpts[:,1], qpts[:,2], 'ro', markersize=3)
    #         #     ax.plot(qpts[:,3], qpts[:,4], qpts[:,5], 'go', markersize=3);
    #         #     ax.plot(qpts[:,6], qpts[:,7], qpts[:,8], 'bo', markersize=3)
    #         #     break

    #     return poses


        
    
    
