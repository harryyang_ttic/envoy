import numpy as np
cimport numpy as np

from libcpp.string cimport string
import cv2

np.import_array()

cdef extern from "slic_utils.hpp" namespace "fsvision":
    int slic(int* buff, int width, int height, int im_type, int* labels, 
             int var, double compactness, int op_type)


def slic_s(np.ndarray[np.uint8_t, ndim=3] img, im_type=cv2.CV_8UC3, 
           superpixel_size=300, compactness=10, op_type=-1):
    """SLIC Superpixels for fixed superpixel size.

    Parameters
    ----------
    img : numpy array, dtype=uint8
        Needs to be C-Contiguous.
    superpixel_size: int, default=300
        Desired size for superpixel
    compactness: douple, default=10
        Degree of compactness of superpixels.

    Returns
    -------
    labels : numpy array

    """

    if (img.shape[2] != 3): raise ValueError("Image needs to have 3 channels.")
    if np.isfortran(img): raise ValueError("The input image is not C-contiguous")

    cdef int h = img.shape[0]
    cdef int w = img.shape[1]
    cdef int ch = img.shape[2]

    cdef np.npy_intp shape[2]
    shape[0] = h; shape[1] = w;
    cdef np.ndarray[np.int32_t, ndim=2] labels = np.PyArray_SimpleNew(2, shape, np.NPY_INT32)

    cdef int nlabels = slic(<int*>img.data, w, h, im_type, <int*> labels.data, 
                            superpixel_size, compactness, op_type)
    
    return labels
