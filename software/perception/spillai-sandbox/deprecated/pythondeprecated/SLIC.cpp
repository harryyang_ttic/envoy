// SLIC.cpp: implementation of the SLIC class.
//
// Copyright (C) Radhakrishna Achanta 2012
// All rights reserved
// Email: firstname.lastname@epfl.ch
//////////////////////////////////////////////////////////////////////
//#include "stdafx.h"
#include <cfloat>
#include <cmath>
#include <iostream>
#include <fstream>
#include "SLIC.hpp"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SLIC::SLIC()
{
    mu_ch1 = std::vector<double> ();
    mu_ch2 = std::vector<double> ();
    mu_ch3 = std::vector<double> ();
    cluster_count = std::vector<double> ();

    klabel = std::vector<int>();
    numlabels = 0;
}

SLIC::~SLIC()
{
}

//==============================================================================
///	RGB2XYZ
///
/// sRGB (D65 illuninant assumption) to XYZ conversion
//==============================================================================
void SLIC::RGB2XYZ(
	const int&		sR,
	const int&		sG,
	const int&		sB,
	double&			X,
	double&			Y,
	double&			Z)
{
	double R = sR/255.0;
	double G = sG/255.0;
	double B = sB/255.0;

	double r, g, b;

	if(R <= 0.04045)	r = R/12.92;
	else				r = pow((R+0.055)/1.055,2.4);
	if(G <= 0.04045)	g = G/12.92;
	else				g = pow((G+0.055)/1.055,2.4);
	if(B <= 0.04045)	b = B/12.92;
	else				b = pow((B+0.055)/1.055,2.4);

	X = r*0.4124564 + g*0.3575761 + b*0.1804375;
	Y = r*0.2126729 + g*0.7151522 + b*0.0721750;
	Z = r*0.0193339 + g*0.1191920 + b*0.9503041;
}

//===========================================================================
///	RGB2LAB
//===========================================================================
void SLIC::RGB2LAB(const int& sR, const int& sG, const int& sB, double& lval, double& aval, double& bval)
{
	//------------------------
	// sRGB to XYZ conversion
	//------------------------
	double X, Y, Z;
	RGB2XYZ(sR, sG, sB, X, Y, Z);

	//------------------------
	// XYZ to LAB conversion
	//------------------------
	double epsilon = 0.008856;	//actual CIE standard
	double kappa   = 903.3;		//actual CIE standard

	double Xr = 0.950456;	//reference white
	double Yr = 1.0;		//reference white
	double Zr = 1.088754;	//reference white

	double xr = X/Xr;
	double yr = Y/Yr;
	double zr = Z/Zr;

	double fx, fy, fz;
	if(xr > epsilon)	fx = pow(xr, 1.0/3.0);
	else				fx = (kappa*xr + 16.0)/116.0;
	if(yr > epsilon)	fy = pow(yr, 1.0/3.0);
	else				fy = (kappa*yr + 16.0)/116.0;
	if(zr > epsilon)	fz = pow(zr, 1.0/3.0);
	else				fz = (kappa*zr + 16.0)/116.0;

	lval = 116.0*fy-16.0;
	aval = 500.0*(fx-fy);
	bval = 200.0*(fy-fz);
}

//===========================================================================
///	DoRGBtoLABConversion
///
///	For whole image: overlaoded floating point version
//===========================================================================
void SLIC::DoRGBtoLABConversion(
	const unsigned int*&		ubuff,
	std::vector<double>&					lvec,
	std::vector<double>&					avec,
	std::vector<double>&					bvec)
{
	int sz = m_width*m_height;
	lvec = std::vector<double>(sz);
	avec = std::vector<double>(sz);
	bvec = std::vector<double>(sz);

	for( int j = 0; j < sz; j++ )
	{
		int r = (ubuff[j] >> 16) & 0xFF;
		int g = (ubuff[j] >>  8) & 0xFF;
		int b = (ubuff[j]      ) & 0xFF;

		RGB2LAB( r, g, b, lvec[j], avec[j], bvec[j] );
	}
}

//===========================================================================
///	DoRGBtoLABConversion
///
/// For whole volume
//===========================================================================
void SLIC::DoRGBtoLABConversion(
	unsigned int**&		ubuff,
	std::vector<std::vector<double> >&					lvec,
	std::vector<std::vector<double> >&					avec,
	std::vector<std::vector<double> >&					bvec)
{
	int sz = m_width*m_height;
	for( int d = 0; d < m_depth; d++ )
	{
		for( int j = 0; j < sz; j++ )
		{
			int r = (ubuff[d][j] >> 16) & 0xFF;
			int g = (ubuff[d][j] >>  8) & 0xFF;
			int b = (ubuff[d][j]      ) & 0xFF;

			RGB2LAB( r, g, b, lvec[d][j], avec[d][j], bvec[d][j] );
		}
	}
}

//=================================================================================
/// DrawContoursAroundSegments
///
/// Internal contour drawing option exists. One only needs to comment the if
/// statement inside the loop that looks at neighbourhood.
//=================================================================================
void SLIC::DrawContoursAroundSegments(
	unsigned int*&			ubuff,
	int*&					labels,
	const int&				width,
	const int&				height,
	const unsigned int&				color )
{
	const int dx8[8] = {-1, -1,  0,  1, 1, 1, 0, -1};
	const int dy8[8] = { 0, -1, -1, -1, 0, 1, 1,  1};

/*	int sz = width*height;

	vector<bool> istaken(sz, false);

	int mainindex(0);
	for( int j = 0; j < height; j++ )
	{
		for( int k = 0; k < width; k++ )
		{
			int np(0);
			for( int i = 0; i < 8; i++ )
			{
				int x = k + dx8[i];
				int y = j + dy8[i];

				if( (x >= 0 && x < width) && (y >= 0 && y < height) )
				{
					int index = y*width + x;

					if( false == istaken[index] )//comment this to obtain internal contours
					{
						if( labels[mainindex] != labels[index] ) np++;
					}
				}
			}
			if( np > 1 )//change to 2 or 3 for thinner lines
			{
				ubuff[mainindex] = color;
				istaken[mainindex] = true;
			}
			mainindex++;
		}
	}*/


	int sz = width*height;
	vector<bool> istaken(sz, false);
	vector<int> contourx(sz);vector<int> contoury(sz);
	int mainindex(0);int cind(0);
	for( int j = 0; j < height; j++ )
	{
		for( int k = 0; k < width; k++ )
		{
			int np(0);
			for( int i = 0; i < 8; i++ )
			{
				int x = k + dx8[i];
				int y = j + dy8[i];

				if( (x >= 0 && x < width) && (y >= 0 && y < height) )
				{
					int index = y*width + x;

					//if( false == istaken[index] )//comment this to obtain internal contours
					{
						if( labels[mainindex] != labels[index] ) np++;
					}
				}
			}
			if( np > 1 )
			{
				contourx[cind] = k;
				contoury[cind] = j;
				istaken[mainindex] = true;
				//img[mainindex] = color;
				cind++;
			}
			mainindex++;
		}
	}

	int numboundpix = cind;//int(contourx.size());
	for( int j = 0; j < numboundpix; j++ )
	{
		int ii = contoury[j]*width + contourx[j];
		ubuff[ii] = 0xffffff;

		for( int n = 0; n < 8; n++ )
		{
			int x = contourx[j] + dx8[n];
			int y = contoury[j] + dy8[n];
			if( (x >= 0 && x < width) && (y >= 0 && y < height) )
			{
				int ind = y*width + x;
				if(!istaken[ind]) ubuff[ind] = 0;
			}
		}
	}
}



//=================================================================================
/// DrawContoursAroundSegments
///
/// Internal contour drawing option exists. One only needs to comment the if
/// statement inside the loop that looks at neighbourhood.
//=================================================================================
void SLIC::GetContoursAroundSegments(
                                     std::vector<cv::Point>& contours, 
	int*&					labels,
	const int&				width,
	const int&				height)
{
	const int dx8[8] = {-1, -1,  0,  1, 1, 1, 0, -1};
	const int dy8[8] = { 0, -1, -1, -1, 0, 1, 1,  1};

	int sz = width*height;
	vector<bool> istaken(sz, false);
	vector<int> contourx(sz);vector<int> contoury(sz);
	int mainindex(0);int cind(0);
	for( int j = 0; j < height; j++ )
	{
		for( int k = 0; k < width; k++ )
		{
			int np(0);
			for( int i = 0; i < 8; i++ )
			{
				int x = k + dx8[i];
				int y = j + dy8[i];

				if( (x >= 0 && x < width) && (y >= 0 && y < height) )
				{
					int index = y*width + x;

					//if( false == istaken[index] )//comment this to obtain internal contours
					{
						if( labels[mainindex] != labels[index] ) np++;
					}
				}
			}
			if( np > 1 )
			{
				contourx[cind] = k;
				contoury[cind] = j;
				istaken[mainindex] = true;
				//img[mainindex] = color;
				cind++;
			}
			mainindex++;
		}
	}

        contours = std::vector<cv::Point>(contourx.size());
        for (int j=0; j<contourx.size(); j++) 
            contours[j] = cv::Point(contourx[j], contoury[j]);
        return;
}


//==============================================================================
///	DetectLabEdges
//==============================================================================
void SLIC::DetectLabEdges(const cv::Mat_<cv::Vec3d>& lab, std::vector<double>& edges)
{
    int width = lab.cols, 
        height = lab.rows;
	int sz = lab.cols * lab.rows;

        const cv::Vec3d* labp = lab.ptr<cv::Vec3d>(0);

	edges.resize(sz,0);
	for( int j = 1; j < lab.rows-1; j++ ) {
            for( int k = 1; k < lab.cols-1; k++ ) {
                int i = j*lab.cols+k;

                double dx = (labp[i-1][0]-labp[i+1][0])*(labp[i-1][0]-labp[i+1][0]) +
                    (labp[i-1][1]-labp[i+1][1])*(labp[i-1][1]-labp[i+1][1]) +
                    (labp[i-1][2]-labp[i+1][2])*(labp[i-1][2]-labp[i+1][2]);

                double dy = (labp[i-width][0]-labp[i+width][0])*(labp[i-width][0]-labp[i+width][0]) +
                    (labp[i-width][1]-labp[i+width][1])*(labp[i-width][1]-labp[i+width][1]) +
                    (labp[i-width][2]-labp[i+width][2])*(labp[i-width][2]-labp[i+width][2]);


                edges[i] = dx*dx + dy*dy; // (orig.)
                // edges[i] = fabs(dx) + fabs(dy); // EDITED 
            }
	}
}

//===========================================================================
///	PerturbSeeds
//===========================================================================
void SLIC::PerturbSeeds(
	std::vector<double>&				kseedsl,
	std::vector<double>&				kseedsa,
	std::vector<double>&				kseedsb,
	std::vector<double>&				kseedsx,
	std::vector<double>&				kseedsy,
        const std::vector<double>&                   edges)
{
	const int dx8[8] = {-1, -1,  0,  1, 1, 1, 0, -1};
	const int dy8[8] = { 0, -1, -1, -1, 0, 1, 1,  1};
	
	int numseeds = kseedsl.size();

        cv::Vec3d* labp = m_lab.ptr<cv::Vec3d>(0);

	for( int n = 0; n < numseeds; n++ )
	{
		int ox = kseedsx[n];//original x
		int oy = kseedsy[n];//original y
		int oind = oy*m_width + ox;

		int storeind = oind;
		for( int i = 0; i < 8; i++ )
		{
			int nx = ox+dx8[i];//new x
			int ny = oy+dy8[i];//new y

			if( nx >= 0 && nx < m_width && ny >= 0 && ny < m_height)
			{
				int nind = ny*m_width + nx;
				if( edges[nind] < edges[storeind])
				{
					storeind = nind;
				}
			}
		}
		if(storeind != oind)
		{
			kseedsx[n] = storeind%m_width;
			kseedsy[n] = storeind/m_width;
			kseedsl[n] = labp[storeind][0];
			kseedsa[n] = labp[storeind][1];
			kseedsb[n] = labp[storeind][2];
		}
	}
}


//===========================================================================
///	GetLABXYSeeds_ForGivenStepSize
///
/// The k seed values are taken as uniform spatial pixel samples.
//===========================================================================
void SLIC::GetLABXYSeeds_ForGivenStepSize(
	vector<double>&				kseedsl,
	vector<double>&				kseedsa,
	vector<double>&				kseedsb,
	vector<double>&				kseedsx,
	vector<double>&				kseedsy,
    const int&					STEP,
    const bool&					perturbseeds,
    const vector<double>&       edgemag)
{
    const bool hexgrid = false;
	int numseeds(0);
	int n(0);

	//int xstrips = m_width/STEP;
	//int ystrips = m_height/STEP;
	int xstrips = (0.5+double(m_width)/double(STEP));
	int ystrips = (0.5+double(m_height)/double(STEP));

    int xerr = m_width  - STEP*xstrips;if(xerr < 0){xstrips--;xerr = m_width - STEP*xstrips;}
    int yerr = m_height - STEP*ystrips;if(yerr < 0){ystrips--;yerr = m_height- STEP*ystrips;}

	double xerrperstrip = double(xerr)/double(xstrips);
	double yerrperstrip = double(yerr)/double(ystrips);

	int xoff = STEP/2;
	int yoff = STEP/2;
	//-------------------------
	numseeds = xstrips*ystrips;
	//-------------------------
	kseedsl.resize(numseeds);
	kseedsa.resize(numseeds);
	kseedsb.resize(numseeds);
	kseedsx.resize(numseeds);
	kseedsy.resize(numseeds);

        cv::Vec3d* labp = m_lab.ptr<cv::Vec3d>(0);
	for( int y = 0; y < ystrips; y++ )
	{
		int ye = y*yerrperstrip;
		for( int x = 0; x < xstrips; x++ )
		{
			int xe = x*xerrperstrip;
            int seedx = (x*STEP+xoff+xe);
            if(hexgrid){ seedx = x*STEP+(xoff<<(y&0x1))+xe; seedx = min(m_width-1,seedx); }//for hex grid sampling
            int seedy = (y*STEP+yoff+ye);
            int i = seedy*m_width + seedx;
			
			kseedsl[n] = labp[i][0];
			kseedsa[n] = labp[i][1];
			kseedsb[n] = labp[i][2];
            kseedsx[n] = seedx;
            kseedsy[n] = seedy;
			n++;
		}
	}

	
	if(perturbseeds)
	{
		PerturbSeeds(kseedsl, kseedsa, kseedsb, kseedsx, kseedsy, edgemag);
	}
}

// //===========================================================================
// ///	GetKValues_LABXYZ
// ///
// /// The k seed values are taken as uniform spatial pixel samples.
// //===========================================================================
// void SLIC::GetKValues_LABXYZ(
// 	vector<double>&				kseedsl,
// 	vector<double>&				kseedsa,
// 	vector<double>&				kseedsb,
// 	vector<double>&				kseedsx,
// 	vector<double>&				kseedsy,
// 	vector<double>&				kseedsz,
//         const int&				STEP)
// {
//     const bool hexgrid = false;
// 	int numseeds(0);
// 	int n(0);

// 	int xstrips = (0.5+double(m_width)/double(STEP));
// 	int ystrips = (0.5+double(m_height)/double(STEP));
// 	int zstrips = (0.5+double(m_depth)/double(STEP));

//     int xerr = m_width  - STEP*xstrips;if(xerr < 0){xstrips--;xerr = m_width - STEP*xstrips;}
//     int yerr = m_height - STEP*ystrips;if(yerr < 0){ystrips--;yerr = m_height- STEP*ystrips;}
//     int zerr = m_depth  - STEP*zstrips;if(zerr < 0){zstrips--;zerr = m_depth - STEP*zstrips;}

// 	double xerrperstrip = double(xerr)/double(xstrips);
// 	double yerrperstrip = double(yerr)/double(ystrips);
// 	double zerrperstrip = double(zerr)/double(zstrips);

// 	int xoff = STEP/2;
// 	int yoff = STEP/2;
// 	int zoff = STEP/2;
// 	//-------------------------
// 	numseeds = xstrips*ystrips*zstrips;
// 	//-------------------------
// 	kseedsl.resize(numseeds);
// 	kseedsa.resize(numseeds);
// 	kseedsb.resize(numseeds);
// 	kseedsx.resize(numseeds);
// 	kseedsy.resize(numseeds);
// 	kseedsz.resize(numseeds);

// 	for( int z = 0; z < zstrips; z++ )
// 	{
// 		int ze = z*zerrperstrip;
// 		int d = (z*STEP+zoff+ze);
// 		for( int y = 0; y < ystrips; y++ )
// 		{
// 			int ye = y*yerrperstrip;
// 			for( int x = 0; x < xstrips; x++ )
// 			{
// 				int xe = x*xerrperstrip;
// 				int i = (y*STEP+yoff+ye)*m_width + (x*STEP+xoff+xe);
				
// 				kseedsl[n] = m_lvecvec[d][i];
// 				kseedsa[n] = m_avecvec[d][i];
// 				kseedsb[n] = m_bvecvec[d][i];
// 				kseedsx[n] = (x*STEP+xoff+xe);
// 				kseedsy[n] = (y*STEP+yoff+ye);
// 				kseedsz[n] = d;
// 				n++;
// 			}
// 		}
// 	}
// }

//===========================================================================
///	PerformSuperpixelSLIC
///
///	Performs k mean segmentation. It is fast because it looks locally, not
/// over the entire image.
//===========================================================================
void SLIC::PerformSuperpixelSLIC(
	std::vector<double>&				kseedsl,
	std::vector<double>&				kseedsa,
	std::vector<double>&				kseedsb,
	std::vector<double>&				kseedsx,
	std::vector<double>&				kseedsy,
        std::vector<int>&					klabels,
        const int&				STEP,
        const std::vector<double>&                   edgemag,
	const double&				M)
{
	int sz = m_width*m_height;
	const int numk = kseedsl.size();
	//----------------
	int offset = STEP;
        //if(STEP < 8) offset = STEP*1.5;//to prevent a crash due to a very small step size
	//----------------
	
	std::vector<double> clustersize(numk, 0);
	std::vector<double> inv(numk, 0);//to store 1/clustersize[k] values

	std::vector<double> sigmal(numk, 0);
	std::vector<double> sigmaa(numk, 0);
	std::vector<double> sigmab(numk, 0);
	std::vector<double> sigmax(numk, 0);
	std::vector<double> sigmay(numk, 0);
	std::vector<double> distvec(sz, DBL_MAX);

	double invwt = 1.0/((STEP/M)*(STEP/M));

        cv::Vec3d* labp = m_lab.ptr<cv::Vec3d>(0);

	int x1, y1, x2, y2;
	double l, a, b;
	double dist;
	double distxy;
	for( int itr = 0; itr < 10; itr++ )
	{
		distvec.assign(sz, DBL_MAX);
		for( int n = 0; n < numk; n++ )
		{
                        y1 = max(0.0,			kseedsy[n]-offset);
                        y2 = min((double)m_height,	kseedsy[n]+offset);
                        x1 = max(0.0,			kseedsx[n]-offset);
                        x2 = min((double)m_width,	kseedsx[n]+offset);


			for( int y = y1; y < y2; y++ )
			{
				for( int x = x1; x < x2; x++ )
				{
					int i = y*m_width + x;

					l = labp[i][0];
					a = labp[i][1];
					b = labp[i][2];

					dist =			(l - kseedsl[n])*(l - kseedsl[n]) +
									(a - kseedsa[n])*(a - kseedsa[n]) +
									(b - kseedsb[n])*(b - kseedsb[n]);

					distxy =		(x - kseedsx[n])*(x - kseedsx[n]) +
									(y - kseedsy[n])*(y - kseedsy[n]);
					
					//------------------------------------------------------------------------
					dist += distxy*invwt;//dist = sqrt(dist) + sqrt(distxy*invwt);//this is more exact
					//------------------------------------------------------------------------
					if( dist < distvec[i] )
					{
						distvec[i] = dist;
						klabels[i]  = n;
					}
				}
			}
		}
		//-----------------------------------------------------------------
		// Recalculate the centroid and store in the seed values
		//-----------------------------------------------------------------
		//instead of reassigning memory on each iteration, just reset.
	
		sigmal.assign(numk, 0);
		sigmaa.assign(numk, 0);
		sigmab.assign(numk, 0);
		sigmax.assign(numk, 0);
		sigmay.assign(numk, 0);
		clustersize.assign(numk, 0);
		//------------------------------------
		//edgesum.assign(numk, 0);
		//------------------------------------

		{int ind(0);
		for( int r = 0; r < m_height; r++ )
		{
			for( int c = 0; c < m_width; c++ )
			{
				sigmal[klabels[ind]] += labp[ind][0];
				sigmaa[klabels[ind]] += labp[ind][1];
				sigmab[klabels[ind]] += labp[ind][2];
				sigmax[klabels[ind]] += c;
				sigmay[klabels[ind]] += r;
				//------------------------------------
				//edgesum[klabels[ind]] += edgemag[ind];
				//------------------------------------
				clustersize[klabels[ind]] += 1.0;
				ind++;
			}
		}}

		{for( int k = 0; k < numk; k++ )
		{
			if( clustersize[k] <= 0 ) clustersize[k] = 1;
			inv[k] = 1.0/clustersize[k];//computing inverse now to multiply, than divide later
		}}
		
		{for( int k = 0; k < numk; k++ )
		{
			kseedsl[k] = sigmal[k]*inv[k];
			kseedsa[k] = sigmaa[k]*inv[k];
			kseedsb[k] = sigmab[k]*inv[k];
			kseedsx[k] = sigmax[k]*inv[k];
			kseedsy[k] = sigmay[k]*inv[k];
			//------------------------------------
			//edgesum[k] *= inv[k];
			//------------------------------------
		}}
	}
}


// //===========================================================================
// ///	SaveSuperpixelLabels
// ///
// ///	Save labels in raster scan order.
// //===========================================================================
// void SLIC::SaveSuperpixelLabels(
// 	const int*&					labels,
// 	const int&					width,
// 	const int&					height,
// 	const string&				filename,
// 	const string&				path) 
// {
// #ifdef WINDOWS
//         char fname[256];
//         char extn[256];
//         _splitpath(filename.c_str(), NULL, NULL, fname, extn);
//         string temp = fname;
//         string finalpath = path + temp + string(".dat");
// #else
//         string nameandextension = filename;
//         size_t pos = filename.find_last_of("/");
//         if(pos != string::npos)//if a slash is found, then take the filename with extension
//         {
//             nameandextension = filename.substr(pos+1);
//         }
//         string newname = nameandextension.replace(nameandextension.rfind(".")+1, 3, "dat");//find the position of the dot and replace the 3 characters following it.
//         string finalpath = path+newname;
// #endif

//         int sz = width*height;
// 	ofstream outfile;
// 	outfile.open(finalpath.c_str(), ios::binary);
// 	for( int i = 0; i < sz; i++ )
// 	{
// 		outfile.write((const char*)&labels[i], sizeof(int));
// 	}
// 	outfile.close();
// }



//===========================================================================
///	EnforceLabelConnectivity
///
///		1. finding an adjacent label for each new component at the start
///		2. if a certain component is too small, assigning the previously found
///		    adjacent label to this component, and not incrementing the label.
//===========================================================================
void SLIC::EnforceLabelConnectivity(
                                    const std::vector<int>& labels,
                                    //input labels that need to be corrected to remove stray labels
                                    std::vector<int>& nlabels,
                                    //new labels                                    // int&						numlabels,//the number of labels changes in the end if segments are removed
                                    const int& K) //the number of superpixels desired by the user
{
    //	const int dx8[8] = {-1, -1,  0,  1, 1, 1, 0, -1};
    //	const int dy8[8] = { 0, -1, -1, -1, 0, 1, 1,  1};

    const int dx4[4] = {-1,  0,  1,  0};
    const int dy4[4] = { 0, -1,  0,  1};

    int width = m_width, height = m_height;
    const int sz = width*height;
    const int SUPSZ = sz/K;
    //nlabels.resize(sz, -1);
    for( int i = 0; i < sz; i++ ) nlabels[i] = -1;
    int label(0);
    std::vector<int> xvec(sz), yvec(sz);
    int oindex(0);
    int adjlabel(0);//adjacent label
    for( int j = 0; j < height; j++ )
	{
            for( int k = 0; k < width; k++ )
		{
                    if( 0 > nlabels[oindex] )
			{
                            nlabels[oindex] = label;
                            //--------------------
                            // Start a new segment
                            //--------------------
                            xvec[0] = k;
                            yvec[0] = j;
                            //-------------------------------------------------------
                            // Quickly find an adjacent label for use later if needed
                            //-------------------------------------------------------
                            {for( int n = 0; n < 4; n++ )
                                    {
					int x = xvec[0] + dx4[n];
					int y = yvec[0] + dy4[n];
					if( (x >= 0 && x < width) && (y >= 0 && y < height) )
                                            {
						int nindex = y*width + x;
						if(nlabels[nindex] >= 0) adjlabel = nlabels[nindex];
                                            }
                                    }}

                            int count(1);
                            for( int c = 0; c < count; c++ )
				{
                                    for( int n = 0; n < 4; n++ )
					{
                                            int x = xvec[c] + dx4[n];
                                            int y = yvec[c] + dy4[n];

                                            if( (x >= 0 && x < width) && (y >= 0 && y < height) )
						{
                                                    int nindex = y*width + x;

                                                    if( 0 > nlabels[nindex] && labels[oindex] == labels[nindex] )
							{
                                                            xvec[count] = x;
                                                            yvec[count] = y;
                                                            nlabels[nindex] = label;
                                                            count++;
							}
						}

					}
				}
                            //-------------------------------------------------------
                            // If segment size is less then a limit, assign an
                            // adjacent label found before, and decrement label count.
                            //-------------------------------------------------------
                            if(count <= SUPSZ >> 2)
				{
                                    for( int c = 0; c < count; c++ )
					{
                                            int ind = yvec[c]*width+xvec[c];
                                            nlabels[ind] = adjlabel;
					}
                                    label--;
				}
                            label++;
			}
                    oindex++;
		}
	}
    numlabels = label;

}



//===========================================================================
///	DoSuperpixelSegmentation_ForGivenSuperpixelSize
///
/// The input parameter ubuff conains RGB values in a 32-bit unsigned integers
/// as follows:
///
/// [1 1 1 1 1 1 1 1]  [1 1 1 1 1 1 1 1]  [1 1 1 1 1 1 1 1]  [1 1 1 1 1 1 1 1]
///
///        Nothing              R                 G                  B
///
/// The RGB values are accessed from (and packed into) the unsigned integers
/// using bitwise operators as can be seen in the function DoRGBtoLABConversion().
///
/// compactness value depends on the input pixels values. For instance, if
/// the input is greyscale with values ranging from 0-100, then a compactness
/// value of 20.0 would give good results. A greater value will make the
/// superpixels more compact while a smaller value would make them more uneven.
///
/// The labels can be saved if needed using SaveSuperpixelLabels()
//===========================================================================
// void SLIC::DoSuperpixelSegmentation_ForGivenSuperpixelSize(const cv::Mat& img, 
//                                                            const int& superpixelsize,
//                                                            const double& compactness)
cv::Mat_<int> SLIC::DoSuperpixelSegmentation_ForGivenSuperpixelSize(const cv::Mat& img, 
                                                                    const int& superpixelsize, 
                                                                    const double& compactness, 
                                                                    const int colorconv) {

    //------------------------------------------------
    const int STEP = sqrt(double(superpixelsize))+0.5;

    //------------------------------------------------
    kseeds_l = vector<double>();
    kseeds_a = vector<double>();
    kseeds_b = vector<double>();
    kseeds_x = vector<double>();
    kseeds_y = vector<double>();
    kseeds_z = vector<double>();

    //--------------------------------------------------
    m_width  = img.cols;
    m_height = img.rows;
    int sz = m_width*m_height;

    //--------------------------------------------------
    std::vector<int> labels(sz,-1);

    //--------------------------------------------------
    // LAB, the default option 
    cv::Mat3b col; 
    cv::cvtColor(img, col, colorconv);
    col.convertTo(m_lab, CV_64FC3);

    //--------------------------------------------------
    bool perturbseeds(false);//perturb seeds is not absolutely necessary, one can set this flag to false

    vector<double> edgemag(0);
    if(perturbseeds) 
        DetectLabEdges(m_lab, edgemag);
    GetLABXYSeeds_ForGivenStepSize(kseeds_l, kseeds_a, kseeds_b, 
                                   kseeds_x, kseeds_y, STEP, perturbseeds, edgemag);

    PerformSuperpixelSLIC(kseeds_l, kseeds_a, kseeds_b, 
                          kseeds_x, kseeds_y, labels, STEP, edgemag,compactness);
    numlabels = kseeds_l.size();

    std::vector<int> nlabels(sz);
    EnforceLabelConnectivity(labels, nlabels, double(sz)/double(STEP*STEP));

    cv::Mat_<int> labelimg = cv::Mat_<int>(img.rows, img.cols, &nlabels[0]).clone();
    return labelimg;
}

//===========================================================================
///	DoSuperpixelSegmentation_ForGivenNumberOfSuperpixels
///
/// The input parameter ubuff conains RGB values in a 32-bit unsigned integers
/// as follows:
///
/// [1 1 1 1 1 1 1 1]  [1 1 1 1 1 1 1 1]  [1 1 1 1 1 1 1 1]  [1 1 1 1 1 1 1 1]
///
///        Nothing              R                 G                  B
///
/// The RGB values are accessed from (and packed into) the unsigned integers
/// using bitwise operators as can be seen in the function DoRGBtoLABConversion().
///
/// compactness value depends on the input pixels values. For instance, if
/// the input is greyscale with values ranging from 0-100, then a compactness
/// value of 20.0 would give good results. A greater value will make the
/// superpixels more compact while a smaller value would make them more uneven.
///
/// The labels can be saved if needed using SaveSuperpixelLabels()
//===========================================================================
cv::Mat_<int> SLIC::DoSuperpixelSegmentation_ForGivenNumberOfSuperpixels(const cv::Mat& img, 
	const int&					K,//required number of superpixels
                                                                         const double&                                   compactness, const int colorconv)//weight given to spatial distance
{
    const int superpixelsize = 0.5+double(img.cols*img.rows)/double(K);
    return DoSuperpixelSegmentation_ForGivenSuperpixelSize(img,superpixelsize,compactness,colorconv);
}

void SLIC::ComputeMeanSuperPixelColor(const cv::Mat& img, const int colorsp ) { 

    int ind=0; 
    vector<cv::Mat> channels;  

    mu_ch1 = vector<double>(numlabels,0);
    mu_ch2 = vector<double>(numlabels,0);
    mu_ch3 = vector<double>(numlabels,0);
    cluster_count = vector<double>(numlabels,0);

    switch(colorsp) { 
    case SLIC_RGB: { 
        cv::split(img, channels);

        // RGB Colorspace mean
        uchar* pimg_b = channels[0].data, *pimg_g = channels[1].data, *pimg_r = channels[2].data;
        for( int r = 0; r < img.rows; r++ ) {
            for( int c = 0; c < img.cols; c++ ) {
                mu_ch1[klabel[ind]] += pimg_b[ind];
                mu_ch2[klabel[ind]] += pimg_g[ind];
                mu_ch3[klabel[ind]] += pimg_r[ind];
                cluster_count[klabel[ind]] += 1.0;
                ind++;
            }
        }

        // Normalize
        for (int j=0; j<numlabels; j++) { 
            if (cluster_count[j] <= 0) cluster_count[j] = 1;
            mu_ch1[j] *= 1.f/cluster_count[j], mu_ch2[j] *= 1.f/cluster_count[j], mu_ch3[j] *= 1.f/cluster_count[j];
        }

        break;
    }
    case SLIC_LAB: { 
        break;
    // case SLIC_HSV:
    //     cv::Mat hsv;
    //     cvtColor(img, hsv, CV_BGR2HSV);
    //     cv::split(hsv, channels);

    //     Vec3b hsv = opencv_utils::bgr_to_hsvvec(Point3f(mu_ch1[j], mu_ch2[j], mu_ch3[j]));
    //     mu_b[j] = hsv[0], mu_g[j] = hsv[1], mu_r[j] = hsv[2];
        
    //     break;
    }
    default: { 
        break;
    }
    }
    return;
}
