// SLIC.h: interface for the SLIC class.
//===========================================================================
// This code implements the superpixel method described in:
//
// Radhakrishna Achanta, Appu Shaji, Kevin Smith, Aurelien Lucchi, Pascal Fua, and Sabine Susstrunk,
// "SLIC Superpixels",
// EPFL Technical Report no. 149300, June 2010.
//===========================================================================
//	Copyright (c) 2012 Radhakrishna Achanta [EPFL]. All rights reserved.
//===========================================================================
//////////////////////////////////////////////////////////////////////

#if !defined(_SLICPP_H_INCLUDED_)
#define _SLICPP_H_INCLUDED_


#include <vector>
#include <string>
#include <algorithm>
#include <opencv2/opencv.hpp>

using namespace std;

enum colorspace { 
    SLIC_RGB, 
    SLIC_HSV, 
    SLIC_LAB
};

class SLIC  
{
public:
	SLIC();
	virtual ~SLIC();
	//============================================================================
	// Superpixel segmentation for a given step size (superpixel size ~= step*step)
	//============================================================================
    cv::Mat_<int> DoSuperpixelSegmentation_ForGivenSuperpixelSize(const cv::Mat& img,
                                                                  const int& superpixelsize, 
                                                                  const double& compactness,
                                                                  const int colorconv = cv::COLOR_BGR2Lab);
	//============================================================================
	// Superpixel segmentation for a given number of superpixels
	//============================================================================
    cv::Mat_<int> DoSuperpixelSegmentation_ForGivenNumberOfSuperpixels(const cv::Mat& img,
                const int&					K,//required number of superpixels
                                                                       const double&                                   compactness, 
                                                                       const int colorconv = cv::COLOR_BGR2Lab);//10-20 is a good value for CIELAB space
	// //============================================================================
	// // Save superpixel labels in a text file in raster scan order
	// //============================================================================
	// void SaveSuperpixelLabels(
	// 	const int*&					labels,
	// 	const int&					width,
	// 	const int&					height,
	// 	const string&				filename,
	// 	const string&				path);
	//============================================================================
	// Function to draw boundaries around superpixels of a given 'color'.
	// Can also be used to draw boundaries around supervoxels, i.e layer by layer.
	//============================================================================
	void DrawContoursAroundSegments(
		unsigned int*&				segmentedImage,
		int*&						labels,
		const int&					width,
		const int&					height,
		const unsigned int&			color );

        void GetContoursAroundSegments(
                                       std::vector<cv::Point>& contours, 
                                       int*&					labels,
                                       const int&				width,
                                       const int&				height);

        void ComputeMeanSuperPixelColor(const cv::Mat& img, const int colorsp );

private:
	//============================================================================
	// The main SLIC algorithm for generating superpixels
	//============================================================================
	void PerformSuperpixelSLIC(
		std::vector<double>&				kseedsl,
		std::vector<double>&				kseedsa,
		std::vector<double>&				kseedsb,
		std::vector<double>&				kseedsx,
		std::vector<double>&				kseedsy,
		std::vector<int>&						klabels,
		const int&					STEP,
                const std::vector<double>&		edgemag,
		const double&				m = 10.0);
	//============================================================================
	// Pick seeds for superpixels when step size of superpixels is given.
	//============================================================================
	void GetLABXYSeeds_ForGivenStepSize(
		std::vector<double>&				kseedsl,
		std::vector<double>&				kseedsa,
		std::vector<double>&				kseedsb,
		std::vector<double>&				kseedsx,
		std::vector<double>&				kseedsy,
		const int&					STEP,
		const bool&					perturbseeds,
		const std::vector<double>&		edgemag);
	//============================================================================
	// Move the superpixel seeds to low gradient positions to avoid putting seeds
	// at region boundaries.
	//============================================================================
	void PerturbSeeds(
		std::vector<double>&				kseedsl,
		std::vector<double>&				kseedsa,
		std::vector<double>&				kseedsb,
		std::vector<double>&				kseedsx,
		std::vector<double>&				kseedsy,
		const std::vector<double>&		edges);
	//============================================================================
	// Detect color edges, to help PerturbSeeds()
	//============================================================================
    void DetectLabEdges(const cv::Mat_<cv::Vec3d>& lab, std::vector<double>& edges);
	//============================================================================
	// sRGB to XYZ conversion; helper for RGB2LAB()
	//============================================================================
	void RGB2XYZ(
		const int&					sR,
		const int&					sG,
		const int&					sB,
		double&						X,
		double&						Y,
		double&						Z);
	//============================================================================
	// sRGB to CIELAB conversion (uses RGB2XYZ function)
	//============================================================================
	void RGB2LAB(
		const int&					sR,
		const int&					sG,
		const int&					sB,
		double&						lval,
		double&						aval,
		double&						bval);
	//============================================================================
	// sRGB to CIELAB conversion for 2-D images
	//============================================================================
	void DoRGBtoLABConversion(
		const unsigned int*&		ubuff,
		std::vector<double>&					lvec,
		std::vector<double>&					avec,
		std::vector<double>&					bvec);
	//============================================================================
	// sRGB to CIELAB conversion for 3-D volumes
	//============================================================================
	void DoRGBtoLABConversion(
		unsigned int**&				ubuff,
		std::vector<std::vector<double> >&					lvec,
		std::vector<std::vector<double> >&					avec,
		std::vector<std::vector<double> >&					bvec);
	//============================================================================
	// Post-processing of SLIC segmentation, to avoid stray labels.
	//============================================================================
	void EnforceLabelConnectivity(
                                      const std::vector<int>&					labels,
                                      std::vector<int>&						nlabels,//input labels that need to be corrected to remove stray labels
		//int&						numlabels,//the number of labels changes in the end if segments are removed
		const int&					K); //the number of superpixels desired by the user


private:
    int m_width;
    int m_height;
    int m_depth;

    cv::Mat_<cv::Vec3d> m_lab;

 public: 
    
    // cv::Mat_<Vec3d> kseeds_lab;
    // cv::Mat_<Vec3d> mu_ch;

        std::vector<double> kseeds_l;
        std::vector<double> kseeds_a;
        std::vector<double> kseeds_b;

        std::vector<double> kseeds_x;
        std::vector<double> kseeds_y;
        std::vector<double> kseeds_z;


        std::vector<double> mu_ch1, mu_ch2, mu_ch3; 
        std::vector<double> cluster_count; 

        std::vector<int> klabel;
        int numlabels;

};

#endif // !defined(_SLICPP_H_INCLUDED_)
