#include "cv2.hpp"
#include "slic_generic.hpp"

BOOST_PYTHON_MODULE(pyslic)
{
    using namespace boost::python;
    class_<SLICGeneric>("SLICGeneric", init<>())
        .def("forGivenSuperpixelSize", &SLICGeneric::forGivenSuperpixelSize)
    ;
}
