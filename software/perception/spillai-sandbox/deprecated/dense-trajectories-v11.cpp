#include "dense-trajectories.hpp"

#include <bot_core/bot_core.h>

using namespace cv;


void InitTrackerInfo(TrackerInfo* tracker, int track_length, int init_gap)
{
	tracker->trackLength = track_length;
	tracker->initGap = init_gap;
}

DescMat* InitDescMat(int height, int width, int nBins)
{
	DescMat* descMat = (DescMat*)malloc(sizeof(DescMat));
	descMat->height = height;
	descMat->width = width;
	descMat->nBins = nBins;
	descMat->desc = (float*)malloc(height*width*nBins*sizeof(float));
	memset( descMat->desc, 0, height*width*nBins*sizeof(float));
	return descMat;
}

void ReleDescMat( DescMat* descMat)
{
	free(descMat->desc);
	free(descMat);
}

void InitDescInfo(DescInfo* descInfo, int nBins, int flag, int orientation, int size, int nxy_cell, int nt_cell)
{
	descInfo->nBins = nBins;
	descInfo->fullOrientation = orientation;
	descInfo->norm = 2;
	descInfo->threshold = min_flow;
	descInfo->flagThre = flag;
	descInfo->nxCells = nxy_cell;
	descInfo->nyCells = nxy_cell;
	descInfo->ntCells = nt_cell;
	descInfo->dim = descInfo->nBins*descInfo->nxCells*descInfo->nyCells;
	descInfo->blockHeight = size;
	descInfo->blockWidth = size;
}

void usage()
{
	fprintf(stderr, "Extract dense trajectories from a video\n\n");
	fprintf(stderr, "Usage: DenseTrack video_file [options]\n");
	fprintf(stderr, "Options:\n");
	fprintf(stderr, "  -h                        Display this message and exit\n");
	fprintf(stderr, "  -S [start frame]          The start frame to compute feature (default: S=0 frame)\n");
	fprintf(stderr, "  -E [end frame]            The end frame for feature computing (default: E=last frame)\n");
	fprintf(stderr, "  -L [trajectory length]    The length of the trajectory (default: L=15 frames)\n");
	fprintf(stderr, "  -W [sampling stride]      The stride for dense sampling feature points (default: W=5 pixels)\n");
	fprintf(stderr, "  -N [neighborhood size]    The neighborhood size for computing the descriptor (default: N=32 pixels)\n");
	fprintf(stderr, "  -s [spatial cells]        The number of cells in the nxy axis (default: nxy=2 cells)\n");
	fprintf(stderr, "  -t [temporal cells]       The number of cells in the nt axis (default: nt=3 cells)\n");
}

void arg_parse(int argc, char** argv)
{
	int c;
	char* executable = basename(argv[0]);
	while((c = getopt (argc, argv, "hS:E:L:W:N:s:t:")) != -1)
	switch(c) {
		case 'S':
		start_frame = atoi(optarg);
		break;
		case 'E':
		end_frame = atoi(optarg);
		break;
		case 'L':
		track_length = atoi(optarg);
		break;
		case 'W':
		min_distance = atoi(optarg);
		break;
		case 'N':
		patch_size = atoi(optarg);
		break;
		case 's':
		nxy_cell = atoi(optarg);
		break;
		case 't':
		nt_cell = atoi(optarg);
		break;

		case 'h':
		usage();
		exit(0);
		break;

		default:
		fprintf(stderr, "error parsing arguments at -%c\n  Try '%s -h' for help.", c, executable );
		abort();
	}
}

DenseTrajectories::DenseTrajectories() { 
    std::cerr << "dense traj. ctor" << std::endl;

    // inits
    fscales = 0;
    show_track = 1; 
    frameNum = 0;
    init_counter = 0;

    // Tracker inits
    InitTrackerInfo(&tracker, track_length, init_gap);
    InitDescInfo(&hogInfo, 8, 0, 1, patch_size, nxy_cell, nt_cell);
    InitDescInfo(&hofInfo, 9, 1, 1, patch_size, nxy_cell, nt_cell);
    InitDescInfo(&mbhInfo, 8, 0, 1, patch_size, nxy_cell, nt_cell);


#if 1
    // GFTT
    int gftt_maxCorners=800; double gftt_qualityLevel=0.04; double gftt_minDistance=10;
    int gftt_blockSize=7; bool gftt_useHarrisDetector=false; double gftt_k=0.04 ;
    _detector = cv::Ptr<cv::FeatureDetector>(
        new cv::GFTTDetector( gftt_maxCorners, gftt_qualityLevel,
                              gftt_minDistance, gftt_blockSize,
                              gftt_useHarrisDetector, gftt_k ));
#else 
    // FAST
    int fast_threshold = 120; bool nonmax = true; 
    _detector = cv::Ptr<cv::FeatureDetector>(
        new cv::FastFeatureDetector( fast_threshold, nonmax ));
#endif
    
    // Pyramid Detector
    detector = cv::Ptr<cv::PyramidAdaptedFeatureDetector>
        (new cv::PyramidAdaptedFeatureDetector(_detector, 0) );

    // Cloud Normals
    cloud_normals = pcl::PointCloud<pcl::Normal>::Ptr (new pcl::PointCloud<pcl::Normal> ());

}

DenseTrajectories::~DenseTrajectories() { 
    std::cerr << "dense traj. dtor" << std::endl;
}


inline bool isFlowCorrect(Point2f u)
{
    return !cvIsNaN(u.x) && !cvIsNaN(u.y) && fabs(u.x) < 1e9 && fabs(u.y) < 1e9;
}

static Vec3b computeColor(float fx, float fy)
{
    static bool first = true;

    // relative lengths of color transitions:
    // these are chosen based on perceptual similarity
    // (e.g. one can distinguish more shades between red and yellow
    //  than between yellow and green)
    const int RY = 15;
    const int YG = 6;
    const int GC = 4;
    const int CB = 11;
    const int BM = 13;
    const int MR = 6;
    const int NCOLS = RY + YG + GC + CB + BM + MR;
    static Vec3i colorWheel[NCOLS];

    if (first)
    {
        int k = 0;

        for (int i = 0; i < RY; ++i, ++k)
            colorWheel[k] = Vec3i(255, 255 * i / RY, 0);

        for (int i = 0; i < YG; ++i, ++k)
            colorWheel[k] = Vec3i(255 - 255 * i / YG, 255, 0);

        for (int i = 0; i < GC; ++i, ++k)
            colorWheel[k] = Vec3i(0, 255, 255 * i / GC);

        for (int i = 0; i < CB; ++i, ++k)
            colorWheel[k] = Vec3i(0, 255 - 255 * i / CB, 255);

        for (int i = 0; i < BM; ++i, ++k)
            colorWheel[k] = Vec3i(255 * i / BM, 0, 255);

        for (int i = 0; i < MR; ++i, ++k)
            colorWheel[k] = Vec3i(255, 0, 255 - 255 * i / MR);

        first = false;
    }

    const float rad = sqrt(fx * fx + fy * fy);
    const float a = atan2(-fy, -fx) / (float)CV_PI;

    const float fk = (a + 1.0f) / 2.0f * (NCOLS - 1);
    const int k0 = static_cast<int>(fk);
    const int k1 = (k0 + 1) % NCOLS;
    const float f = fk - k0;

    Vec3b pix;

    for (int b = 0; b < 3; b++)
    {
        const float col0 = colorWheel[k0][b] / 255.f;
        const float col1 = colorWheel[k1][b] / 255.f;

        float col = (1 - f) * col0 + f * col1;

        if (rad <= 1)
            col = 1 - rad * (1 - col); // increase saturation with radius
        else
            col *= .75; // out of range

        pix[2 - b] = static_cast<uchar>(255.f * col);
    }

    return pix;
}

static void drawOpticalFlow(const Mat_<Point2f>& flow, Mat& dst, float maxmotion = -1)
{
    dst.create(flow.size(), CV_8UC3);
    dst.setTo(Scalar::all(0));

    // determine motion rang:e
    float maxrad = maxmotion;

    if (maxmotion <= 0)
    {
        maxrad = 1;
        for (int y = 0; y < flow.rows; ++y)
        {
            for (int x = 0; x < flow.cols; ++x)
            {
                Point2f u = flow(y, x);

                if (!isFlowCorrect(u))
                    continue;

                maxrad = max(maxrad, sqrt(u.x * u.x + u.y * u.y));
            }
        }
    }

    for (int y = 0; y < flow.rows; ++y)
    {
        for (int x = 0; x < flow.cols; ++x)
        {
            Point2f u = flow(y, x);

            if (isFlowCorrect(u))
                dst.at<Vec3b>(y, x) = computeColor(u.x / maxrad, u.y / maxrad);
        }
    }
}

void
DenseTrajectories::draw(cv::Mat& frame_) { 

}

void DenseTrajectories::normal_estimation(pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, 
                                          pcl::PointCloud<pcl::Normal>::Ptr& cloud_normals) { 
    // Normal estimation
#define INTEGRAL_IMAGE_NORMAL 1
#if INTEGRAL_IMAGE_NORMAL
    pcl::IntegralImageNormalEstimation<pcl::PointXYZRGB, pcl::Normal> ne;
#else 
    pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> ne;
#endif

// #if 0
//     pcl::VoxelGrid<pcl::PointXYZRGB> sor;
//     sor.setInputCloud (cloud->makeShared ());
//     sor.setLeafSize (0.05f, 0.05f, 0.05f); //was 0.01
//     sor.filter (*cloud_decimated);
// #else    
//     // Decimate point cloud by DEPTH_SCALE
//     cloud_decimated->width    =(int) (cloud->width/ (double) DEPTH_SCALE) ;
//     cloud_decimated->height   =(int) (cloud->height/ (double) DEPTH_SCALE);
//     cloud_decimated->is_dense = true;
//     cloud_decimated->points.resize (cloud_decimated->width * cloud_decimated->height);

//     boost::shared_ptr<vector<int> > search_inds (new vector<int> ()); // cloud_decimated->points.size()));
//     for(int v=0, j=0; v<cloud->height; v+=DEPTH_SCALE)
//         for(int u=0; u<cloud->width; u+=DEPTH_SCALE ) { 
//             int idx = v*cloud->width+u; 
//             search_inds->push_back(j);
//             cloud_decimated->points[j++] = cloud->points[idx];
//         }
// #endif
            
#if INTEGRAL_IMAGE_NORMAL
    // options: COVARIANCE_MATRIX, AVERAGE_3D_GRADIENT, AVERAGE_DEPTH_CHANGE, SIMPLE_3D_GRADIENT
    ne.setNormalEstimationMethod (ne.AVERAGE_3D_GRADIENT);
    ne.setDepthDependentSmoothing(true);
    ne.setMaxDepthChangeFactor(0.5f);
    ne.setNormalSmoothingSize(15.0f);    
    // options: BORDER_POLICY_MIRROR, BORDER_POLICY_IGNORE
    ne.setBorderPolicy(ne.BORDER_POLICY_MIRROR);
    ne.setInputCloud(cloud);
    ne.compute(*cloud_normals);
    
    boost::shared_ptr<vector<int> > indices (new vector<int> ());
    for (int j=0; j<cloud_normals->points.size(); j++) 
        if (!((cloud_normals->points[j].normal[0] != cloud_normals->points[j].normal[0]) || 
              (cloud_normals->points[j].normal[1] != cloud_normals->points[j].normal[1]) || 
              (cloud_normals->points[j].normal[2] != cloud_normals->points[j].normal[2])))
            indices->push_back(j);
        else
            cloud_normals->points[j].normal[0] = 0, 
                cloud_normals->points[j].normal[1] = 0, 
                cloud_normals->points[j].normal[2] = 1;

    // Setup kd-tree
    // cloud_decimated_tree->setInputCloud(cloud_decimated, indices);
    // cloud_decimated_tree->setInputCloud(cloud_decimated);
#else
    ne.setInputCloud(cloud_decimated);
    // ne.setIndices(search_inds);
    ne.setSearchMethod(cloud_decimated_tree);
    // ne.setRadiusSearch(0.05);
    ne.setKSearch(9);
    ne.compute(*cloud_normals);
#endif
    std::cout << "Estimated the decimated normals" << std::endl;

    return;

}

template<typename T1, typename T2>
static void muvar_img(const cv::Mat_<T1>& img, cv::Mat_<T2>& mu, cv::Mat_<T2>& var) { 

    cv::blur(img, mu, cv::Size(7, 7));

    cv::Mat_<T2> mu2;
    cv::blur(img.mul(img), mu2, cv::Size(7, 7));

    var = mu2 - mu.mul(mu);

    return;
}

cv::Mat1b 
DenseTrajectories::getValidNormalsMask(const cv::Mat& frame_, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud) { 

    bool debug = false;

    //----------------------------------
    // Normal estimation
    //----------------------------------
    double tic = bot_timestamp_now();
    normal_estimation(cloud, cloud_normals);
    printf("===> NORMAL_ESTIMATION: %4.2f ms\n", (bot_timestamp_now() - tic) * 1e-3); 


    //----------------------------------
    // Convert cloud to cv::Mat
    //----------------------------------
    cv::Mat_<Vec3f> cloudm(cloud->height, cloud->width);
    for (int y=0,j=0; y<cloud->height; y++) { 
        for (int x=0; x<cloud->width; x++, j++) { 
            if (!((cloud->points[j].x != cloud->points[j].x) || 
                  (cloud->points[j].y != cloud->points[j].y) || 
                  (cloud->points[j].z != cloud->points[j].z))) { 
                cv::Vec3f n(cloud->points[j].x, 
                            cloud->points[j].y, 
                            cloud->points[j].z);
                cloudm(y,x) = n;
            }
            else 
                cloudm(y,x) = cv::Vec3f(0,0,0);
        }
    }
    if (debug) cv::imshow("cloud", cloudm);


    //----------------------------------
    // Convert normals to cv::Mat
    //----------------------------------
    cv::Mat_<Vec3f> normals(cloud_normals->height, cloud_normals->width);
    for (int y=0,j=0; y<cloud_normals->height; y++) { 
        for (int x=0; x<cloud_normals->width; x++, j++) { 
            if (!((cloud_normals->points[j].normal[0] != cloud_normals->points[j].normal[0]) || 
                  (cloud_normals->points[j].normal[1] != cloud_normals->points[j].normal[1]) || 
                  (cloud_normals->points[j].normal[2] != cloud_normals->points[j].normal[2]))) { 
                cv::Vec3f n(cloud_normals->points[j].normal[0], 
                            cloud_normals->points[j].normal[1], 
                            cloud_normals->points[j].normal[2]);
                normals(y,x) = n;
            }
        }
    }
    if (debug) cv::imshow("normals", normals);

    //----------------------------------
    // Compute Difference of Normals
    // Compute small and large scale blur
    //----------------------------------
    cv::Mat_<Vec3f> normals_small_scale, normals_large_scale;
    cv::blur(normals, normals_small_scale, cv::Size(5,5));
    cv::blur(normals, normals_large_scale, cv::Size(15,15));

    cv::Mat_<Vec3f> normals_DoN = 0.5 * (normals_small_scale - normals_large_scale);
    if (debug) cv::imshow("normals_DoN", normals_DoN);


    //----------------------------------
    // Compute Mean and Variance
    //----------------------------------
    cv::Mat_<Vec3f> mu, var;
    muvar_img<Vec3f, Vec3f>(normals, mu, var);
    if (debug) cv::imshow("mu", mu);
    if (debug) cv::imshow("var", var);

    //----------------------------------
    // Compute invalid normals mask
    //----------------------------------
    cv::Mat1b mask = cv::Mat1b::zeros(normals.size());

    //----------------------------------
    // NaN normals
    //----------------------------------
    for (int j=0; j<normals.cols * normals.rows; j++) 
        if (normals.at<Vec3f>(j) == Vec3f(0,0,1))
            mask.at<unsigned char>(j) = 255;

#if 1
    //----------------------------------
    // High variance normal regions
    //----------------------------------
    float var_thresh = 0.04;
    for (int j=0; j<var.cols * var.rows; j++) {
        Vec3f v = var.at<Vec3f>(j);
        if (v[0] >= var_thresh || v[1] >= var_thresh || v[2] >= var_thresh)
            mask.at<unsigned char>(j) = 128;
    }

#else
    //----------------------------------
    // High difference of normal regions
    //----------------------------------
    float var_thresh = 0.2;
    for (int j=0; j<var.cols * var.rows; j++) {
        if (cv::norm(normals_DoN.at<Vec3f>(j)) >= var_thresh)
            mask.at<unsigned char>(j) = 128;
    }
#endif

    cv::imshow("mask", mask);

    cv::Mat1b valid_mask = (mask == 0);
    return valid_mask;
}

void 
DenseTrajectories::update(const cv::Mat& frame_, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, cv::Mat& out_) { 

    // cv::Mat1b mask = getValidNormalsMask(frame_, cloud);
    cv::Mat1b mask;
    update(frame_, mask, out_);
    
    return;
}

// void
// DenseTrajectories::update(const unsigned int* ubuff, const int width, const int height, int ch) { 
//     cv::Mat frame_(height, width, CV_8UC3, (unsigned int*)ubuff);
//     cv::Mat1b mask = cv::Mat1b::ones(frame_.size());
//     cv::Mat out; 
//     update(frame_, mask, out);
//     // cv::imshow("out", out);
// }

void
DenseTrajectories::update(const cv::Mat& frame_, cv::Mat& mask, cv::Mat& out_) { 
    IplImage out_ipl(out_);
    IplImage* out = &out_ipl;

    IplImage frame_ipl(frame_);
    IplImage* frame = &frame_ipl;
    int i, j, c;

    if( !image ) {
        // initailize all the buffers
        image = IplImageWrapper( cvGetSize(frame), 8, 3 );
        image->origin = frame->origin;
        prev_image= IplImageWrapper( cvGetSize(frame), 8, 3 );
        prev_image->origin = frame->origin;
        grey = IplImageWrapper( cvGetSize(frame), 8, 1 );
        grey_pyramid = IplImagePyramid( cvGetSize(frame), 8, 1, scale_stride );
        prev_grey = IplImageWrapper( cvGetSize(frame), 8, 1 );
        prev_grey_pyramid = IplImagePyramid( cvGetSize(frame), 8, 1, scale_stride );
        eig_pyramid = IplImagePyramid( cvGetSize(frame), 32, 1, scale_stride );

        cvCopy( frame, image, 0 );
        cvCvtColor( image, grey, CV_BGR2GRAY );
        grey_pyramid.rebuild( grey );

        // how many scale we can have
        scale_num = std::min<std::size_t>(scale_num, grey_pyramid.numOfLevels());
        fscales = (float*)cvAlloc(scale_num*sizeof(float));
        xyScaleTracks.resize(scale_num);

        for( int ixyScale = 0; ixyScale < scale_num; ++ixyScale ) {
            std::list<Track>& tracks = xyScaleTracks[ixyScale];
            fscales[ixyScale] = pow(scale_stride, ixyScale);

            // find good features at each scale separately
            IplImage *grey_temp = 0, *eig_temp = 0;
            std::size_t temp_level = (std::size_t)ixyScale;
            grey_temp = cvCloneImage(grey_pyramid.getImage(temp_level));
            eig_temp = cvCloneImage(eig_pyramid.getImage(temp_level));

            std::vector<CvPoint2D32f> points(0);
            // # spillai: modified (removed dense sampling)
#if 1
            cvDenseSample(grey_temp, eig_temp, points, quality, min_distance);
#else
            // # spillai: modified (removed dense sampling)
            // Mask out regions where features exist
            cv::Mat grey_temp_mat = cv::cvarrToMat(grey_temp);

            cv::Mat smask; 
            cv::resize(mask, smask, grey_temp_mat.size(), 0, 0, cv::INTER_NEAREST);

            std::vector<cv::KeyPoint> c_kpts;
            detector->detect( grey_temp_mat, c_kpts, smask );
            std::cerr << "c_kpts: " << c_kpts.size() << std::endl;
            // Set the neeed parameters to find the refined corners
            cv::Size winSize = cv::Size( 5, 5 );
            cv::Size zeroZone = cv::Size( -1, -1 );
            cv::TermCriteria criteria = TermCriteria( cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 40, 0.001 );

            std::vector<cv::Point2f> cp_kpts;
            cv::KeyPoint::convert(c_kpts, cp_kpts); 

            // Calculate the refined corner locations
            cornerSubPix( grey_temp_mat, cp_kpts, winSize, zeroZone, criteria );

            cv::KeyPoint::convert(cp_kpts, c_kpts);


            points.resize(c_kpts.size());
            for (int j=0; j<c_kpts.size(); j++)
                points[j].x = c_kpts[j].pt.x, points[j].y = c_kpts[j].pt.y;            
#endif
            // save the feature points
            for( i = 0; i < points.size(); i++ ) {
                Track track(tracker.trackLength);
                PointDesc point(hogInfo, hofInfo, mbhInfo, points[i]);
                track.addPointDesc(point);
                tracks.push_back(track);
            }

            cvReleaseImage( &grey_temp );
            cvReleaseImage( &eig_temp );
        }
    }

    // build the image pyramid for the current frame
    cvCopy( frame, image, 0 );
    cvCvtColor( image, grey, CV_BGR2GRAY );
    grey_pyramid.rebuild(grey);

    if( frameNum > 0 ) {
        init_counter++;
        for( int ixyScale = 0; ixyScale < scale_num; ++ixyScale ) {
            // track feature points in each scale separately
            std::vector<CvPoint2D32f> points_in(0);
            std::list<Track>& tracks = xyScaleTracks[ixyScale];
            for (std::list<Track>::iterator iTrack = tracks.begin(); 
                 iTrack != tracks.end(); ++iTrack) {
                CvPoint2D32f point = iTrack->pointDescs.back().point;
                points_in.push_back(point); // collect all the feature points
            }
            int count = points_in.size();
            IplImage *prev_grey_temp = 0, *grey_temp = 0;
            std::size_t temp_level = ixyScale;
            prev_grey_temp = cvCloneImage(prev_grey_pyramid.getImage(temp_level));
            grey_temp = cvCloneImage(grey_pyramid.getImage(temp_level));

            cv::Mat prev_grey_mat = cv::cvarrToMat(prev_grey_temp);
            cv::Mat grey_mat = cv::cvarrToMat(grey_temp);

            std::vector<int> status(count);
            std::vector<CvPoint2D32f> points_out(count);

            // compute the optical flow
            IplImage* flow = cvCreateImage(cvGetSize(grey_temp), IPL_DEPTH_32F, 2);
            cv::Mat flow_mat = cv::cvarrToMat(flow);

            double t1 = bot_timestamp_now();

            float pyrScale = sqrt(2)/2.0;
            int numLevels = 3; 
            int winSize = 5;
            int numIters = 2; 
            int polyN = 7; 
            float polySigma = 1.5; 
            int flags = cv::OPTFLOW_FARNEBACK_GAUSSIAN;

            cv::calcOpticalFlowFarneback( prev_grey_mat ,grey_mat, flow_mat, pyrScale, numLevels, winSize,
                                          numIters, polyN, polySigma, flags);

            printf("===> OF Farneback TIME: %4.2f ms\n", (bot_timestamp_now() - t1) * 1e-3); 

            if (ixyScale == 0) { 
                std::cerr << "flow_mat: " << flow_mat.size() << " " << grey_mat.size() << std::endl;
                // Mat out;
                // drawOpticalFlow(flow_mat, out);
                // opencv_utils::imshow("Flow", out);
            }

            // track feature points by median filtering
            OpticalFlowTracker(flow, points_in, points_out, status);

            int width = grey_temp->width;
            int height = grey_temp->height;

            // # spillai: modified
            // // compute the integral histograms
            // DescMat* hogMat = InitDescMat(height, width, hogInfo.nBins);
            // HogComp(prev_grey_temp, hogMat, hogInfo);

            // DescMat* hofMat = InitDescMat(height, width, hofInfo.nBins);
            // HofComp(flow, hofMat, hofInfo);

            // DescMat* mbhMatX = InitDescMat(height, width, mbhInfo.nBins);
            // DescMat* mbhMatY = InitDescMat(height, width, mbhInfo.nBins);
            // MbhComp(flow, mbhMatX, mbhMatY, mbhInfo);

            std::vector<cv::Scalar> track_colors(tracks.size());
            opencv_utils::fillColors(track_colors);

            // draw(frame);
            
            i = 0;
            for (std::list<Track>::iterator iTrack = tracks.begin(); iTrack != tracks.end(); ++i) {
                if( status[i] == 1 ) { // if the feature point is successfully tracked
                    PointDesc& pointDesc = iTrack->pointDescs.back();
                    CvPoint2D32f prev_point = points_in[i];

                    // # spillai: modified
                    // get the descriptors for the feature point
                    // CvScalar rect = getRect(prev_point, cvSize(width, height), hogInfo);
                    // pointDesc.hog = getDesc(hogMat, rect, hogInfo);
                    // pointDesc.hof = getDesc(hofMat, rect, hofInfo);
                    // pointDesc.mbhX = getDesc(mbhMatX, rect, mbhInfo);
                    // pointDesc.mbhY = getDesc(mbhMatY, rect, mbhInfo);

                    PointDesc point(hogInfo, hofInfo, mbhInfo, points_out[i]);
                    iTrack->addPointDesc(point);

                    // draw this track
                    if( show_track == 1 ) {
                        std::list<PointDesc>& descs = iTrack->pointDescs;
                        std::list<PointDesc>::iterator iDesc = descs.begin();
                        float length = descs.size();
                        CvPoint2D32f point0 = iDesc->point;
                        point0.x *= fscales[ixyScale]; // map the point to first scale
                        point0.y *= fscales[ixyScale];

                        float j = 0;
                        for (iDesc++; iDesc != descs.end(); ++iDesc, ++j) {
                            CvPoint2D32f point1 = iDesc->point;
                            point1.x *= fscales[ixyScale];
                            point1.y *= fscales[ixyScale];

                            cvLine(out, cvPointFrom32f(point0), cvPointFrom32f(point1),
                                   CV_RGB(0,cvFloor(255.0*(j+1.0)/length),0), 1, CV_AA,0);
                            point0 = point1;
                        }
                        cvCircle(out, cvPointFrom32f(point0), 1, CV_RGB(255,0,0), -1, CV_AA,0);
                    }
                    ++iTrack;
                }
                else // remove the track, if we lose feature point
                    iTrack = tracks.erase(iTrack);
            }
            // # spillai: modified
            // ReleDescMat(hogMat);
            // ReleDescMat(hofMat);
            // ReleDescMat(mbhMatX);
            // ReleDescMat(mbhMatY);
            cvReleaseImage( &prev_grey_temp );
            cvReleaseImage( &grey_temp );
            cvReleaseImage( &flow );
        }

        for( int ixyScale = 0; ixyScale < scale_num; ++ixyScale ) {
            std::list<Track>& tracks = xyScaleTracks[ixyScale]; // output the features for each scale
            for( std::list<Track>::iterator iTrack = tracks.begin(); 
                 iTrack != tracks.end(); ) {
                if( iTrack->pointDescs.size() >= tracker.trackLength+1 ) { // if the trajectory achieves the length we want
                    std::vector<CvPoint2D32f> trajectory(tracker.trackLength+1);
                    std::list<PointDesc>& descs = iTrack->pointDescs;
                    std::list<PointDesc>::iterator iDesc = descs.begin();

                    for (int count = 0; count <= tracker.trackLength; ++iDesc, ++count) {
                        trajectory[count].x = iDesc->point.x*fscales[ixyScale];
                        trajectory[count].y = iDesc->point.y*fscales[ixyScale];
                    }
                    float mean_x(0), mean_y(0), var_x(0), var_y(0), length(0);

                    // # spillai: modified
                    if( isValid(trajectory, mean_x, mean_y, var_x, var_y, length) == 1 ) {
                    //     printf("%d\t", frameNum);
                    //     printf("%f\t%f\t", mean_x, mean_y);
                    //     printf("%f\t%f\t", var_x, var_y);
                    //     printf("%f\t", length);
                    //     printf("%f\t", fscales[ixyScale]);

                    //     for (int count = 0; count < tracker.trackLength; ++count)
                    //         printf("%f\t%f\t", trajectory[count].x,trajectory[count].y );

                    //     iDesc = descs.begin();
                    //     int t_stride = cvFloor(tracker.trackLength/hogInfo.ntCells);
                    //     for( int n = 0; n < hogInfo.ntCells; n++ ) {
                    //         std::vector<float> vec(hogInfo.dim);
                    //         for( int t = 0; t < t_stride; t++, iDesc++ )
                    //             for( int m = 0; m < hogInfo.dim; m++ )
                    //                 vec[m] += iDesc->hog[m];
                    //         for( int m = 0; m < hogInfo.dim; m++ )
                    //             printf("%f\t", vec[m]/float(t_stride));
                    //     }

                    //     iDesc = descs.begin();
                    //     t_stride = cvFloor(tracker.trackLength/hofInfo.ntCells);
                    //     for( int n = 0; n < hofInfo.ntCells; n++ ) {
                    //         std::vector<float> vec(hofInfo.dim);
                    //         for( int t = 0; t < t_stride; t++, iDesc++ )
                    //             for( int m = 0; m < hofInfo.dim; m++ )
                    //                 vec[m] += iDesc->hof[m];
                    //         for( int m = 0; m < hofInfo.dim; m++ )
                    //             printf("%f\t", vec[m]/float(t_stride));
                    //     }

                    //     iDesc = descs.begin();
                    //     t_stride = cvFloor(tracker.trackLength/mbhInfo.ntCells);
                    //     for( int n = 0; n < mbhInfo.ntCells; n++ ) {
                    //         std::vector<float> vec(mbhInfo.dim);
                    //         for( int t = 0; t < t_stride; t++, iDesc++ )
                    //             for( int m = 0; m < mbhInfo.dim; m++ )
                    //                 vec[m] += iDesc->mbhX[m];
                    //         for( int m = 0; m < mbhInfo.dim; m++ )
                    //             printf("%f\t", vec[m]/float(t_stride));
                    //     }

                    //     iDesc = descs.begin();
                    //     t_stride = cvFloor(tracker.trackLength/mbhInfo.ntCells);
                    //     for( int n = 0; n < mbhInfo.ntCells; n++ ) {
                    //         std::vector<float> vec(mbhInfo.dim);
                    //         for( int t = 0; t < t_stride; t++, iDesc++ )
                    //             for( int m = 0; m < mbhInfo.dim; m++ )
                    //                 vec[m] += iDesc->mbhY[m];
                    //         for( int m = 0; m < mbhInfo.dim; m++ )
                    //             printf("%f\t", vec[m]/float(t_stride));
                    //     }

                    //     printf("\n");
                    }
                    iTrack = tracks.erase(iTrack);
                }
                else
                    iTrack++;
            }
        }

        if( init_counter == tracker.initGap ) { // detect new feature points every initGap frames
            init_counter = 0;
            for (int ixyScale = 0; ixyScale < scale_num; ++ixyScale) {
                std::list<Track>& tracks = xyScaleTracks[ixyScale];
                std::vector<CvPoint2D32f> points_in(0);
                std::vector<CvPoint2D32f> points_out(0);
                for(std::list<Track>::iterator iTrack = tracks.begin(); 
                    iTrack != tracks.end(); iTrack++, i++) {
                    std::list<PointDesc>& descs = iTrack->pointDescs;
                    CvPoint2D32f point = descs.back().point; // the last point in the track
                    points_in.push_back(point);
                }

                IplImage *grey_temp = 0, *eig_temp = 0;
                std::size_t temp_level = (std::size_t)ixyScale;
                grey_temp = cvCloneImage(grey_pyramid.getImage(temp_level));
                eig_temp = cvCloneImage(eig_pyramid.getImage(temp_level));

#if 1
                cvDenseSample(grey_temp, eig_temp, points_in, points_out, 
                              quality, min_distance);
#else
                // # spillai: modified (removed dense sampling)
                // Mask out regions where features exist
                cv::Mat grey_temp_mat = cv::cvarrToMat(grey_temp);
                cv::Mat1b mask_ = cv::Mat1b::ones(grey_temp_mat.size());
                for (int j=0; j<points_in.size(); j++)
                    cv::circle(mask_, cv::Point(points_in[j].x,points_in[j].y), min_distance, cv::Scalar(0), 1, CV_AA);

                cv::Mat smask; 
                cv::resize(mask, smask, mask_.size(), 0, 0, cv::INTER_NEAREST);
                // Bit-wise AND of features and valid normals mask
                cv::bitwise_and(smask, mask_, mask_);
                std::cerr << "mask size: " << mask_.size() << std::endl;
                cv::imshow("mask_", mask_);

                // Recompute features
                std::vector<cv::KeyPoint> c_kpts;
                detector->detect( grey_temp_mat, c_kpts, mask_ );

                // Set the neeed parameters to find the refined corners
                cv::Size winSize = cv::Size( 5, 5 );
                cv::Size zeroZone = cv::Size( -1, -1 );
                cv::TermCriteria criteria = TermCriteria( cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 40, 0.001 );

                std::vector<cv::Point2f> cp_kpts;
                cv::KeyPoint::convert(c_kpts, cp_kpts); 

                // Calculate the refined corner locations
                cornerSubPix( grey_temp_mat, cp_kpts, winSize, zeroZone, criteria );

                cv::KeyPoint::convert(cp_kpts, c_kpts);


                points_out.resize(c_kpts.size());
                for (int j=0; j<c_kpts.size(); j++)
                    points_out[j].x = c_kpts[j].pt.x, points_out[j].y = c_kpts[j].pt.y;
#endif
                // save the new feature points
                for( i = 0; i < points_out.size(); i++) {
                    Track track(tracker.trackLength);
                    PointDesc point(hogInfo, hofInfo, mbhInfo, points_out[i]);
                    track.addPointDesc(point);
                    tracks.push_back(track);
                }
                cvReleaseImage( &grey_temp );
                cvReleaseImage( &eig_temp );
            }
        }
    }

    cvCopy( frame, prev_image, 0 );
    cvCvtColor( prev_image, prev_grey, CV_BGR2GRAY );
    prev_grey_pyramid.rebuild(prev_grey);


    // if( show_track == 1 ) {
    //     cvShowImage( "DenseTrack", image);
    //     c = cvWaitKey(3);
    //     // if((char)c == 27) break;
    // }
    // get the next frame
    frameNum++;

    return;
}


void
DenseTrajectories::write() { 

}
