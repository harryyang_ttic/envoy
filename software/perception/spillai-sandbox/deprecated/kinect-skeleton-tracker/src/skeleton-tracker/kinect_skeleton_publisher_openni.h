#ifndef __OPENNI_SKELETON_PUBLISHER_H__
#define __OPENNI_SKELETON_PUBLISHER_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <sys/time.h>
#include <unistd.h>
#include <inttypes.h>
#include <vector>
#include <iostream>
#include <sstream>

#include <zlib.h>
#include <glib.h>

#include <XnOpenNI.h>
#include <XnCodecIDs.h>
#include <XnCppWrapper.h>

#include <lcm/lcm-cpp.hpp>
#include <lcmtypes/kinect.hpp>
#include <lcmtypes/erlcm/kinect_person_tracker_status_t.hpp>

#include <er_common/path_util.h>
#include <bot_core/timestamp.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>

// optargs
#include <ConciseArgs>


using namespace std;

#define WIDTH 640
#define HEIGHT 480
#define PI 3.141592
#define CHECK_RC(nRetVal, what)                                         \
    if (nRetVal != XN_STATUS_OK)                                        \
	{                                                               \
         printf("%s failed: %s\n", what, xnGetStatusString(nRetVal));   \
         return nRetVal;                                                \
         }

enum { NumLinks = 16 };
struct Link { XnSkeletonJoint source; XnSkeletonJoint dest; };

// ! List of links.
static const Link skelLinkList(int i)
{
    static const Link list[NumLinks] =  {
        {XN_SKEL_HEAD, XN_SKEL_NECK},
        {XN_SKEL_NECK, XN_SKEL_LEFT_SHOULDER},
        {XN_SKEL_LEFT_SHOULDER, XN_SKEL_LEFT_ELBOW},
        {XN_SKEL_LEFT_ELBOW, XN_SKEL_LEFT_HAND},

        {XN_SKEL_NECK, XN_SKEL_RIGHT_SHOULDER},
        {XN_SKEL_RIGHT_SHOULDER, XN_SKEL_RIGHT_ELBOW},
        {XN_SKEL_RIGHT_ELBOW, XN_SKEL_RIGHT_HAND},

        {XN_SKEL_LEFT_SHOULDER, XN_SKEL_TORSO},
        {XN_SKEL_RIGHT_SHOULDER, XN_SKEL_TORSO},

        {XN_SKEL_TORSO, XN_SKEL_LEFT_HIP},
        {XN_SKEL_LEFT_HIP, XN_SKEL_LEFT_KNEE},
        {XN_SKEL_LEFT_KNEE, XN_SKEL_LEFT_FOOT},

        {XN_SKEL_TORSO, XN_SKEL_RIGHT_HIP},
        {XN_SKEL_RIGHT_HIP, XN_SKEL_RIGHT_KNEE},
        {XN_SKEL_RIGHT_KNEE, XN_SKEL_RIGHT_FOOT},

        {XN_SKEL_LEFT_HIP, XN_SKEL_RIGHT_HIP},
    };
    return list[i];
}

XnBool g_bNeedPose = FALSE;
XnChar g_strPose[20] = "";

//----------------------------------
// State representation
//----------------------------------
struct state_t {
    lcm::LCM lcm;

    BotParam   *b_server;
    BotFrames *b_frames;

    xn::Context        g_Context;
    xn::DepthGenerator g_DepthGenerator;
    xn::UserGenerator  g_UserGenerator;

state_t() : 
    b_server(NULL),
        b_frames(NULL)
    {
        //----------------------------------
        // Bot Param/frames init
        //----------------------------------
        b_server = bot_param_new_from_server(lcm.getUnderlyingLCM(), 1);
        b_frames = bot_frames_get_global (lcm.getUnderlyingLCM(), b_server);
    }

    ~state_t() { 
    }

    void* ptr() { 
        return (void*) this;
    }

    void publishTransforms(const std::string& channel);
};
state_t state;

struct SkeletonPublisherOptions { 
    bool vDEBUG;
    SkeletonPublisherOptions () : vDEBUG(true) {}
};
SkeletonPublisherOptions options;


#endif //  __OPENNI_SKELETON_PUBLISHER_H__
