#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <sys/time.h>
#include <unistd.h>
#include <inttypes.h>
#include <vector>

#include <glib.h>

#include <XnOpenNI.h>
#include <XnCodecIDs.h>
#include <XnCppWrapper.h>

#include "kinect_skeleton_publisher_openni.h"

#include <lcmtypes/kinect_image_msg_t.h>

int debug_mode=0;
XnBool g_bNeedPose   = FALSE;
XnChar g_strPose[20] = "";
int tracker_status = ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_IDLE;
kinect_image_msg_t user_labels_msg;

#define CHECK_RC(nRetVal, what)                                         \
    if (nRetVal != XN_STATUS_OK)                                        \
	{                                                               \
            printf("%s failed: %s\n", what, xnGetStatusString(nRetVal)); \
            return nRetVal;                                             \
	}

// xn::Context        g_Context;
// xn::DepthGenerator g_DepthGenerator;
// xn::UserGenerator  g_UserGenerator;

#define WIDTH 640
#define HEIGHT 480

static void usage(const char* progname)
{
  fprintf (stderr, "Usage: %s [options]\n"
                   "\n"
                   "Options:\n"
                   "  -l URL    Specify LCM URL\n"
                   "  -h        This help message\n", 
                   g_path_get_basename(progname));
  exit(1);
}

void XN_CALLBACK_TYPE User_NewUser(xn::UserGenerator& generator, XnUserID nId, void* pCookie) {
	printf("New User %d\n", nId);

	if (g_bNeedPose)
		g_UserGenerator.GetPoseDetectionCap().StartPoseDetection(g_strPose, nId);
	else
		g_UserGenerator.GetSkeletonCap().RequestCalibration(nId, TRUE);
}

void XN_CALLBACK_TYPE User_LostUser(xn::UserGenerator& generator, XnUserID nId, void* pCookie) {
	printf("Lost user %d\n", nId);
        tracker_status = ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_IDLE;
}

void XN_CALLBACK_TYPE UserCalibration_CalibrationStart(xn::SkeletonCapability& capability, XnUserID nId, void* pCookie) {
	printf("Calibration started for user %d\n", nId);
}

void XN_CALLBACK_TYPE UserCalibration_CalibrationEnd(xn::SkeletonCapability& capability, XnUserID nId, XnBool bSuccess, void* pCookie) {
	if (bSuccess) {
		printf("Calibration complete, start tracking user %d\n*****************************\n", nId);
		g_UserGenerator.GetSkeletonCap().StartTracking(nId);
                tracker_status = ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_TRACKING;
	}
	else {
		printf("Calibration failed for user %d\n-----------------------------\n", nId);
                tracker_status = ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_IDLE;
		if (g_bNeedPose)
			g_UserGenerator.GetPoseDetectionCap().StartPoseDetection(g_strPose, nId);
		else
			g_UserGenerator.GetSkeletonCap().RequestCalibration(nId, TRUE);
	}
}

void XN_CALLBACK_TYPE UserPose_PoseDetected(xn::PoseDetectionCapability& capability, XnChar const* strPose, XnUserID nId, void* pCookie) {
    printf("Pose %s detected for user %d\n", strPose, nId);
    g_UserGenerator.GetPoseDetectionCap().StopPoseDetection(nId);
    g_UserGenerator.GetSkeletonCap().RequestCalibration(nId, TRUE);
}

void publishUserLabels(lcm_t* lcm, const char* msg_channel) {
    // Scene for user tracking
    xn::SceneMetaData sceneMD;
    g_UserGenerator.GetUserPixels(0, sceneMD);

    const XnLabel* pLabel = sceneMD.Data();
    user_labels_msg.timestamp = bot_timestamp_now();

    unsigned char* user_mask_ptr = user_labels_msg.image_data;
    for (int i = 0; i < sceneMD.XRes()*sceneMD.YRes(); ++i) {
        user_mask_ptr[i] = pLabel[i];
    }
    //memcpy(user_labels_msg.image_data, pLabel, sceneMD.XRes()*sceneMD.YRes());
    kinect_image_msg_t_publish(lcm, msg_channel, &user_labels_msg);
    return;
}

void publish2DSkeleton(lcm_t* lcm, const char* msg_channel) { 
    XnUserID users[15];
    XnUInt16 users_count = 15;
    g_UserGenerator.GetUsers(users, users_count);

    int64_t time_now = bot_timestamp_now();
    for (int i = 0; i < users_count; ++i) {
        XnUserID user = users[i];
        if (!g_UserGenerator.GetSkeletonCap().IsTracking(user))
            continue;

        kinect_skeleton_msg_t skeleton_msg;
        skeleton_msg.utime = time_now;
        skeleton_msg.num_links = NumLinks;
        skeleton_msg.user_id = user;  // double
        
        XnPoint3D ni_p[2*NumLinks];
        std::vector<kinect_link_msg_t> _links(NumLinks);
        for (int j = 0; j < NumLinks; ++j) {
            XnSkeletonJointPosition joint_position;
            g_UserGenerator.GetSkeletonCap().
                GetSkeletonJointPosition(user, skelLinkList(j).source, joint_position);
            ni_p[2*j] = joint_position.position;

            g_UserGenerator.GetSkeletonCap().
                GetSkeletonJointPosition(user, skelLinkList(j).dest, joint_position);
            ni_p[2*j+1] = joint_position.position;
        }

        g_DepthGenerator.ConvertRealWorldToProjective(2*NumLinks, ni_p, ni_p);
        
        for (int j=0; j<NumLinks; j++) {
            _links[j].joint_id = skelLinkList(j).dest;
            _links[j].source.x = ni_p[2*j].X, _links[j].source.y = ni_p[2*j].Y, _links[j].source.z = 1;
            _links[j].dest.x = ni_p[2*j+1].X, _links[j].dest.y = ni_p[2*j+1].Y, _links[j].dest.z = 1;
        }

        // Skeleton msg
        skeleton_msg.links = &_links[0];
        kinect_skeleton_msg_t_publish(lcm, msg_channel, &skeleton_msg);
    }
    return;
}

int main(int argc, char** argv)
{

    int c;
    char *lcm_url = NULL;
    // command line options - to publish on a specific url  
    while ((c = getopt (argc, argv, "hdir:jq:zl:")) >= 0) {
        switch (c) {
        case 'l':
            lcm_url = strdup(optarg);
            printf("Using LCM URL \"%s\"\n", lcm_url);
            break;
        case 'd':
            debug_mode=1;
            printf("Debug mode\n");
            break;
        case 'h':
        case '?':
            usage(argv[0]);
        }
    }

    // LCM-related
    lcm_t* lcm = lcm_create(lcm_url);
    char* msg_channel = g_strdup("KINECT_SKELETON_FRAME");

    // Allocate user labels
    user_labels_msg.width = WIDTH;
    user_labels_msg.height = HEIGHT;
    user_labels_msg.image_data_nbytes = 640*480;
    user_labels_msg.image_data_format = KINECT_IMAGE_MSG_T_VIDEO_IR_8BIT;
    user_labels_msg.image_data = (uint8_t*) malloc(WIDTH * HEIGHT);

    if(!lcm) {
        printf("Unable to initialize LCM\n");
        return 1;
    }


    char config_file[2048];
    sprintf(config_file, "%s/openni_tracker.xml", getConfigPath());
    printf("Initialize from xml: %s\n", config_file);
    XnStatus nRetVal = g_Context.InitFromXmlFile(config_file);
    CHECK_RC(nRetVal, "InitFromXml");

    // Depth Generator
    nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_DEPTH, g_DepthGenerator);
    CHECK_RC(nRetVal, "Find depth generator");

    // User Generator
    nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_USER, g_UserGenerator);
    if (nRetVal != XN_STATUS_OK) {
        nRetVal = g_UserGenerator.Create(g_Context);
        CHECK_RC(nRetVal, "Find user generator");
    }

    // Skeleton Cap
    if (!g_UserGenerator.IsCapabilitySupported(XN_CAPABILITY_SKELETON)) {
        printf("Supplied user generator doesn't support skeleton\n");
        return 1;
    }

    XnCallbackHandle hUserCallbacks;
    g_UserGenerator.RegisterUserCallbacks(User_NewUser, User_LostUser, NULL, hUserCallbacks);


    XnCallbackHandle hCalibrationCallbacks;
    g_UserGenerator.GetSkeletonCap().RegisterCalibrationCallbacks(UserCalibration_CalibrationStart, UserCalibration_CalibrationEnd, NULL, hCalibrationCallbacks);

    if (g_UserGenerator.GetSkeletonCap().NeedPoseForCalibration()) {
        g_bNeedPose = TRUE;
        if (!g_UserGenerator.IsCapabilitySupported(XN_CAPABILITY_POSE_DETECTION)) {
            printf("Pose required, but not supported\n");
            return 1;
        }

        XnCallbackHandle hPoseCallbacks;
        g_UserGenerator.GetPoseDetectionCap().RegisterToPoseCallbacks(UserPose_PoseDetected, NULL, NULL, hPoseCallbacks);

        g_UserGenerator.GetSkeletonCap().GetCalibrationPose(g_strPose);
    }

    g_UserGenerator.GetSkeletonCap().SetSkeletonProfile(XN_SKEL_PROFILE_ALL);

    nRetVal = g_Context.StartGeneratingAll();
    CHECK_RC(nRetVal, "StartGenerating");
    printf("Skeleton tracker running..\nWaiting for calibration pose.. \n");

    while (true) { 
        g_Context.WaitAndUpdateAll();
        publishUserLabels(lcm,"KINECT_PERSON_TRACKER_LABELS");
        publish2DSkeleton(lcm,"KINECT_SKELETON_2D");
    }


    kinect_image_msg_t_destroy(&user_labels_msg);
    lcm_destroy(lcm);
  return 0;
}
