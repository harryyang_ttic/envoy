#include "kinect_skeleton_publisher_openni.h"
#include <complex>

int debug_mode=0;
int tracker_status = erlcm::kinect_person_tracker_status_t::STATUS_IDLE;

static
void XN_CALLBACK_TYPE User_NewUser(xn::UserGenerator& generator, 
                                   XnUserID nId, void* pCookie) {
    printf("New User %d\n", nId);
    state_t* st = (state_t*) pCookie;
    if (g_bNeedPose)
        st->g_UserGenerator.GetPoseDetectionCap().StartPoseDetection(g_strPose, nId);
    else
        st->g_UserGenerator.GetSkeletonCap().RequestCalibration(nId, TRUE);
}

static 
void XN_CALLBACK_TYPE User_LostUser(xn::UserGenerator& generator, 
                                    XnUserID nId, void* pCookie) {
    printf("Lost user %d\n", nId);
    tracker_status = erlcm::kinect_person_tracker_status_t::STATUS_IDLE;
}

static 
void XN_CALLBACK_TYPE UserCalibration_CalibrationStart(xn::SkeletonCapability& capability, 
                                                       XnUserID nId, void* pCookie) {
    printf("Calibration started for user %d\n", nId);
}

static 
void XN_CALLBACK_TYPE UserCalibration_CalibrationEnd(xn::SkeletonCapability& capability, 
                                                     XnUserID nId, XnBool bSuccess, void* pCookie) {
    state_t* st = (state_t*) pCookie;
    if (bSuccess) {
        printf("Calibration complete, start tracking user %d\n*****************************\n", nId);
        st->g_UserGenerator.GetSkeletonCap().StartTracking(nId);
        tracker_status = erlcm::kinect_person_tracker_status_t::STATUS_TRACKING;
    }
    else {
        printf("Calibration failed for user %d\n-----------------------------\n", nId);
        tracker_status = erlcm::kinect_person_tracker_status_t::STATUS_IDLE;
        if (g_bNeedPose)
            st->g_UserGenerator.GetPoseDetectionCap().StartPoseDetection(g_strPose, nId);
        else
            st->g_UserGenerator.GetSkeletonCap().RequestCalibration(nId, TRUE);
    }
}

static 
void XN_CALLBACK_TYPE UserPose_PoseDetected(xn::PoseDetectionCapability& capability, 
                                                     XnChar const* strPose, XnUserID nId, 
                                                     void* pCookie) {
    state_t* st = (state_t*) pCookie;
    printf("Pose %s detected for user %d\n", strPose, nId);
    st->g_UserGenerator.GetPoseDetectionCap().StopPoseDetection(nId);
    st->g_UserGenerator.GetSkeletonCap().RequestCalibration(nId, TRUE);
}

static 
void XN_CALLBACK_TYPE UserPose_PoseLost(xn::PoseDetectionCapability& capability, 
                                        XnChar const* strPose, XnUserID nId, 
                                        void* pCookie) {
    state_t* st = (state_t*) pCookie;
    printf("Pose %s lost for user %d\n", strPose, nId);
}

void state_t::publishTransforms(const std::string& channel) {
    XnUserID users[15];
    XnUInt16 users_count = 15;
    g_UserGenerator.GetUsers(users, users_count);

    int64_t time_now = bot_timestamp_now();
    
    //----------------------------------
    // Populate Skeleton List
    //----------------------------------
    kinect::skeleton_list_msg_t skeleton_list_msg;

    for (int j = 0; j < users_count; ++j) {
        XnUserID user = users[j];
        if (!g_UserGenerator.GetSkeletonCap().IsTracking(user))
            continue;

        kinect::skeleton_msg_t skeleton_msg;
        erlcm::kinect_person_tracker_status_t person_tracker_status;

        const int num_links = NumLinks;
        skeleton_msg.utime = time_now;
        skeleton_msg.user_id = user;  // double
        skeleton_msg.num_links = num_links;
        skeleton_msg.links.resize(num_links);

        //----------------------------------
        // Get Skeleton information
        //----------------------------------
        for (int i = 0; i < NumLinks; ++i) {

            XnSkeletonJointPosition joint_position;
            g_UserGenerator.GetSkeletonCap().
                GetSkeletonJointPosition(user, skelLinkList(i).source, joint_position);
            double x = joint_position.position.X / 1000.0;
            double y = joint_position.position.Y / 1000.0;
            double z = joint_position.position.Z / 1000.0;
            skeleton_msg.links[i].joint_id = skelLinkList(i).source;
            skeleton_msg.links[i].source.x = x, skeleton_msg.links[i].source.y = y, 
                skeleton_msg.links[i].source.z = z;

            g_UserGenerator.GetSkeletonCap().
                GetSkeletonJointPosition(user, skelLinkList(i).dest, joint_position);
            x = joint_position.position.X / 1000.0;
            y = joint_position.position.Y / 1000.0;
            z = joint_position.position.Z / 1000.0;

            skeleton_msg.links[i].joint_id = skelLinkList(i).dest;
            skeleton_msg.links[i].dest.x = x, skeleton_msg.links[i].dest.y = y, 
                skeleton_msg.links[i].dest.z = z;
        }
        // Push skeleton
        skeleton_list_msg.skeletons.push_back(skeleton_msg);

        // Publish Skeleton msg
        lcm.publish(channel, &skeleton_msg);

        //----------------------------------
        // Transform skeleton info w.r.t body
        //----------------------------------
        XnSkeletonJointPosition joint_position;
        g_UserGenerator.GetSkeletonCap().
            GetSkeletonJointPosition(user, XN_SKEL_TORSO, joint_position);
        XnPoint3D ni_p = joint_position.position;

        double xyz_kinect[] = {ni_p.X/1000, ni_p.Y/1000, ni_p.Z/1000};
        double xyz_body[3];
        bot_frames_transform_vec (b_frames, "KINECT", "body", xyz_kinect, xyz_body);

        //double X = ni_p.X, Z = ni_p.Z;
        //double r = sqrt((X*X)+(Z*Z)) / 1000;
        //double theta = atan2(X, Z);
        //printf("r,t: %f %f %f\n", r, theta);

        // Person tracking status
        person_tracker_status.utime = time_now;
        person_tracker_status.status = tracker_status;
        person_tracker_status.x = xyz_body[0];
        person_tracker_status.y = xyz_body[1];
        //person_tracker_status.theta = theta;
        //person_tracker_status.r = r;
        lcm.publish("KINECT_PERSON_STATUS_SERVO", &person_tracker_status);

        // // Pointing vector
        // XnSkeletonJointPosition le, lh, re, rh;
        // g_UserGenerator.GetSkeletonCap().
        //     GetSkeletonJointPosition(user, XN_SKEL_LEFT_ELBOW, le);
        // g_UserGenerator.GetSkeletonCap().
        //     GetSkeletonJointPosition(user, XN_SKEL_LEFT_HAND, lh);
        // g_UserGenerator.GetSkeletonCap().
        //     GetSkeletonJointPosition(user, XN_SKEL_RIGHT_ELBOW, re);
        // g_UserGenerator.GetSkeletonCap().
        //     GetSkeletonJointPosition(user, XN_SKEL_RIGHT_HAND, rh);
        // XnPoint3D le_p = le.position;
        // XnPoint3D lh_p = lh.position;
        // XnPoint3D re_p = re.position;
        // XnPoint3D rh_p = rh.position;
        
        // // Publish left and right hand vector
        // erlcm_pointing_vector_t lhandVec;
        // lhandVec.pos[0] = le_p.X;
        // lhandVec.pos[1] = le_p.Y;
        // lhandVec.pos[2] = le_p.Z;

        // double p1[3], p2[3];
        // p1[0] = le_p.X; p1[1] = le_p.Y; p1[2] = le_p.Z;
        // p2[0] = lh_p.X; p2[1] = lh_p.Y; p2[2] = lh_p.Z;

        // double norm = std::sqrt((p1[0]-p2[0])*(p1[0]-p2[0]) + 
        //                         (p1[1]-p2[1])*(p1[1]-p2[1]) + 
        //                         (p1[2]-p2[2])*(p1[2]-p2[2]));

        // lhandVec.vec[0] = (p2[0]-p1[0]) / norm;
        // lhandVec.vec[1] = (p2[1]-p1[1]) / norm;
        // lhandVec.vec[2] = (p2[2]-p1[2]) / norm;

        // erlcm_pointing_vector_t rhandVec;
        // rhandVec.pos[0] = re_p.X;
        // rhandVec.pos[1] = re_p.Y;
        // rhandVec.pos[2] = re_p.Z;

        // p1[0] = re_p.X; p1[1] = re_p.Y; p1[2] = re_p.Z;
        // p2[0] = rh_p.X; p2[1] = rh_p.Y; p2[2] = rh_p.Z;

        // norm = std::sqrt((p1[0]-p2[0])*(p1[0]-p2[0]) + 
        //                  (p1[1]-p2[1])*(p1[1]-p2[1]) + 
        //                  (p1[2]-p2[2])*(p1[2]-p2[2]));

        // rhandVec.vec[0] = (p2[0]-p1[0]) / norm;
        // rhandVec.vec[1] = (p2[1]-p1[1]) / norm;
        // rhandVec.vec[2] = (p2[2]-p1[2]) / norm;

        // erlcm_pointing_vector_t_publish(lcm, "SKELETON_HAND_VECTOR_LEFT", &lhandVec);
        // erlcm_pointing_vector_t_publish(lcm, "SKELETON_HAND_VECTOR_RIGHT", &rhandVec);
    }

    // Publish Skeleton list msg
    skeleton_list_msg.num_skeletons = skeleton_list_msg.skeletons.size();
    lcm.publish(channel+"_LIST", &skeleton_list_msg);

    
    return;
}


int main(int argc, char** argv)
{

    //----------------------------------
    // Opt args
    //----------------------------------
    ConciseArgs opt(argc, (char**)argv);
    opt.add(options.vDEBUG, "d", "debug","Debug Mode");
    opt.parse();

    //----------------------------------
    // args output
    //----------------------------------
    std::cerr << "===========  Skeleton Publisher ============" << std::endl;
    std::cerr << "MODES 1: kinect-skeleton-publisher\n";
    std::cerr << "=============================================\n";
    std::cerr << "=> DEBUG : " << options.vDEBUG << std::endl;
    std::cerr << "===============================================" << std::endl;

    std::string channel("KINECT_SKELETON_FRAME");


    std::stringstream configstr; 
    configstr << getConfigPath() << "/openni_tracker.xml";
    printf("Initialize from xml: %s\n", configstr.str().c_str());
    XnStatus nRetVal = state.g_Context.InitFromXmlFile(configstr.str().c_str());
    CHECK_RC(nRetVal, "InitFromXml");

    // Depth Generator
    nRetVal = state.g_Context.FindExistingNode(XN_NODE_TYPE_DEPTH, state.g_DepthGenerator);
    CHECK_RC(nRetVal, "Find depth generator");

    // User Generator
    nRetVal = state.g_Context.FindExistingNode(XN_NODE_TYPE_USER, state.g_UserGenerator);
    if (nRetVal != XN_STATUS_OK) {
        nRetVal = state.g_UserGenerator.Create(state.g_Context);
        CHECK_RC(nRetVal, "Find user generator");
    }

    // Skeleton Cap
    if (!state.g_UserGenerator.IsCapabilitySupported(XN_CAPABILITY_SKELETON)) {
        printf("Supplied user generator doesn't support skeleton\n");
        return 1;
    }

    XnCallbackHandle hUserCallbacks;
    state.g_UserGenerator.RegisterUserCallbacks(User_NewUser, User_LostUser, &state, hUserCallbacks);


    XnCallbackHandle hCalibrationCallbacks;
    state.g_UserGenerator.GetSkeletonCap().RegisterCalibrationCallbacks(UserCalibration_CalibrationStart, UserCalibration_CalibrationEnd, &state, hCalibrationCallbacks);

    if (state.g_UserGenerator.GetSkeletonCap().NeedPoseForCalibration()) {
        g_bNeedPose = TRUE;
        if (!state.g_UserGenerator.IsCapabilitySupported(XN_CAPABILITY_POSE_DETECTION)) {
            printf("Pose required, but not supported\n");
            return 1;
        }

        XnCallbackHandle hPoseCallbacks;
        state.g_UserGenerator.GetPoseDetectionCap().RegisterToPoseCallbacks(UserPose_PoseDetected, UserPose_PoseLost, &state, hPoseCallbacks);

        state.g_UserGenerator.GetSkeletonCap().GetCalibrationPose(g_strPose);
    }

    state.g_UserGenerator.GetSkeletonCap().SetSkeletonProfile(XN_SKEL_PROFILE_ALL);

    nRetVal = state.g_Context.StartGeneratingAll();
    CHECK_RC(nRetVal, "StartGenerating");
    printf("Skeleton tracker running..\nWaiting for calibration pose.. \n");

    while (true) { 
        state.g_Context.WaitAndUpdateAll();
        state.publishTransforms(channel);
    }


    return 0;
}

