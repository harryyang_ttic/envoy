#include "kinect_skeleton_renderer.h"
#include <lcm/lcm.h>

#include <bot_core/bot_core.h>
#include <bot_vis/bot_vis.h>
#include <bot_frames/bot_frames.h>

//#include <lcmtypes/kinect_frame_msg_t.h>
#include <lcmtypes/kinect_skeleton_msg_t.h>
#include <lcmtypes/kinect_link_msg_t.h>
#include <lcmtypes/kinect_frame_msg_t.h>

#include <kinect/kinect-utils.h>
#include <deque>
#include <iostream>
#include <limits>
#include <algorithm>

std::deque<bot_core_rigid_transform_t*> tf_msgs;
typedef struct _KinectSkeletonRenderer {
    BotRenderer renderer;
    BotGtkParamWidget *pw;
    BotViewer   *viewer;
    lcm_t     *lcm;
    BotFrames *frames;
    char * kinect_frame;

    kinect_skeleton_msg_t* skeleton_msg;
    int width, height;
    GLUquadricObj *quadratic;               // Storage For Our Quadratic Objects ( NEW )
    
} KinectSkeletonRenderer;

static void 
on_skeleton_frame (const lcm_recv_buf_t *rbuf, const char *channel,
		   const kinect_skeleton_msg_t *skeleton_msg, void *user_data )
{
    KinectSkeletonRenderer *self = (KinectSkeletonRenderer*) user_data;

    if(self->skeleton_msg)
        kinect_skeleton_msg_t_destroy(self->skeleton_msg);
    self->skeleton_msg = kinect_skeleton_msg_t_copy(skeleton_msg);

    bot_viewer_request_redraw(self->viewer);
}

static void on_param_widget_changed (BotGtkParamWidget *pw, const char *name, void *user)
{
    KinectSkeletonRenderer *self = (KinectSkeletonRenderer*) user;

    if (!&self->renderer)
    	return;
    
    bot_viewer_request_redraw(self->viewer);
}

static inline void
_matrix_vector_multiply_3x4_4d (const double m[12], const double v[4],
        double result[3])
{
    result[0] = m[0]*v[0] + m[1]*v[1] + m[2] *v[2] + m[3] *v[3];
    result[1] = m[4]*v[0] + m[5]*v[1] + m[6] *v[2] + m[7] *v[3];
    result[2] = m[8]*v[0] + m[9]*v[1] + m[10]*v[2] + m[11]*v[3];
}

static inline void
_matrix_transpose_4x4d (const double m[16], double result[16])
{
    result[0] = m[0];
    result[1] = m[4];
    result[2] = m[8];
    result[3] = m[12];
    result[4] = m[1];
    result[5] = m[5];
    result[6] = m[9];
    result[7] = m[13];
    result[8] = m[2];
    result[9] = m[6];
    result[10] = m[10];
    result[11] = m[14];
    result[12] = m[3];
    result[13] = m[7];
    result[14] = m[11];
    result[15] = m[15];
}

static void _draw(BotViewer *viewer, BotRenderer *renderer)
{

    KinectSkeletonRenderer *self = (KinectSkeletonRenderer*) renderer->user;
    if(self->skeleton_msg==NULL)
      return;
    glPushMatrix();
    if (self->frames==NULL || !bot_frames_have_trans(self->frames, self->kinect_frame, 
                                                     bot_frames_get_root_name(self->frames))){
      // rotate so that X is forward and Z is up
      glRotatef(-90, 1, 0, 0);
    }
    else{
      //project to current frame
      double kinect_to_local_m[16];
      bot_frames_get_trans_mat_4x4_with_utime(self->frames, self->kinect_frame, 
                                              bot_frames_get_root_name(self->frames),
					      self->skeleton_msg->utime, kinect_to_local_m);

      // opengl expects column-major matrices
      double kinect_to_local_m_opengl[16];
      bot_matrix_transpose_4x4d(kinect_to_local_m, kinect_to_local_m_opengl);
      glMultMatrixd(kinect_to_local_m_opengl);
    }

    if (self->skeleton_msg) { 
        // Draw Skeleton along with point cloud
        glLineWidth(5);    
        glColor3f(.7, .1, .1);
        glBegin(GL_LINES);
        for (int i=0; i<self->skeleton_msg->num_links; i++) { 
            const kinect_link_msg_t& link = self->skeleton_msg->links[i];
            glVertex3f(link.source.x, -link.source.y, link.source.z);
            glVertex3f(link.dest.x, -link.dest.y, link.dest.z);
        }
        glEnd();
    }

    glPopMatrix(); //kinect_to_local

  return;
}

static void _free(BotRenderer *renderer)
{
    KinectSkeletonRenderer *self = (KinectSkeletonRenderer*) renderer;

    if(self->skeleton_msg)
        kinect_skeleton_msg_t_destroy(self->skeleton_msg);

    if(self->kinect_frame)
        free(self->kinect_frame);

    gluDeleteQuadric(self->quadratic);                // Delete Quadratic - Free Resources

    free(self);
}

void 
kinect_add_skeleton_renderer_to_viewer(BotViewer* viewer, int priority, lcm_t* lcm, BotFrames * frames, const char * kinect_frame)
{
    KinectSkeletonRenderer *self = (KinectSkeletonRenderer*) calloc(1, sizeof(KinectSkeletonRenderer));

    self->frames = frames;
    if (self->frames!=NULL)
      self->kinect_frame = strdup(kinect_frame);

    self->skeleton_msg = NULL;

    self->quadratic=gluNewQuadric();          // Create A Pointer To The Quadric Object ( NEW )
    gluQuadricNormals(self->quadratic, GLU_SMOOTH);   // Create Smooth Normals ( NEW )
    gluQuadricTexture(self->quadratic, GL_TRUE);      // Create Texture Coords ( NEW )

    BotRenderer *renderer = &self->renderer;

    self->lcm = lcm;
    self->viewer = viewer;
    self->pw = BOT_GTK_PARAM_WIDGET(bot_gtk_param_widget_new());

    renderer->draw = _draw;
    renderer->destroy = _free;
    renderer->name = "Skeleton Renderer";
    renderer->widget = GTK_WIDGET(self->pw);
    renderer->enabled = 1;
    renderer->user = self;

    // bot_gtk_param_widget_add_booleans(self->pw, 
    //                                   BOT_GTK_PARAM_WIDGET_CHECKBOX, 
    //                                   PARAM_NAME_CLOUD_SHOW, 0, NULL);

    g_signal_connect (G_OBJECT (self->pw), "changed",
                      G_CALLBACK (on_param_widget_changed), self);

    kinect_skeleton_msg_t_subscribe(self->lcm, "KINECT_SKELETON_FRAME", on_skeleton_frame, self);

    bot_viewer_add_renderer(viewer, renderer, priority);
}
