// particle_filter_tracker.cpp: implementation of the PartFilterTracker class.
//
//////////////////////////////////////////////////////////////////////

#include "particle_filter_tracker.h"
//#include "adaboostDetect.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

PartFilterTracker::PartFilterTracker() {
    nTrajectories=0;
    frameNo=0;

}

PartFilterTracker::~PartFilterTracker()
{
    // delete[] trajectories;
}

void PartFilterTracker::initTracker(cv::Mat& frame, std::vector<cv::Rect>& regions, int particlesPerObject) { 
    IplImage frame_ipl(frame);
    
    colorFeatures cf;
	
    p_perObject=particlesPerObject;
    nTrajectories=regions.size();
    IplImage* frameHSV = cf.bgr2hsv(&frame_ipl);

    trajectories = std::vector<trajectory>(nTrajectories);

    for (int i=0; i<nTrajectories; i++) {
        ParticleFilter* pf;
        pf=new ParticleFilter;
        pf->initParticles(regions[i], particlesPerObject);
        trajectories[i].object = pf;
        trajectories[i].object->objectID=i;
        trajectories[i].object->weight=1;
        trajectories[i].stratFrame=0;
        trajectories[i].desc = cv::Mat();
	
        trajectories[i].histo=computeHistogram( frameHSV, regions[i], "init_hist" );
		
        // // trajectories[i].points = (CvPoint*) malloc  ( sizeof(CvPoint));
        trajectories[i].points.push_back(trajectories[i].object->getParticleCentre());
    }

    frameNo++;
}


void PartFilterTracker::initTracker(cv::Mat& frame, std::vector<cv::Point>& pts, cv::Mat& desc, int particlesPerObject)
{
    IplImage frame_ipl(frame);
    
    colorFeatures cf;
	
    p_perObject=particlesPerObject;
    nTrajectories=pts.size();
    IplImage* frameHSV = cf.bgr2hsv(&frame_ipl);

    trajectories = std::vector<trajectory>(nTrajectories);

    for (int i=0; i<nTrajectories; i++) {
        ParticleFilter* pf;
        pf=new ParticleFilter;
        pf->initParticles(cv::Rect(cv::Point(cvRound(pts[i].x),cvRound(pts[i].y)),cv::Size(10,10)), particlesPerObject);
        trajectories[i].object = pf;
        trajectories[i].object->objectID=i;
        trajectories[i].object->weight=1;
        trajectories[i].stratFrame=0;
	
        trajectories[i].histo=computeHistogram( frameHSV, cv::Rect(pts[i],cv::Size(10,10)), "init_hist" );
        if (!desc.empty())
            trajectories[i].desc = desc.row(i).clone();

        // // trajectories[i].points = (CvPoint*) malloc  ( sizeof(CvPoint));
        trajectories[i].points.push_back(trajectories[i].object->getParticleCentre());
    }

    frameNo++;
}

void PartFilterTracker::next(cv::Mat& frame, std::vector<cv::Point>& pts, cv::Mat& desc)
{
    IplImage* frameHSV=0;
    colorFeatures cf;
    int w,h;

    IplImage frame_ipl(frame);

    frameHSV=cf.bgr2hsv(&frame_ipl);
	
    w = frame_ipl.width;
    h = frame_ipl.height;
	
    CvRect *rects=0;

    int ntrack=0;
    for (int i=0; i<nTrajectories; i++)
	{
            trajectories[i].object->transition(w,h);

            // trajectories[i].object->updateWeight(frameHSV, trajectories[i].histo);
            trajectories[i].object->updateWeight(pts, desc, trajectories[i].desc);
		
            trajectories[i].object->normalizeWeights();

            trajectories[i].object->resample();
	
            trajectories[i].points.resize(frameNo+1);
            //trajectories[i].points = (CvPoint*) realloc (trajectories[i].points, (frameNo+1) * sizeof(CvPoint));
            trajectories[i].points[frameNo]=trajectories[i].object->getParticleCentre();
	}	

    mergeTrack();

    cvReleaseImage( &frameHSV );
    frameNo++;
}

histogram* PartFilterTracker::computeHistogram(IplImage* frame, cv::Rect region, std::string name)
{
    colorFeatures cf;
    IplImage* tmp;
    histogram* hist;

    cvSetImageROI( frame, region );
    tmp = cvCreateImage( cvGetSize( frame ), IPL_DEPTH_32F, 3 );
    cvCopy( frame, tmp, NULL );
    cvResetImageROI( frame );
    hist = cf.comHistogramHSV( &tmp, 1 );
    cf.normalizeHistogram( hist );


    cv::Mat tmp_mat(tmp);
    cv::cvtColor(tmp_mat, tmp_mat, CV_HSV2BGR);
    imshow(name+"_img", cv::Mat(tmp_mat));


    cvReleaseImage( &tmp );

    cv::Mat3b histimg = cv::Mat::zeros(200, 320, CV_8UC3);
    int binW = histimg.cols / hist->n;

    cv::Mat buf = cv::Mat(1, hist->n, CV_8UC3);
    for( int i = 0; i < hist->n; i++ )
        buf.at<cv::Vec3b>(i) = cv::Vec3b(cv::saturate_cast<uchar>(i*180./hist->n), 255, 255);
    cv::cvtColor(buf, buf, CV_HSV2BGR);

    for( int i = 0; i < hist->n; i++ ) {
        int val = cv::saturate_cast<int>(hist->histo[i]*histimg.rows);
        rectangle( histimg, cv::Point(i*binW,histimg.rows),
                   cv::Point((i+1)*binW,histimg.rows - val),
                   cv::Scalar(buf.at<cv::Vec3b>(i)), CV_FILLED, 8 );
    }
    imshow(name, histimg);


    return hist;
}

void PartFilterTracker::showResults(cv::Mat& frame, int param)
{

    // IplImage frame_ipl(frame);

    for (int i=0; i<nTrajectories; i++)
        {
            CvScalar color;
			
            color = CV_RGB(255,0,0);
            if (trajectories[i].object->weight>0.5)
                {
                    int p=SHOW_ALL;
                    trajectories[i].object->displayParticles( frame, CV_RGB(0,255,255), color , p);
                    CvFont font;
                    cvInitFont(&font,CV_FONT_HERSHEY_PLAIN|CV_FONT_ITALIC,1,1,0,1);
                    char buffer [4];
			
                    sprintf (buffer, "%d",trajectories[i].object->objectID );

                    // cvPutText(&frame_ipl,buffer, cvPoint( cvRound(trajectories[i].object->particles[0].x)+5, cvRound(trajectories[i].object->particles[0].y)+5 ), &font,	cvScalar(255,255,255));
		}
	}
}

cv::Mat PartFilterTracker::subtractObjects(cv::Mat& frame)
{
    IplImage frame_ipl(frame);
    IplImage* tmp;
    tmp = cvCreateImage( cvSize(frame.cols,frame.rows),
                         IPL_DEPTH_8U, frame.channels() );
    cvCopy( &frame_ipl, tmp);
    int w,h;
    w = frame.cols;
    h = frame.rows;

    for (int i=0; i<nTrajectories; i++)
	{
            if (trajectories[i].object->weight<0.5)
                continue;
            CvRect r;
            r= trajectories[i].object->getParticleRect();

            for (int k=r.y; k<r.y+r.height;k++)
		for (int j=r.x; j<r.x+r.width;j++)
                    {
                        if ((k>=0)&&(k<h)&&(j>=0)&&(j<w))
                            {
                                //cvSet2D(tmp,k,j,cvScalar(0,0,0));
                                ((uchar *)(tmp->imageData + k*tmp->widthStep))[j*tmp->nChannels + 0]=0;
                                ((uchar *)(tmp->imageData + k*tmp->widthStep))[j*tmp->nChannels + 1]=0;
                                ((uchar *)(tmp->imageData + k*tmp->widthStep))[j*tmp->nChannels + 2]=0;
                            }
					
                    }
		
	}
    return tmp;
}

void PartFilterTracker::addObjects(cv::Mat& frame, std::vector<cv::Rect>& regions, int nRegions)
{
    IplImage* frameHSV=0;
    IplImage frame_ipl(frame);
    colorFeatures cf;
    int rs=0, nw=0,tr=0, i;
    float d=0, x1,y1, dm=0, x, y;
    int trFound[200];

    for (i=0; i<nTrajectories; i++)
        trFound[i]=0;
    if (nRegions<1)
        return;
	
    frameHSV=cf.bgr2hsv(&frame_ipl);
	
    for (i=nTrajectories; i<nTrajectories+nRegions; i++)
	{
            //////////////////////////////////////////////////
            x = (float)regions[i-nTrajectories].x+regions[i-nTrajectories].width/2;
            y = (float)regions[i-nTrajectories].y+regions[i-nTrajectories].height/2;

            for (int j=0; j<nTrajectories; j++)
		{
		
                    x1 = trajectories[j].object->particles[0].x;
                    y1 = trajectories[j].object->particles[0].y;
		
                    d = sqrtf( (x1 - x) * (x1 - x) + (y1 - y) * (y1 - y));
                    if (j==0)
                        dm=d;
                    if (d<dm)
			{
				
                            tr=j;
                            dm=d;
			}
			
		}
            ////////////////////////////////////////////////////
            if (dm<20)
		{
                    ////////////////////////////////////////////////
                    trajectories[tr].object->resetParticles( regions[i-nTrajectories]);
                    frameHSV=cf.bgr2hsv(&frame_ipl);
                    trajectories[tr].histo=computeHistogram( frameHSV, regions[i-nTrajectories], "add_hist" );
                    ///////////////////////////////////////////////////////////////////
                    trajectories[tr].object->weight=1;
                    trFound[tr]=1;
                    rs++;
		}
            else
		{
		
                    trajectories.resize(nTrajectories+nw+1);
                    // trajectories= (trajectory*) realloc (trajectories, (nTrajectories+nw+1) * sizeof(trajectory));
                    ParticleFilter* pf;
                    pf=new ParticleFilter;
                    pf->initParticles(regions[i-nTrajectories], p_perObject);
                    trajectories[nw+nTrajectories].stratFrame=frameNo;
                    trajectories[nw+nTrajectories].object = pf;
                    trajectories[nw+nTrajectories].object->objectID=nw+nTrajectories;
                    trajectories[nw+nTrajectories].object->weight=0.5;
                    trajectories[nw+nTrajectories].histo=computeHistogram( frameHSV, regions[i-nTrajectories], "add_hist" );
		
                    // trajectories[nw+nTrajectories].points = (CvPoint*) malloc  ( frameNo*sizeof(CvPoint));
                    trajectories[nw+nTrajectories].points = std::vector<cv::Point>(frameNo+1);
                    trajectories[nw+nTrajectories].points[frameNo]=trajectories[nw+nTrajectories].object->getParticleCentre();
                    nw++;
		}
		
	}
    for (int j=0; j<nTrajectories; j++)
	{
            if (trFound[j]==1)
		{
                    if (trajectories[j].object->weight<1)
                        trajectories[j].object->weight+=0.5;
		}
            else
		{
                    trajectories[j].object->weight-=0.5;//0.35
		
		}

	}


    nTrajectories=nTrajectories+nw;

    for (i=0; i<nTrajectories; i++)
        if (trajectories[i].object->weight<-3)
            removeTrack(i);
}

// void PartFilterTracker::updateObjectWeights(cv::Mat& frame, adaboostDetect* adabosst)
// {
//     IplImage* frameHSV=0, *temp=0;
//     colorFeatures cf;
//     int ff=0;
//     for (int i=0; i<nTrajectories; i++)
// 	{
//             colorFeatures cf;
//             int x, y, w, h;
//             w=cvRound(trajectories[i].object->particles[0].width);
//             h=cvRound(trajectories[i].object->particles[0].height);

//             x=cvRound(trajectories[i].object->particles[0].x)-w/2;
//             y=cvRound(trajectories[i].object->particles[0].y)-h/2;
		
//             cvSetImageROI( frame, cvRect(x-5,y-5,w+10,h+10) );
//             temp = cvCreateImage( cvGetSize( frame ),
//                                   IPL_DEPTH_8U, frame->nChannels );
// 	    cvCopy( frame, temp, NULL );
//             int found=0,n=0;
//             CvRect* rgs;
//             n=adabosst->detectObject(temp, &rgs);

//             if (n>0) found=1;
		
//             if (found==1)
// 		{
//                     if (trajectories[i].object->weight<1)
// 			trajectories[i].object->weight+=0.5;
//                     ff++;
// 		}
//             else
// 		{
//                     trajectories[i].object->weight-=0.5;//0.35
//                     if (trajectories[i].object->weight<-4)
//                         removeTrack(i);
// 		}

//             cvReleaseImage( &temp );
//             cvResetImageROI( frame );	
// 	}
// }

void PartFilterTracker::removeTrack(int No)
{
	
    for (int i=No;i<nTrajectories-1;i++)
	{		
            trajectories[i]=trajectories[i+1];
			
	} 
    nTrajectories--;
}


void PartFilterTracker::mergeTrack()
{
    float d=0, x1,y1, x, y;
	
    for (int i=0;i<nTrajectories-1;i++)
	{
            x = trajectories[i].object->particles[0].x;
            y = trajectories[i].object->particles[0].y;
            for (int j=i+1;j<nTrajectories;j++)
		{
                    x1 = trajectories[j].object->particles[0].x;
                    y1 = trajectories[j].object->particles[0].y;

                    d = sqrtf( (x1 - x) * (x1 - x) + (y1 - y) * (y1 - y));
                    if (d<20)
			{
				
                            if (trajectories[i].stratFrame<=trajectories[j].stratFrame)
				{
                                    /////////////////////////				
                                    trajectories[i].object->weight=1;					
                                    trajectories[j].object->weight-=1;
				
				}
                            else
				{				
                                    trajectories[j].object->weight=1;					
                                    trajectories[i].object->weight-=1;
				
				}
				
			}
		}
	} 
}
