#ifndef COLOR_CLASSIFIER_H_
#define COLOR_CLASSIFIER_H_

#include <opencv2/opencv.hpp>
#include <iostream>
#include <set>

#define BACKGROUND_CLASS_ID 0

static std::ostream& operator<<(std::ostream& out, const cv::Vec3b& p)
{
    out<<"("<<int(p[0])<<","<<int(p[1])<<","<<int(p[2])<<")";
    return out;
}

struct sp_color_info { 
    float x;
    float y;
    bool foreground;
    int class_id;
    cv::Point3f pt;

    sp_color_info() { }
    sp_color_info(const cv::Point3f& _pt, bool _foreground, float _x, float _y) { 
        pt = _pt;
        x = _x;
        y = _y;
        foreground = _foreground;
        class_id = -1;
    }
};

struct point_info { 
    cv::Point3f pt;
    int class_id;
    point_info() {}
    point_info(cv::Point3f _pt, int _class_id) {
        pt = _pt;
        class_id = _class_id;
    }

};
class ColorClassifier { 
 public: 
    ColorClassifier(int hsv = 0);
    ~ColorClassifier();

    // float l2_dist_corr(const color_info& c1, const color_info& c2);
    int classifyColor(sp_color_info& color);
    bool filter(sp_color_info& color);
    void classifyColor(std::vector<sp_color_info>& patches);    
    void learn(std::vector<sp_color_info> patches);    
    int foreground_id_count() { return curr_foreground_id; }
 private:
    bool hsv_colorspace;

    std::vector<point_info> bg_patches;
    std::vector<point_info> fg_patches;
    std::set<int> fg_ids;

    cv::Mat fg_training_mat;
    cv::Mat bg_training_mat;

    int curr_foreground_id;
    cv::flann::Index *fg_index, *bg_index; 

    void release();

};

#endif /* COLOR_CLASSIFIER_H_ */
