#include "particle_filter.h"

ParticleFilter::ParticleFilter() {
    nParticles=0;
    rng = cv::RNG(time(NULL));
    particles = std::vector<particle>();
}

ParticleFilter::~ParticleFilter() {
}

void ParticleFilter::resetParticles( cv::Rect region) {
  //particle* particles;
  float x, y;
  int j, width, height;
  
  width = region.width;
  height = region.height;
  x = (float)region.x + width / 2;
  y = (float)region.y + height / 2;
  for( j = 0; j < nParticles; j++ ) {
      particles[j].x = x;
      particles[j].y = y;
      particles[j].s= 1.0;
      particles[j].width = width;
      particles[j].height = height;
      particles[j].w = 1/20;
  }
}


void ParticleFilter::initParticles( cv::Rect region, int particlesPerObject) {
  float x, y;
  int j, width = 10, height = 10;
  
  nParticles=particlesPerObject;
  particles = std::vector<particle>(nParticles);

  width = region.width;
  height = region.height;
  x = (float)region.x + width / 2;
  y = (float)region.y + height / 2;
  // x = region.x;
  // y = region.y;

  for( j = 0; j < nParticles; j++ ) {
      particles[j].x0 = particles[j].xp = particles[j].x = x;
      particles[j].y0 = particles[j].yp = particles[j].y = y;
      particles[j].sp = particles[j].s = 1.0;
      particles[j].width = width;
      particles[j].height = height;
      particles[j].w = 0;
  }

  std::cerr << "Initialized " << particlesPerObject << " particles" << std::endl;

}


particle ParticleFilter::calTransition( particle p, int w, int h ) {
    double x, y, s;
    particle pn;
 
 
    x = A1 * ( p.x - p.x0 ) + A2 * ( p.xp - p.x0 ) +
        B0 * rng.gaussian( TRANS_X_STD ) + p.x0;
    pn.x = (float)MAX( 0.0, MIN( (float)w - 1.0, x ) );
    y = A1 * ( p.y - p.y0 ) + A2 * ( p.yp - p.y0 ) +
        B0 * rng.gaussian( TRANS_Y_STD ) + p.y0;
    pn.y = (float)MAX( 0.0, MIN( (float)h - 1.0, y ) );
    s = A1 * ( p.s - 1.0 ) + A2 * ( p.sp - 1.0 ) +
        B0 * rng.gaussian( TRANS_S_STD ) + 1.0;

    //  pn.s = (float)MAX( 0.1, s );
    pn.s=1.0;//////////////////////Changed
    pn.xp = p.x;
    pn.yp = p.y;
    pn.sp = p.s;
    pn.x0 = p.x0;
    pn.y0 = p.y0;
    pn.width = p.width;
    pn.height = p.height;
    //  pn.histo = p.histo;
    pn.w = 0;

    return pn;
}

void ParticleFilter::transition(int w, int h) {
    for( int j = 0; j < nParticles; j++ ) {
        particles[j] = calTransition( particles[j], w, h);
    }
}

void ParticleFilter::normalizeWeights( void )
{
    float sum = 0;
    int i;

  for( i = 0; i < nParticles; i++ )
    sum += particles[i].w;

  for( i = 0; i < nParticles; i++ )
    particles[i].w /= sum;

}

int particle_cmp(const void* p1,const void* p2 )
{
  particle* _p1 = (particle*)p1;
  particle* _p2 = (particle*)p2;

  if( _p1->w > _p2->w )
    return -1;
  if( _p1->w < _p2->w )
    return 1;
  return 0;
}

void ParticleFilter::resample( void ) {
  particle* new_particles;
  int i, j, np, k = 0;

  qsort( &particles[0], nParticles, sizeof( particle ), &particle_cmp );
  
  new_particles = (particle*)malloc( nParticles * sizeof( particle ) );
  for( i = 0; i < nParticles; i++ )
    {
        np = cvRound( particles[i].w * nParticles );
      for( j = 0; j < np; j++ )
	{
	  new_particles[k++] = particles[i];
	  if( k == nParticles )
	    goto exit;
	}
    }
  while( k < nParticles )
    new_particles[k++] = particles[0];

 exit:

  for( i = 0; i < nParticles; i++ )
      particles[i] = new_particles[i];
  free( new_particles );
}


void ParticleFilter::displayParticle( cv::Mat& img, particle p, cv::Scalar color)
{
  int x0, y0, x1, y1;

  x0 = cvRound( p.x - 0.5 * p.s * p.width );
  y0 = cvRound( p.y - 0.5 * p.s * p.height );
  x1 = x0 + cvRound( p.s * p.width );
  y1 = y0 + cvRound( p.s * p.height );
  
  // rectangle( img, cv::Point( x0, y0 ), cv::Point( x1, y1 ), color, 2, 8, 0 );
  cv::circle(img, cv::Point(cvRound(p.x),cvRound(p.y)), 2, color);

}

void ParticleFilter::displayParticles( cv::Mat& img, cv::Scalar nColor, cv::Scalar hColor , int param)
{
    cv::Scalar color;
    if (param ==SHOW_ALL)
        for( int j = nParticles-1; j >= 0; j-- )
            {
                if(j==0)
                    color = hColor;
                else
                    color = nColor;
                displayParticle( img, particles[j], color);
            }
				
    if (param ==SHOW_SELECTED)
        {
            color = hColor;
            displayParticle( img, particles[0], color );
	
			
        }

}

void ParticleFilter::updateWeight(IplImage* frameHSV, histogram* objectHisto)
{
	colorFeatures cf;
	float s;

        IplImage* likelihood = cf.likelihoodImage( frameHSV, particles[0].width, particles[0].height, objectHisto );
        cv::Mat likelihood_mat(likelihood);
        likelihood_mat *= 255;

        imshow("likelihood", likelihood_mat);

	for( int j = 0; j < nParticles; j++ )
	{
		s = particles[j].s;
		particles[j].w=cf.likelihoodHSV( frameHSV, cvRound(particles[j].y),
								   cvRound( particles[j].x ),
								   cvRound( particles[j].width * s ),
								   cvRound( particles[j].height * s) ,
								   objectHisto );

	}
}

static double l2_dist(const cv::Point& p1, const cv::Point& p2) { 
    return sqrt((p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y));
}

static bool matches_dist_sort(const cv::DMatch& d1, const cv::DMatch& d2) { 
    return d1.distance < d2.distance;
}

void ParticleFilter::updateWeight(std::vector<cv::Point>& pts, cv::Mat& desc, cv::Mat& object_desc) {

    // // First ensure that the pts actually match the original descriptor
    // cv::BruteForceMatcher<cv::Hamming> matcher;
    // cv::Ptr<cv::DescriptorMatcher> _descriptorMatcher = cv::DescriptorMatcher::create( "FlannBased" );
    // std::vector<cv::DMatch> matches;
    // _descriptorMatcher->match( desc, object_desc, matches);

    // // Matches
    // std::vector<cv::Point> matched_pts;
    // std::sort(matches.begin(), matches.end(), matches_dist_sort);

    // std::cerr << "Matches: " << std::endl;
    // for (int j=0; j<matches.size(); j++) { 
    //     std::cerr << matches[j].queryIdx << " " << matches[j].distance << std::endl;
    //     std::cerr << "m("<<l2_dist(pts[matches[j].queryIdx],getParticleCentre()) << ": " << 
    //         pts[matches[j].queryIdx].x << "," << pts[matches[j].queryIdx].y << " => " << 
    //         getParticleCentre().x << "," << getParticleCentre().y << std::endl;
    // }

    float s;
    // std::cerr << " update weight " << pts.size() << std::endl;
    cv::Mat particles_mat = cv::Mat(particles.size(), 2, CV_32FC1);
    for (int j=0; j<particles.size(); j++) { 
        particles_mat.at<float>(j,0) = particles[j].x, particles_mat.at<float>(j,1) = particles[j].y; 
        // std::cerr << "pmat: " << j << " " << particles[j].x << "," << particles[j].y << std::endl;
    }

    cv::Mat pts_mat = cv::Mat(pts.size(), 2, CV_32FC1);
    for (int j=0;j<pts.size();j++) { 
      pts_mat.at<float>(j,0) = pts[j].x, pts_mat.at<float>(j,1) = pts[j].y; 
      // std::cerr << "pts: " << j << " " << pts[j].x << "," << pts[j].y << std::endl;
    }


    cv::flann::Index flann_index(pts_mat, cv::flann::KDTreeIndexParams(4));  // using 4 randomized kdtrees

    // knn (closest pt)
    cv::Mat_<int> inds; 
    cv::Mat_<float> dists;
    flann_index.knnSearch(particles_mat, inds, dists, 2, 
                          cv::flann::SearchParams(64) ); // maximum number of leafs checked

    std::cerr << object_desc.rows << " " << object_desc.cols << std::endl;
    for( int j = 0; j < nParticles; j++ ) {
        s = particles[j].s;
        int idx = inds.at<int>(j,0); // index of pts
        int idx2 = inds.at<int>(j,1); // index of pts

        if (idx < 0 || idx > pts.size())
            particles[j].w = 0;
        else { 
            double d = sqrt(dists.at<float>(j,0));
            double d2 = sqrt(dists.at<float>(j,1));
            // distance
            particles[j].w = exp(-d*d / (2*20));

            // std::cerr << "d: " << d << " d2: " << d2 << " " << idx << " " << idx2 << std::endl;
            // cv::compareHist(cv::Mat_<float>(desc.row(idx)), cv::Mat_<float>(object_desc), CV_COMP_BHATTACHARYYA);

            // particles[j].w = exp(-matches[0].distance*matches[0].distance / (2*50));
            // std::cerr << "desc: " << desc.row(idx) << std::endl << "obje: " << object_desc << std::endl;
            // std::cerr << "match : " << matches[0].distance << " " << idx << " " << d << std::endl;
            // std::cerr << " w: " << particles[j].w << " idx: " << idx << std::endl;
            // std::cerr << pts_mat.at<float>(idx,0) << "," << pts_mat.at<float>(idx,1) << " => " << 
            //     particles_mat.at<float>(j,0) << "," << particles_mat.at<float>(j,1) << std::endl;
            
        }
        // particles[j].w = cv::Mahalanobis(desc.row(idx), object_desc, const Mat& icovar)¶
    }
}

cv::Point ParticleFilter::getParticleCentre() {
    return cv::Point(cvRound(particles[0].x), cvRound(particles[0].y));
}

cv::Rect ParticleFilter::getParticleRect(void) {
    cv::Rect rect;
    
    rect.x = cvRound( particles[0].x - 0.5 * particles[0].s * particles[0].width );
    rect.y = cvRound( particles[0].y - 0.5 * particles[0].s * particles[0].height );
    rect.width = cvRound(particles[0].s * particles[0].width);
    rect.height= cvRound( particles[0].s * particles[0].height );
    return rect;
}

void ParticleFilter::resetCCVWeights() {
    for( int i = 0; i < nParticles; i++ ) {
        particles[i].w=1-particles[i].w;
        particles[i].w= (float)exp( -3 * particles[i].w );
    }
}
