#ifndef __OPENCV_DOTDETECT_HPP__
#define __OPENCV_DOTDETECT_HPP__

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"

#ifdef __cplusplus
extern "C" {
#endif

/****************************************************************************************\
*                           Dominant Orientation Templates                               *
\****************************************************************************************/

namespace cv
{

class CV_EXPORTS DOTDetector
{
public:
    struct CV_EXPORTS TrainParams
    {
        enum { BIN_COUNT = 7 };
        static double BIN_RANGE() { return 180.0 / BIN_COUNT; }

        TrainParams();
        TrainParams( const Size& winSize, int regionSize=7, int minMagnitude=60,
                     int maxStrongestCount=7, int maxNonzeroBits=6,
                     float minRatio=0.85f );

        void read( FileNode& fn );
        void write( FileStorage& fs ) const;

        void isConsistent() const;

        Size winSize;
        int regionSize;

        int minMagnitude;
        int maxStrongestCount;
        int maxNonzeroBits;

        float minRatio;
    };

    struct CV_EXPORTS DetectParams
    {
        DetectParams();
        DetectParams( float minRatio, int minRegionSize, int maxRegionSize, int regionSizeStep,
                      bool isGroup, int groupThreshold=3, double groupEps=0.2f );

        void isConsistent( float minTrainRatio=1.f ) const;

        float minRatio;

        int minRegionSize;
        int maxRegionSize;
        int regionSizeStep;

        bool isGroup;
        int groupThreshold;
        double groupEps;
    };

    struct CV_EXPORTS DOTTemplate
    {
        struct CV_EXPORTS TrainData
        {
            TrainData();
            TrainData( const Mat& maskedImage, const cv::Mat& strongestGradientsMask );

            cv::Mat maskedImage;
            cv::Mat strongestGradientsMask;
        };

        DOTTemplate();
        DOTTemplate( const cv::Mat& quantizedImage, int objectClassID,
                     const cv::Mat& maskedImage=cv::Mat(), const cv::Mat& strongestGradientsMask=cv::Mat() );

        void addObjectClassID( int objectClassID, const cv::Mat& maskedImage=cv::Mat(), const cv::Mat& strongestGradientsMask=cv::Mat() );
        const TrainData* getTrainData( int objectClassID ) const;

        static float computeTexturelessRatio( const cv::Mat& quantizedImage );

        void read( FileNode& fn );
        void write( FileStorage& fs ) const;

        cv::Mat quantizedImage;
        float texturelessRatio;
        std::vector<int> objectClassIDs;
        std::vector<TrainData> trainData;
    };

    DOTDetector();
    DOTDetector( const std::string& filename ); // load from xml-file

    virtual ~DOTDetector();
    void clear();

    void read( FileNode& fn );
    void write( FileStorage& fs ) const;

    void load( const std::string& filename );
    void save( const std::string& filename ) const;

    void train( const string& baseDirName, const TrainParams& trainParams=TrainParams(), bool isAddImageAndGradientMask=false );
    void train( const Mat& image, const TrainParams& trainParams=TrainParams(), bool isAddImageAndGradientMask=false );
    void detectMultiScale( const Mat& image, vector<vector<Rect> >& rects, const DetectParams& detectParams=DetectParams(),
                           vector<vector<float> >* ratios=0, vector<vector<int> >* dotTemplateIndices=0 ) const;

    const vector<DOTTemplate>& getDOTTemplates() const;
    const vector<string>& getObjectClassNames() const;

    static void groupRectanglesList( std::vector<std::vector<cv::Rect> >& rectList, int groupThreshold, double eps );

protected:
    void detectQuantized( const Mat& queryQuantizedImage, float minRatio,
                          vector<vector<Rect> >& rects,
                          vector<vector<float> >& ratios,
                          vector<vector<int> >& dotTemplateIndices ) const;

    TrainParams trainParams;

    std::vector<std::string> objectClassNames;
    std::vector<DOTTemplate> dotTemplates;
};

}
}
#endif
