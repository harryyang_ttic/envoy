#include "ColorClassifier.h"
#include <opencv_utils/imshow_utils.hpp>

ColorClassifier::ColorClassifier(int hsv) {
    hsv_colorspace = (hsv > 0);
    curr_foreground_id = 1;
    fg_index = NULL;
    bg_index = NULL;
}

ColorClassifier::~ColorClassifier() {
    if (fg_index)
        delete fg_index;
    if (bg_index)
        delete bg_index;
}

void ColorClassifier::release() {
    fg_ids.clear();

    fg_patches.clear();
    bg_patches.clear();

    fg_training_mat = cv::Mat();
    bg_training_mat = cv::Mat();
    
    delete fg_index;
    delete bg_index;
}

// float ColorClassifier::l2_dist_corr(const color_info& c1, const color_info& c2) { 

// }

int ColorClassifier::classifyColor(sp_color_info& color) { 
    if (fg_ids.empty())
        return -1;

    cv::Mat pt(1, 3, CV_32FC1);
    if (!hsv_colorspace)
        pt.at<float>(0,0) = color.pt.x, pt.at<float>(0,1) = color.pt.y, pt.at<float>(0,2) = color.pt.z;    
    else 
        pt.at<float>(0,0) = color.pt.x, pt.at<float>(0,1) = color.pt.y, pt.at<float>(0,2) = 0;    
    

    // cv::flann::Index fg_indx(fg_training_mat, cv::flann::KDTreeIndexParams(4)); // using 4 randomized kdtrees
    // cv::flann::Index bg_indx(bg_training_mat, cv::flann::KDTreeIndexParams(4)); // using 4 randomized kdtrees

    // knn (closest pt to FG)
    cv::Mat_<int> fg_inds; 
    cv::Mat_<float> fg_dists;
    fg_index->knnSearch(pt, fg_inds, fg_dists, 1, 
                       cv::flann::SearchParams(64) ); // maximum number of leafs checked

    int fg_idx = fg_inds.at<int>(0,0);
    double fg_dist = sqrt(fg_dists.at<float>(0,0));

    // knn (closest pt to BG)
    cv::Mat_<int> bg_inds; 
    cv::Mat_<float> bg_dists;
    bg_index->knnSearch(pt, bg_inds, bg_dists, 1, 
                       cv::flann::SearchParams(64) ); // maximum number of leafs checked

    int bg_idx = bg_inds.at<int>(0,0);
    double bg_dist = sqrt(bg_dists.at<float>(0,0));

    // std::cerr << "FG dist " << fg_dist << " " << fg_idx
    //           << "BG dist " << bg_dist << " " << bg_idx << std::endl;

    if (fg_dist < bg_dist) { 
        assert(fg_idx >=0 && fg_idx < fg_patches.size());
        return fg_patches[fg_idx].class_id;
    } else 
        return 0;
        
}

bool ColorClassifier::filter(sp_color_info& color) { 
    // if(!enabled) return true;
    int class_id = classifyColor(color);
    color.class_id = class_id;

    // std::cerr << "classifying color " << color.pt << " as " << class_id 
    //           << ((fg_ids.find(class_id) != fg_ids.end()) ? " FG":" BG") << std::endl;

    if (fg_ids.find(class_id) == fg_ids.end())
        return false;

    return true;
}

void ColorClassifier::classifyColor(std::vector<sp_color_info>& patches) { 
    // Update whether foreground or background
    for (int j=0; j<patches.size(); j++) { 
        patches[j].foreground = filter(patches[j]);
    }
    return;
}

// void ColorClassifier::find_decision_boundary_NBC() { 
//     if (!(fg_patches.size()+bg_patches.size()))
//         return;

//     std::vector<Point2f> train_fg_pts;
//     for (std::map<int, sp_color_info>::iterator it = fg_patches.begin(); it != fg_patches.end(); it++) { 
//         int class_id = it->first;
//         const cv::Vec3b vec = it->second.vec;
       
//         pts.push_back(Point3f(vec[0], vec[1], vec[2]));
//         markers.push_back(class_id);
//     }
//     

    
//     pts.clear(), markers.clear();
//     for (int j=0; j<bg_patches.size(); j++) { 
//         const sp_color_info& spj = bg_patches[j];
       
//         pts.push_back(Point3f(spj.vec[0], spj.vec[1], spj.vec[2]));
//         markers.push_back(BACKGROUND_CLASS_ID);
//     }
//     bg_index = cv::flann::Index(pts, cv::flann::KDTreeIndexParams(4)); // using 4 randomized kdtrees

//     Mat train_pts(pts), train_markers(markers);
//     train_pts = train_pts.reshape(1, train_pts.rows);
//     train_pts.convertTo(train_pts, CV_32FC1);


// }

void ColorClassifier::learn(std::vector<sp_color_info> patches) {
    release();
    std::cerr << "Learning: " << std::endl;

    // Setup up points / class_id
    for (int j=0; j<patches.size(); j++) { 
        if (!patches[j].foreground) { 
            bg_patches.push_back(point_info(patches[j].pt, BACKGROUND_CLASS_ID));
            // std::cerr << "bg: " << patches[j].pt << std::endl;
        } else { 
            int class_id = curr_foreground_id++;
            fg_patches.push_back(point_info(patches[j].pt, class_id));
            // std::cerr << "fg: " << patches[j].pt << std::endl;
            assert(fg_ids.find(class_id) == fg_ids.end());
            fg_ids.insert(class_id);
        }
    }

    // Setup flann index
    fg_training_mat = cv::Mat(fg_patches.size(), 3, CV_32FC1);
    for (int j=0; j<fg_patches.size(); j++) { 
        fg_training_mat.at<float>(j,0) = fg_patches[j].pt.x;
        fg_training_mat.at<float>(j,1) = fg_patches[j].pt.y;
        if (!hsv_colorspace)
            fg_training_mat.at<float>(j,2) = fg_patches[j].pt.z;
        else 
            fg_training_mat.at<float>(j,2) = 0;
    }

    bg_training_mat = cv::Mat(bg_patches.size(), 3, CV_32FC1);
    for (int j=0; j<bg_patches.size(); j++) { 
        bg_training_mat.at<float>(j,0) = bg_patches[j].pt.x;
        bg_training_mat.at<float>(j,1) = bg_patches[j].pt.y;
        if (!hsv_colorspace)
            bg_training_mat.at<float>(j,2) = bg_patches[j].pt.z;
        else 
            bg_training_mat.at<float>(j,2) = 0;
    }

    fg_index = new cv::flann::Index(fg_training_mat, cv::flann::KDTreeIndexParams(4)); // using 4 randomized kdtrees
    bg_index = new cv::flann::Index(bg_training_mat, cv::flann::KDTreeIndexParams(4)); // using 4 randomized kdtrees

    // std::cerr << "FG: " << fg_training_mat << std::endl;
    // std::cerr << "BG: " << bg_training_mat << std::endl;

    std::cerr << "BG Patches: " << bg_patches.size() << " FG patches: " << fg_ids.size() << std::endl;
    opencv_utils::displayOverlay("Learning DONE!", 3000);
}
