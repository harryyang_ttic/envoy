// Tracker.h: interface for the Tracker class.
//
//////////////////////////////////////////////////////////////////////

// Opencv includes
#include "kinect_opencv_utils.h"
#include "particle_filter.h"

#ifndef PARTFILTER_TRACKER_H_
#define PARTFILTER_TRACKER_H_

struct trajectory {
    std::vector<cv::Point> points;
    ParticleFilter *object;
    histogram* histo;
    cv::Mat CCV;
    cv::Mat desc;
    int stratFrame;
};

class PartFilterTracker  
{
public:
    void mergeTrack(void);
    void removeTrack(int No);
    // void updateObjectWeights(Mat& frame, adaboostDetect* adabosst);
    void addObjects(cv::Mat& frame, std::vector<cv::Rect>& regions, int nRegions);
    cv::Mat subtractObjects(cv::Mat& frame);
    PartFilterTracker();
    ~PartFilterTracker();
    std::vector<trajectory> trajectories;
    
    int nTrajectories;
    int frameNo;
    int p_perObject;
	
    void initTracker(cv::Mat& frame, std::vector<cv::Rect>& regions, int particlesPerObject);
    void initTracker(cv::Mat& frame, std::vector<cv::Point>& pts, cv::Mat& desc, int particlesPerObject);
    void next(cv::Mat& frame, std::vector<cv::Point>& keypts, cv::Mat& desc);
    //void next(Mat& frame, adaboostDetect* adabosst, char *fileName);
    void showResults(cv::Mat& frame, int param);
	
    int nbins;
    int ccvth;
    int mode;
	
private:
    histogram* computeHistogram(IplImage* frame, cv::Rect region, std::string name);
    int histo_ccv;
};

#endif // PARTFILTER_TRACKER_H_
