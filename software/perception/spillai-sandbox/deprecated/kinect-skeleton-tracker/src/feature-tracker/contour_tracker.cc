#include "contour_tracker.h"
#define CV_SQR(a) ((a) * (a))
// Ideas: 
// Match histograms (id) of segments instead of picking max ids
// Figure out better way to "match" find correspondences b/w images
// FAST features on contours

using namespace cv;

inline int l1_dist(const Point& p1, 
                   const Point& p2) { 
    return abs(p1.x-p2.x) + abs(p1.y-p2.y);
}

inline bool within_dist(const int b1, const int b2, int dist) { 
    if (abs(b1-b2)<=dist || (BIN_SIZE-abs(b1-b2)) <= dist)
        return true;
    else 
        return false;
}

inline bool within_dist(const contour_segment& csj1, 
                        const contour_segment& csj2, int dist) { 

    // Only compare the ones that are closer
    if ( l1_dist(csj1.p1, csj2.p1) <= l1_dist(csj1.p1, csj2.p2) ) { 
        // compare csj1.p1 & csj2.p1, csj1.p2 & csj2.p2 
        // (careful, since the distance is similar, we assume they have similar (b1,b2)
        if (within_dist(csj1.b1,csj2.b1,dist) && within_dist(csj1.b2,csj2.b2,dist))
            return true;
        else 
            return false;
    } else { 
        // compare csj1.p1 & csj2.p2, csj1.p2 & csj2.p1
        if ( within_dist(csj1.b1,csj2.b2,dist) && within_dist(csj1.b2,csj2.b1,dist) )
            return true;
        else 
            return false;
    }
}

static void onMouse( int event, int x, int y, int, void* user_data ) { 

    ContourTracker* cst = (ContourTracker*) user_data;

    if( cst->selectObject )
    {
        cst->selection.x = MIN(x, cst->origin.x);
        cst->selection.y = MIN(y, cst->origin.y);
        cst->selection.width = std::abs(x - cst->origin.x);
        cst->selection.height = std::abs(y - cst->origin.y);

        cst->selection &= Rect(0, 0, cst->image.cols, cst->image.rows);
    }

    switch( event )
    {
    case CV_EVENT_LBUTTONDOWN:
        cst->origin = Point(x,y); 
        cst->selection = Rect(x,y,0,0);
        cst->selectObject = true;
        break;
    case CV_EVENT_LBUTTONUP:
        cst->selectObject = false;
        if( cst->selection.width > 0 && cst->selection.height > 0 )
            cst->trackObject = -1;
        break;
    }

    
}

ContourTracker::ContourTracker() {
    low_thresh = 50;
    high_thresh = 130;
    contour_thresh = 100;
    clength = 10;
    poff = 5;
    contour_train = false;
    temp_avg_frames = 4;

    buildColoredIDs();
    //buildColoredIDsMap();

    canonical_obj_pcl = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);

    namedWindow( "Contour Tracker" );
    cvCreateTrackbar( "low", "Contour Tracker", &low_thresh, 255, 0 );
    cvCreateTrackbar( "high", "Contour Tracker", &high_thresh, 255, 0 );
    cvCreateTrackbar( "poff", "Contour Tracker", &poff, 10, 0 );
    cvCreateTrackbar( "temp_avg_frames", "Contour Tracker", &temp_avg_frames, 10, 0 );
    setMouseCallback( "Contour Tracker", &onMouse, (void*)this );

}

ContourTracker::~ContourTracker() {

}

void ContourTracker::initialize(Mat& canny, const Mat& gray_blur, const Rect& sel) { 

    if (!(kinect->isLive()))
        return;

    const Mat3b& rgb = kinect->getRGB();
    image = rgb.clone();

    // const Mat1b& gray = kinect->getGray();
    // const Mat1b& gray_blur = kinect->getBlurredGray();

    // Mat1b canny;
    // Canny(gray_blur, canny, low_thresh, high_thresh, 3);

    vector < vector<Point> > contours;
    vector<Vec4i> hierarchy;
    findContours(canny, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

    Mat1b output = Mat1b::zeros(image.size());
    for( int i = 0; i<contours.size(); i++ ) {
        const vector<Point>& ci = contours[i];
        if (ci.size() < contour_thresh)
            continue;
        drawContours( output, contours, i, Scalar(20), 1, 8, hierarchy, 0, Point() );
    }

    // Pruning of edges based on median depth
    // pruneEdgesWithDepth(output, sel);

    // canonical_obj_pcl->points.clear();
    // pcl::PointXYZRGB pt;
    // for (int i=0; i<output.rows; i++) { 
    //     for (int j=0; j<output.cols; j++) { 
    //         if (i >= sel.y && i <= sel.y + sel.height && 
    //             j >= sel.x && j <= sel.x + sel.width && 
    //             output(i,j) > 0 ) { 
                    
    //             Vec3b s = kinect->getScalarRGB(i,j);
    //             Vec3f xyz = kinect->getXYZ(i,j);
    //             pt.x = xyz[0], pt.y = xyz[1], pt.z = xyz[2];
    //             pt.r = s[2], pt.g = s[1], pt.b = s[0];
    //             canonical_obj_pcl->points.push_back(pt);
                
    //             output(i,j) = 255;
    //         }
    //     }
    // }

    // canonical_obj_pcl->width = canonical_obj_pcl->points.size();
    // canonical_obj_pcl->height = 1;

    // Build Contours
    vector< contour_segments > contour_segments_list;
    bool goodSegmentation = buildContourSegments(contour_segments_list, contours);

    //findConnectedComponents(contour_segments_list);
    contour_hist = buildContourHistogram(contour_segments_list);

    // plotContourSegments(contour_segments_list);
    // plotStats(contour_segments_list);

    //trackObject = 1;

    imshow("Train Contour", output);

    return;
}

void ContourTracker::pruneEdgesWithDepth(Mat1b& mask, const Rect& sel, float threshold) { 

    if (!(kinect->isLive()))
        return;

    std::vector<float> depths;
    for (int i=0; i<mask.rows; i++)
        for (int j=0; j<mask.cols; j++)
            if (i >= sel.y && i <= sel.y + sel.height && 
                j >= sel.x && j <= sel.x + sel.width && mask(i,j) > 0)
                depths.push_back(kinect->getDepth(i,j));
    if (!depths.size())
        return;

    float depthm = median(depths);
    for (int i=0; i<mask.rows; i++)
        for (int j=0; j<mask.cols; j++)
            if (i >= sel.y && i <= sel.y + sel.height && 
                j >= sel.x && j <= sel.x + sel.width && 
                mask(i,j) > 0 && (fabs(depthm-kinect->getDepth(i,j)) > threshold))
                mask(i,j) = 0;
    return;
}

std::deque<Mat_<float> > gray_vec;
Mat_<float> sum_gray;
static bool temporalAverageGray(Mat1b& avg, kinect_data* kinect, const int n) {
    if (!(kinect->isLive()))
        return false;

    Mat_<float> grayf;
    const Mat1b& gray = kinect->getGray();
    gray.convertTo(grayf, grayf.type());

    if (sum_gray.empty())
        sum_gray = Mat_<float>::zeros(HEIGHT, WIDTH);

    sum_gray += grayf;
    if (gray_vec.size() <= n) { 
        gray_vec.push_back(grayf);
        return false;
    } else { 
        gray_vec.push_back(grayf);

        Mat_<float> av_gray = sum_gray / gray_vec.size();

        sum_gray = sum_gray - gray_vec[0];
        gray_vec.pop_front();

        av_gray.convertTo(avg, avg.type());
        return true;
    }
}
    
                              

static void simple_sobel(Mat& dst, Mat& img) { 

  int scale = 1;
  int delta = 0;
  int ddepth = CV_16S;

  /// Generate grad_x and grad_y
  Mat grad_x, grad_y;
  Mat abs_grad_x, abs_grad_y;
 
  /// Gradient X
  //Scharr( src_gray, grad_x, ddepth, 1, 0, scale, delta, BORDER_DEFAULT );
  Sobel( img, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT );   
  convertScaleAbs( grad_x, abs_grad_x );

  /// Gradient Y  
  //Scharr( src_gray, grad_y, ddepth, 0, 1, scale, delta, BORDER_DEFAULT );
  Sobel( img, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT );   
  convertScaleAbs( grad_y, abs_grad_y );

  /// Total Gradient (approximate)
  addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, dst );

  return;

}

static void sobel_3channel(Mat& dst, Mat& img) { 

    dst.create(img.size(), img.type());
    
    // Allocate temporary buffers
    Size size = img.size();
    Mat sobel_3dx; // per-channel horizontal derivative
    Mat sobel_3dy; // per-channel vertical derivative
    Mat sobel_dx(size, CV_32F);      // maximum horizontal derivative
    Mat sobel_dy(size, CV_32F);      // maximum vertical derivative
    Mat sobel_ag;  // final gradient orientation (unquantized)
    Mat smoothed;

    // Compute horizontal and vertical image derivatives on all color channels separately
    static const int KERNEL_SIZE = 7;
    // For some reason cvSmooth/cv::GaussianBlur, cvSobel/cv::Sobel have different defaults for border handling...
    GaussianBlur(img, smoothed, Size(KERNEL_SIZE, KERNEL_SIZE), 0, 0, BORDER_REPLICATE);
    Sobel(smoothed, sobel_3dx, CV_16S, 1, 0, 3, 1.0, 0.0, BORDER_REPLICATE);
    Sobel(smoothed, sobel_3dy, CV_16S, 0, 1, 3, 1.0, 0.0, BORDER_REPLICATE);

    short * ptrx  = (short *)sobel_3dx.data;
    short * ptry  = (short *)sobel_3dy.data;
    float * ptr0x = (float *)sobel_dx.data;
    float * ptr0y = (float *)sobel_dy.data;
    // float * ptrmg = (float *)magnitude.data;

    const int length1 = static_cast<const int>(sobel_3dx.step1());
    const int length2 = static_cast<const int>(sobel_3dy.step1());
    const int length3 = static_cast<const int>(sobel_dx.step1());
    const int length4 = static_cast<const int>(sobel_dy.step1());
    // const int length5 = static_cast<const int>(magnitude.step1());
    const int length0 = sobel_3dy.cols * 3;

    for (int r = 0; r < sobel_3dy.rows; ++r)
        {
            int ind = 0;

            for (int i = 0; i < length0; i += 3)
                {
                    // Use the gradient orientation of the channel whose magnitude is largest
                    int mag1 = CV_SQR(ptrx[i]) + CV_SQR(ptry[i]);
                    int mag2 = CV_SQR(ptrx[i + 1]) + CV_SQR(ptry[i + 1]);
                    int mag3 = CV_SQR(ptrx[i + 2]) + CV_SQR(ptry[i + 2]);

                    if (mag1 >= mag2 && mag1 >= mag3)
                        {
                            ptr0x[ind] = ptrx[i];
                            ptr0y[ind] = ptry[i];
                            // ptrmg[ind] = (float)mag1;
                        }
                    else if (mag2 >= mag1 && mag2 >= mag3)
                        {
                            ptr0x[ind] = ptrx[i + 1];
                            ptr0y[ind] = ptry[i + 1];
                            // ptrmg[ind] = (float)mag2;
                        }
                    else
                        {
                            ptr0x[ind] = ptrx[i + 2];
                            ptr0y[ind] = ptry[i + 2];
                            // ptrmg[ind] = (float)mag3;
                        }
                    ++ind;
                }
            ptrx += length1;
            ptry += length2;
            ptr0x += length3;
            ptr0y += length4;
            // ptrmg += length5;
        }


  /// Generate grad_x and grad_y
  Mat abs_sobel_dx, abs_sobel_dy;
 
  convertScaleAbs( sobel_dx, abs_sobel_dx );
  convertScaleAbs( sobel_dy, abs_sobel_dy );

  /// Total Gradient (approximate)
  addWeighted( abs_sobel_dx, 0.5, abs_sobel_dy, 0.5, 0, dst );

}
    
void ContourTracker::detectContours(pcl::PointCloud<pcl::PointXYZRGB>::Ptr contour_pcl, 
                                    const Rect& sel) { 
    if (!(kinect->isLive()))
        return;

    // const Mat3b& rgb = kinect->getRGB();
    // image = rgb.clone();

    Mat roi_color; 
    Mat roi(kinect->getBlurredRGB().clone(), sel);
    cvtColor(roi, roi_color, CV_BGR2Lab);

    Mat smax;
    sobel_3channel(smax, roi_color);
    imshow("smax", smax);

    imshow("roi_color", roi_color);
    image = roi_color.clone();

    std::vector<Mat> channels;
    cv::split(roi_color,channels);

    imshow("l", channels[0]);

    Mat s1, s2, s3;
    simple_sobel(s1, channels[0]);
    imshow("s1", s1);

    threshold(smax, smax, 20, 255, THRESH_BINARY);

    vector < vector<Point> > contours;
    vector<Vec4i> hierarchy;
    findContours(smax, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

    Mat msk = Mat::zeros(image.size(), CV_8UC3);
    for( int i = 0; i<contours.size(); i++ ) {
        const vector<Point>& ci = contours[i];
        // if (ci.size() < sel.width)
        //  continue;
        Scalar color( (rand()&255), (rand()&255), (rand()&255) );
        drawContours( msk, contours, i, color, 1, 8, hierarchy, 0, Point() );
    }

    // pruneEdgesWithDepth(msk, sel);
    // if (trackObject < 0)
    //     initialize(canny, gray_blur, selection);
    
    // Fill Contour pcl
    // contour_pcl->points.clear();
    // pcl::PointXYZRGB pt;
    // for (int i=0; i<msk.rows; i++) { 
    //     for (int j=0; j<msk.cols; j++) { 
    //         if (i >= sel.y && i <= sel.y + sel.height && 
    //             j >= sel.x && j <= sel.x + sel.width && 
    //             msk(i,j) > 0 ) { 
    //             Vec3b s = kinect->getScalarRGB(i,j);
    //             Vec3f xyz = kinect->getXYZ(i,j);
    //             pt.x = xyz[0], pt.y = xyz[1], pt.z = xyz[2];
    //             pt.r = s[2], pt.g = s[1], pt.b = s[0];
    //             contour_pcl->points.push_back(pt);
                
    //             msk(i,j) = 255;
    //         }
    //     }
    // }

    // contour_pcl->width = contour_pcl->points.size();
    // contour_pcl->height = 1;


    //if (trackObject) { 
        // Build Contours
        vector< contour_segments > contour_segments_list;
        bool goodSegmentation = buildContourSegments(contour_segments_list, contours);

        //findConnectedComponents(contour_segments_list);
        //matchStats(contour_segments_list);
        plotContourSegments(contour_segments_list);
        //}
    
    // if( selectObject && selection.width > 0 && selection.height > 0 ) {
    //     Mat roi(image, selection);
    //     bitwise_not(roi, roi);
    // }

    // if (!gray_blur.empty())
    //     imshow("Avg", gray_blur);

    imshow("Contour Tracker", image);
    imshow("Canny", msk);
    //extractContourFeatures(contours);

    return;
}

bool ContourTracker::buildContourSegments(vector< contour_segments >& contour_segments_list, 
                                          const std::vector< std::vector <Point> >& contours) { 

    // Create contour segments from existing contours structure
    for( int i = 0; i<contours.size(); i++ ) {
        const vector<Point>& ci = contours[i];
        contour_segments segments;

        if (ci.size() < contour_thresh)
            continue;

        for (int j=0; j<ci.size(); j+=clength) { 
            contour_segment cs(kinect, ci, j, clength, poff);
            segments.push_back(cs);
        }
        contour_segments_list.push_back(segments);
    }

    // // Determine appropriate sample pt per segment
    // for (int i=0; i<contour_segments_list.size(); i++) { 
    //     contour_segments& segments = contour_segments_list[i];
    //     for (int j=0; j<segments.size(); j++) { 
    //         contour_segment& csj = segments[j];
    //         float d1 = fabs(median_depth-csj.depth1), 
    //             d2 = fabs(median_depth-csj.depth2);
    //         if ( d1 < .2 || d2 < .2) { 
    //             if ( d1 < d2 ) { 
    //                 csj.rep = 1;
    //                 csj.B = csj.b1;
    //                 csj.P = csj.p1;
    //             } else { 
    //                 csj.rep = 2;
    //                 csj.B = csj.b2;
    //                 csj.P = csj.p2;
    //             }
                    
    //         }
    //     }
    // }
            
    return true;

}

void ContourTracker::findConnectedComponents(vector< contour_segments >& contour_segments_list) { 

    /////////////////////////////////////
    // Connected components on contour segments
    for (int i=0; i<contour_segments_list.size(); i++) { 
        contour_segments& segments = contour_segments_list[i];

        for (int j=0; j+1<segments.size(); j++) { 
            contour_segment& csj1 = segments[j];
            contour_segment& csj2 = segments[j+1];
            //if (!csj1.isGood() || !csj2.isGood())
            //    continue;
            if (csj1.rep < 0 || csj2.rep < 0)
                continue;
            
            if (within_dist(csj1.B, csj2.B, BIN_DEV)) { 
                csj2.B = csj1.B;
            }

            //assert( (csj1.id >=0 && csj1.id < id_colors.size()) || (csj1.id == -1));
            //assert( (csj2.id >=0 && csj2.id < id_colors.size()) || (csj2.id == -1));
        }
    }
    return;
}


inline double matchHists(const std::vector<float>& lhs, 
                         const std::vector<float>& rhs) { 
    //std::cerr << "Test: " << compareHist(Mat(lhs), Mat(lhs), CV_COMP_CORREL) << std::endl;
    //CV_COMP_BHATTACHARYYA
    //CV_COMP_INTERSECT
    //CV_COMP_CHISQR
    //CV_COMP_CORREL
    return compareHist(Mat(lhs), Mat(rhs), CV_COMP_INTERSECT);
}

void ContourTracker::matchStats(const vector< contour_segments >& contour_segments_list) { 

    if (!contour_hist.size())
        return;

    std::cerr << "Matching stats " << std::endl;
    Mat3b color_mask = Mat3b::zeros(kinect->height, kinect->width);

    std::vector< std::vector<float> > live_contour_hists;


    double norm = matchHists(contour_hist, contour_hist);
    for (int i=0; i<contour_segments_list.size(); i++) { 
        const contour_segments& segments = contour_segments_list[i];

        for (int j=0; j<segments.size(); j+=5) { 

            int max_id = 0;
            std::vector<float> id_votes(id_colors.cols, 0);
            for (int jo=j; jo<j+5 && jo<segments.size(); jo++) { 
                if (segments[jo].rep > 0)
                    id_votes[segments[jo].B]++;
                if (id_votes[segments[jo].B] > max_id)
                    max_id = id_votes[segments[jo].B];
            }

            // Normalize
            if (max_id == 0)
                continue;
            for( int k = 0; k < id_votes.size(); k++ )
                id_votes[k] = id_votes[k] * 1.f / max_id;

            // Match
            double cc = matchHists(id_votes, contour_hist) / norm;
            if (cc < .6)
                continue;
            
            // Color
            for (int jo=j; jo<j+5 && jo<segments.size(); jo++) { 
                const contour_segment& csj = segments[jo];

                for (int k=0; k<csj.points.size(); k++) {
                    if (csj.points[k].y < 0 || csj.points[k].y >= color_mask.rows || 
                        csj.points[k].x < 0 || csj.points[k].x >= color_mask.cols)
                        continue;
                
                    if (color_mask(csj.points[k].y, csj.points[k].x) == Vec3b(0,0,0))
                        color_mask(csj.points[k].y,csj.points[k].x) = Vec3b(cc*255, cc*255,cc*255);

                    if (k == csj.points.size() / 2 && jo % 4 == 0)
                        cv::putText(color_mask,
                                    cv::format("%2.2f",cc ),
                                    Point(csj.points[k].x,csj.points[k].y), 
                                    0, 0.3, Scalar(255,255,255,255),1);    
                }
            }
        }
    }
        
        
    //     live_contour_hists.push_back(id_votes);
    //     // std::cerr<<"H["<<i<<"] : "<<std::endl;
    //     // for (int j=0; j<contour_hists.size(); j++) 
    //     //     std::cerr << j << ": " << matchHists(contour_hists[j], id_votes) << std::endl;
    // }


    // for (int i=0; i<contour_segments_list.size(); i++) { 
    //     const contour_segments& segments = contour_segments_list[i];

    //     double cc = matchHists(live_contour_hists[i], contour_hist) / norm;
    //     if (cc < .6)
    //         continue;

    //     for (int j=0; j<live_contour_hists[i].size(); j++) 
    //         printf("%3.2f ", live_contour_hists[i][j]);
    //     std::cerr<<" H["<<i<<"] : "<<cc<<std::endl;

    //     for (int j=0; j<segments.size(); j++) { 
    //         const contour_segment& csj = segments[j];

    //         for (int k=0; k<csj.points.size(); k++) {
    //             if (csj.points[k].y < 0 || csj.points[k].y >= color_mask.rows || 
    //                 csj.points[k].x < 0 || csj.points[k].x >= color_mask.cols)
    //                 continue;
                
    //             if (color_mask(csj.points[k].y, csj.points[k].x) == Vec3b(0,0,0))
    //                 color_mask(csj.points[k].y,csj.points[k].x) = id_colors.at<Vec3b>(i);
    //         }
    //     }
    // }

    // //std::cerr << "base: " << std::endl;
    // for (int j=0; j<contour_hist.size(); j++) 
    //     printf("%3.2f ", contour_hist[j]);
    // std::cerr << " (" << matchHists(contour_hist, contour_hist) << ") " << std::endl;
    // std::cerr << std::endl;
    imshow("match", color_mask);
    std::cerr << "=============" << std::endl;


}


#if 0
void ContourTracker::extractContourFeatures(const std::vector< std::vector <Point> >& contours) { 
    

    vector< contour_segments > contour_segments_list;
    bool goodSegmentation = buildContourSegments(contour_segments_list, contours);
    //findConnectedComponents(contour_segments_list);
    
    matchStats(contour_segments_list);
    
    plotContourSegments(contour_segments_list);
    plotStats(contour_segments_list);

    // /////////////////////////////////////
    // // std::vector<std::pair<int, uint64_t> > votes_sorted;

    // // for (int j=0; j<id_votes.size(); j++)
    // //     votes_sorted.push_back(std::make_pair(j, id_votes[j]));
    // // std::sort(votes_sorted.begin(), votes_sorted.end(), votes_sort);

    // // std::cerr << "Votes: " ;
    // // for (int j=0; j<votes_sorted.size(); j++)
    // //     std::cerr << "(" << votes_sorted[j].first << ","<<votes_sorted[j].second << ") ";
    // // std::cerr << std::endl;
    
    // /////////////////////////////////////
    // // // Debugging
    // // for (int i=0; i<contour_segments.size(); i++) { 
    // //     vector<contour_segment>& seg = contour_segments[i];

    // //     for (int j=0; j<seg.size(); j++) { 
    // //         if (!seg[j].isGood())
    // //             seg[j].id = id_colors.size()-1;
    // //         else { 
    // //             if (j%2==0)
    // //                 seg[j].id = 0;
    // //             else 
    // //                 seg[j].id = 2;
    // //         }
    // //     }
    // // }
    
    // // /////////////////////////////////////    
    // // // Max votes within a block of 5 segments
    // // // Initialize id_votes
    // // std::vector<std::pair<int, uint64_t> >  _id_votes;
    // // for (int j=0; j<id_colors.size(); j++) 
    // //     _id_votes.push_back(std::make_pair(j, 0));

    // // // Quasi Connected compoenents labeling based on max bin votes
    // // for (int i=0; i<contour_segments.size(); i++) { 
    // //     vector<contour_segment>& seg = contour_segments[i];
    // //     for (int j=0; j<seg.size(); j+=5) { 
            
    // //         std::vector<std::pair<int, uint64_t> > id_votes = _id_votes;
    // //         for (int jo=j; jo<j+5 && jo<seg.size(); jo++) 
    // //             if (seg[jo].isGood())
    // //                 (id_votes[seg[jo].id].second)++;            

    // //         std::sort(id_votes.begin(), id_votes.end(), votes_sort);            
    // //         int max_id = id_votes[0].first;

    // //         // Apply the same id (from max_id) to all the segments
    // //         for (int jo=j; jo<j+5 && jo<seg.size(); jo++) 
    // //             if (seg[jo].isGood())
    // //                 seg[jo].id = max_id;
    // //     }
    // // }
    
    // /////////////////////////////////////    
    // // // Median votes per segment
    // // // Quasi Connected compoenents labeling based on median bin
    // // for (int i=0; i<contour_segments.size(); i++) { 
    // //     vector<contour_segment>& seg = contour_segments[i];
    // //     for (int j=0; j<seg.size(); j+=5) { 
            
    // //         std::vector<contour_segment> patch;
    // //         int last_idx = std::min((int)seg.size(), j+5);
    // //         patch.insert(patch.begin(), seg.begin() + j, seg.begin() + last_idx);
    // //         std::sort(patch.begin(), patch.end(), patch_sort);

    // //         assert(patch.size() > 0);
    // //         // Apply the same id (from max_id) to all the segments
    // //         for (int off=0; off<5 && j+off<seg.size(); off++) 
    // //             if (seg[j+off].isGood())
    // //                 seg[j+off].id = patch[patch.size()/2].id;
    // //     }
    // // }
    // /////////////////////////////////////    


    

    return;

}
#endif
void ContourTracker::plotContourSegments(const vector< contour_segments >& contour_segments_list) { 

    const Mat3b& rgb_blur = kinect->getBlurredRGB();
    const Mat3b& hls = kinect->getHLS();

    Mat3b rgb_blur2;
    resize(rgb_blur, rgb_blur2, Size(rgb_blur.cols*2,rgb_blur.rows*2));

    Mat3b color_mask = Mat3b::zeros(rgb_blur.size());

    // Draw id lines on blurred rgb image
    for (int i=0; i<contour_segments_list.size(); i++) { 
        const contour_segments& segments = contour_segments_list[i];
        for (int j=0; j<segments.size(); j++) { 
            const contour_segment& csj = segments[j];
            // if (csj.isObjectBorder())
            //     continue;
            if (csj.isGood()) { 
            if (csj.rep == 1) 
                cv::putText(rgb_blur2,
                            cv::format("%i",/*hue(cs.p1.y,cs.p1.x),*/csj.b1 ),
                            Point(csj.p1.x*2,csj.p1.y*2), 0, 0.3, Scalar(255,255,255,255),1);    
            
            if (csj.rep == 2)
                cv::putText(rgb_blur2,
                            cv::format("%i",/*hue(cs.p2.y,cs.p2.x),*/csj.b2 ),
                            Point(csj.p2.x*2,csj.p2.y*2), 0, 0.3, Scalar(255,255,255,255),1);    
            
            //if (csj.rep > 0)
            line(rgb_blur2, 
                 Point(csj.p1.x*2,csj.p1.y*2), 
                 Point(csj.p2.x*2,csj.p2.y*2), Scalar(id_colors.at<Vec3b>(csj.id)), 1, 8);
            }
            // else { 
            //     if (csj.p1.y >= 0 && csj.p1.y < hls.rows && csj.p2.y >= 0 && csj.p2.y < hls.rows && 
            //         csj.p1.x >= 0 && csj.p1.x < hls.cols && csj.p2.x >= 0 && csj.p2.x < hls.cols) { 
            //         cv::putText(rgb_blur2,
            //                     cv::format("%i",/*hue(csj.p1.y,csj.p1.x)*/csj.d1),
            //                     Point(csj.p1.x*2,csj.p1.y*2), 0, 0.3, Scalar(0,0,0,255),1);    
                    
            //         cv::putText(rgb_blur2,
            //                     cv::format("%i",/*hue(csj.p2.y,csj.p2.x)*/csj.d2),
            //                     Point(csj.p2.x*2,csj.p2.y*2), 0, 0.3, Scalar(0,0,0,255),1);    
  
            //         line(rgb_blur2, 
            //              Point(csj.p1.x*2,csj.p1.y*2), 
            //              Point(csj.p2.x*2,csj.p2.y*2), Scalar(0,0,0), 1, 8);
            //     }
            // }
        }
    }

    imshow("track", rgb_blur2);

    // Draw ID'd contours on empty image
    for (int i=0; i<contour_segments_list.size(); i++) { 
        const contour_segments& segments = contour_segments_list[i];
        for (int j=0; j<segments.size(); j++) { 
            const contour_segment& csj = segments[j];
            //if (!csj.isGood())
            //    continue;

            for (int k=0; k<csj.points.size(); k++) {
                if (csj.points[k].y < 0 || csj.points[k].y >= color_mask.rows || 
                    csj.points[k].x < 0 || csj.points[k].x >= color_mask.cols)
                    continue;
                
                // if (csj.isObjectBorder()) { 
                //    color_mask(csj.points[k].y,csj.points[k].x) = Vec3b(255, 255, 255);
                //    continue;
                //}
                
                if (color_mask(csj.points[k].y, csj.points[k].x) == Vec3b(0,0,0))
                    color_mask(csj.points[k].y,csj.points[k].x) = id_colors.at<Vec3b>(csj.B);
                //, colorblur_res(csj.points[k].y*2,csj.points[k].x*2) = Vec3b(255, 255, 255);
                        
            }
        }
    }

    imshow("mask", color_mask);
    return;
}


void ContourTracker::plotStats(const vector< contour_segments>& contour_segments_list) { 

    if (contour_train)
        contour_hists.clear();
    
    if (!contour_segments_list.size())
        return;

    // Plot the histogram for every 10 segment (links)
    int bW = 20;
    int bH = 20;
    Mat3b contour_histogram = Mat3b::zeros(contour_segments_list.size() * bH, id_colors.cols*bW);
    for (int i=0; i<contour_segments_list.size(); i++) { 
        const contour_segments& segments = contour_segments_list[i];

        int max_id = 0;
        std::vector<float> id_votes(id_colors.cols, 0);

        for (int j=0; j<segments.size(); j++) { 
            if (segments[j].rep > 0) { 
                id_votes[segments[j].B]++;
                if (id_votes[segments[j].B] > max_id)
                    max_id = id_votes[segments[j].B];
            }
        }

        if (max_id == 0)
            continue;

        for( int j = 0; j < id_votes.size(); j++ ) {
            //int b1 = id2bin_map[j].first, b2 = id2bin_map[j].second;
        
            int h = (id_votes[j] * bH / (max_id + 1)); // +1 so that there's some offset
            rectangle( contour_histogram, Point(j*bW,(i+1)*bH),
                       Point((j+1)*bW,(i+1)*bH - h),
                       Scalar(id_colors.at<Vec3b>(j)), -1, 8 );
            
            cv::putText(contour_histogram,
                        cv::format("%i",i),
                        Point(5,(i+1)*bH-2), 0, 0.3, Scalar(id_colors.at<Vec3b>(i % id_colors.cols)),1);    
            
        }
        if (contour_train)
            contour_hists.push_back(id_votes);
    }
    if (contour_train)
        imshow("contour hist train", contour_histogram);
    else 
        imshow("contour hist", contour_histogram);


}


std::vector<float>
ContourTracker::buildContourHistogram(const vector< contour_segments>& contour_segments_list) { 

    // Plot the histogram for every 10 segment (links)
    int bW = 20;
    int bH = 20;

    Mat3b contour_histogram = Mat3b::zeros(bH, id_colors.cols*bW);

    int max_id = 0;
    std::vector<float> id_votes(id_colors.cols, 0);

    for (int i=0; i<contour_segments_list.size(); i++) { 
        const contour_segments& segments = contour_segments_list[i];

        for (int j=0; j<segments.size(); j++) { 
            if (segments[j].rep > 0) { 
                id_votes[segments[j].B]++;
                if (id_votes[segments[j].B] > max_id)
                    max_id = id_votes[segments[j].B];
            }
        }
    }

    if (max_id == 0)
        return id_votes;

    // Normalize
    for( int j = 0; j < id_votes.size(); j++ ) {
        id_votes[j] = id_votes[j] * 1.f / max_id;
        
        int h = id_votes[j] * bH; // so that there's some offset

        rectangle( contour_histogram, Point(j*bW,bH),
                   Point((j+1)*bW,bH - h),
                   Scalar(id_colors.at<Vec3b>(j)), -1, 8 );
    }

    imshow("Built Contour Hist", contour_histogram);
    return id_votes;
}


#if 0
void ContourTracker::plotStats(const vector< contour_segments>& contour_segments_list) { 

    // Bin to bin transitions
    std::map<int, std::pair<int, int> > id2bin_map;
    for (std::map<std::pair<int, int>, int>::const_iterator it = id_colors_map.begin(); 
         it != id_colors_map.end(); it++) { 
        id2bin_map.insert(make_pair(it->second, it->first));
    }

    // // Histogram
    // vector<int> id_counts(id_colors_map.size(), 0);
    // int max_id = 0;
    // int max_bin = 0;
    // for (int i=0; i<contour_segments_list.size(); i++) { 
    //     const contour_segments& segments = contour_segments_list[i];
    //     for (int j=0; j<segments.size(); j++) { 
    //         const contour_segment& csj = segments[j];            
    //         if (!csj.isGood())
    //             continue;

    //         id_counts[csj.id]++;
    //         id_counts[csj.id]++;

    //         if (id_counts[csj.id] > max_id)
    //             max_id = id_counts[csj.id];
    //         if (id_counts[csj.id] > max_id)
    //             max_id = id_counts[csj.id];
    //     }
    // }

    Mat buf1 = Mat(1, BIN_SIZE, CV_8UC3);
    for( int i = 0; i < BIN_SIZE; i++ )
        buf1.at<Vec3b>(i) = Vec3b(saturate_cast<uchar>(i*180./BIN_SIZE), 128, 255);
    cvtColor(buf1, buf1, CV_HLS2BGR);


    int bW = 20;
    // Mat3b id_counts_mat = Mat3b::zeros(100, id_counts.size()*bW);
    // for( int i = 0; i < id_counts.size(); i++ ) {
    //     if (id2bin_map.find(i) == id2bin_map.end())
    //         continue;

    //     int b1 = id2bin_map[i].first, b2 = id2bin_map[i].second;
        
    //     int h = (id_counts[i] * 100. / max_id);
    //     rectangle( id_counts_mat, Point(i*bW,id_counts_mat.rows),
    //                Point((i+1)*bW,id_counts_mat.rows - h/2 ),
    //                Scalar(buf1.at<Vec3b>(b1)), -1, 8 );

    //     rectangle( id_counts_mat, Point(i*bW,id_counts_mat.rows - h/2),
    //                Point((i+1)*bW,id_counts_mat.rows - h),
    //                Scalar(buf1.at<Vec3b>(b2)), -1, 8 );

    //     line(id_counts_mat, Point(i*bW+bW/2,id_counts_mat.rows - h/4), Point(i*bW+bW/2,id_counts_mat.rows-3*h/4), id_colors[i], 3, 8);

    // }

    // imshow("id_counts", id_counts_mat);


    // // Plot hue variance
    // int s = 4;
    // Mat3b contour_huevar = Mat3b::zeros((contour_segments_list.size()+1) * bW, 500);
    // for (int i=0; i<contour_segments_list.size(); i++) { 
    //     const contour_segments& segments = contour_segments_list[i];
    //     for (int j=0; j<segments.size(); j++) { 
    //         const contour_segment& csj = segments[j];            
    //         if (!csj.isGood())
    //             continue;
            
    //         // segment index along x, contour index along y
    //         circle(contour_huevar, Point((500*j)/segments.size(),(i+1)*bW-2), 2, Scalar(buf1.at<Vec3b>(csj.b1)), CV_FILLED );
    //         circle(contour_huevar, Point((500*j)/segments.size(),(i+1)*bW+2), 2, Scalar(buf1.at<Vec3b>(csj.b2)), CV_FILLED );

    //         // Hue along x, contour index along y
    //         // circle(contour_huevar, Point(hue(csj.p1.y,csj.p1.x) * s,(i+1)*bW), 2, Scalar(buf1.at<Vec3b>(csj.b1)), CV_FILLED );
    //         // circle(contour_huevar, Point(hue(csj.p2.y,csj.p2.x) * s,(i+1)*bW), 2, Scalar(buf1.at<Vec3b>(csj.b2)), CV_FILLED );
    //     }
    // }

    // imshow("huevar", contour_huevar);


    if (contour_train)
        contour_hists.clear();

    // Plot the histogram for every 10 segment (links)
    int bH = 20;
    Mat3b contour_histogram = Mat3b::zeros(contour_segments_list.size() * bH, id_colors_map.size()*bW);
    for (int i=0; i<contour_segments_list.size(); i++) { 
        const contour_segments& segments = contour_segments_list[i];

        int max_id = 0;
        std::vector<float> id_votes(id_colors_map.size(), 0);

        for (int j=0; j<segments.size(); j++) { 
            if (segments[j].isGood())
                id_votes[segments[j].id]++;

            if (id_votes[segments[j].id] > max_id)
                max_id = id_votes[segments[j].id];
        }

        if (max_id == 0)
            continue;

        for( int j = 0; j < id_votes.size(); j++ ) {
            if (id2bin_map.find(j) == id2bin_map.end())
                continue;

            int b1 = id2bin_map[j].first, b2 = id2bin_map[j].second;
        
            int h = (id_votes[j] * bH / (max_id + 1)); // +1 so that there's some offset
            rectangle( contour_histogram, Point(j*bW,(i+1)*bH),
                       Point((j+1)*bW,(i+1)*bH - h/2 ),
                       Scalar(buf1.at<Vec3b>(b1)), -1, 8 );

            rectangle( contour_histogram, Point(j*bW,(i+1)*bH - h/2),
                       Point((j+1)*bW,(i+1)*bH - h),
                       Scalar(buf1.at<Vec3b>(b2)), -1, 8 );
            
            line(contour_histogram, 
                 Point(j*bW+bW/2,(i+1)*bH - h/4), 
                 Point(j*bW+bW/2,(i+1)*bH-3*h/4), id_colors[j], 2, 8);

            cv::putText(contour_histogram,
                        cv::format("%i",i),
                        Point(5,(i+1)*bH-2), 0, 0.3, Scalar(255,255,255,255),1);    
            
        }
        if (contour_train)
            contour_hists.push_back(id_votes);
    }
    if (contour_train)
        imshow("contour hist train", contour_histogram);
    else 
        imshow("contour hist", contour_histogram);


}
#endif

void ContourTracker::connectedComponents(pcl::PointCloud<pcl::PointXYZRGB>::Ptr contour_pcl) { 
    if (!(kinect->isLive()))
        return;

    //Mat bw;
    const Mat1b& gray = kinect->getGray();
    //adaptiveThreshold(gray, bw, 100, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 11, 0);

    Mat bw = gray < 100;

    imshow("bw", bw);

    vector < vector<Point> > contours;
    vector<Vec4i> hierarchy;
    findContours( bw, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );

    Mat dst = Mat::zeros(bw.size(), CV_8UC3);
    if( !contours.empty() && !hierarchy.empty() ) {
        // iterate through all the top-level contours,
        // draw each connected component with its own random color
        int idx = 0;
        for( ; idx >= 0; idx = hierarchy[idx][0] ) {
            Scalar color( (rand()&255), (rand()&255), (rand()&255) );
            drawContours( dst, contours, idx, color, CV_FILLED, 8, hierarchy );
        }
    }

    imshow( "Connected Components", dst );
}
// // ascending id sort
// static bool patch_sort(const contour_segment& lhs, 
//                        const contour_segment& rhs) { 
//     return (lhs.id < rhs.id);
// }


// void HistTracker::pruneRedundantContours(std::vector< std::vector <Point> >& contours) { 

//     // Create a new copy and clear the input contours for pruning
//     std::vector< std::vector <Point> > _contours = contours;
//     contours.clear();

//     Mat1b mask = Mat1b::zeros(HEIGHT, WIDTH);
//     for (int i=0; i<_contours.size(); i++) { 
//         const std::vector<Point>& _ci = _contours[i];
//         std::vector<Point> ci;

//         // Find if there's a redundancy in the linked list
//         for (int j=0; j<_ci.size(); j++) { 
//             if (!mask(_ci[j].y,_ci[j].x)) { 
//                 ci.push_back(_ci[j]);
//             } else { 
//                 if (ci.size()) { 
//                     contours.push_back(ci);
//                     ci.clear();
//                 }
//             }
//         }
        
//         if (ci.size())
//             contours.push_back(ci);
//     }

//     return;

// }


// void HistTracker::detectContours(const kinect_depth_info& kinect_info, const Mat1b& mask) { 
//     if (!frame_fullres.data) 
//         return;

//     // Purely for display purposes
//     Mat output = frame_fullres.clone();

//     Mat1b canny;
//     const Mat1b& gray_blur = kinect->getBlurredGray();

//     Canny(gray_blur, canny, low_thresh, high_thresh, 3);
//     //bitwise_and(gray, mask, gray);    
//     // vector < vector<Point> > contours;
//     // vector<Vec4i> hierarchy;
//     // findContours(gray, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

//     // Mat1b msk = Mat1b::zeros(gray.size());
//     // for( int i = 0; i<contours.size(); i++ ) {
//     //     if (contours[i].size() < contour_thresh)
//     //         continue;
//     //     drawContours( msk, contours, i, Scalar(255), 1, 8, hierarchy, 0, Point() );
//     // }

//     kinect_point_t pt;
//     kinect_point_t pt_null;
//     float NaN = std::numeric_limits<float>::quiet_NaN();
//     pt_null.x = NaN, pt_null.y = NaN, pt_null.z = NaN;
//     pt_null.r = -1, pt_null.g = -1, pt_null.b = -1;

//     kinect_point_list_t msg;
//     std::vector<kinect_point_t> _points;

//     double depth;
//     int f_idx, c_idx;            
//     uint8_t r, g, b;
//     // for( int i = 0; i<contours.size(); i++ ) {
//     //     const std::vector<Point>& ci = contours[i];
//     //     for (int j=0; j<ci.size(); j++) {
//     //         if (ci.size() < contour_thresh)
//     //             continue;
//     uchar* pgray = gray.data;
//     for (int y=0; y<gray.rows; y++, 
//              pgray+=gray.step) { 
//         for (int x=0; x<gray.cols; x++) { 
//             if (!pgray[x])
//                 continue;

//             f_idx = y * WIDTH + x;
//             c_idx = y * WIDTH * 3 + x * 3;

//             depth = kinect_info.getDepth(y, x);

//             pt.r = kinect_info.rgb_img[c_idx + 0];
//             pt.g = kinect_info.rgb_img[c_idx + 1];
//             pt.b = kinect_info.rgb_img[c_idx + 2];

//             pt.x = kinect_info.disparity_map[f_idx*2] * depth, 
//                 pt.y = kinect_info.disparity_map[f_idx*2+1] * depth, 
//                 pt.z = depth;

//             _points.push_back(pt);
//         }
//         //_points.push_back(pt_null);
//     }

//     msg.num_points = _points.size();
//     msg.points = &_points[0];
//     kinect_point_list_t_publish(kinect_info.lcm, "KINECT_IMAGE_CONTOUR", &msg);

//     imshow("track", gray);
//     //imshow("track-mask", msk);
//     return;
// }
