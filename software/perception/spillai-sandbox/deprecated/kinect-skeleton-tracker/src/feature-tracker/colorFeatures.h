// colorFeatures.h: interface for the colorFeatures class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COLORFEATURES_H__B56D9C99_16F2_4A5D_861E_FE6CF8311074__INCLUDED_)
#define AFX_COLORFEATURES_H__B56D9C99_16F2_4A5D_861E_FE6CF8311074__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <cv.h> 
#include <stdio.h>

/* number of bins of HSV in histogram */
#define NH 10
#define NS 10
#define NV 10

/* max HSV values */
#define H_MAX 360.0
#define S_MAX 1.0
#define V_MAX 1.0

/* low thresholds on saturation and value for histogramming */
#define S_THRESH 0.1
#define V_THRESH 0.2

/* distribution parameter */
#define LAMBDA 20
typedef struct histogram {
  float histo[NH*NS + NV];   /**< histogram array */
  int n;                     /**< length of histogram array */
} histogram;


class colorFeatures  
{
public:
	colorFeatures();
	~colorFeatures();


	/////////Convert BGR (opencv) to HSV
	IplImage* bgr2hsv( IplImage* bgr );
	
	///////////Calculate cumulative Histogram of HSV Image
	histogram* comHistogramHSV( IplImage** imgs, int n );

	//////////////Normalized the Histogram sum=1
	void normalizeHistogram( histogram* histo );

	////////////Compute the Square distance of two normalized histograms
	float histoDistSq( histogram* h1, histogram* h2 );

	/////////////get a pixel value from a 32-bit floating-point image.
	float pixval32f( IplImage* img, int r, int c );

	/////////////Set a pixel value from a 32-bit floating-point image.
	void setpix32f(IplImage* img, int r, int c, float val);

	////////////Save Hisgram to a file
	int exportHistogram( histogram* histo, char* filename );
	
	
	////Returns an image containing the likelihood of there being an object(histogram) at each pixel location in an image
	IplImage* likelihoodImage( IplImage* img, int w, int h, histogram* ref_histo );
	
	//Computes the likelihood of there being an object(histogram) at a given location in an image
	float likelihoodHSV( IplImage* img, int r, int c,int w, int h, histogram* ref_histo );
private:
	int histoBinHSV( float h, float s, float v );
};

#endif // !defined(AFX_COLORFEATURES_H__B56D9C99_16F2_4A5D_861E_FE6CF8311074__INCLUDED_)
