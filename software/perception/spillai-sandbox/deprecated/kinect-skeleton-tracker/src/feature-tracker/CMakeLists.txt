cmake_minimum_required(VERSION 2.6.0)

find_package(PkgConfig REQUIRED)
#find_package(PCL 1.2 REQUIRED)
#find_package(Flann)
find_package(GLUT REQUIRED)

pkg_check_modules(EIGEN REQUIRED eigen3)
pkg_check_modules(OPENGL REQUIRED gl)
pkg_check_modules(GLU REQUIRED glu)
#pkg_check_modules(PCL_COMMON REQUIRED pcl_common-1.2)
pkg_check_modules(GSL REQUIRED gsl)


include_directories(
#  ${PCL_INCLUDE_DIRS} 
  ${GSL_INCLUDE_DIRS}
  ${GLUT_INCLUDE_DIRS}
  ${GLU_INCLUDE_DIRS}
  ${EIGEN_INCLUDE_DIRS}
  ${FLANN_INCLUDE_DIRS}
)

#link_directories(${PCL_LIBRARY_DIRS})
#link_directories(${GSL_LIBRARY_DIRS})

add_subdirectory(tld)
add_subdirectory(cvblobs)
include_directories(tld tld/tracker)
link_libraries(tld)

# create an executable, and make it public
add_executable(kinect-feature-tracker 
  kinect_feature_tracker.cc
  freak.cpp
  SLIC.cpp
  particle_filter.cc
  particle_filter_tracker.cc
  colorFeatures.cpp
  ColorClassifier.cc
  #contour_tracker.cc
)

pods_use_pkg_config_packages ( kinect-feature-tracker lcm glib-2.0 lcmtypes_kinect bot2-core image-utils bot2-lcmgl-client lcmtypes_er-lcmtypes
  bot2-frames
  bot2-param-client opencv perception-opencv-utils )


target_link_libraries(kinect-feature-tracker 
  ${LCMTYPES_LIBS} 
#  ${PCL_LIBRARIES} 
  ${EIGEN_LIBRARIES} 
  ${FLANN_LIBRARIES} 
  ${OPENGL_LIBRARIES}
  ${GLU_LIBRARIES}
  ${GLUT_LIBRARIES}
  #${GSL_STATIC_LDFLAGS} #tld
  #${OPENCV_LDFLAGS}
)

pods_install_executables(kinect-feature-tracker)
