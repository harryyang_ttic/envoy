#ifndef HISTTRACKER_H_
#define HISTTRACKER_H_

#include "kinect_opencv_utils.h"

// PCL
#include <pcl/io/pcd_io.h>
#include <pcl/PointIndices.h>
#include <pcl/registration/icp.h>

// LCM includes
#include <lcm/lcm.h>
#include <lcmtypes/kinect_skeleton_msg_t.h>
#include <lcmtypes/kinect_link_msg_t.h>
#include <lcmtypes/kinect_frame_msg_t.h>
#include <lcmtypes/kinect_point2d_t.h>
#include <lcmtypes/kinect_point_t.h>
#include <lcmtypes/kinect_point_list_t.h>
#include <kinect/kinect-utils.h>
#include <bot_core/bot_core.h>


#include <unistd.h>
#include <iomanip>
#include <glib.h>

#define PI 3.141592
#define WIDTH 640
#define HEIGHT 480
#define SCALE 2

struct skeleton_info { 
    std::map<uint32_t, std::pair<cv::Point3f, cv::Point3f> > joint_p;    
    cv::Point3f le, lh, lvec, re, rh, rvec, ee, evec;
};


class HistTracker { 
 private: 
    kinect_data* kinect;

    // Feature tracking
    bool desc_built;

    cv::Mat _descriptors;

    /* cv::SurfFeatureDetector _detector; */
    /* cv::SurfDescriptorExtractor _descriptorExtractor; */
    /* cv::BruteForceMatcher< cv::L2<float> > _descriptorMatcher; */

    cv::GridAdaptedFeatureDetector* _detector;
    cv::BriefDescriptorExtractor _descriptorExtractor;
    cv::BruteForceMatcher<cv::Hamming> _descriptorMatcher;

    std::vector<cv::KeyPoint> _keypoints;
    std::vector<cv::Point3f> _keypoints3d;

    // cv::Ptr<cv::FeatureDetector> _detector;
    // cv::Ptr<cv::DescriptorExtractor> _descriptorExtractor;
    // cv::Ptr<cv::DescriptorMatcher> _descriptorMatcher;

    std::vector<cv::Scalar> id_colors;

 public: 
    HistTracker();
    ~HistTracker();
    void useKinectData(kinect_data* pkinect) { kinect = pkinect; }
    void run();
    //void updateUserLabels(kinect_depth_info& msg);
    //void drawSkeleton(const kinect_depth_info& msg);
    void updatePointingRegion(const cv::Mat1b& limg, const cv::Mat1b& rimg); 
    //void drawSkeletonOnImage(cv::Mat& img, const kinect_depth_info& msg);
    void useCurrentPointLoc(); 
    bool running() { return (trackObject > 0); } 
    bool description_built() { return desc_built == true; }
    
    enum TrackingMode { VISION_MODE, 
                        POINTING_MODE,
                        DEFAULT };

    //void onMouse(int event, int x, int y, int, void*);

    bool selectObject;
    int trackObject;
    cv::Rect selection;
    cv::Point origin;    
    cv::Mat image;

    //private:
    void track(cv::Mat& img);
    bool buildDescriptors(const cv::Mat1b& _mask);
    bool detectObject(const cv::Mat1b& _mask, std::vector<std::pair<cv::Point3f, cv::Point3f> >& corrs,
                      float& median_depth);
    float pruneKeyPointsWithDepth(std::vector<cv::KeyPoint>& keypoints, 
                                 std::vector<cv::Point3f>& keypoints3d, 
                                 double threshold = 0.2);
    void showWindows();
    void setupWindows();

    // Global vars
    int debug_mode;
    TrackingMode mode;
    bool paused;
    bool backprojMode;
    bool showHist;

    /* // Hands */
    /* RotatedRect left_hand, right_hand; */

    // camshift related
    cv::Rect trackWindow;
    cv::RotatedRect trackBox;
    cv::Point last_tracked_pt;
    int binW;
    cv::Mat buf;

    kinect_point2d_t pt_msg;
    float pt_msg_max;

    // Histogram related
    int hsize, bin_dev;
    const float* phranges;
    float hranges[2];

    // Vars for trackbar
    int vmin, vmax, smin, bproj_thresh;

    // Images
    cv::Mat hsv, hue, mask, hist, good_hist, histimg, good_histimg, live_histimg, 
        llive_histimg, rlive_histimg;
    cv::Mat3b frame;
    cv::Mat1b backproj, selection_mask, belief, belief_comp;
    cv::Mat1b lselection_mask, rselection_mask;
 
    void buildColoredIDs() { 
        std::vector<int> vals;
        const int split = 2;

        for (int j=0; j<split; j++)
            vals.push_back(j*(256/split));
        vals.push_back(255);

        // 3^3 - 1 = 26 colors
        for (int i=0; i<vals.size(); i++)
            for (int j=0; j<vals.size(); j++)
                for (int k=0; k<vals.size(); k++)
                    if (!(vals[i] == 0 && vals[j] == 0 && vals[k] == 0))
                        id_colors.push_back(cv::Scalar(vals[i], vals[j], vals[k]));


        /* // Ids */
        /* id_colors.push_back(Scalar(0xff,0xff,0xff));    // white */
        /* //id_colors.push_back(Scalar(0x00, 0x00, 0x00)); // black */
        /* id_colors.push_back(Scalar(0x00, 0x00, 0xff)); // red */
        /* id_colors.push_back(Scalar(0x00, 0x00, 0x80)); // darkred */
        /* id_colors.push_back(Scalar(0x00, 0xff, 0x00)); // green */
        /* id_colors.push_back(Scalar(0x00, 0x80, 0x00)); // darkgreen */
        /* id_colors.push_back(Scalar(0xff, 0x00, 0x00)); // blue */
        /* id_colors.push_back(Scalar(0x80, 0x00, 0x00)); // darkblue */
        /* id_colors.push_back(Scalar(0xff, 0xff, 0x00)); // cyan */
        /* id_colors.push_back(Scalar(0x80, 0x80, 0x00)); // darkcyan */
        /* id_colors.push_back(Scalar(0xff, 0x00, 0xff)); // magenta */
        /* id_colors.push_back(Scalar(0x80, 0x00, 0x80)); // darkmagenta */
        /* id_colors.push_back(Scalar(0x00, 0xff, 0xff)); // yellow */
        /* id_colors.push_back(Scalar(0x00, 0x80, 0x80)); // darkyellow */
        /* id_colors.push_back(Scalar(0x80, 0x80, 0x80)); // gray */
        /* id_colors.push_back(Scalar(0xc0, 0xc0, 0xc0)); // lightgray */
        /* id_colors.push_back(Scalar(0x00, 0xa5, 0xff)); // orange */
        std::cerr << "Total IDs: " << id_colors.size() << std::endl;
    }
};

#endif /* HISTTRACKER_H_ */
