// Ideas : 
// - Canny edge/Water shed detection on depth image
// - Use pointed region to build histogram (hit space for confirming)
// - Segment object in 3D

// ===================================================================
/*

- Use DOT (Dominant Orientation Templates for Real-Time Detection of Texture-Less Objects
- Multimodal approach - Mutimodal templates fro real-time detection of texture-less 
  objects in heavily cluttered scenes
- May be integrate a hybrid tracker on top of DOT
- i.e. DOT running in one thread (possibly using 3d data, refer to 
  www.ros.org/presentations/2010-06-holzer-dot.pdf)
- HybridTracker running on a separate thread with updates for the object
  given every few frames (updates can run at 10fps; should work fine)


http://campar.in.tum.de/Main/StefanHinterstoisser
Binary gradient grid (BiGG) - Bradski similar to DOT

http://www.ros.org/wiki/fast_template_detector
larks


DOT w/ TLD

TLD's work well w/ features (keypts) low-level features

- DoT works well with textureless objects (contoures)
- Use both to bootstrap a contour and features based reconstruction 
- Does going to 3D from contour make tracking possible ??

*/
#define PCL 0
#if PCL
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/octree/octree.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/range_image_visualizer.h>

#include <pcl/range_image/range_image.h>
#include <pcl/range_image/range_image_planar.h>

#include <pcl/features/range_image_border_extractor.h>
#include <pcl/keypoints/narf_keypoint.h>
#include <pcl/features/narf_descriptor.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/icp.h>

#include "contour_tracker.h"
#endif

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/glext.h>

#include <set>

// LCM includes
#include <perception_opencv_utils/calib_utils.hpp>
#include <perception_opencv_utils/imshow_utils.hpp>
#include <perception_opencv_utils/math_utils.hpp>
#include <perception_opencv_utils/plot_utils.hpp>
#include <perception_opencv_utils/color_utils.hpp>
#include <perception_opencv_utils/imgproc_utils.hpp>

#include <opencv2/calib3d/calib3d.hpp>

#include "kinect_opencv_utils.h"
#include <lcmtypes/kinect_image_msg_t.h>
#include <pthread.h>
#include <bot_param/param_client.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <GL/gl.h>

#include <lcmtypes/kinect_frame_msg_t.h>
#include <lcmtypes/kinect_point2d_t.h>
#include <lcmtypes/kinect_point_t.h>
#include <lcmtypes/kinect_point_list_t.h>
#include <kinect/kinect-utils.h>
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include "segment-image.h"

// #include "particle_filter.h"
// #include "particle_filter_tracker.h"

// #include "freak.h"
//#include "hammingseg.h"
#include "SLIC.h"
#include "tld/TLD.h"
#include "ColorClassifier.h"

// #include <pcl/registration/correspondence_rejection_sample_consensus.h>

#define SHOW_ALL_RECTS_BY_ONE 0
#define BINS 16
using namespace cv;

static const uint8_t deci_r = 4;
static const uint16_t nvert = HEIGHT / deci_r;
static const uint16_t nhoriz = WIDTH / deci_r;
static const uint32_t npixels = WIDTH*HEIGHT;
static const uint32_t nnodes = nvert*nhoriz;

typedef struct _state_t state_t;
struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    BotParam *param;
    BotFrames *frames;
};
state_t * state = NULL;


struct MouseEvent
{
    MouseEvent() { event = -1; buttonState = 0; }
    Point pt;
    int event;
    int buttonState;
};
MouseEvent mouse;
MouseEvent mouse_bb;

// UI selection of desired object
Rect selection;
Point origin;
bool selectObject = false;
int trackObject = 0;
bool initLearning = false;

// LCM gl commands for viewer output
bot_lcmgl_t *lcmgl_features;

// Particle filter (using robot pose and object tracker)
// PartFilterTracker pfilt_tracker;

// Tld tracker
tld::TLD* tldtracker;
// ContourTracker ctracker;
bool need_to_init = false;
bool add_remove_pt = false;
std::vector<cv::Point2f> _points[2];

// LINEMOD detector
// Initialize LINEMOD data structures
cv::Ptr<cv::linemod::Detector> lm_detector;  
int matching_threshold = 80;
/// @todo Keys for changing these?
cv::Size roi_size(200, 200);
int learning_lower_bound = 90;
int learning_upper_bound = 95;
int num_classes = 0;
bool show_match_result = true;
bool show_timings = false;
bool learn_online = false;
bool lm_extracted = false;
  
#define HSV 1
ColorClassifier CC(HSV);

int nn_color_classifier_learned = -1;
// Feature descriptor/extractor/matcher
// cv::Ptr<cv::FeatureDetector> _detector;
// cv::Ptr<cv::DescriptorExtractor> _descriptorExtractor;
// cv::Ptr<cv::DescriptorMatcher> _descriptorMatcher;

// Camera intrinsics (bumblebee)
cv::Mat_<double> bb_K, bb_D;
cv::Mat_<double> bb_rvec, bb_R, bb_tvec;
cv::Rect bb_rect(0,0,0,0);
cv::Rect mouse_rect(0,0,0,0);
cv::Mat3b bb_img;

double depth_intrinsics[5];

// Felzenswalb segmentation
int pff_sigma = 100;
int pff_K = 500;
int pff_min = 100;

#if PCL
// ICP transform estimation
pcl::PointCloud<pcl::PointXYZRGB>::Ptr prev_point_cloud_pcl, point_cloud_pcl, canonical_obj_pcl;
Eigen::Matrix4f updated_tf = Eigen::Matrix4f::Identity();
Point3f mean_canonical_obj_pcl;
#endif
cv::Mat1b currMask;
cv::Rect currROI;


// pcl::visualization::RangeImageVisualizer range_image_widget ("Range");
// pcl::visualization::CloudViewer viewer("3D viewer");
// pcl::visualization::PCLVisualizer* viewer;


int mser_delta = 5;
int mser_area_threshold = 101;
int mser_min_margin = 3;
int mser_max_variation = 25;
int mser_min_diversity = 2;

int slic_size = 400;
int slic_dim = sqrt(slic_size);
int slic_k = 30;
int slic_m = 10;
int slic_rgb_dist = 30;

static void usage(const char* progname)
{
  fprintf (stderr, "Usage: %s [options]\n"
                   "\n"
                   "Options:\n"
                   "  -l URL    Specify LCM URL\n"
                   "  -h        This help message\n", 
                   g_path_get_basename(progname));
  exit(1);
}

typedef std::vector<Point2f> Cluster;
typedef std::vector<Point3f> Cluster3D;
typedef std::deque<Cluster> ClusterTrack;
typedef std::deque<Cluster3D> Cluster3DTrack;
std::vector<ClusterTrack> cluster_tracks;
std::vector<Cluster3DTrack> cluster_tracks_3d;

struct Frame { 
    kinect_data kinect;
};

struct FrameData { 
    // std::vector<KeyPoint> keypts;

    int nclasses;
    std::vector<Point2f> pts;
    std::vector<int> labels;

    std::vector<Cluster> class_pts;
    std::vector<int> cluster_corr; // cluster_corr.size() == nclasses;

    std::vector<int> track_id;
    bool processed;

    FrameData() : nclasses(0), processed(false) {}

    // std::vector<Point> pts_next;
    // std::vector<uchar> status;

    // std::vector<Vec3f> keypts3d;
    // std::vector<Vec3f> keypts3d_next;    

    // std::vector<int> query_inds;
    // std::vector<int> train_inds;
    // std::vector<uchar> match_outliers;
    // Mat desc;
};

struct Track { 
    int id; 
    std::vector<Point2f> pts;
};

struct TrackData { 
    std::vector<Track> tracks;
    std::vector<double> conf;
};

// struct Cluster { 
//     int id;
//     std::vector<Point2f> pts;
// };

std::deque<Frame> frame_queue;
std::deque<FrameData> framedata_queue;

TrackData track_data;

#if PCL
void computeICP(Eigen::Matrix4f& icp_tf, Eigen::Matrix4f& guess_tf, 
                pcl::PointCloud<pcl::PointXYZRGB>::Ptr& final, 
                pcl::PointCloud<pcl::PointXYZRGB>::Ptr& live , 
                pcl::PointCloud<pcl::PointXYZRGB>::Ptr& ref) { 

    if (!ref->points.size() || !live->points.size())
        return;

    pcl::IterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;
    //icp.setMaximumIterations (10);
    icp.setTransformationEpsilon (1e-5);
    icp.setRANSACOutlierRejectionThreshold(5);
    //icp.setMaxCorrespondenceDistance (0.05);
    //std::cerr << "Ref: " << ref->points.size() << std::endl;
    icp.setInputCloud(ref);
    icp.setInputTarget(live);

    double t1 = timestamp_us();
    icp.align(*final, guess_tf); 
    double t2 = timestamp_us();

    icp_tf = icp.getFinalTransformation();
    std::cout << icp_tf << std::endl;
    std::cout << "TF eps: " << icp.getTransformationEpsilon () << std::endl;    
    std::cout << "fitness eps: " << icp.getEuclideanFitnessEpsilon () << std::endl;    
    std::cout << "fitness score: " << icp.getFitnessScore () << std::endl;    

    if (!icp.hasConverged())
        return;

    // Update step
    guess_tf = icp_tf;

    Eigen::Matrix4f tf_g_to_can = Eigen::Matrix4f::Identity();
    tf_g_to_can(0,3) = mean_canonical_obj_pcl.x;
    tf_g_to_can(1,3) = mean_canonical_obj_pcl.y;
    tf_g_to_can(2,3) = mean_canonical_obj_pcl.z;

    Eigen::Matrix4f tf_g_to_live = icp_tf * tf_g_to_can;

#define TRANSFORMATION_BASIS 0
#if TRANSFORMATION_BASIS
    computeTransformationBasis(tf_g_to_live);
#endif

    double rot[9];
    rot[0] = tf_g_to_live(0,0), rot[1] = tf_g_to_live(0,1), rot[2] = tf_g_to_live(0,2), 
        rot[3] = tf_g_to_live(1,0), rot[4] = tf_g_to_live(1,1), rot[5] = tf_g_to_live(1,2), 
        rot[6] = tf_g_to_live(2,0), rot[7] = tf_g_to_live(2,1), rot[8] = tf_g_to_live(2,2);

    bot_core_rigid_transform_t tf;
    bot_matrix_to_quat(rot, tf.quat);
    tf.utime = 0; // kinect.timestamp;
    tf.trans[0] = tf_g_to_live(0,3);
    tf.trans[1] = tf_g_to_live(1,3);
    tf.trans[2] = tf_g_to_live(2,3);
    bot_core_rigid_transform_t_publish (state->lcm, "GLOBAL_TO_LOCAL", &tf);

    printf("icp: %f milliseconds\n", (t2 - t1)*1e-3);



    return;

}
#endif

// Adapted from cv_line_template::convex_hull
void templateConvexHull(const std::vector<cv::linemod::Template>& templates, 
                        int num_modalities, cv::Point offset, cv::Size size,
                        cv::Mat& dst)
{
  std::vector<cv::Point> points;
  for (int m = 0; m < num_modalities; ++m)
  {
    for (int i = 0; i < (int)templates[m].features.size(); ++i)
    {
      cv::linemod::Feature f = templates[m].features[i];
      points.push_back(cv::Point(f.x, f.y) + offset);
    }
  }
  
  std::vector<cv::Point> hull;
  cv::convexHull(points, hull);

  dst = cv::Mat::zeros(size, CV_8U);
  const int hull_count = (int)hull.size();
  const cv::Point* hull_pts = &hull[0];
  cv::fillPoly(dst, &hull_pts, &hull_count, 1, cv::Scalar(255));
}

std::vector<CvPoint> maskFromTemplate(const std::vector<cv::linemod::Template>& templates, 
                                      int num_modalities, cv::Point offset, cv::Size size,
                                      cv::Mat& mask, cv::Mat& dst)
{
  templateConvexHull(templates, num_modalities, offset, size, mask);

  const int OFFSET = 10;
  cv::dilate(mask, mask, cv::Mat(), cv::Point(-1,-1), OFFSET);

  CvMemStorage * lp_storage = cvCreateMemStorage(0);
  CvTreeNodeIterator l_iterator;
  CvSeqReader l_reader;
  CvSeq * lp_contour = 0;

  cv::Mat mask_copy = mask.clone();
  IplImage mask_copy_ipl = mask_copy;
  cvFindContours(&mask_copy_ipl, lp_storage, &lp_contour, sizeof(CvContour),
                 CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

  std::vector<CvPoint> l_pts1; // to use as input to cv_primesensor::filter_plane

  cvInitTreeNodeIterator(&l_iterator, lp_contour, 1);
  while ((lp_contour = (CvSeq *)cvNextTreeNode(&l_iterator)) != 0)
  {
    CvPoint l_pt0;
    cvStartReadSeq(lp_contour, &l_reader, 0);
    CV_READ_SEQ_ELEM(l_pt0, l_reader);
    l_pts1.push_back(l_pt0);

    for (int i = 0; i < lp_contour->total; ++i)
    {
      CvPoint l_pt1;
      CV_READ_SEQ_ELEM(l_pt1, l_reader);
      /// @todo Really need dst at all? Can just as well do this outside
      cv::line(dst, l_pt0, l_pt1, CV_RGB(0, 255, 0), 2);

      l_pt0 = l_pt1;
      l_pts1.push_back(l_pt0);
    }
  }
  cvReleaseMemStorage(&lp_storage);

  return l_pts1;
}

#if PCL
void populatePCL(Frame& frame, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& pcl, Rect r = Rect(0,0,WIDTH,HEIGHT) ){
    
    int DECI = 1;

    pcl->width = r.width / DECI;
    pcl->height = r.height / DECI;
    pcl->points.resize(pcl->width * pcl->height);

    // Convert to XYZ
    int i, j, f_idx, c_idx, u, v;
    // cv::Vec2f* disparity_map = (cv::Vec2f*)kinect.disparity_map.data;
    // double* depth_map = (double*)kinect.depth.data;
    unsigned char* rgbp = frame.kinect.rgb.data;
    
    int idx = 0;
    for (i = r.y; i < r.y + r.height; i+=DECI) {
        unsigned char* rgbpv = rgbp + i*frame.kinect.rgb.step;
        // cv::Vec2f* dispv = disparity_map + v*kinect.disparity_map.cols;
        // double* depthv = depth_map + v*kinect.depth.cols;

        for(j = r.x; j < r.x + r.width; j+=DECI) {

            uint8_t r, g, b;
            r = rgbpv[3*j+2]; // s[2];
            g = rgbpv[3*j+1]; // s[1];
            b = rgbpv[3*j+0]; // s[0];
            
            Vec3f xyz = frame.kinect.getXYZ(i,j);
            //Vec3f xyz(depthv[u] / 1000.0 /* dispv[u][0]*/, depthv[u] / 1000.0 /* dispv[u][1]*/, depthv[u] / 1000.0);
            pcl->points[idx].x = xyz[0], pcl->points[idx].y = xyz[1], pcl->points[idx].z = xyz[2];
            pcl->points[idx].r = r, pcl->points[idx].g = g, pcl->points[idx].b = b;

            idx++;

        }
    }

    return;
} 


void populatePCL(Frame& frame, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& pcl, cv::Mat& mask){

    unsigned char* pmask = mask.data;

    int count = 0;
    for(int j=0; j<mask.rows * mask.cols; )
        if (pmask[j++])
            count++;

    int DECI = 1;
    if (mask.empty()) { 
        pcl->width = WIDTH / DECI;
        pcl->height = HEIGHT / DECI;
        pcl->points.resize(pcl->width * pcl->height);
    } else { 
        pcl->width = count;
        pcl->height = 1;
        pcl->points.resize(count);
    }    

    // Convert to XYZ
    int i, j, f_idx, c_idx, u, v;
    // cv::Vec2f* disparity_map = (cv::Vec2f*)kinect.disparity_map.data;
    // double* depth_map = (double*)kinect.depth.data;
    unsigned char* rgbp = frame.kinect.rgb.data;
    pmask = mask.data;
    
    int idx = 0;
    for (i = 0; i < mask.rows; i+=DECI) {
        unsigned char* rgbpv = rgbp + i*frame.kinect.rgb.step;
        unsigned char* maskv = pmask + (i)*mask.step;
        // cv::Vec2f* dispv = disparity_map + v*kinect.disparity_map.cols;
        // double* depthv = depth_map + v*kinect.depth.cols;

        for(j = 0; j < mask.cols; j+=DECI) {
            if (!maskv[j]) 
                continue;
            uint8_t r, g, b;
            r = rgbpv[3*j+2]; // s[2];
            g = rgbpv[3*j+1]; // s[1];
            b = rgbpv[3*j+0]; // s[0];
            
            Vec3f xyz = frame.kinect.getXYZ(i,j);
            //Vec3f xyz(depthv[u] / 1000.0 /* dispv[u][0]*/, depthv[u] / 1000.0 /* dispv[u][1]*/, depthv[u] / 1000.0);
            pcl->points[idx].x = xyz[0], pcl->points[idx].y = xyz[1], pcl->points[idx].z = xyz[2];
            pcl->points[idx].r = r, pcl->points[idx].g = g, pcl->points[idx].b = b;

            idx++;
        }
    }

    return;
} 
#endif

// static void
// on_segmented_frame (const lcm_recv_buf_t *rbuf, const char *channel,
//                     const bot_core_image_t *seg_msg, void *user_data ) { 

//     // Segmented image
//     Mat3b seg = Mat(seg_msg->height, seg_msg->width, CV_8UC3);
//     memcpy(seg.data, seg_msg->data, seg_msg->width * seg_msg->height * 3);

//     // Grayscale segmented image (remove and fix the above memcpy to do this in one pass)
//     Mat1b segu; 
//     seg.convertTo(segu, CV_8UC1);
    
//     imshow("segmented image", segu);
// }



// // DESCRIPTOR
// // Our propose FREAK descriptor
// // (roation invariance, scale invariance, pattern radius corresponding to SMALLEST_KP_SIZE, number of octaves, file containing list of selected pairs)
// // FreakDescriptorExtractor extractor(true, true, 22, 4, kResPath + "selected_pairs.bin");
// FreakDescriptorExtractor extractor(true, true, 22, 4, "");


void convert_to_pffimage(const Mat& img_mat, image<rgb>* img) { 
    unsigned char* imgp = img_mat.data;
    for (int i=0; i<img_mat.rows; i++, imgp += img_mat.step) { 
        for (int j=0; j<img_mat.cols; j++) { 
            rgb curr;
            curr.r = imgp[3*j+2];
            curr.g = imgp[3*j+1];
            curr.b = imgp[3*j];
            img->data[i*img_mat.cols+j] = curr;
        }
    }
    return;
}


void convert_to_mat(image<rgb>* img, Mat& img_mat) { 
    unsigned char* imgp = img_mat.data;
    for (int i=0; i<img_mat.rows; i++, imgp += img_mat.step) { 
        for (int j=0; j<img_mat.cols; j++) { 
            rgb curr = img->data[i*img_mat.cols+j];            
            imgp[3*j+2] = curr.r;
            imgp[3*j+1] = curr.g;
            imgp[3*j] = curr.b;
        }
    }
    return;
}




void eliminate_top_modes(Mat& fg, Mat& bg_hist) { 

    int bins = BINS;

    Point maxIdx(-1,-1);
    double maxVal=0;
    minMaxLoc(bg_hist, 0, &maxVal, 0, &maxIdx);
    
    Mat first_mode = bg_hist.clone();
    for( int l = 0; l < bins; l++ )
        if (!(l >= maxIdx.y-1 && l <= maxIdx.y+1))
            first_mode.at<float>(l) = 0;

    // L,A,B varies from 0 to 255, see cvtColor
    float lab_ranges[] = { 0, 255 };
    const float* ranges[] = { lab_ranges };

    Mat backProject;
    calcBackProject(&fg, 1, 0, first_mode, backProject, ranges);

    opencv_utils::imshow("backProject", backProject);
}

static bool cluster_sizesort(const std::pair<int, int>& lhs, const std::pair<int, int>& rhs) { 
    return lhs.second > rhs.second;
}

struct PartitionPts
{
    PartitionPts() {}
    bool operator()(const Point3f& a, const Point3f& b) const {
        Point3f a_(a), b_(b);
        a_.x = 0; a_.y = 0; 
        b_.x = 0; b_.y = 0; 
        
        return (opencv_utils::l2_dist(a_,b_) < 5);
    }

    // const vector<int>* dstart;
    // const Set2i* pairs;
};

struct contour_info { 
    double area; 
    double r;
    double val;
    int idx;
    contour_info(double _area, double _r, int _idx) : area(_area), r(_r), idx(_idx) { 
        val = _area / (_r * _r);
    }
};

struct SLIC_info { 
    SLIC slic;
    Mat mask;
    std::vector<double> mu_r, mu_g, mu_b, cluster_count;

    SLIC_info() { 
        mask = Mat();
    }

    SLIC_info(const SLIC& _slic, const cv::Mat& _mask) { 
        slic = _slic;
        mask = _mask.clone();
    }
};


    
static bool contours_info_sort_asc(const contour_info& lhs, const contour_info& rhs) { 
    return lhs.val > rhs.val;
}


void pick_low_mi_blob(const Mat& backProject_depth_img, Mat& mask) { 
    //----------------------------------
    // Pick blob with least moment of inertia about the center
    //----------------------------------
    
    // === Find contours ===
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    findContours(backProject_depth_img.clone(), contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

    // === Get the moments ===
    vector<Moments> mu(contours.size() );
    for( int i = 0; i < contours.size(); i++ )
        { mu[i] = moments( contours[i], false ); }

    // === Get the mass centers ===
    vector<Point2f> mc( contours.size() );
    for( int i = 0; i < contours.size(); i++ )
        { mc[i] = Point2f( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00 ); }

    // === Draw contours === 
    Mat drawing = Mat::zeros( backProject_depth_img.size(), CV_8UC3 );

    std::vector<contour_info> contours_info;
    for (int j=0; j<contours.size(); j++)
        contours_info.push_back(contour_info(contourArea(contours[j]), opencv_utils::l2_dist(mc[j], Point2f(mask.cols/2, mask.rows/2)), j));
    std::sort(contours_info.begin(), contours_info.end(), contours_info_sort_asc);

    // === contours info (redefine metric for sort) === 
    // cerr << "contours info: " << endl;
    // for (int j=0; j<contours_info.size(); j++)
    //     cerr << contours_info[j].val << " " << contours_info[j].area << " " << contours_info[j].r << endl;

    // === Fill only 1 object with large area, closer to ROI center === 
    std::vector<Scalar> contour_colors(contours.size());
    opencv_utils::fillColors(contour_colors);
    for( int i = 0; i< contours.size(); i++ ) 
        drawContours( drawing, contours, i, contour_colors[i], 1, 8, hierarchy, 0, Point() ); 
    for( int i = 0; i< contours_info.size() && i<1; i++ ) 
        drawContours( drawing, contours, contours_info[i].idx, contour_colors[contours_info[i].idx], 
                      CV_FILLED, 8, hierarchy, 0, Point() ); 

    // === Create object mask === 
    mask = Mat::zeros(backProject_depth_img.size(), CV_8UC1);
    for( int i = 0; i< contours_info.size() && i<1; i++ ) 
        drawContours( mask, contours, contours_info[i].idx, Scalar(255), CV_FILLED, 8, hierarchy, 0, Point() ); 

    // === Show in a window ===
    opencv_utils::imshow( "Contours", drawing );
    return;
}

SLIC_info slic_superpixel_segmentation(Frame& frame, const Rect& rect = Rect(0,0,WIDTH,HEIGHT)) { 
    Mat img = frame.kinect.getRGB()(rect).clone();
     
    double t1 = timestamp_us();
    unsigned int* imgp = new unsigned int[img.rows * img.cols];
    opencv_utils::convert_to_argb(img, imgp);
    double t2 = timestamp_us();
    // std::cerr << " time: " << (t2-t1)*1e-3 << std::endl;

    //----------------------------------
    // Perform SLIC on the image buffer
    //----------------------------------
    SLIC_info slic_info;
    SLIC& slic = slic_info.slic;
    t1 = timestamp_us();
    slic.DoSuperpixelSegmentation_ForGivenSuperpixelSize(imgp, img.cols, img.rows, slic_size, slic_m);
    t2 = timestamp_us();
    // std::cerr << " superpixel time: " << (t2-t1)*1e-3 << std::endl;

    // Alternately one can also use the function DoSuperpixelSegmentation_ForGivenStepSize() for a desired superpixel size
    //----------------------------------
    // Draw boundaries around segments
    //----------------------------------
    int* klabels = &slic.klabels[0];
    // std::cerr << "NUM: " << slic.klabels.size() << " " << slic.numlabels << std::endl;
    t1 = timestamp_us();
    slic.DrawContoursAroundSegments(imgp, klabels, img.cols, img.rows, 0xff0000);
    t2 = timestamp_us();
    // std::cerr << " draw time: " << (t2-t1)*1e-3 << std::endl;

    //----------------------------------
    // Convert to Mat
    //----------------------------------
    Mat slic_img(img.rows,img.cols, img.type());
    opencv_utils::convert_to_mat(imgp, slic_img);

    //----------------------------------
    // Centroids
    //----------------------------------
    for (int j=0; j<slic.kseeds_x.size(); j++)  { 
        int x = slic.kseeds_x[j];
        int y = slic.kseeds_y[j];
        int n = slic.klabels[y*img.cols + x];

        circle(slic_img, Point2f(x,y), 1, Scalar(0,0,0), CV_FILLED);        
        cv::putText(slic_img, cv::format("%i", n),Point2f(x,y), 0, 0.25, cv::Scalar(255,255,255),1);
    }

    std::vector<Point3f> border_lab_feats;
    std::vector<Point2f> border_lab_feats2d;
    std::set<int> border_labels;

#if 0
    //----------------------------------
    // Border color histogram (in Lab)
    //----------------------------------

    for (int j=0; j<slic.numlabels; j++) { 
        int x = slic.kseeds_x[j];
        int y = slic.kseeds_y[j];
        int n = slic.klabels[y*img.cols + x];
        
        if (seedx<=slic_dim || seedy<= slic_dim || img.cols-seedx<=slic_dim || img.rows-seedy<=slic_dim) { 
            if (seedx <= 0 || seedy <= 0 || seedx > img.cols  || seedy > img.rows )
                continue;
            std::cerr << seedx << " " << seedy << ": (" << 
                slic.kseeds_l[j] << "," << 
                slic.kseeds_a[j] << "," << 
                slic.kseeds_b[j] << ")" << std::endl;
            border_lab_feats.push_back(Point3f(slic.kseeds_l[j], slic.kseeds_a[j], slic.kseeds_b[j]));
            border_lab_feats2d.push_back(Point2f(seedx, seedy));
            border_labels.insert(j);
        }
    }
#else 
    //----------------------------------
    // Border color histogram (in RGB/HSV)
    //----------------------------------
    slic_info.mu_r = vector<double>(slic.numlabels,0);
    slic_info.mu_g = vector<double>(slic.numlabels,0);
    slic_info.mu_b = vector<double>(slic.numlabels,0);
    slic_info.cluster_count = vector<double>(slic.numlabels,0);

    std::vector<double>& mu_r = slic_info.mu_r;
    std::vector<double>& mu_g = slic_info.mu_g;
    std::vector<double>& mu_b = slic_info.mu_b;
    std::vector<double>& cluster_count = slic_info.cluster_count;

    int ind(0);
    vector<Mat> channels;  
    Mat hsv;

    // #if HSV
    // cvtColor(img, hsv, CV_BGR2HSV);
    // cv::split(hsv, channels);
    // #else    
    cv::split(img, channels);
    // #endif

    // Mean computed in RGB color space
    uchar* pimg_b = channels[0].data, *pimg_g = channels[1].data, *pimg_r = channels[2].data;
    for( int r = 0; r < img.rows; r++ ) {
        for( int c = 0; c < img.cols; c++ ) {
            mu_r[klabels[ind]] += pimg_r[ind];
            mu_g[klabels[ind]] += pimg_g[ind];
            mu_b[klabels[ind]] += pimg_b[ind];
            cluster_count[klabels[ind]] += 1.0;
            ind++;
        }
    }

    // Normalize
    for (int j=0; j<slic.numlabels; j++) { 
        if (cluster_count[j] <= 0) cluster_count[j] = 1;
        mu_r[j] *= 1.f/cluster_count[j], mu_g[j] *= 1.f/cluster_count[j], mu_b[j] *= 1.f/cluster_count[j];        

#if HSV
        Vec3b hsv = opencv_utils::bgr_to_hsvvec(Point3f(mu_b[j], mu_g[j], mu_r[j]));
        mu_b[j] = hsv[0], mu_g[j] = hsv[1], mu_r[j] = hsv[2];
#endif
    }
        
    float rgb_scale = 1.f / 255;
    for (int j=0; j<slic.numlabels; j++) { 
        int x = slic.kseeds_x[j];
        int y = slic.kseeds_y[j];
        if (x <= 0 || y <= 0 || x > img.cols  || y > img.rows )
            continue;        
        int n = slic.klabels[y*img.cols + x];
        if (x<=slic_dim || y<= slic_dim || img.cols-x<=slic_dim || img.rows-y<=slic_dim) { 

            border_lab_feats.push_back(Point3f(mu_b[n], mu_g[n], mu_r[n]));
            border_lab_feats2d.push_back(Point2f(x, y));
            border_labels.insert(n);
            // std::cerr << seedx << " " << seedy << ": (" << 
            //     mu_b[j] << "," << 
            //     mu_g[j] << "," << 
            //     mu_r[j] << ")" << std::endl;
        }
    }

#endif

    //----------------------------------
    // Masks for BG and FG
    //----------------------------------
    Mat_<uchar> fg_hist_mask = Mat_<uchar>::ones(1, slic.numlabels);
    Mat_<uchar> bg_hist_mask(1, slic.numlabels);
    for (int j=0; j<slic.numlabels; j++) {
        if (border_labels.find(j) == border_labels.end())
            bg_hist_mask.at<uchar>(j) = 0;
        else 
            bg_hist_mask.at<uchar>(j) = 255;
    }

    //----------------------------------
    // Plot the slic img with mean color features
    //----------------------------------
    Mat3b mean_slic_img(slic_img.rows, slic_img.cols);
    // std::set<int> n_done;
    for (int y=0; y<mean_slic_img.rows; y++) { 
        for (int x=0; x<mean_slic_img.cols; x++) { 
            int idx = y*mean_slic_img.cols + x;
            int n = klabels[idx];
#if HSV
            mean_slic_img(y,x) = opencv_utils::hsv_to_bgrvec(Point3f(mu_b[n], mu_g[n], mu_r[n]));
            // if (n_done.find(n) == n_done.end()) { 
            //     n_done.insert(n);
            //     std::cerr << n << ": " << mu_b[n] << "," << mu_g[n]<< "," << mu_r[n] << ": " << mean_slic_img(y,x) << std::endl;
            // }
#else
            mean_slic_img(y,x) = Vec3b(mu_b[n], mu_g[n], mu_r[n]);
#endif
        }
    }

    // === Plot the border features ===
    for( int j = 0; j < border_lab_feats2d.size(); j++ )
        circle( mean_slic_img, border_lab_feats2d[j], 2, Scalar(0,0,0), CV_FILLED, CV_AA );

    opencv_utils::imshow("Mean slic img", mean_slic_img);

    // ------------------------------
    // Slic color histogram
    // ------------------------------

    // === Find min=max depth === 
    double hue_max = 180, hue_min = 0;
    minMaxLoc(Mat(mu_b), &hue_min, &hue_max, 0, 0);
    const int hue_bins = ((hue_max - hue_min) / 5) + 1;

    // === Heat map based on normed hue ===
    Mat3b mean_hue_img = Mat3b::zeros(slic_img.rows, slic_img.cols);
    std::vector<double> mu_hue_norm(slic.numlabels, 0);
    for (int y=0; y<mean_hue_img.rows; y++) { 
        for (int x=0; x<mean_hue_img.cols; x++) { 
            int idx = y*mean_hue_img.cols + x;
            int n = klabels[idx];
            double hue = double(mu_b[n]);
            mu_hue_norm[n] = (hue - hue_min) * 1.f / (hue_max - hue_min);
            mean_hue_img(y,x) = opencv_utils::hsv_to_bgrvec(Point3f(mu_hue_norm[n] * 120, 255, 255));
        }
    }
    opencv_utils::imshow("slic img normed green", mean_hue_img);

    // std::cerr << "Hue: " << hue_min << " " << hue_max << std::endl;
    // std::cerr << "Hue bins: " << hue_bins << std::endl;

    // === Hue histogram img (only superpixels) ===
    Mat_<float> hue_hist_img(1, slic.numlabels);
    for (int j=0; j<hue_hist_img.cols; j++) { 
        int x = slic.kseeds_x[j];
        int y = slic.kseeds_y[j];
        if ( x <= 0 || y <= 0 || x > img.cols  || y > img.rows )
            continue;        
        int n = slic.klabels[y*img.cols+x];
        
        hue_hist_img.at<float>(n) = mu_b[n];
    }

    // std::cerr << "Hue Hist Img: " << hue_hist_img << std::endl;
    // std::cerr << "Hue Hist Img mask: " << bg_hist_mask << std::endl;

    // === Compute normalized histogram for background given mask and depth values ===
    Mat bg_hue_hist = opencv_utils::compute_1D_histogram(hue_hist_img, bg_hist_mask, 
                                                         "BG hue Histogram", 0, 180, hue_bins); //0-1 normalized
    Mat fg_hue_hist = opencv_utils::compute_1D_histogram(hue_hist_img, fg_hist_mask, 
                                                         "FG hue Histogram", 0, 180, hue_bins); //0-1 normalized

    // std::cerr << "BG Hue hist: " << bg_hue_hist << std::endl << std::endl;
    // std::cerr << "FG Hue hist: " << fg_hue_hist << std::endl << std::endl;

    // === Compute backprojection with all depth values (mean superpixel depth values) ===
    float hue_ranges[] = {0, 180.f};
    Mat backProject_hue;
    const float* hranges[] = { hue_ranges };
    calcBackProject(&hue_hist_img, 1, 0, bg_hue_hist, backProject_hue, hranges); 
    threshold(backProject_hue, backProject_hue, 0.2, 1, THRESH_BINARY_INV);

    // std::cerr << "Backproject hue: " << backProject_hue << std::endl;

    // === Visualize backprojection === 
    Mat1b backProject_hue_img = Mat1b::zeros(slic_img.rows, slic_img.cols);
    for (int y=0; y<backProject_hue_img.rows; y++) { 
        for (int x=0; x<backProject_hue_img.cols; x++) { 
            int idx = y*backProject_hue_img.cols + x;
            int n = klabels[idx];
            backProject_hue_img(y,x) = uchar(backProject_hue.at<float>(n) * 255.f);
        }
    }

    // opencv_utils::imshow("backProject hue", backProject_hue);
    // opencv_utils::imshow("backProject hue img", backProject_hue_img);

    //----------------------------------
    // Plot the slic img with mean depth
    //----------------------------------
    Mat3b mean_slic_depth_img = Mat3b::zeros(slic_img.rows, slic_img.cols);
    Mat_<double> mean_slic_depth = Mat_<double>::zeros(slic_img.rows, slic_img.cols);
    std::vector<double> mu_depth(slic.numlabels, 0);
    std::vector<int> depth_count(slic.numlabels, 0);
    for (int y=0; y<mean_slic_depth_img.rows; y++) { 
        for (int x=0; x<mean_slic_depth_img.cols; x++) { 
            int idx = y*mean_slic_depth_img.cols + x;
            int n = klabels[idx];
            double depth = frame.kinect.getDepth(rect.y + y, rect.x + x);
            if (depth >= 0.1 && depth < 5) { 
                mu_depth[n] += depth; depth_count[n]++;
            }
        }
    }

    // === Normalize ===
    for (int j=0; j<mu_depth.size(); j++) mu_depth[j] *= 1.f / depth_count[j];
    // std::cerr << "mu_depth_norm: " << Mat(mu_depth) << std::endl;
    
    // === Find min=max depth === 
    // Only allow upto 1cm bin resolution
    double depth_max = 1, depth_min = .1;
    minMaxLoc(Mat(mu_depth), &depth_min, &depth_max, 0, 0);
    depth_min -= 0.1; depth_max += 0.1;
    const int depth_bins = ((depth_max - depth_min) / 0.02) + 1;

    // std::cerr << "Depth: " << depth_min << " " << depth_max << std::endl;
    // std::cerr << "Depth bins: " << depth_bins << std::endl;

    // === Heat map based on depth ===
    std::vector<double> mu_depth_norm(slic.numlabels, 0);
    for (int y=0; y<mean_slic_depth_img.rows; y++) { 
        for (int x=0; x<mean_slic_depth_img.cols; x++) { 
            int idx = y*mean_slic_depth_img.cols + x;
            int n = klabels[idx];
            double depth = mu_depth[n];
            mu_depth_norm[n] = (depth - depth_min) * 1.f / (depth_max - depth_min);
            if (depth > 0.1f && depth < 5) { 
                mean_slic_depth(y,x) = mu_depth_norm[n];
                mean_slic_depth_img(y,x) = opencv_utils::hsv_to_bgrvec(Point3f(mu_depth_norm[n] * 120, 255, 255));
            }
        }
    }
    opencv_utils::imshow("slic img depth", mean_slic_depth_img);

    //----------------------------------
    // Slic Mean depth histogram
    //----------------------------------
    Mat_<float> depth_hist_img(1, slic.numlabels);
    for (int j=0; j<depth_hist_img.cols; j++) 
        depth_hist_img.at<float>(j) = mu_depth_norm[j];

    // std::cerr << "Depth Hist Img: " << depth_hist_img << std::endl;
    // std::cerr << "Depth Hist Img mask: " << bg_depth_hist_mask << std::endl;
    
    // === Compute normalized histogram for background given mask and depth values ===
    Mat bg_depth_hist = opencv_utils::compute_1D_histogram(depth_hist_img, bg_hist_mask, 
                                                           "BG depth Histogram", 0, 1, depth_bins); //0-1 normalized
    Mat fg_depth_hist = opencv_utils::compute_1D_histogram(depth_hist_img, fg_hist_mask, 
                                                           "FG depth Histogram", 0, 1, depth_bins); //0-1 normalized

    // std::cerr << "BG Depth hist: " << bg_depth_hist << std::endl << std::endl;
    // std::cerr << "FG Depth hist: " << fg_depth_hist << std::endl << std::endl;
    
    // === Compute backprojection with all depth values (mean superpixel depth values) ===
    float depth_ranges[] = {0, 1};
    const float* ranges[] = { depth_ranges };
    Mat backProject_depth;
    calcBackProject(&depth_hist_img, 1, 0, bg_depth_hist, backProject_depth, ranges);
    threshold(backProject_depth, backProject_depth, 0.2, 1, THRESH_BINARY_INV);
    // std::cerr << "Backproject depth: " << backProject_depth << std::endl;
    
    // === Visualize backprojection === 
    Mat1b backProject_depth_img = Mat1b::zeros(slic_img.rows, slic_img.cols);
    for (int y=0; y<backProject_depth_img.rows; y++) { 
        for (int x=0; x<backProject_depth_img.cols; x++) { 
            int idx = y*backProject_depth_img.cols + x;
            int n = klabels[idx];
            backProject_depth_img(y,x) = backProject_depth.at<float>(n) * 255;
        }
    }

    //----------------------------------
    // Pick blob with least moment of inertia about the center
    //----------------------------------
    pick_low_mi_blob(backProject_depth_img, slic_info.mask);

    // imshow("backProject depth", backProject_depth);
    opencv_utils::imshow("backProject depth img", backProject_depth_img);
    opencv_utils::imshow("slic_info.mask", slic_info.mask);


    //----------------------------------
    // Visualize object RGB values
    //----------------------------------
    Mat slic_obj_img = Mat::zeros(slic_img.size(), CV_8UC3);
    for (int j=0; j<slic_info.slic.numlabels; j++) { 
        int x = slic_info.slic.kseeds_x[j];
        int y = slic_info.slic.kseeds_y[j];
        if (x <= 0 || y <= 0 || x > slic_img.cols  || y > slic_img.rows )
            continue;        
        int n = slic_info.slic.klabels[y*slic_img.cols+x];

        // if (slic_info.mask.at<uchar>(y,x))
        //     std::cerr<<"* "<<n<<": "
        //              <<"("<<slic_info.mu_b[n]<<","<<slic_info.mu_g[n]<<","<<slic_info.mu_r[n]<<")"
        //              <<std::endl;
        // else 
        //     std::cerr<<"  "<<n<<": "
        //              <<"("<<slic_info.mu_b[n]<<","<<slic_info.mu_g[n]<<","<<slic_info.mu_r[n]<<")"
        //              <<std::endl;

#if HSV
        Scalar s = opencv_utils::hsv_to_bgr(Point3f(slic_info.mu_b[n], slic_info.mu_g[n], slic_info.mu_r[n]));
#else
        Scalar s(slic_info.mu_b[n], slic_info.mu_g[n], slic_info.mu_r[n]);
#endif
        circle(slic_obj_img, Point(x,y), 3, s,CV_FILLED);
        cv::putText(slic_obj_img, cv::format("%i", n),Point2f(x,y), 0, 0.25, s,1);
    }
    opencv_utils::imshow("SLIC Object RGB", slic_obj_img);

    //----------------------------------
    // Clean up
    //----------------------------------
    if(imgp) delete [] imgp;
 
    opencv_utils::imshow("slic", slic_img);
    return slic_info;
}


void pff_superpixel_segmentation(Mat& img, const Rect& _r = Rect(0,0,WIDTH,HEIGHT)) { 
    // Mat img = image(r);
     
    double t1 = timestamp_us();

    image<rgb>* imgp = new image<rgb>(img.cols, img.rows);
    convert_to_pffimage(img, imgp);

    double t2 = timestamp_us();
    // std::cerr << " time: " << (t2-t1)*1e-3 << std::endl;

    t1 = timestamp_us();
    int num_ccs;
    image<rgb>* out = segment_image(imgp, pff_sigma * 1.f / 100 , pff_K, pff_min, &num_ccs);
    t2 = timestamp_us();
    // std::cerr << " superpixel time: " << (t2-t1)*1e-3 << std::endl;

    Mat pff_img(out->height(), out->width(), img.type());
    convert_to_mat(out, pff_img);
    delete out;
    imshow("pff", pff_img);
    
    // universe* u = get_segments(imgp, pff_sigma * 1.f / 100, pff_K, pff_min, &num_ccs);

    // // Centroids of segments
    // std::map<int, Point2f> mu_segments;
    // std::map<int, int> mu_segments_count;
    // for (int i=0; i<img.rows; i++) { 
    //     for (int j=0; j<img.cols; j++) { 
    //         int comp = u->find(i * img.cols + j);
    //         if (mu_segments.find(comp) == mu_segments.end()) { 
    //             mu_segments[comp] = Point2f(j,i);
    //             mu_segments_count[comp] = 1;
    //         } else { 
    //             mu_segments[comp] += Point2f(j,i);
    //             mu_segments_count[comp] += 1;

    //         }
    //     }
    // }

    // // divide by total count
    // for (std::map<int, Point2f>::iterator it=mu_segments.begin(); it!=mu_segments.end(); it++) { 
    //     Point2f& p = it->second;
    //     int count = mu_segments_count[it->first];
    //     p.x /= count;
    //     p.y /= count;
    // }        

    // for (std::map<int, Point2f>::iterator it=mu_segments.begin(); it!=mu_segments.end(); it++) { 
    //     frame.keypts.push_back(KeyPoint(it->second,1));
    //     frame.pts.push_back(cv::Point(cvRound(it->second.x), cvRound(it->second.y)));
    // }
    // std::cerr << " keypts: " << frame.keypts.size() << std::endl;
    // std::cerr << " pts: " << frame.pts.size() << std::endl;

    // delete u;
    delete imgp;

    return;
}

// void fast_detector(Frame& frame) { 
//     // detect fast
//     FAST(frame.kinect.getGray(), frame.keypts, 20, true);
//     // _detector->detect( frame.kinect.getGray(), frame.keypts );

//     // // extract
//     // double t1 = timestamp_us();
//     // _descriptorExtractor->compute( frame.kinect.getGray(), frame.keypts, frame.desc );
//     // double t2 = timestamp_us();
//     // std::cout << "extraction time [s]: " << (t2-t1)*1e-3 << std::endl;

//     for (int j=0; j<frame.keypts.size(); j++) 
//         frame.pts.push_back(Point(cvRound(frame.keypts[j].pt.x), cvRound(frame.keypts[j].pt.y)));
//     //KeyPoint::convert(frame.keypts, frame.pts);
//     std::cerr << " keypts: " << frame.keypts.size() << std::endl;
//     std::cerr << " pts: " << frame.pts.size() << std::endl;

// }

// void contour_detector(Frame& frame) { 
//     Mat1b canny;
//     int low_thresh = 20;
//     int high_thresh = 160;
//     Canny(frame.kinect.getBlurredGray(), canny, low_thresh, high_thresh, 3);
    
//     vector < vector<Point> > contours;
//     vector<Vec4i> hierarchy;
//     findContours(canny, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

//     Mat1b output = Mat1b::zeros(frame.kinect.getGray().size());
//     int contour_thresh = 20;
//     for( int i = 0; i<contours.size(); i++ ) {
//         const vector<Point>& ci = contours[i];
//         if (ci.size() < contour_thresh)
//             continue;
//         for (int j=0; j<ci.size(); j++) 
//             frame.pts.push_back(ci[j]);
//         drawContours( output, contours, i, Scalar(40), 1, 8, hierarchy, 0, Point() );
//     }
//     imshow("Train Contour", output);
// }

void simple_match(Ptr<DescriptorMatcher>& descriptorMatcher,
                     const Mat& descriptors1, const Mat& descriptors2,
                  vector<DMatch>& matches12 ) { 
    descriptorMatcher->match( descriptors1, descriptors2, matches12 );
}

void convert_pointf_to_point(const std::vector<cv::Point2f>& ptsf, std::vector<cv::Point>& pts) { 
    pts = std::vector<cv::Point>(ptsf.size());
    for (int j=0; j<ptsf.size(); j++)
        pts[j] = cv::Point(cvRound(ptsf[j].x),cvRound(ptsf[j].y));
    return;
}

// void match_fast_features(Frame& frame) { 
//     if (frame_queue.size()) { 
//         Frame& prev_frame = frame_queue.front();
//         const Mat& prev_gray = prev_frame.kinect.getGray();

//         const std::vector<KeyPoint>& prev_pts = prev_frame.keypts;
//         const std::vector<KeyPoint>& curr_pts = frame.keypts;

//         std::vector< DMatch>   filtered_matches;
//         BruteForceMatcher<Hamming> matcher;

//         // match
//         double t3 = timestamp_us();
//         simple_match(_descriptorMatcher, frame.desc, prev_frame.desc, filtered_matches);
//         double t4 = timestamp_us();
//         std::cout << "matching time [s]: " << (t4-t3)*1e-3 << std::endl;

//         // Draw matches
//         Mat imgMatch;
//         drawMatches(frame.kinect.getGray(), frame.keypts, prev_gray, prev_frame.keypts, 
//                     filtered_matches, imgMatch);
//         std::cerr << "Matches: " << filtered_matches.size() << std::endl;
//         imshow("filtered_matches", imgMatch);
        
//         // association
//         frame.query_inds = vector<int>( filtered_matches.size() );
//         frame.train_inds = vector<int>( filtered_matches.size() );
//         for( size_t i = 0; i < filtered_matches.size(); i++ ) { 
//             frame.query_inds[i] = filtered_matches[i].queryIdx;
//             frame.train_inds[i] = filtered_matches[i].trainIdx;
//         }

//         if (frame.keypts.size() <= 4) 
//             return;

//         Mat H12;
//         std::cout << "< Computing homography (RANSAC)..." << std::endl;

//         std::vector<Point2f> curr_frame_pts; 
//         std::vector<Point2f> prev_frame_pts; 
//         KeyPoint::convert(frame.keypts, curr_frame_pts, frame.query_inds);
//         KeyPoint::convert(prev_frame.keypts, prev_frame_pts, frame.train_inds);
//         H12 = findHomography( Mat(curr_frame_pts), Mat(prev_frame_pts), frame.match_outliers, CV_RANSAC, 3 );

//         // convert to char for drawing ransac 
//         std::vector<char> match_outliers(frame.match_outliers.size(), 0);
//         for (int j=0; j<frame.match_outliers.size(); j++) 
//             match_outliers[j] = (char)frame.match_outliers[j];

//         // Draw matches
//         Mat imgMatchRANSAC;
//         drawMatches(frame.kinect.getGray(), frame.keypts, 
//                     prev_frame.kinect.getGray(), prev_frame.keypts, 
//                     filtered_matches, imgMatchRANSAC, CV_RGB(0,255,0), CV_RGB(0,0,255), match_outliers 
//                     // DrawMatchesFlags::DRAW_OVER_OUTIMG | DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS 
//                     );
//         imshow("matchesRANSAC", imgMatchRANSAC);

//         // Refine points for update step
//         frame.pts = std::vector<cv::Point>();
//         for (int j=0; j<frame.match_outliers.size(); j++)
//             if (frame.match_outliers[j])
//                 frame.pts.push_back(cv::Point(cvRound(curr_frame_pts[j].x),cvRound(curr_frame_pts[j].y)));
//     }
// }

// void match_optical_flow(Frame& frame) { 
//     if (frame_queue.size()) { 
//         Frame& prev_frame = frame_queue.front();
//         const Mat& prev_gray = prev_frame.kinect.getGray();
//         const Mat& curr_gray = frame.kinect.getGray();

//         std::vector<float> err;
//         calcOpticalFlowPyrLK(prev_gray, curr_gray, prev_frame.pts, prev_frame.pts_next, prev_frame.status, err);

//         prev_frame.keypts3d_next = std::vector<Vec3f>(prev_frame.pts.size());
//         for (int j=0; j<prev_frame.pts.size(); j++)
//             if (prev_frame.status[j]) { 
//                 if (prev_frame.pts_next[j].x < 0 || prev_frame.pts_next[j].x >= prev_gray.cols || 
//                     prev_frame.pts_next[j].y < 0 || prev_frame.pts_next[j].y >= prev_gray.rows) { 
//                     prev_frame.status[j] = 0;
//                     continue;
//                 }
//                 prev_frame.keypts3d_next[j] = 
//                     frame.kinect.getXYZ(prev_frame.pts_next[j].y,prev_frame.pts_next[j].x);

//                 if (prev_frame.keypts3d[j][2] < .1 || prev_frame.keypts3d[j][2] > 5 || 
//                     prev_frame.keypts3d_next[j][2] < .1 || prev_frame.keypts3d_next[j][2] > 5) { 
//                     prev_frame.status[j] = 0;
//                 } else { 
//                 }
//             }
//     }
    
// }

Mat prev_gray;
vector<Point2f> prev_contours;
vector<vector<Point2f> > orig_contours;

vector< vector<Point2f> > transform_points(vector<vector<Point2f> >& contours, Mat_<double>& H) { 
    double* _H = (double*)H.data;
    vector< vector<Point2f> > tf_contours;
    for (int j=0; j<contours.size(); j++) { 
        vector<Point2f> tf_contour;
        for (int k=0; k<contours[j].size(); k++) { 
            float n = _H[6] * contours[j][k].x + _H[7] * contours[j][k].y + _H[8];
            float x = _H[0] * contours[j][k].x + _H[1] * contours[j][k].y + _H[2];
            float y = _H[3] * contours[j][k].x + _H[4] * contours[j][k].y + _H[5];
            tf_contour.push_back(Point2f(x,y));
        }
        tf_contours.push_back(tf_contour);
    }
    return tf_contours;

}

vector< vector<Point> > convert_points_to_int(vector<vector<Point2f> >& contours) { 
    vector< vector<Point> > tf_contours;
    for (int j=0; j<contours.size(); j++) { 
        vector<Point> tf_contour;
        for (int k=0; k<contours[j].size(); k++) { 
            tf_contour.push_back(Point(int(contours[j][k].x),int(contours[j][k].y)));
        }
        tf_contours.push_back(tf_contour);
    }
    return tf_contours;
}


vector< vector<Point2f> > convert_points_to_float(vector<vector<Point> >& contours) { 
    vector< vector<Point2f> > tf_contours;
    for (int j=0; j<contours.size(); j++) { 
        vector<Point2f> tf_contour;
        for (int k=0; k<contours[j].size(); k++) { 
            tf_contour.push_back(Point2f(contours[j][k].x,contours[j][k].y));
        }
        tf_contours.push_back(tf_contour);
    }
    return tf_contours;
}

void match_optical_flow(Mat& gray, vector<vector<Point> >& contours, Rect& r) { 
    
    if (!prev_gray.empty() && prev_contours.size()) { 
        
        std::vector<float> err;
        std::vector<Point2f> pts;        
        std::vector<uchar> status, status2;

        calcOpticalFlowPyrLK(prev_gray, gray, prev_contours, pts, status, err);
        Mat_<double> H = findHomography(prev_contours, pts, status2, CV_RANSAC, 1);

        assert(orig_contours.size());
        orig_contours = transform_points(orig_contours, H);
        
        Mat3b color;
        cvtColor(gray, color, CV_GRAY2BGR);
        for (int j=0; j<pts.size(); j++) { 
            if (!(int)status[j])
                continue;
            line(color, prev_contours[j], pts[j], Scalar(status2[j] ? 255 : 0,0, status2[j] ? 0 : 255), 1);
        }

        vector<vector<Point> > orig_contours_int = convert_points_to_int(orig_contours);
        for (int j=0; j<orig_contours.size(); j++) 
            drawContours( color, orig_contours_int, j, Scalar(255,255,255), 1, 8);
        imshow("LK", color);
        
    }
    prev_gray = gray;

    // Subsample
    prev_contours.clear();
    for (int j=0; j<contours.size(); j++) 
        for (int k=0; k<contours[j].size(); k+=4)
            prev_contours.push_back(Point2f(contours[j][k].x + r.x, contours[j][k].y + r.y));

    if (!orig_contours.size()) { 
        for (int j=0; j<contours.size(); j++) {
            vector<Point2f> ci;
            for (int k=0; k<contours[j].size(); k++)
                ci.push_back(Point2f(contours[j][k].x + r.x, contours[j][k].y + r.y));
            orig_contours.push_back(ci);
        }
    }
    return;
}

std::vector<std::pair<Point2f, Point2f> >  
getFlowLines(const Mat& velx,const Mat& vely,double distance_threshold,int blocksize){
    std::vector<std::pair<Point2f, Point2f> > lines;
    float *vx = (float*)velx.data;
    float *vy = (float*)vely.data;
    const float pi = 3.14159265;
    for(int x=0;x<velx.cols;x++)
        for(int y=0;y<velx.rows;y++){
            float x1=x,y1=y,x2=(x+vx[y*velx.cols+x]),y2=(y+vy[y*velx.cols+x]);
            float dist = (y2-y1)*(y2-y1) + (x2-x1)*(x2-x1);
            if(dist > distance_threshold){
                if(!(x%blocksize ==0 || y%blocksize ==0))
                    continue;
                
                double angle = atan2(y1-y2,x1-x2);
                float a1_x = (x2 + 9*cos(angle + pi/4)), a1_y = (y2 + 9*sin(angle + pi/4));
                float a2_x = (x2 + 9*cos(angle - pi/4)), a2_y = (y2 + 9*sin(angle - pi/4));
                lines.push_back(std::make_pair(cv::Point2f(x1,y1),cv::Point2f(x2,y2))),
                    lines.push_back(std::make_pair(cv::Point2f(x2,y2),cv::Point2f(a1_x,a1_y))),
                    lines.push_back(std::make_pair(cv::Point2f(x2,y2),cv::Point2f(a2_x,a2_y))); //push the arrow
            }
        }
  return lines;
}

void match_optical_flow_dense(Mat& gray) { 
    
    if (!prev_gray.empty()) { 
        
        std::vector<float> err;
        std::vector<Point2f> pts;        
        std::vector<uchar> status, status2;
        Mat flow;

        calcOpticalFlowFarneback(prev_gray, gray, flow, 0.5, 2, 9, 10, 7, 1.5, OPTFLOW_FARNEBACK_GAUSSIAN);

	std::vector<Mat> flow_channels;
	cv::split(flow,flow_channels);
        Mat velx = flow_channels[0], vely = flow_channels[1];
        std::vector<std::pair<Point2f, Point2f> > lines = getFlowLines(velx,vely,10,4);
        
        Mat3b color;
        cvtColor(gray, color, CV_GRAY2BGR);
        for (int j=0; j<lines.size(); j++) { 
            line(color, lines[j].first, lines[j].second, Scalar(255,0,0), 1);
        }
  
        imshow("FARNE-LK", color);
        
    }
    prev_gray = gray.clone();

    return;
}

void mser_features(Mat& img) { 
    Mat gray;
    cvtColor(img, gray, CV_BGR2GRAY);
    
    int delta = 5; 
    int max_area = 14400;
    int min_area = 60;
    int max_evolution = 200;
    double area_threshold = mser_area_threshold * 1.f / 100;
    double min_margin = mser_min_margin * 1.f / 1000;
    double max_variation = mser_max_variation * 1.f / 100;
    double min_diversity = mser_min_diversity * 1.f / 10;
    int edge_blur_size = 5;
    
    Mat mser_img = img.clone();
    MSER mser(delta, min_area, max_area, max_variation, min_diversity, 
              max_evolution, area_threshold, min_margin, edge_blur_size);
    
    vector<vector< Point> > msers;
    mser(gray, msers);

    if (!msers.size())
        return;

    std::vector<Scalar> colors(msers.size());
    opencv_utils::fillColors(colors);

    for (int j=0; j<msers.size(); j++) 
        for (int k=0; k<msers[j].size(); k++) 
            circle( mser_img, msers[j][k], 2, colors[j], -1, 8);


    imshow("mser", mser_img);
}

cv::Mat cvtToGray(const Mat& img) { 
    std::vector<Mat1b> channels;
    cv::split(img,channels);
    
    Mat gray;
    addWeighted( channels[2], 0.5, channels[1], 0.5, 0, gray );
    return gray;
}

struct PartitionPts2
{
    PartitionPts2() {}
    bool operator()(const Point2f& a, const Point2f& b) const {
        return (opencv_utils::l2_dist(a,b) < 20);
    }
};

static bool edge_sort_by_count(const std::pair<std::pair<int, int>, int>& lhs, 
                               const std::pair<std::pair<int, int>, int>& rhs) { 
    return lhs.second > rhs.second;
}

void create_tracks();
void infer_track_motion();
// void find_nn_correspondences(const std::vector<Point2f>& pts1, 
//                              const std::vector<Point2f>& pts2, 
//                              std::vector<Point2f>& corr_pts1, 
//                              std::vector<Point2f>& corr_pts2) { 

//     Mat pts1_mat = cv::Mat(pts1.size(), 2, CV_32FC1);
//     for (int j=0; j<pts1.size(); j++) { 
//         pts1_mat.at<float>(j,0) = pts1[j].x;
//         pts1_mat.at<float>(j,1) = pts1[j].y;
//     }

//     Mat pts2_mat = cv::Mat(pts2.size(), 2, CV_32FC1);
//     for (int j=0; j<pts2.size(); j++) { 
//         pts2_mat.at<float>(j,0) = pts2[j].x;
//         pts2_mat.at<float>(j,1) = pts2[j].y;
//     }

//     cv::flann::Index knn_index1(pts1_mat, cv::flann::LinearIndexParams()); // using 4 randomized kdtrees
//     cv::flann::Index knn_index2(pts2_mat, cv::flann::LinearIndexParams()); // using 4 randomized kdtrees

//     knn_index1.knnSearch(pts2_mat, inds, dists, 1, cv::flann::SearchParams());
//     knn_index2.knnSearch(pts1_mat, inds, dists, 1, cv::flann::SearchParams());
        



// }


void track_klt_features(Frame& frame) { 

    FrameData framedata;

    const Mat& img = frame.kinect.getRGB();
    Mat3b color = img.clone();
    Mat gray = cvtToGray(img);
    
    const int MAX_COUNT = 500;
    TermCriteria termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03);
    Size subPixWinSize(5,5), winSize(31,31);

    const int COLOR_COUNT = 30;
    std::vector<Scalar> colors(COLOR_COUNT);
    opencv_utils::fillColors(colors);

    if (need_to_init) { 
        // automatic initialization

#define FST 0
#if FST
        std::vector<KeyPoint> kpts;
        FAST(gray, kpts, 40, true);
        KeyPoint::convert(kpts, framedata.pts);
#else
        goodFeaturesToTrack(gray, framedata.pts, MAX_COUNT, 0.01, 10, Mat(), 3, 0, 0.04);
#endif
        // cornerSubPix(gray, framedata.pts, subPixWinSize, Size(-1,-1), termcrit);

        int& nclasses = framedata.nclasses;
        std::vector<Point2f>& pts = framedata.pts;
        std::vector<int>& labels = framedata.labels;

        std::vector<Cluster>& class_pts = framedata.class_pts;
        std::vector<int>& cluster_corr = framedata.cluster_corr;

        // Partition pts
        nclasses = partition( pts, labels, PartitionPts2() );
        cluster_corr = std::vector<int>(nclasses, -1);
        class_pts = std::vector<Cluster>(nclasses);

        for (int j=0; j<pts.size(); j++) {
            assert(labels[j] >= 0 && labels[j] < nclasses);
            class_pts[labels[j]].push_back(pts[j]);
        }
   
        add_remove_pt = false;
        need_to_init = false;
    } else if (frame_queue.size() && framedata_queue.front().pts.size()) { 
        Frame& prev_frame = frame_queue.front();
        const Mat& prev_img = prev_frame.kinect.getRGB();
        const Mat& prev_gray = prev_frame.kinect.getGray();
        const FrameData& prev_framedata = framedata_queue.front();

        const int& prev_nclasses = prev_framedata.nclasses;
        const std::vector<Point2f>& prev_pts = prev_framedata.pts;
        const std::vector<int>& prev_labels = prev_framedata.labels;

        const std::vector<Cluster>& prev_class_pts = prev_framedata.class_pts;
        const std::vector<int>& prev_cluster_corr = prev_framedata.cluster_corr;

        int& nclasses = framedata.nclasses;
        std::vector<Point2f>& pts = framedata.pts;
        std::vector<int>& labels = framedata.labels;

        std::vector<Cluster>& class_pts = framedata.class_pts;
        std::vector<int>& cluster_corr = framedata.cluster_corr;

#if FST
        std::vector<KeyPoint> kpts;
        FAST(gray, kpts, 40, true);
        KeyPoint::convert(kpts, pts);
#else
        goodFeaturesToTrack(gray, pts, MAX_COUNT, 0.01, 10, Mat(), 3, 0, 0.04);
#endif

        if (!prev_pts.size() || !pts.size())
            goto done;

        nclasses = partition( pts, labels, PartitionPts2() );
        cluster_corr = std::vector<int>(nclasses, -1);
        class_pts = std::vector<Cluster>(nclasses);

        // Convert to vector of vector of points (class->idx)
        for (int j=0; j<pts.size(); j++) {
            assert(labels[j] >= 0 && labels[j] < nclasses);
            class_pts[labels[j]].push_back(pts[j]);
        }

        Mat c1 = prev_img.clone();         
        Mat c2 = img.clone(); 

        // Vote for the closest class by randomly picking points
        Mat prev_pts_mat = cv::Mat(prev_pts.size(), 2, CV_32FC1);
        for (int j=0; j<prev_pts.size(); j++) { 
            prev_pts_mat.at<float>(j,0) = prev_pts[j].x;
            prev_pts_mat.at<float>(j,1) = prev_pts[j].y;
        }

        // std::cerr << "*******************" << std::endl;
        cv::flann::Index knn_index(prev_pts_mat, cv::flann::LinearIndexParams()); // using 4 randomized kdtrees
        std::map<int, std::vector<std::pair< std::pair<int, int>, int> > >  corr_count; 
        for (int j=0; j<class_pts.size(); j++) { 

            Cluster& class_j_pts = class_pts[j];

            // Cluster prev_class_closest_pts;
            Mat class_j_pts_mat = cv::Mat(class_j_pts.size(), 2, CV_32FC1);
            for (int k=0; k<class_j_pts.size(); k++) { 
                class_j_pts_mat.at<float>(k,0) = class_j_pts[k].x;
                class_j_pts_mat.at<float>(k,1) = class_j_pts[k].y;
            }

            cv::Mat_<int> inds;
            cv::Mat_<float> dists;
            knn_index.knnSearch(class_j_pts_mat, inds, dists, 1, cv::flann::SearchParams(64));             

            std::vector<int> class_votes(prev_nclasses, 0);
            for (int k=0; k<class_j_pts.size(); k++) { 
                int idx = inds.at<int>(k); 

                assert(idx >= 0 && idx < prev_labels.size());
                class_votes[prev_labels[idx]]++;
            }
            
            Point maxIdx(-1,-1);
            minMaxLoc(Mat(class_votes), 0, 0, 0, &maxIdx);
            assert(maxIdx.y >= 0 && maxIdx.y < prev_nclasses);

            const Cluster& closest_class_pts = prev_class_pts[maxIdx.y];
            corr_count[maxIdx.y].push_back(std::make_pair(std::make_pair(maxIdx.y,j), closest_class_pts.size() + class_j_pts.size()));

            if (class_j_pts.size() < 4 || closest_class_pts.size() < 4)
                continue;
        }

        // Pick only edges with max cluster overlap
        for (std::map<int, std::vector<std::pair< std::pair<int, int>, int> > >::iterator it=corr_count.begin(); 
             it != corr_count.end(); it++) { 
            int prev_idx = it->first;
            assert(prev_idx >=0 && prev_idx < prev_nclasses);
            std::vector<std::pair<std::pair<int, int>, int> >& edges = it->second;

            if (!edges.size())
                continue;

            if (edges.size() == 1) { 
                std::pair<int, int>& edge_inds = edges[0].first;
                int curr_idx = edge_inds.second;

                assert(curr_idx >=0 && curr_idx < nclasses);
                framedata.cluster_corr[curr_idx] = prev_idx;

            } else { 
                std::sort(edges.begin(), edges.end(), edge_sort_by_count);
                
                std::pair<int, int>& edge_inds = edges[0].first;
                int curr_idx = edge_inds.second;

                assert(curr_idx >=0 && curr_idx < nclasses);
                framedata.cluster_corr[curr_idx] = prev_idx;
                
                // // Connect all
                // for (int j=0; j<edges.size() && j < 2; j++) 
                //     framedata.cluster_corr[edges[j].first.second] = prev_idx;

            }
            
        }

        // Plot top correspondences
        std::vector<Point2f> corr_pts1, corr_pts2;
        for (int j=0; j<class_pts.size(); j++) { 
            Cluster& class_j_pts = class_pts[j];

            int prev_idx = framedata.cluster_corr[j];
            if (prev_idx < 0 || prev_idx >= prev_nclasses)
                continue;

            const Cluster& closest_class_pts = prev_class_pts[prev_idx];


            std::vector<Point2f> pts1, pts2;
            for (int k=0; k<closest_class_pts.size(); k++) { 
                circle( c1, closest_class_pts[k], 2, colors[j%COLOR_COUNT], -1, 8);
                pts1.push_back(closest_class_pts[k]);
                // std::cerr << closest_class_pts[k] << std::endl;
            }
            // std::cerr << "---" << std::endl;
            for (int k=0; k<class_j_pts.size(); k++) { 
                circle( c2, class_j_pts[k], 2, colors[j%COLOR_COUNT], -1, 8);
                pts2.push_back(class_j_pts[k]);
                // std::cerr << class_j_pts[k] << std::endl;
            }

            // find_nn_correspondences(closest_class_pts, class_j_pts, corr_pts1, corr_pts2);
        }

        // opencv_utils::imshow("GoodFeatures Prev", c1);        
        opencv_utils::imshow("GoodFeatures Live", c2);
        // Mat corr = opencv_utils::drawCorrespondences(c1, corr_pts1, c2, corr_pts2);
        // opencv_utils::imshow("Corr", corr);


    }

 done: 
    // First push
    frame_queue.push_front(frame);
    framedata_queue.push_front(framedata);

    // Only pop if more than 2
    if (frame_queue.size() > 2) {
        frame_queue.pop_back();
        framedata_queue.pop_back();
    }
    
    create_tracks();
    infer_track_motion();

    assert(frame_queue.size() == framedata_queue.size());

    return;
}

std::vector<Point3f> convert_to_3d(Frame& frame, std::vector<Point2f>& pts) { 
    std::vector<Point3f> pts3d(pts.size());
    for (int j=0; j<pts.size(); j++)
        pts3d[j] = frame.kinect.getXYZ(pts[j]);
    return pts3d;
}

void create_tracks() { 

    const int MAX_TRACK_SIZE = 2000;
    if (framedata_queue.size() < 2)
        return;

    Frame& frame = frame_queue[0]; 
    Frame& prev_frame = frame_queue[1];

    FrameData& framedata = framedata_queue[0];
    FrameData& prev_framedata = framedata_queue[1];

    if (!cluster_tracks.size()) { 
        assert(prev_framedata.nclasses == prev_framedata.class_pts.size());
        cluster_tracks = std::vector<ClusterTrack>(prev_framedata.nclasses);
        cluster_tracks_3d = std::vector<Cluster3DTrack>(prev_framedata.nclasses);

        // Add the prev frame first
        prev_framedata.track_id = std::vector<int>(prev_framedata.nclasses, -1);
        for (int j=0; j<prev_framedata.class_pts.size(); j++) { 
            cluster_tracks[j].push_back(prev_framedata.class_pts[j]);
            cluster_tracks_3d[j].push_back(convert_to_3d(prev_frame, prev_framedata.class_pts[j]));
            prev_framedata.track_id[j] = j;         
        }
        prev_framedata.processed = true;

        // Add the new frame
        framedata.track_id = std::vector<int>(framedata.nclasses, -1);
        for (int j=0; j<framedata.class_pts.size(); j++) { 
            int prev_idx = framedata.cluster_corr[j];
            if (prev_idx < 0 || prev_idx >= prev_framedata.nclasses)
                continue;

            int track_id = prev_framedata.track_id[prev_idx];
            if (track_id < 0 || track_id >= cluster_tracks.size())
                continue;

            cluster_tracks[track_id].push_back(framedata.class_pts[j]);        
            cluster_tracks_3d[track_id].push_back(convert_to_3d(frame,framedata.class_pts[j]));        
            framedata.track_id[j] = track_id;
        }

        framedata.processed = true;
    } else { 
        // Add the new frame
        framedata.track_id = std::vector<int>(framedata.nclasses, -1);
        for (int j=0; j<framedata.class_pts.size(); j++) { 
            int prev_idx = framedata.cluster_corr[j];
            if (prev_idx < 0 || prev_idx >= prev_framedata.nclasses)
                continue;

            int track_id = prev_framedata.track_id[prev_idx];
            if (track_id < 0 || track_id >= cluster_tracks.size())
                continue;

            cluster_tracks[track_id].push_back(framedata.class_pts[j]);        
            cluster_tracks_3d[track_id].push_back(convert_to_3d(frame,framedata.class_pts[j]));
            if (cluster_tracks[track_id].size() > MAX_TRACK_SIZE) { 
                cluster_tracks[track_id].pop_front();
                cluster_tracks_3d[track_id].pop_front();
            }
            framedata.track_id[j] = track_id;
        }

        framedata.processed = true;
    }

    
    // std::cerr << "Tracks: " << cluster_tracks.size() << std::endl;
    // for (int j=0; j<cluster_tracks.size(); j++)
    //     std::cerr << "t: " << j << " " << cluster_tracks[j].size() << std::endl;

    // Plot the latest cluster from each track_id
    Mat img = frame_queue.front().kinect.getRGB().clone();
    // std::cerr << "Tracks: " << cluster_tracks.size() << std::endl;

    std::vector<Scalar> colors(cluster_tracks.size());
    opencv_utils::fillColors(colors);

    for (int j=0; j<cluster_tracks.size(); j++) { 
        if (!cluster_tracks[j].size())
            continue;
        for (int k=0; k<cluster_tracks[j].size(); k++) { 
            Cluster& cluster = cluster_tracks[j][k];
            for (int l=0; l<cluster.size(); l++)
                circle( img, cluster[l], 2, colors[j%cluster_tracks.size()], -1, 8);
        }
    }
    opencv_utils::imshow("Tracks", img);        

    // // LCMGL ---------------------------------------

    // // gl draw with queue
    // bot_lcmgl_t* lcmgl = lcmgl_features;

    // // Rotation to viewer
    // lcmglPushMatrix();
    // double kinect_to_local_m[16];
    // bot_frames_get_trans_mat_4x4(state->frames,"KINECT",
    //                              bot_frames_get_root_name(state->frames),
    //                              kinect_to_local_m);

    // // opengl expects column-major matrices
    // double kinect_to_local_m_opengl[16];
    // bot_matrix_transpose_4x4d(kinect_to_local_m, kinect_to_local_m_opengl);
    // lcmglMultMatrixd(kinect_to_local_m_opengl);

    // lcmglPointSize(2);
    // // lcmglEnable(GL_DEPTH_TEST);
    // lcmglEnable(GL_BLEND);
    // lcmglBegin(GL_POINTS);
    // float cscale = 1.f/255;
    // for (int j=0; j<cluster_tracks_3d.size(); j++) { 
    //     if (!cluster_tracks_3d[j].size())
    //         continue;
    //     const Scalar& color = colors[j%cluster_tracks_3d.size()];
    //     for (int k=0; k<cluster_tracks_3d[j].size(); k++) { 
    //         lcmglColor4f(color[2]*cscale,color[1]*cscale,color[0]*cscale,1.f - (cluster_tracks_3d[j].size() - k)*1.f/cluster_tracks_3d.size());
    //         Cluster3D& cluster = cluster_tracks_3d[j][k];
    //         for (int l=0; l<cluster.size(); l++) 
    //             lcmglVertex3f(cluster[l].x,cluster[l].y,cluster[l].z);
    //     }
    // }
    // lcmglEnd();
    // // lcmglDisable(GL_DEPTH_TEST);
    // lcmglDisable(GL_BLEND);

    // lcmglPopMatrix();

    // bot_lcmgl_switch_buffer(lcmgl_features);

}

static bool cluster_sort(const Point3f& lhs, const Point3f& rhs) { 
    return lhs.z < rhs.z;
}

void infer_track_motion() {
    if (!cluster_tracks_3d.size())
        return;


    assert(cluster_tracks_3d.size() == cluster_tracks.size());
    std::vector<std::vector<Point3f> > cluster_mu_tracks_3d(cluster_tracks_3d.size());
    std::vector<std::vector<Point2f> > cluster_mu_tracks(cluster_tracks_3d.size());

    for (int j=0; j<cluster_tracks_3d.size(); j++) { 
        if (!cluster_tracks_3d[j].size())
            continue;

        std::vector<Point3f>& cluster_mu_track_3d = cluster_mu_tracks_3d[j];
        std::vector<Point2f>& cluster_mu_track = cluster_mu_tracks[j];
        cluster_mu_track_3d = std::vector<Point3f>(cluster_tracks_3d[j].size(), Point3f(0,0,0) );
        cluster_mu_track = std::vector<Point2f>(cluster_tracks_3d[j].size(), Point2f(0,0) );
       
        for (int k=0; k<cluster_tracks_3d[j].size(); k++) { 

            Point3f& mu_3d = cluster_mu_track_3d[k];
            Point2f& mu = cluster_mu_track[k];

            Cluster3D& cluster3d = cluster_tracks_3d[j][k];
            Cluster& cluster = cluster_tracks[j][k];

            // if (cluster3d.size() < 10)
            //     continue;

            for (int l=0; l<cluster3d.size(); l++)
                mu_3d.x += cluster3d[l].x, mu_3d.y += cluster3d[l].y, mu_3d.z += cluster3d[l].z, 
                    mu.x += cluster[l].x, mu.y += cluster[l].y;

            std::nth_element(cluster3d.begin(), cluster3d.begin() + cluster3d.size() / 2, cluster3d.end(), cluster_sort);
            float s = 1.f / cluster3d.size();
            mu_3d.x *= s, mu_3d.y *= s; // , mu_3d.z *= s;
            mu_3d.z = cluster3d[cluster3d.size()/2].z;
            mu.x *= s, mu.y *= s;
        }

    }
    
    Mat3b img = frame_queue.front().kinect.getRGB().clone();
    Mat3b img0 = Mat3b::zeros(frame_queue.front().kinect.getRGB().size());
    int spacing = img.rows / cluster_mu_tracks_3d.size();

    std::vector<Scalar> colors(cluster_tracks_3d.size());
    opencv_utils::fillColors(colors);

    std::vector<bool> draw_cluster(cluster_tracks_3d.size(), false);
    for (int j=0; j<cluster_mu_tracks_3d.size(); j++) { 
        if (!cluster_mu_tracks_3d[j].size())
            continue;
        
        if (cluster_mu_tracks_3d[j].size() < 50)
            continue;
        
        Mat track_mat = cv::Mat(cluster_mu_tracks_3d[j].size(), 3, CV_32FC1);
        for (int k=0; k<cluster_mu_tracks_3d[j].size(); k++) { 
            track_mat.at<float>(k,0) = cluster_mu_tracks_3d[j][k].x;
            track_mat.at<float>(k,1) = cluster_mu_tracks_3d[j][k].y;
            track_mat.at<float>(k,2) = cluster_mu_tracks_3d[j][k].y;
        }

        PCA pca(track_mat, Mat(), CV_PCA_DATA_AS_ROW, 3);
        std::stringstream ss;
        ss << "val: " << pca.eigenvalues; //  << "\n vec: " << pca.eigenvectors;
        
        if (pca.eigenvalues.at<float>(0) > 1e-3 || pca.eigenvalues.at<float>(1) > 1e-3)
            draw_cluster[j] = true;
        else
            draw_cluster[j] = false;

        // std::cerr << j << ": EVAL: " << pca.eigenvalues << " EVEC: " << pca.eigenvectors << std::endl;
        cv::putText(img, cv::format("%i", j), cluster_mu_tracks[j].end()[-1], 
                    0, 0.45, colors[j%cluster_tracks.size()],1);       
        circle( img, cluster_mu_tracks[j].end()[-1], 3, colors[j%cluster_tracks.size()], -1, 8);

        if (draw_cluster[j])
            cv::putText(img0, cv::format("%i EV: %s", j, ss.str().c_str()), Point(10, (j+1) * spacing), 
                        0, 0.4, colors[j%cluster_tracks.size()],1);

    }

    opencv_utils::imshow("PCA", img);
    opencv_utils::imshow("PCA0", img0);


    // LCMGL ---------------------------------------

    // gl draw with queue
    bot_lcmgl_t* lcmgl = lcmgl_features;

    // Rotation to viewer
    lcmglPushMatrix();
    double kinect_to_local_m[16];
    bot_frames_get_trans_mat_4x4(state->frames,"KINECT",
                                 bot_frames_get_root_name(state->frames),
                                 kinect_to_local_m);

    // opengl expects column-major matrices
    double kinect_to_local_m_opengl[16];
    bot_matrix_transpose_4x4d(kinect_to_local_m, kinect_to_local_m_opengl);
    lcmglMultMatrixd(kinect_to_local_m_opengl);

    lcmglPointSize(5);
    // lcmglEnable(GL_DEPTH_TEST);
    lcmglEnable(GL_BLEND);
    lcmglBegin(GL_POINTS);
    float cscale = 1.f/255;
    for (int j=0; j<cluster_mu_tracks_3d.size(); j++) { 
        if (!cluster_mu_tracks_3d[j].size())
            continue;
        const Scalar& color = colors[j%cluster_mu_tracks_3d.size()];
        for (int k=0; k<cluster_mu_tracks_3d[j].size(); k++) { 
            if (draw_cluster[j])
                lcmglColor4f(color[2]*cscale,color[1]*cscale,color[0]*cscale,1.f/* - (cluster_tracks_3d[j].size() - k)*1.f/cluster_tracks_3d.size()*/);
            else
                lcmglColor4f(.4,.4,.4,1.f);
            // Cluster3D& cluster = cluster_tracks_3d[j][k];
            // for (int l=0; l<cluster.size(); l++) 
            // lcmglVertex3f(cluster[l].x,cluster[l].y,cluster[l].z);
            lcmglVertex3f(cluster_mu_tracks_3d[j][k].x, cluster_mu_tracks_3d[j][k].y, cluster_mu_tracks_3d[j][k].z);
        }
    }
    lcmglEnd();
    // lcmglDisable(GL_DEPTH_TEST);
    lcmglDisable(GL_BLEND);

    lcmglPopMatrix();

    bot_lcmgl_switch_buffer(lcmgl_features);


}

void lk_features(Frame& frame) { 

    FrameData framedata;

    Mat img = frame.kinect.getRGB().clone();
    Mat3b color = img.clone();
    Mat gray = cvtToGray(img);
    
    const int MAX_COUNT = 1000;
    TermCriteria termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03);
    Size subPixWinSize(5,5), winSize(31,31);

    const int COLOR_COUNT = 30;
    std::vector<Scalar> colors(COLOR_COUNT);
    opencv_utils::fillColors(colors);

    if (need_to_init) { 
        // automatic initialization

#define FST 0
#if FST
        std::vector<KeyPoint> kpts;
        FAST(gray, kpts, 20, false);
        KeyPoint::convert(kpts, framedata.pts);
#else
        goodFeaturesToTrack(gray, framedata.pts, MAX_COUNT, 0.01, 5, Mat(), 3, 0, 0.04);
#endif
        // cornerSubPix(gray, framedata.pts, subPixWinSize, Size(-1,-1), termcrit);
        add_remove_pt = false;
        need_to_init = false;
    } else if (frame_queue.size()) { 
        Frame& prev_frame = frame_queue.front();
        Mat prev_img = prev_frame.kinect.getRGB().clone();
        Mat prev_gray = prev_frame.kinect.getGray().clone();
        const FrameData& prev_framedata = framedata_queue.front();

        // const std::vector<Point2f>& prev_pts = prev_framedata.pts;
        std::vector<Point2f> prev_pts; //  = framedata.pts;
        std::vector<Point2f> pts; //  = framedata.pts;

        std::vector<float> err;
        std::vector<uchar> status;

#if FST
        std::vector<KeyPoint> kpts;
        FAST(gray, kpts, 20, false);
        KeyPoint::convert(kpts, prev_pts);
#else
        goodFeaturesToTrack(prev_gray, prev_pts, MAX_COUNT, 0.01, 5, Mat(), 3, 0, 0.04);
#endif

        calcOpticalFlowPyrLK(prev_gray, gray, prev_pts, pts, status, err, winSize, 3, termcrit, 0, 0.001);

        if (!prev_pts.size() || !pts.size())
            goto done;

        Mat c1 = prev_img.clone();         
        Mat c2 = img.clone(); 

        // Vote for the closest class by randomly picking points
        Mat prev_pts_mat = cv::Mat(prev_pts.size(), 2, CV_32FC1);
        for (int j=0; j<prev_pts.size(); j++) { 
            prev_pts_mat.at<float>(j,0) = prev_pts[j].x;
            prev_pts_mat.at<float>(j,1) = prev_pts[j].y;
        }

        // std::cerr << "*******************" << std::endl;
        cv::flann::Index knn_index(prev_pts_mat, cv::flann::KDTreeIndexParams(4)); // using 4 randomized kdtrees

        cv::Mat_<int> inds;
        cv::Mat_<float> dists;
        knn_index.knnSearch(prev_pts_mat, inds, dists, 6, cv::flann::SearchParams(64)); // no: of leafs checked

        for (int j=0; j<prev_pts.size(); j++) { 
            if (!status[j])
                continue;

            float NORM = 0;
            std::vector<float> angles;
            for (int k=0; k<inds.cols; k++) { // first one is the pt itself
                if (dists.at<float>(j,k) > 20) 
                    continue;
                int idx = inds.at<int>(j,k); 
                        
                if (idx >= 0 && idx < prev_pts.size()) { 
                    Point2f p = pts[idx] - prev_pts[idx];
                    float norm = cv::norm(p);
                    NORM += norm;
                    p.x /= norm, p.y /= norm;
                    angles.push_back(atan2(p.y, p.x));
                }
                std::sort(angles.begin(), angles.end());
            }
            if (angles.size()) { 
                // NORM /= angles.size() * 10;
                float med_angle = angles[angles.size() / 2];
                float norm = cv::norm(pts[j] - prev_pts[j]);
                // std::cerr << NORM << " " << cos(med_angle) << " " << sin(med_angle) << " " << med_angle << std::endl;
                Point2f p2(prev_pts[j].x + norm * cos(med_angle), prev_pts[j].y + norm * sin(med_angle));
                line(c1, prev_pts[j], p2, Scalar(0, 0, 255), 1);
            }
        }

        opencv_utils::imshow("LK GoodFeatures", c1);        
    }

 done: 
    // First push
    frame_queue.push_front(frame);
    framedata_queue.push_front(framedata);

    // Only pop if more than 2
    if (frame_queue.size() > 2) {
        frame_queue.pop_back();
        framedata_queue.pop_back();
    }
    
    assert(frame_queue.size() == framedata_queue.size());

    return;
}

void find_corr_bb_in_bumblebee(Frame& frame, const Rect& _r) { 
    Rect r = mouse_rect;

    // double mu_kinect[3] = {0,0,0};
    // int count = 0;
    // for (int j=0; j<frame.keypts.size(); j++) { 
    //     if (frame.keypts[j].pt.x >= r.x - r.width/2 && frame.keypts[j].pt.y >= r.y - r.height/2 && 
    //         frame.keypts[j].pt.x <= r.x + r.width/2 && frame.keypts[j].pt.y <= r.y + r.height/2) { 
    //         for (int d=0; d<3; d++) 
    //             mu_kinect[d] += frame.keypts3d[j][d];
    //         count++;
    //     }            
    // }

    // if (count) { 
    //     for (int d=0; d<3; d++) 
    //         mu_kinect[d] /= count;
    // }

    // double mu_bb[3];
    // bot_frames_transform_vec (state->frames, "KINECT", "bumblebee_left", mu_kinect, mu_bb);
    // std::cerr << std::endl;
    // std::cerr << "bb: " << mu_bb[0] << " " << mu_bb[1] << " " << mu_bb[2] << " " << std::endl;
    // std::cerr << "kinect: " << mu_kinect[0] << " " << mu_kinect[1] << " " << mu_kinect[2] << " " << std::endl;

    // cv::Mat_<float> rvec = Mat_<float>::zeros(3, 1);
    // cv::Mat_<float> tvec = Mat_<float>::zeros(3, 1);
    // cv::Mat_<float> R = cv::Mat_<float>::eye(3,3);
    // Rodrigues(R, rvec);

    // std::vector<cv::Point3f> obj_pts;
    // std::vector<cv::Point2f> img_pts;
    // obj_pts.push_back(cv::Point3f(mu_bb[0],mu_bb[1],mu_bb[2]));
    // projectPoints(obj_pts, rvec, tvec, bb_K, bb_D, img_pts);

    // if (img_pts.size()) { 
    //     std::cerr << "img_pts: " << img_pts[0].x << " " << img_pts[0].y << std::endl;
    //     bb_rect = Rect(img_pts[0].x, img_pts[0].y, 30, 30);
    // }

    double body_to_bb[16];
    bot_frames_get_trans_mat_4x4(state->frames,"body",
                                 "bumblebee_left",
                                 body_to_bb);

    Mat_<double> bb_T = Mat_<double>(4,4,body_to_bb);

    extract_camera_extrinsics(bb_T, bb_rvec, bb_tvec);
    std::cerr << "T: " << bb_T << std::endl;

    Mat_<double> depth_K = pinhole_to_mat(depth_intrinsics);
    Mat_<double> depth_D = Mat_<double>::zeros(5,1);


    cv::Mat3b rgb = frame.kinect.getRGB();
    cv::Mat3b img = cv::Mat3b::zeros(rgb.size());
    cv::Mat3b dimg = cv::Mat3b::zeros(768,1024);

    bool once = false;
    for (int i=0; i<rgb.rows; i++) { 
        for (int j=0; j<rgb.cols; j++) { 
            cv::Vec3f v = frame.kinect.getXYZ(i,j);
            double xyz[3] = {v[0], v[1], v[2]};
            double xyz_bb[3];
            bot_frames_transform_vec (state->frames, "KINECT", "body", xyz, xyz_bb);

            if (!once) { 
                if (xyz[2] > 1) { 
                    std::cerr << "k: " << xyz[0] << " " << xyz[1] << " " << xyz[2] << std::endl;
                    std::cerr << "b: " << xyz_bb[0] << " " << xyz_bb[1] << " " << xyz_bb[2] << std::endl;
                    once = true;

                    std::vector<Point2f> img_pts;
                    std::vector<Point3f> obj_pts;
                    obj_pts.push_back(Point3f(xyz_bb[0],xyz_bb[1],xyz_bb[2]));
                    projectPoints(obj_pts, bb_rvec, bb_tvec, bb_K, bb_D, img_pts);
                    std::cerr << img_pts[0].x << " " << img_pts[0].y << std::endl;
                }
            }

            std::vector<Point2f> img_pts;
            std::vector<Point3f> obj_pts;
            obj_pts.push_back(Point3f(xyz_bb[0],xyz_bb[1],xyz_bb[2]));
            // projectPoints(obj_pts, bb_rvec, bb_tvec, depth_K, depth_D, img_pts);
            projectPoints(obj_pts, bb_rvec, bb_tvec, bb_K, bb_D, img_pts);

            if (!bb_img.empty())
                dimg(i,j) += bb_img(i,j) * .5;

            // dimg(i,j) += rgb(i,j) * .5;
            if (img_pts.size() && img_pts[0].x >=0 && img_pts[0].y >=0 && 
                img_pts[0].x < dimg.cols && img_pts[0].y < dimg.rows) { 
                dimg(img_pts[0].y,img_pts[0].x) += rgb(i,j) * .5;
                
                // dimg(img_pts[0].y,img_pts[0].x) = rgb(i,j) * .5;
            }
        }
    }

    imshow("dimg", dimg);
    return;
}

std::deque<Vec3f> track_pts;
void plot_track(Frame& frame, Rect& r) { 

    cv::Vec3f mu_p;
    int count = 0;
    for (int y=r.y-3; y<r.y+3; y++) 
        for (int x=r.x-3; x<r.x+3; x++) { 
            cv::Vec3f p = frame.kinect.getXYZ(y,x);
            if (p[2] > 0.1) { 
                mu_p += p;
                count++;
            }
            //std::cerr << mu_p[0] << " " << mu_p[1] << " " << mu_p[2] << std::endl;
        }
    if (count)
        mu_p /= count;
    else 
        return;
    track_pts.push_front(mu_p);

    if (track_pts.size() > 100)
        track_pts.pop_back();

    // LCMGL ---------------------------------------

    // gl draw with queue
    bot_lcmgl_t* lcmgl = lcmgl_features;

    // Rotation to viewer
    lcmglPushMatrix();
    double kinect_to_local_m[16];
    bot_frames_get_trans_mat_4x4(state->frames,"KINECT",
                                 bot_frames_get_root_name(state->frames),
                                 kinect_to_local_m);

    // opengl expects column-major matrices
    double kinect_to_local_m_opengl[16];
    bot_matrix_transpose_4x4d(kinect_to_local_m, kinect_to_local_m_opengl);
    lcmglMultMatrixd(kinect_to_local_m_opengl);

    lcmglPointSize(10);
    // lcmglEnable(GL_DEPTH_TEST);
    lcmglEnable(GL_BLEND);
    lcmglBegin(GL_POINTS);
    for (int j=0; j<track_pts.size(); j++) { 
        if (j == 0) {
            lcmglColor4f(0,0,1.f,1.f);
        } else { 
            lcmglColor4f(.6,0,0,1.f - j*1.f/track_pts.size());
        }

        lcmglVertex3f(track_pts[j][0],track_pts[j][1],track_pts[j][2]);
    }
    lcmglEnd();
    // lcmglDisable(GL_DEPTH_TEST);
    lcmglDisable(GL_BLEND);


    // lcmglBegin(GL_LINES);
    // for (int j=0; j<frame_queue.size(); j++) { 
    //     const Frame& frame = frame_queue[j];
    //     lcmglColor4f(0,.3,0,1.f - j*1.f/frame_queue.size());

    //     if (!frame.keypts3d_next.size())
    //         continue;

    //     const std::vector<Vec3f>& curr_featptsj = frame.keypts3d_next;
    //     const std::vector<Vec3f>& prev_featptsj = frame.keypts3d;

    //     for (int k=0; k<curr_featptsj.size(); k++) { 
    //         if (frame.status[k]) { 
    //             const Vec3f& xyz1 = curr_featptsj[k];
    //             const Vec3f& xyz2 = prev_featptsj[k];
    //             lcmglVertex3f(xyz1[0],-xyz1[1],xyz1[2]);
    //             lcmglVertex3f(xyz2[0],-xyz2[1],xyz2[2]);
    //             // std::cerr << " ==========" << std::endl;
    //             // // Vec3f xyz1 = prev_frame.keypts3d[j];
    //             // // Vec3f xyz2 = prev_frame.keypts3d_next[j];
    //             // std::cerr << xyz1[0] << " " << xyz1[1] << " " << xyz1[2] << std::endl;
    //             // std::cerr << xyz2[0] << " " << xyz2[1] << " " << xyz2[2] << std::endl;

    //         }
    //     }


    // }
    // lcmglEnd();

    lcmglPopMatrix();

    bot_lcmgl_switch_buffer(lcmgl_features);
}

#if PCL
void publishPCL(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& pcl, std::string channel = std::string("KINECT_POINT_CLOUD")) { 
    if (!pcl->points.size())
        return;

    kinect_point_list_t point_cloud;
    point_cloud.points = (kinect_point_t*) malloc(sizeof(kinect_point_t) * pcl->points.size());
    memset(point_cloud.points, 0, sizeof(kinect_point_t) * pcl->points.size());
    point_cloud.num_points = pcl->points.size();

    kinect_point_t* points = point_cloud.points;
    int idx=0, f_idx=0;
    for (int j=0; j<pcl->points.size(); j++) {
            points[j].x = pcl->points[j].x;
            points[j].y = pcl->points[j].y;
            points[j].z = pcl->points[j].z;
            points[j].r = pcl->points[j].r;
            points[j].g = pcl->points[j].g;
            points[j].b = pcl->points[j].b;
    }

    kinect_point_list_t_publish(state->lcm, channel.c_str(), &point_cloud);
    free(point_cloud.points);
}

void 
setViewerPose (pcl::visualization::PCLVisualizer& viewer, const Eigen::Affine3f& viewer_pose)
{
    Eigen::Vector3f pos_vector = viewer_pose * Eigen::Vector3f (0, 0, 0);
    Eigen::Vector3f look_at_vector = viewer_pose.rotation () * Eigen::Vector3f (0, 0, 1) + pos_vector;
    Eigen::Vector3f up_vector = viewer_pose.rotation () * Eigen::Vector3f (0, -1, 0);
    viewer.camera_.pos[0] = pos_vector[0];
    viewer.camera_.pos[1] = pos_vector[1];
    viewer.camera_.pos[2] = pos_vector[2];
    viewer.camera_.focal[0] = look_at_vector[0];
    viewer.camera_.focal[1] = look_at_vector[1];
    viewer.camera_.focal[2] = look_at_vector[2];
    viewer.camera_.view[0] = up_vector[0];
    viewer.camera_.view[1] = up_vector[1];
    viewer.camera_.view[2] = up_vector[2];
    viewer.updateCamera ();
}
#endif


// Adapted from cv_show_angles
cv::Mat displayQuantized(const cv::Mat& quantized)
{
  cv::Mat color(quantized.size(), CV_8UC3);
  for (int r = 0; r < quantized.rows; ++r)
  {
    const uchar* quant_r = quantized.ptr(r);
    cv::Vec3b* color_r = color.ptr<cv::Vec3b>(r);
    
    for (int c = 0; c < quantized.cols; ++c)
    {
      cv::Vec3b& bgr = color_r[c];
      switch (quant_r[c])
      {
        case 0:   bgr[0]=  0; bgr[1]=  0; bgr[2]=  0;    break;
        case 1:   bgr[0]= 55; bgr[1]= 55; bgr[2]= 55;    break;
        case 2:   bgr[0]= 80; bgr[1]= 80; bgr[2]= 80;    break;
        case 4:   bgr[0]=105; bgr[1]=105; bgr[2]=105;    break;
        case 8:   bgr[0]=130; bgr[1]=130; bgr[2]=130;    break;
        case 16:  bgr[0]=155; bgr[1]=155; bgr[2]=155;    break;
        case 32:  bgr[0]=180; bgr[1]=180; bgr[2]=180;    break;
        case 64:  bgr[0]=205; bgr[1]=205; bgr[2]=205;    break;
        case 128: bgr[0]=230; bgr[1]=230; bgr[2]=230;    break;
        case 255: bgr[0]=  0; bgr[1]=  0; bgr[2]=255;    break;
        default:  bgr[0]=  0; bgr[1]=255; bgr[2]=  0;    break;
      }
    }
  }
  
  return color;
}

static void draw_subdiv( Mat& img, Subdiv2D& subdiv, Scalar delaunay_color ) {
    vector<Vec6f> triangleList;
    subdiv.getTriangleList(triangleList);
    vector<Point> pt(3);
    
    for( size_t i = 0; i < triangleList.size(); i++ ) {
        Vec6f t = triangleList[i];
        pt[0] = Point(cvRound(t[0]), cvRound(t[1]));
        pt[1] = Point(cvRound(t[2]), cvRound(t[3]));
        pt[2] = Point(cvRound(t[4]), cvRound(t[5]));
        line(img, pt[0], pt[1], delaunay_color, 1, CV_AA, 0);
        line(img, pt[1], pt[2], delaunay_color, 1, CV_AA, 0);
        line(img, pt[2], pt[0], delaunay_color, 1, CV_AA, 0);
    }
}

void drawResponse(const std::vector<cv::linemod::Template>& templates, 
                  int num_modalities, cv::Mat& dst, cv::Point offset, int T, cv::linemod::Match& match)
{
  static const cv::Scalar COLORS[5] = { CV_RGB(0, 0, 255),
                                        CV_RGB(0, 255, 0),
                                        CV_RGB(255, 255, 0),
                                        CV_RGB(255, 140, 0),
                                        CV_RGB(255, 0, 0) };

  std::vector<Point> del_pts;
  for (int m = 0; m < num_modalities; ++m)
  {
    // NOTE: Original demo recalculated max response for each feature in the TxT
    // box around it and chose the display color based on that response. Here
    // the display color just depends on the modality.
    cv::Scalar color = COLORS[m];
    
    cv::Point p;
    for (int i = 0; i < (int)templates[m].features.size(); ++i)
    {
      cv::linemod::Feature f = templates[m].features[i];
      cv::Point pt(f.x + offset.x, f.y + offset.y);
      cv::circle(dst, pt, T / 2, color);
      p = pt;
      del_pts.push_back(pt);
    }
    cv::putText(dst, cv::format("Score: %4.3f", match.similarity),
                p, 0, 0.35, cv::Scalar(255,255,255),1);
  }

  // Subdiv2D subdiv(Rect(0,0,dst.cols,dst.rows));
  // for (int j=0; j<del_pts.size(); j++) 
  //     subdiv.insert(del_pts[j]);
  // draw_subdiv(dst, subdiv, Scalar(255,255,255,.2));

}


void lm_detect(Frame& frame, SLIC_info& slic_info, Rect& r) { 
    std::vector<Mat> sources;

    r.width = 320;
    r.height = 240;
    r &= Rect(0, 0, WIDTH, HEIGHT);

    cv::Mat color = frame.kinect.getRGB()(r).clone();
    cv::Mat depth = frame.kinect.getDepth()(r).clone();
    sources.push_back(color);
    sources.push_back(depth);

    // Perform matching
    std::vector<cv::linemod::Match> matches;
    std::vector<std::string> class_ids;
    std::vector<cv::Mat> quantized_images;
    lm_detector->match(sources, (float)matching_threshold, matches, class_ids, quantized_images);

    std::cerr << "Matches: " << matches.size() << std::endl;
    
    int classes_visited = 0;
    std::set<std::string> visited;
    
    cv::Mat display = frame.kinect.getRGB()(r).clone();
    int num_modalities = (int)lm_detector->getModalities().size();
    
    for (int i = 0; (i < (int)matches.size()) && (classes_visited < num_classes); ++i) {
        cv::linemod::Match m = matches[i];

        if (visited.insert(m.class_id).second) {
            ++classes_visited;

            if (show_match_result) {
                printf("Similarity: %5.1f%%; x: %3d; y: %3d; class: %s; template: %3d\n",
                       m.similarity, m.x, m.y, m.class_id.c_str(), m.template_id);
            }
        
            // Draw matching template
            const std::vector<cv::linemod::Template>& templates = lm_detector->getTemplates(m.class_id, m.template_id);
            drawResponse(templates, num_modalities, display, cv::Point(m.x, m.y), lm_detector->getT(0), m);

            if (learn_online == true) {
                /// @todo Online learning possibly broken by new gradient feature extraction,
                /// which assumes an accurate object outline.
          
                // Compute masks based on convex hull of matched template
                cv::Mat color_mask, depth_mask;
                std::vector<CvPoint> chain = maskFromTemplate(templates, num_modalities,
                                                              cv::Point(m.x, m.y), color.size(),
                                                              color_mask, display);

                // subtractPlane(depth, depth_mask, chain, focal_length);
                Mat depth_color_mask;
                bitwise_and(color_mask, currMask, depth_color_mask);
                opencv_utils::imshow("learn mask", color_mask);
                opencv_utils::imshow("learn mask + depth", depth_color_mask);

                // If pretty sure (but not TOO sure), add new template
                if (learning_lower_bound < m.similarity && m.similarity < learning_upper_bound) {
                    //extract_timer.start();
                    int template_id = lm_detector->addTemplate(sources, m.class_id, depth_color_mask);
                    //extract_timer.stop();
                    if (template_id != -1) {
                        printf("*** Added template (id %d) for existing object class %s***\n",
                               template_id, m.class_id.c_str());
                    }
                    num_classes++;
                }
            }
        }
    }

    opencv_utils::imshow("Display", display);
    
    
}



void tld_track(Frame& frame) { 
    Mat img = frame.kinect.getRGB().clone();
    // lk_features(img);

    if (trackObject) { 
        if (trackObject < 0) { 
            opencv_utils::imshow("first frame", img);
            trackObject = 1;

            // tld tracker selection
            tldtracker->selectObject(frame.kinect.getGray().clone(), &selection);
        } else  {
            IplImage img_ipl(img);
            tldtracker->processImage(&img_ipl);
            int confident = (tldtracker->currConf >= .5) ? 1 : 0;

            CvScalar yellow = CV_RGB(255,255,0);
            CvScalar blue = CV_RGB(0,0,255);
            CvScalar black = CV_RGB(0,0,0);
            CvScalar white = CV_RGB(255,255,255);
        
            if(tldtracker->currBB != NULL) {
                CvScalar rectangleColor = (confident) ? blue : yellow;
                cvRectangle(&img_ipl, tldtracker->currBB->tl(), tldtracker->currBB->br(), rectangleColor, 2, 2, 0);
                
                // plot the tracker bounding box that optical flow predicts
                Rect* trackerBB = tldtracker->medianFlowTracker->trackerBB;
                if (trackerBB != NULL)
                    cvRectangle(&img_ipl, trackerBB->tl(), trackerBB->br(), white, 2, 2, 0);
                
                // good features within the bounding box
                if (trackerBB != NULL) { 
                    std::vector<Point2f> corners;
                    Rect r(*trackerBB);

                    Mat roi_orig(frame.kinect.getRGB().clone(), r);

                    int rx = r.x + r.width / 2;
                    int ry = r.y + r.height / 2;

                    r.width *= 1.4;
                    r.height *= 1.4;

                    r.x = rx - r.width / 2;
                    r.y = ry - r.height / 2;

                    r &= Rect(0, 0, img.cols, img.rows);

                    Mat gray = frame.kinect.getGray();
                    // Mat roi(gray, r);

                    Mat roi_color; 
                    Mat roi(frame.kinect.getRGB().clone(), r);
                    cvtColor(roi, roi_color, CV_BGR2Lab);

                    // Populate PCL with the right ROI
                    // populatePCL(frame, point_cloud_pcl, r);
                    // populatePCL(frame, point_cloud_pcl, currMask);                    
                    // publishPCL(point_cloud_pcl, "KINECT_POINT_CLOUD_GREEN");

                    if (!prev_gray.empty()) { 
                        r.width = prev_gray.cols;
                        r.height = prev_gray.rows;
                    }

                    // lk_features(gray);
                    // mser_features(roi);

                    SLIC_info slic_info = slic_superpixel_segmentation(frame, r);
                    if (!lm_extracted) { 

                        // === Create full size mask === 
                        currMask = Mat1b::zeros(img.size());
                        Mat mask_roi(currMask, r);
                        slic_info.mask.copyTo(mask_roi);

                    } 

                    std::vector<sp_color_info> sp_labels(slic_info.slic.numlabels);
                    for (int j=0; j<slic_info.slic.numlabels; j++) { 

                        int x = slic_info.slic.kseeds_x[j];
                        int y = slic_info.slic.kseeds_y[j];
                        if (x <= 0 || y <= 0 || x > slic_info.mask.cols  || y > slic_info.mask.rows )
                            continue;        
                        int n = slic_info.slic.klabels[y*slic_info.mask.cols + x];
                        // std::cerr << (slic_info.mask.at<uchar>(y,x) ? "fg: " : "bg: ") << n << std::endl;
                        sp_labels[n] = sp_color_info(Point3f(slic_info.mu_r[n], slic_info.mu_g[n], slic_info.mu_b[n]), slic_info.mask.at<uchar>(y,x) ? true : false, x, y);
                    }
                        
                    // -1: init -> 0: learn
                    if (nn_color_classifier_learned == 0) { 
                        // std::cerr << "*********** LEARNING ************" << std::endl;
                        CC.learn(sp_labels);
                        nn_color_classifier_learned = 1;
                    } else if (nn_color_classifier_learned > 0) { 
                        // 0: learned -> 1: update
                        // update
                        // std::cerr << "*********** CLASSIFYING ************" << std::endl;
                        CC.classifyColor(sp_labels);      

                        //----------------------------------
                        // Plot the classified slic img with mean color features
                        //----------------------------------
                        Mat3b mean_slic_img = Mat3b::zeros(slic_info.mask.size());
                        Mat1b classified_slic_img_mask = Mat1b::zeros(slic_info.mask.size());
                        for (int y=0; y<mean_slic_img.rows; y++) { 
                            for (int x=0; x<mean_slic_img.cols; x++) { 
                                int idx = y*mean_slic_img.cols + x;
                                int n = slic_info.slic.klabels[idx];
                                float scale = (sp_labels[n].foreground) ? 1.f : 0.05;
                                classified_slic_img_mask(y,x) = (sp_labels[n].foreground) ? 255 : 0;
#if HSV
                                mean_slic_img(y,x) = opencv_utils::hsv_to_bgrvec(Point3f(slic_info.mu_b[n], slic_info.mu_g[n], slic_info.mu_r[n])) * scale;
#else
                                mean_slic_img(y,x) = Vec3b(slic_info.mu_b[n], slic_info.mu_g[n], slic_info.mu_r[n]) * scale;
#endif
                            }
                        }

                        pick_low_mi_blob(classified_slic_img_mask, slic_info.mask);

                        // === Create full size mask === 
                        currMask = Mat1b::zeros(img.size());
                        Mat mask_roi(currMask, r);
                        slic_info.mask.copyTo(mask_roi);

                        // currMask = slic_info.mask.clone();
                        // currROI = r;
    
                        opencv_utils::imshow("Mean SLIC Classify", mean_slic_img);
                  
                        //----------------------------------
                        // Visualize object RGB values
                        //----------------------------------
                        std::vector<Scalar> colors(CC.foreground_id_count()+1);
                        opencv_utils::fillColors(colors);

                        Mat slic_obj_img = Mat::zeros(slic_info.mask.size(), CV_8UC3);
                        for (int j=0; j<sp_labels.size(); j++) { 
                            int x = sp_labels[j].x;
                            int y = sp_labels[j].y;
                            if (x <= 0 || y <= 0 || x > slic_obj_img.cols  || y > slic_obj_img.rows )
                                continue;        
                            int n = slic_info.slic.klabels[y*slic_obj_img.cols+x];
#if HSV
                            Scalar s = opencv_utils::hsv_to_bgr(Point3f(slic_info.mu_b[n], slic_info.mu_g[n], slic_info.mu_r[n]));
#else
                            Scalar s(slic_info.mu_b[n], slic_info.mu_g[n], slic_info.mu_r[n]);
#endif
                            circle(slic_obj_img, Point(x,y), 3, s, CV_FILLED);
                            if (sp_labels[j].foreground)
                                circle(slic_obj_img, Point(x,y), 5, colors[sp_labels[j].class_id], 1);
                                // circle(slic_obj_img, Point(x,y), 5, CV_RGB(0,255,0), 1);
                            else
                                circle(slic_obj_img, Point(x,y), 5, CV_RGB(55,0,0), 1);
                            // cv::putText(slic_obj_img, cv::format("%i", n),Point2f(x,y), 0, 0.25, s,1);
                        }
                        opencv_utils::imshow("SLIC Classify", slic_obj_img);
                        
                    }


//                     // match_optical_flow(gray, contours, r);
//                     // match_optical_flow_dense(roi_color);
//                     lm_detect(frame, slic_info, r);

                    if (!currMask.empty())
                        opencv_utils::imshow( "Mask connect", currMask );

                    cv::Mat patch = tldtracker->currPosPatch();
                    if (!patch.empty()) { 
                        cv::Mat3b patch3;
                        cvtColor(patch, patch3, CV_GRAY2BGR);
                        resize(patch3, patch3, roi_orig.size());
                        addWeighted( roi_orig, 0.2, patch3, 0.8, 0, patch3 );
                        
                        opencv_utils::imshow("roi_orig+patch", patch3);
                    }
    

                    // *prev_point_cloud_pcl = *point_cloud_pcl;
                    // // imshow("roi", roi);
                }
                // plot_track(frame, *(tldtracker->currBB));
            }
        
            // printf("Current conf: %3.2f\n", tldtracker->currConf);
        }
        opencv_utils::imshow("result", img);
    }
    // find_corr_bb_in_bumblebee(frame, *(tldtracker->currBB));
}

// void partfilt_track(Frame& frame) { 
//     Mat img = frame.kinect.getRGB().clone();
//     if (trackObject) { 
//         if (trackObject < 0) { 
//             // frame initialize points to the PF tracker
//             std::vector<cv::Point> roi_pts;
//             std::cerr << selection.x << " " << selection.width << " " << selection.y << " " << selection.height << std::endl;
//             for (int j=0; j<frame.pts.size(); j++) { 
//                 if (frame.pts[j].x >= selection.x && frame.pts[j].y >= selection.y && 
//                     frame.pts[j].x < selection.x+selection.width && 
//                     frame.pts[j].y < selection.y+selection.height) { 
//                     // circle(img, frame.pts[j], 3, Scalar(0,255,255), CV_FILLED);
//                     roi_pts.push_back(frame.pts[j]);
//                     std::cerr << "pt: " << frame.pts[j].x << " " << frame.pts[j].y << std::endl;
//                 }
//             }

//             opencv_utils::imshow("first frame", img);
//             trackObject = 1;

//             // Initialize Particle Filter Tracker
//             std::vector<cv::Rect> roi_regions;
//             roi_regions.push_back(selection);
//             // pfilt_tracker.initTracker(img, roi_regions, 20);
//             // pfilt_tracker.initTracker(img, roi_pts, frame.desc, 20);

//         } else { 
//             // add points to the pf tracker
//             // pfilt_tracker.next(img, frame.pts, frame.desc);
//             // pfilt_tracker.addObjects(img, regions, n);
//         }
//         // pfilt_tracker.showResults(img, 0);
//         opencv_utils::imshow("result", img);
//     }
// }

int envoy_decompress_bot_core_image_t(const bot_core_image_t * jpeg_image, bot_core_image_t *uncomp_image)
{
  //copy metadata
  uncomp_image->utime = jpeg_image->utime;
  if (uncomp_image->metadata != NULL)
    bot_core_image_metadata_t_destroy(uncomp_image->metadata);
  uncomp_image->nmetadata = jpeg_image->nmetadata;
  if (jpeg_image->nmetadata > 0) {
    uncomp_image->metadata = bot_core_image_metadata_t_copy(jpeg_image->metadata);
  }
  
  uncomp_image->width = jpeg_image->width;
  uncomp_image->height = jpeg_image->height;
  
  uncomp_image->row_stride = jpeg_image->width * 3;//jpeg_image->row_stride;

  int depth=0;
  if (uncomp_image->width>0)
      depth= uncomp_image->row_stride / (double)uncomp_image->width;

  int ret;

  if(uncomp_image->data == NULL){
      int uncomp_size = (uncomp_image->width) * (uncomp_image->height) * depth;
      uncomp_image->data = (unsigned char *) realloc(uncomp_image->data, uncomp_size * sizeof(uint8_t));
  }

  ret = jpeg_decompress_8u_rgb(jpeg_image->data, 
                               jpeg_image->size, uncomp_image->data, 
                               uncomp_image->width, 
                               uncomp_image->height, 
                               uncomp_image->row_stride);
  
  uncomp_image->row_stride = uncomp_image->width * depth;
  uncomp_image->size = uncomp_image->width *uncomp_image->height* depth;
  if (depth == 1) {
    uncomp_image->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY;
  }
  else if (depth == 3) {
    uncomp_image->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB; //TODO: lets just assume its rgb... dunno what else to do :-/
  }
  return ret;
}

static void on_bumblebee_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                                   const bot_core_image_t *msg, 
                                   void *user_data ) {
    if (msg->width != WIDTH || msg->height != HEIGHT)
        return;
    bb_img = Mat3b::zeros(msg->height, msg->width);
    
    static bot_core_image_t * localFrame=(bot_core_image_t *) calloc(1,sizeof(bot_core_image_t));
    if (msg->pixelformat==BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG){
        //create space for decompressed image
        envoy_decompress_bot_core_image_t(msg,localFrame);
        memcpy(bb_img.data, localFrame->data, sizeof(uint8_t)*msg->width*msg->height*3);            
    } else{
        //uncompressed, just copy pointer
        memcpy(bb_img.data, msg->data, sizeof(uint8_t)*msg->width*msg->height*3);            
    }
    cvtColor(bb_img, bb_img, CV_BGR2RGB);

    // if (bb_rect.width > 0 && bb_rect.height > 0 && 
    //     bb_rect.x >= 0 && bb_rect.y >=0  && 
    //     bb_rect.x < bb_img.cols && bb_rect.y < bb_img.rows)
    rectangle(bb_img, mouse_rect, Scalar(255,0,0), 8, 8, 0);

    
    Mat_<double> R0 = Mat_<double>::eye(3,3);
    Mat_<double> t0 = Mat_<double>::zeros(3,1);

    Point3f tl = backproject(Point((mouse_rect.x-mouse_rect.width/2)*1024/640, 
                                   (mouse_rect.y-mouse_rect.height/2)*768.f/480), 
                             1, bb_K, R0, t0);
    Point3f tr = backproject(Point((mouse_rect.x+mouse_rect.width/2)*1024/640, 
                                   (mouse_rect.y-mouse_rect.height/2)*768.f/480), 
                             1, bb_K, R0, t0);
    Point3f br = backproject(Point((mouse_rect.x+mouse_rect.width/2)*1024/640, 
                                   (mouse_rect.y+mouse_rect.height/2)*768.f/480), 
                             1, bb_K, R0, t0);
    Point3f bl = backproject(Point((mouse_rect.x-mouse_rect.width/2)*1024/640, 
                                   (mouse_rect.y+mouse_rect.height/2)*768.f/480), 
                             1, bb_K, R0, t0);

    // std::cerr << "tl: " << tl.x << " " << tl.y << " " << tl.z << std::endl;


    // // LCMGL ---------------------------------------

    // // gl draw with queue
    // bot_lcmgl_t* lcmgl = lcmgl_features;

    // // Rotation to viewer
    // lcmglPushMatrix();
    // double kinect_to_local_m[16];
    // bot_frames_get_trans_mat_4x4(state->frames,"bumblebee_left",
    //                              bot_frames_get_root_name(state->frames),
    //                              kinect_to_local_m);

    // // opengl expects column-major matrices
    // double kinect_to_local_m_opengl[16];
    // bot_matrix_transpose_4x4d(kinect_to_local_m, kinect_to_local_m_opengl);
    // lcmglMultMatrixd(kinect_to_local_m_opengl);

    // lcmglLineWidth(4);
    // lcmglColor3f(.6,0,0);
    // // lcmglEnable(GL_BLEND);
    // lcmglEnable(GL_DEPTH_TEST);

    // lcmglBegin(GL_LINE_LOOP);
    // lcmglVertex3f(tl.x,tl.y,tl.z);
    // lcmglVertex3f(tr.x,tr.y,tr.z);
    // lcmglVertex3f(br.x,br.y,br.z);
    // lcmglVertex3f(bl.x,bl.y,bl.z);
    // lcmglVertex3f(tl.x,tl.y,tl.z);
    // lcmglEnd();
    // // lcmglDisable(GL_BLEND);
    // lcmglDisable(GL_DEPTH_TEST);
    // lcmglPopMatrix();

    // bot_lcmgl_switch_buffer(lcmgl_features);

    // imshow("bumblebee", bb_img);

    return;
}



void  INThandler(int sig)
{
    // lcm_destroy(state->lcm);
    printf("Exiting\n");
    // exit(0);
}

static void onMouse(int event, int x, int y, int flags, void* userdata) {
    MouseEvent* data = (MouseEvent*)userdata;
    
    if (selectObject) {
        selection.x = MIN(x, origin.x);
        selection.y = MIN(y, origin.y);
        selection.width = std::abs(x - origin.x);
        selection.height = std::abs(y - origin.y);
        selection &= Rect(0, 0, WIDTH, HEIGHT);
    }

    switch (event) {
    case CV_EVENT_LBUTTONDOWN:
        origin = Point(x, y);
        selection = Rect(x, y, 0, 0);
        selectObject = true;
        break;
    case CV_EVENT_LBUTTONUP:
        selectObject = false;
        trackObject = -1;
        break;
    }
    return;
}

static void onMouse_bb(int event, int x, int y, int flags, void* userdata) {
    MouseEvent* data = (MouseEvent*)userdata;

    mouse_rect = Rect(x-25,y-25,50,50);       
    mouse_rect &= Rect(0, 0, WIDTH, HEIGHT);

    return;
}


// Functions to store detector and templates in single XML/YAML file
cv::Ptr<cv::linemod::Detector> readLinemod(const std::string& filename)
{
  cv::Ptr<cv::linemod::Detector> detector = new cv::linemod::Detector;
  cv::FileStorage fs(filename, cv::FileStorage::READ);
  detector->read(fs.root());

  cv::FileNode fn = fs["classes"];
  for (cv::FileNodeIterator i = fn.begin(), iend = fn.end(); i != iend; ++i)
    detector->readClass(*i);

  return detector;
}

void writeLinemod(const cv::Ptr<cv::linemod::Detector>& detector, const std::string& filename)
{
  cv::FileStorage fs(filename, cv::FileStorage::WRITE);
  detector->write(fs);

  std::vector<std::string> ids = detector->classIds();
  fs << "classes" << "[";
  for (int i = 0; i < (int)ids.size(); ++i)
  {
    fs << "{";
    detector->writeClass(ids[i], fs);
    fs << "}"; // current class
  }
  fs << "]"; // classes
}

void* opencv_thread(void* arg) { 
    while (1) { 
        lcm_handle(state->lcm);
        std::cerr << "update\n" << std::endl;
    }

    return NULL;
}

static void on_kinect_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                                   const kinect_frame_msg_t *msg, 
                                   void *user_data ) {
    
    if (msg->depth.depth_data_format != KINECT_DEPTH_MSG_T_DEPTH_MM) { 
        std::cerr << "Warning: kinect data format is not KINECT_DEPTH_MSG_T_DEPTH_MM" << std::endl 
                  << "Program may not function properly" << std::endl;
    }
    assert (msg->depth.depth_data_format == KINECT_DEPTH_MSG_T_DEPTH_MM);

    Frame frame;
    // frame.kinect = kinect_data(true);
    frame.kinect.set_calib(depth_intrinsics[0], depth_intrinsics[3], depth_intrinsics[4]); // fx, cx, cy
    frame.kinect.update(msg);

    //----------------------------------
    // Feature tracking with KLT features
    //----------------------------------
    // track_klt_features(frame);
    // lk_features(frame);













    
    slic_superpixel_segmentation(frame);
    // pff_superpixel_segmentation(frame);
    // fast_detector(frame);
    // contour_detector(frame);

    // // Convert to 3D
    // for (int j=0; j<frame.pts.size(); j++)
    //     frame.keypts3d.push_back(frame.kinect.getXYZ(frame.pts[j].y,frame.pts[j].x));

    // match_fast_features(frame); // warning: pts are updated here via RANSAC matching with previous frame
    // match_optical_flow(frame);
    // partfilt_track(frame);
    // tld_track(frame);

    // // LCMGL ---------------------------------------

    // // gl draw with queue
    // bot_lcmgl_t* lcmgl = lcmgl_features;

    // // Rotation to viewer
    // lcmglPushMatrix();
    // double kinect_to_local_m[16];
    // bot_frames_get_trans_mat_4x4(state->frames,"KINECT",
    //                              bot_frames_get_root_name(state->frames),
    //                              kinect_to_local_m);

    // // opengl expects column-major matrices
    // double kinect_to_local_m_opengl[16];
    // bot_matrix_transpose_4x4d(kinect_to_local_m, kinect_to_local_m_opengl);
    // lcmglMultMatrixd(kinect_to_local_m_opengl);

    // lcmglPointSize(2);
    // lcmglEnable(GL_BLEND);
    // lcmglBegin(GL_POINTS);
    // for (int j=0; j<frame_queue.size(); j++) { 
    //     const Frame& frame = frame_queue[j];
    //     if (j == 0) {
    //         lcmglColor4f(0,0,1.f,1.f);
    //     } else { 
    //         lcmglColor4f(.6,0,0,1.f - j*1.f/frame_queue.size());
    //     }

    //     const std::vector<Vec3f>& featptsj = frame.keypts3d;
    //     for (int k=0; k<featptsj.size(); k++) { 
    //         const Vec3f& xyz = featptsj[k];
    //         lcmglVertex3f(xyz[0],-xyz[1],xyz[2]);
    //     }
    // }
    // lcmglEnd();

    // lcmglBegin(GL_LINES);
    // for (int j=0; j<frame_queue.size(); j++) { 
    //     const Frame& frame = frame_queue[j];
    //     lcmglColor4f(0,.3,0,1.f - j*1.f/frame_queue.size());

    //     if (!frame.keypts3d_next.size())
    //         continue;

    //     const std::vector<Vec3f>& curr_featptsj = frame.keypts3d_next;
    //     const std::vector<Vec3f>& prev_featptsj = frame.keypts3d;

    //     for (int k=0; k<curr_featptsj.size(); k++) { 
    //         if (frame.status[k]) { 
    //             const Vec3f& xyz1 = curr_featptsj[k];
    //             const Vec3f& xyz2 = prev_featptsj[k];
    //             lcmglVertex3f(xyz1[0],-xyz1[1],xyz1[2]);
    //             lcmglVertex3f(xyz2[0],-xyz2[1],xyz2[2]);
    //             // std::cerr << " ==========" << std::endl;
    //             // // Vec3f xyz1 = prev_frame.keypts3d[j];
    //             // // Vec3f xyz2 = prev_frame.keypts3d_next[j];
    //             // std::cerr << xyz1[0] << " " << xyz1[1] << " " << xyz1[2] << std::endl;
    //             // std::cerr << xyz2[0] << " " << xyz2[1] << " " << xyz2[2] << std::endl;

    //         }
    //     }


    // }
    // lcmglEnd();
    // lcmglDisable(GL_BLEND);
    // lcmglPopMatrix();

    // bot_lcmgl_switch_buffer(lcmgl_features);

    // opencv draw
    // for (int j=0;j<frame.pts.size();++j) 
    //     circle(img, frame.pts[j], 3, Scalar(0,0,255));

    //std::cout << ">" << H12 << std::endl;

    // if (selectObject && selection.width > 0 && selection.height > 0) {
    //     Mat roi(img, selection);
    //     bitwise_not(roi, roi);
    // }


    // rectangle(img, mouse_rect, Scalar(255,0,0), 8, 8, 0);
    // opencv_utils::imshow("Kinect RGB", img);

    return;
}

int main(int argc, char** argv)
{
    // g_thread_init(NULL);
    setlinebuf (stdout);
    state = (state_t*) calloc(1, sizeof(state_t));

    // Param server, botframes
    state->lcm =  bot_lcm_get_global(NULL);
    // state->mainloop = g_main_loop_new( NULL, FALSE );  
    state->param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->param);

    // Detector / Matcher / Extractor
    // _detector = FeatureDetector::create( "SURF" );
    // _descriptorExtractor = DescriptorExtractor::create( "SURF" );
    // _descriptorMatcher = DescriptorMatcher::create( "FlannBased" );

#if PCL
    // Point clouds
    point_cloud_pcl = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
    prev_point_cloud_pcl = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
    canonical_obj_pcl = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
#endif
    // viewer = new pcl::visualization::PCLVisualizer ("Simple Cloud Viewer");
    // range_image_widget = new pcl::visualization::RangeImageVisualizer ("Range image");

    // ctracker.useKinectData(&kinect);
    
    // New TLD Tracker
    tldtracker = new tld::TLD();

    tldtracker->detectorCascade->imgWidth = WIDTH;
    tldtracker->detectorCascade->imgHeight = HEIGHT;
    tldtracker->detectorCascade->imgWidthStep = WIDTH;

    tldtracker->alternating = false;
    tldtracker->learningEnabled = true;

    tld::DetectorCascade* detectorCascade = tldtracker->detectorCascade;
    detectorCascade->varianceFilter->enabled = true;
    detectorCascade->ensembleClassifier->enabled = true;
    detectorCascade->nnClassifier->enabled = true;

    // classifier
    detectorCascade->useShift = true; 
    detectorCascade->shift = 0.1;
    detectorCascade->minScale = -10;
    detectorCascade->maxScale = 10;
    detectorCascade->minSize = 25;
    detectorCascade->numTrees = 10;
    detectorCascade->numFeatures = 10;
    detectorCascade->nnClassifier->thetaTP = 0.65;
    detectorCascade->nnClassifier->thetaFP = 0.5;

#if 1
    std::cerr << "reading from model" << std::endl;
    tldtracker->readFromFile("model");
    tldtracker->learningEnabled = false;
    trackObject = 1;
#endif

    // LINEMOD detector
    lm_detector = cv::linemod::getDefaultLINEMOD();


    double rvec[3], rpy[3], tvec[3];
    double bb_k[5], bb_d[5];
    bot_param_get_double_array (state->param, "cameras.bumblebee_left.intrinsic_cal.pinhole", 
                                &(bb_k[0]), 5);
    bot_param_get_double_array (state->param, "cameras.bumblebee_left.intrinsic_cal.distortion_k", 
                                &(bb_d[0]), 3);
    bot_param_get_double_array (state->param, "cameras.bumblebee_left.intrinsic_cal.distortion_p", 
                                &(bb_d[3]), 2);

    bot_param_get_double_array(state->param, 
                               "rgbd_cameras.KINECT.intrinsic_cal.depth_fx", &depth_intrinsics[0], 1);
    bot_param_get_double_array(state->param, 
                               "rgbd_cameras.KINECT.intrinsic_cal.depth_fx", &depth_intrinsics[1], 1);
    depth_intrinsics[2] = 0;
    bot_param_get_double_array(state->param, 
                               "rgbd_cameras.KINECT.intrinsic_cal.depth_cx", &depth_intrinsics[3], 1);
    bot_param_get_double_array(state->param, 
                               "rgbd_cameras.KINECT.intrinsic_cal.depth_cy", &depth_intrinsics[4], 1);

    // std::cerr << "d2r: " << depth_to_rgb_T << std::endl;                

    bb_K = pinhole_to_mat(bb_k);
    bb_D = distortion_to_mat(bb_d);

    double body_to_bb[16];
    bot_frames_get_trans_mat_4x4(state->frames,"body",
                                 "bumblebee_left",
                                 body_to_bb);

    Mat_<double> bb_T = Mat_<double>(4,4,body_to_bb);

    extract_camera_extrinsics(bb_T, bb_rvec, bb_tvec);
    Rodrigues(bb_rvec, bb_R);
    // point_cloud_pcl = Mat_<Point3f>(nvert, nhoriz);

    std::cerr << "T: " << bb_T << std::endl;
    std::cerr << "K: " << bb_K << " D: " << bb_D << " rvec: " << bb_rvec << " tvec: " << bb_tvec << std::endl;
    // if (!state->mainloop) {
    //     printf("Couldn't create main loop\n");
    //     return -1;
    // }

    //add lcm to mainloop 
    // bot_glib_mainloop_attach_lcm (state->lcm);

    // Subscribe to the kinect image
    kinect_frame_msg_t_subscribe(state->lcm, "KINECT_FRAME", on_kinect_image_frame, (void*)state);
    bot_core_image_t_subscribe(state->lcm, "FRNT_IMAGE_BUMBLEBEE", on_bumblebee_image_frame, NULL);
    // kinect_skeleton_msg_t_subscribe(state->lcm, "KINECT_SKELETON_FRAME", on_skeleton_frame, NULL);
    lcmgl_features = bot_lcmgl_init (state->lcm, "features");
    // bot_core_image_t_subscribe(state->lcm, "Segmented Image", on_segmented_frame, NULL);

    //adding proper exiting 
    // bot_signal_pipe_glib_quit_on_kill (state->mainloop);

    // Install signal handler to free data.
    signal(SIGINT, INThandler);

    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    // g_main_loop_run(state->mainloop);
    // namedWindow( "Kinect RGB" );
    // namedWindow( "bumblebee" );
    // setMouseCallback("Kinect RGB", onMouse, &mouse);
    // setMouseCallback("bumblebee", onMouse_bb, &mouse_bb);

    // namedWindow("false positives");
    // namedWindow("true positives");
    // namedWindow("first frame");
    // namedWindow("result");
    // createTrackbar( "Sigma * 0.01", "settings", &pff_sigma, 200, NULL );
    // createTrackbar( "K", "settings", &pff_K, 100, NULL );
    // createTrackbar( "min", "settings", &pff_min, 200, NULL );

    // namedWindow("slic");
    // createTrackbar( "k", "settings", &slic_size, 100, NULL );
    // createTrackbar( "m", "settings", &slic_m, 40, NULL );
    // createTrackbar( "rgb_dist", "slic", &slic_rgb_dist, 100, 0 );

    // namedWindow("mser");
    // createTrackbar( "delta", "mser", &mser_delta, 100, 0 );
    // createTrackbar( "area_threshold", "mser", &mser_area_threshold, 400, 0 );
    // createTrackbar( "min_margin", "mser", &mser_min_margin, 1000, 0 );
    // createTrackbar( "max_variation", "mser", &mser_max_variation, 100, 0 );
    // createTrackbar( "min_diversity", "mser", &mser_min_diversity, 10, 0 );


    int lcm_fd = lcm_get_fileno(state->lcm);

    fd_set fds;

    // wait a limited amount of time for an incoming message
    struct timeval timeout = { 
        0,  // seconds
        10000   // microseconds
    };

    while(1) { 
        unsigned char c = cv::waitKey(10) & 0xff;

        FD_ZERO(&fds);
        FD_SET(lcm_fd, &fds);

        // setup the LCM file descriptor for waiting.
        int status = select(lcm_fd + 1, &fds, 0, 0, &timeout);
        if(status && FD_ISSET(lcm_fd, &fds)) {
            // LCM has events ready to be processed.
            lcm_handle(state->lcm);
        }

        if (c == 'q') { 
            break;      
        } else if (c == 'i') { 
            need_to_init = true;
            std::cerr << "need to init" << std::endl;
        } else if (c == 'l') { 
            tldtracker->learningEnabled = !tldtracker->learningEnabled;
            printf("LearningEnabled: %d\n", tldtracker->learningEnabled);
        } else if (c == 'm') { 
            learn_online = !learn_online;
            printf("LINEMOD Learning Enabled: %d\n", learn_online);
        } else if (c == 's') { 
            Frame& frame = frame_queue.front();
            Mat img = frame.kinect.getRGB();
            std::cerr << "writing frame.png" << std::endl;
            imwrite("frame.png", img);
        } else if (c == 'a') {
            tldtracker->alternating = !tldtracker->alternating;
            printf("alternating: %d\n", tldtracker->alternating);
        } else if (c == 'r') { 
            std::cerr << "reading from model" << std::endl;
            tldtracker->readFromFile("model");
            tldtracker->learningEnabled = false;
            trackObject = 1;

        } else if (c== 'R') { 

            // delete detector;
            lm_detector = readLinemod("linemod_templates.yml");
            std::cerr << "LINEMOD: reading from model" << std::endl;
            num_classes = lm_detector->numClasses();


        } else if (c == 'e') { 
            if(tldtracker->currBB != NULL) {
                Frame& frame = frame_queue.front();
#if PCL
                populatePCL(frame, canonical_obj_pcl, currMask);
                publishPCL(point_cloud_pcl, "KINECT_POINT_CLOUD_RED");                    
                std::cerr << "Extracted canonical pose" << std::endl;

                float cx=0, cy=0, cz=0;
                for (int j=0; j<canonical_obj_pcl->points.size(); j++) { 
                    cx += canonical_obj_pcl->points[j].x, 
                        cy += canonical_obj_pcl->points[j].y, 
                        cz += canonical_obj_pcl->points[j].z;
                }
                mean_canonical_obj_pcl = Point3f(cx/canonical_obj_pcl->points.size(),
                                                 cy/canonical_obj_pcl->points.size(),
                                                 cz/canonical_obj_pcl->points.size());

#endif
                
            }
        } else if (c == 'w') { 
            std::cerr << "exiting: writing to model" << std::endl;
            tldtracker->writeToFile("model");
        } else if (c == 'W') { 
            std::cerr << "LINEMOD: writing to model" << std::endl;
            writeLinemod(lm_detector, "linemod_templates.yml");
        } else if (c == 'x') { 
            cerr << "Attempting to extract LineMOD template from currMask" << endl;
            Frame& frame = frame_queue.front();
            Rect bb;

            std::vector<cv::Mat> sources;
            sources.push_back(frame.kinect.getRGB());
            sources.push_back(frame.kinect.getDepth());

            std::string class_id = cv::format("class%d", num_classes);
            int template_id = lm_detector->addTemplate(sources, class_id, currMask, &bb);
            if (template_id != -1) { 
                opencv_utils::displayStatusBar(cv::format("*** Added template (id %d) for new object class %d***\n",
                                                          template_id, num_classes), 3000);
                lm_extracted = true;
            }
            num_classes++;
            nn_color_classifier_learned = 0;
        }


    }

    // bot_glib_mainloop_detach_lcm(state->lcm);
    lcm_destroy(state->lcm);

    // pthread_t tid_opencv;
    // pthread_attr_t opencv_tattr;
    // pthread_attr_init(&opencv_tattr);
    // pthread_create(&tid_opencv, &opencv_tattr, &opencv_thread, NULL);

    
    return 0;
}



                    

// if (!prev_gray_farneback.empty()) { 
//     Mat flow;
//     calcOpticalFlowFarneback(prev_gray_farneback, roi, flow, 0.5, 3, 3, 5, 7, 1.5, OPTFLOW_FARNEBACK_GAUSSIAN);
//     std::vector<Mat> flow_channels;
//     cv::split(flow,flow_channels);
//     Mat velx = flow_channels[0], vely = flow_channels[1];
//     std::vector<std::pair<Point2f, Point2f> > lines = getFlowLines(velx,vely,10,4);

//     for (int j=0; j<lines.size(); j++) 
//         line(roi, lines[j].first, lines[j].second, Scalar(0,255,255));
                        
// }

    
#if 0    
#if 1
    //----------------------------------
    // Partition border color features
    //----------------------------------
    vector<int> labels_part;
    int nclasses = partition( border_lab_feats, labels_part, PartitionPts() );

    std::vector<Scalar> colors(nclasses);
    opencv_utils::fillColors(colors);
    
    Mat part_mat(512, 512, CV_8UC3);
    part_mat = Scalar::all(0);
    for( int j = 0; j < border_lab_feats.size(); j++ ) {
        int clusterIdx = labels_part[j];
        Point ipt = Point(border_lab_feats[j].x, border_lab_feats[j].y);
        circle( part_mat, ipt, 2, colors[clusterIdx], CV_FILLED, CV_AA );
    }
    imshow("part_mat", part_mat);

    //----------------------------------
    // Find centers of labels
    //----------------------------------
    std::vector<Point3f> mu_class(nclasses, Point3f(0,0,0));
    std::vector<int> size_class(nclasses, 0);
    for (int j=0; j<border_lab_feats.size(); j++) { 
        int clusterIdx = labels_part[j];
        mu_class[clusterIdx] += border_lab_feats[j];
        size_class[clusterIdx] += 1;
    }

    // === Mean ===
    for (int j=0; j<mu_class.size(); j++)
        if (size_class[j] > 0)
            mu_class[j] *= 1.f/size_class[j];

    Mat centers(mu_class), labels(labels_part);

#else
    //----------------------------------
    // K-means on the border color features
    //----------------------------------

    Mat kmeans_mat(512, 512, CV_8UC3);
    kmeans_mat = Scalar::all(0);
    Mat kmeans_mat2 = kmeans_mat.clone();

    int k_count = 6;
    Mat labels, centers;
    double kmeans_err = kmeans(Mat(border_lab_feats), k_count, labels, 
                               cv::TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 10, 1.0),
                               3, KMEANS_RANDOM_CENTERS, centers);
    std::cerr << "Kmeans: " << kmeans_err  << std::endl;

    //----------------------------------
    // K-means plot
    //----------------------------------
    std::vector<Scalar> colorTab(k_count);
    opencv_utils::fillColors(colorTab);

    for( int j = 0; j < border_lab_feats.size(); j++ ) {
        int clusterIdx = labels.at<int>(j);
        Point ipt = Point(border_lab_feats[j].z, border_lab_feats[j].y);
        circle( kmeans_mat, ipt, 2, opencv_utils::hsv_to_bgr(Point3f(border_lab_feats[j].z, border_lab_feats[j].y, border_lab_feats[j].x)),
                CV_FILLED, CV_AA );
        circle( kmeans_mat, ipt, 2, colorTab[clusterIdx], CV_FILLED, CV_AA );
    }
    imshow("kmeans_mat", kmeans_mat);
    imshow("kmeans_mat2", kmeans_mat2);
    
    
    if (kmeans_err > 1e5)
        return;
    
#endif
    std::cerr << "labels: " << labels.rows << "," << labels.cols << ": " << labels << std::endl;
    std::cerr << "centers: " << centers.rows << "," << centers.cols << ": " << centers << std::endl;
    
    //----------------------------------
    // Sort the clusters based on modes
    //----------------------------------
    std::vector<int> cluster_size(centers.rows, 0);
    for (int j=0; j<labels.rows; j++) 
        cluster_size[labels.at<int>(j)]++;

    //----------------------------------
    // Cluster idx, and size (sorted by size)
    //----------------------------------
    std::vector<std::pair<int, int> > cluster_ids;
    for (int j=0; j<cluster_size.size(); j++)
        cluster_ids.push_back(std::make_pair(j, cluster_size[j]));
    std::sort(cluster_ids.begin(), cluster_ids.end(), cluster_sizesort);

    for (int j=0; j<cluster_ids.size(); j++)
        std::cerr << cluster_ids[j].first << "->" << cluster_ids[j].second << std::endl;

    //----------------------------------
    // Plot border color features
    //----------------------------------
    int max_modes = 4;
    int border_scale = 20; 
    Mat border_color_feats_mat = Mat::zeros(border_scale * (max_modes + 1), border_lab_feats.size() * border_scale, CV_8UC3);
    
    // Plot the border features in the first row
    for (int j=0; j<border_lab_feats.size(); j++) 
        rectangle(border_color_feats_mat, 
                  Point(j * border_scale, 0), 
                  Point( (j+1) * border_scale, border_scale), 
                  opencv_utils::hsv_to_bgr(Point3f(border_lab_feats[j].z, border_lab_feats[j].y, border_lab_feats[j].x)), CV_FILLED);
    // Plot the top modes the following rows; 1st-mode in the 2nd row, so on


    for (int j=0; j<cluster_ids.size() && j < max_modes; j++) { 
        int cluster_id = cluster_ids[j].first;
        for (int k=0; k<border_lab_feats.size(); k++) { 
            if (cluster_id == labels.at<int>(k)) { 
                rectangle(border_color_feats_mat, 
                          Point(k * border_scale, (j+1) * border_scale), 
                          Point( (k+1) * border_scale, (j+2) * border_scale), 
                          opencv_utils::hsv_to_bgr(Point3f(border_lab_feats[k].z, border_lab_feats[k].y, border_lab_feats[k].x)), CV_FILLED);                
            }
        }
    }
    imshow("border_color_Features", border_color_feats_mat);


    //----------------------------------
    // Find the center that corresponds to the largest cluster
    //----------------------------------
    Point maxIdx(-1,-1);
    minMaxLoc(Mat(cluster_size), 0, 0, 0, &maxIdx);
    if (maxIdx.y >=0 && maxIdx.y < centers.rows) { 
        Mat first_mode = centers.row(maxIdx.y);
        Point3f first_mode_pt(first_mode.at<float>(0,0), 
                              first_mode.at<float>(0,1),
                              first_mode.at<float>(0,2));

        //----------------------------------
        // Find the distance to every other superpixel (from the first mode)
        //----------------------------------
        std::vector<double> dist_to_sp(slic.numlabels, 0);
        for (int j=0; j<slic.numlabels; j++) { 
            int seedx = slic.kseeds_x[j];
            int seedy = slic.kseeds_y[j];
            Point3f pt2(mu_b[j], mu_g[j], mu_r[j]); 
            
            dist_to_sp[j] = opencv_utils::l2_dist(first_mode_pt, pt2);
            // std::cerr << "["<<j<<"]: " << dist_to_sp[j] << ": ("<< pt2.x << "," << pt2.y << "," << pt2.z << ") at " << seedx << "," << seedy << std::endl;
        }    

        std::cerr << "First mode: " << first_mode_pt.x << "," << first_mode_pt.y << "," << first_mode_pt.z << std::endl;
   
        //----------------------------------
        // Find the min distance between centers
        //----------------------------------
        double min_dist = 1e9;
        std::vector<double> dists_vec;
        for (int k=0; k<centers.rows; k++) { 
            // if (j == k) continue;
            if (k == maxIdx.y) continue;
            Point3f p2(centers.at<float>(k,0), centers.at<float>(k,1), centers.at<float>(k,2)); 
            double dist = opencv_utils::l2_dist(first_mode_pt, p2);
            dists_vec.push_back(dist);
            std::cerr << "dist : " << dist << std::endl;
            if (dist < min_dist) min_dist = dist;
        }
        std::sort(dists_vec.begin(), dists_vec.end());
        std::cerr << "Min dist between centers: " << min_dist << std::endl;

        if (!dists_vec.size())
            return;
        //----------------------------------
        // Mask pixels that are farther from background (i.e. pick foreground object)
        //----------------------------------
        Mat1b first_mode_fg(img.rows, img.cols);
        first_mode_fg = 255;
        for (int y=0; y<first_mode_fg.rows; y++) { 
            for (int x=0; x<first_mode_fg.cols; x++) { 
                int idx = y*first_mode_fg.cols + x;
                int n = klabels[idx];
                // slic_img.at<Vec3b>(y,x) = hsv_to_bgrvec(Point3f(mu_b[n], mu_g[n], mu_r[n]));
                // slic_img.at<Vec3b>(y,x) = Vec3b(mu_b[n], mu_g[n], mu_r[n]);
                if (dist_to_sp[n] < slic_rgb_dist) { 
                    slic_img.at<Vec3b>(y,x) = slic_img.at<Vec3b>(y,x) * 0.2; 
                //     slic_img.at<Vec3b>(y,x) = hsv_to_rgbvec(Point3f(mu_r[n], mu_g[n], mu_b[n]));
                    
                }
            }
        }
        imshow("first_mode", first_mode_fg);
    }

#endif



#if 0
                    // pcl::PointCloud<pcl::PointXYZRGB>::Ptr final_pcl(new pcl::PointCloud<pcl::PointXYZRGB>);

                    // Eigen::Matrix4f icp_tf;
                    // computeICP(icp_tf, updated_tf, final_pcl, point_cloud_pcl, canonical_obj_pcl);

                    // ========================================================

                    
                    // --------------------
                    // -----Parameters-----
                    // --------------------
                    float support_size = 0.2f;
                    pcl::RangeImage::CoordinateFrame coordinate_frame = pcl::RangeImage::CAMERA_FRAME;
                    bool setUnseenToMaxRange = true;
                    bool rotation_invariant = true;
                    Eigen::Affine3f scene_sensor_pose = (Eigen::Affine3f)Eigen::Translation3f(0.0f, 0.0f, 0.0f);
                    // Eigen::Affine3f scene_sensor_pose (Eigen::Affine3f::Identity ());
                    pcl::PointCloud<pcl::PointWithViewpoint> far_ranges;
                    float angular_resolution = (float) (  .10f * (M_PI/180.0f));  //   1.0 degree in radians
                    float maxAngleWidth     = (float) (360.0f * (M_PI/180.0f));  // 360.0 degree in radians
                    float maxAngleHeight    = (float) (180.0f * (M_PI/180.0f));  // 180.0 degree in radians
  
                    // -----------------------------------------------
                    // -----Create RangeImage from the PointCloud-----
                    // -----------------------------------------------
                    float noise_level = 0.0;
                    float min_range = 0.0f;
                    int border_size = 1;
                    // boost::shared_ptr<pcl::RangeImage> range_image_ptr (new pcl::RangeImage);
                    pcl::RangeImagePlanar range_image; //  = *range_image_ptr;   
                    range_image.createFromPointCloudWithFixedSize(point_cloud, WIDTH, HEIGHT, 320, 240, 
                                                      576.09757860, 576.09757860, scene_sensor_pose);

                    // range_image.createFromPointCloud (point_cloud, angular_resolution, 
                    //                                   maxAngleWidth, maxAngleHeight,
                    //                                   scene_sensor_pose, coordinate_frame, noise_level, min_range, border_size);
                    std::cerr << range_image << std::endl;
                    // range_image.integrateFarRanges (far_ranges);
                    // if (setUnseenToMaxRange)
                    //     range_image.setUnseenToMaxRange ();
  
                    // // // --------------------------------------------
                    // // // -----Open 3D viewer and add point cloud-----
                    // // // --------------------------------------------
                    // // // pcl::visualization::PCLVisualizer viewer ("3D Viewer");
                    // // viewer->setBackgroundColor (1, 1, 1);
                    // // pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange> range_image_color_handler (range_image_ptr, 0, 0, 0);
                    // // viewer->updatePointCloud (range_image_ptr, range_image_color_handler, "range image");
                    // // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "range image");
                    // // //viewer.addCoordinateSystem (1.0f);
                    // // //PointCloudColorHandlerCustom<PointType> point_cloud_color_handler (point_cloud_ptr, 150, 150, 150);
                    // // //viewer.addPointCloud (point_cloud_ptr, point_cloud_color_handler, "original point cloud");
                    // // viewer->initCameraParameters ();
                    // // setViewerPose (*viewer, range_image.getTransformationToWorldSystem ());
  
                    // // --------------------------
                    // // -----Show range image-----
                    // // --------------------------
                    range_image_widget.showRangeImage (range_image);

                    // --------------------------------
                    // -----Extract NARF keypoints-----
                    // --------------------------------
                    pcl::RangeImageBorderExtractor range_image_border_extractor;
                    pcl::NarfKeypoint narf_keypoint_detector;
                    narf_keypoint_detector.setRangeImageBorderExtractor (&range_image_border_extractor);
                    narf_keypoint_detector.setRangeImage (&range_image);
                    narf_keypoint_detector.getParameters ().support_size = support_size;
  
                    pcl::PointCloud<int> keypoint_indices;
                    narf_keypoint_detector.compute (keypoint_indices);
                    std::cout << "Found "<<keypoint_indices.points.size ()<<" key points.\n";

                    // ----------------------------------------------
                    // -----Show keypoints in range image widget-----
                    // ----------------------------------------------
                    for (size_t i=0; i<keypoint_indices.points.size (); ++i)
                        range_image_widget.markPoint (keypoint_indices.points[i]%range_image.width,
                                                      keypoint_indices.points[i]/range_image.width, pcl::visualization::Vector3ub(255,0,0));
#endif  
#if 0
                    // // -------------------------------------
                    // // -----Show keypoints in 3D viewer-----
                    // // -------------------------------------
                    // pcl::PointCloud<pcl::PointXYZ>::Ptr keypoints_ptr (new pcl::PointCloud<pcl::PointXYZ>);
                    // pcl::PointCloud<pcl::PointXYZ>& keypoints = *keypoints_ptr;
                    // keypoints.points.resize (keypoint_indices.points.size ());
                    // for (size_t i=0; i<keypoint_indices.points.size (); ++i)
                    //     keypoints.points[i].getVector3fMap () = range_image.points[keypoint_indices.points[i]].getVector3fMap ();
                    // pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> keypoints_color_handler (keypoints_ptr, 0, 255, 0);
                    // viewer->addPointCloud<pcl::PointXYZ> (keypoints_ptr, keypoints_color_handler, "keypoints");
                    // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "keypoints");

                    // // ------------------------------------------------------
                    // // -----Extract NARF descriptors for interest points-----
                    // // ------------------------------------------------------
                    // std::vector<int> keypoint_indices2;
                    // keypoint_indices2.resize (keypoint_indices.points.size ());
                    // for (unsigned int i=0; i<keypoint_indices.size (); ++i) // This step is necessary to get the right vector type
                    //     keypoint_indices2[i]=keypoint_indices.points[i];
                    // pcl::NarfDescriptor narf_descriptor (&range_image, &keypoint_indices2);
                    // narf_descriptor.getParameters ().support_size = support_size;
                    // narf_descriptor.getParameters ().rotation_invariant = rotation_invariant;
                    // pcl::PointCloud<pcl::Narf36> narf_descriptors;
                    // narf_descriptor.compute (narf_descriptors);
                    // cout << "Extracted "<<narf_descriptors.size ()<<" descriptors for "
                    //      <<keypoint_indices.points.size ()<< " keypoints.\n";

                    // // Change detection
                    // // Octree resolution - side length of octree voxels
                    // float resolution = .020f;

                    // // Instantiate octree-based point cloud change detection class
                    // pcl::octree::OctreePointCloudChangeDetector<pcl::PointXYZRGB> octree (resolution);

                    // // Add points from cloudA to octree
                    // if (prev_point_cloud_pcl->points.size()) { 
                    //     octree.setInputCloud (prev_point_cloud_pcl);
                    //     octree.addPointsFromInputCloud ();

                    //     // Switch octree buffers: This resets octree but keeps previous tree structure in memory.
                    //     octree.switchBuffers ();

                    //     // Add points from cloudB to octree
                    //     octree.setInputCloud (point_cloud_pcl);
                    //     octree.addPointsFromInputCloud ();

                    //     std::vector<int> newPointIdxVector;

                    //     // Get vector of point indices from octree voxels which did not exist in previous buffer
                    //     octree.getPointIndicesFromNewVoxels (newPointIdxVector);

                    //     // Output points
                    //     std::cout << "Output from getPointIndicesFromNewVoxels: " << newPointIdxVector.size() << std::endl;
                        
                    //     for (size_t i = 0; i < newPointIdxVector.size (); ++i) { 
                    //         // std::cout << i << "# Index:" << newPointIdxVector[i]
                    //         //           << "  Point:" << cloudB->points[newPointIdxVector[i]].x << " "
                    //         //           << cloudB->points[newPointIdxVector[i]].y << " "
                    //         //           << cloudB->points[newPointIdxVector[i]].z << std::endl;
                            
                    //         point_cloud_pcl->points[newPointIdxVector[i]].r = 255;
                    //         point_cloud_pcl->points[newPointIdxVector[i]].g = 0;
                    //         point_cloud_pcl->points[newPointIdxVector[i]].b = 0;
                    //     }
                    // }
                    
                    // viewer.showCloud (point_cloud_pcl);
                    // // publishPCL(point_cloud_pcl);                    
#endif


#if 0
        if (!prev_gray.empty() && prev_pts.size()) { 

            // Compute Homography of clusters  
            std::vector<uchar> status;
            std::vector<float> err;

            if (prev_pts.size()) { 
                
                // Mat prev_pts_mat = cv::Mat(prev_pts.size(), 2, CV_32FC1);
                // for (int j=0; j<prev_pts.size(); j++) { 
                //     prev_pts_mat.at<float>(j,0) = prev_pts[j].x;
                //     prev_pts_mat.at<float>(j,1) = prev_pts[j].y;
                // }

                // // Compute Median direction based on nearest neighbors
                // cv::flann::Index knn_index(prev_pts_mat, cv::flann::KDTreeIndexParams(4)); // using 4 randomized kdtrees

                // cv::Mat_<int> inds;
                // cv::Mat_<float> dists;
                // knn_index.knnSearch(prev_pts_mat, inds, dists, 7, cv::flann::SearchParams(64)); 
                
                // std::vector<bool> done(prev_pts.size(), false);
                // for (int j=0; j<prev_pts.size(); j++) { 
                //     if (done[j]) continue;

                //     // Find clusters
                //     std::vector<Point2f> cluster_pts; 
                //     std::vector<int> cluster_inds; 
                //     for (int k=0; k<inds.cols; k++) { 
                //         if (dists.at<float>(j,k) > 50)
                //             continue;
                        
                //         int idx = inds.at<int>(j,k);
                //         if (idx >= 0 && idx < prev_pts.size() && !done[idx]) { 
                //             cluster_inds.push_back(idx);
                //             cluster_pts.push_back(prev_pts[idx]);
                //         }
                //     }

                    

                //     // Perform RANSAC on the cluster
                //     if (cluster_pts.size()) { 
                //         findHomography(cluster_pts, 
                //     }
                        
                    
                }


                // for (int j=0; j<prev_pts.size(); j++) { 
                //     cv::Mat pt(1, 3, CV_32FC1);
                //     pt.at<float>(0,0) = prev_pts[j].x, pt.at<float>(0,1) = prev_pts[j].y;    
                //     knn_index.knnSearch(pt, inds, dists, 6, cv::flann::SearchParams(64)); // no: of leafs checked

                //     float NORM = 0;
                //     std::vector<float> angles;
                //     for (int k=0; k<inds.cols; k++) { // first one is the pt itself
                //         if (dists.at<float>(k) > 20) 
                //             continue;
                //         int idx = inds.at<int>(k); 
                        
                //         if (idx >= 0 && idx < prev_pts.size()) { 
                //             Point2f p = pts[idx] - prev_pts[idx];
                //             float norm = cv::norm(p);
                //             NORM += norm;
                //             p.x /= norm, p.y /= norm;
                //             angles.push_back(atan2(p.y, p.x));
                //         }
                //         std::sort(angles.begin(), angles.end());
                //     }
                //     if (angles.size())
                //         NORM /= angles.size() * 10;
                    
                //     float med_angle = angles[angles.size() / 2];
                //     line(color, prev_pts[j], Point2f(prev_pts[j].x + NORM * cos(med_angle), prev_pts[j].y + NORM * sin(med_angle)), 
                //          Scalar(0, 0, 255), 1);
                // }
            }
            


            // int k = 0;
            // framedata.track_ids = std::vector<int>(framedata.pts.size());
            // for( int i=0; i < pts.size(); i++ ) {
            //     // if( add_remove_pt ) {
            //     //     if( norm(point - points[1][i]) <= 5 ) {
            //     //         add_remove_pt = false;
            //     //         continue;
            //     //     }
            //     // }
            
            //     // if( !status[i] )
            //     //     continue;

            //     int track_id = prev_track_ids[i];
            //     // track_data.tracks[track_id].pts.push_back(pts[i]);

            //     framedata.track_ids[k] = track_id;
            //     pts[k] = pts[i];
            //     circle( color, pts[i], 2, colors[track_id%COLOR_COUNT], -1, 8);
            //     k++;
            // }
            // pts.resize(k);
            // framedata.track_ids.resize(k);
            
            // std::cerr << "PREV trackIDS: " << std::endl;
            // for (int j=0; j<prev_track_ids.size(); j++)
            //     std::cerr << " " << prev_track_ids[j] << std::endl;

            // std::cerr << "CURR trackIDS: " << std::endl;
            // for (int j=0; j<framedata.track_ids.size(); j++)
            //     std::cerr << " " << framedata.track_ids[j] << std::endl;
#endif
