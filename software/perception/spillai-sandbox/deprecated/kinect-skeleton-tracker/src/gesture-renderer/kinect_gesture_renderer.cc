#include "kinect_gesture_renderer.h"
#include <lcm/lcm.h>

#include <bot_core/bot_core.h>
#include <bot_vis/bot_vis.h>
#include <bot_frames/bot_frames.h>

//#include <lcmtypes/kinect_frame_msg_t.h>
#include <lcmtypes/kinect_point_list_t.h>
#include <lcmtypes/kinect_skeleton_msg_t.h>
#include <lcmtypes/kinect_link_msg_t.h>
#include <lcmtypes/kinect_frame_msg_t.h>

#include <kinect/kinect-utils.h>
#include <deque>
#include <iostream>
#include <limits>
#include <algorithm>
#include <map>

#define PARAM_NAME_CLOUD_SHOW "Show Cloud Updates"
#define PARAM_NAME_TRANSLUCENT_CLOUD "Translucent Point Cloud"
#define PARAM_NAME_TF_TRAIL "Show TF Trail"
#define PARAM_NAME_SHOW_KINECT_FRUSTUM "Show Kinect Frustum"
#define PI 3.141592

static const double fxtoz = 1.11147, fytoz = 0.8336;

std::deque<bot_core_rigid_transform_t*> tf_msgs;
typedef struct _KinectSkeletonRenderer {
    BotRenderer renderer;
    BotGtkParamWidget *pw;
    BotViewer   *viewer;
    lcm_t     *lcm;
    BotFrames *frames;
    char * kinect_frame;

    kinect_skeleton_msg_t* skeleton_msg;
    std::map<std::string, kinect_point_list_t*> point_list_msg;
    kinect_frame_msg_t* frame_msg;
    bot_core_rigid_transform_t* tf_msg;


    int width, height;

    bool show_cloud_updates;
    bool show_kinect_frustum;
    bool show_translucent_cloud;
    bool show_tf_trail;

    GLUquadricObj *quadratic;               // Storage For Our Quadratic Objects ( NEW )
    
} KinectSkeletonRenderer;

static void 
on_kinect_point_list (const lcm_recv_buf_t *rbuf, const char *channel,
        const kinect_point_list_t *point_list_msg, void *user_data )
{
    KinectSkeletonRenderer *self = (KinectSkeletonRenderer*) user_data;

    std::string ch_str(channel);
    // std::cerr << "channel: " << ch_str << std::endl;

    if (self->point_list_msg.find(ch_str) == self->point_list_msg.end()) { 
        self->point_list_msg[ch_str] = kinect_point_list_t_copy(point_list_msg);;
    } else { 
        kinect_point_list_t* msg = self->point_list_msg[ch_str];
        kinect_point_list_t_destroy(msg);
        self->point_list_msg.erase(ch_str);
        self->point_list_msg[ch_str] = kinect_point_list_t_copy(point_list_msg);
    }
    bot_viewer_request_redraw(self->viewer);

}

static void 
on_kinect_frame (const lcm_recv_buf_t *rbuf, const char *channel,
        const kinect_frame_msg_t *frame_msg, void *user_data )
{
    KinectSkeletonRenderer *self = (KinectSkeletonRenderer*) user_data;
    
    if(self->frame_msg)
        kinect_frame_msg_t_destroy(self->frame_msg);
    self->frame_msg = kinect_frame_msg_t_copy(frame_msg);

    bot_viewer_request_redraw(self->viewer);

}

static void 
on_rigid_transform (const lcm_recv_buf_t *rbuf, const char *channel,
		   const bot_core_rigid_transform_t *tf_msg, void *user_data )
{
    KinectSkeletonRenderer *self = (KinectSkeletonRenderer*) user_data;

    if(self->tf_msg)
        bot_core_rigid_transform_t_destroy(self->tf_msg);
    self->tf_msg = bot_core_rigid_transform_t_copy(tf_msg);

    // queued tfs
    if (tf_msgs.size() > 10) { 
        bot_core_rigid_transform_t_destroy(tf_msgs.back());
        tf_msgs.pop_back();
    }

    tf_msgs.push_front(bot_core_rigid_transform_t_copy(tf_msg));

    bot_viewer_request_redraw(self->viewer);
}

static void 
on_skeleton_frame (const lcm_recv_buf_t *rbuf, const char *channel,
		   const kinect_skeleton_msg_t *skeleton_msg, void *user_data )
{
    KinectSkeletonRenderer *self = (KinectSkeletonRenderer*) user_data;

    if(self->skeleton_msg)
        kinect_skeleton_msg_t_destroy(self->skeleton_msg);
    self->skeleton_msg = kinect_skeleton_msg_t_copy(skeleton_msg);

    bot_viewer_request_redraw(self->viewer);
}

static void on_param_widget_changed (BotGtkParamWidget *pw, const char *name, void *user)
{
    KinectSkeletonRenderer *self = (KinectSkeletonRenderer*) user;

    if (!&self->renderer)
    	return;
    
    //show cloud updates
    if (strcmp(name, PARAM_NAME_CLOUD_SHOW) == 0)
        self->show_cloud_updates = bot_gtk_param_widget_get_bool(self->pw, PARAM_NAME_CLOUD_SHOW);
    else if (strcmp(name, PARAM_NAME_TRANSLUCENT_CLOUD) == 0)
        self->show_translucent_cloud = bot_gtk_param_widget_get_bool(self->pw, PARAM_NAME_TRANSLUCENT_CLOUD);
    else if (strcmp(name, PARAM_NAME_TF_TRAIL) == 0)
        self->show_tf_trail = bot_gtk_param_widget_get_bool(self->pw, PARAM_NAME_TF_TRAIL);
    else if (strcmp(name, PARAM_NAME_SHOW_KINECT_FRUSTUM) == 0)
        self->show_kinect_frustum = bot_gtk_param_widget_get_bool(self->pw, PARAM_NAME_SHOW_KINECT_FRUSTUM);

    bot_viewer_request_redraw(self->viewer);
}

static inline void
_matrix_vector_multiply_3x4_4d (const double m[12], const double v[4],
        double result[3])
{
    result[0] = m[0]*v[0] + m[1]*v[1] + m[2] *v[2] + m[3] *v[3];
    result[1] = m[4]*v[0] + m[5]*v[1] + m[6] *v[2] + m[7] *v[3];
    result[2] = m[8]*v[0] + m[9]*v[1] + m[10]*v[2] + m[11]*v[3];
}

static inline void
_matrix_transpose_4x4d (const double m[16], double result[16])
{
    result[0] = m[0];
    result[1] = m[4];
    result[2] = m[8];
    result[3] = m[12];
    result[4] = m[1];
    result[5] = m[5];
    result[6] = m[9];
    result[7] = m[13];
    result[8] = m[2];
    result[9] = m[6];
    result[10] = m[10];
    result[11] = m[14];
    result[12] = m[3];
    result[13] = m[7];
    result[14] = m[11];
    result[15] = m[15];
}

static void 
draw_tf(double* axis_to_local_m, float size, float lineThickness, float opacity) {
  // double axis_to_local_m[16];
  // bot_trans_get_mat_4x4(axis_to_local, axis_to_local_m);

  // opengl expects column-major matrices
  double axis_to_local_m_opengl[16];
  bot_matrix_transpose_4x4d(axis_to_local_m, axis_to_local_m_opengl);

  glPushMatrix();
  // rotate and translate the vehicle
  glMultMatrixd(axis_to_local_m_opengl);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_DEPTH_TEST);

  glLineWidth(lineThickness);
  //x-axis
  glBegin(GL_LINES);
  glColor4f(1, 0, 0, opacity);
  glVertex3f(size, 0, 0);
  glVertex3f(0, 0, 0);
  glEnd();

  //y-axis
  glBegin(GL_LINES);
  glColor4f(0, 1, 0, opacity);
  glVertex3f(0, size, 0);
  glVertex3f(0, 0, 0);
  glEnd();

  //z-axis
  glBegin(GL_LINES);
  glColor4f(0, 0, 1, opacity);
  glVertex3f(0, 0, size);
  glVertex3f(0, 0, 0);
  glEnd();

  glDisable(GL_BLEND);
  glDisable(GL_DEPTH_TEST);

  glPopMatrix();

}

static void 
draw_contours(const kinect_point_list_t* msg, float lineThickness, float opacity) {
  if (msg->num_points <= 1)
      return;

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_DEPTH_TEST);

  glLineWidth(lineThickness);

  int j=1;
  float NaN = std::numeric_limits<float>::quiet_NaN();
  glBegin(GL_POINTS);
  do { 
      // if (msg->points[j].x != msg->points[j].x && 
      //     msg->points[j].y != msg->points[j].y && 
      //     msg->points[j].z != msg->points[j].z) { 
      if (msg->points[j].r < 0 && msg->points[j].g < 0 && msg->points[j].b < 0) { 
          glEnd();
          glBegin(GL_POINTS);
          j+=2; 
          continue;
      }

      glColor4f(msg->points[j-1].r / 255, msg->points[j-1].g / 255, msg->points[j-1].b / 255, opacity);
      glVertex3f(msg->points[j-1].x, msg->points[j-1].y, msg->points[j-1].z);
      //glVertex3f(msg->points[j].x, msg->points[j].y, msg->points[j].z);

      j++; 
  } while (j<msg->num_points);
  glEnd();

  glDisable(GL_BLEND);
  glDisable(GL_DEPTH_TEST);
  return;
}


// Draws an axis-aligned cube at a giveGL_FRONT_AND_BACKn location.
inline void draw_cube(float x, float y, float z, float width) {
    const float hs = 0.5 * width;
    ///*  
    glBegin( GL_TRIANGLE_STRIP );
    // front-left edge
    glVertex3f( x - hs, y + hs, z - hs );
    glVertex3f( x - hs, y - hs, z - hs );
      
    // front-right edge
    glVertex3f( x + hs, y + hs, z - hs );
    glVertex3f( x + hs, y - hs, z - hs );

    // back-right edge
    glVertex3f( x + hs, y + hs, z + hs );
    glVertex3f( x + hs, y - hs, z + hs );
    
    // back-left edge
    glVertex3f( x - hs, y + hs, z + hs );
    glVertex3f( x - hs, y - hs, z + hs );
      
    // front-left edge (again)
    glVertex3f( x - hs, y + hs, z - hs );
    glVertex3f( x - hs, y - hs, z - hs );
    glEnd();
    //*/  
    glBegin( GL_TRIANGLE_STRIP );
    // top-front edge
    glVertex3f( x - hs, y + hs, z - hs );
    glVertex3f( x + hs, y + hs, z - hs );
      
    // top-back edge
    glVertex3f( x - hs, y + hs, z + hs );
    glVertex3f( x + hs, y + hs, z + hs );
    glEnd();
    
    glBegin( GL_TRIANGLE_STRIP );
    // bottom-front edge
    glVertex3f( x - hs, y - hs, z - hs );
    glVertex3f( x + hs, y - hs, z - hs );
        
    // bottom-back edge
    glVertex3f( x - hs, y - hs, z + hs );
    glVertex3f( x + hs, y - hs, z + hs );
    glEnd();  
  
} 

// Draws a pyramid that funnels out from the origin along the z axis
// (i.e., its apex is at the origin, and the z axis is orthogonal to the base).
inline void drawPyramidZ(float vfov, float hfov, float height) {
    const float dxdz = sin( hfov * 0.5 );
    const float dydz = sin( vfov * 0.5 );
  
    const float top = dydz * height;
    const float left = dxdz * height;

    glColor4f(.8,.8,0,.2);  
    glBegin( GL_TRIANGLE_FAN );
    glVertex3f( 0, 0, 0 );
    glVertex3f( +top, +left, height );
    glVertex3f( +top, -left, height );
    glVertex3f( -top, -left, height );
    glVertex3f( -top, +left, height );
    glVertex3f( +top, +left, height );
    glEnd();

    glColor4f(.8,.8,0,1);
    glLineWidth(.2);
    glBegin( GL_LINE_LOOP );

    glVertex3f( +top, +left, height );
    glVertex3f( +top, -left, height );
    glVertex3f( -top, -left, height );
    glVertex3f( -top, +left, height );
    glVertex3f( +top, +left, height );

    glVertex3f( 0, 0, 0 );
    glVertex3f( +top, +left, height );

    glVertex3f( 0, 0, 0 );
    glVertex3f( +top, -left, height );

    glVertex3f( 0, 0, 0 );
    glVertex3f( -top, -left, height );

    glVertex3f( 0, 0, 0 );
    glVertex3f( -top, +left, height );
    glEnd();

}

static void _draw(BotViewer *viewer, BotRenderer *renderer)
{


    KinectSkeletonRenderer *self = (KinectSkeletonRenderer*) renderer->user;

    glPushMatrix();
    if (self->frames==NULL || !bot_frames_have_trans(self->frames, self->kinect_frame, 
                                                     bot_frames_get_root_name(self->frames))){
      // rotate so that X is forward and Z is up
      glRotatef(-90, 1, 0, 0);
    }
    else{
      //project to current frame
      double kinect_to_local_m[16];
      bot_frames_get_trans_mat_4x4_with_utime(self->frames, self->kinect_frame, 
                                              bot_frames_get_root_name(self->frames),
					      self->skeleton_msg->utime, kinect_to_local_m);

      // opengl expects column-major matrices
      double kinect_to_local_m_opengl[16];
      bot_matrix_transpose_4x4d(kinect_to_local_m, kinect_to_local_m_opengl);
      glMultMatrixd(kinect_to_local_m_opengl);
    }

    if (self->skeleton_msg) { 
        // Draw Skeleton along with point cloud
        glLineWidth(5);    
        glColor3f(.7, .1, .1);
        glBegin(GL_LINES);
        for (int i=0; i<self->skeleton_msg->num_links; i++) { 
            const kinect_link_msg_t& link = self->skeleton_msg->links[i];
            glVertex3f(link.source.x, -link.source.y, link.source.z);
            glVertex3f(link.dest.x, -link.dest.y, link.dest.z);
        }
        glEnd();
    }


    if (self->show_translucent_cloud) {  
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_DEPTH_TEST);
    }

    if (self->show_cloud_updates) { 

        // Draw Point cloud from kinect
        if (self->frame_msg) { 
            // Draw point cloud
            glEnable(GL_DEPTH_TEST);
            glBegin(GL_POINTS);
            const int step = 1;
            uint16_t* pdepth = (uint16_t*)self->frame_msg->depth.depth_data;
            unsigned char* prgb = self->frame_msg->image.image_data;
            for (int i=0; i<self->frame_msg->depth.height; i+=step, 
                     pdepth += self->frame_msg->depth.width*step, 
                     prgb += self->frame_msg->depth.width*3*step)
                for (int j=0; j<self->frame_msg->depth.width; j+=step) { 

                    if (self->show_translucent_cloud) { 
                        glColor4f( (int)prgb[3*j] * 1.f / 255, 
                                   (int)prgb[3*j + 1] * 1.f / 255, 
                                   (int)prgb[3*j + 2] * 1.f / 255, .7f );
                    } else { 
                        glColor3f( (int)prgb[3*j] * 1.f / 255, 
                                   (int)prgb[3*j + 1] * 1.f / 255, 
                                   (int)prgb[3*j + 2] * 1.f / 255 );
                    }

                    double depth = pdepth[j] / 1000.f;
                    double fnormx = (j * 1.f / self->frame_msg->depth.width - 0.5);
                    double fnormy = (0.5 - i * 1.f / self->frame_msg->depth.height);
            
                    glVertex3f(fnormx * fxtoz * depth, 
                               fnormy * fytoz * depth, 
                               depth);
                }
            glEnd();
        }
    }

    if (self->show_kinect_frustum) { 

        glPushMatrix();

        // Frustum
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // transparency
        glDisable(GL_LIGHTING);
        glDisable(GL_DEPTH_TEST);

        drawPyramidZ(PI/180 * 65, PI/180 * 50, 5);

        glDisable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);

        glLineWidth(2);
        glBegin(GL_LINES);
        glColor3f(1, 0, 0);
        glVertex3f(0, 0, 0);
        glVertex3f(.3, 0, 0);
    
        glColor3f(0, 1, 0);
        glVertex3f(0, 0, 0);
        glVertex3f(0, -.3, 0);
    
        glColor3f(0, 0, 1);
        glVertex3f(0, 0, 0);
        glVertex3f(0, 0, .3);
        glEnd();

        glPopMatrix();
    }

    // Draw Point Cloud from gesture tracker
    if (self->point_list_msg.size()) { 
        
        for (std::map<std::string, kinect_point_list_t*>::iterator it=self->point_list_msg.begin(); 
             it != self->point_list_msg.end(); it++) { 
            kinect_point_list_t* msg = it->second;
            std::string ch = it->first;
            if (!msg)
                continue;

            // Draw point cloud
            glEnable(GL_DEPTH_TEST);
            glBegin(GL_POINTS);
            for (int j=0; j<msg->num_points; j++) { 
                if (self->show_translucent_cloud) { 
                    glColor4f( msg->points[j].r / 255, 
                               msg->points[j].g / 255, 
                               msg->points[j].b / 255, .5 );
                } else { 
                    if (ch.find("_RED") != std::string::npos)
                        glColor3f( 0.6f, 0, 0);
                    else if (ch.find("_GREEN") != std::string::npos)
                        glColor3f( 0, 0.6f, 0);
                    else if (ch.find("_BLUE") != std::string::npos)
                        glColor3f( 0, 0, 0.6f);
                    else if (ch.find("_YELLOW") != std::string::npos)
                        glColor3f( 0.6f, 0.6f, 0);
                    else if (ch.find("_MAGENTA") != std::string::npos)
                        glColor3f( 0.6f, 0, 0.6f);
                    else if (ch.find("_CYAN") != std::string::npos)
                        glColor3f( 0, 0.6f, 0.6f);
                    else 
                        glColor3f( msg->points[j].r / 255, 
                                   msg->points[j].g / 255, 
                                   msg->points[j].b / 255 );
                }

                glVertex3f(msg->points[j].x, 
                           msg->points[j].y, 
                           msg->points[j].z);
            }
            glEnd();
            

        }

    }


    if (self->show_translucent_cloud) { 
        glDisable(GL_BLEND);
        glDisable(GL_DEPTH_TEST);
    }
    
    int max_tfs = (self->show_tf_trail) ? tf_msgs.size() : std::min(1, (int)tf_msgs.size());
    // Draw Transforms
    for (int j=0; j<max_tfs; j++) { 
        if (!tf_msgs[j])
            continue;
        double tf[16];
        bot_quat_pos_to_matrix(tf_msgs[j]->quat, tf_msgs[j]->trans, tf);
        draw_tf(tf, .1, 3, 1.f - j * 1.f/tf_msgs.size());
    }

    glPopMatrix();

  return;
}

static void _free(BotRenderer *renderer)
{
    KinectSkeletonRenderer *self = (KinectSkeletonRenderer*) renderer;

    for (std::map<std::string, kinect_point_list_t*>::iterator it=self->point_list_msg.begin(); 
         it != self->point_list_msg.end(); it++) { 
        if (it->second) { 
            kinect_point_list_t* msg = it->second;
            kinect_point_list_t_destroy(msg);
        }
    }

    if(self->skeleton_msg)
        kinect_skeleton_msg_t_destroy(self->skeleton_msg);

    if(self->frame_msg)
        kinect_frame_msg_t_destroy(self->frame_msg);

    if(self->kinect_frame)
        free(self->kinect_frame);

    for (int j=0; j<tf_msgs.size(); j++)
        bot_core_rigid_transform_t_destroy(tf_msgs[j]);

    free(self);
}

void 
kinect_add_gesture_renderer_to_viewer(BotViewer* viewer, int priority, lcm_t* lcm, BotFrames * frames, const char * kinect_frame)
{
    KinectSkeletonRenderer *self = new KinectSkeletonRenderer();

    self->frames = frames;
    if (self->frames!=NULL)
      self->kinect_frame = strdup(kinect_frame);
    else 
        self->kinect_frame = "KINECT";

    self->skeleton_msg = NULL;
    self->tf_msg = NULL;

    //const int tf_history_size = 10;
    //tf_msgs = std::deque<bot_core_rigid_transform_t*>() ;

    BotRenderer *renderer = &self->renderer;

    self->lcm = lcm;
    self->viewer = viewer;
    self->pw = BOT_GTK_PARAM_WIDGET(bot_gtk_param_widget_new());
    self->show_cloud_updates = false;
    self->show_kinect_frustum = false;
    self->show_translucent_cloud = false;
    self->show_tf_trail = false;

    renderer->draw = _draw;
    renderer->destroy = _free;
    renderer->name = "Skeleton Renderer";
    renderer->widget = GTK_WIDGET(self->pw);
    renderer->enabled = 1;
    renderer->user = self;

    bot_gtk_param_widget_add_booleans(self->pw, 
                                      BOT_GTK_PARAM_WIDGET_CHECKBOX, 
                                      PARAM_NAME_CLOUD_SHOW, 0, NULL);
    bot_gtk_param_widget_add_booleans(self->pw, 
                                      BOT_GTK_PARAM_WIDGET_CHECKBOX, 
                                      PARAM_NAME_TRANSLUCENT_CLOUD, 0, NULL);
    bot_gtk_param_widget_add_booleans(self->pw, 
                                      BOT_GTK_PARAM_WIDGET_CHECKBOX, 
                                      PARAM_NAME_TF_TRAIL, 0, NULL);
    bot_gtk_param_widget_add_booleans(self->pw, 
                                      BOT_GTK_PARAM_WIDGET_CHECKBOX, 
                                      PARAM_NAME_SHOW_KINECT_FRUSTUM, 0, NULL);

    g_signal_connect (G_OBJECT (self->pw), "changed",
                      G_CALLBACK (on_param_widget_changed), self);

    bot_core_rigid_transform_t_subscribe (lcm, "GLOBAL_TO_LOCAL", on_rigid_transform, self);
    kinect_skeleton_msg_t_subscribe(self->lcm, "KINECT_SKELETON_FRAME", on_skeleton_frame, self);
    kinect_frame_msg_t_subscribe(self->lcm, "KINECT_FRAME", on_kinect_frame, self);
    kinect_point_list_t_subscribe(self->lcm, "KINECT_POINT_CLOUD_.*", on_kinect_point_list, self);

    bot_viewer_add_renderer(viewer, renderer, priority);
}
