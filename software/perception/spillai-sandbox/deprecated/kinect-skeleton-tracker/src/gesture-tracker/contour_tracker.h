#ifndef CONTOURTRACKER_H_
#define CONTOURTRACKER_H_

#include "kinect_opencv_utils.h"

// PCL
#include <pcl/io/pcd_io.h>
#include <pcl/PointIndices.h>
#include <pcl/registration/icp.h>

// LCM includes
#include <lcm/lcm.h>
#include <sys/time.h>
#include <unistd.h>

#define PI 3.141592
#define SCALE 2
#define BIN_SIZE 16
#define BIN_DEV 4
#define USE_SAT 0

static inline std::pair<int, int>
    create_pair(const int p1, const int p2) {
    if (p1 > p2)
        return std::make_pair(p2, p1);
    else
        return std::make_pair(p1, p2);
}

// ==================== contour_segment =====================
// ==========================================================

static std::vector<cv::Scalar> id_colors;
static std::map< std::pair< int, int > , int > id_colors_map;

struct contour_segment {
    //static const int poff = 5; // 5px

    std::vector<cv::Point> points;
    int b1, b2, s1, s2, b, d1, d2, B;
    cv::Point p1, p2, P;
    cv::Point2f nvec, mp, vec;
    int id, rep;

    float depth1, depth2;
    bool obj_border;
    
contour_segment()
: id(-1), b1(-1), b2(-1), s1(-1), s2(-1), d1(-1), d2(-1), b(-1), rep(-1), B(-1), depth1(0), depth2(0) {}

contour_segment(kinect_data* kinect, const std::vector<cv::Point>& contour, int first_idx, int length, int poff)
: id(-1), b1(-1), b2(-1), s1(-1), s2(-1), d1(-1), d2(-1), b(-1), rep(-1), B(-1), depth1(0), depth2(0), obj_border(false) {
        if (contour.size() < first_idx + length + 1)
            return;
        
        points.insert(points.end(), contour.begin() + first_idx, contour.begin() + first_idx + length);
        id = determine_id(kinect, poff);

        //assert( (id >=0 && id < id_colors.size()) || (id == -1));
    }

    /* contour_segment(const Mat1b& hue, const std::vector<Point>& contour_seg, int poff) {  */
    /*     points.insert(points.end(), contour_seg.begin(), contour_seg.end()); */
    /*     id = determine_id(hue, poff); */
    /* } */

    int determine_id(kinect_data* kinect, int poff) {
        if (points.size() < 2)
            return -1;

        const cv::Mat3b& hls = kinect->getHLS();

        vec = cv::Point2f (points.end()[-1].x - points[0].x, points.end()[-1].y - points[0].y);
        float norm = sqrt(vec.x * vec.x + vec.y * vec.y);
        vec.x /= norm, vec.y /= norm;

        nvec = cv::Point2f (vec.y, -vec.x);
        mp = cv::Point2f( ( points.end()[-1].x + points[0].x ) / 2, ( points.end()[-1].y + points[0].y ) / 2);
        
        p1 = cv::Point( ceil(mp.x + nvec.x * poff), ceil(mp.y + nvec.y * poff));
        p2 = cv::Point( ceil(mp.x - nvec.x * poff), ceil(mp.y - nvec.y * poff));

        if (p1.y < 0 || p1.y >= hls.rows || p2.y < 0 || p2.y >= hls.rows ||
            p1.x < 0 || p1.x >= hls.cols || p2.x < 0 || p2.x >= hls.cols)
            return -1;
        
        // Split into 6 bins
        // Hue ranges from 0 to 180 (so as to fit within 255)
        b1 = (int)( hls(p1.y, p1.x)[0] * 1.f / ( 180.f / (BIN_SIZE * 1.f ) ));
        b2 = (int)( hls(p2.y, p2.x)[0] * 1.f / ( 180.f / (BIN_SIZE * 1.f ) ));
        if (b1 == BIN_SIZE) b1 = 0;
        if (b2 == BIN_SIZE) b2 = 0;
            
#if USE_SAT
        s1 = hls(p1.y, p1.x)[1] > 150 ? 1 : 0;
        s2 = hls(p2.y, p2.x)[1] > 150 ? 1 : 0;
#else
        s1 = 0;
        s2 = 0;
#endif

        depth1 = kinect->getDepth(p1.y, p1.x);
        depth2 = kinect->getDepth(p2.y, p2.x);

        //std::cerr << "d1: " << depth1 << " 2:" << depth2 << std::endl;

        // object border if depth difference is more than 4% of depth (assuming error is linear with depth)
        obj_border = (depth1 == 0 && depth2 != 0) ||
            (depth1 != 0 && depth2 == 0) ||
            (depth1 == 0 && depth2 == 0) ||
            (fabs(depth1-depth2) >= (depth1 + depth2)/2 * .04);

        assert(!(b1 < 0 || b1 >= BIN_SIZE || b2 < 0 || b2 >= BIN_SIZE));
        if (b1 < 0 || b1 >= BIN_SIZE ||
            b2 < 0 || b2 >= BIN_SIZE)
            return -1;

#if USE_SAT
        // Don't care about similar colors
        if ((b1 == b2) && (s1 == s2))
            return -1;
        d1 = b1*2+s1, d2 = b2*2+s2;
#else
        if (b1 == b2)
            return -1;
        d1 = b1, d2 = b2;
#endif

        std::pair<int, int> p = create_pair(d1,d2);
        //std::pair<int, int> p = create_pair(b1, b2);
        b = p.first;

        if (fabs(depth1 - depth2) < 0.1)
            return 0;
        else 
            return BIN_SIZE/2;

        //assert(id_colors_map.find(p) != id_colors_map.end());
        return 1; //id_colors_map[p];
    }

    bool isGood() const { return (id >= 0); }
    bool isObjectBorder() const { return obj_border; }

};
typedef std::vector< contour_segment > contour_segments;

// ========================================================== // 

// ==================== Contour Tracker =====================
// ==========================================================

class ContourTracker { 
 private: 
    kinect_data* kinect;
    std::vector< std::vector<float> > contour_hists;
    std::vector<float> contour_hist;
 public:
    ContourTracker(); 
    ~ContourTracker();
    void useKinectData(kinect_data* pkinect) { kinect = pkinect; }
    void initialize(cv::Mat1b& canny, const cv::Mat1b& gray_blur, const cv::Rect& sel);
    //void initialize(const cv::Rect& sel);
    void detectContours(pcl::PointCloud<pcl::PointXYZRGB>::Ptr contour_pcl,
                        const cv::Rect& sel);    
    void connectedComponents(pcl::PointCloud<pcl::PointXYZRGB>::Ptr contour_pcl);    
    void extractContourFeatures(const std::vector< std::vector <cv::Point> >& contours);
    bool buildContourSegments(std::vector< contour_segments >& contour_segments_list,
                              const std::vector< std::vector <cv::Point> >& contours);
    void findConnectedComponents(std::vector< contour_segments >& contour_segments_list);
    void matchStats(const std::vector< contour_segments >& contour_segments_list);
    std::vector<float> buildContourHistogram(const std::vector< contour_segments>& contour_segments_list);

    void pruneEdgesWithDepth(cv::Mat1b& mask, const cv::Rect& sel, float threshold = 0.1);
    void plotContourSegments(const std::vector< contour_segments >& contour_segments_list);
    void plotStats(const std::vector< contour_segments>& contour_segments_list);

    bool selectObject;
    int trackObject;
    cv::Rect selection;
    cv::Point origin;    
    cv::Mat image;

    int low_thresh;
    int high_thresh;
    int contour_thresh;
    int clength;
    int poff;
    int temp_avg_frames;
    bool contour_train;

    // Histogram related
    cv::Mat id_colors;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr canonical_obj_pcl;

void buildColoredIDs() {

    id_colors = cv::Mat(1, BIN_SIZE, CV_8UC3);
    for( int i = 0; i < BIN_SIZE; i++ )
        id_colors.at<cv::Vec3b>(i) = cv::Vec3b(cv::saturate_cast<uchar>(i*180./BIN_SIZE), 128, 255);
    cv::cvtColor(id_colors, id_colors, CV_HLS2BGR);

    drawIDMap();
    std::cerr << "Total BINS: " << id_colors.cols << std::endl;
}

 void drawIDMap() { 
     int bW = 50;
     cv::Mat mapimg = cv::Mat::zeros(bW, bW*id_colors.cols, CV_8UC3);

     for (int i=0; i<id_colors.cols; i++) { 
         cv::Point p11(i*bW,0), p12((i+1)*bW,bW);

         // Draw upper half of the color map
         cv::rectangle( mapimg,p11,p12,cv::Scalar(id_colors.at<cv::Vec3b>(i)), -1, 8 );
     }

     cv::imshow("map", mapimg);
 }
};
// ========================================================== //


/* static void buildColoredIDsMap() { */
/*     if (!id_colors.size()) { */
/*         std::cerr << "ID COLORS NOT BUILT" << std::endl; */
/*         return; */
/*     } */

/* #if USE_SAT */
/*     for (int bin1=0, id=0; bin1 < BIN_SIZE; bin1++) { */
/*         for (int bin2=0; bin2 < BIN_SIZE; bin2++) { */
/*             for (int s1=0; s1<2; s1++) { */
/*                 for (int s2=0; s2<2; s2++) { */
/*                     if ((bin1 == bin2) && (s1 == s2)) */
/*                         continue; */

/*                     std::pair<int, int> p = create_pair(bin1*2+s1, bin2*2+s2); */
/*                     if (id_colors_map.find(p) == id_colors_map.end()) */
/*                         id_colors_map.insert(make_pair(p, id++)); */
/*                 } */
/*             } */
/*         } */
/*     } */
/* #else */
/*     for (int bin1=0, id=0; bin1 < BIN_SIZE; bin1++) { */
/*         for (int bin2=0; bin2 < BIN_SIZE; bin2++) { */
/*             if (bin1 == bin2) */
/*                 continue; */
/*             std::pair<int, int> p = create_pair(bin1, bin2); */
/*             if (id_colors_map.find(p) == id_colors_map.end()) */
/*                 id_colors_map.insert(make_pair(p, id++)); */
/*         } */
/*     } */
/* #endif */
        
/*     std::cerr << "Total Hue Bins: " << BIN_SIZE << std::endl; */
/* #if USE_SAT */
/*     std::cerr << "Total Sat Bins: " << 2 << std::endl; */
/* #else */
/*     std::cerr << "Total Sat Bins: " << 0 << std::endl; */
/* #endif */
/*     std::cerr << "Total ID Maps: " << id_colors_map.size() << std::endl; */

/* #if USE_SAT */
/*     cv::Mat buf1 = cv::Mat(1, BIN_SIZE*2, CV_8UC3); */
/*     for( int i = 0; i < BIN_SIZE; i++ ) */
/*         for (int s=0; s < 2; s++) */
/*             buf1.at<Vec3b>(i*2+s) = Vec3b(saturate_cast<uchar>(i*180./BIN_SIZE), s == 0 ? 128 : 255, 255); */
/*     cvtColor(buf1, buf1, CV_HSV2BGR); */
/* #else */
/*     cv::Mat buf1 = cv::Mat(1, BIN_SIZE, CV_8UC3); */
/*     for( int i = 0; i < BIN_SIZE; i++ ) */
/*         buf1.at<cv::Vec3b>(i) = cv::Vec3b(cv::saturate_cast<uchar>(i*180./BIN_SIZE), 255, 255); */
/*     cvtColor(buf1, buf1, CV_HSV2BGR); */
/* #endif */


/* #if USE_SAT */
/*     int bW = 20; */
/* #else */
/*     int bW = 50; */
/* #endif */
/*     cv::Mat mapimg = cv::Mat::zeros(bW*2, bW*id_colors_map.size(), CV_8UC3); */
        

/*     // Draw colors first */
/*     int i=0; */
/*     for (std::map<std::pair<int, int>, int>::iterator it=id_colors_map.begin(); */
/*          it!=id_colors_map.end(); it++) { */

/*         cv::Point p11(i*bW,0), p12((i+1)*bW,bW); */
/*         cv::Point p21(i*bW,bW), p22((i+1)*bW,2*bW); */

/*         cv::Point p1( (p11.x+p12.x) / 2, (p11.y+p12.y) / 2); */
/*         cv::Point p2( (p21.x+p22.x) / 2, (p21.y+p22.y) / 2); */

/*         // Draw upper half of the color map */
/*         rectangle( mapimg,p11,p12,cv::Scalar(buf1.at<cv::Vec3b>(it->first.first)), -1, 8 ); */

/*         // Draw lower half of the color map */
/*         rectangle( mapimg,p21,p22,cv::Scalar(buf1.at<cv::Vec3b>(it->first.second)), -1, 8 ); */

/*         // Draw colored line */
/*         line(mapimg,p1,p2,id_colors[it->second], 2); */

/*         i++; */
/*     } */

/*     // Draw text second */
/*     i=0; */
/*     for (std::map<std::pair<int, int>, int>::iterator it=id_colors_map.begin(); */
/*          it!=id_colors_map.end(); it++) { */

/*         cv::Point p11(i*bW,0), p12((i+1)*bW,bW); */
/*         cv::Point p21(i*bW,bW), p22((i+1)*bW,2*bW); */

/*         cv::Point p1( (p11.x+p12.x) / 2, (p11.y+p12.y) / 2); */
/*         cv::Point p2( (p21.x+p22.x) / 2, (p21.y+p22.y) / 2); */

/*         // Write out the ID of the color */
/*         cv::putText(mapimg, */
/*                     cv::format("%i", it->first.first), */
/*                     cv::Point(p1.x,p1.y - bW/5), 0, 0.35, cv::Scalar(0,0,0,255),1); */

            
/*         // Write out the ID of the color */
/*         cv::putText(mapimg, */
/*                     cv::format("%i", it->first.second), */
/*                     cv::Point(p2.x,p2.y + bW/5), 0, 0.35, cv::Scalar(0,0,0,255),1); */

/*         // Write out the ID of the line */
/*         cv::putText(mapimg, */
/*                     cv::format("%i", it->second), */
/*                     cv::Point((p1.x+p2.x)/2,(p1.y+p2.y)/2), 0, 0.35, cv::Scalar(0,0,0,255),1); */

/*         i++; */
/*     } */

/*     imshow("map", mapimg); */
/* } */


#endif /* CONTOURTRACKER_H_ */
