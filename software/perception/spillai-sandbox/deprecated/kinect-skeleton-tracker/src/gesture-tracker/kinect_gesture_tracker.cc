// Ideas : 
// - Canny edge/Water shed detection on depth image
// - Use pointed region to build histogram (hit space for confirming)
// - Segment object in 3D

// ===================================================================
/*

- Use DOT (Dominant Orientation Templates for Real-Time Detection of Texture-Less Objects
- Multimodal approach - Mutimodal templates fro real-time detection of texture-less 
  objects in heavily cluttered scenes
- May be integrate a hybrid tracker on top of DOT
- i.e. DOT running in one thread (possibly using 3d data, refer to 
  www.ros.org/presentations/2010-06-holzer-dot.pdf)
- HybridTracker running on a separate thread with updates for the object
  given every few frames (updates can run at 10fps; should work fine)


http://campar.in.tum.de/Main/StefanHinterstoisser
Binary gradient grid (BiGG) - Bradski similar to DOT

http://www.ros.org/wiki/fast_template_detector
larks
*/

// LCM includes
#include <kinect-skeleton-tracker/kinect_skeleton_publisher_openni.h>
#include "hist_tracker.h"
#include "contour_tracker.h"
#include "dotdetect.h"


#include "hybridtracker.hpp"
//#include "tld/TLD.h"

#include <pcl/keypoints/sift_keypoint.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <lcmtypes/kinect_image_msg_t.h>

#include <pthread.h>
#include <bot_param/param_client.h>
#define SHOW_ALL_RECTS_BY_ONE 0

using namespace cv;
//using namespace tld;

Scalar color_tab_[] =
    {
        Scalar(0, 0, 255),
        Scalar(0,255,0),
        Scalar(255,100,100),
        Scalar(255,0,255),
        Scalar(0,255,255)
    };

int img_count = 0;
struct MouseEvent
{
    MouseEvent() { event = -1; buttonState = 0; }
    Point pt;
    int event;
    int buttonState;
};
MouseEvent mouse;

kinect_image_msg_t projector_image_msg; 

Rect selection;
Point origin;
bool selectObject = false;
int trackObject = 0;
bool initLearning = false;


#define PI 3.141592

static const uint8_t deci_r = 4;

static const uint16_t nvert = HEIGHT / deci_r;
static const uint16_t nhoriz = WIDTH / deci_r;
static const uint32_t npixels = WIDTH*HEIGHT;
static const uint32_t nnodes = nvert*nhoriz;

const double fxtoz = 1.11147, fytoz = 0.8336;
double projector_to_camera_rvec[3] = { 0, 0, 0 } ; 
double projector_to_camera_tvec[3] = { 0, 0, 0 }; 
double projector_intrinsics[5] = { 0, 0, 0, 0, 0 }; 
double projector_distortion[5] = { 0, 0, 0, 0, 0 }; 
double projector_image_width = 0; 
double projector_image_height = 0; 
Rect proj_selection(0,0,1,1);

// Map skeleton joint to location
// std::map<uint32_t, std::pair<Point2f, Point2f> > joint_p2;
// cv::Point2f le, lh, lvec, re, rh, rvec, ee, evec;

bool done = false;

// Last Point cloud
pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud_pcl, canonical_obj_pcl;
Point3f mean_canonical_obj_pcl;
Eigen::Matrix4f updated_tf = Eigen::Matrix4f::Identity();

kinect_data kinect;

skeleton_info skel_info;

// Tracker
HistTracker tracker;
ContourTracker ctracker;
HybridTracker* hdtracker = NULL;
//TLD* tldtracker;

// // 1. Train detector
DOTDetector dotDetector;
DOTDetector::TrainParams trainParams;
DOTDetector::DetectParams detectParams;

HybridTrackerParams params;


Mat_<float> projector_K, projector_D, cam2proj_rvec, cam2proj_tvec, cam_rvec, cam_tvec;

// LCM related
// lcm_t* lcm = NULL;

/*////////////////////////////////////////////////////////////////////
  Dot detector 
/////////////////////////////////////////////////////////////////////*/

// #define SHOW_ALL_RECTS_BY_ONE 0

static void fillColors( vector<Scalar>& colors )
{
    cv::RNG rng = theRNG();

    for( size_t ci = 0; ci < colors.size(); ci++ )
        colors[ci] = Scalar( rng(256), rng(256), rng(256) );
}

static void usage(const char* progname)
{
  fprintf (stderr, "Usage: %s [options]\n"
                   "\n"
                   "Options:\n"
                   "  -l URL    Specify LCM URL\n"
                   "  -h        This help message\n", 
                   g_path_get_basename(progname));
  exit(1);
}

static bool depth_pcl_sort(const std::pair<float, pcl::PointXYZRGB>& lhs, 
                           const std::pair<float, pcl::PointXYZRGB>& rhs) { 
    return (lhs.first < rhs.first);
}

void prunePCLWithDepth(pcl::PointCloud<pcl::PointXYZRGB>::Ptr& pcl, float threshold = 0.1) { 

    if (!pcl->points.size())
        return;
    std::vector<std::pair<float, pcl::PointXYZRGB> > depths;
    for (int j=0; j<pcl->points.size(); j++) { 
        depths.push_back(std::make_pair(pcl->points[j].z, pcl->points[j]));
    }

    if (!depths.size())
        return;

    size_t n = depths.size() / 2;
    std::nth_element(depths.begin(), depths.begin() + n, depths.end(), depth_pcl_sort);

    pcl->points.clear();
    float depthm = depths[n].first;
    for (int j=0; j<depths.size(); j++)
        if (fabs(depthm-depths[j].first) < threshold)
            pcl->points.push_back(depths[j].second);

    pcl->width = pcl->points.size();
    pcl->height = 1;
    return;
}

void populatePCL(pcl::PointCloud<pcl::PointXYZRGB>::Ptr& pcl){
    pcl->width = nhoriz;
    pcl->height = nvert;
    pcl->points.resize(nvert*nhoriz);

    //bool user_label_mask = (kinect_info.user_labels_img_debug.data != 0);

    // Convert to XYZ
    int i, j, idx, f_idx, c_idx, u, v;
    //double* disparity_map = kinect_info.disparity_map;
    for (i = 0; i < nvert; i++) {
        for(j = 0; j < nhoriz; j++) {
            idx = i*nhoriz + j;
            v = i*deci_r, u = j*deci_r;
            //f_idx = v*WIDTH+u;

            uint8_t r, g, b;
            //c_idx = v*WIDTH*3 + u*3;
            // if (user_label_mask && kinect_info.user_labels_img.data[v*WIDTH + u]) { 
            //     r = kinect_info.user_labels_img_debug.data[c_idx + 2];
            //     g = kinect_info.user_labels_img_debug.data[c_idx + 1];
            //     b = kinect_info.user_labels_img_debug.data[c_idx + 0];
            // } else { 
            Vec3b s = kinect.getScalarRGB(v, u);
            r = s[2];
            g = s[1];
            b = s[0];
                //            }
            
            Vec3f xyz = kinect.getXYZ(v,u);
            pcl->points[idx].x = xyz[0], pcl->points[idx].y = xyz[1], pcl->points[idx].z = xyz[2];
            pcl->points[idx].r = r, pcl->points[idx].g = g, pcl->points[idx].b = b;
        }
    }

    return;
} 


void extractPCL(pcl::PointCloud<pcl::PointXYZRGB>::Ptr& object_pcl, const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& pcl, const Mat1b& _belief, float median_depth = 0) {

    if (!_belief.data || !pcl->points.size())
        return;

    Mat1b belief;
    if (belief.rows != nvert || belief.cols != nhoriz) 
        resize(_belief, belief, Size(nhoriz,nvert));
    else
        belief = _belief;

    int idx, u, v, k=0;
    const uchar* pb = belief.data;
    for (int i=0; i<nvert; i++) { 
        for (int j=0; j<nhoriz; j++) { 
            idx = i*nhoriz + j;
            if (pb[idx]) { 
                if (median_depth == 0)
                    object_pcl->points.push_back(pcl->points[idx]);
                else {  
                    if (fabs(pcl->points[idx].z-median_depth) < .1)
                        object_pcl->points.push_back(pcl->points[idx]);
                }
            }
        }
    }

    object_pcl->width = object_pcl->points.size(); 
    object_pcl->height = 1;

    return;                
}

void publishPCL(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& pcl) { 
    if (!pcl->points.size())
        return;

    kinect_point_list_t point_cloud;
    point_cloud.points = (kinect_point_t*) malloc(sizeof(kinect_point_t) * pcl->points.size());
    memset(point_cloud.points, 0, sizeof(kinect_point_t) * pcl->points.size());
    point_cloud.num_points = pcl->points.size();

    kinect_point_t* points = point_cloud.points;
    int idx=0, f_idx=0;
    for (int j=0; j<pcl->points.size(); j++) {
            points[j].x = pcl->points[j].x;
            points[j].y = pcl->points[j].y;
            points[j].z = pcl->points[j].z;
            points[j].r = pcl->points[j].r;
            points[j].g = pcl->points[j].g;
            points[j].b = pcl->points[j].b;
    }

    kinect_point_list_t_publish(kinect.lcm, "KINECT_POINT_CLOUD", &point_cloud);
    free(point_cloud.points);
}

inline Vec3b color_picker(int i) { 
    switch (i) { 
    case 0:
        return Vec3b(155, 0, 0);
    case 1:
        return Vec3b(0, 155, 0);
    case 2:
        return Vec3b(0, 0, 155);
    case 3:
        return Vec3b(155, 155, 0);
    case 4:
        return Vec3b(0, 155, 155);
    case 5:
        return Vec3b(155, 0, 155);
    default: 
        return Vec3b(155, 0, 0);
    }
}

void publishPCL(const std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr>& pcls) { 

    int num_points = 0;
    for (int i=0; i<pcls.size(); i++) 
        num_points += pcls[i]->size();

    if (!num_points)
        return;

    kinect_point_list_t point_cloud;
    point_cloud.num_points = num_points;
    point_cloud.points = (kinect_point_t*) malloc(sizeof(kinect_point_t) * num_points );
    memset(point_cloud.points, 0, sizeof(kinect_point_t) * num_points );

    kinect_point_t* points = point_cloud.points;
    for (int i=0, idx=0; i<pcls.size(); i++) { 
        const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& pcli = pcls[i];

        Vec3b c = color_picker(i);
        for (int j=0; j<pcli->points.size(); j++, idx++) {
            points[idx].x = pcli->points[j].x;
            points[idx].y = pcli->points[j].y;
            points[idx].z = pcli->points[j].z;

            points[idx].r = c[0];
            points[idx].g = c[1];
            points[idx].b = c[2];
        }
    }

    kinect_point_list_t_publish(kinect.lcm, "KINECT_POINT_CLOUD", &point_cloud);
    free(point_cloud.points);
}

void computeICP(Eigen::Matrix4f& icp_tf, Eigen::Matrix4f& guess_tf, 
                pcl::PointCloud<pcl::PointXYZRGB>::Ptr& final, 
                pcl::PointCloud<pcl::PointXYZRGB>::Ptr& live , 
                pcl::PointCloud<pcl::PointXYZRGB>::Ptr& ref) { 

    if (!ref->points.size() || !live->points.size())
        return;

    pcl::IterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;
    //icp.setMaximumIterations (10);
    icp.setTransformationEpsilon (1e-5);
    //icp.setMaxCorrespondenceDistance (0.05);
    //std::cerr << "Ref: " << ref->points.size() << std::endl;
    icp.setInputCloud(ref);
    icp.setInputTarget(live);

    double t1 = timestamp_us();
    icp.align(*final, guess_tf); 
    double t2 = timestamp_us();

    icp_tf = icp.getFinalTransformation();
    //std::cout << icp_tf << std::endl;
    //std::cout << "TF eps: " << icp.getTransformationEpsilon () << std::endl;    
    //std::cout << "fitness eps: " << icp.getEuclideanFitnessEpsilon () << std::endl;    
    //std::cout << "fitness score: " << icp.getFitnessScore () << std::endl;    

    if (!icp.hasConverged())
        return;

    // Update step
    guess_tf = icp_tf;

    Eigen::Matrix4f tf_g_to_can = Eigen::Matrix4f::Identity();
    tf_g_to_can(0,3) = mean_canonical_obj_pcl.x;
    tf_g_to_can(1,3) = mean_canonical_obj_pcl.y;
    tf_g_to_can(2,3) = mean_canonical_obj_pcl.z;

    Eigen::Matrix4f tf_g_to_live = icp_tf * tf_g_to_can;

    double rot[9];
    rot[0] = tf_g_to_live(0,0), rot[1] = tf_g_to_live(0,1), rot[2] = tf_g_to_live(0,2), 
        rot[3] = tf_g_to_live(1,0), rot[4] = tf_g_to_live(1,1), rot[5] = tf_g_to_live(1,2), 
        rot[6] = tf_g_to_live(2,0), rot[7] = tf_g_to_live(2,1), rot[8] = tf_g_to_live(2,2);

    bot_core_rigid_transform_t tf;
    bot_matrix_to_quat(rot, tf.quat);
    tf.utime = kinect.timestamp;
    tf.trans[0] = tf_g_to_live(0,3);
    tf.trans[1] = tf_g_to_live(1,3);
    tf.trans[2] = tf_g_to_live(2,3);
    bot_core_rigid_transform_t_publish (kinect.lcm, "GLOBAL_TO_LOCAL", &tf);

    printf("icp: %f milliseconds\n", (t2 - t1)*1e-3);

    return;

}

inline float l2_dist(const pcl::PointXYZ& p1, const pcl::PointXYZ& p2) { 
    return sqrt( (p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y) + (p1.z-p2.z)*(p1.z-p2.z) );
}

static bool sort_source_depth(const std::pair<Point3f, Point3f>& lhs, 
                              const std::pair<Point3f, Point3f>& rhs) { 
    return (lhs.first.z < rhs.first.z);
}

void transformationEstimation(std::vector<std::pair<Point3f, Point3f> >& corrs) { 
    if (!corrs.size()) { 
        //std::cerr << " No Corrs: " << std::endl;
        return;
    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr source (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr target (new pcl::PointCloud<pcl::PointXYZ>);

    boost::shared_ptr<pcl::Correspondences> correspondences (new pcl::Correspondences);
    //boost::shared_ptr<pcl::Correspondences> correspondences_result_rej_sac (new pcl::Correspondences);

    // Sort correspondences based on their depth
    // Prune out ones that are not enclosed within the object
    // Assuming the features found on the object have the same depth
    std::sort(corrs.begin(), corrs.end(), sort_source_depth);

    size_t n = corrs.size() / 2;
    std::nth_element(corrs.begin(), corrs.begin() + n, corrs.end(), sort_source_depth);

    int idx = 0;
    float mpz = corrs[n].first.z;
    float cx = 0, cy = 0, cz = 0;
    for (std::vector<std::pair<Point3f,Point3f> >::iterator it = corrs.begin(); 
         it!=corrs.end(); ) { 
        if (fabs(mpz-it->first.z) > 0.1) // 10 cm
            it = corrs.erase(it);
        else { 
            cx += it->first.x, cy += it->first.y, cz += it->first.z;
            
            pcl::PointXYZ ps(it->first.x, it->first.y, it->first.z);
            pcl::PointXYZ pt(it->second.x, it->second.y, it->second.z);
            
            source->points.push_back(ps);
            target->points.push_back(pt);

            correspondences->push_back(pcl::Correspondence(idx, idx, l2_dist(ps, pt)));        
            it++; idx++;
        }
    }

    if (corrs.size() < 4) { 
        std::cerr << " not enough 3d corrs" << std::endl;
        return;
    }
    cx /= corrs.size(), cy /= corrs.size(), cz /= corrs.size();

    source->width = corrs.size(); 
    source->height = 1; 

    target->width = corrs.size(); 
    target->height = 1; 

    // pcl::registration::CorrespondenceRejectorSampleConsensus<pcl::PointXYZ> corr_rej_sac;
    // corr_rej_sac.setInputCloud (source);
    // corr_rej_sac.setTargetCloud (target);
    // corr_rej_sac.setInlierThreshold (.1);
    // corr_rej_sac.setMaxIterations (20);
    // corr_rej_sac.setInputCorrespondences (correspondences);
    // corr_rej_sac.getCorrespondences (*correspondences_result_rej_sac);
    // Eigen::Matrix4f transform_res_from_SAC = corr_rej_sac.getBestTransformation ();


    Eigen::Matrix4f transform_res_from_SVD;
    pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> trans_est_svd;
    trans_est_svd.estimateRigidTransformation(*source, *target,
                                              *correspondences,
                                              transform_res_from_SVD);

    Eigen::Matrix4f tf_g_to_can = Eigen::Matrix4f::Identity();
    tf_g_to_can(0,3) = cx;
    tf_g_to_can(1,3) = cy;
    tf_g_to_can(2,3) = cz;

    Eigen::Matrix4f tf_g_to_live = transform_res_from_SVD * tf_g_to_can;

    //std::cerr << tf_g_to_live << std::endl;
    double rot[9];
    rot[0] = tf_g_to_live(0,0), rot[1] = tf_g_to_live(0,1), rot[2] = tf_g_to_live(0,2), 
        rot[3] = tf_g_to_live(1,0), rot[4] = tf_g_to_live(1,1), rot[5] = tf_g_to_live(1,2), 
        rot[6] = tf_g_to_live(2,0), rot[7] = tf_g_to_live(2,1), rot[8] = tf_g_to_live(2,2);

    bot_core_rigid_transform_t tf;
#if 1
    bot_matrix_to_quat(rot, tf.quat);
#else
    tf.quat[0] = 1;
    tf.quat[1] = 0; 
    tf.quat[2] = 0; 
    tf.quat[3] = 0;
#endif
    tf.utime = kinect.timestamp;
    tf.trans[0] = tf_g_to_live(0,3);
    tf.trans[1] = tf_g_to_live(1,3);
    tf.trans[2] = tf_g_to_live(2,3);
 
    bot_core_rigid_transform_t_publish (kinect.lcm, "GLOBAL_TO_LOCAL", &tf);

    // std::cerr << "RigidT: " << std::endl;
    // std::cout << transform_res_from_SVD << std::endl;
    return;

}

void compute(const Mat1b& belief) { 
    if (!tracker.running())
        return;

    float median_depth = 0;
    std::vector<std::pair<Point3f, Point3f> > corrs;
    tracker.detectObject(belief, corrs, median_depth);
    transformationEstimation(corrs);

    // ICP
    // Convert kinect msg to pcl point cloud
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr object_pcl(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr final_pcl(new pcl::PointCloud<pcl::PointXYZRGB>);
    extractPCL(object_pcl, point_cloud_pcl, belief, median_depth);

    Eigen::Matrix4f icp_tf;
    computeICP(icp_tf, updated_tf, final_pcl, object_pcl, canonical_obj_pcl);

    publishPCL(final_pcl);

    return;
}

// static void 
// on_skeleton_2d_frame (const lcm_recv_buf_t *rbuf, const char *channel,
// 		   const kinect_skeleton_msg_t *skeleton_msg, void *user_data ) {

//     if(kinect_info.skeleton_msg)
//         kinect_skeleton_msg_t_destroy(kinect_info.skeleton_msg);
//     kinect_info.skeleton_msg = kinect_skeleton_msg_t_copy(skeleton_msg);

//     tracker.updateSkeleton(kinect_info);

// }


void hueSegment(const Mat1b& mask) { 
    const float tolerance = 0.01;
    const float delta_hue = 10*PI/180;

    // pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB>);
    // tree->setInputCloud (point_cloud_pcl);

    pcl::PointIndices in, out;
    uchar* pimg = mask.data;
    for (int i=0; i<mask.rows; i++, 
             pimg+=mask.step)
        for (int j=0; j<mask.cols; j++)
            if (pimg[j])
                in.indices.push_back(i*mask.cols + j);
    // in.header = point_cloud_pcl->header;
    //seededHueSegmentation(*point_cloud_pcl, tree, tolerance, in, out, delta_hue);

    // // Output
    // pcl::PointCloud<pcl::PointXYZRGB>::Ptr object_pcl(new pcl::PointCloud<pcl::PointXYZRGB>);
    // object_pcl->width = out.indices.size(); 
    // object_pcl->height = 1;
    // object_pcl->points.resize(out.indices.size());

    // for (int j=0; j<out.indices.size(); j++) 
    //     object_pcl->points[j] = point_cloud_pcl->points[out.indices[j]];

    // publishPCL(object_pcl);

    return;
}

void buildSkeletonMap(const kinect_skeleton_msg_t *skeleton_msg) { 

    skel_info.joint_p.clear();
    for (int i=0; i<skeleton_msg->num_links; i++) { 
        const kinect_link_msg_t& link = skeleton_msg->links[i];
        skel_info.joint_p.insert(std::make_pair(link.joint_id, 
                                                std::make_pair(Point3f(link.source.x, 
                                                                       link.source.y, 
                                                                       link.source.z), 
                                                               Point3f(link.dest.x, 
                                                                       link.dest.y, 
                                                                       link.dest.z))));
    }
    return;
}

void computePointingVectors() { 

    skel_info.le = skel_info.joint_p[XN_SKEL_LEFT_HAND].first;
    skel_info.lh = skel_info.joint_p[XN_SKEL_LEFT_HAND].second;
    skel_info.lvec = cv::Point3f(skel_info.lh.x-skel_info.le.x, 
                                 skel_info.lh.y-skel_info.le.y, 
                                 skel_info.lh.z-skel_info.le.z);
    float norm = sqrt(skel_info.lvec.x * skel_info.lvec.x + 
                      skel_info.lvec.y * skel_info.lvec.y + 
                      skel_info.lvec.z * skel_info.lvec.z);
    skel_info.lvec.x /= norm, skel_info.lvec.y /= norm, skel_info.lvec.z /= norm;

    skel_info.re = skel_info.joint_p[XN_SKEL_RIGHT_HAND].first;
    skel_info.rh = skel_info.joint_p[XN_SKEL_RIGHT_HAND].second;
    skel_info.rvec = cv::Point3f(skel_info.rh.x-skel_info.re.x, 
                                 skel_info.rh.y-skel_info.re.y, 
                                 skel_info.rh.z-skel_info.re.z);
    norm = sqrt(skel_info.rvec.x * skel_info.rvec.x + 
                skel_info.rvec.y * skel_info.rvec.y + 
                skel_info.rvec.z * skel_info.rvec.z);
    skel_info.rvec.x /= norm, skel_info.rvec.y /= norm, skel_info.rvec.z /= norm;

    return;
}

void trackPointingRegion() { 

    if (!point_cloud_pcl->points.size())
        return;

    float x, y, z;
    Mat1b maskr = Mat1b::zeros(nvert, nhoriz);
    Mat1b maskl = maskr.clone();

    uchar* pmaskr = maskr.data;
    uchar* pmaskl = maskl.data;

    tracker.pt_msg_max = 0;

    int minx = WIDTH, miny = HEIGHT, maxx = 0, maxy = 0;
    int idx, u, v, f_idx;
    int r, g, b;
    for (int i=0; i<nvert; i++) { 
        for (int j=0; j<nhoriz; j++) { 
            idx = i*nhoriz + j;
            
            x = point_cloud_pcl->points[idx].x;
            y = point_cloud_pcl->points[idx].y;
            z = point_cloud_pcl->points[idx].z;

            if (z == 0) continue;

            r = point_cloud_pcl->points[idx].r;
            g = point_cloud_pcl->points[idx].g;
            b = point_cloud_pcl->points[idx].b;

            // Check if point is within pointed region
            float rdx = x - skel_info.rh.x, rdy = y - skel_info.rh.y, rdz = z - skel_info.rh.z;
            float rn =  sqrt(rdx * rdx + rdy * rdy + rdz * rdz); 
            float rrad = (rdx / rn) * skel_info.rvec.x + (rdy / rn) * skel_info.rvec.y + (rdz / rn) * skel_info.rvec.z;

            float ldx = x - skel_info.lh.x, ldy = y - skel_info.lh.y, ldz = z - skel_info.lh.z;
            float ln =  sqrt(ldx * ldx + ldy * ldy + ldz * ldz); 
            float lrad = (ldx / ln) * skel_info.lvec.x + (ldy / ln) * skel_info.lvec.y + (ldz / ln) * skel_info.lvec.z;             

            if (rrad > 0.96 && fabs(rdz) < .1) { // 10 deg cone (red)
                // point_cloud_pcl->points[idx].r = 155;
                // point_cloud_pcl->points[idx].g = 0;
                // point_cloud_pcl->points[idx].b = 0;

                // pmaskr[idx] = 255;
            }
            
            if (lrad > 0.866) { 
                if (lrad > 0.96/* && fabs(ldz) < .1*/) { // 10 deg cone (blue)
                    // point_cloud_pcl->points[idx].r = 0;
                    // point_cloud_pcl->points[idx].g = 0;
                    // point_cloud_pcl->points[idx].b = 155;

                    minx = std::min(minx, int(x)); 
                    miny = std::min(miny, int(y)); 
                    maxx = std::max(maxx, int(x)); 
                    maxy = std::max(maxy, int(y)); 

                    pmaskl[idx] = 255;
                    
                    if (i==0 && j==0 )
                    std::cerr << z << std::endl;
                    if (lrad > tracker.pt_msg_max) { 
                        tracker.pt_msg.u = j * deci_r;                
                        tracker.pt_msg.v = i * deci_r;
                        tracker.pt_msg_max = lrad;
                    }                        

                } else { 
                    pmaskr[idx] = 255;
                }
            }
        }
    }


    // Mat1b maskl_resize; 
    // resize(maskl, maskl_resize, Size(WIDTH, HEIGHT));
    // uchar* ptr = maskl_resize.data;    
    // idx=0;
    // for (int i=0; i<maskl_resize.rows; i++) { 
    //     for (int j=0; j<maskl_resize.cols; j++) { 
    //         //if (ptr[idx++]) { 
    //             cv::Vec3f p = kinect.getXYZ(i,j);
    //             obj_pts.push_back(Point3f(p[0]*1000, -p[1]*1000, p[2]*1000));
    //             //}
    //     }
    // }
    // imshow("test", maskl_resize);
    
    Mat3b proj_maskr = Mat1b::zeros(projector_image_height, projector_image_width);
    proj_maskr = cv::Vec3b(255, 255, 255);

    proj_selection.x = MAX(0, minx * deci_r); 
    proj_selection.y = MAX(0, miny * deci_r); 
    proj_selection.width = (maxx - minx) * deci_r; 
    proj_selection.height = (maxy - miny) * deci_r; 
    proj_selection &= Rect(0, 0, WIDTH, HEIGHT);    

    std::vector<Point2f> img_pts;
    std::vector<Point2f> proj_pts;
    // std::vector<Point3f> obj_pts;
    Mat obj_pts;

    Mat_<float> homography = Mat_<float>::zeros(3, 3);

    // homography(0,0) = -3.81353831e+00; 
    // homography(0,1) = -8.75213090e-03; 
    // homography(0,2) = 3.22612939e+03; 
    // homography(1,0) = -5.80510534e-02; 
    // homography(1,1) = -4.28503656e+00; 
    // homography(1,2) = 2.43050439e+03; 
    // homography(2,0) = 9.57517623e-05; 
    // homography(2,1) = 2.06767843e-04; 
    // homography(2,2) = 1.00000000e+00; 

    // img_pts.push_back(Point2f(proj_selection.y,proj_selection.x));
    // img_pts.push_back(Point2f(proj_selection.y+proj_selection.height,proj_selection.x));
    // img_pts.push_back(Point2f(proj_selection.y+proj_selection.height,proj_selection.x+proj_selection.width));
    // img_pts.push_back(Point2f(proj_selection.y,proj_selection.x+proj_selection.width));
    // perspectiveTransform(Mat(img_pts), obj_pts, homography);

    projectPoints(obj_pts, cam2proj_rvec, cam2proj_tvec, projector_K, projector_D, proj_pts); 

    for (int j=0; j<proj_pts.size(); j++) { 
        float x1 = proj_pts[j].x, y1 = proj_pts[j].y;
        float x2 = proj_pts[(j+1)%proj_pts.size()].x, y2 = proj_pts[(j+1)%proj_pts.size()].y;
        float x3 = proj_pts[(j+2)%proj_pts.size()].x, y3 = proj_pts[(j+2)%proj_pts.size()].y;
        //printf("(%f,%f) <= (%f,%f,%f)\n", x1, y1, obj_pts[j].x, obj_pts[j].y, obj_pts[j].z);
        //printf("(%f,%f) <= (%f,%f,%f)\n", x2, y2, obj_pts[j].x, obj_pts[j].y, obj_pts[j].z);

        if (x1 >= 0 && x1 < proj_maskr.cols && y1 >=0 && y1 < proj_maskr.rows && 
            x2 >= 0 && x2 < proj_maskr.cols && y2 >=0 && y2 < proj_maskr.rows) { 
            line(proj_maskr,Point(int(x1),int(y1)),Point(int(x2),int(y2)),Scalar(255, 0, 0), 8, 4);
        }
        if (x1 >= 0 && x1 < proj_maskr.cols && y1 >=0 && y1 < proj_maskr.rows && 
            x3 >= 0 && x3 < proj_maskr.cols && y3 >=0 && y3 < proj_maskr.rows) { 
            line(proj_maskr,Point(int(x1),int(y1)),Point(int(x3),int(y3)),Scalar(255, 0, 0), 8, 4);
        }

    }
    
    imshow("projector", proj_maskr);
        
        // projector_image_msg.timestamp = bot_timestamp_now();
        // memset(projector_image_msg.image_data, 0, projector_image_msg.image_data_nbytes);
        // unsigned char* _ptr = projector_image_msg.image_data;
        // for (int i = 0; i < img_pts.size(); ++i) {
        //     float x = img_pts[i].x, y = img_pts[i].y;
        //     if (x >= 0 && x < projector_image_width && y >=0 && y < projector_image_height) { 
        //         int idx = int(y)*projector_image_width+int(x);
        //         _ptr[idx] = 255;
        //     }
        // }
        // kinect_image_msg_t_publish(kinect.lcm, "PROJECTOR_IMAGE", &projector_image_msg);
    //hueSegment(maskl);

    tracker.updatePointingRegion(maskl, maskr);

    return;
}


static bool contours_sort(const std::vector<Point>& lhs, const std::vector<Point>& rhs) { 
    return lhs.size() > rhs.size();
}

inline void fill_mask(Mat1b& mask, const RotatedRect& rr) { 
    mask = 0;
    if (rr.size.height >=0 && rr.size.width >=0)
        ellipse( mask, rr, 255, CV_FILLED );
    return;
}


static void 
on_skeleton_frame (const lcm_recv_buf_t *rbuf, const char *channel,
		   const kinect_skeleton_msg_t *skeleton_msg, void *user_data )
{
    if(kinect.skeleton_msg)
        kinect_skeleton_msg_t_destroy(kinect.skeleton_msg);
    kinect.skeleton_msg = kinect_skeleton_msg_t_copy(skeleton_msg);

    //tracker.drawSkeleton(kinect_info);

    // Map for skeleton joints - skel_info
    buildSkeletonMap(skeleton_msg);

    // Left and right pointing vector - skel_info.le, lh, lvec, re, rh, rvec    
    computePointingVectors();

    // Use the above information to find points within a cone of 10 deg
    // Also colors the point cloud
    trackPointingRegion();

    return;
}

static void
on_segmented_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                    const bot_core_image_t *seg_msg, void *user_data ) { 

    // Segmented image
    Mat3b seg = Mat(seg_msg->height, seg_msg->width, CV_8UC3);
    memcpy(seg.data, seg_msg->data, seg_msg->width * seg_msg->height * 3);

    // Grayscale segmented image (remove and fix the above memcpy to do this in one pass)
    Mat1b segu; 
    seg.convertTo(segu, CV_8UC1);

    // Get the binary belief image and resize it to the segmented image
    Mat1b belief = tracker.belief;
    if (seg.rows != belief.rows || seg.cols != belief.cols) 
        resize(belief, belief, Size(seg.cols,seg.rows));

    // Blob segment
    RNG rng(12345);
    vector < vector<Point> > contours;
    vector<Vec4i> hierarchy;
    findContours( belief, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
    // findContours( segu, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

    // Mat1b mask = Mat1b::zeros(seg.size());
    // for( int i = 0; i<contours.size() && i<2; i++ ) {
    //     Scalar gscale = Scalar( rng.uniform(0, 255) );
    //     drawContours( mask, contours, i, gscale, CV_FILLED, 8, hierarchy, 0, Point() );
    // }
    
    // std::vector<int> votes(contours.size(), 0);
    // for (int y=0; y<segu.rows; y++) { 
    //     for (int x=0; x<segu.cols; x++) { 
    //         if (!belief(y,x))
    //             continue;
            
    //         for (int j=0; j<contours.size(); j++) 
    //             if (pointPolygonTest(contours[j], Point(x,y), false) > 0) { 
    //                 votes[j]++;
    //                 break;
    //             }
    
    //     }
    // }

    // Descending sort blob
    std::sort(contours.begin(), contours.end(), contours_sort);

    // Get the moments
    vector<Moments> mu(contours.size() );
    for( int i = 0; i < contours.size(); i++ )
        mu[i] = moments( contours[i], false );

    //  Get the mass centers:
    vector<Point2f> mc( contours.size() );
    for( int i = 0; i < contours.size(); i++ )
        mc[i] = Point2f( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00 );

    Mat1b mask = Mat1b::zeros(seg.size());
    for( int i = 0; i<contours.size() && i<2; i++ ) {
        // if (i > 1 && contours[i].size() * 1.f / contours[i-1].size() < .7f) 
        //     break;

        //Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
        // drawContours( seg, contours, i, color, CV_FILLED, 8, hierarchy, 0, Point() );

        //Scalar gscale = Scalar( rng.uniform(0, 255) );
        // drawContours( mask, contours, i, gscale, CV_FILLED, 8, hierarchy, 0, Point() );

        Vec3b c = seg(mc[i].y, mc[i].x);
            
        for (int y=0; y<seg.rows; y++) 
            for (int x=0; x<seg.cols; x++) 
                if (seg(y,x)[0] == c[0] && seg(y,x)[1] == c[1] && seg(y,x)[2] == c[2])
                    mask(y,x) = 1;

        floodFill(mask, Point(mc[i].x,mc[i].y), 255);
        threshold(mask, mask, 2, 255, THRESH_BINARY);

        break;
    }

    // // Resize to the right scale
    // resize(mask, tracker.belief_comp, Size(nhoriz, nvert));
    tracker.belief_comp = mask;

    compute(tracker.belief_comp);

    imshow("M1 + M2", mask);
    imshow("Segmented Image", seg);

    return;
}

int envoy_decompress_bot_core_image_t(const bot_core_image_t * jpeg_image, bot_core_image_t *uncomp_image)
{
  //copy metadata
  uncomp_image->utime = jpeg_image->utime;
  if (uncomp_image->metadata != NULL)
    bot_core_image_metadata_t_destroy(uncomp_image->metadata);
  uncomp_image->nmetadata = jpeg_image->nmetadata;
  if (jpeg_image->nmetadata > 0) {
    uncomp_image->metadata = bot_core_image_metadata_t_copy(jpeg_image->metadata);
  }
  
  uncomp_image->width = jpeg_image->width;
  uncomp_image->height = jpeg_image->height;
  
  uncomp_image->row_stride = jpeg_image->width * 3;//jpeg_image->row_stride;

  int depth=0;
  if (uncomp_image->width>0)
      depth= uncomp_image->row_stride / (double)uncomp_image->width;

  int ret;

  if(uncomp_image->data == NULL){
      int uncomp_size = (uncomp_image->width) * (uncomp_image->height) * depth;
      uncomp_image->data = (unsigned char *) realloc(uncomp_image->data, uncomp_size * sizeof(uint8_t));
  }

  ret = jpeg_decompress_8u_rgb(jpeg_image->data, 
                               jpeg_image->size, uncomp_image->data, 
                               uncomp_image->width, 
                               uncomp_image->height, 
                               uncomp_image->row_stride);
  
  uncomp_image->row_stride = uncomp_image->width * depth;
  uncomp_image->size = uncomp_image->width *uncomp_image->height* depth;
  if (depth == 1) {
    uncomp_image->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY;
  }
  else if (depth == 3) {
    uncomp_image->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB; //TODO: lets just assume its rgb... dunno what else to do :-/
  }
  return ret;
}

static void on_bumblebee_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                                   const bot_core_image_t *msg, 
                                   void *user_data ) {
    if (msg->width != WIDTH || msg->height != HEIGHT)
        return;
    Mat3b img = Mat3b::zeros(msg->height, msg->width);
    
    static bot_core_image_t * localFrame=(bot_core_image_t *) calloc(1,sizeof(bot_core_image_t));
    if (msg->pixelformat==BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG){
        //create space for decompressed image
        envoy_decompress_bot_core_image_t(msg,localFrame);
        memcpy(img.data, localFrame->data, sizeof(uint8_t)*msg->width*msg->height*3);            
    } else{
        //uncompressed, just copy pointer
        memcpy(img.data, msg->data, sizeof(uint8_t)*msg->width*msg->height*3);            
    }
    cvtColor(img, img, CV_BGR2RGB);

#if 0
    Mat gray;
    cvtColor(img, gray, CV_RGB2GRAY);
    IplImage img_ipl = IplImage(img);

    if (trackObject < 0) { 
        //tldtracker->selectObject(gray, &selection);
        trackObject = 1;
    } 
    
    if (trackObject) { 
        tldtracker->processImage(&img_ipl);
        int confident = (tldtracker->currConf >= .5) ? 1 : 0;

        CvScalar yellow = CV_RGB(255,255,0);
        CvScalar blue = CV_RGB(0,0,255);
        CvScalar black = CV_RGB(0,0,0);
        CvScalar white = CV_RGB(255,255,255);
        
        if(tldtracker->currBB != NULL) {
            CvScalar rectangleColor = (confident) ? blue : yellow;
            cvRectangle(&img_ipl, tldtracker->currBB->tl(), tldtracker->currBB->br(), rectangleColor, 8, 8, 0);
        }
        
        printf("Current conf: %3.2f\n", tldtracker->currConf);
    }

    if (selectObject && selection.width > 0 && selection.height > 0) {
        Mat roi(img, selection);
        bitwise_not(roi, roi);
    }
#endif
    imshow("bumblebee", img);

    return;
}

static void on_dragonfly_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                                   const bot_core_image_t *msg, 
                                   void *user_data ) {
    Mat1b img = Mat1b::zeros(msg->height, msg->width);
    
    static bot_core_image_t * localFrame=(bot_core_image_t *) calloc(1,sizeof(bot_core_image_t));
    if (msg->pixelformat==BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG){
        //create space for decompressed image
        envoy_decompress_bot_core_image_t(msg,localFrame);
        memcpy(img.data, localFrame->data, sizeof(uint8_t)*msg->width*msg->height);            
    } else{
        //uncompressed, just copy pointer
        memcpy(img.data, msg->data, sizeof(uint8_t)*msg->width*msg->height);            
    }
    //cvtColor(img, img, CV_BGR2RGB);

    Mat3b proj_maskr = Mat1b::zeros(projector_image_height, projector_image_width);
    proj_maskr = cv::Vec3b(255, 255, 255);

    std::vector<Point2f> img_pts;
    std::vector<Point2f> proj_pts;
    std::vector<Point2f> obj_pts_;
    std::vector<Point3f> obj_pts;
    Mat obj_pts_mat;

    Mat_<float> homography = Mat_<float>::zeros(3, 3);

    // homography(0,0) = 2.50365806e+00;  
    // homography(0,1) = -1.07753903e-01; 
    // homography(0,2) = -9.47022705e+02; 
    // homography(1,0) = -8.22404996e-02; 
    // homography(1,1) = 2.91557384e+00; 
    // homography(1,2) = -6.94544434e+02; 
    // homography(2,0) = -1.30375163e-04; 
    // homography(2,1) = 9.05552151e-05; 
    // homography(2,2) = 1.00000000e+00; 

    homography(0,0) = .114636123;  
    homography(0,1) = -2.60837460; 
    homography(0,2) = 1324.01038; 
    homography(1,0) = 2.41556573; 
    homography(1,1) = .407334924; 
    homography(1,2) = -764.489624; 
    homography(2,0) = .0000309860952; 
    homography(2,1) = .000697071897; 
    homography(2,2) = 1.00000000; 

    img_pts.push_back(Point2f(proj_selection.x,proj_selection.y));
    img_pts.push_back(Point2f(proj_selection.x+proj_selection.width,proj_selection.y));
    img_pts.push_back(Point2f(proj_selection.x+proj_selection.width,proj_selection.y+proj_selection.height));
    img_pts.push_back(Point2f(proj_selection.x,proj_selection.y+proj_selection.height));
    //perspectiveTransform(img_pts, proj_pts, homography);

    // 3d from 2d coordinates (in camera's frame of reference
    perspectiveTransform(img_pts, obj_pts_, homography);

    // 3rd coordinate
    for (int j=0; j<obj_pts.size(); j++)
        obj_pts.push_back(Point3f(obj_pts_[j].x, obj_pts_[j].y, 0));
    
    Mat_<float> T = Mat_<float>::eye(4,4);
    Mat_<float> R = Mat_<float>::eye(3,3);
    Rodrigues(cam_rvec, R);
    for (int i=0; i<3; i++) 
        for (int j=0; j<3; j++) 
            T(i,j) = R(i,j);
    for (int j=0; j<3; j++) 
        T(j,3) = cam_tvec(j,0);
    
    // object points in global coordinates
    std::vector<Point3f> obj_pts_3d;
    transform(obj_pts, obj_pts_3d, R);
    for (int j=0; j<obj_pts_3d.size(); j++) { 
        Point3f p = obj_pts_3d[j];
        obj_pts_3d[j] = Point3f(p.x + cam_tvec(0.0), p.y+cam_tvec(1,0), p.z+cam_tvec(2,0));
    }

    projectPoints(obj_pts_3d, cam2proj_rvec, cam2proj_tvec, projector_K, projector_D, proj_pts); 

    //std::cerr << proj_pts.rows << " " << proj_pts.cols << std::endl;
    for (int j=0; j<proj_pts.size(); j++) { 
        float x1 = proj_pts[j].x, y1 = proj_pts[j].y;
        float x2 = proj_pts[(j+1)%proj_pts.size()].x, y2 = proj_pts[(j+1)%proj_pts.size()].y;
        float x3 = proj_pts[(j+2)%proj_pts.size()].x, y3 = proj_pts[(j+2)%proj_pts.size()].y;
        // printf("(%f,%f) <= (%f,%f)\n", x1, y1, img_pts[j].x, img_pts[j].y);
        //printf("(%f,%f) <= (%f,%f,%f)\n", x1, y1, obj_pts[j].x, obj_pts[j].y, obj_pts[j].z);
        //printf("(%f,%f) <= (%f,%f,%f)\n", x2, y2, obj_pts[j].x, obj_pts[j].y, obj_pts[j].z);

        if (x1 >= 0 && x1 < proj_maskr.cols && y1 >=0 && y1 < proj_maskr.rows && 
            x2 >= 0 && x2 < proj_maskr.cols && y2 >=0 && y2 < proj_maskr.rows) { 
            line(proj_maskr,Point(int(x1),int(y1)),Point(int(x2),int(y2)),Scalar(255, 0, 0), 8, 4);
        }
        if (x1 >= 0 && x1 < proj_maskr.cols && y1 >=0 && y1 < proj_maskr.rows && 
            x3 >= 0 && x3 < proj_maskr.cols && y3 >=0 && y3 < proj_maskr.rows) { 
            line(proj_maskr,Point(int(x1),int(y1)),Point(int(x3),int(y3)),Scalar(255, 0, 0), 8, 4);
        }

    }
    imshow("projector", proj_maskr);

    Mat3b img3;
    cvtColor(img, img3, CV_GRAY2BGR);
    rectangle(img3,
         Point(proj_selection.x,proj_selection.y),
         Point(proj_selection.x+proj_selection.width,proj_selection.y+proj_selection.height),
         Scalar(255, 0, 0));

    imshow("dragonfly", img3);
    return;
}

static void on_kinect_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                                   const kinect_frame_msg_t *msg, 
                                   void *user_data ) {
    
    if (msg->depth.depth_data_format != KINECT_DEPTH_MSG_T_DEPTH_MM) { 
        std::cerr << "Warning: kinect data format is not KINECT_DEPTH_MSG_T_DEPTH_MM" << std::endl 
                  << "Program may not function properly" << std::endl;
    }
    assert (msg->depth.depth_data_format == KINECT_DEPTH_MSG_T_DEPTH_MM);

    double t1 = timestamp_us();    
    // Update the kinect_data struct with depth and rgb information
    kinect.update(msg);
    double t2 = timestamp_us();

    Mat img = kinect.getRGB().clone();
    Mat gray = kinect.getGray().clone();

    tracker.run();

    // Copy point cloud
    populatePCL(point_cloud_pcl);

#define PROJECTOR 0
#if PROJECTOR
    Mat3b proj_maskr = Mat1b::zeros(projector_image_height, projector_image_width);
    proj_maskr = cv::Vec3b(255, 255, 255);

    std::vector<Point2f> img_pts;
    std::vector<Point3f> obj_pts;
    
    cv::Vec3f p = kinect.getXYZ(proj_selection.y,proj_selection.x);
    obj_pts.push_back(Point3f(p[0]*1000, -p[1]*1000, p[2]*1000));

    p = kinect.getXYZ(proj_selection.y+proj_selection.height,proj_selection.x);
    obj_pts.push_back(Point3f(p[0]*1000, -p[1]*1000, p[2]*1000));

    p = kinect.getXYZ(proj_selection.y+proj_selection.height,proj_selection.x+proj_selection.width);
    obj_pts.push_back(Point3f(p[0]*1000, -p[1]*1000, p[2]*1000));

    p = kinect.getXYZ(proj_selection.y,proj_selection.x+proj_selection.width);
    obj_pts.push_back(Point3f(p[0]*1000, -p[1]*1000, p[2]*1000));

    projectPoints(obj_pts, cam2proj_rvec, cam2proj_tvec, projector_K, projector_D, img_pts); 

    for (int j=0; j<img_pts.size(); j++) { 
        float x1 = img_pts[j].x, y1 = img_pts[j].y;
        float x2 = img_pts[(j+1)%img_pts.size()].x, y2 = img_pts[(j+1)%img_pts.size()].y;
        float x3 = img_pts[(j+2)%img_pts.size()].x, y3 = img_pts[(j+2)%img_pts.size()].y;
        // printf("(%f,%f) <= (%f,%f,%f)\n", x1, y1, obj_pts[j].x, obj_pts[j].y, obj_pts[j].z);
        //printf("(%f,%f) <= (%f,%f,%f)\n", x2, y2, obj_pts[j].x, obj_pts[j].y, obj_pts[j].z);

        //if (x1 >= 0 && x1 < proj_maskr.cols && y1 >=0 && y1 < proj_maskr.rows && 
        //    x2 >= 0 && x2 < proj_maskr.cols && y2 >=0 && y2 < proj_maskr.rows) { 
            line(proj_maskr,Point(int(x1),int(y1)),Point(int(x2),int(y2)),Scalar(255, 0, 0), 8, 4);
            //}
            //if (x1 >= 0 && x1 < proj_maskr.cols && y1 >=0 && y1 < proj_maskr.rows && 
            //x3 >= 0 && x3 < proj_maskr.cols && y3 >=0 && y3 < proj_maskr.rows) { 
            line(proj_maskr,Point(int(x1),int(y1)),Point(int(x3),int(y3)),Scalar(255, 0, 0), 8, 4);
            //}

    }
    imshow("projector", proj_maskr);
    rectangle(img,
         Point(proj_selection.x,proj_selection.y),
         Point(proj_selection.x+proj_selection.width,proj_selection.y+proj_selection.height),
         Scalar(255, 0, 0));
#endif
// #if 0
//     IplImage img_ipl = IplImage(img);

//     if (trackObject < 0 && !initLearning) { 
//         tldtracker->selectObject(gray, &selection);
//         trackObject = 1;
//         initLearning = true;
//     } // else if (trackObject < 0 && initLearning) { 
//     //     tldtracker->currBB = &selection;
//     //     tldtracker->currImg = gray;
//     //     trackObject = 1;
//     // } 

//     if (trackObject) { 
//         tldtracker->processImage(&img_ipl);
//         int confident = (tldtracker->currConf >= .5) ? 1 : 0;

//         CvScalar yellow = CV_RGB(255,255,0);
//         CvScalar blue = CV_RGB(0,0,255);
//         CvScalar black = CV_RGB(0,0,0);
//         CvScalar white = CV_RGB(255,255,255);
        
//         if(tldtracker->currBB != NULL) {
//             CvScalar rectangleColor = (confident) ? blue : yellow;
//             cvRectangle(&img_ipl, tldtracker->currBB->tl(), tldtracker->currBB->br(), rectangleColor, 8, 8, 0);
//         }
// 	Rect* detectorBB = tldtracker->detectorCascade->detectionResult->detectorBB;
//         if(detectorBB != NULL) {
//             CvScalar rectangleColor = white;
//             cvRectangle(&img_ipl, detectorBB->tl(), detectorBB->br(), rectangleColor, 4, 8, 0);
//         }

        
//         //printf("Current conf: %3.2f\n", tldtracker->currConf);
//     }

// #else

//     // DOT Detector test
//     if (trackObject < 0) { 
//         //Rect r(mouse.pt.x-25,mouse.pt.y-25,50,50);
//         Mat roi(gray, selection);
//         dotDetector.train( roi, trainParams, false );

//         rectangle(img, 
//                   Point(selection.x, selection.y), 
//                   Point(selection.x+selection.width,selection.y+selection.height),
//                   Scalar(255, 255, 0), 1, 8);
            
//         imshow("train", roi);
//         trackObject = 1;
//     }


//     if (trackObject) { 
//         const vector<string>& objectClassNames = dotDetector.getObjectClassNames();
//         const vector<DOTDetector::DOTTemplate>& dotTemplates = dotDetector.getDOTTemplates();

//         vector<Scalar> colors( objectClassNames.size() );
//         fillColors( colors );
//         std::cout << "Templates count " << dotTemplates.size() << std::endl;

//         Mat gray;
//         cvtColor(img, gray, CV_RGB2GRAY);
//         blur(gray, gray, cv::Size(5,5), cv::Point(-1,-1));
            
//         vector<vector<Rect> > rects;
//         dotDetector.detectMultiScale( gray, rects, detectParams );

//         const int textStep = 25;
//         for( size_t ci = 0; ci < objectClassNames.size(); ci++ ) {
//             putText( img, objectClassNames[ci], Point(textStep, textStep*(1+ci)), 1, 2, colors[ci], 3 );
//             for( size_t ri = 0; ri < rects[ci].size(); ri++ ) {
//                 rectangle( img, rects[ci][ri], colors[ci], 1 );
//             }
//         }
//     }


//     // if (trackObject < 0) {
//     //     hdtracker->newTracker(img, selection);
//     //     trackObject = 1;
//     // }

//     // if (trackObject) {
//     //     hdtracker->updateTracker(img);
//     //     Rect win = hdtracker->getTrackingWindow();
//     //     rectangle(image, win, Scalar(0,255,0), 2, CV_AA);
//     // }

// #endif

    if (selectObject && selection.width > 0 && selection.height > 0) {
        Mat roi(img, selection);
        bitwise_not(roi, roi);
    }

    imshow("Kinect RGB", img);

#if 1
    // Copy point cloud
    populatePCL(point_cloud_pcl);
    double t3 = timestamp_us();

    // Update tracker with rgb image
    // Also tracks the object (can check via tracker.running())
    tracker.run();
    double t4 = timestamp_us();

    // Find feature descriptors from hist mask
    fill_mask(tracker.belief, tracker.trackBox);
    compute(tracker.belief);
    double t5 = timestamp_us();

    std::cout << "======================================" << std::endl;
    // std::cerr << "update time: " << (t2 - t1)*1e-3 << " ms" << std::endl;
    // std::cerr << "populate time: " << (t3 - t2)*1e-3 << " ms" << std::endl;
    // std::cerr << "HT run time: " << (t4 - t3)*1e-3 << " ms" << std::endl;
    // std::cerr << "feature run time: " << (t5 - t4)*1e-3 << " ms" << std::endl;

    std::cerr << "TOTAL: " << (t5 - t1)*1e-3 << " ms" << std::endl;
#else

    return;

    // Copy point cloud
    //populatePCL(point_cloud_pcl);

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr contour_pcl(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr final_pcl(new pcl::PointCloud<pcl::PointXYZRGB>);
    //ctracker.contour_train = false;
    
    Rect sel = tracker.trackBox.boundingRect();
    sel.x *= SCALE, sel.y *= SCALE, sel.width *= SCALE, sel.height *= SCALE;
    ctracker.detectContours(contour_pcl, sel);

    // Eigen::Matrix4f icp_tf;
    // computeICP(icp_tf, updated_tf, final_pcl, contour_pcl, ctracker.canonical_obj_pcl);

    // std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> pcls;
    // pcls.push_back(ctracker.canonical_obj_pcl);
    // pcls.push_back(contour_pcl);
    // pcls.push_back(final_pcl);

    // publishPCL(pcls);

    // //ctracker.connectedComponents(contour_pcl);
#endif
    return;
}

// static void on_kinect_user_labels (const lcm_recv_buf_t *rbuf, const char *channel,
//                                    const kinect_image_msg_t *msg, 
//                                    void *user_data ) {
    
//     // copy user_labels
//     if (!kinect_info.user_labels_img.data)
//         kinect_info.user_labels_img.create(msg->height, msg->width);
//     memcpy(kinect_info.user_labels_img.data, msg->image_data, msg->height * msg->width);
//     tracker.updateUserLabels(kinect_info);

//     drawUserSkeletons();

//     return;
// }


void  INThandler(int sig)
{
    //if (hdtracker)
    //    delete hdtracker;
    // if (tldtracker)
    //     delete tldtracker;

    lcm_destroy(kinect.lcm);
    printf("Exiting\n");
    exit(0);
}

static void onMouse(int event, int x, int y, int flags, void* userdata) {
    MouseEvent* data = (MouseEvent*)userdata;
    proj_selection.x = MAX(0, x - 20); 
    proj_selection.y = MAX(0, y - 20); 
    proj_selection.width = 40; 
    proj_selection.height = 40; 
    proj_selection &= Rect(0, 0, 1296, 964);    

    if (selectObject) {
        selection.x = MIN(x, origin.x);
        selection.y = MIN(y, origin.y);
        selection.width = std::abs(x - origin.x);
        selection.height = std::abs(y - origin.y);
        selection &= Rect(0, 0, WIDTH, HEIGHT);
    }

    switch (event) {
    case CV_EVENT_LBUTTONDOWN:
        origin = Point(x, y);
        selection = Rect(x, y, 0, 0);
        selectObject = true;
        break;
    case CV_EVENT_LBUTTONUP:
        selectObject = false;
        trackObject = -1;
        break;
    }

    // if (!selectObject) { 
    //     switch( event ) {
    //     case CV_EVENT_LBUTTONDOWN:
    //         data->event = event;
    //         data->pt = Point(x,y);
    //         data->buttonState = flags;
    //         selectObject = true;
    //         break;
    //     case CV_EVENT_LBUTTONUP:
    //         break;
    //     }
    // }
    return;
}

void* lcm_handler(void* data) {
}

int main(int argc, char** argv)
{

    int c;
    char *lcm_url = NULL;

    // command line options - to publish on a specific url  
    while ((c = getopt (argc, argv, "vhir:jq:zl:")) >= 0) {
        switch (c) {
        case 'l':
            lcm_url = strdup(optarg);
            printf("Using LCM URL \"%s\"\n", lcm_url);
            break;
        case 'h':
        case '?':
            usage(argv[0]);
        }
    }

    point_cloud_pcl = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
    canonical_obj_pcl = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);

    // LCM-related
    kinect.lcm = lcm_create(lcm_url);
    if(!kinect.lcm) {
        printf("Unable to initialize LCM\n");
        return 1;
    } 

    // Initialize the tracker with kinect data pointer
    tracker.useKinectData(&kinect);
    ctracker.useKinectData(&kinect);

    // Get BotFrames instance
    // BotParam * param = bot_param_new_from_server(kinect.lcm, 0);
    // if (!param) {
    //     fprintf (stderr, "Error getting BotParam. Are you running the server?\n");
    //     return -1;
    // }

    // bot_param_get_double_array (param, "calibration.projector.extrinsic_cal.R", 
    //                             &(projector_to_camera_rvec[0]), 3);
    // bot_param_get_double_array (param, "calibration.projector.extrinsic_cal.T", 
    //                             &(projector_to_camera_tvec[0]), 3);
    // bot_param_get_double_array (param, "calibration.projector.intrinsic_cal.width", &projector_image_width, 1);
    // bot_param_get_double_array (param, "calibration.projector.intrinsic_cal.height",&projector_image_height, 1);
    // bot_param_get_double_array (param, "calibration.projector.intrinsic_cal.pinhole", 
    //                             &(projector_intrinsics[0]), 5);
    // bot_param_get_double_array (param, "calibration.projector.intrinsic_cal.distortion_k", 
    //                             &(projector_distortion[0]), 5);
    // bot_param_get_double_array (param, "calibration.projector.intrinsic_cal.distortion_p", 
    //                             &(projector_distortion[3]), 2);

    projector_K = Mat_<float>::zeros(3, 3);
    projector_D.create(5, 1);
    cam2proj_rvec = Mat_<float>::zeros(3, 1);
    cam2proj_tvec = Mat_<float>::zeros(3, 1);
    cam_rvec = Mat_<float>::zeros(3, 1);
    cam_tvec = Mat_<float>::zeros(3, 1);

    projector_K(0, 0) = 2.65340304e+03; // projector_intrinsics[0];
    projector_K(1, 1) = 2.92807626e+03;  // projector_intrinsics[1];
    projector_K(0, 1) = 0; // projector_intrinsics[2];
    projector_K(0, 2) = 5.67922706e+02; // projector_intrinsics[3];
    projector_K(1, 2) = 7.04857611e+02; // projector_intrinsics[4];
    projector_K(2, 2) = 1; // projector_intrinsics[4];

    // projector_K(0, 0) = 838.347; // projector_intrinsics[0];
    // projector_K(1, 1) = 836.554;  //  projector_intrinsics[1];
    // projector_K(0, 1) = 0; // projector_intrinsics[2];
    // projector_K(0, 2) = 640; // projector_intrinsics[3];
    // projector_K(1, 2) = 400; // projector_intrinsics[4];
    // projector_K(2, 2) = 1; // projector_intrinsics[4];

    projector_D(0,0) = 1.17541840e-01;
    projector_D(1,0) = -7.73418493e-01;
    projector_D(2,0) = 4.45593202e-03;
    projector_D(3,0) = -2.93875462e-03;
    projector_D(4,0) = -3.69131008e+00;

    cam2proj_rvec(0,0) = 0.06774267;
    cam2proj_rvec(1,0) = 0.18203191;
    cam2proj_rvec(2,0) =  0.00150253;

    cam2proj_tvec(0,0) =  -236.49159241; 
    cam2proj_tvec(1,0) = 27.91162682;
    cam2proj_tvec(2,0) = -132.54951477;

    cam_rvec(0,0) = -0.27680904;
    cam_rvec(1,0) = -0.53654575;
    cam_rvec(2,0) = -1.54035747;

    cam_tvec(0,0) = -771.58239746;
    cam_tvec(1,0) = 156.20904541;
    cam_tvec(2,0) = 1714.90527344;

    projector_image_width = 1280; 
    projector_image_height = 800;

    // for (int j=0; j<projector_D.rows; j++) 
    //     projector_D(j,0) = projector_distortion[j];

    // for (int j=0; j<cam2proj_tvec.rows; j++) 
    //     cam2proj_tvec(j,0) = projector_to_camera_tvec[j];

    // for (int j=0; j<cam2proj_rvec.rows; j++) 
    //     cam2proj_rvec(j,0) = projector_to_camera_rvec[j];

    std::cerr << cam2proj_rvec << std::endl;
    std::cerr << cam2proj_tvec << std::endl;
    std::cerr << projector_K << std::endl;
    std::cerr << projector_D << std::endl;

    // Allocate user labels
    projector_image_msg.width = projector_image_width;
    projector_image_msg.height = projector_image_height;
    projector_image_msg.image_data_nbytes = projector_image_height*projector_image_width;
    projector_image_msg.image_data_format = KINECT_IMAGE_MSG_T_VIDEO_IR_8BIT;
    projector_image_msg.image_data = (uint8_t*) malloc(projector_image_msg.image_data_nbytes);

    // Dot detector training
    trainParams.winSize = Size(42, 42);
    trainParams.regionSize = 7;
    trainParams.minMagnitude = 60; // we ignore pixels with magnitude less then minMagnitude
    trainParams.maxStrongestCount = 7; // we find such count of strongest gradients for each region
    trainParams.maxNonzeroBits = 6; // we filter very textured regions (that have more then maxUnzeroBits count of 1s (ones) in the template)
    trainParams.minRatio = 0.85f;

    // 2. Detect objects
    detectParams.minRatio = 0.8f;
    detectParams.minRegionSize = 5;
    detectParams.maxRegionSize = 11;
#if SHOW_ALL_RECTS_BY_ONE
    detectParams.isGroup = false;
#endif


    // motion model params
    params.motion_model = CvMotionModel::LOW_PASS_FILTER;
    //params.motion_model = CvMotionModel::EM;
    params.low_pass_gain = 0.1;
    // mean shift params
    params.ms_tracker_weight = 0.4;
    params.ms_params.tracking_type = CvMeanShiftTrackerParams::HS;
    // feature tracking params
    params.ft_tracker_weight = 0.6;
    params.ft_params.feature_type = CvFeatureTrackerParams::OPTICAL_FLOW;
    params.ft_params.window_size = 0;

    // New TLD Tracker
    //hdtracker = new HybridTracker(params);
    //tldtracker = new TLD();

    // tldtracker->detectorCascade->imgWidth = WIDTH;
    // tldtracker->detectorCascade->imgHeight = HEIGHT;
    // tldtracker->detectorCascade->imgWidthStep = WIDTH;

    // Subscribe to the kinect image
    kinect_frame_msg_t_subscribe(kinect.lcm, "KINECT_FRAME", on_kinect_image_frame, NULL);
    //bot_core_image_t_subscribe(kinect.lcm, "FRNT_IMAGE_BUMBLEBEE", on_bumblebee_image_frame, NULL);
    bot_core_image_t_subscribe(kinect.lcm, "FRNT_IMAGE_BUMBLEBEE", on_dragonfly_image_frame, NULL);
    // kinect_image_msg_t_subscribe(lcm, "KINECT_PERSON_TRACKER_LABELS", on_kinect_user_labels, NULL);

    // either will be active
    kinect_skeleton_msg_t_subscribe(kinect.lcm, "KINECT_SKELETON_FRAME", on_skeleton_frame, NULL);
    // kinect_skeleton_msg_t_subscribe(lcm, "KINECT_SKELETON_2D", on_skeleton_2d_frame, NULL);

    bot_core_image_t_subscribe(kinect.lcm, "SEGMENTED_FRAME", on_segmented_frame, NULL);

    // Install signal handler to free data.
    signal(SIGINT, INThandler);

    namedWindow( "Kinect RGB" );
    namedWindow( "dragonfly" );
    setMouseCallback("dragonfly", onMouse, &mouse);

#if PROJECTOR
    namedWindow( "projector", CV_WINDOW_NORMAL );
    setWindowProperty( "projector", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
    //showImage("projector");
#endif

    // pthread_t lcm_th;
    // pthread_create(&lcm_th, NULL, lcm_handler, NULL);

    while(1) { 

        lcm_handle(kinect.lcm);

        unsigned char c = cv::waitKey(10) & 0xff;
        
        if (c == 'q') { 
            break;
        } else if (c == 't') { 
            std::cerr << "Using current pointed region" << std::endl;
            tracker.useCurrentPointLoc();
            assert (tracker.mode == HistTracker::VISION_MODE);
        } else if (c == 'f') { // Extract features and do icp
            // Feature extraction & copy depth img for 3d extraction
            tracker.buildDescriptors(tracker.belief);
        } else if (c == 'e') { // Extract all points within colormatch and do icp
            std::cerr << "Captured canonical object pose" << std::endl;    

            fill_mask(tracker.belief, tracker.trackBox);
            extractPCL(canonical_obj_pcl, point_cloud_pcl, tracker.belief);

            prunePCLWithDepth(canonical_obj_pcl);

            float cx=0, cy=0, cz=0;
            for (int j=0; j<canonical_obj_pcl->points.size(); j++) { 
                cx += canonical_obj_pcl->points[j].x, 
                    cy += canonical_obj_pcl->points[j].y, 
                    cz += canonical_obj_pcl->points[j].z;
            }
            mean_canonical_obj_pcl = Point3f(cx/canonical_obj_pcl->points.size(),
                                             cy/canonical_obj_pcl->points.size(),
                                             cz/canonical_obj_pcl->points.size());


        }  else if (c == 'c') { // Extract contours from canny and do icp
            // Rect sel = tracker.trackBox.boundingRect();
            // sel.x *= SCALE, sel.y *= SCALE, sel.width *= SCALE, sel.height *= SCALE;
            // ctracker.initialize(sel);

            // float cx=0, cy=0, cz=0;
            // for (int j=0; j<ctracker.canonical_obj_pcl->points.size(); j++) { 
            //     if (ctracker.canonical_obj_pcl->points[j].z == 0)
            //         continue;
            //     cx += ctracker.canonical_obj_pcl->points[j].x, 
            //         cy += ctracker.canonical_obj_pcl->points[j].y, 
            //         cz += ctracker.canonical_obj_pcl->points[j].z;
            // }

            // mean_canonical_obj_pcl = Point3f(cx/ctracker.canonical_obj_pcl->points.size(),
            //                                  cy/ctracker.canonical_obj_pcl->points.size(),
            //                                  cz/ctracker.canonical_obj_pcl->points.size());
            
        } else if (c == 'm') { 
            ctracker.drawIDMap();
        } else if (c == 'i') { 
            updated_tf = Eigen::Matrix4f::Identity();
        } else if (c == 'l') { 
            // tldtracker->learningEnabled = !tldtracker->learningEnabled;
            // printf("LearningEnabled: %d\n", tldtracker->learningEnabled);
        } else if (c == 'a') {
            // tldtracker->alternating = !tldtracker->alternating;
            // printf("alternating: %d\n", tldtracker->alternating);
        } else if (c== 's') { 
            Mat gray = kinect.getGray().clone();
            
            std::stringstream ss; 
            ss << "gray" << img_count++ << ".png";
            cv::imwrite(ss.str(), gray);
            std::cerr << "Saving " << ss.str() << std::endl;
        }
        
        // else if (c == 'c') { 
        //     // ICP

        //     vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> pcls;
        //     pcl::PointCloud<pcl::PointXYZRGB>::Ptr contour_pcl(new pcl::PointCloud<pcl::PointXYZRGB>);
        //     pcl::PointCloud<pcl::PointXYZRGB>::Ptr tf_contour_pcl(new pcl::PointCloud<pcl::PointXYZRGB>);
        //     pcl::PointCloud<pcl::PointXYZRGB>::Ptr final_pcl(new pcl::PointCloud<pcl::PointXYZRGB>);
        //     tracker.detectContours2(kinect_info, contour_pcl);

        //     Eigen::Vector3f initial_offset (.1, 0, 0);
        //     float angle = M_PI/12;
        //     Eigen::Quaternionf initial_rotation (cos (angle / 2), 0, 0, sin (angle / 2));
        //     pcl::transformPointCloud (*contour_pcl, *tf_contour_pcl, initial_offset, initial_rotation);


        //     pcl::IterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;
        //     icp.setInputCloud(tf_contour_pcl);
        //     icp.setInputTarget(contour_pcl);
        //     icp.setEuclideanFitnessEpsilon(1e-4);

        //     double t1 = timestamp_us();

        //     icp.align(*final_pcl); 

        //     double t2 = timestamp_us();
        //     printf("track: %f milliseconds\n", (t2 - t1)*1e-3);
            
        //     Eigen::Matrix4f icp_tf = icp.getFinalTransformation();
            
        //     std::cout << "in: " << contour_pcl->size() << std::endl;            
        //     std::cout << "score: " << icp.getFitnessScore() << std::endl;            
            
        //     pcls.push_back(contour_pcl);
        //     pcls.push_back(tf_contour_pcl);
        //     pcls.push_back(final_pcl);

        //     publishPCL(pcls);
        // } 
        // else if (c == 'v') { 
        //     tracker.detectContours2(kinect_info, canonical_obj_pcl);
        // } 
        // else if (c == 'b') { 
        //     vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> pcls;
        //     pcl::PointCloud<pcl::PointXYZRGB>::Ptr tf_contour_pcl(new pcl::PointCloud<pcl::PointXYZRGB>);
        //     pcl::PointCloud<pcl::PointXYZRGB>::Ptr final_pcl(new pcl::PointCloud<pcl::PointXYZRGB>);
        //     tracker.detectContours2(kinect_info, tf_contour_pcl);

        //     pcl::IterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;
        //     icp.setInputCloud(tf_contour_pcl);
        //     icp.setInputTarget(canonical_obj_pcl);
        //     icp.setEuclideanFitnessEpsilon(1e-4);

        //     double t1 = timestamp_us();

        //     icp.align(*final_pcl); 

        //     double t2 = timestamp_us();
        //     printf("track: %f milliseconds\n", (t2 - t1)*1e-3);
            
        //     Eigen::Matrix4f icp_tf = icp.getFinalTransformation();
            
        //     std::cout << "in: " << canonical_obj_pcl->size() << std::endl;            
        //     std::cout << "score: " << icp.getFitnessScore() << std::endl;            
            
        //     pcls.push_back(canonical_obj_pcl);
        //     pcls.push_back(tf_contour_pcl);
        //     pcls.push_back(final_pcl);

        //     publishPCL(pcls);
        // } 
        // else if (c == 'r') 
        // for (int j=0;j<tracker.id_votes.size(); j++) 
        //     tracker.id_votes[j] = 0;
    }

    return 0;
}


// void findUserHands(pcl::PointCloud<pcl::PointXYZRGB>& user_pcl, 
//                    pcl::PointCloud<pcl::PointXYZRGB>& pcl) { 
//     if (!kinect_info.user_labels_img.data || !kinect_info.skeleton_msg)
//         return;

//     // std::vector<int> user_votes(16, 0);
//     // for (int i=0; i<kinect_info.skeleton_msg->num_links; i++) { 
//     //     const kinect_link_msg_t& link = kinect_info.skeleton_msg->links[i];
//     //     user_votes[kinect_info.user_labels_img(link.source.y, link.source.x)]++;
//     //     user_votes[kinect_info.user_labels_img(link.dest.y, link.dest.x)]++;
//     // }
    
//     // int max_user = 0;
//     // for (int j=0; j<user_votes.size(); j++) 
//     //     if (user_votes[j] > max_user) max_user = user_votes[j];

//     // if (!max_user)
//     //     return;

//     joint_p2.clear();
//     //std::cerr << "======" << std::endl;
//     for (int i=0; i<kinect_info.skeleton_msg->num_links; i++) { 
//         const kinect_link_msg_t& link = kinect_info.skeleton_msg->links[i];
//         // std::cerr << "------" << std::endl;
//         // std::cerr << link.joint_id << std::endl;
//         // std::cerr << link.source.x << " " << link.source.y << std::endl;
//         // std::cerr << link.dest.x << " " << link.dest.y << std::endl;

//         joint_p2.insert(std::make_pair(link.joint_id, 
//                                       std::make_pair(Point2f(link.source.x, link.source.y), 
//                                                      Point2f(link.dest.x, link.dest.y))));
//     }
    
//     if (joint_p2.find(XN_SKEL_LEFT_HAND) == joint_p2.end() || 
//         joint_p2.find(XN_SKEL_RIGHT_HAND) == joint_p2.end())
//         return;


//     // left and right pointing vector
//     le = joint_p2[XN_SKEL_LEFT_HAND].first;
//     lh = joint_p2[XN_SKEL_LEFT_HAND].second;
//     lvec = cv::Point2f(lh.x-le.x, lh.y-le.y);
//     float norm = sqrt(lvec.x * lvec.x + lvec.y * lvec.y);
//     lvec.x /= norm, lvec.y /= norm;

//     re = joint_p2[XN_SKEL_RIGHT_HAND].first;
//     rh = joint_p2[XN_SKEL_RIGHT_HAND].second;
//     rvec = cv::Point2f(rh.x-re.x, rh.y-re.y);
//     norm = sqrt(rvec.x * rvec.x + rvec.y * rvec.y);
//     rvec.x /= norm, rvec.y /= norm;

//     int lx = lh.x, ly = lh.y;
//     for (int d=5; d<100; d+=5) { 
//         lx = (int)(lh.x + lvec.x * d), ly = (int)(lh.y + lvec.y * d);
//         if (ly > kinect_info.user_labels_img.rows || ly < 0 || 
//             lx > kinect_info.user_labels_img.cols || lx < 0)
//             break;
//         if (!kinect_info.user_labels_img.data[ly * kinect_info.user_labels_img.step + lx])
//             break;
//     }

//     int rx = rh.x, ry = rh.y;
//     for (int d=5; d<100; d+=5) { 
//         rx = (int)(rh.x + rvec.x * d), ry = (int)(rh.y + rvec.y * d);
//         if (ry > kinect_info.user_labels_img.rows || ry < 0 || 
//             rx > kinect_info.user_labels_img.cols || rx < 0)
//             break;
//         if (!kinect_info.user_labels_img.data[ry * kinect_info.user_labels_img.step + rx])
//             break;
//     }

//     float langle = atan2(lh.y - ly, lh.x - lx);
//     float rangle = atan2(rh.y - ry, rh.x - rx);
//     float ldst = sqrt( (lh.y - le.y) * (lh.y - le.y) + (lh.x - le.x) * (lh.x - le.x));
//     float rdst = sqrt( (rh.y - re.y) * (rh.y - re.y) + (rh.x - re.x) * (rh.x - re.x));
//     RotatedRect lhand(Point2f( (lh.x+lx)/2, (lh.y+ly)/2 ), Size(ldst / 2, ldst / 3), langle * 180 / PI);
//     RotatedRect rhand(Point2f( (rh.x+rx)/2, (rh.y+ry)/2 ), Size(rdst / 2, rdst / 3), rangle * 180 / PI);
//     //tracker.updateHandEstimates(lhand, rhand);
    
//     Mat1b dst = Mat1b::zeros(kinect_info.user_labels_img.size());
//     //line(dst,lh,Point(lx,ly),Scalar(255, 255, 255), 2);
//     ellipse(dst, rhand, 255, CV_FILLED);
//     //ellipse(dst, lhand, Scalar(255, 255, 0), CV_FILLED);
    
//     bitwise_and(dst, kinect_info.user_labels_img, dst);
//     threshold(dst, dst, 0, 255, CV_THRESH_BINARY);

//     // Prune
//     int i, j, idx, f_idx, u, v;
//     for (i = 0; i < nvert; i++) {
//         for(j = 0; j < nhoriz; j++) {
//             idx = i*nhoriz + j;
//             v = i*deci_r, u = j*deci_r;
//             f_idx = v*WIDTH+u;
//             if(dst.data[f_idx])
//                 pcl.points[idx].r = 255, pcl.points[idx].g = 0, pcl.points[idx].b = 0;
//             //user_pcl.points.push_back(pcl.points[idx]);
//         }
//     }

//     //imshow("hands", dst);
//     return;
// }
 
// void findUserRect(std::map<int, pcl::PointCloud<pcl::PointXYZRGB> >& pcl_segments) { 

//     std::map<int, std::vector<Point> > label_pts;

//     int ymin = std::numeric_limits<int>::max(), ymax = std::numeric_limits<int>::min(), 
//         zmin = ymin, zmax = ymax;
//     for (std::map<int, pcl::PointCloud<pcl::PointXYZRGB> >::const_iterator it = pcl_segments.begin(); 
//          it != pcl_segments.end(); it++) { 
//         const pcl::PointCloud<pcl::PointXYZRGB>& pcl = it->second;
//         std::vector<Point> pts;
//         for (int j=0; j<pcl.points.size(); j++) { 
//             if (pcl.points[j].y * 1000 < ymin)
//                 ymin = (int)(pcl.points[j].y * 1000);
//             if (pcl.points[j].z * 1000 < zmin)
//                 zmin = (int)(pcl.points[j].z * 1000) ;        
//             if (pcl.points[j].y * 1000 > ymax)
//                 ymax = (int)(pcl.points[j].y * 1000);
//             if (pcl.points[j].z * 1000 > zmax)
//                 zmax = (int)(pcl.points[j].z * 1000);        
//             pts.push_back(Point((int) (pcl.points[j].y * 1000), (int) (pcl.points[j].z * 1000)));
//         }
//         label_pts.insert(std::make_pair(it->first, pts));
//     }

//     if (ymax - ymin > 2000 || zmax - zmin > 2000)
//         return;
        
    
//     Mat3b labels = Mat3b::zeros(2500, 2000);
//     for (std::map<int, std::vector<Point> >::const_iterator it = label_pts.begin(); 
//          it != label_pts.end(); it++) { 
//         const int label = it->first;
//         const std::vector<Point>& pts = it->second;

//         Moments m = moments(pts);
//         double theta = 0.5 * atan2(2*m.mu11,(m.mu20 - m.mu02));
//         Point c(m.m10 / m.m00, m.m01 / m.m00);
//         for (int j=0; j<pts.size(); j++) 
//             circle(labels, Point(pts[j].x - ymin, pts[j].y - zmin), 2, color_tab_[label], -1);

// 	double x1,y1,x2,y2;
// 	double lengthLine = 200.;

// 	x1=c.x-lengthLine*cos(theta);
// 	y1=c.y-lengthLine*sin(theta);
// 	x2=c.x+lengthLine*cos(theta);
// 	y2=c.y+lengthLine*sin(theta);
// 	line(labels,Point(int(x1)-ymin,int(y1)-zmin),Point(int(x2)-ymin,int(y2)-zmin),color_tab_[label]);
//         //ellipse(labels, c, Size(400, 200), theta * 180 / PI, 0, 360, color_tab_[label], 2);
//         // ellipse(labels, c - Point(ymin, zmin), Size(400, 200), theta * 180 / PI, 0, 360, color_tab_[label], 2);
//     }

//     //imshow("Labels", labels);
//     return;
        
// }

// void drawUserSkeletons() { 
//     if (!kinect_info.user_labels_img.data)
//         return;

//     std::set<uchar> labels;
//     uchar* plabel = kinect_info.user_labels_img.data;
//     for (int j=0; j<kinect_info.user_labels_img.cols * kinect_info.user_labels_img.rows; j++, plabel++) 
//         if (*plabel > 0 && labels.find(*plabel) == labels.end())
//             labels.insert(*plabel);
        
//     if (!labels.size())
//         return;


//     RNG rng(12345);
//     for (std::set<uchar>::const_iterator it=labels.begin(); it != labels.end(); it++) {
//         Mat user, dst, dst8x, dst8y;
//         Mat1b dst8 = Mat1b::zeros(kinect_info.user_labels_img.size());
//         Mat1b dst8c = Mat1b::zeros(kinect_info.user_labels_img.size());
//         int l = (int) *it;
//         inRange(kinect_info.user_labels_img, l, l, user);    
//         threshold(user, user, l, 255, THRESH_BINARY);
//         dilate(user, user, Mat(), Point(-1,-1), 10);

// 	// vector < vector<Point> > contours;
//         // vector<Vec4i> hierarchy;

//         // findContours( user, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
//         // for( int i = 0; i< contours.size(); i++ ) {
//         //     if (contours[i].size() > 100) 
//         //         drawContours( dst8, contours, i, Scalar(255), CV_FILLED, 8, hierarchy, 0, Point() );
//         // }
//         //

//         distanceTransform(user, dst, CV_DIST_L1, 3);
//         dst.convertTo(dst8, CV_8UC1);

//         //Canny(dst8, dst8, low_thresh, high_thresh, 5);
//         Scharr( dst8, dst8x, CV_8UC1, 1, 0 );   
//         Scharr( dst8, dst8y, CV_8UC1, 0, 1 );   
//         //bitwise_or(dst8x, dst8y, dst8c);

//         threshold(dst8y, dst8c, 1, 255, THRESH_BINARY);

//         // int off = 5;
//         // uchar* pdst = dst8.data + off*dst8.step;
//         // int count;
//         // for (int i=off; i<dst8.rows-off; i++, pdst+=dst8.step) { 
//         //     for (int j=off; j<dst8.cols-off; j++) { 
//         //         count = 0;

//         //         uchar u = pdst[j - off*dst8.step];
//         //         uchar ul = pdst[j - off*dst8.step - off];
//         //         uchar ur = pdst[j - off*dst8.step + off];

//         //         uchar d = pdst[j + off*dst8.step];
//         //         uchar dl = pdst[j + off*dst8.step - off];
//         //         uchar dr = pdst[j + off*dst8.step + off];

//         //         uchar l = pdst[j - off];
//         //         uchar r = pdst[j + off];
//         //         uchar v = pdst[j];
                
//         //         // if (v > u) count++;
//         //         // if (v > ul) count++;
//         //         // if (v > ur) count++;

//         //         // if (v > d) count++;
//         //         // if (v > dl) count++;
//         //         // if (v > dr) count++;

//         //         // if (v > r) count++;
//         //         // if (v > l) count++;

//         //         // if (count >= 5)
//         //         //  dst8c(i,j) = 255;

//         //         // if ((v > u && v >= d) || (v >= u && v > d))
//         //         //     dst8c(i,j) = 255;
//         //         // if ((v > r && v >= l) || (v >= r && v > l))
//         //         //     dst8c(i,j) = 255;
//         //         if ((v > ul && v > dr) && (v > ur && v > dl))
//         //          dst8c(i,j) = 255;
//         //     }
//         // }

//         std::stringstream ss;
//         ss << "skel " << l;
//         //imshow(ss.str(), dst8c);
//         imshow("skel", dst8c);
//     }
        

// }

// void findUser(pcl::PointCloud<pcl::PointXYZRGB>& user_pcl, const pcl::PointCloud<pcl::PointXYZRGB>& pcl) { 
//     if (!kinect_info.user_labels_img_debug.data)
//         return;

//     std::set<int> labels;
//     uchar* p = kinect_info.user_labels_img.data;
//     for (int j=0; j<kinect_info.user_labels_img.rows * kinect_info.user_labels_img.cols; j++, p++)
//         if (*p > 0)
//             labels.insert((int)*p);

//     if (!labels.size())
//         return;

//     // Find mean
//     std::vector<double> sumx(16, 0), sumy(16, 0), counts(16, 0);
//     p = kinect_info.user_labels_img.data;
//     for (int i=0; i<kinect_info.user_labels_img.rows; i++, 
//              p+=kinect_info.user_labels_img.step)
//         for (int j=0; j<kinect_info.user_labels_img.cols; j++)
//             if (p[j])
//                 counts[(int)p[j]]++, sumx[(int)p[j]] += j, sumy[(int)p[j]] += i;
            
//     int w = kinect_info.user_labels_img.cols, h = kinect_info.user_labels_img.rows;
//     std::map<int, Point2f> means;
//     for (int j=0; j<sumx.size(); j++)
//         if (sumx[j] != 0)
//             means.insert(std::make_pair(j, Point2f(sumx[j] / counts[j], 
//                                                    sumy[j] / counts[j])));

    

//     Mat _dst;
//     int scale = 256 / labels.size();
//     Mat _dst8 = kinect_info.user_labels_img * scale;
//     cvtColor(_dst8, _dst, CV_GRAY2RGB);

//     for (std::map<int, Point2f>::const_iterator it = means.begin(); it != means.end(); it++)
//         ellipse(_dst, it->second, Size(30, 30), 0 * 180 / PI, 0, 360, color_tab_[it->first], 2);
//     // for (int l=min; l<=max; l++) { 
//     //     Mat dst8;
//     //     inRange(kinect_info.user_labels_img, l, l, dst8);
        
//     //     Moments m = moments(dst8);
//     //     double theta = 0.5 * atan2(2*m.mu11,(m.mu20 - m.mu02));
//     //     Point c(m.m10 / m.m00, m.m01 / m.m00);

//     //     float mu20 = m.mu20 / m.m00, mu02 = m.mu02 / m.m00, 
//     //         mu11 = m.mu11 / m.m00;
//     //     float inner = 0.5 * sqrt( 4 * mu11*mu11 + (mu20 - mu02)*(mu20 - mu02) );
//     //     float a = 0.5 * (mu20 + mu02) + inner; 
//     //     float b = 0.5 * (mu20 + mu02) - inner;

//     //     std::cerr << " a, b " << l << " " << a << " " << b << std::endl;
//     //     // threshold(dst8, dst8, l, 255, THRESH_BINARY);
//     //     // cvtColor(dst8, dst, CV_GRAY2RGB);
//     //     ellipse(_dst, c, Size(200, 50), theta * 180 / PI, 0, 360, color_tab_[l], 2);

//     //     //imshow("dst", dst);
//     // }
//     imshow("dst", _dst);    

// }
// void splitPCL(std::map<int, pcl::PointCloud<pcl::PointXYZRGB> >& pcl_segments, 
//               const pcl::PointCloud<pcl::PointXYZRGB>& pcl) { 
//     if (!kinect_info.user_labels_img_debug.data)
//         return;

//     int idx, u, v;
//     for (int i=0; i<nvert; i++) { 
//         for (int j=0; j<nhoriz; j++) { 
//             idx = i*nhoriz + j;
//             v = i*deci_r, u = j*deci_r;
//             int label = kinect_info.user_labels_img.data[v*WIDTH + u];
//             if (label)
//                 pcl_segments[label].points.push_back(pcl.points[idx]);
//         }
//     }
//     return;                
// }
