/*
*/

#include <set>

// LCM includes
#include <perception_opencv_utils/calib_utils.hpp>
#include <perception_opencv_utils/imshow_utils.hpp>
#include <perception_opencv_utils/math_utils.hpp>
#include <perception_opencv_utils/plot_utils.hpp>
#include <perception_opencv_utils/color_utils.hpp>
#include <perception_opencv_utils/imgproc_utils.hpp>

#include "kinect_opencv_utils.h"
#include <lcmtypes/kinect_image_msg_t.h>

#include <lcmtypes/kinect_frame_msg_t.h>
#include <kinect/kinect-utils.h>
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>

#include "CompressiveTracker.h"

// #include <pcl/registration/correspondence_rejection_sample_consensus.h>

#define SHOW_ALL_RECTS_BY_ONE 0
using namespace cv;

typedef struct _state_t state_t;
struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    BotParam *param;
    BotFrames *frames;
};
state_t * state = NULL;


struct MouseEvent {
    MouseEvent() { event = -1; buttonState = 0; }
    Point pt;
    int event;
    int buttonState;
};
MouseEvent mouse;

// UI selection of desired object
Rect selection;
Point origin;
bool selectObject = false;
int trackObject = 0;

// tld::TLD* tldtracker;
CompressiveTracker ct;

cv::Mat1b currMask;
cv::Rect currROI;

Mat prev_gray;

static void usage(const char* progname)
{
  fprintf (stderr, "Usage: %s [options]\n"
                   "\n"
                   "Options:\n"
                   "  -l URL    Specify LCM URL\n"
                   "  -h        This help message\n", 
                   g_path_get_basename(progname));
  exit(1);
}

struct Frame { 
    kinect_data kinect;
    cv::Rect BB;
};

void ct_track(Frame& frame) { 
    Mat img = frame.kinect.getRGB().clone();
    Mat gray = frame.kinect.getGray().clone();
    if (trackObject) { 
        if (trackObject < 0) { 
            trackObject = 1;

            // CTselection
            ct.init(gray, selection);

            rectangle(img, selection, Scalar(0,0,255));
            opencv_utils::imshow("CT First Frame", img);

        } else  {
            ct.processFrame(gray, selection);

            rectangle(img, selection, Scalar(0,0,255));
            opencv_utils::imshow("CT", img);

            std::cerr << "Processing frame: " << selection << std::endl;

        }
        
    }
}



static void onMouse(int event, int x, int y, int flags, void* userdata) {
    MouseEvent* data = (MouseEvent*)userdata;
    if (selectObject) {
        selection.x = MIN(x, origin.x);
        selection.y = MIN(y, origin.y);
        selection.width = std::abs(x - origin.x);
        selection.height = std::abs(y - origin.y);
        selection &= Rect(0, 0, WIDTH, HEIGHT);
    }

    switch (event) {
    case CV_EVENT_LBUTTONDOWN:
        origin = Point(x, y);
        selection = Rect(x, y, 0, 0);
        selectObject = true;
        break;
    case CV_EVENT_LBUTTONUP:
        selectObject = false;
        trackObject = -1;
        break;
    }
    return;
}

void  INThandler(int sig)
{
    // lcm_destroy(state->lcm);
    printf("Exiting\n");
    // exit(0);
}

static void on_kinect_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                            const kinect_frame_msg_t *msg, 
                            void *user_data ) {
    
    Frame frame;
    frame.kinect.update(msg);

    ct_track(frame);

    Mat img = frame.kinect.getRGB().clone();
    if (selectObject && selection.width > 0 && selection.height > 0) {
        Mat roi(img, selection);
        bitwise_not(roi, roi);
    }

    imshow("Kinect RGB", img);    
    return;
}

int main(int argc, char** argv)
{
    // g_thread_init(NULL);
    setlinebuf (stdout);
    state = (state_t*) calloc(1, sizeof(state_t));

    // Param server, botframes
    state->lcm =  bot_lcm_get_global(NULL);
    // state->mainloop = g_main_loop_new( NULL, FALSE );  
    state->param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->param);

    // Subscribe to the kinect image
    namedWindow( "Kinect RGB" );
    kinect_frame_msg_t_subscribe(state->lcm, "KINECT_FRAME", on_kinect_image_frame, (void*)state);
    setMouseCallback("Kinect RGB", onMouse, &mouse);

    // Install signal handler to free data.
    signal(SIGINT, INThandler);

    fprintf(stderr, "Starting Main Loop\n");

    int lcm_fd = lcm_get_fileno(state->lcm);
    fd_set fds;

    // wait a limited amount of time for an incoming message
    struct timeval timeout = { 
        0,  // seconds
        10000   // microseconds
    };

    while(1) { 
        unsigned char c = cv::waitKey(10) & 0xff;

        FD_ZERO(&fds);
        FD_SET(lcm_fd, &fds);

        // setup the LCM file descriptor for waiting.
        int status = select(lcm_fd + 1, &fds, 0, 0, &timeout);
        if(status && FD_ISSET(lcm_fd, &fds)) {
            // LCM has events ready to be processed.
            lcm_handle(state->lcm);
        }

        if (c == 'q') 
            break;      



    }

    lcm_destroy(state->lcm);

    
    return 0;
}
