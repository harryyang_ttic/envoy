#ifndef _HIGH_TRACKER
#define _HIGH_TRACKER
#endif

#include <unistd.h>

#ifdef __APPLE
#include <GL/gl.h>
#else
#include <GL/gl.h>
#endif


#include <er_carmen/carmen.h>
#include <interfaces/localize3d_messages.h>
#include <gsl/gsl_fit.h>
#include <image_utils/pixels.h>
#include <image_utils/jpeg.h>


#include <check_gridmap/check_gridmap.h>
#include <bot_frames/bot_frames.h>
/* global variables */

//LCM stuff
#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <bot_vis/bot_vis.h>
//bot param stuff
#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>

#include <lcmtypes/er_lcmtypes.h>
#include <er_lcmtypes/lcm_channel_names.h>

#include <interfaces/map3d_interface.h>


//servo commands
#include <lcmtypes/dynamixel_cmd_list_t.h>
#include <lcmtypes/dynamixel_status_list_t.h>
#include <dynamixel/dynamixel.h>

#include "person_messages.h"
#include "person_interface.h"
#include "person_localizecore.h"

#define MOVED_PERSON_THRESHOLD 3
#define REALLOCATE_TIME 0.5//1.0  //no of seconds to wait to reallocate
#define MOVED_OBS_TIME 5.0  //no of seconds to wait to reallocate
#define CLEARING_RADIUS 0.3 //radius to clear around person estimate
#define SEGMENT_GAP 0.1//0.06//0.03 //gap used in deciding segments 
#define PERSON_OBS_RADIUS 0.4
#define MAX_PERSON_HISTORY 60//40//40 //20//80
#define REALLOCATION_DISTANCE 5.0 //this is the distance beyond which filters get pruned regardless of observations

#define MAX_REL_PERSON_HISTORY 10

enum FollowingState{
    FOLLOWING, //person is being tracked and followed - issuing navigation commands to the navigator 
    IDLE //not following (can still have a track on the person)
};

enum TrackingState{
    FOUND, //person has been found
    LOOKING, //looking for person (should look for a particular time and give an error message if we cant find the person
    NOT_LOOKING,//system is initialized and has no track on the person 
    LOST // system has lost track of the person 
};

typedef struct _point_avg_object{
    carmen_point_t *history;
    carmen_point_t *temp_history;
    int max_size;
    int current_size;
} point_avg_object;

typedef struct _double_avg_object{
    double *history;
    double *temp_history;
    int max_size;
    int current_size;
} double_avg_object;

typedef struct _person_cam_renderer {
    bot_core_image_t *last_image;
    BotGlTexture *texture;  
    uint8_t *uncompresed_buffer;
    int uncompressed_buffer_size;
    int width, height;
} person_cam_renderer_t;

typedef struct{
    int id; 
    erlcm_point_t local_pos;
    erlcm_point_t gpos;
    double rel_vel;
    double tv_mag; 
    double rv_mag; 
    double heading; 
    carmen3d_localize_particle_filter_p person_filter;
    double time_since;
    double time_created;
    int candidate_counts; 

    int found_obs_last; //obs was associated in the last check
    double time_since_obs; 
    double time_since_motion; 
    int motion_count; 

    point_avg_object relative_person_history;
    point_avg_object global_person_history; //larger history kept to extrapolate heading 
    point_avg_object global_person_history_small; //smaller history - kept for actual velocity 
    double_avg_object person_heading_history;
} person_track_t;

typedef struct{
    person_track_t *people_tracks; 
    carmen3d_localize_summary_p people_summary;
    int no_people;
    int max_ind; //index for the filter 
    int actual_filter_size;
    int active_person_ind;   
} people_list_t; 

typedef struct {
    lcm_t * lcm;
    BotParam *b_server; 

    check_gridmap_t* check_gridmap;
    int tourguide_mode;
    int create_moving; 
    int use_classifier;
    carmen3d_localize_param_t param;

    erlcm_map_t current_map;

    people_list_t people_list; //clean up - we will do this later 

    int summary_active;
    int first_person_obs;

    enum FollowingState following_state;  
    enum TrackingState tracking_state;
    double started_looking_time; 

    double kinect_x;
    double kinect_y;
    int64_t kinect_lost_time; 

    int use_planar_laser; 
    int multi_people_tracking;
    int use_map;

    //laser parameters - ideally this should be done for all relavent lasers 
    carmen_point_t std;    

    int new_person_detection;
    int first_person_detection;

    int verbose;

    int high_laser;
    int pruning_mode; 

    laser_offset_t front_laser_offset;
    laser_offset_t rear_laser_offset;

    int use_kinect; 

    erlcm_robot_laser_t *robot_fl;
    erlcm_robot_laser_t *robot_rl;

    bot_core_planar_lidar_t *planar_fl;
    bot_core_planar_lidar_t *planar_rl;
  

    erlcm_raw_odometry_msg_t* odom_msg;

    int have_front_laser;
    int have_rear_laser;

    carmen_point_t last_robot_position;
    carmen_point_t person_position;

    point_avg_object robot_history; //smaller history - kept for actual velocity 

    carmen_point_t global_robot_pose; 

    person_cam_renderer_t *cr;

    bot_lcmgl_t *lcmgl_kp_position;
    bot_lcmgl_t *lcmgl_moved_legs;
    bot_lcmgl_t *lcmgl_leg_obs;
    bot_lcmgl_t *lcmgl_act_pos;
    bot_lcmgl_t *lcmgl_person_history;
    bot_lcmgl_t *lcmgl_velocity; 
    bot_lcmgl_t *lcmgl_person_zone;
    bot_lcmgl_t *lcmgl_person;
    
} state_t;

void print_tracking_state(state_t *s){
    if(s->tracking_state == LOST){
        fprintf(stderr,"Tracking State : Person Lost");
    }
    if(s->tracking_state == LOOKING){
        fprintf(stderr,"Tracking State : Person Looking");
    }
    if(s->tracking_state == FOUND){
        fprintf(stderr,"Tracking State : Person Found");
    }
    if(s->tracking_state == NOT_LOOKING){
        fprintf(stderr,"Tracking State : Not Looking for person  ");
    }
}

void update_tracking_state(enum TrackingState new_state, state_t *s);

void publish_person_status_to_kinect(state_t *s){
    
    erlcm_kinect_person_tracker_status_t msg; 
    msg.utime = bot_timestamp_now();


    msg.sender = ERLCM_KINECT_PERSON_TRACKER_STATUS_T_SENDER_LASER;
    if(s->tracking_state == LOOKING){
        msg.status = ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_LOOKING;
    }
    else if(s->tracking_state == FOUND){
        msg.status = ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_TRACKING;          
    }
    else if(s->tracking_state == LOST){
        msg.status = ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_LOST;
    }
    else if(s->tracking_state == NOT_LOOKING){
        msg.status = ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_LOST;
    }
    else{
        msg.status = ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_LOST;
    }
    
    if(s->people_list.active_person_ind >= 0){
        //should not hard code 
        double dy = s->people_list.people_summary[s->people_list.active_person_ind].mean.y - 0.15; 
        double dx = s->people_list.people_summary[s->people_list.active_person_ind].mean.x - 0.14; 

        double theta = atan2(dy,dx); 
        
        msg.theta = theta; 
        msg.r = hypot(dx,dy);
    }
    else{
        msg.theta = 0; 
        msg.r = 0;

    }

    erlcm_kinect_person_tracker_status_t_publish(s->lcm, "LASER_PSERSON_STATUS_SERVO", &msg);   
}

static void
kinect_init_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                     const erlcm_kinect_person_tracker_status_t *msg, void *user_data)
{
    state_t *s = (state_t *)user_data;

    bot_lcmgl_t *lcmgl = s->lcmgl_kp_position; 

    if(msg->status == ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_TRACKING){
        if(s->people_list.active_person_ind == -1){
            if(msg->r != -1){

                double x = 0.14 + msg->r* cos(msg->theta);
                double y = 0.15 + msg->r* sin(msg->theta);

                fprintf(stderr, "Kinect : r : %f Theta : %f => (%f,%f)\n", 
                        msg->r, msg->theta, x, y);
                
                s->kinect_x = x;
                s->kinect_y = y;

                update_tracking_state(LOOKING,s);

                if(lcmgl){
                    float rtheta = s->global_robot_pose.theta;
                    double pos[3];
                    float leg_x = s->global_robot_pose.x + s->kinect_x * cos(rtheta) - s->kinect_y * sin(rtheta);
                    float leg_y = s->global_robot_pose.y + s->kinect_x * sin(rtheta) + s->kinect_y *cos(rtheta);

                    lcmglLineWidth (5);
                    lcmglColor3f(1.0, 1.0, 0.0);
                    lcmglCircle(pos, 0.5);

                    bot_lcmgl_switch_buffer (lcmgl);
                }
            }
        }
    }

    //for now atleast its not publishing LOST status (always looking or tracking
    
    if(msg->status == ERLCM_KINECT_PERSON_TRACKER_STATUS_T_STATUS_LOST){
        //Do we trust the kinect to not loose the person ??

        if(s->people_list.active_person_ind >= 0){
            update_tracking_state(LOST,s);
        }
    }

}

//lcm message publishers
void publish_speech_msg(char* property, state_t *s)
{
    erlcm_speech_cmd_t msg;
    msg.utime = bot_timestamp_now();
    msg.cmd_type = "FOLLOWER";
    msg.cmd_property = property;
    erlcm_speech_cmd_t_publish(s->lcm, "PERSON_TRACKER", &msg);
}

void publish_speech_following_msg(char* property, state_t *s)
{
    erlcm_speech_cmd_t msg;
    msg.utime = bot_timestamp_now();
    msg.cmd_type = "FOLLOWER";
    msg.cmd_property = property;
    erlcm_speech_cmd_t_publish(s->lcm, "PERSON_TRACKER", &msg);
}

void publish_speech_tracking_msg(char* property, state_t *s)
{
    erlcm_speech_cmd_t msg;
    msg.utime = bot_timestamp_now();
    msg.cmd_type = "TRACKER";
    msg.cmd_property = property;
    erlcm_speech_cmd_t_publish(s->lcm, "PERSON_TRACKER", &msg);
}

//publishes both planar lidar (for the person navigator and robot_laser (for slam)
void robot_laser_pub(char * CHAN, carmen_robot_laser_message * robot, state_t *s)
{
    erlcm_robot_laser_t msg;
    memset(&msg, 0, sizeof(msg));
    msg.laser.rad0 = robot->config.start_angle;
    msg.laser.radstep = robot->config.angular_resolution;
    msg.laser.nranges = robot->num_readings;
    msg.laser.ranges = robot->range;
    msg.pose.pos[0] = robot->robot_pose.x;
    msg.pose.pos[1] = robot->robot_pose.y;
    msg.pose.pos[2] = s->param.front_laser_offset; //this is added to the front laser - will cause an error in other laser configurations
    double rpy[3] = { 0, 0, robot->robot_pose.theta };
    bot_roll_pitch_yaw_to_quat(rpy, msg.pose.orientation);
    msg.utime = robot->timestamp * 1e6;
    msg.laser.utime = msg.utime;
    msg.pose.utime = msg.utime;
    msg.cov.x = 1;
    msg.cov.xy = 0;
    msg.cov.y = 1;
    msg.cov.yaw = .1;
    //bot_core_planar_lidar_t_publish(s->lcm,"HOKUYO_FORWARD",&msg.laser);
    erlcm_robot_laser_t_publish(s->lcm,CHAN,&msg);
}

void init_point_avg_object(point_avg_object *object, int size){
    object->history = calloc(size,sizeof(carmen_point_t));
    object->temp_history = calloc(size,sizeof(carmen_point_t));
    object->max_size = size;
    object->current_size = 0;
}
void init_double_avg_object(double_avg_object *object, int size){
    object->history = calloc(size,sizeof(double));
    object->temp_history = calloc(size,sizeof(double));
    object->max_size = size;
    object->current_size = 0;
}

void free_point_avg_object(point_avg_object *object){
    free(object->history);
    free(object->temp_history);
}

void free_double_avg_object(double_avg_object *object){
    free(object->history);
    free(object->temp_history);
}

void reset_point_avg_object(point_avg_object *object){
    object->current_size = 0;
}
void reset_double_avg_object(double_avg_object *object){
    object->current_size = 0;
}

void add_point_to_history_object(point_avg_object *object,
                                 carmen_point_t new_value){
  
    memcpy(object->temp_history, object->history, (object->max_size -1)*sizeof(carmen_point_t));
    memcpy(&(object->history[1]), object->temp_history, (object->max_size -1)*sizeof(carmen_point_t));
    object->history[0].x = new_value.x;
    object->history[0].y = new_value.y;
    object->history[0].theta = new_value.theta;
    object->current_size = fmin(++object->current_size, object->max_size);
}

void add_double_to_history_object(double_avg_object *object,
                                  double new_value){
  
    memcpy(object->temp_history, object->history, (object->max_size -1)*sizeof(double));
    memcpy(&(object->history[1]), object->temp_history, (object->max_size -1)*sizeof(double));
    object->history[0] = new_value;
    object->current_size = fmin(++object->current_size, object->max_size);
}

carmen_point_t get_average_point_from_history_object(point_avg_object *object, int start_ind, int end_ind){
    carmen_point_t average = {.0,.0, .0};
    
    int his_len = (end_ind - start_ind) + 1;  
  
    if(his_len >0){
        for(int i=fmin(start_ind,object->max_size -1) ; i <= fmin(end_ind, object->max_size -1); i++){
            average.x += object->history[i].x;
            average.y += object->history[i].y;
            average.theta += object->history[i].theta;
        }
        average.x /= his_len;
        average.y /= his_len;
        average.theta /= his_len;    
    }

    return average; 
}

double get_average_double_from_history_object(double_avg_object *object, int start_ind, int end_ind){
    double average = .0;

    int his_len = (end_ind - start_ind) + 1;  
  
    if(his_len >0){
        for(int i=fmin(start_ind,object->max_size -1) ; i <= fmin(end_ind, object->max_size -1); i++){
            average += object->history[i];
        }
        average /= his_len;
    }

    return average; 
}

void get_bestfit_from_history_object(point_avg_object *object, int start_ind, int end_ind, double *c0, double *c1){
    carmen_point_t average = {.0,.0, .0};
    
    int his_len = (end_ind - start_ind) + 1;   

    if(his_len >1){
        double *x = calloc(his_len, sizeof(double));
        double *y = calloc(his_len, sizeof(double));

        double cov00, cov01, cov11, sumsq; //c0, c1, 

        for(int i=0;i< his_len; i++){
            x[i] = object->history[i+start_ind].x;
            y[i] = object->history[i+start_ind].y;
        }

        gsl_fit_linear (x,1, y,1, his_len, c0, c1, &cov00, &cov01, &cov11, &sumsq);
        free(x);
        free(y);
    }
}


void robot_rearlaser_pub(char * CHAN, carmen_robot_laser_message * robot, state_t *s)
{
    erlcm_robot_laser_t msg;
    memset(&msg, 0, sizeof(msg));
    msg.laser.rad0 = robot->config.start_angle;
    msg.laser.radstep = robot->config.angular_resolution;
    msg.laser.nranges = robot->num_readings;
    msg.laser.ranges = robot->range;
    msg.pose.pos[0] = robot->robot_pose.x;
    msg.pose.pos[1] = robot->robot_pose.y;
    msg.pose.pos[2] = s->param.rear_laser_offset; //this is added to the front laser - will cause an error in other laser configurations
    double rpy[3] = { 0, 0, robot->robot_pose.theta };
    bot_roll_pitch_yaw_to_quat(rpy, msg.pose.orientation);
    msg.utime = robot->timestamp * 1e6;
    msg.laser.utime = msg.utime;
    msg.pose.utime = msg.utime;
    msg.cov.x = 1;
    msg.cov.xy = 0;
    msg.cov.y = 1;
    msg.cov.yaw = .1;
    erlcm_robot_laser_t_publish(s->lcm,CHAN,&msg);
}

void add_point_to_history(carmen_point_t *history, carmen_point_t *temp_history, carmen_point_t new_value){
  
    memcpy(temp_history, history, (MAX_PERSON_HISTORY-1)*sizeof(carmen_point_t));
    memcpy(&history[1], temp_history, (MAX_PERSON_HISTORY-1)*sizeof(carmen_point_t));
    history[0].x = new_value.x;
    history[0].y = new_value.y;
    history[0].theta = new_value.theta;
}

carmen_point_t get_average_point_from_history(carmen_point_t *history, int start_ind, int end_ind){
    carmen_point_t average = {.0,.0, .0};
    
    int his_len = (end_ind - start_ind) + 1;  
  
    if(his_len >0){
        for(int i=fmin(start_ind,MAX_PERSON_HISTORY-1) ; i <= fmin(end_ind, MAX_PERSON_HISTORY-1); i++){
            average.x += history[i].x;
            average.y += history[i].y;
            average.theta += history[i].theta;
        }
        average.x /= his_len;
        average.y /= his_len;
        average.theta /= his_len;    
    }

    return average; 
}

void get_bestfit_from_history(carmen_point_t *history, int start_ind, int end_ind, double *c0, double *c1){
    carmen_point_t average = {.0,.0, .0};
    
    int his_len = (end_ind - start_ind) + 1;   

    if(his_len >1){
        double *x = calloc(his_len, sizeof(double));
        double *y = calloc(his_len, sizeof(double));

        double cov00, cov01, cov11, sumsq; //c0, c1, 

        for(int i=0;i< his_len; i++){
            x[i] = history[i+start_ind].x;
            y[i] = history[i+start_ind].y;
        }

        gsl_fit_linear (x,1, y,1, his_len, c0, c1, &cov00, &cov01, &cov11, &sumsq);

    }
}

void add_double_to_history(double *history, double *temp_history, double new_value){
  
    memcpy(temp_history, history, (MAX_PERSON_HISTORY-1)*sizeof(double));
    memcpy(history+1, temp_history, (MAX_PERSON_HISTORY-1)*sizeof(double));
    history[0] = new_value;
}


double get_average_double_from_history(double *history, int start_ind, int end_ind){
    double average = .0;
    
    for(int i=fmin(start_ind,MAX_PERSON_HISTORY-1) ; i <= fmin(end_ind, MAX_PERSON_HISTORY-1); i++){
        average += history[i];
    }
  
    int his_len = (end_ind - start_ind) + 1;

    if(his_len >0){
        average /= his_len;
        return average;
    }
    else{
        return .0;
    }
}

void publish_mission_control(lcm_t *lc, int type){
    erlcm_mission_control_msg_t msg;
    msg.utime = bot_timestamp_now();
    msg.type = type;
    erlcm_mission_control_msg_t_publish(lc, MISSION_CONTROL_CHANNEL, &msg);
}

//State updates based on speech commands 


//tracking commands are not being handled by this - it is now being handled by the person navigator
void update_following_state(enum FollowingState new_state, state_t *s) {
    if(new_state==IDLE){//should stop the wheelchair
        if(s->following_state == FOLLOWING){
        }
        else{
        }

        s->following_state = new_state;
        //issue comand for the robot to stop - and the navigator to clear 
        //current goal - otherwise it will keep using the old goal 
    }
    else if(new_state==FOLLOWING){//check if we have a tracking of the person 
        if(s->tracking_state == FOUND){//if person is tracked the start following
            s->following_state = new_state;

            //why are we handling this in the tracker
            publish_speech_following_msg("FOLLOWING", s);
        }
        else{//we dont have an estimate of the person 
            s->following_state = IDLE;
            //- i.e. there is no path to follow the person - we should use this if navigator issues such a message
        }
    }
}


void send_tracking_changed_status(state_t *s){
    erlcm_person_tracking_status_t msg;
    msg.utime = bot_timestamp_now();
    if(s->tracking_state == LOST){
        msg.status = ERLCM_PERSON_TRACKING_STATUS_T_STATUS_LOST;
    }
    else if(s->tracking_state == FOUND){
        msg.status = ERLCM_PERSON_TRACKING_STATUS_T_STATUS_TRACKING;
    }
    else if(s->tracking_state == LOOKING){
        msg.status = ERLCM_PERSON_TRACKING_STATUS_T_STATUS_LOOKING;
    }
    else if(s->tracking_state == NOT_LOOKING){
        msg.status = ERLCM_PERSON_TRACKING_STATUS_T_STATUS_IDLE;
    }
    msg.sender = ERLCM_PERSON_TRACKING_STATUS_T_SENDER_LASER; 
    
    erlcm_person_tracking_status_t_publish(s->lcm, "LASER_PERSON_STATUS_CHANGE", &msg);
}

void update_tracking_state(enum TrackingState new_state, state_t *s){
    int status_change = 0;

    if(new_state != s->tracking_state){
        status_change = 1;
    }
    s->tracking_state = new_state;
    if(new_state==LOST){
        s->people_list.active_person_ind = -1;
        //publish_speech_tracking_msg("LOST", s);
        update_following_state(IDLE, s);
        if(status_change)
            send_tracking_changed_status(s);
    }else if (new_state == FOUND){
        //publish_speech_tracking_msg("FOUND", s);
        if(status_change)
            send_tracking_changed_status(s);
        //we should not start following immediately - should follow only 
        //when we get a start following command
    }else if (new_state == LOOKING){
        //reset the person filer - maybe even clear up the current person filter??
        s->people_list.active_person_ind = -1;
        //publish_speech_tracking_msg("LOOKING", s); 
        if(status_change)
            send_tracking_changed_status(s);
        update_following_state(IDLE, s); //we need to update the tracking state because this looking message may be 
        //generated becaue the guide wants the chair to reaquire him in the case of the chair following a wrong target
    }  
}

void querry_tracking_state(state_t *s){
    if(s->tracking_state== FOUND){
        publish_speech_tracking_msg("SEE_YES", s);
    }
    else if((s->tracking_state== LOST) ||(s->tracking_state== NOT_LOOKING)){
        publish_speech_tracking_msg("SEE_NO", s);
    }
    else if(s->tracking_state== LOOKING){
        publish_speech_tracking_msg("SEE_LOOKING", s);
    }    
}

void initialize_filter(carmen3d_localize_particle_filter_p pfilter,
                       carmen_point_t mean, carmen_point_t std)
{
    carmen_localize_initialize_particles_gaussian_person(pfilter,
                                                         mean,
                                                         std);
}

//published at the end of each laser message - for the person navigator
void publish_lcm_heading_goal(state_t *s){ 

    erlcm_goal_heading_msg_t msg;
    msg.utime = bot_timestamp_now();
    if(s->people_list.active_person_ind !=-1){  //We have active person filter  
        float person_x = s->people_list.people_summary[s->people_list.active_person_ind].mean.x;
        float person_y = s->people_list.people_summary[s->people_list.active_person_ind].mean.y;
        //check condition to start following a person
        float act_dist = hypot(person_x,person_y);
        float act_angle = atan2(person_y,person_x);
        msg.target_heading = act_angle;
        if(act_dist >= 0.75){//heading restriction removed from here
            msg.tracking_state = 0; //0 - suitable for following 
        }
        else{
            msg.tracking_state = 1;
        }
    }
    else{
        msg.target_heading = 0.0;
        msg.tracking_state = -1;
    }
    erlcm_goal_heading_msg_t_publish(s->lcm, "GOAL_HEADING", &msg);
}

void get_global_point(double xy[2], double gxy[2], state_t *s){
    double ctheta = cos(s->global_robot_pose.theta);
    double stheta = sin(s->global_robot_pose.theta);
    gxy[0] = s->global_robot_pose.x + xy[0] * ctheta - xy[1] * stheta;
    gxy[1] = s->global_robot_pose.y + xy[0] * stheta + xy[1] * ctheta;
}

void send_servo_speed(state_t *s, int32_t max_speed, int32_t goal_position){
    //set the velocity of the servo 

    dynamixel_cmd_list_t servo_msg; 
    servo_msg.utime = bot_timestamp_now();
    servo_msg.ncommands = 1;

    servo_msg.commands = (dynamixel_cmd_t *) calloc(
                                                    1,
                                                    sizeof(dynamixel_cmd_t));

    int32_t max_torque = 300;
    int8_t torque_enable = 0;
  
    dynamixel_cmd_t * cmd = servo_msg.commands;
  
    cmd->servo_id = 0;
    cmd->torque_enable = torque_enable;
    cmd->goal_position = goal_position;
    cmd->max_speed = max_speed;
    cmd->max_torque = max_torque;

    dynamixel_cmd_list_t_publish(s->lcm,
                                 "DYNAMIXEL_COMMAND",
                                 &servo_msg);
  
    free(servo_msg.commands); 
}

void print_guide_msg(double time, state_t *s)  //people message - carmen message published relative to the robot 
//- for every robot laser message
{
    if(!s->verbose){
        return;
    }
    fprintf(stderr, "------------------ Person Status ----------------\n");
    fprintf(stderr, "---------- Person Ind : %d\n", s->people_list.active_person_ind);
    
    for(int i=0; i < s->people_list.no_people; i++){
        fprintf(stderr, "[%d] Last Obs : %f Motion : %f M count : %d (%f,%f) : %f Vel : %f,%f Closing Vel : %f\n", 
                s->people_list.people_tracks[i].id, 
                time - s->people_list.people_tracks[i].time_since, 
                time - s->people_list.people_tracks[i].time_since_motion, 
                s->people_list.people_tracks[i].motion_count,
                s->people_list.people_tracks[i].local_pos.x, 
                s->people_list.people_tracks[i].local_pos.y, 
                s->people_list.people_tracks[i].heading,
                s->people_list.people_tracks[i].tv_mag,
                s->people_list.people_tracks[i].rv_mag, 
                s->people_list.people_tracks[i].rel_vel
                );
    }
}

void publish_guide_msg_orig(double time, state_t *s)  //people message - carmen message published relative to the robot 
//- for every robot laser message
{
    if(s->verbose){
        fprintf(stderr, "---------- Person Ind : %d\n", s->people_list.active_person_ind);
    }

    int moved_person_threshold = MOVED_PERSON_THRESHOLD; 

    static int prev_heading_value = 0;

    erlcm_people_pos_msg_t p_list;
    p_list.utime = time * 1e6;
    
    //count the filters with enough counts 
    int valid_count = 0;
    int remapped_id = -1;
    for(int i=0; i < s->people_list.no_people; i++){
        if(s->people_list.people_tracks[i].motion_count > moved_person_threshold || (i == s->people_list.active_person_ind)){
            valid_count++;
        }
        if(i == s->people_list.active_person_ind){
            remapped_id = valid_count -1; 
        }
    }
    
    p_list.num_people = valid_count;//s->people_list.no_people;
    p_list.followed_person = remapped_id;//s->people_list.active_person_ind;
  
  
    p_list.people_pos = (erlcm_person_status_t *) calloc(p_list.num_people, sizeof(erlcm_person_status_t));
  
    valid_count = 0;
    for(int i=0; i < s->people_list.no_people; i++){
        
        if(s->people_list.people_tracks[i].motion_count > moved_person_threshold || (i == s->people_list.active_person_ind)){
            p_list.people_pos[valid_count].id = s->people_list.people_tracks[i].id;
            p_list.people_pos[valid_count].last_obs =  s->people_list.people_tracks[i].time_since;
            p_list.people_pos[valid_count].pos[0] = s->people_list.people_tracks[i].local_pos.x;
            p_list.people_pos[valid_count].pos[1] = s->people_list.people_tracks[i].local_pos.y;
            p_list.people_pos[valid_count].closing_velocity = s->people_list.people_tracks[i].rel_vel;
            p_list.people_pos[valid_count].trans_vel_mag = s->people_list.people_tracks[i].tv_mag;
            p_list.people_pos[valid_count].rot_vel_mag = s->people_list.people_tracks[i].rv_mag;
            p_list.people_pos[valid_count].person_heading = s->people_list.people_tracks[i].heading; 

            valid_count++;
        }
    }
  
    erlcm_people_pos_msg_t_publish(s->lcm,"PEOPLE_LIST",&p_list);
    free(p_list.people_pos);

    static erlcm_guide_info_t guide_msg;
    memset(&guide_msg,0,sizeof(erlcm_guide_info_t));
    guide_msg.utime = time * 1e6; //bot_timestamp_now();//people_list.utime; //this is wrong - need to be the observation time 

    if(s->people_list.active_person_ind >=0){
        int ind = s->people_list.active_person_ind; 
        guide_msg.tracking_state = 1;    
        guide_msg.pos[0] = s->people_list.people_tracks[ind].local_pos.x;
        guide_msg.pos[1] = s->people_list.people_tracks[ind].local_pos.y;
        guide_msg.vel_mag = s->people_list.people_tracks[ind].tv_mag;
        guide_msg.rot_vel_mag = s->people_list.people_tracks[ind].rv_mag;
        guide_msg.person_heading = s->people_list.people_tracks[ind].heading;
        guide_msg.closing_velocity = s->people_list.people_tracks[ind].rel_vel;  

    }
    erlcm_guide_info_t_publish(s->lcm,"GUIDE_POS",&guide_msg);  

    //publish status 
    /*erlcm_person_tracking_status_t status_msg;
      status_msg.utime = guide_msg.utime;
      if(s->tracking_state == FOUND){
      status_msg.status = ERLCM_PERSON_TRACKING_STATUS_T_STATUS_TRACKING; 
      }
      else if(s->tracking_state == LOOKING){
      status_msg.status = ERLCM_PERSON_TRACKING_STATUS_T_STATUS_LOOKING; 
      }
      else if(s->tracking_state == NOT_LOOKING){
      status_msg.status = ERLCM_PERSON_TRACKING_STATUS_T_STATUS_IDLE; 
      }
      else if(s->tracking_state == LOST){
      status_msg.status = ERLCM_PERSON_TRACKING_STATUS_T_STATUS_LOST; 
      }

      status_msg.sender = ERLCM_PERSON_TRACKING_STATUS_T_SENDER_LASER; 

      erlcm_person_tracking_status_t_publish(s->lcm, "LASER_PERSON_TRACKER_STATUS", &status_msg);
    */

    publish_person_status_to_kinect(s);

    if(s->people_list.active_person_ind >=0){
        int ind = s->people_list.active_person_ind; 
        //calculate theta

        double theta = atan2(s->people_list.people_tracks[ind].local_pos.y - 0.24,
                             s->people_list.people_tracks[ind].local_pos.x - 0.12);
			 
        //s->people_list.people_tracks[ind].heading; 
    
        if(s->verbose){
            fprintf(stderr, "Theta : %f\n", theta);
            
            if(fabs(theta) > bot_to_radians(150)){
                fprintf(stderr, "Too far back\n"); 
            }
        }
    
        theta = bot_clamp(theta, bot_to_radians(-145), bot_to_radians(145));

        int heading_value = bot_to_degrees(theta) *3.3 + 512;
        
        if(s->verbose){
            fprintf(stderr, "Servo Heading value : %d\n", heading_value);
        }
    
        //if(fabs(heading_value - prev_heading_value) > 10){
        //send_servo_speed(s, 100, heading_value); 
        //}
    
        prev_heading_value = heading_value;
    }
}

void publish_guide_msg(double time, state_t *s)  //people message - carmen message published relative to the robot 
//- for every robot laser message
{
    if(s->verbose){
        fprintf(stderr, "---------- Person Ind : %d\n", s->people_list.active_person_ind);
    }

    static int prev_heading_value = 0;

    erlcm_people_pos_msg_t p_list;
    p_list.utime = time * 1e6;
    
    //count the filters with enough counts 
    int valid_count = 0;
    int remapped_id = -1;

    //do the merging search 
    int *assigned_id = calloc(s->people_list.no_people, sizeof(int));
    memset(assigned_id, -1, s->people_list.no_people * sizeof(int)); 

    for(int i=0; i < s->people_list.no_people; i++){
        if((s->people_list.people_tracks[i].motion_count > MOVED_PERSON_THRESHOLD || (i == s->people_list.active_person_ind)) && assigned_id[i] == -1){
            assigned_id[i] = valid_count;
            
            double min_dist = 1000;
            int min_ind = -1;
            double x0 = s->people_list.people_tracks[i].local_pos.x;
            double y0 = s->people_list.people_tracks[i].local_pos.y;

            for(int j=0; j < s->people_list.no_people; j++){
                if((s->people_list.people_tracks[j].motion_count > MOVED_PERSON_THRESHOLD || (j == s->people_list.active_person_ind)) && assigned_id[j] == -1){
                    double x1 = s->people_list.people_tracks[j].local_pos.x;
                    double y1 = s->people_list.people_tracks[j].local_pos.y;
                    
                    double temp_d = hypot(x1-x0, y1-y0);
                    if(min_dist > temp_d){
                        min_dist = temp_d; 
                        min_ind = j;
                    }                     
                }
            }

            if(min_dist < 0.6){
                assigned_id[min_ind] = valid_count;
            }            
            
            valid_count++;
        }
    }

    fprintf(stderr,"======= Valid Count : %d", valid_count);
    
    p_list.num_people = valid_count;//s->people_list.no_people;
    p_list.followed_person = remapped_id;//s->people_list.active_person_ind;
  
  
    p_list.people_pos = (erlcm_person_status_t *) calloc(p_list.num_people, sizeof(erlcm_person_status_t));
  
    
    for(int k=0; k < valid_count; k++){
        //search through for up to two indecies
        int ind_1 = -1;
        int ind_2 = -1;

        int two_legs = 0;

        for(int i=0; i < s->people_list.no_people; i++){
            if(assigned_id[i] == k){
                if(ind_1 < 0){
                    ind_1 = i;
                }
                else{
                    ind_2 = i;
                    two_legs = 1;
                }
            }
        }

        //how do we assign the velocities etc ??
        p_list.people_pos[k].id = s->people_list.people_tracks[ind_1].id;        
        
        if(two_legs){
            p_list.people_pos[k].last_obs = fmax(s->people_list.people_tracks[ind_1].time_since, 
                                              s->people_list.people_tracks[ind_2].time_since);
            p_list.people_pos[k].pos[0] = (s->people_list.people_tracks[ind_1].local_pos.x + 
                                           s->people_list.people_tracks[ind_2].local_pos.x)/2;
            p_list.people_pos[k].pos[1] = (s->people_list.people_tracks[ind_1].local_pos.y + 
                                           s->people_list.people_tracks[ind_2].local_pos.y)/2;       
            p_list.people_pos[k].moved_count = fmax(s->people_list.people_tracks[ind_1].motion_count, 
                                                    s->people_list.people_tracks[ind_2].motion_count);

        }
        else{
            p_list.people_pos[k].last_obs = s->people_list.people_tracks[ind_1].time_since;
            p_list.people_pos[k].pos[0] = s->people_list.people_tracks[ind_1].local_pos.x;       
            p_list.people_pos[k].pos[1] = s->people_list.people_tracks[ind_1].local_pos.y;

            p_list.people_pos[k].moved_count = s->people_list.people_tracks[ind_1].motion_count;
        }

        p_list.people_pos[k].closing_velocity = s->people_list.people_tracks[ind_1].rel_vel;
        p_list.people_pos[k].trans_vel_mag = s->people_list.people_tracks[ind_1].tv_mag;
        p_list.people_pos[k].rot_vel_mag = s->people_list.people_tracks[ind_1].rv_mag;
        p_list.people_pos[k].person_heading = s->people_list.people_tracks[ind_1].heading;
    }

    free(assigned_id);
  
    erlcm_people_pos_msg_t_publish(s->lcm,"PEOPLE_LIST",&p_list);
    free(p_list.people_pos);

    static erlcm_guide_info_t guide_msg;
    memset(&guide_msg,0,sizeof(erlcm_guide_info_t));
    guide_msg.utime = time * 1e6; 

    if(s->people_list.active_person_ind >=0){
        int ind = s->people_list.active_person_ind; 
        guide_msg.tracking_state = 1;    
        guide_msg.pos[0] = s->people_list.people_tracks[ind].local_pos.x;
        guide_msg.pos[1] = s->people_list.people_tracks[ind].local_pos.y;
        guide_msg.vel_mag = s->people_list.people_tracks[ind].tv_mag;
        guide_msg.rot_vel_mag = s->people_list.people_tracks[ind].rv_mag;
        guide_msg.person_heading = s->people_list.people_tracks[ind].heading;
        guide_msg.closing_velocity = s->people_list.people_tracks[ind].rel_vel;
    }

    erlcm_guide_info_t_publish(s->lcm,"GUIDE_POS",&guide_msg);  

    publish_person_status_to_kinect(s);

    if(s->people_list.active_person_ind >=0){
        int ind = s->people_list.active_person_ind; 
        //calculate theta

        double theta = atan2(s->people_list.people_tracks[ind].local_pos.y - 0.24,
                             s->people_list.people_tracks[ind].local_pos.x - 0.12);
			 
        if(s->verbose){
            fprintf(stderr, "Theta : %f\n", theta);
            
            if(fabs(theta) > bot_to_radians(150)){
                fprintf(stderr, "Too far back\n"); 
            }
        }
    
        theta = bot_clamp(theta, bot_to_radians(-145), bot_to_radians(145));

        int heading_value = bot_to_degrees(theta) *3.3 + 512;
        
        if(s->verbose){
            fprintf(stderr, "Servo Heading value : %d\n", heading_value);
        }    
    
        prev_heading_value = heading_value;
    }
}



int 
initialize_people_filters(state_t *s)
{
    if(s->verbose){
        fprintf(stderr,"Filters Initialized \n");
    }
    
    s->people_list.people_tracks = (person_track_t *) calloc(s->people_list.actual_filter_size, sizeof(person_track_t )); 
  

    //dont think this needs to happen here
    /*for(int i=0; i < s->people_list.actual_filter_size; i++){
      s->people_list.people_tracks[i].person_filter = (carmen3d_localize_particle_filter_p ) 
      calloc(1, sizeof(carmen_localize_particle_filter)); 
      }*/
  
    /*  s->people_filter = (carmen3d_localize_particle_filter_p *)
        malloc(s->people_list.actual_filter_size*sizeof(carmen3d_localize_particle_filter_p)); 
        carmen_test_alloc(s->people_filter);
    */
    s->people_list.people_summary  = (carmen3d_localize_summary_p) 
        malloc(s->people_list.actual_filter_size*sizeof(carmen3d_localize_summary_t));
    carmen_test_alloc(s->people_list.people_summary);
    
    /*s->time_since = (double *) malloc(s->people_list.actual_filter_size*sizeof(double));
      carmen_test_alloc(s->time_since);*/
    return 0;
}



int create_new_person(carmen_point_t person_obs, carmen_point_t std, double time, state_t *s)  
//this is called once the filters that have observations have been updated so this 
//wont reallocate a filter that has an observation in this time period
{
    if(s->people_list.actual_filter_size <=s->people_list.no_people){//we dont have enough filters to allocate -increase by 5
        if(s->verbose){
            fprintf(stderr,"Increasing total no of filters");
        }
        /*carmen3d_localize_particle_filter_p *temp_people_filter = (carmen3d_localize_particle_filter_p *)
          realloc(s->people_filter,(s->people_list.actual_filter_size+5)*sizeof(carmen3d_localize_particle_filter_p)); 
          carmen_test_alloc(temp_people_filter);
          s->people_filter = temp_people_filter;
        */
    
        person_track_t *temp_people_tracks = (person_track_t *)
            realloc(s->people_list.people_tracks,(s->people_list.actual_filter_size+5)*sizeof(person_track_t)); 
        carmen_test_alloc(temp_people_tracks);
        s->people_list.people_tracks = temp_people_tracks;

    
        carmen3d_localize_summary_p temp_people_summary  = (carmen3d_localize_summary_p) 
            realloc(s->people_list.people_summary,(s->people_list.actual_filter_size+5)*sizeof(carmen3d_localize_summary_t));
        carmen_test_alloc(temp_people_summary);
        s->people_list.people_summary = temp_people_summary;    
        s->people_list.actual_filter_size +=5;    
    }

    memset(&s->people_list.people_tracks[s->people_list.no_people], 0, sizeof(person_track_t)); 

    s->people_list.people_tracks[s->people_list.no_people].id = s->people_list.max_ind;
    s->people_list.people_tracks[s->people_list.no_people].person_filter = carmen3d_localize_particle_filter_person_new(&(s->param)); //create a new filter 
    //this will be freed when that person is not being tracked
    initialize_filter(s->people_list.people_tracks[s->people_list.no_people].person_filter,person_obs,std);  
    carmen_localize_summarize_person(s->people_list.people_tracks[s->people_list.no_people].person_filter, &(s->people_list.people_summary[s->people_list.no_people]));
    s->people_list.people_tracks[s->people_list.no_people].time_since = time;
    s->people_list.people_tracks[s->people_list.no_people].time_created = time;
    s->people_list.people_tracks[s->people_list.no_people].time_since_motion = time; 
    s->people_list.people_tracks[s->people_list.no_people].motion_count = 1;
    init_point_avg_object(&(s->people_list.people_tracks[s->people_list.no_people].relative_person_history), MAX_REL_PERSON_HISTORY);
    init_point_avg_object(&(s->people_list.people_tracks[s->people_list.no_people].global_person_history_small), MAX_REL_PERSON_HISTORY);
    init_point_avg_object(&(s->people_list.people_tracks[s->people_list.no_people].global_person_history), MAX_PERSON_HISTORY);
    init_double_avg_object(&(s->people_list.people_tracks[s->people_list.no_people].person_heading_history), MAX_PERSON_HISTORY);

    s->people_list.no_people++;
    s->people_list.max_ind++; 
  
    return s->people_list.no_people -1;
}

int 
prune_filters(double time_obs, state_t *s){
    int k,current_act_people = 0;

    int valid_filter_count = 0;
  
    for(int j=0;j<s->people_list.no_people;j++){//cycle through the array 
        double dist_from_chair = hypot((s->people_list.people_summary[j].mean.x),(s->people_list.people_summary[j].mean.y));
    
        //have a smaller realloc time for the non-following filters - would cut down on a lot of things 
        //also use the lower number of particles - i.e. move away from ipc param daemon
    
        if(((time_obs- s->people_list.people_tracks[j].time_since) <= REALLOCATE_TIME) && (j == s->people_list.active_person_ind || (dist_from_chair < REALLOCATION_DISTANCE))
           && (time_obs- s->people_list.people_tracks[j].time_created <= MOVED_OBS_TIME ||
               (time_obs- s->people_list.people_tracks[j].time_created > MOVED_OBS_TIME && 
                s->people_list.people_tracks[j].motion_count > MOVED_PERSON_THRESHOLD))){
            valid_filter_count++;  
    
        }
        else if (s->verbose){
            if(dist_from_chair > REALLOCATION_DISTANCE){                
                fprintf(stderr, "+++++++++++++++ Pruned for Distance = > Filter ID : %d Dist from chair : %f\n",
                        s->people_list.people_tracks[j].id, dist_from_chair);
            }

            if((time_obs- s->people_list.people_tracks[j].time_since) > REALLOCATE_TIME){
                fprintf(stderr, "+++++++++++++++ Pruned for Time = > Filter ID : %d Dist from chair : %f\n",
                        s->people_list.people_tracks[j].id, time_obs- s->people_list.people_tracks[j].time_since);
            }
            if(!((time_obs- s->people_list.people_tracks[j].time_created > MOVED_OBS_TIME && 
                  s->people_list.people_tracks[j].motion_count > MOVED_PERSON_THRESHOLD) || time_obs- s->people_list.people_tracks[j].time_created < MOVED_OBS_TIME)){
                fprintf(stderr, "+++++++++++++++ Pruned for Lack of motion = > Time : %f Count : %d\n", 
                        time_obs- s->people_list.people_tracks[j].time_created, 
                        s->people_list.people_tracks[j].motion_count);
            }

        }      
    }


    //if the current no_of filters is larger than the live filters, 
    //remove the dead filters and reorder the live ones
    if(s->people_list.no_people > valid_filter_count){//current_act_people) &(current_act_people>=0)){  
        int i=0,j;
  
        //allocate memory for the reduced filters 

    
        carmen3d_localize_summary_p new_people_summary  = (carmen3d_localize_summary_p) 
            malloc(valid_filter_count*sizeof(carmen3d_localize_summary_t));
        carmen_test_alloc(new_people_summary);

        person_track_t *temp_people_tracks = (person_track_t *)
            malloc((valid_filter_count)*sizeof(person_track_t)); 
        carmen_test_alloc(temp_people_tracks);
       

        for(j=0;j<s->people_list.no_people;j++){//cycle through the array 
            double dist_from_chair = hypot((s->people_list.people_summary[j].mean.x),(s->people_list.people_summary[j].mean.y));

            if(((time_obs- s->people_list.people_tracks[j].time_since ) <= REALLOCATE_TIME) && ((j==s->people_list.active_person_ind) || (dist_from_chair < REALLOCATION_DISTANCE))
               && 
               (time_obs- s->people_list.people_tracks[j].time_created <= MOVED_OBS_TIME ||
               (time_obs- s->people_list.people_tracks[j].time_created > MOVED_OBS_TIME && 
                s->people_list.people_tracks[j].motion_count > MOVED_PERSON_THRESHOLD))){

                memcpy(&temp_people_tracks[i],&(s->people_list.people_tracks[j]), sizeof(person_track_t));
                memcpy(&new_people_summary[i],&(s->people_list.people_summary[j]),sizeof(carmen3d_localize_summary_t));

                if(j == s->people_list.active_person_ind){//if the position of the active filter in the filter list is changed update the location
                    s->people_list.active_person_ind = i;
                }
                //fprintf(stderr,"\n");
                i++;	
            }
            else{//free the unused filter
                if(j == s->people_list.active_person_ind){//if the position of the active filter in the filter list is changed update the location
                    s->people_list.active_person_ind = -1;  //-1 reflects the fact that there is no active filter
                    update_tracking_state(LOST, s);
                    fprintf(stderr,"+++++++ Person Lost +++++++ **** \n");
                    publish_speech_msg("LOST", s);
                }

                //fprintf(stderr,"==\tPruning Filter \n");
                free(s->people_list.people_tracks[j].person_filter->particles); //free the filter 
                for(k = 0; k < s->people_list.people_tracks[j].person_filter->param->num_particles; k++) {
                    free(s->people_list.people_tracks[j].person_filter->temp_weights[k]);
                }

                free(s->people_list.people_tracks[j].person_filter->temp_weights);	
                free_point_avg_object(&(s->people_list.people_tracks[j].relative_person_history));
                free_point_avg_object(&(s->people_list.people_tracks[j].global_person_history_small));
                free_point_avg_object(&(s->people_list.people_tracks[j].global_person_history));
                free_double_avg_object(&(s->people_list.people_tracks[j].person_heading_history));
            }
        }
        free(s->people_list.people_tracks);
        free(s->people_list.people_summary);

        s->people_list.people_tracks = temp_people_tracks;
        s->people_list.people_summary = new_people_summary;

        s->people_list.no_people = valid_filter_count;  //update the new no of filters
        s->people_list.actual_filter_size = s->people_list.no_people;
    } 
    return 0;
}


//lcm handlers 
void mode_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                  const char * channel __attribute__((unused)), 
                  const erlcm_tagged_node_t * msg,
                  void * user  __attribute__((unused)))
{
    state_t *s = (state_t *) user; 
    char* type = msg->type;
    char* new_mode = msg->label;
    if(!strcmp(type,"mode")){
        //mode has changed - 
        if(!strcmp(new_mode,"navigation")){//navigation mode - stop following guide
            s->tourguide_mode = 0;
            fprintf(stderr,"Navigation mode\n");
        }
        if(!strcmp(new_mode,"tourguide")){//tourguide mode
            s->tourguide_mode = 1;
            fprintf(stderr,"Tourguide mode\n");
        }
    }
}

int 
lcm_detect_person_both_basic(state_t *s){ 
    //have no laser messages - return 
    if(s->use_map){
        check_gridmap_update(s->check_gridmap);
    }

    if((!s->have_rear_laser && !s->have_front_laser)){
        return -1;
    }
    if (s->current_map.map != NULL){
        if(s->verbose){
            fprintf(stderr,"Checking with Gridmap\n");
            fprintf(stderr,"Have map => Size (%d,%d)\n", s->current_map.config.x_size, s->current_map.config.y_size);
        }
    }

    for(int j=0;j<s->people_list.no_people;j++){//cycle through the array 
        s->people_list.people_tracks[j].found_obs_last = 0;
    }

    if(s->verbose){
        fprintf(stderr,"==============================================\n");
    }

    //velocities 
    double vel_x = .0, vel_y = .0;
    double rel_vel_x = .0, rel_vel_y = .0;
    double u_vel_x = .0, u_vel_y = .0;
    double person_vel_average = .0;
    double learning_rate = 0.3;
    double rot_vel_mag = 0.0;
    double approach_vel = .0;

    //heading 
    //static double person_heading_average = .0;
    double best_fit_heading = 0.0;

    //obs time in (s)
    double time_obs = ((double)s->robot_fl->utime) /  1e6 ; 

    double rpy[3] = {0,0,0};  
    bot_quat_to_roll_pitch_yaw (s->robot_fl->pose.orientation, rpy) ;

    carmen_point_t robot_position;

    robot_position.x = s->robot_fl->pose.pos[0];
    robot_position.y = s->robot_fl->pose.pos[1];
    robot_position.theta = rpy[2];
  
    bot_lcmgl_t *lcmgl;

    static double vel[2] = {.0,.0};
    static double prev_time = 0;
  
    //update the filter summaries based on the robot motion 
    for(int j=0;j<s->people_list.no_people;j++){
        carmen_localize_update_filter_center(s->last_robot_position,robot_position,&(s->people_list.people_summary[j].mean));
    }
  
    int no_points = 0;
    carmen_point_p points = NULL;  //point array for the laser readings

    static carmen_point_p prev_points = NULL;
    static int prev_no_points = 0;
  
    carmen_feet_observations person_obs;  

    if(!s->have_rear_laser){
        points = (carmen_point_p)malloc(s->robot_fl->laser.nranges *sizeof(carmen_point_t));  
        carmen_test_alloc(points);
    
        //----- Sachi : This pruning needs to change 
        if(s->people_list.active_person_ind >=0){
            carmen_point_t person_loc;
            person_loc.x = s->people_list.people_summary[s->people_list.active_person_ind].mean.x;
            person_loc.y = s->people_list.people_summary[s->people_list.active_person_ind].mean.y;
            person_loc.theta = .0;

            person_obs = lcm_detect_segments_from_planar_lidar_pruned(&s->robot_fl->laser, s->front_laser_offset, s->robot_fl->pose, 
                                                                      NULL, s->rear_laser_offset, s->robot_fl->pose, 
                                                                      SEGMENT_GAP,points, &no_points,
                                                                      &person_loc, s->high_laser, 
                                                                      s->use_classifier, s->pruning_mode);
      
        }
        else{
            person_obs = lcm_detect_segments_from_planar_lidar(&s->robot_fl->laser, s->front_laser_offset,s->robot_fl->pose,
                                                               NULL, s->rear_laser_offset, s->robot_fl->pose, 
                                                               SEGMENT_GAP,points, &no_points, 
                                                               s->high_laser, s->use_classifier);
        }    
    }
    else{
        points = (carmen_point_p)malloc((s->robot_fl->laser.nranges + s->robot_rl->laser.nranges)*sizeof(carmen_point_t)); 
        carmen_test_alloc(points); 
        if(s->people_list.active_person_ind >=0){
            carmen_point_t person_loc;
            person_loc.x = s->people_list.people_summary[s->people_list.active_person_ind].mean.x;
            person_loc.y = s->people_list.people_summary[s->people_list.active_person_ind].mean.y;
            person_loc.theta = .0;

            person_obs = lcm_detect_segments_from_planar_lidar_pruned(&s->robot_fl->laser, s->front_laser_offset,s->robot_fl->pose,
                                                                      &s->robot_rl->laser, s->rear_laser_offset, s->robot_rl->pose,
                                                                      SEGMENT_GAP,points, &no_points, &person_loc, s->high_laser, s->use_classifier, s->pruning_mode);
        }
        else{
            person_obs = lcm_detect_segments_from_planar_lidar(&s->robot_fl->laser, s->front_laser_offset,s->robot_fl->pose, 
                                                               &s->robot_rl->laser, s->rear_laser_offset, s->robot_rl->pose,
                                                               SEGMENT_GAP,points, &no_points, s->high_laser, s->use_classifier); 
        }
    }
  
    //create tracks for moving observations 
    carmen_feet_observations small_moved_obs;
    get_small_moving_feet_segments(&person_obs, &small_moved_obs, prev_points, prev_no_points, s->check_gridmap);
    
  
    lcmgl =  s->lcmgl_moved_legs;

    if(s->verbose){      
        fprintf(stderr,"++++++++++ Small Moved Obs : %d\n", small_moved_obs.no_obs);
    }

    if (1 && lcmgl){// && is_front_laser){ //turn to 1 if u want to draw
        float rtheta = s->global_robot_pose.theta;
        double pos[3];
        for (int i=0;i<small_moved_obs.no_obs;i++){
            float leg_x = s->global_robot_pose.x + small_moved_obs.locations[i].center_x * cos(rtheta) - small_moved_obs.locations[i].center_y * sin(rtheta);
            float leg_y = s->global_robot_pose.y + small_moved_obs.locations[i].center_x * sin(rtheta) + small_moved_obs.locations[i].center_y*cos(rtheta);
            pos[0] = leg_x;
            pos[1] = leg_y;
            pos[2] = .0;  
     
            lcmglLineWidth (5);
            lcmglColor3f(1.0, 1.0, .0);
            lcmglCircle(pos, 0.1);
        } 
        bot_lcmgl_switch_buffer (lcmgl);       
    } 

    //update the moved feet count 
    for(int j=0;j<s->people_list.no_people;j++){
        double closest_dist = 1000;
        
        for (int i=0;i< small_moved_obs.no_obs;i++){
            double m_x = small_moved_obs.locations[i].center_x;
            double m_y = small_moved_obs.locations[i].center_y;
            
            double temp_dist = hypot((s->people_list.people_summary[j].mean.x - m_x),(s->people_list.people_summary[j].mean.y-m_y));
            if(closest_dist > temp_dist){
                closest_dist = temp_dist;
            }
            
            if(temp_dist < 0.4){
                fprintf(stderr,"Found moved leg : %d\n", j);
                s->people_list.people_tracks[j].motion_count++;
                break;
            }
        }
    }
    
    carmen_feet_observations moved_obs;// = small_moved_obs;
    get_moving_feet_segments(&person_obs, &moved_obs, prev_points, prev_no_points, s->check_gridmap);
    
    if(s->verbose){      
        fprintf(stderr,"++++++++++ Moved Obs : %d\n", moved_obs.no_obs);
    }

    if(s->multi_people_tracking || s->people_list.active_person_ind < 0){
        //call the new feet detector
        carmen_point_p moved_person = (carmen_point_p) malloc(moved_obs.no_obs*sizeof(carmen_point_t)); 
        carmen_test_alloc(moved_person);
  
        //copy the observations on to points
      
        if(s->people_list.active_person_ind==-1 || s->multi_people_tracking){
            double *near_current = (double *) calloc(moved_obs.no_obs, sizeof(double));

            int *closest_filter = (int *) calloc(moved_obs.no_obs, sizeof(int));

            

            for (int i=0;i< moved_obs.no_obs;i++){
                moved_person[i].x = moved_obs.locations[i].center_x;
                moved_person[i].y = moved_obs.locations[i].center_y;
                moved_person[i].theta = 0.0;
                int closest_obs = -1;
                //check if there are other filters near by
                double min_dist = 1000;
                for(int j=0;j<s->people_list.no_people;j++){
                    double temp_dist = hypot((s->people_list.people_summary[j].mean.x - moved_person[i].x),(s->people_list.people_summary[j].mean.y-moved_person[i].y));

                    double heading_to_obs = atan2(s->people_list.people_summary[j].mean.y-moved_person[i].y, 
                                                  s->people_list.people_summary[j].mean.x - moved_person[i].x);

                    double heading_difference = bot_mod2pi(heading_to_obs - s->people_list.people_tracks[j].heading ); 
                    if((temp_dist < min_dist)){/* && fabs(heading_difference) < bot_to_radians(30) || (temp_dist < min_dist && temp_dist < 0.3)){*/
                        min_dist = temp_dist;
                        closest_obs = j; 
                    }	 
                }
                near_current[i] = min_dist; 
                closest_filter[i] = closest_obs;
                
            }

            //fix alignment issue between the two lasers 
            //also - maybe merge the observations between the two sets and prune

            for (int i=0;i< moved_obs.no_obs;i++){
                fprintf(stderr, "\tObs :%d => Min Dist : %f Associated Filter : %d\n",
                        i, near_current[i], closest_filter[i]);
                if(near_current[i] < 0.8){
                    fprintf(stderr, "-------------Too close to exisiting person - not creating\n");
                }
                else{
                     double dist_from_robot = hypot(moved_person[i].y, moved_person[i].x);
                     double obs_heading = atan2(moved_person[i].y, moved_person[i].x);
                      
                     fprintf(stderr, "\t\tDist : %f Heading : %f\n", dist_from_robot, 
                             bot_to_degrees(obs_heading));
                     if(dist_from_robot < 5.0){
                         fprintf(stderr,"----------- Creating Filter\n");
                         create_new_person(moved_person[i],s->std, time_obs, s);
                     }
                     else{
                         fprintf(stderr,"----------- Too Far\n");
                     }
                }
            }

            free(near_current);
            free(closest_filter);            
        
        }
        free(moved_person);
    }

    if(s->verbose && 0){
        fprintf(stderr,"No of Filters : %d\n", s->people_list.no_people);  
    }
    static carmen_point_t prev_velocity = {.0,.0,.0};
    carmen_localize_update_person_hostory(s->last_robot_position,robot_position,
                                          &prev_velocity, 1);


    carmen_feet_observations used_person_obs;
  
    //Prune observations if they are in the same place as in the last frame - based on motion only
    //used_person_obs = prune_observations(&person_obs);
    used_person_obs = person_obs; //no pruning 
  
    int full_count = used_person_obs.no_obs;
    carmen_feet_seg* feet_obs = used_person_obs.locations;
    int i,j;

    carmen_leg_t *person_det = (carmen_leg_t *) malloc(full_count*sizeof(carmen_leg_t)); 
    carmen_test_alloc(person_det);

    carmen_leg_t *full_person_det = (carmen_leg_t *) malloc(full_count*sizeof(carmen_leg_t)); 
    carmen_test_alloc(full_person_det);

    int meta_count = 0;

    //copy the observations on to points
    for (i=0;i<full_count;i++){
        double l_leg_x = feet_obs[i].center_x;
        double l_leg_y = feet_obs[i].center_y;
        
        person_det[meta_count].x = l_leg_x; 
        person_det[meta_count].y = l_leg_y;
        person_det[meta_count].theta = 0.0;
        person_det[meta_count].front_laser = feet_obs[i].front_laser;
        meta_count++;
        
        full_person_det[i].x = l_leg_x;
        full_person_det[i].y = l_leg_y;
        full_person_det[i].theta = 0.0;
        full_person_det[i].front_laser = feet_obs[i].front_laser;
    }
  
    person_det = (carmen_leg_t *) realloc(person_det,meta_count*sizeof(carmen_leg_t));
    //draw possible legs 
    lcmgl = s->lcmgl_leg_obs;

    if (1 && lcmgl){
        lcmglColor3f(0, 1.0, 0);
    
        float rtheta = s->global_robot_pose.theta;
        double pos[3];
        for (i=0;i<meta_count;i++){
            float leg_x = s->global_robot_pose.x + person_det[i].x * cos(rtheta) - person_det[i].y * sin(rtheta);
            float leg_y = s->global_robot_pose.y + person_det[i].x * sin(rtheta) + person_det[i].y*cos(rtheta);
            pos[0] = leg_x;
            pos[1] = leg_y;
            pos[2] = .0;

            lcmglColor3f(.0, 1.0, .0);    
            lcmglLineWidth (5);      
            lcmglCircle(pos, 0.1);
            if(s->verbose){
                char seg_info[512];
                sprintf(seg_info,"[%d]%d-%d:%d", feet_obs[i].front_laser, feet_obs[i].start_segment, feet_obs[i].end_segment, 
                        feet_obs[i].start_segment - feet_obs[i].end_segment);
                bot_lcmgl_text(lcmgl, pos, seg_info);
            } 
        }
        bot_lcmgl_switch_buffer (lcmgl);  
    } 

    

    if(s->verbose && 0){
        fprintf(stderr,"No of Filters : %d\n", s->people_list.no_people);
    }
    //prune dead filters
    prune_filters(time_obs, s);  
  
    if(s->verbose){
        fprintf(stderr,"No of Filters : %d\n", s->people_list.no_people);
    }
    //Filter initiation code
    //create a filter for the observation that is not near any of the current filters
  
    double **dist;
    dist = (double **) malloc(meta_count*sizeof(double *));
    carmen_test_alloc(dist);
    //create a distance matrix between the filters and the observations
    for (i=0;i<meta_count;i++){
        dist[i] = (double *) malloc(s->people_list.no_people*sizeof(double));
        carmen_test_alloc(dist[i]);
        for(j=0;j<s->people_list.no_people;j++){
            dist[i][j] = hypot((s->people_list.people_summary[j].mean.x-person_det[i].x),(s->people_list.people_summary[j].mean.y-person_det[i].y));
        }
    }

    //allocate each observation to the closest person filter 
    int *no_obs_count = (int *) malloc(s->people_list.no_people*sizeof(int)); //no of observations that each filter can see
    carmen_test_alloc(no_obs_count);
    memset(no_obs_count,0,s->people_list.no_people*sizeof(int));

    int original_filter_size = s->people_list.no_people;
  
    int *allocated_to_filter = (int *) malloc(meta_count*sizeof(int)); //no of observations that each filter can see
    carmen_test_alloc(allocated_to_filter);
    memset(allocated_to_filter,0,meta_count*sizeof(int));
  
    int **allocated_observations = (int **) malloc(s->people_list.no_people*sizeof(int *)); //what observations are assigned to each filter
    carmen_test_alloc(allocated_observations);
    for (j=0;j<s->people_list.no_people;j++){
        allocated_observations[j] = (int *) malloc(meta_count*sizeof(int)); //what observations are assigned to each filter
        carmen_test_alloc(allocated_observations[j]);
        memset(allocated_observations[j],-1,meta_count*sizeof(int));
    }

    double filter_creation_angle = M_PI/12; //M_PI/12;
    double filter_creation_dist = 3.0;//3.0;

    if(s->people_list.active_person_ind >=0){//first do this for the tourguide 
        double dist_to_person = 10000.0;
    
        double person_angle = atan2(s->people_list.people_summary[s->people_list.active_person_ind].mean.y, s->people_list.people_summary[s->people_list.active_person_ind].mean.x);
        int use_front_laser = 0;
    
        //63 degrees is the angle at which the rear laser hits the front laser's 90 degree side scans

        if(person_angle < M_PI/180*63 && person_angle >= -M_PI/180*63){
            use_front_laser = 1;
        }
    
        int ind = -1;
        int full_ind = -1;
   
        for (i=0;i<meta_count;i++){ //search through
            if(s->verbose){
                fprintf(stderr,"%d : %f\n",i, dist[i][s->people_list.active_person_ind]);
            }
      
            if(person_det[i].front_laser != use_front_laser){
                continue;
            }
            if(dist[i][s->people_list.active_person_ind] < PERSON_OBS_RADIUS){ //within the boundry
                allocated_to_filter[i]=1; //make all these observations out of bounds for the other filters
                if(s->verbose){
                    fprintf(stderr,"Found Observation : %d\n", i);
                }
	
                if(dist[i][s->people_list.active_person_ind] < dist_to_person){
                    ind = i;
                    dist_to_person = dist[i][s->people_list.active_person_ind];
                }
            }
        }
        if(ind ==-1){      
            //do a full search of these observations - through both laser observations
            for (i=0;i<meta_count;i++){ //search through	
                if(dist[i][s->people_list.active_person_ind] < PERSON_OBS_RADIUS){ //within the boundry
                    allocated_to_filter[i]=1; //make all these observations out of bounds for the other filters
                    if(dist[i][s->people_list.active_person_ind] < dist_to_person){
                        ind = i;
                        dist_to_person = dist[i][s->people_list.active_person_ind];
                    }
                }	
            }
        }
        if(ind==-1 && (meta_count <full_count)){ //we still havent found a good observation
            //have not found a good observation yet
            for (i=0;i<full_count;i++){ //search through
                if(full_person_det[i].front_laser != use_front_laser){
                    continue;
                }

                double temp_dist =  hypot((s->people_list.people_summary[s->people_list.active_person_ind].mean.x - full_person_det[i].x),
                                          (s->people_list.people_summary[s->people_list.active_person_ind].mean.y - full_person_det[i].y));

                if(temp_dist < PERSON_OBS_RADIUS){ //within the boundry
                    if(temp_dist < dist_to_person){
                        full_ind = i;
                        dist_to_person = temp_dist;
                    }
                }

            }
        }
    
        if(ind==-1 && full_ind==-1){
            carmen_localize_run_person_no_obs(s->people_list.people_tracks[s->people_list.active_person_ind].person_filter, robot_position);
            carmen_localize_summarize_person(s->people_list.people_tracks[s->people_list.active_person_ind].person_filter, &(s->people_list.people_summary[s->people_list.active_person_ind]));

            s->people_list.people_tracks[s->people_list.active_person_ind].found_obs_last = 0;
        }
        else if(ind>=0){//(no_obs_count[j]==1){
            int obs = ind;//allocated_observations[j][0];
            carmen_point_t assigned_obs;
            assigned_obs.x = person_det[obs].x;
            assigned_obs.y = person_det[obs].y;
            assigned_obs.theta = person_det[obs].theta;

            double min_leg_dist = 0.5;//used to be 0.5
            //the higher this is the more likely we will get merged to false leg observation if we get near enough

            int closest_leg = -1;
      
            int g_ind = s->people_list.active_person_ind; 
      
            if(s->verbose){
                fprintf(stderr,"Active Filter Index : %d =  Valid Filter Count : %d\n", s->people_list.active_person_ind, s->people_list.no_people);
            }

            s->people_list.people_tracks[s->people_list.active_person_ind].found_obs_last = 1;
      
            carmen_localize_run_person_with_obs(s->people_list.people_tracks[s->people_list.active_person_ind].person_filter, robot_position, assigned_obs);
            //this tends to mess up the tracking sometimes - not sure if the benifits of this outweigh the cost       
            carmen_localize_summarize_person(s->people_list.people_tracks[s->people_list.active_person_ind].person_filter, &(s->people_list.people_summary[s->people_list.active_person_ind]));
            s->people_list.people_tracks[s->people_list.active_person_ind].time_since = time_obs;
            //      s->time_since[s->people_list.active_person_ind] = time_obs; 

            s->people_list.people_tracks[g_ind].local_pos.x = s->people_list.people_summary[s->people_list.active_person_ind].mean.x;
            s->people_list.people_tracks[g_ind].local_pos.y = s->people_list.people_summary[s->people_list.active_person_ind].mean.y;

            double g_x = s->people_list.people_tracks[g_ind].local_pos.x;
            double g_y = s->people_list.people_tracks[g_ind].local_pos.y;

            float rtheta = s->global_robot_pose.theta;
            float leg_x = s->global_robot_pose.x + g_x * cos(rtheta) - g_y * sin(rtheta);
            float leg_y = s->global_robot_pose.y + g_x * sin(rtheta) + g_y*cos(rtheta);
            s->people_list.people_tracks[g_ind].gpos.x = leg_x;
            s->people_list.people_tracks[g_ind].gpos.y = leg_y;

            lcmgl = s->lcmgl_act_pos;
            if(lcmgl){
                double pos[3];
	
                pos[0] = leg_x;
                pos[1] = leg_y;
                pos[2] = .0; 
	  
                lcmglColor3f(1.0, 0.5, .0);

                lcmglLineWidth (5);      
                lcmglCircle(pos, 0.15);
	  
                bot_lcmgl_switch_buffer (lcmgl);
            }
        }
        else if(full_ind>=0){//(no_obs_count[j]==1){
            int obs = full_ind;//allocated_observations[j][0];
            carmen_point_t assigned_obs;
            assigned_obs.x = full_person_det[obs].x;
            assigned_obs.y = full_person_det[obs].y;
            assigned_obs.theta = full_person_det[obs].theta;

            carmen_localize_run_person_with_obs(s->people_list.people_tracks[s->people_list.active_person_ind].person_filter, robot_position, assigned_obs);
            carmen_localize_summarize_person(s->people_list.people_tracks[s->people_list.active_person_ind].person_filter, &(s->people_list.people_summary[s->people_list.active_person_ind]));

            s->people_list.people_tracks[s->people_list.active_person_ind].found_obs_last = 1;
            s->people_list.people_tracks[s->people_list.active_person_ind].time_since = time_obs;
            //      s->time_since[s->people_list.active_person_ind] = time_obs; 
        }
        //person location has been updated    
    }

    if(s->verbose){
        fprintf(stderr,"++++++++++++++After Associating the obs\n");
        for(int j=0;j<s->people_list.no_people;j++){
            fprintf(stderr, " [%d] Local : %f,%f Global : %f,%f\n", s->people_list.people_tracks[j].id, 
                    s->people_list.people_tracks[j].local_pos.x, 
                    s->people_list.people_tracks[j].local_pos.y,
                    s->people_list.people_tracks[j].gpos.x, 
                    s->people_list.people_tracks[j].gpos.y); 
        }
    }


    fprintf(stderr,"\t\t++++++++++++++++++++++++++++++++++++++++++++++\n");
  
    for(j=0;j<s->people_list.no_people;j++){//for each filter search the close observations and pick the one closest to the wheelchair 

        if(j==s->people_list.active_person_ind){
            continue;
        }
        
        /*double dist_to_wheelchair = 10000.0;
        int ind = -1;
        for (i=0;i<meta_count;i++){ //search through
            if(!allocated_to_filter[i] &&(dist[i][j] < 0.4)){ //within the boundry
                double temp_dist = hypot(person_det[i].x, person_det[i].y);
                if(temp_dist < dist_to_wheelchair){
                    ind = i;
                    dist_to_wheelchair = temp_dist;
                }
            }
            }*/
        

        double dist_to_filter = 10000.0;
        int ind = -1;

        for (i=0;i<meta_count;i++){ //search through
            if(!allocated_to_filter[i] &&(dist[i][j] < 1.0)){ //within the boundry
                double temp_dist = dist[i][j]; 
                if(temp_dist < dist_to_filter){
                    ind = i;
                    dist_to_filter = temp_dist;
                }
            }
        }

        //if(no_obs_count[j]==0){
        if(ind==-1){
            fprintf(stderr,"\t\t Filter : %d Obs not found => Dist : %f\n", j, dist_to_filter);
            
            carmen_localize_run_person_no_obs(s->people_list.people_tracks[j].person_filter, robot_position);
            carmen_localize_summarize_person(s->people_list.people_tracks[j].person_filter, &(s->people_list.people_summary[j]));
            s->people_list.people_tracks[j].found_obs_last = 0;
        }
        else if(ind>=0){//(no_obs_count[j]==1){
            fprintf(stderr,"\t\t Filter : %d Obs Found : %d => Dist : %f\n", j, ind, dist_to_filter);
            
            allocated_to_filter[ind] = 1;
            int obs = ind;//allocated_observations[j][0];
            carmen_point_t assigned_obs;
            assigned_obs.x = person_det[obs].x;
            assigned_obs.y = person_det[obs].y;
            assigned_obs.theta = person_det[obs].theta;
            carmen_localize_run_person_with_obs(s->people_list.people_tracks[j].person_filter, robot_position, assigned_obs);
            carmen_localize_summarize_person(s->people_list.people_tracks[j].person_filter, &(s->people_list.people_summary[j]));
            s->people_list.people_tracks[j].found_obs_last = 1;
            s->people_list.people_tracks[j].time_since = time_obs;
            //      s->time_since[j] = time_obs; 
        }

        s->people_list.people_tracks[j].local_pos.x = s->people_list.people_summary[j].mean.x;
        s->people_list.people_tracks[j].local_pos.y = s->people_list.people_summary[j].mean.y;

    
        double g_x = s->people_list.people_tracks[j].local_pos.x;
        double g_y = s->people_list.people_tracks[j].local_pos.y;
    
        float rtheta = s->global_robot_pose.theta;
        float leg_x = s->global_robot_pose.x + g_x * cos(rtheta) - g_y * sin(rtheta);
        float leg_y = s->global_robot_pose.y + g_x * sin(rtheta) + g_y*cos(rtheta);
        s->people_list.people_tracks[j].gpos.x = leg_x;
        s->people_list.people_tracks[j].gpos.y = leg_y;
    }

    /*if(s->people_list.active_person_ind < 0){ //creating new filters only if there is no active filter - // we should prob change this so it creates only one filer 
      for (i=0;i<meta_count;i++){
      if(allocated_to_filter[i]==0){
      double angle = atan2(person_det[i].y,person_det[i].x);
      double dist = hypot(person_det[i].x,person_det[i].y);
	
      if (fabs(angle)< filter_creation_angle && dist < filter_creation_dist){
      int near_filter = 0;
      double min_dist = 1.0;
      double temp_dist;
      for(j=0;j<s->people_list.no_people;j++){
      temp_dist = hypot((s->people_summary[j].mean.x-person_det[i].x),(s->people_summary[j].mean.y-person_det[i].y));
      if(temp_dist< min_dist){
      min_dist = temp_dist;
      near_filter = 1;
      break;
      }
      }
      if(near_filter==0){//no nearby filter was created 
      carmen_point_t assigned_obs;
      assigned_obs.x = person_det[i].x;
      assigned_obs.y = person_det[i].y;
      assigned_obs.theta = person_det[i].theta;
      create_new_person(assigned_obs,s->std, time_obs, s);
      }
      }      
      }
      }
      }*/


    if(s->tracking_state==LOOKING && s->use_kinect){//if we are in the tracking state we should reaquire 
        //??make two passes - first check if we have an observations 
        
        double max_dist = 1.0;
        int best_ind = -1;
        

        for(j=0;j<s->people_list.no_people;j++){
            double temp_dist = hypot(s->people_list.people_summary[j].mean.x - s->kinect_x,
                                     s->people_list.people_summary[j].mean.y - s->kinect_y);
            
            if(temp_dist < max_dist){
                max_dist = temp_dist;
                best_ind = j;
            }           
        }
        if(best_ind >=0){
            s->people_list.active_person_ind = best_ind;
            fprintf(stderr,"Person Found\n");
        }
        else{
            fprintf(stderr, "Couldnt find anyone there - creating a new person : (%f,%f)\n ", s->kinect_x, s->kinect_y);
            //perhapse change to look for the nearest leg observation
            
            carmen_point_t kinect_person_pos = {s->kinect_x, s->kinect_y, 0};
            int k_ind = create_new_person(kinect_person_pos ,s->std, time_obs, s);

            s->people_list.active_person_ind = k_ind;
            
            
        }
        

        //kill the other filters only if we are in single person tracking mode 
        if(s->people_list.active_person_ind >=0){
            if(!s->multi_people_tracking){
                for(int k=0;k<s->people_list.no_people;k++){
                    if(k != s->people_list.active_person_ind){
                        s->people_list.people_tracks[k].time_since = .0; 
                        //s->time_since[k] = .0;
                    }
                }
            }
            
            int g_ind = s->people_list.active_person_ind; 
            
            s->people_list.people_tracks[g_ind].local_pos.x = s->people_list.people_summary[s->people_list.active_person_ind].mean.x;
            s->people_list.people_tracks[g_ind].local_pos.y = s->people_list.people_summary[s->people_list.active_person_ind].mean.y;
            update_tracking_state(FOUND,s);  //person located	
            
            fprintf(stderr,"====== New Person Following Activated =========\n");      
            
        }
        else{
            //fprintf(stderr,"**** No good filter found **** \n");
        }
    }
  
    else if(s->tracking_state==LOOKING){//if we are in the tracking state we should reaquire 
        //??make two passes - first check if we have an observations 
        double max_dist = 2.5;
        double too_close_dist = 0.5;
        //double min_dist = 0.5;
        double follow_angle = M_PI/6;
        double temp_dist;
        double temp_angle;
        fprintf(stderr,"Looking for person - Non Kinect\n");
        
        
        double angle_offset = 10000;
        
        for(j=0;j<s->people_list.no_people;j++){
            temp_dist = hypot(s->people_list.people_summary[j].mean.x,s->people_list.people_summary[j].mean.y);
            temp_angle = atan2(s->people_list.people_summary[j].mean.y,s->people_list.people_summary[j].mean.x);
            if((fabs(temp_angle) < follow_angle) 
               && (temp_dist < max_dist) && (temp_dist > too_close_dist)){
                if(angle_offset > fabs(temp_angle)){
                    angle_offset = fabs(temp_angle);
                    s->people_list.active_person_ind = j;	
                }
            }
        }
        
        //kill the other filters only if we are in single person tracking mode 
        if(s->people_list.active_person_ind >=0){
            if(!s->multi_people_tracking){
                for(int k=0;k<s->people_list.no_people;k++){
                    if(k != s->people_list.active_person_ind){
                        s->people_list.people_tracks[k].time_since = .0; 
                        //s->time_since[k] = .0;
                    }
                }
            }
            
            int g_ind = s->people_list.active_person_ind; 
            
            s->people_list.people_tracks[g_ind].local_pos.x = s->people_list.people_summary[s->people_list.active_person_ind].mean.x;
            s->people_list.people_tracks[g_ind].local_pos.y = s->people_list.people_summary[s->people_list.active_person_ind].mean.y;
            update_tracking_state(FOUND,s);  //person located	

            fprintf(stderr,"====== New Person Following Activated =========\n");      
        }
    }

    //caclulate track information 

    carmen_localize_update_person_hostory(s->last_robot_position,robot_position, s->robot_history.history, s->robot_history.current_size);
    carmen_point_t zero_pos = {.0, .0, time_obs};
    add_point_to_history_object(&(s->robot_history), zero_pos);    

    carmen_point_t robot_start_point = get_average_point_from_history_object(&(s->robot_history), 
                                                                             0,
                                                                             s->robot_history.current_size/2.0-1);

    //    carmen_point_t r_end_point = get_average_point_from_history(person_history, 
    carmen_point_t robot_end_point = get_average_point_from_history_object(&(s->robot_history), 
                                                                           s->robot_history.current_size/2.0,
                                                                           s->robot_history.current_size-1);  

    double robot_vel_x = .0, robot_vel_y = .0;
    double robot_vel_mag = .0;
    if(robot_start_point.theta - robot_end_point.theta >0){      
        robot_vel_x = (robot_start_point.x - robot_end_point.x)/(robot_start_point.theta - robot_end_point.theta);
        robot_vel_y = (robot_start_point.y - robot_end_point.y)/(robot_start_point.theta - robot_end_point.theta);
        //rel_vel_heading = atan2(rel_vel_y, rel_vel_x);
        robot_vel_mag = hypot(robot_vel_x, robot_vel_y);
    }
    else{
        robot_vel_x = 0;
        robot_vel_y = 0;
    }
    if(s->verbose){
        fprintf(stderr, "Robot Velocity : %f\n" , robot_vel_mag); 
    }

    lcmgl = s->lcmgl_person_history;

    //if(s->people_list.no_people>0){//we have a valid person location
    for(int i=0; i< s->people_list.no_people; i++){
    

        carmen_localize_update_person_hostory(s->last_robot_position,robot_position, s->people_list.people_tracks[i].global_person_history.history, s->people_list.people_tracks[i].global_person_history.current_size);
        carmen_localize_update_person_hostory(s->last_robot_position,robot_position, s->people_list.people_tracks[i].global_person_history_small.history, s->people_list.people_tracks[i].global_person_history_small.current_size);

        //might need to do this to obtain the robot's averaged velocity and then calculate relative vel from that 
        
        carmen_point_t g_pos = {s->people_list.people_tracks[i].local_pos.x, s->people_list.people_tracks[i].local_pos.y, time_obs};


        //NEW METHOD
        add_point_to_history_object(&(s->people_list.people_tracks[i].global_person_history), g_pos);
        add_point_to_history_object(&(s->people_list.people_tracks[i].global_person_history_small), g_pos);
        add_point_to_history_object(&(s->people_list.people_tracks[i].relative_person_history), g_pos);

    
        //calculate relative velocity 
        carmen_point_t r_start_point = get_average_point_from_history_object(&(s->people_list.people_tracks[i].relative_person_history), 
                                                                             0,
                                                                             s->people_list.people_tracks[i].relative_person_history.current_size/2.0-1);

        //    carmen_point_t r_end_point = get_average_point_from_history(person_history, 
        carmen_point_t r_end_point = get_average_point_from_history_object(&(s->people_list.people_tracks[i].relative_person_history), 
                                                                           s->people_list.people_tracks[i].relative_person_history.current_size/2.0,
                                                                           s->people_list.people_tracks[i].relative_person_history.current_size-1);  
    

        double rel_vel_heading = 0;
        double rel_vel_mag = 0;

        double heading_to_person = atan2(s->people_list.people_tracks[i].local_pos.y, s->people_list.people_tracks[i].local_pos.x);

        if(r_start_point.theta - r_end_point.theta >0){      
            rel_vel_x = (r_start_point.x - r_end_point.x)/(r_start_point.theta - r_end_point.theta);
            rel_vel_y = (r_start_point.y - r_end_point.y)/(r_start_point.theta - r_end_point.theta);
            rel_vel_heading = atan2(rel_vel_y, rel_vel_x);
            rel_vel_mag = hypot(rel_vel_x, rel_vel_y);
        }
        else{
            rel_vel_x = 0;
            rel_vel_y = 0;
        }

        approach_vel = - rel_vel_mag * cos(rel_vel_heading - heading_to_person); 

        if(s->verbose){
            fprintf(stderr, "Approach Velocity : %f\n", approach_vel);
        }
        //calculating velocity relative to ground truth
        //carmen_point_t start_point = get_average_point_from_history(person_history_global, 
        carmen_point_t start_point_h = get_average_point_from_history_object(&(s->people_list.people_tracks[i].global_person_history),
                                                                             0,
                                                                             s->people_list.people_tracks[i].global_person_history.current_size/2.0-1);

        //carmen_point_t end_point = get_average_point_from_history(person_history_global, 
        carmen_point_t end_point_h = get_average_point_from_history_object(&(s->people_list.people_tracks[i].global_person_history),
                                                                           s->people_list.people_tracks[i].global_person_history.current_size/2.0,
                                                                           s->people_list.people_tracks[i].global_person_history.current_size-1);

        double dx = start_point_h.x - end_point_h.x; 
        //double dy = start_point_h.y - end_point_h.y; 

        double m = .0, c = .0;

        get_bestfit_from_history_object(&(s->people_list.people_tracks[i].global_person_history),
                                        0,
                                        s->people_list.people_tracks[i].global_person_history.current_size-1, &c, &m);
				    

        best_fit_heading = atan(m); //this can point to either way - need to use the location of the points to extract the actual angle 

        //calculating velocity relative to ground truth
        //carmen_point_t start_point = get_average_point_from_history(person_history_global, 
        carmen_point_t start_point = get_average_point_from_history_object(&(s->people_list.people_tracks[i].global_person_history_small),
                                                                           0,
                                                                           s->people_list.people_tracks[i].global_person_history_small.current_size/2.0-1);

        //carmen_point_t end_point = get_average_point_from_history(person_history_global, 
        carmen_point_t end_point = get_average_point_from_history_object(&(s->people_list.people_tracks[i].global_person_history_small),
                                                                         s->people_list.people_tracks[i].global_person_history_small.current_size/2.0,
                                                                         s->people_list.people_tracks[i].global_person_history_small.current_size-1);    
        if(dx < 0){//in the second or third quardrent 
            best_fit_heading = carmen_normalize_theta(best_fit_heading + M_PI);
        }

        if(s->verbose){
            fprintf(stderr,"c : %f, m : %f: Angle : %f\n", c, m, best_fit_heading / M_PI*180);
        }
    
        if(start_point.theta - end_point.theta >0){      
            vel_x = (start_point.x - end_point.x)/(start_point.theta - end_point.theta);
            vel_y = (start_point.y - end_point.y)/(start_point.theta - end_point.theta);
        }
        else{
            vel_x = 0;
            vel_y = 0;
        }
        //lets do an instantaenous calculation
        if(s->verbose){
            fprintf(stderr,"velocity : (%f,%f)\n", vel_x, vel_y);
        }

        u_vel_x = vel_x;
        u_vel_y = vel_y;
        if(hypot(vel_x,vel_y)<= 0.2){
            //u_vel_x = prev_velocity.x;
            //u_vel_y = prev_velocity.y;
            //set to zero if small motions
            u_vel_x = 0;
            u_vel_y = 0;
        }

        if(hypot(vel_x,vel_y)> 0.2){
            prev_velocity.x = vel_x;
            prev_velocity.y = vel_y;
        }

        //not sure if we should use this - if less than a certain value we should just set the magnitude to 0 
        double vel_mag = hypot(u_vel_x, u_vel_y);
        double vel_heading = atan2(u_vel_y,u_vel_x);
    
        //person_heading_average is pretty wrong when we smoothen it 
        s->people_list.people_tracks[i].tv_mag = vel_mag;

        if(vel_mag > 0.2){
            s->people_list.people_tracks[i].heading = best_fit_heading;//vel_heading;
        }

        if(approach_vel > 0.2){
            s->people_list.people_tracks[i].rel_vel = approach_vel; 
        }
    
        if(lcmgl){
            lcmglColor3f(1.0, .0, 0);
            //lcmglBegin(GL_LINES);

            //draw the person heading also 
      
            //not sure if we should draw this anymore 

            for(int k=0;k< s->people_list.people_tracks[i].global_person_history.current_size;k++){

                float rtheta = s->global_robot_pose.theta;
	
                double pos[3] = {.0,.0,.0};
                double leg_xy[2] = {s->people_list.people_tracks[i].global_person_history.history[k].x, s->people_list.people_tracks[i].global_person_history.history[k].y};

                get_global_point(leg_xy, pos,s);
	
                lcmglColor3f(1.0, .0, .0);

                lcmglLineWidth (5);      
                lcmglCircle(pos, 0.02);
            }      
        }
    }

    if(lcmgl){
        bot_lcmgl_switch_buffer (lcmgl);
    }

    lcmgl = s->lcmgl_velocity;
    
    if(lcmgl){
        for(int i=0; i< s->people_list.no_people; i++){    
            //we should have a threshold to decide if the person is not moving 
            //if not moving maybe use the old qualifying velocities??
            double x_vel, y_vel;
    
            //x_vel = vel_mag* cos(person_heading_average);
            x_vel = s->people_list.people_tracks[i].tv_mag* cos( s->people_list.people_tracks[i].heading);
            //y_vel = vel_mag* sin(person_heading_average);
            y_vel = s->people_list.people_tracks[i].tv_mag* sin(s->people_list.people_tracks[i].heading);

            //double alpha = atan2(guide_pos.y, guide_pos.x);
            double rtheta = s->global_robot_pose.theta; //+ alpha;
            double vel_g_x = x_vel*cos(rtheta) - y_vel*sin(rtheta);
            double vel_g_y = x_vel*sin(rtheta) + y_vel*cos(rtheta);           
      
            //if(hypot(vel_x, vel_y)> 0.2){
            if(i == s->people_list.active_person_ind){
                lcmglColor3f(1.0, .0, .0);
            }
            else{
                lcmglColor3f(0.0, 1.0, 0.2);
            }
            //}
            //else{
            //lcmglColor3f(1.0, 0.0, 1.0);
            //}

            if(s->verbose){
                fprintf(stderr,"Guide GPOS %f,%f\n" , s->people_list.people_tracks[i].gpos.x, 
                        s->people_list.people_tracks[i].gpos.y);
            }

            lcmglLineWidth (6);    
            lcmglBegin(GL_LINES);
            lcmglVertex3d(s->people_list.people_tracks[i].gpos.x, s->people_list.people_tracks[i].gpos.y, .0);
            lcmglVertex3d(s->people_list.people_tracks[i].gpos.x + vel_g_x, s->people_list.people_tracks[i].gpos.y+ vel_g_y, .0);
            lcmglEnd();

            if(0){//draw left and right side positions
                //temp calculation - done here 
                double alpha = atan2(y_vel, x_vel);

                //alternative positions
                double p1_alpha =  bot_mod2pi(alpha + M_PI/2);
                double p2_alpha =  bot_mod2pi(alpha - M_PI/2);

                double distance_to_side = 1.0;
     
                double p1_pos[3] = {.0,.0,.0};
                double p2_pos[3] = {.0,.0,.0};
 
                double p1_x = distance_to_side * cos(p1_alpha);
                double p1_y = distance_to_side * sin(p1_alpha);

                p1_pos[0] = s->people_list.people_tracks[i].gpos.x + p1_x*cos(rtheta) - p1_y*sin(rtheta);
                p1_pos[1] = s->people_list.people_tracks[i].gpos.y + p1_x*sin(rtheta) + p1_y*cos(rtheta);

                double p2_x = distance_to_side * cos(p2_alpha);
                double p2_y = distance_to_side * sin(p2_alpha);
      
                p2_pos[0] = s->people_list.people_tracks[i].gpos.x + p2_x*cos(rtheta) - p2_y*sin(rtheta);
                p2_pos[1] = s->people_list.people_tracks[i].gpos.y + p2_x*sin(rtheta) + p2_y*cos(rtheta);     


                lcmglLineWidth (8);    
                //left is red
                lcmglColor3f(1.0, 0.0, 0.0);
                lcmglCircle(p1_pos, 0.1);
                //right is 
                lcmglColor3f(1.0, 1.0, 0.0);
                lcmglCircle(p2_pos, 0.1);
            }
        }
        bot_lcmgl_switch_buffer (lcmgl);
    }

    if(s->tracking_state==LOOKING && 0){

        //check the people heading towards the robot 
    
        //??make two passes - first check if we have an observations 
        double max_dist = 2.0;
        double temp_dist;
        double temp_angle;
        fprintf(stderr,"Looking for person\n");
    
        double closest_dist = 1.0; 

        int candidate_ind = -1; 

        double min_angle_offset = 1000;

        fprintf(stderr, "+++++++++++++ Guide test ++++++++++++\n");
    
        for(j=0;j<s->people_list.no_people;j++){

      
            temp_dist = hypot(s->people_list.people_tracks[j].local_pos.x,
                              s->people_list.people_tracks[j].local_pos.y);
            temp_angle = bot_mod2pi(bot_mod2pi(atan2(s->people_list.people_tracks[j].local_pos.y,
                                                     s->people_list.people_tracks[j].local_pos.x)
                                               - s->people_list.people_tracks[j].heading) - M_PI);

            double p_heading = atan2(s->people_list.people_tracks[j].local_pos.y,
                                     s->people_list.people_tracks[j].local_pos.x);


            double m1 = tan(s->people_list.people_tracks[j].heading); 
            double c1= s->people_list.people_tracks[j].local_pos.y - m1 *  s->people_list.people_tracks[j].local_pos.x;
      
            double x2 = -c1 / (m1 + 1/m1); 
            double y2 = c1 / (m1*m1 + 1); 

            double c_dist = hypot(x2, y2); 

      
      
      

            if(temp_dist < max_dist && s->people_list.people_tracks[j].tv_mag > 0.3
               && c_dist < 1.0 && fabs(temp_angle) < bot_to_radians(30)){
                //increase the candiate counts 
                s->people_list.people_tracks[j].candidate_counts++; 

                if(closest_dist > c_dist){
                    closest_dist = c_dist; 
                    candidate_ind = -1;//j;	
                }  
            }
            else{
                s->people_list.people_tracks[j].candidate_counts = bot_max(0, 
                                                                           s->people_list.people_tracks[j].candidate_counts-1); 
	
            }

            if (s->verbose) {
                fprintf(stderr, "[%d] Closest Dist : %f Vel : %f Counts : %d Angle : %f\n", j , c_dist, 
                        s->people_list.people_tracks[j].tv_mag, 	s->people_list.people_tracks[j].candidate_counts, 
                        temp_angle ); 
            }

      
            //if(temp_dist < max_dist && (p_heading < bot_to_radians(60) && p_heading > bot_to_radians(-60))

            if(0){
                if(temp_dist < max_dist && (p_heading < bot_to_radians(60) && p_heading > bot_to_radians(-60))
                   && s->people_list.people_tracks[j].tv_mag > 0.2){
                    if(min_angle_offset > fabs(temp_angle)){
                        min_angle_offset = temp_angle; 
                        candidate_ind = j;
                    }
                }
            }
        }
    

        //loop through 
        if(s->people_list.active_person_ind == -1 && 0){
            int max_count = 0; 
            int ind = -1; 
            for (j=0;j<s->people_list.no_people;j++){
                if(max_count < s->people_list.people_tracks[j].candidate_counts){
                    max_count = s->people_list.people_tracks[j].candidate_counts;
                    ind = j;
                }
            }
            if(max_count > 10 && ind >=0){
                s->people_list.active_person_ind = ind;
                s->tracking_state== FOUND; 
            }
        }

        
        //kill the other filters only if we are in single person tracking mode 
        if(s->people_list.active_person_ind >=0){
            
            fprintf(stderr, "[[[[[[[[[[[[[ Person Track ID : %d ]]]]]]]]]]\n", s->people_list.people_tracks[s->people_list.active_person_ind].id);
            if(!s->multi_people_tracking){
                for(int k=0;k<s->people_list.no_people;k++){
                    if(k != s->people_list.active_person_ind){
                        s->people_list.people_tracks[k].time_since = .0; 
                        //s->time_since[k] = .0;
                    }
                }
            }
            
            update_tracking_state(FOUND,s);  //person located	
            //fprintf(stderr,"====== New Person Following Activated =========\n");      
        }
        else{
            //fprintf(stderr,"**** No good filter found **** \n");
        }
    }
    
    if(s->people_list.active_person_ind >=0 && s->verbose){
        fprintf(stderr, "==========Person Track ID : %d ============\n", s->people_list.people_tracks[s->people_list.active_person_ind].id);
    }
    
    if(s->verbose){
        for(j=0;j<s->people_list.no_people;j++){
            fprintf(stderr, " [%d] Local : %f,%f Global : %f,%f\n", s->people_list.people_tracks[j].id, 
                    s->people_list.people_tracks[j].local_pos.x, 
                    s->people_list.people_tracks[j].local_pos.y,
                    s->people_list.people_tracks[j].gpos.x, 
                    s->people_list.people_tracks[j].gpos.y); 
        }
    }
    
    if(s->verbose){
        fprintf(stderr,"Getting smoothed History\n");
        fprintf(stderr,"Guide Msg\n");
    }
    
    publish_guide_msg(time_obs, s);
    print_guide_msg(time_obs, s);

    if(s->people_list.no_people >=0){

        lcmgl = s->lcmgl_person;

        double pos[3];
    
        lcmglLineWidth (6);    

        for(i=0;i< s->people_list.no_people;i++){
            float person_x = .0, person_y = .0;
            float theta = s->global_robot_pose.theta;

            person_x = s->people_list.people_summary[i].mean.x;
            person_y = s->people_list.people_summary[i].mean.y;
      
            pos[0] = s->people_list.people_tracks[i].gpos.x;
            pos[1] = s->people_list.people_tracks[i].gpos.y;
            pos[2] = .0;  

            if (1 && lcmgl) {//turn to 1 if u want to draw
                lcmglLineWidth (5);
                if(i==s->people_list.active_person_ind){
                    lcmglColor3f(1.0, .0, .0);
                }
                else{
                    lcmglColor3f(.0, 1.0, 1.0);
                }
                lcmglCircle(pos, 0.15);
            
      
                if(s->verbose){
                    char seg_info[512];
                    sprintf(seg_info,"[%d] [P]: %.2f, %.2f [O]: %d\n[T]%.3f [H]:%.3f\n[tv]:%.3f [rv]:%.3f [moved] : %d", s->people_list.people_tracks[i].id, 
                            s->people_list.people_tracks[i].local_pos.x, s->people_list.people_tracks[i].local_pos.y,
                            s->people_list.people_tracks[i].found_obs_last,
                            s->people_list.people_tracks[i].time_since - time_obs, 
                            bot_to_degrees(s->people_list.people_tracks[i].heading), 
                            s->people_list.people_tracks[i].tv_mag,
                            s->people_list.people_tracks[i].rv_mag, 
                            s->people_list.people_tracks[i].motion_count
                            );
                    bot_lcmgl_text(lcmgl, pos, seg_info);
                }   
            }  
        }
        if(lcmgl){
            bot_lcmgl_switch_buffer (lcmgl);
        }
    }
  
    if(s->verbose){
        fprintf(stderr,"No of filters %d \n", s->people_list.no_people);
        fprintf(stderr,"------------------------------------------------------------\n");
    }
    free(person_det);
    free(full_person_det);

    for (i=0;i<meta_count;i++){
        free(dist[i]);
    }
    free(dist);

    for (j=0;j<original_filter_size;j++){
        free(allocated_observations[j]);
    }
    free(allocated_observations);
    free(no_obs_count);
    prev_time = time_obs;
    free(allocated_to_filter);
    free(used_person_obs.locations);
    free(prev_points);
    prev_points = points;
    prev_no_points = no_points;

    return 1;
}


void lcm_robotlaser_to_carmen_laser(erlcm_robot_laser_t *msg, carmen_robot_laser_message *robot, double laser_offset)
{
    //convert the lcm message to a carmen message because all the functions have been 
    //already written to handle carmen messages

    robot->config.accuracy = .1;
    robot->config.laser_type = HOKUYO_UTM;
    robot->config.remission_mode = REMISSION_NONE;
    robot->config.angular_resolution = msg->laser.radstep;
    robot->config.fov = msg->laser.nranges * msg->laser.radstep;
    robot->config.maximum_range = 30; 
    robot->config.start_angle = msg->laser.rad0;
    robot->num_readings = msg->laser.nranges;
    robot->range = msg->laser.ranges;
    robot->robot_pose.x = msg->pose.pos[0];
    robot->robot_pose.y = msg->pose.pos[1];
    double rpy[3] = {0,0,0};  
    bot_quat_to_roll_pitch_yaw (msg->pose.orientation, rpy) ;
    robot->robot_pose.theta = rpy[2];
    robot->laser_pose.x = robot->robot_pose.x + laser_offset*cos(rpy[2]);
    robot->laser_pose.y = robot->robot_pose.y + laser_offset*sin(rpy[2]);
    robot->laser_pose.theta = rpy[2];
    robot->timestamp = msg->utime/1e6;
}

void lcm_planar_laser_to_robot_laser(bot_core_planar_lidar_t *laser, erlcm_robot_laser_t *robot, 
                                     state_t *s)
{

    memcpy(&(robot->laser),laser, sizeof(bot_core_planar_lidar_t));
  
    if(s->odom_msg!=NULL){
        robot->pose.pos[0] = s->odom_msg->x;
        robot->pose.pos[1] = s->odom_msg->y;
    
        double rpy[3] = {.0,.0,s->odom_msg->theta};
        bot_roll_pitch_yaw_to_quat (rpy, robot->pose.orientation) ;
    }

    robot->utime = laser->utime;
    if(s->verbose)
        fprintf(stderr, "Utime : %f\n", (double) (robot->utime)); 
}




void lcm_planar_laser_to_carmen_laser(bot_core_planar_lidar_t *laser, carmen_robot_laser_message *robot, 
                                      double laser_offset, double laser_ang_offset, state_t *s)
{
    //convert the lcm message to a carmen message because all the functions have been 
    //already written to handle carmen messages

    robot->config.accuracy = .1;
    robot->config.laser_type = HOKUYO_UTM;
    robot->config.remission_mode = REMISSION_NONE;
    robot->config.angular_resolution = laser->radstep;
    robot->config.fov = laser->nranges * laser->radstep;
    robot->config.maximum_range = 30; 
    robot->config.start_angle = laser->rad0 + laser_ang_offset;
    robot->num_readings = laser->nranges;
    robot->range = laser->ranges;
    if(s->odom_msg!=NULL){
        robot->robot_pose.x = s->odom_msg->x;//global_robot_pose.x;
        robot->robot_pose.y = s->odom_msg->y;//global_robot_pose.y;
        robot->robot_pose.theta = carmen_normalize_theta(s->odom_msg->theta);//global_robot_pose.theta;
    }
    else{
        robot->robot_pose.x = .0;//global_robot_pose.x;
        robot->robot_pose.y = .0;//global_robot_pose.y;
        robot->robot_pose.theta = .0;//global_robot_pose.theta;
    }
    robot->laser_pose.x = robot->robot_pose.x + laser_offset*cos(robot->robot_pose.theta);
    robot->laser_pose.y = robot->robot_pose.y + laser_offset*sin(robot->robot_pose.theta);
    robot->laser_pose.theta = robot->robot_pose.theta;
    robot->timestamp = laser->utime/1e6;
}


static void multi_gridmap_handler(const lcm_recv_buf_t *rbuf, 
                                  const char *channel, 
                                  const erlcm_multi_gridmap_t *msg, 
                                  void *user)
{
    state_t *s = (state_t *)user;
    if (s->verbose)
        fprintf(stderr,"=M= : Current Floor : %d\n",msg->current_floor_ind);

    static erlcm_gridmap_t* staticmsg = NULL;
    if (staticmsg != NULL) {
        erlcm_gridmap_t_destroy(staticmsg);
    }
    staticmsg = erlcm_gridmap_t_copy(&msg->maps[msg->current_floor_ind].gridmap);
  
    /*if(current_map !=NULL){
      erlcm_gridmap_t_destroy(current_map);
      }
      current_map =  erlcm_gridmap_t_copy(msg->maps[msg->current_floor].gridmap);	*/
    if (s->current_map.map != NULL)
        free(s->current_map.map);
    if (s->current_map.complete_map != NULL)
        free(s->current_map.complete_map);
    carmen3d_map_uncompress_lcm_map(&(s->current_map), staticmsg);
}

static void mapserver_gridmap_handler(const lcm_recv_buf_t *rbuf, 
                                      const char *channel, 
                                      const erlcm_gridmap_t *msg, 
                                      void *user)
{
    state_t *s = (state_t *)user; 
    static erlcm_gridmap_t* staticmsg = NULL;
    if (staticmsg != NULL) {
        erlcm_gridmap_t_destroy(staticmsg);
    }
    staticmsg = erlcm_gridmap_t_copy(msg);
  
    /*if(current_map !=NULL){
      erlcm_gridmap_t_destroy(current_map);
      }
      current_map =  erlcm_gridmap_t_copy(msg->maps[msg->current_floor].gridmap);	*/
    if (s->current_map.map != NULL)
        free(s->current_map.map);
    if (s->current_map.complete_map != NULL)
        free(s->current_map.complete_map);
    carmen3d_map_uncompress_lcm_map(&(s->current_map), staticmsg);
    s->current_map.map_zero.x = .0;
    s->current_map.map_zero.y = .0;
}


/*void speech_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const erlcm_speech_cmd_t * msg,
  void * user  __attribute__((unused)))
  {
  state_t *s = (state_t *)user;
  static erlcm_speech_cmd_t* staticmsg = NULL;

  if (staticmsg != NULL) {
  erlcm_speech_cmd_t_destroy(staticmsg);
  }
  staticmsg = erlcm_speech_cmd_t_copy(msg);
  char* cmd = staticmsg->cmd_type;
  char* property = staticmsg->cmd_property;

  //person tracker related speech commands
  if(strcmp(cmd,"TRACKER")==0){
  if(strcmp(property,"START_LOOKING")==0){
  //guide has asked us to look for him
  if(!s->use_kinect){
  update_tracking_state(LOOKING,s);
  }
  fprintf(stderr,"Start to track command heard - waiting for kinect init\n");
  }    
  else if(strcmp(property,"STATUS")==0){
  querry_tracking_state(s);
  }
  }
  if(strcmp(cmd,"FOLLOWER")==0){
  if(strcmp(property,"START_FOLLOWING")==0){
  //guide has asked us to follow him 
  //this we use only if we do not have the person in sight
  if(s->people_list.active_person_ind ==-1){
  update_tracking_state(LOOKING,s);
  fprintf(stderr,"Starting to track\n");
  }
  }  
  }
  }*/

void tracking_cmd_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const erlcm_person_tracking_cmd_t * msg,
                          void * user  __attribute__((unused)))
{
    state_t *s = (state_t *)user;

    if (s->verbose)
        fprintf(stderr,"Tracking Cmd received\n");

    if(msg->command == ERLCM_PERSON_TRACKING_CMD_T_CMD_START_TRACKING){
        if(!s->use_kinect){
            //update_tracking_state(LOOKING,s);
            //if use kinect - we use the latest kinect position - otherwise - we use the normal in front position      
            fprintf(stderr,"Start to track command heard - waiting for person to move to front\n");
        }
        else{
            fprintf(stderr,"Start to track command heard - waiting for kinect init\n");
        }
    }
    else if(msg->command == ERLCM_PERSON_TRACKING_CMD_T_CMD_PERSON_IN_FRONT){
        //reset tracking 
        //update_tracking_state(LOST,s);
        
        
        fprintf(stderr, "Dialog manager to reinit person - Setting to LOST\n");

        if(!s->use_kinect){
            //update_tracking_state(LOOKING,s);
            //if use kinect - we use the latest kinect position - otherwise - we use the normal in front position      
            fprintf(stderr,"Start to track command heard - waiting for person to move in front\n");
            update_tracking_state(LOOKING,s);
        }
        else{
            fprintf(stderr,"Start to track command heard - waiting for kinect init\n");
        }
    }
    else if(msg->command == ERLCM_PERSON_TRACKING_CMD_T_CMD_SEND_STATUS){
        querry_tracking_state(s);
    }
    
}

//person tracker related speech commands
/*if(strcmp(cmd,"TRACKER")==0){
  if(strcmp(property,"START_LOOKING")==0){
  //guide has asked us to look for him
  if(!s->use_kinect){
  update_tracking_state(LOOKING,s);
  }
  fprintf(stderr,"Start to track command heard - waiting for kinect init\n");
  }    
  else if(strcmp(property,"STATUS")==0){
  querry_tracking_state(s);
  }
  }
  if(strcmp(cmd,"FOLLOWER")==0){
  if(strcmp(property,"START_FOLLOWING")==0){
  //guide has asked us to follow him 
  //this we use only if we do not have the person in sight
  if(s->people_list.active_person_ind ==-1){
  update_tracking_state(LOOKING,s);
  fprintf(stderr,"Starting to track\n");
  }
  }  
  }
  }*/


void lcm_frontlaser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const erlcm_robot_laser_t * msg,
                            void * user  __attribute__((unused)))
{  
    state_t *s = (state_t *) user; 
    //static erlcm_robot_laser_t* staticmsg = NULL;

    if (s->robot_fl != NULL) {
        erlcm_robot_laser_t_destroy(s->robot_fl);
    }
    s->robot_fl = erlcm_robot_laser_t_copy(msg);

    //lcm_robotlaser_to_carmen_laser(s->robot_fl,&s->carmen_robot_frontlaser, s->param.front_laser_offset);
    s->have_front_laser = 1;

    if(s->verbose)
        fprintf(stderr,"Mode : %d\n", s->tourguide_mode);
  
    if(s->multi_people_tracking){
        if(s->verbose){
            fprintf(stderr,"Multi Person tracking\n");
        }
        lcm_detect_person_both_basic(s);
    }
    else{
        if(s->verbose){
            fprintf(stderr,"Single Person tracking\n");
        }
        lcm_detect_person_both_basic(s);
    }

    double rpy[3] = {0,0,0};  
    bot_quat_to_roll_pitch_yaw (s->robot_fl->pose.orientation, rpy) ;

    s->last_robot_position.x = s->robot_fl->pose.pos[0];
    s->last_robot_position.y = s->robot_fl->pose.pos[1];
    s->last_robot_position.theta = rpy[2]; 
  
}

void lcm_rearlaser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const erlcm_robot_laser_t * msg,
                           void * user  __attribute__((unused)))
{
    state_t *s = (state_t *) user; 
    //  erlcm_robot_laser_t* staticmsg = s->robot_rl;

    if (s->robot_rl != NULL) {
        erlcm_robot_laser_t_destroy(s->robot_rl);
    }
    s->robot_rl = erlcm_robot_laser_t_copy(msg);

    //we only add update the rear laser data - we only run the localize once 
    //lcm_robotlaser_to_carmen_laser(staticmsg,&s->carmen_robot_rearlaser, s->param.rear_laser_offset);  

    s->have_rear_laser = 1;
    if(s->verbose){
        fprintf(stderr, "Have Rear Laser\n");
    }
}

void lcm_base_odometry_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                               const char * channel __attribute__((unused)), 
                               const erlcm_raw_odometry_msg_t * msg,
                               void * user  __attribute__((unused)))
{
    state_t *s = (state_t *) user; 
    if(s->verbose){
        fprintf(stderr,"ODOM\n");
    }
    if (s->odom_msg != NULL) {
        erlcm_raw_odometry_msg_t_destroy(s->odom_msg);
    }
    s->odom_msg = erlcm_raw_odometry_msg_t_copy(msg);
}

void lcm_frontplanar_laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                                   const bot_core_planar_lidar_t * msg,
                                   void * user  __attribute__((unused)))
{
    state_t *s = (state_t *)user; 
  

    if(s->robot_fl ==NULL){
        s->robot_fl = calloc(1, sizeof(erlcm_robot_laser_t)); 
    }

    if (s->planar_fl != NULL) {
        bot_core_planar_lidar_t_destroy(s->planar_fl);
    }
    s->planar_fl = bot_core_planar_lidar_t_copy(msg);

    memcpy(&(s->robot_fl->laser), s->planar_fl, sizeof(bot_core_planar_lidar_t));
  
    if(s->odom_msg!=NULL){
        s->robot_fl->pose.pos[0] = s->odom_msg->x;
        s->robot_fl->pose.pos[1] = s->odom_msg->y;
    
        double rpy[3] = {.0,.0,s->odom_msg->theta};
        bot_roll_pitch_yaw_to_quat (rpy, s->robot_fl->pose.orientation) ;
    }

    s->robot_fl->utime = s->planar_fl->utime;
  
    if(s->verbose){
        fprintf(stderr, "Utime : %f\n", (double) (s->robot_fl->utime)); 
        fprintf(stderr,"Mode : %d\n", s->tourguide_mode);
    }
    s->have_front_laser = 1;

    //detect_person_both();
    if(s->multi_people_tracking){
        lcm_detect_person_both_basic(s);
    }
    else{
        if(s->verbose){
            fprintf(stderr,"Single Person tracking\n");
        }
        lcm_detect_person_both_basic(s);
    }

    double rpy[3] = {0,0,0};  
    bot_quat_to_roll_pitch_yaw (s->robot_fl->pose.orientation, rpy) ;


    s->last_robot_position.x = s->robot_fl->pose.pos[0];
    s->last_robot_position.y = s->robot_fl->pose.pos[1];
    s->last_robot_position.theta = rpy[2]; 
}

void lcm_rearplanar_laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                                  const bot_core_planar_lidar_t * msg,
                                  void * user  __attribute__((unused)))
{
    state_t *s = (state_t *)user; 
    bot_core_planar_lidar_t* staticmsg = s->planar_rl;

    if (staticmsg != NULL) {
        bot_core_planar_lidar_t_destroy(staticmsg);
    }
    staticmsg = bot_core_planar_lidar_t_copy(msg);

    //lcm_planar_laser_to_carmen_laser(staticmsg,&s->carmen_robot_rearlaser, s->param.rear_laser_offset, s->param.rear_laser_angle_offset, s);
    //lcm_planar_laser_to_robot_laser(staticmsg, s->robot_rl, s);
    if(s->robot_rl ==NULL){
        s->robot_rl = calloc(1, sizeof(erlcm_robot_laser_t)); 
    }

    memcpy(&(s->robot_rl->laser),staticmsg, sizeof(bot_core_planar_lidar_t));
  
    if(s->odom_msg!=NULL){
        s->robot_rl->pose.pos[0] = s->odom_msg->x;//global_robot_pose.x;
        s->robot_rl->pose.pos[1] = s->odom_msg->y;//global_robot_pose.y;
    
        double rpy[3] = {.0,.0,s->odom_msg->theta};
        bot_roll_pitch_yaw_to_quat (rpy, s->robot_rl->pose.orientation) ;
    }

    s->robot_rl->utime = staticmsg->utime;

    s->have_rear_laser = 1;
}



static void pose_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                         const bot_core_pose_t * msg, void * user  __attribute__((unused))) 
{
    state_t *s = (state_t *)user; 
    static bot_core_pose_t *prevUpdate = NULL;

    if (prevUpdate == NULL) {
        prevUpdate = bot_core_pose_t_copy(msg);
    }

    double rpy[3];
    bot_quat_to_roll_pitch_yaw(msg->orientation, rpy);
  
    s->global_robot_pose.x = msg->pos[0];
    s->global_robot_pose.y = msg->pos[1];
    s->global_robot_pose.theta = rpy[2];

    if(s->verbose){
        fprintf(stderr,"Pos (x,y,theta) : (%f,%f,%f)\n",
                msg->pos[0],msg->pos[1],rpy[2]);    
    }
}

void read_parameters(state_t *s)
{
    BotParam *c = s->b_server;
    BotFrames *frames = bot_frames_get_global (s->lcm, s->b_server);

    char key[2048];
    int no_particles = 0;
    sprintf(key, "%s.no_particles", "person_tracking");

    //no particles 
    if (0 != bot_param_get_int(c, key, &no_particles)){
        fprintf(stderr,"\tError Reading Params\n");
    }else{
        fprintf(stderr,"\tNo of Particles : %d\n",no_particles);
    }
    s->param.num_particles = no_particles;
    

       // Find the position of the forward-facing LIDAR
    char *coord_frame;
    if (s->high_laser)
        coord_frame = bot_param_get_planar_lidar_coord_frame (c, "TOP_LASER");
    else
        coord_frame = bot_param_get_planar_lidar_coord_frame (c, "SKIRT_FRONT");

    if (!coord_frame)
        fprintf (stderr, "\tError determining front laser coordinate frame\n");

    BotTrans front_laser_to_body;
    if (!bot_frames_get_trans (frames, coord_frame, "body", &front_laser_to_body))
        fprintf (stderr, "\tError determining front laser coordinate frame\n");
    else
        fprintf(stderr,"\tFront Laser Pos : (%f,%f,%f)\n",
                front_laser_to_body.trans_vec[0], front_laser_to_body.trans_vec[1], front_laser_to_body.trans_vec[2]);

    s->param.front_laser_offset = front_laser_to_body.trans_vec[0];
    s->param.front_laser_side_offset = front_laser_to_body.trans_vec[1];

    s->front_laser_offset.dx = front_laser_to_body.trans_vec[0];
    s->front_laser_offset.dy = front_laser_to_body.trans_vec[1];

    fprintf(stderr,"\tFront Laser Pos : %f,%f\n", s->param.front_laser_offset, 
            s->param.front_laser_side_offset);


    double front_rpy[3];
    bot_quat_to_roll_pitch_yaw (front_laser_to_body.rot_quat, front_rpy);
    s->param.front_laser_angle_offset  = front_rpy[2];
    s->front_laser_offset.dtheta = front_rpy[2];



    // Now for the rear laser
    coord_frame = NULL;
    coord_frame = bot_param_get_planar_lidar_coord_frame (c, "SKIRT_REAR");

    if (!coord_frame)
        fprintf (stderr, "\tError determining rear laser coordinate frame\n");

    BotTrans rear_laser_to_body;
    if (!bot_frames_get_trans (frames, coord_frame, "body", &rear_laser_to_body))
        fprintf (stderr, "\tError determining LIDAR coordinate frame\n");
    else
        fprintf(stderr,"\tRear Laser Pos : (%f,%f,%f)\n",
                rear_laser_to_body.trans_vec[0], rear_laser_to_body.trans_vec[1], rear_laser_to_body.trans_vec[2]);

    s->param.rear_laser_offset = rear_laser_to_body.trans_vec[0];
    s->param.rear_laser_side_offset = rear_laser_to_body.trans_vec[1];

    s->rear_laser_offset.dx = rear_laser_to_body.trans_vec[0];
    s->rear_laser_offset.dy = rear_laser_to_body.trans_vec[1];

    fprintf(stderr,"\tRear Laser Pos : %f,%f\n", s->param.rear_laser_offset, 
            s->param.rear_laser_side_offset);

    double rear_rpy[3];
    bot_quat_to_roll_pitch_yaw (rear_laser_to_body.rot_quat, rear_rpy);
    s->rear_laser_offset.dtheta = rear_rpy[2];
    s->param.rear_laser_angle_offset = rear_rpy[2];



    /* 
     * //laser params
     * double position[3];
     * if(s->high_laser){
     *     sprintf(key, "%s.%s.%s.position", "calibration", "planar_lidars", "TOP_LASER");
     * }
     * else{
     *     sprintf(key, "%s.%s.%s.position", "calibration", "planar_lidars", "SKIRT_FRONT");
     * }
     * if(3 != bot_param_get_double_array(c, key, position, 3)){
     *     fprintf(stderr,"\tError Reading Params\n");
     * }else{
     *     fprintf(stderr,"\tFront Laser Pos : (%f,%f,%f)\n",position[0], position[1], position[2]);
     * }
     * s->param.front_laser_offset = position[0];
     * s->param.front_laser_side_offset = position[1];
     * 
     * s->front_laser_offset.dx = position[0];
     * s->front_laser_offset.dy = position[1];
     * 
     * fprintf(stderr,"\tFront Laser Pos : %f,%f\n", s->param.front_laser_offset, 
     *         s->param.front_laser_side_offset);
     * 
     * sprintf(key, "%s.%s.%s.position", "calibration", "planar_lidars", "SKIRT_REAR");
     * if(3 != bot_param_get_double_array(c, key, position, 3)){
     *     fprintf(stderr,"\tError Reading Params\n");
     * }else{
     *     fprintf(stderr,"\tRear Laser Pos : (%f,%f,%f)\n",position[0], position[1], position[2]);
     * }
     * s->param.rear_laser_offset = position[0];
     * s->rear_laser_offset.dx = position[0];
     * s->rear_laser_offset.dy = position[1];
     * 
     * double rpy[3];
     * if(s->high_laser){
     *     sprintf(key, "%s.%s.%s.rpy", "calibration", "planar_lidars", "TOP_LASER");
     * }
     * else{
     *     sprintf(key, "%s.%s.%s.rpy", "calibration", "planar_lidars", "SKIRT_FRONT");
     * }
     * if(3 != bot_param_get_double_array(c, key, rpy, 3)){
     *     fprintf(stderr,"\tError Reading Params\n");
     * }else{
     *     fprintf(stderr,"\tFront Laser RPY : (%f,%f,%f)\n",rpy[0], rpy[1], rpy[2]);
     * }
     * s->param.front_laser_angle_offset  = carmen_degrees_to_radians(rpy[2]);
     * s->front_laser_offset.dtheta = carmen_degrees_to_radians(rpy[2]);
     * 
     * sprintf(key, "%s.%s.%s.rpy", "calibration", "planar_lidars", "SKIRT_REAR");
     * if(3 != bot_param_get_double_array(c, key, rpy, 3)){
     *     fprintf(stderr,"\tError Reading Params\n");
     * }else{
     *     fprintf(stderr,"\tRear Laser RPY : (%f,%f,%f)\n",rpy[0], rpy[1], rpy[2]);
     * }
     * s->rear_laser_offset.dtheta = carmen_degrees_to_radians(rpy[2]);
     * s->param.rear_laser_angle_offset = carmen_degrees_to_radians(rpy[2]);
     */
}

void shutdown_tracker(int x)
{
    if(x == SIGINT) {
        carmen_verbose("Disconnecting from IPC network.\n");
        exit(1);
    }
}

int main(int argc, char **argv)
{
    //carmen3d_localize_param_t param;

    state_t *s = calloc(1, sizeof(state_t)); 
    //set to default
    s->high_laser = 0; 
    s->pruning_mode = 1; 

    int draw_all = 0;

    int c;

    while ((c = getopt (argc, argv, "cklfmhdbP:v")) >= 0) {
        switch (c) {
        case 'l':
            s->use_planar_laser = 1;
            break;
        case 'h':
            s->high_laser = 1;
            fprintf(stderr, "Using High laser\n");
            break;
        case 'f':
            s->multi_people_tracking = 1;
            fprintf(stderr,"Tracking multiple people\n");
            break;
        case 'm':
            s->use_map = 1;
            break;
        case 'd':
            draw_all =1;
            fprintf(stderr,"Drawing all");      
            break;      
        case 'c':
            s->create_moving = 1;
        case 'v':
            s->verbose = 1;
            fprintf(stderr,"Verbose");      
            break; 
        case 'P':      
            s->pruning_mode = atoi(strdup(optarg));
            if(s->pruning_mode > 2){
                s->pruning_mode = 0;
                fprintf(stderr, "Invalid Pruning Mode - Select 0-2 \n(0 - no pruning, 1 - prune around guide, 2 - new pruning\n");  
            }
            fprintf(stderr,"Pruning Mode : %d\n", s->pruning_mode);
            break; 
        case 'k':
            s->use_kinect = 1;
            fprintf(stderr, "Using Kinect");
            break;
        case '?':
            fprintf (stderr, "Usage: %s [-l]\n\
                        Options:\n			\
                        -l     Use Planar Laser \n  \
                        -b     Use basic mode (use the front laser and rear laser)\n  \
                        -m     Use map to remove false observations\n  \
                        -d     Debug mode - prints some debug information\n \
                        -r     Clears observations from the rear laser - not working\n"
	       
                     , argv[0]);
        return 1;
        }
    }

    s->robot_fl = NULL;
    s->robot_rl = NULL;
    s->planar_fl = NULL;
    s->planar_rl = NULL; 

    s->people_list.people_tracks = NULL; 
    s->people_list.no_people = 0;
    s->people_list.active_person_ind = -1;
    s->people_list.people_summary = NULL;
    s->check_gridmap = NULL;
    gboolean publish_check_gridmap = TRUE;

    if(s->use_map){
        s->check_gridmap = check_gridmap_create (0, 0, TRUE, FALSE, publish_check_gridmap , TRUE, FALSE);
    }

    init_point_avg_object(&(s->robot_history), MAX_REL_PERSON_HISTORY);

    s->current_map.map = NULL;
    s->current_map.complete_map = NULL;
    /* Setup exit handler */
    signal(SIGINT, shutdown_tracker);

    s->lcm = bot_lcm_get_global(NULL);
  
    s->b_server = bot_param_new_from_server(s->lcm, 1);
  
    s->lcmgl_person_history = NULL;
    s->lcmgl_person_zone =  NULL;
    s->lcmgl_moved_legs = NULL;
    s->lcmgl_leg_obs = NULL; 

    s->lcmgl_act_pos = bot_lcmgl_init(s->lcm, "act_pos");
    s->lcmgl_kp_position =  bot_lcmgl_init(s->lcm, "kinect_pos");

    if(draw_all){
        s->lcmgl_person_history = bot_lcmgl_init(s->lcm, "person_history");
        s->lcmgl_person_zone = bot_lcmgl_init(s->lcm, "person_zone");
        s->lcmgl_moved_legs = bot_lcmgl_init(s->lcm, "moved_leg_obs");
        s->lcmgl_leg_obs = bot_lcmgl_init(s->lcm,"leg_obs");
    }

    s->lcmgl_velocity = bot_lcmgl_init(s->lcm, "velocity");

    s->lcmgl_person = bot_lcmgl_init(s->lcm, "person");
  
    read_parameters(s);

    fprintf(stderr,"Parameters No of Particles : %d \n",s->param.num_particles);

    //std deviation of the gaussian distributed particles 
    s->std.x = 1.0;
    s->std.y = 1.0;
    s->std.theta = carmen_degrees_to_radians(4.0); 

    s->people_list.actual_filter_size = 5; 
    s->people_list.active_person_ind = -1;

    s->cr = NULL; 
  
    s->following_state = IDLE;
    s->tracking_state = NOT_LOOKING;  
    send_tracking_changed_status(s);

    initialize_people_filters(s);
  
    s->global_robot_pose.x = 0.0;
    s->global_robot_pose.y = 0.0;
    s->global_robot_pose.theta = 0.0;

    fprintf(stderr,"Subscribing to LCM Laser\n");

    if(s->use_planar_laser){
        if(s->high_laser){
            bot_core_planar_lidar_t_subscribe(s->lcm,"TOP_LASER", lcm_frontplanar_laser_handler, s);
        }
        else{
            bot_core_planar_lidar_t_subscribe(s->lcm,"SKIRT_FRONT", lcm_frontplanar_laser_handler, s);
            bot_core_planar_lidar_t_subscribe(s->lcm,"SKIRT_REAR", lcm_rearplanar_laser_handler, s);
        }
        erlcm_raw_odometry_msg_t_subscribe(s->lcm, "ODOMETRY", lcm_base_odometry_handler,s);
    }
    else{
        if(!s->high_laser){
            erlcm_robot_laser_t_subscribe(s->lcm,"REAR_ROBOT_LASER", lcm_rearlaser_handler, s);
            erlcm_robot_laser_t_subscribe(s->lcm,"ROBOT_LASER", lcm_frontlaser_handler, s);
        }
    }
    bot_core_pose_t_subscribe(s->lcm, POSE_CHANNEL, pose_handler, s);
    if(s->use_map){
        fprintf(stderr,"Using the map to remove person observations\n");
        erlcm_multi_gridmap_t_subscribe(s->lcm, "MULTI_FLOOR_MAPS", multi_gridmap_handler, s); 
        erlcm_gridmap_t_subscribe(s->lcm, "MAP_SERVER", mapserver_gridmap_handler, s); 
    }

    //prob should handle only the tracker related stuff here
    //erlcm_speech_cmd_t_subscribe(s->lcm, "PERSON_TRACKER", speech_handler, s);
    
    erlcm_person_tracking_cmd_t_subscribe(s->lcm, "PERSON_TRACKING_CMD", tracking_cmd_handler, s);
  
    //for messages sent from the N810
    //erlcm_speech_cmd_t_subscribe(s->lcm, "TABLET_FOLLOWER", speech_handler, s);

    erlcm_tagged_node_t_subscribe(s->lcm, "WHEELCHAIR_MODE", mode_handler, s);
    
    //erlcm_kinect_person_tracker_cmd_t_subscribe(s->lcm, "KINECT_TRACKER", kinect_init_handler, s);
    erlcm_kinect_person_tracker_status_t_subscribe(s->lcm, "KINECT_PERSON_STATUS_SERVO", kinect_init_handler, s);

    //send_servo_speed(s, 100, 512);

    //lets use glib laser 
    while (1)
        lcm_handle (s->lcm);

    return 0;
}
















































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































