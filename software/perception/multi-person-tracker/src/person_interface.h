#ifndef PERSON_INTERFACE_H
#define PERSON_INTERFACE_H

#include "person_messages.h"
#include <check_gridmap/check_gridmap.h>
#include <lcmtypes/er_lcmtypes.h>

#ifdef __cplusplus
extern "C" {
#endif

    typedef struct {
        float center_x, center_y, radius;
    } carmen_circle;

    typedef struct {
        double dx;
        double dy;
        double dtheta;
    } laser_offset_t;

    typedef struct {
        double x;
        double y;
        double theta;
        int front_laser;
    } carmen_leg_t;

    typedef struct {
        float center_x, center_y;
        int start_segment, end_segment;
        int front_laser;
    } carmen_feet_seg;

    /*typedef struct {
      float center_x, center_y, size;
      } carmen_feet_seg;*/

    typedef struct {
        carmen_feet_seg* locations;
        //carmen_circle pruned_obs;
        int no_obs;
        //float robot_theta;
        carmen_point_t robot_pose;
    } carmen_feet_observations;

    typedef struct {
        carmen_circle* locations;
        //carmen_circle pruned_obs;
        int no_obs;
        //float robot_theta;
        carmen_point_t robot_pose;
    } carmen_person_observations;

    typedef struct {
        carmen_circle* elements;
        int count;
    } carmen_circle_set;

    typedef struct {
        carmen_feet_seg* elements;
        int count;
    } carmen_feet_set;

    typedef struct _laser_point2d {
        double pos[2]; 
    } laser_point2d;

    typedef struct _laser_segment {
        laser_point2d *points;
        double xy[2]; 
        //features         
        int index; 
        int is_person; 
        int no_points;
        double width;  
        double jump_dist_pre;
        double jump_dist_suc;
        double sdev;
        double mean_dev_from_median;
        double linearity;
        double circulrity;
        double radius;
        double boundry_length;
        double boundry_regularity;
        double mean_curvature;
        double mean_angular_difference;
        //double mean_speed;// - harder to do 
    } laser_segment; 

    typedef struct _segment_collection {
        int no_segments;
        laser_segment *segments;
    } segment_collection;

    /*enum PRUNING_MODE{
      NONE, //do not prune
      PERSON, // remove observations not near the person 
      ROBOT //remove observations not near the robot 
      };*/


    carmen_person_observations
    prune_observations(carmen_person_observations* person_obs);

    int 
    merge_legs(carmen_person_observations *person_obs);

    carmen_person_observations 
    prune_segments(carmen_person_observations* person_obs, carmen_person_observations* prev_person_obs);

    /*
      int prune_segments(carmen_person_observations* person_obs, carmen_person_observations* prev_person_obs);
    */

    //carmen_feet_observations
    int
    get_moving_feet_segments(carmen_feet_observations* new_obs, carmen_feet_observations* moved_obs, carmen_point_p points, int no, check_gridmap_t *gm);

    int
    get_small_moving_feet_segments(carmen_feet_observations* new_obs, carmen_feet_observations* moved_obs, carmen_point_p points, int no, check_gridmap_t *gm);

    int
    get_moving_feet_segments_1(carmen_feet_observations* new_obs, carmen_feet_observations* prev_obs, 
                               carmen_feet_observations* moved_obs, carmen_point_p points, int no);


    carmen_feet_observations
    lcm_detect_segments_from_robot_laser(erlcm_robot_laser_t *fl, laser_offset_t fl_offset,
                                         erlcm_robot_laser_t *rl, laser_offset_t rl_offset,
                                         float max_seg_point_gap,
                                         carmen_point_p points, int *no_points, int high_mode, 
                                         int use_classifier);

    carmen_feet_observations
    lcm_detect_segments_from_planar_lidar(bot_core_planar_lidar_t *fl, laser_offset_t fl_offset, bot_core_pose_t fl_pose, 
                                          bot_core_planar_lidar_t *rl, laser_offset_t rl_offset, bot_core_pose_t rl_pose, 
                                          float max_seg_point_gap,
                                          carmen_point_p points, int *no_points, int high_mode, 
                                          int use_classifier);

    carmen_feet_observations
    lcm_detect_segments_from_robot_laser_pruned(erlcm_robot_laser_t *fl, laser_offset_t fl_offset,
                                                erlcm_robot_laser_t *rl, laser_offset_t rl_offset,
                                                float max_seg_point_gap,
                                                carmen_point_p points, int *no_points, 
                                                carmen_point_p person_loc, int high_mode, int use_classifier, int pruning_mode);

    carmen_feet_observations
    lcm_detect_segments_from_planar_lidar_pruned(bot_core_planar_lidar_t *fl, laser_offset_t fl_offset, bot_core_pose_t fl_pose, 
                                                 bot_core_planar_lidar_t *rl, laser_offset_t rl_offset, bot_core_pose_t rl_pose, 
                                                 float max_seg_point_gap,
                                                 carmen_point_p points, int *no_points, 
                                                 carmen_point_p person_loc, int high_mode, int use_classifier, int pruning_mode);

    int 
    range_to_points(carmen_robot_laser_message *laser, carmen_point_p points, laser_offset_t laser_offset);

    int
    detect_circles(carmen_point_p segment_points, int count, carmen_circle* circle);

    /*carmen_person_observations 
      detect_segments(carmen_robot_laser_message *laser_msg, float max_seg_point_gap,float laser_offset);*/

    carmen_feet_observations 
    detect_segments(carmen_robot_laser_message *laser_msg, float max_seg_point_gap,laser_offset_t laser_offset, carmen_point_p points, int high_mode, int use_classifier);

    carmen_feet_observations
    detect_segments_both(carmen_robot_laser_message *fl, laser_offset_t fl_offset,
                         carmen_robot_laser_message *rl, laser_offset_t rl_offset,
                         float max_seg_point_gap,
                         carmen_point_p points, int *no_points, int high_mode, 
                         int use_classifier);

    carmen_feet_observations
    detect_segments_both_pruned(carmen_robot_laser_message *fl, laser_offset_t fl_offset,
                                carmen_robot_laser_message *rl, laser_offset_t rl_offset,
                                float max_seg_point_gap,
                                carmen_point_p points, int *no_points,
                                carmen_point_p person_loc, int high_mode, int use_classifier);


#ifdef __cplusplus
}
#endif

#endif
