#include <er_carmen/carmen.h>
#include "person_messages.h"
#include "person_interface.h"
#include <bot_core/bot_core.h>

#include <gsl/gsl_fit.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_blas.h>

#define MIN_FEET_POINTS 10
//#define USE_ADA //doesnt seem to work consistently 
//#ifdef _HIGH_TRACKER
#define H_GAP_MAX 0.8 //0.6 - for legs
#define H_GAP_MIN 0.2 //0.05
#define PRUNING_DIST_FROM_ROBOT 3.0
#define MOVING_DIST_FROM_ROBOT 5.0//3.0
#define PRUNING_DIST_FROM_GUIDE 1.0
//#endif

//#ifndef _HIGH_TRACKER
#define L_GAP_MAX 0.6 //- for legs
#define L_GAP_MIN 0.05
//#endif


static void get_statistics(double *x,double *y, int size, double *stddev, 
                           double *mean_dev_from_median, double *radius, 
                           double *residual_sum, double *boundry_length,
                           double *b_length_sdev, double *k_avg, double *avg_angle){
    double x_mean = 0;
    double y_mean = 0; 
    double x_sq_sum = 0;
    double y_sq_sum = 0;

    x_mean = gsl_stats_mean(x,1,size);
    y_mean = gsl_stats_mean(y,1,size);

    double x_var = gsl_stats_variance(x,1,size);
    double y_var = gsl_stats_variance(y,1,size);

    double gsl_var = hypot(x_var, y_var);

    *stddev = pow(gsl_var,0.5);

    //fprintf(stderr, "gsl_variance : %f\n", gsl_var);

    /*for(int i=0; i< size; i++){
      x_sq_sum += x[i]*x[i]; 
      y_sq_sum += y[i]*y[i]; 
    
      //x_mean += x[i];
      //y_mean += y[i];
      }

      //x_mean /=size;
      //y_mean /=size; 

      double x_comp = x_sq_sum / size - pow(x_mean,2);
      double y_comp = y_sq_sum / size - pow(y_mean,2);

      double var = size / (size-1) * (x_comp + y_comp); //this is correct  

      fprintf(stderr, "calculated_variance : %f\n", gsl_var);

      *stddev = pow(var, 0.5);*/ 
    double *x_sorted = calloc(size, sizeof(double));
    double *y_sorted = calloc(size, sizeof(double));

    memcpy(x_sorted, x, size* sizeof(double));
    memcpy(y_sorted, y, size* sizeof(double));

    gsl_sort (x_sorted, 1, size);
    gsl_sort (y_sorted, 1, size);
  
    double x_median = gsl_stats_median_from_sorted_data (x_sorted, 
                                                         1, size);

    double y_median = gsl_stats_median_from_sorted_data (y_sorted, 
                                                         1, size);

    free(x_sorted);
    free(y_sorted);

    *mean_dev_from_median = 0;
  
    for(int i=0; i< size; i++){
        *mean_dev_from_median += hypot(x[i] - x_median, y[i] - y_median);
    }
  
    *mean_dev_from_median /=size;

    //circulatrity 
  
    double *a_data = calloc(size * 3, sizeof(double)); 
    double *b_data = calloc(size, sizeof(double)); 

    /*double *a_mul_data = calloc(size*size, sizeof(double)); 
      memset(a_mul_data, 0, size*size*sizeof(double)); 
    */
    for(int i=0; i< size; i++){
        a_data[3*i] = -2 * x[i];
        a_data[3*i+1] = -2 * y[i];
        a_data[3*i+2] = 1;

        b_data[i] = -pow(x[i],2) -pow(y[i],2);
    }
  
    gsl_matrix_view a_mat = gsl_matrix_view_array (a_data, size, 3);
    gsl_matrix_view b_mat = gsl_matrix_view_array (b_data, size, 1);
    gsl_matrix *a_trans_mat = gsl_matrix_alloc(3,size);
    //gsl_matrix_view_array (a_mul_data, size, size);//gsl_matrix_alloc(3,size);//gsl_matrix_view_array (a_data, size, 3);

    gsl_matrix *a_result = gsl_matrix_alloc(3,3);
    gsl_matrix *a_inv_result = gsl_matrix_alloc(3,3);
    gsl_matrix *a_inv_trans_result = gsl_matrix_alloc(3,size);
  
    gsl_matrix *solution = gsl_matrix_alloc(3,1);
  
    gsl_matrix_transpose_memcpy(a_trans_mat, &a_mat.matrix); 

    //gsl_matrix_mul_elements(a_trans_mat,a_mat);  

    gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
                    1.0, a_trans_mat, &a_mat.matrix,//->matrix,
                    0.0, a_result);//->matrix);

    int s;
    gsl_permutation * p = gsl_permutation_alloc (3);

    gsl_linalg_LU_decomp (a_result, p, &s);
    gsl_linalg_LU_invert(a_result, p, a_inv_result);

    gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
                    1.0, a_inv_result, a_trans_mat,//->matrix,
                    0.0, a_inv_trans_result);

    gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
                    1.0, a_inv_trans_result, &b_mat.matrix,//->matrix,
                    0.0, solution);

    //gsl_matrix_fprintf(stdout, solution, "%g");

    double x_c = gsl_matrix_get(solution, 0,0);
    double y_c = gsl_matrix_get(solution, 1,0);
    double k = gsl_matrix_get(solution, 2,0);
    *radius = pow(pow(x_c,2) + pow(y_c,2) - k, 0.5); 

    *residual_sum = 0;
  
    for(int i=0; i< size; i++){
        *residual_sum += pow(*radius - pow(pow(x[i] - x_c,2) + pow(y[i] - y_c, 2),0.5),2); 
    }  

    //fprintf(stderr, " Radius : %f\n",   *radius);

    //gsl_linalg_cholesky_decomp(a_result); //doesnt work - needs to be positive semi-definite
    //gsl_linalg_cholesky_invert(a_result);

    //gsl_linalg_cholesky_invert(a_trans_mat)
  
    //gsl_matrix_view b = gsl_vector_view_array (b_data, size);

    //gsl_vector *x = gsl_vector_alloc (4);
    free(a_data);
    free(b_data);
    gsl_matrix_free (a_trans_mat);
    gsl_matrix_free (a_result);
    gsl_matrix_free (a_inv_result);
    gsl_matrix_free (a_inv_trans_result);
    gsl_matrix_free (solution);  

    *boundry_length = 0;

    double *b_lengths = calloc(size-1, sizeof(double)); 

    for(int i=1; i< size; i++){
        b_lengths[i-1] = hypot(x[i] - x[i-1], y[i] - y[i-1]);
        *boundry_length += b_lengths[i-1]; 
    }
  
    double b_length_var = gsl_stats_variance(b_lengths,1,size-1);
    *b_length_sdev = pow(b_length_var,0.5);

    free(b_lengths);

    *k_avg = 0;
    *avg_angle = 0;
    for(int i=2; i< size; i++){
        double xy1[2] = {x[i] - x[i-2], y[i] - y[i-2]};
        double xy2[2] = {x[i-1] - x[i-2], y[i-1] - y[i-2]};
        double xy3[2] = {x[i] - x[i-1], y[i] - y[i-1]};

        double dtheta = atan2(xy3[1], xy3[0]) - atan2(xy2[1], xy2[0]); 

        while(dtheta > M_PI){
            dtheta -=2*M_PI; 
        }

        while(dtheta < -M_PI){
            dtheta +=2*M_PI; 
        }

        *avg_angle +=dtheta; 

        double d1 = hypot(x[i]-x[i-1], y[i]-y[i-1]);
        double d2 = hypot(x[i]-x[i-2], y[i]-y[i-2]);
        double d3 = hypot(x[i-2]-x[i-1], y[i-2]-y[i-1]);

        double area = fabs(xy1[0]*xy2[1] - xy2[0] * xy1[1]) / 2; 
        *k_avg += 4 * area / (d1 * d2 * d3);
    }
    *avg_angle /=(size -2);
    *k_avg /=(size-2);

}

static int calculate_features(laser_segment *curr_seg){
    int size = curr_seg->no_points; 
    double width = hypot(curr_seg->points[0].pos[0]- curr_seg->points[size-1].pos[0], curr_seg->points[0].pos[1]- curr_seg->points[size-1].pos[1]);
    if(width > 1.5){//too large - we ignore these segments 
        //fprintf(stderr,"Ignoring - Too large\n");
        return -1;
    }
    if(size <= 2){
        //fprintf(stderr,"Ignoring : Too few points\n");
        return -1;
    }

    double *x = calloc(size, sizeof(double));
    double *y = calloc(size, sizeof(double));
    double c0, c1, cov00, cov01, cov11, sumsq; //c0, c1, 

    double xy[2] = {0,0};
  
    for(int i=0;i< size; i++){
        x[i] = curr_seg->points[i].pos[0];
        xy[0] += x[i];
        y[i] = curr_seg->points[i].pos[1];
        xy[1] += y[i];
    }

    curr_seg->xy[0] = xy[0]/ size;
    curr_seg->xy[1] = xy[1]/ size;
  
    gsl_fit_linear (x,1, y,1, size, &c0, &c1, &cov00, &cov01, &cov11, &sumsq);
    //fprintf(stderr,"C0 : %f C1: %f\n", c0, c1);
  
    //gsl_sort(xy, 2, size);

    double stddev = 0;
    double mean_dev_from_median = 0;
    double radius = 0;
    double circularity = 0;
    double boundry_length = 0;
    double b_length_sdev = 0;
    double k_avg = 0;
    double avg_angle = 0;
    get_statistics(x,y,size, &stddev, &mean_dev_from_median, &radius, &circularity, &boundry_length, &b_length_sdev, &k_avg, &avg_angle);

    curr_seg->sdev = stddev;
    curr_seg->mean_dev_from_median = mean_dev_from_median; 
    curr_seg->width = width; 
    curr_seg->linearity = sumsq;
    curr_seg->circulrity = circularity;
    curr_seg->radius = radius; 
    curr_seg->boundry_length = boundry_length;
    curr_seg->boundry_regularity = b_length_sdev;  
    curr_seg->mean_curvature = k_avg;
    curr_seg->mean_angular_difference = avg_angle;
    curr_seg->is_person = 0;

    free(x);
    free(y);  
    /*fprintf(stderr,"Size : %d Width : %f, Std Dev : %f Mean Dev from median : %f Sum Sq : %f\n",size, width, stddev, mean_dev_from_median, sumsq);
      fprintf(stderr,"Radius : %f, Circularity : %f Boundry Length : %f B length dev : %f k avg : %f k_avg_angle :%f\n", radius, circularity, boundry_length, b_length_sdev, k_avg, avg_angle);*/
}

static double get_score(double f, double threshold, int sign, double weight){
    if(f >= threshold * sign){
        return +weight;
    }
    else{
        return -weight; 
    }
}

static int test_features(laser_segment *curr_seg){
    double weight[4] = {1.9617, 1.2939, 1.3797, 0.8995};
    //coordinate 7 , 12, 3, 4
    int sign[4] = {1, 1, 1, 1};
    double thresholds[4] = {0.0126, 37.0364, 0.8852, 1.6167};

    double scores = 0;//[4] = {0,0,0,0};
    scores += get_score(curr_seg->linearity,thresholds[0], sign[0], weight[0]);
    scores += get_score(curr_seg->mean_curvature,thresholds[1], sign[1], weight[1]);
    scores += get_score(curr_seg->jump_dist_pre,thresholds[2], sign[2], weight[2]);
    scores += get_score(curr_seg->jump_dist_pre,thresholds[3], sign[3], weight[3]);
  
    //fprintf(stderr,"\tScore : %f\n", scores); 
  
    if(scores >=0){
        return 1;
    }
    else{
        return 0;
    }  
    /*lc->no_points, lc->width, lc->jump_dist_pre, 
      lc->jump_dist_suc, lc->sdev, lc->mean_dev_from_median, 
      lc->linearity, lc->circulrity, 
      lc->radius, lc->boundry_length, 
      lc->boundry_regularity, lc->mean_curvature,
      lc->mean_angular_difference*/
  
}


static int check_segment(laser_segment *curr_seg, carmen_feet_seg* circle, int start_segment, int end_segment, int front_laser){
    //print out the statistcs 
    /*for(int i=0;i< curr_seg->no_points; i++){
      fprintf(stderr,"\t%d : %f,%f\n", i, curr_seg->points[i].pos[0], curr_seg->points[i].pos[1]);
      }*/

    int to_add = calculate_features(curr_seg);
  
    if(to_add ==-1){    
        return 0;
    }

    //test features 
    int is_person = test_features(curr_seg);  
  
    if(is_person>0){
        fprintf(stderr,"Person found : %d\n", is_person);

        circle->center_x = curr_seg->xy[0];
        circle->center_y = curr_seg->xy[1];
        circle->start_segment = start_segment;
        circle->end_segment = end_segment;
        circle->front_laser = front_laser;
        //circle->radius = curr_seg->width;
    }
    return is_person; 
}

static double get_distance_points(carmen_point_t start, carmen_point_t end)
{
    return hypot((start.x-end.x),(start.y-end.y)); 
}

int push_obs(carmen_circle_set *list, carmen_circle new_element)//these list will not be used over the allocated limit
{
    list->elements[list->count] = new_element;
    list->count++;
    return 1;
}

void reject_outliers(carmen_robot_laser_message *laser){ //reject outlier points - i.e. max scans 
    double last_valid_range = 0.0;
    for(int i = 1; i < laser->num_readings; i++){
        if(laser->range[i] < 0.02){
            laser->range[i] = last_valid_range;
        }
        else
            last_valid_range = laser->range[i];
    }
    return;
}

carmen_circle remove_obs(carmen_circle_set *list, int index)//removes the item of this index and reallocates the rest
{
    int i;
    carmen_circle element;
    element = list->elements[index];
    list->count--;
    for (i=index;i<list->count;i++){
        list->elements[i] = list->elements[i+1];
    }
    return element;  
}

void swap(float *a, float *b)
{
    float t=*a; *a=*b; *b=t;
}
void sort(float arr[], int beg, int end)
{
    if (end > beg + 1)
        {
            float piv = arr[beg];
            int l = beg + 1, r = end;
            while (l < r)
                {
                    if (arr[l] <= piv)
                        l++;
                    else
                        swap(&arr[l], &arr[--r]);
                }
            swap(&arr[--l], &arr[beg]);
            sort(arr, beg, l);
            sort(arr, r, end);
        }
}

float get_median(float *ranges, int no_ranges, int center_ind, int width){
    int lower_ind = fmax(0, center_ind - width);
    int upper_ind = fmin(center_ind+width, no_ranges -1);
    int size = upper_ind-lower_ind+1;
    static float *vals = NULL;
    if(vals == NULL){
        vals = (float *)calloc(width*2 + 1, sizeof(float));
    }

    memcpy(vals, &ranges[lower_ind], sizeof(float)*size);
    sort(vals, 0, size -1);
    if(size % 2 ==1){
        int middle_ind = (int) floor(size/2.0);
        return vals[middle_ind];
    }
    else{
        //even 
        int middle_ind = size /2.0;
        float median = (vals[middle_ind-1] + vals[middle_ind])/2;
        return median;
    }  
}

//add support to the angle offset as well 

int 
range_to_points(carmen_robot_laser_message *laser, carmen_point_p points, laser_offset_t laser_offset)//float laser_offset)
{
    int count = 0;
    int i;

    static float *new_ranges = NULL;
    if(new_ranges ==NULL){
        new_ranges = (float *) calloc(laser->num_readings, sizeof(float));
    }
  
    for(i = 0; i < laser->num_readings; i++) {//prefilter points 
        float median = get_median(laser->range, laser->num_readings, i, 3);
        if(fabs(median - laser->range[i]) > 0.5){//2.0){
            new_ranges[i] = median;
        }    
        else{
            new_ranges[i] = laser->range[i];
        }
    }

    static float *sharp_ranges = NULL;
    if(sharp_ranges ==NULL){
        sharp_ranges = (float *) calloc(laser->num_readings, sizeof(float));
    }
    sharp_ranges[0] = new_ranges[0];
    sharp_ranges[laser->num_readings-1] = new_ranges[laser->num_readings-1];
    for(i = 1; i < laser->num_readings-1; i++) {
        sharp_ranges[i] = (-1*new_ranges[i-1] + 4 * new_ranges[i] - new_ranges[i+1])/2;

        //fprintf(stderr,"NR : %f , PR : %f, NeR : %f, Sharp : %f\n", new_ranges[i-1] , 
        //	    new_ranges[i], new_ranges[i+1], sharp_ranges[i]);
        if(sharp_ranges[i] - new_ranges[i] > 0.5){
            //fprintf(stderr,"+");
            //fprintf(stderr,"R : %f NR : %f , PR : %f, NeR : %f, Sharp : %f\n", laser->range[i], 
            //	      new_ranges[i-1] , new_ranges[i], new_ranges[i+1], sharp_ranges[i]);
        }
    } 

 
    //sharpen 
    //memcpy(laser->range, new_ranges, laser->num_readings*sizeof(float));
    //memcpy(laser->range, sharp_ranges, laser->num_readings*sizeof(float));
 
    /*for(i = 0; i < laser->num_readings; i++) {
      double theta = laser->config.start_angle +
      (double)i * laser->config.angular_resolution ;

      points[i].x = laser->range[i]*cos(theta)+laser_offset; // + (laser->laser_pose.x - laser->robot_pose.x) ;
      points[i].y = laser->range[i]*sin(theta); // + (laser->laser_pose.y - laser->robot_pose.y) ;
      points[i].theta = theta;    
      count++;
      }*/

    /*for(i = 0; i < laser->num_readings; i++) {
      double theta = laser->config.start_angle +
      (double)i * laser->config.angular_resolution ;

      points[i].x = sharp_ranges[i]*cos(theta)+laser_offset; // + (laser->laser_pose.x - laser->robot_pose.x) ;
      points[i].y = sharp_ranges[i]*sin(theta); // + (laser->laser_pose.y - laser->robot_pose.y) ;
      points[i].theta = theta;    
      count++;
      }*/

    for(i = 0; i < laser->num_readings; i++) {
        //does this start angle get set somewhere such as in the robot module??
    
        double theta = laser->config.start_angle +
            (double)i * laser->config.angular_resolution ;

        points[i].x = sharp_ranges[i]*cos(theta)+ laser_offset.dx; // + (laser->laser_pose.x - laser->robot_pose.x) ;
        points[i].y = sharp_ranges[i]*sin(theta)+ laser_offset.dy; // + (laser->laser_pose.y - laser->robot_pose.y) ;
        points[i].theta = theta;    
        count++;
    }
  

  
  
    return count;
}

int 
lcm_range_to_points(bot_core_planar_lidar_t *laser, carmen_point_p points, laser_offset_t laser_offset, int sharpen)//float laser_offset)
{
    int count = 0;
    int i;
    

    if(sharpen){
    
        static float *new_ranges = NULL;
        if(new_ranges ==NULL){
            new_ranges = (float *) calloc(laser->nranges, sizeof(float));
        }
    
        for(i = 0; i < laser->nranges; i++) {//prefilter points 
            float median = get_median(laser->ranges, laser->nranges, i, 3);
            if(fabs(median - laser->ranges[i]) > 0.5){//2.0){
                new_ranges[i] = median;
            }    
            else{
                new_ranges[i] = laser->ranges[i];
            }
        }

        static float *sharp_ranges = NULL;
        if(sharp_ranges ==NULL){
            sharp_ranges = (float *) calloc(laser->nranges, sizeof(float));
        }
        sharp_ranges[0] = new_ranges[0];
        sharp_ranges[laser->nranges-1] = new_ranges[laser->nranges-1];
        for(i = 1; i < laser->nranges-1; i++) {
            sharp_ranges[i] = (-1*new_ranges[i-1] + 4 * new_ranges[i] - new_ranges[i+1])/2;

            //fprintf(stderr,"NR : %f , PR : %f, NeR : %f, Sharp : %f\n", new_ranges[i-1] , 
            //	    new_ranges[i], new_ranges[i+1], sharp_ranges[i]);
            if(sharp_ranges[i] - new_ranges[i] > 0.5){
                //fprintf(stderr,"+");
                //fprintf(stderr,"R : %f NR : %f , PR : %f, NeR : %f, Sharp : %f\n", laser->range[i], 
                //	      new_ranges[i-1] , new_ranges[i], new_ranges[i+1], sharp_ranges[i]);
            }
        } 

        for(i = 0; i < laser->nranges; i++) {
            double theta = laser->rad0 + laser_offset.dtheta + 
                (double)i * laser->radstep;

            points[i].x = sharp_ranges[i]*cos(theta)+ laser_offset.dx; // + (laser->laser_pose.x - laser->robot_pose.x) ;
            points[i].y = sharp_ranges[i]*sin(theta)+ laser_offset.dy; // + (laser->laser_pose.y - laser->robot_pose.y) ;
            points[i].theta = theta;    
            count++;
        } 
    }
    else{
        for(i = 0; i < laser->nranges; i++) {
            double theta = laser->rad0 + laser_offset.dtheta + 
                (double)i * laser->radstep;

            points[i].x = laser->ranges[i]*cos(theta)+ laser_offset.dx; // + (laser->laser_pose.x - laser->robot_pose.x) ;
            points[i].y = laser->ranges[i]*sin(theta)+ laser_offset.dy; // + (laser->laser_pose.y - laser->robot_pose.y) ;
            points[i].theta = theta;    
            count++;
        } 
    }  
    return count;
}


int 
range_to_points_cluster(carmen_robot_laser_message *laser, carmen_point_p points, laser_offset_t laser_offset)
{
    int count = 0;
    int i;

    static float *new_ranges = NULL;
    if(new_ranges ==NULL){
        new_ranges = (float *) calloc(laser->num_readings, sizeof(float));
    }
  
    for(i = 0; i < laser->num_readings; i++) {//prefilter points 
        float median = get_median(laser->range, laser->num_readings, i, 3);
        if(fabs(median - laser->range[i]) > 0.5){//2.0){
            new_ranges[i] = median;
        }    
        else{
            new_ranges[i] = laser->range[i];
        }
    }

    static float *sharp_ranges = NULL;
    if(sharp_ranges ==NULL){
        sharp_ranges = (float *) calloc(laser->num_readings, sizeof(float));
    }
    sharp_ranges[0] = new_ranges[0];
    sharp_ranges[laser->num_readings-1] = new_ranges[laser->num_readings-1];
    for(i = 1; i < laser->num_readings-1; i++) {
        sharp_ranges[i] = (-1*new_ranges[i-1] + 4 * new_ranges[i] - new_ranges[i+1])/2;

        //fprintf(stderr,"NR : %f , PR : %f, NeR : %f, Sharp : %f\n", new_ranges[i-1] , 
        //	    new_ranges[i], new_ranges[i+1], sharp_ranges[i]);
        if(sharp_ranges[i] - new_ranges[i] > 0.5){
            //fprintf(stderr,"+");
            //fprintf(stderr,"R : %f NR : %f , PR : %f, NeR : %f, Sharp : %f\n", laser->range[i], 
            //	      new_ranges[i-1] , new_ranges[i], new_ranges[i+1], sharp_ranges[i]);
        }
    } 

 
    //sharpen 
    //memcpy(laser->range, new_ranges, laser->num_readings*sizeof(float));
    //memcpy(laser->range, sharp_ranges, laser->num_readings*sizeof(float));
 
    /*for(i = 0; i < laser->num_readings; i++) {
      double theta = laser->config.start_angle +
      (double)i * laser->config.angular_resolution ;

      points[i].x = laser->range[i]*cos(theta)+laser_offset; // + (laser->laser_pose.x - laser->robot_pose.x) ;
      points[i].y = laser->range[i]*sin(theta); // + (laser->laser_pose.y - laser->robot_pose.y) ;
      points[i].theta = theta;    
      count++;
      }*/

    /*for(i = 0; i < laser->num_readings; i++) {
      double theta = laser->config.start_angle +
      (double)i * laser->config.angular_resolution ;

      points[i].x = sharp_ranges[i]*cos(theta)+laser_offset; // + (laser->laser_pose.x - laser->robot_pose.x) ;
      points[i].y = sharp_ranges[i]*sin(theta); // + (laser->laser_pose.y - laser->robot_pose.y) ;
      points[i].theta = theta;    
      count++;
      }*/

    //ANNpointArray dataPts = NULL;
    //dataPts = annAllocPts(no_points, dim);
    //clustered_pts data_pts;  //this would need to be a cpp prog to do this 
  
    for(i = 0; i < laser->num_readings; i++) {
        //does this start angle get set somewhere such as in the robot module??
    
        double theta = laser->config.start_angle +
            (double)i * laser->config.angular_resolution ;

        points[i].x = sharp_ranges[i]*cos(theta)+ laser_offset.dx; // + (laser->laser_pose.x - laser->robot_pose.x) ;
        points[i].y = sharp_ranges[i]*sin(theta)+ laser_offset.dy; // + (laser->laser_pose.y - laser->robot_pose.y) ;
        points[i].theta = theta;    
        count++;
    }
  

  
  
    return count;
}



int 
range_to_points_basic(carmen_robot_laser_message *laser, carmen_point_p points, float laser_offset)
{
    int count = 0;
    //reject_outliers(laser);
    int i;
    for(i = 0; i < laser->num_readings; i++) {
        /*if (laser->range[i] < 0.02){
          continue;
          }*/
    
        double theta = laser->config.start_angle +
            (double)i * laser->config.angular_resolution ;

        points[i].x = laser->range[i]*cos(theta)+laser_offset; // + (laser->laser_pose.x - laser->robot_pose.x) ;
        points[i].y = laser->range[i]*sin(theta); // + (laser->laser_pose.y - laser->robot_pose.y) ;
        points[i].theta = theta;    
        count++;
    }
    return count;
}

double get_mean_dist(carmen_point_p segment_points, int count) 
//gives the mean distance for the segment from the robot
{
    int i;
    double mean_dist = 0.0;

    for (i=0;i<count;i++){
        mean_dist+= hypot(segment_points[i].x,segment_points[i].y);
    }
    mean_dist = mean_dist/count;

    return mean_dist;
}

carmen_point_t get_mean_point(carmen_point_p segment_points, int count)
//gives the mean point for the segment
{
    int i;  
    carmen_point_t mean_point;
    mean_point.x = 0.0;
    mean_point.y = 0.0;
    mean_point.theta = 0.0;

    for (i=0;i<count;i++){
        mean_point.x += segment_points[i].x; 
        mean_point.y += segment_points[i].y; 
    }
  
    mean_point.x = mean_point.x/count;
    mean_point.y = mean_point.y/count;
  
    return mean_point;
}

carmen_circle merge_circles(carmen_circle point1, carmen_circle point2){
  
    carmen_circle new_point;
    new_point.center_x = (point1.center_x + point2.center_x)/2;
    new_point.center_y = (point1.center_y + point2.center_y)/2;
    new_point.radius = point1.radius + point2.radius;
    return new_point;
}

int merge_legs(carmen_person_observations *person_obs)
//takes in all the observations and prunes and combines the leg detections
//combines if only two observations are detected within 0.4m from eachother
//removes if more than two observations exist within 0.4m
{
    //fprintf(stderr,"====== Detecting and Pruning leg observations ===== \n");
    carmen_circle* locations = person_obs->locations;
    int no_obs = person_obs->no_obs;
    if(no_obs<=1){
        return 1;
    }
    //more than one observation - need to merge
    else{
        carmen_circle* temp_cluster;
        carmen_circle* unassigned_observations;
        carmen_circle* final_observations;

        float max_seperation = 0.8; //max possible distance between two observations - to be considered fro merging
        float combine_seperation = 0.5;

        //all observations that are close are added to the temp_cluster
        temp_cluster = (carmen_circle *) malloc(no_obs*sizeof(carmen_circle));  
    
        //merged observations are added to this final list
        //final_observations = (carmen_circle *) malloc(no_obs*sizeof(carmen_circle));
        final_observations = person_obs->locations;
    
        //observations that havent yet been moved to temp or merged list 
        unassigned_observations = (carmen_circle *) malloc(no_obs*sizeof(carmen_circle));

        carmen_test_alloc(temp_cluster);
        //carmen_test_alloc(final_observations);
        carmen_test_alloc(unassigned_observations);

        carmen_circle_set temp_list;
        carmen_circle_set unassigned_list;
        carmen_circle_set final_list;
    
        temp_list.count = 0;
        temp_list.elements = temp_cluster;
        unassigned_list.count = no_obs;
        unassigned_list.elements = locations;
        final_list.count =0;
        final_list.elements = final_observations;
    
        int i,j;
        float distance;

        for(j=0;j<unassigned_list.count;j++){ 
            //remove large observations to the final list - as they are unlikely to come from one leg
            if(unassigned_list.elements[j].radius > 0.4){
                //fprintf(stderr,"Found a possible combined leg\n");
                push_obs(&final_list,remove_obs(&unassigned_list,0));
            }
        }

        while(unassigned_list.count !=0){
            push_obs(&temp_list,remove_obs(&unassigned_list,0)); //move the first one to the temp list
      
            for(i=0;i<temp_list.count;i++){
                //compare everything in the temp list with everything in the unassigned list
                //move things to the temp_list if found to be close to something in the temp
	
                for(j=0;j<unassigned_list.count;j++){
                    //compare the points in temp with points in unassigned and move them to the temp if close enough
                    distance = hypot(temp_list.elements[i].center_x-unassigned_list.elements[j].center_x,
                                     temp_list.elements[i].center_y-unassigned_list.elements[j].center_y);
                    if(distance<max_seperation){//if point was within the given margin add to the temp cluster
                        push_obs(&temp_list,remove_obs(&unassigned_list,j));
                        //added_count++;
                        j--;//since the next point in the unassgined has moved to the current point move the count back by one
                    }
                }
            }
            if(temp_list.count==1){//move this point to the final list
                //fprintf(stderr,"Found a possible leg\n");
                push_obs(&final_list,remove_obs(&temp_list,0)); //*******or should we discard this????? 
                //since it cant be a two leg segment
            }

            else if(temp_list.count==2){//leg detection - combine the observation into one 
                distance = hypot(temp_list.elements[0].center_x-temp_list.elements[1].center_x, 
                                 temp_list.elements[0].center_y-temp_list.elements[1].center_y);
                if(distance < combine_seperation){//if they are withing combination range combine
                    //fprintf(stderr,"Found Two possible legs\n");	
                    carmen_circle new_point = merge_circles(remove_obs(&temp_list,0),remove_obs(&temp_list,0));
                    push_obs(&final_list,new_point);
                }
                else{//otherwise leave as is
                    push_obs(&final_list,remove_obs(&temp_list,0));
                    push_obs(&final_list,remove_obs(&temp_list,0));
                }

            }
            else if(temp_list.count>2){//add everything to the observation
                int no_items = temp_list.count;
                for(i=0;i<no_items;i++){
                    push_obs(&final_list,remove_obs(&temp_list,0));
                } 
            }

            /*else if(temp_list.count>2){ //combine as much as possible
              fprintf(stderr,"Found Multiles\n");
              double **dist_array = (double **) malloc(temp_list.count*sizeof(double *));
              for(i=0;i<temp_list.count;i++){
              dist_array[i] = (double *) malloc(temp_list.count*sizeof(double));
              } 

              for(i=0;i<temp_list.count;i++){
              for(j=0;j<temp_list.count;j++){
              if (i==j){
              dist_array[i][j] = 100.0;
              }
              else{
              dist_array[i][j] = hypot(temp_list.elements[i].center_x-temp_list.elements[j].center_x,				       
              temp_list.elements[i].center_y-temp_list.elements[j].center_y);
              }
              }
              }
              //do till one or no indexes remain 

              //find the min distance betweenn any two remaining points - merge and remove from unassigned index list
              int assigned_count = 0;
              int *assigned_inds = (int *) malloc(temp_list.count*sizeof(int));

              do//while(assigned_count>= temp_list.count-1);
              {	  
              double min_dist = 1000;
              int min_ind_x = -1;
              int min_ind_y = -1;
	  
              int k;

              for(i=0;i<temp_list.count;i++){
              //if i is already assigned skip 
              int i_assigned = 0;
              for(k=0;k< assigned_count;k++){
              if(i==assigned_inds[k]){
              i_assigned  =1;
              }
              }
              if(i_assigned ==1){
              continue;
              }
              //otherwise
              else{
              for(j=0;j<temp_list.count;j++){
              //if j is already assigned skip 
              int j_assigned = 0;
              for(k=0;k< assigned_count;k++){
              if(j==assigned_inds[k]){
              j_assigned = 1;
              }
              }
              if(j_assigned ==1){
              continue;
              }
              //otherwise check for min dist
              else if(dist_array[i][j]< min_dist){
              min_dist = dist_array[i][j];
              min_ind_x = i;
              min_ind_y = j;
              }
              }
              }
              }
              if(min_ind_x>=0 && min_ind_y >=0){
              carmen_circle new_point = merge_circles(temp_list.elements[min_ind_x],temp_list.elements[min_ind_y]);
              assigned_inds[assigned_count] = min_ind_x;
              assigned_inds[assigned_count+1] = min_ind_y;
              assigned_count +=2;
              //push_obs(&final_list,new_point);
              }
              }while(assigned_count< temp_list.count-1);

              temp_list.count=0;
              }*/
            else{
                temp_list.count=0;
            }
        }
        person_obs->locations = final_list.elements;
        person_obs->no_obs = final_list.count;   

        free(temp_cluster);
        //free(final_observations);
        free(unassigned_observations);

    }
    return 1;
}

carmen_person_observations
prune_observations(carmen_person_observations* person_obs)  //removes the observations that are nearby ones seen earlier
{
    static carmen_person_observations prev_person_obs;

    carmen_person_observations pruned_obs;

    int first_person_obs = 0;

    if (first_person_obs == 0){
        first_person_obs =1;
        prev_person_obs.no_obs = person_obs->no_obs;
        prev_person_obs.robot_pose = person_obs->robot_pose;
        prev_person_obs.locations = (carmen_circle *) malloc(person_obs->no_obs*sizeof(carmen_circle));
        memcpy(prev_person_obs.locations, person_obs->locations,
               person_obs->no_obs*sizeof(carmen_circle));    
        //prev_person_obs.locations = person_obs->locations;
        pruned_obs = *person_obs;
    }
  
    else{    
        pruned_obs = prune_segments(person_obs,&prev_person_obs);

        prev_person_obs.no_obs = person_obs->no_obs;
        prev_person_obs.robot_pose = person_obs->robot_pose;
        prev_person_obs.locations = (carmen_circle *) malloc(person_obs->no_obs*sizeof(carmen_circle));
        memcpy(prev_person_obs.locations, person_obs->locations,
               person_obs->no_obs*sizeof(carmen_circle));
        //prev_person_obs.locations = person_obs->locations;
    }
  
    return pruned_obs;
}

carmen_person_observations
prune_segments(carmen_person_observations* person_obs, carmen_person_observations* prev_person_obs)
{
    carmen_person_observations pruned_obs;
  
    carmen_circle* prev_list = prev_person_obs->locations;
  
    carmen_circle *pruned_list = (carmen_circle *) malloc(person_obs->no_obs*sizeof(carmen_circle));
    carmen_test_alloc(pruned_list);
  
    int prune_count = 0;
    //fprintf(stderr,"Prune Segments \n");

    if (prev_person_obs->no_obs>0){//if there was any observations in the last scan 
        //convert to the new co-ordinate frame
    
        double dx = person_obs->robot_pose.x - prev_person_obs->robot_pose.x;
        double dy = person_obs->robot_pose.y - prev_person_obs->robot_pose.y;
        double dtheta = person_obs->robot_pose.theta - prev_person_obs->robot_pose.theta;
    
        double theta_0 = prev_person_obs->robot_pose.theta;
    
        double h = dx*cos(theta_0) + dy* sin(theta_0);
        double k = -dx*sin(theta_0) + dy* cos(theta_0);
    
        int i;
        for (i =0; i < prev_person_obs->no_obs; i++){ //convert all the previous 
            //observations to current robot coordinate frame
            double x0 = prev_list[i].center_x;
            double y0 = prev_list[i].center_y;
            double x1 = x0 * cos(dtheta) + y0* sin(dtheta) -h;
            double y1 = -x0* sin(dtheta) + y0 * cos(dtheta) - k;
            prev_list[i].center_x = x1;
            prev_list[i].center_y = y1;
        }
    }
  
    int meta_count = person_obs->no_obs;
    carmen_circle* circle_list = person_obs->locations;
  
    if (meta_count>0){
        int i;
        for (i =0; i < meta_count; i++){
            double x0 = circle_list[i].center_x;
            double y0 = circle_list[i].center_y;
      
            int j;
            //search the previous points
            double min_distance = 1000.0;
      
            for (j =0; j < prev_person_obs->no_obs; j++){
                double x2 = prev_list[j].center_x;
                double y2 = prev_list[j].center_y;
                double temp_dist = hypot((x0-x2),(y0-y2));
                if(temp_dist<min_distance){
                    min_distance = temp_dist;
                }		  
            }
            if(min_distance > 0.05){ //if there was no previous observations that are clsoe by
                pruned_list[prune_count] = circle_list[i];
                prune_count++;
            }
        }

        //fprintf(stderr," No of items %d Old No : %d ", prune_count, person_obs->no_obs);
        pruned_obs.no_obs = prune_count;
        pruned_obs.locations = pruned_list;
        pruned_obs.robot_pose = person_obs->robot_pose;
    }
    else{
        pruned_obs.no_obs = person_obs->no_obs;
        pruned_obs.locations = person_obs->locations;
        pruned_obs.robot_pose = person_obs->robot_pose;
    }

    free(prev_person_obs->locations);

    return pruned_obs;
}


//carmen_feet_observations
//carmen_feet_observations *
int 
get_moving_feet_segments(carmen_feet_observations* new_obs, carmen_feet_observations *moved_obs, carmen_point_p points, int no_points, check_gridmap_t *gm)
{
    static carmen_feet_observations prev_obs;
    static int first_obs = 0;
    //carmen_feet_observations moved_obs;
    memset(moved_obs, 0, sizeof(carmen_feet_observations));

    if(first_obs ==0){
        first_obs =1;
        prev_obs.locations  = (carmen_feet_seg *) malloc(new_obs->no_obs *sizeof(carmen_feet_seg));
        prev_obs.no_obs = new_obs->no_obs;
        memcpy(prev_obs.locations, new_obs->locations, new_obs->no_obs *sizeof(carmen_feet_seg));
        memcpy(&prev_obs.robot_pose, &new_obs->robot_pose, sizeof(carmen_point_t));
        fprintf(stderr,"===== First feet observation\n ====");
        return 0;//moved_obs;
    }
    else{
        //add the code here

        // *****************************************
    
        int prune_count = 0;
        //fprintf(stderr,"Prune Segments \n");

        double dx = new_obs->robot_pose.x - prev_obs.robot_pose.x;
        double dy = new_obs->robot_pose.y - prev_obs.robot_pose.y;
        double dtheta = new_obs->robot_pose.theta - prev_obs.robot_pose.theta;
        
        double theta_0 = prev_obs.robot_pose.theta;
        
        double h = dx*cos(theta_0) + dy* sin(theta_0);
        double k = -dx*sin(theta_0) + dy* cos(theta_0);        
      
        if (prev_obs.no_obs>0){//if there was any observations in the last scan 
            //convert to the new co-ordinate frame
            
            for (int i =0; i < prev_obs.no_obs; i++){ //convert all the previous 
                //observations to current robot coordinate frame
                double x0 = prev_obs.locations[i].center_x;
                double y0 = prev_obs.locations[i].center_y;

                double x1 = x0 * cos(dtheta) + y0* sin(dtheta) -h;
                double y1 = -x0* sin(dtheta) + y0 * cos(dtheta) - k;
                prev_obs.locations[i].center_x = x1;
                prev_obs.locations[i].center_y = y1;
            }
        }
  
        int obs_count = new_obs->no_obs;
        carmen_feet_seg* new_legs = new_obs->locations;
  
        if (obs_count>0){
            //allocate space for the moved legs
      
            carmen_feet_seg *prev_list = prev_obs.locations;
            moved_obs->locations  = (carmen_feet_seg *) malloc(new_obs->no_obs *sizeof(carmen_feet_seg));
            memcpy(&moved_obs->robot_pose, &new_obs->robot_pose, sizeof(carmen_point_t));
      
            for (int i =0; i < obs_count; i++){
                double x0 = new_legs[i].center_x;
                double y0 = new_legs[i].center_y;

                double gx = new_obs->robot_pose.x + x0 * cos(new_obs->robot_pose.theta) - y0* sin(new_obs->robot_pose.theta);
                double gy = new_obs->robot_pose.y + x0 * sin(new_obs->robot_pose.theta) + y0 *cos(new_obs->robot_pose.theta);
                
                int occupied = 0;
                
                if(gm){
                //this needs to be convered to robot frame                  
                   occupied = check_gridmap_check_point(gm, gx, gy);
                }
                //search the previous points
                double min_distance = 1000.0;
      
                for (int j =0; j < prev_obs.no_obs; j++){
                    double x2 = prev_list[j].center_x;
                    double y2 = prev_list[j].center_y;
                    double temp_dist = hypot((x0-x2),(y0-y2));
                    if(temp_dist<min_distance){
                        min_distance = temp_dist;
                    }		  
                }

                //fprintf(stderr, "\tPossible moved Obs : %d => %f\n", i, min_distance);
                
                //*************Sachi : Not sure if this is correct 
                if((min_distance > 0.1) && (min_distance < 0.3) && !occupied){ //if there was no previous observations that are close by	  
                    //fprintf(stderr, "\tChecking Possible moved Obs : %d => %f", i, min_distance);
                    //search to see if there were points here
                    double min_distance_static = 1000.0;

                    //reproject the current points to the old frame 
                    double c_x = h + x0 * cos(dtheta) - y0 * sin(dtheta); 
                    double c_y = k + x0 * sin(dtheta) + y0 * cos(dtheta); 

                    for(int k=0; k<no_points; k++){
                        double temp_dist = hypot((c_x - points[k].x),(c_y -points[k].y));
                        if(temp_dist < min_distance){
                            min_distance_static = temp_dist;
                        }
                    }

                    //fprintf(stderr, "\tMin Dist static : %f\n", min_distance_static);

                    //**********This might also need to be adjusted to check people moving near the guide 
                    if((min_distance > 0.1 && min_distance_static > 0.1)){// && (hypot(x0,y0) < MOVING_DIST_FROM_ROBOT)){
                        memcpy(&moved_obs->locations[moved_obs->no_obs],&new_legs[i],sizeof(carmen_feet_seg));	    
                        moved_obs->no_obs++;
                        //fprintf(stderr, "\tMoved Dist : %f Dist from Robot : %f\n", min_distance, hypot(x0,y0));
                    }	
                }
                
                else{
                    //fprintf(stderr, "\n");
                }
                /*if((min_distance > 0.1) && (min_distance < 0.4)){
                  memcpy(&moved_obs->locations[moved_obs->no_obs],&new_legs[i],sizeof(carmen_feet_seg));	    
                  fprintf(stderr, "Adding to moved\n");
                  moved_obs->no_obs++;

                  }*/	
            }

            if(new_obs->no_obs != moved_obs->no_obs){
                moved_obs->locations  = (carmen_feet_seg *) realloc(moved_obs->locations, moved_obs->no_obs *sizeof(carmen_feet_seg));
            }
        }
        
    
        /*fprintf(stderr,"Prev Obs \n");
          for (int i=0; i < prev_obs.no_obs; i++){
          fprintf(stderr,"\t %d (%f,%f)\n", i, prev_obs.locations[i].center_x, prev_obs.locations[i].center_y);
          }
    
          fprintf(stderr,"New Obs \n");
          for (int i=0; i < new_obs->no_obs; i++){
          fprintf(stderr,"\t %d (%f,%f)\n", i, new_obs->locations[i].center_x, new_obs->locations[i].center_y);
          }

          fprintf(stderr,"Moved Obs \n");
          for (int i=0; i < moved_obs->no_obs; i++){
          fprintf(stderr,"\t %d (%f,%f)\n", i, moved_obs->locations[i].center_x, moved_obs->locations[i].center_y);
          } 
          fprintf(stderr,"Updating the previous observation\n");*/
        if(new_obs->no_obs != prev_obs.no_obs){
            prev_obs.locations  = (carmen_feet_seg *) realloc(prev_obs.locations, new_obs->no_obs *sizeof(carmen_feet_seg));
        }
        prev_obs.no_obs = new_obs->no_obs;
        memcpy(prev_obs.locations, new_obs->locations, new_obs->no_obs *sizeof(carmen_feet_seg));
        memcpy(&prev_obs.robot_pose, &new_obs->robot_pose, sizeof(carmen_point_t));    
    }
    return 0;
    //return &moved_obs; //should not return NULL
}


//carmen_feet_observations
//carmen_feet_observations *
int 
get_small_moving_feet_segments(carmen_feet_observations* new_obs, carmen_feet_observations *moved_obs, carmen_point_p points, int no_points, check_gridmap_t *gm)
{
    static carmen_feet_observations prev_obs;
    static int first_obs = 0;
    //carmen_feet_observations moved_obs;
    memset(moved_obs, 0, sizeof(carmen_feet_observations));

    if(first_obs ==0){
        first_obs =1;
        prev_obs.locations  = (carmen_feet_seg *) malloc(new_obs->no_obs *sizeof(carmen_feet_seg));
        prev_obs.no_obs = new_obs->no_obs;
        memcpy(prev_obs.locations, new_obs->locations, new_obs->no_obs *sizeof(carmen_feet_seg));
        memcpy(&prev_obs.robot_pose, &new_obs->robot_pose, sizeof(carmen_point_t));
        fprintf(stderr,"===== First feet observation\n ====");
        return 0;//moved_obs;
    }
    else{
        int prune_count = 0;
        //fprintf(stderr,"Prune Segments \n");

        double dx = new_obs->robot_pose.x - prev_obs.robot_pose.x;
        double dy = new_obs->robot_pose.y - prev_obs.robot_pose.y;
        double dtheta = new_obs->robot_pose.theta - prev_obs.robot_pose.theta;
        
        double theta_0 = prev_obs.robot_pose.theta;
        
        double h = dx*cos(theta_0) + dy* sin(theta_0);
        double k = -dx*sin(theta_0) + dy* cos(theta_0);        
      
        if (prev_obs.no_obs>0){//if there was any observations in the last scan 
            //convert to the new co-ordinate frame
	
            
            for (int i =0; i < prev_obs.no_obs; i++){ //convert all the previous 
                //observations to current robot coordinate frame
                double x0 = prev_obs.locations[i].center_x;
                double y0 = prev_obs.locations[i].center_y;

                double x1 = x0 * cos(dtheta) + y0* sin(dtheta) -h;
                double y1 = -x0* sin(dtheta) + y0 * cos(dtheta) - k;
                prev_obs.locations[i].center_x = x1;
                prev_obs.locations[i].center_y = y1;
            }
        }
  
        int obs_count = new_obs->no_obs;
        carmen_feet_seg* new_legs = new_obs->locations;
  
        if (obs_count>0){
            //allocate space for the moved legs
      
            carmen_feet_seg *prev_list = prev_obs.locations;
            moved_obs->locations  = (carmen_feet_seg *) malloc(new_obs->no_obs *sizeof(carmen_feet_seg));
            memcpy(&moved_obs->robot_pose, &new_obs->robot_pose, sizeof(carmen_point_t));
      
            for (int i =0; i < obs_count; i++){
                double x0 = new_legs[i].center_x;
                double y0 = new_legs[i].center_y;

                double gx = new_obs->robot_pose.x + x0 * cos(new_obs->robot_pose.theta) - y0* sin(new_obs->robot_pose.theta);
                double gy = new_obs->robot_pose.y + x0 * sin(new_obs->robot_pose.theta) + y0 *cos(new_obs->robot_pose.theta);

                int occupied = 0;
                if(gm){
                  //this needs to be convered to robot frame 
                   occupied = check_gridmap_check_point(gm, gx, gy);
                }
                //search the previous points
                double min_distance = 1000.0;
      
                for (int j =0; j < prev_obs.no_obs; j++){
                    double x2 = prev_list[j].center_x;
                    double y2 = prev_list[j].center_y;
                    double temp_dist = hypot((x0-x2),(y0-y2));
                    if(temp_dist<min_distance){
                        min_distance = temp_dist;
                    }		  
                }

                //fprintf(stderr, "\tPossible moved Obs : %d => %f\n", i, min_distance);
                
                //*************Sachi : Not sure if this is correct 
                if((min_distance > 0.05) && (min_distance < 0.3) && !occupied){ //if there was no previous observations that are close by	  
                    //fprintf(stderr, "\tChecking Possible moved Obs : %d => %f", i, min_distance);
                    //search to see if there were points here
                    double min_distance_static = 1000.0;

                    //reproject the current points to the old frame 
                    double c_x = h + x0 * cos(dtheta) - y0 * sin(dtheta); 
                    double c_y = k + x0 * sin(dtheta) + y0 * cos(dtheta); 

                    for(int k=0; k<no_points; k++){
                        double temp_dist = hypot((c_x - points[k].x),(c_y -points[k].y));
                        if(temp_dist < min_distance){
                            min_distance_static = temp_dist;
                        }
                    }

                    //fprintf(stderr, "\tMin Dist static : %f\n", min_distance_static);

                    //**********This might also need to be adjusted to check people moving near the guide 
                    if((min_distance > 0.05 && min_distance_static > 0.05)){// && (hypot(x0,y0) < MOVING_DIST_FROM_ROBOT)){
                        memcpy(&moved_obs->locations[moved_obs->no_obs],&new_legs[i],sizeof(carmen_feet_seg));	    
                        moved_obs->no_obs++;
                        //fprintf(stderr, "\tMoved Dist : %f Dist from Robot : %f\n", min_distance, hypot(x0,y0));
                    }	
                }
                
                else{
                    //fprintf(stderr, "\n");
                }
                /*if((min_distance > 0.1) && (min_distance < 0.4)){
                  memcpy(&moved_obs->locations[moved_obs->no_obs],&new_legs[i],sizeof(carmen_feet_seg));	    
                  fprintf(stderr, "Adding to moved\n");
                  moved_obs->no_obs++;

                  }*/	
            }

            if(new_obs->no_obs != moved_obs->no_obs){
                moved_obs->locations  = (carmen_feet_seg *) realloc(moved_obs->locations, moved_obs->no_obs *sizeof(carmen_feet_seg));
            }
        }
        
    
        /*fprintf(stderr,"Prev Obs \n");
          for (int i=0; i < prev_obs.no_obs; i++){
          fprintf(stderr,"\t %d (%f,%f)\n", i, prev_obs.locations[i].center_x, prev_obs.locations[i].center_y);
          }
    
          fprintf(stderr,"New Obs \n");
          for (int i=0; i < new_obs->no_obs; i++){
          fprintf(stderr,"\t %d (%f,%f)\n", i, new_obs->locations[i].center_x, new_obs->locations[i].center_y);
          }

          fprintf(stderr,"Moved Obs \n");
          for (int i=0; i < moved_obs->no_obs; i++){
          fprintf(stderr,"\t %d (%f,%f)\n", i, moved_obs->locations[i].center_x, moved_obs->locations[i].center_y);
          } 
          fprintf(stderr,"Updating the previous observation\n");*/
        if(new_obs->no_obs != prev_obs.no_obs){
            prev_obs.locations  = (carmen_feet_seg *) realloc(prev_obs.locations, new_obs->no_obs *sizeof(carmen_feet_seg));
        }
        prev_obs.no_obs = new_obs->no_obs;
        memcpy(prev_obs.locations, new_obs->locations, new_obs->no_obs *sizeof(carmen_feet_seg));
        memcpy(&prev_obs.robot_pose, &new_obs->robot_pose, sizeof(carmen_point_t));    
    }
    return 0;
    //return &moved_obs; //should not return NULL
}

int 
get_moving_feet_segments_1(carmen_feet_observations* new_obs, carmen_feet_observations *prev_obs, carmen_feet_observations *moved_obs, carmen_point_p points, int no_points)
{
    //static carmen_feet_observations prev_obs;
    static int first_obs = 0;
    //carmen_feet_observations moved_obs;
    memset(moved_obs, 0, sizeof(carmen_feet_observations));

    if(first_obs ==0){
        first_obs =1;
        prev_obs->locations = (carmen_feet_seg *) malloc(new_obs->no_obs *sizeof(carmen_feet_seg));
        prev_obs->no_obs = new_obs->no_obs;
        memcpy(prev_obs->locations, new_obs->locations, new_obs->no_obs *sizeof(carmen_feet_seg));
        memcpy(&prev_obs->robot_pose, &new_obs->robot_pose, sizeof(carmen_point_t));
        fprintf(stderr,"===== First feet observation\n ====");
        return 0;//moved_obs;
    }
    else{
        //add the code here

        // *****************************************
    
        int prune_count = 0;
        //fprintf(stderr,"Prune Segments \n");
      
        if (prev_obs->no_obs>0){//if there was any observations in the last scan 
            //convert to the new co-ordinate frame
	
            double dx = new_obs->robot_pose.x - prev_obs->robot_pose.x;
            double dy = new_obs->robot_pose.y - prev_obs->robot_pose.y;
            double dtheta = new_obs->robot_pose.theta - prev_obs->robot_pose.theta;
    
            double theta_0 = prev_obs->robot_pose.theta;
    
            double h = dx*cos(theta_0) + dy* sin(theta_0);
            double k = -dx*sin(theta_0) + dy* cos(theta_0);
      
            for (int i =0; i < prev_obs->no_obs; i++){ //convert all the previous 
                //observations to current robot coordinate frame
                double x0 = prev_obs->locations[i].center_x;
                double y0 = prev_obs->locations[i].center_y;

                double x1 = x0 * cos(dtheta) + y0* sin(dtheta) -h;
                double y1 = -x0* sin(dtheta) + y0 * cos(dtheta) - k;
                prev_obs->locations[i].center_x = x1;
                prev_obs->locations[i].center_y = y1;
            }
        }
    
        int obs_count = new_obs->no_obs;
        carmen_feet_seg* new_legs = new_obs->locations;

    
  
        if (obs_count>0){
            //allocate space for the moved legs
      
            carmen_feet_seg *prev_list = prev_obs->locations;
            moved_obs->locations  = (carmen_feet_seg *) malloc(new_obs->no_obs *sizeof(carmen_feet_seg));
            memcpy(&moved_obs->robot_pose, &new_obs->robot_pose, sizeof(carmen_point_t));
      
            for (int i =0; i < obs_count; i++){
                double x0 = new_legs[i].center_x;
                double y0 = new_legs[i].center_y;
      
                //search the previous points
                double min_distance = 1000.0;
      
                for (int j =0; j < prev_obs->no_obs; j++){
                    double x2 = prev_list[j].center_x;
                    double y2 = prev_list[j].center_y;
                    double temp_dist = hypot((x0-x2),(y0-y2));
                    if(temp_dist<min_distance){
                        min_distance = temp_dist;
                    }		  
                }
                if((min_distance > 0.1) && (min_distance < 0.2)){ //if there was no previous observations that are clsoe by	  
                    //search to see if there were points here
                    double min_distance = 1000.0;
                    for(int k=0; k<no_points; k++){
                        double temp_dist = hypot((x0-points[k].x),(y0-points[k].y));
                        if(temp_dist < min_distance){
                            min_distance = temp_dist;
                        }
                    }
                    if((min_distance > 0.02) && (hypot(x0,y0) < 2.0)){
                        memcpy(&moved_obs->locations[moved_obs->no_obs],&new_legs[i],sizeof(carmen_feet_seg));	    
                        moved_obs->no_obs++;
                    }
                }
            }
            if(new_obs->no_obs != moved_obs->no_obs){
                moved_obs->locations  = (carmen_feet_seg *) realloc(moved_obs->locations, moved_obs->no_obs *sizeof(carmen_feet_seg));
            }
        }
    
        /*fprintf(stderr,"Prev Obs \n");
          for (int i=0; i < prev_obs.no_obs; i++){
          fprintf(stderr,"\t %d (%f,%f)\n", i, prev_obs.locations[i].center_x, prev_obs.locations[i].center_y);
          }
    
          fprintf(stderr,"New Obs \n");
          for (int i=0; i < new_obs->no_obs; i++){
          fprintf(stderr,"\t %d (%f,%f)\n", i, new_obs->locations[i].center_x, new_obs->locations[i].center_y);
          }

          fprintf(stderr,"Moved Obs \n");
          for (int i=0; i < moved_obs->no_obs; i++){
          fprintf(stderr,"\t %d (%f,%f)\n", i, moved_obs->locations[i].center_x, moved_obs->locations[i].center_y);
          } 
          fprintf(stderr,"Updating the previous observation\n");*/
        if(new_obs->no_obs != prev_obs->no_obs){
            prev_obs->locations  = (carmen_feet_seg *) realloc(prev_obs->locations, new_obs->no_obs *sizeof(carmen_feet_seg));
        }
        prev_obs->no_obs = new_obs->no_obs;
        memcpy(prev_obs->locations, new_obs->locations, new_obs->no_obs *sizeof(carmen_feet_seg));
        memcpy(&prev_obs->robot_pose, &new_obs->robot_pose, sizeof(carmen_point_t));    
    }
    return 0;
    //return &moved_obs; //should not return NULL
}



int detect_feet(carmen_point_p segment_points, int count, carmen_circle* circle, int high_mode)
{
    double gap = get_distance_points(segment_points[0], segment_points[count-1]);

    double mean_dist = get_mean_dist(segment_points, count);

    if(count < 10){
        return 0;
    }

    double gap_min = 0;
    double gap_max = 0;

    if(high_mode){
        gap_max = H_GAP_MAX;
        gap_min = H_GAP_MIN;
    }
    else{
        gap_max = L_GAP_MAX;
        gap_min = L_GAP_MIN;
    }


    if ((gap>gap_min) && (gap<gap_max) && (mean_dist <5.0)){//3.5 //last value was 4.5 but incresed now 
        carmen_point_t mean_point = get_mean_point(segment_points,count);

        circle->center_x = mean_point.x;
        circle->center_y = mean_point.y;
        circle->radius = gap; //gap from the first to the last point in the segment     
        return 1;
    }
    else{
        return 0;
    }
}

//this is the one that is being used
int detect_feet_new(carmen_point_p segment_points, int count, carmen_feet_seg* circle, int start_segment, int end_segment, int front_laser, int high_mode)
{
    double gap = get_distance_points(segment_points[0], segment_points[count-1]);

    double mean_dist = get_mean_dist(segment_points, count);

    if(count < MIN_FEET_POINTS){
        return 0;
    }
    //fprintf(stderr,"Feet Count : %d\n",count);
    //used earlier 0.05 - 0.6
    double gap_min = 0;
    double gap_max = 0;

    if(high_mode){
        gap_max = H_GAP_MAX;
        gap_min = H_GAP_MIN;
    }
    else{
        gap_max = L_GAP_MAX;
        gap_min = L_GAP_MIN;
    }


    if ((gap>gap_min) && (gap<gap_max) && (mean_dist <10.0)){//3.5 //last value was 4.5 but incresed now 
        carmen_point_t mean_point = get_mean_point(segment_points,count);
        circle->center_x = mean_point.x;
        circle->center_y = mean_point.y;
        circle->start_segment = start_segment;
        circle->end_segment = end_segment;
        circle->front_laser = front_laser;
        //fprintf(stderr,"Person found Old\n");
        return 1;
    
    }
    else{
        carmen_point_t mean_point = get_mean_point(segment_points,count);
        circle->center_x = mean_point.x;
        circle->center_y = mean_point.y;
        circle->start_segment = start_segment;
        circle->end_segment = end_segment;
        circle->front_laser = front_laser;
        return 0;
    }
}

int detect_feet_pruned(carmen_point_p segment_points, int count, carmen_feet_seg* circle, int start_segment, int end_segment, int front_laser, carmen_point_p person_loc, int high_mode, int pruning_mode)
{
    double gap = get_distance_points(segment_points[0], segment_points[count-1]);


    double mean_dist = get_mean_dist(segment_points, count);

    if(count < MIN_FEET_POINTS){
        return 0;
    }
    //fprintf(stderr,"Feet Count : %d\n",count);
    double gap_min = 0;
    double gap_max = 0;

    if(high_mode){
        gap_max = H_GAP_MAX;
        gap_min = H_GAP_MIN;
    }
    else{
        gap_max = L_GAP_MAX;
        gap_min = L_GAP_MIN;
    }


    if ((gap>gap_min) && (gap<gap_max) && (mean_dist <10.0)){//3.5 //last value was 4.5 but incresed now
        carmen_point_t mean_point = get_mean_point(segment_points,count);
        if(pruning_mode == 1){
            if(person_loc !=NULL){
                //check if the observation is within 1m of the person 
                double t_dist = hypot((mean_point.x - person_loc->x),(mean_point.y - person_loc->y));
                if (t_dist > 1.0){//this should also be a changable parameter
                    return 0;
                }      
            }
        }
    
        if(pruning_mode == 2){
            int close_to_person = 1; 
            //check if close to person 
            if(person_loc !=NULL){
                //check if the observation is within 1m of the person 
                double t_dist = hypot((mean_point.x - person_loc->x),(mean_point.y - person_loc->y));
                if (t_dist > PRUNING_DIST_FROM_GUIDE){//this should also be a changable parameter
                    close_to_person = 0; 
                }      
            }
            //check if close to the robot 
            if(mean_dist > PRUNING_DIST_FROM_ROBOT && !close_to_person){
                return 0;
            }
        }

        circle->center_x = mean_point.x;
        circle->center_y = mean_point.y;
        circle->start_segment = start_segment;
        circle->end_segment = end_segment;
        circle->front_laser = front_laser;		
        //fprintf(stderr,"Person found Old\n");
        return 1;
    }
    else{
        return 0;
    }
}

int do_segment_pruned(carmen_point_p points, int no_points, carmen_feet_seg *circle_list, float max_seg_point_gap, int front_laser, carmen_point_p person_loc, int high_mode, int use_classifier, int pruning_mode)//, int meta_count)
{
    int seg_count = 0;
  
    carmen_point_p segment_points;  
    segment_points = (carmen_point_p)malloc(no_points*sizeof(carmen_point_t));  
    carmen_test_alloc(segment_points);  
  
    segment_points[0] = points[0];

    int point_count = 1;  
    int start_segment = 0, end_segment = 0;

    int i;
    double distance = 0.0;

    laser_segment curr_seg; 
    memset(&curr_seg, 0, sizeof(laser_segment));

    laser_point2d *seg_points = malloc(sizeof(laser_point2d) * no_points);

    //segmentation procedure
    for(i = 1; i < no_points; i++){
        distance = hypot(points[i].x -segment_points[point_count-1].x, 
                         points[i].y - segment_points[point_count-1].y);
    
        if(distance < max_seg_point_gap && i <= no_points-1 ){ 
            //within the segment - need to send this if this point is the last one from the laser
            //is within the same segment
            if(point_count==0){//initialize the end segment to be the start segment
                start_segment = i;
                end_segment = start_segment;
            }
            else{//increment the end segment
                end_segment++;
            }      
            //count++;      
            segment_points[point_count] = points[i];      
            point_count++;

            seg_points[curr_seg.no_points].pos[0] = points[i].x;
            seg_points[curr_seg.no_points].pos[1] = points[i].y;
            curr_seg.no_points++;

            if(i == no_points-1){ //we are at the end of the laser scan - check if this is a foot 	
                curr_seg.jump_dist_suc = 100;
                curr_seg.points = malloc(sizeof(laser_point2d) * curr_seg.no_points);
                memcpy(curr_seg.points,seg_points, sizeof(laser_point2d) * curr_seg.no_points);
                int detection = 0;
#ifdef USE_ADA
                detection = check_segment(&curr_seg, &circle_list[seg_count], start_segment, end_segment, front_laser);
#endif
                free(curr_seg.points);
#ifndef USE_ADA
                detection = detect_feet_pruned(segment_points, point_count, &circle_list[seg_count], start_segment, end_segment, front_laser, person_loc, high_mode, 1); 
#endif
                seg_count = seg_count + detection;
            }
        }
    
        else{ //********************there is no outlier detection here 
            curr_seg.jump_dist_suc = distance;
            //add the as a segment 
            curr_seg.points = malloc(sizeof(laser_point2d) * curr_seg.no_points);
            memcpy(curr_seg.points,seg_points, sizeof(laser_point2d) * curr_seg.no_points);
            int detection = 0;
#ifdef USE_ADA
            detection = check_segment(&curr_seg, &circle_list[seg_count], start_segment, end_segment, front_laser);
#endif
            free(curr_seg.points);
	
            curr_seg.jump_dist_pre = distance; 
            curr_seg.no_points = 0;
            seg_points[curr_seg.no_points].pos[0] = points[i].x;
            seg_points[curr_seg.no_points].pos[1] = points[i].y;
            curr_seg.no_points++;

#ifndef USE_ADA
            detection = detect_feet_pruned(segment_points, point_count, &circle_list[seg_count], start_segment, end_segment, front_laser, person_loc, high_mode, 1);       
#endif
            seg_count = seg_count + detection;      

            //reset the segment_points - to start with the first of the current new segment
            start_segment = i;
            end_segment = start_segment;
      
            //since the point i was not an outlier it should be the start of a new segment
            point_count = 0;
            segment_points[point_count] = points[i];
            point_count++;	    
        }
    }
    free(segment_points);
    free(seg_points);

    return seg_count;
}




int do_segment(carmen_point_p points, int no_points, carmen_feet_seg *circle_list, float max_seg_point_gap, int front_laser, int high_mode, int use_classifier)//, int meta_count)
{
    int seg_count = 0;
  
    carmen_point_p segment_points;  
    segment_points = (carmen_point_p)malloc(no_points*sizeof(carmen_point_t));  
    carmen_test_alloc(segment_points);  
  
    //first point is added to the segment 
    segment_points[0] = points[0];

    int point_count = 1;  
    int start_segment = 0, end_segment = 0;

    int i;
    int i_back = 0;
    double distance = 0.0;

    laser_segment curr_seg; 
    memset(&curr_seg, 0, sizeof(laser_segment));

    laser_point2d *seg_points = malloc(sizeof(laser_point2d) * no_points);

    //segmentation procedure
    for(i = 1; i < no_points; i++){
        //**********There might still be an error here - check 
    
        distance = hypot(points[i].x -points[i_back].x,
                         points[i].y -points[i_back].y );
        //fprintf(stderr,"[%d - %d]: %f\t", i, front_laser, distance);
    
    

        if(distance < max_seg_point_gap && i <= no_points-1 ){ 
            i_back++;

            seg_points[curr_seg.no_points].pos[0] = points[i].x;
            seg_points[curr_seg.no_points].pos[1] = points[i].y;
            curr_seg.no_points++;

            //within the segment - need to send this if this point is the last one from the laser
            //is within the same segment
            if(point_count==0){//initialize the end segment to be the start segment
                start_segment = i;
                end_segment = start_segment;
            }
            else{//increment the end segment
                end_segment++;
            }      
            //count++;      
            segment_points[point_count] = points[i];      
            point_count++;
            if(i == no_points-1){ //we are at the end of the laser scan - check if this is a foot 	
                curr_seg.jump_dist_suc = 100;
                curr_seg.points = malloc(sizeof(laser_point2d) * curr_seg.no_points);
                memcpy(curr_seg.points,seg_points, sizeof(laser_point2d) * curr_seg.no_points);
                
                int detection = 0;
#ifdef USE_ADA
                detection = check_segment(&curr_seg, &circle_list[seg_count],start_segment, end_segment, front_laser);
#endif
                free(curr_seg.points);
#ifndef USE_ADA
                detection = detect_feet_new(segment_points, point_count, &circle_list[seg_count], start_segment, end_segment, front_laser, high_mode); 
#endif
                //calculate and do the classification 	
                seg_count += detection;
            }
        }
    
        else{ 
            int outlier_found = 0;
            //added handling of outliers 
            for(int j = fmin(i+1,no_points-1) ; j < fmin(i+3, no_points-1);j++){
                double temp_dist = hypot(points[j].x -points[i_back].x,
                                         points[j].y -points[i_back].y );
                if(temp_dist < max_seg_point_gap){
                    end_segment = j;
                    segment_points[point_count] = points[j];
                    seg_points[curr_seg.no_points].pos[0] = points[j].x;
                    seg_points[curr_seg.no_points].pos[1] = points[j].y;
                    curr_seg.no_points++;

                    point_count++;
                    outlier_found = 1;
                    i_back = j;
                    i = i_back;
                    break;
                }
            }
            if(!outlier_found){
                curr_seg.jump_dist_suc = distance;
                //add the as a segment 
                curr_seg.points = malloc(sizeof(laser_point2d) * curr_seg.no_points);
                memcpy(curr_seg.points,seg_points, sizeof(laser_point2d) * curr_seg.no_points);

                int detection = 0;
#ifdef USE_ADA
                detection = check_segment(&curr_seg, &circle_list[seg_count], start_segment, end_segment, front_laser);
#endif
                free(curr_seg.points);
	
                curr_seg.jump_dist_pre = distance; 
                curr_seg.no_points = 0;
                seg_points[curr_seg.no_points].pos[0] = points[i].x;
                seg_points[curr_seg.no_points].pos[1] = points[i].y;
                curr_seg.no_points++;

                //fprintf(stderr,"Gap : %f   [%d]:(%f,%f) - [%d](%f,%f)\n", distance,i-1, points[i-1].x, points[i-1].y, i,
                //	points[i].x, points[i].y);
#ifndef USE_ADA
                detection = detect_feet_new(segment_points, point_count, &circle_list[seg_count], start_segment, end_segment, front_laser, high_mode); 
#endif
                seg_count += detection;      

                //reset the segment_points - to start with the first of the current new segment
                start_segment = i;
                end_segment = start_segment;
      
                //since the point i was not an outlier it should be the start of a new segment
                point_count = 0;
                segment_points[point_count] = points[i];
                point_count++;	
                i_back++;
            }    
        }
    }
    free(segment_points);
    free(seg_points);

    return seg_count;
}

int do_segment_act(carmen_point_p points, int no_points, carmen_feet_seg *circle_list, float max_seg_point_gap, int front_laser, int high_mode)//, int meta_count)
{
    int seg_count = 0;
  
    carmen_point_p segment_points;  
    segment_points = (carmen_point_p)malloc(no_points*sizeof(carmen_point_t));  
    carmen_test_alloc(segment_points);  
  
    segment_points[0] = points[0];

    int point_count = 1;  
    int start_segment = 0, end_segment = 0;

    int i;
    double distance = 0.0;

    //segmentation procedure
    for(i = 1; i < no_points; i++){
        distance = hypot(points[i].x -segment_points[point_count-1].x, 
                         points[i].y - segment_points[point_count-1].y);
    
        if(distance < max_seg_point_gap && i <= no_points-1 ){ 
            //within the segment - need to send this if this point is the last one from the laser
            //is within the same segment
            if(point_count==0){//initialize the end segment to be the start segment
                start_segment = i;
                end_segment = start_segment;
            }
            else{//increment the end segment
                end_segment++;
            }      
            //count++;      
            segment_points[point_count] = points[i];      
            point_count++;
            if(i == no_points-1){ //we are at the end of the laser scan - check if this is a foot 	
                int detection = detect_feet_new(segment_points, point_count, &circle_list[seg_count], start_segment, end_segment, front_laser, high_mode); 
                seg_count = seg_count + detection;
            }
        }
    
        else{ 
            //fprintf(stderr,"[%d] Gap : %f\n", i, distance);
            int detection = detect_feet_new(segment_points, point_count, &circle_list[seg_count], start_segment, end_segment, front_laser, high_mode); 
            seg_count = seg_count + detection;      

            //reset the segment_points - to start with the first of the current new segment
            start_segment = i;
            end_segment = start_segment;
      
            //since the point i was not an outlier it should be the start of a new segment
            point_count = 0;
            segment_points[point_count] = points[i];
            point_count++;	    
        }
    }
    free(segment_points);

    return seg_count;
}

void test_pass()
/*bot_core_planar_lidar_t fl, laser_offset_t fl_offset, //bot_core_pose_t fl_pose, 
  bot_core_planar_lidar_t rl, laser_offset_t rl_offset, //bot_core_pose_t rl_pose, 
  float max_seg_point_gap,
  carmen_point_p points, int *no_points, int high_mode, 
  int use_classifier)*/
{
    //fprintf(stderr, "Done\n"); 
}

carmen_feet_observations
lcm_detect_segments_from_robot_laser(erlcm_robot_laser_t *fl, laser_offset_t fl_offset,
                                     erlcm_robot_laser_t *rl, laser_offset_t rl_offset,
                                     float max_seg_point_gap,
                                     carmen_point_p points, int *no_points, int high_mode, 
                                     int use_classifier){

    //fprintf(stderr, "Called 01 \n");

    //this call itself fails 
  
    return lcm_detect_segments_from_planar_lidar(&(fl->laser), fl_offset, fl->pose,
                                                 &(rl->laser), rl_offset, rl->pose, 
                                                 max_seg_point_gap, points, 
                                                 no_points, high_mode, use_classifier); 
			       
}

//we dont need the pose - lets just assume for now that the readings are coming at the same time 

carmen_feet_observations
lcm_detect_segments_from_planar_lidar(bot_core_planar_lidar_t *fl, laser_offset_t fl_offset, bot_core_pose_t fl_pose, 
                                      bot_core_planar_lidar_t *rl, laser_offset_t rl_offset, bot_core_pose_t rl_pose, 
                                      float max_seg_point_gap,
                                      carmen_point_p points, int *no_points, int high_mode, 
                                      int use_classifier) 
//segments the laser scans, detects circles and returns valid set of observations - count and positions
{
    carmen_point_p fr_points = (carmen_point_p)malloc((fl->nranges)*sizeof(carmen_point_t));  
    carmen_test_alloc(fr_points); 

    carmen_point_p re_points = NULL;

    //convert the range scans to x,y points - in the robot coordinate frame
    int fr_no_points = 0;
    int re_no_points = 0;

    fr_no_points = lcm_range_to_points(fl, fr_points,fl_offset, 0);

    //fprintf(stderr, "Points Obtained\n");

    if(rl!=NULL){
        re_points = (carmen_point_p)malloc((rl->nranges)*sizeof(carmen_point_t));  
        carmen_test_alloc(re_points); 
        re_no_points = lcm_range_to_points(rl, re_points,rl_offset, 0);
    }

    *no_points = fr_no_points + re_no_points;
  
    //copy over the points - to be returned to the caller
    memset(points, 0, (fr_no_points + re_no_points) * sizeof(carmen_point_t));
    memcpy(points, fr_points, fr_no_points * sizeof(carmen_point_t));

    //variable to hold the possible person observations
    carmen_feet_seg *front_seg = (carmen_feet_seg*) malloc(fr_no_points*sizeof(carmen_feet_seg));
    carmen_test_alloc(front_seg);
    carmen_feet_seg *rear_seg = NULL;
    if(rl!=NULL){
        memcpy(points + fr_no_points , re_points , re_no_points * sizeof(carmen_point_t));
        rear_seg = (carmen_feet_seg*) malloc(re_no_points*sizeof(carmen_feet_seg));
        carmen_test_alloc(rear_seg);  
    }

    //fprintf(stderr, "Allocated\n");
  
    int fr_seg_count = 0;
    int re_seg_count = 0;  
  
    fr_seg_count = do_segment(fr_points,fr_no_points, front_seg, max_seg_point_gap, 1, high_mode, use_classifier);
    if(rl!=NULL){
        re_seg_count = do_segment(re_points,re_no_points, rear_seg, max_seg_point_gap, 0, high_mode, use_classifier);
    }

    carmen_feet_seg *combined_seg = (carmen_feet_seg*) malloc((fr_seg_count + re_seg_count)*sizeof(carmen_feet_seg));
    carmen_test_alloc(combined_seg);

    memset(combined_seg, 0, (fr_seg_count + re_seg_count) * sizeof(carmen_feet_seg));
    memcpy(combined_seg, front_seg, fr_seg_count * sizeof(carmen_feet_seg));
    if(rl!=NULL){
        memcpy(combined_seg + fr_seg_count , rear_seg , re_seg_count * sizeof(carmen_feet_seg));
    }
      
    carmen_feet_observations person_obs;
    person_obs.locations = combined_seg;
    person_obs.no_obs = fr_seg_count + re_seg_count;
  
    double rpy[3] = {0,0,0};  
    bot_quat_to_roll_pitch_yaw (fl_pose.orientation, rpy) ;

    person_obs.robot_pose.x = fl_pose.pos[0];
    person_obs.robot_pose.y = fl_pose.pos[1];
    person_obs.robot_pose.theta = rpy[2];

    free(fr_points);
    free(re_points);
    free(front_seg);
    free(rear_seg);

    return person_obs;
}

carmen_feet_observations
lcm_detect_segments_from_robot_laser_pruned(erlcm_robot_laser_t *fl, laser_offset_t fl_offset, 
                                            erlcm_robot_laser_t *rl, laser_offset_t rl_offset,
                                            float max_seg_point_gap,
                                            carmen_point_p points, int *no_points, 
                                            carmen_point_p person_loc, int high_mode, int use_classifier, int pruning_mode) 
{
    return lcm_detect_segments_from_planar_lidar_pruned(&fl->laser, fl_offset, fl->pose, 
                                                        &rl->laser, rl_offset, rl->pose, 
                                                        max_seg_point_gap, points, 
                                                        no_points, person_loc, high_mode, use_classifier, pruning_mode); 


}

carmen_feet_observations
lcm_detect_segments_from_planar_lidar_pruned(bot_core_planar_lidar_t *fl, laser_offset_t fl_offset, bot_core_pose_t fl_pose, 
                                             bot_core_planar_lidar_t *rl, laser_offset_t rl_offset, bot_core_pose_t rl_pose, 
                                             float max_seg_point_gap,
                                             carmen_point_p points, int *no_points, 
                                             carmen_point_p person_loc, int high_mode, int use_classifier, 
                                             int pruning_mode) 
//segments the laser scans, detects circles and returns valid set of observations - count and positions
{
    carmen_point_p fr_points = (carmen_point_p)malloc((fl->nranges)*sizeof(carmen_point_t));  
    carmen_test_alloc(fr_points); 

    carmen_point_p re_points = NULL;

    //convert the range scans to x,y points - in the robot coordinate frame
    int fr_no_points = 0;
    int re_no_points = 0;

    fr_no_points = lcm_range_to_points(fl, fr_points,fl_offset, 0);

    if(rl!=NULL){
        re_points = (carmen_point_p)malloc((rl->nranges)*sizeof(carmen_point_t));  
        carmen_test_alloc(re_points); 
        re_no_points = lcm_range_to_points(rl, re_points,rl_offset, 0);
    }
    *no_points = fr_no_points + re_no_points;
  
    //copy over the points - to be returned to the caller
    memset(points, 0, (fr_no_points + re_no_points) * sizeof(carmen_point_t));
    memcpy(points, fr_points, fr_no_points * sizeof(carmen_point_t));

    //variable to hold the possible person observations
    carmen_feet_seg *front_seg = (carmen_feet_seg*) malloc(fr_no_points*sizeof(carmen_feet_seg));
    carmen_test_alloc(front_seg);
    carmen_feet_seg *rear_seg = NULL;
    if(rl!=NULL){
        memcpy(points + fr_no_points , re_points , re_no_points * sizeof(carmen_point_t));
        rear_seg = (carmen_feet_seg*) malloc(re_no_points*sizeof(carmen_feet_seg));
        carmen_test_alloc(rear_seg);  
    }
  
    int fr_seg_count = 0;
    int re_seg_count = 0;  
  
    fr_seg_count = do_segment_pruned(fr_points,fr_no_points, front_seg, max_seg_point_gap, 1, person_loc, high_mode, use_classifier, pruning_mode);
    if(rl!=NULL){
        re_seg_count = do_segment_pruned(re_points,re_no_points, rear_seg, max_seg_point_gap, 0, person_loc, high_mode, use_classifier, pruning_mode);
    }

    carmen_feet_seg *combined_seg = (carmen_feet_seg*) malloc((fr_seg_count + re_seg_count)*sizeof(carmen_feet_seg));
    carmen_test_alloc(combined_seg);

    memset(combined_seg, 0, (fr_seg_count + re_seg_count) * sizeof(carmen_feet_seg));
    memcpy(combined_seg, front_seg, fr_seg_count * sizeof(carmen_feet_seg));
    if(rl!=NULL){
        memcpy(combined_seg + fr_seg_count , rear_seg , re_seg_count * sizeof(carmen_feet_seg));
    }
      
    carmen_feet_observations person_obs;
    person_obs.locations = combined_seg;
    person_obs.no_obs = fr_seg_count + re_seg_count;

    double rpy[3] = {0,0,0};  
    bot_quat_to_roll_pitch_yaw (fl_pose.orientation, rpy) ;

    person_obs.robot_pose.x = fl_pose.pos[0];
    person_obs.robot_pose.y = fl_pose.pos[1];
    person_obs.robot_pose.theta = rpy[2];
    //person_obs.robot_pose = fl->robot_pose;

    free(fr_points);
    free(re_points);
    free(front_seg);
    free(rear_seg);

    return person_obs;
}

carmen_feet_observations
detect_segments_both(carmen_robot_laser_message *fl, laser_offset_t fl_offset,//float fl_offset,
                     carmen_robot_laser_message *rl, laser_offset_t rl_offset,//float rl_offset,
                     float max_seg_point_gap,
                     carmen_point_p points, int *no_points, int high_mode, 
                     int use_classifier) 
//segments the laser scans, detects circles and returns valid set of observations - count and positions
{
    carmen_point_p fr_points = (carmen_point_p)malloc((fl->num_readings)*sizeof(carmen_point_t));  
    carmen_test_alloc(fr_points); 

    carmen_point_p re_points = NULL;

    //convert the range scans to x,y points - in the robot coordinate frame
    int fr_no_points = 0;
    int re_no_points = 0;

    fr_no_points = range_to_points(fl, fr_points,fl_offset);

    if(rl!=NULL){
        re_points = (carmen_point_p)malloc((rl->num_readings)*sizeof(carmen_point_t));  
        carmen_test_alloc(re_points); 
        re_no_points = range_to_points(rl, re_points,rl_offset);
    }
    *no_points = fr_no_points + re_no_points;
  
    //copy over the points - to be returned to the caller
    memset(points, 0, (fr_no_points + re_no_points) * sizeof(carmen_point_t));
    memcpy(points, fr_points, fr_no_points * sizeof(carmen_point_t));

    //variable to hold the possible person observations
    carmen_feet_seg *front_seg = (carmen_feet_seg*) malloc(fr_no_points*sizeof(carmen_feet_seg));
    carmen_test_alloc(front_seg);
    carmen_feet_seg *rear_seg = NULL;
    if(rl!=NULL){
        memcpy(points + fr_no_points , re_points , re_no_points * sizeof(carmen_point_t));
        rear_seg = (carmen_feet_seg*) malloc(re_no_points*sizeof(carmen_feet_seg));
        carmen_test_alloc(rear_seg);  
    }
  
    int fr_seg_count = 0;
    int re_seg_count = 0;  
  
    fr_seg_count = do_segment(fr_points,fr_no_points, front_seg, max_seg_point_gap, 1, high_mode, use_classifier);
    if(rl!=NULL){
        re_seg_count = do_segment(re_points,re_no_points, rear_seg, max_seg_point_gap, 0, high_mode, use_classifier);
    }

    carmen_feet_seg *combined_seg = (carmen_feet_seg*) malloc((fr_seg_count + re_seg_count)*sizeof(carmen_feet_seg));
    carmen_test_alloc(combined_seg);

    memset(combined_seg, 0, (fr_seg_count + re_seg_count) * sizeof(carmen_feet_seg));
    memcpy(combined_seg, front_seg, fr_seg_count * sizeof(carmen_feet_seg));
    if(rl!=NULL){
        memcpy(combined_seg + fr_seg_count , rear_seg , re_seg_count * sizeof(carmen_feet_seg));
    }
      
    carmen_feet_observations person_obs;
    person_obs.locations = combined_seg;
    person_obs.no_obs = fr_seg_count + re_seg_count;
    person_obs.robot_pose = fl->robot_pose;

    free(fr_points);
    free(re_points);
    free(front_seg);
    free(rear_seg);

    return person_obs;
}

carmen_feet_observations
detect_segments_both_pruned(carmen_robot_laser_message *fl, laser_offset_t fl_offset,
                            carmen_robot_laser_message *rl, laser_offset_t rl_offset,
                            float max_seg_point_gap,
                            carmen_point_p points, int *no_points, 
                            carmen_point_p person_loc, int high_mode, int use_classifier) 
//segments the laser scans, detects circles and returns valid set of observations - count and positions
{
    carmen_point_p fr_points = (carmen_point_p)malloc((fl->num_readings)*sizeof(carmen_point_t));  
    carmen_test_alloc(fr_points); 

    carmen_point_p re_points = NULL;

    //convert the range scans to x,y points - in the robot coordinate frame
    int fr_no_points = 0;
    int re_no_points = 0;

    fr_no_points = range_to_points(fl, fr_points,fl_offset);

    if(rl!=NULL){
        re_points = (carmen_point_p)malloc((rl->num_readings)*sizeof(carmen_point_t));  
        carmen_test_alloc(re_points); 
        re_no_points = range_to_points(rl, re_points,rl_offset);
    }
    *no_points = fr_no_points + re_no_points;
  
    //copy over the points - to be returned to the caller
    memset(points, 0, (fr_no_points + re_no_points) * sizeof(carmen_point_t));
    memcpy(points, fr_points, fr_no_points * sizeof(carmen_point_t));

    //variable to hold the possible person observations
    carmen_feet_seg *front_seg = (carmen_feet_seg*) malloc(fr_no_points*sizeof(carmen_feet_seg));
    carmen_test_alloc(front_seg);
    carmen_feet_seg *rear_seg = NULL;
    if(rl!=NULL){
        memcpy(points + fr_no_points , re_points , re_no_points * sizeof(carmen_point_t));
        rear_seg = (carmen_feet_seg*) malloc(re_no_points*sizeof(carmen_feet_seg));
        carmen_test_alloc(rear_seg);  
    }
  
    int fr_seg_count = 0;
    int re_seg_count = 0;  
  
    fr_seg_count = do_segment_pruned(fr_points,fr_no_points, front_seg, max_seg_point_gap, 1, person_loc, high_mode, use_classifier, 1);
    if(rl!=NULL){
        re_seg_count = do_segment_pruned(re_points,re_no_points, rear_seg, max_seg_point_gap, 0, person_loc, high_mode, use_classifier, 1);
    }

    carmen_feet_seg *combined_seg = (carmen_feet_seg*) malloc((fr_seg_count + re_seg_count)*sizeof(carmen_feet_seg));
    carmen_test_alloc(combined_seg);

    memset(combined_seg, 0, (fr_seg_count + re_seg_count) * sizeof(carmen_feet_seg));
    memcpy(combined_seg, front_seg, fr_seg_count * sizeof(carmen_feet_seg));
    if(rl!=NULL){
        memcpy(combined_seg + fr_seg_count , rear_seg , re_seg_count * sizeof(carmen_feet_seg));
    }
      
    carmen_feet_observations person_obs;
    person_obs.locations = combined_seg;
    person_obs.no_obs = fr_seg_count + re_seg_count;
    person_obs.robot_pose = fl->robot_pose;

    free(fr_points);
    free(re_points);
    free(front_seg);
    free(rear_seg);

    return person_obs;
}



carmen_feet_observations
detect_segments(carmen_robot_laser_message *laser_msg, float max_seg_point_gap, laser_offset_t laser_offset,carmen_point_p points, int high_mode, int use_classifier) 
//segments the laser scans, detects circles and returns valid set of observations - count and positions
{
    int i;
    int meta_count = 0;  
    int start_count = 1;  
    double distance = 0.0;
    int point_count = 1;  
    int outlier_limit = 2;//5;  //No of points that can make up an outlier

    //convert the range scans to x,y points - in the robot coordinate frame
    int no_points = range_to_points(laser_msg, points,laser_offset);  

    carmen_point_p segment_points;  
    segment_points = (carmen_point_p)malloc((laser_msg->num_readings)*sizeof(carmen_point_t));  
    carmen_test_alloc(segment_points);  

    //variable to hold the possible person observations
    carmen_feet_seg *circle_list = (carmen_feet_seg* ) malloc(start_count*sizeof(carmen_feet_seg));
    carmen_test_alloc(circle_list);

    segment_points[0] = points[0];

    int start_segment = 0, end_segment = 0;
 
    //segmentation procedure
    for(i = 1; i < no_points; i++){
        distance = hypot(points[i].x -segment_points[point_count-1].x, 
                         points[i].y - segment_points[point_count-1].y);
    
        if(distance < max_seg_point_gap && i <= no_points-1 ){ //within the segment - need to send this if this point is the last one from the laser
            //is within the same segment
            if(point_count==0){//initialize the end segment to be the start segment
                start_segment = i;
                end_segment = start_segment;
            }
            else{//increment the end segment
                end_segment++;
            }      
            //count++;      
            segment_points[point_count] = points[i];      
            point_count++;
            if(i == no_points-1){ //we are at the end of the laser scan - check if this is a foot 	
                int detection = detect_feet_new(segment_points, point_count, &circle_list[meta_count], start_segment, end_segment, 1, high_mode); 
                //fprintf(stderr,"End of Reading reached - checking Feet Count : %d, Feet Detected : %d\n",point_count,detection); 
                meta_count = meta_count + detection;
	
                if (meta_count >=start_count-1){//reallocate the pointers if the size becomes too small	      
                    carmen_feet_seg *temp_list = (carmen_feet_seg *) realloc(circle_list,(meta_count+5)*sizeof(carmen_feet_seg));
                    carmen_test_alloc(temp_list);
                    circle_list = temp_list;
                }
            }
      
        }
    
        //outlier removal
        else{ 
            //fprintf(stderr,"Gap : %f\n", distance);
            int detection = detect_feet_new(segment_points, point_count, &circle_list[meta_count], start_segment, end_segment, 1, high_mode); 

            meta_count = meta_count + detection;
      
            if (meta_count >=start_count-1){//reallocate the pointers if the size becomes too small	      
                carmen_feet_seg *temp_list = (carmen_feet_seg *) realloc(circle_list,(meta_count+5)*sizeof(carmen_feet_seg));
                carmen_test_alloc(temp_list);
                circle_list = temp_list;
            }
      
            //reset the segment_points - to start with the first of the current new segment
            start_segment = i;
            end_segment = start_segment;
      
            //since the point i was not an outlier it should be the start of a new segment
            point_count = 0;
            segment_points[point_count] = points[i];
            point_count++;	    
        }
    }

    /*//Point which might be a boundry point or an outlier
      int k;  //Do a forward search to see if this is an outlier or a new segment
      for (k = 0; k<outlier_limit; k++){
      //make sure that this forward search is withing the num-points
	
      float temp_distance = hypot(points[i+k].x -points[i+k+1].x , 
      points[i+k].y - points[i+k+1].y);
	
      if(temp_distance < max_seg_point_gap && i+k < laser_msg->num_readings-1 ){ 	   
	  
      if ((k == outlier_limit-1)){  //no outlier detected and therefore the new points are a new segment

      //segment identified and sent to feet detection 
      //int detection = detect_feet(segment_points, point_count, &circle_list[meta_count]); 
      int detection = detect_feet_new(segment_points, point_count, &circle_list[meta_count], start_segment, end_segment); 

      meta_count = meta_count + detection;
	    
      if (meta_count >=start_count-1){//reallocate the pointers if the size becomes too small	      
      carmen_feet_seg *temp_list = (carmen_feet_seg *) realloc(circle_list,(meta_count+5)*sizeof(carmen_feet_seg));
      carmen_test_alloc(temp_list);
      circle_list = temp_list;
      }
	    
      //reset the segment_points - to start with the first of the current new segment
      start_segment = i;
      end_segment = start_segment;

      //since the point i was not an outlier it should be the start of a new segment
      point_count = 0;
      segment_points[point_count] = points[i];
      point_count++;	    
      }
      continue;	    
      }	
      else{//possible outlier detected - this does not mean that the next point is close to the last point of the segment
      if(i+k < laser_msg->num_readings-1){
      distance = hypot(points[i+k].x -segment_points[point_count].x, 
      points[i+k].y - segment_points[point_count].y);
      if(distance < max_seg_point_gap){//the point after the outlier is part of the earlier segment
      end_segment= i+k;
      segment_points[point_count] = points[i+k];      
      point_count++;
      i = i+k+1;
      break; 
      }
      }
      else{//there is either no point after the outlier end or the point after the outlier is outside our last segment
      start_segment = i;
      end_segment = start_segment;
      point_count = 0;
      segment_points[point_count] = points[i];
      point_count++;
      }	 
      }
      }
      }
      }*/

    free(segment_points);
    //free(points);
	         
    carmen_feet_observations person_obs;
    person_obs.locations = circle_list;
    person_obs.no_obs = meta_count;
    person_obs.robot_pose = laser_msg->robot_pose;

    return person_obs;
}

