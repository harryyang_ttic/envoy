#include <carmen/carmen.h>
#include <carmen/localize_messages.h>
#include "person_messages.h"
#include "person_interface.h"
#include "person_localizecore.h"
#include <unistd.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

/* global variables */

//LCM stuff
#include <lcm/lcm.h>
#include <bot/bot_core.h>

//Carmen3d stuff
#include <carmen3d/carmen3d_lcmtypes.h>
#include <carmen3d/carmen3d_common.h>
#include <carmen3d/lcm_utils.h>
#include <carmen3d/agile-globals.h>

#define REALLOCATE_TIME 1.0  //no of seconds to wait to reallocate
#define CLEARING_RADIUS 0.3 //radius to clear around person estimate
#define SEGMENT_GAP 0.05 //gap used in deciding segments 


typedef struct _State {
    carmen3d_localize_param_t param;
    carmen_localize_map_t map;
    carmen_map_placelist_t placelist;
    carmen3d_localize_particle_filter_p filter;
    carmen3d_localize_summary_t summary;

    carmen3d_localize_particle_filter_p *people_filter = NULL;
    carmen3d_localize_summary_p people_summary = NULL;
    double *time_since = NULL;
    carmen_point_p people_positions = NULL;
    erlcm_point_t *people_pos = NULL;

    int multi_people_tracking = 0;
    int no_people = 0;
    int actual_filter_size = 5;

    int new_person_detection = 0;
    int first_person_detection = 0;
    int summary_active = 0;
    int first_person_obs = 0;
    int active_filter_ind = -1;

    int debug_mode = 0;

    double lcm_count = 0;
    double ipc_count = 0;

    //carmen_robot_laser_message front_laser;
    carmen_robot_laser_message carmen_robot_frontlaser;
    carmen_robot_laser_message carmen_robot_rearlaser;
    int have_front_laser = 0;
    int have_rear_laser = 0;

    carmen_point_t last_robot_position;
    carmen_point_t person_position;
    carmen_point_t new_person_position;
    carmen_person_test_message test_message;
    carmen_person_globalpos_message gpos_message;
    carmen_point_t global_person_position;

    carmen_point_t global_robot_pose;
    lcm_t * lcm;
} State;



enum FollowingState{
    FOLLOWING, //person is being tracked and followed - issuing navigation commands to the navigator 
    IDLE //not following (can still have a track on the person)
};

enum TrackingState{
    FOUND, //person has been found
    LOOKING, //looking for person (should look for a particular time and give an error message if we cant find the person
    NOT_LOOKING,//system is initialized and has no track on the person 
    LOST // system has lost track of the person 
};
enum FollowingState following_state = IDLE;
enum TrackingState tracking_state = NOT_LOOKING;
double started_looking_time = 0.0;



//lcm message publishers

inline int max(int x,int y){
    if(x>y)
        return x;
    else
        return y;
}
inline int min(int x,int y){
    if(x>y)
        return y;
    else
        return x;
}

void publish_speech_msg(char* property)
{
    erlcm_speech_cmd_t msg;
    msg.utime = bot_timestamp_now();
    msg.cmd_type = "FOLLOWER";
    msg.cmd_property = property;
    erlcm_speech_cmd_t_publish(lcm, "PERSON_TRACKER", &msg);
}

void publish_speech_following_msg(char* property)
{
    erlcm_speech_cmd_t msg;
    msg.utime = bot_timestamp_now();
    msg.cmd_type = "FOLLOWER";
    msg.cmd_property = property;
    erlcm_speech_cmd_t_publish(lcm, "PERSON_TRACKER", &msg);
}

void publish_speech_tracking_msg(char* property)
{
    erlcm_speech_cmd_t msg;
    msg.utime = bot_timestamp_now();
    msg.cmd_type = "TRACKER";
    msg.cmd_property = property;
    erlcm_speech_cmd_t_publish(lcm, "PERSON_TRACKER", &msg);
}

//publishes both planar lidar (for the person navigator and robot_laser (for slam)
void robot_laser_pub(char * CHAN, carmen_robot_laser_message * robot, lcm_t * lcm)
{
    fprintf(stderr,"F\n");
    erlcm_robot_laser_t msg;
    memset(&msg, 0, sizeof(msg));
    msg.laser.rad0 = robot->config.start_angle;
    msg.laser.radstep = robot->config.angular_resolution;
    msg.laser.nranges = robot->num_readings;
    msg.laser.ranges = robot->range;
    msg.pose.pos[0] = robot->robot_pose.x;
    msg.pose.pos[1] = robot->robot_pose.y;
    msg.pose.pos[2] = param.front_laser_offset; //this is added to the front laser - will cause an error in other laser configurations
    double rpy[3] = { 0, 0, robot->robot_pose.theta };
    bot_roll_pitch_yaw_to_quat(rpy, msg.pose.orientation);
    msg.utime = robot->timestamp * 1e6;
    msg.laser.utime = msg.utime;
    msg.pose.utime = msg.utime;
    msg.cov.x = 1;
    msg.cov.xy = 0;
    msg.cov.y = 1;
    msg.cov.yaw = .1;
    //bot_core_planar_lidar_t_publish(lcm,"HOKUYO_FORWARD",&msg.laser);
    erlcm_robot_laser_t_publish(lcm,CHAN,&msg);
    fprintf(stderr,"L");
}

void publish_mission_control(lcm_t *lc, int type){
    erlcm_mission_control_msg_t msg;
    msg.utime = bot_timestamp_now();
    msg.type = type;
    erlcm_mission_control_msg_t_publish(lc, MISSION_CONTROL_CHANNEL, &msg);
}

//State updates based on speech commands 


//tracking commands are not being handled by this - it is now being handled by the person navigator
void update_following_state(enum FollowingState new_state){
    if(new_state==IDLE){//should stop the wheelchair
        if(following_state == FOLLOWING){
            fprintf(stderr,"Stopping Wheelchair\n");
            publish_mission_control(lcm, CARMEN3D_MISSION_CONTROL_MSG_T_NAVIGATOR_CLEAR_GOAL);
            publish_speech_following_msg("STOPPED");
        }
        else{
            fprintf(stderr,"Chair is already stopped\n");
        }

        following_state = new_state;
        //issue comand for the robot to stop - and the navigator to clear 
        //current goal - otherwise it will keep using the old goal 
    }
    else if(new_state==FOLLOWING){//check if we have a tracking of the person 
        if(tracking_state == FOUND){//if person is tracked the start following
            following_state = new_state;
            fprintf(stderr,"Following Person\n");
            publish_speech_following_msg("FOLLOWING");
        }
        else{//we dont have an estimate of the person 
            following_state = IDLE;
            fprintf(stderr,"Unable to Follow\n");
            publish_speech_following_msg("UNABLE_TO_FOLLOW_LOST"); //there should also be additional reasons
            //- i.e. there is no path to follow the person - we should use this if navigator issues such a message
        }
    }
}

void update_tracking_state(enum TrackingState new_state){
    tracking_state = new_state;
    if(new_state==LOST){
        active_filter_ind = -1;
        publish_speech_tracking_msg("LOST");
        update_following_state(IDLE);
    }else if (new_state == FOUND){
        publish_speech_tracking_msg("FOUND");
        //we should not start following immediately - should follow only 
        //when we get a start following command
    }else if (new_state == LOOKING){
        //reset the person filer - maybe even clear up the current person filter??
        active_filter_ind = -1;
        publish_speech_tracking_msg("LOOKING"); 
        update_following_state(IDLE); //we need to update the tracking state because this looking message may be 
        //generated becaue the guide wants the chair to reaquire him in the case of the chair following a wrong target
    }  
}

void querry_tracking_state(void){
    if(tracking_state== FOUND){
        publish_speech_tracking_msg("SEE_YES");
    }
    else if((tracking_state== LOST) ||(tracking_state== NOT_LOOKING)){
        publish_speech_tracking_msg("SEE_NO");
    }
    else if(tracking_state== LOOKING){
        publish_speech_tracking_msg("SEE_LOOKING");
    }    
}

void initialize_particles(carmen_point_t mean, carmen_point_t std)
{
    carmen_localize_initialize_particles_gaussian_person(filter,
                                                         mean,
                                                         std);
}

void initialize_filter(carmen3d_localize_particle_filter_p pfilter,
                       carmen_point_t mean, carmen_point_t std)
{
    carmen_localize_initialize_particles_gaussian_person(pfilter,
                                                         mean,
                                                         std);
}

//published at the end of each laser message - for the person navigator
void publish_lcm_heading_goal(){ 

    erlcm_goal_heading_msg_t msg;
    msg.utime = bot_timestamp_now();
    if(active_filter_ind !=-1){  //We have active person filter  
        float person_x = people_summary[active_filter_ind].mean.x;
        float person_y = people_summary[active_filter_ind].mean.y;
        //check condition to start following a person
        float act_dist = hypot(person_x,person_y);
        float act_angle = atan2(person_y,person_x);
        msg.target_heading = act_angle;
        if(act_dist >= 0.75){//heading restriction removed from here
            msg.tracking_state = 0; //0 - suitable for following 
            fprintf(stderr, "Person in followable Location \n");
        }
        else{
            msg.tracking_state = 1;
            fprintf(stderr,"Person within range - Not following");
        }
    }
    else{
        msg.target_heading = 0.0;
        msg.tracking_state = -1;
        fprintf(stderr,"Person Location unknown");
    }
    erlcm_goal_heading_msg_t_publish(lcm, "GOAL_HEADING", &msg);
}


void publish_lcm_goal() //called everytime we get a pose message - for the navigator 
{  
    if(active_filter_ind !=-1){  //We have active person filter  

        //Publishing goal interms of global framework
        erlcm_point_t goal;
        float person_x = people_summary[active_filter_ind].mean.x;
        float person_y = people_summary[active_filter_ind].mean.y;
    
        //check condition to start following a person
        float act_dist = hypot(person_x,person_y);
        float act_angle = atan2(person_y,person_x);
        if((act_dist >= 0.75) || act_angle >= M_PI/30){
            float theta = global_robot_pose.theta;
            goal.x = global_robot_pose.x + person_x*cos(theta) - person_y*sin(theta);
            goal.y = global_robot_pose.y + person_x*sin(theta) + person_y*cos(theta);
            goal.z = 0.0;
            goal.yaw = 0.0;
            goal.pitch = 0.0;
            goal.roll = 0.0;
      
            carmen3d_navigator_goal_msg_t msg;
            msg.goal = goal;
            msg.use_theta = 0; //whether to use theta
            msg.utime = bot_timestamp_now();
            msg.nonce = irand(10000000);//no idea where this function is implemented -random();
            msg.sender = CARMEN3D_NAVIGATOR_GOAL_MSG_T_SENDER_WAYPOINT_TOOL;
            carmen3d_navigator_goal_msg_t_publish(lcm, NAV_GOAL_CHANNEL, &msg);
            fprintf(stderr, "Publishing Navigator Goal: x %f y %f z %f yaw %f\n", goal.x, goal.y, goal.z, goal.yaw);
        }    
        else{
            fprintf(stderr,"Person within range - Not following");
        }
    }
    else{
        fprintf(stderr,"Person Location unknown");
    }
}

void publish_people_message()  //people message - carmen message published relative to the robot 
//- for every robot laser message
{
    carmen3d_people_pos_msg_t people_list;
    people_list.utime = bot_timestamp_now();
    people_list.num_people = no_people;
    people_list.followed_person = active_filter_ind;
  
    people_list.people_pos = (erlcm_point_t *) malloc(no_people*sizeof(erlcm_point_t));
    carmen_test_alloc(people_list.people_pos);
    memcpy(people_list.people_pos,people_pos, no_people*sizeof(erlcm_point_t));
  
    carmen3d_people_pos_msg_t_publish(lcm,"PEOPLE_LIST",&people_list);
    free(people_list.people_pos);
}

int 
initialize_people_filters(int no_filters, State *s)
{
    fprintf(stderr,"Filters Initialized \n");
    s->people_filter = (carmen3d_localize_particle_filter_p *)
        malloc(no_filters*sizeof(carmen3d_localize_particle_filter_p)); 
    carmen_test_alloc(s->people_filter);
  
    s->people_summary  = (carmen3d_localize_summary_p) 
        malloc(no_filters*sizeof(carmen3d_localize_summary_t));
    carmen_test_alloc(s->people_summary);
    
    s->time_since = (double *) malloc(no_filters*sizeof(double));
    carmen_test_alloc(s->time_since);
    return 0;
}



int create_new_person(carmen_point_t person_obs, carmen_point_t std, double time)  
//this is called once the filters that have observations have been updated so this 
//wont reallocate a filter that has an observation in this time period
{
    if(actual_filter_size <=no_people){//we dont have enough filters to allocate -increase by 5
        fprintf(stderr,"Increasing total no of filters");
        carmen3d_localize_particle_filter_p *temp_people_filter = (carmen3d_localize_particle_filter_p *)
            realloc(people_filter,(actual_filter_size+5)*sizeof(carmen3d_localize_particle_filter_p)); 
        carmen_test_alloc(temp_people_filter);
        people_filter = temp_people_filter;
    
        carmen3d_localize_summary_p temp_people_summary  = (carmen3d_localize_summary_p) 
            realloc(people_summary,(actual_filter_size+5)*sizeof(carmen3d_localize_summary_t));
        carmen_test_alloc(temp_people_summary);
        people_summary = temp_people_summary;
    
        double * temp_time_since = (double *) realloc(time_since, (actual_filter_size+5)*sizeof(double));
        carmen_test_alloc(temp_time_since);
        time_since = temp_time_since;
        actual_filter_size +=5;

    }
    people_filter[no_people] = carmen3d_localize_particle_filter_person_new(&param); //create a new filter 
    //this will be freed when that person is not being tracked
    initialize_filter(people_filter[no_people],person_obs,std);  
    carmen_localize_summarize_person(people_filter[no_people], &people_summary[no_people]);
    time_since[no_people] = time;
    no_people++;
  
    return 0;
}

int 
prune_filters(double time_obs){
    int k,current_act_people = 0;

    int valid_filter_count = 0;
  
    for(int j=0;j<no_people;j++){//cycle through the array 
        double dist_from_chair = hypot((people_summary[j].mean.x),(people_summary[j].mean.y));
    
        //have a smaller realloc time for the non-following filters - would cut down on a lot of things 
        //also use the lower number of particles - i.e. move away from ipc param daemon
    
        if(((time_obs-time_since[j]) <= REALLOCATE_TIME) && ((j== active_filter_ind) || (dist_from_chair < 2.0))){
            valid_filter_count++;
        }
    }
    //if the current no_of filters is larger than the live filters, 
    //remove the dead filters and reorder the live ones
    if(no_people>valid_filter_count){//current_act_people) &(current_act_people>=0)){  
        int i=0,j;
  
        //allocate memory for the reduced filters 
    
        fprintf(stderr,"New filters created \n");
        carmen3d_localize_particle_filter_p *new_people_filter = (carmen3d_localize_particle_filter_p *)
            malloc(valid_filter_count*sizeof(carmen3d_localize_particle_filter_p)); 
        carmen_test_alloc(new_people_filter);
  
        carmen3d_localize_summary_p new_people_summary  = (carmen3d_localize_summary_p) 
            malloc(valid_filter_count*sizeof(carmen3d_localize_summary_t));
        carmen_test_alloc(new_people_summary);
    
        double *new_time_since = (double *) malloc(valid_filter_count*sizeof(double));
        carmen_test_alloc(new_time_since); 
    

        for(j=0;j<no_people;j++){//cycle through the array 
            double dist_from_chair = hypot((people_summary[j].mean.x),(people_summary[j].mean.y));
            if(((time_obs-time_since[j]) <= REALLOCATE_TIME) && ((j== active_filter_ind) || (dist_from_chair < 2.0))){
                memcpy(&new_people_filter[i],&people_filter[j], sizeof(carmen3d_localize_particle_filter_p));
                memcpy(&new_people_summary[i],&people_summary[j],sizeof(carmen3d_localize_summary_t));
                new_time_since[i] = time_since[j];
                if(j == active_filter_ind){//if the position of the active filter in the filter list is changed update the location
                    active_filter_ind = i;
                }
                fprintf(stderr,"\n");
                i++;	
            }
            else{//free the unused filter
                if(j == active_filter_ind){//if the position of the active filter in the filter list is changed update the location
                    active_filter_ind = -1;  //-1 reflects the fact that there is no active filter
                    update_tracking_state(LOST);
                    fprintf(stderr,"+++++++ Person Lost +++++++ **** \n");
                    publish_speech_msg("LOST");
                }

                fprintf(stderr,"==\tPruning Filter \n");
                free(people_filter[j]->particles); //free the filter 
                for(k = 0; k < people_filter[j]->param->num_particles; k++) {
                    free(people_filter[j]->temp_weights[k]);
                }
                free(people_filter[j]->temp_weights);
            }
        }
        free(people_filter);
        free(people_summary);
        free(time_since);

        people_filter = new_people_filter;
        people_summary = new_people_summary;
        time_since = new_time_since;

        fprintf(stderr,"Reallocated Filters %d \n",i);
        no_people = valid_filter_count;  //update the new no of filters
        actual_filter_size = no_people;
    } 
    return 0;
}


int 
detect_person_both_basic(){ 
    if((!have_rear_laser && !have_front_laser)){
        return -1;
    }
  
    //int front_laser indicates if this is the front laser - i.e. we remove person legs only from that 
    double time_obs = carmen_robot_frontlaser.timestamp; //carmen_get_time();  //time of the last observation

    carmen_point_t robot_pos = carmen_robot_frontlaser.robot_pose;
    int no_points = 0;
    carmen_point_p points = NULL;  //point array for the laser readings
    carmen_feet_observations person_obs;

    if(!have_rear_laser){
        points = (carmen_point_p)malloc(carmen_robot_frontlaser.num_readings *sizeof(carmen_point_t));  
        carmen_test_alloc(points);
        person_obs = detect_segments_both(&carmen_robot_frontlaser, param.front_laser_offset,
                                          NULL, param.rear_laser_offset, 
                                          SEGMENT_GAP,points, &no_points); //runs segment detection - used at 0.2 
    
    }
    else{
        points = (carmen_point_p)malloc((carmen_robot_frontlaser.num_readings + carmen_robot_rearlaser.num_readings)*sizeof(carmen_point_t)); 
        carmen_test_alloc(points); 
        person_obs = detect_segments_both(&carmen_robot_frontlaser, param.front_laser_offset,
                                          &carmen_robot_rearlaser, param.rear_laser_offset, 
                                          SEGMENT_GAP,points, &no_points); //runs segment detection - used at 0.2 
    }

    //call the new feet detector
    //get_moving_feet_segments(&person_obs);

    //need to add function door detection 
  
    //Merge the person's legs (i.e. two close by detections - in to one)
    //merge_legs(&person_obs);

    carmen_feet_observations used_person_obs;
  
    //Prune observations if they are in the same place as in the last frame - based on motion only
    //used_person_obs = prune_observations(&person_obs);
    used_person_obs = person_obs; //no pruning 
  
    carmen_point_t std;    
    std.x = 1.0;
    std.y = 1.0;
    std.theta = carmen_degrees_to_radians(4.0); 
  
    int meta_count = used_person_obs.no_obs;
    carmen_feet_seg* feet_obs = used_person_obs.locations;
    int i,j;

    carmen_point_p person_det = (carmen_point_p) malloc(meta_count*sizeof(carmen_point_t)); 
    carmen_test_alloc(person_det);

    //copy the observations on to points
    for (i=0;i<meta_count;i++){
        person_det[i].x = feet_obs[i].center_x;
        person_det[i].y = feet_obs[i].center_y;
        person_det[i].theta = 0.0;			
    }

    //draw possible legs 
    bot_lcmgl_t *lcmgl = globals_get_lcmgl("leg_obs",1);

    if (1 && lcmgl){// && is_front_laser){ //turn to 1 if u want to draw
        lcmglColor3f(0, 1.0, 0);
    
        float rtheta = global_robot_pose.theta;
        double pos[3];
        for (i=0;i<meta_count;i++){
            float leg_x = global_robot_pose.x + person_det[i].x * cos(rtheta) - person_det[i].y * sin(rtheta);
            float leg_y = global_robot_pose.y + person_det[i].x * sin(rtheta) + person_det[i].y*cos(rtheta);
            pos[0] = leg_x;
            pos[1] = leg_y;
            pos[2] = 2.0;  
     
            lcmglLineWidth (5);
            lcmglColor3f(.0, 1.0, .0);
            lcmglCircle(pos, 0.1);
      
            if(debug_mode){
                char seg_info[512];
                sprintf(seg_info,"[%d] %d-%d:%d", feet_obs[i].front_laser, feet_obs[i].start_segment, feet_obs[i].end_segment, 
                        feet_obs[i].start_segment - feet_obs[i].end_segment);
                bot_lcmgl_text(lcmgl, pos, seg_info);
            }
        } 
    } 

    bot_lcmgl_switch_buffer (lcmgl);

    //prune dead filters
    prune_filters(time_obs);  

    //Filter initiation code
    //create a filter for the observation that is not near any of the current filters

    carmen_point_t robot_position;
  
    robot_position.x = carmen_robot_frontlaser.robot_pose.x;
    robot_position.y = carmen_robot_frontlaser.robot_pose.y;
    robot_position.theta = carmen_robot_frontlaser.robot_pose.theta;

    //update the filter summaries based on the robot motion 
    for(j=0;j<no_people;j++){
        carmen_localize_update_filter_center(last_robot_position,robot_position,&people_summary[j].mean);
    }
  
    double **dist;
    dist = (double **) malloc(meta_count*sizeof(double *));
    carmen_test_alloc(dist);
    //create a distance matrix between the filters and the observations
    for (i=0;i<meta_count;i++){
        dist[i] = (double *) malloc(no_people*sizeof(double));
        carmen_test_alloc(dist[i]);
        for(j=0;j<no_people;j++){
            dist[i][j] = hypot((people_summary[j].mean.x-person_det[i].x),(people_summary[j].mean.y-person_det[i].y));
        }
    }

    //allocate each observation to the closest person filter 
    int *no_obs_count = (int *) malloc(no_people*sizeof(int)); //no of observations that each filter can see
    carmen_test_alloc(no_obs_count);
    memset(no_obs_count,0,no_people*sizeof(int));

    int original_filter_size = no_people;
  
    int *allocated_to_filter = (int *) malloc(meta_count*sizeof(int)); //no of observations that each filter can see
    carmen_test_alloc(allocated_to_filter);
    memset(allocated_to_filter,0,meta_count*sizeof(int));
  
    int **allocated_observations = (int **) malloc(no_people*sizeof(int *)); //what observations are assigned to each filter
    carmen_test_alloc(allocated_observations);
    for (j=0;j<no_people;j++){
        allocated_observations[j] = (int *) malloc(meta_count*sizeof(int)); //what observations are assigned to each filter
        carmen_test_alloc(allocated_observations[j]);
        memset(allocated_observations[j],-1,meta_count*sizeof(int));
    }

    double filter_creation_angle = M_PI/12; //M_PI/12;
    double filter_creation_dist = 3.0;//3.0;

    for (i=0;i<meta_count;i++){  //if there is no person_filter near the observation - then allocate a new filter
        double min_dist = 0.4;//1.0;//worked also with 0.8
        int best_filter = -1;
    
        //search through all the person filters 
        for(j=0;j<no_people;j++){
            if(dist[i][j]<min_dist){
                min_dist = dist[i][j];
                best_filter = j;	
            }
        }
        if(best_filter >=0){//add current observation to the closest filter observation list
            int index = no_obs_count[best_filter];
            allocated_observations[best_filter][index]= i;
            no_obs_count[best_filter]++;
            allocated_to_filter[i]=1;
        }
    }
  
    for(j=0;j<no_people;j++){
        if(no_obs_count[j]==0){
            carmen_localize_run_person_no_obs(people_filter[j], robot_pos);
            carmen_localize_summarize_person(people_filter[j], &people_summary[j]);
        }
        else if(no_obs_count[j]==1){
            int obs = allocated_observations[j][0];
            fprintf(stderr,"People Filter Ind : %d , Loc : (%f,%f)\n", j, 
                    people_summary[j].mean.x, people_summary[j].mean.y); 
            carmen_localize_run_person_with_obs(people_filter[j], robot_pos,person_det[obs]);
            carmen_localize_summarize_person(people_filter[j], &people_summary[j]);
            time_since[j] = time_obs; 
        }

        else{//find the two observations that are closest to eachother 
            int k;
            double dist1 = 100;
            int obs1 = -1;
            int obs2 = -1;
            for(k=0;k<no_obs_count[j];k++){
                int obs = allocated_observations[j][k];
                if(dist[obs][j]<dist1){
                    dist1 = dist[obs][j];
                    obs1 = obs;
                }
            }

            double dist2 = 100;
            double temp_dist;
      
            for(k=0;k<no_obs_count[j];k++){
                int obs = allocated_observations[j][k];
                if(obs==obs1)
                    continue;
                else{
                    temp_dist = hypot((person_det[obs].x - person_det[obs1].x),(person_det[obs].y - person_det[obs1].y));
                    if(temp_dist < dist2){
                        dist2 = temp_dist;
                        obs2 = obs;
                    }
                }
            }

            if(dist2<0.01){//0.4){
                carmen_point_t mid_point;
                mid_point.x = (person_det[obs1].x + person_det[obs2].x)/2;
                mid_point.y = (person_det[obs1].y + person_det[obs2].y)/2;
                mid_point.theta = (person_det[obs1].theta + person_det[obs2].theta)/2; 
	
                carmen_localize_run_person_with_obs(people_filter[j], robot_pos, mid_point);
            }
            else{
                carmen_localize_run_person_with_obs(people_filter[j], robot_pos, person_det[obs1]);
            }
            carmen_localize_summarize_person(people_filter[j], &people_summary[j]);
            time_since[j] = time_obs;
        }    
    }
    if(active_filter_ind < 0){ //creating new filters only if there is no active filter - // we should prob change this so it creates only one filer 
        for (i=0;i<meta_count;i++){
            if(allocated_to_filter[i]==0){
                double angle = atan2(person_det[i].y,person_det[i].x);
                double dist = hypot(person_det[i].x,person_det[i].y);
	
                /*if(active_filter_ind >=0){//this seems to create more filters than necessary 
                //and looses filters
                double current_x = people_summary[active_filter_ind].mean.x;
                double current_y = people_summary[active_filter_ind].mean.y;
                double dist_from_person = hypot(person_det[i].x-current_x,person_det[i].y-current_y);
                if(dist_from_person < 2.0){
                create_new_person(person_det[i],std, time_obs);
                }
                }
                else*/
                if (fabs(angle)< filter_creation_angle && dist < filter_creation_dist){
                    int near_filter = 0;
                    double min_dist = 1.0;
                    double temp_dist;
                    for(j=0;j<no_people;j++){
                        temp_dist = hypot((people_summary[j].mean.x-person_det[i].x),(people_summary[j].mean.y-person_det[i].y));
                        if(temp_dist< min_dist){
                            min_dist = temp_dist;
                            near_filter = 1;
                            break;
                        }
                    }
                    if(near_filter==0){//no nearby filter was created 
                        create_new_person(person_det[i],std, time_obs);
                    }
                }      
            }
        }
    }
    fprintf(stderr,"Current States => Tracking : %d, Following: %d\n",tracking_state, following_state);    

    if(tracking_state==LOOKING){//if we are in the tracking state we should reaquire 
        double min_dist = 4.0;
        double follow_angle = M_PI/12;
        double temp_dist;
        double temp_angle;
        fprintf(stderr,"Looking for person\n");
        for(j=0;j<no_people;j++){
            temp_dist = hypot(people_summary[j].mean.x,people_summary[j].mean.y);
            temp_angle = atan2(people_summary[j].mean.y,people_summary[j].mean.x);
            if((fabs(temp_angle) < follow_angle) && (temp_dist < min_dist)){
                min_dist = temp_dist;
                active_filter_ind = j;	
            }
        }
        //selects the closest observation with-in the angle cone 
        if(active_filter_ind >=0){
            update_tracking_state(FOUND);  //person located	
            fprintf(stderr,"====== New Person Following Activated =========\n");      
        }
        else{
            fprintf(stderr,"**** No good filter found **** \n");
        }
    }
    else{
        if(active_filter_ind>=0){
            fprintf(stderr,"== Current Person Followed is %d : (%f,%f)\n", active_filter_ind, 
                    people_summary[active_filter_ind].mean.x,people_summary[active_filter_ind].mean.y);
        }
        else{
            fprintf(stderr,"== No one being tracked \n");
        }
    }

    fprintf(stderr,"New filter Positions\n");
    for (j=0;j<no_people;j++){
        fprintf(stderr,"(%f,%f)\t",people_summary[j].mean.x,people_summary[j].mean.y);
    }

    if(no_people>0){    
        erlcm_point_t* test_people_pos = (erlcm_point_t *) 
            realloc(people_pos,no_people*sizeof(erlcm_point_t));
        carmen_test_alloc(test_people_pos);
        people_pos = test_people_pos;
    }

    int person_pos = 0;

    for(j=0;j<no_people;j++){
        people_pos[person_pos].x = people_summary[j].mean.x;
        people_pos[person_pos].y = people_summary[j].mean.y;
        people_pos[person_pos].z = 0.0;
        people_pos[person_pos].yaw = 0.0;
        people_pos[person_pos].pitch = 0.0;
        people_pos[person_pos].roll = people_summary[j].mean.theta;
        person_pos++;
    }

    //publishing a person removed robot and planar lidar message
    //if(is_front_laser){
    float distance_from_person = 0.0;
    static carmen_robot_laser_message* carmen_laser;
  
  
    if(carmen_laser == NULL){
        carmen_laser = (carmen_robot_laser_message *) malloc(sizeof(carmen_robot_laser_message));
        carmen_test_alloc(carmen_laser);
    }
    memcpy(carmen_laser,&carmen_robot_frontlaser, sizeof(carmen_robot_laser_message));
    if(carmen_laser->range==NULL){
        carmen_laser->range = (float *) malloc(carmen_robot_frontlaser.num_readings * sizeof(float));
        carmen_test_alloc(carmen_laser->range);
    }
  
    memcpy(carmen_laser->range, carmen_robot_frontlaser.range, 
           carmen_robot_frontlaser.num_readings * sizeof(float));
  
    double person_leg_range = 1.0;
    int removed_legs = 0;
    if(active_filter_ind >=0){  //if we are tracking a person -remove the observations attached to the person 
        for (i=0;i<meta_count;i++){
            distance_from_person = hypot(feet_obs[i].center_x - people_summary[active_filter_ind].mean.x,
                                         feet_obs[i].center_y - people_summary[active_filter_ind].mean.y);
            if(distance_from_person < person_leg_range){
                removed_legs++;
                int p;
                fprintf(stderr,"[%d:%d]\n",feet_obs[i].start_segment, feet_obs[i].end_segment);
                for(p = max(feet_obs[i].start_segment-2,0); p <=min(feet_obs[i].end_segment+2,carmen_robot_frontlaser.num_readings);p++){
                    carmen_laser->range[p] = 0.001;  //also might have implication on the max range conversion done in laser driver
                } 	  
            }
        }
        fprintf(stderr,"No of Observations Removed : %d\n", removed_legs);
    
        //remove any points within 0.3 
        for(int p = 0; p < carmen_robot_frontlaser.num_readings ;p++){
            distance_from_person = hypot(points[i].x - people_summary[active_filter_ind].mean.x,
                                         points[i].y - people_summary[active_filter_ind].mean.y);
            if(distance_from_person < CLEARING_RADIUS){
                carmen_laser->range[p] = 0.001;  	  
            }
        }
    }
    
    robot_laser_pub("PERSON_ROBOT_LASER",carmen_laser,lcm);
  
    publish_people_message();

    if(active_filter_ind >=0){//draw person if we have a person being tracked
        //bot_lcmgl_t *lcmgl = globals_get_lcmgl("person",1);
        lcmgl = globals_get_lcmgl("person",1);

        double pos[3];

        float person_x = people_summary[active_filter_ind].mean.x;
        float person_y = people_summary[active_filter_ind].mean.y;
        float theta = global_robot_pose.theta;

        float goal_x = global_robot_pose.x + person_x * cos(theta) - person_y*sin(theta);
        float goal_y = global_robot_pose.y + person_x * sin(theta) + person_y*cos(theta);

        pos[0] = goal_x;
        pos[1] = goal_y;
        pos[2] = 2.0;  

        if (1 && lcmgl) {//turn to 1 if u want to draw
            lcmglLineWidth (5);
            lcmglColor3f(1.0, .0, .0);
            lcmglCircle(pos, 0.15);
        }
        bot_lcmgl_switch_buffer (lcmgl);
    }
  
    fprintf(stderr,"No of filters %d \n", no_people);
    fprintf(stderr,"------------------------------------------------------------\n");
    free(person_det);

    for (i=0;i<meta_count;i++){
        free(dist[i]);
    }
    free(dist);

    for (j=0;j<original_filter_size;j++){
        free(allocated_observations[j]);
    }
    free(allocated_observations);
    free(no_obs_count);
    free(allocated_to_filter);
    free(used_person_obs.locations);
    free(points);

    return 1;
}


int 
detect_person_both_multi(){ 
    if((!have_rear_laser && !have_front_laser)){
        return -1;
    }
  
    //int front_laser indicates if this is the front laser - i.e. we remove person legs only from that 
    double time_obs = carmen_robot_frontlaser.timestamp; //carmen_get_time();  //time of the last observation

    carmen_point_t robot_pos = carmen_robot_frontlaser.robot_pose;
    int no_points = 0;
    carmen_point_p points = NULL;  //point array for the laser readings

    static carmen_point_p prev_points = NULL;
    //static carmen_feet_observations prev_obs;
    //static carmen_feet_observations *prev_obs =NULL;
  
    //memset(prev_obs, 0, sizeof(prev_obs));
    static int prev_no_points = 0;

    carmen_point_t std;    
    std.x = 1.0;
    std.y = 1.0;
    std.theta = carmen_degrees_to_radians(4.0); 

    carmen_feet_observations person_obs;

    if(!have_rear_laser){
        points = (carmen_point_p)malloc(carmen_robot_frontlaser.num_readings *sizeof(carmen_point_t));  
        carmen_test_alloc(points);
        person_obs = detect_segments_both(&carmen_robot_frontlaser, param.front_laser_offset,
                                          NULL, param.rear_laser_offset, 
                                          SEGMENT_GAP,points, &no_points); //runs segment detection - used at 0.2 
    
    }
    else{
        points = (carmen_point_p)malloc((carmen_robot_frontlaser.num_readings + carmen_robot_rearlaser.num_readings)*sizeof(carmen_point_t)); 
        carmen_test_alloc(points); 
        person_obs = detect_segments_both(&carmen_robot_frontlaser, param.front_laser_offset,
                                          &carmen_robot_rearlaser, param.rear_laser_offset, 
                                          SEGMENT_GAP,points, &no_points); //runs segment detection - used at 0.2 
    }

    //call the new feet detector
    carmen_feet_observations moved_obs;
    get_moving_feet_segments(&person_obs, &moved_obs, prev_points, prev_no_points);
  
    carmen_feet_observations used_person_obs;
  
    //Prune observations if they are in the same place as in the last frame - based on motion only
    //used_person_obs = prune_observations(&person_obs);
    used_person_obs = person_obs; //no pruning 
  
 
  
    int meta_count = used_person_obs.no_obs;
    carmen_feet_seg* feet_obs = used_person_obs.locations;
    int i,j;

    carmen_point_p person_det = (carmen_point_p) malloc(meta_count*sizeof(carmen_point_t)); 
    carmen_test_alloc(person_det);

    //copy the observations on to points
    for (i=0;i<meta_count;i++){
        person_det[i].x = feet_obs[i].center_x;
        person_det[i].y = feet_obs[i].center_y;
        person_det[i].theta = 0.0;			
    }

    //draw possible legs 
    bot_lcmgl_t *lcmgl = globals_get_lcmgl("leg_obs",1);

    if (1 && lcmgl){// && is_front_laser){ //turn to 1 if u want to draw
        lcmglColor3f(0, 1.0, 0);
    
        float rtheta = global_robot_pose.theta;
        double pos[3];
        for (i=0;i<meta_count;i++){
            float leg_x = global_robot_pose.x + person_det[i].x * cos(rtheta) - person_det[i].y * sin(rtheta);
            float leg_y = global_robot_pose.y + person_det[i].x * sin(rtheta) + person_det[i].y*cos(rtheta);
            pos[0] = leg_x;
            pos[1] = leg_y;
            pos[2] = 2.0;  
     
            lcmglLineWidth (5);
            lcmglColor3f(.0, 1.0, .0);
            lcmglCircle(pos, 0.1);
            if(debug_mode){
                char seg_info[512];
                sprintf(seg_info,"[%d]%d-%d:%d", feet_obs[i].front_laser, feet_obs[i].start_segment, feet_obs[i].end_segment, 
                        feet_obs[i].start_segment - feet_obs[i].end_segment);
                bot_lcmgl_text(lcmgl, pos, seg_info);
            } 
        }
    } 

    bot_lcmgl_switch_buffer (lcmgl);

    lcmgl = globals_get_lcmgl("moved_leg_obs",1);

    if (1 && lcmgl){// && is_front_laser){ //turn to 1 if u want to draw
        float rtheta = global_robot_pose.theta;
        double pos[3];
        for (i=0;i<moved_obs.no_obs;i++){
            float leg_x = global_robot_pose.x + moved_obs.locations[i].center_x * cos(rtheta) - moved_obs.locations[i].center_y * sin(rtheta);
            float leg_y = global_robot_pose.y + moved_obs.locations[i].center_x * sin(rtheta) + moved_obs.locations[i].center_y*cos(rtheta);
            pos[0] = leg_x;
            pos[1] = leg_y;
            pos[2] = 2.0;  
     
            lcmglLineWidth (5);
            lcmglColor3f(1.0, 1.0, .0);
            lcmglCircle(pos, 0.1);
        } 
    } 

    bot_lcmgl_switch_buffer (lcmgl);

    //prune dead filters
    prune_filters(time_obs);  

    //Filter initiation code
    //create a filter for the observation that is not near any of the current filters

    carmen_point_t robot_position;
  
    robot_position.x = carmen_robot_frontlaser.robot_pose.x;
    robot_position.y = carmen_robot_frontlaser.robot_pose.y;
    robot_position.theta = carmen_robot_frontlaser.robot_pose.theta;

    //update the filter summaries based on the robot motion 
    for(j=0;j<no_people;j++){
        carmen_localize_update_filter_center(last_robot_position,robot_position,&people_summary[j].mean);
    }

    carmen_point_p moved_person = (carmen_point_p) malloc(moved_obs.no_obs*sizeof(carmen_point_t)); 
    carmen_test_alloc(moved_person);

    //copy the observations on to points
    for (int i=0;i< moved_obs.no_obs;i++){
        moved_person[i].x = moved_obs.locations[i].center_x;
        moved_person[i].y = moved_obs.locations[i].center_y;
        moved_person[i].theta = 0.0;
        //check if there are other filters near by
        double min_dist = 1000;
        for(j=0;j<no_people;j++){
            double temp_dist = hypot((people_summary[j].mean.x - moved_person[i].x),(people_summary[j].mean.y-moved_person[i].y));
            if(temp_dist < min_dist){
                min_dist = temp_dist;
            }
        }
        if(min_dist > 0.8){//1.0){
            //check if the distance between the person being followed and this new location is farther than 1.0 ? (or 1.2?)
            if(active_filter_ind>=0){
                double temp_dist = hypot((people_summary[active_filter_ind].mean.x - moved_person[i].x),
                                         (people_summary[active_filter_ind].mean.y-moved_person[i].y));
                if(temp_dist > 1.0){	  
                    fprintf(stderr,"----------- Creating Filter\n");	     
                    create_new_person(moved_person[i],std, time_obs);   
                }
                else{
                    fprintf(stderr,"----------- Too Close to the person being followed\n");
                }
            }
            else{   
                fprintf(stderr,"----------- Creating Filter\n");	     
                create_new_person(moved_person[i],std, time_obs);
            }
        }
    }

  
    double **dist;
    dist = (double **) malloc(meta_count*sizeof(double *));
    carmen_test_alloc(dist);
    //create a distance matrix between the filters and the observations
    for (i=0;i<meta_count;i++){
        dist[i] = (double *) malloc(no_people*sizeof(double));
        carmen_test_alloc(dist[i]);
        for(j=0;j<no_people;j++){
            dist[i][j] = hypot((people_summary[j].mean.x-person_det[i].x),(people_summary[j].mean.y-person_det[i].y));
        }
    }

    //allocate each observation to the closest person filter 
    int *no_obs_count = (int *) malloc(no_people*sizeof(int)); //no of observations that each filter can see
    carmen_test_alloc(no_obs_count);
    memset(no_obs_count,0,no_people*sizeof(int));

    int original_filter_size = no_people;
  
    int *allocated_to_filter = (int *) malloc(meta_count*sizeof(int)); //no of observations that each filter can see
    carmen_test_alloc(allocated_to_filter);
    memset(allocated_to_filter,0,meta_count*sizeof(int));
  
    int **allocated_observations = (int **) malloc(no_people*sizeof(int *)); //what observations are assigned to each filter
    carmen_test_alloc(allocated_observations);
    for (j=0;j<no_people;j++){
        allocated_observations[j] = (int *) malloc(meta_count*sizeof(int)); //what observations are assigned to each filter
        carmen_test_alloc(allocated_observations[j]);
        memset(allocated_observations[j],-1,meta_count*sizeof(int));
    }

    double filter_creation_angle = M_PI/12; //M_PI/12;
    double filter_creation_dist = 3.0;//3.0;

    /*for (i=0;i<meta_count;i++){  //if there is no person_filter near the observation - then allocate a new filter
      double min_dist = 0.4;//0.4;//1.0;//worked also with 0.8
      int best_filter = -1;
    
      //search through all the person filters 
      for(j=0;j<no_people;j++){
      if(dist[i][j]<min_dist){
      min_dist = dist[i][j];
      best_filter = j;	
      }
      }
      if(best_filter >=0){//add current observation to the closest filter observation list
      int index = no_obs_count[best_filter];
      allocated_observations[best_filter][index]= i;
      no_obs_count[best_filter]++;
      allocated_to_filter[i]=1;
      }
      }*/
  
    if(active_filter_ind >=0){//first do this for the tourguide 
        double dist_to_wheelchair = 10000.0;
        int ind = -1;
        for (i=0;i<meta_count;i++){ //search through
            if(dist[i][active_filter_ind] < 0.4){ //within the boundry
                allocated_to_filter[i]=1; //make all these observations out of bounds for the other filters
                double temp_dist = hypot(person_det[i].x, person_det[i].y);
                if(temp_dist < dist_to_wheelchair){
                    ind = i;
                    dist_to_wheelchair = temp_dist;
                }
            }
        }
        //if(no_obs_count[j]==0){
        if(ind==-1){
            carmen_localize_run_person_no_obs(people_filter[active_filter_ind], robot_pos);
            carmen_localize_summarize_person(people_filter[active_filter_ind], &people_summary[active_filter_ind]);
        }
        else if(ind>=0){//(no_obs_count[j]==1){
            int obs = ind;//allocated_observations[j][0];
            carmen_localize_run_person_with_obs(people_filter[active_filter_ind], robot_pos,person_det[obs]);
            carmen_localize_summarize_person(people_filter[active_filter_ind], &people_summary[active_filter_ind]);
            time_since[active_filter_ind] = time_obs; 
        }
    }

  
    for(j=0;j<no_people;j++){//for each filter search the close observations and pick the one closest to the wheelchair 
        if(j==active_filter_ind){
            continue;
        }
        double dist_to_wheelchair = 10000.0;
        int ind = -1;
        for (i=0;i<meta_count;i++){ //search through
            if(!allocated_to_filter[i] &&(dist[i][j] < 0.4)){ //within the boundry
                double temp_dist = hypot(person_det[i].x, person_det[i].y);
                if(temp_dist < dist_to_wheelchair){
                    ind = i;
                    dist_to_wheelchair = temp_dist;
                }
            }
        }
        //if(no_obs_count[j]==0){
        if(ind==-1){
            carmen_localize_run_person_no_obs(people_filter[j], robot_pos);
            carmen_localize_summarize_person(people_filter[j], &people_summary[j]);
        }
        else if(ind>=0){//(no_obs_count[j]==1){
            allocated_to_filter[ind] = 1;
            int obs = ind;//allocated_observations[j][0];
            carmen_localize_run_person_with_obs(people_filter[j], robot_pos,person_det[obs]);
            carmen_localize_summarize_person(people_filter[j], &people_summary[j]);
            time_since[j] = time_obs; 
        }

        /*else{//find the two observations that are closest to eachother 
          int k;
          double dist1 = 100;
          int obs1 = -1;
          int obs2 = -1;
          for(k=0;k<no_obs_count[j];k++){
          int obs = allocated_observations[j][k];
          if(dist[obs][j]<dist1){
          dist1 = dist[obs][j];
          obs1 = obs;
          }
          }

          double dist2 = 100;
          double temp_dist;
      
          for(k=0;k<no_obs_count[j];k++){
          int obs = allocated_observations[j][k];
          if(obs==obs1)
          continue;
          else{
          temp_dist = hypot((person_det[obs].x - person_det[obs1].x),(person_det[obs].y - person_det[obs1].y));
          if(temp_dist < dist2){
          dist2 = temp_dist;
          obs2 = obs;
          }
          }
          }

          if(dist2<0.01){//0.4){
          carmen_point_t mid_point;
          mid_point.x = (person_det[obs1].x + person_det[obs2].x)/2;
          mid_point.y = (person_det[obs1].y + person_det[obs2].y)/2;
          mid_point.theta = (person_det[obs1].theta + person_det[obs2].theta)/2; 
	
          carmen_localize_run_person_with_obs(people_filter[j], robot_pos, mid_point);
          }
          else{
          carmen_localize_run_person_with_obs(people_filter[j], robot_pos, person_det[obs1]);
          }
          carmen_localize_summarize_person(people_filter[j], &people_summary[j]);
          time_since[j] = time_obs;
          } */   
    }

    /*if(active_filter_ind < 0){ //creating new filters only if there is no active filter - // we should prob change this so it creates only one filer 
      for (i=0;i<meta_count;i++){
      if(allocated_to_filter[i]==0){
      double angle = atan2(person_det[i].y,person_det[i].x);
      double dist = hypot(person_det[i].x,person_det[i].y);
	
      if (fabs(angle)< filter_creation_angle && dist < filter_creation_dist){
	  int near_filter = 0;
	  double min_dist = 1.0;
	  double temp_dist;
	  for(j=0;j<no_people;j++){
      temp_dist = hypot((people_summary[j].mean.x-person_det[i].x),(people_summary[j].mean.y-person_det[i].y));
      if(temp_dist< min_dist){
      min_dist = temp_dist;
      near_filter = 1;
      break;
      }
	  }
	  if(near_filter==0){//no nearby filter was created 
      create_new_person(person_det[i],std, time_obs);
	  }
      }      
      }
      }
      }*/
    fprintf(stderr,"Current States => Tracking : %d, Following: %d\n",tracking_state, following_state);    

    if(tracking_state==LOOKING){//if we are in the tracking state we should reaquire 
        double min_dist = 4.0;
        double too_close_dist = 0.5;
        //double min_dist = 0.5;
        double follow_angle = M_PI/12;
        double temp_dist;
        double temp_angle;
        fprintf(stderr,"Looking for person\n");
        for(j=0;j<no_people;j++){
            temp_dist = hypot(people_summary[j].mean.x,people_summary[j].mean.y);
            temp_angle = atan2(people_summary[j].mean.y,people_summary[j].mean.x);
            if((fabs(temp_angle) < follow_angle) 
               && (temp_dist < min_dist) && (temp_dist > too_close_dist)){
                min_dist = temp_dist;
                active_filter_ind = j;	
            }
        }
        //selects the closest observation with-in the angle cone 
        if(active_filter_ind >=0){
            update_tracking_state(FOUND);  //person located	
            fprintf(stderr,"====== New Person Following Activated =========\n");      
        }
        else{
            fprintf(stderr,"**** No good filter found **** \n");
        }
    }
    else{
        if(active_filter_ind>=0){
            fprintf(stderr,"== Current Person Followed is %d : (%f,%f)\n", active_filter_ind, 
                    people_summary[active_filter_ind].mean.x,people_summary[active_filter_ind].mean.y);
        }
        else{
            fprintf(stderr,"== No one being tracked \n");
        }
    }

    fprintf(stderr,"New filter Positions\n");
    for (j=0;j<no_people;j++){
        fprintf(stderr,"(%f,%f)\t",people_summary[j].mean.x,people_summary[j].mean.y);
    }

    if(no_people>0){    
        erlcm_point_t* test_people_pos = (erlcm_point_t *) 
            realloc(people_pos,no_people*sizeof(erlcm_point_t));
        carmen_test_alloc(test_people_pos);
        people_pos = test_people_pos;
    }

    int person_pos = 0;

    for(j=0;j<no_people;j++){
        people_pos[person_pos].x = people_summary[j].mean.x;
        people_pos[person_pos].y = people_summary[j].mean.y;
        people_pos[person_pos].z = 0.0;
        people_pos[person_pos].yaw = 0.0;
        people_pos[person_pos].pitch = 0.0;
        people_pos[person_pos].roll = people_summary[j].mean.theta;
        person_pos++;
    }

    //publishing a person removed robot and planar lidar message
    //if(is_front_laser){
    float distance_from_person = 0.0;
    static carmen_robot_laser_message* carmen_laser;
  
  
    if(carmen_laser == NULL){
        carmen_laser = (carmen_robot_laser_message *) malloc(sizeof(carmen_robot_laser_message));
        carmen_test_alloc(carmen_laser);
    }
    memcpy(carmen_laser,&carmen_robot_frontlaser, sizeof(carmen_robot_laser_message));
    if(carmen_laser->range==NULL){
        carmen_laser->range = (float *) malloc(carmen_robot_frontlaser.num_readings * sizeof(float));
        carmen_test_alloc(carmen_laser->range);
    }
  
    memcpy(carmen_laser->range, carmen_robot_frontlaser.range, 
           carmen_robot_frontlaser.num_readings * sizeof(float));
  
    double person_leg_range = 1.0;
    int removed_legs = 0;
    if(active_filter_ind >=0){  //if we are tracking a person -remove the observations attached to the person 
        for (i=0;i<meta_count;i++){
            distance_from_person = hypot(feet_obs[i].center_x - people_summary[active_filter_ind].mean.x,
                                         feet_obs[i].center_y - people_summary[active_filter_ind].mean.y);
            if(distance_from_person < person_leg_range){
                removed_legs++;
                int p;
                fprintf(stderr,"[%d:%d]\n",feet_obs[i].start_segment, feet_obs[i].end_segment);
                for(p = max(feet_obs[i].start_segment-2,0); p <=min(feet_obs[i].end_segment+2,carmen_robot_frontlaser.num_readings);p++){
                    carmen_laser->range[p] = 0.001;  //also might have implication on the max range conversion done in laser driver
                } 	  
            }
        }
        fprintf(stderr,"No of Observations Removed : %d\n", removed_legs);
    
        //remove any points within 0.3 
        for(int p = 0; p < carmen_robot_frontlaser.num_readings ;p++){
            distance_from_person = hypot(points[i].x - people_summary[active_filter_ind].mean.x,
                                         points[i].y - people_summary[active_filter_ind].mean.y);
            if(distance_from_person < CLEARING_RADIUS){
                carmen_laser->range[p] = 0.001;  	  
            }
        }
    }
    
    robot_laser_pub("PERSON_ROBOT_LASER",carmen_laser,lcm);
  
    publish_people_message();

    /*if(active_filter_ind >=0){//draw person if we have a person being tracked
    //bot_lcmgl_t *lcmgl = globals_get_lcmgl("person",1);
    lcmgl = globals_get_lcmgl("person",1);

    double pos[3];

    float person_x = people_summary[active_filter_ind].mean.x;
    float person_y = people_summary[active_filter_ind].mean.y;
    float theta = global_robot_pose.theta;

    float goal_x = global_robot_pose.x + person_x * cos(theta) - person_y*sin(theta);
    float goal_y = global_robot_pose.y + person_x * sin(theta) + person_y*cos(theta);

    pos[0] = goal_x;
    pos[1] = goal_y;
    pos[2] = 2.0;  

    if (1 && lcmgl) {//turn to 1 if u want to draw
    lcmglLineWidth (5);
    lcmglColor3f(1.0, .0, .0);
    lcmglCircle(pos, 0.15);
    }
    bot_lcmgl_switch_buffer (lcmgl);
    }*/

  
    if(no_people >=0){//draw person if we have a person being tracked
        //bot_lcmgl_t *lcmgl = globals_get_lcmgl("person",1);
        lcmgl = globals_get_lcmgl("person",1);

        double pos[3];

        for(i=0;i< no_people;i++){
            float person_x = people_summary[i].mean.x;
            float person_y = people_summary[i].mean.y;
            float theta = global_robot_pose.theta;

            float goal_x = global_robot_pose.x + person_x * cos(theta) - person_y*sin(theta);
            float goal_y = global_robot_pose.y + person_x * sin(theta) + person_y*cos(theta);

            pos[0] = goal_x;
            pos[1] = goal_y;
            pos[2] = 2.0;  

            if (1 && lcmgl) {//turn to 1 if u want to draw
                lcmglLineWidth (5);
                if(i==active_filter_ind){
                    lcmglColor3f(1.0, .0, .0);
                }
                else{
                    lcmglColor3f(.0, 1.0, 1.0);
                }
                lcmglCircle(pos, 0.15);
            }
        }
        bot_lcmgl_switch_buffer (lcmgl);
    }
  
    fprintf(stderr,"No of filters %d \n", no_people);
    fprintf(stderr,"------------------------------------------------------------\n");
    free(person_det);

    for (i=0;i<meta_count;i++){
        free(dist[i]);
    }
    free(dist);

    for (j=0;j<original_filter_size;j++){
        free(allocated_observations[j]);
    }
    free(allocated_observations);
    free(no_obs_count);
    free(allocated_to_filter);
    free(used_person_obs.locations);
    //free(prev_obs->locations);
    //memcpy(prev_obs->locations,used_person_obs.locations,used_person_obs.no_obs*sizeof(carmen_feet_seg)); 

    free(prev_points);
    prev_points = points;
    prev_no_points = no_points;
    //free(points);

    return 1;
}

int 
detect_person_both_multi_1(){ //this function is working  
    if((!have_rear_laser && !have_front_laser)){
        return -1;
    }
  
    //int front_laser indicates if this is the front laser - i.e. we remove person legs only from that 
    double time_obs = carmen_robot_frontlaser.timestamp; //carmen_get_time();  //time of the last observation

    carmen_point_t robot_pos = carmen_robot_frontlaser.robot_pose;
    int no_points = 0;
    carmen_point_p points = NULL;  //point array for the laser readings

    static carmen_point_p prev_points = NULL;
    static carmen_feet_observations* prev_obs; 
    static int first_obs = 0;
    if(prev_obs == NULL){//initialize the previous points
        prev_obs = (carmen_feet_observations *) malloc(sizeof(carmen_feet_observations));
        memset(prev_obs, 0, sizeof(prev_obs));
        prev_obs->locations = NULL;
    }
    static int prev_no_points = 0;

    carmen_point_t std;    
    std.x = 1.0;
    std.y = 1.0;
    std.theta = carmen_degrees_to_radians(4.0); 

    carmen_feet_observations person_obs;

    if(!have_rear_laser){
        points = (carmen_point_p)malloc(carmen_robot_frontlaser.num_readings *sizeof(carmen_point_t));  
        carmen_test_alloc(points);
        person_obs = detect_segments_both(&carmen_robot_frontlaser, param.front_laser_offset,
                                          NULL, param.rear_laser_offset, 
                                          0.1,points, &no_points); //runs segment detection - used at 0.2 
    
    }
    else{
        points = (carmen_point_p)malloc((carmen_robot_frontlaser.num_readings + carmen_robot_rearlaser.num_readings)*sizeof(carmen_point_t)); 
        carmen_test_alloc(points); 
        person_obs = detect_segments_both(&carmen_robot_frontlaser, param.front_laser_offset,
                                          &carmen_robot_rearlaser, param.rear_laser_offset, 
                                          0.1,points, &no_points); //runs segment detection - used at 0.2 
    }

    //call the new feet detector
    /*carmen_feet_observations moved_obs;
      get_moving_feet_segments(&person_obs, &moved_obs, prev_points, prev_no_points);
    */

    bot_lcmgl_t *  lcmgl = globals_get_lcmgl("leg_obs_prev",1);
    if (1 && lcmgl){
        if(first_obs){
            float rtheta = global_robot_pose.theta;
            double pos[3];
            lcmglColor3f(1.0, 1.0, 0);
            for (int i=0;i<prev_obs->no_obs;i++){//transformed prev locations
                float leg_x = global_robot_pose.x + prev_obs->locations[i].center_x * cos(rtheta) - 
                    prev_obs->locations[i].center_y * sin(rtheta);
                float leg_y = global_robot_pose.y + prev_obs->locations[i].center_x * sin(rtheta) + 
                    prev_obs->locations[i].center_y*cos(rtheta);
                pos[0] = leg_x;
                pos[1] = leg_y;
                pos[2] = 2.0;  
     
                lcmglLineWidth (5);
                //lcmglColor3f(1.0, 1.0, 0);
                lcmglCircle(pos, 0.1);
            }
        }
    } 
    bot_lcmgl_switch_buffer (lcmgl);
    
    carmen_feet_observations moved_obs;
    get_moving_feet_segments_1(&person_obs, prev_obs, &moved_obs, prev_points, prev_no_points);




    //need to add function door detection 
  
    //Merge the person's legs (i.e. two close by detections - in to one)
    //merge_legs(&person_obs);

    carmen_feet_observations used_person_obs;
  
    //Prune observations if they are in the same place as in the last frame - based on motion only
    //used_person_obs = prune_observations(&person_obs);
    used_person_obs = person_obs; //no pruning 
  
 
  
    int meta_count = used_person_obs.no_obs;
    carmen_feet_seg* feet_obs = used_person_obs.locations;
    int i,j;

    carmen_point_p person_det = (carmen_point_p) malloc(meta_count*sizeof(carmen_point_t)); 
    carmen_test_alloc(person_det);

    //copy the observations on to points
    for (i=0;i<meta_count;i++){
        person_det[i].x = feet_obs[i].center_x;
        person_det[i].y = feet_obs[i].center_y;
        person_det[i].theta = 0.0;			
    }

    //draw possible legs 
    lcmgl = globals_get_lcmgl("leg_obs",1);

    if (1 && lcmgl){// && is_front_laser){ //turn to 1 if u want to draw
        lcmglColor3f(0, 1.0, 0);
    
        float rtheta = global_robot_pose.theta;
        double pos[3];
        for (i=0;i<meta_count;i++){
            float leg_x = global_robot_pose.x + person_det[i].x * cos(rtheta) - person_det[i].y * sin(rtheta);
            float leg_y = global_robot_pose.y + person_det[i].x * sin(rtheta) + person_det[i].y*cos(rtheta);
            pos[0] = leg_x;
            pos[1] = leg_y;
            pos[2] = 2.0;  
     
            lcmglLineWidth (5);
            //lcmglColor3f(0, 1.0, 0);
            lcmglCircle(pos, 0.1);
        } 
    }
    bot_lcmgl_switch_buffer (lcmgl);





    lcmgl = globals_get_lcmgl("moved_leg_obs",1);

    if (1 && lcmgl){// && is_front_laser){ //turn to 1 if u want to draw
        float rtheta = global_robot_pose.theta;
        double pos[3];
        for (i=0;i<moved_obs.no_obs;i++){
            float leg_x = global_robot_pose.x + moved_obs.locations[i].center_x * cos(rtheta) - moved_obs.locations[i].center_y * sin(rtheta);
            float leg_y = global_robot_pose.y + moved_obs.locations[i].center_x * sin(rtheta) + moved_obs.locations[i].center_y*cos(rtheta);
            pos[0] = leg_x;
            pos[1] = leg_y;
            pos[2] = 2.0;  
     
            lcmglLineWidth (5);
            lcmglColor3f(1.0, 1.0, .0);
            lcmglCircle(pos, 0.1);
        } 
    } 

    bot_lcmgl_switch_buffer (lcmgl);

    //prune dead filters
    prune_filters(time_obs);  

    //Filter initiation code
    //create a filter for the observation that is not near any of the current filters

    carmen_point_t robot_position;
  
    robot_position.x = carmen_robot_frontlaser.robot_pose.x;
    robot_position.y = carmen_robot_frontlaser.robot_pose.y;
    robot_position.theta = carmen_robot_frontlaser.robot_pose.theta;

    //update the filter summaries based on the robot motion 
    for(j=0;j<no_people;j++){
        carmen_localize_update_filter_center(last_robot_position,robot_position,&people_summary[j].mean);
    }

    carmen_point_p moved_person = (carmen_point_p) malloc(moved_obs.no_obs*sizeof(carmen_point_t)); 
    carmen_test_alloc(moved_person);

    //copy the observations on to points
    for (int i=0;i< moved_obs.no_obs;i++){
        moved_person[i].x = moved_obs.locations[i].center_x;
        moved_person[i].y = moved_obs.locations[i].center_y;
        moved_person[i].theta = 0.0;
        //check if there are other filters near by
        double min_dist = 1000;
        for(j=0;j<no_people;j++){
            double temp_dist = hypot((people_summary[j].mean.x - moved_person[i].x),(people_summary[j].mean.y-moved_person[i].y));
            if(temp_dist < min_dist){
                min_dist = temp_dist;
            }
        }
        if(min_dist > 1.0){
            fprintf(stderr,"----------- Creating Filter\n");
            create_new_person(moved_person[i],std, time_obs);      
        }
    }

  
    double **dist;
    dist = (double **) malloc(meta_count*sizeof(double *));
    carmen_test_alloc(dist);
    //create a distance matrix between the filters and the observations
    for (i=0;i<meta_count;i++){
        dist[i] = (double *) malloc(no_people*sizeof(double));
        carmen_test_alloc(dist[i]);
        for(j=0;j<no_people;j++){
            dist[i][j] = hypot((people_summary[j].mean.x-person_det[i].x),(people_summary[j].mean.y-person_det[i].y));
        }
    }

    //allocate each observation to the closest person filter 
    int *no_obs_count = (int *) malloc(no_people*sizeof(int)); //no of observations that each filter can see
    carmen_test_alloc(no_obs_count);
    memset(no_obs_count,0,no_people*sizeof(int));

    int original_filter_size = no_people;
  
    int *allocated_to_filter = (int *) malloc(meta_count*sizeof(int)); //no of observations that each filter can see
    carmen_test_alloc(allocated_to_filter);
    memset(allocated_to_filter,0,meta_count*sizeof(int));
  
    int **allocated_observations = (int **) malloc(no_people*sizeof(int *)); //what observations are assigned to each filter
    carmen_test_alloc(allocated_observations);
    for (j=0;j<no_people;j++){
        allocated_observations[j] = (int *) malloc(meta_count*sizeof(int)); //what observations are assigned to each filter
        carmen_test_alloc(allocated_observations[j]);
        memset(allocated_observations[j],-1,meta_count*sizeof(int));
    }

    double filter_creation_angle = M_PI/12; //M_PI/12;
    double filter_creation_dist = 3.0;//3.0;

    for (i=0;i<meta_count;i++){  //if there is no person_filter near the observation - then allocate a new filter
        double min_dist = 0.4;//1.0;//worked also with 0.8
        int best_filter = -1;
    
        //search through all the person filters 
        for(j=0;j<no_people;j++){
            if(dist[i][j]<min_dist){
                min_dist = dist[i][j];
                best_filter = j;	
            }
        }
        if(best_filter >=0){//add current observation to the closest filter observation list
            int index = no_obs_count[best_filter];
            allocated_observations[best_filter][index]= i;
            no_obs_count[best_filter]++;
            allocated_to_filter[i]=1;
        }
    }
  
    for(j=0;j<no_people;j++){
        if(no_obs_count[j]==0){
            carmen_localize_run_person_no_obs(people_filter[j], robot_pos);
            carmen_localize_summarize_person(people_filter[j], &people_summary[j]);
        }
        else if(no_obs_count[j]==1){
            int obs = allocated_observations[j][0];
            carmen_localize_run_person_with_obs(people_filter[j], robot_pos,person_det[obs]);
            carmen_localize_summarize_person(people_filter[j], &people_summary[j]);
            time_since[j] = time_obs; 
        }

        else{//find the two observations that are closest to eachother 
            int k;
            double dist1 = 100;
            int obs1 = -1;
            int obs2 = -1;
            for(k=0;k<no_obs_count[j];k++){
                int obs = allocated_observations[j][k];
                if(dist[obs][j]<dist1){
                    dist1 = dist[obs][j];
                    obs1 = obs;
                }
            }

            double dist2 = 100;
            double temp_dist;
      
            for(k=0;k<no_obs_count[j];k++){
                int obs = allocated_observations[j][k];
                if(obs==obs1)
                    continue;
                else{
                    temp_dist = hypot((person_det[obs].x - person_det[obs1].x),(person_det[obs].y - person_det[obs1].y));
                    if(temp_dist < dist2){
                        dist2 = temp_dist;
                        obs2 = obs;
                    }
                }
            }

            if(dist2<0.01){//0.4){
                carmen_point_t mid_point;
                mid_point.x = (person_det[obs1].x + person_det[obs2].x)/2;
                mid_point.y = (person_det[obs1].y + person_det[obs2].y)/2;
                mid_point.theta = (person_det[obs1].theta + person_det[obs2].theta)/2; 
	
                carmen_localize_run_person_with_obs(people_filter[j], robot_pos, mid_point);
            }
            else{
                carmen_localize_run_person_with_obs(people_filter[j], robot_pos, person_det[obs1]);
            }
            carmen_localize_summarize_person(people_filter[j], &people_summary[j]);
            time_since[j] = time_obs;
        }    
    }
    if(active_filter_ind < 0){ //creating new filters only if there is no active filter - // we should prob change this so it creates only one filer 
        for (i=0;i<meta_count;i++){
            if(allocated_to_filter[i]==0){
                double angle = atan2(person_det[i].y,person_det[i].x);
                double dist = hypot(person_det[i].x,person_det[i].y);

                if (fabs(angle)< filter_creation_angle && dist < filter_creation_dist){
                    int near_filter = 0;
                    double min_dist = 1.0;
                    double temp_dist;
                    for(j=0;j<no_people;j++){
                        temp_dist = hypot((people_summary[j].mean.x-person_det[i].x),(people_summary[j].mean.y-person_det[i].y));
                        if(temp_dist< min_dist){
                            min_dist = temp_dist;
                            near_filter = 1;
                            break;
                        }
                    }
                    if(near_filter==0){//no nearby filter was created 
                        create_new_person(person_det[i],std, time_obs);
                    }
                }      
            }
        }
    }
    fprintf(stderr,"Current States => Tracking : %d, Following: %d\n",tracking_state, following_state);    

    if(tracking_state==LOOKING){//if we are in the tracking state we should reaquire 
        double min_dist = 4.0;
        double follow_angle = M_PI/12;
        double temp_dist;
        double temp_angle;
        fprintf(stderr,"Looking for person\n");
        for(j=0;j<no_people;j++){
            temp_dist = hypot(people_summary[j].mean.x,people_summary[j].mean.y);
            temp_angle = atan2(people_summary[j].mean.y,people_summary[j].mean.x);
            if((fabs(temp_angle) < follow_angle) && (temp_dist < min_dist)){
                min_dist = temp_dist;
                active_filter_ind = j;	
            }
        }
        //selects the closest observation with-in the angle cone 
        if(active_filter_ind >=0){
            update_tracking_state(FOUND);  //person located	
            fprintf(stderr,"====== New Person Following Activated =========\n");      
        }
        else{
            fprintf(stderr,"**** No good filter found **** \n");
        }
    }
    else{
        if(active_filter_ind>=0){
            fprintf(stderr,"== Current Person Followed is %d : (%f,%f)\n", active_filter_ind, 
                    people_summary[active_filter_ind].mean.x,people_summary[active_filter_ind].mean.y);
        }
        else{
            fprintf(stderr,"== No one being tracked \n");
        }
    }

    fprintf(stderr,"New filter Positions\n");
    for (j=0;j<no_people;j++){
        fprintf(stderr,"(%f,%f)\t",people_summary[j].mean.x,people_summary[j].mean.y);
    }

    if(no_people>0){    
        erlcm_point_t* test_people_pos = (erlcm_point_t *) 
            realloc(people_pos,no_people*sizeof(erlcm_point_t));
        carmen_test_alloc(test_people_pos);
        people_pos = test_people_pos;
    }

    int person_pos = 0;

    for(j=0;j<no_people;j++){
        people_pos[person_pos].x = people_summary[j].mean.x;
        people_pos[person_pos].y = people_summary[j].mean.y;
        people_pos[person_pos].z = 0.0;
        people_pos[person_pos].yaw = 0.0;
        people_pos[person_pos].pitch = 0.0;
        people_pos[person_pos].roll = people_summary[j].mean.theta;
        person_pos++;
    }

    //publishing a person removed robot and planar lidar message
    //if(is_front_laser){
    float distance_from_person = 0.0;
    static carmen_robot_laser_message* carmen_laser;
  
  
    if(carmen_laser == NULL){
        carmen_laser = (carmen_robot_laser_message *) malloc(sizeof(carmen_robot_laser_message));
        carmen_test_alloc(carmen_laser);
    }
    memcpy(carmen_laser,&carmen_robot_frontlaser, sizeof(carmen_robot_laser_message));
    if(carmen_laser->range==NULL){
        carmen_laser->range = (float *) malloc(carmen_robot_frontlaser.num_readings * sizeof(float));
        carmen_test_alloc(carmen_laser->range);
    }
  
    memcpy(carmen_laser->range, carmen_robot_frontlaser.range, 
           carmen_robot_frontlaser.num_readings * sizeof(float));
  
    double person_leg_range = 1.0;
    int removed_legs = 0;
    if(active_filter_ind >=0){  //if we are tracking a person -remove the observations attached to the person 
        for (i=0;i<meta_count;i++){
            distance_from_person = hypot(feet_obs[i].center_x - people_summary[active_filter_ind].mean.x,
                                         feet_obs[i].center_y - people_summary[active_filter_ind].mean.y);
            if(distance_from_person < person_leg_range){
                removed_legs++;
                int p;
                fprintf(stderr,"[%d:%d]\n",feet_obs[i].start_segment, feet_obs[i].end_segment);
                for(p = max(feet_obs[i].start_segment-2,0); p <=min(feet_obs[i].end_segment+2,carmen_robot_frontlaser.num_readings);p++){
                    carmen_laser->range[p] = 0.001;  //also might have implication on the max range conversion done in laser driver
                } 	  
            }
        }
        fprintf(stderr,"No of Observations Removed : %d\n", removed_legs);
    
        //remove any points within 0.3 
        for(int p = 0; p < carmen_robot_frontlaser.num_readings ;p++){
            distance_from_person = hypot(points[i].x - people_summary[active_filter_ind].mean.x,
                                         points[i].y - people_summary[active_filter_ind].mean.y);
            if(distance_from_person < CLEARING_RADIUS){
                carmen_laser->range[p] = 0.001;  	  
            }
        }
    }
    
    robot_laser_pub("PERSON_ROBOT_LASER",carmen_laser,lcm);
  
    publish_people_message();
  
    if(no_people >=0){//draw person if we have a person being tracked
        lcmgl = globals_get_lcmgl("person",1);

        double pos[3];

        for(i=0;i< no_people;i++){
            float person_x = people_summary[i].mean.x;
            float person_y = people_summary[i].mean.y;
            float theta = global_robot_pose.theta;

            float goal_x = global_robot_pose.x + person_x * cos(theta) - person_y*sin(theta);
            float goal_y = global_robot_pose.y + person_x * sin(theta) + person_y*cos(theta);

            pos[0] = goal_x;
            pos[1] = goal_y;
            pos[2] = 2.0;  

            if (1 && lcmgl) {//turn to 1 if u want to draw
                lcmglLineWidth (5);
                if(i==active_filter_ind){
                    lcmglColor3f(1.0, .0, .0);
                }
                else{
                    lcmglColor3f(.0, 1.0, 1.0);
                }
                lcmglCircle(pos, 0.15);
            }
        }
        bot_lcmgl_switch_buffer (lcmgl);
    }
  
    fprintf(stderr,"No of filters %d \n", no_people);
    fprintf(stderr,"------------------------------------------------------------\n");
    free(person_det);

    for (i=0;i<meta_count;i++){
        free(dist[i]);
    }
    free(dist);

    for (j=0;j<original_filter_size;j++){
        free(allocated_observations[j]);
    }
    free(allocated_observations);
    free(no_obs_count);
    free(allocated_to_filter);

    free(prev_points);
    prev_points = points;
    prev_no_points = no_points;
    if(first_obs ==0){
        first_obs =1;
    }
    return 1;
}

void lcm_robotlaser_to_carmen_laser(erlcm_robot_laser_t *msg, carmen_robot_laser_message *robot, double laser_offset)
{
    //convert the lcm message to a carmen message because all the functions have been 
    //already written to handle carmen messages

    robot->config.accuracy = .1;
    robot->config.laser_type = HOKUYO_UTM;
    robot->config.remission_mode = REMISSION_NONE;
    robot->config.angular_resolution = msg->laser.radstep;
    robot->config.fov = msg->laser.nranges * msg->laser.radstep;
    robot->config.maximum_range = 30; 
    robot->config.start_angle = msg->laser.rad0;
    robot->num_readings = msg->laser.nranges;
    robot->range = msg->laser.ranges;
    robot->robot_pose.x = msg->pose.pos[0];
    robot->robot_pose.y = msg->pose.pos[1];
    double rpy[3] = {0,0,0};  
    bot_quat_to_roll_pitch_yaw (msg->pose.orientation, rpy) ;
    robot->robot_pose.theta = rpy[2];
    robot->laser_pose.x = robot->robot_pose.x + laser_offset*cos(rpy[2]);
    robot->laser_pose.y = robot->robot_pose.y + laser_offset*sin(rpy[2]);
    robot->laser_pose.theta = rpy[2];
    robot->timestamp = msg->utime/1e6;
}

void lcm_planar_laser_to_carmen_laser(bot_core_planar_lidar_t *laser, carmen_robot_laser_message *robot, 
                                      double laser_offset, double laser_ang_offset)
{
    //convert the lcm message to a carmen message because all the functions have been 
    //already written to handle carmen messages

    robot->config.accuracy = .1;
    robot->config.laser_type = HOKUYO_UTM;
    robot->config.remission_mode = REMISSION_NONE;
    robot->config.angular_resolution = laser->radstep;
    robot->config.fov = laser->nranges * laser->radstep;
    robot->config.maximum_range = 30; 
    robot->config.start_angle = laser->rad0 + laser_ang_offset;
    robot->num_readings = laser->nranges;
    robot->range = laser->ranges;
    robot->robot_pose.x = global_robot_pose.x;
    robot->robot_pose.y = global_robot_pose.y;
    robot->robot_pose.theta = global_robot_pose.theta;
    robot->laser_pose.x = robot->robot_pose.x + laser_offset*cos(robot->robot_pose.theta);
    robot->laser_pose.y = robot->robot_pose.y + laser_offset*sin(robot->robot_pose.theta);
    robot->laser_pose.theta = robot->robot_pose.theta;
    robot->timestamp = laser->utime/1e6;
}

void speech_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const erlcm_speech_cmd_t * msg,
                    void * user  __attribute__((unused)))
{
    static erlcm_speech_cmd_t* staticmsg = NULL;

    if (staticmsg != NULL) {
		erlcm_speech_cmd_t_destroy(staticmsg);
    }
    staticmsg = erlcm_speech_cmd_t_copy(msg);
    char* cmd = staticmsg->cmd_type;
    char* property = staticmsg->cmd_property;

    //person tracker related speech commands
    if(strcmp(cmd,"TRACKER")==0){
        if(strcmp(property,"START_LOOKING")==0){
            //guide has asked us to look for him
            update_tracking_state(LOOKING);
            fprintf(stderr,"Starting to track\n");
        }else if(strcmp(property,"STATUS")==0){
            querry_tracking_state();
        }
    }
}

void lcm_frontlaser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const erlcm_robot_laser_t * msg,
                            void * user  __attribute__((unused)))
{
    static erlcm_robot_laser_t* staticmsg = NULL;

    if (staticmsg != NULL) {
        erlcm_robot_laser_t_destroy(staticmsg);
    }
    staticmsg = erlcm_robot_laser_t_copy(msg);

    lcm_robotlaser_to_carmen_laser(staticmsg,&carmen_robot_frontlaser, param.front_laser_offset);
    have_front_laser = 1;
  
    if(multi_people_tracking){
        detect_person_both_multi();
    }
    else{
        detect_person_both_basic();
    }
  
    last_robot_position.x = carmen_robot_frontlaser.robot_pose.x;
    last_robot_position.y = carmen_robot_frontlaser.robot_pose.y;
    last_robot_position.theta = carmen_robot_frontlaser.robot_pose.theta;
}

void lcm_rearlaser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const erlcm_robot_laser_t * msg,
                           void * user  __attribute__((unused)))
{
    static erlcm_robot_laser_t* staticmsg = NULL;

    if (staticmsg != NULL) {
        erlcm_robot_laser_t_destroy(staticmsg);
    }
    staticmsg = erlcm_robot_laser_t_copy(msg);

    //we only add update the rear laser data - we only run the localize once 
    lcm_robotlaser_to_carmen_laser(staticmsg,&carmen_robot_rearlaser, param.rear_laser_offset);
    have_rear_laser = 1;
}

void lcm_frontplanar_laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                                   const bot_core_planar_lidar_t * msg,
                                   void * user  __attribute__((unused)))
{
    static bot_core_planar_lidar_t* staticmsg = NULL;

    if (staticmsg != NULL) {
        bot_core_planar_lidar_t_destroy(staticmsg);
    }
    staticmsg = bot_core_planar_lidar_t_copy(msg);

    lcm_planar_laser_to_carmen_laser(staticmsg,&carmen_robot_frontlaser, param.front_laser_offset, param.front_laser_angle_offset);
    have_front_laser = 1;
  
    //detect_person_both();
    if(multi_people_tracking){
        detect_person_both_multi();
    }
    else{
        detect_person_both_basic();
    }
  
    last_robot_position.x = carmen_robot_frontlaser.robot_pose.x;
    last_robot_position.y = carmen_robot_frontlaser.robot_pose.y;
    last_robot_position.theta = carmen_robot_frontlaser.robot_pose.theta;
}

void lcm_rearplanar_laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                                  const bot_core_planar_lidar_t * msg,
                                  void * user  __attribute__((unused)))
{
    static bot_core_planar_lidar_t* staticmsg = NULL;

    if (staticmsg != NULL) {
        bot_core_planar_lidar_t_destroy(staticmsg);
    }
    staticmsg = bot_core_planar_lidar_t_copy(msg);

    lcm_planar_laser_to_carmen_laser(staticmsg,&carmen_robot_rearlaser, param.rear_laser_offset, param.rear_laser_angle_offset);
    have_rear_laser = 1;
}



static void pose_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                         const bot_core_pose_t * msg, void * user  __attribute__((unused))) 
{
    static bot_core_pose_t *prevUpdate = NULL;

    if (prevUpdate == NULL) {
        prevUpdate = bot_core_pose_t_copy(msg);
    }

    double rpy[3];
    bot_quat_to_roll_pitch_yaw(msg->orientation, rpy);
  
    global_robot_pose.x = msg->pos[0];
    global_robot_pose.y = msg->pos[1];
    global_robot_pose.theta = rpy[2];

    fprintf(stderr,"Pos (x,y,theta) : (%f,%f,%f)\n",
            msg->pos[0],msg->pos[1],rpy[2]);    
}

void read_parameters(carmen3d_localize_param_p param)
{
    BotConf *c = globals_get_config();
    char key[2048];
    int no_particles = 0;
    sprintf(key, "%s.no_particles", "person_tracking");

    //no particles 
    if (0 != bot_conf_get_int(c, key, &no_particles)){
        fprintf(stderr,"\tError Reading Params\n");
    }else{
        fprintf(stderr,"\tNo of Particles : %d\n",no_particles);
    }
    param->num_particles = no_particles;
    
    //laser params
    double position[3];
    sprintf(key, "%s.%s.%s.position", "calibration", "planar_lidars", "SKIRT_FRONT");
    if(3 != bot_conf_get_double_array(c, key, position, 3)){
        fprintf(stderr,"\tError Reading Params\n");
    }else{
        fprintf(stderr,"\tFront Laser Pos : (%f,%f,%f)\n",position[0], position[1], position[2]);
    }
    param->front_laser_offset = position[0];
    
    sprintf(key, "%s.%s.%s.position", "calibration", "planar_lidars", "SKIRT_REAR");
    if(3 != bot_conf_get_double_array(c, key, position, 3)){
        fprintf(stderr,"\tError Reading Params\n");
    }else{
        fprintf(stderr,"\tRear Laser Pos : (%f,%f,%f)\n",position[0], position[1], position[2]);
    }
    param->rear_laser_offset = position[0];


    double rpy[3];
    sprintf(key, "%s.%s.%s.rpy", "calibration", "planar_lidars", "SKIRT_FRONT");
    if(3 != bot_conf_get_double_array(c, key, rpy, 3)){
        fprintf(stderr,"\tError Reading Params\n");
    }else{
        fprintf(stderr,"\tFront Laser RPY : (%f,%f,%f)\n",rpy[0], rpy[1], rpy[2]);
    }
    param->front_laser_angle_offset  = carmen_degrees_to_radians(rpy[2]);
  
    sprintf(key, "%s.%s.%s.rpy", "calibration", "planar_lidars", "SKIRT_REAR");
    if(3 != bot_conf_get_double_array(c, key, rpy, 3)){
        fprintf(stderr,"\tError Reading Params\n");
    }else{
        fprintf(stderr,"\tRear Laser RPY : (%f,%f,%f)\n",rpy[0], rpy[1], rpy[2]);
    }
    param->rear_laser_angle_offset = carmen_degrees_to_radians(rpy[2]);
}

void shutdown_tracker(int x)
{
    if(x == SIGINT) {
        carmen_verbose("Disconnecting from IPC network.\n");
        exit(1);
    }
}

int main(int argc, char **argv)
{
    State s;

    // so that redirected stdout won't be insanely buffered.
    setlinebuf(stdout);

    memset (&s, 0, sizeof (State));

    int c;
    int use_planar_laser = 0;  

    while ((c = getopt (argc, argv, "lmhd")) >= 0) {
        switch (c) {
        case 'l':
            use_planar_laser = 1;
            break;
        case 'm':
            s.multi_people_tracking = 1;
            break;
        case 'd':
            s.debug_mode = 1;
            fprintf(stderr,"Debug");
            break;      
        case 'h':
        case '?':
            fprintf (stderr, "Usage: %s [-l]\n\
                        Options:\n			\
                        -l     Use Planar Laser \n"
                     , argv[0]);
            return 1;
        }
    }

    memset(&s.carmen_robot_frontlaser,0,sizeof(carmen_robot_frontlaser));
    memset(&s.carmen_robot_rearlaser,0,sizeof(carmen_robot_rearlaser));

    /* Setup exit handler */
    signal(SIGINT, shutdown_tracker);

    read_parameters(&param);

    fprintf(stderr,"Parameters No of Particles : %d \n",s.param.num_particles);
  
    memset(&s.global_person_position, 0, 3*sizeof(double));
  
    /*s.global_person_position.x = 0.0;
      s.global_person_position.y = 0.0;
      s.global_person_position.theta = 0.0;*/

    initialize_people_filters(actual_filter_size, &s);
  
    s.global_robot_pose.x = 0.0;
    s.global_robot_pose.y = 0.0;
    s.global_robot_pose.theta = 0.0;

    lcm = globals_get_lcm();

    fprintf(stderr,"Subscribing to LCM Laser\n");

    if(use_planar_laser){
        bot_core_planar_lidar_t_subscribe(lcm,"SKIRT_FRONT", lcm_frontplanar_laser_handler, &s);
        bot_core_planar_lidar_t_subscribe(lcm,"SKIRT_FRONT", lcm_frontplanar_laser_handler, &s);
        bot_core_planar_lidar_t_subscribe(lcm,"SKIRT_REAR", lcm_rearplanar_laser_handler, &s);
    }
    else{
        erlcm_robot_laser_t_subscribe(lcm,"REAR_ROBOT_LASER", lcm_rearlaser_handler, &s);
        erlcm_robot_laser_t_subscribe(lcm,"ROBOT_LASER", lcm_frontlaser_handler, &s);
    }
    bot_core_pose_t_subscribe(lcm, POSE_CHANNEL, pose_handler, &s);
    erlcm_multi_gridmap_t_subscribe(self->lc, "MULTI_FLOOR_MAPS", multi_gridmap_handler, &s);  
  
    //prob should handle only the tracker related stuff here
    erlcm_speech_cmd_t_subscribe(lcm, "PERSON_TRACKER", speech_handler, &s);
  
    //for messages sent from the N810
    erlcm_speech_cmd_t_subscribe(lcm, "TABLET_FOLLOWER", speech_handler, NULL);

    while (1)
        lcm_handle (lcm);

    return 0;
}
