#ifndef _HIGH_TRACKER
#define _HIGH_TRACKER
#endif

#include <unistd.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif


#include <gsl/gsl_fit.h>
#include <image_utils/pixels.h>
#include <image_utils/jpeg.h>
/* global variables */

//LCM stuff
#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <bot_vis/bot_vis.h>
//bot param stuff
#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>

#include <lcmtypes/er_lcmtypes.h>
#include <er_lcmtypes/lcm_channel_names.h>


#include <interfaces/map3d_interface.h>
#include <er_carmen/carmen.h>
#include <interfaces/localize3d_messages.h>


#include "person_messages.h"
#include "person_interface.h"
#include "person_localizecore.h"

#define REALLOCATE_TIME 1.0  //no of seconds to wait to reallocate
#define CLEARING_RADIUS 0.3 //radius to clear around person estimate
#define SEGMENT_GAP 0.1//0.06//0.03 //gap used in deciding segments 
#define PERSON_OBS_RADIUS 0.4
#define MAX_PERSON_HISTORY 60//40//40 //20//80

#define MAX_REL_PERSON_HISTORY 10

enum FollowingState{
  FOLLOWING, //person is being tracked and followed - issuing navigation commands to the navigator 
  IDLE //not following (can still have a track on the person)
};

enum TrackingState{
  FOUND, //person has been found
  LOOKING, //looking for person (should look for a particular time and give an error message if we cant find the person
  NOT_LOOKING,//system is initialized and has no track on the person 
  LOST // system has lost track of the person 
};

typedef struct _point_avg_object{
  carmen_point_t *history;
  carmen_point_t *temp_history;
  int max_size;
  int current_size;
} point_avg_object;

typedef struct _double_avg_object{
  double *history;
  double *temp_history;
  int max_size;
  int current_size;
} double_avg_object;

typedef struct _person_cam_renderer {
  bot_core_image_t *last_image;
  BotGlTexture *texture;  
  uint8_t *uncompresed_buffer;
  int uncompressed_buffer_size;
  int width, height;
} person_cam_renderer_t;

typedef struct{
  erlcm_point_t local_pos;
  erlcm_point_t g_pos;
  double rel_vel;
  carmen3d_localize_particle_filter_p people_filter;
  carmen3d_localize_summary_p people_summary;
  double time_since;

  point_avg_object relative_person_history;
  point_avg_object global_person_history; //larger history kept to extrapolate heading 
  point_avg_object global_person_history_small; //smaller history - kept for actual velocity 
  double_avg_object person_heading_history;
} person_track_t;

typedef struct{
  person_track_t *people; 
  int no_people; 
  int active_filter_ind; 
} people_list_t; 

typedef struct {
  lcm_t * lcm;
  BotParam *b_server; 

  int tourguide_mode;
  int use_classifier;

  carmen3d_localize_param_t param;
  carmen3d_localize_particle_filter_p filter;
  carmen3d_localize_summary_t summary;

  erlcm_map_t current_map;

  //  people_list_t people_list; //clean up - we will do this later 
  


  //-------------------- Should be moved in to the person track-------------
  carmen3d_localize_particle_filter_p *people_filter;
  carmen3d_localize_summary_p people_summary;
  double *time_since;
  carmen_point_p people_positions;
  erlcm_point_t *people_pos;
  erlcm_point_t guide_pos;
  erlcm_point_t guide_gpos;
  double rel_vel;
  int no_people;
  int actual_filter_size;
  int summary_active;
  int first_person_obs;
  int active_filter_ind;
    
  //-------------------------------------------------------------------------

  enum FollowingState following_state;  
  enum TrackingState tracking_state;
  double started_looking_time;  

  int use_planar_laser; 
  int multi_people_tracking;
  int use_map;

  //laser parameters - ideally this should be done for all relavent lasers 
  carmen_point_t std;    

  int new_person_detection;
  int first_person_detection;
  int clear_rearlaser;

  int debug_mode;

  double lcm_count;
  double ipc_count;

  int high_laser;
  int pruning_mode; 

  laser_offset_t front_laser_offset;
  laser_offset_t rear_laser_offset;

  carmen_robot_laser_message carmen_robot_frontlaser;
  carmen_robot_laser_message carmen_robot_rearlaser;

  erlcm_robot_laser_t *robot_fl;
  erlcm_robot_laser_t *robot_rl;

  bot_core_planar_lidar_t *planar_fl;
  bot_core_planar_lidar_t *planar_rl;
  

  erlcm_raw_odometry_msg_t* odom_msg;

  int have_front_laser;
  int have_rear_laser;

  carmen_point_t last_robot_position;
  carmen_point_t person_position;

  //history objects 
  point_avg_object relative_person_history;
  point_avg_object global_person_history; //larger history kept to extrapolate heading 
  point_avg_object global_person_history_small; //smaller history - kept for actual velocity 
  point_avg_object robot_history; //smaller history - kept for actual velocity 
  double_avg_object person_heading_history;

  carmen_point_t global_robot_pose; 

  person_cam_renderer_t *cr;

  bot_lcmgl_t *lcmgl_moved_legs;
  bot_lcmgl_t *lcmgl_leg_obs;
  bot_lcmgl_t *lcmgl_act_pos;
  bot_lcmgl_t *lcmgl_person_history;
  bot_lcmgl_t *lcmgl_velocity; 
  bot_lcmgl_t *lcmgl_person_zone;
  bot_lcmgl_t *lcmgl_person;
  bot_lcmgl_t *lcmgl_person_img; 
} state_t;


/*
  int tourguide_mode = 1;
  int use_classifier = 1;
  carmen3d_localize_param_t param;
  carmen3d_localize_particle_filter_p filter;
  carmen3d_localize_summary_t summary;

  carmen3d_map_t current_map;// = NULL;

  carmen3d_localize_particle_filter_p *people_filter = NULL;
  carmen3d_localize_summary_p people_summary = NULL;
  double *time_since = NULL;
  carmen_point_p people_positions = NULL;
  erlcm_point_t *people_pos = NULL;
  erlcm_point_t guide_pos;
  erlcm_point_t guide_gpos;
  double rel_vel = 0;

  carmen_point_t std;    
  erlcm_raw_odometry_msg_t* odom_msg;

  int multi_people_tracking = 0;
  int use_map = 0;
  int no_people = 0;
  int actual_filter_size = 5;

  int new_person_detection = 0;
  int first_person_detection = 0;
  int summary_active = 0;
  int first_person_obs = 0;
  int active_filter_ind = -1;
  int clear_rearlaser= 1;

  int debug_mode = 0;

  double lcm_count = 0;
  double ipc_count = 0;

  int high_laser= 1;

  laser_offset_t front_laser_offset;
  laser_offset_t rear_laser_offset;
  //carmen_robot_laser_message front_laser;
  carmen_robot_laser_message carmen_robot_frontlaser;
  carmen_robot_laser_message carmen_robot_rearlaser;
  int have_front_laser = 0;
  int have_rear_laser = 0;

  carmen_point_t last_robot_position;
  carmen_point_t person_position;

  //history objects 
  point_avg_object relative_person_history;
  point_avg_object global_person_history; //larger history kept to extrapolate heading 
  point_avg_object global_person_history_small; //smaller history - kept for actual velocity 
  point_avg_object robot_history; //smaller history - kept for actual velocity 
  double_avg_object person_heading_history;

  carmen_point_t new_person_position;
  carmen_person_test_message test_message;
  carmen_person_globalpos_message gpos_message;
  carmen_point_t global_person_position;

  carmen_point_t global_robot_pose; 

*/









static void
on_image (const lcm_recv_buf_t *rbuf, const char *channel,
	  const bot_core_image_t *msg, void *user_data)
{
  state_t *s = (state_t *)user_data; 
  fprintf(stderr,"Received\n");
  if(s->cr==NULL){
    s->cr = (person_cam_renderer_t*) calloc (1, sizeof (person_cam_renderer_t));

    s->cr->texture = NULL;
    s->cr->last_image = NULL;
    s->cr->uncompressed_buffer_size = msg->width * msg->height * 3;
    s->cr->uncompresed_buffer =
      (uint8_t*) malloc (s->cr->uncompressed_buffer_size);
  }

  
  
  if(s->cr->last_image !=NULL){
    bot_core_image_t_destroy(s->cr->last_image);
  }
  s->cr->last_image = bot_core_image_t_copy (msg);

  s->cr->width = s->cr->last_image->width;//(int)(cr->last_image->width/4.0);
  s->cr->height = s->cr->last_image->height; //(int)(cr->last_image->height/4.0);

  /*
    double xscale = cr->width / bot_camtrans_get_image_width(cr->camtrans);
    double yscale = cr->width / bot_camtrans_get_image_width(cr->camtrans);
    assert(fabs(xscale - yscale) < 1e-6);
    bot_camtrans_scale_image(cr->camtrans, xscale);
  */
  uint8_t *tex_src = NULL;
  int stride = 0;
  GLenum gl_format;

  bot_lcmgl_t *lcmgl = s->lcmgl_person_img; //bot_lcmgl_init(s->lcm, "person_img");//globals_get_lcmgl("person_img",1);

  if (s->cr->last_image->pixelformat == 0 ||
      s->cr->last_image->pixelformat == PIXEL_FORMAT_GRAY ||
      s->cr->last_image->pixelformat == PIXEL_FORMAT_BAYER_BGGR ||
      s->cr->last_image->pixelformat == PIXEL_FORMAT_BAYER_RGGB ||
      s->cr->last_image->pixelformat == PIXEL_FORMAT_BAYER_GRBG ||
      s->cr->last_image->pixelformat == PIXEL_FORMAT_BAYER_GBRG) {
    
    stride = s->cr->last_image->width;
    gl_format = GL_LUMINANCE;
    tex_src = s->cr->last_image->data;
  }
  else if (s->cr->last_image->pixelformat == PIXEL_FORMAT_MJPEG) {
    bot_core_image_t * msg = s->cr->last_image;
    
    // might need to JPEG decompress...
    stride = s->cr->last_image->width * 3;
    int buf_size = msg->height * stride;
    if (s->cr->uncompressed_buffer_size < buf_size) {
      s->cr->uncompresed_buffer =
	realloc (s->cr->uncompresed_buffer, buf_size);
      s->cr->uncompressed_buffer_size = buf_size;
    }
    jpeg_decompress_to_8u_rgb (msg->data, msg->size,
			       s->cr->uncompresed_buffer, msg->width, msg->height, stride);

    gl_format = GL_RGB;
    tex_src = s->cr->uncompresed_buffer;	  
    //fprintf(stderr,"Uncompressed\n");
  }
  else{
    fprintf(stderr, "Unhandled format\n");
    return;
  }

  lcmglColor3f(1.0, 1.0, 1.0);

  //need to scale this image - too large 

  
  int prev_gray_texid = bot_lcmgl_texture2d(lcmgl, tex_src,
					    s->cr->width, s->cr->height, stride,
					    BOT_LCMGL_RGB, BOT_LCMGL_COMPRESS_NONE);
  
  fprintf(stderr, "Width : %d Height : %d stride : %d\n", s->cr->width, s->cr->height, stride);

  bot_lcmgl_push_matrix(lcmgl);
  bot_lcmgl_translated(lcmgl, 0, s->cr->height + 10, 0);
  
  bot_lcmgl_texture_draw_quad(lcmgl, prev_gray_texid,
			      0, 0, 0,
			      0, s->cr->height, 0,
			      s->cr->width, s->cr->height, 0,
			      s->cr->width, 0, 0);

  /*
    bot_lcmgl_texture_draw_quad(bot_lcmgl_t *lcmgl, int texture_id,
    double x_top_left,  double y_top_left,  double z_top_left,
    double x_bot_left,  double y_bot_left,  double z_bot_left,
    double x_bot_right, double y_bot_right, double z_bot_right,
    double x_top_right, double y_top_right, double z_top_right)    
  */
  

  bot_lcmgl_pop_matrix(lcmgl);

  bot_lcmgl_switch_buffer (lcmgl);
}


//lcm message publishers
void publish_speech_msg(char* property, state_t *s)
{
  erlcm_speech_cmd_t msg;
  msg.utime = bot_timestamp_now();
  msg.cmd_type = "FOLLOWER";
  msg.cmd_property = property;
  erlcm_speech_cmd_t_publish(s->lcm, "PERSON_TRACKER", &msg);
}

void publish_speech_following_msg(char* property, state_t *s)
{
  erlcm_speech_cmd_t msg;
  msg.utime = bot_timestamp_now();
  msg.cmd_type = "FOLLOWER";
  msg.cmd_property = property;
  erlcm_speech_cmd_t_publish(s->lcm, "PERSON_TRACKER", &msg);
}

void publish_speech_tracking_msg(char* property, state_t *s)
{
  erlcm_speech_cmd_t msg;
  msg.utime = bot_timestamp_now();
  msg.cmd_type = "TRACKER";
  msg.cmd_property = property;
  erlcm_speech_cmd_t_publish(s->lcm, "PERSON_TRACKER", &msg);
}

//publishes both planar lidar (for the person navigator and robot_laser (for slam)
void robot_laser_pub(char * CHAN, carmen_robot_laser_message * robot, state_t *s)
{
  //fprintf(stderr,"F\n");
  erlcm_robot_laser_t msg;
  memset(&msg, 0, sizeof(msg));
  msg.laser.rad0 = robot->config.start_angle;
  msg.laser.radstep = robot->config.angular_resolution;
  msg.laser.nranges = robot->num_readings;
  msg.laser.ranges = robot->range;
  msg.pose.pos[0] = robot->robot_pose.x;
  msg.pose.pos[1] = robot->robot_pose.y;
  msg.pose.pos[2] = s->param.front_laser_offset; //this is added to the front laser - will cause an error in other laser configurations
  double rpy[3] = { 0, 0, robot->robot_pose.theta };
  bot_roll_pitch_yaw_to_quat(rpy, msg.pose.orientation);
  msg.utime = robot->timestamp * 1e6;
  msg.laser.utime = msg.utime;
  msg.pose.utime = msg.utime;
  msg.cov.x = 1;
  msg.cov.xy = 0;
  msg.cov.y = 1;
  msg.cov.yaw = .1;
  //bot_core_planar_lidar_t_publish(s->lcm,"HOKUYO_FORWARD",&msg.laser);
  erlcm_robot_laser_t_publish(s->lcm,CHAN,&msg);
  //fprintf(stderr,"L");
}

void init_point_avg_object(point_avg_object *object, int size){
  object->history = calloc(size,sizeof(carmen_point_t));
  object->temp_history = calloc(size,sizeof(carmen_point_t));
  object->max_size = size;
  object->current_size = 0;
}
void init_double_avg_object(double_avg_object *object, int size){
  object->history = calloc(size,sizeof(double));
  object->temp_history = calloc(size,sizeof(double));
  object->max_size = size;
  object->current_size = 0;
}

void reset_point_avg_object(point_avg_object *object){
  object->current_size = 0;
}
void reset_double_avg_object(double_avg_object *object){
  object->current_size = 0;
}

void add_point_to_history_object(point_avg_object *object,
				 carmen_point_t new_value){
  
  memcpy(object->temp_history, object->history, (object->max_size -1)*sizeof(carmen_point_t));
  memcpy(&(object->history[1]), object->temp_history, (object->max_size -1)*sizeof(carmen_point_t));
  object->history[0].x = new_value.x;
  object->history[0].y = new_value.y;
  object->history[0].theta = new_value.theta;
  object->current_size = fmin(++object->current_size, object->max_size);
}

void add_double_to_history_object(double_avg_object *object,
				  double new_value){
  
  memcpy(object->temp_history, object->history, (object->max_size -1)*sizeof(double));
  memcpy(&(object->history[1]), object->temp_history, (object->max_size -1)*sizeof(double));
  object->history[0] = new_value;
  object->current_size = fmin(++object->current_size, object->max_size);
}

carmen_point_t get_average_point_from_history_object(point_avg_object *object, int start_ind, int end_ind){
  carmen_point_t average = {.0,.0, .0};
    
  int his_len = (end_ind - start_ind) + 1;  
  
  //fprintf(stderr,"Point \n");
  if(his_len >0){
    for(int i=fmin(start_ind,object->max_size -1) ; i <= fmin(end_ind, object->max_size -1); i++){
      //fprintf(stderr,"[%d]=%f,%f\n", i, history[i].x, history[i].y);
      average.x += object->history[i].x;
      average.y += object->history[i].y;
      average.theta += object->history[i].theta;
    }
    average.x /= his_len;
    average.y /= his_len;
    average.theta /= his_len;    
  }

  return average; 
}

double get_average_double_from_history_object(double_avg_object *object, int start_ind, int end_ind){
  double average = .0;

  int his_len = (end_ind - start_ind) + 1;  
  
  //fprintf(stderr,"Point \n");
  if(his_len >0){
    for(int i=fmin(start_ind,object->max_size -1) ; i <= fmin(end_ind, object->max_size -1); i++){
      //fprintf(stderr,"[%d]=%f,%f\n", i, history[i].x, history[i].y);
      average += object->history[i];
    }
    average /= his_len;
  }

  return average; 
}

void get_bestfit_from_history_object(point_avg_object *object, int start_ind, int end_ind, double *c0, double *c1){
  carmen_point_t average = {.0,.0, .0};
    
  int his_len = (end_ind - start_ind) + 1;   

  //fprintf(stderr,"Point \n");
  if(his_len >1){
    double *x = calloc(his_len, sizeof(double));
    double *y = calloc(his_len, sizeof(double));

    double cov00, cov01, cov11, sumsq; //c0, c1, 

    for(int i=0;i< his_len; i++){
      x[i] = object->history[i+start_ind].x;
      y[i] = object->history[i+start_ind].y;
    }

    gsl_fit_linear (x,1, y,1, his_len, c0, c1, &cov00, &cov01, &cov11, &sumsq);
    free(x);
    free(y);
  }
}


void robot_rearlaser_pub(char * CHAN, carmen_robot_laser_message * robot, state_t *s)
{
  erlcm_robot_laser_t msg;
  memset(&msg, 0, sizeof(msg));
  msg.laser.rad0 = robot->config.start_angle;
  msg.laser.radstep = robot->config.angular_resolution;
  msg.laser.nranges = robot->num_readings;
  msg.laser.ranges = robot->range;
  msg.pose.pos[0] = robot->robot_pose.x;
  msg.pose.pos[1] = robot->robot_pose.y;
  msg.pose.pos[2] = s->param.rear_laser_offset; //this is added to the front laser - will cause an error in other laser configurations
  double rpy[3] = { 0, 0, robot->robot_pose.theta };
  bot_roll_pitch_yaw_to_quat(rpy, msg.pose.orientation);
  msg.utime = robot->timestamp * 1e6;
  msg.laser.utime = msg.utime;
  msg.pose.utime = msg.utime;
  msg.cov.x = 1;
  msg.cov.xy = 0;
  msg.cov.y = 1;
  msg.cov.yaw = .1;
  //bot_core_planar_lidar_t_publish(lcm,CHAN,&msg.laser);
  erlcm_robot_laser_t_publish(s->lcm,CHAN,&msg);
  //fprintf(stderr,"l");
}

void add_point_to_history(carmen_point_t *history, carmen_point_t *temp_history, carmen_point_t new_value){
  
  memcpy(temp_history, history, (MAX_PERSON_HISTORY-1)*sizeof(carmen_point_t));
  memcpy(&history[1], temp_history, (MAX_PERSON_HISTORY-1)*sizeof(carmen_point_t));
  history[0].x = new_value.x;
  history[0].y = new_value.y;
  history[0].theta = new_value.theta;
}

carmen_point_t get_average_point_from_history(carmen_point_t *history, int start_ind, int end_ind){
  carmen_point_t average = {.0,.0, .0};
    
  int his_len = (end_ind - start_ind) + 1;  
  
  //fprintf(stderr,"Point \n");
  if(his_len >0){
    for(int i=fmin(start_ind,MAX_PERSON_HISTORY-1) ; i <= fmin(end_ind, MAX_PERSON_HISTORY-1); i++){
      //fprintf(stderr,"[%d]=%f,%f\n", i, history[i].x, history[i].y);
      average.x += history[i].x;
      average.y += history[i].y;
      average.theta += history[i].theta;
    }
    average.x /= his_len;
    average.y /= his_len;
    average.theta /= his_len;    
  }

  return average; 
}

void get_bestfit_from_history(carmen_point_t *history, int start_ind, int end_ind, double *c0, double *c1){
  carmen_point_t average = {.0,.0, .0};
    
  int his_len = (end_ind - start_ind) + 1;   

  //fprintf(stderr,"Point \n");
  if(his_len >1){
    double *x = calloc(his_len, sizeof(double));
    double *y = calloc(his_len, sizeof(double));

    double cov00, cov01, cov11, sumsq; //c0, c1, 

    for(int i=0;i< his_len; i++){
      x[i] = history[i+start_ind].x;
      y[i] = history[i+start_ind].y;
    }

    gsl_fit_linear (x,1, y,1, his_len, c0, c1, &cov00, &cov01, &cov11, &sumsq);

  }
}

void add_double_to_history(double *history, double *temp_history, double new_value){
  
  memcpy(temp_history, history, (MAX_PERSON_HISTORY-1)*sizeof(double));
  memcpy(history+1, temp_history, (MAX_PERSON_HISTORY-1)*sizeof(double));
  history[0] = new_value;
}


double get_average_double_from_history(double *history, int start_ind, int end_ind){
  double average = .0;
    
  for(int i=fmin(start_ind,MAX_PERSON_HISTORY-1) ; i <= fmin(end_ind, MAX_PERSON_HISTORY-1); i++){
    //fprintf(stderr,"His : [%d] : %f\n", i, history[i]);
    average += history[i];
  }
  
  int his_len = (end_ind - start_ind) + 1;

  if(his_len >0){
    average /= his_len;
    return average;
  }
  else{
    return .0;
  }
}

void publish_mission_control(lcm_t *lc, int type){
  erlcm_mission_control_msg_t msg;
  msg.utime = bot_timestamp_now();
  msg.type = type;
  erlcm_mission_control_msg_t_publish(lc, MISSION_CONTROL_CHANNEL, &msg);
}

//State updates based on speech commands 


//tracking commands are not being handled by this - it is now being handled by the person navigator
void update_following_state(enum FollowingState new_state, state_t *s) {
  if(new_state==IDLE){//should stop the wheelchair
    if(s->following_state == FOLLOWING){
      //fprintf(stderr,"Stopping Wheelchair\n");
      //publish_mission_control(lcm, CARMEN3D_MISSION_CONTROL_MSG_T_NAVIGATOR_CLEAR_GOAL);
      //no need to say this - we already know that the chair has stopped
      //publish_speech_following_msg("STOPPED");
    }
    else{
      //fprintf(stderr,"Chair is already stopped\n");
    }

    s->following_state = new_state;
    //issue comand for the robot to stop - and the navigator to clear 
    //current goal - otherwise it will keep using the old goal 
  }
  else if(new_state==FOLLOWING){//check if we have a tracking of the person 
    if(s->tracking_state == FOUND){//if person is tracked the start following
      s->following_state = new_state;
      //fprintf(stderr,"Following Person\n");

      //why are we handling this in the tracker
      publish_speech_following_msg("FOLLOWING", s);
    }
    else{//we dont have an estimate of the person 
      s->following_state = IDLE;
      //fprintf(stderr,"Unable to Follow\n");
      //publish_speech_following_msg("UNABLE_TO_FOLLOW_LOST"); //there should also be additional reasons
      //- i.e. there is no path to follow the person - we should use this if navigator issues such a message
    }
  }
}

void update_tracking_state(enum TrackingState new_state, state_t *s){
  s->tracking_state = new_state;
  if(new_state==LOST){
    s->active_filter_ind = -1;
    publish_speech_tracking_msg("LOST", s);
    update_following_state(IDLE, s);
  }else if (new_state == FOUND){
    publish_speech_tracking_msg("FOUND", s);
    //we should not start following immediately - should follow only 
    //when we get a start following command
  }else if (new_state == LOOKING){
    //reset the person filer - maybe even clear up the current person filter??
    s->active_filter_ind = -1;
    publish_speech_tracking_msg("LOOKING", s); 
    update_following_state(IDLE, s); //we need to update the tracking state because this looking message may be 
    //generated becaue the guide wants the chair to reaquire him in the case of the chair following a wrong target
  }  
}

void querry_tracking_state(state_t *s){
  if(s->tracking_state== FOUND){
    publish_speech_tracking_msg("SEE_YES", s);
  }
  else if((s->tracking_state== LOST) ||(s->tracking_state== NOT_LOOKING)){
    publish_speech_tracking_msg("SEE_NO", s);
  }
  else if(s->tracking_state== LOOKING){
    publish_speech_tracking_msg("SEE_LOOKING", s);
  }    
}

void initialize_filter(carmen3d_localize_particle_filter_p pfilter,
		       carmen_point_t mean, carmen_point_t std)
{
  carmen_localize_initialize_particles_gaussian_person(pfilter,
						       mean,
						       std);
}

//published at the end of each laser message - for the person navigator
void publish_lcm_heading_goal(state_t *s){ 

  erlcm_goal_heading_msg_t msg;
  msg.utime = bot_timestamp_now();
  if(s->active_filter_ind !=-1){  //We have active person filter  
    float person_x = s->people_summary[s->active_filter_ind].mean.x;
    float person_y = s->people_summary[s->active_filter_ind].mean.y;
    //check condition to start following a person
    float act_dist = hypot(person_x,person_y);
    float act_angle = atan2(person_y,person_x);
    msg.target_heading = act_angle;
    if(act_dist >= 0.75){//heading restriction removed from here
      msg.tracking_state = 0; //0 - suitable for following 
      //fprintf(stderr, "Person in followable Location \n");
    }
    else{
      msg.tracking_state = 1;
      //fprintf(stderr,"Person within range - Not following");
    }
  }
  else{
    msg.target_heading = 0.0;
    msg.tracking_state = -1;
    //fprintf(stderr,"Person Location unknown");
  }
  erlcm_goal_heading_msg_t_publish(s->lcm, "GOAL_HEADING", &msg);
}


void publish_people_message(state_t *s)  //people message - carmen message published relative to the robot 
//- for every robot laser message
{
  /*carmen3d_people_pos_msg_t people_list;
    people_list.utime = bot_timestamp_now();
    people_list.num_people = s->no_people;
    people_list.followed_person = s->active_filter_ind;
  
    people_list.people_pos = (erlcm_point_t *) malloc(s->no_people*sizeof(erlcm_point_t));
    carmen_test_alloc(s->people_list.people_pos);
    memcpy(people_list.people_pos, s->people_pos, s->no_people*sizeof(erlcm_point_t));
  
    carmen3d_people_pos_msg_t_publish(lcm,"PEOPLE_LIST",&people_list);
    free(people_list.people_pos);

    static erlcm_guide_info_t guide_msg;
    memset(&guide_msg,0,sizeof(erlcm_guide_info_t));
    guide_msg.utime = people_list.utime; //this is wrong - need to be the observation time 

    if(active_filter_ind >=0){
    guide_msg.tracking_state = 1;    
    guide_msg.pos[0] = guide_pos.x;
    guide_msg.pos[1] = guide_pos.y;
    //guide_msg.rel_v = rel_vel;
    }
    erlcm_guide_info_t_publish(lcm,"GUIDE_POS",&guide_msg);  */
}

void get_global_point(double xy[2], double gxy[2], state_t *s){
  //fprintf(stderr,"Converting\n");
  double ctheta = cos(s->global_robot_pose.theta);
  double stheta = sin(s->global_robot_pose.theta);
  gxy[0] = s->global_robot_pose.x + xy[0] * ctheta - xy[1] * stheta;
  gxy[1] = s->global_robot_pose.y + xy[0] * stheta + xy[1] * ctheta;
}

void publish_guide_msg(double time, double tv_mag, double rv_mag, double heading, double rel_vel, state_t *s)  //people message - carmen message published relative to the robot 
//- for every robot laser message
{
  /*carmen3d_people_pos_msg_t people_list;
  people_list.utime = bot_timestamp_now();
  people_list.num_people = no_people;
  people_list.followed_person = active_filter_ind;
  
    people_list.people_pos = (erlcm_point_t *) malloc(no_people*sizeof(erlcm_point_t));
    carmen_test_alloc(people_list.people_pos);
    memcpy(people_list.people_pos,people_pos, no_people*sizeof(erlcm_point_t));
  
    carmen3d_people_pos_msg_t_publish(lcm,"PEOPLE_LIST",&people_list);
    free(people_list.people_pos);*/

  static erlcm_guide_info_t guide_msg;
  memset(&guide_msg,0,sizeof(erlcm_guide_info_t));
  guide_msg.utime = time * 1e6; //bot_timestamp_now();//people_list.utime; //this is wrong - need to be the observation time 

  if(s->active_filter_ind >=0){
    guide_msg.tracking_state = 1;    
    guide_msg.pos[0] = s->guide_pos.x;
    guide_msg.pos[1] = s->guide_pos.y;
    guide_msg.vel_mag = tv_mag;
    guide_msg.rot_vel_mag = rv_mag;
    guide_msg.person_heading = heading;
    guide_msg.closing_velocity = rel_vel;
  }
  erlcm_guide_info_t_publish(s->lcm,"GUIDE_POS",&guide_msg);  
}

int 
initialize_people_filters(state_t *s)
{
  fprintf(stderr,"Filters Initialized \n");
  s->people_filter = (carmen3d_localize_particle_filter_p *)
    malloc(s->actual_filter_size*sizeof(carmen3d_localize_particle_filter_p)); 
  carmen_test_alloc(s->people_filter);
  
  s->people_summary  = (carmen3d_localize_summary_p) 
    malloc(s->actual_filter_size*sizeof(carmen3d_localize_summary_t));
  carmen_test_alloc(s->people_summary);
    
  s->time_since = (double *) malloc(s->actual_filter_size*sizeof(double));
  carmen_test_alloc(s->time_since);
  return 0;
}



int create_new_person(carmen_point_t person_obs, carmen_point_t std, double time, state_t *s)  
//this is called once the filters that have observations have been updated so this 
//wont reallocate a filter that has an observation in this time period
{
  if(s->actual_filter_size <=s->no_people){//we dont have enough filters to allocate -increase by 5
    fprintf(stderr,"Increasing total no of filters");
    carmen3d_localize_particle_filter_p *temp_people_filter = (carmen3d_localize_particle_filter_p *)
      realloc(s->people_filter,(s->actual_filter_size+5)*sizeof(carmen3d_localize_particle_filter_p)); 
    carmen_test_alloc(temp_people_filter);
    s->people_filter = temp_people_filter;
    
    carmen3d_localize_summary_p temp_people_summary  = (carmen3d_localize_summary_p) 
      realloc(s->people_summary,(s->actual_filter_size+5)*sizeof(carmen3d_localize_summary_t));
    carmen_test_alloc(temp_people_summary);
    s->people_summary = temp_people_summary;
    
    double * temp_time_since = (double *) realloc(s->time_since, (s->actual_filter_size+5)*sizeof(double));
    carmen_test_alloc(temp_time_since);
    s->time_since = temp_time_since;
    s->actual_filter_size +=5;
    
  }
  s->people_filter[s->no_people] = carmen3d_localize_particle_filter_person_new(&(s->param)); //create a new filter 
  //this will be freed when that person is not being tracked
  initialize_filter(s->people_filter[s->no_people],person_obs,std);  
  carmen_localize_summarize_person(s->people_filter[s->no_people], &(s->people_summary[s->no_people]));
  s->time_since[s->no_people] = time;
  s->no_people++;
  
  return 0;
}

int 
prune_filters(double time_obs, state_t *s){
  int k,current_act_people = 0;

  int valid_filter_count = 0;
  
  for(int j=0;j<s->no_people;j++){//cycle through the array 
    double dist_from_chair = hypot((s->people_summary[j].mean.x),(s->people_summary[j].mean.y));
    
    //have a smaller realloc time for the non-following filters - would cut down on a lot of things 
    //also use the lower number of particles - i.e. move away from ipc param daemon
    
    if(((time_obs-s->time_since[j]) <= REALLOCATE_TIME) && ((j== s->active_filter_ind) || (dist_from_chair < 2.0))){
      valid_filter_count++;
    }
  }
  //if the current no_of filters is larger than the live filters, 
  //remove the dead filters and reorder the live ones
  if(s->no_people>valid_filter_count){//current_act_people) &(current_act_people>=0)){  
    int i=0,j;
  
    //allocate memory for the reduced filters 
    
    fprintf(stderr,"New filters created \n");
    carmen3d_localize_particle_filter_p *new_people_filter = (carmen3d_localize_particle_filter_p *)
      malloc(valid_filter_count*sizeof(carmen3d_localize_particle_filter_p)); 
    carmen_test_alloc(new_people_filter);
  
    carmen3d_localize_summary_p new_people_summary  = (carmen3d_localize_summary_p) 
      malloc(valid_filter_count*sizeof(carmen3d_localize_summary_t));
    carmen_test_alloc(new_people_summary);
    
    double *new_time_since = (double *) malloc(valid_filter_count*sizeof(double));
    carmen_test_alloc(new_time_since); 
    

    for(j=0;j<s->no_people;j++){//cycle through the array 
      double dist_from_chair = hypot((s->people_summary[j].mean.x),(s->people_summary[j].mean.y));
      if(((time_obs-s->time_since[j]) <= REALLOCATE_TIME) && ((j==s->active_filter_ind) || (dist_from_chair < 2.0))){
	memcpy(&new_people_filter[i],&(s->people_filter[j]), sizeof(carmen3d_localize_particle_filter_p));
	memcpy(&new_people_summary[i],&(s->people_summary[j]),sizeof(carmen3d_localize_summary_t));
	new_time_since[i] = s->time_since[j];
	if(j == s->active_filter_ind){//if the position of the active filter in the filter list is changed update the location
	  s->active_filter_ind = i;
	}
	//fprintf(stderr,"\n");
	i++;	
      }
      else{//free the unused filter
	if(j == s->active_filter_ind){//if the position of the active filter in the filter list is changed update the location
	  s->active_filter_ind = -1;  //-1 reflects the fact that there is no active filter
	  update_tracking_state(LOST, s);
	  fprintf(stderr,"+++++++ Person Lost +++++++ **** \n");
	  publish_speech_msg("LOST", s);
	}

	//fprintf(stderr,"==\tPruning Filter \n");
	free(s->people_filter[j]->particles); //free the filter 
	for(k = 0; k < s->people_filter[j]->param->num_particles; k++) {
	  free(s->people_filter[j]->temp_weights[k]);
	}
	free(s->people_filter[j]->temp_weights);
      }
    }
    free(s->people_filter);
    free(s->people_summary);
    free(s->time_since);

    s->people_filter = new_people_filter;
    s->people_summary = new_people_summary;
    s->time_since = new_time_since;

    //fprintf(stderr,"Reallocated Filters %d \n",i);
    s->no_people = valid_filter_count;  //update the new no of filters
    s->actual_filter_size = s->no_people;
  } 
  return 0;
}


//lcm handlers 
void mode_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
		  const char * channel __attribute__((unused)), 
		  const erlcm_tagged_node_t * msg,
		  void * user  __attribute__((unused)))
{
  state_t *s = (state_t *) user; 
  char* type = msg->type;
  char* new_mode = msg->label;
  if(!strcmp(type,"mode")){
    //mode has changed - 
    if(!strcmp(new_mode,"navigation")){//navigation mode - stop following guide
      s->tourguide_mode = 0;
      fprintf(stderr,"Navigation mode\n");
    }
    if(!strcmp(new_mode,"tourguide")){//tourguide mode
      s->tourguide_mode = 1;
      fprintf(stderr,"Tourguide mode\n");
    }
  }
}

int 
detect_person_both_multi(state_t *s){ 
  if((!s->have_rear_laser && !s->have_front_laser)){
    return -1;
  }
  return -1; 
}


int 
detect_person_both_basic(state_t *s){ 
  
  //have no laser messages - return 
  if((!s->have_rear_laser && !s->have_front_laser)){
    return -1;
  }
  if (s->current_map.map != NULL){
    fprintf(stderr,"Checking with Gridmap\n");
    fprintf(stderr,"Have map => Size (%d,%d)\n", s->current_map.config.x_size, s->current_map.config.y_size);
  }

  //velocities 
  double vel_x = .0, vel_y = .0;
  double rel_vel_x = .0, rel_vel_y = .0;
  double u_vel_x = .0, u_vel_y = .0;
  double person_vel_average = .0;
  double learning_rate = 0.3;
  double rot_vel_mag = 0.0;
  double approach_vel = .0;

  //heading 
  static double person_heading_average = .0;
  double best_fit_heading = 0.0;

  double time_obs = s->carmen_robot_frontlaser.timestamp; 
  carmen_point_t robot_pos = s->carmen_robot_frontlaser.robot_pose;

  bot_lcmgl_t *lcmgl;
  carmen_point_t robot_position;

  static double vel[2] = {.0,.0};
  static double last_person_pos[3] = {.0,.0,.0};
  static double prev_time = 0;
  robot_position.x = s->carmen_robot_frontlaser.robot_pose.x;
  robot_position.y = s->carmen_robot_frontlaser.robot_pose.y;
  robot_position.theta = s->carmen_robot_frontlaser.robot_pose.theta;

  //update the filter summaries based on the robot motion 
  for(int j=0;j<s->no_people;j++){
    carmen_localize_update_filter_center(s->last_robot_position,robot_position,&(s->people_summary[j].mean));
  }
  
  int no_points = 0;
  carmen_point_p points = NULL;  //point array for the laser readings

  static carmen_point_p prev_points = NULL;
  static int prev_no_points = 0;
  
  carmen_feet_observations person_obs;

  fprintf(stderr," At the start\n");

  if(!s->have_rear_laser){
    points = (carmen_point_p)malloc(s->carmen_robot_frontlaser.num_readings *sizeof(carmen_point_t));  
    carmen_test_alloc(points);
    if(s->active_filter_ind >=0){
      carmen_point_t person_loc;
      person_loc.x = s->people_summary[s->active_filter_ind].mean.x;
      person_loc.y = s->people_summary[s->active_filter_ind].mean.y;
      person_loc.theta = .0;
      person_obs = detect_segments_both_pruned(&(s->carmen_robot_frontlaser), s->front_laser_offset,
					       NULL, s->rear_laser_offset, 
					       SEGMENT_GAP,points, &no_points, &person_loc, s->high_laser, s->use_classifier);  
    }
    else{
      person_obs = detect_segments_both(&(s->carmen_robot_frontlaser), s->front_laser_offset,
					NULL, s->rear_laser_offset, 
					SEGMENT_GAP,points, &no_points, s->high_laser, s->use_classifier);  

      //call the new feet detector
      carmen_feet_observations moved_obs;
      get_moving_feet_segments(&person_obs, &moved_obs, prev_points, prev_no_points, NULL);

      fprintf(stderr," No of Moved Obs : %d\n", moved_obs.no_obs);
  
      lcmgl = s->lcmgl_moved_legs; //bot_lcmgl_init(s->lcm, "moved_leg_obs");//globals_get_lcmgl("moved_leg_obs",1);

      if (1 && lcmgl){// && is_front_laser){ //turn to 1 if u want to draw
	float rtheta = s->global_robot_pose.theta;
	double pos[3];
	for (int i=0;i<moved_obs.no_obs;i++){
	  float leg_x = s->global_robot_pose.x + moved_obs.locations[i].center_x * cos(rtheta) - moved_obs.locations[i].center_y * sin(rtheta);
	  float leg_y = s->global_robot_pose.y + moved_obs.locations[i].center_x * sin(rtheta) + moved_obs.locations[i].center_y*cos(rtheta);
	  pos[0] = leg_x;
	  pos[1] = leg_y;
	  pos[2] = 2.0;  
     
	  lcmglLineWidth (5);
	  lcmglColor3f(1.0, 1.0, .0);
	  lcmglCircle(pos, 0.1);
	} 
      } 

      bot_lcmgl_switch_buffer (lcmgl);

      carmen_point_p moved_person = (carmen_point_p) malloc(moved_obs.no_obs*sizeof(carmen_point_t)); 
      carmen_test_alloc(moved_person);
  
      //copy the observations on to points
      if(s->active_filter_ind==-1){
	for (int i=0;i< moved_obs.no_obs;i++){
	  moved_person[i].x = moved_obs.locations[i].center_x;
	  moved_person[i].y = moved_obs.locations[i].center_y;
	  moved_person[i].theta = 0.0;
	  //check if there are other filters near by
	  double min_dist = 1000;
	  for(int j=0;j<s->no_people;j++){
	    double temp_dist = hypot((s->people_summary[j].mean.x - moved_person[i].x),(s->people_summary[j].mean.y-moved_person[i].y));
	    if(temp_dist < min_dist){
	      min_dist = temp_dist;
	    }
	  }
	  if(min_dist < 0.8){
	    //fprintf(stderr,"Already tracking a person near - not adding new people\n");
	  }
	  else{   
	    //fprintf(stderr,"----------- Creating Filter\n");	     
	    create_new_person(moved_person[i],s->std, time_obs, s);
	  }
	}
      }
      free(moved_person);
    }
  }
  else{
    points = (carmen_point_p)malloc((s->carmen_robot_frontlaser.num_readings + s->carmen_robot_rearlaser.num_readings)*sizeof(carmen_point_t)); 
    carmen_test_alloc(points); 
    if(s->active_filter_ind >=0){
      carmen_point_t person_loc;
      person_loc.x = s->people_summary[s->active_filter_ind].mean.x;
      person_loc.y = s->people_summary[s->active_filter_ind].mean.y;
      person_loc.theta = .0;
      person_obs = detect_segments_both_pruned(&(s->carmen_robot_frontlaser), s->front_laser_offset,
					       &(s->carmen_robot_rearlaser), s->rear_laser_offset, 
					       SEGMENT_GAP,points, &no_points, &person_loc, s->high_laser, s->use_classifier); 
    }
    else{
      person_obs = detect_segments_both(&(s->carmen_robot_frontlaser), s->front_laser_offset,
					&(s->carmen_robot_rearlaser), s->rear_laser_offset, 
					SEGMENT_GAP,points, &no_points, s->high_laser, s->use_classifier); 

      //call the new feet detector
      carmen_feet_observations moved_obs;
      get_moving_feet_segments(&person_obs, &moved_obs, prev_points, prev_no_points, NULL);
  
      fprintf(stderr," No of Moved Obs : %d\n", moved_obs.no_obs);

      lcmgl = s->lcmgl_moved_legs;// bot_lcmgl_init(s->lcm,"moved_leg_obs"); //globals_get_lcmgl("moved_leg_obs",1);

      if (1 && lcmgl){// && is_front_laser){ //turn to 1 if u want to draw
	float rtheta = s->global_robot_pose.theta;
	double pos[3];
	for (int i=0;i<moved_obs.no_obs;i++){
	  float leg_x = s->global_robot_pose.x + moved_obs.locations[i].center_x * cos(rtheta) - moved_obs.locations[i].center_y * sin(rtheta);
	  float leg_y = s->global_robot_pose.y + moved_obs.locations[i].center_x * sin(rtheta) + moved_obs.locations[i].center_y*cos(rtheta);
	  pos[0] = leg_x;
	  pos[1] = leg_y;
	  pos[2] = 2.0;  
     
	  lcmglLineWidth (5);
	  lcmglColor3f(1.0, 1.0, .0);
	  lcmglCircle(pos, 0.1);
	} 
      } 

      bot_lcmgl_switch_buffer (lcmgl);

      carmen_point_p moved_person = (carmen_point_p) malloc(moved_obs.no_obs*sizeof(carmen_point_t)); 
      carmen_test_alloc(moved_person);
  
      //copy the observations on to points
      if(s->active_filter_ind==-1){
	for (int i=0;i< moved_obs.no_obs;i++){
	  moved_person[i].x = moved_obs.locations[i].center_x;
	  moved_person[i].y = moved_obs.locations[i].center_y;
	  moved_person[i].theta = 0.0;
	  //check if there are other filters near by
	  double min_dist = 1000;
	  for(int j=0;j<s->no_people;j++){
	    double temp_dist = hypot((s->people_summary[j].mean.x - moved_person[i].x),(s->people_summary[j].mean.y-moved_person[i].y));
	    if(temp_dist < min_dist){
	      min_dist = temp_dist;
	    }
	  }
	  if(min_dist < 0.8){
	    //fprintf(stderr,"Already tracking a person near - not adding new people\n");
	  }
	  else{   
	    //fprintf(stderr,"----------- Creating Filter\n");	     
	    create_new_person(moved_person[i],s->std, time_obs, s);
	  }
	}
      }
      free(moved_person);
    }
  }

  static carmen_point_t prev_velocity = {.0,.0,.0};
  carmen_localize_update_person_hostory(s->last_robot_position,robot_position,
					&prev_velocity, 1);


  carmen_feet_observations used_person_obs;
  
  //Prune observations if they are in the same place as in the last frame - based on motion only
  //used_person_obs = prune_observations(&person_obs);
  used_person_obs = person_obs; //no pruning 
  
 
  
  int full_count = used_person_obs.no_obs;
  carmen_feet_seg* feet_obs = used_person_obs.locations;
  int i,j;

  carmen_leg_t *person_det = (carmen_leg_t *) malloc(full_count*sizeof(carmen_leg_t)); 
  carmen_test_alloc(person_det);

  carmen_leg_t *full_person_det = (carmen_leg_t *) malloc(full_count*sizeof(carmen_leg_t)); 
  carmen_test_alloc(full_person_det);

  int meta_count = 0;

  if (s->current_map.map != NULL){
    //fprintf(stderr,"Checking with the map\n");
  }
  //fprintf(stderr,"Obs before pruning : %d\n", full_count);


  //copy the observations on to points
  for (i=0;i<full_count;i++){
    double l_leg_x = feet_obs[i].center_x;
    double l_leg_y = feet_obs[i].center_y;
    if(s->current_map.map != NULL){
      //fprintf(stderr,"Checking with the map\n");
      float rtheta = s->global_robot_pose.theta;
      float leg_x = s->global_robot_pose.x + l_leg_x * cos(rtheta) - l_leg_y * sin(rtheta);
      float leg_y = s->global_robot_pose.y + l_leg_x * sin(rtheta) + l_leg_y*cos(rtheta);

      carmen_point_t leg_pt;
      memset(&leg_pt,0,sizeof(carmen_point_t));
      leg_pt.x = leg_x;
      leg_pt.y = leg_y;
      int l_x, l_y;

      int occupied = 0;
	
      carmen3d_map3d_global_to_map_index_coordinates(leg_pt, s->current_map.midpt, s->current_map.map_zero,
						     s->current_map.config.resolution, &l_x, &l_y);

      int search_size = 1;
	
      for(int j=fmax(0,l_x - search_size); j<=fmin(l_x+search_size,s->current_map.config.x_size-1); j++){
	for(int k=fmax(0,l_y - search_size); k<=fmin(l_y+search_size,s->current_map.config.x_size-1); k++){
	  if(s->current_map.map[j][k] >0.7){
	    occupied = 1;
	    break;
	  }
	}
	if(occupied){
	  break;
	}
      }
      if(!occupied){
	person_det[meta_count].x = l_leg_x;
	person_det[meta_count].y = l_leg_y;
	person_det[meta_count].theta = 0.0;	
	person_det[meta_count].front_laser = feet_obs[i].front_laser;
	meta_count++;
      }
      full_person_det[i].x = l_leg_x;
      full_person_det[i].y = l_leg_y;
      full_person_det[i].theta = 0.0;
      full_person_det[i].front_laser = feet_obs[i].front_laser;
    }
    else{
      person_det[meta_count].x = l_leg_x; 
      person_det[meta_count].y = l_leg_y;
      person_det[meta_count].theta = 0.0;
      person_det[meta_count].front_laser = feet_obs[i].front_laser;
      meta_count++;

      full_person_det[i].x = l_leg_x;
      full_person_det[i].y = l_leg_y;
      full_person_det[i].theta = 0.0;
      full_person_det[i].front_laser = feet_obs[i].front_laser;
    }
  }
  
  person_det = (carmen_leg_t *) realloc(person_det,meta_count*sizeof(carmen_leg_t));
  //fprintf(stderr,"Obs after pruning : %d\n", meta_count);
  //draw possible legs 
  lcmgl = s->lcmgl_leg_obs;// bot_lcmgl_init(s->lcm,"leg_obs");// globals_get_lcmgl("leg_obs",1);

  if (1 && lcmgl){// && is_front_laser){ //turn to 1 if u want to draw
    lcmglColor3f(0, 1.0, 0);
    
    float rtheta = s->global_robot_pose.theta;
    double pos[3];
    for (i=0;i<meta_count;i++){
      float leg_x = s->global_robot_pose.x + person_det[i].x * cos(rtheta) - person_det[i].y * sin(rtheta);
      float leg_y = s->global_robot_pose.y + person_det[i].x * sin(rtheta) + person_det[i].y*cos(rtheta);
      pos[0] = leg_x;
      pos[1] = leg_y;
      pos[2] = 2.0; 

      lcmglColor3f(.0, 1.0, .0);    
      lcmglLineWidth (5);      
      lcmglCircle(pos, 0.1);
      if(s->debug_mode){
	char seg_info[512];
	sprintf(seg_info,"[%d]%d-%d:%d", feet_obs[i].front_laser, feet_obs[i].start_segment, feet_obs[i].end_segment, 
		feet_obs[i].start_segment - feet_obs[i].end_segment);
	bot_lcmgl_text(lcmgl, pos, seg_info);
      } 
    }
  } 

  bot_lcmgl_switch_buffer (lcmgl);  

  //prune dead filters
  prune_filters(time_obs, s);  

  //Filter initiation code
  //create a filter for the observation that is not near any of the current filters
  
  double **dist;
  dist = (double **) malloc(meta_count*sizeof(double *));
  carmen_test_alloc(dist);
  //create a distance matrix between the filters and the observations
  for (i=0;i<meta_count;i++){
    dist[i] = (double *) malloc(s->no_people*sizeof(double));
    carmen_test_alloc(dist[i]);
    for(j=0;j<s->no_people;j++){
      dist[i][j] = hypot((s->people_summary[j].mean.x-person_det[i].x),(s->people_summary[j].mean.y-person_det[i].y));
    }
  }

  //allocate each observation to the closest person filter 
  int *no_obs_count = (int *) malloc(s->no_people*sizeof(int)); //no of observations that each filter can see
  carmen_test_alloc(no_obs_count);
  memset(no_obs_count,0,s->no_people*sizeof(int));

  int original_filter_size = s->no_people;
  
  int *allocated_to_filter = (int *) malloc(meta_count*sizeof(int)); //no of observations that each filter can see
  carmen_test_alloc(allocated_to_filter);
  memset(allocated_to_filter,0,meta_count*sizeof(int));
  
  int **allocated_observations = (int **) malloc(s->no_people*sizeof(int *)); //what observations are assigned to each filter
  carmen_test_alloc(allocated_observations);
  for (j=0;j<s->no_people;j++){
    allocated_observations[j] = (int *) malloc(meta_count*sizeof(int)); //what observations are assigned to each filter
    carmen_test_alloc(allocated_observations[j]);
    memset(allocated_observations[j],-1,meta_count*sizeof(int));
  }

  double filter_creation_angle = M_PI/12; //M_PI/12;
  double filter_creation_dist = 3.0;//3.0;

  if(s->active_filter_ind >=0){//first do this for the tourguide 
    double dist_to_person = 10000.0;
    
    double person_angle = atan2(s->people_summary[s->active_filter_ind].mean.y, s->people_summary[s->active_filter_ind].mean.x);
    int use_front_laser = 0;
    
    //fprintf(stderr,"Person Angle : %f\n", person_angle * 180/M_PI);
    
    //63 degrees is the angle at which the rear laser hits the front laser's 90 degree side scans

    if(person_angle < M_PI/180*63 && person_angle >= -M_PI/180*63){
      //fprintf(stderr,"Using only the front laser observations");
      use_front_laser = 1;
    }
    else{
      //fprintf(stderr,"Using only the rear laser observations");
    }
    
    int ind = -1;
    int full_ind = -1;
    if(s->debug_mode){
      fprintf(stderr,"==============================================\n");
    }
    for (i=0;i<meta_count;i++){ //search through
      if(s->debug_mode){
	fprintf(stderr,"%d : %f\n",i, dist[i][s->active_filter_ind]);
      }
      
      if(person_det[i].front_laser != use_front_laser){
	continue;
      }
      if(dist[i][s->active_filter_ind] < PERSON_OBS_RADIUS){ //within the boundry
	allocated_to_filter[i]=1; //make all these observations out of bounds for the other filters
	if(s->debug_mode){
	  fprintf(stderr,"Found Observation : %d\n", i);
	}
	
	if(dist[i][s->active_filter_ind] < dist_to_person){
	  ind = i;
	  dist_to_person = dist[i][s->active_filter_ind];
	}
      }
    }
    if(ind ==-1){      
      //do a full search of these observations - through both laser observations
      for (i=0;i<meta_count;i++){ //search through	
	if(dist[i][s->active_filter_ind] < PERSON_OBS_RADIUS){ //within the boundry
	  allocated_to_filter[i]=1; //make all these observations out of bounds for the other filters
	  if(dist[i][s->active_filter_ind] < dist_to_person){
	    ind = i;
	    dist_to_person = dist[i][s->active_filter_ind];
	  }
	}	
      }
    }
    if(ind==-1 && (meta_count <full_count)){ //we still havent found a good observation
      //have not found a good observation yet
      //fprintf(stderr,"Looking through all observations\n");
      for (i=0;i<full_count;i++){ //search through
	if(full_person_det[i].front_laser != use_front_laser){
	  continue;
	}

	double temp_dist =  hypot((s->people_summary[s->active_filter_ind].mean.x - full_person_det[i].x),
				  (s->people_summary[s->active_filter_ind].mean.y - full_person_det[i].y));
	/*if(temp_dist < 0.4){ //within the boundry
	  double temp_dist_1 = hypot(full_person_det[i].x, full_person_det[i].y);
	  if(temp_dist_1 < dist_to_wheelchair){
	  full_ind = i;
	  dist_to_wheelchair = temp_dist_1;
	  }
	  }*/
	if(temp_dist < PERSON_OBS_RADIUS){ //within the boundry
	  if(temp_dist < dist_to_person){
	    full_ind = i;
	    dist_to_person = temp_dist;
	  }
	}

      }
    }
    
    //fprintf(stderr,"Ind : %d, Full Ind : %d\n",ind, full_ind);

    //if(no_obs_count[j]==0){
    if(ind==-1 && full_ind==-1){
      carmen_localize_run_person_no_obs(s->people_filter[s->active_filter_ind], robot_pos);
      carmen_localize_summarize_person(s->people_filter[s->active_filter_ind], &(s->people_summary[s->active_filter_ind]));
    }
    else if(ind>=0){//(no_obs_count[j]==1){
      int obs = ind;//allocated_observations[j][0];
      carmen_point_t assigned_obs;
      assigned_obs.x = person_det[obs].x;
      assigned_obs.y = person_det[obs].y;
      assigned_obs.theta = person_det[obs].theta;

      double min_leg_dist = 0.5;//used to be 0.5
      //the higher this is the more likely we will get merged to false leg observation if we get near enough

      int closest_leg = -1;

      //no merging as we do not get more than one observation per person 

      /*for (i=0;i<meta_count;i++){ //search through
	if(i==ind || (use_front_laser != person_det[i].front_laser)){
	continue;
	}
	double leg_dist = hypot(person_det[i].x - person_det[obs].x, person_det[i].y - person_det[obs].y);
	if(leg_dist < min_leg_dist){
	closest_leg = i;
	min_leg_dist = leg_dist;
	}
	}*/      
      
      if(closest_leg >=0){
	//fprintf(stderr,"Found a close leg Dist : %f\n", min_leg_dist);

	s->guide_pos.x = (person_det[obs].x + person_det[closest_leg].x)/2;
	s->guide_pos.y = (person_det[obs].y + person_det[closest_leg].y)/2;
	
      }
      else{
	s->guide_pos.x = person_det[obs].x;
	s->guide_pos.y = person_det[obs].y;
      }
      
      carmen_point_t person_position_obs;
      person_position_obs.x = s->guide_pos.x;
      person_position_obs.y = s->guide_pos.y;    
#ifdef PRINT_OP
      fprintf(stderr,"Active Filter Index : %d =  Valid Filter Count : %d\n", s->active_filter_ind, s->no_people);
#endif
      carmen_localize_run_person_with_obs(s->people_filter[s->active_filter_ind], robot_pos, assigned_obs);
      //this tends to mess up the tracking sometimes - not sure if the benifits of this outweigh the cost       
      //carmen_localize_run_person_with_obs(people_filter[active_filter_ind], robot_pos, person_position_obs);
      carmen_localize_summarize_person(s->people_filter[s->active_filter_ind], &(s->people_summary[s->active_filter_ind]));
      s->time_since[s->active_filter_ind] = time_obs; 

#ifdef PRINT_OP
      fprintf(stderr,"GUIDE pos : %f,%f\tFilter Pos : %f,%f\n", s->guide_pos.x, s->guide_pos.y, s->people_summary[s->active_filter_ind].mean.x, s->people_summary[s->active_filter_ind].mean.y);
#endif      
      s->guide_pos.x = s->people_summary[s->active_filter_ind].mean.x;
      s->guide_pos.y = s->people_summary[s->active_filter_ind].mean.y;

      float rtheta = s->global_robot_pose.theta;
      float leg_x = s->global_robot_pose.x + s->guide_pos.x * cos(rtheta) - s->guide_pos.y * sin(rtheta);
      float leg_y = s->global_robot_pose.y + s->guide_pos.x * sin(rtheta) + s->guide_pos.y*cos(rtheta);
      s->guide_gpos.x = leg_x;
      s->guide_gpos.y = leg_y;

      lcmgl = s->lcmgl_act_pos;// bot_lcmgl_init(s->lcm, "act_pos");//globals_get_lcmgl("act_pos",1);
      if(lcmgl){
	double pos[3];
	
	pos[0] = leg_x;
	pos[1] = leg_y;
	pos[2] = 2.0; 
	  
	lcmglColor3f(1.0, 0.5, .0);

	lcmglLineWidth (5);      
	lcmglCircle(pos, 0.15);
	  
	bot_lcmgl_switch_buffer (lcmgl);
      }
    }
    else if(full_ind>=0){//(no_obs_count[j]==1){
      int obs = full_ind;//allocated_observations[j][0];
      carmen_point_t assigned_obs;
      assigned_obs.x = full_person_det[obs].x;
      assigned_obs.y = full_person_det[obs].y;
      assigned_obs.theta = full_person_det[obs].theta;

      carmen_localize_run_person_with_obs(s->people_filter[s->active_filter_ind], robot_pos, assigned_obs);
      carmen_localize_summarize_person(s->people_filter[s->active_filter_ind], &(s->people_summary[s->active_filter_ind]));
      s->time_since[s->active_filter_ind] = time_obs; 
    }

    //person location has been updated    
  }

  
  for(j=0;j<s->no_people;j++){//for each filter search the close observations and pick the one closest to the wheelchair 
    if(j==s->active_filter_ind){
      continue;
    }
    double dist_to_wheelchair = 10000.0;
    int ind = -1;
    for (i=0;i<meta_count;i++){ //search through
      if(!allocated_to_filter[i] &&(dist[i][j] < 0.4)){ //within the boundry
	double temp_dist = hypot(person_det[i].x, person_det[i].y);
	if(temp_dist < dist_to_wheelchair){
	  ind = i;
	  dist_to_wheelchair = temp_dist;
	}
      }
    }
    //if(no_obs_count[j]==0){
    if(ind==-1){
      carmen_localize_run_person_no_obs(s->people_filter[j], robot_pos);
      carmen_localize_summarize_person(s->people_filter[j], &(s->people_summary[j]));
    }
    else if(ind>=0){//(no_obs_count[j]==1){
      allocated_to_filter[ind] = 1;
      int obs = ind;//allocated_observations[j][0];
      carmen_point_t assigned_obs;
      assigned_obs.x = person_det[obs].x;
      assigned_obs.y = person_det[obs].y;
      assigned_obs.theta = person_det[obs].theta;
      carmen_localize_run_person_with_obs(s->people_filter[j], robot_pos, assigned_obs);
      carmen_localize_summarize_person(s->people_filter[j], &(s->people_summary[j]));
      s->time_since[j] = time_obs; 
    }

  }

  if(s->active_filter_ind < 0){ //creating new filters only if there is no active filter - // we should prob change this so it creates only one filer 
    for (i=0;i<meta_count;i++){
      if(allocated_to_filter[i]==0){
	double angle = atan2(person_det[i].y,person_det[i].x);
	double dist = hypot(person_det[i].x,person_det[i].y);
	
	if (fabs(angle)< filter_creation_angle && dist < filter_creation_dist){
	  int near_filter = 0;
	  double min_dist = 1.0;
	  double temp_dist;
	  for(j=0;j<s->no_people;j++){
	    temp_dist = hypot((s->people_summary[j].mean.x-person_det[i].x),(s->people_summary[j].mean.y-person_det[i].y));
	    if(temp_dist< min_dist){
	      min_dist = temp_dist;
	      near_filter = 1;
	      break;
	    }
	  }
	  if(near_filter==0){//no nearby filter was created 
	    carmen_point_t assigned_obs;
	    assigned_obs.x = person_det[i].x;
	    assigned_obs.y = person_det[i].y;
	    assigned_obs.theta = person_det[i].theta;
	    create_new_person(assigned_obs,s->std, time_obs, s);
	  }
	}      
      }
    }
  }
  //fprintf(stderr,"Current States => Tracking : %d, Following: %d\n",tracking_state, following_state);    

  /*if(tracking_state==LOOKING){//if we are in the tracking state we should reaquire 
    double max_dist = 4.0;
    double too_close_dist = 0.5;
    //double min_dist = 0.5;
    double follow_angle = M_PI/6;
    double temp_dist;
    double temp_angle;
    fprintf(stderr,"Looking for person\n");

    double angle_offset = 10000;

    for(j=0;j<no_people;j++){
    temp_dist = hypot(people_summary[j].mean.x,people_summary[j].mean.y);
    temp_angle = atan2(people_summary[j].mean.y,people_summary[j].mean.x);
    if((fabs(temp_angle) < follow_angle) 
    && (temp_dist < max_dist) && (temp_dist > too_close_dist)){
    if(angle_offset > fabs(temp_angle)){
    angle_offset = fabs(temp_angle);
    active_filter_ind = j;	
    }
    }
    }
    
    if(active_filter_ind >=0){
    for(int k=0;k<no_people;k++){
    if(k != active_filter_ind){
    time_since[k] = .0;
    }
    }
    guide_pos.x = people_summary[active_filter_ind].mean.x;
    guide_pos.y = people_summary[active_filter_ind].mean.y;
    update_tracking_state(FOUND);  //person located	
    //fprintf(stderr,"====== New Person Following Activated =========\n");      
    }*/

  if(s->tracking_state==LOOKING){//if we are in the tracking state we should reaquire 
    //??make two passes - first check if we have an observations 
    double max_dist = 4.0;
    double too_close_dist = 0.5;
    //double min_dist = 0.5;
    double follow_angle = M_PI/6;
    double temp_dist;
    double temp_angle;
    fprintf(stderr,"Looking for person\n");

    //selects the closest observation with-in the angle cone 
    /*for(j=0;j<no_people;j++){
      temp_dist = hypot(people_summary[j].mean.x,people_summary[j].mean.y);
      temp_angle = atan2(people_summary[j].mean.y,people_summary[j].mean.x);
      if((fabs(temp_angle) < follow_angle) 
      && (temp_dist < max_dist) && (temp_dist > too_close_dist)){
	
	
      max_dist = temp_dist;
      active_filter_ind = j;		
      }
      }*/

    double angle_offset = 10000;

    for(j=0;j<s->no_people;j++){
      temp_dist = hypot(s->people_summary[j].mean.x,s->people_summary[j].mean.y);
      temp_angle = atan2(s->people_summary[j].mean.y,s->people_summary[j].mean.x);
      if((fabs(temp_angle) < follow_angle) 
	 && (temp_dist < max_dist) && (temp_dist > too_close_dist)){
	if(angle_offset > fabs(temp_angle)){
	  angle_offset = fabs(temp_angle);
	  s->active_filter_ind = j;	
	}
      }
    }
    
    if(s->active_filter_ind >=0){
      for(int k=0;k<s->no_people;k++){
	if(k != s->active_filter_ind){
	  s->time_since[k] = .0;
	}
      }
      s->guide_pos.x = s->people_summary[s->active_filter_ind].mean.x;
      s->guide_pos.y = s->people_summary[s->active_filter_ind].mean.y;
      update_tracking_state(FOUND,s);  //person located	
      //fprintf(stderr,"====== New Person Following Activated =========\n");      
    }
    else{
      //fprintf(stderr,"**** No good filter found **** \n");
    }
  }
  else{
    if(s->active_filter_ind>=0){
      s->guide_pos.x = s->people_summary[s->active_filter_ind].mean.x;
      s->guide_pos.y = s->people_summary[s->active_filter_ind].mean.y;
      //fprintf(stderr,"== Current Person Followed is %d : (%f,%f)\n", active_filter_ind, 
      //      people_summary[active_filter_ind].mean.x,people_summary[active_filter_ind].mean.y);
    }
    else{
      //fprintf(stderr,"== No one being tracked \n");
    }
  }

  //fprintf(stderr,"New filter Positions\n");
  for (j=0;j<s->no_people;j++){
    //fprintf(stderr,"(%f,%f)\t",people_summary[j].mean.x,people_summary[j].mean.y);
  }

  if(s->no_people>0){    
    erlcm_point_t* test_people_pos = (erlcm_point_t *) 
      realloc(s->people_pos,s->no_people*sizeof(erlcm_point_t));
    carmen_test_alloc(test_people_pos);
    s->people_pos = test_people_pos;
  }

  int person_pos = 0;

  for(j=0;j<s->no_people;j++){
    s->people_pos[person_pos].x = s->people_summary[j].mean.x;
    s->people_pos[person_pos].y = s->people_summary[j].mean.y;
    s->people_pos[person_pos].z = 0.0;
    s->people_pos[person_pos].yaw = 0.0;
    s->people_pos[person_pos].pitch = 0.0;
    s->people_pos[person_pos].roll = s->people_summary[j].mean.theta;
    person_pos++;
  }

  //publishing a person removed robot and planar lidar message
  float distance_from_person = 0.0;
  static carmen_robot_laser_message* carmen_laser;  
  
  if(carmen_laser == NULL){
    carmen_laser = (carmen_robot_laser_message *) malloc(sizeof(carmen_robot_laser_message));
    carmen_test_alloc(carmen_laser);
  }
  memcpy(carmen_laser,&(s->carmen_robot_frontlaser), sizeof(carmen_robot_laser_message));
  if(carmen_laser->range==NULL){
    carmen_laser->range = (float *) malloc(s->carmen_robot_frontlaser.num_readings * sizeof(float));
    carmen_test_alloc(carmen_laser->range);
  }
  
  memcpy(carmen_laser->range, s->carmen_robot_frontlaser.range, 
	 s->carmen_robot_frontlaser.num_readings * sizeof(float));

  double person_leg_range = 1.0;
  int removed_legs = 0;
  if(s->active_filter_ind >=0){  //if we are tracking a person -remove the observations attached to the person 
    //fprintf(stderr,"Person Location : %f,%f \n",people_summary[active_filter_ind].mean.x, people_summary[active_filter_ind].mean.y);
    for (i=0;i<used_person_obs.no_obs;i++){
      if(!feet_obs[i].front_laser){//this if the front laser - skipping rear laser stuff
	continue;
      }
      distance_from_person = hypot(feet_obs[i].center_x - s->people_summary[s->active_filter_ind].mean.x,
				   feet_obs[i].center_y - s->people_summary[s->active_filter_ind].mean.y);
      //fprintf(stderr,"\tDistance : %f\n", distance_from_person);
      if(distance_from_person < person_leg_range){
	removed_legs++;
	int p;
	//fprintf(stderr,"[%d:%d]\n",feet_obs[i].start_segment, feet_obs[i].end_segment);
	for(p = fmax(feet_obs[i].start_segment-3,0); p <=fmin(feet_obs[i].end_segment+3,s->carmen_robot_frontlaser.num_readings-1);p++){
	  carmen_laser->range[p] = 0.001;  //also might have implication on the max range conversion done in laser driver
	} 	  
      }
    }
#ifdef PRINT_OP
    fprintf(stderr,"No of Observations Removed : %d\n", removed_legs);
#endif
    //remove any points within 0.3 
    for(int p = 0; p < s->carmen_robot_frontlaser.num_readings ;p++){
      distance_from_person = hypot(points[p].x - s->people_summary[s->active_filter_ind].mean.x,
				   points[p].y - s->people_summary[s->active_filter_ind].mean.y);

      //fprintf(stderr,"\tDistance : %f\n", distance_from_person);
      if(distance_from_person < CLEARING_RADIUS){
	carmen_laser->range[p] = 0.001;  	  
      }
    }
  }
    
  //robot_laser_pub("PERSON_ROBOT_LASER",carmen_laser,lcm);

  //clearing the rear laser of person observations

  if(s->clear_rearlaser && s->have_rear_laser){
    static carmen_robot_laser_message* rear_carmen_laser;
    
    if(rear_carmen_laser == NULL){
      rear_carmen_laser = (carmen_robot_laser_message *) malloc(sizeof(carmen_robot_laser_message));
      carmen_test_alloc(rear_carmen_laser);
    }
    memcpy(rear_carmen_laser,&(s->carmen_robot_rearlaser), sizeof(carmen_robot_laser_message));
    if(rear_carmen_laser->range==NULL){
      rear_carmen_laser->range = (float *) malloc(s->carmen_robot_rearlaser.num_readings * sizeof(float));
      carmen_test_alloc(rear_carmen_laser->range);
    }
    
    memcpy(rear_carmen_laser->range, s->carmen_robot_rearlaser.range, 
	   s->carmen_robot_rearlaser.num_readings * sizeof(float));

    double person_leg_range = 1.0;
    int removed_legs = 0;
    if(s->active_filter_ind >=0){  //if we are tracking a person -remove the observations attached to the person 
      for (i=0;i<used_person_obs.no_obs;i++){
	if(feet_obs[i].front_laser){//this if the rear laser - skipping front laser stuff
	  continue;
	}
	distance_from_person = hypot(feet_obs[i].center_x - s->people_summary[s->active_filter_ind].mean.x,
				     feet_obs[i].center_y - s->people_summary[s->active_filter_ind].mean.y);
	//fprintf(stderr,"\tDistance : %f\n", distance_from_person);
	if(distance_from_person < person_leg_range){
	  removed_legs++;
	  int p;
	  //fprintf(stderr,"[%d:%d]\n",feet_obs[i].start_segment, feet_obs[i].end_segment);
	  for(p = fmax(feet_obs[i].start_segment-3,0); p <=fmin(feet_obs[i].end_segment+3,s->carmen_robot_rearlaser.num_readings-1);p++){
	    rear_carmen_laser->range[p] = 0.001;  //also might have implication on the max range conversion done in laser driver
	  } 	  
	}
      }
#ifdef PRINT_OP
      fprintf(stderr,"No of Observations Removed : %d\n", removed_legs);
#endif
      //remove any points within 0.3 
      for(int p = 0; p < s->carmen_robot_rearlaser.num_readings ;p++){
	distance_from_person = hypot(points[p].x - s->people_summary[s->active_filter_ind].mean.x,
				     points[p].y - s->people_summary[s->active_filter_ind].mean.y);

	//fprintf(stderr,"\tDistance : %f\n", distance_from_person);
	if(distance_from_person < CLEARING_RADIUS){
	  rear_carmen_laser->range[p] = 0.001;  	  
	}
      }
    }
    
    //robot_rearlaser_pub("PERSON_REAR_ROBOT_LASER",rear_carmen_laser,lcm);
  }
  
  if(s->active_filter_ind>=0){//we have a valid person location
    
    //update the global headings and the positions
    //NEW METHOD
    
    carmen_localize_update_person_hostory(s->last_robot_position,robot_position, s->robot_history.history, s->robot_history.current_size);
    carmen_localize_update_person_hostory(s->last_robot_position,robot_position, s->global_person_history.history, s->global_person_history.current_size);
    carmen_localize_update_person_hostory(s->last_robot_position,robot_position, s->global_person_history_small.history, s->global_person_history_small.current_size);

    //might need to do this to obtain the robot's averaged velocity and then calculate relative vel from that 
    
#ifdef PRINT_OP
    fprintf(stderr,"Updating Person global history (%f,%f)\n", guide_pos.x, guide_pos.y);    
#endif
    carmen_point_t g_pos = {s->guide_pos.x, s->guide_pos.y, time_obs};

    carmen_point_t zero_pos = {.0, .0, time_obs};

    //NEW METHOD
    add_point_to_history_object(&(s->global_person_history), g_pos);
    add_point_to_history_object(&(s->global_person_history_small), g_pos);
    add_point_to_history_object(&(s->relative_person_history), g_pos);
    add_point_to_history_object(&(s->robot_history), zero_pos);     
    
    //calculate relative velocity 
    carmen_point_t r_start_point = get_average_point_from_history_object(&(s->relative_person_history), 
									 0,
									 s->relative_person_history.current_size/2.0-1);

    /*get_average_point_from_history(person_history, 
      0,
      valid_history/2.0-1);*/

    //    carmen_point_t r_end_point = get_average_point_from_history(person_history, 
    carmen_point_t r_end_point = get_average_point_from_history_object(&(s->relative_person_history), 
								       s->relative_person_history.current_size/2.0,
								       s->relative_person_history.current_size-1);  
    

    double rel_vel_heading = 0;
    double rel_vel_mag = 0;

    double heading_to_person = atan2(s->guide_pos.y, s->guide_pos.x);

    if(r_start_point.theta - r_end_point.theta >0){      
      rel_vel_x = (r_start_point.x - r_end_point.x)/(r_start_point.theta - r_end_point.theta);
      rel_vel_y = (r_start_point.y - r_end_point.y)/(r_start_point.theta - r_end_point.theta);
      rel_vel_heading = atan2(rel_vel_y, rel_vel_x);
      rel_vel_mag = hypot(rel_vel_x, rel_vel_y);
    }
    else{
      rel_vel_x = 0;
      rel_vel_y = 0;
    }


    carmen_point_t robot_start_point = get_average_point_from_history_object(&(s->robot_history), 
									     0,
									     s->robot_history.current_size/2.0-1);

    /*get_average_point_from_history(person_history, 
      0,
      valid_history/2.0-1);*/

    //    carmen_point_t r_end_point = get_average_point_from_history(person_history, 
    carmen_point_t robot_end_point = get_average_point_from_history_object(&(s->robot_history), 
									   s->robot_history.current_size/2.0,
									   s->robot_history.current_size-1);  

    double robot_vel_x = .0, robot_vel_y = .0;
    double robot_vel_mag = .0;
    if(robot_start_point.theta - robot_end_point.theta >0){      
      robot_vel_x = (robot_start_point.x - robot_end_point.x)/(robot_start_point.theta - robot_end_point.theta);
      robot_vel_y = (robot_start_point.y - robot_end_point.y)/(robot_start_point.theta - robot_end_point.theta);
      //rel_vel_heading = atan2(rel_vel_y, rel_vel_x);
      robot_vel_mag = hypot(robot_vel_x, robot_vel_y);
    }
    else{
      robot_vel_x = 0;
      robot_vel_y = 0;
    }

    fprintf(stderr, "Robot Velocity : %f\n" , robot_vel_mag); 

    approach_vel = - rel_vel_mag * cos(rel_vel_heading - heading_to_person); 

    fprintf(stderr, "Approach Velocity : %f\n", approach_vel);

    //calculating velocity relative to ground truth
    //carmen_point_t start_point = get_average_point_from_history(person_history_global, 
    carmen_point_t start_point_h = get_average_point_from_history_object(&(s->global_person_history),
									 0,
									 s->global_person_history.current_size/2.0-1);

    //carmen_point_t end_point = get_average_point_from_history(person_history_global, 
    carmen_point_t end_point_h = get_average_point_from_history_object(&(s->global_person_history),
								       s->global_person_history.current_size/2.0,
								       s->global_person_history.current_size-1);

    double dx = start_point_h.x - end_point_h.x; 
    //double dy = start_point_h.y - end_point_h.y; 

    double m = .0, c = .0;

    /*get_bestfit_from_history(person_history_global, 
      0,
      valid_history-1, &c, &m);*/
    get_bestfit_from_history_object(&(s->global_person_history),
				    0,
				    s->global_person_history.current_size-1, &c, &m);
				    

    best_fit_heading = atan(m); //this can point to either way - need to use the location of the points to extract the actual angle 

    //fprintf(stderr,"Start : %f,%f End : %f,%f => dx : %f dy: %f\n", start_point_h.x, start_point_h.y, end_point_h.x,  end_point_h.y, dx, dy);

    //calculating velocity relative to ground truth
    //carmen_point_t start_point = get_average_point_from_history(person_history_global, 
    carmen_point_t start_point = get_average_point_from_history_object(&(s->global_person_history_small),
								       0,
								       s->global_person_history_small.current_size/2.0-1);

    //carmen_point_t end_point = get_average_point_from_history(person_history_global, 
    carmen_point_t end_point = get_average_point_from_history_object(&(s->global_person_history_small),
								     s->global_person_history_small.current_size/2.0,
								     s->global_person_history_small.current_size-1);    
    if(dx < 0){//in the second or third quardrent 
      best_fit_heading = carmen_normalize_theta(best_fit_heading + M_PI);
    }
#ifdef PRINT_OP
    fprintf(stderr,"c : %f, m : %f: Angle : %f\n", c, m, best_fit_heading / M_PI*180);
#endif
    
    if(start_point.theta - end_point.theta >0){      
      vel_x = (start_point.x - end_point.x)/(start_point.theta - end_point.theta);
      vel_y = (start_point.y - end_point.y)/(start_point.theta - end_point.theta);
    }
    else{
      vel_x = 0;
      vel_y = 0;
    }
    //lets do an instantaenous calculation
    
    fprintf(stderr,"velocity : (%f,%f)\n", vel_x, vel_y);

    u_vel_x = vel_x;
    u_vel_y = vel_y;
    if(hypot(vel_x,vel_y)<= 0.2){
      //u_vel_x = prev_velocity.x;
      //u_vel_y = prev_velocity.y;
      //set to zero if small motions
      u_vel_x = 0;
      u_vel_y = 0;
    }

    if(hypot(vel_x,vel_y)> 0.2){
      prev_velocity.x = vel_x;
      prev_velocity.y = vel_y;
    }

    lcmgl = s->lcmgl_person_history; //bot_lcmgl_init(s->lcm, "person_history");//globals_get_lcmgl("person_hsitory",1);
    if(lcmgl){
      lcmglColor3f(1.0, .0, 0);
      //lcmglBegin(GL_LINES);

      //draw the person heading also 
      
      for(int k=0;k<s->global_person_history.current_size;k++){

	float rtheta = s->global_robot_pose.theta;
	
	double pos[3] = {.0,.0,.0};
	double leg_xy[2] = {s->global_person_history.history[k].x, s->global_person_history.history[k].y};

	get_global_point(leg_xy, pos,s);
	
	//lcmglVertex3d(leg_x, leg_y, 2.0);
	lcmglColor3f(1.0, .0, .0);

	lcmglLineWidth (5);      
	lcmglCircle(pos, 0.02);
      }
      bot_lcmgl_switch_buffer (lcmgl);
    }
  }
  else{
    reset_point_avg_object(&(s->relative_person_history));
    reset_point_avg_object(&(s->global_person_history));   
  }

  //not sure if we should use this - if less than a certain value we should just set the magnitude to 0 
  double vel_mag = hypot(u_vel_x, u_vel_y);
  double vel_heading = atan2(u_vel_y,u_vel_x);
    
  //person_heading_average is pretty wrong when we smoothen it 
  if(vel_mag > 0.2){
    person_heading_average = best_fit_heading;//vel_heading;
  }
  //person_heading_average = get_average_double_from_history(heading_history, 0, valid_heading_history-1);//person_heading_average * (1 - learning_rate) + vel_heading * learning_rate;
  person_vel_average = person_vel_average * (1 - learning_rate) + vel_mag * learning_rate;


  lcmgl = s->lcmgl_velocity; //bot_lcmgl_init(s->lcm, "velocity");//globals_get_lcmgl("velocity",1);
    
  if(lcmgl){
    //we should have a threshold to decide if the person is not moving 
    //if not moving maybe use the old qualifying velocities??
    double x_vel, y_vel;
    
    //x_vel = vel_mag* cos(person_heading_average);
    x_vel = 1.0* cos(person_heading_average);
    //y_vel = vel_mag* sin(person_heading_average);
    y_vel = 1.0* sin(person_heading_average);

    //double alpha = atan2(guide_pos.y, guide_pos.x);
    double rtheta = s->global_robot_pose.theta; //+ alpha;
    double vel_g_x = x_vel*cos(rtheta) - y_vel*sin(rtheta);
    double vel_g_y = x_vel*sin(rtheta) + y_vel*cos(rtheta);           
      
    //if(hypot(vel_x, vel_y)> 0.2){
    lcmglColor3f(0.0, 1.0, 0.2);
    //}
    //else{
    //lcmglColor3f(1.0, 0.0, 1.0);
    //}

    fprintf(stderr,"Guide GPOS %f,%f\n" , s->guide_gpos.x, s->guide_gpos.y);
      
    lcmglBegin(GL_LINES);
    lcmglVertex3d(s->guide_gpos.x, s->guide_gpos.y, 2.0);
    lcmglVertex3d(s->guide_gpos.x + vel_g_x, s->guide_gpos.y+ vel_g_y, 2.0);
    lcmglEnd();

    //temp calculation - done here 
    double alpha = atan2(y_vel, x_vel);

    //alternative positions
    double p1_alpha =  bot_mod2pi(alpha + M_PI/2);
    double p2_alpha =  bot_mod2pi(alpha - M_PI/2);

    double distance_to_side = 1.0;
     
    double p1_pos[3] = {.0,.0,.0};
    double p2_pos[3] = {.0,.0,.0};
 
    double p1_x = distance_to_side * cos(p1_alpha);
    double p1_y = distance_to_side * sin(p1_alpha);

    p1_pos[0] = s->guide_gpos.x + p1_x*cos(rtheta) - p1_y*sin(rtheta);
    p1_pos[1] = s->guide_gpos.y + p1_x*sin(rtheta) + p1_y*cos(rtheta);

    double p2_x = distance_to_side * cos(p2_alpha);
    double p2_y = distance_to_side * sin(p2_alpha);
      
    p2_pos[0] = s->guide_gpos.x + p2_x*cos(rtheta) - p2_y*sin(rtheta);
    p2_pos[1] = s->guide_gpos.y + p2_x*sin(rtheta) + p2_y*cos(rtheta);     


    lcmglLineWidth (8);    
    //left is red
    lcmglColor3f(1.0, 0.0, 0.0);
    lcmglCircle(p1_pos, 0.1);
    //right is 
    lcmglColor3f(1.0, 1.0, 0.0);
    lcmglCircle(p2_pos, 0.1);

    bot_lcmgl_switch_buffer (lcmgl);
  }

  //carmen_localize_update_person_heading(last_robot_position,robot_position,smoothed_heading_history, valid_heading_history-1);
  
  fprintf(stderr,"Person Heading Average : %f\n", person_heading_average);
  fprintf(stderr,"Getting smoothed History\n");
  //add_double_to_history(smoothed_heading_history, temp_smoothed_heading_history, person_heading_average);

  //double start_heading = get_average_double_from_history(smoothed_heading_history, 
  //								0,
  //							valid_heading_history/2.0-1);

  //double end_heading = get_average_double_from_history(smoothed_heading_history, 
  //						       valid_heading_history/2.0,
  //						       valid_heading_history-1);

  //forgoing the rv calculation - doesnt seem to be that accrate 

  /*lcmgl = globals_get_lcmgl("person_arc",1);

    if(elapsed_time > 0){
    rot_vel_mag = (start_heading - end_heading)/elapsed_time;
    double arc_radius = 10000.0;
    
    //rot_vel_mag = -0.5;//0.5;
    if(fabs(rot_vel_mag) >0){
    arc_radius = fabs(1.0 / rot_vel_mag);//fabs(vel_mag / rot_vel_mag);
    }
    fprintf(stderr, "Start Heading : %f - End Heading : %f : Elapsed Time : %f, Omega : %f Radius : %f\n", start_heading, end_heading, elapsed_time, rot_vel_mag, arc_radius);

    
    
    //lets do the calculation and draw the circle here 
    double lxy[2] = {.0,.0};
    double xyz[3] = {.0,.0,.0}; //center
    
    double ctheta = cos(person_heading_average);
    double stheta = sin(person_heading_average);
    if(rot_vel_mag > 0){
    lxy[0] = guide_pos.x - arc_radius * stheta;Ma
    lxy[1] = guide_pos.y + arc_radius * ctheta;      
    }
    else{//if(rot_vel_mag > 0){
    lxy[0] = guide_pos.x + arc_radius * stheta;
    lxy[1] = guide_pos.y - arc_radius * ctheta;      
    }
    get_global_point(lxy, xyz);
    
    bot_lcmgl_circle(lcmgl, xyz, arc_radius);
    }
    bot_lcmgl_switch_buffer (lcmgl);  */

#ifdef PRINT_OP
  fprintf(stderr,"Guide Msg\n");
#endif
  //publish_guide_msg(vel_mag, vel_heading, relative_vel);
  //publish_guide_msg(vel_mag, person_heading_average, relative_vel);

  //this should be the observation time 

  publish_guide_msg(time_obs, vel_mag, rot_vel_mag, person_heading_average, approach_vel, s);
  //publish_guide_msg(person_vel_average, person_heading_average, relative_vel);
 
  if(s->no_people >=0){//draw person if we have a person being tracked   
    lcmgl = s->lcmgl_person_zone; //bot_lcmgl_init(s->lcm, "person_zone");//globals_get_lcmgl("person_zone",1);

    //rel_vel_mag
    if(approach_vel >0){
      lcmglColor3f(1.0, .0, .0);
    }
    else{
      lcmglColor3f(1.0, .0, 1.0);
    }

    //2s distance 
    double radius = 1.0;//fmax(approach_vel * 2.0, 1.0);  //we will use a fixed radius 
    
    fprintf(stderr,"Safety Radius : %f\n", radius);
    
    double xyz[3] = { s->guide_gpos.x, s->guide_gpos.y , 0 };
    lcmglCircle(xyz, radius);

    bot_lcmgl_switch_buffer (lcmgl);

    lcmgl = s->lcmgl_person;//bot_lcmgl_init(s->lcm, "person");//globals_get_lcmgl("person",1);

    double pos[3];

    for(i=0;i< s->no_people;i++){
      float person_x = .0, person_y = .0;
      float theta = s->global_robot_pose.theta;
      if(i==s->active_filter_ind){
	//person_x = guide_pos.x;
	//person_y = guide_pos.y;
	person_x = s->people_summary[i].mean.x;
	person_y = s->people_summary[i].mean.y;

      }
      else{
	person_x = s->people_summary[i].mean.x;
	person_y = s->people_summary[i].mean.y;
      }
      float goal_x = s->global_robot_pose.x + person_x * cos(theta) - person_y*sin(theta);
      float goal_y = s->global_robot_pose.y + person_x * sin(theta) + person_y*cos(theta);


      pos[0] = goal_x;
      pos[1] = goal_y;
      pos[2] = 2.0;  

      if (1 && lcmgl) {//turn to 1 if u want to draw
	lcmglLineWidth (5);
	if(i==s->active_filter_ind){
	  last_person_pos[0] = goal_x;
	  last_person_pos[1] = goal_y;

	  lcmglColor3f(1.0, .0, .0);
	}
	else{
	  lcmglColor3f(.0, 1.0, 1.0);
	}
	lcmglCircle(pos, 0.15);
      }
    }
    bot_lcmgl_switch_buffer (lcmgl);
  }
  
  fprintf(stderr,"No of filters %d \n", s->no_people);
  fprintf(stderr,"------------------------------------------------------------\n");
  free(person_det);
  free(full_person_det);

  for (i=0;i<meta_count;i++){
    free(dist[i]);
  }
  free(dist);

  for (j=0;j<original_filter_size;j++){
    free(allocated_observations[j]);
  }
  free(allocated_observations);
  free(no_obs_count);
  prev_time = time_obs;
  free(allocated_to_filter);
  free(used_person_obs.locations);
  free(prev_points);
  prev_points = points;
  prev_no_points = no_points;

  return 1;
}


int 
lcm_detect_person_both_basic(state_t *s){ 
  
  //have no laser messages - return 
  if((!s->have_rear_laser && !s->have_front_laser)){
    return -1;
  }
  if (s->current_map.map != NULL){
    fprintf(stderr,"Checking with Gridmap\n");
    fprintf(stderr,"Have map => Size (%d,%d)\n", s->current_map.config.x_size, s->current_map.config.y_size);
  }

  //velocities 
  double vel_x = .0, vel_y = .0;
  double rel_vel_x = .0, rel_vel_y = .0;
  double u_vel_x = .0, u_vel_y = .0;
  double person_vel_average = .0;
  double learning_rate = 0.3;
  double rot_vel_mag = 0.0;
  double approach_vel = .0;

  //heading 
  static double person_heading_average = .0;
  double best_fit_heading = 0.0;

  //obs time in (s)
  double time_obs = ((double)s->robot_fl->utime) /  1e6 ; 

  double rpy[3] = {0,0,0};  
  bot_quat_to_roll_pitch_yaw (s->robot_fl->pose.orientation, rpy) ;

  carmen_point_t robot_position;

  robot_position.x = s->robot_fl->pose.pos[0];
  robot_position.y = s->robot_fl->pose.pos[1];
  robot_position.theta = rpy[2];
  
  bot_lcmgl_t *lcmgl;

  static double vel[2] = {.0,.0};
  static double last_person_pos[3] = {.0,.0,.0};
  static double prev_time = 0;
  
  //update the filter summaries based on the robot motion 
  for(int j=0;j<s->no_people;j++){
    carmen_localize_update_filter_center(s->last_robot_position,robot_position,&(s->people_summary[j].mean));
  }
  
  int no_points = 0;
  carmen_point_p points = NULL;  //point array for the laser readings

  static carmen_point_p prev_points = NULL;
  static int prev_no_points = 0;
  
  carmen_feet_observations person_obs;

  

  if(!s->have_rear_laser){
    points = (carmen_point_p)malloc(s->robot_fl->laser.nranges *sizeof(carmen_point_t));  
    carmen_test_alloc(points);
    
    //----- Sachi : This pruning needs to change 
    if(s->active_filter_ind >=0){
      carmen_point_t person_loc;
      person_loc.x = s->people_summary[s->active_filter_ind].mean.x;
      person_loc.y = s->people_summary[s->active_filter_ind].mean.y;
      person_loc.theta = .0;

      person_obs = lcm_detect_segments_from_planar_lidar_pruned(&s->robot_fl->laser, s->front_laser_offset, s->robot_fl->pose, 
								NULL, s->rear_laser_offset, s->robot_fl->pose, 
								SEGMENT_GAP,points, &no_points,
								&person_loc, s->high_laser, 
								s->use_classifier, s->pruning_mode);
      
    }
    else{
      person_obs = lcm_detect_segments_from_planar_lidar(&s->robot_fl->laser, s->front_laser_offset,s->robot_fl->pose,
							 NULL, s->rear_laser_offset, s->robot_fl->pose, 
							 SEGMENT_GAP,points, &no_points, 
							 s->high_laser, s->use_classifier);
    }

    /*if(s->active_filter_ind <0 || s->multi_people_tracking){
      //call the new feet detector
      carmen_feet_observations moved_obs;
      get_moving_feet_segments(&person_obs, &moved_obs, prev_points, prev_no_points);
  
      lcmgl = bot_lcmgl_init(s->lcm, "moved_leg_obs");//globals_get_lcmgl("moved_leg_obs",1);

      if (1 && lcmgl){// && is_front_laser){ //turn to 1 if u want to draw
	float rtheta = s->global_robot_pose.theta;
	double pos[3];
	for (int i=0;i<moved_obs.no_obs;i++){
	  float leg_x = s->global_robot_pose.x + moved_obs.locations[i].center_x * cos(rtheta) - moved_obs.locations[i].center_y * sin(rtheta);
	  float leg_y = s->global_robot_pose.y + moved_obs.locations[i].center_x * sin(rtheta) + moved_obs.locations[i].center_y*cos(rtheta);
	  pos[0] = leg_x;
	  pos[1] = leg_y;
	  pos[2] = 2.0;  
     
	  lcmglLineWidth (5);
	  lcmglColor3f(1.0, 1.0, .0);
	  lcmglCircle(pos, 0.1);
	} 
      } 

      bot_lcmgl_switch_buffer (lcmgl);

      carmen_point_p moved_person = (carmen_point_p) malloc(moved_obs.no_obs*sizeof(carmen_point_t)); 
      carmen_test_alloc(moved_person);
  
      //copy the observations on to points
      if(s->active_filter_ind==-1){
	for (int i=0;i< moved_obs.no_obs;i++){
	  moved_person[i].x = moved_obs.locations[i].center_x;
	  moved_person[i].y = moved_obs.locations[i].center_y;
	  moved_person[i].theta = 0.0;
	  //check if there are other filters near by
	  double min_dist = 1000;
	  for(int j=0;j<s->no_people;j++){
	    double temp_dist = hypot((s->people_summary[j].mean.x - moved_person[i].x),(s->people_summary[j].mean.y-moved_person[i].y));
	    if(temp_dist < min_dist){
	      min_dist = temp_dist;
	    }
	  }
	  if(min_dist < 0.8){
	    //fprintf(stderr,"Already tracking a person near - not adding new people\n");
	  }
	  else{   
	    //fprintf(stderr,"----------- Creating Filter\n");	     
	    create_new_person(moved_person[i],s->std, time_obs, s);
	  }
	}
      }
      free(moved_person);
      }*/
  }
  else{
    points = (carmen_point_p)malloc((s->robot_fl->laser.nranges + s->robot_rl->laser.nranges)*sizeof(carmen_point_t)); 
    carmen_test_alloc(points); 
    if(s->active_filter_ind >=0){
      carmen_point_t person_loc;
      person_loc.x = s->people_summary[s->active_filter_ind].mean.x;
      person_loc.y = s->people_summary[s->active_filter_ind].mean.y;
      person_loc.theta = .0;

      person_obs = lcm_detect_segments_from_planar_lidar_pruned(&s->robot_fl->laser, s->front_laser_offset,s->robot_fl->pose,
								&s->robot_rl->laser, s->rear_laser_offset, s->robot_rl->pose,
								SEGMENT_GAP,points, &no_points, &person_loc, s->high_laser, s->use_classifier, s->pruning_mode);
    }
    else{
      person_obs = lcm_detect_segments_from_planar_lidar(&s->robot_fl->laser, s->front_laser_offset,s->robot_fl->pose, 
							 &s->robot_rl->laser, s->rear_laser_offset, s->robot_rl->pose,
							 SEGMENT_GAP,points, &no_points, s->high_laser, s->use_classifier); 
    }
  }
  
  //create tracks for moving observations 

  if(s->multi_people_tracking || s->active_filter_ind < 0){
    //call the new feet detector
    carmen_feet_observations moved_obs;
    get_moving_feet_segments(&person_obs, &moved_obs, prev_points, prev_no_points, NULL);
  
    lcmgl =  bot_lcmgl_init(s->lcm,"moved_leg_obs"); 

    fprintf(stderr,"++++++++++ Moved Obs : %d\n", moved_obs.no_obs);

    if (1 && lcmgl){// && is_front_laser){ //turn to 1 if u want to draw
      float rtheta = s->global_robot_pose.theta;
      double pos[3];
      for (int i=0;i<moved_obs.no_obs;i++){
	float leg_x = s->global_robot_pose.x + moved_obs.locations[i].center_x * cos(rtheta) - moved_obs.locations[i].center_y * sin(rtheta);
	float leg_y = s->global_robot_pose.y + moved_obs.locations[i].center_x * sin(rtheta) + moved_obs.locations[i].center_y*cos(rtheta);
	pos[0] = leg_x;
	pos[1] = leg_y;
	pos[2] = 2.0;  
     
	lcmglLineWidth (5);
	lcmglColor3f(1.0, 1.0, .0);
	lcmglCircle(pos, 0.1);
      } 
    } 
    bot_lcmgl_switch_buffer (lcmgl);

    carmen_point_p moved_person = (carmen_point_p) malloc(moved_obs.no_obs*sizeof(carmen_point_t)); 
    carmen_test_alloc(moved_person);
  
    //copy the observations on to points
      
    if(s->active_filter_ind==-1 || s->multi_people_tracking){
      for (int i=0;i< moved_obs.no_obs;i++){
	moved_person[i].x = moved_obs.locations[i].center_x;
	moved_person[i].y = moved_obs.locations[i].center_y;
	moved_person[i].theta = 0.0;
	//check if there are other filters near by
	double min_dist = 1000;
	for(int j=0;j<s->no_people;j++){
	  double temp_dist = hypot((s->people_summary[j].mean.x - moved_person[i].x),(s->people_summary[j].mean.y-moved_person[i].y));
	  if(temp_dist < min_dist){
	    min_dist = temp_dist;
	  }
	}
	if(min_dist < 0.8){
	  //fprintf(stderr,"Already tracking a person near - not adding new people\n");
	}
	else{   
	  //fprintf(stderr,"----------- Creating Filter\n");	     
	  create_new_person(moved_person[i],s->std, time_obs, s);
	}
      }
    }
    free(moved_person);
  }


  static carmen_point_t prev_velocity = {.0,.0,.0};
  carmen_localize_update_person_hostory(s->last_robot_position,robot_position,
					&prev_velocity, 1);


  carmen_feet_observations used_person_obs;
  
  //Prune observations if they are in the same place as in the last frame - based on motion only
  //used_person_obs = prune_observations(&person_obs);
  used_person_obs = person_obs; //no pruning 
  
  int full_count = used_person_obs.no_obs;
  carmen_feet_seg* feet_obs = used_person_obs.locations;
  int i,j;

  carmen_leg_t *person_det = (carmen_leg_t *) malloc(full_count*sizeof(carmen_leg_t)); 
  carmen_test_alloc(person_det);

  carmen_leg_t *full_person_det = (carmen_leg_t *) malloc(full_count*sizeof(carmen_leg_t)); 
  carmen_test_alloc(full_person_det);

  int meta_count = 0;

  //copy the observations on to points
  for (i=0;i<full_count;i++){
    double l_leg_x = feet_obs[i].center_x;
    double l_leg_y = feet_obs[i].center_y;
    if(s->current_map.map != NULL){
      //fprintf(stderr,"Checking with the map\n");
      float rtheta = s->global_robot_pose.theta;
      float leg_x = s->global_robot_pose.x + l_leg_x * cos(rtheta) - l_leg_y * sin(rtheta);
      float leg_y = s->global_robot_pose.y + l_leg_x * sin(rtheta) + l_leg_y*cos(rtheta);

      carmen_point_t leg_pt;
      memset(&leg_pt,0,sizeof(carmen_point_t));
      leg_pt.x = leg_x;
      leg_pt.y = leg_y;
      int l_x, l_y;

      int occupied = 0;
	
      carmen3d_map3d_global_to_map_index_coordinates(leg_pt, s->current_map.midpt, s->current_map.map_zero,
						     s->current_map.config.resolution, &l_x, &l_y);

      int search_size = 1;
	
      for(int j=fmax(0,l_x - search_size); j<=fmin(l_x+search_size,s->current_map.config.x_size-1); j++){
	for(int k=fmax(0,l_y - search_size); k<=fmin(l_y+search_size,s->current_map.config.x_size-1); k++){
	  if(s->current_map.map[j][k] >0.7){
	    occupied = 1;
	    break;
	  }
	}
	if(occupied){
	  break;
	}
      }
      if(!occupied){
	person_det[meta_count].x = l_leg_x;
	person_det[meta_count].y = l_leg_y;
	person_det[meta_count].theta = 0.0;	
	person_det[meta_count].front_laser = feet_obs[i].front_laser;
	meta_count++;
      }
      full_person_det[i].x = l_leg_x;
      full_person_det[i].y = l_leg_y;
      full_person_det[i].theta = 0.0;
      full_person_det[i].front_laser = feet_obs[i].front_laser;
    }
    else{
      person_det[meta_count].x = l_leg_x; 
      person_det[meta_count].y = l_leg_y;
      person_det[meta_count].theta = 0.0;
      person_det[meta_count].front_laser = feet_obs[i].front_laser;
      meta_count++;

      full_person_det[i].x = l_leg_x;
      full_person_det[i].y = l_leg_y;
      full_person_det[i].theta = 0.0;
      full_person_det[i].front_laser = feet_obs[i].front_laser;
    }
  }
  
  person_det = (carmen_leg_t *) realloc(person_det,meta_count*sizeof(carmen_leg_t));
  //fprintf(stderr,"Obs after pruning : %d\n", meta_count);
  //draw possible legs 
  lcmgl = bot_lcmgl_init(s->lcm,"leg_obs");// globals_get_lcmgl("leg_obs",1);

  if (1 && lcmgl){// && is_front_laser){ //turn to 1 if u want to draw
    lcmglColor3f(0, 1.0, 0);
    
    float rtheta = s->global_robot_pose.theta;
    double pos[3];
    for (i=0;i<meta_count;i++){
      float leg_x = s->global_robot_pose.x + person_det[i].x * cos(rtheta) - person_det[i].y * sin(rtheta);
      float leg_y = s->global_robot_pose.y + person_det[i].x * sin(rtheta) + person_det[i].y*cos(rtheta);
      pos[0] = leg_x;
      pos[1] = leg_y;
      pos[2] = 2.0; 

      lcmglColor3f(.0, 1.0, .0);    
      lcmglLineWidth (5);      
      lcmglCircle(pos, 0.1);
      if(s->debug_mode){
	char seg_info[512];
	sprintf(seg_info,"[%d]%d-%d:%d", feet_obs[i].front_laser, feet_obs[i].start_segment, feet_obs[i].end_segment, 
		feet_obs[i].start_segment - feet_obs[i].end_segment);
	bot_lcmgl_text(lcmgl, pos, seg_info);
      } 
    }
  } 

  bot_lcmgl_switch_buffer (lcmgl);  

  //prune dead filters
  prune_filters(time_obs, s);  

  //Filter initiation code
  //create a filter for the observation that is not near any of the current filters
  
  double **dist;
  dist = (double **) malloc(meta_count*sizeof(double *));
  carmen_test_alloc(dist);
  //create a distance matrix between the filters and the observations
  for (i=0;i<meta_count;i++){
    dist[i] = (double *) malloc(s->no_people*sizeof(double));
    carmen_test_alloc(dist[i]);
    for(j=0;j<s->no_people;j++){
      dist[i][j] = hypot((s->people_summary[j].mean.x-person_det[i].x),(s->people_summary[j].mean.y-person_det[i].y));
    }
  }

  //allocate each observation to the closest person filter 
  int *no_obs_count = (int *) malloc(s->no_people*sizeof(int)); //no of observations that each filter can see
  carmen_test_alloc(no_obs_count);
  memset(no_obs_count,0,s->no_people*sizeof(int));

  int original_filter_size = s->no_people;
  
  int *allocated_to_filter = (int *) malloc(meta_count*sizeof(int)); //no of observations that each filter can see
  carmen_test_alloc(allocated_to_filter);
  memset(allocated_to_filter,0,meta_count*sizeof(int));
  
  int **allocated_observations = (int **) malloc(s->no_people*sizeof(int *)); //what observations are assigned to each filter
  carmen_test_alloc(allocated_observations);
  for (j=0;j<s->no_people;j++){
    allocated_observations[j] = (int *) malloc(meta_count*sizeof(int)); //what observations are assigned to each filter
    carmen_test_alloc(allocated_observations[j]);
    memset(allocated_observations[j],-1,meta_count*sizeof(int));
  }

  double filter_creation_angle = M_PI/12; //M_PI/12;
  double filter_creation_dist = 3.0;//3.0;

  if(s->active_filter_ind >=0){//first do this for the tourguide 
    double dist_to_person = 10000.0;
    
    double person_angle = atan2(s->people_summary[s->active_filter_ind].mean.y, s->people_summary[s->active_filter_ind].mean.x);
    int use_front_laser = 0;
    
    //fprintf(stderr,"Person Angle : %f\n", person_angle * 180/M_PI);
    
    //63 degrees is the angle at which the rear laser hits the front laser's 90 degree side scans

    if(person_angle < M_PI/180*63 && person_angle >= -M_PI/180*63){
      //fprintf(stderr,"Using only the front laser observations");
      use_front_laser = 1;
    }
    else{
      //fprintf(stderr,"Using only the rear laser observations");
    }
    
    int ind = -1;
    int full_ind = -1;
    if(s->debug_mode){
      fprintf(stderr,"==============================================\n");
    }
    for (i=0;i<meta_count;i++){ //search through
      if(s->debug_mode){
	fprintf(stderr,"%d : %f\n",i, dist[i][s->active_filter_ind]);
      }
      
      if(person_det[i].front_laser != use_front_laser){
	continue;
      }
      if(dist[i][s->active_filter_ind] < PERSON_OBS_RADIUS){ //within the boundry
	allocated_to_filter[i]=1; //make all these observations out of bounds for the other filters
	if(s->debug_mode){
	  fprintf(stderr,"Found Observation : %d\n", i);
	}
	
	if(dist[i][s->active_filter_ind] < dist_to_person){
	  ind = i;
	  dist_to_person = dist[i][s->active_filter_ind];
	}
      }
    }
    if(ind ==-1){      
      //do a full search of these observations - through both laser observations
      for (i=0;i<meta_count;i++){ //search through	
	if(dist[i][s->active_filter_ind] < PERSON_OBS_RADIUS){ //within the boundry
	  allocated_to_filter[i]=1; //make all these observations out of bounds for the other filters
	  if(dist[i][s->active_filter_ind] < dist_to_person){
	    ind = i;
	    dist_to_person = dist[i][s->active_filter_ind];
	  }
	}	
      }
    }
    if(ind==-1 && (meta_count <full_count)){ //we still havent found a good observation
      //have not found a good observation yet
      for (i=0;i<full_count;i++){ //search through
	if(full_person_det[i].front_laser != use_front_laser){
	  continue;
	}

	double temp_dist =  hypot((s->people_summary[s->active_filter_ind].mean.x - full_person_det[i].x),
				  (s->people_summary[s->active_filter_ind].mean.y - full_person_det[i].y));

	if(temp_dist < PERSON_OBS_RADIUS){ //within the boundry
	  if(temp_dist < dist_to_person){
	    full_ind = i;
	    dist_to_person = temp_dist;
	  }
	}

      }
    }
    
    if(ind==-1 && full_ind==-1){
      carmen_localize_run_person_no_obs(s->people_filter[s->active_filter_ind], robot_position);
      carmen_localize_summarize_person(s->people_filter[s->active_filter_ind], &(s->people_summary[s->active_filter_ind]));
    }
    else if(ind>=0){//(no_obs_count[j]==1){
      int obs = ind;//allocated_observations[j][0];
      carmen_point_t assigned_obs;
      assigned_obs.x = person_det[obs].x;
      assigned_obs.y = person_det[obs].y;
      assigned_obs.theta = person_det[obs].theta;

      double min_leg_dist = 0.5;//used to be 0.5
      //the higher this is the more likely we will get merged to false leg observation if we get near enough

      int closest_leg = -1;
      
      if(closest_leg >=0){
	//fprintf(stderr,"Found a close leg Dist : %f\n", min_leg_dist);

	s->guide_pos.x = (person_det[obs].x + person_det[closest_leg].x)/2;
	s->guide_pos.y = (person_det[obs].y + person_det[closest_leg].y)/2;
	
      }
      else{
	s->guide_pos.x = person_det[obs].x;
	s->guide_pos.y = person_det[obs].y;
      }
      
      carmen_point_t person_position_obs;
      person_position_obs.x = s->guide_pos.x;
      person_position_obs.y = s->guide_pos.y;    
#ifdef PRINT_OP
      fprintf(stderr,"Active Filter Index : %d =  Valid Filter Count : %d\n", s->active_filter_ind, s->no_people);
#endif
      carmen_localize_run_person_with_obs(s->people_filter[s->active_filter_ind], robot_position, assigned_obs);
      //this tends to mess up the tracking sometimes - not sure if the benifits of this outweigh the cost       
      //carmen_localize_run_person_with_obs(people_filter[active_filter_ind], robot_pos, person_position_obs);
      carmen_localize_summarize_person(s->people_filter[s->active_filter_ind], &(s->people_summary[s->active_filter_ind]));
      s->time_since[s->active_filter_ind] = time_obs; 

#ifdef PRINT_OP
      fprintf(stderr,"GUIDE pos : %f,%f\tFilter Pos : %f,%f\n", s->guide_pos.x, s->guide_pos.y, s->people_summary[s->active_filter_ind].mean.x, s->people_summary[s->active_filter_ind].mean.y);
#endif      
      s->guide_pos.x = s->people_summary[s->active_filter_ind].mean.x;
      s->guide_pos.y = s->people_summary[s->active_filter_ind].mean.y;

      float rtheta = s->global_robot_pose.theta;
      float leg_x = s->global_robot_pose.x + s->guide_pos.x * cos(rtheta) - s->guide_pos.y * sin(rtheta);
      float leg_y = s->global_robot_pose.y + s->guide_pos.x * sin(rtheta) + s->guide_pos.y*cos(rtheta);
      s->guide_gpos.x = leg_x;
      s->guide_gpos.y = leg_y;

      lcmgl = bot_lcmgl_init(s->lcm, "act_pos");//globals_get_lcmgl("act_pos",1);
      if(lcmgl){
	double pos[3];
	
	pos[0] = leg_x;
	pos[1] = leg_y;
	pos[2] = 2.0; 
	  
	lcmglColor3f(1.0, 0.5, .0);

	lcmglLineWidth (5);      
	lcmglCircle(pos, 0.15);
	  
	bot_lcmgl_switch_buffer (lcmgl);
      }
    }
    else if(full_ind>=0){//(no_obs_count[j]==1){
      int obs = full_ind;//allocated_observations[j][0];
      carmen_point_t assigned_obs;
      assigned_obs.x = full_person_det[obs].x;
      assigned_obs.y = full_person_det[obs].y;
      assigned_obs.theta = full_person_det[obs].theta;

      carmen_localize_run_person_with_obs(s->people_filter[s->active_filter_ind], robot_position, assigned_obs);
      carmen_localize_summarize_person(s->people_filter[s->active_filter_ind], &(s->people_summary[s->active_filter_ind]));
      s->time_since[s->active_filter_ind] = time_obs; 
    }

    //person location has been updated    
  }

  
  for(j=0;j<s->no_people;j++){//for each filter search the close observations and pick the one closest to the wheelchair 
    if(j==s->active_filter_ind){
      continue;
    }
    double dist_to_wheelchair = 10000.0;
    int ind = -1;
    for (i=0;i<meta_count;i++){ //search through
      if(!allocated_to_filter[i] &&(dist[i][j] < 0.4)){ //within the boundry
	double temp_dist = hypot(person_det[i].x, person_det[i].y);
	if(temp_dist < dist_to_wheelchair){
	  ind = i;
	  dist_to_wheelchair = temp_dist;
	}
      }
    }
    //if(no_obs_count[j]==0){
    if(ind==-1){
      carmen_localize_run_person_no_obs(s->people_filter[j], robot_position);
      carmen_localize_summarize_person(s->people_filter[j], &(s->people_summary[j]));
    }
    else if(ind>=0){//(no_obs_count[j]==1){
      allocated_to_filter[ind] = 1;
      int obs = ind;//allocated_observations[j][0];
      carmen_point_t assigned_obs;
      assigned_obs.x = person_det[obs].x;
      assigned_obs.y = person_det[obs].y;
      assigned_obs.theta = person_det[obs].theta;
      carmen_localize_run_person_with_obs(s->people_filter[j], robot_position, assigned_obs);
      carmen_localize_summarize_person(s->people_filter[j], &(s->people_summary[j]));
      s->time_since[j] = time_obs; 
    }

  }

  if(s->active_filter_ind < 0){ //creating new filters only if there is no active filter - // we should prob change this so it creates only one filer 
    for (i=0;i<meta_count;i++){
      if(allocated_to_filter[i]==0){
	double angle = atan2(person_det[i].y,person_det[i].x);
	double dist = hypot(person_det[i].x,person_det[i].y);
	
	if (fabs(angle)< filter_creation_angle && dist < filter_creation_dist){
	  int near_filter = 0;
	  double min_dist = 1.0;
	  double temp_dist;
	  for(j=0;j<s->no_people;j++){
	    temp_dist = hypot((s->people_summary[j].mean.x-person_det[i].x),(s->people_summary[j].mean.y-person_det[i].y));
	    if(temp_dist< min_dist){
	      min_dist = temp_dist;
	      near_filter = 1;
	      break;
	    }
	  }
	  if(near_filter==0){//no nearby filter was created 
	    carmen_point_t assigned_obs;
	    assigned_obs.x = person_det[i].x;
	    assigned_obs.y = person_det[i].y;
	    assigned_obs.theta = person_det[i].theta;
	    create_new_person(assigned_obs,s->std, time_obs, s);
	  }
	}      
      }
    }
  }

  if(s->tracking_state==LOOKING){//if we are in the tracking state we should reaquire 
    //??make two passes - first check if we have an observations 
    double max_dist = 4.0;
    double too_close_dist = 0.5;
    //double min_dist = 0.5;
    double follow_angle = M_PI/6;
    double temp_dist;
    double temp_angle;
    fprintf(stderr,"Looking for person\n");
    

    double angle_offset = 10000;

    for(j=0;j<s->no_people;j++){
      temp_dist = hypot(s->people_summary[j].mean.x,s->people_summary[j].mean.y);
      temp_angle = atan2(s->people_summary[j].mean.y,s->people_summary[j].mean.x);
      if((fabs(temp_angle) < follow_angle) 
	 && (temp_dist < max_dist) && (temp_dist > too_close_dist)){
	if(angle_offset > fabs(temp_angle)){
	  angle_offset = fabs(temp_angle);
	  s->active_filter_ind = j;	
	}
      }
    }
    
    //kill the other filters only if we are in single person tracking mode 
    if(s->active_filter_ind >=0 && !s->multi_people_tracking){
      for(int k=0;k<s->no_people;k++){
	if(k != s->active_filter_ind){
	  s->time_since[k] = .0;
	}
      }
      s->guide_pos.x = s->people_summary[s->active_filter_ind].mean.x;
      s->guide_pos.y = s->people_summary[s->active_filter_ind].mean.y;
      update_tracking_state(FOUND,s);  //person located	
      //fprintf(stderr,"====== New Person Following Activated =========\n");      
    }
    else{
      //fprintf(stderr,"**** No good filter found **** \n");
    }
  }
  else{
    if(s->active_filter_ind>=0){
      s->guide_pos.x = s->people_summary[s->active_filter_ind].mean.x;
      s->guide_pos.y = s->people_summary[s->active_filter_ind].mean.y;
      //fprintf(stderr,"== Current Person Followed is %d : (%f,%f)\n", active_filter_ind, 
      //      people_summary[active_filter_ind].mean.x,people_summary[active_filter_ind].mean.y);
    }
    else{
      //fprintf(stderr,"== No one being tracked \n");
    }
  }

  if(s->no_people>0){    
    erlcm_point_t* test_people_pos = (erlcm_point_t *) 
      realloc(s->people_pos,s->no_people*sizeof(erlcm_point_t));
    carmen_test_alloc(test_people_pos);
    s->people_pos = test_people_pos;
  }

  int person_pos = 0;

  for(j=0;j<s->no_people;j++){
    s->people_pos[person_pos].x = s->people_summary[j].mean.x;
    s->people_pos[person_pos].y = s->people_summary[j].mean.y;
    s->people_pos[person_pos].z = 0.0;
    s->people_pos[person_pos].yaw = 0.0;
    s->people_pos[person_pos].pitch = 0.0;
    s->people_pos[person_pos].roll = s->people_summary[j].mean.theta;
    person_pos++;
  }

  //publishing a person removed robot and planar lidar message
  
  /*
  float distance_from_person = 0.0;
  static carmen_robot_laser_message* carmen_laser;  
  
  if(carmen_laser == NULL){
    carmen_laser = (carmen_robot_laser_message *) malloc(sizeof(carmen_robot_laser_message));
    carmen_test_alloc(carmen_laser);
  }
  memcpy(carmen_laser,&(s->carmen_robot_frontlaser), sizeof(carmen_robot_laser_message));
  if(carmen_laser->range==NULL){
    carmen_laser->range = (float *) malloc(s->carmen_robot_frontlaser.num_readings * sizeof(float));
    carmen_test_alloc(carmen_laser->range);
  }
  
  memcpy(carmen_laser->range, s->carmen_robot_frontlaser.range, 
	 s->carmen_robot_frontlaser.num_readings * sizeof(float));

  double person_leg_range = 1.0;
  int removed_legs = 0;
  if(s->active_filter_ind >=0){  //if we are tracking a person -remove the observations attached to the person 
    //fprintf(stderr,"Person Location : %f,%f \n",people_summary[active_filter_ind].mean.x, people_summary[active_filter_ind].mean.y);
    for (i=0;i<used_person_obs.no_obs;i++){
      if(!feet_obs[i].front_laser){//this if the front laser - skipping rear laser stuff
	continue;
      }
      distance_from_person = hypot(feet_obs[i].center_x - s->people_summary[s->active_filter_ind].mean.x,
				   feet_obs[i].center_y - s->people_summary[s->active_filter_ind].mean.y);
      //fprintf(stderr,"\tDistance : %f\n", distance_from_person);
      if(distance_from_person < person_leg_range){
	removed_legs++;
	int p;
	//fprintf(stderr,"[%d:%d]\n",feet_obs[i].start_segment, feet_obs[i].end_segment);
	for(p = fmax(feet_obs[i].start_segment-3,0); p <=fmin(feet_obs[i].end_segment+3,s->carmen_robot_frontlaser.num_readings-1);p++){
	  carmen_laser->range[p] = 0.001;  //also might have implication on the max range conversion done in laser driver
	} 	  
      }
    }
#ifdef PRINT_OP
    fprintf(stderr,"No of Observations Removed : %d\n", removed_legs);
#endif
    //remove any points within 0.3 
    for(int p = 0; p < s->carmen_robot_frontlaser.num_readings ;p++){
      distance_from_person = hypot(points[p].x - s->people_summary[s->active_filter_ind].mean.x,
				   points[p].y - s->people_summary[s->active_filter_ind].mean.y);

      //fprintf(stderr,"\tDistance : %f\n", distance_from_person);
      if(distance_from_person < CLEARING_RADIUS){
	carmen_laser->range[p] = 0.001;  	  
      }
    }
  }
    
  //robot_laser_pub("PERSON_ROBOT_LASER",carmen_laser,lcm);

  //clearing the rear laser of person observations

  if(s->clear_rearlaser && s->have_rear_laser){
    static carmen_robot_laser_message* rear_carmen_laser;
    
    if(rear_carmen_laser == NULL){
      rear_carmen_laser = (carmen_robot_laser_message *) malloc(sizeof(carmen_robot_laser_message));
      carmen_test_alloc(rear_carmen_laser);
    }
    memcpy(rear_carmen_laser,&(s->carmen_robot_rearlaser), sizeof(carmen_robot_laser_message));
    if(rear_carmen_laser->range==NULL){
      rear_carmen_laser->range = (float *) malloc(s->carmen_robot_rearlaser.num_readings * sizeof(float));
      carmen_test_alloc(rear_carmen_laser->range);
    }
    
    memcpy(rear_carmen_laser->range, s->carmen_robot_rearlaser.range, 
	   s->carmen_robot_rearlaser.num_readings * sizeof(float));

    double person_leg_range = 1.0;
    int removed_legs = 0;
    if(s->active_filter_ind >=0){  //if we are tracking a person -remove the observations attached to the person 
      for (i=0;i<used_person_obs.no_obs;i++){
	if(feet_obs[i].front_laser){//this if the rear laser - skipping front laser stuff
	  continue;
	}
	distance_from_person = hypot(feet_obs[i].center_x - s->people_summary[s->active_filter_ind].mean.x,
				     feet_obs[i].center_y - s->people_summary[s->active_filter_ind].mean.y);
	//fprintf(stderr,"\tDistance : %f\n", distance_from_person);
	if(distance_from_person < person_leg_range){
	  removed_legs++;
	  int p;
	  //fprintf(stderr,"[%d:%d]\n",feet_obs[i].start_segment, feet_obs[i].end_segment);
	  for(p = fmax(feet_obs[i].start_segment-3,0); p <=fmin(feet_obs[i].end_segment+3,s->carmen_robot_rearlaser.num_readings-1);p++){
	    rear_carmen_laser->range[p] = 0.001;  //also might have implication on the max range conversion done in laser driver
	  } 	  
	}
      }
#ifdef PRINT_OP
      fprintf(stderr,"No of Observations Removed : %d\n", removed_legs);
#endif
      //remove any points within 0.3 
      for(int p = 0; p < s->carmen_robot_rearlaser.num_readings ;p++){
	distance_from_person = hypot(points[p].x - s->people_summary[s->active_filter_ind].mean.x,
				     points[p].y - s->people_summary[s->active_filter_ind].mean.y);

	//fprintf(stderr,"\tDistance : %f\n", distance_from_person);
	if(distance_from_person < CLEARING_RADIUS){
	  rear_carmen_laser->range[p] = 0.001;  	  
	}
      }
    }
    
    //robot_rearlaser_pub("PERSON_REAR_ROBOT_LASER",rear_carmen_laser,lcm);
  }
  */
  if(s->active_filter_ind>=0){//we have a valid person location
    
    //update the global headings and the positions
    //NEW METHOD
    
    carmen_localize_update_person_hostory(s->last_robot_position,robot_position, s->robot_history.history, s->robot_history.current_size);
    carmen_localize_update_person_hostory(s->last_robot_position,robot_position, s->global_person_history.history, s->global_person_history.current_size);
    carmen_localize_update_person_hostory(s->last_robot_position,robot_position, s->global_person_history_small.history, s->global_person_history_small.current_size);

    //might need to do this to obtain the robot's averaged velocity and then calculate relative vel from that 
    
#ifdef PRINT_OP
    fprintf(stderr,"Updating Person global history (%f,%f)\n", guide_pos.x, guide_pos.y);    
#endif
    carmen_point_t g_pos = {s->guide_pos.x, s->guide_pos.y, time_obs};

    carmen_point_t zero_pos = {.0, .0, time_obs};

    //NEW METHOD
    add_point_to_history_object(&(s->global_person_history), g_pos);
    add_point_to_history_object(&(s->global_person_history_small), g_pos);
    add_point_to_history_object(&(s->relative_person_history), g_pos);
    add_point_to_history_object(&(s->robot_history), zero_pos);     
    
    //calculate relative velocity 
    carmen_point_t r_start_point = get_average_point_from_history_object(&(s->relative_person_history), 
									 0,
									 s->relative_person_history.current_size/2.0-1);

    /*get_average_point_from_history(person_history, 
      0,
      valid_history/2.0-1);*/

    //    carmen_point_t r_end_point = get_average_point_from_history(person_history, 
    carmen_point_t r_end_point = get_average_point_from_history_object(&(s->relative_person_history), 
								       s->relative_person_history.current_size/2.0,
								       s->relative_person_history.current_size-1);  
    

    double rel_vel_heading = 0;
    double rel_vel_mag = 0;

    double heading_to_person = atan2(s->guide_pos.y, s->guide_pos.x);

    if(r_start_point.theta - r_end_point.theta >0){      
      rel_vel_x = (r_start_point.x - r_end_point.x)/(r_start_point.theta - r_end_point.theta);
      rel_vel_y = (r_start_point.y - r_end_point.y)/(r_start_point.theta - r_end_point.theta);
      rel_vel_heading = atan2(rel_vel_y, rel_vel_x);
      rel_vel_mag = hypot(rel_vel_x, rel_vel_y);
    }
    else{
      rel_vel_x = 0;
      rel_vel_y = 0;
    }


    carmen_point_t robot_start_point = get_average_point_from_history_object(&(s->robot_history), 
									     0,
									     s->robot_history.current_size/2.0-1);

    /*get_average_point_from_history(person_history, 
      0,
      valid_history/2.0-1);*/

    //    carmen_point_t r_end_point = get_average_point_from_history(person_history, 
    carmen_point_t robot_end_point = get_average_point_from_history_object(&(s->robot_history), 
									   s->robot_history.current_size/2.0,
									   s->robot_history.current_size-1);  

    double robot_vel_x = .0, robot_vel_y = .0;
    double robot_vel_mag = .0;
    if(robot_start_point.theta - robot_end_point.theta >0){      
      robot_vel_x = (robot_start_point.x - robot_end_point.x)/(robot_start_point.theta - robot_end_point.theta);
      robot_vel_y = (robot_start_point.y - robot_end_point.y)/(robot_start_point.theta - robot_end_point.theta);
      //rel_vel_heading = atan2(rel_vel_y, rel_vel_x);
      robot_vel_mag = hypot(robot_vel_x, robot_vel_y);
    }
    else{
      robot_vel_x = 0;
      robot_vel_y = 0;
    }

    fprintf(stderr, "Robot Velocity : %f\n" , robot_vel_mag); 

    approach_vel = - rel_vel_mag * cos(rel_vel_heading - heading_to_person); 

    fprintf(stderr, "Approach Velocity : %f\n", approach_vel);

    //calculating velocity relative to ground truth
    //carmen_point_t start_point = get_average_point_from_history(person_history_global, 
    carmen_point_t start_point_h = get_average_point_from_history_object(&(s->global_person_history),
									 0,
									 s->global_person_history.current_size/2.0-1);

    //carmen_point_t end_point = get_average_point_from_history(person_history_global, 
    carmen_point_t end_point_h = get_average_point_from_history_object(&(s->global_person_history),
								       s->global_person_history.current_size/2.0,
								       s->global_person_history.current_size-1);

    double dx = start_point_h.x - end_point_h.x; 
    //double dy = start_point_h.y - end_point_h.y; 

    double m = .0, c = .0;

    /*get_bestfit_from_history(person_history_global, 
      0,
      valid_history-1, &c, &m);*/
    get_bestfit_from_history_object(&(s->global_person_history),
				    0,
				    s->global_person_history.current_size-1, &c, &m);
				    

    best_fit_heading = atan(m); //this can point to either way - need to use the location of the points to extract the actual angle 

    //fprintf(stderr,"Start : %f,%f End : %f,%f => dx : %f dy: %f\n", start_point_h.x, start_point_h.y, end_point_h.x,  end_point_h.y, dx, dy);

    //calculating velocity relative to ground truth
    //carmen_point_t start_point = get_average_point_from_history(person_history_global, 
    carmen_point_t start_point = get_average_point_from_history_object(&(s->global_person_history_small),
								       0,
								       s->global_person_history_small.current_size/2.0-1);

    //carmen_point_t end_point = get_average_point_from_history(person_history_global, 
    carmen_point_t end_point = get_average_point_from_history_object(&(s->global_person_history_small),
								     s->global_person_history_small.current_size/2.0,
								     s->global_person_history_small.current_size-1);    
    if(dx < 0){//in the second or third quardrent 
      best_fit_heading = carmen_normalize_theta(best_fit_heading + M_PI);
    }
#ifdef PRINT_OP
    fprintf(stderr,"c : %f, m : %f: Angle : %f\n", c, m, best_fit_heading / M_PI*180);
#endif
    
    if(start_point.theta - end_point.theta >0){      
      vel_x = (start_point.x - end_point.x)/(start_point.theta - end_point.theta);
      vel_y = (start_point.y - end_point.y)/(start_point.theta - end_point.theta);
    }
    else{
      vel_x = 0;
      vel_y = 0;
    }
    //lets do an instantaenous calculation
    
    fprintf(stderr,"velocity : (%f,%f)\n", vel_x, vel_y);

    u_vel_x = vel_x;
    u_vel_y = vel_y;
    if(hypot(vel_x,vel_y)<= 0.2){
      //u_vel_x = prev_velocity.x;
      //u_vel_y = prev_velocity.y;
      //set to zero if small motions
      u_vel_x = 0;
      u_vel_y = 0;
    }

    if(hypot(vel_x,vel_y)> 0.2){
      prev_velocity.x = vel_x;
      prev_velocity.y = vel_y;
    }

    lcmgl = bot_lcmgl_init(s->lcm, "person_history");//globals_get_lcmgl("person_hsitory",1);
    if(lcmgl){
      lcmglColor3f(1.0, .0, 0);
      //lcmglBegin(GL_LINES);

      //draw the person heading also 
      
      for(int k=0;k<s->global_person_history.current_size;k++){

	float rtheta = s->global_robot_pose.theta;
	
	double pos[3] = {.0,.0,.0};
	double leg_xy[2] = {s->global_person_history.history[k].x, s->global_person_history.history[k].y};

	get_global_point(leg_xy, pos,s);
	
	//lcmglVertex3d(leg_x, leg_y, 2.0);
	lcmglColor3f(1.0, .0, .0);

	lcmglLineWidth (5);      
	lcmglCircle(pos, 0.02);
      }
      bot_lcmgl_switch_buffer (lcmgl);
    }
  }
  else{
    reset_point_avg_object(&(s->relative_person_history));
    reset_point_avg_object(&(s->global_person_history));   
  }

  //not sure if we should use this - if less than a certain value we should just set the magnitude to 0 
  double vel_mag = hypot(u_vel_x, u_vel_y);
  double vel_heading = atan2(u_vel_y,u_vel_x);
    
  //person_heading_average is pretty wrong when we smoothen it 
  if(vel_mag > 0.2){
    person_heading_average = best_fit_heading;//vel_heading;
  }
  //person_heading_average = get_average_double_from_history(heading_history, 0, valid_heading_history-1);//person_heading_average * (1 - learning_rate) + vel_heading * learning_rate;
  person_vel_average = person_vel_average * (1 - learning_rate) + vel_mag * learning_rate;


  lcmgl = bot_lcmgl_init(s->lcm, "velocity");//globals_get_lcmgl("velocity",1);
    
  if(lcmgl){
    //we should have a threshold to decide if the person is not moving 
    //if not moving maybe use the old qualifying velocities??
    double x_vel, y_vel;
    
    //x_vel = vel_mag* cos(person_heading_average);
    x_vel = 1.0* cos(person_heading_average);
    //y_vel = vel_mag* sin(person_heading_average);
    y_vel = 1.0* sin(person_heading_average);

    //double alpha = atan2(guide_pos.y, guide_pos.x);
    double rtheta = s->global_robot_pose.theta; //+ alpha;
    double vel_g_x = x_vel*cos(rtheta) - y_vel*sin(rtheta);
    double vel_g_y = x_vel*sin(rtheta) + y_vel*cos(rtheta);           
      
    //if(hypot(vel_x, vel_y)> 0.2){
    lcmglColor3f(0.0, 1.0, 0.2);
    //}
    //else{
    //lcmglColor3f(1.0, 0.0, 1.0);
    //}

    fprintf(stderr,"Guide GPOS %f,%f\n" , s->guide_gpos.x, s->guide_gpos.y);
      
    lcmglBegin(GL_LINES);
    lcmglVertex3d(s->guide_gpos.x, s->guide_gpos.y, 2.0);
    lcmglVertex3d(s->guide_gpos.x + vel_g_x, s->guide_gpos.y+ vel_g_y, 2.0);
    lcmglEnd();

    //temp calculation - done here 
    double alpha = atan2(y_vel, x_vel);

    //alternative positions
    double p1_alpha =  bot_mod2pi(alpha + M_PI/2);
    double p2_alpha =  bot_mod2pi(alpha - M_PI/2);

    double distance_to_side = 1.0;
     
    double p1_pos[3] = {.0,.0,.0};
    double p2_pos[3] = {.0,.0,.0};
 
    double p1_x = distance_to_side * cos(p1_alpha);
    double p1_y = distance_to_side * sin(p1_alpha);

    p1_pos[0] = s->guide_gpos.x + p1_x*cos(rtheta) - p1_y*sin(rtheta);
    p1_pos[1] = s->guide_gpos.y + p1_x*sin(rtheta) + p1_y*cos(rtheta);

    double p2_x = distance_to_side * cos(p2_alpha);
    double p2_y = distance_to_side * sin(p2_alpha);
      
    p2_pos[0] = s->guide_gpos.x + p2_x*cos(rtheta) - p2_y*sin(rtheta);
    p2_pos[1] = s->guide_gpos.y + p2_x*sin(rtheta) + p2_y*cos(rtheta);     


    lcmglLineWidth (8);    
    //left is red
    lcmglColor3f(1.0, 0.0, 0.0);
    lcmglCircle(p1_pos, 0.1);
    //right is 
    lcmglColor3f(1.0, 1.0, 0.0);
    lcmglCircle(p2_pos, 0.1);

    bot_lcmgl_switch_buffer (lcmgl);
  }

  //carmen_localize_update_person_heading(last_robot_position,robot_position,smoothed_heading_history, valid_heading_history-1);
  
  fprintf(stderr,"Person Heading Average : %f\n", person_heading_average);
  fprintf(stderr,"Getting smoothed History\n");
  //add_double_to_history(smoothed_heading_history, temp_smoothed_heading_history, person_heading_average);

  //double start_heading = get_average_double_from_history(smoothed_heading_history, 
  //								0,
  //							valid_heading_history/2.0-1);

  //double end_heading = get_average_double_from_history(smoothed_heading_history, 
  //						       valid_heading_history/2.0,
  //						       valid_heading_history-1);

  //forgoing the rv calculation - doesnt seem to be that accrate 

  /*lcmgl = globals_get_lcmgl("person_arc",1);

    if(elapsed_time > 0){
    rot_vel_mag = (start_heading - end_heading)/elapsed_time;
    double arc_radius = 10000.0;
    
    //rot_vel_mag = -0.5;//0.5;
    if(fabs(rot_vel_mag) >0){
    arc_radius = fabs(1.0 / rot_vel_mag);//fabs(vel_mag / rot_vel_mag);
    }
    fprintf(stderr, "Start Heading : %f - End Heading : %f : Elapsed Time : %f, Omega : %f Radius : %f\n", start_heading, end_heading, elapsed_time, rot_vel_mag, arc_radius);

    
    
    //lets do the calculation and draw the circle here 
    double lxy[2] = {.0,.0};
    double xyz[3] = {.0,.0,.0}; //center
    
    double ctheta = cos(person_heading_average);
    double stheta = sin(person_heading_average);
    if(rot_vel_mag > 0){
    lxy[0] = guide_pos.x - arc_radius * stheta;Ma
    lxy[1] = guide_pos.y + arc_radius * ctheta;      
    }
    else{//if(rot_vel_mag > 0){
    lxy[0] = guide_pos.x + arc_radius * stheta;
    lxy[1] = guide_pos.y - arc_radius * ctheta;      
    }
    get_global_point(lxy, xyz);
    
    bot_lcmgl_circle(lcmgl, xyz, arc_radius);
    }
    bot_lcmgl_switch_buffer (lcmgl);  */

#ifdef PRINT_OP
  fprintf(stderr,"Guide Msg\n");
#endif
  //publish_guide_msg(vel_mag, vel_heading, relative_vel);
  //publish_guide_msg(vel_mag, person_heading_average, relative_vel);

  //this should be the observation time 

  publish_guide_msg(time_obs, vel_mag, rot_vel_mag, person_heading_average, approach_vel, s);
  //publish_guide_msg(person_vel_average, person_heading_average, relative_vel);
 
  if(s->no_people >=0){//draw person if we have a person being tracked   
    lcmgl = bot_lcmgl_init(s->lcm, "person_zone");//globals_get_lcmgl("person_zone",1);

    //rel_vel_mag
    if(approach_vel >0){
      lcmglColor3f(1.0, .0, .0);
    }
    else{
      lcmglColor3f(1.0, .0, 1.0);
    }

    //2s distance 
    double radius = 1.0;//fmax(approach_vel * 2.0, 1.0);  //we will use a fixed radius 
    
    fprintf(stderr,"Safety Radius : %f\n", radius);
    
    double xyz[3] = { s->guide_gpos.x, s->guide_gpos.y , 0 };
    lcmglCircle(xyz, radius);

    bot_lcmgl_switch_buffer (lcmgl);

    lcmgl = bot_lcmgl_init(s->lcm, "person");//globals_get_lcmgl("person",1);

    double pos[3];

    for(i=0;i< s->no_people;i++){
      float person_x = .0, person_y = .0;
      float theta = s->global_robot_pose.theta;
      if(i==s->active_filter_ind){
	//person_x = guide_pos.x;
	//person_y = guide_pos.y;
	person_x = s->people_summary[i].mean.x;
	person_y = s->people_summary[i].mean.y;

      }
      else{
	person_x = s->people_summary[i].mean.x;
	person_y = s->people_summary[i].mean.y;
      }
      float goal_x = s->global_robot_pose.x + person_x * cos(theta) - person_y*sin(theta);
      float goal_y = s->global_robot_pose.y + person_x * sin(theta) + person_y*cos(theta);


      pos[0] = goal_x;
      pos[1] = goal_y;
      pos[2] = 2.0;  

      if (1 && lcmgl) {//turn to 1 if u want to draw
	lcmglLineWidth (5);
	if(i==s->active_filter_ind){
	  last_person_pos[0] = goal_x;
	  last_person_pos[1] = goal_y;

	  lcmglColor3f(1.0, .0, .0);
	}
	else{
	  lcmglColor3f(.0, 1.0, 1.0);
	}
	lcmglCircle(pos, 0.15);
      }
    }
    bot_lcmgl_switch_buffer (lcmgl);
  }
  
  fprintf(stderr,"No of filters %d \n", s->no_people);
  fprintf(stderr,"------------------------------------------------------------\n");
  free(person_det);
  free(full_person_det);

  for (i=0;i<meta_count;i++){
    free(dist[i]);
  }
  free(dist);

  for (j=0;j<original_filter_size;j++){
    free(allocated_observations[j]);
  }
  free(allocated_observations);
  free(no_obs_count);
  prev_time = time_obs;
  free(allocated_to_filter);
  free(used_person_obs.locations);
  free(prev_points);
  prev_points = points;
  prev_no_points = no_points;

  return 1;
}


void lcm_robotlaser_to_carmen_laser(erlcm_robot_laser_t *msg, carmen_robot_laser_message *robot, double laser_offset)
{
  //convert the lcm message to a carmen message because all the functions have been 
  //already written to handle carmen messages

  robot->config.accuracy = .1;
  robot->config.laser_type = HOKUYO_UTM;
  robot->config.remission_mode = REMISSION_NONE;
  robot->config.angular_resolution = msg->laser.radstep;
  robot->config.fov = msg->laser.nranges * msg->laser.radstep;
  robot->config.maximum_range = 30; 
  robot->config.start_angle = msg->laser.rad0;
  robot->num_readings = msg->laser.nranges;
  robot->range = msg->laser.ranges;
  robot->robot_pose.x = msg->pose.pos[0];
  robot->robot_pose.y = msg->pose.pos[1];
  double rpy[3] = {0,0,0};  
  bot_quat_to_roll_pitch_yaw (msg->pose.orientation, rpy) ;
  robot->robot_pose.theta = rpy[2];
  robot->laser_pose.x = robot->robot_pose.x + laser_offset*cos(rpy[2]);
  robot->laser_pose.y = robot->robot_pose.y + laser_offset*sin(rpy[2]);
  robot->laser_pose.theta = rpy[2];
  robot->timestamp = msg->utime/1e6;
}

void lcm_planar_laser_to_robot_laser(bot_core_planar_lidar_t *laser, erlcm_robot_laser_t *robot, 
				     state_t *s)
{

  /*
  if(robot !=NULL){
    if(robot->laser != NULL){
      bot_core_planar_lidar_t_destroy(robot->laser);
    }    
  }
  else{
    robot = calloc(1, sizeof(erlcm_robot_laser_t)); 
    }*/

  /*if(robot ==NULL){
    robot = calloc(1, sizeof(erlcm_robot_laser_t)); 
    fprintf(stderr, "Created Laser message\n"); 
    }*/

  memcpy(&(robot->laser),laser, sizeof(bot_core_planar_lidar_t));
  
  if(s->odom_msg!=NULL){
    robot->pose.pos[0] = s->odom_msg->x;//global_robot_pose.x;
    robot->pose.pos[1] = s->odom_msg->y;//global_robot_pose.y;
    
    double rpy[3] = {.0,.0,s->odom_msg->theta};
    bot_roll_pitch_yaw_to_quat (rpy, robot->pose.orientation) ;
  }

  robot->utime = laser->utime;
  fprintf(stderr, "Utime : %f\n", (double) (robot->utime)); 
}




void lcm_planar_laser_to_carmen_laser(bot_core_planar_lidar_t *laser, carmen_robot_laser_message *robot, 
				      double laser_offset, double laser_ang_offset, state_t *s)
{
  //convert the lcm message to a carmen message because all the functions have been 
  //already written to handle carmen messages

  robot->config.accuracy = .1;
  robot->config.laser_type = HOKUYO_UTM;
  robot->config.remission_mode = REMISSION_NONE;
  robot->config.angular_resolution = laser->radstep;
  robot->config.fov = laser->nranges * laser->radstep;
  robot->config.maximum_range = 30; 
  robot->config.start_angle = laser->rad0 + laser_ang_offset;
  robot->num_readings = laser->nranges;
  robot->range = laser->ranges;
  if(s->odom_msg!=NULL){
    robot->robot_pose.x = s->odom_msg->x;//global_robot_pose.x;
    robot->robot_pose.y = s->odom_msg->y;//global_robot_pose.y;
    robot->robot_pose.theta = carmen_normalize_theta(s->odom_msg->theta);//global_robot_pose.theta;
  }
  else{
    robot->robot_pose.x = .0;//global_robot_pose.x;
    robot->robot_pose.y = .0;//global_robot_pose.y;
    robot->robot_pose.theta = .0;//global_robot_pose.theta;
  }
  robot->laser_pose.x = robot->robot_pose.x + laser_offset*cos(robot->robot_pose.theta);
  robot->laser_pose.y = robot->robot_pose.y + laser_offset*sin(robot->robot_pose.theta);
  robot->laser_pose.theta = robot->robot_pose.theta;
  robot->timestamp = laser->utime/1e6;
}


static void multi_gridmap_handler(const lcm_recv_buf_t *rbuf, 
				  const char *channel, 
				  const erlcm_multi_gridmap_t *msg, 
				  void *user)
{
  state_t *s = (state_t *)user;
  fprintf(stderr,"=M= : Current Floor : %d\n",msg->current_floor_ind);

  static erlcm_gridmap_t* staticmsg = NULL;
  if (staticmsg != NULL) {
    erlcm_gridmap_t_destroy(staticmsg);
  }
  staticmsg = erlcm_gridmap_t_copy(&msg->maps[msg->current_floor_ind].gridmap);
  
  /*if(current_map !=NULL){
    erlcm_gridmap_t_destroy(current_map);
    }
    current_map =  erlcm_gridmap_t_copy(msg->maps[msg->current_floor].gridmap);	*/
  if (s->current_map.map != NULL)
    free(s->current_map.map);
  if (s->current_map.complete_map != NULL)
    free(s->current_map.complete_map);
  carmen3d_map_uncompress_lcm_map(&(s->current_map), staticmsg);
  fprintf(stderr,"Acquired gridmap\n");
}

static void mapserver_gridmap_handler(const lcm_recv_buf_t *rbuf, 
				      const char *channel, 
				      const erlcm_gridmap_t *msg, 
				      void *user)
{
  state_t *s = (state_t *)user; 
  static erlcm_gridmap_t* staticmsg = NULL;
  if (staticmsg != NULL) {
    erlcm_gridmap_t_destroy(staticmsg);
  }
  staticmsg = erlcm_gridmap_t_copy(msg);
  
  /*if(current_map !=NULL){
    erlcm_gridmap_t_destroy(current_map);
    }
    current_map =  erlcm_gridmap_t_copy(msg->maps[msg->current_floor].gridmap);	*/
  if (s->current_map.map != NULL)
    free(s->current_map.map);
  if (s->current_map.complete_map != NULL)
    free(s->current_map.complete_map);
  carmen3d_map_uncompress_lcm_map(&(s->current_map), staticmsg);
  s->current_map.map_zero.x = .0;
  s->current_map.map_zero.y = .0;
  fprintf(stderr,"Acquired gridmap\n");
}


void speech_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const erlcm_speech_cmd_t * msg,
		    void * user  __attribute__((unused)))
{
  state_t *s = (state_t *)user;
  static erlcm_speech_cmd_t* staticmsg = NULL;

  if (staticmsg != NULL) {
    erlcm_speech_cmd_t_destroy(staticmsg);
  }
  staticmsg = erlcm_speech_cmd_t_copy(msg);
  char* cmd = staticmsg->cmd_type;
  char* property = staticmsg->cmd_property;

  //person tracker related speech commands
  if(strcmp(cmd,"TRACKER")==0){
    if(strcmp(property,"START_LOOKING")==0){
      //guide has asked us to look for him
      update_tracking_state(LOOKING,s);
      fprintf(stderr,"Starting to track\n");
    }    
    else if(strcmp(property,"STATUS")==0){
      querry_tracking_state(s);
    }
  }
  if(strcmp(cmd,"FOLLOWER")==0){
    if(strcmp(property,"START_FOLLOWING")==0){
      //guide has asked us to follow him 
      //this we use only if we do not have the person in sight
      if(s->active_filter_ind ==-1){
	update_tracking_state(LOOKING,s);
	fprintf(stderr,"Starting to track\n");
      }
    }  
  }
}

void lcm_frontlaser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const erlcm_robot_laser_t * msg,
			    void * user  __attribute__((unused)))
{  
  state_t *s = (state_t *) user; 
  //static erlcm_robot_laser_t* staticmsg = NULL;

  if (s->robot_fl != NULL) {
    erlcm_robot_laser_t_destroy(s->robot_fl);
  }
  s->robot_fl = erlcm_robot_laser_t_copy(msg);

  //lcm_robotlaser_to_carmen_laser(s->robot_fl,&s->carmen_robot_frontlaser, s->param.front_laser_offset);
  s->have_front_laser = 1;

  fprintf(stderr,"Mode : %d\n", s->tourguide_mode);
  
  if(s->multi_people_tracking){
#ifdef PRINT_OP
    fprintf(stderr,"Multi Person tracking\n");
#endif
    //detect_person_both_multi(s);
    lcm_detect_person_both_basic(s);
  }
  else{
#ifdef PRINT_OP
    fprintf(stderr,"Single Person tracking\n");
#endif
    lcm_detect_person_both_basic(s);
  }

  double rpy[3] = {0,0,0};  
  bot_quat_to_roll_pitch_yaw (s->robot_fl->pose.orientation, rpy) ;

  s->last_robot_position.x = s->robot_fl->pose.pos[0];
  s->last_robot_position.y = s->robot_fl->pose.pos[1];
  s->last_robot_position.theta = rpy[2]; 
  
}

void lcm_rearlaser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const erlcm_robot_laser_t * msg,
			   void * user  __attribute__((unused)))
{
  state_t *s = (state_t *) user; 
  //  erlcm_robot_laser_t* staticmsg = s->robot_rl;

  if (s->robot_rl != NULL) {
    erlcm_robot_laser_t_destroy(s->robot_rl);
  }
  s->robot_rl = erlcm_robot_laser_t_copy(msg);

  //we only add update the rear laser data - we only run the localize once 
  //lcm_robotlaser_to_carmen_laser(staticmsg,&s->carmen_robot_rearlaser, s->param.rear_laser_offset);  

  s->have_rear_laser = 1;
  fprintf(stderr, "Have Rear Laser\n");
}

void lcm_base_odometry_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
			       const char * channel __attribute__((unused)), 
			       const erlcm_raw_odometry_msg_t * msg,
			       void * user  __attribute__((unused)))
{
  state_t *s = (state_t *) user; 
  //static erlcm_raw_odometry_msg_t* staticmsg = NULL;
#ifdef PRINT_OP
  fprintf(stderr,"ODOM\n");
#endif
  if (s->odom_msg != NULL) {
    erlcm_raw_odometry_msg_t_destroy(s->odom_msg);
  }
  s->odom_msg = erlcm_raw_odometry_msg_t_copy(msg);

  //lcm_odometry_to_carmen_odometry(staticmsg,&carmen_robot_latest_odometry);
}

void lcm_frontplanar_laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
				   const bot_core_planar_lidar_t * msg,
				   void * user  __attribute__((unused)))
{
  state_t *s = (state_t *)user; 
  

  if(s->robot_fl ==NULL){
    s->robot_fl = calloc(1, sizeof(erlcm_robot_laser_t)); 
  }

  if (s->planar_fl != NULL) {
    bot_core_planar_lidar_t_destroy(s->planar_fl);
  }
  s->planar_fl = bot_core_planar_lidar_t_copy(msg);

  memcpy(&(s->robot_fl->laser), s->planar_fl, sizeof(bot_core_planar_lidar_t));
  
  if(s->odom_msg!=NULL){
    s->robot_fl->pose.pos[0] = s->odom_msg->x;//global_robot_pose.x;
    s->robot_fl->pose.pos[1] = s->odom_msg->y;//global_robot_pose.y;
    
    double rpy[3] = {.0,.0,s->odom_msg->theta};
    bot_roll_pitch_yaw_to_quat (rpy, s->robot_fl->pose.orientation) ;
  }

  s->robot_fl->utime = s->planar_fl->utime;

  fprintf(stderr, "Utime : %f\n", (double) (s->robot_fl->utime)); 

  s->have_front_laser = 1;
  
  fprintf(stderr,"Mode : %d\n", s->tourguide_mode);

  //detect_person_both();
  if(s->multi_people_tracking){
    //detect_person_both_multi(s);
    lcm_detect_person_both_basic(s);
  }
  else{
    fprintf(stderr,"Single Person tracking\n");
    //detect_person_both_basic(s);
    lcm_detect_person_both_basic(s);
  }

  double rpy[3] = {0,0,0};  
  bot_quat_to_roll_pitch_yaw (s->robot_fl->pose.orientation, rpy) ;


  s->last_robot_position.x = s->robot_fl->pose.pos[0];
  s->last_robot_position.y = s->robot_fl->pose.pos[1];
  s->last_robot_position.theta = rpy[2]; 
}

void lcm_rearplanar_laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
				  const bot_core_planar_lidar_t * msg,
				  void * user  __attribute__((unused)))
{
  state_t *s = (state_t *)user; 
  bot_core_planar_lidar_t* staticmsg = s->planar_rl;

  if (staticmsg != NULL) {
    bot_core_planar_lidar_t_destroy(staticmsg);
  }
  staticmsg = bot_core_planar_lidar_t_copy(msg);

  //lcm_planar_laser_to_carmen_laser(staticmsg,&s->carmen_robot_rearlaser, s->param.rear_laser_offset, s->param.rear_laser_angle_offset, s);
  //lcm_planar_laser_to_robot_laser(staticmsg, s->robot_rl, s);
  if(s->robot_rl ==NULL){
    s->robot_rl = calloc(1, sizeof(erlcm_robot_laser_t)); 
  }

  memcpy(&(s->robot_rl->laser),staticmsg, sizeof(bot_core_planar_lidar_t));
  
  if(s->odom_msg!=NULL){
    s->robot_rl->pose.pos[0] = s->odom_msg->x;//global_robot_pose.x;
    s->robot_rl->pose.pos[1] = s->odom_msg->y;//global_robot_pose.y;
    
    double rpy[3] = {.0,.0,s->odom_msg->theta};
    bot_roll_pitch_yaw_to_quat (rpy, s->robot_rl->pose.orientation) ;
  }

  s->robot_rl->utime = staticmsg->utime;

  s->have_rear_laser = 1;
}



static void pose_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
			 const bot_core_pose_t * msg, void * user  __attribute__((unused))) 
{
  state_t *s = (state_t *)user; 
  static bot_core_pose_t *prevUpdate = NULL;

  if (prevUpdate == NULL) {
    prevUpdate = bot_core_pose_t_copy(msg);
  }

  double rpy[3];
  bot_quat_to_roll_pitch_yaw(msg->orientation, rpy);
  
  s->global_robot_pose.x = msg->pos[0];
  s->global_robot_pose.y = msg->pos[1];
  s->global_robot_pose.theta = rpy[2];

#ifdef PRINT_OP
  fprintf(stderr,"Pos (x,y,theta) : (%f,%f,%f)\n",
	  msg->pos[0],msg->pos[1],rpy[2]);    
#endif
}

void read_parameters(state_t *s)
{
  BotParam *c = s->b_server;
  char key[2048];
  int no_particles = 0;
  sprintf(key, "%s.no_particles", "person_tracking");

  //no particles 
  if (0 != bot_param_get_int(c, key, &no_particles)){
    fprintf(stderr,"\tError Reading Params\n");
  }else{
    fprintf(stderr,"\tNo of Particles : %d\n",no_particles);
  }
  s->param.num_particles = no_particles;
    
  //laser params
  double position[3];
  if(s->high_laser){
    sprintf(key, "%s.%s.%s.position", "calibration", "planar_lidars", "TOP_LASER");
  }
  else{
    sprintf(key, "%s.%s.%s.position", "calibration", "planar_lidars", "SKIRT_FRONT");
  }
  if(3 != bot_param_get_double_array(c, key, position, 3)){
    fprintf(stderr,"\tError Reading Params\n");
  }else{
    fprintf(stderr,"\tFront Laser Pos : (%f,%f,%f)\n",position[0], position[1], position[2]);
  }
  s->param.front_laser_offset = position[0];
  s->param.front_laser_side_offset = position[1];

  s->front_laser_offset.dx = position[0];
  s->front_laser_offset.dy = position[1];

  fprintf(stderr,"\tFront Laser Pos : %f,%f\n", s->param.front_laser_offset, 
	  s->param.front_laser_side_offset);
    
  sprintf(key, "%s.%s.%s.position", "calibration", "planar_lidars", "SKIRT_REAR");
  if(3 != bot_param_get_double_array(c, key, position, 3)){
    fprintf(stderr,"\tError Reading Params\n");
  }else{
    fprintf(stderr,"\tRear Laser Pos : (%f,%f,%f)\n",position[0], position[1], position[2]);
  }
  s->param.rear_laser_offset = position[0];
  s->rear_laser_offset.dx = position[0];
  s->rear_laser_offset.dy = position[1];

  double rpy[3];
  if(s->high_laser){
    sprintf(key, "%s.%s.%s.rpy", "calibration", "planar_lidars", "TOP_LASER");
  }
  else{
    sprintf(key, "%s.%s.%s.rpy", "calibration", "planar_lidars", "SKIRT_FRONT");
  }
  if(3 != bot_param_get_double_array(c, key, rpy, 3)){
    fprintf(stderr,"\tError Reading Params\n");
  }else{
    fprintf(stderr,"\tFront Laser RPY : (%f,%f,%f)\n",rpy[0], rpy[1], rpy[2]);
  }
  s->param.front_laser_angle_offset  = carmen_degrees_to_radians(rpy[2]);
  s->front_laser_offset.dtheta = carmen_degrees_to_radians(rpy[2]);

  sprintf(key, "%s.%s.%s.rpy", "calibration", "planar_lidars", "SKIRT_REAR");
  if(3 != bot_param_get_double_array(c, key, rpy, 3)){
    fprintf(stderr,"\tError Reading Params\n");
  }else{
    fprintf(stderr,"\tRear Laser RPY : (%f,%f,%f)\n",rpy[0], rpy[1], rpy[2]);
  }
  s->rear_laser_offset.dtheta = carmen_degrees_to_radians(rpy[2]);
  s->param.rear_laser_angle_offset = carmen_degrees_to_radians(rpy[2]);
}

void shutdown_tracker(int x)
{
  if(x == SIGINT) {
    carmen_verbose("Disconnecting from IPC network.\n");
    exit(1);
  }
}

int main(int argc, char **argv)
{
  //carmen3d_localize_param_t param;

  state_t *s = calloc(1, sizeof(state_t)); 
  s->clear_rearlaser = 1;
  s->high_laser = 1; 
  s->pruning_mode = 1; 

  int c;

  while ((c = getopt (argc, argv, "lfmhdrbP:")) >= 0) {
    switch (c) {
    case 'l':
      s->use_planar_laser = 1;
      break;
    case 'b':
      s->high_laser = 0;
      break;
    case 'f':
      s->multi_people_tracking = 1;
      fprintf(stderr,"Tracking multiple people\n");
      break;
    case 'm':
      s->use_map = 1;
      break;
    case 'd':
      s->debug_mode = 1;
      fprintf(stderr,"Debug");      
      break;      
    case 'r':
      s->clear_rearlaser = 1;
      fprintf(stderr,"Clearing rearlaser");
      break;
    case 'P':      
      s->pruning_mode = atoi(strdup(optarg));
      if(s->pruning_mode > 2){
	s->pruning_mode = 0;
	fprintf(stderr, "Invalid Pruning Mode - Select 0-2 \n(0 - no pruning, 1 - prune around guide, 2 - new pruning\n");  
      }
      fprintf(stderr,"Pruning Mode : %d\n", s->pruning_mode);
      break;      
    case 'h':
    case '?':
      fprintf (stderr, "Usage: %s [-l]\n\
                        Options:\n			\
                        -l     Use Planar Laser \n  \
                        -b     Use basic mode (use the front laser and rear laser)\n  \
                        -m     Use map to remove false observations\n  \
                        -d     Debug mode - prints some debug information\n \
                        -r     Clears observations from the rear laser - not working\n"
	       
	       , argv[0]);
    return 1;
    }
  }

  s->robot_fl = NULL;
  s->robot_rl = NULL;
  s->planar_fl = NULL;
  s->planar_rl = NULL; 

  memset(&(s->guide_pos), 0, sizeof(erlcm_point_t));
  memset(&(s->guide_gpos), 0, sizeof(erlcm_point_t));
  memset(&(s->carmen_robot_frontlaser),0,sizeof(s->carmen_robot_frontlaser));
  memset(&(s->carmen_robot_rearlaser),0,sizeof(s->carmen_robot_rearlaser));

  //these will move inside more structures 
  init_point_avg_object(&(s->relative_person_history), MAX_REL_PERSON_HISTORY);
  init_point_avg_object(&(s->global_person_history_small), MAX_REL_PERSON_HISTORY);
  init_point_avg_object(&(s->global_person_history), MAX_PERSON_HISTORY);
  init_double_avg_object(&(s->person_heading_history), MAX_PERSON_HISTORY); 
  init_point_avg_object(&(s->robot_history), MAX_REL_PERSON_HISTORY);

  s->current_map.map = NULL;
  s->current_map.complete_map = NULL;
  /* Setup exit handler */
  signal(SIGINT, shutdown_tracker);

  s->lcm = bot_lcm_get_global(NULL);//globals_get_lcm();
  
  s->b_server = bot_param_new_from_server(s->lcm, 1);
  
  s->lcmgl_moved_legs = bot_lcmgl_init(s->lcm, "moved_leg_obs");
  s->lcmgl_leg_obs = bot_lcmgl_init(s->lcm,"leg_obs");
  s->lcmgl_act_pos = bot_lcmgl_init(s->lcm, "act_pos");
  s->lcmgl_person_history = bot_lcmgl_init(s->lcm, "person_history");
  s->lcmgl_velocity = bot_lcmgl_init(s->lcm, "velocity");
  s->lcmgl_person_zone = bot_lcmgl_init(s->lcm, "person_zone");
  s->lcmgl_person = bot_lcmgl_init(s->lcm, "person");
  s->lcmgl_person_img = bot_lcmgl_init(s->lcm, "person_img");
  
  read_parameters(s);

  fprintf(stderr,"Parameters No of Particles : %d \n",s->param.num_particles);

  //std deviation of the gaussian distributed particles 
  s->std.x = 1.0;
  s->std.y = 1.0;
  s->std.theta = carmen_degrees_to_radians(4.0); 

  s->actual_filter_size = 5; 
  s->active_filter_ind = -1;

  s->people_filter = NULL; 
  s->people_summary = NULL; 
  s->time_since = NULL;
  s->people_positions = NULL; 
  s->people_pos = NULL;
  s->cr = NULL; 
  
  s->following_state = IDLE;
  s->tracking_state = NOT_LOOKING; 
  initialize_people_filters(s);
  
  s->global_robot_pose.x = 0.0;
  s->global_robot_pose.y = 0.0;
  s->global_robot_pose.theta = 0.0;

  fprintf(stderr,"Subscribing to LCM Laser\n");

  if(s->use_planar_laser){
    if(s->high_laser){
      bot_core_planar_lidar_t_subscribe(s->lcm,"TOP_LASER", lcm_frontplanar_laser_handler, s);
    }
    else{
      bot_core_planar_lidar_t_subscribe(s->lcm,"SKIRT_FRONT", lcm_frontplanar_laser_handler, s);
      bot_core_planar_lidar_t_subscribe(s->lcm,"SKIRT_REAR", lcm_rearplanar_laser_handler, s);
    }
    erlcm_raw_odometry_msg_t_subscribe(s->lcm, "ODOMETRY", lcm_base_odometry_handler,s);
  }
  else{
    if(!s->high_laser){
      erlcm_robot_laser_t_subscribe(s->lcm,"REAR_ROBOT_LASER", lcm_rearlaser_handler, s);
      erlcm_robot_laser_t_subscribe(s->lcm,"ROBOT_LASER", lcm_frontlaser_handler, s);
    }
  }
  bot_core_pose_t_subscribe(s->lcm, POSE_CHANNEL, pose_handler, s);
  if(s->use_map){
    fprintf(stderr,"Using the map to remove person observations\n");
    erlcm_multi_gridmap_t_subscribe(s->lcm, "MULTI_FLOOR_MAPS", multi_gridmap_handler, s); 
    erlcm_gridmap_t_subscribe(s->lcm, "MAP_SERVER", mapserver_gridmap_handler, s); 
  }

  //prob should handle only the tracker related stuff here
  erlcm_speech_cmd_t_subscribe(s->lcm, "PERSON_TRACKER", speech_handler, s);
  
  //for messages sent from the N810
  erlcm_speech_cmd_t_subscribe(s->lcm, "TABLET_FOLLOWER", speech_handler, s);

  erlcm_tagged_node_t_subscribe(s->lcm, "WHEELCHAIR_MODE", mode_handler, s);
#if 0
  bot_core_image_t_subscribe (s->lcm, "CAMLCM_IMAGE", on_image, s);
#endif

  //lets use glib laser 
  while (1)
    lcm_handle (s->lcm);

  return 0;
}
