#ifndef CARMEN_PERSON_MESSAGES_H
#define CARMEN_PERSON_MESSAGES_H

#ifdef __cplusplus
extern "C" {
#endif

  //These are messages 

typedef struct {
  carmen_point_t globalpos, globalpos_std, robot_pos;
  double timestamp;
  char *host;
} carmen_person_globalpos_message;

#define CARMEN_PERSON_GLOBALPOS_NAME "carmen_person_globalpos"
#define CARMEN_PERSON_GLOBALPOS_FMT  "{{double,double,double},{double,double,double},{double,double,double},double,string}"

typedef struct {
  int no_people;
  int followed_person;
  carmen_point_p person_pos; 
  double timestamp;
  char *host;
} carmen_person_people_message;

#define CARMEN_PERSON_PEOPLE_NAME "carmen_person_peoplepos"
#define CARMEN_PERSON_PEOPLE_FMT  "{int,int,<{double,double,double}:1>,double,string}"

typedef struct {
  int no;
  double *values;
  double timestamp;
  char *host;
} carmen_person_test_message;

#define CARMEN_PERSON_TEST_NAME "carmen_person_test"
#define CARMEN_PERSON_TEST_FMT  "{int,<double:1>,double,string}"


#ifdef __cplusplus
}
#endif

#endif
