#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <nodder/nodder_extractor.h>
#include <lcmtypes/erlcm_xyz_point_list_t.h>

#include <occ_map/VoxelMap.hpp>
#include <lcmtypes/occ_map_voxel_map_t.h>
#include <lcmtypes/erlcm_slam_full_graph_t.h>
#include <lcmtypes/er_lcmtypes.h>

#include <bot_lcmgl_client/lcmgl.h>

#include <velodyne/velodyne_extractor.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif 

#define VELODYNE_DATA_CIRC_SIZE 10

typedef struct _state_t state_t;

typedef struct {
    xyz_point_list_t *points;  
    int node_id; //we will use the node id to draw the map 
    double tf_pos[3]; 
    double theta;    
} point_pose_t;

struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    BotFrames *frames; 
    BotParam *param;
    char *log_filename;
    lcm_eventlog_t *read_log; 
    erlcm_slam_full_graph_t *slam_graph;
    
    GHashTable *point_hashtable;
    GHashTable *slam_nodes_hashtable;
    GHashTable *valid_ind_hashtable;
    
    BotPtrCircular   *nodder_data_circ;
    GArray   *point_cloud_array;      
    GArray   *slam_cloud_array;

    int node_selection_mode;
    int start_id;
    int end_id;
    //this could be a set of disjoint ids
    int do_filter; 
    int use_nodder;
    int draw;
    int publish_maps;
    int raytrace;
    occ_map::FloatVoxelMap *fvm;
    
    double p_pos[3];
    int p_size;
    int s_size;
    int verbose; 
    GMutex *mutex;
};

int is_valid(state_t *s, int id){
    if(s->node_selection_mode ==0)
        return 1;
    if(s->node_selection_mode ==1 && (id < s->start_id || id > s->end_id))
        return 0;
    if(s->node_selection_mode ==2){
        int *ind = (int *) g_hash_table_lookup(s->valid_ind_hashtable, &id);
        if(ind !=NULL)
            return 1;
        else return 0;
    }
        
    return 1;
}

void convert_slam_point_msg_to_point_pose_t(erlcm_slam_matched_point_list_t *slam_point_msg, point_pose_t *p_pose){
    p_pose->points = create_xyz_list( slam_point_msg->points.no);//(xyz_point_list_t *) calloc(1, sizeof(xyz_point_list_t));
    p_pose->points->utime = slam_point_msg->utime;
    p_pose->node_id = slam_point_msg->node_id;
    p_pose->tf_pos[0] = slam_point_msg->tf_pos[0];
    p_pose->tf_pos[1] = slam_point_msg->tf_pos[1];
    p_pose->tf_pos[2] = slam_point_msg->tf_pos[2];
    p_pose->theta = slam_point_msg->theta;
    memcpy(p_pose->points->points, slam_point_msg->points.points, p_pose->points->no_points *sizeof(erlcm_full_scan_point_t)); 
    
}

void
circ_free_nodder_data(void *user, void *p) {
    xyz_point_list_t *np = (xyz_point_list_t *) p; 
    destroy_xyz_list(np);
}

void get_rpy_frame(BotFrames *frame, int64_t utime, double rpy[3], char *from_frame, char *to_frame, double rpy_to_frame[3]){
    double quat_from_frame[4], quat_to_frame[4];
    bot_roll_pitch_yaw_to_quat(rpy, quat_from_frame);
    
    BotTrans from_to_trans;
    bot_frames_get_trans_with_utime (frame, from_frame, to_frame, utime, 
                                     &from_to_trans);
    
    bot_quat_mult (quat_to_frame, from_to_trans.rot_quat, quat_from_frame);
    bot_quat_to_roll_pitch_yaw (quat_to_frame, rpy_to_frame);

}

void publish_voxel_map(state_t *s){

    g_mutex_lock(s->mutex);
    
    //cant use this if things have changed 
    int64_t start_time = bot_timestamp_now();
    erlcm_slam_full_graph_t *msg = s->slam_graph;
    
    if(!msg){
        g_mutex_unlock(s->mutex);
        return;
    }

    double xyz0[3] = { 100, 100, -5 };
    double xyz1[3] = { -100, -100, 8 };
    double mpp[3] = { .1, .1, .1 };

    double dX =0, dY=0;

    double global_to_local[12];
    if (!bot_frames_get_trans_mat_3x4 (s->frames, "global",
                                       "local", // msg->utime,
                                       global_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        g_mutex_unlock(s->mutex);
        return;
    }

    GHashTableIter iter;
    gpointer key, value;
    
    if(s->verbose)
        fprintf(stderr,"Voxel Map \n");

    g_hash_table_iter_init (&iter, s->point_hashtable);
    while (g_hash_table_iter_next (&iter, &key, &value)){
        point_pose_t *p_list = (point_pose_t *) value;
        int *id = (int *) key;

        if(!is_valid(s,*id))
            continue;

        if(p_list){
            
            xyz_point_list_t *points = p_list->points;
            
            //find the matching slam node 
            erlcm_slam_full_graph_node_t *slam_node = (erlcm_slam_full_graph_node_t *) g_hash_table_lookup(s->slam_nodes_hashtable, id);

            if(!slam_node){
                continue;
            }
            
            if(s->verbose)
                fprintf(stderr, " %d No of Points : %d -> Match : %d\n", *id , p_list->points->no_points, slam_node->id);
        
            double slam_g[3] = {slam_node->pos[0], slam_node->pos[1], 0};
            double slam_l[3];

            bot_vector_affine_transform_3x4_3d (global_to_local, slam_g , slam_l);
        
            if(xyz0[0] > slam_l[0] - 10){
                xyz0[0] = slam_l[0] - 10;
            }
            if(xyz0[1] > slam_l[1] - 10){
                xyz0[1] = slam_l[1] - 10;
            }

            if(xyz1[0] < slam_l[0] + 10){
                xyz1[0] = slam_l[0] + 10;
            }
            if(xyz1[1] < slam_l[1] + 10){
                xyz1[1] = slam_l[1] + 10;
            }       
        }
    }

    if(s->verbose)
        fprintf(stderr, "Extent : %f,%f => %f,%f\n", xyz0[0], xyz0[1], xyz1[0], xyz1[1]);
    
    s->fvm = new occ_map::FloatVoxelMap(xyz0, xyz1, mpp, 0);

    int64_t end1_time = bot_timestamp_now();
    if(s->verbose)
        fprintf(stderr,"Time to create map : %f\n", (end1_time - start_time)/1.0e6);
    if(s->verbose)
        fprintf(stderr, "Size of Slam cloud : %d\n", s->s_size);
    
    g_hash_table_iter_init (&iter, s->point_hashtable);
    while (g_hash_table_iter_next (&iter, &key, &value)){
        point_pose_t *p_list = (point_pose_t *) value;
        int *id = (int *) key;

         if(!is_valid(s,*id))
            continue;

        xyz_point_list_t *points = p_list->points;
        if(!p_list)
            continue;

        erlcm_slam_full_graph_node_t *slam_node = (erlcm_slam_full_graph_node_t *) g_hash_table_lookup(s->slam_nodes_hashtable, id);

        if(!slam_node){
            continue;
        }

        //we know the global position of the anchor node 
        //convert the points to the global position and then convert to the local position 
        //figure out how to convert directly 
        BotTrans bodyToGlobal;

        dX = p_list->tf_pos[0];
        dY = p_list->tf_pos[1];
        double dT = p_list->theta;
        
        bodyToGlobal.trans_vec[0] = slam_node->pos[0] + dX * cos(slam_node->t) - dY * sin(slam_node->t);
        bodyToGlobal.trans_vec[1] = slam_node->pos[1] + dX * sin(slam_node->t) + dY * cos(slam_node->t);
        bodyToGlobal.trans_vec[2] = 0;

        double rpy[3] = { 0,0,  slam_node->t + dT};
        bot_roll_pitch_yaw_to_quat(rpy, bodyToGlobal.rot_quat);

        double pos_l[3], pos_b[3] = {0}, pos_g[3];
        //fprintf(stderr, "Size : %d\n", points->no_points);

        bot_trans_apply_vec(&bodyToGlobal, pos_b, pos_g);
        bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);

        pos_g[0] = slam_node->pos[0];
        pos_g[1] = slam_node->pos[1];
        pos_g[2] = 0;
        bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);
        pos_b[0] = 0;
        pos_b[1] = 0;
        pos_b[2] = 0;
        bot_trans_apply_vec(&bodyToGlobal, pos_b, pos_g);
        bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);

        double vp[3];

        vp[0] =  pos_g[0] + dX * cos(slam_node->t) 
            - dY * sin(slam_node->t);
        vp[1] =  pos_g[1] + dX * sin(slam_node->t) 
            + dY * cos(slam_node->t);
        vp[2] = 1.229;

        for (int k = 0; k < points->no_points; ++k){
          pos_b[0] = points->points[k].xyz[0];
          pos_b[1] = points->points[k].xyz[1];
          pos_b[2] = points->points[k].xyz[2];

          double xyz_s[3] = {0}, xyz_g[3] = {0}, xyz_l[3] = {0};
            
          xyz_s[0] = dX + pos_b[0] * cos(dT) - pos_b[1] * sin(dT); 
          xyz_s[1] = dY + pos_b[0] * sin(dT) + pos_b[1] * cos(dT); 
          xyz_s[2] = pos_b[2]; 
          
         
          xyz_g[0] =  pos_g[0] + xyz_s[0] * cos(slam_node->t) 
              - xyz_s[1] * sin(slam_node->t);
          xyz_g[1] =  pos_g[1] + xyz_s[0] * sin(slam_node->t) 
              + xyz_s[1] * cos(slam_node->t);
          xyz_g[2] = xyz_s[2];

          bot_vector_affine_transform_3x4_3d (global_to_local, xyz_g , xyz_l);
          
          //instead of this find sensor position and then do raytrace 
          
          if(s->fvm->isInMap(xyz_l)){
              if(s->raytrace){
                  s->fvm->raytrace(vp, xyz_l, -0.2, 0.99);
              }
              else{
                  s->fvm->writeValue(xyz_l,0.99);
              }
              //s->fvm->raytrace(vp, xyz_l, -1, 1);//0.99);
          }
        }
    }

    int64_t end2_time = bot_timestamp_now();
    if(s->verbose)
        fprintf(stderr,"Time to fill map : %f\n", (end2_time - end1_time)/1.0e6);
    
    
    const occ_map_voxel_map_t * m_msg = s->fvm->get_voxel_map_t(bot_timestamp_now());

    int64_t end3_time = bot_timestamp_now();
    if(s->verbose)
        fprintf(stderr,"Time to publish map : %f\n", (end3_time - end2_time)/1.0e6);

    occ_map_voxel_map_t_publish(s->lcm, "VOXEL_MAP",m_msg);
    delete s->fvm;

    g_mutex_unlock(s->mutex);
}



void draw_map_points(state_t *s, int filter){
    g_mutex_lock(s->mutex);

    erlcm_slam_full_graph_t *msg = s->slam_graph;
    int64_t start_time = bot_timestamp_now();
    if(!msg){
        g_mutex_unlock(s->mutex);
        return;
    }
    
    bot_lcmgl_t *lcmgl = bot_lcmgl_init(s->lcm, "slam_3d_points");
    bot_lcmgl_point_size(lcmgl, 2);
    bot_lcmgl_begin(lcmgl, GL_POINTS);
     
    GHashTableIter iter;
    gpointer key, value;
    double dX =0, dY=0;
    
    double global_to_local[12];
    if (!bot_frames_get_trans_mat_3x4 (s->frames, "global",
                                       "local", // msg->utime,
                                       global_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        g_mutex_unlock(s->mutex);
        return;
    }
    
    g_hash_table_iter_init (&iter, s->point_hashtable);
    while (g_hash_table_iter_next (&iter, &key, &value)){
        point_pose_t *p_list = (point_pose_t *) value;
        int *id = (int *) key;

        //if(*id < s->start_id || *id > s->end_id)
        if(!is_valid(s,*id))
            continue;

        xyz_point_list_t *points = p_list->points;
        if(!p_list)
            continue;

        erlcm_slam_full_graph_node_t *slam_node = (erlcm_slam_full_graph_node_t *) g_hash_table_lookup(s->slam_nodes_hashtable, id);

        if(!slam_node){
            continue;
        }

        //we know the global position of the anchor node 
        //convert the points to the global position and then convert to the local position 
        //figure out how to convert directly 
        BotTrans bodyToGlobal;

        dX = p_list->tf_pos[0];
        dY = p_list->tf_pos[1];
        double dT = p_list->theta;
        
        bodyToGlobal.trans_vec[0] = slam_node->pos[0] + dX * cos(slam_node->t) - dY * sin(slam_node->t);
        bodyToGlobal.trans_vec[1] = slam_node->pos[1] + dX * sin(slam_node->t) + dY * cos(slam_node->t);
        bodyToGlobal.trans_vec[2] = 0;

        double rpy[3] = { 0,0,  slam_node->t + dT};
        bot_roll_pitch_yaw_to_quat(rpy, bodyToGlobal.rot_quat);

        double pos_l[3], pos_b[3] = {0}, pos_g[3];
        //fprintf(stderr, "Size : %d\n", points->no_points);

        bot_trans_apply_vec(&bodyToGlobal, pos_b, pos_g);
        bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);

        pos_g[0] = slam_node->pos[0];
        pos_g[1] = slam_node->pos[1];
        pos_g[2] = 0;
        bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);
        pos_b[0] = 0;
        pos_b[1] = 0;
        pos_b[2] = 0;
        bot_trans_apply_vec(&bodyToGlobal, pos_b, pos_g);
        bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);
        
        for (int k = 0; k < points->no_points; ++k){
          pos_b[0] = points->points[k].xyz[0];
          pos_b[1] = points->points[k].xyz[1];
          pos_b[2] = points->points[k].xyz[2];

          double xyz_s[3] = {0}, xyz_g[3] = {0}, xyz_l[3] = {0};
            
          xyz_s[0] = dX + pos_b[0] * cos(dT) - pos_b[1] * sin(dT); 
          xyz_s[1] = dY + pos_b[0] * sin(dT) + pos_b[1] * cos(dT); 
          xyz_s[2] = pos_b[2]; 
          xyz_g[0] =  pos_g[0] + xyz_s[0] * cos(slam_node->t) 
              - xyz_s[1] * sin(slam_node->t);
          xyz_g[1] =  pos_g[1] + xyz_s[0] * sin(slam_node->t) 
              + xyz_s[1] * cos(slam_node->t);
          xyz_g[2] = xyz_s[2];

          bot_vector_affine_transform_3x4_3d (global_to_local, xyz_g , xyz_l);

          double z_norm = (xyz_l[2] + 2.0) / (5.0);
          float * jetC = bot_color_util_jet(z_norm);
          bot_lcmgl_color3f(lcmgl, jetC[0], jetC[1], jetC[2]);
          bot_lcmgl_vertex3f(lcmgl, xyz_l[0], xyz_l[1], xyz_l[2]);
        }
    }
 
    bot_lcmgl_end(lcmgl);

    bot_lcmgl_switch_buffer(lcmgl);

    int64_t end_time = bot_timestamp_now();
    if(s->verbose)
        fprintf(stderr,"Time to draw map : %f\n", (end_time - start_time)/1.0e6);

    g_mutex_unlock(s->mutex);
}    

static void point_request_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                         const char * channel __attribute__((unused)), 
                         const erlcm_slam_node_select_t * msg,
                         void * user  __attribute__((unused)))
{
    state_t *state = (state_t *)user;
    
    if(msg->type_of_nodes == ERLCM_SLAM_NODE_SELECT_T_NODES_ALL){
        state->start_id = 0;
        state->end_id = 100000;
        state->node_selection_mode = 0;
    }
    else if(msg->type_of_nodes == ERLCM_SLAM_NODE_SELECT_T_NODES_START_END && msg->no_nodes >=2
            && msg->node_ind[0] < msg->node_ind[1]){
        state->start_id = msg->node_ind[0];
        state->end_id = msg->node_ind[1];
        state->node_selection_mode = 1;
    }
    else if(msg->type_of_nodes == ERLCM_SLAM_NODE_SELECT_T_NODES_VALID_NODE){ 
        g_hash_table_remove_all (state->valid_ind_hashtable);
        for(int i=0; i<  msg->no_nodes; i++){
            int *p = (int *) calloc(1, sizeof(int));
            int *p_val = (int *) calloc(1, sizeof(int));
            *p = msg->node_ind[i];
            *p_val = msg->node_ind[i];
            g_hash_table_insert(state->valid_ind_hashtable, p, p_val);
        }
        state->node_selection_mode = 2;
    }

    if(msg->mode == ERLCM_SLAM_NODE_SELECT_T_PUBLISH_VOXEL_MAP){
        publish_voxel_map(state);
    }
    if(msg->mode == ERLCM_SLAM_NODE_SELECT_T_DRAW_ALL_POINTS){
        draw_map_points(state, 0);
    }
    if(msg->mode == ERLCM_SLAM_NODE_SELECT_T_DRAW_FILTERED_POINTS){
        draw_map_points(state, 1);
    }
}

int main(int argc, char **argv)
{
    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    state->lcm =  bot_lcm_get_global(NULL);

    state->param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->param);
    
    state->point_cloud_array = g_array_new (FALSE, TRUE, sizeof (xyz_point_list_t *));
    
    state->slam_cloud_array = g_array_new (FALSE, TRUE, sizeof (point_pose_t *));

    state->nodder_data_circ = bot_ptr_circular_new (VELODYNE_DATA_CIRC_SIZE,
                                                    circ_free_nodder_data, state);
    state->mainloop = g_main_loop_new( NULL, FALSE );  
    state->start_id = 0;
    state->end_id = 100000;

    const char *optstring = "vhdmnfl:s:e:r";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "verbose", no_argument, 0, 'v' }, 
                                  { "start", required_argument, 0, 's' }, 
                                  { "end", required_argument, 0, 'e' }, 
                                  { "logfile", required_argument, 0, 'l' }, 
                                  { "raytrace", required_argument, 0, 'r' }, 
                                  { "draw", no_argument, 0, 'd' }, 
                                  { "filter", no_argument, 0, 'f' }, 
                                  { "publish-maps", no_argument, 0, 'm' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c= getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    //usage(argv[0]);
            fprintf(stderr, "No help found - please add help\n");
	    break;
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
                state->verbose = 1;
		break;
	    }
        case 'r':
	    {
		fprintf(stderr,"Using raytrace\n");
                state->raytrace = 1;
		break;
	    }
        case 'f':
	    {
		fprintf(stderr,"filtering\n");
                state->do_filter = 1;
		break;
	    }
        case 'n':
	    {
		fprintf(stderr,"Using Nodder\n");
                state->use_nodder = 1;
		break;
	    }
        case 'l':
	    {
		fprintf(stderr,"Logging\n");
                state->log_filename = strdup(optarg);
		break;
	    }
        case 's':
	    {
                state->node_selection_mode = 1;
                state->start_id = atoi(optarg);
                fprintf(stderr,"start id : %d \n", state->start_id);
		break;
	    }
        case 'e':
            {
                state->node_selection_mode = 1;
                state->end_id = atoi(optarg);
                fprintf(stderr,"End id : %d \n", state->end_id);
		break;
	    }
            
        case 'd':
	    {
		fprintf(stderr,"Drawing\n");
                state->draw = 1;
		break;
	    }
            
        case 'm':
	    {
		fprintf(stderr,"Publishing Maps\n");
                state->publish_maps = 1;
		break;
	    }
        }
    }
  
    if(!state->log_filename){
        fprintf(stderr,"No log file given - exiting\n");
        return -1;
    }

    if(state->start_id > state->end_id){
        fprintf(stderr,"Start index greater than end index\n");
        return -1;
    }

    if (!state->mainloop){
	printf("Couldn't create main loop\n");
	return -1;
    }

    /*double xyz0[3] = { -10, -10, -0.5 };
    double xyz1[3] = { 10, 10, 3 };
    //double mpp[3] = { .02, .02, .02 };
    double mpp[3] = { .1, .1, .1 };*/
    
    state->fvm = NULL; //new occ_map::FloatVoxelMap(xyz0, xyz1, mpp, 0); 

    if(state->log_filename){        
        state->read_log = lcm_eventlog_create(state->log_filename, "r"); 
    }

    state->mutex = g_mutex_new ();
    
    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);
    
    char *points_channel_name = "SLAM_3D_POINTS";
    char *slam_graph_channel_name = "SLAM_FULL_GRAPH";

    lcm_eventlog_event_t *slam_event = NULL;
    state->slam_graph = NULL;

    state->point_hashtable = g_hash_table_new(g_int_hash, g_int_equal);
    state->slam_nodes_hashtable = g_hash_table_new(g_int_hash, g_int_equal);
    state->valid_ind_hashtable = g_hash_table_new_full(g_int_hash, g_int_equal,free, free); 
                                                       
    for (lcm_eventlog_event_t *event = lcm_eventlog_read_next_event (state->read_log);
         event != NULL;
         event = lcm_eventlog_read_next_event (state->read_log)) {
        
        // fprintf(stderr,"Channel : %s\n",event->channel);
      
        int decode_status = 0;
        if (strcmp (points_channel_name, event->channel) == 0) {
            erlcm_slam_matched_point_list_t *msg = (erlcm_slam_matched_point_list_t *) calloc(1, sizeof(erlcm_slam_matched_point_list_t));
            decode_status =  erlcm_slam_matched_point_list_t_decode (event->data, 0, event->datalen, msg);
            point_pose_t *p_pose = (point_pose_t *) calloc(1,sizeof(point_pose_t));
            
            convert_slam_point_msg_to_point_pose_t(msg, p_pose);
            
            g_hash_table_insert(state->point_hashtable, &p_pose->node_id, p_pose);

            //fprintf(stderr, "Decoding event : %s\n",  event->channel); 
            if (decode_status < 0)
                fprintf (stderr, "Error %d decoding message\n", decode_status);
            //we should hash them - be easier to render or publish 
        }
        
        if(strcmp (slam_graph_channel_name, event->channel) == 0) {
            
            if(state->slam_graph)
                erlcm_slam_full_graph_t_destroy(state->slam_graph);
            state->slam_graph = (erlcm_slam_full_graph_t *) calloc(1, sizeof(erlcm_slam_full_graph_t));
            //fprintf(stderr, "Decoding event : %s\n",  event->channel); 
            decode_status =  erlcm_slam_full_graph_t_decode (event->data, 0, event->datalen, state->slam_graph);
            if (decode_status < 0)
                fprintf (stderr, "Error %d decoding message\n", decode_status);
            //lcm_eventlog_free_event (event);
        }
        //else {
        lcm_eventlog_free_event (event);
        //}
    }

    fprintf(stderr, "Hashtable Size : %d\n", g_hash_table_size (state->point_hashtable)); 
    
   

    if(!state->slam_graph){
        fprintf(stderr, "No slam graph in log - exiting\n");
        return -1;
    }

    for(int i=0; i< state->slam_graph->no_nodes; i++){
        g_hash_table_insert(state->slam_nodes_hashtable, &state->slam_graph->nodes[i].id, &state->slam_graph->nodes[i]);
    }
    
    erlcm_slam_node_select_t_subscribe(state->lcm, "SLAM_3D_POINT_REQUEST", point_request_handler, state);
    
    if(state->publish_maps)
        publish_voxel_map(state);
    if(state->draw)
        draw_map_points(state,0);
        
    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
    
    bot_glib_mainloop_detach_lcm(state->lcm);

    if(state->read_log){
        lcm_eventlog_destroy (state->read_log);
    }
}
