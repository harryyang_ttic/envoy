#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <nodder/nodder_extractor.h>
#include <lcmtypes/erlcm_xyz_point_list_t.h>

#include <occ_map/VoxelMap.hpp>
#include <lcmtypes/occ_map_voxel_map_t.h>

typedef struct _state_t state_t;

struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
  BotFrames *frames; 
  BotParam *param;
    nodder_extractor_state_t *nodder; 
    int verbose; 
};

//doesnt do anything right now - timeout function
gboolean heartbeat_cb_1 (gpointer data)
{
    state_t *s = (state_t *)data;
    //fprintf(stderr, "Called \n"); 
    xyz_point_list_t *ret = nodder_extract_new_points(s->nodder);

    if(ret != NULL){
        if(s->verbose)
            fprintf(stderr, "Size of Points : %d \n", ret->no_points);

        erlcm_xyz_point_list_t msg;
        msg.utime = bot_timestamp_now(); 
        msg.no_points = ret->no_points;
    
        msg.points = (erlcm_xyz_point_t *)calloc(msg.no_points, sizeof(erlcm_xyz_point_t));

        for (size_t k = 0; k < ret->no_points; ++k){
            msg.points[k].xyz[0] = ret->points[k].xyz[0];//cloud_p->points[k].x; 
            msg.points[k].xyz[1] = ret->points[k].xyz[1]; 
            msg.points[k].xyz[2] = ret->points[k].xyz[2]; 
        }

        //publish
        erlcm_xyz_point_list_t_publish(s->lcm, "PCL_XYZ_LIST", &msg);
        free(msg.points);    

        destroy_xyz_list(ret);
    } 
    //return true to keep running
    return TRUE;
}

gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;
    //fprintf(stderr, "Called \n"); 
    xyz_point_list_t *ret = nodder_extract_new_points_frame(s->nodder, "local");

    if(ret != NULL){
        if(s->verbose)
            fprintf(stderr, "Size of Points : %d \n", ret->no_points);

        double xyz0[3] = { -20, -20, 0 };
        double xyz1[3] = { 20, 20, 5 };
        double mpp[3] = { .1, .1, .1 };
        occ_map::FloatVoxelMap fvm(xyz0, xyz1, mpp, 0);
        
        double ixyz[3];

        for (size_t k = 0; k < ret->no_points; ++k){
            if(fvm.isInMap(ret->points[k].xyz)){
                fvm.writeValue(ret->points[k].xyz,0.99);
            }
        }
        
        const occ_map_voxel_map_t * msg = fvm.get_voxel_map_t(bot_timestamp_now());
        occ_map_voxel_map_t_publish(s->lcm, "VOXEL_MAP",msg);
        
        destroy_xyz_list(ret);
    } 
    //return true to keep running
    return TRUE;
}


void nodder_cb(int64_t utime, void *data)
{
  state_t *s = (state_t *)data;
  //fprintf(stderr, "Called \n"); 
  xyz_point_list_t *ret = nodder_extract_new_points_frame(s->nodder, "local");

  if(ret != NULL){
    if(s->verbose)
      fprintf(stderr, "Size of Points : %d \n", ret->no_points);

    //find the size of the map based on the current position 
    //copy and convert 
    double body_to_local[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
						  "local",  ret->utime,
						  body_to_local)) {
      fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
      return;
    }
    
    double bot_body[3] = {0,0,0};
    double bot_local[3];

    bot_vector_affine_transform_3x4_3d (body_to_local, bot_body, bot_local);

    //actually we should get the extent of the scan here 

    //actually there isnt much useful info behind the robot 

    double xyz0[3] = { -10 + bot_local[0], -10 + bot_local[1], 0 };
    double xyz1[3] = { 10 + bot_local[0], 10 + bot_local[1] , 5 };
    double mpp[3] = { .1, .1, .1 };
    occ_map::FloatVoxelMap fvm(xyz0, xyz1, mpp, 0);
        
    double ixyz[3];

    for (size_t k = 0; k < ret->no_points; ++k){
      if(fvm.isInMap(ret->points[k].xyz)){
	fvm.writeValue(ret->points[k].xyz,0.99);
      }
    }
        
    const occ_map_voxel_map_t * msg = fvm.get_voxel_map_t(bot_timestamp_now());
    occ_map_voxel_map_t_publish(s->lcm, "VOXEL_MAP",msg);
        
    destroy_xyz_list(ret);
  }  
}

int main(int argc, char **argv)
{
    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    state->lcm =  bot_lcm_get_global(NULL);

    state->param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->param);

    state->nodder = nodder_extractor_init_full(state->lcm, 1, -40, 40, NULL, 
					       &nodder_cb, state);

    if(state->nodder== NULL){
        fprintf(stderr,"Issue getting collector\n");
        return -1;
    }

    state->mainloop = g_main_loop_new( NULL, FALSE );  

    const char *optstring = "vh";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "verbose", no_argument, 0, 'v' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    //usage(argv[0]);
            fprintf(stderr, "No help found - please add help\n");
	    break;
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
                state->verbose = 1;
		break;
	    }
        }
    }
  
    if (!state->mainloop){
	printf("Couldn't create main loop\n");
	return -1;
    }
    
    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);
    
    /* heart beat*/
    //g_timeout_add (100, heartbeat_cb, state);
    
    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
    
    bot_glib_mainloop_detach_lcm(state->lcm);
}
