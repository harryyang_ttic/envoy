#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <nodder/nodder_extractor.h>
#include <lcmtypes/erlcm_xyz_point_list_t.h>

#include <occ_map/VoxelMap.hpp>
#include <lcmtypes/occ_map_voxel_map_t.h>
#include <lcmtypes/erlcm_slam_full_graph_t.h>
#include <lcmtypes/er_lcmtypes.h>

#include <bot_lcmgl_client/lcmgl.h>

#include <velodyne/velodyne_extractor.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif 

#define VELODYNE_DATA_CIRC_SIZE 10

typedef struct _state_t state_t;

typedef struct {
    xyz_point_list_t *points;  
    int node_id; //we will use the node id to draw the map 
    double tf_pos[3]; 
    double theta;    
} point_pose_t;

struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    BotFrames *frames; 
    BotParam *param;
    char *log_filename;
    lcm_eventlog_t *write_log; 
    
    BotPtrCircular   *nodder_data_circ;
    GArray   *point_cloud_array;      
    GArray   *slam_cloud_array;      
    nodder_extractor_state_t *nodder;
    velodyne_extractor_state_t *velodyne;
    int do_filter; 
    int reduce_footprint;
    int use_nodder;
    int draw;
    int publish_maps;
    erlcm_slam_full_graph_t *slam_graph;
    //nodder_extractor_state_t *nodder;
    occ_map::FloatVoxelMap *fvm;
    
    double p_pos[3];
    int p_size;
    int s_size;
    int verbose; 
    GMutex *mutex;
};

void publish_3d_points(state_t *s, point_pose_t *p_pose){
    erlcm_slam_matched_point_list_t slam_point_msg; 
    slam_point_msg.utime =  p_pose->points->utime;
    slam_point_msg.node_id = p_pose->node_id;
    slam_point_msg.tf_pos[0] =  p_pose->tf_pos[0];
    slam_point_msg.tf_pos[1] =  p_pose->tf_pos[1];
    slam_point_msg.tf_pos[2] =  0;
    slam_point_msg.theta = p_pose->theta;
    slam_point_msg.points.no =  p_pose->points->no_points;
    slam_point_msg.points.points = (erlcm_full_scan_point_t *)calloc( p_pose->points->no_points, sizeof(erlcm_full_scan_point_t));
    memcpy(slam_point_msg.points.points, p_pose->points->points, p_pose->points->no_points *sizeof(erlcm_full_scan_point_t)); 
    erlcm_slam_matched_point_list_t_publish(s->lcm,"SLAM_3D_POINTS", &slam_point_msg);
    
    free(slam_point_msg.points.points);    
}

void save_to_log(state_t *s , const lcm_recv_buf_t *rbuf, const char *channel){
    if(!s->log_filename)
        return;
    fprintf(stderr, " +++++++ Called for channel : %s\n", channel);
    //writing to file 
    int channellen = strlen(channel);    
    int64_t mem_sz = sizeof(lcm_eventlog_event_t) + channellen + 1 + rbuf->data_size;
    
    lcm_eventlog_event_t *le = (lcm_eventlog_event_t*) malloc(mem_sz);
    memset(le, 0, mem_sz);
    
    le->timestamp = rbuf->recv_utime;
    le->channellen = channellen;
    le->datalen = rbuf->data_size;

    le->channel = ((char*)le) + sizeof(lcm_eventlog_event_t);
    strcpy(le->channel, channel);
    le->data = le->channel + channellen + 1;
    
    assert((char*)le->data + rbuf->data_size == (char*)le + mem_sz);
    memcpy(le->data, rbuf->data, rbuf->data_size);
    
    if(0 != lcm_eventlog_write_event(s->write_log, le)) {
        fprintf(stderr, "Error\n"); 
    }
    else{
        fprintf(stderr,"Saved to file\n");
    }
}

//a hack to get it to write to log file 
static void slam_point_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                         const char * channel __attribute__((unused)), 
                         const erlcm_slam_matched_point_list_t * msg,
                         void * user  __attribute__((unused)))
{
    state_t *s = (state_t *)user;
    save_to_log(s,rbuf, channel);
}

//seems to write but some issue with playing it back - dont use 
int write_to_log_slam_points(state_t *s, erlcm_slam_matched_point_list_t *s_msg, char *channel){   
    
    if(!s->write_log)
        return -1;
    
    int max_data_size = erlcm_slam_matched_point_list_t_encoded_size (s_msg);
    uint8_t *buf = (uint8_t*) malloc (max_data_size);
    if (!buf) return -1;
    int data_size = erlcm_slam_matched_point_list_t_encode (buf, 0, max_data_size, s_msg);
    if (data_size < 0) {
        free (buf);
        return data_size;
    }

    fprintf(stderr, "Data Size : %d\n", data_size);
   
    int channellen = strlen(channel);    
    fprintf(stderr, "Channel Length : %d\n", channellen);
    int64_t mem_sz = sizeof(lcm_eventlog_event_t) + channellen + 1 + data_size;
    
    lcm_eventlog_event_t *le = (lcm_eventlog_event_t*) malloc(mem_sz);
    memset(le, 0, mem_sz);
    
    le->timestamp = 0;//bot_timestamp_now();
    le->channellen = channellen;
    le->datalen = data_size;

    le->channel = ((char*)le) + sizeof(lcm_eventlog_event_t);
    strcpy(le->channel, channel);
    le->data = le->channel + channellen + 1;
    
    assert((char*)le->data + data_size == (char*)le + mem_sz);
    memcpy(le->data, buf, data_size);

    if(0 != lcm_eventlog_write_event(s->write_log, le)) {
        fprintf(stderr, "Error\n"); 
    }
    else{
        fprintf(stderr,"Saved to file\n");
    }

    free (buf);

    return 0;
}

void
circ_free_nodder_data(void *user, void *p) {
    xyz_point_list_t *np = (xyz_point_list_t *) p; 
    destroy_xyz_list(np);
}

void get_rpy_frame(BotFrames *frame, int64_t utime, double rpy[3], char *from_frame, char *to_frame, double rpy_to_frame[3]){
    double quat_from_frame[4], quat_to_frame[4];
    bot_roll_pitch_yaw_to_quat(rpy, quat_from_frame);
    
    BotTrans from_to_trans;
    bot_frames_get_trans_with_utime (frame, from_frame, to_frame, utime, 
                                     &from_to_trans);
    
    bot_quat_mult (quat_to_frame, from_to_trans.rot_quat, quat_from_frame);
    bot_quat_to_roll_pitch_yaw (quat_to_frame, rpy_to_frame);

}

void publish_voxel_map(state_t *s){
    int64_t start_time = bot_timestamp_now();
    erlcm_slam_full_graph_t *msg = s->slam_graph;
    
    if(!msg)
        return;

    double xyz0[3] = { 100, 100, -5 };
    double xyz1[3] = { -100, -100, 8 };
    //double mpp[3] = { .1, .1, .1 };
    double mpp[3] = { .1, .1, .1 };

    double dX =0, dY=0;

    double global_to_local[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "global",
                                                  "local",  msg->utime,
                                                  global_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;
    }

    int j = 0;

    fprintf(stderr, "Size of Slam cloud : %d\n", s->s_size);
    
    for (int i=0; i<(s->s_size); i++) {
        point_pose_t *p_list = g_array_index(s->slam_cloud_array, point_pose_t *, i);
        xyz_point_list_t *points = p_list->points;
        //find the size of the map based on the current position 
        //copy and convert 
        
        while(p_list->node_id != msg->nodes[j].id){
            if(j > msg->no_nodes-1){
                break;
            }
            j++;
        }
        
        double slam_g[3] = {msg->nodes[j].pos[0], msg->nodes[j].pos[1], 0};
        double slam_l[3];

        bot_vector_affine_transform_3x4_3d (global_to_local, slam_g , slam_l);
        
        if(xyz0[0] > slam_l[0] - 10){
            xyz0[0] = slam_l[0] - 10;
        }
         if(xyz0[1] > slam_l[1] - 10){
            xyz0[1] = slam_l[1] - 10;
        }

        if(xyz1[0] < slam_l[0] + 10){
            xyz1[0] = slam_l[0] + 10;
        }
        if(xyz1[1] < slam_l[1] + 10){
            xyz1[1] = slam_l[1] + 10;
        }       
    }
    
    fprintf(stderr, "Extent : %f,%f => %f,%f\n", xyz0[0], xyz0[1], xyz1[0], xyz1[1]);
    
    occ_map::FloatVoxelMap *fvm = new occ_map::FloatVoxelMap(xyz0, xyz1, mpp, 0);

    int64_t end1_time = bot_timestamp_now();
    fprintf(stderr,"Time to create map : %f\n", (end1_time - start_time)/1.0e6);

    j = 0;

    fprintf(stderr, "Size of Slam cloud : %d\n", s->s_size);
    
    for (int i=0; i<(s->s_size); i++) {
        point_pose_t *p_list = g_array_index(s->slam_cloud_array, point_pose_t *, i);
        xyz_point_list_t *points = p_list->points;
       
        
        while(p_list->node_id != msg->nodes[j].id){
            if(j > msg->no_nodes-1){
                break;
            }
            j++;
        }
        
        if(p_list->node_id != msg->nodes[j].id){
            fprintf(stderr, "Pose Not found\n");
            break;
        }

        //we know the global position of the anchor node 
        //convert the points to the global position and then convert to the local position 
        //figure out how to convert directly 
        BotTrans bodyToGlobal;

        dX = p_list->tf_pos[0];
        dY = p_list->tf_pos[1];
        double dT = p_list->theta;
        
        bodyToGlobal.trans_vec[0] = msg->nodes[j].pos[0] + dX * cos(msg->nodes[j].t) - dY * sin(msg->nodes[j].t);
        bodyToGlobal.trans_vec[1] = msg->nodes[j].pos[1] + dX * sin(msg->nodes[j].t) + dY * cos(msg->nodes[j].t);
        bodyToGlobal.trans_vec[2] = 0;

        double rpy[3] = { 0,0,  msg->nodes[j].t + dT};
        bot_roll_pitch_yaw_to_quat(rpy, bodyToGlobal.rot_quat);

        double pos_l[3], pos_b[3] = {0}, pos_g[3];

        bot_trans_apply_vec(&bodyToGlobal, pos_b, pos_g);
        bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);

        pos_g[0] = msg->nodes[j].pos[0];
        pos_g[1] = msg->nodes[j].pos[1];
        pos_g[2] = 0;
        bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);
        pos_b[0] = 0;
        pos_b[1] = 0;
        pos_b[2] = 0;
        bot_trans_apply_vec(&bodyToGlobal, pos_b, pos_g);
        bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);

        double vp[3];

        vp[0] =  pos_g[0] + dX * cos(msg->nodes[j].t) 
            - dY * sin(msg->nodes[j].t);
        vp[1] =  pos_g[1] + dX * sin(msg->nodes[j].t) 
            + dY * cos(msg->nodes[j].t);
        vp[2] = 1.229;
        
        for (int k = 0; k < points->no_points; ++k){
          pos_b[0] = points->points[k].xyz[0];
          pos_b[1] = points->points[k].xyz[1];
          pos_b[2] = points->points[k].xyz[2];

          double xyz_s[3] = {0}, xyz_g[3] = {0}, xyz_l[3] = {0};
            
          xyz_s[0] = dX + pos_b[0] * cos(dT) - pos_b[1] * sin(dT); 
          xyz_s[1] = dY + pos_b[0] * sin(dT) + pos_b[1] * cos(dT); 
          xyz_s[2] = pos_b[2]; 
          xyz_g[0] =  msg->nodes[j].pos[0] + xyz_s[0] * cos(msg->nodes[j].t) 
              - xyz_s[1] * sin(msg->nodes[j].t);
          xyz_g[1] =  msg->nodes[j].pos[1] + xyz_s[0] * sin(msg->nodes[j].t) 
              + xyz_s[1] * cos(msg->nodes[j].t);
          xyz_g[2] = xyz_s[2];

          bot_vector_affine_transform_3x4_3d (global_to_local, xyz_g , xyz_l);
          if(fvm->isInMap(xyz_l)){
              fvm->writeValue(xyz_l,0.99);
              //fvm->raytrace(vp, xyz_l, -0.2, 0.99);
          }
        }
    }

    int64_t end2_time = bot_timestamp_now();
    fprintf(stderr,"Time to fill map : %f\n", (end2_time - end1_time)/1.0e6);
    
    
    const occ_map_voxel_map_t * m_msg = fvm->get_voxel_map_t(bot_timestamp_now());

    int64_t end3_time = bot_timestamp_now();
    fprintf(stderr,"Time to publish map : %f\n", (end3_time - end2_time)/1.0e6);

    occ_map_voxel_map_t_publish(s->lcm, "VOXEL_MAP",m_msg);
    delete fvm;     
}

static void slam_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                         const char * channel __attribute__((unused)), 
                         const erlcm_slam_full_graph_t * msg,
                         void * user  __attribute__((unused)))
{

    state_t *s = (state_t *)user;

    g_mutex_lock(s->mutex);

    if(s->slam_graph){
        erlcm_slam_full_graph_t_destroy(s->slam_graph);
    }
    s->slam_graph = erlcm_slam_full_graph_t_copy(msg);
    
    erlcm_slam_full_graph_node_t *last_node = &msg->nodes[msg->no_nodes-1];

    save_to_log(s, rbuf, channel);

    double time_gap = 10000000.0;
    int c_ind = -1;
    int64_t match_utime = 0;

    //we should make it a circular buffer
    fprintf(stderr, "Buffer size : %d\n" , bot_ptr_circular_size(s->nodder_data_circ));
    for (int i=0; i<(bot_ptr_circular_size(s->nodder_data_circ)); i++) {
        xyz_point_list_t *points = (xyz_point_list_t *) bot_ptr_circular_index(s->nodder_data_circ, i);
        if(fabs(last_node->utime - points->utime)/1.0e6 < time_gap){
            time_gap = fabs(last_node->utime - points->utime)/ 1.0e6;
            c_ind = i;
            match_utime = points->utime;
        }
    }

    fprintf(stderr, "Time gap %f -> ind :%d\n", time_gap, c_ind);

    if(c_ind == -1 || time_gap > 1.0){
        fprintf(stderr,"No Close node found\n");
        g_mutex_unlock(s->mutex);
        return;
    }
    
    double b_to_local_node_time[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                  "local",  last_node->utime, 
                                                  b_to_local_node_time)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;
    }
    
    //find the transform from the slam pos to the node point pos 
    double b_to_local[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                  "local",  match_utime, //p_pose->points->utime, 
                                                  b_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;
    }

    double slam_pose[3] = {last_node->pos[0], last_node->pos[1], 0}, local_slam_pose[3];
    double body_pose[3]= {0}, body_local_pose[3];

    bot_vector_affine_transform_3x4_3d (b_to_local_node_time, body_pose, local_slam_pose);

    bot_vector_affine_transform_3x4_3d (b_to_local, body_pose, body_local_pose);
    
    fprintf(stderr, "Local Pose for SLAM node : %f,%f => %f,%f\n", local_slam_pose[0], local_slam_pose[1], 
            body_local_pose[0], body_local_pose[1]);

    double dX = body_local_pose[0] - local_slam_pose[0];
    double dY = body_local_pose[1] - local_slam_pose[1];

    fprintf(stderr,"Transform size : %f\n", hypot(dX,dY));

    if(hypot(dX,dY) > 0.5){
        fprintf(stderr,"large transform - returning\n");
        g_mutex_unlock(s->mutex);
        return;
    }

    double rpy_slam_body[3] = {0,0, 0}, rpy_slam_local[3];

    get_rpy_frame(s->frames, last_node->utime, rpy_slam_body, "body", "local", rpy_slam_local);
    
    double rpy_bot_body[3] = {0}, rpy_bot_local[3];
    get_rpy_frame(s->frames, match_utime, rpy_bot_body, "body", "local", rpy_bot_local);
    
    if(bot_mod2pi(rpy_bot_local[2] - rpy_slam_local[2]) > 0.5){
        fprintf(stderr,"Too large a theta\n");
        g_mutex_unlock(s->mutex);
        return;
    }
    
    point_pose_t *p_pose = (point_pose_t *) calloc(1, sizeof(point_pose_t));

    p_pose->points = copy_xyz_list((xyz_point_list_t *)bot_ptr_circular_index(s->nodder_data_circ, c_ind));
    p_pose->node_id = last_node->id;

    p_pose->tf_pos[0] = dX * cos(rpy_slam_local[2]) + dY * sin(rpy_slam_local[2]);
    p_pose->tf_pos[1] = -dX * sin(rpy_slam_local[2]) + dY * cos(rpy_slam_local[2]);

    p_pose->theta = bot_mod2pi(rpy_bot_local[2] - rpy_slam_local[2]);

    fprintf(stderr, "Diff %f,%f - theta : %f\n", p_pose->tf_pos[0], p_pose->tf_pos[1], p_pose->theta);

    
    publish_3d_points(s, p_pose);

    if(s->reduce_footprint){
        //delete the allocated mem 
        destroy_xyz_list(p_pose->points);
        free(p_pose);
        g_mutex_unlock(s->mutex);
        return;
    }
    
    g_array_append_val (s->slam_cloud_array, p_pose);

    s->s_size++;

    if(s->publish_maps){
        fprintf(stderr, "Publishing Voxel map\n");
        publish_voxel_map(s);
    }

    g_mutex_unlock(s->mutex);
}


static void slam_handler_draw(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                         const char * channel __attribute__((unused)), 
                         const erlcm_slam_full_graph_t * msg,
                         void * user  __attribute__((unused)))
{

    state_t *s = (state_t *)user;

    g_mutex_lock(s->mutex);

    save_to_log(s, rbuf, channel);

    
    erlcm_slam_full_graph_node_t *last_node = &msg->nodes[msg->no_nodes-1];

    double time_gap = 10000000.0;
    int c_ind = -1;

    int64_t start_time = bot_timestamp_now();

    int64_t match_utime = 0;

    //we should make it a circular buffer
    fprintf(stderr, "Buffer size : %d\n" , bot_ptr_circular_size(s->nodder_data_circ));
    for (int i=0; i<(bot_ptr_circular_size(s->nodder_data_circ)); i++) {
        xyz_point_list_t *points = (xyz_point_list_t *) bot_ptr_circular_index(s->nodder_data_circ, i);
        if(fabs(last_node->utime - points->utime)/1.0e6 < time_gap){
            time_gap = fabs(last_node->utime - points->utime)/ 1.0e6;
            c_ind = i;
            match_utime = points->utime;
        }
    }

    fprintf(stderr, "Time gap %f -> ind :%d\n", time_gap, c_ind);

    if(c_ind == -1 || time_gap > 0.5){
        fprintf(stderr,"No Close node found\n");
        g_mutex_unlock(s->mutex);
        return;
    }

    double b_to_local_node_time[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                  "local",  last_node->utime, 
                                                  b_to_local_node_time)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;
    }

    //find the transform from the slam pos to the node point pos 
    double b_to_local[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                  "local",  match_utime, //p_pose->points->utime, 
                                                  b_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;
    }

    double local_slam_pose[3];
    double body_pose[3]= {0}, body_local_pose[3];

    bot_vector_affine_transform_3x4_3d (b_to_local_node_time, body_pose, local_slam_pose);

    bot_vector_affine_transform_3x4_3d (b_to_local, body_pose, body_local_pose);
    
    fprintf(stderr, "Local Pose for SLAM node : %f,%f => %f,%f\n", local_slam_pose[0], local_slam_pose[1], 
            body_local_pose[0], body_local_pose[1]);

    double dX = body_local_pose[0] - local_slam_pose[0];
    double dY = body_local_pose[1] - local_slam_pose[1];

    fprintf(stderr,"Transform size : %f\n", hypot(dX,dY));

    if(hypot(dX,dY) > 0.5){
        fprintf(stderr,"large transform - returning\n");
        g_mutex_unlock(s->mutex);
        return;
    }

    double rpy_slam_body[3] = {0,0, 0}, rpy_slam_local[3];

    //get_rpy_frame(s->frames, last_node->utime, rpy_slam_global, "global", "local", rpy_slam_local);
    get_rpy_frame(s->frames, last_node->utime, rpy_slam_body, "body", "local", rpy_slam_local);
    

    
    double rpy_bot_body[3] = {0}, rpy_bot_local[3];
    get_rpy_frame(s->frames, match_utime, rpy_bot_body, "body", "local", rpy_bot_local);
    
    if(bot_mod2pi(rpy_bot_local[2] - rpy_slam_local[2]) > 0.5){
        fprintf(stderr,"Too large a theta\n");
        g_mutex_unlock(s->mutex);
        return;
    }
    
    point_pose_t *p_pose = (point_pose_t *) calloc(1, sizeof(point_pose_t));

    p_pose->tf_pos[0] = dX * cos(rpy_slam_local[2]) + dY * sin(rpy_slam_local[2]);
    p_pose->tf_pos[1] = -dX * sin(rpy_slam_local[2]) + dY * cos(rpy_slam_local[2]);

    //if this theta is too large prob should forget about these points 
    p_pose->theta = bot_mod2pi(rpy_bot_local[2] - rpy_slam_local[2]);

    p_pose->points = copy_xyz_list((xyz_point_list_t *)bot_ptr_circular_index(s->nodder_data_circ, c_ind));
    p_pose->node_id = last_node->id;

    fprintf(stderr,"Transform size : %f\n", hypot(dX,dY));

    if(hypot(dX,dY) > 0.5){
        fprintf(stderr,"large transform - returning\n");
        g_mutex_unlock(s->mutex);
        return;
    }

    fprintf(stderr, "+++++ Diff %f,%f - theta : %f\n", p_pose->tf_pos[0], p_pose->tf_pos[1], p_pose->theta);

    publish_3d_points(s, p_pose);

    if(s->reduce_footprint){
        //delete the allocated mem 
        destroy_xyz_list(p_pose->points);
        free(p_pose);
        g_mutex_unlock(s->mutex);
        return;
    }

    g_array_append_val (s->slam_cloud_array, p_pose);

    s->s_size++;

    double global_to_local[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "global",
                                                  "local",  msg->utime,
                                                  global_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        g_mutex_unlock(s->mutex);
        return;
    }

    int j = 0;

    fprintf(stderr, "Size of Slam cloud : %d\n", s->s_size);
    bot_lcmgl_t *lcmgl = bot_lcmgl_init(s->lcm, "slam_3d_points");

    for (int i=0; i<(s->s_size); i++) {

        bot_lcmgl_point_size(lcmgl, 2);

        point_pose_t *p_list = g_array_index(s->slam_cloud_array, point_pose_t *, i);

        xyz_point_list_t *points = p_list->points;
        //find the size of the map based on the current position 
        //copy and convert 
        double global_to_local[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "global",
                                                      "local",  msg->utime,
                                                      global_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            g_mutex_unlock(s->mutex);
            return;
        }
        
        while(p_list->node_id != msg->nodes[j].id){
            if(j > msg->no_nodes-1){
                break;
            }
            j++;
        }
        
        if(p_list->node_id != msg->nodes[j].id){
            fprintf(stderr, "Pose Not found\n");
            break;
        }

        //we know the global position of the anchor node 
        //convert the points to the global position and then convert to the local position 
        //figure out how to convert directly 
        BotTrans bodyToGlobal;

        dX = p_list->tf_pos[0];
        dY = p_list->tf_pos[1];
        double dT =  p_list->theta;

        fprintf(stderr, "[%d] - Dist : %f Theta : %f Delta T : %f\n", i, hypot(dX,dY), bot_to_degrees(dT), (p_list->points->utime - msg->nodes[j].utime)/1.0e6);
        
        bodyToGlobal.trans_vec[0] = msg->nodes[j].pos[0] + dX * cos(msg->nodes[j].t) - dY * sin(msg->nodes[j].t);
        bodyToGlobal.trans_vec[1] = msg->nodes[j].pos[1] + dX * sin(msg->nodes[j].t) + dY * cos(msg->nodes[j].t);
        bodyToGlobal.trans_vec[2] = 0;

        double rpy[3] = { 0,0,  msg->nodes[j].t + dT};//p_list->theta};//slampose->rp[0], slampose->rp[1], value.t() };
        bot_roll_pitch_yaw_to_quat(rpy, bodyToGlobal.rot_quat);
        
        double pos_l[3], pos_b[3] = {0}, pos_g[3];

        bot_lcmgl_color3f(lcmgl, 0, 1.0, 0);
        bot_trans_apply_vec(&bodyToGlobal, pos_b, pos_g);
        bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);

        bot_lcmgl_begin(lcmgl, GL_LINES);
        bot_lcmgl_vertex3f(lcmgl, pos_l[0], pos_l[1], pos_l[2]);
        pos_g[0] = msg->nodes[j].pos[0];
        pos_g[1] = msg->nodes[j].pos[1];
        pos_g[2] = 0;
        bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);
        bot_lcmgl_vertex3f(lcmgl, pos_l[0], pos_l[1], pos_l[2]);

        bot_lcmgl_end(lcmgl);

        bot_lcmgl_point_size(lcmgl, 2);
        
        bot_lcmgl_begin(lcmgl, GL_POINTS);

        bot_lcmgl_color3f(lcmgl, 0, 1.0, 0);
        pos_b[0] = 0;
        pos_b[1] = 0;
        pos_b[2] = 0;
        bot_trans_apply_vec(&bodyToGlobal, pos_b, pos_g);
        bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);
        bot_lcmgl_vertex3f(lcmgl, pos_l[0], pos_l[1], pos_l[2]);

        bot_lcmgl_color3f(lcmgl, 1, 0, 0);

        int no_points = 20;

        for (int k = 0; k < points->no_points; ++k){
            double z_norm = (points->points[k].xyz[2] + 2.0) / (5.0);
            float * jetC = bot_color_util_jet(z_norm);
            bot_lcmgl_color3f(lcmgl, jetC[0], jetC[1], jetC[2]);
            pos_b[0] = points->points[k].xyz[0];
            pos_b[1] = points->points[k].xyz[1];
            pos_b[2] = points->points[k].xyz[2];
            
            double xyz_s[3] = {0}, xyz_g[3] = {0}, xyz_l[3] = {0};
            
            xyz_s[0] = dX + pos_b[0] * cos(dT) - pos_b[1] * sin(dT); 
            xyz_s[1] = dY + pos_b[0] * sin(dT) + pos_b[1] * cos(dT); 
            xyz_s[2] = pos_b[2]; 
            //bot_trans_apply_vec(&bodyToGlobal, pos_b, pos_g);
            xyz_g[0] =  msg->nodes[j].pos[0] + xyz_s[0] * cos(msg->nodes[j].t) 
                - xyz_s[1] * sin(msg->nodes[j].t);
            xyz_g[1] =  msg->nodes[j].pos[1] + xyz_s[0] * sin(msg->nodes[j].t) 
              + xyz_s[1] * cos(msg->nodes[j].t);
            xyz_g[2] = xyz_s[2];
            
            bot_vector_affine_transform_3x4_3d (global_to_local, xyz_g , xyz_l);
            bot_lcmgl_vertex3f(lcmgl, xyz_l[0], xyz_l[1], xyz_l[2]);
        }

        bot_lcmgl_end(lcmgl);
    }

    bot_lcmgl_switch_buffer(lcmgl);

    int64_t end_time = bot_timestamp_now();
    fprintf(stderr,"Time to draw map : %f\n", (end_time - start_time)/1.0e6);

    g_mutex_unlock(s->mutex);
}    

static void
on_laser (const lcm_recv_buf_t *rbuf, const char *channel,
          const bot_core_planar_lidar_t *msg, void *user)
{
    state_t *s = (state_t *)user;
    g_mutex_lock(s->mutex);

    

    double body_to_local[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                  "local",  msg->utime,
                                                  body_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        g_mutex_unlock(s->mutex);
        return;
    }
    
    double b_pos[3] = {0}, g_pos[3];
    bot_vector_affine_transform_3x4_3d (body_to_local, b_pos, g_pos);
    double dist = hypot(g_pos[0] - s->p_pos[0], g_pos[1] - s->p_pos[1]);

    //fprintf(stderr, "Dist : %f\n", dist);
    
    if(dist < 1.0){
        g_mutex_unlock(s->mutex);
        return;
    }

    s->p_pos[0] = g_pos[0];
    s->p_pos[1] = g_pos[1];
        
    xyz_point_list_t *ret = (xyz_point_list_t *) calloc(1, sizeof(xyz_point_list_t));
    ret->points = (xyz_point_t *) calloc(msg->nranges , sizeof(xyz_point_t)); 
    ret->utime = msg->utime;

    double sensor_to_body[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "SKIRT_FRONT",
                                                  "body",  msg->utime,
                                                  sensor_to_body)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        g_mutex_unlock (s->mutex);
        return;        
    }

    int k=0;

    for (int i=0; i<(msg->nranges); i++) {
            double theta = msg->rad0 + 
                (double)i * msg->radstep;

            double xyz_sensor[3] = {msg->ranges[i] * cos(theta), msg->ranges[i]*sin(theta), 0}, xyz_local[3];

            if(theta < -M_PI/2 || theta > M_PI/2)
                continue;

            bot_vector_affine_transform_3x4_3d (sensor_to_body, xyz_sensor, ret->points[k].xyz);
            k++; 
    }

    ret->no_points = k; 
    fprintf(stderr," New Size : %d\n", k);
    ret->points = (xyz_point_t *) realloc(ret->points , k * sizeof(xyz_point_t));

    bot_ptr_circular_add (s->nodder_data_circ, ret);

    erlcm_slam_node_init_t s_msg; 
    s_msg.utime = ret->utime;
    s_msg.sender = 1;
    erlcm_slam_node_init_t_publish(s->lcm, "SLAM_NODE_INIT", &s_msg);
    
    fprintf(stderr, "Request Time : %f\n", ret->utime/1.0e6);
    g_mutex_unlock(s->mutex);
}

void point_source_cb(int64_t utime, void *data)
{
    state_t *s = (state_t *)data;
    g_mutex_lock(s->mutex);
    //fprintf(stderr, "Called \n"); 
    
    xyz_point_list_t *ret =  NULL;
    if(s->use_nodder)
        ret = nodder_extract_points_frame_decimate(s->nodder, "body", 0, 8.0);
    else{
        if(s->do_filter){
            ret = velodyne_extract_points_frame_decimate_compensation(s->velodyne, "body", 2, 0.4, 10.0);
        }
        else{
            ret = velodyne_extract_points_frame_decimate_compensation(s->velodyne, "body", 30, 0.4, 10.0);
        }
    }
 
    if(ret != NULL){
                        
        if(s->verbose)
            fprintf(stderr, "Size of Points : %d \n", ret->no_points);

        double body_to_local[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                      "local",  ret->utime,
                                                      body_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            g_mutex_unlock(s->mutex);
            return;
        }

        double b_pos[3] = {0}, g_pos[3];
        bot_vector_affine_transform_3x4_3d (body_to_local, b_pos, g_pos);
        double dist = hypot(g_pos[0] - s->p_pos[0], g_pos[1] - s->p_pos[1]);

        erlcm_slam_node_init_t msg; 
        msg.utime = ret->utime;
        msg.sender = 1;
        erlcm_slam_node_init_t_publish(s->lcm, "SLAM_NODE_INIT", &msg);
        
        fprintf(stderr, "Request Time : %f\n", ret->utime/1.0e6);
        
        xyz_point_list_t *filtered  = NULL;
        
        if(s->do_filter){
            int64_t start_time = bot_timestamp_now();
            filtered = create_xyz_list(ret->no_points);
            //not that this might not work in all implementations 
            memset(s->fvm->data, 0, s->fvm->num_cells * sizeof(float)); 

            int j = 0;
            
            filtered->utime = ret->utime;
            
            for (int k = 0; k < ret->no_points; ++k){                
                if(s->fvm->isInMap(ret->points[k].xyz) && s->fvm->readValue(ret->points[k].xyz) == 0){
                    s->fvm->writeValue(ret->points[k].xyz, 1);
                    memcpy(filtered->points[j].xyz, ret->points[k].xyz, 3 *sizeof(double));
                    j++;
                }
            }
            
            filtered->no_points = j;
            realloc_xyz_list(filtered, j);
            fprintf(stderr, "Time taken : %f Original : %d  Filtered size : %d\n", (bot_timestamp_now() - start_time)/1.0e6, ret->no_points, j);
            
        }
    
        memcpy(s->p_pos, g_pos, 3*sizeof(double));
        if(s->do_filter){
            destroy_xyz_list(ret);
            bot_ptr_circular_add (s->nodder_data_circ, filtered);
        }
        else{
            bot_ptr_circular_add (s->nodder_data_circ, ret);
        }

        s->p_size++;
    }
    g_mutex_unlock(s->mutex);
}   

void usage(char *fname){
    printf("Usage: %s [options]\n"
           "options are:\n"
           "--help,             -h   Display this message \n"
           "--verbose,          -v   Verbose output\n"
           "--logfile,          -l   Output log filename \n"
           "--reduce_footprint  -r   Does not keep the history of points\n"
           "--filter            -f   Subsamples the points\n"
           "--draw              -d   Draws the points\n"
           "--publish-maps      -m   Publish VoxelMaps\n"
           , fname);
    exit(1);
}

int main(int argc, char **argv)
{
    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    state->lcm =  bot_lcm_get_global(NULL);

    state->param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->param);
    
    state->point_cloud_array = g_array_new (FALSE, TRUE, sizeof (xyz_point_list_t *));
    
    state->slam_cloud_array = g_array_new (FALSE, TRUE, sizeof (point_pose_t *));

    state->nodder_data_circ = bot_ptr_circular_new (VELODYNE_DATA_CIRC_SIZE,
                                                    circ_free_nodder_data, state);
    state->mainloop = g_main_loop_new( NULL, FALSE );  

    const char *optstring = "vhdmnfl:r";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "verbose", no_argument, 0, 'v' }, 
                                  { "nodder", no_argument, 0, 'n' }, 
                                  { "logfile", required_argument, 0, 'l' }, 
                                  { "draw", no_argument, 0, 'd' }, 
                                  { "reduce_footprint", no_argument, 0, 'r' }, //under this method points are logged - not saved 
                                  { "filter", no_argument, 0, 'f' }, 
                                  { "publish-maps", no_argument, 0, 'm' }, 
				  { 0, 0, 0, 0 } };

    int c;

    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    usage(argv[0]);
        //fprintf(stderr, "No help found - please add help\n");
	    break;
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
                state->verbose = 1;
		break;
	    }
        case 'r':
	    {
		fprintf(stderr,"Reduce footprint\n");
                state->reduce_footprint = 1;
		break;
	    }
        case 'f':
	    {
		fprintf(stderr,"filtering\n");
                state->do_filter = 1;
		break;
	    }
        case 'n':
	    {
		fprintf(stderr,"Using Nodder\n");
                state->use_nodder = 1;
		break;
	    }
        case 'l':
	    {
		fprintf(stderr,"Logging\n");
                state->log_filename = strdup(optarg);
		break;
	    }
            
        case 'd':
	    {
		fprintf(stderr,"Drawing\n");
                state->draw = 1;
		break;
	    }
            
        case 'm':
	    {
		fprintf(stderr,"Publishing Maps\n");
                state->publish_maps = 1;
		break;
	    }
        }
    }
  
    if (!state->mainloop){
	printf("Couldn't create main loop\n");
	return -1;
    }

    if(!state->log_filename)
        state->reduce_footprint = 0;

    state->nodder = NULL;
    state->velodyne = NULL;

    double xyz0[3] = { -10, -10, -0.5 };
    double xyz1[3] = { 10, 10, 3 };
    //double mpp[3] = { .02, .02, .02 };
    double mpp[3] = { .1, .1, .1 };
    
    state->fvm = new occ_map::FloatVoxelMap(xyz0, xyz1, mpp, 0); 

    if(state->use_nodder)
        state->nodder = nodder_extractor_init_full(state->lcm, 1, -40, 40, NULL, 
                                                   &point_source_cb, state);
    else
        state->velodyne = velodyne_extractor_init(state->lcm, point_source_cb, state);
    
    
    if(state->log_filename){
        if(g_file_test(state->log_filename, G_FILE_TEST_EXISTS)){
        fprintf(stderr,"File already exists\n");
        return -1; 
        }
        state->write_log = lcm_eventlog_create(state->log_filename, "w"); 
    }

    state->mutex = g_mutex_new ();
    
    if(state->draw){
        erlcm_slam_full_graph_t_subscribe(state->lcm, "SLAM_FULL_GRAPH", slam_handler_draw, state);
    }
    else{
        erlcm_slam_full_graph_t_subscribe(state->lcm, "SLAM_FULL_GRAPH", slam_handler, state);
    }

    erlcm_slam_matched_point_list_t_subscribe(state->lcm,"SLAM_3D_POINTS", slam_point_handler, state);

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);
        
    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
    
    bot_glib_mainloop_detach_lcm(state->lcm);
}
