#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <nodder/nodder_extractor.h>
#include <lcmtypes/erlcm_xyz_point_list_t.h>

#include <occ_map/VoxelMap.hpp>
#include <lcmtypes/occ_map_voxel_map_t.h>
#include <lcmtypes/erlcm_slam_full_graph_t.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <lcmtypes/er_lcmtypes.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif 

#define NODDER_DATA_CIRC_SIZE 10

typedef struct _state_t state_t;

typedef struct {
    xyz_point_list_t *points;  
    int node_id; //we will use the node id to draw the map 
    double tf_pos[3]; 
    double theta;
} point_pose_t;

struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    BotFrames *frames; 
    BotParam *param;
    BotPtrCircular   *nodder_data_circ;
    GArray   *point_cloud_array;      
    GArray   *slam_cloud_array;      
    nodder_extractor_state_t *nodder;
    
    double p_pos[3];
    int p_size;
    int s_size;
    int verbose; 
    GMutex *mutex;
};

void
circ_free_nodder_data(void *user, void *p) {
    xyz_point_list_t *np = (xyz_point_list_t *) p; 
    destroy_xyz_list(np);
}

void get_rpy_frame(BotFrames *frame, int64_t utime, double rpy[3], char *from_frame, char *to_frame, double rpy_to_frame[3]){
    double quat_from_frame[4], quat_to_frame[4];
    bot_roll_pitch_yaw_to_quat(rpy, quat_from_frame);
    
    BotTrans from_to_trans;
    bot_frames_get_trans_with_utime (frame, from_frame, to_frame, utime, 
                                     &from_to_trans);
    
    bot_quat_mult (quat_to_frame, from_to_trans.rot_quat, quat_from_frame);
    bot_quat_to_roll_pitch_yaw (quat_to_frame, rpy_to_frame);

}

static void slam_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                         const char * channel __attribute__((unused)), 
                         const erlcm_slam_full_graph_t * msg,
                         void * user  __attribute__((unused)))
{

    state_t *s = (state_t *)user;

    g_mutex_lock(s->mutex);
    
    erlcm_slam_full_graph_node_t *last_node = &msg->nodes[msg->no_nodes-1];

    double time_gap = 10000000.0;
    int c_ind = -1;

    //we should make it a circular buffer
    fprintf(stderr, "Buffer size : %d\n" , bot_ptr_circular_size(s->nodder_data_circ));
    for (int i=0; i<(bot_ptr_circular_size(s->nodder_data_circ)); i++) {
        //xyz_point_list_t *points = g_array_index(s->point_cloud_array, xyz_point_list_t *, i);

        xyz_point_list_t *points = (xyz_point_list_t *) bot_ptr_circular_index(s->nodder_data_circ, i);
        if(fabs(last_node->utime - points->utime)/1.0e6 < time_gap){
            time_gap = fabs(last_node->utime - points->utime)/ 1.0e6;
            c_ind = i;
        }
    }

    fprintf(stderr, "Time gap %f -> ind :%d\n", time_gap, c_ind);

    if(c_ind == -1 || time_gap > 1.0){
        fprintf(stderr,"No Close node found\n");
        g_mutex_unlock(s->mutex);
        return;
    }
    
    double g_to_local[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "global",
                                                  "local",  last_node->utime, 
                                                  g_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;
    }
    
    point_pose_t *p_pose = (point_pose_t *) calloc(1, sizeof(point_pose_t));

    p_pose->points = copy_xyz_list((xyz_point_list_t *)bot_ptr_circular_index(s->nodder_data_circ, c_ind));
    p_pose->node_id = last_node->id;

    //find the transform from the slam pos to the node point pos 

    double b_to_local[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                  "local",  p_pose->points->utime, 
                                                  b_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;
    }

    double slam_pose[3] = {last_node->pos[0], last_node->pos[1], 0}, local_slam_pose[3];
    double body_pose[3]= {0}, body_local_pose[3];

    bot_vector_affine_transform_3x4_3d (g_to_local, slam_pose, local_slam_pose);

    bot_vector_affine_transform_3x4_3d (b_to_local, body_pose, body_local_pose);
    
    fprintf(stderr, "Local Pose for SLAM node : %f,%f => %f,%f\n", local_slam_pose[0], local_slam_pose[1], 
            body_local_pose[0], body_local_pose[1]);

    double dX = body_local_pose[0] - local_slam_pose[0];
    double dY = body_local_pose[1] - local_slam_pose[1];

    double rpy_slam_global[3] = {0,0, last_node->t}, rpy_slam_local[3];

    get_rpy_frame(s->frames, last_node->utime, rpy_slam_global, "global", "local", rpy_slam_local);

    /*fprintf(stderr, "Slam : (%f, %f, %f) => Diff %f\n", 
            rpy_slam_local[0], rpy_slam_local[1], rpy_slam_local[2], 
            last_node->t);*/

    p_pose->tf_pos[0] = dX * cos(rpy_slam_local[2]) + dY * sin(rpy_slam_local[2]);
    p_pose->tf_pos[1] = -dX * sin(rpy_slam_local[2]) + dY * cos(rpy_slam_local[2]);

    double rpy_bot_body[3] = {0}, rpy_bot_local[3];
    get_rpy_frame(s->frames, p_pose->points->utime, rpy_bot_body, "body", "local", rpy_bot_local);                
    //if this theta is too large prob should forget about these points 

    p_pose->theta = bot_mod2pi(rpy_bot_local[2] - rpy_slam_local[2]);

    fprintf(stderr, "Diff %f,%f - theta : %f\n", p_pose->tf_pos[0], p_pose->tf_pos[1], p_pose->theta);

    /*fprintf(stderr, "Roll Bot : (%f,%f,%f) -> Slam : (%f, %f, %f) => Diff %f\n", 
            rpy_bot_local[0], rpy_bot_local[1], rpy_bot_local[2], 
            rpy_slam_local[0], rpy_slam_local[1], rpy_slam_local[2], p_pose->theta);*/

    g_array_append_val (s->slam_cloud_array, p_pose);
    
    s->s_size++;

    //not sure if this clears the data in it 
    g_array_free (s->point_cloud_array, TRUE);
    s->point_cloud_array =  g_array_new (FALSE, TRUE, sizeof (xyz_point_list_t *));
    
    //check and find the closest one 

    //check the prev list and find the closest match 
    //add to the permanant list and dump the cache
    bot_lcmgl_t *lcmgl = bot_lcmgl_init(s->lcm,"nodder_points");
    
    bot_lcmgl_color3f(lcmgl, 1, 0, 0);

    bot_lcmgl_point_size(lcmgl, 1);

    double global_to_local[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "global",
                                                  "local",  msg->utime,
                                                  global_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        g_mutex_unlock(s->mutex);
        return;
    }

    int j = 0;

    fprintf(stderr, "Size of Slam cloud : %d\n", s->s_size);
    
    for (int i=0; i<(s->s_size); i++) {
        point_pose_t *p_list = g_array_index(s->slam_cloud_array, point_pose_t *, i);
        xyz_point_list_t *points = p_list->points;
        //find the size of the map based on the current position 
        //copy and convert 
        double global_to_local[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "global",
                                                      "local",  msg->utime,
                                                      global_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            g_mutex_unlock(s->mutex);
            return;
        }
        
        /*
           double pos[2];
           double t;          
         */
        //fprintf(stderr,"Pose id : %d => %d\n", p_list->node_id, j);
        
        while(p_list->node_id != msg->nodes[j].id){
            if(j > msg->no_nodes-1){
                break;
            }
            j++;
        }
        
        //fprintf(stderr, "[%d] Pose : %d\n", i , j);
        
        if(p_list->node_id != msg->nodes[j].id){
            fprintf(stderr, "Pose Not found\n");
            break;
        }

        //we know the global position of the anchor node 
        //convert the points to the global position and then convert to the local position 
        //figure out how to convert directly 
        BotTrans bodyToGlobal;

        dX = p_list->tf_pos[0];
        dY = p_list->tf_pos[1];
        double dT = p_list->theta;
        
        bodyToGlobal.trans_vec[0] = msg->nodes[j].pos[0] + dX * cos(msg->nodes[j].t) - dY * sin(msg->nodes[j].t);
        bodyToGlobal.trans_vec[1] = msg->nodes[j].pos[1] + dX * sin(msg->nodes[j].t) + dY * cos(msg->nodes[j].t);
        bodyToGlobal.trans_vec[2] = 0;

        double rpy[3] = { 0,0,  msg->nodes[j].t + dT};//p_list->theta};//slampose->rp[0], slampose->rp[1], value.t() };
        bot_roll_pitch_yaw_to_quat(rpy, bodyToGlobal.rot_quat);
        
        /*bodyToGlobal.trans_vec[0] = msg->nodes[j].pos[0] + p_list->tf_pos[0] * cos(msg->nodes[j].t) - p_list->tf_pos[1] * sin(msg->nodes[j].t);
        bodyToGlobal.trans_vec[1] = msg->nodes[j].pos[1] + p_list->tf_pos[0] * sin(msg->nodes[j].t) + p_list->tf_pos[1] * cos(msg->nodes[j].t);
        bodyToGlobal.trans_vec[2] = 0;

        double rpy[3] = { 0,0,  msg->nodes[j].t + M_PI/6};//p_list->theta};//slampose->rp[0], slampose->rp[1], value.t() };
        bot_roll_pitch_yaw_to_quat(rpy, bodyToGlobal.rot_quat);
        */
        double pos_l[3], pos_b[3] = {0}, pos_g[3];
        fprintf(stderr, "Size : %d\n", points->no_points);

        bot_lcmgl_color3f(lcmgl, 0, 1.0, 0);
        bot_trans_apply_vec(&bodyToGlobal, pos_b, pos_g);
        bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);

        bot_lcmgl_begin(lcmgl, GL_LINES);
        bot_lcmgl_vertex3f(lcmgl, pos_l[0], pos_l[1], pos_l[2]);
        pos_g[0] = msg->nodes[j].pos[0];
        pos_g[1] = msg->nodes[j].pos[1];
        pos_g[2] = 0;
        bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);
        bot_lcmgl_vertex3f(lcmgl, pos_l[0], pos_l[1], pos_l[2]);

        bot_lcmgl_end(lcmgl);

        bot_lcmgl_begin(lcmgl, GL_POINTS);

        bot_lcmgl_color3f(lcmgl, 0, 1.0, 0);
        pos_b[0] = 0;
        pos_b[1] = 0;
        pos_b[2] = 0;
        bot_trans_apply_vec(&bodyToGlobal, pos_b, pos_g);
        bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);
        bot_lcmgl_vertex3f(lcmgl, pos_l[0], pos_l[1], pos_l[2]);

        bot_lcmgl_color3f(lcmgl, 1, 0, 0);

        /*for(int k=0;k< 10; k++){
            double r = 1;
            double theta = M_PI/10 * k;
            pos_b[0] = r * cos(theta);
            pos_b[1] = r * sin(theta);

            double xyz_s[3] = {0}, xyz_g[3] = {0}, xyz_l[3] = {0};
            
            xyz_s[0] = dX + pos_b[0] * cos(dT) - pos_b[1] * sin(dT); 
            xyz_s[1] = dY + pos_b[0] * sin(dT) + pos_b[1] * cos(dT); 
            //bot_trans_apply_vec(&bodyToGlobal, pos_b, pos_g);
            xyz_g[0] =  msg->nodes[j].pos[0] + xyz_s[0] * cos(msg->nodes[j].t) 
                - xyz_s[1] * sin(msg->nodes[j].t);
            xyz_g[1] =  msg->nodes[j].pos[1] + xyz_s[0] * sin(msg->nodes[j].t) 
                + xyz_s[1] * cos(msg->nodes[j].t);

            bot_vector_affine_transform_3x4_3d (global_to_local, xyz_g , xyz_l);
            bot_lcmgl_vertex3f(lcmgl, xyz_l[0], xyz_l[1], xyz_l[2]);
            }*/
        
        for (int k = 0; k < points->no_points; ++k){
          pos_b[0] = points->points[k].xyz[0];
          pos_b[1] = points->points[k].xyz[1];
          pos_b[2] = points->points[k].xyz[2];

          double xyz_s[3] = {0}, xyz_g[3] = {0}, xyz_l[3] = {0};
            
          xyz_s[0] = dX + pos_b[0] * cos(dT) - pos_b[1] * sin(dT); 
          xyz_s[1] = dY + pos_b[0] * sin(dT) + pos_b[1] * cos(dT); 
          xyz_s[2] = pos_b[2]; 
          //bot_trans_apply_vec(&bodyToGlobal, pos_b, pos_g);
          xyz_g[0] =  msg->nodes[j].pos[0] + xyz_s[0] * cos(msg->nodes[j].t) 
              - xyz_s[1] * sin(msg->nodes[j].t);
          xyz_g[1] =  msg->nodes[j].pos[1] + xyz_s[0] * sin(msg->nodes[j].t) 
              + xyz_s[1] * cos(msg->nodes[j].t);
          xyz_g[2] = xyz_s[2];

          bot_vector_affine_transform_3x4_3d (global_to_local, xyz_g , xyz_l);
          bot_lcmgl_vertex3f(lcmgl, xyz_l[0], xyz_l[1], xyz_l[2]);
        }

        /*for (int k = 0; k < points->no_points; ++k){
          pos_b[0] = points->points[k].xyz[0];
          pos_b[1] = points->points[k].xyz[1];
          pos_b[2] = points->points[k].xyz[2];
          bot_trans_apply_vec(&bodyToGlobal, pos_b, pos_g);
          bot_vector_affine_transform_3x4_3d (global_to_local, pos_g , pos_l);
          
          bot_lcmgl_vertex3f(lcmgl, pos_l[0], pos_l[1], pos_l[2]);
          }*/
        bot_lcmgl_end(lcmgl);
    }

    bot_lcmgl_switch_buffer(lcmgl);
    g_mutex_unlock(s->mutex);
}  

void nodder_cb(int64_t utime, void *data)
{
    state_t *s = (state_t *)data;
    g_mutex_lock(s->mutex);
    //fprintf(stderr, "Called \n"); 
    xyz_point_list_t *ret = nodder_extract_points_frame_decimate(s->nodder, "body", 2, 8.0);

    if(ret != NULL){
        if(s->verbose)
            fprintf(stderr, "Size of Points : %d \n", ret->no_points);

        double body_to_local[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                      "local",  ret->utime,
                                                      body_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            g_mutex_unlock(s->mutex);
            return;
        }

        double b_pos[3] = {0}, g_pos[3];
        bot_vector_affine_transform_3x4_3d (body_to_local, b_pos, g_pos);
        double dist = hypot(g_pos[0] - s->p_pos[0], g_pos[1] - s->p_pos[1]);
        /*if(dist< 2.0){
        //fprintf(stderr, "Skipping\n");
        destroy_xyz_list(ret);
        return;
        }*/
    
        //        fprintf(stderr, "Adding Dist traveled: %f\n", dist);
    
        memcpy(s->p_pos, g_pos, 3*sizeof(double));
        //g_array_append_val (s->point_cloud_array, ret);
    
        //bot_ptr_circular_add (s->nodder_data_circ, ret);


        s->p_size++;

        erlcm_slam_node_init_t msg; 
        msg.utime = ret->utime;
        msg.sender = 0;
        erlcm_slam_node_init_t_publish(s->lcm, "SLAM_NODE_INIT", &msg);
    }
    g_mutex_unlock(s->mutex);
}   

int main(int argc, char **argv)
{
    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    state->lcm =  bot_lcm_get_global(NULL);

    state->param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->param);

    state->nodder = nodder_extractor_init_full(state->lcm, 1, -40, 40, NULL, 
					       &nodder_cb, state);

    state->point_cloud_array = g_array_new (FALSE, TRUE, sizeof (xyz_point_list_t *));
    
    state->slam_cloud_array = g_array_new (FALSE, TRUE, sizeof (point_pose_t *));

    state->nodder_data_circ = bot_ptr_circular_new (NODDER_DATA_CIRC_SIZE,
                                                    circ_free_nodder_data, state);
    
    if(state->nodder== NULL){
        fprintf(stderr,"Issue getting collector\n");
        return -1;
    }

    state->mainloop = g_main_loop_new( NULL, FALSE );  

    const char *optstring = "vh";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "verbose", no_argument, 0, 'v' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    //usage(argv[0]);
            fprintf(stderr, "No help found - please add help\n");
	    break;
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
                state->verbose = 1;
		break;
	    }
        }
    }
  
    if (!state->mainloop){
	printf("Couldn't create main loop\n");
	return -1;
    }

    state->mutex = g_mutex_new ();
    
    erlcm_slam_full_graph_t_subscribe(state->lcm, "SLAM_FULL_GRAPH", slam_handler, state);

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);
    
    /* heart beat*/
    //g_timeout_add (100, heartbeat_cb, state);
    
    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
    
    bot_glib_mainloop_detach_lcm(state->lcm);
}
