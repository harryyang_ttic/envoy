#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

//#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <lcmtypes/object_door_list_t.h>
#include <lcmtypes/object_door_t.h>

#include <bot_param/param_client.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <GL/gl.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <full_laser/full_laser.h>
#include "door_detect_laser.h"

#define LOCAL_AREA_RADIUS 5.0

typedef struct
{
    lcm_t *lcm;
    GMainLoop *mainloop;
    BotParam   *b_server;
    BotFrames *frames;
    bot_lcmgl_t *lcmgl;
    GMutex *mutex;
    GList *door_list;
    bot_core_planar_lidar_t *last_laser;
    full_laser_state_t *full_laser;
    int run_always;
    int draw;

}state_t;

int compare_doorway(doorway_t *d, doorway_t *d1, BotFrames *frames);

void update_doors(bot_core_planar_lidar_t *last_laser, state_t *s);

static void full_laser_update_handler(int64_t utime, bot_core_planar_lidar_t *msg, void *user)
{
    state_t *s = (state_t *)user;
    if(s->last_laser != NULL){
        bot_core_planar_lidar_t_destroy(s->last_laser);
    }
    s->last_laser = bot_core_planar_lidar_t_copy(msg);

    if(s->run_always)
        update_doors(s->last_laser, s);
}


static void
on_laser(const lcm_recv_buf_t *rbuf, const char *channel,
         const bot_core_planar_lidar_t *msg, void *user){
    state_t *s = (state_t *)user;
    //this is not necessary - the timeout is threadsafe 

    if(s->last_laser != NULL){
        bot_core_planar_lidar_t_destroy(s->last_laser);
    }
    s->last_laser = bot_core_planar_lidar_t_copy(msg);

    if(s->run_always)
        update_doors(s->last_laser, s);
}

int compare_doorway(doorway_t *d, doorway_t *d1, BotFrames *frames){

    double local_pos1[3];
    local_pos1[0] = d->start[0];
    local_pos1[1] = d->start[1];
    local_pos1[2] = 0;
    double local_pos2[3];
    local_pos2[0] = d->end[0];
    local_pos2[1] = d->end[1];
    local_pos2[2] = 0;

    double local_pos3[3];
    local_pos3[0] = d1->start[0];
    local_pos3[1] = d1->start[1];
    local_pos3[2] = 0;
    double local_pos4[3];
    local_pos4[0] = d1->end[0];
    local_pos4[1] = d1->end[1];
    local_pos4[2] = 0;

  
    double r1[3];
    double r2[3];
    double r3[3];
    double r4[3];

    bot_frames_transform_vec(frames, "local", "body", local_pos1, r1);
    bot_frames_transform_vec(frames, "local", "body", local_pos2, r2);
    bot_frames_transform_vec(frames, "local", "body", local_pos3, r3);
    bot_frames_transform_vec(frames, "local", "body", local_pos4, r4);


    double p1_meanx = (r1[0] + r2[0]) / (double)2;
    double p1_meany = (r1[1] + r2[1]) / (double)2;
    double p2_meanx = (r3[0] + r4[0]) / (double)2;
    double p2_meany = (r3[1] + r4[1]) / (double)2;

    double dist = hypot(p1_meanx - p2_meanx, p1_meany - p2_meany);

    if(dist < 0.5)
        return 1;

    return 0;
}

void update_doors(bot_core_planar_lidar_t *last_laser, state_t *s){
    

    if(last_laser != NULL){
        int64_t last_utime = last_laser->utime;

        //Extract doorways using API
    
        doors_t *doors = detect_doors(last_laser, s->frames);

        fprintf(stderr, "No of doors : %d\n", doors->num);

        //Check with door list to see whether needs to update
        for(int i = 0;i < doors->num;i++){

            doorway_t *d = (doorway_t *) calloc(1, sizeof(doorway_t));
            d->utime = last_laser->utime;

            //message is in local frame and store in glist
      
            double body1[3];
            double body2[3];
            body1[0] = doors->doors[i].start[0];
            body1[1] = doors->doors[i].start[1];
            body1[2] = 0;
            body2[0] = doors->doors[i].end[0];
            body2[1] = doors->doors[i].end[1];
            body2[2] = 0;

            /*
              double r1[3];
              double r2[3];

              bot_frames_transform_vec(s->frames, "body", "local", body1, r1);
              bot_frames_transform_vec(s->frames, "body", "local", body2, r2);
            */

            d->start[0] = body1[0];
            d->start[1] = body1[1];
            d->end[0] = body2[0];
            d->end[1] = body2[1];
            d->count = 1;   //Set first time seen


            //whether there exists a near doorway
            int exist = 0;
            for(int j = 0;j < g_list_length(s->door_list);j++){
                doorway_t *door = (doorway_t *)g_list_nth(s->door_list, j)->data;
                exist = compare_doorway(d, door, s->frames);
                //important to break out of loop
                if(exist == 1){
                    door->count += 1;    //set to have seen one more time
                    //update the position??
                    break;
                }
            }
            if(!exist){
                fprintf(stderr, "Add a new doorway to the list\n");
                s->door_list = g_list_append(s->door_list, d);
            }
            else{
                free(d);
            }
        }

        destroy_doors(doors);

        //Remove doors that are 10m away from current robot pose
        for(int i = 0;i < g_list_length(s->door_list);i++){
            doorway_t *door = (doorway_t *)g_list_nth(s->door_list, i)->data;

            //Get current pose of the robot at local frame
            double transition_matrix[12];
            double p[3];
            bot_frames_get_trans_mat_4x4_with_utime(s->frames, "body", "local", last_laser->utime, transition_matrix);
            p[0] = transition_matrix[3];
            p[1] = transition_matrix[7];
            p[2] = 0;

            double dist1 = hypot(p[0] - door->start[0], p[1] - door->start[1]);
            double dist2 = hypot(p[0] - door->end[0], p[1] - door->end[1]);
      
            double min;
      
            if(dist1 <= dist2)
                min = dist1;
            else
                min = dist2;

            //If 10m away from that doorway, remove that doorway from the list
            if(min >= LOCAL_AREA_RADIUS){
                doorway_t *door = (doorway_t *)g_list_nth(s->door_list, i)->data;
                s->door_list = g_list_remove(s->door_list, g_list_nth(s->door_list, i)->data);
	      
                destroy_door(door);
                fprintf(stderr, "Remove a door from door list.\n");
            }
        }


        //Update door ID
        for(int i = 0;i < g_list_length(s->door_list);i++){
            doorway_t *door = (doorway_t *)g_list_nth(s->door_list, i)->data;
            door->id = i + 1;
        }

        fprintf(stderr, "Size of door list: %d\n", g_list_length(s->door_list));

        object_door_list_t msg;

        if(g_list_length(s->door_list) != 0){

            GList *valid_doors = NULL;

            //Set this number for how many times the doorway needs to be seen before publishing
            int check_num = 1;

            //only get those doors that have count >= 3
            int count_num = 0;
            for(int i = 0;i < g_list_length(s->door_list);i++){
                doorway_t *door = (doorway_t *)g_list_nth(s->door_list, i)->data;
	
                if(door->count >= check_num){
                    count_num++;
                    valid_doors = g_list_prepend(valid_doors, door);
                }
            }

            //Publish a message - using the laser's utime 
            msg.utime = last_utime;
            msg.no_door = count_num;

            msg.doors = (object_door_t *) calloc(msg.no_door, sizeof(object_door_t));

            for(int i = 0;i < msg.no_door;i++){
                msg.doors[i].utime = last_utime;//bot_timestamp_now();
                //doorway_t *door = (doorway_t *)g_list_nth(s->door_list, i)->data;
                doorway_t *door = (doorway_t *)g_list_nth(valid_doors, i)->data;
                //local pose, convert to body frame for messages
		
		
                double local1[3];
                double local2[3];
                local1[0] = door->start[0];
                local1[1] = door->start[1];
                local1[2] = 0;
                local2[0] = door->end[0];
                local2[1] = door->end[1];
                local2[2] = 0;

                /*
                  double r1[3];
                  double r2[3];

                  bot_frames_transform_vec(s->frames, "local", "body", local1, r1);
                  bot_frames_transform_vec(s->frames, "local", "body", local2, r2);
                */


                msg.doors[i].point1[0] = local1[0];
                msg.doors[i].point1[1] = local1[1];
                msg.doors[i].point2[0] = local2[0];
                msg.doors[i].point2[1] = local2[1];

                if(s->draw){
	  
                    bot_lcmgl_color3f(s->lcmgl, 1, 0, 0);
                    bot_lcmgl_line_width(s->lcmgl, 5);
                    bot_lcmgl_begin(s->lcmgl, GL_LINE_STRIP);

                    bot_lcmgl_vertex3f(s->lcmgl, door->start[0], door->start[1], 0);
                    bot_lcmgl_vertex3f(s->lcmgl, door->end[0], door->end[1], 0);

                    bot_lcmgl_end(s->lcmgl);
                }
            }
      
            object_door_list_t_publish(s->lcm, "DOOR_LIST", &msg);
            g_list_free(valid_doors);
            free(msg.doors);
            valid_doors = NULL;
 
        }
        else{
            msg.utime = last_utime;//bot_timestamp_now();
            msg.no_door = 0;
            msg.doors = NULL;
            object_door_list_t_publish(s->lcm, "DOOR_LIST", &msg);
        }
        if(s->draw)
            bot_lcmgl_switch_buffer(s->lcmgl);
    }

    //Weilun - destory the message you calloc'ed
}


gboolean heartbeat_cb (gpointer data)
{
    //if using this - need to add mutex 

    state_t *s = (state_t *)data;

    static int64_t last_utime = 0;

    //g_mutex_lock(s->mutex);
    if(s->last_laser == NULL || (s->last_laser->utime == last_utime)){
        //g_mutex_unlock(s->mutex);
        return TRUE;
    }
  
    bot_core_planar_lidar_t *last_laser = s->last_laser;
    last_utime = last_laser->utime;
    update_doors(last_laser, s);

    //return true to keep running
    return TRUE;
}



void subscribe_to_channels(state_t *s)
{
    //bot_core_pose_t_subscribe(s->lcm, "POSE", on_pose ,s);
   
}  


static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -d    Draw doorways in viewer\n"
             "  -f    Use skirt front\n"
             "  -n    Use nodding laser\n"
             "  -s    Use simulated laser\n"
             "  -h    This help message\n", 
             g_path_get_basename(progname));
    exit(1);
}

int 
main(int argc, char **argv)
{

    const char *optstring = "dfnsha";
    int simulate = 1;
    int draw = 0;
    int c;

   

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    state->last_laser = NULL;
    state->door_list = NULL;
    state->run_always = 0;

    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
            case 'd':
                draw = 1;
                break;
            case 'f':
                simulate = 0;
                break;
            case 'n':
                simulate = 2;
                break;
            case 'a':
                state->run_always = 1;
                break;
            case 's':
                simulate = 1;
                break;
            case 'h': //help
                usage(argv[0]);
                break;
            default:
                usage(argv[0]);
                break;
        }
    }


    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
    state->b_server = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->b_server);
    state->draw = draw;
    state->mutex = g_mutex_new();
    if(draw)
        state->lcmgl = bot_lcmgl_init(state->lcm,"door-detection");
  
    if(simulate == 1)
        state->full_laser = full_laser_init(state->lcm, 540, &full_laser_update_handler, state);
    else if(simulate == 0)
        bot_core_planar_lidar_t_subscribe(state->lcm, "SKIRT_FRONT", on_laser, state);
    else if(simulate == 2)
        bot_core_planar_lidar_t_subscribe(state->lcm, "NODDING_LASER", on_laser, state);

    if(simulate == 0)
        fprintf(stderr, "Using skirt front laser.\n");
    else if(simulate == 1)
        fprintf(stderr, "Using 360 degree simulated laser.\n");
    else if(simulate == 2)
        fprintf(stderr, "Using nodding laser\n");
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    /* heart beat*/
    g_timeout_add(25, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


