//Use velodyne info to detect doorways. It can only detect doorways that are 3-5m apart. Not a classifier on spot.

#include "door_detect_laser.h"




typedef struct _state_t state_t;

struct _state_t {
  lcm_t *lcm;
  GMainLoop *mainloop;
  BotParam   *b_server;
  full_laser_state_t *full_laser;
  BotFrames *frames;
  bot_lcmgl_t *lcmgl;
  bot_core_planar_lidar_t *last_laser;
  object_door_list_t *last_doors;
  //GList *line_segments;
};

static void full_laser_update_handler(int64_t utime, bot_core_planar_lidar_t *msg, void *user)
{
  state_t *s = (state_t *)user;
  if(s->last_laser != NULL){
    bot_core_planar_lidar_t_destroy(s->last_laser);
  }
  s->last_laser = bot_core_planar_lidar_t_copy(msg);
}


static void
on_laser(const lcm_recv_buf_t *rbuf, const char *channel,
	 const bot_core_planar_lidar_t *msg, void *user){
  state_t *self = (state_t *)user;
  if(self->last_laser != NULL){
    bot_core_planar_lidar_t_destroy(self->last_laser);
  }
  self->last_laser = bot_core_planar_lidar_t_copy(msg);
}


static void
on_door(const lcm_recv_buf_t *rbuf, const char *channel,
	const object_door_list_t *msg, void *user){
  state_t *self = (state_t *)user;
  if(self->last_doors != NULL){
    object_door_list_t_destroy(self->last_doors);
  }
  self->last_doors = object_door_list_t_copy(msg);
}

    

void 
draw_potential_lines(bot_core_planar_lidar_t *laser, BotFrames *frames, bot_lcmgl_t *lcmgl, lcm_t *lcm)//, GList *line_segments)
{

  doors_t *doors = detect_doors(laser, frames);

  fprintf(stderr, "No of doors detected: %d\n", doors->num);

  for(int i = 0;i < doors->num;i++){

    double body1[3];
    double body2[3];
    body1[0] = doors->doors[i].start[0];
    body1[1] = doors->doors[i].start[1];
    body1[2] = 0;
    body2[0] = doors->doors[i].end[0];
    body2[1] = doors->doors[i].end[1];
    body2[2] = 0;

    double r1[3];
    double r2[3];

    bot_frames_transform_vec(frames, "body", "local", body1, r1);
    bot_frames_transform_vec(frames, "body", "local", body2, r2);


    bot_lcmgl_color3f(lcmgl, 1, 0, 0);
    bot_lcmgl_line_width(lcmgl, 20);
    bot_lcmgl_begin(lcmgl, GL_LINE_STRIP);

    bot_lcmgl_vertex3f(lcmgl, r1[0], r1[1], 0);
    bot_lcmgl_vertex3f(lcmgl, r2[0], r2[1], 0);

    bot_lcmgl_end(lcmgl);

    fprintf(stderr, "Coordinate. %f %f %f %f\n", r1[0], r1[1], r2[0], r2[1]);
  }

  bot_lcmgl_switch_buffer(lcmgl);
  free(doors);
}



//Try to capture some line segments
gboolean heartbeat_cb (gpointer data)
{
  state_t *s = (state_t *)data;

  static int64_t last_utime = 0;

  if(s->last_laser == NULL || (s->last_laser->utime == last_utime)){
    return TRUE;
  }
  
  last_utime = s->last_laser->utime;

  /*
  if(s->last_laser != NULL)
    draw_potential_lines(s->last_laser, s->frames, s->lcmgl, s->lcm);//, s->line_segments);
  */

  double pos[2] = {120, 100};
  double heading = 0.5;
  doorway_t *door = detect_doorway_infront_at_dist(s->last_doors, pos, heading, -0.1, 0.1);

  if(door != NULL){
    double transition_matrix[12];
    double p[3];
    bot_frames_get_trans_mat_4x4_with_utime(s->frames, "body", "local", s->last_laser->utime, transition_matrix);
    p[0] = transition_matrix[3];
    p[1] = transition_matrix[7];
    p[2] = 0;

    bot_lcmgl_color3f(s->lcmgl, 0, 0, 1);
    //bot_lcmgl_push_matrix(s->lcmgl);
    //bot_lcmgl_translated(s->lcmgl, p[0], p[1], 0);
    bot_lcmgl_circle(s->lcmgl, p, 1.5);
    //bot_lcmgl_pop_matrix(s->lcmgl);
    bot_lcmgl_switch_buffer(s->lcmgl);

  }

  else{
    fprintf(stderr, "No doors in front\n");
     bot_lcmgl_switch_buffer(s->lcmgl);
  }
  
  //return true to keep running
  return TRUE;
}

/*
void subscribe_to_channels(state_t *self)
{
  bot_core_planar_lidar_t_subscribe(self->lcm, "SKIRT_FRONT", on_laser, self);
  object_door_list_t_subscribe(self->lcm, "DOOR_LIST", on_door, self);
}  
*/

static void usage(const char* progname)
{
  fprintf (stderr, "Usage: %s [options]\n"
	   "\n"
	   "Options:\n"
	   "  -f    Use skirt front\n"
	   "  -s    Use simulated laser\n"
	   "  -h    This help message\n", 
	   g_path_get_basename(progname));
  exit(1);
}


int 
main(int argc, char **argv)
{

  const char *optstring = "fsh";
  int simulate = 1;
  int c;

  while ((c = getopt (argc, argv, optstring)) >= 0) {
    switch (c) {
    case 'f':
      simulate = 0;
      fprintf(stderr, "Using skirt front laser.\n");
      break;
    case 's':
      simulate = 1;
      fprintf(stderr, "Using 360 degree simulated laser.\n");
      break;
    case 'h': //help
      usage(argv[0]);
      break;
    default:
      usage(argv[0]);
      break;
    }
  }

  g_thread_init(NULL);
  setlinebuf (stdout);
  state_t *state = (state_t*) calloc(1, sizeof(state_t));

  state->lcm =  bot_lcm_get_global(NULL);
  state->b_server = bot_param_new_from_server(state->lcm, 1);
  state->frames = bot_frames_get_global (state->lcm, state->b_server);
  state->lcmgl = bot_lcmgl_init(state->lcm,"line-extraction");
  if(simulate)
    state->full_laser = full_laser_init(state->lcm, 540, &full_laser_update_handler, state);
  else
    bot_core_planar_lidar_t_subscribe(state->lcm, "SKIRT_FRONT", on_laser, state);

  object_door_list_t_subscribe(state->lcm, "DOOR_LIST", on_door, state);

  state->last_laser = NULL;
  state->last_doors = NULL;

  state->mainloop = g_main_loop_new( NULL, FALSE );  
  
  if (!state->mainloop) {
    printf("Couldn't create main loop\n");
    return -1;
  }

  //add lcm to mainloop 
  bot_glib_mainloop_attach_lcm (state->lcm);

  /* heart beat*/
  g_timeout_add (50, heartbeat_cb, state);

  //adding proper exiting 
  bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
  fprintf(stderr, "Starting Main Loop\n");

  ///////////////////////////////////////////////
  g_main_loop_run(state->mainloop);
  
  bot_glib_mainloop_detach_lcm(state->lcm);
}
