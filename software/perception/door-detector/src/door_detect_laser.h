#ifndef __DOOR_DETECT_LASER_H__
#define __DOOR_DETECT_LASER_H__


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <getopt.h>

#include <gsl/gsl_blas.h>
#include <lcm/lcm.h>
//#include <lcmtypes/er_lcmtypes.h>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <GL/gl.h>
#include <lcmtypes/object_door_list_t.h>
#include <lcmtypes/object_door_t.h>
//#include <point_types/point_types.h>
//#include <lcmtypes/erlcm_xyz_point_list_t.h>
#include <full_laser/full_laser.h>


#ifdef __cplusplus
extern "C" {
#endif

    typedef struct _xy_t xy_t;
    struct _xy_t{
        double xy[2];
    };

    void destroy_xy(xy_t *point);

    typedef struct _line_t line_t;
    struct _line_t{
        double start_xy[2];
        double end_xy[2];
        double gradient;
        double intersect;
        int start_index; 
        int end_index;
    };

    void destroy_line(line_t *line);

    typedef struct _doorway_t doorway_t;
    struct _doorway_t{
        double start[2];
        double end[2];
        double length;
        int64_t utime;
        int count;     //count how many times we have seen this door
        int id;
    };

    void destroy_door(doorway_t *door);

    typedef struct _doors_t doors_t;
    struct _doors_t{
        int num;
        doorway_t *doors;
    };

    void destroy_doors(doors_t *doors);


    double calc_dist(double gradient, double intersect, double x, double y);

    double calc_gradient(double start[2], double end[2]);

    double calc_intersect(double start[2], double gradient);

    doorway_t * findParLines(line_t *l1, line_t *l2, bot_core_planar_lidar_t *laser, BotFrames *frame);

    doorway_t *findOutRightLines(line_t *l1, line_t *l2, bot_core_planar_lidar_t *laser, BotFrames *frame);

    line_t *extract_line_from_points(bot_core_planar_lidar_t *laser, int start_index, int end_index, 
                                     BotFrames *frame);

    void split_and_merge(bot_core_planar_lidar_t *laser, int start_index, 
                         int end_index, double threshold, int count, BotFrames *frames);

    doors_t *detect_doors(bot_core_planar_lidar_t *laser, BotFrames *frames);

    int check_intersect(double start[2], double end[2], double min_dist, double max_dist);

    doorway_t *detect_doorway_infront(object_door_list_t *msg, double pos[2], double heading);

    doorway_t *detect_doorway_infront_at_dist(object_door_list_t *msg, double pos[2], double heading, 
                                              double min_dist, double max_dist);

    doorway_t *detect_doorway_at_pos(object_door_list_t *msg, double pos[2], double heading, 
                                     double min_dist, double max_dist);

    doorway_t *detect_doorway_between_pos(object_door_list_t *msg, double pos_1[2], double pos_2[2]);
    
#ifdef __cplusplus
}
#endif

#endif
