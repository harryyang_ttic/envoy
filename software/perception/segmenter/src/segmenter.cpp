#ifndef SEGMENTER
#define SEGMENTER 1

#include <stdio.h>
#include <math.h>
#include <time.h>

// For Eigen
#include "pcl/common/common_headers.h"

#include "pcl/common/pca.h"
#include <pcl/pcl_base.h>

//#include <pcl/console/print.h>

#define NUM_EDGES 3
#define IDA_MASK 0xFFFFF
#define IDB_MASK 0xFFFFF
#define NO_EDGE 0xFFFFF
#define IDA_SHIFT 44
#define IDB_SHIFT 24
#define WEIGHT_SHIFT 0
#define WEIGHT_MASK 0xFFFFF

#include "UnionFindSimple.hpp"

class segmenter{

    double depthThreshold;
    int joiningThreshold;
    int minSize;

    uint32_t nnodes;
    uint64_t nedges;

    bool v;
    uint16_t nvert, nhoriz;
    int thresholdConstant;
    double* threshDepth;

    public:

struct segment_features{
    uint32_t id;

    Eigen::Vector4f xyz;
    Eigen::MatrixXf eigen_vectors;
    Eigen::VectorXf eigen_values;

    double first_principle_max;
    double first_principle_min;
    double second_principle_max;
    double second_principle_min;

    double first_std_dev;
    double second_std_dev;

    double curvature;

    uint8_t r, g, b;

    uint32_t size;

    uint16_t lifetime;
};

struct segment_template{
    double length;
    double width;
    double first_principle_stddev;
    double second_principle_stddev;
    double curvature;
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint32_t size;

    double stddev;
    uint32_t template_id;
};

struct segment_group{
    std::vector<segment_template> segments;
    std::vector<double> distances;
    uint32_t group_id;
};

struct segment{
    uint32_t id;
    segment_features sf;
    std::vector<uint32_t> pts;
};

double getTimeDiff(timeval a, timeval b)
{
    double first = a.tv_sec + (a.tv_usec/1000000.0);
    double second = b.tv_sec + (b.tv_usec/1000000.0);

    return (first - second)*1000;
}

void segmentGraph(UnionFindSimple *uf, uint64_t* newEdgeSortList){

    if (v) printf("segmenting graph\n");
    struct timeval tva, tvb;
    gettimeofday(&tva, NULL);

    // arbitrarily put the max at 6 meters
    memset(threshDepth, depthThreshold, nnodes*sizeof(double));
/**/

    int i;
    for(i = 0; i < nedges; i++)
    {
        int a = (int)((((long long)(newEdgeSortList[i])) >> IDA_SHIFT) & IDA_MASK);
        int b = (int)((((long long)(newEdgeSortList[i])) >> IDB_SHIFT) & IDB_MASK);

        // Since these are sorted, if there is a NO_EDGE, there are not any more
        if (b == NO_EDGE)
        {
            break;
        }
        int aId = (int) uf->getRepresentative(a);
        int bId = (int) uf->getRepresentative(b);

        // If they have already been joined, skip
        if(aId == bId){
            continue;
        }

        int wdepth = (int)((newEdgeSortList[i]) & WEIGHT_MASK);

        // joint if edge passes FH criteria
        if (wdepth < joiningThreshold )
  	{
            uf->connectNodes(aId, bId);
	}
        else if (wdepth <= threshDepth[aId] && wdepth <= threshDepth[bId])
        {
            int root = (int) uf->connectNodes(aId, bId);
            threshDepth[root] = wdepth + thresholdConstant/uf->getSetSize(root);
        }
    }

    gettimeofday(&tvb, NULL);
    if (v) printf("finished joining\n");

//    printf ("joining 1 = %lf\n", getTimeDiff(tvb, tva));

    if (v) printf("done segmenting\n");
}

void joinGraph(UnionFindSimple *uf, uint64_t* newEdgeSortList){

    struct timeval tva, tvb;
    gettimeofday(&tva, NULL);

    int i;

    // enforce min size to ignore noise
    for(i = 0; i < nedges; i++)
    {
        int a = (int)((((long long)(newEdgeSortList[i])) >> IDA_SHIFT) 
                                                                    & IDA_MASK);
        int b = (int)((((long long)(newEdgeSortList[i])) >> IDB_SHIFT) 
                                                                    & IDB_MASK);

        if(b == NO_EDGE)
        {
            break;
        }
        int aId = (int) uf->getRepresentative(a);
        int bId = (int) uf->getRepresentative(b);

        int sza = uf->getSetSize(aId);
        int szb = uf->getSetSize(bId);

        if (sza < minSize || szb < minSize)
        {
            uf->connectNodes(aId, bId);
        }
    } 
    gettimeofday(&tvb, NULL);

//    printf ("joining 2 = %lf\n", getTimeDiff(tvb, tva));

    if (v) printf("done segmenting\n");
}


void getFeatures(std::map<uint32_t, segmenter::segment > &groupedSeg,
                pcl::PointCloud<pcl::PointXYZRGB> &cloud)
{
    if(v) printf("getting features\n");
    std::map<uint32_t, segmenter::segment>::iterator mapIt;

    for (mapIt = groupedSeg.begin(); mapIt != groupedSeg.end(); mapIt++)
    {
        if ((*mapIt).first == 0) continue;

        pcl::PointCloud<pcl::PointXYZ> segment_points;

        uint32_t id = (*mapIt).first;
        (*mapIt).second.id = id;
        std::vector<uint32_t>::iterator it;
        for (it = (*mapIt).second.pts.begin(); it != (*mapIt).second.pts.end(); it++)
        {
            uint32_t pos = (*it);
            pcl::PointXYZ pt;
            pt.x = cloud.points[pos].x;
            pt.y = cloud.points[pos].y;
            pt.z = cloud.points[pos].z;
            segment_points.push_back(pt);
        }
    
        pcl::PCA<pcl::PointXYZ> pca(segment_points);

        (*mapIt).second.sf.xyz = pca.getMean();
        (*mapIt).second.sf.eigen_vectors = pca.getEigenVectors();
        (*mapIt).second.sf.eigen_values = pca.getEigenValues();

    }

    uint32_t maxPos = 0;
    double maxNorm = 0;
    
    // Now get the principle a
    for (mapIt = groupedSeg.begin(); mapIt != groupedSeg.end(); mapIt++)
    {
        if ((*mapIt).first == 0) continue;

        segment_features &sf = (*mapIt).second.sf;
        uint32_t r = 0, g = 0, b = 0;
        Eigen::Vector3f first_principle_axis(3), second_principle_axis(3);
        first_principle_axis(0) = sf.eigen_vectors(0,0);
        first_principle_axis(1) = sf.eigen_vectors(1,0);
        first_principle_axis(2) = sf.eigen_vectors(2,0);
        second_principle_axis(0) = sf.eigen_vectors(0,1);
        second_principle_axis(1) = sf.eigen_vectors(1,1);
        second_principle_axis(2) = sf.eigen_vectors(2,1);

        sf.first_principle_max = 0;
        sf.first_principle_min = 1000;
        sf.second_principle_max = 0;
        sf.second_principle_min = 1000;

        sf.size = (*mapIt).second.pts.size() * pow(sf.xyz(2),2);

        sf.curvature = sf.eigen_values(0)/sf.eigen_values(1);

        double std_dev_first_1 = 0;
        double std_dev_first_2 = 0;
        double std_dev_second_1 = 0;
        double std_dev_second_2 = 0;

        std::vector<uint32_t>::iterator it;
        for (it = (*mapIt).second.pts.begin(); it != (*mapIt).second.pts.end(); it++)
        {
            uint32_t pos = (*it);
            Eigen::Vector3f vec;

            vec(0) = cloud.points[pos].x - sf.xyz(0);
            vec(1) = cloud.points[pos].y - sf.xyz(1);
            vec(2) = cloud.points[pos].z - sf.xyz(2);

            double dot = 
                        vec(0) * first_principle_axis(0) +
                        vec(1) * first_principle_axis(1) +
                        vec(2) * first_principle_axis(2);
            std_dev_first_1 += pow(dot,2);
            std_dev_first_2 += dot;

            if (dot > sf.first_principle_max)
                sf.first_principle_max = dot;
            else if (dot < sf.first_principle_min)
                sf.first_principle_min = dot;

            double dot2 = 
                        vec(0) * second_principle_axis(0) +
                        vec(1) * second_principle_axis(1) +
                        vec(2) * second_principle_axis(2);
            std_dev_second_1 += pow(dot2,2);
            std_dev_second_2 += dot2;

            if (dot2 > sf.second_principle_max)
                sf.second_principle_max = dot2;
            else if (dot2 < sf.second_principle_min)
                sf.second_principle_min = dot2;
                
            r += cloud.points[pos].r;
            g += cloud.points[pos].g;
            b += cloud.points[pos].b;
        }
        int size = (*mapIt).second.pts.size();
        sf.first_std_dev = sqrt(std_dev_first_1/size - 
                                pow(std_dev_first_2/size,2));
        sf.second_std_dev = sqrt(std_dev_second_1/size - 
                                pow(std_dev_second_2/size,2));

        r /= size;
        g /= size;
        b /= size;

        sf.r = r&0xFF;
        sf.g = g&0xFF;
        sf.b = b&0xFF;
    }
}

segmenter(uint16_t nv, uint16_t nh, uint8_t DECI, bool verbose)
{
    v = verbose;
    nvert = nv;
    nhoriz = nh;

    depthThreshold = 60000/(DECI*DECI);
    thresholdConstant = 2*depthThreshold/DECI;
    joiningThreshold = 800;
    minSize = 240/(DECI*DECI);

    if (v) printf("starting segmenter\n");
    nnodes = nvert*nhoriz;
    // -1 because there are only 4 real edges, but 5 nodes
    nedges = nnodes*(NUM_EDGES-1);

    threshDepth = (double*)calloc(nnodes, sizeof(double));
}

~segmenter()
{
    free(threshDepth);
}
};
#endif
