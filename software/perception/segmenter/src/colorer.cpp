#include <stdio.h>
#include <math.h>
#include <time.h>

#include "colorer.hpp"

//static struct segmenter::segment_group;


static struct segmenter::segment_template drawer = {1.0,0.25,0.24,0.06,15.0,0,0,0,52000, 0.15, 10};
static struct segmenter::segment_template paper = {.28,0.21,0.076,0.055,1.9,0,0,0,9000, 0.05, 50};
static struct segmenter::segment_template door = {1.9,0.8,0.55,0.20,5.56,0,0,0,450000, 0.10, 100};


static struct segmenter::segment_template chair_bottom = {0.45,0.42,0.15,0.1,1.9,0,0,0,26000, 0.10, 20};
static struct segmenter::segment_template chair_back = {0.45,0.35,0.11,0.07,2.2,0,0,0,26000, 0.10, 15};

#define prob_scalar (1/(sqrt(2*3.141592654)*sigma))
#define sigma_scaler ((-0.5)/(pow(sigma,2)))

inline void find_segments (std::map<uint32_t, segmenter::segment> &groupedSeg,
                            segmenter::segment_template &seg, 
                            std::vector<uint32_t> &segments)
{
    assert(segments.size() == 0);

    std::map<uint32_t, segmenter::segment>::iterator mapIt;

    double sigma;
    for (mapIt = groupedSeg.begin(); mapIt != groupedSeg.end(); mapIt++)
    {
        segmenter::segment_features &sf = (*mapIt).second.sf;

        sigma = seg.stddev*seg.length;
        double length_prob;
        length_prob = prob_scalar *exp(sigma_scaler*pow((sf.first_principle_max-
                                    sf.first_principle_min) - seg.length,2));
        sigma = seg.stddev*seg.width;
        double width_prob;
        width_prob = prob_scalar *exp(sigma_scaler*pow((sf.second_principle_max-
                                    sf.second_principle_min) - seg.width,2));
        sigma = seg.stddev*seg.first_principle_stddev;
        double length_stddev_prob;
        length_stddev_prob = prob_scalar*exp(sigma_scaler*pow(sf.first_std_dev -
                                    seg.first_principle_stddev, 2));
        sigma = seg.stddev*seg.second_principle_stddev;
        double width_stddev_prob;
        width_stddev_prob = prob_scalar*exp(sigma_scaler*pow(sf.second_std_dev -
                                    seg.second_principle_stddev,2));
        sigma = seg.stddev*seg.curvature;
        double curvature_prob;
        curvature_prob = prob_scalar*exp(sigma_scaler*pow(sf.curvature - 
                                    seg.curvature,2));
        /*
        sigma = 0.2*seg.size;
        double size_prob;
        size_prob = prob_scalar*exp(sigma_scaler*pow((sf.size - 
                                    seg.size),2));
        */
        double prob = length_prob * width_prob * length_stddev_prob *
                      width_stddev_prob * curvature_prob;// * size_prob;

        double threshold = 1;
        if (prob > threshold &&
            sf.size > 0.6*seg.size &&
            sf.size < 2*seg.size)
        {
            segments.push_back((*mapIt).first);
     //       (*mapIt).second.id = seg.template_id;
//            printf("%d - ", sf.size);
  //          printf("%lf %lf %lf %lf %lf %lf\n", length_prob,width_prob,length_stddev_prob, width_stddev_prob, curvature_prob, prob);
            
        }
    }
}

void colorer::matchGroup(std::map<uint32_t, segmenter::segment> &groupedSeg)
{
    struct segmenter::segment_group sg;
    /**/
    sg.segments.push_back(drawer);
    sg.distances.push_back(0.26);
    sg.distances.push_back(0.27);
    sg.distances.push_back(0.26);
    sg.distances.push_back(0.55);
    sg.distances.push_back(0.56);
    sg.distances.push_back(0.82);
    /**/
    /*
    sg.segments.push_back(chair_bottom);
    sg.segments.push_back(chair_back);
    sg.distances.push_back(0.35);
    */

    std::vector<uint32_t> segments;
    std::vector<segmenter::segment_template>::iterator templates;
    std::map<uint32_t, segmenter::segment>::iterator mapIt;
    for (templates = sg.segments.begin(); templates != sg.segments.end(); templates++)
    {
        find_segments(groupedSeg, *templates, segments);
    }
    // Got all segments that are related to the groups templates
    // No deleting all others
    if (segments.size() > 0)
    {
        std::vector<uint32_t>::iterator keep_seg = segments.begin();
        std::vector<uint32_t> mapErase(groupedSeg.size());
        for (mapIt = groupedSeg.begin(); mapIt != groupedSeg.end(); mapIt++)
        {
            if ((*mapIt).first == (*keep_seg))
            {
                keep_seg++;
            }
            else
            {
                mapErase.push_back((*mapIt).first);
            }
        }
        std::vector<uint32_t>::iterator eraseIt;
        for (eraseIt = mapErase.begin(); eraseIt < mapErase.end(); eraseIt++)
        {
            groupedSeg.erase(*eraseIt);
        }
    }
    else
    {
        groupedSeg.clear();
        return;
    }
    // Now only segments left are from the template

    int num_matching = 0;
    std::map<uint32_t, segmenter::segment>::reverse_iterator comparer;

    std::vector<uint32_t> group_segments;
    // For all segments in my list
    for (mapIt = groupedSeg.begin(); mapIt != groupedSeg.end(); mapIt++)
    {
        segmenter::segment_features &sf = (*mapIt).second.sf;
        Eigen::Vector3f vec;
        vec(0) = sf.xyz(0);
        vec(1) = sf.xyz(1);
        vec(2) = sf.xyz(2);

        // Compare each segment to all following segments starting at end
        // and going to the the mapIt spot
        comparer = groupedSeg.rbegin();
        for (; (*comparer).first != (*mapIt).first; comparer++)
        {
            segmenter::segment_features &sf2 = (*comparer).second.sf;
            Eigen::Vector3f vec2;
            vec2(0) = vec(0) - sf2.xyz(0);
            vec2(1) = vec(1) - sf2.xyz(1);
            vec2(2) = vec(2) - sf2.xyz(2);

            double dist = vec2.norm();

            // For every combination of segments, check whether the distances
            // are within a percentage of one of the groups distances
            // There are NO 0 distances, i.e. self distances
            // O(n^2 * G!) where n = num segments, and G is the ||group||
            std::vector<double>::iterator distances;
            for (distances = sg.distances.begin(); distances != sg.distances.end(); distances++)
            {
//                printf("%lf %lf %lf %lf\n", dist, *distances, fabs(dist - (*distances)), 0.1*(*distances));
                if (fabs(dist - (*distances)) < 0.15*(*distances))
                {
                    num_matching++;
                    group_segments.push_back((*mapIt).first);
                    group_segments.push_back((*comparer).first);
                    break;
                }
            }
        }
    }

    //printf("num_matching = %d\n", num_matching);
    int min_matching = 3;
    if (sg.distances.size() == 1) min_matching = 1;
    else if (sg.distances.size() == 3) min_matching = 2;
    else if (sg.distances.size() == 2) printf("not enough distances!\n");

    if (num_matching >= min_matching)
    {
        std::vector<uint32_t>::iterator grouped_segment;
        for (grouped_segment =group_segments.begin(); grouped_segment != group_segments.end(); grouped_segment++)
        {
            groupedSeg[*grouped_segment].id = 31;
        }
    }

/*
    for (mapIt = groupedSeg.begin(); mapIt != groupedSeg.end(); mapIt++)
    {
        segmenter::segment_features &sf = (*mapIt).second.sf;

        std::cout <<"mean = \n"<<sf.xyz<<"\n";
        std::cout <<"size = \n"<<sf.size<<"\n";
        std::cout <<"vecs = \n"<<sf.eigen_vectors<<"\n";
        std::cout <<"vals = \n"<<sf.eigen_values<<"\n";
        std::cout << "1st principle max \n" << sf.first_principle_max<<"\n";
        std::cout << "1st principle min \n" << sf.first_principle_min<<"\n";
        std::cout << "2nd principle max \n" << sf.second_principle_max<<"\n";
        std::cout << "2nd principle min \n" << sf.second_principle_min<<"\n";
        std::cout << "1st principle stddev \n" << sf.first_std_dev<<"\n";
        std::cout << "2nd principle stddev \n" << sf.second_std_dev<<"\n";
        std::cout<<"\n";
        printf("--------------------------------\n");
    }
    printf("\n");
*/
}

void colorer::matchSegments(std::map<uint32_t, segmenter::segment> &groupedSeg)
{
    std::vector<uint32_t> segments;

    //find_segments(groupedSeg, drawer, segments);
    //find_segments(groupedSeg, paper, segments);
    find_segments(groupedSeg, door, segments);
    //find_segments(groupedSeg, chair_back, segments);
    //find_segments(groupedSeg, chair_bottom, segments);

    std::map<uint32_t, segmenter::segment>::iterator mapIt;

    
    if (segments.size() > 0)
    {
        std::vector<uint32_t>::iterator keep_seg = segments.begin();
        std::vector<uint32_t> mapErase(groupedSeg.size());
        for (mapIt = groupedSeg.begin(); mapIt != groupedSeg.end(); mapIt++)
        {
            if ((*mapIt).first == (*keep_seg))
            {
                keep_seg++;
            }
            else
            {
                mapErase.push_back((*mapIt).first);
            }
        }
        std::vector<uint32_t>::iterator eraseIt;
        for (eraseIt = mapErase.begin(); eraseIt < mapErase.end(); eraseIt++)
        {
            groupedSeg.erase(*eraseIt);
        }
    }
    else
    {
        groupedSeg.clear();
    }
  
  /*
    for (mapIt = groupedSeg.begin(); mapIt != groupedSeg.end(); mapIt++)
    {
        segmenter::segment_features &sf = (*mapIt).second.sf;

        std::cout <<"mean = \n"<<sf.xyz<<"\n";
        std::cout <<"size = \n"<<sf.size<<"\n";
        std::cout <<"vecs = \n"<<sf.eigen_vectors<<"\n";
        std::cout <<"vals = \n"<<sf.eigen_values<<"\n";
        std::cout << "1st principle max \n" << sf.first_principle_max<<"\n";
        std::cout << "1st principle min \n" << sf.first_principle_min<<"\n";
        std::cout << "2nd principle max \n" << sf.second_principle_max<<"\n";
        std::cout << "2nd principle min \n" << sf.second_principle_min<<"\n";
        std::cout << "1st principle stddev \n" << sf.first_std_dev<<"\n";
        std::cout << "2nd principle stddev \n" << sf.second_std_dev<<"\n";
        std::cout<<"\n";
        printf("--------------------------------\n");
    }
    */
}


void colorer::colorSegmentsVoting(UnionFindSimple *uf, std::map<uint32_t, segmenter::segment > &groupedSeg)
{
    if (v) printf("starting to color segments\n");

    int i;
    memset(segments, 0, nnodes*sizeof(segmentVoting));

    int j, k;
    uint32_t numSegments = 0;
    for (i = 0; i < numConsistent; i++)
    {
        consistentPoint cp = consistentSegments[i];

        int t = uf->getRepresentative(cp.y*nhoriz+cp.x);

        // If this point was set to zero before, reassign it
        if (cp.lifetime == 0)
        {
            consistentSegments[i].id = t;
        }

        // If the point is small, take away its assignment
        if (uf->getSetSize(t) < minSize)
        {
            consistentSegments[i].lifetime = 0;
            consistentSegments[i].id = t;
            continue;
        }

        for (j = 0; j < numVoters; j++)
        {
            // If we are at the end of the list of voters, make a new one
            if (segments[t].votes[j] == 0)
            {
                if (j == 0) numSegments++;
                segments[t].id[j] = cp.id;
            }
            // If the segment id is this one, add to it and exit
            if (segments[t].id[j] == cp.id)
            { 
                segments[t].votes[j]++;
                break; 
            }
        }
    }

    // Add the number of segments for a size hack, will remove later
    //printf("num segments = %d\n", numSegments);
    groupedSeg[0].id = numSegments;
    uint32_t* checkDuplicatesID = (uint32_t*)malloc(numSegments*
                                                        sizeof(uint32_t));
    uint32_t* checkDuplicatesNum = (uint32_t*)malloc(numSegments*
                                                        sizeof(uint32_t));
    int end = 0;
    uint32_t max = 0, id = 0;
    for (i = 0; i < numConsistent; i++)
    {
        consistentPoint cp = consistentSegments[i];

        int t = uf->getRepresentative(cp.y*nhoriz+cp.x);

        if (uf->getSetSize(t) < minSize) continue;

        // If we haven't seen this before
        if (segments[t].votes[0] != 0)
        {
            max = 0;
            for (j = 0; j < numVoters; j++)
            {
                if (segments[t].votes[j] == 0) break;
                if (segments[t].votes[j] > max)
                {
                    max = segments[t].votes[j];
                    id = segments[t].id[j];
                }
                segments[t].votes[j] = 0;
                segments[t].id[j] = 0;
            }
            segments[t].id[0] = id;
        }


        for (j = 0; j < numSegments; j++)
        {
            if (checkDuplicatesID[j] == t)
                break;

            if (j == end)
            {
                checkDuplicatesID[j] = t;
                checkDuplicatesNum[j] = uf->getSetSize(t);
                end++;
                break;
            }
        }

        // Reset wrong segments
        if (cp.id != segments[t].id[0])
        {
            consistentSegments[i].id = segments[t].id[0];
            consistentSegments[i].lifetime = 1;
        }
        else
        {
            consistentSegments[i].lifetime++;
        }
    }

    // Insertion sort of the IDs based on the size of the segment
    // TODO get rid of this
    for (i = 1; i < numSegments; i++)
    {
        uint32_t value = checkDuplicatesNum[i];
        uint32_t id = checkDuplicatesID[i];

        for (j = i-1; j >= 0 && checkDuplicatesNum[j] < value; j--)
        {
            checkDuplicatesNum[j+1] = checkDuplicatesNum[j];
            checkDuplicatesID[j+1] = checkDuplicatesID[j];
        }
        checkDuplicatesNum[j+1] = value;
        checkDuplicatesID[j+1] = id;
    }


    // Now there is a mapping from current IDs to size

//    memset(segmented_img, 0, 3*nnodes*sizeof(uint8_t));
    memset(checkDuplicatesNum, 0, numSegments*sizeof(uint32_t));

    // Match segment IDs to consistent segment ids (in nums)
    end = 0;
    uint32_t idx;
    for (i = 0; i < numConsistent; i++)
    {
        consistentPoint cp = consistentSegments[i];
        idx = uf->getRepresentative(cp.y*nhoriz+cp.x);

        segments[idx].votes[0] = 0;
        segments[idx].id[0] = 0;

        if (uf->getSetSize(idx) < minSize) continue;

        for (j = 0; j < numSegments; j++)
        {
            if (idx == checkDuplicatesID[j] && checkDuplicatesNum[j] == 0){
                checkDuplicatesNum[j] = cp.id;
                if (++end >= numSegments) i = numConsistent;
                break;
            }
        }
    }

    for (i = 0; i < numSegments; i++)
    {
        for (j = i+1; j < numSegments; j++)
        {
            if (checkDuplicatesNum[i] == checkDuplicatesNum[j])
            {
                checkDuplicatesNum[j] = checkDuplicatesID[j];
            }
        }
    }

    std::map<uint32_t, uint32_t> idMap;

    // Now there is a mapping from current IDs to color ids
    for (i = 0; i < numConsistent; i++)
    {
        consistentPoint cp = consistentSegments[i];
        idx = uf->getRepresentative(cp.y*nhoriz+cp.x);

        for (j = 0; j < numSegments; j++)
        {
            if (idx == checkDuplicatesID[j])
            {
                if (consistentSegments[i].id != checkDuplicatesNum[j])
                {
                    consistentSegments[i].lifetime = 1;
                }
                else
                    cp.id = checkDuplicatesNum[j];

                consistentSegments[i].id = checkDuplicatesNum[j];
                break;
            }
        }

        idMap[idx] = cp.id;
        segments[idx].votes[0] += cp.lifetime;
        segments[idx].id[0]++;

    }

    for (i = 0; i < numConsistent; i++)
    {
        consistentPoint cp = consistentSegments[i];
        idx = uf->getRepresentative(cp.y*nhoriz+cp.x);

        if (segments[idx].votes[0] / segments[idx].id[0] < 4)
            idMap.erase(idx);

    }

    for (i=0; i<nnodes; i++)
    {
        int t = uf->getRepresentative(i);
 
        if (uf->getSetSize(t) < minSize)
        {
            continue;
        }
        else if (idMap.find(t) != idMap.end())
        {
            groupedSeg[idMap[(uint32_t)t]].pts.push_back(i);
            groupedSeg[idMap[(uint32_t)t]].sf.lifetime = 
                            segments[idx].votes[0] / segments[idx].id[0];
        }
    }

    free(checkDuplicatesID);
    free(checkDuplicatesNum);
}

void colorer::colorSegmentsCentroid(UnionFindSimple *uf, uint8_t* segmented_img)
{
    if (v) printf("starting to color segments\n");

    int i;
    // Move centers
    for (i = 0; i < numConsistent; i++)
    {
        consistentPoint cp = consistentSegments[i];

        // If the count is 0 (the end 
        if (cp.count == ~0)
        {
            break;
        }

        // Get the parent ID
        int t = uf->getRepresentative(cp.y*nhoriz+cp.x);

        // change the point location to that point
        consistentSegments[i].x = t%nhoriz;
        consistentSegments[i].y = t/nhoriz;
    }

    // Combine points
    int j;
    for (i = 0; i < numConsistent; i++)
    {
        consistentPoint cp = consistentSegments[i];

        if (cp.count == ~0)
            break;

        if (cp.count == 0)
            continue;

        uint16_t x = cp.x;
        uint16_t y = cp.y;

        for (j = i+1; j < numConsistent; j++)
        {
            consistentPoint cpj = consistentSegments[j];

            if (cpj.count == ~0) break;
            if (cpj.count == 0) continue;

            if (cpj.x == x && cpj.y == y)
            {
                consistentSegments[i].count += cpj.count;
                consistentSegments[j].count = 0;
            }
        }
    }

    // Delete small ones
    for (i = 0; i < numConsistent; i++)
    {
        consistentPoint cp = consistentSegments[i];

        if (cp.count == ~0)
            break;

        if (cp.count == 0)
            continue;

        if (uf->getSetSize(cp.y*nhoriz+cp.x) <= 1)
            consistentSegments[i].count = 0;
    }

    int lastZero;
    // Find first zero;
    for (lastZero = 0; lastZero < numConsistent; lastZero++)
    {
        if (consistentSegments[lastZero].count == 0 ||
            consistentSegments[lastZero].count == ~0)
            break;
    }

    // Shuffle the values to the front
    for (i = lastZero+1; i < numConsistent; i++)
    {
        // if at the end of the known values
        if (consistentSegments[i].count == ~0)
        {
            break;
        }

        if (consistentSegments[i].count != 0)
        {
            consistentSegments[lastZero].x = consistentSegments[i].x;
            consistentSegments[lastZero].y = consistentSegments[i].y;
            consistentSegments[lastZero].count = consistentSegments[i].count;

            consistentSegments[i].count = 0;;

            lastZero++;
        }
    }
    printf("there are %d segments\n", lastZero);

    // fill in remaining 0s
    for (;lastZero < i; lastZero++)
    {
        consistentSegments[lastZero].count = ~0;
    }

    segmented_img = (uint8_t*)memset(segmented_img, 0, 3*nnodes);

    for (i = 0; i < numConsistent; i++)
    {
        if (consistentSegments[i].count == ~0)
        {
            break;
        }
        consistentPoint cp = consistentSegments[i];
        segmented_img[3*(cp.y*nhoriz+cp.x)] = cp.count%256;
        segmented_img[3*(cp.y*nhoriz+cp.x)+1] = cp.count*3%256;
        segmented_img[3*(cp.y*nhoriz+cp.x)+2] = cp.count*5%256;
    }

    for (i=0; i<nnodes; i++){ 
        int t = uf->getRepresentative(i);

        if (segmented_img[3*t] != 0)
        {
            segmented_img[3*i] = segmented_img[3*t];
            segmented_img[3*i+1] = segmented_img[3*t+1];
            segmented_img[3*i+2] = segmented_img[3*t+2];
        }
        else if (uf->getSetSize(t) < minSize)
        {
            segmented_img[3*i] = 0;
            segmented_img[3*i+1] = 0;
            segmented_img[3*i+2] = 0;
        }
        else if (i == t)
        {
            segmented_img[3*i] = 0;
            segmented_img[3*i+1] = 0xFF;
            segmented_img[3*i+2] = 0;
//printf("drawing green dot\n");
        }
        else
        {
            segmented_img[3*i] = 0;//t%256;
            segmented_img[3*i+1] = 0;//t%256;
            segmented_img[3*i+2] = 0;//t%256;
//printf("default\n");
        }
    }
    if (v) printf("Finished coloring segments\n");
    count++;
}

colorer::colorer(uint16_t nv, uint16_t nh, uint8_t DECI, bool verbose)
{
    v = verbose;
    nvert = nv;
    nhoriz = nh;
    deci = DECI;
    count = 0;

    minSize = 240/(DECI*DECI);
    spacing = 8;

    if (v) printf("starting segmenter\n");
    nnodes = nvert*nhoriz;
    numConsistent = nnodes / (spacing*spacing);

    consistentSegments = (consistentPoint*)calloc(numConsistent, sizeof(consistentPoint));
    segments = (segmentVoting*)malloc(nnodes * sizeof(segmentVoting));

    // Odd indexing for making id's different enough
    int i;
    for (i = 0; i < nnodes/spacing; i+=spacing)
    {
        consistentPoint cp;
        cp.x = i%nhoriz;
        cp.y = i/nhoriz*spacing;
        cp.count = 1;
        cp.id = i;
        cp.lifetime = 0;
        consistentSegments[i/spacing] = cp;
    }

}
/*
~colorer()
{
    free(consistentSegments);
}
*/

void colorer::drawCross(uint8_t *segmented_img, uint16_t trackX, uint16_t trackY)
{
        if (v) printf("Starting to draw cross\n");
        segmented_img[3*((trackY)*nhoriz+(trackX))] = 0xFF;
        segmented_img[3*((trackY)*nhoriz+(trackX))+1] = 0;
        segmented_img[3*((trackY)*nhoriz+(trackX))+2] = 0;

        segmented_img[3*((trackY)*nhoriz+(trackX-1))] = 0xFF;
        segmented_img[3*((trackY)*nhoriz+(trackX-1))+1] = 0;
        segmented_img[3*((trackY)*nhoriz+(trackX-1))+2] = 0;

        segmented_img[3*((trackY)*nhoriz+(trackX-2))] = 0xFF;
        segmented_img[3*((trackY)*nhoriz+(trackX-2))+1] = 0;
        segmented_img[3*((trackY)*nhoriz+(trackX-2))+2] = 0;

        segmented_img[3*((trackY)*nhoriz+(trackX+1))] = 0xFF;
        segmented_img[3*((trackY)*nhoriz+(trackX+1))+1] = 0;
        segmented_img[3*((trackY)*nhoriz+(trackX+1))+2] = 0;

        segmented_img[3*((trackY)*nhoriz+(trackX+2))] = 0xFF;
        segmented_img[3*((trackY)*nhoriz+(trackX+2))+1] = 0;
        segmented_img[3*((trackY)*nhoriz+(trackX+2))+2] = 0;

        segmented_img[3*((trackY+1)*nhoriz+(trackX))] = 0xFF;
        segmented_img[3*((trackY+1)*nhoriz+(trackX))+1] = 0;
        segmented_img[3*((trackY+1)*nhoriz+(trackX))+2] = 0;

        segmented_img[3*((trackY+2)*nhoriz+(trackX))] = 0xFF;
        segmented_img[3*((trackY+2)*nhoriz+(trackX))+1] = 0;
        segmented_img[3*((trackY+2)*nhoriz+(trackX))+2] = 0;

        segmented_img[3*((trackY-1)*nhoriz+(trackX))] = 0xFF;
        segmented_img[3*((trackY-1)*nhoriz+(trackX))+1] = 0;
        segmented_img[3*((trackY-1)*nhoriz+(trackX))+2] = 0;

        segmented_img[3*((trackY-2)*nhoriz+(trackX))] = 0xFF;
        segmented_img[3*((trackY-2)*nhoriz+(trackX))+1] = 0;
        segmented_img[3*((trackY-2)*nhoriz+(trackX))+2] = 0;

        if (v) printf("finished drawing cross\n");
}

void colorer::drawBox(uint8_t *segmented_img, segmentFeatures sf)
{
    if (v) printf("Starting to draw box\n");

    uint16_t minJ = (sf.minJ < 20/deci) ? 0:(sf.minJ-20/deci);
    uint16_t minI = (sf.minI < 20/deci) ? 0:(sf.minI-20/deci);

    uint16_t maxJ = (sf.maxJ>nvert-21/deci)?(nvert-1):(sf.maxJ+20/deci);
    uint16_t maxI = (sf.maxI>nhoriz-21/deci)?(nhoriz-1):(sf.maxI+20/deci);

    int i,j;

    for (i = minI; i < maxI; i++)
    {
        segmented_img[3*(minJ * nhoriz + i)] = 0xFF;
        segmented_img[3*(minJ * nhoriz + i) + 1] = 0;
        segmented_img[3*(minJ * nhoriz + i) + 2] = 0;

        segmented_img[3*(maxJ * nhoriz + i)] = 0xFF;
        segmented_img[3*(maxJ * nhoriz + i) + 1] = 0;
        segmented_img[3*(maxJ * nhoriz + i) + 2] = 0;
    }


    for (j = minJ; j < maxJ; j++)
    {
        segmented_img[3*(j * nhoriz + minI)] = 0xFF;
        segmented_img[3*(j * nhoriz + minI) + 1] = 0;
        segmented_img[3*(j * nhoriz + minI) + 2] = 0;

        segmented_img[3*(j * nhoriz + maxI)] = 0xFF;
        segmented_img[3*(j * nhoriz + maxI) + 1] = 0;
        segmented_img[3*(j * nhoriz + maxI) + 2] = 0;
    }
    if (v) printf("finished drawing box\n");
}
