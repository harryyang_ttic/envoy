#include <stdio.h>
#include <bot_core/bot_core.h>
#include <map>
#include <vector>

#include "UnionFindSimple.hpp"
#include "segmenter.cpp"

class colorer{

private:

    int minSize;
    uint8_t spacing;

    uint32_t nnodes;
    uint32_t numConsistent;

    bool v;
    uint16_t nvert, nhoriz;
    uint8_t deci;
    uint8_t count;

    struct segmentFeatures{
        uint16_t minJ, minI, maxJ, maxI;
    };

    struct consistentPoint{
        uint16_t x;
        uint16_t y;
        uint32_t count;
        uint32_t id;
        uint16_t lifetime;
    };

    static const uint8_t numVoters = 50;

    struct segmentVoting{
        uint32_t votes[numVoters];
        uint32_t id[numVoters];
    };

    segmentVoting* segments;
    consistentPoint* consistentSegments;

    struct segmentFeature{
        // Along axis
        float x, y, z;

        uint8_t r, g, b;
        float colorVariance;
    };


    void drawCross(uint8_t*, uint16_t, uint16_t);
    void drawBox(uint8_t*, segmentFeatures);

public:

    colorer(uint16_t, uint16_t, uint8_t, bool);
    void colorSegmentsVoting(UnionFindSimple *uf, std::map<uint32_t, segmenter::segment > &groupedSeg);
    void colorSegmentsCentroid(UnionFindSimple *uf, uint8_t* segmented_img);

    void matchSegments(std::map<uint32_t, segmenter::segment> &groupedSeg);
    void matchGroup(std::map<uint32_t, segmenter::segment> &groupedSeg);

};
