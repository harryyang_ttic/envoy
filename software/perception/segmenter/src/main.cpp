#include <stdio.h>
#include <pthread.h>

#include <string.h>

#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
#include <lcmtypes/erlcm_person_tracking_cmd_t.h>
#include <lcmtypes/erlcm_person_tracking_status_t.h>
#include <lcmtypes/kinect_point2d_t.h>

#include <pcl/common/common_headers.h>

#include "kinectGrabber.cpp"
#include "segmenter.cpp"
#include "colorer.hpp"

static uint8_t DECI;

struct drawingStruct
{
    UnionFindSimple *uf;
    pthread_mutex_t drawingMutex;
    pthread_cond_t drawingCond;
    lcm_t *lcm;
    uint8_t* segmentedImg;
    uint16_t nvert;
    uint16_t nhoriz;
    bool v;
};

void* lcm_thread_handler(void *l)
{
    lcm_t* lcm = (lcm_t*)(l);
    while(1)
	{
            lcm_handle(lcm);
	}
}

static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -v      Set verbose to true.\n"
             "  -d      Publish segmented image.\n"
             "  -l      Use libfreenect driver.\n"
             "  -u      Sub sample.\n"
             "  -h      This help message.\n",
             g_path_get_basename(progname));
    exit(1);
}

void checkResults(int rc)
{
    if (rc != 0)
    {
        printf("Pthread error, exiting out now\n");
        exit(1);
    }
}

// Draw the segmented image
void* drawImage(void *dS){
    drawingStruct *drawing = (drawingStruct*)dS;

    colorer color(drawing->nvert, drawing->nhoriz, DECI, drawing->v);

    while(1)
    {
        // Grab hold of mutex?
        pthread_cond_wait(&(drawing->drawingCond), &(drawing->drawingMutex));

//        color.colorSegmentsVoting(drawing->uf, drawing->segmentedImg);

//        color.colorSegmentsCentroid(drawing->uf, drawing->segmentedImg);

        bot_core_image_t image;
        image.utime = 0;
        image.width = drawing->nhoriz;
        image.height = drawing->nvert;
        image.row_stride = image.width*3;
        image.pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB;//GRAY;//RGB;
        image.size = image.height*image.row_stride;
        image.data = drawing->segmentedImg;
        image.nmetadata = 0;
        image.metadata = NULL;

        bot_core_image_t_publish(drawing->lcm,"Segmented Image",&image);

        int rc = pthread_mutex_unlock(&(drawing->drawingMutex));
        checkResults(rc);
    }
}

void drawImage(lcm_t *lcm, uint16_t nvert, uint16_t nhoriz, 
        std::map<uint32_t, segmenter::segment > groupedSeg, char* channel){

    // Created segmented Img with all zeros
    uint8_t segmentedImg[nvert*nhoriz*3];// = {0};
    memset(segmentedImg, 0, nvert*nhoriz*3*sizeof(uint8_t));

    std::map<uint32_t, segmenter::segment>::iterator mapIt;
    for (mapIt = groupedSeg.begin(); mapIt != groupedSeg.end(); mapIt++)
    {
        uint32_t id = (*mapIt).second.id;
        std::vector<uint32_t>::iterator it;
        for (it = (*mapIt).second.pts.begin(); it != (*mapIt).second.pts.end(); it++)
        {
            uint32_t pos = (*it);
            segmentedImg[3*pos] = id&0xFF;
            segmentedImg[3*pos+1] = (id*3)&0xFF;
            segmentedImg[3*pos+2] = (id*5)&0xFF;
        }
    }

        bot_core_image_t image;
        image.utime = 0;
        image.width = nhoriz;
        image.height = nvert;
        image.row_stride = image.width*3;
        image.pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB;//GRAY;//RGB;
        image.size = image.height*image.row_stride;
        image.data = segmentedImg;
        image.nmetadata = 0;
        image.metadata = NULL;

        bot_core_image_t_publish(lcm, channel, &image);
}

int main(int argc, char **argv){
    printf("starting\n");

    int c, rc;
    bool v = false;
    bool draw = false;
    bool libfreenect = false;
    bool raw = false;
    DECI = 1;
    // command line options - to throtle - to ignore image publish  
    while ((c = getopt (argc, argv, "hvdpu:lcr")) >= 0) {
    	switch (c) {
    	case 'v': //ignore images 
            v = true;
            printf("Setting verbose to true\n");
            break;
    	case 'r': //ignore images 
            raw = true;
            printf("Setting raw to true\n");
            break;
        case 'd':
            draw = true;
            printf("Drawing segmented images\n");
            break;
        case 'l':
            libfreenect = true;
            printf("Running with libfreenect\n");
            break;
        case 'u':
            DECI = atoi(optarg);
            printf("Subsampling by %d\n", DECI);
            break;
      	case 'h':
        default:
            usage(argv[0]);
            break;
        }
    }

    lcm_t* lcm = lcm_create(NULL);
    pthread_mutex_t drawing_mutex = PTHREAD_MUTEX_INITIALIZER;


    int width = 640;
    int height = 480;
    uint16_t nvert = height / DECI;
    uint16_t nhoriz = width / DECI;
    uint32_t nnodes = nvert*nhoriz;

    uint64_t* newEdgeSortList = (uint64_t*)malloc(nnodes*(NUM_EDGES-1)*sizeof(uint64_t));

    kinectGrabber kg(lcm, DECI, libfreenect, v);
    segmenter seg(nvert, nhoriz, DECI, v);
    colorer color(nvert, nhoriz, DECI, v);

    UnionFindSimple uf(nnodes);

    if (v) printf("starting lcm thread\n");
    pthread_t lcm_thread;
    rc = pthread_create(&lcm_thread, NULL, lcm_thread_handler, lcm);
    checkResults(rc);

    static const pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud_ptr (new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>& cloud = *point_cloud_ptr;

    struct timeval tva, tvb,tvc,tvd,tve, tvf, tvg, tvh;
    int i;
    double time1 = 0;
    int j = 0;
    for (i=0;;i++)
    {
        gettimeofday(&tva, NULL);
        bool newData = kg.get_kinect_data(newEdgeSortList, cloud);
        gettimeofday(&tvb, NULL);
        if (newData == true)
        {
            uf.reset();
            if (v) printf("segmenting\n");
            seg.segmentGraph(&uf, newEdgeSortList);
            gettimeofday(&tvc,NULL);
            //seg.joinGraph(&uf, newEdgeSortList);

            gettimeofday(&tvd,NULL);

            std::map<uint32_t, segmenter::segment > groupedSeg;
            color.colorSegmentsVoting(&uf, groupedSeg);
            gettimeofday(&tve,NULL);

            seg.getFeatures(groupedSeg, cloud);
            gettimeofday(&tvf,NULL);
            /*
            printf("size = %d, num = %d\n", groupedSeg.size(), groupedSeg.begin()->second.id);
            double size = groupedSeg.size();
            double num_segments = groupedSeg.begin()->second.id;
            if (num_segments - size > 15)
            {
                printf("cluttered\n");
            }
            else
                printf("not cluttered\n");
*/
            if (draw == true)
            {
                drawImage(lcm, nvert, nhoriz, groupedSeg, "RAW_SEGMENTATION");
            }

            if (raw == false)
            {
                color.matchSegments(groupedSeg);
//                color.matchGroup(groupedSeg);
            }
            gettimeofday(&tvg,NULL);


            if (draw == true)
            {
                if(v) printf("drawing segmented image\n");
                drawImage(lcm, nvert, nhoriz, groupedSeg, "SEGMENTED_IMG");

                gettimeofday(&tvh,NULL);
            }
/**
printf ("getting data = %lf\n", seg.getTimeDiff(tvb, tva) );
printf ("segmenting = %lf\n", seg.getTimeDiff(tvc, tvb) );
printf ("joining = %lf\n", seg.getTimeDiff(tvd, tvc) );
printf ("coloring = %lf\n", seg.getTimeDiff(tve, tvd) );
printf ("getting features = %lf\n", seg.getTimeDiff(tvf, tve) );
printf ("matching = %lf\n", seg.getTimeDiff(tvg, tvf) );
printf ("drawing = %lf\n", seg.getTimeDiff(tvh, tvg) );
printf ("total = %lf\n", seg.getTimeDiff(tvh, tva) );
/**/
            j++;
            time1 += (1000/(seg.getTimeDiff(tvh,tva)));
            printf("total fps = %lf\n\n", time1/j);
            cloud.clear();
        }
        else
        {
            if (v) printf("There is no new kinect Data, is it publishing?\n");
            usleep(10000);
        }
    }
}
