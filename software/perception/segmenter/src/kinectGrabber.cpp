#include <lcm/lcm.h>
#include <lcmtypes/kinect_frame_msg_t.h>
#include <bot_core/bot_core.h>

/*#include "libfreenect.h"
#include "libfreenect_sync.h"
#include <stdio.h>

#include <stdlib.h>
#include <math.h>
#include <kinect/kinect-utils.h>
*/
#include <zlib.h>
#include "jpeg-utils-ijg.h"
#include <pthread.h>
#include <errno.h>

#define numThreads (1)

#define NUM_EDGES 3
#define COUNT_SORT_SIZE 6000
#define IDA_MASK 0xFFFFF
#define IDB_MASK 0xFFFFF
#define NO_EDGE 0xFFFFF
#define IDA_SHIFT 44
#define IDB_SHIFT 24
#define WEIGHT_SHIFT 0
#define WEIGHT_MASK 0xFFFFF

class kinectGrabber
{
private:

    bool v;
    bool libfreenect;
    double* xyz;
    uint8_t* rgb;
    uint8_t* grey;

    uint32_t* edges;
    uint64_t* edgeSortList;
    uint64_t* newEdgeSortList;
    uint64_t* finishedEdgeList;
    uint64_t* counts;

    uint16_t nvert, nhoriz;
    uint32_t nnodes, nedges;

    static const uint16_t wid = 640, hei = 480;
    static const double max_edge_length = 0.10;

    uint16_t* depth_image;
    uint8_t* rgb_image;
    uint8_t* grad_img;
    uint8_t deci;

    pthread_mutex_t m, workMutex;
    pthread_cond_t cond;
    bool newData;

    struct buildStruct{
        pthread_barrier_t *barrier;

        uint32_t start;
        uint32_t end;
    };


        pthread_barrier_t barr;


    double RawDepthToMeters(uint16_t depthValue){
        if (depthValue < 2047) {
            return (1.0 /((double)(depthValue) * -0.0030711016 + 3.3309495161));
        }
        return 0.0f;
    }

    double getTimeDiff(timeval a, timeval b)
    {
        double first = a.tv_sec + (a.tv_usec/1000000.0);
        double second = b.tv_sec + (b.tv_usec/1000000.0);

        return (first - second)*1000;
    }

    inline void waitThread()
    {
        pthread_cond_wait(&cond, &workMutex);
        pthread_mutex_unlock(&workMutex);
    }

    inline int threadBarrier(pthread_barrier_t *barri)
    {
        int rc = pthread_barrier_wait(barri);
        if(rc != 0 && rc != PTHREAD_BARRIER_SERIAL_THREAD)
        {
            printf("Could not wait on barrier\n");
            exit(-1);
        }
        return rc;
    }

    void* runThreads(buildStruct *threadInfo)
    {

        timeval tva, tvd, tve;
        double fx_d = 1.0 / 5.9421434211923247e+02;
        double fy_d = 1.0 / 5.9104053696870778e+02;
        double cx_d = 3.3930780975300314e+02;
        double cy_d = 2.4273913761751615e+02;

        if (threadBarrier(threadInfo->barrier) == PTHREAD_BARRIER_SERIAL_THREAD)
        { 
            waitThread(); 
        }

//        uint32_t edgeStartIdx = threadInfo->start*(NUM_EDGES-1);
//        uint32_t edgeEndIdx = threadInfo->end*(NUM_EDGES-1) + NUM_EDGES - 1;

        while(1)
        {

            threadBarrier(&barr);
            if(v) printf("KG: starting to process data\n");

            gettimeofday(&tva, NULL);

            uint32_t idx = threadInfo->start;
            uint32_t rawIdx = idx * deci;
            uint32_t edgePos;
            int i, j;

            for (; idx < threadInfo->end; idx++, rawIdx += deci){
                i = idx%nhoriz;
                j = idx/nhoriz;
                if (libfreenect == true)
                {
                    double fx_d = 1.0 / 5.9421434211923247e+02;
                    double fy_d = 1.0 / 5.9104053696870778e+02;
                    double cx_d = 3.3930780975300314e+02;
                    double cy_d = 2.4273913761751615e+02;

                    double depth = RawDepthToMeters((depth_image)[rawIdx]);
                    // Don't need xy
                    xyz[(idx)*3] = ((i - cx_d) * depth * fx_d);
                    xyz[(idx)*3 + 1] = ((j-cy_d) * depth * fy_d);
                    xyz[(idx)*3 + 2] = depth;//(depth > 3)?3:depth;
                }
                else
                {
//                    xyz[(idx)*3] = 0;
//                    xyz[(idx)*3 + 1] = 0;
                    double depth = depth_image[rawIdx]/1000.0;
                    xyz[(idx)*3] = ((i - cx_d) * depth * fx_d);
                    xyz[(idx)*3 + 1] = ((j-cy_d) * depth * fy_d);
                    xyz[(idx)*3 + 2] = depth;
                }

                uint8_t r, g, b;
                r = rgb_image[rawIdx*3];
                g = rgb_image[rawIdx*3+1];
                b = rgb_image[rawIdx*3+2];
                rgb[(idx)*3] = rgb_image[rawIdx*3];
                rgb[(idx)*3+1] = rgb_image[rawIdx*3+1];
                rgb[(idx)*3+2] = rgb_image[rawIdx*3+2];

                grey[idx] = (uint8_t) (((uint16_t)r+(uint16_t)g+(uint16_t)b)/3);
                if (idx > nhoriz && idx%nhoriz != 0)
                { 
                    grad_img[idx] = abs(grey[idx] -grey[idx-nhoriz])
                                        + abs(grey[idx] - grey[idx-1]);
                }
                else grad_img[idx] = 0xFF;

                edgePos = idx*NUM_EDGES;
                edges[edgePos]   = idx;
                edges[edgePos+1] = idx + 1;          // The right node
                edges[edgePos+2] = idx + nhoriz;     // the bottom node
            }
            if(v) printf("Finished converting depth to XYZ\n");

            threadBarrier(threadInfo->barrier);

            idx = threadInfo->start;
            i = idx *(NUM_EDGES-1);
            for (; idx < threadInfo->end; idx++)
            {
               edgePos = idx*NUM_EDGES;

                if (xyz[3*(idx)+2] <= 0.01 || idx%nhoriz == nhoriz-1 ||
                    idx/nhoriz == nvert-1 || grad_img[idx] > 20 || 
                    fabs(xyz[idx*3 + 2] - xyz[(idx+1)*3+2])
                                                            > max_edge_length ||
                    fabs(xyz[idx*3 + 2] - xyz[(idx+nhoriz)*3+2])
                                                            > max_edge_length)
                {
                    /*
                    if (fabs(xyz[idx*3 + 2] - xyz[edges[edgePos+1]*3+2]) > max_edge_length ||
                        fabs(xyz[idx*3 + 2] - xyz[edges[edgePos+2]*3+2]) > max_edge_length)
                    {
                        printf("%lf, %lf\n",fabs(xyz[idx*3 + 2] - xyz[edges[edgePos+1]*3+2]),
                                            fabs(xyz[idx*3 + 2] - xyz[edges[edgePos+2]*3+2]));
                    }
                    */
                    //printf(" - %lf %lf %lf\n", xyz[idx*3+2], xyz[edges[edgePos+1]*3+2], xyz[edges[edgePos+2]*3+2]);
                    edgeSortList[i++] = NO_EDGE;
                    edgeSortList[i++] = NO_EDGE;
                    continue;
                }
                uint32_t aBig = idx;


                for (j = 1; j < NUM_EDGES; j++){
                    int bBig = edges[++edgePos];

                    // Explicitly done due to casting confusion
                    uint64_t ida = aBig & IDA_MASK;
                    ida = ida << IDA_SHIFT;
                    uint64_t idb = bBig & IDB_MASK;
                    idb = idb << IDB_SHIFT;

                    uint64_t error = ida | idb;
/**/
                    double t = 100* ((double)(sqrt(
                                    pow(rgb[aBig*3]-rgb[bBig*3],2) +
                                    pow(rgb[aBig*3+1]-rgb[bBig*3+1],2) +
                                    pow(rgb[aBig*3+2]-rgb[bBig*3+2],2))));

                    error |= (uint64_t(t)) & WEIGHT_MASK;
                    edgeSortList[i++] = error;

                    // If there is no edge, the value of edgeSortList doesn't matter
                }
            }

            if (v) printf("KG: Finished building all edges\n");
            // Make sure all the threads are done
            i = threadBarrier(threadInfo->barrier);
            if (i == PTHREAD_BARRIER_SERIAL_THREAD)
            {
                gettimeofday(&tvd, NULL);

                for (i = 0; i < nedges; i++)
                {
                    int a = (int)((((long long)(edgeSortList[i])) >> IDA_SHIFT)
                                                                    & IDA_MASK);
                    int b = (int)((((long long)(edgeSortList[i])) >> IDB_SHIFT)
                                                                    & IDB_MASK);

                    if(b != NO_EDGE)
                    {
                        int w = (int) ((edgeSortList[i] & WEIGHT_MASK));
                        if (w >= COUNT_SORT_SIZE) w = COUNT_SORT_SIZE - 1;
                        counts[w]++;
                    }
                }

                // accumulate
                for(j = 1; j < COUNT_SORT_SIZE; j++){
                    counts[j] += counts[j-1];
                }

                pthread_mutex_lock(&m);
                for(j = 0; j < nedges; j++)
                {
                    int a = (int)((((long long)(edgeSortList[j])) >> IDA_SHIFT)
                                                                  & IDA_MASK);
                    int b = (int)((((long long)(edgeSortList[j])) >> IDB_SHIFT)
                                                                  & IDB_MASK);

                    if(b != NO_EDGE)
                    {
                        int w = (int) (edgeSortList[j] & WEIGHT_MASK);
                        if (w >= COUNT_SORT_SIZE) w = COUNT_SORT_SIZE-1;
                        int temp = --counts[w];
//                        newEdgeSortList[temp] = edgeSortList[j];
                        finishedEdgeList[temp] = edgeSortList[j];
                        edgeSortList[j] = ~0;
                    }
                }
                
                if (v) printf("KG: finished sorting edges\n");

                newData = true;
                pthread_mutex_unlock(&m);

                gettimeofday(&tve, NULL);
//                printf("last sort = %lf\n", getTimeDiff(tve, tvd));
//                if (v) 
//                printf("total build time = %lf\n",getTimeDiff(tve,tva));
                memset(counts, 0, COUNT_SORT_SIZE*sizeof(uint64_t));
                memset(newEdgeSortList, ~0, nedges*sizeof(uint64_t));
                waitThread();
            }
        }
    }

    void kinect_handler(const lcm_recv_buf_t* lcm, const char* chanel,
                        const kinect_frame_msg_t* msg){
        if (v) printf("reading kinect\n");
        // If there is already a new graph ready, ignore.
        // Ignores the latest, but works in practice well
        if (newData == true)
        {
            if (v) printf("There is already new data\n");
            return;
        }

        if(msg->image.image_data_format == KINECT_IMAGE_MSG_T_VIDEO_RGB) {
            if (v) printf("reading image\n");
            memcpy(rgb_image, msg->image.image_data,hei*wid*3*sizeof(uint8_t));
        } else if(msg->image.image_data_format == KINECT_IMAGE_MSG_T_VIDEO_RGB_JPEG) {
            jpegijg_decompress_8u_rgb(msg->image.image_data,
                                      msg->image.image_data_nbytes, rgb_image, wid,
                                      hei, wid * 3);
        }
        
        if(msg->depth.compression == KINECT_DEPTH_MSG_T_COMPRESSION_NONE) {
            memcpy(depth_image, msg->depth.depth_data,hei*wid*sizeof(uint16_t));
        }  else if (msg->depth.compression ==
                    KINECT_DEPTH_MSG_T_COMPRESSION_ZLIB) {
            if (v) printf("decompressing\n");
            unsigned long dlen = msg->depth.uncompressed_size; 
            uncompress((uint8_t*)(depth_image), &dlen, msg->depth.depth_data,
                       msg->depth.depth_data_nbytes);
        }

        if(v) printf("got all kinect data\n");

        // If the thread is ready, start it
        pthread_cond_broadcast(&cond);
        if (v) printf("finished running kinect handler\n");
    }

    static void
    lcm_handler(const lcm_recv_buf_t* lcm, const char* chanel,
                const kinect_frame_msg_t* msg, void* user){
        kinectGrabber* s = static_cast<kinectGrabber*>(user);
        if (s != NULL)
            {
                s->kinect_handler(lcm, chanel, msg);
            }
        else printf("message is null\n");
        
    }

    struct threadArgs{
        kinectGrabber* kg;
        buildStruct *args;
    };

    static void* threadSetup(void* pv)
    {
        threadArgs* tArgs = static_cast<threadArgs*>(pv);
        kinectGrabber* kg = tArgs->kg;
        kg->runThreads(tArgs->args);
    }

public:

    kinectGrabber(lcm_t* l, uint8_t decimate, bool lf, bool verbose)
    {
        if (verbose) printf("starting kinect grabber\n");
        m = PTHREAD_MUTEX_INITIALIZER;
        workMutex = PTHREAD_MUTEX_INITIALIZER;
        cond = PTHREAD_COND_INITIALIZER;
        libfreenect = lf;
        v = verbose;
        deci = decimate;

        nvert = hei / deci;
        nhoriz = wid / deci;
        nnodes = nvert*nhoriz;
        nedges = nnodes*(NUM_EDGES-1);

        newData = false;

        depth_image = (uint16_t*)malloc((hei*wid)*sizeof(uint16_t));
        rgb_image = (uint8_t*)malloc((hei*wid)*3*sizeof(uint8_t));
        xyz = (double*) malloc(3*nnodes*sizeof(double));
        rgb = (uint8_t*) malloc(3*nnodes*sizeof(uint8_t));
        grey = (uint8_t*) malloc(nnodes*sizeof(uint8_t));
        grad_img = (uint8_t*) malloc(nnodes*sizeof(uint8_t));

        edgeSortList = (uint64_t*)malloc(nedges* sizeof(uint64_t));
        newEdgeSortList = (uint64_t*)malloc(nedges*sizeof(uint64_t));
        finishedEdgeList = (uint64_t*)malloc(nedges*sizeof(uint64_t));
        edges = (uint32_t*)calloc((nnodes*NUM_EDGES), sizeof(uint32_t));
        counts = (uint64_t*)malloc(COUNT_SORT_SIZE*sizeof(uint64_t));

//        memset(edgeSortList, ~0, nedges*sizeof(uint64_t));
        memset(newEdgeSortList, ~0, nedges*sizeof(uint64_t));

//        pthread_barrier_t barr;

        if (pthread_barrier_init(&barr, NULL, numThreads))
        {
            printf("Could not create a barrier\n"); 
            exit(1);
        }

        buildStruct *run = (buildStruct*)malloc(sizeof(buildStruct)*numThreads);
        pthread_t *thread = (pthread_t*)malloc(sizeof(pthread_t)*numThreads);
        threadArgs *ta = (threadArgs*)malloc(sizeof(threadArgs)*numThreads);

        int i;
        for (i = 0; i < numThreads; i++){
            run[i].start = (nnodes/numThreads)*i; 
            run[i].end = (nnodes/numThreads)*(i+1); 
            run[i].barrier = &barr;
            
            ta[i].kg = this;
            ta[i].args = &(run[i]);
            pthread_create(&thread[i], NULL, &threadSetup, &ta[i]);
        }

        kinect_frame_msg_t_subscribe(l, "KINECT_FRAME", lcm_handler, this);

        if (v) printf("finished kinect initializer\n");
    }

    ~kinectGrabber()
    {
        free(depth_image);
        free(rgb_image);
        free(xyz);
        free(rgb);
        free(grey);
        free(grad_img);

        free(edgeSortList);
        free(newEdgeSortList);
        free(edges);
        free(counts);
    }

    bool get_kinect_data(uint64_t *doneEdgeList, pcl::PointCloud<pcl::PointXYZRGB>& cloud)
    {
        if (v) printf("getting kinect data\n");
        if (newData == false)
        {
            return false;
        }
        pthread_mutex_lock(&m);
        memcpy(doneEdgeList, finishedEdgeList, nedges*sizeof(uint64_t));

        int i, j = 0;
        for (i = 0; i< nnodes; i++)
        {
            pcl::PointXYZRGB point(rgb[j], rgb[j+1], rgb[j+2]);
            point.x = xyz[j++];
            point.y = xyz[j++];
            point.z = xyz[j++];
            cloud.points.push_back(point);
        }

        newData = false;
        pthread_mutex_unlock(&m);

        if (v) printf("Got kinect data\n");
        return true;
    }
};
