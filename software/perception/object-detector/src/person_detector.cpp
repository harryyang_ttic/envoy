/* $Revision: 1.8.4.3 $ */
/*
 *	engdemo.c
 *
 *	This is a simple program that illustrates how to call the MATLAB
 *	Engine functions from a C program.
 *
 * Copyright 1984-2003 The MathWorks, Inc.
 * All rights reserved
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "engine.h"
#include <lcm/lcm.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <bot_vis/bot_vis.h>
#include <image_utils/pixels.h>
#include <image_utils/jpeg.h>

#include <er_common/envoy_opencv_utils.hpp>

#define  BUFSIZE 256
//refere
//http://www.mathworks.com/help/techdoc/apiref/bqoqnz0.html

void call_matlab(Engine *ep)
{
  mxArray *T = NULL, *result = NULL;
  char buffer[BUFSIZE+1];
  double time[10] = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0 };
  /* 
   * Create a variable for our data
   */
  T = mxCreateDoubleMatrix(1, 10, mxREAL);
  memcpy((void *)mxGetPr(T), (void *)time, sizeof(time));
  /*
   * Place the variable T into the MATLAB workspace
   */
  engPutVariable(ep, "T", T);

  /*
   * Evaluate a function of time, distance = (1/2)g.*t.^2
   * (g is the acceleration due to gravity)
   */
  engEvalString(ep, "D = .5.*(-9.8).*T.^2;");

  /*
   * Plot the result
   */
  engEvalString(ep, "plot(T,D);");
  engEvalString(ep, "title('Position vs. Time for a falling object');");
  engEvalString(ep, "xlabel('Time (seconds)');");
  engEvalString(ep, "ylabel('Position (meters)');");

  sleep(1);
  printf("Done for Part I.\n");
  mxDestroyArray(T);
  engEvalString(ep, "close;");
  mxDestroyArray(result);
}


  // generate color image with three "horizontal bands" in 
  // blue, green and red respectively
IplImage* gen_color_img ()
{
  const int w = 256, h = 192;
  IplImage* pimg = cvCreateImage (cvSize (w,h), IPL_DEPTH_8U, 3);

  const size_t step = pimg->widthStep;
  unsigned char *p, *prow; 

  // blue
  p = (unsigned char*)pimg->imageData;
  prow = p;
  for (int i = 0; i < h/3; ++i) {
    for (int j = 0; j < w; ++j) {
      *p = 255; *(p+1) = 0; *(p+2) = 0;
      p += 3;
    }
    prow += step; p = prow;
  }

  // green
  p = (unsigned char*)pimg->imageData;
  p += (h/3)*step;
  prow = p;
  for (int i = 0; i < h/3; ++i) {
    for (int j = 0; j < w; ++j) {
      *p = 0; *(p+1) = 255; *(p+2) = 0;
      p += 3;
    }
    prow += step; p = prow;
  }

  // red
  p = (unsigned char*)pimg->imageData;
  p += (2*h/3)*step;
  prow = p;
  for (int i = 0; i < h/3; ++i) {
    for (int j = 0; j < w; ++j) {
      *p = 0; *(p+1) = 0; *(p+2) = 255;
      p += 3;
    }
    prow += step; p = prow;
  }
  return pimg;
}

mxArray * get_mat_array (IplImage* pimg)
{
  fprintf(stderr,"Height : %d, Width : %d , Depth : %d\n", pimg->width, 
	  pimg->height, pimg->depth);

  mwSize dims[3];
  dims[0] = pimg->height;
  dims[1] = pimg->width;
  dims[2] = 3;
  static mxArray *mat_pts = NULL;
  if(mat_pts == NULL){
    mat_pts = mxCreateNumericArray(3,dims, mxUINT8_CLASS , mxREAL);
  }
  char *pdata=NULL;
  pdata = (char *) mxGetData(mat_pts);
  int l = 0;
  for (int k = 0;k< dims[2]; k++){
    l = 0;
    for (int i = 0;i< pimg->height; i++){
      for (int j = 0;j< pimg->width; j++){	
	pdata[k * pimg->height * pimg->width + j*pimg->height + i] = pimg->imageData[3*l+(2-k)];
	//2-k is there because RGB are fliped in opencv
	l++;
      }   
    }
  }  
  return mat_pts;
}


//call this periodically - on the latest image
void tracker_image_handler(const bot_core_image_t *msg, Engine * ep)
{
  static int count = 0;
  count++;
  if(count > 20){
    count = 0;
  }
  if(count !=0){
    //fprintf(stderr,"Skipping\n");
    return;
  }
  else{
    fprintf(stderr,"Processing\n");
    int64_t start_time = bot_timestamp_now();
    static CvMat *frame = NULL;

    CvMat tmpIm = get_opencv_header_for_bot_core_image_t(msg);

    allocateOrResizeCloneMat(&frame,&tmpIm);
    cvCopy(&tmpIm,frame);

    //currentFrameTime = bot_timestamp_to_double(msg->utime);

    cvConvertImage(frame, frame, CV_CVTIMG_SWAP_RB);
    static IplImage tmpHeader;
    IplImage * currentFrame;

    currentFrame = cvGetImage(frame,&tmpHeader);

    // Make sure image is divisible by 2
    assert( currentFrame->width%2 == 0 && currentFrame->height%2 == 0);

    // Create an image for the output
    static IplImage* out = NULL;
    if(out == NULL){
      out = cvCreateImage( cvSize(currentFrame->width/2,currentFrame->height/2), currentFrame->depth, currentFrame->nChannels);
    }
    // Reduce the image by 2
    cvPyrDown( currentFrame, out );

    mxArray *mat_pts = get_mat_array(out);
    fprintf(stderr,"Done 1\n");
    engPutVariable (ep, "image", mat_pts);
    engEvalString(ep, "imshow(image)");
    fprintf(stderr,"Sent to matlab\n");
    engEvalString(ep, "addpath('/home/sachih/software/envoy/software/object-detector/src')");
    engEvalString(ep, "check_for_person_mat(image, model,csc_model);");
    //    engEvalString(ep, "check_for_person(image, model,csc_model);");
    int64_t end_time = bot_timestamp_now();
    
    fprintf(stderr,"Time : %f" , ((double) (end_time - start_time)) / 1e6);

    //not sure - this following free causes a crash 
    //cvReleaseImage( &currentFrame );
    
    //cvReleaseImage( &out);

    //might need to do some more freeing 
    fprintf(stderr,"Handled \n");
  }
}


void test_image_handler(Engine * ep)
{
  /*static CvMat *frame = NULL;
  
  CvMat tmpIm = get_opencv_header_for_carmen3d_image_t(msg);

  allocateOrResizeCloneMat(&frame,&tmpIm);
  cvCopy(&tmpIm,frame);

  //currentFrameTime = bot_timestamp_to_double(msg->utime);

  cvConvertImage(frame, frame, CV_CVTIMG_SWAP_RB);
  static IplImage tmpHeader;
  IplImage * currentFrame;
  currentFrame = cvGetImage(frame,&tmpHeader);
  //cv_display(cv_img, dim, depth, width_step);
  //this is a working display
  //cvNamedWindow( "Image view", 1 );
  //cvShowImage( "Image view", currentFrame );
  //cvWaitKey(0); // very important, contains event processing loop inside
  //cvDestroyWindow( "Image view" );
  
  //cvSaveImage("curr.jpg" , currentFrame);
  //engEvalString(ep, "check_for_object('curr.jpg', model);");
  //engEvalString(ep, "cascade_person_test;");*/
  engEvalString(ep, "cascade_person_test;");
  //cvReleaseImage( &currentFrame );
  fprintf(stderr,"Handled \n");
  //figure out how to convert this openCV image to matlab matrix (simmilar to the one in imread
  
}

void run_image(Engine *ep)
{
  //engEvalString(ep, "cd ;");
  //engEvalString(ep, "addpath star-cascade");
  //getting an issue with the path 
  engEvalString(ep, "cascade_person_test;");
  
  /*mxArray *T = NULL, *result = NULL;
  char buffer[BUFSIZE+1];
  double time[10] = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0 };
  
  T = mxCreateDoubleMatrix(1, 10, mxREAL);
  memcpy((void *)mxGetPr(T), (void *)time, sizeof(time));
  
  engPutVariable(ep, "T", T);

  
  engEvalString(ep, "D = .5.*(-9.8).*T.^2;");

  
  engEvalString(ep, "plot(T,D);");
  engEvalString(ep, "title('Position vs. Time for a falling object');");
  engEvalString(ep, "xlabel('Time (seconds)');");
  engEvalString(ep, "ylabel('Position (meters)');");

  sleep(1);
  printf("Done for Part I.\n");
  mxDestroyArray(T);
  engEvalString(ep, "close;");
  mxDestroyArray(result);*/
}


static void lcm_image_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), 
			      const bot_core_image_t * msg,
			      void * user)
{

  Engine * ep = (Engine *) user;
  fprintf(stderr,"Image\n");
  //decompress jpeg
  static bot_core_image_t * localFrame=(bot_core_image_t *) calloc(1,sizeof(bot_core_image_t));
  const bot_core_image_t * newFrame=NULL;
  if (msg->pixelformat==BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG){
    //create space for decompressed image
    envoy_decompress_bot_core_image_t(msg,localFrame);
    //copy pointer for working locally
    newFrame = localFrame;
  }
  else{
    //uncompressed, just copy pointer
    newFrame = msg;
  }  
  tracker_image_handler(newFrame, ep);
}

int main()
{
	Engine *ep;

	fprintf(stderr,"Creating Reference ");
	
	lcm_t * lcm;

	lcm = lcm_create(NULL);
	if(!lcm)
	  return 1;

	if (!(ep = engOpen("\0"))) {
	  fprintf(stderr, "\nCan't start MATLAB engine\n");
	  return EXIT_FAILURE;
	}
    

	engEvalString(ep,"load('inriaperson_final');");
	engEvalString(ep,"thresh = -0.5;");
	engEvalString(ep,"pca = 5;");
	engEvalString(ep,"csc_model = cascade_model(model, '2007', pca, thresh);");

	/*exlcm_example_t_subscription_t * sub =
        exlcm_example_t_subscribe(lcm, "EXAMPLE", &my_handler, ep);

	carmen3d_floor_change_msg_t_subscribe(lcm, "FLOOR_STAUS", &floor_change_handler, ep);
        */

	fprintf(stderr,"Subscribing to image\n");
	bot_core_image_t_subscribe(lcm, "CAMLCM_IMAGE", &lcm_image_handler, ep);
	
	while(1)
	  lcm_handle(lcm);

	lcm_destroy(lcm);
	return 0;

	/*
	 * Start the MATLAB engine locally by executing the string
	 * "matlab"
	 *
	 * To start the session on a remote host, use the name of
	 * the host as the string rather than \0
	 *
	 * For more complicated cases, use any string with whitespace,
	 * and that string will be executed literally to start MATLAB
	 */
	

	/*
	 * PART I
	 *
	 * For the first half of this demonstration, we will send data
	 * to MATLAB, analyze the data, and plot the result.
	 */

	


	/*
	 * PART II
	 *
	 * For the second half of this demonstration, we will request
	 * a MATLAB string, which should define a variable X.  MATLAB
	 * will evaluate the string and create the variable.  We
	 * will then recover the variable, and determine its type.
	 */
	  
	/*
	 * Use engOutputBuffer to capture MATLAB output, so we can
	 * echo it back.  Ensure first that the buffer is always NULL
	 * terminated.
	 */

	/*
	 * We're done! Free memory, close MATLAB engine and exit.
	 */
	printf("Done!\n");
	
	engClose(ep);
	
	return EXIT_SUCCESS;
}







