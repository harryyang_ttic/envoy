/* $Revision: 1.8.4.3 $ */
/*
 *	engdemo.c
 *
 *	This is a simple program that illustrates how to call the MATLAB
 *	Engine functions from a C program.
 *
 * Copyright 1984-2003 The MathWorks, Inc.
 * All rights reserved
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "engine.h"
#include <lcm/lcm.h>
#include "exlcm_example_t.h"
#include <bot/bot_core.h>
#include <carmen3d/lcm_utils.h>
#include <carmen3d/agile-globals.h>
#include <carmen3d/carmen3d_common.h>

#include <carmen3d/carmen3d_opencv_utils.hpp>
#include <carmen3d/carmen3d_common.h>
#include <carmen3d/lcm_utils.h>
#include <carmen3d/carmen3d_jpeg_codec.h>

#define  BUFSIZE 256
//refere
//http://www.mathworks.com/help/techdoc/apiref/bqoqnz0.html




void call_matlab(Engine *ep)
{
  mxArray *T = NULL, *result = NULL;
  char buffer[BUFSIZE+1];
  double time[10] = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0 };
  /* 
   * Create a variable for our data
   */
  T = mxCreateDoubleMatrix(1, 10, mxREAL);
  memcpy((void *)mxGetPr(T), (void *)time, sizeof(time));
  /*
   * Place the variable T into the MATLAB workspace
   */
  engPutVariable(ep, "T", T);

  /*
   * Evaluate a function of time, distance = (1/2)g.*t.^2
   * (g is the acceleration due to gravity)
   */
  engEvalString(ep, "D = .5.*(-9.8).*T.^2;");

  /*
   * Plot the result
   */
  engEvalString(ep, "plot(T,D);");
  engEvalString(ep, "title('Position vs. Time for a falling object');");
  engEvalString(ep, "xlabel('Time (seconds)');");
  engEvalString(ep, "ylabel('Position (meters)');");

  sleep(1);
  printf("Done for Part I.\n");
  mxDestroyArray(T);
  engEvalString(ep, "close;");
  mxDestroyArray(result);
}

static void lcm_image_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const carmen3d_image_t * msg,
    void * user  __attribute__((unused)))
{
  //decompress jpeg
  static carmen3d_image_t * localFrame=(carmen3d_image_t *) calloc(1,sizeof(carmen3d_image_t));
  const carmen3d_image_t * newFrame=NULL;
  if (msg->pixelformat==CARMEN3D_IMAGE_T_PIXEL_FORMAT_MJPEG){
    //create space for decompressed image
    carmen3d_decompress_carmen3d_image_t(msg,localFrame);
    //copy pointer for working locally
    newFrame = localFrame;
  }
  else{
    //uncompressed, just copy pointer
    newFrame = msg;
  }

  fprintf(stderr,"Image\n");
  //tracker_image_handler(newFrame);
}

void run_image(Engine *ep)
{
  //engEvalString(ep, "cd ;");
  //engEvalString(ep, "addpath star-cascade");
  //getting an issue with the path 
  engEvalString(ep, "cascade_person_test;");
  
  /*mxArray *T = NULL, *result = NULL;
  char buffer[BUFSIZE+1];
  double time[10] = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0 };
  
  T = mxCreateDoubleMatrix(1, 10, mxREAL);
  memcpy((void *)mxGetPr(T), (void *)time, sizeof(time));
  
  engPutVariable(ep, "T", T);

  
  engEvalString(ep, "D = .5.*(-9.8).*T.^2;");

  
  engEvalString(ep, "plot(T,D);");
  engEvalString(ep, "title('Position vs. Time for a falling object');");
  engEvalString(ep, "xlabel('Time (seconds)');");
  engEvalString(ep, "ylabel('Position (meters)');");

  sleep(1);
  printf("Done for Part I.\n");
  mxDestroyArray(T);
  engEvalString(ep, "close;");
  mxDestroyArray(result);*/
}

static void floor_change_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), 
			const carmen3d_floor_change_msg_t * msg,
			  void * user)
{
  fprintf(stderr,"Calling Matlab\n"); //need to fix the map loader to create floor map
  Engine * ep = (Engine *) user;
  call_matlab(ep);
}


static void
my_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
        const exlcm_example_t * msg, void * user)
{
    int i;
    printf("Received message on channel \"%s\":\n", channel);
    Engine * ep = (Engine *) user;
    run_image(ep);
}

int main()

{
	Engine *ep;

	fprintf(stderr,"Creating Reference ");
	
	lcm_t * lcm;

	lcm = lcm_create(NULL);
	if(!lcm)
	  return 1;

	if (!(ep = engOpen("\0"))) {
	  fprintf(stderr, "\nCan't start MATLAB engine\n");
	  return EXIT_FAILURE;
	}
    

	exlcm_example_t_subscription_t * sub =
        exlcm_example_t_subscribe(lcm, "EXAMPLE", &my_handler, ep);

	carmen3d_floor_change_msg_t_subscribe(lcm, "FLOOR_STAUS", &floor_change_handler, ep);

	carmen3d_image_t_subscribe(lcm, "CAMLCM_IMG", &lcm_image_handler, NULL);
	
	while(1)
	  lcm_handle(lcm);

	exlcm_example_t_unsubscribe(lcm, sub);
	lcm_destroy(lcm);
	return 0;

	/*
	 * Start the MATLAB engine locally by executing the string
	 * "matlab"
	 *
	 * To start the session on a remote host, use the name of
	 * the host as the string rather than \0
	 *
	 * For more complicated cases, use any string with whitespace,
	 * and that string will be executed literally to start MATLAB
	 */
	

	/*
	 * PART I
	 *
	 * For the first half of this demonstration, we will send data
	 * to MATLAB, analyze the data, and plot the result.
	 */

	


	/*
	 * PART II
	 *
	 * For the second half of this demonstration, we will request
	 * a MATLAB string, which should define a variable X.  MATLAB
	 * will evaluate the string and create the variable.  We
	 * will then recover the variable, and determine its type.
	 */
	  
	/*
	 * Use engOutputBuffer to capture MATLAB output, so we can
	 * echo it back.  Ensure first that the buffer is always NULL
	 * terminated.
	 */

	/*
	 * We're done! Free memory, close MATLAB engine and exit.
	 */
	printf("Done!\n");
	
	engClose(ep);
	
	return EXIT_SUCCESS;
}







