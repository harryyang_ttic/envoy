/* $Revision: 1.8.4.3 $ */
/*
 *	engdemo.c
 *
 *	This is a simple program that illustrates how to call the MATLAB
 *	Engine functions from a C program.
 *
 * Copyright 1984-2003 The MathWorks, Inc.
 * All rights reserved
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "engine.h"
#include <lcm/lcm.h>
#include "exlcm_example_t.h"

#define  BUFSIZE 256

static void
my_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
        const exlcm_example_t * msg, void * user)
{
    int i;
    printf("Received message on channel \"%s\":\n", channel);
    printf("  position    = (%f, %f, %f)\n",
            msg->position[0], msg->position[1], msg->position[2]);
    printf("  orientation = (%f, %f, %f, %f)\n",
            msg->orientation[0], msg->orientation[1], msg->orientation[2],
            msg->orientation[3]);
    printf("  ranges:");
    for(i = 0; i < msg->num_ranges; i++)
        printf(" %d", msg->ranges[i]);
    printf("\n");
    

    mxArray *T = NULL, *result = NULL;
    char buffer[BUFSIZE+1];
    double time[10] = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0 };


    
    Engine * ep = (Engine *) user;
    /* 
     * Create a variable for our data
     */
    T = mxCreateDoubleMatrix(1, 10, mxREAL);
    memcpy((void *)mxGetPr(T), (void *)time, sizeof(time));
    /*
     * Place the variable T into the MATLAB workspace
     */
    engPutVariable(ep, "T", T);

    /*
     * Evaluate a function of time, distance = (1/2)g.*t.^2
     * (g is the acceleration due to gravity)
     */
    engEvalString(ep, "D = .5.*(-9.8).*T.^2;");

    /*
     * Plot the result
     */
    engEvalString(ep, "plot(T,D);");
    engEvalString(ep, "title('Position vs. Time for a falling object');");
    engEvalString(ep, "xlabel('Time (seconds)');");
    engEvalString(ep, "ylabel('Position (meters)');");

    /*
     * use fgetc() to make sure that we pause long enough to be
     * able to see the plot
     */
    //printf("Hit return to continue\n\n");
    //fgetc(stdin);
    sleep(1);
    /*
     * We're done for Part I! Free memory, close MATLAB figure.
     */
    printf("Done for Part I.\n");
    mxDestroyArray(T);
    engEvalString(ep, "close;");
    mxDestroyArray(result);
}

int main()

{
	Engine *ep;

	fprintf(stderr,"Creating Reference ");
	
	lcm_t * lcm;

	lcm = lcm_create(NULL);
	if(!lcm)
	  return 1;

	if (!(ep = engOpen("\0"))) {
	  fprintf(stderr, "\nCan't start MATLAB engine\n");
	  return EXIT_FAILURE;
	}
    

	exlcm_example_t_subscription_t * sub =
        exlcm_example_t_subscribe(lcm, "EXAMPLE", &my_handler, ep);
	
	while(1)
	  lcm_handle(lcm);

	exlcm_example_t_unsubscribe(lcm, sub);
	lcm_destroy(lcm);
	return 0;

	/*
	 * Start the MATLAB engine locally by executing the string
	 * "matlab"
	 *
	 * To start the session on a remote host, use the name of
	 * the host as the string rather than \0
	 *
	 * For more complicated cases, use any string with whitespace,
	 * and that string will be executed literally to start MATLAB
	 */
	

	/*
	 * PART I
	 *
	 * For the first half of this demonstration, we will send data
	 * to MATLAB, analyze the data, and plot the result.
	 */

	


	/*
	 * PART II
	 *
	 * For the second half of this demonstration, we will request
	 * a MATLAB string, which should define a variable X.  MATLAB
	 * will evaluate the string and create the variable.  We
	 * will then recover the variable, and determine its type.
	 */
	  
	/*
	 * Use engOutputBuffer to capture MATLAB output, so we can
	 * echo it back.  Ensure first that the buffer is always NULL
	 * terminated.
	 */

	/*
	 * We're done! Free memory, close MATLAB engine and exit.
	 */
	printf("Done!\n");
	
	engClose(ep);
	
	return EXIT_SUCCESS;
}







