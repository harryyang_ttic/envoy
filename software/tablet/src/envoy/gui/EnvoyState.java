package envoy.gui;

import java.io.Serializable;

public class EnvoyState implements Serializable { 
    public String currentState; 
    public String previousState;
    public String speechText;
    public String action;
    public int currentFloor;
    public boolean cloudConnect;
    public short personState; //this might need to be some enum or something
    public String finalGoal;
    public String currentGoal;
    public long utime;
    
    public EnvoyState(){
    	utime = 0;
    	currentState = "Init";
    	previousState = "Init";
    	speechText = "blah blah blah";
    	action = "Nothing";
    	currentFloor = 0;
    	cloudConnect = true;
    	finalGoal = "Sachi's office";
    	currentGoal = "Elevator";
    	personState = 0;
    }
}