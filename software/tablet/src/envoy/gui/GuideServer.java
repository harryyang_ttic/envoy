package envoy.gui;

import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.io.ObjectOutputStream;

import lcm.lcm.*;
import bot_core.image_t;
import erlcm.*;
import java.awt.image.*;
import javax.imageio.*;

import kinect.frame_msg_t;
import kinect.image_msg_t;

public class GuideServer {
	
    LCM lcm = null;	
    static byte[] buffer = null;
    static EnvoyState ob = new EnvoyState();

    static long last_received_utime = 0;
    static private Object lock = new Object();
    static private Object statelock = new Object();
    static String deviceType = "kinect";
    static LCMImageBuffer im_buffer = null;
    
    public class ImageSubscriber extends Thread implements LCMSubscriber{
        LCM lcm; 

        public ImageSubscriber(LCM _lc){
            lcm = _lc;
        }
        
        public ImageSubscriber(){
            try {
                lcm = new LCM();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        BufferedImage handleRAW(image_t v)
        {
            BufferedImage bi = new BufferedImage(v.width, v.height, 
                                                 BufferedImage.TYPE_INT_RGB);
            for (int y = 0; y < v.height; y++) {
                for (int x = 0; x < v.width; x++) {
                    int g = v.data[x+y*v.row_stride] & 0xff;
                    int rgb = (g<<16)|(g<<8)|g;
                    bi.setRGB(x, y, rgb);
                }
            }
            return bi;
        }

        BufferedImage handleRGB(image_t v)
        {
            BufferedImage bi = new BufferedImage(v.width, v.height, 
                                                 BufferedImage.TYPE_INT_RGB);
            for (int y = 0; y < v.height; y++) {
                for (int x = 0; x < v.width; x++) {
                    int r = v.data[y*v.row_stride + x*3 + 0] & 0xff;
                    int g = v.data[y*v.row_stride + x*3 + 1] & 0xff;
                    int b = v.data[y*v.row_stride + x*3 + 2] & 0xff;
                    int rgb = (r<<16)|(g<<8)|b;
                    bi.setRGB(x, y, rgb);
                }
            }
            return bi;
        }

        BufferedImage handleJPEG(image_t v)
        {
            try {
                return ImageIO.read(new ByteArrayInputStream(v.data));
            } catch (IOException ex) {
                return null;
            }
        }
        
		
        public void handleState(erlcm.bot_state_t state) throws IOException, InterruptedException
        {
            //update the status object 
            synchronized ( statelock) {
                ob.utime = state.utime;
                ob.action = state.action;
                if(state.connected_to_cloud >0)
                    ob.cloudConnect = true;
                else
                    ob.cloudConnect = false;
                ob.currentFloor = state.current_floor;
                ob.currentGoal = state.current_goal;
                ob.currentState = state.new_state;
                ob.previousState = state.prev_state;
                ob.speechText = state.speech_response;				
                ob.finalGoal = state.final_goal;
                ob.personState = state.have_person;
                statelock.notifyAll();
            }
        }
		
        public void handleKinectClass(frame_msg_t v) throws IOException, InterruptedException
        {
        	
            synchronized (deviceType) {
            	if(!deviceType.equalsIgnoreCase("kinect")){
                    return;
                }
            }
        	
            if (v.image.width==0 || v.image.height==0)
                return;

            BufferedImage bi = null;

            switch (v.image.image_data_format) 
                {
                case image_msg_t.VIDEO_RGB_JPEG:
                    // = handleJPEG(v);
                    ByteArrayInputStream is = new ByteArrayInputStream(v.image.image_data); 
                    
                    //this might not be needed 
                    synchronized ( lock ) {
                        im_buffer = new LCMImageBuffer();
                        im_buffer.image_utime = v.image.timestamp;
                        im_buffer.buffer = new byte[is.available()];
                        //buffer = new byte[is.available()];
                        //this is working 
                        is.read(im_buffer.buffer);//is.read(buffer); 
                        lock.notifyAll();
                    }
                    //System.out.println("Buffer Size : " + buffer.length);
                    break;
                case image_msg_t.VIDEO_RGB:
                    //System.out.println("RGB");
                    break;
                }

        }

        public void handleImageClass(image_t v, String channel) throws IOException, InterruptedException
        {
            if (v.width==0 || v.height==0)
                return;
            
            synchronized (deviceType) {
            	if(!deviceType.equalsIgnoreCase("RGB")){
                    return;
                }
            }
            
            
            BufferedImage bi = null;
            

            switch (v.pixelformat) 
                {
                case image_t.PIXEL_FORMAT_MJPEG:
                    // = handleJPEG(v);
                    ByteArrayInputStream is = new ByteArrayInputStream(v.data); 
                    
                    //this might not be needed 
                    synchronized ( lock ) {
                        im_buffer = new LCMImageBuffer();
                        im_buffer.image_utime = v.utime;
                        im_buffer.buffer = new byte[is.available()];
                        //this is working 
                        is.read(im_buffer.buffer); 
                        lock.notifyAll();
                    }
                    break;
                    
                    //other formats not handled yet
                    /*case image_t.PIXEL_FORMAT_GRAY:
                      case image_t.PIXEL_FORMAT_BAYER_BGGR:
                      case image_t.PIXEL_FORMAT_BAYER_GBRG:
                      case image_t.PIXEL_FORMAT_BAYER_GRBG:
                      case image_t.PIXEL_FORMAT_BAYER_RGGB:
                      bi = //handleRAW(v);
                      break;
                      case image_t.PIXEL_FORMAT_RGB:
                      bi = handleRGB(v);
                      break;	*/
                }

            /*(bi != null)
              ji.setImage(bi);*/
        }

        public void messageReceived(LCM lcm, String channel, LCMDataInputStream ins)
        {
        	System.out.printf("+");
            try {
            	if(channel.equals("CAMLCM_IMAGE")){
                    image_t v = new image_t(ins);
                    handleImageClass(v, channel);
                    last_received_utime = v.utime;
            	}
                if(channel.equals("FRNT_IMAGE_BUMBLEBEE_LEFT_VISION")){
                    System.out.printf("+");
                    image_t v = new image_t(ins);
                    handleImageClass(v, channel);
                    last_received_utime = v.utime;
            	}
                if(channel.equals("FRNT_IMAGE_BUMBLEBEE_LEFT")){
                    System.out.printf("+");
                    image_t v = new image_t(ins);
                    handleImageClass(v, channel);
                    last_received_utime = v.utime;
            	}

            	if(channel.equals("KINECT_FRAME")){
                    frame_msg_t v = new frame_msg_t(ins);
                    last_received_utime = v.image.timestamp;
                    handleKinectClass(v);
            	}
            	else if(channel.equals("ENVOY_STATUS")){
                    erlcm.bot_state_t state = new erlcm.bot_state_t(ins);
                    handleState(state);
            		
            	}
            	//we should have another channel that tells us about the processes running
            
            } catch (IOException ex) {
                System.out.println("ex: "+ex);
                return;
            }
            catch (InterruptedException ex) {
                System.out.println("ex: "+ex);
                return;
            }            
        }

        public void run() {

            try {
                lcm.subscribe("CAMLCM_IMAGE", this);
                
                lcm.subscribe("FRNT_IMAGE_BUMBLEBEE", this);

                lcm.subscribe("FRNT_IMAGE_BUMBLEBEE_VISION", this);

                lcm.subscribe("FRNT_IMAGE_BUMBLEBEE_LEFT_VISION", this);
                
                lcm.subscribe("FRNT_IMAGE_BUMBLEBEE_LEFT", this);
                
                lcm.subscribe("ENVOY_STATUS", this);
                
                lcm.subscribe("KINECT_FRAME", this);
            
                while(true){
                
                    Thread.sleep(100);
                }
            }
            
            catch (InterruptedException ex) { }
        }
        
    }   

    public class DataSender extends Thread {
        ObjectOutputStream out;
        public  GuideServer my_display;
        long last_sent_utime = -1;
        long last_state_sent_utime = -1;
        int count = 0; 
        
        public DataSender(ObjectOutputStream output, GuideServer display) {
            my_display = display; 
            out = output;
            
        }
        public void run() {
            try {
                while(true){
                    //we can try to send this from an LCM stream (or some other buffer)
                    synchronized (lock) {
                        /*if(im_buffer != null && last_sent_utime != im_buffer.image_utime){
                            System.out.println("Trying to send Img\n");
                            out.writeObject(im_buffer);
                            last_sent_utime = im_buffer.image_utime; 
                            System.out.println("Sending Img :" + im_buffer.image_utime / 1.0e6);
                        }
                        else{

                        }*/
                       
						String test = new String("bla bla"  + (count++));
                        out.writeObject(test);
                        System.out.printf("Sent data\n");
                        lock.notifyAll();
                    }

                    //out.flush();
                    
                    //Thread.sleep(5);
                    System.out.printf("Done\n");
                } 
                
                
            }
            /*catch(InterruptedException e){
                System.out.println("Exit called\n");                
                }*/
        
            catch (IOException e) {
                
                System.out.println("IO Exception - Socket Failed\n");   
            }

            finally{
                try{
                    out.close();
                }
                catch(IOException e){
                    System.out.println("Error Closing stuff\n");   	
                }
            }
            return; 
        }
    }
    
    public class DataReceiver extends Thread {
    	ObjectInputStream in;
        public  GuideServer my_display;
        LCM lcm; 
        int count; 
        
        public DataReceiver(ObjectInputStream input, GuideServer display, LCM _lcm) {
            my_display = display; 
            //System.out.println("Default size: " + client.getReceiveBufferSize());
            in = input;
            lcm = _lcm;
            count = 0;
        }
        
        long utime_now()
        {
            return System.nanoTime()/1000;
        }   
        
        public void run() {
            try {
            	Object obj = null;
            	while ((obj = in.readObject()) != null) {
                    //System.out.print("+");
                    //seems to be getting stale data

                    if(obj.getClass().equals(TaggedPointList.class)){
            			count++;
                        TaggedPointList tpList = (TaggedPointList) obj;
                        System.out.println("Received Tagged Point List : " + tpList.image_utime);
                        System.out.printf("Utime : %f Size of Point list : %d Type : %s\n", tpList.image_utime / 1.0e6, tpList.points.size(), tpList.type);
                                
                        reacquisition_segmentation_t msg = new reacquisition_segmentation_t(); 
                                
                        msg.utime = tpList.image_utime;
                                
                        msg.object_id = count;
                                
                        msg.roi_utime = tpList.image_utime;
                        msg.image_utime = tpList.image_utime;
                                
                        point2d_list_t point_list = new point2d_list_t();

                        point_list.npoints = tpList.points.size();
                        //init the array
                        point_list.points = new point2d_t[point_list.npoints];
                        System.out.printf("Size of allocated array : %d\n", point_list.points.length);
                                
                        //these points dont seem to get updated **** 
                        for(int i=0; i < point_list.npoints; i++){
                            System.out.printf("Point : %f,%f\n", tpList.points.get(i).x, tpList.points.get(i).y);
                        }

                        //we need to publish a object message here that creates the object 

                        //init the elements in the array
                        for(int i=0; i < point_list.npoints; i++){
                            point_list.points[i] = new point2d_t();
                            point_list.points[i].x = tpList.points.get(i).x;
                            point_list.points[i].y = tpList.points.get(i).y;
                        }

                        msg.camera = "bumblebee_left";
                        msg.roi = point_list;

                        reacquisition_segmentation_list_t mlist = new reacquisition_segmentation_list_t();
                        mlist.utime = msg.roi_utime;
                        mlist.num_segs = 1;
                        mlist.segmentations = new reacquisition_segmentation_t[1];
                        mlist.segmentations[0] = msg;
                        lcm.publish( "TABLET_SEGMENTATION", mlist);

                        object_list_t olist = new object_list_t();
                        olist.utime = msg.roi_utime;
                        olist.num_objects = 1;
                        olist.objects = new object_t[1];
                        
                        object_t tagged_obj = new object_t();
                        tagged_obj.utime = msg.roi_utime;
                        tagged_obj.id = count; 
                        //assumed a 1-to-1 mapping 
                        tagged_obj.object_type = tpList.type;
                        tagged_obj.label = tpList.label;
                        olist.objects[0] = tagged_obj; 
                        lcm.publish( "OBJECTS_UPDATE_ADD", olist);

                    }
            		
                    if(obj.getClass().equals(String.class)){
                        String rec = (String) obj;
                        if(rec.startsWith("Direction:")){
                            String dir = rec.substring(10);
                            //publish in LCM 
                            System.out.println("Recieved : " + dir);
                            erlcm.servo_position_list_t msg = new erlcm.servo_position_list_t();
                            msg.utime = utime_now();
                            msg.dof = 2;
                            msg.values = new double[2];
                            //String dir = (String) obj;
                            if(dir.equals("LEFT")){
                                msg.values[0] = 5.0;
                            }
                            else if(dir.equals("RIGHT")){
                                msg.values[0] = -5.0;
                            }
                            else if(dir.equals("UP")){
                                msg.values[1] = 2.0;
                            }
                            else if(dir.equals("DOWN")){
                                msg.values[1] = -2.0;
                            }
                            msg.command_type = 0x0011;
	            			
                            lcm.publish( "HEAD_POSITION_ADJUSTMENT", msg);
                        }
                        else if(rec.startsWith("Device:")){
                            synchronized (deviceType) {
                                deviceType = rec.substring(7);
                                System.out.println("Setting new Device : " + deviceType);
                            }
            				
                        }
                    }
                }               
                
            }
            catch (IOException e) {
                System.out.println("Socket Failed\n");   
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            finally{
                try{
                    in.close();
                }
                catch(IOException e){
                    System.out.println("Error Closing stuff\n");   	
                }
            }
            return; 
        }
    }
     

    public GuideServer () {
    	try {
            lcm = new LCM();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    protected void addClient(Socket client, int type) throws IOException {
    	if(type == 1){
    		
            DataSender ds = new DataSender(new ObjectOutputStream(client.getOutputStream()), this);
            //in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            ds.start();
            
            //DataReceiver dr = new DataReceiver(new ObjectInputStream(client.getInputStream()), this, lcm);
            //dr.start();
    	}    
    }

    protected void createListener(){
        ImageSubscriber im_sub = new ImageSubscriber(lcm);
        im_sub.start();
    }

    public static void main(String [] argv) {
        System.err.printf("Started\n");
        ServerSocket serverSocket = null;            

        try {
            serverSocket = new ServerSocket(4444);
            serverSocket.setReuseAddress(true);      
            if (!serverSocket.isBound()){

                System.out.println("Server already running\n");
                System.exit(-1);
            }

            
            try{
            	FileInputStream fis = new FileInputStream("world.jpg");
            	buffer = new byte[fis.available()];
            	fis.read(buffer);
            	fis.close();
                System.out.printf("Stock Image found\n");
            }
            catch(IOException e){
            	System.out.println("No Image found - waiting for LCM stream");
            	
            }
         
            GuideServer d = new GuideServer();
            
            d.createListener();

            do {
                Socket client = serverSocket.accept();
                d.addClient(client, 1);
                System.out.println("Adding New Client\n");
            } while (true);
        } 
        catch (IOException e) {
            System.err.println("I couldn't create a new socket.\n"+
                               "You probably are already running DisplayServer.\n");
            System.err.println(e);
            System.exit(-1);
        }
    }    
}
