package envoy.gui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TaggedPointList implements Serializable{
	
    List<TaggedPoint> points;
    //String type = null;
    short type;
    String label = null;
    long image_utime = 0;
    
    public static final short UNKNOWN = (short) 0;
    public static final short TABLE = (short) 1;
    public static final short CHAIR = (short) 2;
    public static final short TRASHCAN = (short) 3;
    public static final short BED = (short) 4;
    public static final short FRIDGE = (short) 5;
    public static final short MICROWAVE = (short) 6;
    public static final short TV = (short) 7;
    public static final short ELEVATOR_DOOR = (short) 8;
    public static final short LAPTOP = (short) 9;
    public static final short WATER_FOUNTAIN = (short) 10;

    public TaggedPointList() {
        // TODO Auto-generated constructor stub
        points = new ArrayList<TaggedPoint>();
        type = UNKNOWN;	
        label = "None";
    }
    //List<Point> tempPoints = new ArrayList<Point>();   

    //add tagged utime, image utime, msg utime, camera channel 
    public TaggedPointList(TaggedPointList tp) {
        // TODO Auto-generated constructor stub
        points = new ArrayList<TaggedPoint>();
        for(int i=0; i < tp.points.size(); i++){
            TaggedPoint t = new TaggedPoint(tp.points.get(i).x, tp.points.get(i).y, tp.points.get(i).time);
            points.add(t);
        }
        image_utime = tp.image_utime;
        type = tp.type;	 
        label = tp.label;
    }
}


