package envoy.gui;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicBoolean;

import envoy.gui.ClickDirection.DIR;

import android.util.Log;

public class SocketSender extends Thread{
	
	Socket socket = null;
	ObjectOutputStream out = null;
	static protected Object lock = new Object();
	AtomicBoolean started = new AtomicBoolean(true);//(false);
	protected long utime = 0;
	SocketCreator creator;
	
	static final long utime_now()
	{
		return System.nanoTime()/1000;
	}
	
	public SocketSender(Socket sk, SocketCreator creator) {
		socket = sk;
		this.creator = creator;
		setDaemon(true);
	}
		
	public void run()
	{
		try {
			Log.e("Error", "Sender Called");
			out = new ObjectOutputStream(socket.getOutputStream());
            
        } catch (UnknownHostException e) {                       
            Log.e("Err","Error - Unknown Host");
        } catch (IOException e) {
            Log.e("Err","Error - IO");
        }           
		
		boolean run = true;
        long last_utime = 0;
    	while(run){
    		
    		synchronized (SocketCreator.socketLock) {
				if(creator.myApp.started.get() == false){
					run = false;
				}
				SocketCreator.socketLock.notifyAll();
			}
    		//String st = "Return : " + utime_now();
    		//Log.e("Error", "Sending");
    		try {
				//out.writeObject(st);
				
				/*if(DrawView.dir!=null &&
						last_utime < DrawView.dir.utime){
					String dir = "NONE";
					if(DrawView.dir.direction == DIR.LEFT){
						dir = "LEFT";
					}
					else if(DrawView.dir.direction == DIR.RIGHT){
						dir = "RIGHT";
					}
					else if(DrawView.dir.direction == DIR.UP){
						dir = "UP";
					}
					else if(DrawView.dir.direction == DIR.DOWN){
						dir = "DOWN";
					}
					last_utime = DrawView.dir.utime;
					if(!dir.equalsIgnoreCase("NONE"))
						out.writeObject(dir);
				}*/
    			if(creator.myApp.deviceUpdated.get()){
    				creator.myApp.deviceUpdated.set(false);
    				String dir = "Device:" + creator.myApp.imageSource;
    				out.writeObject(dir);
    			}
    			
    			if(creator.myApp.dir!=null &&
						last_utime < creator.myApp.dir.utime && !creator.myApp.dir.sent){
					String dir = "Direction:NONE";
					creator.myApp.dir.sent = true;
					if(creator.myApp.dir.direction == DIR.LEFT){
						dir = "Direction:LEFT";
					}
					else if(creator.myApp.dir.direction == DIR.RIGHT){
						dir = "Direction:RIGHT";
					}
					else if(creator.myApp.dir.direction == DIR.UP){
						dir = "Direction:UP";
					}
					else if(creator.myApp.dir.direction == DIR.DOWN){
						dir = "Direction:DOWN";
					}
					last_utime = creator.myApp.dir.utime;
					if(!dir.equalsIgnoreCase("NONE"))
						out.writeObject(dir);
				}    			
				
				if(creator.myApp.valid_data.get()){
					out.writeObject(creator.myApp.pList);
					creator.myApp.valid_data.set(false);
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.e("Error", "Writing Error");
				e.printStackTrace();
				break;
			}
    		
    		try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				break;
			}
    	}
    	
    	try {
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }      	
}
