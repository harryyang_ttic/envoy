package envoy.gui;

import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.http.ConnectionClosedException;

import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

public class SocketListener extends Thread{
	protected boolean new_image = false;
	Socket socket = null;
	ObjectInputStream in = null;
	protected boolean save_file = false;//new AtomicBoolean(false);     
	static protected Object lock = new Object();
	protected byte[] buffer = null;
	protected EnvoyState envoyState = null;
	
	boolean mExternalStorageAvailable = false;
	boolean mExternalStorageWriteable = false;
	protected long utime;
	SocketCreator creator;
	
	static final long utime_now()
	{
		return System.nanoTime()/1000;
	}
	
	public SocketListener(Socket sk, SocketCreator creator) {
		this.creator = creator;
		socket = sk;
		setDaemon(true);
	}
		
	public void run()
	{
		try {
			in = new ObjectInputStream(socket.getInputStream());
            //serverQuerry = "Connected";
            /*if(context != null){
            	Toast.makeText(context, "Connected to Server " , Toast.LENGTH_LONG).show();
            }*/
            creator.connected = true;
            //Log.d("Sucess","Connected");
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: taranis.");
            creator.serverQuerry = "Error - Can't Find Host";
                
            //Log.e("Err","Error - Unknown Host");
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to: taranis.");
            creator.serverQuerry = "Error - IO Error";            
            //Log.e("Err","Error - IO");
        }            	
        
        //we expect to get a stream of data 
        try {
        	boolean run = true;
        	
        	while(run){            	
        		//the locking stuff has been commented out - should be put back at some point 
        		synchronized (creator.socketLock) {
    				if(creator.myApp.started.get() == false){
    					run = false;
    				}
    				creator.socketLock.notifyAll();
				}
        		
        		Object ob = in.readObject();
        		
        		byte[] b = new byte[10];
        		        	
        		if(b.getClass().equals(ob.getClass()) && !creator.myApp.stopBufferUpdate.get()){
        			buffer = (byte [])ob;
        			utime = utime_now();
        		}
        		else if(String.class.equals(ob.getClass())){
        			creator.serverQuerry = ""  + ob.getClass() + " -> " + b.getClass();
    	        }
        		else if(EnvoyState.class.equals(ob.getClass())){
        			envoyState = (EnvoyState) ob;
        		}
        		//System.gc();
        		Log.d("REC", "Received");
            	//tell the gui to update the image
            	new_image = true;
            	
            	Runtime.getRuntime().freeMemory();
            	
        	} 
		} 
        catch(java.lang.OutOfMemoryError e){
        	Runtime.getRuntime().freeMemory();
        	Log.e("err", "Out of Memeory");
        }
        catch(java.net.SocketException e){
        	Runtime.getRuntime().freeMemory();
        	Log.e("err", "Socket Error");
        }
        catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();	
			creator.serverQuerry = "Server Error : " +  e.toString();//"IO Error";
			//connected = false;
			Log.e("Err","Error - IO");
		 
		}
        
        catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			creator.serverQuerry = "Class Error";
			//connected = false;
			//Log.e("Err","Error - Class Error");
		}             
        catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			creator.serverQuerry = "Null Pointer Exception";
			
			//connected = false;
			//Log.e("Err","Error - Null Pointer");
		} 
        finally{
        	Log.e("Err","Resetting");
        	creator.connected = false;
        	//creator.started.set(false);
        }
        
        try {
			in.close();			
			//creator.connected = false;	            
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//connected = false;
			//Log.e("Err","Error - IO Closing Socket");
		}
        catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			creator.serverQuerry = "Null Pointer Exception";
			//connected = false;
			//Log.e("Err","Error - Null Pointer Closing Socket");
		} 
    }      	
}
