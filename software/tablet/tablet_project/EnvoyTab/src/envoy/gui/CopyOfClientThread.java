package envoy.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.io.StreamCorruptedException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicBoolean;

import envoy.gui.LCMImageView.DialogState;
import envoy.gui.LCMImageView.NavigationState;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class CopyOfClientThread //extends Thread
{
	//private boolean connected = false;

	protected boolean new_image = false;//new AtomicBoolean(false);
	protected String hostname = EnvoyApp.serverIP;//"192.168.1.11";//"sachih.csail.mit.edu";
	protected int socket = 4444;
	Socket kkSocket = null;
	
	ListenerThread listener = null;
    //PrintWriter out = null;
    //ObjectInputStream in = null;
    Context context = null;
    protected boolean save_file = false;//new AtomicBoolean(false);     
    static protected Object lock = new Object();
    protected byte[] buffer = null;
    protected EnvoyState envoyState = null;
	static protected Object socketLock = new Object();

	boolean mExternalStorageAvailable = false;
	boolean mExternalStorageWriteable = false;
	protected String serverQuerry = null;
	protected boolean connected = false;//new AtomicBoolean(false);
	AtomicBoolean started = new AtomicBoolean(true);
	protected long utime;
	
	static final long utime_now()
    {
        return System.nanoTime()/1000;
    }
	
    public CopyOfClientThread(String Host, int Socket, Context context)
    {
        //setDaemon(true);
        this.context = context;
        hostname = Host;
        socket = Socket;
        checkPermissions();
        /*try {
			kkSocket = new Socket(hostname, socket);
			serverQuerry = "Connected";
			
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			serverQuerry = "Unknown Host";
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			serverQuerry = "IO Error";
		}*/
    }
    
    public CopyOfClientThread(Context context)
    {
        //setDaemon(true);
        this.context = context;
        //checkPermissions();
        //new PrintWriter(kkSocket.getOutputStream(), true));
        //in = new ObjectInputStream(kkSocket.getInputStream());
    }
    
    public void stop(){
    	if(listener != null){
    		listener.stop();
    	}
    }
    
    public boolean isAlive(){
    	if(listener != null){
    		return listener.isAlive();
    	}
    	return false;
    }
    
    public void start()
    {
    	try {
			kkSocket = new Socket(hostname, socket);
			serverQuerry = "Connected";		
			try {    		
				listener = new ListenerThread(new ObjectInputStream(kkSocket.getInputStream()));
				listener.start();
			} catch (StreamCorruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			serverQuerry = "Unknown Host";
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			serverQuerry = "IO Error";
		}
    	/*if(kkSocket != null){
	    	try {    		
					ListenerThread listener = new ListenerThread(new ObjectInputStream(kkSocket.getInputStream()));
					listener.start();
				} catch (StreamCorruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		
    	}
    	else{
    		serverQuerry = "No Socket";
    	}*/
    	serverQuerry = "Connected";
    }
   
    
    void checkPermissions(){
    	if(save_file){
            String state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state)) {
                // We can read and write the media
                mExternalStorageAvailable = mExternalStorageWriteable = true;
            } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
                // We can only read the media
                mExternalStorageAvailable = true;
                mExternalStorageWriteable = false;
            } else {
            	// Something else is wrong. It may be one of many other states, but all we need
            	//  to know is we can neither read nor write
            	mExternalStorageAvailable = mExternalStorageWriteable = false;
            }
    	}
    }
    
    void saveToDisk(){
    	File path = Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_PICTURES);
        File file = new File(path, "world.jpg");
        FileOutputStream fos;
		try {
			fos = new FileOutputStream(file);
			fos.write(buffer);
	        fos.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  

        //to update the gallery - not needed if we are doing other stuff using the file
        MediaScannerConnection.scanFile(this.context,
        		new String[] { file.toString() }, null,
        		new MediaScannerConnection.OnScanCompletedListener() {
        	public void onScanCompleted(String path, Uri uri) {
        		//Log.i("ExternalStorage", "Scanned " + path + ":");
        		//Log.i("ExternalStorage", "-> uri=" + uri);
        	}
        });
    }
    
    public class ListenerThread extends Thread{
    	ObjectInputStream in;
    	public ListenerThread(ObjectInputStream input){
    		in = input;
    		setDaemon(true);
    	}
    	
    	public void run()
        {
    		//do this check if we are saving to disk
                    
            //we expect to get a stream of data 
            try {
            	
            	boolean run = true;
            	
            	while(run){            	
            		//the locking stuff has been commented out - should be put back at some point 
            		synchronized (socketLock) {
        				if(started.get() == false){
        					run = false;
        				}
        				socketLock.notifyAll();
    				}
            		
            		Object ob = in.readObject();
            			
            		byte[] b = new byte[10];
            			
            		if(b.getClass().equals(ob.getClass())){
            			buffer = (byte [])ob;	
            			System.gc();
            		}
            		else if(String.class.equals(ob.getClass())){
            			serverQuerry = ""  + ob.getClass() + " -> " + b.getClass();
        	           	
            			Log.d("Err", "String " + ((String) ob));
            		}
            		else if(EnvoyState.class.equals(ob.getClass())){
            			envoyState = (EnvoyState) ob;
            		}
                	//tell the gui to update the image
                	new_image = true;
                	utime = utime_now();
                	    
                	//saving to disk - and telling the gallery to update
                	if(save_file && mExternalStorageAvailable){
                		saveToDisk();
                	}
            	}
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();	
    			serverQuerry = "Server Error : " +  e.toString();//"IO Error";
    			connected = false;
    			//Log.e("Err","Error - IO");
    			
    		} catch (ClassNotFoundException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    			
    			serverQuerry = "Class Error";
    			connected = false;
    			//Log.e("Err","Error - Class Error");
    		}             
            catch (NullPointerException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    			serverQuerry = "Null Pointer Exception";
    			
    			connected = false;
    			//Log.e("Err","Error - Null Pointer");
    		} 

            
            try {
    			in.close();
                kkSocket.close();
                connected = false;	            
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    			connected = false;    			
    		}
            catch (NullPointerException e) {
            	e.printStackTrace();
    			serverQuerry = "Null Pointer Exception";
    			connected = false;    			
    		} 
        }
    }
    
    /*public void run()
    {
    	try {
    		//create socket 
    		//we might need another socket 
            kkSocket = new Socket(hostname, socket);
            out = new PrintWriter(kkSocket.getOutputStream(), true);
            in = new ObjectInputStream(kkSocket.getInputStream());
            serverQuerry = "Connected";
            
            connected = true;
            //Log.d("Sucess","Connected");
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: taranis.");
            serverQuerry = "Error - Can't Find Host";
            connected = false;    
            //Log.e("Err","Error - Unknown Host");
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to: taranis.");
            serverQuerry = "Error - IO Error";
            connected = false;
            //Log.e("Err","Error - IO");
        }            	
        	
    	//do this check if we are saving to disk
    	if(save_file){
            String state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state)) {
                // We can read and write the media
                mExternalStorageAvailable = mExternalStorageWriteable = true;
            } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
                // We can only read the media
                mExternalStorageAvailable = true;
                mExternalStorageWriteable = false;
            } else {
            	// Something else is wrong. It may be one of many other states, but all we need
            	//  to know is we can neither read nor write
            	mExternalStorageAvailable = mExternalStorageWriteable = false;
            }
    	}
        
        //we expect to get a stream of data 
        try {
        	
        	boolean run = true;
        	
        	while(run){            	
        		//the locking stuff has been commented out - should be put back at some point 
        		synchronized (socketLock) {
    				if(started.get() == false){
    					run = false;
    				}
    				socketLock.notifyAll();
				}
        		
        		
        		Object ob = in.readObject();
        			
        		byte[] b = new byte[10];
        			
        		//Log.e("Error ", "" + ob.getClass());
        		//envoyStateText = ""  + ob.getClass() + " -> " + EnvoyState.class;
        			
        		if(b.getClass().equals(ob.getClass())){
        			buffer = (byte [])ob;	
        		//call this if needed
        			System.gc();
        		//lock.notifyAll();
        		}
        		else if(String.class.equals(ob.getClass())){
        			serverQuerry = ""  + ob.getClass() + " -> " + b.getClass();
    	           	
        			Log.d("Err", "String " + ((String) ob));
        		}
        		else if(EnvoyState.class.equals(ob.getClass())){
        			//serverQuerry = ""  + ob.getClass() + " -> " + b.getClass();
        			envoyState = (EnvoyState) ob;
        		}
            	//tell the gui to update the image
            	new_image = true;
            	utime = utime_now();
            	    
            	//saving to disk - and telling the gallery to update
            	if(save_file && mExternalStorageAvailable){
            		File path = Environment.getExternalStoragePublicDirectory(
            				Environment.DIRECTORY_PICTURES);
                    File file = new File(path, "world.jpg");
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(buffer);  
                    fos.close();
	
                    //to update the gallery - not needed if we are doing other stuff using the file
                    MediaScannerConnection.scanFile(this.context,
                    		new String[] { file.toString() }, null,
                    		new MediaScannerConnection.OnScanCompletedListener() {
                    	public void onScanCompleted(String path, Uri uri) {
                    		//Log.i("ExternalStorage", "Scanned " + path + ":");
                    		//Log.i("ExternalStorage", "-> uri=" + uri);
                    	}
                    });
            	}
            	//Log.d("Rec","Received Image");
        	}            	
        	
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();	
			serverQuerry = "Server Error : " +  e.toString();//"IO Error";
			connected = false;
			//Log.e("Err","Error - IO");
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			serverQuerry = "Class Error";
			connected = false;
			//Log.e("Err","Error - Class Error");
		}             
        catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			serverQuerry = "Null Pointer Exception";
			
			connected = false;
			//Log.e("Err","Error - Null Pointer");
		} 

        
        try {
        	out.close();
			in.close();
            
            kkSocket.close();
            connected = false;	            
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			connected = false;
			//Log.e("Err","Error - IO Closing Socket");
		}
        catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			serverQuerry = "Null Pointer Exception";
			connected = false;
			//Log.e("Err","Error - Null Pointer Closing Socket");
		} 
    } */     
}

