package envoy.gui;

import java.util.List;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import envoy.gui.ClickDirection.DIR;

public class DrawView extends View implements OnTouchListener {
    private static final String TAG = "DrawView";
    
    Paint paint = new Paint();
    Bitmap bitmap = null; 
    //AtomicBoolean startedDraw = new AtomicBoolean(false); 
    boolean sent = false;
    boolean allowDrawing = false;
    boolean moveHead = false;
    Context context; 
    TaggedPoint lastPoint;
    //static TaggedPointList pList = new TaggedPointList();
    //static ClickDirection dir = new ClickDirection();
    //static AtomicBoolean valid_data = new AtomicBoolean(false);
    //Direction with time 
    EnvoyApp myApp;
    
    public DrawView(Context context, AttributeSet attr, EnvoyApp app){
    	   super(context, attr);    	   
    	   this.context = context;
    	   myApp = app;
    	   setFocusable(true);
           setFocusableInTouchMode(true);

           this.setOnTouchListener(this);

           paint.setColor(Color.WHITE);
           paint.setAntiAlias(true);
           
           paint.setStrokeWidth((float) 5.0);
           paint.setColor(Color.RED);
           paint.setStyle(Paint.Style.STROKE);
           
   	       bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.picture);
    }
    
    
    
    public DrawView(Context context, EnvoyApp app) {
        super(context);
        this.context = context;
        setFocusable(true);
        setFocusableInTouchMode(true);
        myApp = app;
        this.setOnTouchListener(this);

        paint.setAntiAlias(true);
        paint.setStrokeWidth((float) 5.0);
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
       
	    bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.picture);
    }
    
    public void setRedStroke(){
    	paint.setColor(Color.RED);
    }
    
    public void SetBitmap(Bitmap bitmap){
    	this.bitmap = bitmap;
    }
    
    public Bitmap resizeImage(){
    	int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int newWidth = 500;
        int newHeight = 500;
         
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
         
      // create a matrix for the manipulation
        Matrix matrix = new Matrix();
         // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
         // rotate the Bitmap
         //matrix.postRotate(45);
  
         // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                           width, height, matrix, true);
        
        return resizedBitmap;
    }
    
    public void cleanPoints(){
    	int size = myApp.pList.points.size();
    	int valid_index = 0;//size - 1;
    	for(int i=size -1; i > 1; i--){
    		TaggedPoint start = myApp.pList.points.get(i-1);
    		TaggedPoint end = myApp.pList.points.get(i);
    		if(Math.abs(start.time - end.time) > 0.2e3){
    			//maybe resize the List (and drop the old points 
    			//otherwise this will keep growing
    			valid_index = i;
    			break;    			
    		}    		
    	}
    	if(valid_index > 0){ //remove the old points
    		//while(valid_index > 0)
    		for(int i=0; i < valid_index - 1; i++){
    			myApp.pList.points.remove(0);
    		}
    	}
    }

    @Override
    public void onDraw(Canvas canvas) {
    	
    	/*int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int newWidth = 500;
        int newHeight = 500;
         
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
         
      // createa matrix for the manipulation
        Matrix matrix = new Matrix();
         // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
         // rotate the Bitmap
         //matrix.postRotate(45);
  
         // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                           width, height, matrix, true);*/
         
 	    Paint paintImg = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
 	    canvas.drawBitmap(bitmap , 0, 0, paintImg);  
 	   
 	    sent = false;
 	    
    	int size = myApp.pList.points.size();
    	int valid_index = 0;//size - 1;
    	for(int i=size -1; i > 1; i--){
    		TaggedPoint start = myApp.pList.points.get(i-1);
    		TaggedPoint end = myApp.pList.points.get(i);
    		if(Math.abs(start.time - end.time) > 0.2e3){
    			//maybe resize the List (and drop the old points 
    			//otherwise this will keep growing
    			valid_index = i;
    			break;    			
    		}
    		canvas.drawLine(start.x, start.y, end.x, end.y, paint);
    	}
    	if(valid_index > 0){ //remove the old points
    		//while(valid_index > 0)
    		for(int i=0; i < valid_index - 1; i++){
    			myApp.pList.points.remove(0);
    		}
    	}
    	
    	if(lastPoint !=null && false){
	    	Paint p1 = new Paint();
	    	p1.setAntiAlias(true);
	        p1.setStrokeWidth((float) 15.0);
	        p1.setColor(Color.WHITE);
	        p1.setStyle(Paint.Style.STROKE);
	       
	    	canvas.drawPoint(lastPoint.x , lastPoint.y, p1);
	    	p1.setStrokeWidth((float) 1.0);
	    	canvas.drawText("Last Point : " + lastPoint.x + " , " + lastPoint.y, lastPoint.x + 2, lastPoint.y+2, p1);
    	}
    	
    	//use this possibly to add rounded edges 
    	//RoundRectShape
    	//add a boarder
    	Rect rect = new Rect();
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(3);
        getLocalVisibleRect(rect);
        canvas.drawRect(rect, paint); 
    }
        
    void updateDirection(MotionEvent event){
    	float margin = 150;
    	float x_min = margin;
    	float x_max = this.getWidth() - margin;
    	float y_min = margin; 
    	float y_max = this.getHeight() - margin;
    	float x = event.getX();
    	float y = event.getY();
    	    	 
    	if(x < x_min && y < y_max && y > y_min){
    		myApp.dir.direction = DIR.LEFT;  	
    	}
    	else if(x > x_max && y < y_max && y > y_min){
    		myApp.dir.direction = DIR.RIGHT;  	
    	}
    	else if(y < y_min && x < x_max && x > x_min){
    		myApp.dir.direction = DIR.UP;  	
    	}
    	else if(y > y_max && x < x_max && x > x_min){
    		myApp.dir.direction = DIR.DOWN;  	
    	}
    	else{
    		myApp.dir.direction = DIR.NONE;	
    	}
    	myApp.dir.utime = event.getEventTime();
    }

    public boolean onTouch(View view, MotionEvent event) {
    	/*if(moveHead){
    		lastPoint = new TaggedPoint(event.getX(), event.getY(), event.getEventTime());
    		updateDirection(event);
    		invalidate();
    	}*/
    	
    	if(allowDrawing){
	        TaggedPoint point = new TaggedPoint();
	        point.x = event.getX();
	        point.y = event.getY();
	        point.time = event.getEventTime();
	        myApp.pList.points.add(point);
	        invalidate();
    	}
        //Log.d(TAG, "point: " + point);
        return true;
    }
    
    public void clearPoints(){
    	myApp.pList.points.clear();
    	invalidate();
    }
    
    public void sendPoints(){
    	sent = true;
    	paint.setColor(Color.GREEN);
    	invalidate();
    }
    
}

