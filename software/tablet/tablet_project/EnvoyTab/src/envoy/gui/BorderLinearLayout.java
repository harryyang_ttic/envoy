package envoy.gui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.widget.LinearLayout;

public class BorderLinearLayout extends LinearLayout{

	public BorderLinearLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public void onDraw(Canvas canvas) {		
		super.onDraw(canvas);
		Rect rect = new Rect();
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(3);
        getLocalVisibleRect(rect);
        //canvas.drawRect(rect, paint);
        canvas.drawLine(rect.left-1, rect.bottom-1, rect.right-1, rect.bottom-1, paint);
    	//use this possibly to add rounded edges 
    	//RoundRectShape
    	//add a boarder
    	 
    }
}
