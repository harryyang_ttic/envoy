package envoy.gui;

import android.util.Log;

class MonitorThread extends Thread
{
	EnvoyApp myApp;
	
	MonitorThread(EnvoyApp app){
		myApp = app;
	}
	
	public void run(){
		while(true){
			boolean run = true;
			
			run = myApp.started.get();
			
			if(!run){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}					
			}
			
			else{ //see if there is a thread running - and if its connected
				if(myApp != null && (myApp.clThread ==null) || (myApp.clThread !=null && !myApp.clThread.connected)){
					if(myApp.clThread !=null && !myApp.clThread.connected)
						Log.e("Err", "Not connected");
					else
						Log.e("Err", "Not Created");
						
					myApp.clThread = new SocketCreator(myApp);
					myApp.clThread = myApp.clThread;		    			
					myApp.clThread.start();
					Log.e("Stuck", "Thread stuck - restarting new one");
				}
				//Log.e("err","Sleeping");
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	/*public void run(){
		while(true){
			boolean run = true;
			
			run = myApp.started.get();
			
			if(myApp.clThread == null){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}					
			}
			else{ //might need to fix this one 
				synchronized (myApp.clThread.socketLock) {
					run = myApp.clThread.started.get(); 
					myApp.clThread.socketLock.notifyAll();
				} 
				
				if(!myApp.clThread.connected && run){
					if(!myApp.clThread.connected )
						Log.e("Err", "Not connected");
					if(myApp.clThread.connected )
						Log.e("Err", "Connected");
					if(run)
						Log.e("Err", "Running");
					else{
						Log.e("Err", "Not Running");
					}
					myApp.clThread = new SocketCreator(myApp);
					myApp.clThread = myApp.clThread;		    			
					myApp.clThread.start();
					Log.e("Stuck", "Thread stuck - restarting new one");
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}*/
}