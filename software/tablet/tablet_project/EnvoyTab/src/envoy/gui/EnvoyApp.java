package envoy.gui;

import java.util.concurrent.atomic.AtomicBoolean;

import envoy.gui.ClickDirection.LEVEL;
import android.app.Application;
import android.content.Context;

public class EnvoyApp extends Application {

	//private String myState;
	static TaggedPointList pList = new TaggedPointList();
    //static ClickDirection dir = new ClickDirection();
    static AtomicBoolean valid_data = new AtomicBoolean(false);
	static SocketCreator clThread = null;
	static MonitorThread mThread = null;
	static AtomicBoolean started = new AtomicBoolean(false);
	static LEVEL headLevel = LEVEL.MEDIUM;
	static AtomicBoolean stopBufferUpdate = new AtomicBoolean(false);
	static String imageSource = "Kinect";
	public static final String SERVER_IP_STRING = "serverIP";
	static String serverIP = "sachih.csail.mit.edu";
	static ClickDirection dir = new ClickDirection();
	static String[] items = new String[] {"192.168.1.11", "sachih.csail.mit.edu", 
		"envoy-wireless.csail.mit.edu", "192.168.237.82"};
	
	static String[] channels = new String[] {"Kinect", "RGB"};
	static AtomicBoolean deviceUpdated = new AtomicBoolean(false);
	
	private static EnvoyApp instance = null;

    public EnvoyApp() {
        instance = this;
        if (null == instance)
        {
			instance = new EnvoyApp();
        }
        if(mThread == null){
        	mThread = new MonitorThread(this);
        	mThread.start();
        }
    }

	public static Context getContext() {
		if (null == instance)
        {
			instance = new EnvoyApp();
        }
		return instance;
    }
}