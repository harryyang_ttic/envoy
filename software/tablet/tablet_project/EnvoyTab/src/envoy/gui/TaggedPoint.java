package envoy.gui;

import java.io.Serializable;

class TaggedPoint implements Serializable{
    float x, y;
    long time;
    
    public TaggedPoint(){
    	
    }
    public TaggedPoint(float x, float y, long time){
    	this.x = x;
    	this.y = y; 
    	this.time = time;
    }
    public String toString() {
        return x + ", " + y;
    }
}
