package envoy.gui;

import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import envoy.gui.ClickDirection.LEVEL;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class DebugView extends Activity {
	static String TAG_ID = "SETTINGS";
	
	EnvoyApp myApp;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        /*ScrollView sv = new ScrollView(this);*/
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        //sv.addView(ll);
        
        LinearLayout hl = new LinearLayout(this);
        hl.setOrientation(LinearLayout.HORIZONTAL);
        
        TextView tv = new TextView(this);
        tv.setText("Server IP          ");
        hl.addView(tv);
        
        myApp = (EnvoyApp)getApplication();
        //serverList.add("192.168.1.11");
        //serverList.add("sachih.csail.mit.edu");
        //String[] items = new String[] {"192.168.1.11", "sachih.csail.mit.edu"};//(String[]) EnvoyTabActivity.serverList.toArray();//new String[] {"One", "Two", "Three"};
        Spinner spinner = new Spinner(this);//(Spinner) findViewById(R.id.mySpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, myApp.items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        
        spinner.setAdapter(adapter);
        int saved_id = 0;
        for(int i = 0; i< myApp.items.length; i++){
        	if(myApp.items[i].equalsIgnoreCase(myApp.serverIP)){
        		saved_id = i;
        		break;
        	}        	
        }
        spinner.setSelection(saved_id);
        spinner.setOnItemSelectedListener(new MyOnItemSelectedListener());
        hl.addView(spinner);
        
        ll.addView(hl);
        
        LinearLayout hlServo = new LinearLayout(this);
        hlServo.setOrientation(LinearLayout.HORIZONTAL);
        
        TextView tv1 = new TextView(this);
        tv1.setText("Servo Speed     ");
        hlServo.addView(tv1);
        
        //add the image channels - or device types 
        String[] levels = new String[] {"Small", "Medium", "Large"};
        Spinner spinnerServo = new Spinner(this);//(Spinner) findViewById(R.id.mySpinner);
        ArrayAdapter<String> adapterServo = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, levels);
        adapterServo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        
        spinnerServo.setAdapter(adapterServo);
                
        spinnerServo.setSelection(1);
        spinnerServo.setOnItemSelectedListener(new OnServoSelectedListener());
        hlServo.addView(spinnerServo);
        
        ll.addView(hlServo);
        
        LinearLayout hlCam = new LinearLayout(this);
        hlCam.setOrientation(LinearLayout.HORIZONTAL);
        
        TextView tvCam = new TextView(this);
        tvCam.setText("Image Sources  ");
        hlCam.addView(tvCam);
        

        //serverList.add("192.168.1.11");
        //serverList.add("sachih.csail.mit.edu");
        //"Bumblebee", "Dragonfly"};
        
        Spinner spinnerCam = new Spinner(this);//(Spinner) findViewById(R.id.mySpinner);
        ArrayAdapter<String> adapterCam = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, myApp.channels);
        adapterCam.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        
        spinnerCam.setAdapter(adapterCam);
                
        int saved_id_cam = 0;
        for(int i = 0; i< myApp.channels.length; i++){
        	if(myApp.channels[i].equalsIgnoreCase(myApp.imageSource)){
        		saved_id_cam = i;
        		break;
        	}        	
        }
        
        myApp.deviceUpdated.set(true);
        
        spinnerCam.setSelection(saved_id_cam);
        spinnerCam.setOnItemSelectedListener(new OnCamSelectedListener());
        hlCam.addView(spinnerCam);
        
        ll.addView(hlCam);
        
        setContentView(ll);
        
        //we need another spinner to show all the channels 
    }
    
    public class OnCamSelectedListener implements OnItemSelectedListener {
        
        public void onItemSelected(AdapterView<?> parent,
            View view, int pos, long id) {
          
          SharedPreferences settings = getSharedPreferences(myApp.SERVER_IP_STRING, 0);
          SharedPreferences.Editor editor = settings.edit();          
          editor.putString("DeviceType", parent.getItemAtPosition(pos).toString());
          editor.commit();
          synchronized (myApp.imageSource) {
        	  if(!myApp.imageSource.equalsIgnoreCase(parent.getItemAtPosition(pos).toString())){
        		  myApp.imageSource = parent.getItemAtPosition(pos).toString();
        		  myApp.deviceUpdated.set(true);
        	  }
          }
        }

        public void onNothingSelected(AdapterView parent) {
          // Do nothing.
        }
    }
    
    public class OnServoSelectedListener implements OnItemSelectedListener {
        
        public void onItemSelected(AdapterView<?> parent,
            View view, int pos, long id) {
        	if(parent.getItemAtPosition(pos).toString().equalsIgnoreCase("LARGE")){
        		myApp.headLevel = LEVEL.LARGE;
        	}	
        	if(parent.getItemAtPosition(pos).toString().equalsIgnoreCase("MEDIUM")){
        		myApp.headLevel = LEVEL.MEDIUM;
        	}
        	if(parent.getItemAtPosition(pos).toString().equalsIgnoreCase("SMALL")){
        		myApp.headLevel = LEVEL.SMALL;
        	}
        }

        public void onNothingSelected(AdapterView parent) {
          // Do nothing.
        }
    }
    
    public class MyOnItemSelectedListener implements OnItemSelectedListener {
    
        public void onItemSelected(AdapterView<?> parent,
            View view, int pos, long id) {
          Toast.makeText(parent.getContext(), "Server : " +
              parent.getItemAtPosition(pos).toString(), Toast.LENGTH_LONG).show();
          myApp.serverIP = parent.getItemAtPosition(pos).toString(); 
          //save stuff on to disk - to load 
          SharedPreferences settings = getSharedPreferences(myApp.SERVER_IP_STRING, 0);
          SharedPreferences.Editor editor = settings.edit();          
          editor.putString("serverIP", parent.getItemAtPosition(pos).toString());
          editor.commit();
        }

        public void onNothingSelected(AdapterView parent) {
          // Do nothing.
        }
    }
}