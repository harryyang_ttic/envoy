package envoy.gui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FrontView extends Activity {

	public class DialogState{
		Button prevState = null;
		Button currState = null;
		Button currSpeech = null;
		Button currAction = null;
		
		PorterDuffColorFilter greenFilter = null;
		PorterDuffColorFilter redFilter = null;
		
		public DialogState(Context c){
			prevState = new Button(c);
			//since gray turns red
			
			prevState.setOnClickListener(new Button.OnClickListener() { public void onClick (View v){ changeColor();}});
			//prevState.setBackgroundColor(Color.GRAY); //this messes up the button
			
			//prevState.getBackground().setColorFilter(new ColorFilter());
			currState = new Button(c);
			//currState.setBackgroundColor(Color.GREEN);
			PorterDuffColorFilter greenFilter = new PorterDuffColorFilter(Color.GREEN, PorterDuff.Mode.SRC_ATOP);
			
			redFilter = new PorterDuffColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
			
			prevState.getBackground().setColorFilter(redFilter);//0xFFFF0000, PorterDuff.Mode.MULTIPLY); //this one does it nicer
			currState.getBackground().setColorFilter(greenFilter);//0xFFFF0000, PorterDuff.Mode.MULTIPLY); //this one does it nicer
			currSpeech = new Button(c);			
			currAction = new Button(c);
		}
		
		private void changeColor(){
			prevState.getBackground().setColorFilter(greenFilter);
		}
	}
	
	public class NavigationState{
		Button currentFloor = null;
		Button endGoal = null;
		Button currGoal = null;
		
		
		public NavigationState(Context c){
			currentFloor = new Button(c);
			endGoal = new Button(c);
			currGoal = new Button(c);
		}
	}
	
	private DialogState dsButtons = null;
	private NavigationState navButtons = null;
	private byte[] buffer = null;
	private TextView serverResult = null;
	private TextView imageText = null;
	
	private long utime;
	private boolean new_image = false;
	static private Object lock = new Object();
	static boolean started = false;
	private String serverQuerry = null; 

	private ImageView im1 = null, im2 = null, im3 = null, im4 = null;
	private static Thread picThread = null;
	
	private Button b = null, b1 = null;
	
	private long mStartTime = 0;

	private Handler mHandler = new Handler();	

	private boolean connected = false;
	static final long utime_now()
    {
        return System.nanoTime()/1000;
    }   
	
	class PictureThread extends Thread
    {
		//private boolean connected = false;
		
		private String hostname = "192.168.1.11";//"sachih.csail.mit.edu";
		private int socket = 4444;
		Socket kkSocket = null;
        PrintWriter out = null;
        ObjectInputStream in = null;
        Context context = null;
        private boolean save_file = false;         

    	boolean mExternalStorageAvailable = false;
    	boolean mExternalStorageWriteable = false;
    	
        public PictureThread(String Host, int Socket, Context context)
        {
            setDaemon(true);
            this.context = context;
            hostname = Host;
            socket = Socket;
        }
        
        public PictureThread(Context context)
        {
            setDaemon(true);
            this.context = context;
        }
        
        public void run()
        {
        	try {
        		//create socket 
        		//we might need another socket 
                kkSocket = new Socket(hostname, socket);
                out = new PrintWriter(kkSocket.getOutputStream(), true);
                in = new ObjectInputStream(kkSocket.getInputStream());
                serverQuerry = "Connected";
                connected = true;
                Log.d("Sucess","Connected");
            } catch (UnknownHostException e) {
                System.err.println("Don't know about host: taranis.");
                serverQuerry = "Error - Can't Find Host";
                connected = false;    
                Log.e("Err","Error - Unknown Host");
            } catch (IOException e) {
                System.err.println("Couldn't get I/O for the connection to: taranis.");
                serverQuerry = "Error - IO Error";
                connected = false;
                Log.e("Err","Error - IO");
            }            	
            	
        	//do this check if we are saving to disk
        	if(save_file){
	            String state = Environment.getExternalStorageState();
	
	            if (Environment.MEDIA_MOUNTED.equals(state)) {
	                // We can read and write the media
	                mExternalStorageAvailable = mExternalStorageWriteable = true;
	            } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
	                // We can only read the media
	                mExternalStorageAvailable = true;
	                mExternalStorageWriteable = false;
	            } else {
	            	// Something else is wrong. It may be one of many other states, but all we need
	            	//  to know is we can neither read nor write
	            	mExternalStorageAvailable = mExternalStorageWriteable = false;
	            }
        	}
            
            //we expect to get a stream of data 
            try {
            	
            	while(true){            	
            		//the locking stuff has been commented out - should be put back at some point 
            		//synchronized ( lock ) {
            			//this is a blocking call
            			//buffer = (byte[])in.readObject();
            		
            			Object ob = in.readObject();
            			
            			byte[] b = new byte[10];
            			
            			//Log.e("Error ", "" + ob.getClass());
            			
            			if(b.getClass().equals(ob.getClass())){
            				buffer = (byte [])ob;	
            			/*ObjectInputStream.GetField fields = in.readFields();
            			buffer = (byte []) fields.get("Image_buf", null);*/
            			//call this if needed
            				System.gc();
            			//lock.notifyAll();
            			}
            			else if(String.class.equals(ob)){
            				Log.d("Err", "String " + ((String) ob));
            			}
            			/*else if(EnvoyState.class.equals(ob)){
            				state = (EnvoyState) ob;
            			}*/
            		//}
	            	
            		//tell the gui to update the image
	            	new_image = true;
	            	utime = utime_now();
	            	    
	            	//saving to disk - and telling the gallery to update
	            	if(save_file && mExternalStorageAvailable){
	            		File path = Environment.getExternalStoragePublicDirectory(
	            				Environment.DIRECTORY_PICTURES);
	                    File file = new File(path, "world.jpg");
	                    FileOutputStream fos = new FileOutputStream(file);
	                    fos.write(buffer);  
	                    fos.close();
		
	                    //to update the gallery - not needed if we are doing other stuff using the file
	                    MediaScannerConnection.scanFile(this.context,
	                    		new String[] { file.toString() }, null,
	                    		new MediaScannerConnection.OnScanCompletedListener() {
	                    	public void onScanCompleted(String path, Uri uri) {
	                    		Log.i("ExternalStorage", "Scanned " + path + ":");
	                    		Log.i("ExternalStorage", "-> uri=" + uri);
	                    	}
	                    });
	            	}
	            	
	            	
	            	serverQuerry = /*"Received and Wrote File" + buffer.length*/ ""  + ob.getClass() + " -> " + b.getClass();
	            	
	            	Log.d("Rec","Received Image");
            	}            	
            	
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();	
				serverQuerry = "Server Error : " +  e.toString();//"IO Error";
				connected = false;
				Log.e("Err","Error - IO");
				
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				serverQuerry = "Class Error";
				connected = false;
				Log.e("Err","Error - Class Error");
			}             
            catch (NullPointerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				serverQuerry = "Null Pointer Exception";
				connected = false;
				Log.e("Err","Error - Null Pointer");
			} 

            
            try {
            	out.close();
				in.close();
	            
	            kkSocket.close();
	            connected = false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				connected = false;
				Log.e("Err","Error - IO Closing Socket");
			}
            catch (NullPointerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				serverQuerry = "Null Pointer Exception";
				connected = false;
				Log.e("Err","Error - Null Pointer Closing Socket");
			} 
        }      
    }
	
	private class Updater implements Runnable{
		
		//HashMap<String, ChannelData> = new HashMap<String,ChannelData>();
		
		private Context mContext;
		private DecimalFormat format = new DecimalFormat("###.##");
		   //private
		public  Updater(Context c){
			mContext = c;			
		}
		
		//this updates the GUI elements 
		public void run() {
			
			if(serverQuerry != null)
				serverResult.setText(serverQuerry);

			if(buffer != null && new_image){
				BitmapFactory.Options opt = new BitmapFactory.Options();
	    	    opt.inDither = true;
	    	    opt.inPreferredConfig =  Bitmap.Config.ARGB_8888;// .Config.ARGB_8888;
	    	    	    	    
			}
			
			//set text on buttons and stuff
			mHandler.postDelayed(this,100);			
		}		
	}
	
	class MonitorThread extends Thread
    {
		Context context = null;
		MonitorThread(Context c){
			context = c;
		}
		
		public void run(){
			while(true){
				if(!connected && started){
					new PictureThread(context).start();	
					Log.e("Stuck", "Thread stuck - restarting new one");
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
    }
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        
        int s_width = metrics.widthPixels;
        int s_height = metrics.heightPixels;
        
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        
        //lets add a layout for a set of icons
        LinearLayout vl = new LinearLayout(this);
        vl.setOrientation(LinearLayout.HORIZONTAL);
        
        int no_icons = 4;
        LayoutParams imgParams = 
        		//new LayoutParams(70, 70);
        		new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        
        im1 = new ImageView(this);
        im1.setImageResource(R.drawable.ic_launcher);
        im1.setAdjustViewBounds(true); // set the ImageView bounds to match the Drawable's dimensions
        im1.setLayoutParams(imgParams);
        
        im2 = new ImageView(this);
        im2.setImageResource(R.drawable.ic_launcher);
        im2.setAdjustViewBounds(true); // set the ImageView bounds to match the Drawable's dimensions
        im2.setLayoutParams(imgParams);
        
        im3 = new ImageView(this);
        im3.setImageResource(R.drawable.ic_launcher);
        im3.setAdjustViewBounds(true); // set the ImageView bounds to match the Drawable's dimensions
        im3.setLayoutParams(imgParams);
        
        im4 = new ImageView(this);
        im4.setImageResource(R.drawable.ic_launcher);
        im4.setAdjustViewBounds(true); // set the ImageView bounds to match the Drawable's dimensions
        im4.setLayoutParams(imgParams);
        
        
        vl.addView(im1);
        vl.addView(im2);
        vl.addView(im3);
        vl.addView(im4);
        ll.addView(vl);

        LinearLayout dialogNavPanel = new LinearLayout(this);
        dialogNavPanel.setOrientation(LinearLayout.HORIZONTAL);

        ll.addView(dialogNavPanel);
        
        LayoutParams layoutParams = 
        		//new LayoutParams(70, 70);
        		new LayoutParams((int) (s_width/2.0), LayoutParams.WRAP_CONTENT);
        
        //boarder doesnt seem to work
        BorderLinearLayout dialogStatus = new BorderLinearLayout(this);//new LinearLayout(this);
        dialogStatus.setOrientation(LinearLayout.VERTICAL);
        dialogStatus.setLayoutParams(layoutParams);
        //add textViews (or maybe modified text views)
        /*TextView prevState = new MyTextView(this);
        prevState.setText("DM : Heard Speech");
        dialogStatus.addView(prevState);
                
        TextView currState = new TextView(this);
        currState.setText("DM : Current State");
        dialogStatus.addView(currState);
        
        TextView heardSpeech = new TextView(this);
        heardSpeech.setText("DM : Heard Speech");
        dialogStatus.addView(heardSpeech);
        
        TextView currAction = new TextView(this);
        currAction.setText("DM : Action");
        dialogStatus.addView(currAction);*/
        
        dsButtons = new DialogState(this);
        
        //Button prevState = new Button(this);
        dsButtons.prevState.setText("DM : Prev State");
        dialogStatus.addView(dsButtons.prevState);
                
        //Button currState = new Button(this);
        //currState.setText("DM : Current State");
        dsButtons.currState.setText("DM : Current State");
        dialogStatus.addView(dsButtons.currState);
        
        //Button heardSpeech = new Button(this);
        //heardSpeech.setText("DM : Heard Speech");
        dsButtons.currSpeech.setText("DM : Heard Speech");
        dialogStatus.addView(dsButtons.currSpeech);
        
        //Button currAction = new Button(this);
        //currAction.setText("DM : Action");
        dsButtons.currAction.setText("DM : Action");
        dialogStatus.addView(dsButtons.currAction);
        
        dialogNavPanel.addView(dialogStatus);
        
        LinearLayout navStatus = new LinearLayout(this);
        navStatus.setOrientation(LinearLayout.VERTICAL);
        navStatus.setLayoutParams(layoutParams);
        //add textViews (or maybe modified text views)
        /*TextView currentFloor = new TextView(this);
        currentFloor.setText("Navigator: Current Floor");
        navStatus.addView(currentFloor);
        
        
        TextView endGoal = new TextView(this);
        endGoal.setText("Navigator: End Goal is Loc 1");
        navStatus.addView(endGoal);
        
        TextView subGoal = new TextView(this);
        subGoal.setText("Navigator: We are going to the Elevator");
        navStatus.addView(subGoal);*/
        
        navButtons = new NavigationState(this);
        //Button currentFloor = new Button(this);
        navButtons.currentFloor.setText("Navigator: Current Floor");
        navStatus.addView(navButtons.currentFloor);
                
        navButtons.endGoal.setText("Navigator: End Goal is Loc 1");
        navStatus.addView(navButtons.endGoal);
        
        navButtons.currGoal.setText("Navigator: We are going to the Elevator");
        navStatus.addView(navButtons.currGoal);
        
        dialogNavPanel.addView(navStatus);
       
        serverResult = new TextView(this);
        serverResult.setText("This is the Main tab: Line 1");
        ll.addView(serverResult);
               
        imageText = new TextView(this);
        imageText.setText("This is the Main tab: Line 2");
        ll.addView(imageText);
        
        LinearLayout vl_img = new LinearLayout(this);
        vl_img.setOrientation(LinearLayout.HORIZONTAL);
        ll.addView(vl_img);
        //AttributeSet at = new AttributeSet();
        LayoutParams imageParams = 
        		new LayoutParams((int) (s_width/2.0), 300);//LayoutParams.WRAP_CONTENT);
        
        ImageView mapImg = new ImageView(this);
        mapImg.setImageResource(R.drawable.map);
        mapImg.setAdjustViewBounds(true); // set the ImageView bounds to match the Drawable's dimensions
        mapImg.setLayoutParams(imageParams);
        
        vl_img.addView(mapImg);
        
        LinearLayout vl_button = new LinearLayout(this);
        vl_button.setOrientation(LinearLayout.HORIZONTAL);
        ll.addView(vl_button);
 
        /*dv = new DrawView(this);//(DrawView) findViewById(R.layout.);//new DrawView(this);
        dv.setLayoutParams(layoutParams);
        ll.addView(dv);*/
        
        
        b = new Button(this);
        b.setText("Get Picture");        
        b.setOnClickListener(new Button.OnClickListener() { public void onClick (View v){ getPicture(); }});
        LayoutParams btnParams = 
        		new LayoutParams((int)(s_width/2.0), LayoutParams.WRAP_CONTENT);
        
        b.setLayoutParams(btnParams);
        vl_button.addView(b);
        
        b1 = new Button(this);
        b1.setText("Stop");        
        b1.setLayoutParams(btnParams);
        //b1.setOnClickListener(new Button.OnClickListener() { public void onClick (View v){ getPicture(); }});
        vl_button.addView(b1);
                
      //add thread to update gui 
        if(mStartTime == 0L){
			mStartTime = SystemClock.uptimeMillis();
		}
        
        Updater mUpdateTimeTask = new Updater(this);
        
		Thread t = new Thread(mUpdateTimeTask);
        t.start();     
                
        new MonitorThread(this).start();
        
        setContentView(ll);
    }
    
    private void getPicture()
    {
    		if(!connected){
    			started = true;
    			b.setText("Streaming");
    			picThread =	new PictureThread(this);
    			picThread.start();
    		}
    		else{
    			b.setText("Start Streaming"); 
    		}
    }
 
}