package envoy.gui;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;

public class EnvoyMain extends TabActivity {
    /** Called when the activity is first created. */	
	
	static String test = "Test";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Resources res = getResources(); // Resource object to get Drawables
        TabHost tabHost = getTabHost();  // The activity TabHost
        TabHost.TabSpec spec;  // Resusable TabSpec for each tab
        Intent intent;  // Reusable Intent for each tab

        // Create an Intent to launch an Activity for the tab (to be reused)
        /*intent = new Intent().setClass(this, FrontView.class);

        spec = tabHost.newTabSpec("front").setIndicator("Front",
                res.getDrawable(R.drawable.ic_tab_artists))
            .setContent(intent);
        tabHost.addTab(spec);*/
        
        intent = new Intent().setClass(this, LCMImageView.class);
        
        // Initialize a TabSpec for each tab and add it to the TabHost
        spec = tabHost.newTabSpec("lcm").setIndicator("LCM",
                          res.getDrawable(R.drawable.ic_tab_artists))
                      .setContent(intent);
        
        tabHost.addTab(spec);
        
        
        
        /*intent = new Intent().setClass(this, MainView.class);
        // Initialize a TabSpec for each tab and add it to the TabHost
        spec = tabHost.newTabSpec("main").setIndicator("Main",
                          res.getDrawable(R.drawable.ic_tab_artists))
                      .setContent(intent);
        tabHost.addTab(spec);*/

        intent = new Intent().setClass(this, ComplexView.class);
        spec = tabHost.newTabSpec("complex").setIndicator("Complex",
                          res.getDrawable(R.drawable.ic_tab_artists))
                      .setContent(intent);
        tabHost.addTab(spec);
        
        // Do the same for the other tabs
        intent = new Intent().setClass(this, DebugView.class);
        spec = tabHost.newTabSpec("debug").setIndicator("Debug",
                          res.getDrawable(R.drawable.ic_tab_artists))
                      .setContent(intent);
        tabHost.addTab(spec);

        tabHost.setCurrentTab(0);
    }
}