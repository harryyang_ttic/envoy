package envoy.gui;

import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicBoolean;

import android.util.Log;

public class SocketCreator extends Thread{
	Socket socket;
	SocketSender sr;
	SocketListener sk;
	protected String serverQuerry = null;
	protected boolean connected = false;//new AtomicBoolean(false);
	//AtomicBoolean started = new AtomicBoolean(true);//(false);
	static protected Object socketLock = new Object();
	EnvoyApp myApp; 
	
	public SocketCreator(EnvoyApp app) {
		setDaemon(true);
		myApp = app;
		Log.e("Error", "Created Socket");
		// TODO Auto-generated constructor stub
	}
			
	public void run(){
		try {
			socket = new Socket(myApp.serverIP, 4444);
			//connected = true;
			
	    	sr = new SocketSender(socket, this);
	    	sr.start();
	    	
	    	sk = new SocketListener(socket, this);	    	
	    	sk.start();
	    	
	    	sk.join();
	    	sr.join();
	    	
	    	connected = false;

		} 
		catch (ConnectException e) {
			// TODO Auto-generated catch block
			Log.e("Error", "Unreachable Host");
			e.printStackTrace();
		}
		catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			Log.e("Error", "Unknown Host");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("Error", "IO Error");
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			if(socket !=null){
				try {
					socket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			Log.e("Error", "Done");
		}
	}
}
