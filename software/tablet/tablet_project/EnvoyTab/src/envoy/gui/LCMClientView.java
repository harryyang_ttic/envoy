package envoy.gui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.concurrent.atomic.AtomicBoolean;

import envoy.gui.FrontView.DialogState;
import envoy.gui.FrontView.NavigationState;
import android.app.Activity;
import android.content.Context;
import android.content.pm.LabeledIntent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LCMClientView extends Activity {
	static String TAG_ID = "LCM";
	
	private long last_draw_utime = 0;
	private DialogState dsButtons = null;
	private NavigationState navButtons = null;
	private TextView serverResult = null, stateText = null;
	private TextView imageText = null;
	
	private DrawView dv = null;
	
	EnvoyApp myApp;

	private String envoyStateText = null;

	//private ImageView im1 = null, im2 = null, im3 = null, im4 = null;
	private ImageButton im1 = null, im2 = null, im3 = null, im4 = null;
	
	private static SocketCreator picThread = null;
	
	private Button b = null, b1 = null;
	
	private long mStartTime = 0;

	private Handler mHandler = new Handler();
	
	public class DialogState{
		Button prevState = null;
		Button currState = null;
		Button currSpeech = null;
		Button currAction = null;
		
		PorterDuffColorFilter greenFilter = null;
		PorterDuffColorFilter redFilter = null;
		
		public DialogState(Context c){
			prevState = new Button(c);
			//since gray turns red
			
			prevState.setOnClickListener(new Button.OnClickListener() { public void onClick (View v){ changeColor();}});
			//prevState.setBackgroundColor(Color.GRAY); //this messes up the button
			
			//prevState.getBackground().setColorFilter(new ColorFilter());
			currState = new Button(c);
			//currState.setBackgroundColor(Color.GREEN);
			PorterDuffColorFilter greenFilter = new PorterDuffColorFilter(Color.GREEN, PorterDuff.Mode.SRC_ATOP);
			
			redFilter = new PorterDuffColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
			
			prevState.getBackground().setColorFilter(redFilter);//0xFFFF0000, PorterDuff.Mode.MULTIPLY); //this one does it nicer
			currState.getBackground().setColorFilter(greenFilter);//0xFFFF0000, PorterDuff.Mode.MULTIPLY); //this one does it nicer
			currSpeech = new Button(c);			
			currAction = new Button(c);
		}
		
		private void changeColor(){
			prevState.getBackground().setColorFilter(greenFilter);
		}
	}
	
	public class NavigationState{
		Button currentFloor = null;
		Button endGoal = null;
		Button currGoal = null;
		Button personTrack = null;
		
		public NavigationState(Context c){
			currentFloor = new Button(c);
			endGoal = new Button(c);
			currGoal = new Button(c);
			personTrack = new Button(c);
		}
	}	
	
	private class Updater implements Runnable{
		
		private Context mContext;
		private DecimalFormat format = new DecimalFormat("###.##");
		   //private
		public  Updater(Context c){
			mContext = c;			
		}
		
		//this updates the GUI elements 
		public void run() {
			serverResult.setText("Server IP : " + myApp.serverIP);
			
			/*if(picThread != null && false){// && picThread.sk != null){
				//if(picThread.serverQuerry != null)					
				
				if(picThread.sk != null && picThread.sk.buffer != null && picThread.sk.utime > last_draw_utime){
				
					BitmapFactory.Options opt = new BitmapFactory.Options();
		    	    opt.inDither = true;
		    	    opt.inPreferredConfig =  Bitmap.Config.ARGB_8888;// .Config.ARGB_8888;
		    	    
		    	    Bitmap bitmap = null;
		    	    //locking for the memory
		    	    //synchronized ( lock ) {
		    	    bitmap = BitmapFactory.decodeByteArray(picThread.sk.buffer, 0, picThread.sk.buffer.length, opt);
		    	    	//lock.notifyAll();
		    	    //}
		    	    last_draw_utime = picThread.sk.utime;
		    	    //updating the GUI 
		    	    if(bitmap !=null && dv != null){	    	    	
		    	    	dv.SetBitmap(bitmap);
		    	    
		    	    	dv.invalidate();
		    	    	//System.gc();
		    	    	//picThread.new_image = false;
		    	    }
				}
			}*/
		    	    
		    	if(myApp.clThread != null && myApp.clThread.connected){
		    		b.setText("Streaming");	    	    	
		    	}
		    	else{
		    		if(myApp.started.get()){
		    			b.setText("Connecting");
		    	    }
		    		else{
		    			b.setText("Start Streaming");
		    	    }
		    	}
		    	    
		    	if(envoyStateText != null){
		    		stateText.setText(envoyStateText);	    	    	
		    	}
		    	if(myApp.clThread != null && myApp.clThread.sk != null && myApp.clThread.sk.envoyState != null && dsButtons !=null && navButtons != null){
		    		
		    	        dsButtons.prevState.setText("Prev State : " + myApp.clThread.sk.envoyState.previousState);
		    	        dsButtons.currState.setText("Current State : " + myApp.clThread.sk.envoyState.currentState);
		    	        dsButtons.currSpeech.setText("Speech : " + myApp.clThread.sk.envoyState.speechText);
		    	        dsButtons.currAction.setText("Action : " + myApp.clThread.sk.envoyState.action);
		    	        navButtons.currentFloor.setText("Curr Floor : " + myApp.clThread.sk.envoyState.currentFloor);
		    	        navButtons.endGoal.setText("End Goal : " + myApp.clThread.sk.envoyState.finalGoal);
		    	        navButtons.currGoal.setText("Curr Goal : " + myApp.clThread.sk.envoyState.currentGoal);
		    	        if(myApp.clThread.sk.envoyState.personState == 0){
		    	        	navButtons.personTrack.setText("Lost Person");
		    	        }
		    	        else if(myApp.clThread.sk.envoyState.personState == 1){
		    	        	navButtons.personTrack.setText("Tracking Person");
		    	        }
		    	        else if(myApp.clThread.sk.envoyState.personState == 2){
		    	        	navButtons.personTrack.setText("Looking for Person");
		    	        }
		    	        imageText.setText("New Image Received : " + myApp.clThread.sk.utime/ 1.0e6);
		    	}
			
		    	else{
		    		imageText.setText("Not Conneted");
		    	}
			
			//set text on buttons and stuff
			mHandler.postDelayed(this,100);			
		}		
	}
	
	
	
	 @Override
	public void onDetachedFromWindow()
	    {
	        // Detach us from our callback
	        //if (fCallback != null) fCallback.pThis = null;
		    Log.e("Err", "detached from Window");
	        super.onDetachedFromWindow();
	    }
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        
        int s_width = metrics.widthPixels;
        int s_height = metrics.heightPixels;
        
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        
        //lets add a layout for a set of icons
        LinearLayout vl = new LinearLayout(this);
        vl.setOrientation(LinearLayout.HORIZONTAL);
        
        int no_icons = 4;
        LayoutParams imgParams = 
        		new LayoutParams(80, 80);
        		//new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        
        im1 = new ImageButton(this);
        im1.setImageResource(R.drawable.person);
        im1.setAdjustViewBounds(true); // set the ImageView bounds to match the Drawable's dimensions
        im1.setLayoutParams(imgParams);
        
        im2 = new ImageButton(this);
        im2.setImageResource(R.drawable.wifi_icon);
        im2.setAdjustViewBounds(true); // set the ImageView bounds to match the Drawable's dimensions
        im2.setLayoutParams(imgParams);
        
        im3 = new ImageButton(this);
        im3.setImageResource(R.drawable.ic_app);
        im3.setAdjustViewBounds(true); // set the ImageView bounds to match the Drawable's dimensions
        im3.setLayoutParams(imgParams);
        
        im4 = new ImageButton(this);
        im4.setImageResource(R.drawable.ic_app);
        im4.setAdjustViewBounds(true); // set the ImageView bounds to match the Drawable's dimensions
        im4.setLayoutParams(imgParams);        
        
        ImageButton im5 = new ImageButton(this);
        im5.setImageResource(R.drawable.person);
        im5.setAdjustViewBounds(true);
        im5.setLayoutParams(imgParams);
        
        vl.addView(im1);
        vl.addView(im2);
        vl.addView(im3);
        vl.addView(im4);
        vl.addView(im5);
        ll.addView(vl);
                
        LinearLayout stateImageLayout = new LinearLayout(this);
        stateImageLayout.setOrientation(LinearLayout.HORIZONTAL);
        ll.addView(stateImageLayout);
        //AttributeSet at = new AttributeSet();
        LayoutParams layoutParams =
        		new LayoutParams((int) (s_width /2.0), 500);//LayoutParams.WRAP_CONTENT);
        		//new LayoutParams(LayoutParams.FILL_PARENT, 500);//LayoutParams.WRAP_CONTENT);
        
        LayoutParams dialogStateLayout = 
        		//new LayoutParams(70, 70);
        		new LayoutParams((int) (s_width/2.0), LayoutParams.WRAP_CONTENT);
        
        dv = new DrawView(this, myApp);//(DrawView) findViewById(R.layout.);//new DrawView(this);
        dv.setLayoutParams(layoutParams);
        //ll.addView(dv);
        stateImageLayout.addView(dv);
        
        LinearLayout dialogStatus = new LinearLayout(this);//new LinearLayout(this);//new LinearLayout(this);
        dialogStatus.setOrientation(LinearLayout.VERTICAL);
        dialogStatus.setLayoutParams(dialogStateLayout);
        
        /*LinearLayout navStatus = new LinearLayout(this);
        navStatus.setOrientation(LinearLayout.VERTICAL);
        navStatus.setLayoutParams(dialogStateLayout);*/
        
        stateImageLayout.addView(dialogStatus);
        
        TextView dialogOutline = new MyTextView(this);
        dialogOutline.setText("Dialog Status");
        dialogOutline.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        dialogOutline.setTextSize(20);
        dialogStatus.addView(dialogOutline);
        
        dsButtons = new DialogState(this);
        
        //Button prevState = new Button(this);
        dsButtons.prevState.setText("DM : Prev State");
        dialogStatus.addView(dsButtons.prevState);
                
        //Button currState = new Button(this);
        //currState.setText("DM : Current State");
        dsButtons.currState.setText("DM : Current State");
        dialogStatus.addView(dsButtons.currState);
        
        //Button heardSpeech = new Button(this);
        //heardSpeech.setText("DM : Heard Speech");
        dsButtons.currSpeech.setText("DM : Heard Speech");
        dialogStatus.addView(dsButtons.currSpeech);
        
        //Button currAction = new Button(this);
        //currAction.setText("DM : Action");
        dsButtons.currAction.setText("DM : Action");
        dialogStatus.addView(dsButtons.currAction);
                
        TextView navOutline = new MyTextView(this);
        navOutline.setText("Navigation Status");
        navOutline.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        navOutline.setTextSize(20);
        dialogStatus.addView(navOutline);
        
        navButtons = new NavigationState(this);
        //Button currentFloor = new Button(this);
        navButtons.currentFloor.setText("Navigator: Current Floor");
        dialogStatus.addView(navButtons.currentFloor);
                
        navButtons.endGoal.setText("Navigator: End Goal is Loc 1");
        dialogStatus.addView(navButtons.endGoal);
        
        navButtons.currGoal.setText("Navigator: We are going to the Elevator");
        dialogStatus.addView(navButtons.currGoal);
        
        TextView personOutline = new MyTextView(this);
        personOutline.setText("Person Status");
        personOutline.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        personOutline.setTextSize(20);
        dialogStatus.addView(personOutline);
        
        navButtons.personTrack.setText("Tracker: Tracking Person");
        dialogStatus.addView(navButtons.personTrack);
        
                       
        serverResult = new TextView(this);
        serverResult.setText("This is the Main tab: Line 1");
        ll.addView(serverResult);
               
        imageText = new TextView(this);
        imageText.setText("This is the Main tab: Line 2");
        ll.addView(imageText);
        
        stateText = new TextView(this);
        stateText.setText("This is State message");
        //ll.addView(stateText);
        
        LinearLayout vl_button = new LinearLayout(this);
        vl_button.setOrientation(LinearLayout.HORIZONTAL);
        ll.addView(vl_button);
        
        
        b = new Button(this);
        b.setText("Start Streaming");        
        b.setOnClickListener(new Button.OnClickListener() { public void onClick (View v){ getPicture(); }});
        LayoutParams btnParams = 
        		new LayoutParams((int)(s_width/2.0), LayoutParams.WRAP_CONTENT);
        
        b.setLayoutParams(btnParams);
        vl_button.addView(b);
        
        b1 = new Button(this);
        b1.setText("Stop");        
        b1.setLayoutParams(btnParams);
        b1.setOnClickListener(new Button.OnClickListener() { public void onClick (View v){ getPictureConn(); }});
        vl_button.addView(b1);
                
      //add thread to update gui 
        if(mStartTime == 0L){
			mStartTime = SystemClock.uptimeMillis();
		}
        
        Updater mUpdateTimeTask = new Updater(this);
        
		Thread t = new Thread(mUpdateTimeTask);
        t.start();     
          
        //stopping the monitor thread for now
        //new MonitorThread(this).start();
        
        setContentView(ll);
    }
    
    private void getPictureConn(){
    	/*Socket socket;
		try {
			socket = new Socket(EnvoyTabActivity.serverIP, 4444);
	    	SocketListener sk = new SocketListener(socket);
	    	sk.run();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
    	SocketCreator sk = new SocketCreator(myApp);
    	sk.start();
    }
   
    
    private void getPicture()
    {
    	if(myApp.started.get() ==false){
    		b.setText("Connecting");
    		myApp.started.set(true);
    	}
    	else{
    		myApp.started.set(false);
    		b.setText("Start");
    	}
    	
    		/*if(picThread == null){
    			b.setText("Connecting");
    			myApp.clThread =	new SocketCreator(myApp);
    			picThread = myApp.clThread;
    			picThread.started.set(true);
    			picThread.start();
    			Toast.makeText(this, "Connecting to Server " , Toast.LENGTH_LONG).show();
    			
    		}
    		else if(!picThread.started.get()){
    			b.setText("Connecting");
    			
    			myApp.clThread =	new SocketCreator(myApp);
    			picThread = myApp.clThread;
    			picThread.started.set(true);
    			picThread.start();
    			Toast.makeText(this, "Connecting to Server " , Toast.LENGTH_LONG).show();    			  
    		}
    		else{
    			synchronized (picThread.socketLock) {
    				picThread.started.set(false);
    			
    				//picThread.stopThreads();
    				picThread.socketLock.notifyAll();
				}
    			
    			
    			b.setText("Stopping");
    		}*/
    }
}