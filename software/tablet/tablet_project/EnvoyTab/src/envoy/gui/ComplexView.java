package envoy.gui;

import envoy.gui.Utils;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import envoy.gui.ClickDirection.DIR;
import envoy.gui.ClickDirection.LEVEL;
import envoy.gui.DebugView.MyOnItemSelectedListener;
import envoy.gui.DebugView.OnCamSelectedListener;
import envoy.gui.LCMImageView.PictureThread;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.MergeCursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.renderscript.ProgramFragmentFixedFunction.Builder.EnvMode;
import android.speech.RecognizerIntent;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.GridLayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class ComplexView extends Activity {
	static String TAG_ID = "IMAGE";
	//Button clear, tag, send, move;
	//private ImageButton clear, tag, start, send;
	private Button clear, tag, start, send;
	Button move;
	//ClientThread picThread = null;
	//static DrawView dv = null;
	DrawView dv = null;
	TextView textview = null;
	private TextView speechResult= null;
	private Handler mHandler = new Handler();	
	private long last_draw_utime = 0;
	TextView buttonView = null;
	boolean drawing =false;
	Spinner spinner = null;
	
	//static TaggedPointList tpList= null;
	private static final int REQUEST_CODE = 1235;
	private ListView wordsList;	
	
	String clickedButton = "None";
	
	EnvoyApp myApp;
	
	private class Updater implements Runnable{
		
		//HashMap<String, ChannelData> = new HashMap<String,ChannelData>();
		
		private Context mContext;
		private DecimalFormat format = new DecimalFormat("###.##");
		BitmapFactory.Options opt; 
		   //private
		public  Updater(Context c){
			mContext = c;	
			opt = new BitmapFactory.Options();
    	    opt.inDither = true;
    	    opt.inPreferredConfig =  Bitmap.Config.ARGB_8888;// .Config.ARGB_8888;
		}
		
		//this updates the GUI elements 
		public void run() {
			if(myApp.clThread != null && myApp.clThread.sk!=null){					
				if(myApp.started.get()){
		    		start.setText("Connected");		    		
		    	}
		    	else{
		    		start.setText("Connect");
		    		
		    	}
				SocketCreator picThread  = myApp.clThread; 
				if(picThread.sk.buffer != null && 
						picThread.sk.utime > last_draw_utime){
					try{	    
						Bitmap bitmap = null;
						bitmap = BitmapFactory.decodeByteArray(picThread.sk.buffer, 0, picThread.sk.buffer.length, opt);
						//updating the GUI 
			    	    if(bitmap !=null && dv != null){	    	    	
			    	    	dv.SetBitmap(bitmap);
			    	    
			    	    	dv.invalidate();
			    	    	picThread.sk.new_image = false;		    	    	
			    	    }
					}
					catch(java.lang.OutOfMemoryError e){
				        	Runtime.getRuntime().freeMemory();
				        	Log.e("err", "Out of Memeory");
				    }
		    	    
		    	    
				}
				if(textview !=null){
	    	    	textview.setText("New Image Received : " + picThread.sk.utime/ 1.0e6);
	    	    }
				last_draw_utime = picThread.sk.utime;
			}
			else if(textview !=null){
				textview.setText("Not Conneted");
			}
			if(clickedButton !=null && buttonView !=null){
					buttonView.setText(clickedButton);
			}
			//set text on buttons and stuff
			mHandler.postDelayed(this,100);			
		}		
	}

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        myApp = (EnvoyApp) getApplication();
        //EnvoyTabActivity.clThread = new ClientThread(this);
        //EnvoyTabActivity.clThread.start();
        
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        
        int s_width = metrics.widthPixels;
        int s_height = metrics.heightPixels;        
        
        
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        
        LinearLayout hl = new LinearLayout(this);
        hl.setOrientation(LinearLayout.HORIZONTAL);
        ll.addView(hl);
        
        LinearLayout vl_buttons = new LinearLayout(this);
        vl_buttons.setOrientation(LinearLayout.HORIZONTAL);
        
        LinearLayout vl_buttons1 = new LinearLayout(this);
        vl_buttons1.setOrientation(LinearLayout.HORIZONTAL);
        
        //GridLayout gl = new GridView(this);
      //AttributeSet at = new AttributeSet();
        LayoutParams layoutParams =
        		new LayoutParams((int) (s_width -300), s_height - 150);
                
        dv = new DrawView(this, myApp);//.getApplicationContext());//(//new DrawView(this);//(DrawView) findViewById(R.layout.);//new DrawView(this);
        dv.setLayoutParams(layoutParams);
        
        LayoutParams moveButtonParams =
        		new LayoutParams((int) (50), s_height - 150);
        
        ImageButton moveRight = new ImageButton(this);
        moveRight.setLayoutParams(moveButtonParams);
        moveRight.setOnClickListener(new Button.OnClickListener() { public void onClick (View v){ rightButtonClicked();}});
        
        
        ImageButton moveLeft = new ImageButton(this);
        moveLeft.setLayoutParams(moveButtonParams);
        
        moveLeft.setOnClickListener(new Button.OnClickListener() { public void onClick (View v){ leftButtonClicked();}});
        
        hl.addView(moveLeft);
        hl.addView(dv);
        hl.addView(moveRight);
        
        LayoutParams btnParams = 
        		new LayoutParams(180,LayoutParams.WRAP_CONTENT);
        
        LayoutParams largeBtnParams = 
        		new LayoutParams(180, LayoutParams.WRAP_CONTENT);
        		//new LayoutParams(150, LayoutParams.WRAP_CONTENT);
        
        clear = new Button(this);
        //clear.setImageResource(R.drawable.eraser_sized);
        clear.setText("Clear");
        clear.setLayoutParams(btnParams);
        clear.setOnClickListener(new Button.OnClickListener() { public void onClick (View v){ clearPoints(); }});
        
        
        send = new Button(this);
        send.setText("Send");        
        //send.setImageResource(R.drawable.email_scaled);
        send.setLayoutParams(btnParams);
        send.setOnClickListener(new Button.OnClickListener() { public void onClick (View v){ sendTag();}});
                
        Spinner spinnerCam = new Spinner(this);
        ArrayAdapter<String> adapterCam = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, myApp.channels);
        adapterCam.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        
        spinnerCam.setAdapter(adapterCam);
                
        int saved_id_cam = 0;
        for(int i = 0; i< myApp.channels.length; i++){
        	if(myApp.channels[i].equalsIgnoreCase(myApp.imageSource)){
        		saved_id_cam = i;
        		break;
        	}        	
        }
        
        myApp.deviceUpdated.set(true);
        spinnerCam.setSelection(saved_id_cam);
        spinnerCam.setOnItemSelectedListener(new OnCamSelectedListener());
        	
                
        tag = new Button(this);
        //tag.setImageResource(R.drawable.pencil_scaled);
        tag.setText("Draw");
        tag.setLayoutParams(btnParams);
        tag.setOnClickListener(new Button.OnClickListener() { public void onClick (View v){ startTagging();}});
        
        start = new Button(this);
        //start.setImageResource(R.drawable.globe_scaled);
        start.setText("Connect");
        start.setLayoutParams(btnParams);
        start.setOnClickListener(new Button.OnClickListener() { public void onClick (View v){ startConnection();}});
        
        move = new Button(this);
        move.setText("Move Head");
        move.setLayoutParams(largeBtnParams);
        move.setOnClickListener(new Button.OnClickListener() { public void onClick (View v){ moveHead();}});
        
        LinearLayout bl = new LinearLayout(this);
        bl.setOrientation(LinearLayout.VERTICAL);
        
        
     
        //bl.addView(tag);
        //bl.addView(clear);
        //vl_buttons.addView(start);
        //vl_buttons.addView(send);
        //vl_buttons1.addView(tag);
        //vl_buttons1.addView(clear);
        
        
        //hl.addView(vl_buttons);
        //bl.addView(vl_buttons);
        //bl.addView(vl_buttons1);
        
        
        String[] objectTypes = new String[] {"Door", "Microwave", "Fridge", "TV", "Door handle", "Computer", "Chair", "Water fountain", 
        						"Staircase", "Monitor", "Chair", "Table", "Cup"};
        spinner = new Spinner(this);//(Spinner) findViewById(R.id.mySpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, objectTypes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new MyOnItemSelectedListener());
        TextView spinnerText = new MyTextView(this);//TextView(this);
        
        spinnerText.setText("Tag Type");
        spinnerText.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        spinnerText.setTextSize(17);
                
        TextView serverText = new MyTextView(this);
        serverText.setText("Server");
        serverText.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        serverText.setTextSize(17);
        
        bl.addView(serverText);
        bl.addView(start);
        bl.addView(spinnerCam);
        bl.addView(spinnerText);
        bl.addView(spinner);
        bl.addView(send);
        bl.addView(tag);
        bl.addView(clear);
        
        /*TextView headText = new MyTextView(this);
        headText.setText("Head");
        serverText.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        serverText.setTextSize(17);
        bl.addView(move);*/
        hl.addView(bl);
        
        Button speakButton = new Button(this);
        speakButton.setText("Speak");
        speakButton.setLayoutParams(largeBtnParams);
        speakButton.setOnClickListener(new Button.OnClickListener() { public void onClick (View v){ speakButtonClicked();}});

        LayoutParams wordLayout= 
        		new LayoutParams(90, 90);
        
        
        /*wordsList = new ListView(this);//(ListView) findViewById(R.id.list);
        wordsList.setLayoutParams(wordLayout);
        
        PackageManager pm = getPackageManager();
        List<ResolveInfo> activities = pm.queryIntentActivities(
                new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
        if (activities.size() == 0)
        {
            speakButton.setEnabled(false);
            speakButton.setText("Recognizer not present");
        }*/
        
        speechResult = new TextView(this);
        speechResult.setText("Speech Reults");
        
        
        bl.addView(speakButton);
        bl.addView(speechResult);
        //bl.addView(wordsList);
        
        textview = new TextView(this);
        textview.setText("Server result");
        ll.addView(textview);
        
        buttonView = new TextView(this);
        buttonView.setText("Button Clicked");
        ll.addView(buttonView);
        
        //picThread = EnvoyTabActivity.clThread;
        
        Updater mUpdateTimeTask = new Updater(this);
        
        Thread t = new Thread(mUpdateTimeTask);
        t.start();   
        setContentView(ll);
    }
    
    public class OnCamSelectedListener implements OnItemSelectedListener {
        
        public void onItemSelected(AdapterView<?> parent,
            View view, int pos, long id) {
          
          SharedPreferences settings = getSharedPreferences(myApp.SERVER_IP_STRING, 0);
          SharedPreferences.Editor editor = settings.edit();          
          editor.putString("DeviceType", parent.getItemAtPosition(pos).toString());
          editor.commit();
          synchronized (myApp.imageSource) {
        	  if(!myApp.imageSource.equalsIgnoreCase(parent.getItemAtPosition(pos).toString())){
        		  myApp.imageSource = parent.getItemAtPosition(pos).toString();
        		  myApp.deviceUpdated.set(true);
        	  }
          }
        }

        public void onNothingSelected(AdapterView parent) {
          // Do nothing.
        }
    }
    
    /**
     * Handle the action of the button being clicked
     */
    public void speakButtonClicked()
    {
        startVoiceRecognitionActivity();
    }
    
    public void rightButtonClicked()
    {
    	myApp.dir.utime = Utils.utime_now();
    	myApp.dir.direction = DIR.RIGHT;
    	myApp.dir.sent = false;
    	myApp.dir.level = myApp.headLevel; 
    }
 
    public void leftButtonClicked()
    {
    	myApp.dir.utime = Utils.utime_now();
    	myApp.dir.direction = DIR.LEFT;
    	myApp.dir.sent = false;
    	myApp.dir.level = myApp.headLevel; 
    }
    /**
     * Fire an intent to start the voice recognition activity.
     */
    private void startVoiceRecognitionActivity()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Voice recognition Demo...");
        startActivityForResult(intent, REQUEST_CODE);
    }
 
    /**
     * Handle the results from the voice recognition activity.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
    	if (requestCode == REQUEST_CODE && resultCode == RESULT_OK)
        {
            // Populate the wordsList with the String values the recognition engine thought it heard
            ArrayList<String> matches = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
            
            if(matches.size()>=1){
            	speechResult.setText(matches.get(0));
            }
            /*wordsList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                    matches));*/
        }
        /*if (requestCode == REQUEST_CODE && resultCode == RESULT_OK)
        {
            // Populate the wordsList with the String values the recognition engine thought it heard
            ArrayList<String> matches = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
            wordsList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                    matches));
        }*/
        super.onActivityResult(requestCode, resultCode, data);
    }
    
    void startConnection(){
    	if(myApp.started.get()){
    		start.setText("Connect");
    		myApp.started.set(false);
    	}
    	else{
    		myApp.started.set(true);
    		start.setText("Connected");
    		
    	}
    	/*if(myApp.clThread == null || myApp.clThread.connected ==false){ 
    		myApp.clThread =	new SocketCreator(myApp);		
    		myApp.clThread.started.set(true);
    		myApp.clThread.start();
			Toast.makeText(this, "Connecting to Server " , Toast.LENGTH_LONG).show();
    	}*/
    }
    
    void clearPoints(){
    	//clear track 
    	Toast.makeText(this, "Cleared", Toast.LENGTH_SHORT).show();
    	clickedButton = "Clear";
    	//clear.setText("Clearing");
    	dv.clearPoints();
    }
    
    void startTagging(){
    	//clear track 
    	if(dv.allowDrawing){
    		tag.setText("Draw");
    		//drawing = false;
    		dv.allowDrawing = false;
    		Toast.makeText(this, "Done drawing", Toast.LENGTH_SHORT).show();
    		//tag.setText("Start Tagging");
    		//PorterDuffColorFilter greenFilter = new PorterDuffColorFilter(Color.GREEN, PorterDuff.Mode.DARKEN);			
    		//tag.setColorFilter(greenFilter);
    		myApp.stopBufferUpdate.set(false);
    		dv.clearPoints();
    	}
    	else{
    		myApp.stopBufferUpdate.set(true);
    		if(myApp.clThread != null && myApp.clThread.sk != null){
    			myApp.pList.image_utime =myApp.clThread.sk.utime;
    		}
    		else{
    			myApp.pList.image_utime = 0;
    		}
    					
    		tag.setText("Drawing");
    		dv.clearPoints();
    		dv.setRedStroke();
    		Toast.makeText(this, "Draw outline", Toast.LENGTH_SHORT).show();
    		dv.allowDrawing = true;
    		//PorterDuffColorFilter greenFilter = new PorterDuffColorFilter(Color.GREEN, PorterDuff.Mode.LIGHTEN);			
    		//tag.setColorFilter(greenFilter);
    		
    		//tag.setText("Tagging");
    		//drawing = true;
    	}
    }
    
    void sendTag(){
    	//Toast.makeText(this.getParent(), "Sent Tag", Toast.LENGTH_LONG);
    	//send back the tagged points to the server  
    	Toast.makeText(this, "Sent to server", Toast.LENGTH_SHORT).show();
    	clickedButton = "Send";
    	dv.sendPoints();
    	dv.allowDrawing = false;
    	//start the buffer update again
    	myApp.stopBufferUpdate.set(false);
    	
    	//tpList = dv.getPoints();    
    	dv.cleanPoints();
    	//dv.pList.type = spi//.getItemAtPosition(pos).toString()
    	myApp.pList.type = spinner.getSelectedItem().toString();
    	//*********Need to set this utime to the one in which we started to draw 
    	//dv.pList.image_utime = 0;
    	Toast.makeText(this, "Sending Item: " + myApp.pList.type , Toast.LENGTH_SHORT).show();
    	myApp.valid_data.set(true);
    	//send the points to the server
    }
    
    void moveHead(){
    	if(dv.moveHead){
    		dv.moveHead = false;
    		move.setText("Move Head");
    	}
    	else{
    		move.setText("Stop Head");
    		dv.moveHead = true;
    	}
    }
    
    public class MyOnItemSelectedListener implements OnItemSelectedListener {
        
        public void onItemSelected(AdapterView<?> parent,
            View view, int pos, long id) {
          Toast.makeText(parent.getContext(), "Item : " +
              parent.getItemAtPosition(pos).toString(), Toast.LENGTH_SHORT).show();
         
          //save stuff on to disk - to load 
          /*SharedPreferences settings = getSharedPreferences(EnvoyTabActivity.SERVER_IP_STRING, 0);
          SharedPreferences.Editor editor = settings.edit();          
          editor.putString("serverIP", parent.getItemAtPosition(pos).toString());
          editor.commit();*/
        }

        public void onNothingSelected(AdapterView parent) {
          // Do nothing.
        }
    }
}