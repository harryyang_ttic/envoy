#ifndef __dp_planner_hpp__
#define __dp_planner_hpp__

#include <vector>
#include <occ_map/VoxelMap.hpp>
#include <occ_map/PixelMap.hpp>

namespace dp_planner {

#define DPP_UNK_THRESH .01
#define DPP_OCC_THRESH .1

class Waypoint {
public:
  Waypoint(double x = 0, double y = 0, double z = 0, double theta = 0) :
      x(x), y(y), z(z), theta(theta)
  {
  }
  Waypoint(const double xyzt[4]) :
      x(xyzt[0]), y(xyzt[1]), z(xyzt[2]), theta(xyzt[3])
  {
  }
  double x;
  double y;
  double z;
  double theta;
};

occ_map::FloatVoxelMap * createUtilityMap(const occ_map::FloatVoxelMap * voxmap, const double start[3]);

bool
dpPathToGoal(const occ_map::FloatVoxelMap * voxmap, const double start_xyzt[4], const double goal_xyzt[4],
    std::vector<Waypoint>& waypoints);

/**
 * 2D Pixel Map DP Planner
 */
class Waypoint2d {
public:
  Waypoint2d(double x = 0, double y = 0, double theta = 0) :
      x(x), y(y), theta(theta)
  {
  }
  Waypoint2d(const double xyt[3]) :
      x(xyt[0]), y(xyt[1]), theta(xyt[2])
  {
  }
  double x;
  double y;
  double theta;
};

occ_map::FloatPixelMap * createUtilityMap(const occ_map::FloatPixelMap *pixmap, const double start[2]);
occ_map::FloatPixelMap * dilateMap(const occ_map::FloatPixelMap *pixmap, double dilation_size);
void dilateMap(const occ_map::FloatPixelMap *pixmap, double dilation_size,
    occ_map::FloatPixelMap * pixmap_dilated_to_update,
    const int active_region_ixy0[2] = NULL, const int active_region_ixy1[2] = NULL);
static inline void dilateMap(const occ_map::FloatPixelMap *pixmap, double dilation_size,
    occ_map::FloatPixelMap * pixmap_dilated_to_update,
    const double active_region_xy0[2], const double active_region_xy1[2])
{
  int active_region_ixy0[2];
  int active_region_ixy1[2];
  pixmap->worldToTable(active_region_xy0, active_region_ixy0);
  pixmap->worldToTable(active_region_xy1, active_region_ixy1);
  dilateMap(pixmap, dilation_size, pixmap_dilated_to_update, active_region_ixy0, active_region_ixy1);
}
bool dpPathToGoal(const occ_map::FloatPixelMap *pixmap, const double start_xyt[3], const double goal_xyt[3],
    std::vector<Waypoint2d>& waypoints);

}

#endif
