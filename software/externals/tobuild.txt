# list of subdirectories to build, one one each line.  Empty lines
# and lines starting with '#' are ignored
#lcm
#libbot2
#Eigen_pod
#opencv-latest-pod
apriltags
scanmatch
octomap
occ-map
common_utils
isam
opencv-utils
camunits
libsvm
#qhull_09-pod
ann
#lear_gist
jpeg-utils
dp-planner
visualization
pcl-dep
pcl-latest-pod
#mrpt-pod
visualization
kdl
