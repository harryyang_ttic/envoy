file(GLOB cpp_files *.cpp)
file(GLOB hpp_files *.hpp)
add_library(opencv-utils SHARED ${cpp_files})

# set the library API version.  Increment this every time the public API
# changes.
set_target_properties(opencv-utils PROPERTIES SOVERSION 1)

pods_use_pkg_config_packages(opencv-utils 
    bot2-core opencv)

target_link_libraries(opencv-utils
    pthread)

# make the shared library public
pods_install_libraries(opencv-utils)
#make the header public
pods_install_headers(${hpp_files} DESTINATION opencv_utils)

pods_install_pkg_config_file(opencv-utils
    LIBS -lopencv-utils -lpthread
    REQUIRES bot2-core opencv
    VERSION 0.0.1)
