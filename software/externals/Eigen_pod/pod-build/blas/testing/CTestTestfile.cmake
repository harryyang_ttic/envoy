# CMake generated Testfile for 
# Source directory: /home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing
# Build directory: /home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/pod-build/blas/testing
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(sblat1 "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/runblastest.sh" "sblat1" "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/sblat1.dat")
ADD_TEST(sblat2 "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/runblastest.sh" "sblat2" "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/sblat2.dat")
ADD_TEST(sblat3 "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/runblastest.sh" "sblat3" "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/sblat3.dat")
ADD_TEST(dblat1 "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/runblastest.sh" "dblat1" "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/dblat1.dat")
ADD_TEST(dblat2 "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/runblastest.sh" "dblat2" "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/dblat2.dat")
ADD_TEST(dblat3 "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/runblastest.sh" "dblat3" "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/dblat3.dat")
ADD_TEST(cblat1 "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/runblastest.sh" "cblat1" "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/cblat1.dat")
ADD_TEST(cblat2 "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/runblastest.sh" "cblat2" "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/cblat2.dat")
ADD_TEST(cblat3 "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/runblastest.sh" "cblat3" "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/cblat3.dat")
ADD_TEST(zblat1 "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/runblastest.sh" "zblat1" "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/zblat1.dat")
ADD_TEST(zblat2 "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/runblastest.sh" "zblat2" "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/zblat2.dat")
ADD_TEST(zblat3 "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/runblastest.sh" "zblat3" "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/blas/testing/zblat3.dat")
