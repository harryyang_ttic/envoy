# Install script for directory: /home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/harry/Documents/Robotics/envoy/software/build")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/AdolcForward;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/BVH;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/IterativeSolvers;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/MatrixFunctions;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/MoreVectorization;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/AutoDiff;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/AlignedVector3;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/Polynomials;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/FFT;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/NonLinearOptimization;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/SparseExtra;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/IterativeSolvers;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/NumericalDiff;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/Skyline;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/MPRealSupport;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/OpenGLSupport;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/KroneckerProduct;/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen/Splines")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/envoy/software/build/include/eigen3/unsupported/Eigen" TYPE FILE FILES
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/AdolcForward"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/BVH"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/IterativeSolvers"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/MatrixFunctions"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/MoreVectorization"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/AutoDiff"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/AlignedVector3"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/Polynomials"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/FFT"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/NonLinearOptimization"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/SparseExtra"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/IterativeSolvers"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/NumericalDiff"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/Skyline"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/MPRealSupport"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/OpenGLSupport"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/KroneckerProduct"
    "/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/Splines"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/home/harry/Documents/Robotics/envoy/software/externals/Eigen_pod/pod-build/unsupported/Eigen/src/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

