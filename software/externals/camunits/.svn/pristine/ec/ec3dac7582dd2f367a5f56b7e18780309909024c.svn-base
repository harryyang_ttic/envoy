REPO := http://camunits.googlecode.com/svn/trunk/
CHECKOUT_DIR := camunits

all: $(CHECKOUT_DIR)/Makefile
	$(MAKE) -C $(CHECKOUT_DIR) install

# Default to a less-verbose build.  If you want all the gory compiler output,
# run "make VERBOSE=1"
$(VERBOSE).SILENT:

# Figure out where to build the software.
#   Use BUILD_PREFIX if it was passed in.
#   If not, search up to four parent directories for a 'build' directory.
#   Otherwise, use ./build.
ifeq "$(BUILD_PREFIX)" ""
BUILD_PREFIX:=$(shell for pfx in ./ .. ../.. ../../.. ../../../..; do d=`pwd`/$$pfx/build;\
               if [ -d $$d ]; then echo $$d; exit 0; fi; done; echo `pwd`/build)
endif
# create the build directory if needed, and normalize its path name
BUILD_PREFIX:=$(shell mkdir -p $(BUILD_PREFIX) && cd $(BUILD_PREFIX) && echo `pwd`)

# Default to a release build.  If you want to enable debugging flags, run
# "make BUILD_TYPE=Debug"
OPT_FLAGS := -O3
ifeq "$(BUILD_TYPE)" "Debug"
OPT_FLAGS = -g
endif

UNAME:=$(shell uname -s)
ifeq ($(UNAME),Darwin)
	LDFLAGS_LOCAL="-L$(BUILD_PREFIX)/lib $(LDFLAGS)"
else
	LDFLAGS_LOCAL="-L$(BUILD_PREFIX)/lib -Wl,-R$(BUILD_PREFIX)/lib $(LDFLAGS)"
endif

$(CHECKOUT_DIR)/Makefile:
	$(MAKE) configure

.PHONY: configure
configure: $(CHECKOUT_DIR)/bootstrap.sh
	@echo "\nChecking out code into $(CHECKOUT_DIR)\n\n"
	
	@echo "\nBUILD_PREFIX: $(BUILD_PREFIX)\n\n"

	# run autoconf to generate and configure the build scripts
	@cd $(CHECKOUT_DIR) && ./bootstrap.sh && \
		./configure --prefix=$(BUILD_PREFIX) \
		PKG_CONFIG_PATH=$(PKG_CONFIG_PATH):$(BUILD_PREFIX)/lib/pkgconfig \
		CFLAGS="-I$(BUILD_PREFIX)/include $(OPT_FLAGS) $(CFLAGS)" \
		CXXFLAGS="-I$(BUILD_PREFIX)/include $(OPT_FLAGS) $(CXXFLAGS)" \
		LDFLAGS=$(LDFLAGS_LOCAL)
		
$(CHECKOUT_DIR)/bootstrap.sh:
	@svn checkout $(REPO) $(CHECKOUT_DIR)
	

clean:
	-if [ -e $(CHECKOUT_DIR)/Makefile ]; then $(MAKE) -C $(CHECKOUT_DIR) clean uninstall; fi
	-if [ -e $(CHECKOUT_DIR)/Makefile ]; then $(MAKE) -C $(CHECKOUT_DIR) distclean; fi

# other (custom) targets are passed through to the cmake-generated Makefile 
%::
	$(MAKE) -C pod-build $@
