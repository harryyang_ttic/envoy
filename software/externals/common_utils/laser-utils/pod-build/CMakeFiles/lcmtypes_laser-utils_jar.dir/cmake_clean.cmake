FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/cpp"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/lcmtypes_laser-utils_jar"
  "lcmtypes_laser-utils.jar"
  "../lcmtypes/java/laser/raw_t.class"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/lcmtypes_laser-utils_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
