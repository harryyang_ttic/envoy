# Install script for directory: /home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/src

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/harry/Documents/Robotics/envoy/software/build")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen_utils" TYPE FILE FILES
    "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/src/eigen_numerical.hpp"
    "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/src/eigen_lcmgl.hpp"
    "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/src/eigen_rand.hpp"
    "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/src/eigen_utils.hpp"
    "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/src/eigen_select_block.hpp"
    "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/src/eigen_rigidbody.hpp"
    "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/src/eigen_lcm.hpp"
    "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/src/eigen_gl.hpp"
    "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/src/eigen_lcm_matlab_rpc.hpp"
    "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/src/eigen_file_io.hpp"
    "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/src/eigen_utils_common.hpp"
    "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/src/eigen_select_block.hxx"
    "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/src/eigen_lcm.hxx"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FOREACH(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libeigen-utils.so.1"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libeigen-utils.so"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      FILE(RPATH_CHECK
           FILE "${file}"
           RPATH "/home/harry/Documents/Robotics/envoy/software/build/lib")
    ENDIF()
  ENDFOREACH()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/pod-build/lib/libeigen-utils.so.1"
    "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/pod-build/lib/libeigen-utils.so"
    )
  FOREACH(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libeigen-utils.so.1"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libeigen-utils.so"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      FILE(RPATH_CHANGE
           FILE "${file}"
           OLD_RPATH "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/pod-build/lib:/home/harry/Documents/Robotics/envoy/software/build/lib:"
           NEW_RPATH "/home/harry/Documents/Robotics/envoy/software/build/lib")
      IF(CMAKE_INSTALL_DO_STRIP)
        EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "${file}")
      ENDIF(CMAKE_INSTALL_DO_STRIP)
    ENDIF()
  ENDFOREACH()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/pod-build/lib/pkgconfig/eigen-utils.pc")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/pod-build/src/eigen-fftw/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/envoy/software/externals/common_utils/eigen-utils/pod-build/src/test-eigen/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

