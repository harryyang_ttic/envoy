FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/cpp"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/lcmtypes_eigen-utils_jar"
  "lcmtypes_eigen-utils.jar"
  "../lcmtypes/java/rigid_body/pose_t.class"
  "../lcmtypes/java/eigen_utils/matlab_rpc_ack_t.class"
  "../lcmtypes/java/eigen_utils/eigen_dense_t.class"
  "../lcmtypes/java/eigen_utils/matlab_rpc_return_t.class"
  "../lcmtypes/java/eigen_utils/matlab_rpc_command_t.class"
  "../lcmtypes/java/eigen_utils/eigen_matrixxd_t.class"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/lcmtypes_eigen-utils_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
