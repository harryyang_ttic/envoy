FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/cpp"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/lcmtypes_occ-map_jar"
  "lcmtypes_occ-map.jar"
  "../lcmtypes/java/occ_map/pixel_map_t.class"
  "../lcmtypes/java/occ_map/voxel_map_t.class"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/lcmtypes_occ-map_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
