FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/cpp"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/lcmtypes_scanmatch_jar"
  "lcmtypes_scanmatch.jar"
  "../lcmtypes/java/sm/planar_lidar_t.class"
  "../lcmtypes/java/sm/pose_t.class"
  "../lcmtypes/java/sm/rigid_transform_2d_t.class"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/lcmtypes_scanmatch_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
