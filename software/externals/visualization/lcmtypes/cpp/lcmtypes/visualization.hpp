#ifndef __lcmtypes_visualization_hpp__
#define __lcmtypes_visualization_hpp__

#include "vs/pallet_enum_t.hpp"
#include "vs/text_collection_t.hpp"
#include "vs/link_collection_t.hpp"
#include "vs/text_t.hpp"
#include "vs/cov_collection_t.hpp"
#include "vs/point3d_t.hpp"
#include "vs/link_t.hpp"
#include "vs/color_t.hpp"
#include "vs/obj_t.hpp"
#include "vs/cov_t.hpp"
#include "vs/reset_collections_t.hpp"
#include "vs/obj_collection_t.hpp"
#include "vs/collection_config_t.hpp"
#include "vs/pallet_slot_pair_t.hpp"
#include "vs/system_status_t.hpp"
#include "vs/localize_reinitialize_cmd_t.hpp"
#include "vs/point3d_list_collection_t.hpp"
#include "vs/pallet_slot_t.hpp"
#include "vs/pallet_list_t.hpp"
#include "vs/point3d_list_t.hpp"
#include "vs/property_t.hpp"
#include "vs/pallet_t.hpp"

#endif
