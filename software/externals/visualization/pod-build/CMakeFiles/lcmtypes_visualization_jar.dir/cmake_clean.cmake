FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/cpp"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/lcmtypes_visualization_jar"
  "lcmtypes_visualization.jar"
  "../lcmtypes/java/vs/link_t.class"
  "../lcmtypes/java/vs/point3d_list_collection_t.class"
  "../lcmtypes/java/vs/reset_collections_t.class"
  "../lcmtypes/java/vs/pallet_t.class"
  "../lcmtypes/java/vs/cov_collection_t.class"
  "../lcmtypes/java/vs/pallet_slot_pair_t.class"
  "../lcmtypes/java/vs/localize_reinitialize_cmd_t.class"
  "../lcmtypes/java/vs/link_collection_t.class"
  "../lcmtypes/java/vs/obj_collection_t.class"
  "../lcmtypes/java/vs/system_status_t.class"
  "../lcmtypes/java/vs/point3d_list_t.class"
  "../lcmtypes/java/vs/pallet_slot_t.class"
  "../lcmtypes/java/vs/pallet_enum_t.class"
  "../lcmtypes/java/vs/text_t.class"
  "../lcmtypes/java/vs/text_collection_t.class"
  "../lcmtypes/java/vs/cov_t.class"
  "../lcmtypes/java/vs/point3d_t.class"
  "../lcmtypes/java/vs/pallet_list_t.class"
  "../lcmtypes/java/vs/obj_t.class"
  "../lcmtypes/java/vs/property_t.class"
  "../lcmtypes/java/vs/collection_config_t.class"
  "../lcmtypes/java/vs/color_t.class"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/lcmtypes_visualization_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
