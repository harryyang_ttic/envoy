# CMake generated Testfile for 
# Source directory: /home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/src/testing
# Build directory: /home/harry/Documents/Robotics/envoy/software/externals/octomap/pod-build/src/testing
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(MathVector "/home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/bin/unit_tests" "MathVector")
ADD_TEST(MathPose "/home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/bin/unit_tests" "MathPose")
ADD_TEST(InsertRay "/home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/bin/unit_tests" "InsertRay")
ADD_TEST(ReadTree "/home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/bin/unit_tests" "ReadTree")
ADD_TEST(CastRay "/home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/bin/unit_tests" "CastRay")
ADD_TEST(InsertScan "/home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/bin/unit_tests" "InsertScan")
ADD_TEST(ReadGraph "/home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/bin/unit_tests" "ReadGraph")
ADD_TEST(StampedTree "/home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/bin/unit_tests" "StampedTree")
ADD_TEST(OcTreeKey "/home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/bin/unit_tests" "OcTreeKey")
ADD_TEST(OcTreeIterator "/home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/bin/unit_tests" "OcTreeIterator")
ADD_TEST(test_pruning "/home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/bin/test_pruning")
ADD_TEST(test_iterators "/home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/bin/test_iterators" "/home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/src/testing/geb079.bt")
ADD_TEST(test_mapcollection "/home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/bin/test_mapcollection" "/home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/src/testing/mapcoll.txt")
ADD_TEST(test_color_tree "/home/harry/Documents/Robotics/envoy/software/externals/octomap/octomap-v1.4.2/octomap/bin/test_color_tree")
