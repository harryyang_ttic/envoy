function [x] = agile_matlab_common()
%
% AGILE_MATLAB_COMMON Empty function used to test whether Agile's common
% directory is in Matlab's path.
%   [X] = AGILE_MATLAB_COMMON() Return value X is always true(1,1).

x = true(1,1);