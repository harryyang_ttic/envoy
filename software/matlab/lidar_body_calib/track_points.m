% function points = track_points(raw,max_dist,max_hits)
% Tracks positions of previously extracted points over time.
% raw: cell array, where each cell contains the positions of extracted
%   points from a single scan
% max_dist: maximum distance allowed between previous and current point
%   position to be considered a match
% max_misses: maximum number of consecutive scans before an unobserved
%   point is considered lost
% max_hits: maximum number of hits to keep in a particular track 
% tracks: matrix containing all track data, with four columns:
%   [scan#, trackid#, x, y]
% num_points: vector containing the number of observations in each track
function tracks = track_points(raw,max_dist,max_misses,max_hits)

% Initialize lists
cur_points = zeros(0,5);    % Currently tracked points
match_list = {};            % Growing list of matches

% Loop over each scan
current_id = 1;
for i = 1:numel(raw)
    % Match the currently tracked points with the current scan
    inds = match_point_set_pair(cur_points, raw{i},max_dist);

    % Find indices of actual matches
    good_inds = inds(inds>0);

    % Append valid matches to list
    match_list{i} = [i*ones(numel(good_inds),1),cur_points(good_inds,3),raw{i}(inds>0,:)];

    % Find indices of non-matched points
    bad_map = true(size(cur_points,1),1);
    bad_map(good_inds) = false;
    bad_inds = find(bad_map);

    % Bump up their misses (they were not seen this scan)
    cur_points(bad_inds,4) = cur_points(bad_inds,4)+1;

    % Reset misses of good matches
    cur_points(good_inds,4) = 0;

    % Bump up hits of good matches
    cur_points(good_inds,5) = cur_points(good_inds,5)+1;
    
    % Update last known locations of tracked points
    cur_points(good_inds,1:2) = raw{i}(inds>0,:);

    % Append new points (ones from scan that did not match anything)
    new_point_ids = current_id:current_id+sum(inds==0)-1;
    current_id = current_id+sum(inds==0);
    cur_points = [cur_points; [raw{i}(inds==0,:) ,new_point_ids', zeros(numel(new_point_ids),2)]];

    % If any misses or hits exceed max, remove them
    cur_points(cur_points(:,4)>max_misses | cur_points(:,5)>max_hits,:) = [];
end

% put all observations into a single matrix
total_num = 0;
for i = 1:numel(match_list)
    total_num = total_num + size(match_list{i},1);
end
tracks.data = zeros(total_num,4);
cur = 1;
for i = 1:numel(match_list)
    tracks.data(cur:cur+size(match_list{i},1)-1,:) = match_list{i};
    cur = cur+size(match_list{i},1);
end

% sort the matrix and determine the number of points in each track
tracks.data = tracks.data(:,[2,1,3,4]);
tracks.data = sortrows(tracks.data,[1,2]);
dx = diff(tracks.data(:,1));
starts = [1;find(dx~=0)+1];
ends = [starts(2:end)-1;numel(dx)+1];
tracks.data = tracks.data(:,[3,4,2]);
tracks.num_points = ends-starts+1;

