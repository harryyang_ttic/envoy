function [R,T,e,pt_mean,pt_cov] = optimize_yaw_xy_from_tracks(tracks, poses, R_init, T_init, R_fixed, T_fixed, verbose, should_optimize)

% set up initial parameters
if (nargin<4)
    R_init = eye(3);
    T_init = [0;0;0];
end

params_ = [rot2rpy(R_init);T_init(:)];
params = [params_(3); params_(4:5)]; % yaw, x, y to be optimized

fixed_params_ = [rot2rpy(R_fixed); T_fixed];
fixed_params = [fixed_params_(1:2); fixed_params_(6)]; % roll,
                                                        % pitch, z fixed

if (nargin<5)
    verbose = true;
end

if (nargin<6)
    should_optimize = true;
end

% set up poses
P_body = zeros(3,4,numel(poses));
for i = 1:numel(poses)
    P_body(:,:,i) = [quat2rot(poses(i).orientation),poses(i).position(:)];
end
P_body = reshape(shiftdim(P_body,2),[size(P_body,3),3,4]);


% set up optimization parameters
if (verbose)
    opts = optimset('display','iter','maxfunevals',1e6);
else
    opts = optimset('display','off','maxfunevals',1e6);
end

% do optimization
if (should_optimize)
    params = lsqnonlin(@error_func_yaw_xy_tracks, params, [], [], opts, ...
                       fixed_params, tracks, P_body, false);
end

% extract final pose
R = rpy2rot([fixed_params(1:2); params(1)]);
T = [params(2:3);fixed_params(3)];

% get final error stats
[e,pt_mean,pt_cov] = error_func_yaw_xy_tracks(params, fixed_params, tracks, P_body, verbose);
