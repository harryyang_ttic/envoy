function [R,T,e,pt_mean,pt_cov] = optimize_pose_from_tracks(tracks, poses, R_init, T_init, R_fixed, T_fixed, verbose, should_optimize)

% set up initial parameters
if (nargin<4)
    R_init = eye(3);
    T_init = [0;0;0];
end
params = [rot2rpy(R_init);T_init(:)];

if (nargin<5)
    verbose = true;
end

if (nargin<6)
    should_optimize = true;
end

% set up poses
P_body = zeros(3,4,numel(poses));
for i = 1:numel(poses)
    P_body(:,:,i) = [quat2rot(poses(i).orientation),poses(i).position(:)];
end
P_body = reshape(shiftdim(P_body,2),[size(P_body,3),3,4]);


% set up optimization parameters
if (verbose)
    opts = optimset('display','iter','maxfunevals',1e6);
else
    opts = optimset('display','off','maxfunevals',1e6);
end

% do optimization
if (should_optimize)
    params = lsqnonlin(@error_func_tracks, params, [], [], opts, ...
                       tracks, P_body, false);
end

% extract final pose
R = rpy2rot(params(1:3));
T = params(4:6);

% get final error stats
[e,pt_mean,pt_cov] = error_func_tracks(params, tracks, P_body, verbose);
