function e = error_func_patches(params,patch_dat,P,point_counts)

% grab rigid sensor-to-body transform from parameter vector
R = rpy2rot(params(1:3));
T = params(4:6);
P_sb = [R,T(:);0,0,0,1];

e = zeros(sum(point_counts),1);

pts = patch_dat(:,3:4);
pts = [P_sb(1,1)*pts(:,1) + P_sb(1,2)*pts(:,2) + P_sb(1,3),...
    P_sb(2,1)*pts(:,1) + P_sb(2,2)*pts(:,2) + P_sb(2,3),...
    P_sb(3,1)*pts(:,1) + P_sb(3,2)*pts(:,2) + P_sb(3,3)];

pts = [pts(:,1).*P.p11 + pts(:,2).*P.p12 + pts(:,3).*P.p13 + P.p14,...
    pts(:,1).*P.p21 + pts(:,2).*P.p22 + pts(:,3).*P.p23 + P.p24,...
    pts(:,1).*P.p31 + pts(:,2).*P.p32 + pts(:,3).*P.p33 + P.p34];

diffs = diff(patch_dat(:,1));
starts = [1;find(diffs>0)+1];
ends = [starts(2:end)-1;size(patch_dat,1)];

% loop over patches
ptr = 1;
for i = 1:numel(starts)

    % grab points for this patch
    patch_pts = pts(starts(i):ends(i),:);
    
    % find mean location of this feature in world coords
    avg = mean(patch_pts,1);
    
    % find plane
    p = [patch_pts(:,1)-avg(1),patch_pts(:,2)-avg(2),patch_pts(:,3)-avg(3)];
    [u,s,v] = svd(p,0);

    % error = difference between each point and the mean
    diffs = p*v(:,3);

    % set appropriate entries in error vector
    e(ptr:ptr+numel(diffs)-1) = diffs;
    ptr = ptr+numel(diffs);
end
