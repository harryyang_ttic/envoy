function inds = match_point_set_pair(p1,p2,max_dist)

% Initialize output indices.
% There will be one index per point in second set.
inds = zeros(size(p2,1),1);

% If either of the two lists is empty, nothing can match
if (numel(p1)==0 || numel(p2)==0)
    return;
end

% Compute exhaustive list of distances between sets 1 and 2
dx = repmat(p1(:,1),[1,size(p2,1)])-repmat(p2(:,1)',[size(p1,1),1]);
dy = repmat(p1(:,2),[1,size(p2,1)])-repmat(p2(:,2)',[size(p1,1),1]);
dists = dx.^2+dy.^2;

% Find best match for each point in set 2
[minvals1,minidx1] = min(dists,[],1);

% Find best match for each point in set 1
[minvals2,minidx2] = min(dists',[],1);

% Valid matches are ones that agree (closest in set 1 and set 2)
% Also the match distance must be less than the specified threshold
good_ind = minvals1<max_dist^2 & minidx2(minidx1)==(1:numel(minidx1));

% Set the non-zero (valid) indices
inds(good_ind) = minidx1(good_ind);
