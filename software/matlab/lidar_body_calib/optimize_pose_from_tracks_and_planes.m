function [R,T] = optimize_pose_from_tracks_and_planes(tracks,patch_dat,poses,R_init,T_init)

% set up initial parameters
if (nargin<5)
    R_init = eye(3);
    T_init = [0;0;0];
end
init_params = [rot2rpy(R_init);T_init(:)];

% set up poses
P_body = zeros(4,4,numel(poses));
for i = 1:numel(poses)
    P_body(:,:,i) = [quat2rot(poses(i).orientation),poses(i).position(:);0,0,0,1];
end

% cache pose data
inds = patch_dat(:,2);
P.p11 = squeeze(P_body(1,1,inds));
P.p12 = squeeze(P_body(1,2,inds));
P.p13 = squeeze(P_body(1,3,inds));
P.p14 = squeeze(P_body(1,4,inds));
P.p21 = squeeze(P_body(2,1,inds));
P.p22 = squeeze(P_body(2,2,inds));
P.p23 = squeeze(P_body(2,3,inds));
P.p24 = squeeze(P_body(2,4,inds));
P.p31 = squeeze(P_body(3,1,inds));
P.p32 = squeeze(P_body(3,2,inds));
P.p33 = squeeze(P_body(3,3,inds));
P.p34 = squeeze(P_body(3,4,inds));


% cache tracks
trk = {};
for i = 1:size(tracks.v,2)
    ind = full(tracks.v(:,i)>0);
    x = full(tracks.x(ind,i));
    y = full(tracks.y(ind,i));
    trk{i} = [x,y,find(ind)];
end

% determine size of error vectors
e_size = zeros(2,1);
for i = 1:numel(trk)
    e_size(1) = e_size(1)+2*size(trk{i},1);
end
diffs = diff(patch_dat(:,1));
starts = [1;find(diffs>0)+1];
ends = [starts(2:end)-1;numel(diffs)+1];
point_counts = ends-starts+1;
e_size(2) = sum(point_counts);

% run optimization
opts = optimset('display','iter');
params = lsqnonlin(@error_func, init_params, [], [], opts, trk, patch_dat, P_body, P, e_size, point_counts, false);

% extract final pose
R = rpy2rot(params(1:3));
T = params(4:6);



function e = error_func(params, trk, patch_dat, P_body, P, error_size, point_counts, draw_it)

e1 = error_func_tracks(params, trk, P_body, error_size(1), draw_it);
e2 = error_func_patches(params, patch_dat, P, point_counts);

e = [e1;e2];
