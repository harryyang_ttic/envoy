% first column is the time index
% remaining columns are data
function out = split_observations_by_time(in, max_duration)

% form column containing object ids
new_column = [];
for i = 1:numel(in.num_points)
    new_column = [new_column; repmat(i,[in.num_points(i),1])];
end

% sort by object id, then by time index
out.data = sortrows([new_column,in.data], [1,2]);

% split objects by duration
totals = cumsum(in.num_points);
ends = totals;
starts = [1;ends(1:end-1)+1];
next_num = totals(end)+1;
for i = 1:numel(starts)
    ptr = starts(i);
    while (ptr <= ends(i))
        time_inds = in.data(ptr:ends(i),2);
        time_inds = time_inds-time_inds(1);
        last_ind = ptr + find(time_inds <= max_duration,1,'last')-1;
        out.data(ptr:last_ind,1) = next_num;
        next_num = next_num+1;
        ptr = last_ind+1;
    end
end

% reformat output data
out.data = sortrows(out.data,[1,2]);
diffs = diff(out.data(:,1));
starts = [1;find(diffs>0)+1];
ends = [starts(2:end)-1;numel(diffs)+1];
out.num_points = ends-starts+1;
out.data = out.data(:,2:end);
