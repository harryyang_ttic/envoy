% P is n x 4 x 4
function out = transform_sensor_to_local2(in,P,R,T)

P = P(in(:,4),:,:);

x = in(:,1);
y = in(:,2);
z = in(:,3);

px = x*R(1,1) + y*R(1,2) + z*R(1,3) + T(1);
py = x*R(2,1) + y*R(2,2) + z*R(2,3) + T(2);
pz = x*R(3,1) + y*R(3,2) + z*R(3,3) + T(3);

out = zeros(size(P,1),3);
out(:,1) = P(:,1,1).*px + P(:,1,2).*py + P(:,1,3).*pz + P(:,1,4);
out(:,2) = P(:,2,1).*px + P(:,2,2).*py + P(:,2,3).*pz + P(:,2,4);
out(:,3) = P(:,3,1).*px + P(:,3,2).*py + P(:,3,3).*pz + P(:,3,4);
