function ind = indices_from_ids(counts, ids)

ends = cumsum(counts);
starts = [1;ends(1:end-1)+1];
ind = [];
for i = 1:numel(ids)
    ind = [ind, starts(ids(i)):ends(ids(i))];
end
