function [R,T] = calib_single_lidar(scans, poses)

%% set some params

% post extraction
max_post_width = 0.2;          % width of post in scan profile (m)
min_post_range_delta = 0.1;    % range difference between post and bg (m)
min_post_range = 0.5;          % min allowed range from sensor to post (m)
max_post_range = 20;           % max allowed range from sensor to post (m)
max_invalid_range = 0.2;       % invalid ranges are from 0 to this (m)
max_actual_range = 80;         % invalid ranges are replaced with this (m)

% post tracking
max_post_track_dist = 0.5;     % distance between successive observations (m)
max_post_track_misses = 300;   % number of consecutive non-observations (scans)
max_post_track_hits = 600;     % observations of post (scans)  TODO: make this total lifetime rather than hits
min_post_track_length = 50;    % lifetime of post (scans)

% track optimization
max_post_track_std = 0.2;      % standard deviation of post track residual (m)
post_inlier_frac = 0.9;        % fraction of inliers to retain at each iteration

% % construction of scene and planar patches
% max_point_integration_range = 40;  % range to observed points in integrated scans (m)
% point_grid_size_posts = 0.5;       % linear size of each post grid cell (m)
% point_grid_size_patches = 2;       % linear size of each patch grid cell (m)
% min_init_patch_points = 500;       % number of points in planar patch
% min_final_patch_points = 500;      % number of points in planar patch
% max_height_std = 0.1;              % standard deviation of patch point z (m)
% max_patch_duration = 300;          % number of consecutive observations of a patch (scans)


%% detect posts
fprintf('Detecting posts...');
posts = {};
for i = 1:numel(scans)
    posts{i} = extract_posts_from_scan(scans(i).ranges,scans(i).thetas,...
        max_post_width, min_post_range_delta, ...
        [min_post_range, max_post_range], [max_invalid_range,max_actual_range]);
end
fprintf('done.\n');

%% track posts
fprintf('Tracking posts...');
tracks_orig = track_points(posts,max_post_track_dist,max_post_track_misses,max_post_track_hits);
fprintf('done.\n');

%% remove features with too few observations
bad_tracks = find(tracks_orig.num_points < min_post_track_length);
bad_ind = indices_from_ids(tracks_orig.num_points, bad_tracks);
tracks_orig.data(bad_ind,:) = [];
tracks_orig.num_points(bad_tracks) = [];
tracks = tracks_orig;

%% initial optimization of pose using only point tracks
R = eye(3);
T = [0;0;0];
for iter = 1:50
    [R,T,err,pt_mean,pt_cov] = optimize_pose_from_tracks(tracks,poses,R,T);
    rpy = rot2rpy(R);
    if(rpy(2)<0)
        rpy(2) = -rpy(2);
        R = rpy2rot(rpy);
    end
    
    evals = zeros(size(pt_cov,3),1);
    for i = 1:numel(evals)
        evals(i) = max(eig(pt_cov(:,:,i)));
    end
    if (sum(evals > max_post_track_std^2) == 0)
        break;
    end
    [evals,sort_ind] = sort(evals);
    bad_tracks = sort_ind(ceil(post_inlier_frac * numel(evals)):end);
    bad_ind = indices_from_ids(tracks.num_points, bad_tracks);
    tracks.data(bad_ind,:) = [];
    tracks.num_points(bad_tracks) = [];
end
R_tracks = R;
T_tracks = T;

return;

%% find clusters of scan points likely to be coplanar on ground surface

% integrate points according to approximate calibration
[all_pts,scan_inds,scan_pt_inds,sensor_pts] = ...
    integrate_point_cloud(scans,poses,R_tracks,T_tracks,max_point_integration_range);

% organize points into grid structure
[cell_pts, cell_inds] = create_point_grid(all_pts, point_grid_size_patches);

% construct z variance grid
zvar = nan(size(cell_pts));
zmin = zvar;
zmax = zvar;
count = zvar;
for i = 1:numel(zvar)
    p = cell_pts{i};
    if (size(p,1)>min_init_patch_points)
        zvar(i) = var(p(:,3));
        zmin(i) = min(p(:,3));
        zmax(i) = max(p(:,3));
        count(i) = size(p,1);
    end
end

% create patches that are likely to be flat
good_ind = find(zvar < max_height_std^2);
total = 0;
for i = 1:numel(good_ind)
    total = total + numel(cell_inds{good_ind(i)});
end

dat = zeros(total,4);
cnt = 1;
for i = 1:numel(good_ind)
    patch_inds = cell_inds{good_ind(i)};
    n = numel(patch_inds);
    dat(cnt:cnt+n-1,:) = [repmat(i,[n,1]),scan_inds(patch_inds),sensor_pts(patch_inds,:)];
    cnt = cnt+n;
end
dat = sortrows(dat,[1,2]);

next_num = max(dat(:,1))+1;
dx = diff(dat(:,1));
patch_starts = [1;find(dx>0)+1];
patch_ends = [patch_starts(2:end)-1;numel(dx)+1];
for i = 1:numel(patch_starts)
    ptr = patch_starts(i);
    while (ptr <= patch_ends(i))
        scan_inds = dat(ptr:patch_ends(i),2);
        scan_inds = scan_inds-scan_inds(1);
        last_ind = ptr + find(scan_inds <= max_patch_duration,1,'last')-1;
        dat(ptr:last_ind,1) = next_num;
        next_num = next_num+1;
        ptr = last_ind+1;
    end
end
dat = sortrows(dat,[1,2]);

% remove entries with too few points
dx = diff(dat(:,1));
patch_starts = [1;find(dx~=0)+1];
patch_ends = [patch_starts(2:end)-1;numel(dx)+1];
patch_sizes = patch_ends-patch_starts;
bad_ind = find(patch_sizes<min_final_patch_points);
bad_dat = [];
for i = 1:numel(bad_ind)
    bad_dat = [bad_dat, patch_starts(bad_ind(i)):patch_ends(bad_ind(i))];
end
dat(bad_dat,:) = [];

% remap patch indices
u = unique(dat(:,1));
remapper = zeros(u(end),1);
remapper(u) = 1:numel(u);
dat(:,1) = remapper(dat(:,1));


%% optimize over patches and posts
[R,T] = optimize_pose_from_tracks_and_planes(tracks,dat,poses,R,T);
