function out = project_onto_ground_plane(xyz, ground_plane)
% size(xyz)
    
    t0 = (ground_plane(1).*xyz(:,1)+ground_plane(2).*xyz(:,2)+ ...
           ground_plane(3).*xyz(:,3)-ground_plane(4));
    
    % out = [xyz(:,1) + ground_plane(1) * t0, xyz(:,2) +
    % ground_plane(2) * t0, xyz(:,3) + ground_plane(3) * t0];
    out = [xyz(:,1) - ground_plane(1) .* t0, xyz(:,2) - ground_plane(2) ...
           .* t0, xyz(:,3) - ground_plane(3) .*t0];
    