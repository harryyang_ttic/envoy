function [e,pt_mean,pt_cov] = error_func_yaw_xy_tracks(params,fixed_params,tracks,P_body,draw_it)

% fixed params are roll, pitch and z
% params that are optimized are yaw, x, and y
% grab rigid sensor-to-body transform from parameter vector
R = rpy2rot([fixed_params(1:2); params(1)]);
T = [params(2:3); fixed_params(3)];

% set up figure for plot
if (draw_it)
    figure(22);
    clf;
    hold on;
end

point_sums = cumsum(tracks.num_points);
e = zeros(2*point_sums(end),1);

% initialize mean and covariance for each feature
pt_mean = zeros(numel(tracks.num_points),2);
pt_cov = zeros(2,2,size(pt_mean,1));

% transform all track data
all_pts = transform_sensor_to_local2(tracks.data, ...
                                    P_body, R, T);

all_pts = all_pts(:,1:2);
ends = point_sums;
starts = [1;ends(1:end-1)+1];

% loop over tracks
colors = ['y-';'b-';'r-';'g-';'m-'];
for i = 1:numel(starts)

    % find mean location of this feature in world coords
    pts = all_pts(starts(i):ends(i),:);
    avg = sum(pts,1)/size(pts,1);
    pt_mean(i,:) = avg;

    % error = difference between each point and the mean
    diffs = [pts(:,1)-avg(1),pts(:,2)-avg(2)];

    % compute cov
    pt_cov(:,:,i) = diffs'*diffs/size(diffs,1);
    
    % set appropriate entries in error vector
    e(2*starts(i)-1:2*ends(i)) = diffs(:);
    
    if (draw_it)
        plot(pts(:,1),pts(:,2),colors(mod(i,4)+1));
        plot(avg(1),avg(2),'r.');
    end
end

if (draw_it)
    axis equal;
    hold off;
    drawnow;
end
