%% notes
% use alignment of sensors to each other at the end
% use robust nonlinear least squares

clear;
close all;

%% user variables
%LOG_FILE = ['/mnt/tig-storage/envoy/logs/2012_04_15_calibration/' ...
%            'lcmlog-2012-04-15.00'];
%LOG_FILE = ['/home/spillai/data/2012_calibration_velodyne_kinect/' ...
%            'lcmlog-2012-04-25.03'];
% LOG_FILE ='/home/spillai/data/2012_calibration_velodyne_kinect/sensor_frame/lcmlog-2012-05-01.00'
% LOG_FILE ='/home/spillai/data/2012_06_08_poles/lcmlog-2012-06-08.00'
LOG_FILE ='/home/spillai/data/2012_06_08_poles/post-processed-pose-from-scanmatch2/lcmlog-2012-06-14.00'
LOG_START_FRAC = 0;
LOG_END_FRAC = 1;
LIDARS = {'SKIRT_FRONT'};
%LIDARS = {'SKIRT_FRONT_RIGHT'};

%LOG_FILE = 'd:/projects/agile/data/2010_05_17_waverly_calibration/lcmlog-2010-05-17.00';
%LOG_START_FRAC = 0;
%LOG_END_FRAC = 0.4;
%LIDARS = {'BROOM_FRONT_RIGHT','BROOM_FRONT_LEFT','BROOM_REAR_RIGHT','BROOM_REAR_LEFT',...
%    'SKIRT_FRONT_RIGHT','SKIRT_FRONT_LEFT','SKIRT_REAR_RIGHT','SKIRT_REAR_LEFT','SKIRT_REAR',...
%    'BROOM_CARRIAGE','HORIZ_TINE_RIGHT','HORIZ_TINE_LEFT'};


%% aux variables (automatic)
CHANNEL_TYPES = cellstr(repmat('bot_core.planar_lidar_t',[numel(LIDARS),1]));
CHANNELS = [LIDARS, 'POSE'];
CHANNEL_TYPES{end+1} = 'bot_core.pose_t';

%% read log data
setup_lcm;
logdata = read_from_log(LOG_FILE, CHANNELS, CHANNEL_TYPES, [LOG_START_FRAC,LOG_END_FRAC]);

%% calibrate sensors

for i = 1:numel(LIDARS)
    % select set to process
    chan = lower(LIDARS{i});
    scans = logdata.(chan);

    % synchronize poses
    poses = synchronize_poses(logdata.pose, [logdata.(chan).t]);
    poses(end) = [];
    scans = scans([poses.out_ind]);
    
    % optimize
    [R,T] = calib_single_lidar(scans, poses);
    calib.(chan).name = upper(chan);
    calib.(chan).R = R;
    calib.(chan).T = T;
end

min_max_range = [0.2 3];

% %% plot registered points
% sub_scans = scans(1:200);
% sub_poses = poses(1:200);
% p = integrate_point_cloud(sub_scans,sub_poses,R,T,min_max_range);
% figure;
% plot3k(p); axis equal; view3d
% hold on;
% h = [];
% for i = 1:2:numel(sub_scans)
%     p = integrate_point_cloud(sub_scans(i),sub_poses(i),R,T,min_max_range);
%     if (numel(h)>0)
%         delete(h);
%     end
%     h = myplot3(p,'r.','markersize',20);
%     plot_pose(quat2rot(sub_poses(i).orientation), sub_poses(i).position, '', 0.5);
%     drawnow;
% end
% hold off;

%% plot posts
%figure;
%for i = 1:numel(posts)
%    myplot(posts{i},'r.');
%    axis([0,40,-20,20]);
%    drawnow;
%end

%% plot tracks
% find start for each feature
% track_ends = cumsum(tracks.num_points);
% track_starts = [1;track_ends(1:end-1)+1];
% starts = zeros(sum(tracks.num_points),1);
% for i = 1:numel(track_starts)
%     starts(i) = tracks.data(track_starts(i),3);
% end

% create new data structure
% new_column = [];
% for i = 1:numel(track_starts)
%     new_column = [new_column; repmat(i,[tracks.num_points(i),1])];
% end
% dat = sortrows([tracks.data,new_column],3);
% trk.v = sparse(dat(:,3),dat(:,4),ones(size(dat,1),1));
% trk.x = sparse(dat(:,3),dat(:,4),dat(:,1));
% trk.y = sparse(dat(:,3),dat(:,4),dat(:,2));

%plot
% figure;
% for i = 1:size(trk.v,1)
%     feats = find(trk.v(i,:)>0);
%     cur_starts = starts(feats);
%     clf
%     hold on;
%     for k = 1:numel(feats)
%         past_inds = cur_starts(k):i;
%         past_inds = past_inds(trk.v(cur_starts(k):i,feats(k))>0);
%         x = trk.x(past_inds,feats(k));
%         y = trk.y(past_inds,feats(k));
%         plot(x,y,'b-');
%         plot(x(end),y(end),'r.');
%     end
%     %myplot(posts{i}, 'ko');
%     hold off;
    
%     axis([0,40,-20,20]);
%     title(sprintf('scan %d\n', i));
%     drawnow;
% %    pause(1);
% end


%% plot sensor pose
fields = fieldnames(calib);
figure; hold on;
for i = 1:numel(fields)
    cc = calib.(fields{i});
    plot_pose(cc.R, [cc.T(1:2);0], '', 0.5);

    disp('Final Pose Estimate: '); fields{i};
    R = rot2rpy(cc.R)*180/pi, cc.T
    %plot_pose(cc.R, [cc.T(1:2);0], cc.name, 0.5);
end
hold off;
axis equal;
view3d

%% plot scans
figure;
for i = 1:numel(scans)
    p = [scans(i).ranges(:).*cos(scans(i).thetas(:)),scans(i).ranges(:).*sin(scans(i).thetas(:))];
    myplot(p,'r.');
    axis([0,40,-40,40]);
    drawnow;
end
