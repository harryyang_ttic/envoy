% function posts = extract_posts_from_scan(r,t,max_width,min_dr,min_range,range_range)
% Extracts post-like observations from a single scan.
% r: array of range values
% t: array of theta values
% max_width: maximum width of a post
% min_dr: minimum difference in range for post candidate
% range_range: min and max range allowed
% invalid_range: max invalid range and its replacement
% posts: n x 2 array of extracted 2d post locations (x-y pairs)
function posts = extract_posts_from_scan(r,t,max_width,min_dr,range_range,invalid_range)

% Replace invalid ranges with max range
r(r<invalid_range(1)) = invalid_range(2);

% Compute deltas in range to look for high-low-high profiles
dr = diff(r(:));

% Whether each difference corresponds to a potential start or end of a post
is_start = dr<-min_dr;
is_end = dr>min_dr;

% Find all endpoint indices
inds = find(is_start | is_end);

% Label each endpoint as start (0) or end (1)
dat = [inds, is_end(inds)];

% Find places where a start immediately precedes an end
diffs = diff(dat(:,2));
inds = find(diffs>0);

% Compute index pairs corresponding to post data in between start and end
cands = [dat(inds,1)+1,dat(inds+1,1)];

% Compute endpoint xy locations
r = r(cands);
t = t(cands);
endpts = [r.*cos(t),r.*sin(t)];

% Remove any candidate that is too wide
dists = hypot(endpts(:,1)-endpts(:,2),endpts(:,3)-endpts(:,4));
bad_ind = dists>max_width;
endpts(bad_ind,:) = [];

% Form posts as average of start and end point
posts = 0.5*[endpts(:,1)+endpts(:,2), endpts(:,3)+endpts(:,4)];

% Remove posts that are too close to or far from sensor
range2 = posts(:,1).^2 + posts(:,2).^2;
bad_ind = range2 < range_range(1)^2 | range2 > range_range(2)^2;
posts(bad_ind,:) = [];
