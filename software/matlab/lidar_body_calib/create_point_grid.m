function [cell_pts,cell_inds,xform] = create_point_grid(pts,binsize)

minpt = min(pts(:,1:2),[],1);
p = [(pts(:,1)-minpt(1))/binsize,(pts(:,2)-minpt(2))/binsize]+1;
sz = max(round(p),[],1);

cell_inds = (round(p(:,1))-1)*sz(2) + round(p(:,2));
pt_inds = (1:size(pts,1))';
dat = sortrows([pts,pt_inds,cell_inds],5);
diffs = diff(dat(:,5));
start_inds = [1;find(diffs)+1];
end_inds = [start_inds(2:end)-1;size(dat,1)];

cell_inds = cell(sz(2),sz(1));
cell_pts = cell(sz(2),sz(1));
for i = 1:numel(start_inds)
    cell_pts{dat(start_inds(i),5)} = dat(start_inds(i):end_inds(i),1:3);
    cell_inds{dat(start_inds(i),5)} = dat(start_inds(i):end_inds(i),4);
end

xform = [1,0,1;0,1,1;0,0,1]*diag([1/binsize,1/binsize,1])*[1,0,-minpt(1);0,1,-minpt(2);0,0,1];
xform = inv(xform);
