%% notes
% use alignment of sensors to each other at the end
% use robust nonlinear least squares
clear;
close all;
%% user variables
% lcmlog-2012-04-15.00
% LOG_FILE =
% '/mnt/tig-storage/envoy/logs/2012_04_15_calibration/lcmlog-2012-04-24.00';
% LOG_FILE = ['/home/spillai/data/2012_calibration_velodyne_kinect/' ...
%           'lcmlog-2012-04-25.03'];
%LOG_FILE ='/home/spillai/data/2012_calibration_velodyne_kinect/sensor_frame/lcmlog-2012-05-01.00'
% LOG_FILE ='/home/spillai/data/2012_06_08_poles/post-processed/lcmlog-2012-06-12.00'
LOG_FILE ='/home/spillai/data/2012_06_08_poles/post-processed-pose-from-scanmatch2/lcmlog-2012-06-14.01'
LOG_START_FRAC = 0;
LOG_END_FRAC = 1;
LIDARS = {'SKIRT_FRONT'};
%LIDARS = {'SKIRT_FRONT_RIGHT'};

% LOG_FILE = 'd:/projects/agile/data/2010_05_17_waverly_calibration/lcmlog-2010-05-17.00';
%LOG_START_FRAC = 0;
%LOG_END_FRAC = 0.4;
%LIDARS = {'BROOM_FRONT_RIGHT','BROOM_FRONT_LEFT','BROOM_REAR_RIGHT','BROOM_REAR_LEFT',...
%    'SKIRT_FRONT_RIGHT','SKIRT_FRONT_LEFT','SKIRT_REAR_RIGHT','SKIRT_REAR_LEFT','SKIRT_REAR',...
%    'BROOM_CARRIAGE','HORIZ_TINE_RIGHT','HORIZ_TINE_LEFT'};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% un the following (after updating perception, pcl-utils, lcmtypes and velodyne and config folders)
% er-velodyne-pole-segmenter (ross's pole extraction stuff)
% pcl_er_cylinder_fitting (my cylinder fitting stuff) - publishes erlcm_cylinder_list_t on CYLINDER_ channel
% pcl_er_velodyne -s (my ground plane estimation) - publishes erlcm_plane_model_t on GROUND_PLANE channel
% The cylinders and ground plane are both in the velodyne body frame. 

%% aux variables (automatic)
CHANNEL_TYPES = cellstr(repmat('bot_core.planar_lidar_t', ...
                               [numel(LIDARS),1]));
CHANNELS = [LIDARS, 'POSE','CYLINDER_LIST','PCL_SEGMENT_LIST','GROUND_PLANE'];
CHANNEL_TYPES{end+1} = 'bot_core.pose_t';
CHANNEL_TYPES{end+1} = 'erlcm.cylinder_list_t';
CHANNEL_TYPES{end+1} = 'erlcm.segment_list_t';
CHANNEL_TYPES{end+1} = 'erlcm.plane_model_t';

%% read log data
setup_lcm;
logdata = read_from_log(LOG_FILE, CHANNELS, CHANNEL_TYPES, [LOG_START_FRAC,LOG_END_FRAC]);

%% calibrate sensors

vel_scans_orig = logdata.pcl_segment_list;
poses = synchronize_poses(logdata.pose, [vel_scans_orig.t]);
vel_scans = vel_scans_orig([poses.out_ind]);

skirt_scans_orig = logdata.skirt_front;

% synchronize poses
scan_ind = synchronize_sensors(logdata.skirt_front, [vel_scans_orig.t]);
skirt_scans = skirt_scans_orig([scan_ind]);

% optimize
ground_plane = sum(vertcat(logdata.ground_plane(:).params))/ ...
    size(logdata.ground_plane,2);
ground_plane(1:3) = ground_plane(1:3)/norm(ground_plane(1:3))

[R,T] = calib_velodyne(vel_scans, skirt_scans, poses, ground_plane);
calib.cylinder_list.R = R;
calib.cylinder_list.T = T;

min_max_range = [-10 10];

%% plot registered points
sub_scans = skirt_scans(:);
sub_poses = poses(:);

hold on;
h = [];
for i = 1:2:numel(sub_poses)
    plot_pose(quat2rot(sub_poses(i).orientation), sub_poses(i).position, '', 0.5);
    drawnow;
end
hold off;


%% plot tracks
% find start for each feature
% track_ends = cumsum(tracks.num_points);
% track_starts = [1;track_ends(1:end-1)+1];
% starts = zeros(sum(tracks.num_points),1);
% for i = 1:numel(track_starts)
%     starts(i) = tracks.data(track_starts(i),3);
% end

% create new data structure
% new_column = [];
% for i = 1:numel(track_starts)
%     new_column = [new_column; repmat(i,[tracks.num_points(i),1])];
% end
% dat = sortrows([tracks.data,new_column],3);
% trk.v = sparse(dat(:,3),dat(:,4),ones(size(dat,1),1));
% trk.x = sparse(dat(:,3),dat(:,4),dat(:,1));
% trk.y = sparse(dat(:,3),dat(:,4),dat(:,2));

%plot
% figure;
% for i = 1:size(trk.v,1)
%     feats = find(trk.v(i,:)>0);
%     cur_starts = starts(feats);
%     clf
%     hold on;
%     for k = 1:numel(feats)
%         past_inds = cur_starts(k):i;
%         past_inds = past_inds(trk.v(cur_starts(k):i,feats(k))>0);
%         x = trk.x(past_inds,feats(k));
%         y = trk.y(past_inds,feats(k));
%         plot(x,y,'b-');
%         plot(x(end),y(end),'r.');
%     end
%     %myplot(posts{i}, 'ko');
%     hold off;
    
%     axis([0,40,-20,20]);
%     title(sprintf('scan %d\n', i));
%     drawnow;
% %    pause(1);
% end


%% plot sensor pose
fields = fieldnames(calib);
figure; hold on;
for i = 1:numel(fields)
    cc = calib.(fields{i})
    plot_pose(cc.R, [cc.T(1:2);0], '', 0.5);
    str = fields{i};
    str = sprintf('Final Pose Estimate: %s',str); 
    R = rot2rpy(cc.R)*180/pi
    T = [cc.T(1:2);-ground_plane(4)]
    %plot_pose(cc.R, [cc.T(1:2);0], cc.name, 0.5);
end
hold off;
axis equal;
view3d
