function [R,T] = calib_velodyne(vel_scans, skirt_scans, poses, ground_plane)

%% set some params

% post tracking
max_post_track_dist = 0.3;     % distance between successive observations (m)
max_post_track_misses = 300;   % number of consecutive non-observations (scans)
max_post_track_hits = 600;     % observations of post (scans)  TODO: make this total lifetime rather than hits
min_post_track_length = 30;    % lifetime of post (scans)

% track optimization
max_post_track_std = 0.1;      % standard deviation of post track residual (m)
post_inlier_frac = 0.9;        % fraction of inliers to retain at each iteration


%% detect posts
fprintf('Detecting posts...');

vel_posts = {};
for i=1:numel(vel_scans)
    % Draw
    R = quat2rot(poses(i).orientation);
    T = poses(i).position(:);
    
    seg_count = vel_scans(i).count;
    seg_centers = [];
    for j=1:seg_count
        count = vel_scans(i).seg{j}.count;
        pts = zeros(count,3);
        for k=1:count
            pts(k,:) = vel_scans(i).seg{j}.points(k).xyz';
        end;

        p = project_onto_ground_plane(pts, ground_plane);

        xs = p(:,1);
        ys = p(:,2);
        zs = p(:,3);
        [xc,yc,r,a] = circfit(xs,ys);
        zc = mean(p(:,3));
        
        if r > 0.03 & r < 0.1
            seg_centers = vertcat(seg_centers, [xc,yc,zc]);
        end
    end;
    vel_posts{i} = seg_centers;
end;


fprintf('done.\n');

%% track posts
fprintf('Tracking posts...');
tracks_orig = track_points2(vel_posts,max_post_track_dist,max_post_track_misses,max_post_track_hits);
fprintf('done.\n');

%% remove features with too few observations
bad_tracks = find(tracks_orig.num_points < min_post_track_length);
bad_ind = indices_from_ids(tracks_orig.num_points, bad_tracks);
tracks_orig.data(bad_ind,:) = [];
tracks_orig.num_points(bad_tracks) = [];
tracks = tracks_orig;

%% initial optimization of pose using only point tracks
rpy = [atan2(ground_plane(2),ground_plane(3)); ...
       atan2(ground_plane(1),ground_plane(3));0];
RPY = rpy * 180 / pi

R_fixed = rpy2rot(rpy);
T_fixed = [0;0;-ground_plane(4)];

R = eye(3);
T = [0;0;-ground_plane(4)];

for iter = 1:50
    [R,T,err,pt_mean,pt_cov] = optimize_yaw_xy_from_tracks(tracks,poses,R,T,R_fixed,T_fixed,true,true);
    rpy = rot2rpy(R);
    if(rpy(2)<0)
        rpy(2) = -rpy(2);
        R = rpy2rot(rpy);
    end
    
    evals = zeros(size(pt_cov,3),1);
    for i = 1:numel(evals)
        evals(i) = max(eig(pt_cov(:,:,i)));
    end
    if (sum(evals > max_post_track_std^2) == 0)
        break;
    end
    [evals,sort_ind] = sort(evals);
    bad_tracks = sort_ind(ceil(post_inlier_frac * numel(evals)):end);
    bad_ind = indices_from_ids(tracks.num_points, bad_tracks);
    tracks.data(bad_ind,:) = [];
    tracks.num_points(bad_tracks) = [];
end
R_tracks = R;
T_tracks = T;

return;
