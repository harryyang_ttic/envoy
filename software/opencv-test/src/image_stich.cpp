#include <stdio.h>
#include <iostream>
 
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/stitching/stitcher.hpp"
 
using namespace cv;
 
void readme();
 
/** @function main */
int main( int argc, char** argv )
{
    if( argc < 3 )
        { readme(); return -1; }
 
    vector<Mat> imgs;
    
    

    // Load the images
    Mat image1= imread( argv[2] );
    Mat image2= imread( argv[1] );

    imgs.push_back(image1);
    imgs.push_back(image2);

    if(argc == 4){ 
        Mat image3 = imread( argv[3] );
        imgs.push_back(image3);
    }
    Mat gray_image1;
    Mat gray_image2;
    // Convert to Grayscale
    cvtColor( image1, gray_image1, CV_RGB2GRAY );
    cvtColor( image2, gray_image2, CV_RGB2GRAY );
 
    imshow("first image",image2);
    imshow("second image",image1);
 
    if( !gray_image1.data || !gray_image2.data )
        { std::cout<< " --(!) Error reading images " << std::endl; return -1; }
 
    //-- Step 1: Detect the keypoints using SURF Detector
    int minHessian = 400;
 
    SurfFeatureDetector detector( minHessian );
 
    std::vector< KeyPoint > keypoints_object, keypoints_scene;
 
    detector.detect( gray_image1, keypoints_object );
    detector.detect( gray_image2, keypoints_scene );

    // Add results to image and save.
    cv::Mat surf_output_1;
    cv::drawKeypoints(image1, keypoints_object, surf_output_1);
    cv::imshow("surf_result_1", surf_output_1);

    cv::Mat surf_output_2;
    cv::drawKeypoints(image2, keypoints_scene, surf_output_2);
    cv::imshow("surf_result_2", surf_output_2);
 
    //-- Step 2: Calculate descriptors (feature vectors)
    SurfDescriptorExtractor extractor;
 
    Mat descriptors_object, descriptors_scene;
 
    extractor.compute( gray_image1, keypoints_object, descriptors_object );
    extractor.compute( gray_image2, keypoints_scene, descriptors_scene );
 
    //-- Step 3: Matching descriptor vectors using FLANN matcher
    FlannBasedMatcher matcher;
    std::vector< DMatch > matches;
    matcher.match( descriptors_object, descriptors_scene, matches );
 
    double max_dist = 0; double min_dist = 100;
 
    //-- Quick calculation of max and min distances between keypoints
    for( int i = 0; i < descriptors_object.rows; i++ )
        { double dist = matches[i].distance;
            if( dist < min_dist ) min_dist = dist;
            if( dist > max_dist ) max_dist = dist;
        }
 
    printf("-- Max dist : %f \n", max_dist );
    printf("-- Min dist : %f \n", min_dist );
 
    //-- Use only "good" matches (i.e. whose distance is less than 3*min_dist )
    std::vector< DMatch > good_matches;
 
    for( int i = 0; i < descriptors_object.rows; i++ ){
        if( matches[i].distance < 3*min_dist ){
            good_matches.push_back( matches[i]); 
        }
    }

    std::vector< Point2f > obj;
    std::vector< Point2f > scene;
 
    for( int i = 0; i < good_matches.size(); i++ )
        {
            //-- Get the keypoints from the good matches
            obj.push_back( keypoints_object[ good_matches[i].queryIdx ].pt );
            scene.push_back( keypoints_scene[ good_matches[i].trainIdx ].pt );
        }

    std::vector< KeyPoint > keypoints_object_good, keypoints_scene_good;
     
    //just the set of possible good matches 
    for( int i = 0; i < good_matches.size(); i++ )
        {
            //-- Get the keypoints from the good matches
            keypoints_object_good.push_back( keypoints_object[ good_matches[i].queryIdx ] );
            keypoints_scene_good.push_back( keypoints_scene[ good_matches[i].trainIdx ] );
        }

    // Add results to image and save.
    cv::Mat surf_output_gd_1;
    cv::drawKeypoints(image1, keypoints_object_good, surf_output_gd_1);
    cv::imshow("surf_result_gd_1", surf_output_1);

    cv::Mat surf_output_gd_2;
    cv::drawKeypoints(image2, keypoints_scene_good, surf_output_gd_2);
    cv::imshow("surf_result_gd_2", surf_output_2);
 
    //ransac is used to find the actual good matches 
    //and the homography
    // Find the Homography Matrix
    Mat H = findHomography( obj, scene, CV_RANSAC );

    std::cout << H << std::endl;
    // Use the Homography Matrix to warp the images
    cv::Mat result;
    warpPerspective(image1,result,H,cv::Size(image1.cols+image2.cols,image1.rows));    
    cv::Mat half(result,cv::Rect(0,0,image2.cols,image2.rows));
    image2.copyTo(half);
    imshow( "Result", result );
     
    
    //wrapper examples 
    //opencv-latest-pod/opencv/samples/cpp/stitching_detailed.cpp and stitching.cpp
    //http://books.google.co.in/books?id=seAgiOfu2EIC&printsec=frontcover&dq=learning%20opencv&hl=en&sa=X&ei=RYbTT8r6FIrqrQeptYj8Dw&redir_esc=y#v=onepage&q=learning%20opencv&f=false
    //http://stackoverflow.com/questions/16284126/opencv-stitcher-class-with-overlapping-stationary-cameras
    Mat pano;
    Stitcher stitcher = Stitcher::createDefault(false);
    //stitcher.setWarper(new PlaneWarper());
    stitcher.setWarper(new SphericalWarper());
    /*
      if (warp_type == "plane") warper_creator = new cv::PlaneWarper();
        else if (warp_type == "cylindrical") warper_creator = new cv::CylindricalWarper();
        else if (warp_type == "spherical") warper_creator = new cv::SphericalWarper();
        else if (warp_type == "fisheye") warper_creator = new cv::FisheyeWarper();
        else if (warp_type == "stereographic") warper_creator = new cv::StereographicWarper();
        else if (warp_type == "compressedPlaneA2B1") warper_creator = new cv::CompressedRectilinearWarper(2, 1);
        else if (warp_type == "compressedPlaneA1.5B1") warper_creator = new cv::CompressedRectilinearWarper(1.5, 1);
        else if (warp_type == "compressedPlanePortraitA2B1") warper_creator = new cv::CompressedRectilinearPortraitWarper(2, 1);
        else if (warp_type == "compressedPlanePortraitA1.5B1") warper_creator = new cv::CompressedRectilinearPortraitWarper(1.5, 1);
        else if (warp_type == "paniniA2B1") warper_creator = new cv::PaniniWarper(2, 1);
        else if (warp_type == "paniniA1.5B1") warper_creator = new cv::PaniniWarper(1.5, 1);
        else if (warp_type == "paniniPortraitA2B1") warper_creator = new cv::PaniniPortraitWarper(2, 1);
        else if (warp_type == "paniniPortraitA1.5B1") warper_creator = new cv::PaniniPortraitWarper(1.5, 1);
        else if (warp_type == "mercator") warper_creator = new cv::MercatorWarper();
        else if (warp_type == "transverseMercator") warper_creator = new cv::TransverseMercatorWarper();
     */

    //stitcher.setWarper(new CylindricalWarper());
    /*
      stitcher.setFeaturesFinder(new detail::SurfFeaturesFinder(1000,3,4,3,4));
    //stitcher.setFeaturesFinder(new detail::OrbFeaturesFinder());
        stitcher.setRegistrationResol(0.1);
        stitcher.setSeamEstimationResol(0.1);
        stitcher.setCompositingResol(0.6);
stitcher.setPanoConfidenceThresh(1);
        stitcher.setWaveCorrection(true);
        stitcher.setWaveCorrectKind(detail::WAVE_CORRECT_HORIZ);
        stitcher.setFeaturesMatcher(new detail::BestOf2NearestMatcher(false,0.3));
        stitcher.setBundleAdjuster(new detail::BundleAdjusterRay());

     */
    
    stitcher.stitch(imgs, pano);
    imshow("Sticher Pipeline", pano);
    
    waitKey(0);
    return 0;
}
 
/** @function readme */
void readme()
{ std::cout << " Usage: Panorama < img1 > < img2 >" << std::endl; }
