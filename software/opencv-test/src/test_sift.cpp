#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <bot_vis/bot_vis.h>
#include <image_utils/pixels.h>
#include <image_utils/jpeg.h>
#include <opencv_utils/opencv_utils.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <jpeg-utils/jpeg-utils.h>
//#include <er_common/envoy_opencv_utils.hpp>

typedef struct
{
    BotParam   *b_server;
    lcm_t      *lcm;
    GMainLoop *mainloop;
    GMutex *mutex;
    bot_core_image_t *received_image; 
    int verbose; 
} state_t;

typedef struct _person_cam_renderer {
    bot_core_image_t *last_image;
    BotGlTexture *texture;
  
    uint8_t *uncompresed_buffer;
    int uncompressed_buffer_size;
    int width, height;

    
} person_cam_renderer_t;

/*int envoy_decompress_jpeg_image(unsigned char * jpeg_image, int jpeg_size, unsigned char ** uncomp_image,
    int *uncomp_width, int *uncomp_height, int *uncomp_depth)
{
  struct jpeg_decompress_struct cinfo;
  struct jpeg_source_mgr jsrc;
  my_jpeg_error_mgr_t jerr;

  cinfo.err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = _error_exit;
  if (setjmp(jerr.setjmp_buffer)) {
    // code execution starts here if the _error_exit handler was called
    jpeg_destroy_decompress(&cinfo);
    return -1;
  }

  jpeg_create_decompress (&cinfo);

  jsrc.next_input_byte = jpeg_image;
  jsrc.bytes_in_buffer = jpeg_size;
  jsrc.init_source = init_source;
  jsrc.fill_input_buffer = fill_input_buffer;
  jsrc.skip_input_data = skip_input_data;
  jsrc.resync_to_restart = jpeg_resync_to_restart;
  jsrc.term_source = term_source;
  cinfo.src = &jsrc;

  jpeg_read_header(&cinfo, TRUE);
  if (cinfo.num_components == 3)
    cinfo.out_color_space = JCS_RGB;
  else if (cinfo.num_components == 1)
    cinfo.out_color_space = JCS_GRAYSCALE;
  else {
    jpeg_destroy_decompress(&cinfo);
    printf("ERROR: %d is an unsupported number of components for jpeg decompression!\n", cinfo.output_components);
    exit(1);
  }

  // store the image information 
  if (*uncomp_image == NULL || *uncomp_width != (int) cinfo.image_width || *uncomp_height != (int) cinfo.image_height
      || *uncomp_depth != (int) cinfo.num_components) {
    *uncomp_width = cinfo.image_width;
    *uncomp_height = cinfo.image_height;
    *uncomp_depth = cinfo.num_components;
    int uncomp_size = (*uncomp_width) * (*uncomp_height) * (*uncomp_depth);
    *uncomp_image = (unsigned char *) realloc(*uncomp_image, uncomp_size * sizeof(unsigned char));
  }

  if (!(cinfo.dc_huff_tbl_ptrs[0] || cinfo.dc_huff_tbl_ptrs[1] || cinfo.ac_huff_tbl_ptrs[0]
      || cinfo.ac_huff_tbl_ptrs[1])) {
    _jpeg_std_huff_tables(&cinfo);
  }

  jpeg_start_decompress(&cinfo);

  int stride = (*uncomp_width) * (*uncomp_depth);
  while (cinfo.output_scanline < (unsigned int) *uncomp_height) {
    uint8_t * row = (uint8_t *) *uncomp_image + cinfo.output_scanline * stride;
    jpeg_read_scanlines(&cinfo, &row, 1);
  }
  jpeg_finish_decompress(&cinfo);
  jpeg_destroy_decompress(&cinfo);
  return 0;
}



int envoy_compress_image_to_jpeg(int uncomp_width, int uncomp_height, int uncomp_depth, unsigned char *uncomp_image,
    int max_jpeg_size, unsigned char ** jpeg_image, int * jpeg_size)
{
  int quality = 100;

  int stride = uncomp_width * uncomp_depth;

  struct jpeg_compress_struct cinfo;
  struct jpeg_error_mgr jerr;
  struct jpeg_destination_mgr jdest;

  if (*jpeg_image == NULL || max_jpeg_size <= 0) {
    //allocate enough space for the jpeg image
    max_jpeg_size = uncomp_width * uncomp_height * uncomp_depth;
    *jpeg_image = (unsigned char *) realloc(*jpeg_image, max_jpeg_size * sizeof(unsigned char));
  }

  int out_size = max_jpeg_size;

  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_compress (&cinfo);
  jdest.next_output_byte = *jpeg_image;
  jdest.free_in_buffer = out_size;
  jdest.init_destination = init_destination;
  jdest.empty_output_buffer = empty_output_buffer;
  jdest.term_destination = term_destination;
  cinfo.dest = &jdest;

  cinfo.image_width = uncomp_width;
  cinfo.image_height = uncomp_height;

  if (uncomp_depth == 3) {
    cinfo.input_components = 3;
    cinfo.in_color_space = JCS_RGB;
  }
  else if (uncomp_depth == 1) {
    cinfo.input_components = 1;
    cinfo.in_color_space = JCS_GRAYSCALE;
  }
  else {
    printf("ERROR: Can't compress %d of color channels\n", uncomp_depth);
  }
  jpeg_set_defaults(&cinfo);
  jpeg_set_quality(&cinfo, quality, TRUE);

  jpeg_start_compress(&cinfo, TRUE);
  while (cinfo.next_scanline < (unsigned int) uncomp_height) {
    JSAMPROW row = (JSAMPROW) (uncomp_image + cinfo.next_scanline * stride);
    jpeg_write_scanlines(&cinfo, &row, 1);
  }
  jpeg_finish_compress(&cinfo);
  *jpeg_size = out_size - jdest.free_in_buffer;
  jpeg_destroy_compress(&cinfo);
  return 0;
}

int envoy_compress_bot_core_image_t(const bot_core_image_t *uncomp_image, bot_core_image_t * jpeg_image)
{
  //copy metadata
  jpeg_image->utime = uncomp_image->utime;
  if (jpeg_image->metadata != NULL)
    bot_core_image_metadata_t_destroy(jpeg_image->metadata);
  jpeg_image->nmetadata = uncomp_image->nmetadata;
  if (jpeg_image->nmetadata>0){
    jpeg_image->metadata = bot_core_image_metadata_t_copy(uncomp_image->metadata);
  }
  jpeg_image->width = uncomp_image->width;
  jpeg_image->height = uncomp_image->height;
  jpeg_image->row_stride = uncomp_image->row_stride; //shouldn't be used since this is JPEG
  jpeg_image->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG;
  int depth = uncomp_image->row_stride / uncomp_image->width;
  int ret = envoy_compress_image_to_jpeg(uncomp_image->width, uncomp_image->height, depth, uncomp_image->data,
      jpeg_image->size, &jpeg_image->data, &jpeg_image->size);
  return ret;
}*/


int envoy_decompress_bot_core_image_t(const bot_core_image_t * jpeg_image, bot_core_image_t *uncomp_image)
{
  //copy metadata
  uncomp_image->utime = jpeg_image->utime;
  if (uncomp_image->metadata != NULL)
    bot_core_image_metadata_t_destroy(uncomp_image->metadata);
  uncomp_image->nmetadata = jpeg_image->nmetadata;
  if (jpeg_image->nmetadata > 0) {
    uncomp_image->metadata = bot_core_image_metadata_t_copy(jpeg_image->metadata);
  }
  
  uncomp_image->width = jpeg_image->width;
  uncomp_image->height = jpeg_image->height;
  
  uncomp_image->row_stride = jpeg_image->width * 3;//jpeg_image->row_stride;
  fprintf(stderr, "R : %d D : %d\n", jpeg_image->width, jpeg_image->row_stride);
  int depth=0;
  if (uncomp_image->width>0)
      depth= uncomp_image->row_stride / (double)uncomp_image->width;

  int ret;
  fprintf(stderr,"called1 : %d\n", uncomp_image->row_stride);//depth);

  if(uncomp_image->data == NULL){
      int uncomp_size = (uncomp_image->width) * (uncomp_image->height) * depth;
      uncomp_image->data = (unsigned char *) realloc(uncomp_image->data, uncomp_size * sizeof(uint8_t));
  }

  /*if(depth == 1){
      ret = jpeg_decompress_8u_gray(jpeg_image->data, 
                                    jpeg_image->size, uncomp_image->data, 
                                    uncomp_image->width, 
                                    uncomp_image->height, 
                                    uncomp_image->row_stride);
  }
  else if(depth == 3){*/
  ret = jpeg_decompress_8u_rgb(jpeg_image->data, 
                               jpeg_image->size, uncomp_image->data, 
                               uncomp_image->width, 
                               uncomp_image->height, 
                               uncomp_image->row_stride);
  
  fprintf(stderr,"called\n");
      //  }
  /*envoy_decompress_jpeg_image(jpeg_image->data, jpeg_image->size, &uncomp_image->data,
              &uncomp_image->width, &uncomp_image->height, &depth);*/
  uncomp_image->row_stride = uncomp_image->width * depth;
  uncomp_image->size = uncomp_image->width *uncomp_image->height* depth;
  if (depth == 1) {
    uncomp_image->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY;
  }
  else if (depth == 3) {
    uncomp_image->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB; //TODO: lets just assume its rgb... dunno what else to do :-/
  }
  return ret;
}

/*
 * Creates an opencv image header for a carmen3d_image_t structure
 * FYI: THIS FUNCTION DOES NOT COPY THE DATA!
 */
static inline CvMat get_opencv_header_for_bot_core_image_t(const bot_core_image_t * c3d_im)
{
  if (c3d_im->pixelformat == BOT_CORE_IMAGE_T_PIXEL_FORMAT_BGR || c3d_im->pixelformat
      == BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB) {
    return cvMat(c3d_im->height, c3d_im->width, CV_8UC3, c3d_im->data);
  }
  else if (c3d_im->pixelformat == BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY) {
    return cvMat(c3d_im->height, c3d_im->width, CV_8UC1, c3d_im->data);
  }
  else if (c3d_im->pixelformat == BOT_CORE_IMAGE_T_PIXEL_FORMAT_FLOAT_GRAY32) {
    return cvMat(c3d_im->height, c3d_im->width, CV_32FC1, c3d_im->data);
  }
  else {
    fprintf(stderr, "ERROR: invalid bot_core_image_t pixel format!\n");
    assert(false);
    return cvMat(0, 0, 0, NULL); //make compiler happy
  }
}


void sift_extractor(state_t *s){
    /*if(s->received_image ==NULL){
        return;
        }*/

    fprintf(stderr, "Extracting Sift\n");
    
    IplImage* img;

    static bot_core_image_t * localFrame=(bot_core_image_t *) calloc(1,sizeof(bot_core_image_t));
    const bot_core_image_t * newFrame=NULL;
    if (s->received_image->pixelformat==BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG){
        //create space for decompressed image
        envoy_decompress_bot_core_image_t(s->received_image,localFrame);
        //copy pointer for working locally
        newFrame = localFrame;
    }
    else{
        //uncompressed, just copy pointer
        newFrame = s->received_image;
    }  
    

    //better copy this - otherwise will crash stuff
    CvMat cvImg = get_opencv_header_for_bot_core_image_t(newFrame);

    CvMat *frame = &cvImg;//NULL;
    //Copy(&cvImg,frame);

    static IplImage tmpHeader;
    IplImage * currentFrame;

    currentFrame = cvGetImage(frame,&tmpHeader);

    const cv::Mat input = cv::Mat(currentFrame, false);

    //cv::SiftFeatureDetector detector;
    //cv::StarFeatureDetector detector;
    //cv::FastFeatureDetector detector;
    cv::SurfFeatureDetector surf_detector;
    std::vector<cv::KeyPoint> surf_keypoints;
    surf_detector.detect(input, surf_keypoints);

    // Add results to image and save.
    cv::Mat surf_output;
    cv::drawKeypoints(input, surf_keypoints, surf_output);
    cv::imwrite("surf_result.jpg", surf_output);

    cv::StarFeatureDetector star_detector;
    std::vector<cv::KeyPoint> star_keypoints;
    star_detector.detect(input, star_keypoints);

    // Add results to image and save.
    cv::Mat star_output;
    cv::drawKeypoints(input, star_keypoints, star_output);
    cv::imwrite("star_result.jpg", star_output);

    cv::SiftFeatureDetector sift_detector;
    std::vector<cv::KeyPoint> sift_keypoints;
    sift_detector.detect(input, sift_keypoints);

    // Add results to image and save.
    cv::Mat sift_output;
    cv::drawKeypoints(input, sift_keypoints, sift_output);
    cv::imwrite("sift_result.jpg", sift_output);
}

void sift_extractor_read_from_file(state_t *s){
    /*if(s->received_image ==NULL){
        return;
        }*/

    fprintf(stderr, "Extracting Sift\n");
    
    //IplImage* img;
    //img=cvLoadImage("input.jpg", 1);

    //CvMat cvImg = get_opencv_header_for_bot_core_image_t(s->received_image);

    //const cv::Mat input = cv::Mat(img, false);

    //    const cv::Mat input =  
    const cv::Mat input = cv::imread("input.jpg", 0); //Load as grayscale
    
    //cv::SiftFeatureDetector detector;
    //cv::StarFeatureDetector detector;
    //cv::FastFeatureDetector detector;
    cv::SurfFeatureDetector surf_detector;
    std::vector<cv::KeyPoint> surf_keypoints;
    surf_detector.detect(input, surf_keypoints);

    // Add results to image and save.
    cv::Mat surf_output;
    cv::drawKeypoints(input, surf_keypoints, surf_output);
    cv::imwrite("surf_result.jpg", surf_output);

    cv::StarFeatureDetector star_detector;
    std::vector<cv::KeyPoint> star_keypoints;
    star_detector.detect(input, star_keypoints);

    // Add results to image and save.
    cv::Mat star_output;
    cv::drawKeypoints(input, star_keypoints, star_output);
    cv::imwrite("star_result.jpg", star_output);

    cv::SiftFeatureDetector sift_detector;
    std::vector<cv::KeyPoint> sift_keypoints;
    sift_detector.detect(input, sift_keypoints);

    // Add results to image and save.
    cv::Mat sift_output;
    cv::drawKeypoints(input, sift_keypoints, sift_output);
    cv::imwrite("sift_result.jpg", sift_output);
}

// ============ LCM thread ========

static void
on_image (const lcm_recv_buf_t *rbuf, const char *channel, 
        const bot_core_image_t *image, void *user_data)
{
    // this method is always invoked from the LCM thread.
    state_t *self = (state_t *) user_data;
    g_mutex_lock (self->mutex);

    if (self->received_image) {
        bot_core_image_t_destroy(self->received_image);
    }
    self->received_image = bot_core_image_t_copy(image);
    fprintf(stderr,".");
    sift_extractor(self);
    
    g_mutex_unlock (self->mutex);
}




int main(int argc, const char* argv[])
{
    g_thread_init(NULL);
    setlinebuf (stdout);

    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    state->lcm =  bot_lcm_get_global(NULL);
    state->b_server = bot_param_new_from_server(state->lcm, 1);

    //pthread_create(&state->work_thread, NULL, track_work_thread, state);

    state->mutex = g_mutex_new ();

    state->received_image = NULL;
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    bot_core_image_t_subscribe(state->lcm , "CAMLCM_IMAGE", on_image, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
    

    return 0;
}
