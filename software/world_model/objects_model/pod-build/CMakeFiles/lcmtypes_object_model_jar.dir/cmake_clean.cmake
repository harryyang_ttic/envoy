FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/cpp"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/lcmtypes_object_model_jar"
  "lcmtypes_object_model.jar"
  "../lcmtypes/java/om/xml_cmd_t.class"
  "../lcmtypes/java/om/object_t.class"
  "../lcmtypes/java/om/object_list_t.class"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/lcmtypes_object_model_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
