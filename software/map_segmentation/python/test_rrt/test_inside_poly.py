def point_inside_polygon(x,y,poly):

    n = len(poly)
    inside =False

    p1x,p1y = poly[0]
    for i in range(n+1):
        p2x,p2y = poly[i % n]
        if y > min(p1y,p2y):
            if y <= max(p1y,p2y):
                if x <= max(p1x,p2x):
                    if p1y != p2y:
                        xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                    if p1x == p2x or x <= xinters:
                        inside = not inside
        p1x,p1y = p2x,p2y

    return inside

if __name__ == "__main__":
    x = 38.486085
    y = 58.978962
    poly = [[34.799148,60.992209], [39.954570,60.992209], [39.954570,54.889873], [34.273085,54.469022], [33.957447,59.203593]]
    result = point_inside_polygon(x,y, poly)
    print result
    x = 48.293495
    y = 54.600654 
    result = point_inside_polygon(x,y, poly)
    print result
