from rrt.rrg_gridmap_semantic2 import rrg_gridmap_semantic2
import carmen_maptools
from pylab import *
from sys import argv
from scipy import transpose, zeros
from scipy.cluster.hierarchy import fclusterdata
from pyTklib import tklib_euclidean_distance
from spectral_clustering import spectral_clustering_W
from spectral_clustering_utils import dists2weights_perona
import lcm
from carmen3d.seg_point_list_t import seg_point_list_t
from carmen3d.seg_point_t import seg_point_t
from carmen3d.seg_point_list_array_t import seg_point_list_array_t
from carmen3d.cluster_list_t import cluster_list_t
from carmen3d.cluster_t import cluster_t
from carmen3d.place_t import place_t
from carmen3d.place_array_t import place_array_t
from carmen3d.seg_sem_vec_list_t import seg_sem_vec_list_t
from carmen3d.seg_sem_vec_t import seg_sem_vec_t
from carmen3d.cluster_conn_t import cluster_conn_t

def publish_clusters(final_clusters):
    print "------Publishing Clusters------"
    msg = cluster_list_t()
    clusters = []

    means = []

    for c in final_clusters:
        c_msg = cluster_t()
        c_msg.cluster_id = c['cluster_id']
        c_msg.no_points = c['no_points']
        points = []
        conns = []
        
        for i in range(c['no_points']):
            pt = seg_point_t()
            con = cluster_conn_t()
            pt.pos = [c['points'][i][0], c['points'][i][1], .0]
            points.append(pt)
            if(c.has_key('conn')):
                con.no_conn = len(c['conn'][i])
                con.conn_ind = c['conn'][i]
            else:
                con.no_conn = 0
                con.conn_ind = []
            conns.append(con)

        c_msg.mean = c['mean']
        c_msg.points = points
        c_msg.connections = conns    
        
        clusters.append(c_msg)            

    msg.no_clusters = len(final_clusters)
        
    msg.clusters = clusters

    lc = lcm.LCM()
    lc.publish("CLUSTERS", msg.encode())


def test1():
    #show the rrg points
    print argv[1], arg[3]
    if("direction_floor_8_full" in argv[1]):
        myrrg = rrg_gridmap_semantic2(argv[1], argv[2], [30.0,18.0], places=arg[3])
    elif("direction_floor_1" in argv[1]):
        myrrg = rrg_gridmap_semantic2(argv[1], argv[2], [86.0,58.0], places=arg[3])
    else:
        myrrg = rrg_gridmap_semantic2(argv[1], argv[2], places=arg[3])

    vtags, myD = myrrg.get_nearest_tag_dict()
        
    mymap = myrrg.get_map()
    myrrg.create(2000)

    #show the clustering
    figure()
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    
    vtags, myD = myrrg.get_nearest_tag_dict()
    
    plt_vals = {}
    for u in vtags.keys():
        if(myD[u] == None):
            continue
        if(plt_vals.has_key(vtags[u])):
            plt_vals[vtags[u]].append(myD[u])
        else:
            plt_vals[vtags[u]] = [myD[u]]

    
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w', 'b', 'g', 'r']
    for i, key in enumerate(plt_vals.keys()):
        print plt_vals[key]
        #print transpose(plt_vals[key])
        XY = transpose(plt_vals[key])
        plot(XY[0], XY[1], colors[i]+'o')
    title("nearest clusters")

    figure()
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    means, final_clusters, pts = myrrg._cluster_regions_spectral_new(alpha=1.0, t=0.185, max_dist=0.4)
    title("clustered regions")
    plot(means[0], means[1], 'g^')

    publish_clusters(final_clusters)
    
    shapes = ['o','x','<', '>', '^','p','d']
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']

    i=0; j=0;
    for k, mypts in enumerate(pts):
        if(j >= len(colors)):
            j=0
            i+=1
        if(i >= len(shapes)):
            i = 0;
        mypts = transpose(mypts)
        #print mypts
        #print len(colors), j
        #print len(shapes), i
        plot(mypts[0], mypts[1], colors[j]+shapes[i])
        
        for m in range(len(mypts[0])):
            mean_x = means[0][k]; mean_y = means[1][k];
            plot([mean_x, mypts[0][m]], [mean_y, mypts[1][m]], colors[j]+'-')

        j+=1
    show()
    

if __name__ == "__main__":
    test1()
