from rrt.rrg_gridmap import rrg_gridmap
import carmen_maptools
from pylab import *
from sys import argv
from scipy import transpose, zeros
from scipy.cluster.hierarchy import fclusterdata
from pyTklib import tklib_euclidean_distance
from spectral_clustering import spectral_clustering_W
from spectral_clustering_utils import dists2weights_perona

def segment_map():
    
    myrrg = rrg_gridmap() 
    
    mymap = myrrg.get_map()
    
    myrrg.create(num_expansions=3000,max_size=1000)
#    myrrg.create(1200)
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    
    
    pts_XY = transpose(myrrg.nodes_pts)
    plot(pts_XY[0], pts_XY[1], 'ro')

    #show the paths generated
    figure()
    #carmen_maptools.plot_tklib_log_gridmap(mymap)
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    
    xy1 = [26.0, 22.0]  
    xy2 = [41.0, 53.0]

    D, Paths = myrrg.get_path(xy1, transpose([xy2]))
    mypath = Paths[0]
    d=D[0]
    print "distance", d
    print "path", mypath
    plot(mypath[0], mypath[1], 'ko-')

    #show a path with poses
    figure()
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
        
    xyt1 = [26.0, 22.0, .0]  
    xyt2 = [41.0, 53.0, .0]

    D, Paths = myrrg.get_path_xyth(xyt1, transpose([xyt2]))
    mypath = Paths[0]
    d=D[0]
    
    print "distance", d
    print "path", mypath
    plot(mypath[0], mypath[1], 'ko')
    
    for i, th in enumerate(mypath[2]):
        plot([mypath[0][i], mypath[0][i]+1.0*cos(th)], [mypath[1][i], mypath[1][i]+1.0*sin(th)], 'k-')
        
    #show the paths generated
    figure()
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")

    D, Paths = myrrg.get_path_xyth_interpolate(xyt1, transpose([xyt2]))
    mypath = Paths[0]
    d=D[0]
    print "distance", d
    #print "path interpolated", mypath
    plot(mypath[0], mypath[1], 'ko')
    
    for i, th in enumerate(mypath[2]):
        plot([mypath[0][i], mypath[0][i]+1.0*cos(th)], [mypath[1][i], mypath[1][i]+1.0*sin(th)], 'k-')


    #show the clustering
    figure()
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    

    means, pts = myrrg._cluster_regions_spectral(alpha=0.01)
    title("clustered regions")
    plot(means[0], means[1], 'g^')
    
    shapes = ['o','x','<', '>', '^','p','d']
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']

    i=0; j=0;
    for k, mypts in enumerate(pts):
        if(j >= len(colors)):
            j=0
            i+=1
        if(i >= len(shapes)):
            i = 0;
        mypts = transpose(mypts)
        #print mypts
        #print len(colors), j
        #print len(shapes), i
        plot(mypts[0], mypts[1], colors[j]+shapes[i])
        
        for m in range(len(mypts[0])):
            mean_x = means[0][k]; mean_y = means[1][k];
            plot([mean_x, mypts[0][m]], [mean_y, mypts[1][m]], colors[j]+'-')

        j+=1
    show()
    

if __name__ == "__main__":
    segment_map()
