from pyTklib import *
from scipy import arctan2, transpose, cos, sin, array, zeros, argmin, mean, arange, ones, size, nan
from spectral_clustering import spectral_clustering_W
from spectral_clustering_utils import dists2weights_perona
from math import *
import copy
from sys import argv
import lcm
from carmen3d.cluster_list_t import cluster_list_t
from carmen3d.cluster_t import cluster_t
from carmen3d.seg_point_t import seg_point_t
from carmen3d.cluster_conn_t import cluster_conn_t

'''def convert_to_single(map, ind):
    #return map.get_map_width()*ind[1] + ind[0]
    return 100000*ind[1] + ind[0]
    

def convert_to_double_ind(map, ind):
    i = ind %  100000#map.get_map_width()
    j = ind /  100000#map.get_map_width()
    return [i,j]'''

def convert_to_single(map, ind):
    return map.get_map_width()*ind[1] + ind[0]

    

def convert_to_double_ind(map, ind):
    i = ind %  map.get_map_width()
    j = ind /  map.get_map_width()
    return [i,j]


def get_neigh(map, ind):
    print ""

def filter_map(map):
    ignored_count = 0

    occ_locs = array(map.get_occupied_locations())
    occ_inds = array(map.get_occupied_inds())
    
    occ_ind_list = []

    for i in range(len(occ_inds[0])):
        curr_ind = [occ_inds[0,i], occ_inds[1,i]]
        occ_ind_list.append(convert_to_single(map,curr_ind))#(curr_ind)

    #print occ_ind_list
    occ_ind_set = set(occ_ind_list)
    
    obstacles = []

    for i in range(len(occ_inds[0])):
        print "Ind : " , i , "/ ", len(occ_inds[0])
        
        neigh = []
        for j in range(-1,2):
            for k in range(-1,2):
                if(j==0 and k== 0):
                    continue
                offset = array([j,k])
                check_ind = convert_to_single(map,occ_inds[:,i] + offset)
                neigh.append(check_ind)
        
        neigh_set = set(neigh)

        inter = occ_ind_set.intersection(neigh_set)
                #if(map.ind_occupied(check_ind)):

        if(not inter): #there are no surrounding obstacle
            #curr_obs = []
            print "Idel Obs" 
            ignored_count += 1
            #for j in neigh:
            #    curr_obs.append(j)
        else:
            #check if there are obstacle clusters
            #full_points = copy.copy(neigh)
            full_points = copy.copy(list(inter))
            full_points.append(convert_to_single(map,occ_inds[:,i]))
            conn_clusters = []
            connected = False

            if(len(obstacles)>0):
                to_merge_cluster = []
                for k_ind, k in enumerate(obstacles):
                    if(set(k).intersection(set(full_points))):
                        print "Found Obstacle group"
                        conn_clusters.append(k_ind)
                        to_merge_cluster.append(k)
                        #k.extend(full_points)  #will cause duplication
                
                if(len(conn_clusters) ==1):
                    print "Found only one cluster - Adding all points"
                    obstacles[conn_clusters[0]].extend(full_points)
                    connected = True
                elif(len(conn_clusters) >1):
                    print "Found more than one connected cluster - merging"
                    
                    for l in to_merge_cluster:
                        obstacles.remove(l)

                    merged_cluster = []

                    for l in to_merge_cluster:
                        merged_cluster.extend(l)
                    
                    print "Merged Cluster" 
                    #print merged_cluster

                    already_in_cluster = set(merged_cluster).intersection(set(full_points))

                    to_add = set(already_in_cluster).symmetric_difference(set(full_points))
                    
                    print "Adding to merged cluster", to_add

                    for l in to_add:                        
                        #print l
                        merged_cluster.extend([l])

                    #print "Full merged Cluster"
                    #print merged_cluster
                    connected = True
                    obstacles.append(merged_cluster)

            if(not connected):
                print "No matching cluster found"
                print "Adding to the cluster"
                obstacles.append(full_points)


            #print "=========Obstacles================="
            #for k in obstacles:
            #    print len(k)
            print "==================================="
                
    print "Done Clustering"
    for k_ind, k in enumerate(obstacles):
        print "Size : " , len(k)
        #print k     
    print "Total Number of Obstacles : ", len(obstacles) 
    print "Ignored Points : " , ignored_count

    msg = cluster_list_t()
    clusters = []
    for k_ind, k in enumerate(obstacles):
        c_msg = cluster_t()
        c_msg.cluster_id = k_ind
        c_msg.no_points = len(k)
        points = []
        conns = []

        c_sum = [.0,.0]
        
        for j in k:
            curr_ind = convert_to_double_ind(map, j)
            curr_pt = map.to_xy(curr_ind)
            pt = seg_point_t()
            pt.pos = [curr_pt[0], curr_pt[1], .0]
            c_sum[0] += curr_pt[0]
            c_sum[1] += curr_pt[1]
            points.append(pt)
            con = cluster_conn_t()
            con.no_conn = 0
            con.conn_ind = []
            conns.append(con)
            
        c_sum[0] = c_sum[0] / len(k)
        c_sum[1] = c_sum[1] / len(k)
        c_msg.mean = c_sum
        c_msg.points = points
        c_msg.connections = conns    
        clusters.append(c_msg) 
    msg.no_clusters = len(obstacles)
    msg.clusters = clusters

    lc = lcm.LCM()
    lc.publish("CLUSTERS", msg.encode())

def test():
    map = tklib_log_gridmap()
    print "Map Location : " , argv[1]
    map.load_carmen_map(argv[1])
    print "Map : Loaded"
    filter_map(map)

if __name__ == "__main__":
    test()
