import numpy

class memoized(object):
    """
       source: http://wiki.python.org/moin/PythonDecoratorLibrary#Memoize
       Decorator that caches a function's return value each time it is called.
       If called later with the same arguments, the cached value is returned, and
       not re-evaluated.
       """ 
    def __init__(self, func):
        self.func = func
        self.cache = {}
    def __call__(self, *args):
        try:
            return self.cache[args]
        except KeyError:
            self.cache[args] = value = self.func(*args)
            return value
        except TypeError:
            # uncachable -- for instance, passing a list as an argument.
            # Better to not cache than to blow up entirely.
            return self.func(*args)
    def __repr__(self):
        """Return the function's docstring."""
        return self.func.__doc__





"""
From
http://code.activestate.com/recipes/325205/	
Works on instance methods. 
"""

def memoize(function):
    return Memoize(function)

def hashable(x):
    if isinstance(x, list) or isinstance(x, tuple) or isinstance(x, numpy.ndarray):
        return tuple([hashable(o) for o in x])
    elif isinstance(x, dict):
        return tuple([hashable(o) for o in x.iteritems()])
    elif isinstance(x, set):
        return frozenset(x)
    else:
        return x
   

class MemoizeInstance(object):
    def __init__(self, fn):
        self.cache={}
        self.fn=fn

    def __get__(self, instance, cls=None):
        self.instance = instance
        return self

    def __call__(self,*args):
        key = hashable(args)
        try:
            if self.cache.has_key(key):
                return self.cache[key]
            else:
                object = self.cache[key] = self.fn(self.instance, *args)
                return object
        except:
            print "utilities/memoized.py: raised error with args:", args
            raise
             

def memoizeInstance(function):
    return MemoizeInstance(function)


class Memoize(object):
    """
    Memoize an instance method, using object identity rather than
    equality for the first self argument.  That should be faster than
    using hash based stuff if the object is complex.
    """
    def __init__(self, fn):
        self.cache={}
        self.fn=fn

    def __get__(self, instance, cls=None):
        self.instance = instance
        return self

    def __call__(self,*args):
        try:
            selfArg = args[0]
            key = (id(selfArg),) + hashable(args[1:])

            if self.cache.has_key(key):
                return self.cache[key]
            else:
                object = self.cache[key] = self.fn(self.instance, *args)
                return object
        except:
            print "utilities/memoized.py: raised error with args:", args
            raise
