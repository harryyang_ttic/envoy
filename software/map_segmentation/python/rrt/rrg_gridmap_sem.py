from pyTklib import *
from scipy import arctan2, transpose, cos, sin, array, zeros, argmin, mean, arange, ones
from spectral_clustering import spectral_clustering_W
from spectral_clustering_utils import dists2weights_perona
from math import *
import lcm
from carmen3d.seg_point_list_t import seg_point_list_t
from carmen3d.seg_point_t import seg_point_t
from carmen3d.seg_point_list_array_t import seg_point_list_array_t
from carmen3d.cluster_list_t import cluster_list_t
from carmen3d.cluster_t import cluster_t
from carmen3d.place_t import place_t
from carmen3d.place_array_t import place_array_t

class rrg_graph:
    def __init__(self, init_loc, places=None):
        tklib_init_rng(0)
        self.root_I = 0
        self.nodes_pts = [init_loc]
        self.places = places
        
        self.semantic_vec = []
        #add semantic vector if places are loaded
        if(self.places !=None):
            #publish the place locations 
            lc = lcm.LCM()
            msg = place_array_t()
            msg.no_places = len(self.places)
            place_array = []
            for i in self.places:
                pl = place_t()
                pl.label = i['name']
                pl.no_locs = len(i['loc'])
                locs = []
                for j in i['loc']:
                  loc = seg_point_t()  
                  loc.pos = [j[0], j[1], .0]
                  locs.append(loc)
                pl.locs = locs
                place_array.append(pl)

            msg.places = place_array

            lc.publish("PLACE_LOCATIONS", msg.encode())
            self.semantic_vec = [self.get_sem_vector(init_loc)]
        self.graph = {}
        self.expansion_dist = 1.0
    
    def get_semantic_dist(self,v1, v2):
        temp_total = 0
        
        for i in range(len(v1)):
            temp_total += pow(v1[i] - v2[i],2)

        return sqrt(temp_total)

    def get_sem_vector(self, node_loc):

        map = self.get_map()
        visibility = []

        min_dist = 10000
        min_ind = -1

        temp_label_dist = 10000*ones(len(self.places))

        for l in range(len(self.places)):
            found = 0
            #search through all the tagged positions for each location
            #to see if they are visible and within the perimeter
            for k in self.places[l]['loc']:

                theta = arctan2(k[1]-node_loc[1], k[0]-node_loc[0])
                d, = map.ray_trace(node_loc[0], node_loc[1], [theta])
                d_true = tklib_euclidean_distance(node_loc, k);

                if d >= d_true and d_true < 10.0:  
                    #maybe add a distance metric as well i.e. within a certain distance
                    #visibility.append(1)
                    temp_label_dist[l] = d_true 
                    found = 1
                
                    if min_dist > d_true:
                        min_ind = l
                        min_dist = d_true

                    #shouldn;t stop the loop as we are also trying to find the closest landmark

            if found == 0:
                visibility.append(0)
            else:
                visibility.append(1)

        return visibility

    def expand_graph(self):
        map = self.get_map()
        
        sloc = map.get_random_open_location(0.0)
        
        ###***********No of locations*******###
        no_neighbours = 100

        #get nearest neighbors (from_loc)
        I_nn = kNN_index(sloc, transpose(self.nodes_pts), 
                         min(no_neighbours, len(self.nodes_pts)));
        i = I_nn[0]
        from_loc = self.nodes_pts[int(i)]

        #get the orientation from the nearest neighbor and 
        #    ray trace to see if the path is free
        theta = arctan2(sloc[1]-from_loc[1], sloc[0]-from_loc[0])
        d, = map.ray_trace(from_loc[0], from_loc[1], [theta])
        d_true = tklib_euclidean_distance(from_loc, sloc);

        #if the path is free then extend by that length 
        if(d > d_true):
            d=d_true
        else:
            return
            #if(d < self.expansion_dist):
            #    return
            #else:
            #    d = self.expansion_dist

        #get the new node and add it to the set of nodes
        new_node_xy = [from_loc[0]+d*cos(theta), from_loc[1]+d*sin(theta)]
        self.nodes_pts.append(new_node_xy)

        #add the semantic vector for this new node if necessary
        if(self.places !=None):
            self.semantic_vec.append(self.get_sem_vector(new_node_xy))

        #I_nn = kNN_index(new_node_xy, transpose(self.nodes_pts), 
        #                 min(no_neighbours, len(self.nodes_pts)));

        #now add the relevant connections to the neighbors
        for i in I_nn:
            theta = arctan2(self.nodes_pts[int(i)][1]-new_node_xy[1], 
                            self.nodes_pts[int(i)][0]-new_node_xy[0])
            
            d, = map.ray_trace(new_node_xy[0], new_node_xy[1], [theta])
            d_true = tklib_euclidean_distance(new_node_xy, self.nodes_pts[int(i)]);
            
            if(d > d_true):
                if(self.graph.has_key(int(i))):
                    self.graph[int(i)].add((len(self.nodes_pts)-1))
                else:
                    self.graph[int(i)] = set([(len(self.nodes_pts)-1)])
                
                if(self.graph.has_key(len(self.nodes_pts)-1)):
                    self.graph[len(self.nodes_pts)-1].add(int(i))
                else:
                    self.graph[len(self.nodes_pts)-1] = set([int(i)])

    def get_path_xyth_interpolate(self, pose1, poses_XYTh, use_interpolated_dest_orientation=False):
        
        D, Paths = self.get_path_xyth(pose1, poses_XYTh)
        
        Paths_Int = []
        for j in range(len(D)):
            path=Paths[j]
            #X_r = [0,0]; Y_r = [0,0.1]; Th_r = [0,0.1];
            X_r = [pose1[0]]; Y_r = [pose1[1]]; Th_r = [pose1[2]];

            for i in range(1,len(path[0])):
                x1, y1, th1 = path[:,i-1]
                x2, y2, th2 = path[:,i]

                if(x1 == x2 and y1 == y2):
                    X, Y, Th =self.interpolate_theta(x1, y1, th1, th2, 0.0174)
                elif(th1 == th2):
                    X, Y, Th =self.interpolate_path(x1, y1, x2, y2, th1, 0.1)
                else:
                    print "path should not exist"
                    raise 
                
                X_r.extend(X); Y_r.extend(Y); Th_r.extend(Th);
            Paths_Int.append([X_r, Y_r, Th_r])
        
        return D, Paths_Int

    def interpolate_theta(self, x, y, th1, th2, angle):
        #print "thdiff:", th2-th1
        #print "norm thdiff:", tklib_normalize_theta(th2-th1)
        
        if(tklib_normalize_theta(th2-th1) >= 0):
            Th = arange(0, tklib_normalize_theta(th2-th1), angle)+th1
        else:
            Th = arange(0, tklib_normalize_theta(th2-th1), -1.0*angle)+th1
        
        X = zeros(len(Th))*1.0 + x; 
        Y = zeros(len(Th))*1.0 + y;

        return [X, Y, Th]
    
    def interpolate_path(self, x1, y1, x2, y2, th1, dist):
        d = tklib_euclidean_distance([x1,y1], [x2,y2])
        th = arctan2(y2-y1, x2-x1)
        
        D = arange(0, d, dist)
        Th = ones(len(D))*1.0*th
        
        X = D*cos(Th)+x1; Y=D*sin(th)+y1;

        return [X, Y, Th]

    def get_path_xyth(self, pose1, poses_XYTh):
        D, paths_XY = self.get_path(pose1[0:2], poses_XYTh[0:2,:])
        
        paths_XYTh = []
        for j in range(len(D)):
            path = paths_XY[j]

            th1 = arctan2(path[1,0]-pose1[1], path[0,0]-pose1[0])
            X = [pose1[0], pose1[0]]; Y = [pose1[1], pose1[1]]; Th = [pose1[2], th1];
        
            for i in range(len(path[0])):
                o_th = Th[-1]
                X.append(path[0,i]); Y.append(path[1,i]); Th.append(o_th);
            
                if(i < len(path[0])-1):
                    n_th = arctan2(path[1,i+1]-path[1,i], path[0,i+1]-path[0,i]);
                    X.append(path[0,i]); Y.append(path[1,i]); Th.append(n_th);
            
            pose2 = poses_XYTh[:,j]
            th_end = arctan2(pose2[1]-Y[-1], pose2[0]-X[-1])
            X.extend([X[-1], pose2[0], pose2[0]]); 
            Y.extend([Y[-1], pose2[1], pose2[1]]); 
            Th.extend([th_end, th_end, pose2[2]]);
            paths_XYTh.append(array([X,Y,Th]))
            
        return D, paths_XYTh            

    #need to fill out
    def get_path(self, xy1, dests_XY):
        #get the relevant indices
        i1, = kNN_index(xy1, transpose(self.nodes_pts), 1)
        
            
        #initialize the various datastructures
        dist = zeros(len(self.nodes_pts))+10.0e100
        dist[i1] = 0
        prev = zeros(len(self.nodes_pts))-1
        
        Q = range(len(self.nodes_pts))
        
        while(len(Q) > 0):        
            i = argmin(dist.take(Q));
            u = Q[i]
            
            if(dist[u] > 10.0e99):
                break

            Q.remove(u)
            
            for v in self.graph[u]:
                d_alt = dist[u]+tklib_euclidean_distance(self.nodes_pts[u], 
                                                         self.nodes_pts[v])
                if(d_alt < dist[v]):
                    dist[v] = d_alt
                    prev[v] = u


        I = []
        for j in range(len(dests_XY[0])):
            I.append(kNN_index(dests_XY[:,j], transpose(self.nodes_pts), 1)[0])

        D = dist.take(I)
        
        paths_XY = []
        for i_dst in I:
            path = [int(i_dst)]
            while(prev[path[-1]]!=-1):
                path.append(int(prev[path[-1]]))

            #print path
            #paths_XY.append(transpose(array(self.nodes_pts).take(list(reversed(path)), axis=0)))
            paths_XY.append(transpose([self.nodes_pts[i] for i in reversed(path)]))
        
        return D, paths_XY



                    
class rrg_gridmap(rrg_graph):
    def __init__(self, map_filename, init_loc=None, places=None):
        tklib_init_rng(0);
        
        self.map_filename = map_filename
        
        self.map = None
        self.map = self.get_map()
        
        if(init_loc == None):
            self.init_loc = self.map.get_random_open_location(0.5)
        else:
            self.init_loc = init_loc

        rrg_graph.__init__(self, self.init_loc, places)
        
    def get_map(self):
        if(self.map == None):
            self.map = tklib_log_gridmap()
            self.map.load_carmen_map(self.map_filename)

        return self.map

    def create(self, num_expansions=100):
        map = self.get_map()
        
        for i in range(num_expansions):
            self.expand_graph()        


    def get_distance_matrix(self):
        print "Size of Dist matrix " , len(self.graph.keys()), len(self.nodes_pts)

        dists = zeros([len(self.nodes_pts), len(self.nodes_pts)])-1.0

        for i, u in enumerate(self.graph.keys()):
            for j, v in enumerate(self.graph[u]):
                #distance is caluclated only for the visible nodes
                dists[u,v] = tklib_euclidean_distance(self.nodes_pts[u], self.nodes_pts[v])
                dists[v,u] = dists[u,v]

        return dists

    def get_distance_matrix_semantic(self, places=None):
        print "Size of Dist matrix " , len(self.graph.keys()), len(self.nodes_pts)
        lc = lcm.LCM()

        dists = zeros([len(self.nodes_pts), len(self.nodes_pts)])-1.0
        
        msg = seg_point_list_array_t()
        msg.no_pts = len(self.graph)
        points = []

        for i, u in enumerate(self.graph.keys()):

            #lcm message
            seg_list = seg_point_list_t()
            seg_list.curr_point = seg_point_t()
            seg_list.curr_point.pos = [self.nodes_pts[u][0], self.nodes_pts[u][1],.0]
            seg_list.no_conn_pts = len(self.graph[u])
            conn_pts = []
            seg_list.sem_dist = []
            seg_list.metric_dist = []
            seg_list.comb_dist = []
            

            for j, v in enumerate(self.graph[u]):
                pt = seg_point_t()
                pt.pos = [self.nodes_pts[v][0], self.nodes_pts[v][1],.0]
                conn_pts.append(pt)

                #distance is caluclated only for the visible nodes
                
                met_dist = tklib_euclidean_distance(self.nodes_pts[u], self.nodes_pts[v])
                sem_dist = tklib_euclidean_distance(self.semantic_vec[u], self.semantic_vec[v])
                

                beta = 2.0/3.0

                if(sum(self.semantic_vec[u]) == 0 and sum(self.semantic_vec[v]) == 0):
                    beta = 0.5

                comb_dist = beta * sem_dist + (1- beta) * met_dist

                #add to lcm message
                seg_list.sem_dist.append(sem_dist)
                seg_list.metric_dist.append(met_dist)
                seg_list.comb_dist.append(comb_dist)

                dists[u,v] = comb_dist
                dists[v,u] = dists[u,v]
                
            seg_list.conn_pts = conn_pts
            points.append(seg_list)

        #publish lcm message
        msg.points = points
        lc.publish("SEG_WEIGHTS_ARRAY", msg.encode())
        return dists

    #cluster the regions and return the centroid and labels
    def cluster_regions_spectral(self, alpha=0.5):
        print "get distances"
        dists = self.get_distance_matrix()
        
        print "do spectral clustering"
        W = dists2weights_perona(dists, alpha)
        labels, k = spectral_clustering_W(W,None,numkmeans=1,seed_number=0)
        labels = labels[0]
        
        print "postprocess data and return"
        class_to_I = {}
        for i, c in enumerate(labels):
            if(class_to_I.has_key(int(c))):
                class_to_I[int(c)].append(i)
            else:
                class_to_I[int(c)] = [i]

        #get the centroid points and return these as a representative of the 
        # region
        means = []
        for c in class_to_I:
            pts = array(self.nodes_pts).take(class_to_I[c], axis=0)
            
            mpt = mean(pts, axis=0)

            #get the nearest points to each of the mean points
            i, = kNN_index(mpt, transpose(pts), 1)
            means.append(pts[int(i)])
        means = transpose(means)
        
        return means

    #cluster the regions and return the centroid and labels
    def cluster_regions_spectral_semantic(self, alpha=0.5):#, places=None):
        print "++++Get distances++++"
            
        if(self.places==None):
            print "Basic Distances"
            dists = self.get_distance_matrix()
        else:
            print "Semantic Distance"
            dists = self.get_distance_matrix_semantic()
            for i in self.places:
                print i
        
        print "do spectral clustering"
        W = dists2weights_perona(dists, alpha)
        labels, k = spectral_clustering_W(W,None,numkmeans=1,seed_number=0)
        labels = labels[0]
        
        
        print "postprocess data and return"
        class_to_I = {}
        for i, c in enumerate(labels):
            if(class_to_I.has_key(int(c))):
                class_to_I[int(c)].append(i)
            else:
                class_to_I[int(c)] = [i]

        #get the centroid points and return these as a representative of the region
        msg = cluster_list_t()
        clusters = []

        means = []
        for c in class_to_I:
            c_msg = cluster_t()
            pts = array(self.nodes_pts).take(class_to_I[c], axis=0)
            c_msg.cluster_id = c
            c_msg.no_points = len(pts)
            points = []
            for i in range(len(pts)):
                pt = seg_point_t()
                pt.pos = [pts[i][0], pts[i][1], .0]
                points.append(pt)
            mpt = mean(pts, axis=0)
            c_msg.mean = [mpt[0], mpt[1]]
            c_msg.points = points
            
        
            clusters.append(c_msg)

            #get the nearest points to each of the mean points
            i, = kNN_index(mpt, transpose(pts), 1)
            means.append(pts[int(i)])

        means = transpose(means)

        msg.no_clusters = len(class_to_I)
        
        msg.clusters = clusters

        lc = lcm.LCM()
        lc.publish("CLUSTERS", msg.encode())
        
        return means
