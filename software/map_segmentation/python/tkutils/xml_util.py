from sys import argv
import platform

#this is a file for reading xml files

if(int(platform.python_version_tuple()[1]) <= 4):
    from elementtree.ElementTree import *
else:
    from xml.etree.ElementTree import *


def parse_xml(xml_filename):
    rss = parse(xml_filename).getroot()

    roothash={}
    roothash[rss.tag]=parse_xml_recurse(rss)
    return roothash

def parse_xml_recurse(element):
    if(len(element.getchildren()) == 0):
        return element.text
    
    myhash = {}
    for newelt in element.getchildren():
        if(not myhash.has_key(newelt.tag)):
            myhash[newelt.tag] = parse_xml_recurse(newelt)
        else:
            if(not isinstance(myhash[newelt.tag], list)):
                tmpelt = myhash[newelt.tag] 
                myhash[newelt.tag] = [tmpelt]
            myhash[newelt.tag].append(parse_xml_recurse(newelt))

    return myhash

def save_xml(myhash, outfilename):
    ofile = open(outfilename, 'w')
    save_xml_recurse(myhash, ofile, 0)

def save_xml_recurse(myhash, ofile, depth):

    for elt in myhash.keys():
        if(isinstance(myhash[elt], dict)):
            ofile.write(("\t"*depth)+"<"+str(elt)+">\n")
            save_xml_recurse(myhash[elt], ofile, depth+1) 
            ofile.write(("\t"*depth)+"</"+str(elt)+">\n");
        elif(isinstance(myhash[elt], list)):
            for myelt in myhash[elt]:
                ofile.write(("\t"*depth)+"<"+str(elt)+">\n")
                save_xml_recurse(myelt, ofile, depth+1) 
                ofile.write(("\t"*depth)+"</"+str(elt)+">\n");
        else:
            ofile.write(("\t"*depth)+"<"+str(elt)+">")
            ofile.write(str(myhash[elt]))
            ofile.write("</"+str(elt)+">\n");


if __name__=="__main__":
    if(len(argv) == 2):
        myhash = parse_xml(argv[1])
        save_xml(myhash, 'test.xml')
        
        for elt in myhash.keys():
            if(elt == 'object'):
                for myelt in myhash[elt]:
                    print myelt
            else:
                print elt, myhash[elt]
    else:
        print "usage:\n\t python voc_tools.py filename"

