add_definitions(
  -Wall -g -O3 -DNDEBUG -Wno-unused
  -Wno-variadic-macros 
  -DVL_LOWE_STRICT 
  -DVL_USEFASTMATH
  )

add_executable (sift-extractor main.cpp)

set(REQUIRED_LIBS sift)

pods_use_pkg_config_packages(sift-extractor ${REQUIRED_LIBS})

pods_install_executables(sift-extractor)
