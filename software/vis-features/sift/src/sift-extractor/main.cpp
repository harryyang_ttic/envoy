/* This module computes SIFT features given LCM images as input.
 * It is based on SIFT++ (now known as Vlfeat), by Andrea Vedaldi.
 * http://www.vlfeat.org/
 *
 */

#include<string>
#include<iostream>
#include<iomanip>
#include<fstream>
#include<sstream>
#include<algorithm>
#include <glib.h>

#include <GL/gl.h>

#include <sift/sift.hpp>

extern "C" {
#include<getopt.h>
#if defined (VL_MAC)
#include<libgen.h>
#else
#include<string.h>
#endif
#include<assert.h>
}

#include <bot_core/bot_core.h>
//#include <image_utils/jpeg.h>
//#include <image_utils/pixels.h>
//#include "pngload.h"
#include <signal.h>

#define DEFAULT_LEVELS 3
#define DEFAULT_SIGMA_INIT 1.6
#define DEFAULT_PEAK_THRESH 0.15
#define DEFAULT_DOUBLEIMSIZE 0
#define DEFAULT_SCALE_FACTOR 1.0
#define DEFAULT_VERBOSE 0
#define DEFAULT_DEBUG 0
#define DEFAULT_DRAW_LCMGL 0

#ifndef ABS
#define ABS(x)    (((x) > 0) ? (x) : (-(x)))
#define MAX(x,y)  (((x) > (y)) ? (x) : (y))
#define MIN(x,y)  (((x) < (y)) ? (x) : (y))
#endif

using namespace std ;


static char * bool2str(bool in) {
    if (1 == in) {
        return (char*)"enabled";
    } else {
        return (char*)"disabled";
    }
}

static void usage()
{
    char *s_doubleimresize = bool2str(DEFAULT_DOUBLEIMSIZE);
    char *s_debug = bool2str(DEFAULT_DEBUG);
    char *s_verbose = bool2str(DEFAULT_VERBOSE);
    fprintf (stderr, "usage: sift [options]\n"
             "\n"
             "  -h, --help              shows this help text and exits\n"
             "  -c, --channel           listen to images on this channel\n"
             "  -o, --output-channel    publish sift features on this channel\n"
             "                            (default <input_channel>_SIFT)\n"
             "  -l, --levels            number of levels per octave (default %d)\n"
             "  -s, --sigma             sigma for smoothing (default %.2lf)\n"
             "  -p, --peakthresh        Peak threshold for min-max suppression (default %.2lf)\n"
             "  -d, --doubleimsize      To double image size up front (default %s)\n"
             "  -x, --scalefactor       To scale images (0.0-1.0, default %.2lf)\n"
             "  -g, --debug             Save images to file (default %s)\n"
             "  -n, --draw              Draw LCMGL\n"
             "  -v, --verbose           be verbose (default %s)\n",
             DEFAULT_LEVELS, DEFAULT_SIGMA_INIT, DEFAULT_PEAK_THRESH,
             s_doubleimresize, DEFAULT_SCALE_FACTOR, s_debug, s_verbose
            );
}

static void
parse_opts(int argc, char *argv[], int *levels, double *sigma_init,
           double *peak_thresh, int *doubleimsize, double *scale_factor,
           gboolean *verbose, char **channel_in, char **channel_out,
           gboolean *debug, gboolean *draw_lcmgl, gboolean *input_kinect)
{
    
    char *optstring = (char*)"hvgl:s:p:dx:c:o:nk";
    char c;
    struct option long_opts[] = {
        {"help", no_argument, 0, 'h'},
        {"verbose", no_argument, 0, 'v'},
        {"debug", no_argument, 0, 'g'},
        {"channel", required_argument, 0, 'c'},
        {"output-channel", required_argument, 0, 'o'},
        {"levels", required_argument, 0, 'l'},
        {"sigma", required_argument, 0, 's'},
        {"peakthresh", required_argument, 0, 'p'},
        {"scalefactor", required_argument, 0, 'x'},
        {"draw", required_argument, 0, 'n'},
        {"doubleimsize", no_argument, 0, 'd'},
        {0, 0, 0, 0}};

    while ((c = getopt_long (argc, argv, optstring, long_opts, 0)) >= 0)
    {
        switch (c) 
            {
            case 'c':
                *channel_in = strdup (optarg);
                break;
            case 'k':
                *input_kinect = 1;
                break;
            case 'o':
                *channel_out = strdup (optarg);
                break;
            case 'l':
                *levels = atoi (optarg);
                break;
            case 's':
                *sigma_init = atof (optarg);
                break;
            case 'n':
                *draw_lcmgl = 1;
                break;
            case 'p':
                *peak_thresh = atof (optarg);
                break;
            case 'd':
                *doubleimsize=1;
                break;
            case 'x':
                *scale_factor = atof (optarg);
                break;
            case 'v':
                *verbose=1;
                break;
            case 'g':
                *debug=1;
                break;
            case 'h':
            default:
                usage();
                exit(1);
        }
    }
}

void sigproc(int foo)
{
    std::cout << "Exiting." << std::endl;
    exit(0);
}

int main (int argc, char *argv[])
{
    setlinebuf(stdout);
    signal(SIGINT, sigproc);
    int levels=DEFAULT_LEVELS;
    double sigma_init=DEFAULT_SIGMA_INIT;
    double peak_thresh=DEFAULT_PEAK_THRESH;
    int doubleimsize=DEFAULT_DOUBLEIMSIZE;
    double scale_factor=DEFAULT_SCALE_FACTOR;
    gboolean verbose = DEFAULT_VERBOSE;
    char *channel_in = NULL;
    char *channel_out = NULL;
    gboolean debug = DEFAULT_DEBUG;
    gboolean draw_lcmgl = DEFAULT_DRAW_LCMGL;
    gboolean input_kinect = 0;

    parse_opts(argc, argv, &levels, &sigma_init, &peak_thresh,
               &doubleimsize, &scale_factor, &verbose, &channel_in,
               &channel_out, &debug, &draw_lcmgl, &input_kinect);

    if (!channel_in) {
        fprintf (stderr, "Error: missing input channel.\n");
        usage();
        return -1;
    }

    sift_state_t *sift =
        sift_state_create(channel_in, channel_out, levels, sigma_init,
                          peak_thresh, doubleimsize, scale_factor, verbose,
                          debug, draw_lcmgl,input_kinect);

    printf ("Running sift input channel %s, output channel %s.\n"
            "        Levels = %d, Sigma = %.3f, Peak thresh = %.4f,\n"
            "        Double image size: %d, Scale factor: %.2f\n",
            channel_in, sift->output_channel, levels, sigma_init, peak_thresh,
            doubleimsize, scale_factor);

    GMainLoop *mainloop;
    mainloop = g_main_loop_new(NULL, FALSE);
    g_main_loop_run (mainloop);

    sift_state_destroy(sift);
    
    return 0;

}

