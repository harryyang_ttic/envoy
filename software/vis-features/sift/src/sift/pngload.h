#ifndef _PNG_LOAD_H__
#define _PNG_LOAD_H__

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include <png.h>

void png_get_info(const char* fileName, int *width, int *height, int *channels );
void png_write_fname (const char *filename, unsigned char *img, int width, int height, int nchannels);

#endif
