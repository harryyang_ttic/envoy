/* This module computes SIFT features given LCM images as input.
 * It is based on SIFT++ (now known as Vlfeat), by Andrea Vedaldi.
 * http://www.vlfeat.org/
 *
 */
#include "sift.hpp"

#include<string>
#include<iostream>
#include<iomanip>
#include<fstream>
#include<sstream>
#include<algorithm>
#include <glib.h>

#include <GL/gl.h>

extern "C" {
#include<getopt.h>
#if defined (VL_MAC)
#include<libgen.h>
#else
#include<string.h>
#endif
#include<assert.h>
}

#include <bot_core/bot_core.h>
#include <image_utils/pixels.h>
#include <image_utils/jpeg.h>
#include "pngload.h"
#include <iostream>
#include <fstream>

using namespace std ;

size_t const not_found = numeric_limits<size_t>::max() - 1 ;

/** @brief Case insensitive character comparison
 **
 ** This predicate returns @c true if @a a and @a b are equal up to
 ** case.
 **
 ** @return predicate value.
 **/
    inline
bool ciIsEqual(char a, char b)
{
    return 
        tolower((char unsigned)a) == 
        tolower((char unsigned)b) ;
}

/** @brief Case insensitive extension removal
 **
 ** The function returns @a name with the suffix $a ext removed.  The
 ** suffix is matched case-insensitve.
 **
 ** @return @a name without @a ext.
 **/
    string
removeExtension(string name, string ext)
{
    string::iterator pos = 
        find_end(name.begin(),name.end(),ext.begin(),ext.end(),ciIsEqual) ;

    // make sure the occurence is at the end
    if(pos+ext.size() == name.end()) {
        return name.substr(0, pos-name.begin()) ;
    } else {
        return name ;
    }
}


/** @brief Insert descriptor into stream
 **
 ** The function writes a descriptor in ASCII/binary format
 ** and in integer/floating point format into the stream.
 **
 ** @param os output stream.
 ** @param descr_pt descriptor (floating point)
 ** @param binary write binary descriptor?
 ** @param fp write floating point data?
 **/
    std::ostream&
insertDescriptor(std::ostream& os,
        VL::float_t const * descr_pt,
        bool binary,
        bool fp )
{
#define RAW_CONST_PT(x) reinterpret_cast<char const*>(x)
#define RAW_PT(x)       reinterpret_cast<char*>(x)

    if( fp ) {

        /* convert to 32 bits floats (single precision) */
        VL::float32_t fdescr_pt [128] ;
        for(int i = 0 ; i < 128 ; ++i)
            fdescr_pt[i] = VL::float32_t( descr_pt[i]) ;

        if( binary ) {
            /* 
               Test for endianess. Recall: big_endian = the most significant
               byte at lower memory address.
               */
            short int const word = 0x0001 ;
            bool little_endian = RAW_CONST_PT(&word)[0] ;

            /* 
               We save in big-endian (network) order. So if this machine is
               little endiand do the appropriate conversion.
               */
            if( little_endian ) {
                for(int i = 0 ; i < 128 ; ++i) {
                    VL::float32_t tmp = fdescr_pt[ i ] ;        
                    char* pt  = RAW_PT(fdescr_pt + i) ;
                    char* spt = RAW_PT(&tmp) ;
                    pt[0] = spt[3] ;
                    pt[1] = spt[2] ;
                    pt[2] = spt[1] ;
                    pt[3] = spt[0] ;
                }
            }            
            os.write( RAW_PT(fdescr_pt), 128 * sizeof(VL::float32_t) ) ;

        } else {

            for(int i = 0 ; i < 128 ; ++i) 
                os << ' ' 
                    << fdescr_pt[i] ;
        }

    } else {

        VL::uint8_t idescr_pt [128] ;

        for(int i = 0 ; i < 128 ; ++i)
            idescr_pt[i] = uint8_t(float_t(512) * descr_pt[i]) ;

        if( binary ) {

            os.write( RAW_PT(idescr_pt), 128) ;	

        } else { 

            for(int i = 0 ; i < 128 ; ++i) 
                os << ' ' 
                    << uint32_t( idescr_pt[i] ) ;
        }
    }
    return os ;
}

/* keypoint list */
typedef vector<pair<VL::Sift::Keypoint,VL::float_t> > Keypoints ;

/* predicate used to order keypoints by increasing scale */
bool cmpKeypoints (Keypoints::value_type const&a,
        Keypoints::value_type const&b) {
    return a.first.sigma < b.first.sigma ;
}

int compare_keypoints_descending_abs (const void *a, const void *b)
{
    const sift_point_feature_t *da = (const sift_point_feature_t *) a;
    const sift_point_feature_t *db = (const sift_point_feature_t *) b;
     
    return (fabs(db->value) - fabs(da->value));
}
// -------------------------------------------------------------------
//                                                             library
// -------------------------------------------------------------------
    sift_point_feature_list_t*
get_keypoints (float *data, int width, int height,
        int levels, double sigma_init, 
        double peak_thresh, int doubleimsize, int64_t utime)
{
    printf ("width, height: %d x %d, double: %d, peak: %.2lf\n", width, height, doubleimsize, peak_thresh);

    int    first          = doubleimsize ? -1 : 0 ; // set this to -1 if you want to upsample the image
    int    octaves        = -1 ;
    //  int    levels         = 3 ;
    float  threshold      = peak_thresh / levels / 2;
    float  edgeThreshold  = 10.0f;
    float  magnif         = 3.0 ;
    int    noorient       = 0 ;
    int    haveKeypoints  = 0 ;
    int    unnormalized   = 0 ;
    string outputFilenamePrefix ;
    string outputFilename ;
    string descriptorsFilename ;
    string keypointsFilename ;

    // -----------------------------------------------------------------
    //                                            Loop over input images
    // -----------------------------------------------------------------      
    VL::PgmBuffer buffer ;

    // set buffer data
    buffer.width  = width ;
    buffer.height = height ;
    buffer.data   = data ;

    // ---------------------------------------------------------------
    //                                            Gaussian scale space
    // ---------------------------------------------------------------    
    int         O      = octaves ;    
    int const   S      = levels ;
    int const   omin   = first ;
    float const sigman = .5 ;
    float const sigma0 = sigma_init * powf(2.0f, 1.0f / S) ;

    // optionally autoselect the number number of octaves
    // we downsample up to 8x8 patches
    if(O < 1) {
        O = std::max
            (int
             (std::floor
              (log2
               (std::min(buffer.width,buffer.height))) - omin -3), 1) ;
    }

    // initialize scalespace
    VL::Sift sift(buffer.data, buffer.width, buffer.height, 
            sigman, sigma0,
            O, S,
            omin, -1, S+1) ;

    //dbg (DBG_INFO, "siftpp: Gaussian scale space completed");


    // -------------------------------------------------------------
    //                                             Run SIFT detector
    // -------------------------------------------------------------
    if( ! haveKeypoints ) {

        //      dbg (DBG_INFO,   "siftpp: running detector  "
        //"threshold             : "
        //    "edge-threshold        : "
        //    );

        sift.detectKeypoints(threshold, edgeThreshold) ;

        cout
            << "siftpp: detector completed with "
            << sift.keypointsEnd() - sift.keypointsBegin()
            << " keypoints"
            << endl ;

    }

    // -------------------------------------------------------------
    //                  Run SIFT orientation detector and descriptor
    // -------------------------------------------------------------    

    /* set descriptor options */
    sift.setNormalizeDescriptor( ! unnormalized ) ;
    sift.setMagnification( magnif ) ;

    /*
       if( ! noorient &   nodescr) dbg (DBG_INFO, "[sift2] computing keypoint orientations") ;
       if(   noorient & ! nodescr) dbg (DBG_INFO, "[sift2] computing keypoint descriptors" );
       if( ! noorient & ! nodescr) dbg (DBG_INFO, "[sift2] computing orientations and descriptors" );
       if(   noorient &   nodescr) dbg (DBG_INFO, "[sift2] finalizing") ; 
       */

    // -------------------------------------------------------------
    //            Run detector, compute orientations and descriptors
    // -------------------------------------------------------------

    sift_point_feature_list_t *set = (sift_point_feature_list_t*)malloc(sizeof(sift_point_feature_list_t));
    set->num = 0;
    set->pts = NULL;
    set->utime = utime;
    set->width = width;
    set->height = height;
    set->desc_size = 128;

    for( VL::Sift::KeypointsConstIter iter = sift.keypointsBegin() ;
            iter != sift.keypointsEnd() ; ++iter ) {

        // detect orientations
        VL::float_t angles [4] ;
        int nangles ;
        if( ! noorient ) {
            nangles = sift.computeKeypointOrientations(angles, *iter) ;
        } else {
            nangles = 1;
            angles[0] = VL::float_t(0) ;
        }

        if (nangles > 0) {
            for (int a=0;a<nangles;a++) {
                assert (false);
                set->pts = (sift_point_feature_t*)realloc(set->pts, (set->num+1)*sizeof(sift_point_feature_t));
                
                sift_point_feature_t *ft = set->pts + set->num;
                ft->col = iter->x;
                ft->row = iter->y;
                ft->scale = iter->sigma;
                ft->size = 128;
                ft->laplacian = 1;
                ft->desc = (float*)malloc(ft->size*sizeof(float));
                ft->value = iter->v;
                VL::float_t desc[128];
                sift.computeKeypointDescriptor(desc, *iter, angles[a]) ;
                for (int i=0;i<128;i++) {
                    ft->desc[i] = desc[i];
                }
                // Convert float vector to integer. Assume largest value in normalized
                // vector is likely to be less than 0.5.
                ft->orientation = angles[a];

                set->num++;
            }
        }
    } // next keypoint
    qsort(set->pts, set->num, sizeof(sift_point_feature_t), compare_keypoints_descending_abs);
    return set;
}

static void *
sift_run (void *user);

static void
on_image (const lcm_recv_buf_t *rbuf, const char *channel, 
          const bot_core_image_t *msg, void *user_data );

static void
on_kinect_image (const lcm_recv_buf_t *rbuf, const char *channel, 
          const kinect_frame_msg_t *msg, void *user_data );

sift_state_t *
sift_state_create (const char *channel_in, const char *channel_out, int levels,
                   double sigma_init, double peak_thresh, int doubleimsize,
                   double scale_factor, gboolean verbose, gboolean debug, gboolean draw_lcmgl, gboolean input_kinect)
{
    sift_state_t *self = (sift_state_t*)calloc(1,sizeof(sift_state_t));
    self->sift_core = sift_core_create(levels, sigma_init, peak_thresh,
                                       doubleimsize, scale_factor);
    self->channel = strdup(channel_in);
    if (channel_out == NULL) {
        self->output_channel = g_strconcat (self->channel, "_SIFT", NULL);
    } else {
        self->output_channel = strdup(channel_out);
    }
//     self->levels = levels;
//     self->sigma_init = sigma_init;
//     self->peak_thresh = peak_thresh;
//     self->doubleimsize = doubleimsize;
//     self->scale_factor = scale_factor;
    self->input_kinect = input_kinect;
    self->verbose = verbose;
    self->debug = debug;
    self->draw_lcmgl = draw_lcmgl; 
    self->b_bot_timestamp_init = FALSE;
    
    self->processing = FALSE;
    if (!g_thread_supported ()) {
        g_thread_init (NULL);
    }
    self->mutex = g_mutex_new ();
    self->condition = g_cond_new ();
    self->running = TRUE;
    self->main_thread =
        g_thread_create (sift_run, self, TRUE, NULL);

    self->lcm = bot_lcm_get_global(NULL);//globals_get_lcm ();  
    g_assert(self->lcm);
    
    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (self->lcm);

    if(!self->input_kinect){
        self->image_sub =
            bot_core_image_t_subscribe (self->lcm, channel_in,
                                        on_image, self);
    }
    else{
        self->kinect_sub = kinect_frame_msg_t_subscribe(self->lcm, channel_in, 
                                                        on_kinect_image, self);
    }

    // Create lcmgl
    // TODO: this assumes <image name>_SIFT as the channel
    char lcmgl_channel[256];
    sprintf (lcmgl_channel, "VISION.%s.REACQUIRE_SIFT", channel_in);
    if (self->debug) {
        printf ("Sending lcmgl messages on %s\n", lcmgl_channel);
    }
    self->lcmgl = bot_lcmgl_init(self->lcm, lcmgl_channel);//globals_get_lcmgl (lcmgl_channel, 1);
    return self;
}

void
sift_state_destroy (sift_state_t *self)
{
    if (NULL != self->mutex) {
        g_mutex_lock (self->mutex);
        self->running = FALSE;
        g_mutex_unlock (self->mutex);
    }

    if (NULL != self->image_sub) {
        bot_core_image_t_unsubscribe(self->lcm, self->image_sub);
    }

    if (NULL != self->lcm) {
        //globals_release_lcm (self->lcm);
        bot_glib_mainloop_detach_lcm(self->lcm);
    }

    if (NULL != self->lcmgl) {
        bot_lcmgl_destroy (self->lcmgl);
    }

    if (NULL != self->channel) {
        free (self->channel);
    }

    if (NULL != self->output_channel) {
        free (self->output_channel);
    }

    if (NULL != self->condition) {
        g_cond_free (self->condition);
    }

    if (NULL != self->mutex) {
        g_mutex_free (self->mutex);
    }

    free (self);    
}

void on_image (const lcm_recv_buf_t *rbuf, const char *channel, 
               const bot_core_image_t *msg, void *user_data )
{
    sift_state_t *self = (sift_state_t*)user_data;
    if (0 != strcasecmp (channel, self->channel)) {
        return;
    }

    if (self->debug && !self->b_bot_timestamp_init) {
        self->b_bot_timestamp_init = TRUE;
        self->bot_timestamp_init = msg->utime;
    }

    fprintf(stderr, "Width : %d Row step: %d\n", msg->width, msg->row_stride);
    
    fprintf(stderr, ".");

    if (!self->processing) {
        // tell thread to process
        if (self->debug) {
            printf("Processing bot_core_image_t message, relative timestamp = %.2lf sec.\n",
                   ((double)(msg->utime-self->bot_timestamp_init)/1000000.));
        }
        g_mutex_lock (self->mutex);
        if (NULL != self->msg) {
            delete self->msg;
        }
        self->msg = bot_core_image_t_copy (msg);

        self->processing = TRUE;
        g_cond_signal (self->condition);
        g_mutex_unlock (self->mutex);

    } else {
        if (self->debug) {
            printf("Skipped bot_core_image_t message, relative timestamp = %.2lf sec.\n",
                   ((double)(msg->utime-self->bot_timestamp_init)/1000000.));
        }
    }
}

void on_kinect_image (const lcm_recv_buf_t *rbuf, const char *channel, 
               const kinect_frame_msg_t *msg, void *user_data )
{
    sift_state_t *self = (sift_state_t*)user_data;
    if (0 != strcasecmp (channel, self->channel)) {
        return;
    }

    if (self->debug && !self->b_bot_timestamp_init) {
        self->b_bot_timestamp_init = TRUE;
        self->bot_timestamp_init = msg->timestamp;
    }

    fprintf(stderr, ".");

    if (!self->processing) {
        // tell thread to process
        if (self->debug) {
            printf("Processing kinect_frame_msg_t message, relative timestamp = %.2lf sec.\n",
                   ((double)(msg->image.timestamp-self->bot_timestamp_init)/1000000.));
        }
        g_mutex_lock (self->mutex);

        if (NULL != self->msg) {
            delete self->msg;
        }

        self->msg = (bot_core_image_t *) calloc(1,sizeof(bot_core_image_t));
        //copy the kinect image stuff over 
        self->msg->utime =  msg->image.timestamp;
        self->msg->width = msg->image.width;
        self->msg->height = msg->image.height;
        self->msg->metadata = NULL;
        //???
        self->msg->row_stride = 0;//msg->image.width;
        if(msg->image.image_data_format == KINECT_IMAGE_MSG_T_VIDEO_RGB)
            self->msg->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB;
        else if(msg->image.image_data_format == KINECT_IMAGE_MSG_T_VIDEO_RGB_JPEG){
            self->msg->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG;
        }
        
        self->msg->size =  msg->image.image_data_nbytes;
        self->msg->data = (uint8_t*)calloc(self->msg->size, sizeof(uint8_t));
        memcpy(self->msg->data, msg->image.image_data, self->msg->size * sizeof(uint8_t));

        
        //self->msg = bot_core_image_t_copy (msg);
        
        self->processing = TRUE;
        g_cond_signal (self->condition);
        g_mutex_unlock (self->mutex);

    } else {
        if (self->debug) {
            printf("Skipped bot_core_image_t message, relative timestamp = %.2lf sec.\n",
                   ((double)(msg->timestamp-self->bot_timestamp_init)/1000000.));
        }
    }
}


sift_point_feature_list_t *
sift_compute_uncompressed(int32_t width, int32_t height, int levels,
                          float *sift_data, double peak_thresh,
                          int doubleimsize, double sigma_init, int64_t utime)
{
    // compute SIFT features
    
    sift_point_feature_list_t* keypoints =
        get_keypoints (sift_data, width, height,
                       levels, sigma_init, peak_thresh,
                       doubleimsize, utime);
    return keypoints;

}
/**
 * Inputs: image width and height, # of image channels,
 *         reusable buffer sift_data = (float*)calloc(width*height,sizeof(float));
 *         sift params: # of levels, peak threshold, doubleimsize,
 *         message time, sigma_init
 * Outputs: features (caller's responsiblity to clean up)
 */
sift_point_feature_list_t *
sift_compute_uncompressed(int32_t width, int32_t height, int levels,
                          int32_t pixel_stride, int32_t xmin, int32_t ymin,
                          float *sift_data,
                          int nchannels, double peak_thresh,
                          int doubleimsize, double sigma_init,
                          int64_t utime, const uint8_t *data)
{
    // raw data for debugging
    unsigned char *din = NULL, *dout=NULL;
#undef DEBUG_SIFT
#ifdef DEBUG_SIFT
    din = (unsigned char*)calloc(width*height,1);
    dout = (unsigned char*)calloc(width*height*3,1);
#endif // DEBUG_SIFT
                
    for (int row=ymin;row<height+ymin;row++) {
        for (int col=xmin;col<width+xmin;col++) {
            /* NOTE: This needs to be fixed to handle the general stride case:
                     stride = byte_stride, not pixel_stride and
                     index = nchannels * col + row * byte_stride;
            */
            int index = nchannels * (pixel_stride *  row + col);
            float val = .0;
            if (nchannels == 1)
                val = 1.0*data[index];
            else
                val = (1.0 * data[index] + 1.0 * data[index+1] +
                       1.0 * data[index+2])/3;
#ifdef DEBUG_SIFT
            din[width * row + col] = int(val);
            dout[3*(width * row + col)] = int(val);
            dout[3*(width * row + col)+1] = int(val);
            dout[3*(width * row + col)+2] = int(val);
#endif // DEBUG_SIFT
            sift_data[width * (row-ymin) + (col-xmin)] = fmin (255.0,
                                                               fmax (0.0, (float) val));
        }
    }
    
    // compute SIFT features
    sift_point_feature_list_t* keypoints =
        get_keypoints (sift_data, width, height,
                       levels, sigma_init, peak_thresh,
                       doubleimsize, utime);
    if (xmin || ymin) {
        double xoffset = static_cast<double>(xmin);
        double yoffset = static_cast<double>(ymin);
        for (int i=0; i<keypoints->num; ++i) {
            keypoints->pts[i].col += xoffset;
            keypoints->pts[i].row += yoffset;
        }
    }
    return keypoints;

}

sift_point_feature_list_t *
sift_compute_uncompressed(int32_t width, int32_t height,
                          int32_t pixel_stride, int32_t xmin, int32_t ymin,
                          sift_core_t *sift_core,
                          int64_t utime, const uint8_t *data,
                          int nchannels)
{

    return sift_compute_uncompressed(width, height, sift_core->levels,
                                     pixel_stride, xmin, ymin,
                                     sift_core->sift_data, nchannels,
                                     sift_core->peak_thresh,
                                     sift_core->doubleimsize,
                                     sift_core->sigma_init, utime, data);
}

sift_point_feature_list_t *
sift_compute_compressed(int32_t width, int32_t height,
                        int32_t pixel_stride, int32_t xmin, int32_t ymin,
                        int32_t xmax, int32_t ymax,
                        int32_t pixelformat, sift_core_t *sift_core,
                        int64_t utime, uint8_t *input_data,
                        int32_t input_data_size)
{
    unsigned char *data = NULL;
    int nchannels;

    // uncompress JPEG images
    if (pixelformat == PIXEL_FORMAT_MJPEG) {
        int required_buf_size = width * height * 3;

        if (sift_core->uncompressed_buffer_size < required_buf_size) {
            sift_core->uncompressed_buffer = 
                (unsigned char*)realloc (sift_core->uncompressed_buffer,
                                         required_buf_size);
            sift_core->uncompressed_buffer_size = required_buf_size;
        }

        jpeg_decompress_8u_rgb (input_data, input_data_size,
                                   sift_core->uncompressed_buffer, width,
                                   height, width*3);

        data = sift_core->uncompressed_buffer;
        nchannels = 3;
    }
    else if (pixelformat == PIXEL_FORMAT_GRAY) {
        data = input_data;
        nchannels = 1;
    }
    else if (pixelformat == PIXEL_FORMAT_RGB) {
        data = input_data;
        nchannels = 3;
    } else {
        assert(0);
    }

//     int byte_stride = nchannels * pixel_stride;

    if (data) {
        // sift++ takes as input normalized grayscale data
        if (!sift_core->sift_data) {
            sift_core->sift_data = (float*)calloc(width*height,sizeof(float));
        }
        int32_t sub_width = xmax - xmin + 1;
        int32_t sub_height = ymax - ymin + 1;
        return sift_compute_uncompressed(sub_width, sub_height, pixel_stride,
                                         xmin, ymin, sift_core, utime,
                                         data, nchannels);
    } else {
        return NULL;
    }
}


void *
sift_run (void *user)
{
    sift_state_t *self = (sift_state_t *)user;

    while (self->running) {

        g_mutex_lock (self->mutex);
        while (FALSE == self->processing) {
            g_cond_wait (self->condition, self->mutex);
        }
        g_mutex_unlock (self->mutex);

        GTimer *timer = g_timer_new ();
         if (self->debug) {
             fprintf(stderr,"Width : %d Height : %d Pixel Format : %d Size : %d" , 
                     self->msg->width, 
                     self->msg->height, 
                     self->msg->pixelformat, 
                     self->msg->size);
         }
        
        sift_point_feature_list_t * features =
            sift_compute_compressed(self->msg->width, self->msg->height,
                                    self->msg->width, 0, 0,
                                    self->msg->width-1, self->msg->height-1,
                                    self->msg->pixelformat,
                                    self->sift_core, self->msg->utime,
                                    self->msg->data, self->msg->size);
                                    
        if (self->sift_core->sift_data) {
            // publish
            if (features) {
                if (self->output_channel)
                    sift_point_feature_list_t_publish (self->lcm, self->output_channel, features);
                double secs = g_timer_elapsed (timer, NULL);
                if (self->draw_lcmgl) {
                    printf("Sending features via lcmgl.\n");
                    bot_lcmgl_t *lcmgl = self->lcmgl;
                    lcmglColor3f(1.0,0.0,0.0);
                    lcmglPointSize(3.0);
                    lcmglBegin(GL_POINTS);
                    for (int ipt=0;ipt<features->num;ipt++) {
                        lcmglVertex2f(features->pts[ipt].col,
                                      features->pts[ipt].row);
                    }
                    bot_lcmgl_switch_buffer (self->lcmgl);
                }
                printf("Writing [%d] features\n", features->num);

                if (self->verbose) {
//                     printf ("sift: %d features\ton channel %s\tat %.1f Hz\n", features->num, self->channel, 1.0/secs);
                    printf ("sift: %d features\ton channel %s\tat %.2f secs\n", features->num, self->channel, secs);
                }

            }
        }

        sift_point_feature_list_t_destroy (features);
        g_timer_destroy(timer);
        // not processing anymore
        g_mutex_lock (self->mutex);
        self->processing = FALSE;
        g_mutex_unlock (self->mutex);
    }
    
    return NULL;
}

sift_core_t *
sift_core_create (int levels, double sigma_init, double peak_thresh,
                  int doubleimsize, double scale_factor)
{
    sift_core_t *self = (sift_core_t*)calloc(1,sizeof(sift_core_t));
    self->levels = levels;
    self->sigma_init = sigma_init;
    self->peak_thresh = peak_thresh;
    self->doubleimsize = doubleimsize;
    self->scale_factor = scale_factor;
    self->sift_data = NULL;
    self->uncompressed_buffer = NULL;
    return self;
}

void
sift_core_destroy (sift_core_t *self)
{
    if (NULL != self->sift_data) {
        free(self->sift_data);
    }
    if (NULL != self->uncompressed_buffer) {
        free(self->uncompressed_buffer);
    }
    free (self);
    
}
