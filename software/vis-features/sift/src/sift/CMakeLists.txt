add_library(sift SHARED 
    sift.cpp
    sift-driver.cpp)

set(REQUIRED_LIBS glib-2.0 gthread-2.0 
    lcm bot2-core bot2-lcmgl-client image-utils
    lcmtypes_bot2-core lcmtypes_sift lcmtypes_kinect)

pods_use_pkg_config_packages(sift ${REQUIRED_LIBS})

# set the library API version.  Increment this every time the public API
# changes.
set_target_properties(sift PROPERTIES SOVERSION 1)

# make the header public
pods_install_headers(sift.hpp sift.ipp DESTINATION sift)

# make the library public
pods_install_libraries(sift)

# create a pkg-config file for the library, to make it for other software to
# use it.
pods_install_pkg_config_file(sift
    LIBS -lsift
    REQUIRES ${REQUIRED_LIBS}
    VERSION 0.0.1)