#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
#include <jpeg-utils/jpeg-utils.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>
#include <lcmtypes/bot_core_image_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <gist/gist.h>

#include <image_utils/pixels.h>
#include <image_utils/jpeg.h>

const static int nblocks = 4;
const static int n_scale = 3;
const static int orientations_per_scale[50]={8,8,4};

#define POSE_LIST_SIZE 10

const static int GIST_SIZE = 128;

typedef struct
{
    BotParam   *b_server;
    lcm_t      *lcm;
    GMainLoop *mainloop;
    pthread_t  work_thread;
    char *chan;
    GMutex *mutex;

    GList *pose_list;     

    bot_core_pose_t *last_pose;
    bot_core_image_t *img;
    bot_core_image_t *img_decomp;
    
    int publish_all;
    int verbose; 
    image_list_t *gabor;
} state_t;

int envoy_decompress_bot_core_image_t(const bot_core_image_t * jpeg_image, bot_core_image_t *uncomp_image)
{
  //copy metadata
  uncomp_image->utime = jpeg_image->utime;
  if (uncomp_image->metadata != NULL)
    bot_core_image_metadata_t_destroy(uncomp_image->metadata);
  uncomp_image->nmetadata = jpeg_image->nmetadata;
  if (jpeg_image->nmetadata > 0) {
    uncomp_image->metadata = bot_core_image_metadata_t_copy(jpeg_image->metadata);
  }
  
  uncomp_image->width = jpeg_image->width;
  uncomp_image->height = jpeg_image->height;
  
  uncomp_image->row_stride = jpeg_image->width * 3;//jpeg_image->row_stride;
  fprintf(stderr, "R : %d D : %d\n", jpeg_image->width, jpeg_image->row_stride);
  int depth=0;
  if (uncomp_image->width>0)
      depth= uncomp_image->row_stride / (double)uncomp_image->width;

  int ret;
  fprintf(stderr,"called1 : %d\n", uncomp_image->row_stride);//depth);

  if(uncomp_image->data == NULL){
      int uncomp_size = (uncomp_image->width) * (uncomp_image->height) * depth;
      uncomp_image->data = (unsigned char *) realloc(uncomp_image->data, uncomp_size * sizeof(uint8_t));
  }

  /*if(depth == 1){
      ret = jpeg_decompress_8u_gray(jpeg_image->data, 
                                    jpeg_image->size, uncomp_image->data, 
                                    uncomp_image->width, 
                                    uncomp_image->height, 
                                    uncomp_image->row_stride);
  }
  else if(depth == 3){*/
  ret = jpeg_decompress_8u_rgb(jpeg_image->data, 
                               jpeg_image->size, uncomp_image->data, 
                               uncomp_image->width, 
                               uncomp_image->height, 
                               uncomp_image->row_stride);
  
  fprintf(stderr,"called\n");
      //  }
  /*envoy_decompress_jpeg_image(jpeg_image->data, jpeg_image->size, &uncomp_image->data,
              &uncomp_image->width, &uncomp_image->height, &depth);*/
  uncomp_image->row_stride = uncomp_image->width * depth;
  uncomp_image->size = uncomp_image->width *uncomp_image->height* depth;
  if (depth == 1) {
    uncomp_image->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY;
  }
  else if (depth == 3) {
    uncomp_image->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB; //TODO: lets just assume its rgb... dunno what else to do :-/
  }
  return ret;
}

int convert_bot_image_to_lear(state_t *s, color_image_t *dst){
    int64_t start = bot_timestamp_now();
    if(!s->img || dst == NULL || s->gabor == NULL){
        return 1;
    }
    else{
        //assumed to be resized 
        bot_core_image_t * v = s->img_decomp;        
        fprintf(stderr,"Format : %d\n", v->pixelformat );
        
        if(s->img->pixelformat == BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG){
            envoy_decompress_bot_core_image_t(s->img, v);
            fprintf(stderr,"JPEG\n");
        }
        if(s->img->pixelformat == BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB){
                //envoy_decompress_bot_core_image_t(s->img, v);
                v = s->img;
                fprintf(stderr,"RGB\n");
        }

        fprintf(stderr,"Width : %d Height : %d\n", v->width, v->height);
        if(v->width != GIST_SIZE || v->height != GIST_SIZE){
            fprintf(stderr,"Image of wrong size - please resize : %d,%d\n", GIST_SIZE, GIST_SIZE);
            return 1;
        }
        
        //might be better padding on the sides 
        
        fprintf(stderr,"Running GIST\n");
        int i = 0;
        for (int y = 0; y < v->height; y++) {
            for (int x = 0; x < v->width; x++) {
                int r = v->data[y*v->row_stride + x*3 + 0] & 0xff;
                int g = v->data[y*v->row_stride + x*3 + 1] & 0xff;
                int b = v->data[y*v->row_stride + x*3 + 2] & 0xff;
                dst->c1[i] = r;
                dst->c2[i] = g;
                dst->c3[i] = b;
                i++;
            }
        }
        
        //assert(src->width == GIST_SIZE && src->height == GIST_SIZE);
        //assert(src->depth == IPL_DEPTH_8U);
        //assert(i == (GIST_SIZE * GIST_SIZE));
        float *desc = color_gist_scaletab_with_gabor(s->gabor, dst, nblocks, n_scale, orientations_per_scale);
        int descsize=0;
        
        /* compute descriptor size */
        for(int j=0;j<n_scale;j++) 
            descsize+=nblocks*nblocks*orientations_per_scale[j];

        descsize*=3; /* color */

        /* print descriptor */
        for(int j=0;j<descsize;j++) 
            printf("%.4f ",desc[j]);

        erlcm_gist_descriptor_t msg; 
        msg.utime = s->img->utime;
        msg.desc_size = descsize;
        msg.descriptor = (float *) calloc(msg.desc_size, sizeof(float));
        memcpy(msg.descriptor, desc, msg.desc_size*sizeof(float));
        erlcm_gist_descriptor_t_publish(s->lcm, "GIST_DESCRIPTOR_RESULT", &msg);
        free(msg.descriptor);

        printf("\n");
  
        free(desc);
    }

    int64_t end = bot_timestamp_now();

    fprintf(stderr,"Time : %f\n", (end-start)/ 1.0e6);
}



static void on_image(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                     const char * channel __attribute__((unused)),
                     const bot_core_image_t * msg, 
                     void * user  __attribute__((unused))) {
    
    state_t *s = (state_t *) user; 
    if(!s->img_decomp){
        s->img_decomp = (bot_core_image_t *) calloc(1,sizeof(bot_core_image_t));
    }
    
    g_mutex_lock(s->mutex);
    if(s->img){
        bot_core_image_t_destroy(s->img);
    }
    s->img = bot_core_image_t_copy(msg);
    g_mutex_unlock(s->mutex);

    /*color_image_t *lear = color_image_new(GIST_SIZE, GIST_SIZE);

    convert_bot_image_to_lear(s,lear);
    color_image_delete(lear);*/
    //call gist stuff
}

static void on_pose(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
			     const bot_core_pose_t * msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 
    if(s->last_pose !=NULL){
	bot_core_pose_t_destroy(s->last_pose);
    }
    s->last_pose = bot_core_pose_t_copy(msg);
    if(s->verbose){
	fprintf(stderr,"Pose : (%f,%f)\n",msg->pos[0], msg->pos[1]);//, rpy[2], vel[0] , vel[1]);
    }

    bot_core_pose_t *pose = bot_core_pose_t_copy(msg);

    fprintf(stderr, "Size of List (at start): %d\n", g_list_length(s->pose_list));

    if(g_list_length(s->pose_list) < POSE_LIST_SIZE){
	fprintf(stderr,"Adding\n");
	s->pose_list = g_list_prepend (s->pose_list, (pose));
    }
    else{
	fprintf(stderr, "Removing and Inserting\n");
	GList* last = g_list_last (s->pose_list);
	bot_core_pose_t *last_pose = (bot_core_pose_t *) last->data;
	fprintf(stderr,"Pose : %.3f (%f,%f)\n",last_pose->utime / 1000000.0 , last_pose->pos[0], last_pose->pos[1]);
	
	//	s->pose_list = g_list_remove_link (s->pose_list, last);	
	bot_core_pose_t_destroy((bot_core_pose_t *) last->data);
	s->pose_list = g_list_delete_link (s->pose_list, last);	

	
	fprintf(stderr, "Size of List (After remove): %d\n", g_list_length(s->pose_list));
	s->pose_list = g_list_prepend (s->pose_list , (gpointer) (pose));
    }
    
    fprintf(stderr, "Size of List (at end): %d\n", g_list_length(s->pose_list));
}

//pthread
static void *track_work_thread(void *user)
{
    state_t *s = (state_t*) user;

    printf("obstacles: track_work_thread()\n");

    while(1){
	fprintf(stderr, " T - called ()\n");
	
	usleep(50000);
    }
}

void read_parameters_from_conf(state_t *s)
{
    BotParam *b_param = s->b_server; 
    double max_t_vel =  bot_param_get_double_or_fail(b_param, "robot.max_t_vel");
    double max_r_vel = bot_param_get_double_or_fail(b_param, "robot.max_r_vel");
  
    char **planar_lidar_names = bot_param_get_all_planar_lidar_names(b_param);
  
    if(planar_lidar_names) {
	for (int pind = 0; planar_lidar_names[pind] != NULL; pind++) {
	    fprintf(stderr, "Channel : %s\n", planar_lidar_names[pind]);
	}
    }
  
    g_strfreev(planar_lidar_names);
}

//doesnt do anything right now - timeout function
gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;
    
    static int64_t last_utime = 0;
    
    g_mutex_lock(s->mutex);
    if(s->img == NULL || last_utime == s->img->utime){
        g_mutex_unlock(s->mutex);
        return TRUE;
    }

    last_utime = s->img->utime;
    color_image_t *lear = color_image_new(GIST_SIZE, GIST_SIZE);

    convert_bot_image_to_lear(s,lear);
    color_image_delete(lear);
    g_mutex_unlock(s->mutex);
    //do the periodic stuff 
    if(s->verbose){
	fprintf(stderr, "Callback - Timeout\n");
    }
    //return true to keep running
    return TRUE;
}

void subscribe_to_channels(state_t *s)
{
    //bot_core_pose_t_subscribe(s->lcm, "POSE", on_pose ,s);
    bot_core_image_t_subscribe(s->lcm , s->chan, on_image, s);
}  


static void usage(char * funcName)
{
    printf("Usage: %s [options]\n"
	   "options are:\n"
	   "--help,        -h    display this message \n"
	   "--publish_all, -a    publish robot laser messages for all channels \n"
	   "--verbose,     -v    be verbose", funcName);
    exit(1);

}

int 
main(int argc, char **argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));
    state->last_pose = NULL;
    state->pose_list = NULL;
    state->chan = NULL;

    const char *optstring = "havc:";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "publish_all", no_argument, 0, 'a' }, 
                                  { "channel", no_argument, 0, 'c' }, 
				  { "verbose", no_argument, 0, 'v' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    usage(argv[0]);
	    break;
	case 'a':
	    {
		fprintf(stderr,"Publishing all laser channels\n");
		break;
	    }
        case 'c':
            {
                state->chan = strdup(optarg);
                break;
            }
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
		state->verbose = 1;
		break;
	    }
	default:
	    {
		usage(argv[0]);
		break;
	    }
	}
    }

    if(state->chan == NULL){
        state->chan = strdup("CAMLCM");
    }


    state->mutex = g_mutex_new ();
    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
    state->b_server = bot_param_new_from_server(state->lcm, 1);

    //pthread_create(&state->work_thread, NULL, track_work_thread, state);

    subscribe_to_channels(state);

    state->img = NULL;
    state->gabor = create_gabor(n_scale, orientations_per_scale, GIST_SIZE, GIST_SIZE);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    read_parameters_from_conf(state);

    /* heart beat*/
    g_timeout_add (200, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


