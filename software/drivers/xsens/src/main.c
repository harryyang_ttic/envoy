#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <inttypes.h>
#include <getopt.h>
#include <lcmtypes/xsens_ins_t.h>
#include "xsens.h"
#include <bot_core/bot_core.h>

static int verbose;
uint32_t device_id;
static char * pub_channel = "XSENS";

static void imu_callback (xsens_t *xs, int64_t utime, uint16_t imu_time, 
                          xsens_data_t *xd, void *user)
{
  int i;
    xsens_ins_t imu;

    (void) xs;
    (void) imu_time;

    // copy data into the imu_t
    imu.utime = utime;
    imu.device_time = imu_time;
     for ( i = 0; i < 3; i++) {
        imu.accel[i] = xd->linear_accel[i];
        imu.gyro[i] = xd->rotation_rate[i];
        imu.mag[i] = xd->magnetometer[i];
    }

    for ( i = 0; i < 4; i++) {
        imu.quat[i] = xd->quaternion[i];
    }

    // and transmit.
    lcm_t *lcm = (lcm_t*) user;
    if (!lcm)
        return;

    xsens_ins_t_publish (lcm, pub_channel, &imu);

    if (verbose) {
        int64_t t = bot_timestamp_now ();
        //printf ("%"PRId64".%06"PRId64" ",
		//        bot_timestamp_seconds (t), bot_timestamp_useconds (t));
        //printf ("orientation: %8.4f %8.4f %8.4f %8.4f\n",
        //        imu.q[0], imu.q[1], imu.q[2], imu.q[3]);

	double rpy[3];
	bot_quat_to_roll_pitch_yaw (imu.quat, rpy);
	printf("%8.4f %8.4f %8.4f\n", rpy[0], rpy[1], rpy[2]);
    }

}

static void usage ()
{
    fprintf (stderr, "usage: xsens [options]\n"
             "\n"
             "  -h, --help             shows this help text and exits\n"
             "  -d, --device           device     [/dev/ttyUSB0]\n"
             "  -c, --channel          lcm chanel [XSENS] \n"
             "  -g  --gain             cross-over frequency between gyro/accelerometer e.g. 0.1\n"
             "  -m  --magnetometer     magnetometer gain e.g. 0\n"
             "  -v, --verbose          be verbose\n"
             "  -s, --status           report device status and exit\n"
             "      --amd              enable 'Adjust for Magnetic Disturbances'");
}


int main (int argc, char *argv[])
{
    setlinebuf (stdout);


    char *optstring = "hd:g:mvsac:";
    char c;
    struct option long_opts[] = {
        {"help", no_argument, 0, 'h'},
        {"device", required_argument, 0, 'd'},
        {"gain", required_argument, 0, 'g'},
        {"magnetometer", required_argument, 0, 'm'},
        {"verbose", no_argument, 0, 'v'},
        {"status", no_argument, 0, 's'},
        {"amd", no_argument, 0, 'a'},
        {"channel",required_argument},
        {0, 0, 0, 0}};

    char *device = "/dev/ttyUSB0";
    int status = 0;
    verbose = 0;
    int set_amd = 0;
    float gain = 0.1, magnetometer = 0.0;
    while ((c = getopt_long (argc, argv, optstring, long_opts, 0)) >= 0)
    {
        switch (c) 
        {
        case 'd':
            device = strdup (optarg);
            break;
        case 'g':
            gain = strtod (optarg, 0);
            break;
        case 'm':
            magnetometer = strtod (optarg, 0);
            break;
        case 'c':
          pub_channel = strdup(optarg);
          break;
        case 's':
            status = 1;
            break;
        case 'a':
            set_amd = 1;
            break;
        case 'v':
            verbose = 1;
            break;
        case 'h':
        default:
            usage ();
            return 1;
        }
    }

    xsens_t *xs = xsens_create (device);
    if (!xs)
        return 1;

    /// say hello to the device
    int res;
    uint8_t ver[3];
    char str[256];
    uint16_t amd;

    xsens_get_product_code (xs, str);
    xsens_get_firmware_rev (xs, ver);
    fprintf (stderr, "XSens is model \"%s\" with firmware %d.%d.%d\n",
             str, ver[0], ver[1], ver[2]);
    xsens_get_device_id (xs, &device_id);
    fprintf (stderr, "Serial number %08x\n", device_id);
    if (status) return 0;


    xsens_set_filter_gain (xs, gain);

    xsens_set_filter_magnetometer (xs, magnetometer);

    xsens_get_filter_settings (xs, &gain, &magnetometer);
    fprintf (stderr, "Filter settings: gain is %.3f, magnetometer weight is %.3f\n",
             gain, magnetometer);

    xsens_get_amd_setting (xs, &amd);
    fprintf (stderr, "Adjust for Magnetic Disturbances (AMD) is %s\n",
             amd ? "on" : "off");

    if (set_amd >= 0 && set_amd != amd) {
        fprintf (stderr, "Changing AMD to %d\n", set_amd);
        xsens_set_amd_setting (xs, set_amd);
    }

    // XXX if read-back settings don't match, loop?

    lcm_t *lcm = lcm_create(NULL);

    // setup event loop

    xsens_set_callback (xs, imu_callback, lcm);
    xsens_change_output_mode (xs,
                 XSENS_MODE_CALIBRATED | XSENS_MODE_ORIENTATION,
                 XSENS_SETTING_TIMESTAMPS | XSENS_SETTING_QUATERNION);

    res = xsens_run (xs);
    if (res < 0) {
        xsens_destroy (xs);
        sleep (1);
    }

    // XXX be able to recover if connection is lost

    return 0;
}
