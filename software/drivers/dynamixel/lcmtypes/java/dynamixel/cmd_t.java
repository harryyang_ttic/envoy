/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package dynamixel;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class cmd_t implements lcm.lcm.LCMEncodable
{
    public short servo_id;
    public byte torque_enable;
    public int goal_position;
    public int max_speed;
    public int max_torque;
 
    public cmd_t()
    {
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0xb21cd897bb47f770L;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(dynamixel.cmd_t.class))
            return 0L;
 
        classes.add(dynamixel.cmd_t.class);
        long hash = LCM_FINGERPRINT_BASE
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeShort(this.servo_id); 
 
        outs.writeByte(this.torque_enable); 
 
        outs.writeInt(this.goal_position); 
 
        outs.writeInt(this.max_speed); 
 
        outs.writeInt(this.max_torque); 
 
    }
 
    public cmd_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public cmd_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static dynamixel.cmd_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        dynamixel.cmd_t o = new dynamixel.cmd_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.servo_id = ins.readShort();
 
        this.torque_enable = ins.readByte();
 
        this.goal_position = ins.readInt();
 
        this.max_speed = ins.readInt();
 
        this.max_torque = ins.readInt();
 
    }
 
    public dynamixel.cmd_t copy()
    {
        dynamixel.cmd_t outobj = new dynamixel.cmd_t();
        outobj.servo_id = this.servo_id;
 
        outobj.torque_enable = this.torque_enable;
 
        outobj.goal_position = this.goal_position;
 
        outobj.max_speed = this.max_speed;
 
        outobj.max_torque = this.max_torque;
 
        return outobj;
    }
 
}

