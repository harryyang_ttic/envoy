// THIS IS AN AUTOMATICALLY GENERATED FILE.  DO NOT MODIFY
// BY HAND!!
//
// Generated by lcm-gen

#include <string.h>
#include "lcmtypes/dynamixel_cmd_address_vals_t.h"

static int __dynamixel_cmd_address_vals_t_hash_computed;
static int64_t __dynamixel_cmd_address_vals_t_hash;

int64_t __dynamixel_cmd_address_vals_t_hash_recursive(const __lcm_hash_ptr *p)
{
    const __lcm_hash_ptr *fp;
    for (fp = p; fp != NULL; fp = fp->parent)
        if (fp->v == __dynamixel_cmd_address_vals_t_get_hash)
            return 0;

    __lcm_hash_ptr cp;
    cp.parent =  p;
    cp.v = (void*)__dynamixel_cmd_address_vals_t_get_hash;
    (void) cp;

    int64_t hash = (int64_t)0xf32bc6110408e051LL
         + __int64_t_hash_recursive(&cp)
         + __int32_t_hash_recursive(&cp)
         + __int32_t_hash_recursive(&cp)
         + __int32_t_hash_recursive(&cp)
         + __int32_t_hash_recursive(&cp)
        ;

    return (hash<<1) + ((hash>>63)&1);
}

int64_t __dynamixel_cmd_address_vals_t_get_hash(void)
{
    if (!__dynamixel_cmd_address_vals_t_hash_computed) {
        __dynamixel_cmd_address_vals_t_hash = __dynamixel_cmd_address_vals_t_hash_recursive(NULL);
        __dynamixel_cmd_address_vals_t_hash_computed = 1;
    }

    return __dynamixel_cmd_address_vals_t_hash;
}

int __dynamixel_cmd_address_vals_t_encode_array(void *buf, int offset, int maxlen, const dynamixel_cmd_address_vals_t *p, int elements)
{
    int pos = 0, thislen, element;

    for (element = 0; element < elements; element++) {

        thislen = __int64_t_encode_array(buf, offset + pos, maxlen - pos, &(p[element].utime), 1);
        if (thislen < 0) return thislen; else pos += thislen;

        thislen = __int32_t_encode_array(buf, offset + pos, maxlen - pos, &(p[element].nservos), 1);
        if (thislen < 0) return thislen; else pos += thislen;

        thislen = __int32_t_encode_array(buf, offset + pos, maxlen - pos, &(p[element].address), 1);
        if (thislen < 0) return thislen; else pos += thislen;

        thislen = __int32_t_encode_array(buf, offset + pos, maxlen - pos, p[element].servo_id, p[element].nservos);
        if (thislen < 0) return thislen; else pos += thislen;

        thislen = __int32_t_encode_array(buf, offset + pos, maxlen - pos, p[element].value, p[element].nservos);
        if (thislen < 0) return thislen; else pos += thislen;

    }
    return pos;
}

int dynamixel_cmd_address_vals_t_encode(void *buf, int offset, int maxlen, const dynamixel_cmd_address_vals_t *p)
{
    int pos = 0, thislen;
    int64_t hash = __dynamixel_cmd_address_vals_t_get_hash();

    thislen = __int64_t_encode_array(buf, offset + pos, maxlen - pos, &hash, 1);
    if (thislen < 0) return thislen; else pos += thislen;

    thislen = __dynamixel_cmd_address_vals_t_encode_array(buf, offset + pos, maxlen - pos, p, 1);
    if (thislen < 0) return thislen; else pos += thislen;

    return pos;
}

int __dynamixel_cmd_address_vals_t_encoded_array_size(const dynamixel_cmd_address_vals_t *p, int elements)
{
    int size = 0, element;
    for (element = 0; element < elements; element++) {

        size += __int64_t_encoded_array_size(&(p[element].utime), 1);

        size += __int32_t_encoded_array_size(&(p[element].nservos), 1);

        size += __int32_t_encoded_array_size(&(p[element].address), 1);

        size += __int32_t_encoded_array_size(p[element].servo_id, p[element].nservos);

        size += __int32_t_encoded_array_size(p[element].value, p[element].nservos);

    }
    return size;
}

int dynamixel_cmd_address_vals_t_encoded_size(const dynamixel_cmd_address_vals_t *p)
{
    return 8 + __dynamixel_cmd_address_vals_t_encoded_array_size(p, 1);
}

int __dynamixel_cmd_address_vals_t_decode_array(const void *buf, int offset, int maxlen, dynamixel_cmd_address_vals_t *p, int elements)
{
    int pos = 0, thislen, element;

    for (element = 0; element < elements; element++) {

        thislen = __int64_t_decode_array(buf, offset + pos, maxlen - pos, &(p[element].utime), 1);
        if (thislen < 0) return thislen; else pos += thislen;

        thislen = __int32_t_decode_array(buf, offset + pos, maxlen - pos, &(p[element].nservos), 1);
        if (thislen < 0) return thislen; else pos += thislen;

        thislen = __int32_t_decode_array(buf, offset + pos, maxlen - pos, &(p[element].address), 1);
        if (thislen < 0) return thislen; else pos += thislen;

        p[element].servo_id = (int32_t*) lcm_malloc(sizeof(int32_t) * p[element].nservos);
        thislen = __int32_t_decode_array(buf, offset + pos, maxlen - pos, p[element].servo_id, p[element].nservos);
        if (thislen < 0) return thislen; else pos += thislen;

        p[element].value = (int32_t*) lcm_malloc(sizeof(int32_t) * p[element].nservos);
        thislen = __int32_t_decode_array(buf, offset + pos, maxlen - pos, p[element].value, p[element].nservos);
        if (thislen < 0) return thislen; else pos += thislen;

    }
    return pos;
}

int __dynamixel_cmd_address_vals_t_decode_array_cleanup(dynamixel_cmd_address_vals_t *p, int elements)
{
    int element;
    for (element = 0; element < elements; element++) {

        __int64_t_decode_array_cleanup(&(p[element].utime), 1);

        __int32_t_decode_array_cleanup(&(p[element].nservos), 1);

        __int32_t_decode_array_cleanup(&(p[element].address), 1);

        __int32_t_decode_array_cleanup(p[element].servo_id, p[element].nservos);
        if (p[element].servo_id) free(p[element].servo_id);

        __int32_t_decode_array_cleanup(p[element].value, p[element].nservos);
        if (p[element].value) free(p[element].value);

    }
    return 0;
}

int dynamixel_cmd_address_vals_t_decode(const void *buf, int offset, int maxlen, dynamixel_cmd_address_vals_t *p)
{
    int pos = 0, thislen;
    int64_t hash = __dynamixel_cmd_address_vals_t_get_hash();

    int64_t this_hash;
    thislen = __int64_t_decode_array(buf, offset + pos, maxlen - pos, &this_hash, 1);
    if (thislen < 0) return thislen; else pos += thislen;
    if (this_hash != hash) return -1;

    thislen = __dynamixel_cmd_address_vals_t_decode_array(buf, offset + pos, maxlen - pos, p, 1);
    if (thislen < 0) return thislen; else pos += thislen;

    return pos;
}

int dynamixel_cmd_address_vals_t_decode_cleanup(dynamixel_cmd_address_vals_t *p)
{
    return __dynamixel_cmd_address_vals_t_decode_array_cleanup(p, 1);
}

int __dynamixel_cmd_address_vals_t_clone_array(const dynamixel_cmd_address_vals_t *p, dynamixel_cmd_address_vals_t *q, int elements)
{
    int element;
    for (element = 0; element < elements; element++) {

        __int64_t_clone_array(&(p[element].utime), &(q[element].utime), 1);

        __int32_t_clone_array(&(p[element].nservos), &(q[element].nservos), 1);

        __int32_t_clone_array(&(p[element].address), &(q[element].address), 1);

        q[element].servo_id = (int32_t*) lcm_malloc(sizeof(int32_t) * q[element].nservos);
        __int32_t_clone_array(p[element].servo_id, q[element].servo_id, p[element].nservos);

        q[element].value = (int32_t*) lcm_malloc(sizeof(int32_t) * q[element].nservos);
        __int32_t_clone_array(p[element].value, q[element].value, p[element].nservos);

    }
    return 0;
}

dynamixel_cmd_address_vals_t *dynamixel_cmd_address_vals_t_copy(const dynamixel_cmd_address_vals_t *p)
{
    dynamixel_cmd_address_vals_t *q = (dynamixel_cmd_address_vals_t*) malloc(sizeof(dynamixel_cmd_address_vals_t));
    __dynamixel_cmd_address_vals_t_clone_array(p, q, 1);
    return q;
}

void dynamixel_cmd_address_vals_t_destroy(dynamixel_cmd_address_vals_t *p)
{
    __dynamixel_cmd_address_vals_t_decode_array_cleanup(p, 1);
    free(p);
}

int dynamixel_cmd_address_vals_t_publish(lcm_t *lc, const char *channel, const dynamixel_cmd_address_vals_t *p)
{
      int max_data_size = dynamixel_cmd_address_vals_t_encoded_size (p);
      uint8_t *buf = (uint8_t*) malloc (max_data_size);
      if (!buf) return -1;
      int data_size = dynamixel_cmd_address_vals_t_encode (buf, 0, max_data_size, p);
      if (data_size < 0) {
          free (buf);
          return data_size;
      }
      int status = lcm_publish (lc, channel, buf, data_size);
      free (buf);
      return status;
}

struct _dynamixel_cmd_address_vals_t_subscription_t {
    dynamixel_cmd_address_vals_t_handler_t user_handler;
    void *userdata;
    lcm_subscription_t *lc_h;
};
static
void dynamixel_cmd_address_vals_t_handler_stub (const lcm_recv_buf_t *rbuf,
                            const char *channel, void *userdata)
{
    int status;
    dynamixel_cmd_address_vals_t p;
    memset(&p, 0, sizeof(dynamixel_cmd_address_vals_t));
    status = dynamixel_cmd_address_vals_t_decode (rbuf->data, 0, rbuf->data_size, &p);
    if (status < 0) {
        fprintf (stderr, "error %d decoding dynamixel_cmd_address_vals_t!!!\n", status);
        return;
    }

    dynamixel_cmd_address_vals_t_subscription_t *h = (dynamixel_cmd_address_vals_t_subscription_t*) userdata;
    h->user_handler (rbuf, channel, &p, h->userdata);

    dynamixel_cmd_address_vals_t_decode_cleanup (&p);
}

dynamixel_cmd_address_vals_t_subscription_t* dynamixel_cmd_address_vals_t_subscribe (lcm_t *lcm,
                    const char *channel,
                    dynamixel_cmd_address_vals_t_handler_t f, void *userdata)
{
    dynamixel_cmd_address_vals_t_subscription_t *n = (dynamixel_cmd_address_vals_t_subscription_t*)
                       malloc(sizeof(dynamixel_cmd_address_vals_t_subscription_t));
    n->user_handler = f;
    n->userdata = userdata;
    n->lc_h = lcm_subscribe (lcm, channel,
                                 dynamixel_cmd_address_vals_t_handler_stub, n);
    if (n->lc_h == NULL) {
        fprintf (stderr,"couldn't reg dynamixel_cmd_address_vals_t LCM handler!\n");
        free (n);
        return NULL;
    }
    return n;
}

int dynamixel_cmd_address_vals_t_subscription_set_queue_capacity (dynamixel_cmd_address_vals_t_subscription_t* subs,
                              int num_messages)
{
    return lcm_subscription_set_queue_capacity (subs->lc_h, num_messages);
}

int dynamixel_cmd_address_vals_t_unsubscribe(lcm_t *lcm, dynamixel_cmd_address_vals_t_subscription_t* hid)
{
    int status = lcm_unsubscribe (lcm, hid->lc_h);
    if (0 != status) {
        fprintf(stderr,
           "couldn't unsubscribe dynamixel_cmd_address_vals_t_handler %p!\n", hid);
        return -1;
    }
    free (hid);
    return 0;
}

