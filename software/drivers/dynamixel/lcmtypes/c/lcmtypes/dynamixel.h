#ifndef __lcmtypes_dynamixel_h__
#define __lcmtypes_dynamixel_h__

#include "dynamixel_cmd_t.h"
#include "dynamixel_status_t.h"
#include "dynamixel_cmd_list_t.h"
#include "dynamixel_status_list_t.h"
#include "dynamixel_cmd_address_vals_t.h"

#endif
