//main.h

#ifndef __CM700_MAIN_H__
#define __CM700_MAIN_H__

#define DEBUG 1

#include <sys/time.h>
#define DERR(x) printf ("%s Error: ", __func__); printf (x); printf ("\n")
#define DINF(x) printf ("%s Info:  ", __func__); printf (x); printf ("\n")
static int64_t _timestamp_now()
{
    struct timeval tv;
    gettimeofday (&tv, NULL);
    return (int64_t) tv.tv_sec * 1000000 + tv.tv_usec;
}



#endif //__CM700_MAIN_H__
