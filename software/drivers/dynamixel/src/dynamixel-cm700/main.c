//dynamixel-cm700
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif
#include <unistd.h>
#include <sys/select.h>
#include <lcmtypes/dynamixel_cmd_list_t.h>
#include <lcmtypes/dynamixel_cmd_address_vals_t.h>
#include <lcmtypes/dynamixel_status_list_t.h>
#include <lcmtypes/bot_core_sensor_status_t.h>
#include <dynamixel/dynamixel.h>
#include "cm700.h"
#include "main.h"

const char default_dev[] = "/dev/microcontroller";
const char default_arm_dev[] = "/dev/microcontroller_arm";
const char status_channel[] = "DYNAMIXEL_STATUS";
const char command_channel[] =  "DYNAMIXEL_COMMAND";
const char arm_status_channel[] = "ARM_DYNAMIXEL_STATUS";
const char arm_command_channel[] =  "ARM_DYNAMIXEL_COMMAND";
const char command_channel_address[] = "DYNAMIXEL_COMMAND_ADDRESSES";
const char arm_command_channel_address[] = "ARM_DYNAMIXEL_COMMAND_ADDRESSES";

typedef struct _state_t state_t;

//function prototype
bool setup_cm700(state_t *state);

struct _state_t{
    int arm_mode;
    int debug_mode;
    lcm_t     *lcm;
    cm700_t *cm;
};

static void command_handler (const lcm_recv_buf_t * rbuf, const char * channel_name, 
                             const dynamixel_cmd_list_t * msg, void * user)
{
    state_t *state = (state_t *) user;
    state->cm->last_cmd_received_utime = msg->utime;
    state->cm->last_delta_sent_utime = msg->utime;
    cm700_send_commands(state->cm, (dynamixel_cmd_list_t *)msg);
    //fprintf(stderr, "Cmd received\n");
}

static void command_handler_addresses (const lcm_recv_buf_t * rbuf, const char * channel_name, 
                                       const dynamixel_cmd_address_vals_t * msg, void * user)
{
    //state_t *state = (state_t *) user;

    //printf("Command received\n");
  
    //    if(!cm700_send_address_vals(state->cm, (dynamixel_cmd_address_vals_t *) msg))
        //printf("Failed to send addresses.\n");
}

int main(int argc, char **argv){
    char *dev = NULL;
    int verbose = 0;
    state_t *state = (state_t *) calloc (1, sizeof (state_t));
    int c;
    while ((c = getopt (argc, argv, "ad:vf")) >= 0) {
        switch (c) {
        case 'd':
            dev = strdup(optarg);
            break;
        case 'f':
            fprintf(stderr, "Setting debug mode\n");
            state->debug_mode = 1;
            break;
        case 'a':
            state->arm_mode = 1;
            fprintf(stderr, "Setting arm mode\n");
            break;
        case 'v':
            verbose = 1;
            break;
        }
    }
    
    //DINF("started!");
    bool exit_flag = false;
    
    //open up the serial port
    //create an lcm type
    if(dev ==NULL){
        if(state->arm_mode){
            state->cm = cm700_open(default_arm_dev);
        }
        else{
            state->cm = cm700_open(default_dev);
        }
    }
    else{
        state->cm =  cm700_open(dev);
    }

    if (state->cm == NULL){
        DERR("couldn't open device\n");
        free (state);
        exit(1);
    }


    state->lcm = lcm_create(NULL);  
    
    //DINF("starting command subscription");
    dynamixel_cmd_list_t_subscription_t * sub;
    dynamixel_cmd_address_vals_t_subscription_t * sub_add;
    if(state->arm_mode){
        sub = dynamixel_cmd_list_t_subscribe(state->lcm, arm_command_channel, &command_handler, state);
        sub_add = dynamixel_cmd_address_vals_t_subscribe(state->lcm, arm_command_channel_address, &command_handler_addresses, state);
    }
    else{
        sub = dynamixel_cmd_list_t_subscribe(state->lcm, command_channel, &command_handler, state);
        sub_add = dynamixel_cmd_address_vals_t_subscribe(state->lcm, command_channel_address, &command_handler_addresses, state);
    }

    if (!setup_cm700(state)){
        DERR("failed to set sensors");
        exit_flag = true;
    }
    else {
        //DINF("setup cm700\n");
    }
    int count = 0;
    while (!exit_flag){
        int64_t read_time = _timestamp_now();
        int lcm_fd = lcm_get_fileno(state->lcm);
        int cm700_fd = cm700_get_fd(state->cm);
        fd_set fds;

        FD_ZERO(&fds);
        FD_SET(lcm_fd, &fds);
        FD_SET(STDIN_FILENO, &fds);
        FD_SET(cm700_fd, &fds);

        int max_fd = (cm700_fd > lcm_fd) ? cm700_fd : lcm_fd;

        //printf ("cm700_fd = %d, lcm_fd = %d, max = %d\n", cm700_fd, lcm_fd, max_fd);

        //there is no timeout because the MCU should asyncronously interrupt
        int status = select (max_fd + 1, &fds, NULL, NULL, NULL); 
        //cm700_print_debug(state->cm);    

        if (status < 0){
            printf ("ERROR!\n");
        }
        if (FD_ISSET(cm700_fd, &fds)){

            if (cm700_read_status(state->cm)){    
                //now the status list should be populated 
	      
                //	      fprintf(stderr," ==== Status read\n");
                //we should check and see if the goal position of the relavent servos have been set to the goal position we have
                count = (count+1)%13;
                if(count == 0){
                    cm700_compare_to_status_and_command(state->cm); //see if it has sent; resend
                }
                if(state->arm_mode){
                    if(dynamixel_status_list_t_publish(state->lcm, arm_status_channel, cm700_get_dsl(state->cm)) != 0) {
                        DERR("publishing to LCM faild");
                        printf ("Error: publishing to LCM!\n");
                      
                        //                    exit_flag = true;
                    }
                }
                else{
                    if(dynamixel_status_list_t_publish(state->lcm, status_channel, cm700_get_dsl(state->cm)) != 0) {
                        DERR("publishing to LCM faild");
                        printf ("Error: publishing to LCM!\n");
                      
                        //                    exit_flag = true;
                    }
                }
                int64_t end_time = _timestamp_now();
                double dt_ms = (end_time - read_time) * 1e-3;
                
                
                //static int r_count = 0; 
                static int64_t last_report_time = 0;
                static double r_time = 0;
                
                r_time = (1-0.1) * r_time + 0.1 * dt_ms;
                
                if((end_time - last_report_time)/1.0e6 > 1.0){
                    last_report_time = end_time; 
                    printf ("Read frequency = %.03f\n", 1000/r_time);
                    
                    bot_core_sensor_status_t msg;
                    msg.utime = bot_timestamp_now();
                    if(state->arm_mode)
                        msg.sensor_name = "ARM_DYNAMIXEL_SERVO"; //maybe use some indexing - to make it unique
                    else
                        msg.sensor_name = "DYNAMIXEL_SERVO"; //maybe use some indexing - to make it unique
                    msg.rate = 1000/r_time; 
                    msg.type = BOT_CORE_SENSOR_STATUS_T_DYNAMIXEL_SERVO;
                    bot_core_sensor_status_t_publish(state->lcm, "SENSOR_STATUS_SERVO", &msg);
                }

                read_time = _timestamp_now();                
            }
            //cm700_print_debug(state->cm);
            
        }
        if (FD_ISSET(lcm_fd, &fds)){
            lcm_handle(state->lcm);
        }
        
        if (FD_ISSET(STDIN_FILENO, &fds)){
            //DINF("User exit\n");
            exit_flag = true;
        }
    }
    
    cm700_stop(state->cm);
    //    DINF("destroying cm700_t structure");
    //    cm700_destroy(state->cm);
    //    DINF("unsubscribing to cmd_list");
    //    dynamixel_cmd_list_t_unsubscribe(state->lcm, sub);
    //    DINF("destroying LCM");
    //    lcm_destroy(state->lcm);
    //DINF("freeing state");
    free(state);
    
    return 0;
}

//default sensor list settings
bool setup_cm700(state_t *state){
    bool success_flag = true;
    
    cm700_stop(state->cm);
    //DINF ("clearing sensor");
    if (!cm700_clear_sensors(state->cm)){
        DERR("failed to clear sensors");
        //success_flag = false;
    }
    else {    
        //DINF ("success!");
    }
    
    //DINF ("adding goal position");
    if (!cm700_add_sensor(state->cm, DYNAMIXEL_CTL_GOAL_POSITION)){
        DERR("failed to add sensor");
        success_flag = false;
    }
    else {    
        //DINF ("success!");
    }

    
	//DINF ("adding present position sensor");
    if (!cm700_add_sensor(state->cm, DYNAMIXEL_CTL_PRESENT_POSITION)){
        DERR("failed to add sensor");
        success_flag = false;
    }
    else {    
        //DINF ("success!");
    }

    if(state->debug_mode){
        //DINF ("moving flag");
        if (!cm700_add_sensor(state->cm, DYNAMIXEL_CTL_MOVING)){
            DERR("failed to add sensor");
            success_flag = false;
        }
        else {    
            //DINF ("success!");
        }
        

        //DINF ("present voltage");
        if (!cm700_add_sensor(state->cm, DYNAMIXEL_CTL_PRESENT_VOLTAGE)){
            DERR("failed to add sensor");
            success_flag = false;
        }
        else {    
            //DINF ("success!");
        }
    }

    //DINF ("adding present speed");
    if (!cm700_add_sensor(state->cm, DYNAMIXEL_CTL_PRESENT_SPEED)){
        DERR("failed to add sensor");
        success_flag = false;
    }
    else {    
        //DINF ("success!");
    }

    //DINF ("adding set moving speed");
    if (!cm700_add_sensor(state->cm, DYNAMIXEL_CTL_MOVING_SPEED)){
        DERR("failed to add sensor");
        success_flag = false;
    }
    else {    
        //DINF ("success!");
    }

    //DINF ("adding torque en sensor");
    if (!cm700_add_sensor(state->cm, DYNAMIXEL_CTL_TORQUE_ENABLE)){
        DERR("failed to add sensor");
        success_flag = false;
    }
    else {    
        //DINF ("success!");
    }
    
    if(state->debug_mode){
        //DINF ("adding load sensor");
        if (!cm700_add_sensor(state->cm, DYNAMIXEL_CTL_PRESENT_LOAD)){
            DERR("failed to add sensor");
            success_flag = false;
        }
        else {    
	    // DINF ("success!");
        }
        
        //DINF ("adding torque limit sensor");
        if (!cm700_add_sensor(state->cm, DYNAMIXEL_CTL_TORQUE_LIMIT)){
            DERR("failed to add sensor");
            success_flag = false;
        }
        else {
	    // DINF ("success!");
        }

        //DINF ("adding CCW Compliance Margin sensor");
        if (!cm700_add_sensor(state->cm, DYNAMIXEL_CTL_CCW_COMPLIANCE_SLOPE)){
            DERR("failed to add sensor");
            success_flag = false;
        }
        else {
            //DINF ("success!");
        }
    }
    
    //    for(int i=0; i<7; i++){
    //      if(!(dynamixel_write(state->cm->fd, i, 0x1C, 1)))
    //	printf("CCW set");
    //      if(!(dynamixel_write(state->cm->fd, i, 0x1D, 1)))
    //	printf("CW set");
    //    }

    cm700_start(state->cm);
    // DINF("exiting");
    return success_flag;
}
