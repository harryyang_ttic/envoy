cmake_minimum_required(VERSION 2.6.0)

set(POD_NAME cm700)

pkg_check_modules(GLIB2 REQUIRED glib-2.0)

if(NOT LCM_FOUND)
    return()
endif()

add_definitions(-std=gnu99 -Wall -DBUILD_PATH='"${CMAKE_INSTALL_PREFIX}"')
#add_definitions(-std=gnu99 -Wall)

include_directories(${GLIB2_INCLUDE_DIRS} 
    ${GTK2_INCLUDE_DIRS}
    ${OPENGL_INCLUDE_DIR}
    ${GLUT_INCLUDE_DIR}
    ${LCM_INCLUDE_DIRS}
    ${BOT2_CORE_INCLUDE_DIRS}
    )

set(link_libraries 
    ${GLIB2_LDFLAGS}
    ${GTK2_LDLFLAGS}
    ${OPENGL_LIBRARIES}
    ${GLUT_LIBRARIES}
    ${LCM_LDFLAGS}
	${LCMTYPES_LIBS}
    ${BOT2_CORE_LDFLAGS}
    bot2-vis
    )


add_executable(dynamixel-cm700 
    main.c 
	serial.c
	cm700.c
	)

target_link_libraries(dynamixel-cm700  ${link_libraries})
pods_use_pkg_config_packages(dynamixel-cm700 
    dynamixel
    lcm
    bot2-core)


pods_install_executables(dynamixel-cm700)
