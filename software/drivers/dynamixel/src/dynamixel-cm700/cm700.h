//cm700.h

#ifndef __CM700_H__
#define __CM700_H__

#include <stdbool.h>
#include <bot_core/bot_core.h>
#include <lcmtypes/dynamixel_cmd_list_t.h>
#include <lcmtypes/dynamixel_cmd_address_vals_t.h>
#include <lcmtypes/dynamixel_status_list_t.h>

//#define CM700_BAUD	57600
#define MIN_TIME_TO_SEND_DELTA 0.5 //(sec)

#define CM700_BAUD 1000000

#define SERVO_DEBUG				"SD"
#define SERVO_COMMAND			"SC"
#define SERVO_COMMAND_FAIL		"SRF"
#define SERVO_COMMAND_SUCCESS	"SRS"
#define SERVO_STATUS			"SS"

#define SERVO_COMMAND_DATA 		'D'
#define SERVO_COMMAND_DATA_ADDRESSES    'A'

#define SERVO_ADD_SENSOR 		'+'
#define SERVO_REMOVE_SENSOR 	'-'
#define SERVO_ADD_SERVO			'X'
#define SERVO_REMOVE_SERVO		'Z'
#define SERVO_QUERY_SERVO		'Y'
#define SERVO_QUERY_COUNT 		'C'
#define SERVO_QUERY_SENSOR 		'S'
#define SERVO_CLEAR_SENSORS		'K'
#define SERVO_START				'['
#define SERVO_STOP				']'
#define SERVO_HELP				'?'

/**
 * cm700 read/write data between the servo controller (cm700) and the host (linux)
 */

typedef struct _cm700_t cm700_t;

struct _cm700_t{
    int 					fd;
    int 					nwrite_servos;
    int 					nsensors;
    int 					nread_servos;
    char 					*out_buffer;
    char 					*in_buffer;
    dynamixel_status_list_t	*dsl;
    dynamixel_cmd_list_t *cmd;

    int64_t last_cmd_received_utime;
    int64_t last_delta_sent_utime;
};


/**
 * cm700_open
 *
 * Description: intializes a cm700 object with 
 *
 * Parameter:
 *	device: string representing the location of the serial device
 *
 * Return:
 * 	handle to a cm700_t structure
 *	NULL if error
 */
cm700_t * cm700_open(const char * device);

void cm700_add_to_command_list(cm700_t *cm, dynamixel_cmd_list_t *cmd);
/**
 * cm700_destroy
 *
 * Description: release any resources the cm700_t used
 *
 * Parameter:
 * 	cm: self
 *
 * Return:
 *	nothing
 */
void cm700_destroy(cm700_t *cm);


bool cm700_send_address_vals(cm700_t *cm, dynamixel_cmd_address_vals_t * cmd);

/**
 * cm700_send_commands
 *
 * Description: reads in a LCM dynamixel_cmd_list_t structure and send commands to the 
 *	cm700 MCU
 *
 * Parameter:
 *	cm: self
 *	cmd_list: dynamixel_cmd_list_t structure
 *
 * Return:
 *	true: send successful
 *	false: failed to send successfully
 */
bool cm700_send_commands(cm700_t *cm, dynamixel_cmd_list_t * cmd_list);

/**
 * cm700_get_num_of_servos
 *
 * Description: get the number of servos attached to the CM700 MCU
 *
 * Parameter:
 *	cm: self
 *	
 * Return:
 *	number of servos attached to the cm700 MCU
 *	-1 if error
 */
int	cm700_get_num_of_servos(cm700_t *cm);

/**
 * cm700_get_sensor_count
 *
 * Description: get the number of sensors being read from each servos
 *
 * Parameters:
 *	cm: self
 *
 * Return:
 *	number of sensors being read from each servo
 */
int cm700_get_sensor_count(cm700_t *cm);

/**
 * cm700_clear_sensors
 *
 * Description: clears the sensors being read from the cm700 MCU
 *
 * Parameters:
 *	cm: self
 *
 * Return:
 *	true: success
 *	false: fail
 */
bool cm700_clear_sensors(cm700_t *cm);

/**
 * cm700_add_sensor
 *
 * Description: commands the cm700 to read the sensor with the specified address
 *	from each servo
 *
 * Parameters:
 *	cm: self
 *	address; address of sensor to read
 *		these address values can be found in the dynamixel.h file
 *
 * Return:
 *	true = success
 *	false = fail
 */
bool cm700_add_sensor(cm700_t *cm, int sensor_address);

/**
 * cm700_remove_sensor
 * 
 * Description: removes the sensor with the specified address for all servos
 *
 * Parameters:
 *	cm: self
 *	address: address of the sensor to read from each servo
 *		these addresses can be found in the dynamixel.h file
 *
 * Return:
 *	true = success
 *	false = fail
 */
bool cm700_remove_sensor(cm700_t *cm, int sensor_address);

/*
 * cm700_read_status
 *
 * Description: read/parse the data read from the cm700 MCU
 *
 * Parmeters:
 *	cm: self
 *
 * Return:
 *	true = succes
 * 	false = fail
 */
bool cm700_read_status(cm700_t *cm);

/**
 * cm700_get_fd
 *
 * Description: gets the file descriptor associated with the serial device
 *	this is used for the select function
 *
 * Parameters:
 *	cm: self
 *
 * Return:
 *	integer file descriptor
 *	return NULL if failed
 */
int cm700_get_fd(cm700_t *cm);

/**
 * cm700_flush
 *
 * Description: flush the output buffer to the serial port, this was mostly used for
 * 	debug and may not be needed any more
 *
 * Parameters:
 *	cm: self
 *
 * Return:
 *	status: 0 = success, !0 = fail
 */
int cm700_flush(cm700_t *cm);

/**
 * cm700_start
 *
 * Description: commands the cm700 MCU to start sending data to the host
 *
 * Parameters:
 *	cm: self
 *
 * Return:
 *	true: success
 * 	false: fail
 */
bool cm700_start(cm700_t *cm);

/**
 * cm700_stop
 *
 * Description: commands the cm700 MCU to stop sending data to the host
 *
 * Parameters:
 *	cm: self
 *
 * Return:
 *  true: success
 *	false: fail
 */
bool cm700_stop(cm700_t *cm);

/**
 * cm700_print_debug
 *
 * Description: print debug output data to the command line
 *
 * Parameters:
 *	cm: self
 *
 * Return:
 *	none
 */
void cm700_print_debug(cm700_t *cm);

/**
 * cm700_get_dsl
 *
 * Description: processes the data read from the serial port and generates a dynamixel_status_list_t structure
 *
 * Parameters:
 *	cm: self
 *
 * Return:
 * 	a dynamixel_status_list_t structure
 *	NULL if error
 */
dynamixel_status_list_t * cm700_get_dsl(cm700_t * cm);

void cm700_compare_to_status_and_command(cm700_t *cm);

#endif //__CM700_H__

