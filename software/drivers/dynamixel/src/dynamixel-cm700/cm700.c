
//cm700.c
#include <stdio.h>
#include <stdlib.h>
#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <poll.h>
#include <errno.h>
#include <dynamixel/dynamixel.h>
#include "cm700.h"
#include "serial.h"
#include "main.h"
#include <lcmtypes/dynamixel_status_list_t.h>
#include <lcmtypes/dynamixel_status_t.h>

#define BUFFER_SIZE 600
#define READ_TIMEOUT 500
//servo command
/* 
   "SCD" + 
   # of servos (1) + 
   # of servos * (id(2) + torque_en(1) + goal_pos(4) + speed(4) + torque(4))
   '\n' + '\0'

*/
#define OUT_BUF_CALC(x) 			3 + 1 + (x * (2 + 1 + 4 + 4 + 4)) + 2 + 1 

#define ID_OFFSET(x) 				3 + 1 + (x * (2 + 1 + 4 + 4 + 4))
#define TORQUE_EN_OFFSET(x) 		ID_OFFSET(x) + 2
#define GOAL_POS_OFFSET(x) 			TORQUE_EN_OFFSET(x) + 1
#define SPEED_OFFSET(x) 			GOAL_POS_OFFSET(x) + 4
#define TORQUE_OFFSET(x) 			SPEED_OFFSET(x) + 4

//servo status
/*
  "SS" +
  # of servos (1) +
  # of sensors (1) +
  # of servos * # of sensors * (2 + 2 + 4)
*/

#define IN_BUF_CALC(x, y)				2 + 1 + 1 + ( x * ( y * (2 + 2 + 4))) 

#define IN_SERVO_OFFSET 				2
#define IN_SENSOR_OFFSET 				IN_SERVO_OFFSET + 1
#define IN_SERVO_ID_OFFSET(x, y) 		IN_SENSOR_OFFSET + 1 + x * ( 2 + (y * ( 2 + 4))) 
#define IN_SEN_ADDRESS_OFFSET(x, y, z)	IN_SERVO_ID_OFFSET(x, y) + 2 + (z * (2 + 4))
#define IN_SEN_VALUE_OFFSET(x, y, z)	IN_SEN_ADDRESS_OFFSET(x, y, z) + 2


//function prototypes
dynamixel_status_list_t * generate_status_list(int nservos, int nsensors);
bool populate_status_list(cm700_t *cm);
char generate_crc(char * buffer, int size);
static int _fileutils_write_fully(int fd, const void *b, int len);
static int _fileutils_read_fully_timeout(int fd, char *bufin, int len, int msTimeout);


static bool fd_check(cm700_t *cm){
    if (cm->fd == -1){
        return false;
    }
    return true;
}

int get_index(int servo_id, dynamixel_status_list_t *dsl){
    int ind = -1;
    for(int i=0; i < dsl->nservos; i++){
        if(servo_id == dsl->servos[i].servo_id){
            return i;
        }
    }
    return ind;

}

void cm700_compare_to_status_and_command(cm700_t *cm){
    if(cm->cmd == NULL || cm->dsl == NULL || cm->cmd->ncommands == 0){
        return;
    }

    int64_t c_time = bot_timestamp_now();

    //fprintf(stderr, "Delta : %f\n", (c_time - cm->last_delta_sent_utime) / 1.0e6);
    if(cm->last_delta_sent_utime < 0){
        //return - no delta found the last time we checked
        return;
    }

    if((c_time - cm->last_delta_sent_utime)/1.0e6 < MIN_TIME_TO_SEND_DELTA){
        //fprintf(stderr, "Too short a time - waiting\n");
        return;
    }
  
    fprintf(stderr, "Checking Delta : %f\n", (c_time - cm->last_delta_sent_utime) / 1.0e6);
    dynamixel_cmd_list_t *delta_msg;
    delta_msg = (dynamixel_cmd_list_t *) calloc(1,sizeof(dynamixel_cmd_list_t));
    delta_msg->ncommands = 0;
    int loc[] = {-1,-1,-1};
    int s_id;
    for (int j=0; j < cm->dsl->servos[0].num_values; j++){
        //store sensor address locations
        if (cm->dsl->servos[0].addresses[j] == 24){
            loc[0] = j;
            //      printf("stored torque_en location\n");
            continue;
        }
        if (cm->dsl->servos[0].addresses[j] == 30){
            loc[1] = j;
            //      printf("stored goal_pos location\n");
            continue;
        }
        if (cm->dsl->servos[0].addresses[j] == 34){
            loc[2] = j;
            //      printf("stored max_torque location\n");
            continue;
        }
    }
  
    delta_msg->commands = (dynamixel_cmd_t *) calloc(cm->cmd->ncommands, sizeof(dynamixel_cmd_t));

    for(int i=0; i < cm->cmd->ncommands; i++){ //goes through commands
        s_id = get_index(cm->cmd->commands[i].servo_id,cm->dsl);
        if(s_id== -1)
            continue;
        if (loc[0] >=0 && cm->dsl->servos[s_id].values[loc[0]] == cm->cmd->commands[i].torque_enable){
            if  (loc[1] >=0 && cm->dsl->servos[s_id].values[loc[1]] == cm->cmd->commands[i].goal_position){
                //if  (loc[2] >=0 && cm->dsl->servos[s_id].values[loc[2]] == cm->cmd->commands[i].max_torque){
                //	  printf("command applied\n");
                continue;
                //}
            }
        }
        //add command to delta_msg

        fprintf(stderr, "Resending servo %d command\n", s_id);
        fprintf(stderr, "TQ : %d - %d Goal : %d - %d Max TQ = %d - %d\n", 
                cm->dsl->servos[s_id].values[loc[0]], cm->cmd->commands[i].torque_enable, 
                cm->dsl->servos[s_id].values[loc[1]], cm->cmd->commands[i].goal_position, 
                cm->dsl->servos[s_id].values[loc[2]],cm->cmd->commands[i].max_torque);
            
        delta_msg->ncommands++;
    
        //    printf("command counter incremented");
        delta_msg->commands[delta_msg->ncommands-1].servo_id = s_id;
        //    printf("servo_id: %d\n", delta_msg->commands[delta_msg->ncommands-1].servo_id);
        delta_msg->commands[delta_msg->ncommands-1].torque_enable = cm->cmd->commands[i].torque_enable;
        //    printf("torque_en: %d\n", delta_msg->commands[delta_msg->ncommands-1].torque_enable);
        delta_msg->commands[delta_msg->ncommands-1].max_speed = cm->cmd->commands[i].max_speed;
        //    printf("max_speed: %d\n", delta_msg->commands[delta_msg->ncommands-1].max_speed);
        delta_msg->commands[delta_msg->ncommands-1].goal_position = cm->cmd->commands[i].goal_position;
        //    printf("goal_pos: %d\n", delta_msg->commands[delta_msg->ncommands-1].goal_position);
        delta_msg->commands[delta_msg->ncommands-1].max_torque = cm->cmd->commands[i].max_torque;
        //    printf("max_torque: %d\n", delta_msg->commands[delta_msg->ncommands-1].max_torque);
    }
  
    for(int i=0; i< delta_msg->ncommands; i++){
        //fprintf(stderr, "");
    }
    //send the unapplied commands 
    //  fprintf(stderr,"Delta List Size : %d\n", delta_msg->ncommands);
    if(delta_msg->ncommands >0){
        fprintf(stderr, "Delta Found => Sending (time) %f\n", (c_time - cm->last_delta_sent_utime)/1.0e6);
        cm->last_delta_sent_utime = bot_timestamp_now();
        cm700_send_commands(cm, delta_msg);
    }
    else{
        cm->last_delta_sent_utime = -1;
    }
    
    free(delta_msg->commands);
    free(delta_msg);
}

void cm700_add_to_command_list(cm700_t *cm, dynamixel_cmd_list_t *cmd_list){

    for(int j=0; j < cmd_list->ncommands; j++ ){  //this is the number of commands in the most recent command
        int found = 0;
        int loc = -1;
        for(int i=0; i < cm->cmd->ncommands; i++){   //this is the number of commands in the held commands
            if(cm->cmd->commands[i].servo_id == cmd_list->commands[j].servo_id){
                found = 1;
                loc = i; //where the servo command is? figure out where that is
                break;
            }

        }

        if(found == 0){
            cm->cmd->ncommands++;
            cm->cmd->commands = (dynamixel_cmd_t *) realloc(cm->cmd->commands, cm->cmd->ncommands * sizeof(dynamixel_cmd_t));
            loc = cm->cmd->ncommands-1;
	
            //then realloc the memo
            //increase the size 
        }
        //update the values: torque_enable, goal_position, max_speed, max_torque
        cm->cmd->commands[loc].servo_id = cmd_list->commands[j].servo_id;
        cm->cmd->commands[loc].torque_enable = cmd_list->commands[j].torque_enable;
        cm->cmd->commands[loc].goal_position = cmd_list->commands[j].goal_position;
        cm->cmd->commands[loc].max_speed = cmd_list->commands[j].max_speed;
        cm->cmd->commands[loc].max_torque = cmd_list->commands[j].max_torque;
        //printf("servo_id: %d \n torque_enable: %d \n goal_position: %d \n max_speed: %d \n max_torque: %d \n",cm->cmd->commands[loc].servo_id,cm->cmd->commands[loc].torque_enable,cm->cmd->commands[loc].goal_position,cm->cmd->commands[loc].max_speed,cm->cmd->commands[loc].max_torque);
    }  
}

cm700_t * cm700_open(const char * device){
    //DINF("entered");
    //generate the cm700_t structure
    cm700_t * cm = calloc (1, sizeof(cm700_t));
    //open the serial port
    int baud = CM700_BAUD;
    //printf ("baudrate = %d\n", baud);
    cm->fd = serial_open(device, baud, true);
    serial_setbaud(cm->fd, baud);
    if (cm->fd == -1){
        free(cm);
        return NULL;
    }
    //	int count = write (cm->fd, "SCS\n", 5);
    //	printf ("count = %d\n", count);
    //initialize the state variables
    cm->nwrite_servos = 0;
    cm->nread_servos = 0;
    cm->nsensors = 0;
    cm->out_buffer = NULL;
    cm->in_buffer = calloc(BUFFER_SIZE, sizeof(char));
    cm->cmd = (dynamixel_cmd_list_t *) calloc(1, sizeof(dynamixel_cmd_list_t));
    cm->last_cmd_received_utime = 0;
    cm->last_delta_sent_utime = 0;
    cm->cmd->ncommands = 0;
    cm->cmd->commands = NULL;
    return cm;
}
void cm700_destroy(cm700_t *cm){
    //close the serial port
    if (cm->fd != -1){
        serial_close(cm->fd);
    }
    if (cm->out_buffer != NULL){
        free(cm->out_buffer);
    }
    if (cm->in_buffer != NULL){
        free(cm->in_buffer);
    }
    //destroy the structure
    free(cm);
}

bool cm700_send_address_vals(cm700_t *cm, dynamixel_cmd_address_vals_t *cmd){
    //printf("sending command\n");
    if (cm->out_buffer != NULL){
        free(cm->out_buffer);
    }
    /*  cm->out_buffer = (char *) calloc (6+6*(cmd->nservos)+1, sizeof(char));
        if (cm->out_buffer == NULL){
        return false;
        }
        sprintf(cm->out_buffer, "%s%c%1x", SERVO_COMMAND, SERVO_COMMAND_DATA_ADDRESSES, cmd->nservos);
        sprintf(&cm->out_buffer[4], "%02d", cmd->address);
        for(int i=0; i<cmd->nservos; i++){
        sprintf(&cm->out_buffer[6+6*i], "%02X", cmd->servo_id[i]);
        sprintf(&cm->out_buffer[8+6*i], "%04d", cmd->value[i]);
        }
      
        fprintf(stderr,"Cmd : %s\n" ,cm->out_buffer);
      
        //  char crc = generate_crc (&cm->out_buffer[0], 3+4*(cmd->nservos));
        //  cm->out_buffer[6+6*(cmd->nservos)] =  (( crc & 0xF0) >> 4) + '0';
        //  cm->out_buffer[6+6*(cmd->nservos) + 1] =  ( crc & 0x0F) + '0';
        cm->out_buffer[6+6*(cmd->nservos)] = '\n';*/
  
    int count;
    cm->out_buffer = (char *) calloc ((3+1+2+6*(cmd->nservos)+1), sizeof(char));
    sprintf(cm->out_buffer, "SCA%01x%02d", cmd->nservos, cmd->address); //SERVO_COMMAND, SERVO_COMMAND_DATA_ADDRESSES);
    for(int i=0; i<(cmd->nservos); i++)
        {
            sprintf(&(cm->out_buffer[6+6*i]), "%02x%04d", cmd->servo_id[i], cmd->value[i]);
        } 

    char crc = generate_crc (&cm->out_buffer[0], 6+6*(cmd->nservos));
    sprintf(&(cm->out_buffer[6+6*(cmd->nservos)]), "%c", (( crc & 0xF0) >> 4) + '0');
    sprintf(&(cm->out_buffer[6+6*(cmd->nservos)+1]), "%c", ( crc & 0x0F) + '0');
    sprintf(&(cm->out_buffer[6+6*(cmd->nservos)+2]), "\n");

    count = _fileutils_write_fully(cm->fd, cm->out_buffer, 3+1+2+6*(cmd->nservos)+3);  
    //fprintf(stderr,"Cmd : %s" ,cm->out_buffer);
    //count = _fileutils_write_fully(cm->fd, "SCA729000001010001020001030001040001050001060001\n", 97); 
    if (count < (3+1+2+6*(cmd->nservos)+3)){ 
        return false;
    } 

    //int count = write (cm->fd, cm->out_buffer, OUT_BUF_CALC(cm->nwrite_servos));
	
    //cm700_add_to_command_list(cm, cmd_list); //add command to list
	
    return true;

}

bool cm700_send_commands(cm700_t *cm, dynamixel_cmd_list_t * cmd_list){

    //	DINF("entered");
    if (cm == NULL){
        return false;
    }

    if (!fd_check(cm)){
        return false;
    }
    //printf ("Out Buffer = %s\n", &cm->out_buffer[0]); //printf ("Out Buffer = %s\n", &cm->out_buffer[0]);
    //check to see if the number of servos has changed
    if (cm->nwrite_servos != cmd_list->ncommands){
        //generate a new output buffer
        if (cm->out_buffer != NULL){
            free(cm->out_buffer);
        }
        cm->nwrite_servos = cmd_list->ncommands;

        //size of the buffer
        cm->out_buffer = (char *) 
            calloc (OUT_BUF_CALC(cm->nwrite_servos), sizeof(char));
        if (cm->out_buffer == NULL){
            return false;
        }
        sprintf (cm->out_buffer, "%s%c%1x", 
                 SERVO_COMMAND, 
                 SERVO_COMMAND_DATA,
                 cm->nwrite_servos);
        cm->out_buffer[OUT_BUF_CALC(cm->nwrite_servos) - 2] = '\r';
        cm->out_buffer[OUT_BUF_CALC(cm->nwrite_servos) - 1] = '\n';
    }
	
    //populate the outbuffer
    //	printf ("calculated buffer size with %d servos = %d\n", cm->nwrite_servos, OUT_BUF_CALC(cm->nwrite_servos));
	
    for (int i = 0; i < cm->nwrite_servos; i++){
        sprintf (&cm->out_buffer[ID_OFFSET(i)], 
                 "%02X", 
                 cmd_list->commands[i].servo_id); 
        //		printf ("servo id at offset %d = %2x [2]\n", ID_OFFSET(i), cmd_list->commands[i].servo_id);
        sprintf (&cm->out_buffer[TORQUE_EN_OFFSET(i)],
                 "%01d",
                 cmd_list->commands[i].torque_enable);
        //		printf ("servo torque en at offset %d = %1d [1]\n", TORQUE_EN_OFFSET(i), cmd_list->commands[i].torque_enable);
        sprintf (&cm->out_buffer[GOAL_POS_OFFSET(i)],
                 "%04d",
                 cmd_list->commands[i].goal_position);
        //		printf ("goal pos at offset %d = %4d [4]\n", GOAL_POS_OFFSET(i), cmd_list->commands[i].goal_position);
        sprintf (&cm->out_buffer[SPEED_OFFSET(i)],
                 "%04d",
                 cmd_list->commands[i].max_speed);
        //		printf ("speed at offset %d = %4d [4]\n", SPEED_OFFSET(i), cmd_list->commands[i].max_speed);
        sprintf (&cm->out_buffer[TORQUE_OFFSET(i)],
                 "%04d",
                 cmd_list->commands[i].max_torque);
        //		printf ("torque at offset %d = %4d [4]\n", TORQUE_OFFSET(i), cmd_list->commands[i].max_torque);
    }

    //fprintf(stderr,"Cmd : %s\n" ,cm->out_buffer);

    char crc = generate_crc (&cm->out_buffer[0], OUT_BUF_CALC(cm->nwrite_servos) - 3);
    cm->out_buffer[OUT_BUF_CALC(cm->nwrite_servos) - 3] =  (( crc & 0xF0) >> 4) + '0';
    cm->out_buffer[OUT_BUF_CALC(cm->nwrite_servos) - 2] =  ( crc & 0x0F) + '0';
    cm->out_buffer[OUT_BUF_CALC(cm->nwrite_servos) - 1] = '\n';
    //printf ("Out Buffer = %s\n", &cm->out_buffer[0]);

    //this is where the writing to the device happens 
    //printf("%d\n", cm->fd);
    int count = _fileutils_write_fully(cm->fd, cm->out_buffer, OUT_BUF_CALC(cm->nwrite_servos));
	
    //int count = write (cm->fd, cm->out_buffer, OUT_BUF_CALC(cm->nwrite_servos));
    if (count < OUT_BUF_CALC(cm->nwrite_servos)){
        return false;
    }
	
    cm700_add_to_command_list(cm, cmd_list); //add command to list
	
    return true;
}

bool cm700_read_status(cm700_t *cm){
    //	DINF("entered");
    if (cm == NULL){
        return false;
    }
    if (!fd_check(cm)){
        return false;
    }
    //cm700_print_debug(cm);
    //check if there is enough to read the status
    int count = _fileutils_read_fully_timeout(cm->fd, cm->in_buffer, BUFFER_SIZE, READ_TIMEOUT);

    if(count > 2){
        if(cm->in_buffer[0] == 'S' && cm->in_buffer[1] == 'S' && cm->in_buffer[2] =='A'){
            //printf("%s\n", cm->in_buffer);
        }
    }
    //cm700_print_debug(cm);

    //find out if we got all the information

    if (count < IN_SERVO_OFFSET + 2){
        //not enough for even a read
        //printf ("Error: not enough data to even get the counts!\n");
        return false;
    }

    int servo_count = 0;
    int sensor_count = 0;

    sscanf (cm->in_buffer, "SS%1d%1d", &servo_count, &sensor_count);
    //	printf ("servo count = %d, sensor_count = %d\n", servo_count, sensor_count);	
    if (cm->nread_servos != servo_count || cm->nsensors != sensor_count){
        //		printf ("changing servo count from %d to %d, and sensor_count from %d to %d\n",
        //					cm->nread_servos, servo_count,
        //					cm->nsensors, sensor_count);
        //need to regenerate a new read buffer	
        //free(cm->in_buffer);
        cm->nread_servos = servo_count;
        cm->nsensors = sensor_count;
        //cm->in_buffer = (char *)
        //		calloc(
        //				IN_BUF_CALC(cm->nread_servos, cm->nsensors), 
        //				sizeof (char));
        //printf ("calculated new in buffer size\n");
        if (cm->dsl != NULL){
            dynamixel_status_list_t_destroy(cm->dsl);
        }

        //need to generate a new sensor list
        cm->dsl = generate_status_list(cm->nread_servos, cm->nsensors);
        //return false;
    }

    //	printf ("DSL value\n");
    //	printf ("dsl = %p\n", cm->dsl);
    //	printf ("dsl->servo_id %p\n", &cm->dsl->servos[0].servo_id);
    //	printf ("debug data...\n");
    //	cm700_print_debug(cm);

    /*
      if (count != IN_BUF_CALC(cm->nread_servos, cm->nsensors)){
      printf ("%d didn't match %d\n", count, IN_BUF_CALC(cm->nread_servos, cm->nsensors));
      printf ("didn't read enough bytes to parse\n");
      return false;
      }
    */
    if (!populate_status_list(cm)){
        printf ("Error: didn't populate status_list\n");
        return false;
    }
    cm->dsl->utime = _timestamp_now();

    //	DINF("exiting");
    return true;

}
int cm700_get_fd(cm700_t *cm){
    if (cm == NULL){
        return -1;
    }

    return cm->fd;
}

int	cm700_get_num_of_servos(cm700_t *cm){
    //DINF("entered");
    if (!fd_check(cm)){
        return -1;
    }
    //call the command to get the number of servos
    char buffer[BUFFER_SIZE];
    int count = snprintf (
                          &buffer[0], 
                          BUFFER_SIZE, 
                          "%s%c\n", 
                          SERVO_COMMAND, 
                          SERVO_QUERY_SERVO); 
	
    //int write_count = write (cm->fd, &buffer[0], count);
    int write_count = _fileutils_write_fully(cm->fd, &buffer[0], count);
    if (write_count != count){
        printf ("Error: output count doesn't equal input count!\n");
    }

    //	int read_count = read(
    //							cm->fd, 
    //							&buffer[0], 
    //							strlen (SERVO_COMMAND_SUCCESS) + 
    //								2 /*size of 2 digit variable*/ + 
    //								1 /*size of '\n'*/+ 
    //								1 /*size of '\0'*/);  
    int read_count = _fileutils_read_fully_timeout(
                                                   cm->fd, 
                                                   &buffer[0], 
                                                   strlen (SERVO_COMMAND_SUCCESS) + 
                                                   2 /*size of 2 digit variable*/ + 
                                                   1 /*size of '\n'*/+ 
                                                   1 /*size of '\0'*/,
                                                   READ_TIMEOUT);  

    if (read_count <= 0){
        return -1;
    }
    int num_servos = -1; //if -1 is returned then the read failed
    sscanf (&buffer[0], SERVO_COMMAND_SUCCESS  "%d\n", &num_servos);

    return num_servos;
}
int cm700_get_sensor_count(cm700_t *cm){
    //DINF("entered");
    if (!fd_check(cm)){
        return -1;
    }

    //call the command to get the number of servos
    char buffer[BUFFER_SIZE];
    int count = snprintf (
                          &buffer[0], 
                          BUFFER_SIZE, 
                          "%s%c\n", 
                          SERVO_COMMAND, 
                          SERVO_QUERY_SENSOR); 
	
    //int write_count = write (cm->fd, &buffer[0], count);
    int write_count = _fileutils_write_fully(cm->fd, &buffer[0], count);
    if (write_count != count){
        printf ("Error: output count doesn't equal input count!\n");
    }
    //	int read_count = read(
    //							cm->fd, 
    //							&buffer[0], 
    //							strlen (SERVO_COMMAND_SUCCESS) + 
    //								2 /*size of 2 digit variable*/ + 
    //								1 /*size of '\n'*/+ 
    //								1 /*size of '\0'*/);  


    //	int read_count = _fileutils_read_fully_timeout(
    //							cm->fd, 
    //							&buffer[0], 
    //							strlen (SERVO_COMMAND_SUCCESS) + 
    //								2 /*size of 2 digit variable*/ + 
    //								1 /*size of '\n'*/+ 
    //								1 /*size of '\0'*/,
    //								READ_TIMEOUT);  
    int read_count = _fileutils_read_fully_timeout(
                                                   cm->fd, 
                                                   &buffer[0], 
                                                   BUFFER_SIZE,
                                                   READ_TIMEOUT);  



    if (read_count <= 0){
        return -1;
    }
    int num_sensors = -1; //if -1 is returned then the read failed
    sscanf (&buffer[0], SERVO_COMMAND_SUCCESS  "%d\n", &num_sensors);

    return num_sensors;

}
bool cm700_clear_sensors(cm700_t *cm){
		
    if (!fd_check(cm)){
        return false;
    }
	
    //call the command to get the number of servos
    char buffer[BUFFER_SIZE];
    int count = snprintf (
                          &buffer[0], 
                          BUFFER_SIZE, 
                          "%s%c\r\n", 
                          SERVO_COMMAND, 
                          SERVO_CLEAR_SENSORS); 
    buffer[count] = 0;
    //printf ("writing size = %d: %s", count, &buffer[0]);
		
    int write_count = _fileutils_write_fully(cm->fd, &buffer[0], count );
    //int write_count = write (cm->fd, &buffer[0], count);
    if (write_count != count){
        DERR("output count doesn't equal input count!");
    }

    //	int read_count = read(
    //							cm->fd, 
    //							&buffer[0], 
    //							strlen (SERVO_COMMAND_SUCCESS) +
    //							1 /*size of '\n'*/);  

    memset (&buffer[0], 0, BUFFER_SIZE);

    //	int read_count = _fileutils_read_fully_timeout(
    //							cm->fd, 
    //							&buffer[0], 
    //							strlen (SERVO_COMMAND_SUCCESS) 
    //								 /*size of '\n'*/,
    //							READ_TIMEOUT);  
    int read_count = _fileutils_read_fully_timeout(
                                                   cm->fd, 
                                                   &buffer[0], 
                                                   BUFFER_SIZE,
                                                   READ_TIMEOUT);  

    //printf ("read count = %d\n", read_count);

    if (read_count <= 0){
        DERR("read fail");
        printf ("read count == %d\n", read_count);
        return false;
    }

    if (strncmp (&buffer[0], SERVO_COMMAND_SUCCESS, strlen(SERVO_COMMAND_SUCCESS)) == 0){
        return true;
    }
    return false;
}
bool cm700_add_sensor(cm700_t *cm, int sensor_address){
    //DINF("entered");
    if (!fd_check(cm)){
        return false;
    }	
    //call the command to get add a sensor
    char buffer[BUFFER_SIZE];
    int count = snprintf (
                          &buffer[0], 
                          BUFFER_SIZE, 
                          "%s%c%d\r\n", 
                          SERVO_COMMAND, 
                          SERVO_ADD_SENSOR,
                          sensor_address); 
    buffer[count] = 0;
	
    //printf ("writing: %d characters string == %s", count, &buffer[0]);
	
    int write_count = _fileutils_write_fully(cm->fd, &buffer[0], count);
    //int write_count = write (cm->fd, &buffer[0], count);
    if (write_count != count){
        printf ("Error: output count doesn't equal input count!\n");
    }
    //	int read_count = read(
    //							cm->fd, 
    //							&buffer[0], 
    //							strlen (SERVO_COMMAND_SUCCESS) +
    //							1 /*size of '\n'*/);  

    /*	memset (&buffer[0], 0, BUFFER_SIZE);
        int read_count = _fileutils_read_fully_timeout(
        cm->fd, 
        &buffer[0], 
        strlen (SERVO_COMMAND_SUCCESS) + 
        1,
        READ_TIMEOUT);  
    */
    int read_count = _fileutils_read_fully_timeout(
                                                   cm->fd, 
                                                   &buffer[0], 
                                                   BUFFER_SIZE,
                                                   READ_TIMEOUT);  


    if (read_count <= 0){
        return false;
    }

    if (strncmp (&buffer[0], SERVO_COMMAND_SUCCESS, strlen(SERVO_COMMAND_SUCCESS)) == 0){
        return true;
    }
    return false;
}
bool cm700_remove_sensor(cm700_t *cm, int sensor_address){
    //DINF("entered");
    if (!fd_check(cm)){
        return false;
    }
    //call the command to get add a sensor
    char buffer[BUFFER_SIZE];
    int count = snprintf (
                          &buffer[0], 
                          BUFFER_SIZE, 
                          "%s%c%d\n", 
                          SERVO_COMMAND, 
                          SERVO_REMOVE_SENSOR,
                          sensor_address); 
	
    int write_count = _fileutils_write_fully(cm->fd, &buffer[0], count);
    //int write_count = write (cm->fd, &buffer[0], count);
    if (write_count != count){
        printf ("Error: output count doesn't equal input count!\n");
    }

    //	int read_count = read(
    //							cm->fd, 
    //							&buffer[0], 
    //							strlen (SERVO_COMMAND_SUCCESS) +
    //							1 /*size of '\n'*/);  
    //	int read_count = _fileutils_read_fully_timeout(
    //							cm->fd, 
    //				o			&buffer[0], 
    //							strlen (SERVO_COMMAND_SUCCESS) + 
    //								1 /*size of '\n'*/ +
    //								1,
    //								READ_TIMEOUT);  
    int read_count = _fileutils_read_fully_timeout(
                                                   cm->fd, 
                                                   &buffer[0], 
                                                   BUFFER_SIZE,
                                                   READ_TIMEOUT);  



    if (read_count <= 0){
        return false;
    }

    if (strncmp (&buffer[0], SERVO_COMMAND_SUCCESS, strlen(SERVO_COMMAND_SUCCESS)) == 0){
        return true;
    }
    return false;
}
//private functions
dynamixel_status_list_t * generate_status_list(int nservos, int nsensors){
    //DINF("entered");
    //printf ("servos = %d, sensors = %d\n", nservos, nsensors);
    dynamixel_status_list_t *dsl = (dynamixel_status_list_t *) 
        calloc (1, sizeof (dynamixel_status_list_t));	
    dsl->utime = 0;
    dsl->nservos = nservos;
    //populate all the servos
    dynamixel_status_t * servos = calloc (nservos, sizeof (dynamixel_status_t));;
	
    for (int i = 0; i < nservos; i++){
        servos[i].num_values = nsensors;
        servos[i].addresses = calloc(nsensors, sizeof (int16_t));
        servos[i].values = calloc(nsensors, sizeof (int32_t));
        if (servos[i].addresses == NULL) {
            DERR("Addresses == NULL\n");
        }
        if (servos[i].values == NULL){
            DERR ("Values == NULL\n");
        }
    }

    dsl->servos = servos;
    return dsl;
}

bool populate_status_list(cm700_t *cm){	
    int servo_count = 0;
    int sensor_count = 0;
    sscanf (cm->in_buffer, "SS%1d%1d", &servo_count, &sensor_count);
	
    //printf ("inbuffer = %s\n", cm->in_buffer);
    //	printf ("length = %d\n", (int)strlen(cm->in_buffer));
    if ((int) strlen(cm->in_buffer) == 0){
        //DINF ("exiting");
        return false;
    }

    for (int i = 0; i < servo_count; i++){
        sscanf(
               &cm->in_buffer[IN_SERVO_ID_OFFSET(i, sensor_count)], 
               "%2X", 
               (int *)&cm->dsl->servos[i].servo_id);
        //		printf ("servo_id = %d\n", cm->dsl->servos[i].servo_id);
        for (int j = 0; j < sensor_count; j++){
      
            sscanf(
                   &cm->in_buffer[IN_SEN_ADDRESS_OFFSET(i, sensor_count, j)],
                   "%02X",
                   (unsigned int *)&cm->dsl->servos[i].addresses[j]);
            sscanf (
                    &cm->in_buffer[IN_SEN_VALUE_OFFSET(i, sensor_count, j)],
                    "%04X",
                    &cm->dsl->servos[i].values[j]);
            //			printf ("\tsensor address = %4d : %d\n", cm->dsl->servos[i].addresses[j], cm->dsl->servos[i].values[j]);
      
        }
        cm->dsl->servos[i].num_values = sensor_count;
    }
  
    return true;	
}


static int 
_fileutils_write_fully(int fd, const void *b, int len){
    int cnt=0;
    int thiscnt;
    unsigned char *bb=(unsigned char*) b;

    while (cnt<len){
        thiscnt=write(fd, &bb[cnt], len-cnt);
        if (thiscnt<0) {
            perror ("write");
            return -1;
        }
        cnt+=thiscnt;
    }

    return cnt;
}


/* returns -1 on error, 0 on timeout, nchar on success. */
static int 
_fileutils_read_timeout(int fd, char *buf, int maxlen, int msTimeout)
{
    //DINF("entered");
    //printf ("timeout: %d\n", msTimeout);
    struct pollfd pfd;
    int len;
    int res;
                                                                                             
    pfd.fd=fd;
    pfd.events=POLLIN;
    char *in_buff = buf;

    res=poll(&pfd, 1, msTimeout);
    //printf ("res = %d\n", res);
    if (res<0){
        //error
        perror("poll");
        DERR("poll");
        return -1;
    }
	
    if (res==0){
        //timeout
        DERR("timeout");
        errno=ETIMEDOUT;
        return 0;
    }
	

    len=read(fd, in_buff, maxlen);

    if (len<0){
        DERR("read");
        perror("read");
        return -1;
    }
    if (len == 0) {
        //fprintf (stderr, "end of file\n");
        return -1;
    }
    return len;
}

/* returns -1 on error, nchar on timeout or success. */
static int 
_fileutils_read_fully_timeout(int fd, char *bufin, int len, int msTimeout){
    //	DINF("entered");
    char *buf=(char*) bufin;
    int readsofar=0;
    int thisread=0;
    memset (buf, 0, len);

    while (readsofar<len){
        thisread=_fileutils_read_timeout(fd, &buf[readsofar], len-readsofar, msTimeout);
        if (thisread == 0){
            return readsofar;
        }
        if (thisread < 0){
            return thisread;
        }

        readsofar+=thisread;
        if (buf[readsofar - 1] == '\n'){
            //			printf ("end of line!!\n");
            break;
        }
    }

    return len;
}

int cm700_flush(cm700_t *cm){
    char buf[BUFFER_SIZE];
    //DINF("flushing");
    while (_fileutils_read_fully_timeout(cm->fd, &buf[0], BUFFER_SIZE, 200) > 0){};
    return 0;
	
		
}
bool cm700_start(cm700_t *cm){
    char buffer[BUFFER_SIZE];
    int count = snprintf (
                          &buffer[0], 
                          BUFFER_SIZE, 
                          "%s%c\r\n", 
                          SERVO_COMMAND, 
                          SERVO_START); 
    buffer[count] = 0;
    //printf ("writing size = %d: %s", count, &buffer[0]);
		
    int write_count = _fileutils_write_fully(cm->fd, &buffer[0], count );
    //int write_count = write (cm->fd, &buffer[0], count);
    if (write_count != count){
        DERR("output count doesn't equal input count!");
    }
    memset (&buffer[0], 0, BUFFER_SIZE);
    int read_count = _fileutils_read_fully_timeout(
                                                   cm->fd, 
                                                   &buffer[0], 
                                                   BUFFER_SIZE,
                                                   READ_TIMEOUT);  

    //printf ("read count = %d\n", read_count);

    if (read_count <= 0){
        DERR("read fail");
        printf ("read count == %d\n", read_count);
        return false;
    }

    if (strncmp (&buffer[0], SERVO_COMMAND_SUCCESS, strlen(SERVO_COMMAND_SUCCESS)) == 0){
        return true;
    }
    return false;
}
bool cm700_stop(cm700_t *cm){
    char buffer[BUFFER_SIZE];
    int count = snprintf (
                          &buffer[0], 
                          BUFFER_SIZE, 
                          "%s%c\r\n", 
                          SERVO_COMMAND, 
                          SERVO_STOP); 
    buffer[count] = 0;
    //printf ("writing size = %d: %s", count, &buffer[0]);
		
    int write_count = _fileutils_write_fully(cm->fd, &buffer[0], count );
    //int write_count = write (cm->fd, &buffer[0], count);
    if (write_count != count){
        DERR("output count doesn't equal input count!");
    }
    memset (&buffer[0], 0, BUFFER_SIZE);
    int read_count = _fileutils_read_fully_timeout(
                                                   cm->fd, 
                                                   &buffer[0], 
                                                   BUFFER_SIZE,
                                                   READ_TIMEOUT);  

    //printf ("read count = %d\n", read_count);

    if (read_count <= 0){
        DERR("read fail");
        printf ("read count == %d\n", read_count);
        return false;
    }

    if (strncmp (&buffer[0], SERVO_COMMAND_SUCCESS, strlen(SERVO_COMMAND_SUCCESS)) == 0){
        return true;
    }
    return false;
}
void cm700_print_debug(cm700_t * cm){
    for (int i = 0; i < cm->nread_servos; i++){
        for (int j = 0; j < cm->nsensors; j++){			
            printf ("cm->dsl->servos[%d].values[%d] = %d\n", 
                    i, 
                    j, 
                    cm->dsl->servos[i].values[j]);
            printf ("cm->dsl->servos[%d].addresses[%d] = %d\n", 
                    i,
                    j,
                    cm->dsl->servos[i].addresses[j]);
        }
    }

}
dynamixel_status_list_t * cm700_get_dsl(cm700_t *cm){
    return cm->dsl;
}

char generate_crc(char * buffer, int size){
    char crc = 0;
    for (int i = 0; i < size; i++){
        crc += buffer[i];
    }
    return crc;
}
