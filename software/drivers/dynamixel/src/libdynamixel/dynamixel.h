#ifndef __dynamixel_driver_h__
#define __dynamixel_driver_h__

/**
 * SECTION: dynamixel
 * @title: Dynamixel servo driver
 * @short_description: Functions for interfacing with a Dynamixel servo
 * @include: libdynamixel/dynamixel.h
 *
 * These functions should be capable of interfacing with the following servos:
 *      AX-12+
 *      AX-18F
 *      RX-10
 *      DX-117
 *      RX-24F
 *      RX-28
 *      RX-64
 *      EX-106+
 *
 * Usage requires a valid file descriptor (presumably an opened serial port).
 * These functions do not include routines for establishing and configuring the
 * serial connection.
 */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define DYNAMIXEL_BROADCAST_ID 0xFE

// dynamixel_read_uint16(fd, unit_id, DYNAMIXEL_CTL_MODEL_NUMBER) should yield
// this value for AX-18F servos
#define DYNAMIXEL_MODEL_NUMBER_AX18 0x0012

#define DYNAMIXEL_UNIT_ID_MIN 1
#define DYNAMIXEL_UNIT_ID_MAX 253

#define DYNAMIXEL_CMD_PING         0x01
#define DYNAMIXEL_CMD_READ_DATA    0x02
#define DYNAMIXEL_CMD_WRITE_DATA   0x03
#define DYNAMIXEL_CMD_REG_WRITE    0x04
#define DYNAMIXEL_CMD_ACTION       0x05
#define DYNAMIXEL_CMD_RESET        0x06
#define DYNAMIXEL_CMD_SYNC_WRITE   0x83

typedef enum {
    DYNAMIXEL_ERR_INPUT_VOLTAGE_FLAG = (1 << 0),
    DYNAMIXEL_ERR_ANGLE_LIMIT_FLAG   = (1 << 1),
    DYNAMIXEL_ERR_OVERHEATING_FLAG   = (1 << 2),
    DYNAMIXEL_ERR_RANGE_FLAG         = (1 << 3),
    DYNAMIXEL_ERR_CHECKSUM_FLAG      = (1 << 4),
    DYNAMIXEL_ERR_OVERLOAD_FLAG      = (1 << 5),
    DYNAMIXEL_ERR_INSTRUCTION_FLAG   = (1 << 6),

    DYNAMIXEL_ERR_COMM_FLAG          = (1 << 16)
} dynamixel_error_flag_t;

typedef enum {
    // === EEPROM ===
    DYNAMIXEL_CTL_MODEL_NUMBER = 0,     // model number lower byte
    DYNAMIXEL_CTL_MODEL_NUMBER_H,       // model number upper byte
    DYNAMIXEL_CTL_FIRMWARE_VERSION,
    DYNAMIXEL_CTL_ID,
    DYNAMIXEL_CTL_BAUD_RATE,
    DYNAMIXEL_CTL_RETURN_DELAY_TIME,
    DYNAMIXEL_CTL_CW_ANGLE_LIMIT,
    DYNAMIXEL_CTL_CW_ANGLE_LIMIT_H,
    DYNAMIXEL_CTL_CCW_ANGLE_LIMIT,
    DYNAMIXEL_CTL_CCW_ANGLE_LIMIT_H,
    // reserved
    DYNAMIXEL_CTL_TEMP_LIMIT_HIGH = 0x0B,
    DYNAMIXEL_CTL_VOLTAGE_LIMIT_LOW,
    DYNAMIXEL_CTL_VOLTAGE_LIMIT_HIGH,
    DYNAMIXEL_CTL_TORQUE_MAX,
    DYNAMIXEL_CTL_TORQUE_MAX_H,
    DYNAMIXEL_CTL_STATUS_RETURN_LEVEL,
    DYNAMIXEL_CTL_ALARM_LED,
    DYNAMIXEL_CTL_ALARM_SHUTDOWN,
    // reserved
    DYNAMIXEL_CTL_DOWN_CALIBRATION = 0x14,
    DYNAMIXEL_CTL_DOWN_CALIBRATION_H,
    DYNAMIXEL_CTL_UP_CALIBRATION,
    DYNAMIXEL_CTL_UP_CALIBRATION_H,

    // === RAM ===
    DYNAMIXEL_CTL_TORQUE_ENABLE,
    DYNAMIXEL_CTL_LED,
    DYNAMIXEL_CTL_CW_COMPLIANCE_MARGIN,
    DYNAMIXEL_CTL_CCW_COMPLIANCE_MARGIN,
    DYNAMIXEL_CTL_CW_COMPLIANCE_SLOPE,
    DYNAMIXEL_CTL_CCW_COMPLIANCE_SLOPE,
    DYNAMIXEL_CTL_GOAL_POSITION,
    DYNAMIXEL_CTL_GOAL_POSITION_H,
    DYNAMIXEL_CTL_MOVING_SPEED,
    DYNAMIXEL_CTL_MOVING_SPEED_H,
    DYNAMIXEL_CTL_TORQUE_LIMIT,
    DYNAMIXEL_CTL_TORQUE_LIMIT_H,
    DYNAMIXEL_CTL_PRESENT_POSITION,
    DYNAMIXEL_CTL_PRESENT_POSITION_H,
    DYNAMIXEL_CTL_PRESENT_SPEED,
    DYNAMIXEL_CTL_PRESENT_SPEED_H,
    DYNAMIXEL_CTL_PRESENT_LOAD,
    DYNAMIXEL_CTL_PRESENT_LOAD_H,
    DYNAMIXEL_CTL_PRESENT_VOLTAGE,
    DYNAMIXEL_CTL_PRESENT_TEMP,
    DYNAMIXEL_CTL_REGISTERED_INSTRUCTION,
    // reserved
    DYNAMIXEL_CTL_MOVING = 0x2E,
    DYNAMIXEL_CTL_LOCK,
    DYNAMIXEL_CTL_PUNCH,
    DYNAMIXEL_CTL_PUNCH_H,
} dynamixel_control_table_address_t;


/**
 * dynamixel_baud_rate_t:
 * 
 * Baud rate values suitable for reading and writing with
 * DYNAMIXEL_CTL_BAUD_RATE
 */
typedef enum  {
    DYNAMIXEL_BAUD_1000000 = 1,
    DYNAMIXEL_BAUD_500000  = 3,
    DYNAMIXEL_BAUD_400000  = 4,
    DYNAMIXEL_BAUD_250000  = 7,
    DYNAMIXEL_BAUD_200000  = 9,
    DYNAMIXEL_BAUD_115200  = 16,
    DYNAMIXEL_BAUD_57600   = 34,
    DYNAMIXEL_BAUD_19200   = 103,
    DYNAMIXEL_BAUD_9600    = 207
} dynamixel_baud_rate_t;

/**
 * dynamixel_ping:
 * sends a PING command.
 *
 * Returns: 0 on success, or sets %dynamixel_error_flag_t values on failure.
 */
int dynamixel_ping(int serial_fd, uint8_t unit_id, uint8_t *responder_id);

/**
 * dynamixel_read:
 * 
 * Reads a value from the servo control table.
 *
 * Returns: 0 on success, or sets %dynamixel_error_flag_t values on failure.
 */
int dynamixel_read(int serial_fd, uint8_t unit_id, uint8_t address,
        uint16_t *result);

/**
 * dynamixel_write:
 *
 * Write a value into the servo control table.
 *
 * Returns: 0 on success, or sets %dynamixel_error_flag_t values on failure.
 */
int dynamixel_write(int serial_fd, uint8_t unit_id, uint8_t address, 
        uint16_t val);

/**
 * dynamixel_reg_write:
 *
 * Write a value into the servo control table, but delay execution until
 * dynamixel_action() is invoked.
 *
 * Returns: 0 on success, or sets %dynamixel_error_flag_t values on failure.
 */
int dynamixel_reg_write(int serial_fd, uint8_t unit_id, uint8_t address,
        uint16_t val);

/**
 * dynamixel_action:
 *
 * Commands the servo to execute all pending instructions that were queued up
 * with dynamixel_reg_write().
 *
 * Returns: 0 on success, or sets %dynamixel_error_flag_t values on failure.
 */
int dynamixel_action(int serial_fd, uint8_t unit_id);

/**
 * dynamixel_reset:
 *
 * Resets the servo control tables to factory defaults.
 *
 * Returns: 0 on success, or sets %dynamixel_error_flag_t values on failure.
 */
int dynamixel_reset(int serial_fd, uint8_t unit_id);

/**
 * dynamixel_sync_write:
 * @serial_fd:  serial port file descriptor
 * @address:    the address to write to (see: dynamixel_control_table_address_t)
 * @nservos:    the number of servos to address
 * @unit_id:    unit IDs for each servo
 * @params:     parameter values to set, one for each servo.
 *
 * Set values for multiple servos at once.  This command does not receive
 * feedback from individual servos.
 *
 * Returns: 0 if the command was sent, or sets %dynamixel_error_flag_t on failure.
 */
int dynamixel_sync_write(int serial_fd, uint8_t address, int nservos, 
        const uint8_t *unit_ids, const uint16_t *params);

/**
 * dynamixel_print_errors:
 *
 * Debugging function.  Prints error messages to stderr.
 */
void dynamixel_print_errors(int status);

#ifdef __cplusplus
}
#endif

#endif
