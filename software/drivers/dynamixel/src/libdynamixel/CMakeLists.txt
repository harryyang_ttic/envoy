add_definitions(-std=gnu99 -Wall)

add_library(dynamixel SHARED dynamixel.c)

# set the library API version.  Increment this every time the public API
# changes.
set_target_properties(dynamixel PROPERTIES SOVERSION 1)

pods_install_headers(dynamixel.h DESTINATION dynamixel)

pods_install_libraries(dynamixel)

pods_install_pkg_config_file(dynamixel
    VERSION 0.0.1
    LIBS -ldynamixel 
    CFLAGS ""
    )
