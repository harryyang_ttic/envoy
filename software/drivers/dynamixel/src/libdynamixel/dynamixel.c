#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <poll.h>
#include <errno.h>

#include "dynamixel.h"

#define MAX_PARAMS 255
#define MAX_STATUS_PACKET_SIZE_BYTES 1024

//#define dbg(...) do { fprintf(stderr, "%s:%d: ", __FILE__, __LINE__); fprintf(stderr, __VA_ARGS__); } while(0)
#define dbg(...)

#define err(...) do { fprintf(stderr, "%s:%d: ", __FILE__, __LINE__); fprintf(stderr, __VA_ARGS__); } while(0)


#define DEFAULT_TIMEOUT_MS 500

typedef struct _InstructionPacket {
    uint8_t unit_id;
    uint8_t instruction;
    uint8_t nparams;
    uint8_t params[MAX_PARAMS];
} InstructionPacket;

typedef struct _StatusPacket {
    uint8_t unit_id;
    uint8_t error;
    uint8_t nparams;
    uint8_t params[MAX_PARAMS];
} StatusPacket;

static inline uint16_t bytes_to_uint16_t(uint8_t *data) {
    return data[0] | (data[1] << 8);
}

static inline void dump_packet(uint8_t *data, int len) {
    fprintf(stderr, "[");
    for(int i=0; i<len; i++) {
        fprintf(stderr, " %02X", data[i]);
    }
    fprintf(stderr, " ]\n");
}

static int _fileutils_write_fully(int fd, const void *b, int len);
static int _fileutils_read_fully_timeout(int fd, void *bufin, int len, int msTimeout);

typedef struct _ErrorStr {
    int flag;
    const char *msg;
} ErrorStr;

ErrorStr ERR_MSGS[] = {
    { DYNAMIXEL_ERR_INPUT_VOLTAGE_FLAG, "Input voltage error" },
    { DYNAMIXEL_ERR_ANGLE_LIMIT_FLAG,   "Angle limit error" },
    { DYNAMIXEL_ERR_OVERHEATING_FLAG,   "Overheating error" },
    { DYNAMIXEL_ERR_RANGE_FLAG,         "Range error" },
    { DYNAMIXEL_ERR_CHECKSUM_FLAG,      "Checksum error" },
    { DYNAMIXEL_ERR_OVERLOAD_FLAG,      "Overload error" },
    { DYNAMIXEL_ERR_INSTRUCTION_FLAG,   "Instruction error" },
    { DYNAMIXEL_ERR_COMM_FLAG,          "Communication error" },
    { 0, NULL }
};

void
dynamixel_print_errors(int error) 
{
    for(int i=0; ERR_MSGS[i].msg; i++) {
        if(error & ERR_MSGS[i].flag) {
            fprintf(stderr, "Dynamixel: %s\n", ERR_MSGS[i].msg);
        }
    }
}

static int 
dynamixel_send_instruction(int serial_fd, const InstructionPacket *cmd,
        StatusPacket *result)
{
    int msglen = cmd->nparams + 6;
    uint8_t msg[msglen];
    msg[0] = 0xff;
    msg[1] = 0xff;
    msg[2] = cmd->unit_id;
    msg[3] = cmd->nparams + 2;
    msg[4] = cmd->instruction;
    uint8_t checksum = msg[2] + msg[3] + msg[4];
    for(int i=0; i<cmd->nparams; i++) {
        msg[5 + i] = cmd->params[i];
        checksum += cmd->params[i];
    }
    msg[5 + cmd->nparams] = ~checksum;

//    dbg("Transmitting: ");
//    dump_packet(msg, msglen);

    int status = _fileutils_write_fully(serial_fd, msg, msglen);
    if(status < 0) {
        err("write failed\n");
        return status;
    }

    if(result)
        memset(result, 0, sizeof(StatusPacket));

    int response_expected = (cmd->unit_id != DYNAMIXEL_BROADCAST_ID || 
            cmd->instruction == DYNAMIXEL_CMD_PING);
    if(response_expected) {
        uint8_t response_header[4];
        // first 4 bytes of response tells us how long the response is
        status = _fileutils_read_fully_timeout(serial_fd, response_header, 
                4, DEFAULT_TIMEOUT_MS);
        if(status < 0) {
            err("Error reading response\n");
            return status;
        }
        if(status < 4) {
            err("Timeout waiting for response\n");
            return -1;
        }

        // check header magic bytes
        if(response_header[0] != 0xFF || response_header[1] != 0xFF) {
            err("received invalid response header 0x%02X 0x%02X\n", 
                response_header[0], response_header[1]);
            return -1;
        }

        int length = response_header[3];
        // read the rest of the response
        uint8_t remainder[length];
        status = _fileutils_read_fully_timeout(serial_fd, remainder, length, 
                DEFAULT_TIMEOUT_MS);
        if(status < 0) {
            err("Error reading response\n");
            return status;
        }
        uint8_t checksum = response_header[2] + response_header[3];
        for(int j=0; j<length - 1; j++) 
            checksum += remainder[j];
        checksum = ~checksum;

        // verify packet checksum
        if(checksum != remainder[length-1]) {
            err("Status packet failed checksum (expected 0x%02X, received 0x%02X)\n",
                    checksum, remainder[length-1]);
            return status;
        }

        if(remainder[0])
            dynamixel_print_errors(remainder[0]);

        // fill in result
        if(result) {
            memset(result, 0, sizeof(StatusPacket));
            result->unit_id = response_header[2];
            result->nparams = length - 2;
            result->error = remainder[0];

            for(int i=0; i<length - 2; i++) {
                result->params[i] = remainder[1 + i];
            }

        }
        dbg("response 0x%02X received from unit 0x%02X\n", remainder[0], 
                response_header[2]);
    }
    return 0;
}

static int
dynamixel_write_data(int serial_fd, uint8_t unit_id, uint8_t address, 
        uint8_t instruction, uint8_t len, const uint8_t *data, 
        StatusPacket *result)
{
    InstructionPacket cmd;
    memset(&cmd, 0, sizeof(cmd));
    cmd.unit_id = unit_id;
    cmd.instruction = instruction;
    cmd.nparams = 1 + len;
    cmd.params[0] = address;
    if(data) {
        memcpy(cmd.params + 1, data, len);
    }

    int comm_status = dynamixel_send_instruction(serial_fd, &cmd, result);
    if(comm_status < 0) {
        dbg("write data failed\n");
        return DYNAMIXEL_ERR_COMM_FLAG;
    }
    return result->error;
}

static int
dynamixel_read_data(int serial_fd, uint8_t unit_id, uint8_t address, uint8_t length, 
        StatusPacket *result)
{
    InstructionPacket cmd;
    memset(&cmd, 0, sizeof(cmd));
    cmd.unit_id = unit_id;
    cmd.instruction = DYNAMIXEL_CMD_READ_DATA;
    cmd.nparams = 2;
    cmd.params[0] = address;
    cmd.params[1] = length;
    int comm_status = dynamixel_send_instruction(serial_fd, &cmd, result);
    if(comm_status < 0) {
        dbg("read data failed\n");
        return DYNAMIXEL_ERR_COMM_FLAG;
    }
    return result->error;
}

static int
_get_control_nbits(uint8_t address)
{
    switch(address) {
        case DYNAMIXEL_CTL_MODEL_NUMBER:
        case DYNAMIXEL_CTL_CW_ANGLE_LIMIT:
        case DYNAMIXEL_CTL_CCW_ANGLE_LIMIT:
        case DYNAMIXEL_CTL_TORQUE_MAX:
        case DYNAMIXEL_CTL_DOWN_CALIBRATION:
        case DYNAMIXEL_CTL_UP_CALIBRATION:
        case DYNAMIXEL_CTL_GOAL_POSITION:
        case DYNAMIXEL_CTL_MOVING_SPEED:
        case DYNAMIXEL_CTL_TORQUE_LIMIT:
        case DYNAMIXEL_CTL_PRESENT_POSITION:
        case DYNAMIXEL_CTL_PRESENT_SPEED:
        case DYNAMIXEL_CTL_PRESENT_LOAD:
        case DYNAMIXEL_CTL_PUNCH:
            return 16;
            break;
        case DYNAMIXEL_CTL_FIRMWARE_VERSION:
        case DYNAMIXEL_CTL_ID:
        case DYNAMIXEL_CTL_BAUD_RATE:
        case DYNAMIXEL_CTL_RETURN_DELAY_TIME:
        case DYNAMIXEL_CTL_TEMP_LIMIT_HIGH:
        case DYNAMIXEL_CTL_VOLTAGE_LIMIT_LOW:
        case DYNAMIXEL_CTL_VOLTAGE_LIMIT_HIGH:
        case DYNAMIXEL_CTL_STATUS_RETURN_LEVEL:
        case DYNAMIXEL_CTL_ALARM_LED:
        case DYNAMIXEL_CTL_ALARM_SHUTDOWN:
        case DYNAMIXEL_CTL_TORQUE_ENABLE:
        case DYNAMIXEL_CTL_LED:
        case DYNAMIXEL_CTL_CW_COMPLIANCE_MARGIN:
        case DYNAMIXEL_CTL_CCW_COMPLIANCE_MARGIN:
        case DYNAMIXEL_CTL_CW_COMPLIANCE_SLOPE:
        case DYNAMIXEL_CTL_CCW_COMPLIANCE_SLOPE:
        case DYNAMIXEL_CTL_PRESENT_VOLTAGE:
        case DYNAMIXEL_CTL_PRESENT_TEMP:
        case DYNAMIXEL_CTL_REGISTERED_INSTRUCTION:
        case DYNAMIXEL_CTL_MOVING:
        case DYNAMIXEL_CTL_LOCK:
            return 8;
            break;
        default:
            return 0;
            break;
    }
}

int 
dynamixel_ping(int serial_fd, uint8_t unit_id, uint8_t *responder_id)
{
    InstructionPacket cmd;
    memset(&cmd, 0, sizeof(cmd));
    cmd.unit_id = unit_id;
    cmd.instruction = DYNAMIXEL_CMD_PING;
    cmd.nparams = 0;

    StatusPacket result;
    memset(&result, 0, sizeof(result));
    int comm_status = dynamixel_send_instruction(serial_fd, &cmd, &result);
    if(comm_status < 0) {
        dbg("ping failed\n");
        return DYNAMIXEL_ERR_COMM_FLAG;
    }

    if(responder_id)
        *responder_id = result.unit_id;

    return result.error;
}

static int
dynamixel_read_uint16(int serial_fd, uint8_t unit_id, uint8_t address, 
        uint16_t *result)
{
    StatusPacket response;
    int rstatus = dynamixel_read_data(serial_fd, unit_id, address, 2, &response);
    if(result && !(response.error & DYNAMIXEL_ERR_COMM_FLAG))
        *result = bytes_to_uint16_t(response.params);
    return rstatus;
}

static int
dynamixel_read_uint8(int serial_fd, uint8_t unit_id, uint8_t address, 
        uint8_t *result)
{
    StatusPacket response;
    int rstatus = dynamixel_read_data(serial_fd, unit_id, address, 1, &response);
    if(result && !(response.error & DYNAMIXEL_ERR_COMM_FLAG))
        *result = response.params[0];
    return rstatus;
}

int 
dynamixel_read(int serial_fd, uint8_t unit_id, uint8_t address, 
        uint16_t *result)
{
    switch(_get_control_nbits(address)) {
        case 16:
            return dynamixel_read_uint16(serial_fd, unit_id, address, result);
        case 8:
            {
                uint8_t val8;
                int status = dynamixel_read_uint8(serial_fd, unit_id, address, &val8);
                if(result && !status)
                    *result = val8;
                return status;
            }
        default:
            return DYNAMIXEL_ERR_INSTRUCTION_FLAG;
    }
}

static int 
dynamixel_write_uint16(int serial_fd, uint8_t unit_id, uint8_t address, uint16_t val)
{
    StatusPacket response;
    uint8_t data[2] = { val & 0xFF, val >> 8 };
    int rstatus = dynamixel_write_data(serial_fd, unit_id, address, 
            DYNAMIXEL_CMD_WRITE_DATA, 2, data, &response);
    return rstatus;
}

static int 
dynamixel_write_uint8(int serial_fd, uint8_t unit_id, uint8_t address, uint8_t val)
{
    StatusPacket response;
    return dynamixel_write_data(serial_fd, unit_id, address, DYNAMIXEL_CMD_WRITE_DATA, 1, 
            &val, &response);
}

int 
dynamixel_write(int serial_fd, uint8_t unit_id, uint8_t address, 
        uint16_t val)
{
    switch(_get_control_nbits(address)) {
        case 16:
            return dynamixel_write_uint16(serial_fd, unit_id, address, val);
        case 8:
            return dynamixel_write_uint8(serial_fd, unit_id, address, (uint8_t)val);
        default:
            return DYNAMIXEL_ERR_INSTRUCTION_FLAG;
    }
}


static int 
dynamixel_reg_write_uint16(int serial_fd, uint8_t unit_id, uint8_t address, 
        uint16_t val)
{
    StatusPacket response;
    uint8_t data[2] = { val & 0xFF, val >> 8 };
    int rstatus = dynamixel_write_data(serial_fd, unit_id, address, 
            DYNAMIXEL_CMD_REG_WRITE, 2, data, &response);
    return rstatus;
}

static int 
dynamixel_reg_write_uint8(int serial_fd, uint8_t unit_id, uint8_t address, uint8_t val)
{
    StatusPacket response;
    return dynamixel_write_data(serial_fd, unit_id, address, DYNAMIXEL_CMD_REG_WRITE, 1, 
            &val, &response);
}

int 
dynamixel_reg_write(int serial_fd, uint8_t unit_id, uint8_t address, 
        uint16_t val)
{
    switch(_get_control_nbits(address)) {
        case 16:
            return dynamixel_reg_write_uint16(serial_fd, unit_id, address, val);
        case 8:
            return dynamixel_reg_write_uint8(serial_fd, unit_id, address, (uint8_t)val);
        default:
            return DYNAMIXEL_ERR_INSTRUCTION_FLAG;
    }
}

int 
dynamixel_action(int serial_fd, uint8_t unit_id)
{
    InstructionPacket cmd;
    memset(&cmd, 0, sizeof(cmd));
    cmd.unit_id = unit_id;
    cmd.instruction = DYNAMIXEL_CMD_ACTION;
    
    StatusPacket response;
    int comm_status = dynamixel_send_instruction(serial_fd, &cmd, &response);
    if(comm_status < 0) {
        dbg("Action command failed\n");
        return DYNAMIXEL_ERR_COMM_FLAG;
    }
    return response.error;
}

int 
dynamixel_reset(int serial_fd, uint8_t unit_id)
{
    InstructionPacket cmd;
    memset(&cmd, 0, sizeof(cmd));
    cmd.unit_id = unit_id;
    cmd.instruction = DYNAMIXEL_CMD_RESET;
    
    StatusPacket response;
    int comm_status = dynamixel_send_instruction(serial_fd, &cmd, &response);
    if(comm_status < 0) {
        dbg("Reset command failed\n");
        return DYNAMIXEL_ERR_COMM_FLAG;
    }
    return response.error;
}

static int 
dynamixel_sync_write_uint16(int serial_fd, uint8_t address, int nservos, 
        const uint8_t *unit_ids, const uint16_t*params)
{
    int datalen = nservos * 3 + 1;
    uint8_t data[datalen];
    data[0] = 2;
    for(int i=0; i<nservos; i++) {
        data[1 + i*3 + 0] = unit_ids[i];
        data[1 + i*3 + 1] = params[i] & 0xFF;
        data[1 + i*3 + 2] = params[i] >> 8;
    }
    StatusPacket response;
    return dynamixel_write_data(serial_fd, DYNAMIXEL_BROADCAST_ID, address, 
            DYNAMIXEL_CMD_SYNC_WRITE, datalen, data, &response);
}

static int 
dynamixel_sync_write_uint8(int serial_fd, uint8_t address, int nservos, 
        const uint8_t *unit_ids, const uint8_t*params)
{
    int datalen = nservos * 2 + 1;
    uint8_t data[datalen];
    data[0] = 2;
    for(int i=0; i<nservos; i++) {
        data[1 + i*2 + 0] = unit_ids[i];
        data[1 + i*2 + 1] = params[i];
    }
    StatusPacket response;
    return dynamixel_write_data(serial_fd, DYNAMIXEL_BROADCAST_ID, address, 
            DYNAMIXEL_CMD_SYNC_WRITE, datalen, data, &response);
}

int 
dynamixel_sync_write(int serial_fd, uint8_t address, int nservos, 
        const uint8_t *unit_ids, const uint16_t*params)
{
    switch(_get_control_nbits(address)) {
        case 16:
            return dynamixel_sync_write_uint16(serial_fd, address, nservos,
                    unit_ids, params);
        case 8:
            {
                uint8_t params8[nservos];
                for(int i=0; i<nservos; i++)
                    params8[i] = (uint8_t) params[i];
                return dynamixel_sync_write_uint8(serial_fd, address, nservos, 
                        unit_ids, params8);
            }
            break;
        default:
            return DYNAMIXEL_ERR_INSTRUCTION_FLAG;
            break;
    }
}

static int 
_fileutils_write_fully(int fd, const void *b, int len)
{
  int cnt=0;
  int thiscnt;
  unsigned char *bb=(unsigned char*) b;

  while (cnt<len)
    {
      thiscnt=write(fd, &bb[cnt], len-cnt);
      if (thiscnt<0) {
        perror ("write");
	return -1;
      }
      cnt+=thiscnt;
    }

  return cnt;
}

/* returns -1 on error, 0 on timeout, nchar on success. */
static int 
_fileutils_read_timeout(int fd, void *buf, int maxlen, int msTimeout)
{
  struct pollfd pfd;
  int len;
  int res;
                                                                                             
  pfd.fd=fd;
  pfd.events=POLLIN;

  res=poll(&pfd, 1, msTimeout);
  if (res<0) // error
    {
      perror("poll");
      return -1;
    }
  if (res==0) // timeout
    {
      errno=ETIMEDOUT;
      return 0;
    }

  len=read(fd, buf, maxlen);
  if (len<0)
    {
      perror("read");
      return -1;
    }
  if (len == 0) {
    fprintf (stderr, "end of file\n");
    return -1;
  }
  return len;
}

/* returns -1 on error, nchar on timeout or success. */
static int 
_fileutils_read_fully_timeout(int fd, void *bufin, int len, int msTimeout)
{
  char *buf=(char*) bufin;
  int readsofar=0;
  int thisread=0;

  while (readsofar<len)
    {
      thisread=_fileutils_read_timeout(fd, &buf[readsofar], len-readsofar, msTimeout);
      if (thisread == 0)
	return readsofar;
      if (thisread < 0)
	return thisread;

      readsofar+=thisread;
    }

  return len;
}

