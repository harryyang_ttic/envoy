//servo_controller.h

#ifndef __SERVO_CONTROLLER__
#define __SERVO_CONTROLLER__

#include "serial.h"
#include "main.h"
#include <lcm/lcm.h>
#include <lcmtypes/dynamixel_status_list_t.h>
#include <lcmtypes/dynamixel_cmd_list_t.h>
#include <dynamixel/dynamixel.h>

#define NUM_OF_PARAMS 5

int sc_read_telemetry (dynamixel_status_list_t * servos_status, state_t * state); 
int sc_write_commands (const dynamixel_cmd_list_t *servos_command, state_t *state);
list_head_t * sc_open(int max_num_of_servos, int fd);
int sc_close(list_head_t * servo_list);
int sc_characterize_read(state_t *state);
struct _servo_node_t {
	int fd;
};

#endif
