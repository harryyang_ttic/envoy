#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <glib.h>
#include <sys/time.h>
#include <math.h>
#include <getopt.h>
#include <sys/select.h>
#include <inttypes.h>
#include <stdbool.h>
#include <signal.h>
#include "main.h"
#include "servo_controller.h"
#include "serial.h"
#include "list.h"
#include "gridpower.h"
#include <lcm/lcm.h>
#include <lcmtypes/dynamixel_status_list_t.h>
#include <lcmtypes/dynamixel_cmd_list_t.h>
#include <dynamixel/dynamixel.h>
#include <bot_core/bot_core.h>

//Function Prototypes
static void usage()
{
    fprintf (stderr, "usage: dynamixel [options]\n"
            "\n"
            "  -h, --help             shows this help text and exits\n"
            "  -b, --buad <s>      	  baudrate\n"
            "  -d, --devicename <s>   device to connect to\n"
			"  -t, --tcp <s>		  tcp device\n"
			"	example: dynamixel-lcm -t 192.168.123.100:10001\n"
             );
}



/*
 * command_handler
 *
 * Description: called when new LCM command data comes in
 *
 */
static void command_handler (
								const lcm_recv_buf_t * rbuf, 
								const char * channel_name, 
								const dynamixel_cmd_list_t * msg, 
								void * user){
	printf ("in %s\n", __func__);
	state_t * state = (state_t *) user;
	//check if the channel names matchu up
	if (strcmp(state->lcm_command_channel, channel_name)){
		//doesn't match up
		printf ("Unknown channel name detected\n");
		return;
	}

	if (sc_write_commands(msg, state)){
		fprintf (stderr, "Error writing servo commands\n");
		state->exit_flag = 1;
	}
		
}
int main(int argc, char *argv[])
{
	
	int exit_flag = 0;
   	state_t *state = (state_t*) calloc(1, sizeof(state_t));
   	setlinebuf (stdout);
	bool tcp_flag = false;
	int fd = -1;
	gridpower_t *gp = NULL;

	char *optstring = "hc:d:t:";
	int c;
    struct option long_opts[] = {
        {"help", 		no_argument, 		0, 'h' },
        {"baud", 		required_argument, 	0, 'b' },
        {"devicename", 	optional_argument, 	0, 'd' },
		{"tcp",			required_argument,	0, 't'},
        {0, 0, 0, 0}
    };

	int baud = 1000000;
	//int baud = 57600;
	char * url = strdup("/dev/ttyUSB0");


	//get options
	while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0){
		switch (c){
			case 'd':
				free(url);
				url = strdup(optarg);
				break;
			case 'b':
				baud = atoi(optarg);
			    if(baud != 9600 && baud != 19200 && baud != 57600 &&
                   baud != 115200 && baud != 200000 && baud != 250000 &&
                   baud != 400000 && baud != 500000 && baud != 1000000 ) {
                    fprintf(stderr, "Invalid baud rate\n");
                    return 1;
                }
                break;
			case 't':
				tcp_flag = true;
				free(url);
				
				url = strdup(optarg);
				break;
            case 'h':
            default:
                usage();
		
		}
	}

	//open the servos
	if (tcp_flag){
		printf ("looking for TCP device...\n");
		gp = gp_open_url(url);
		if (gp == NULL){
			printf ("Error: couldn't find tcp device %s\n", url);
			return 1;
		}
		fd = gp_get_fd(gp);
		printf ("found device!\n");
	}
	else {
		printf ("openning serial device...\n");
		fd = serial_open (url, baud, true);  
		if (fd == -1){
			printf ("Error: couldn't fund serial device %s\n", url);
			return 1;
		}
	}
	state->servo_list = sc_open(10, fd);
	printf ("Number of servos found: %d\n", list_get_size(state->servo_list));

	state->lcm_command_channel = strdup(COMMAND_CHANNEL);
	state->lcm_status_channel = strdup(STATUS_CHANNEL);
	
	//sensor status structure
	printf ("list size  = %d\n", list_get_size(state->servo_list) * NUM_OF_PARAMS);
	printf ("Number of parameters = %d\n", NUM_OF_PARAMS);

	//populate the servo structure
	dynamixel_status_t * servos = calloc(
						list_get_size(state->servo_list), 
						sizeof(dynamixel_status_t));

	//dynamixel_status_t servos[NUM_OF_SERVOS]; 
	printf ("populating servos\n");
 	for (int i = 0; i < list_get_size(state->servo_list); i++){
		printf ("populating %d\n", i);
		servos[i].num_values = NUM_OF_PARAMS;
		servos[i].addresses = calloc(NUM_OF_PARAMS, sizeof(short int));
		if  (servos[i].addresses == NULL){
			printf ("Failed to allocate space for the addresses!\n");
			state->exit_flag = 1;
		}

		servos[i].values = calloc(NUM_OF_PARAMS, sizeof(int));
		if (servos[i].values == NULL){
			printf("Failed to allocate space for the values!\n");
		}
	}
			
	printf ("populating sensor_status\n");
	dynamixel_status_list_t sensors_status = { 
		.utime = 0,
		.nservos = list_get_size(state->servo_list),
		.servos = servos 
	};
	

    char *lcm_provider = NULL;
	//open up the default UDP lcm provider/subscriber
	printf ("openning LCM\n");
    state->lcm = lcm_create(lcm_provider);
	state->exit_flag = 0;

	dynamixel_cmd_list_t_subscription_t * sub = 
		dynamixel_cmd_list_t_subscribe(
									state->lcm, 
									state->lcm_command_channel, 
									&command_handler, 
									state); 		
	if (sub == NULL){
		printf ("Failed to setup subscription\n");
	}

	sc_characterize_read(state);

	struct timeval timeout = {
		0,
		MS_DELAY * 1000
	};

	printf ("entering main loop\n");

    while(!exit_flag) {
		//setup the LCM file descriptor for waiting
		int lcm_fds = lcm_get_fileno(state->lcm);
		//get filedescriptor froms from static serial ports
		fd_set fds;
		FD_ZERO(&fds);
		FD_SET(lcm_fds, &fds);
		FD_SET(STDIN_FILENO, &fds);
		//wait limit amount of time for an incomming message
	 	int status = select (lcm_fds + 1, &fds, NULL, NULL, &timeout);
		
		if (status == 0){
			timeout.tv_sec	= 0;
			timeout.tv_usec = MS_DELAY * 1000;

			//this is only for characterizing the reads
			int64_t read_time = _timestamp_now();


			//timeout occured, read the status
			int read_status = sc_read_telemetry (&sensors_status, state);			
			if (read_status != 0){
				fprintf (stderr, "Error reading servos\n");
				exit_flag = 1;
			}
			//DEBUG TIMING DATA
			if (DEBUG) {
				int64_t end_time = _timestamp_now();
				double dt_ms = (end_time - read_time) * 1e-3;
//				printf("Time for read = %.03f\n", dt_ms);
			}
//			printf ("publishing...\n");

			if (dynamixel_status_list_t_publish (
					state->lcm, 
					state->lcm_status_channel, 
					&sensors_status) != 0){

				fprintf(stderr, "Error publishing to LCM\n");
				exit_flag = 1;
			}
//			printf ("published!\n");

		}
		else {
			if (DEBUG) printf ("status == %d\n", status);
			if (FD_ISSET(lcm_fds, &fds)){
				//received an lcm command
				lcm_handle(state->lcm);
				exit_flag = state->exit_flag;
			}
			if (FD_ISSET(STDIN_FILENO, &fds)){
				exit_flag = 1;
			}
		}
    }

	//free the servo status
	for (int i = 0; i < sensors_status.nservos; i++){
		free(servos[i].addresses);
		free(servos[i].values);
	}
	free(servos);

	sc_close(state->servo_list);
	if (tcp_flag){
		gp_close(gp);
	}
	dynamixel_cmd_list_t_unsubscribe(state->lcm, sub);
    lcm_destroy(state->lcm);
    free(state);

    return 0;
}

