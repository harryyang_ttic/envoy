//gridpower.c

#include <stdlib.h>
#include <string.h>
#include <bot_core/bot_core.h>
#include "gridpower.h"


struct _gridpower_t {
	char 	*ipaddr;
	int		port;

	bot_ssocket_t *data_socket;
};

gridpower_t * gp_open(const char *ipaddr, int port){
	int retval = 0;
	gridpower_t *gp = calloc(1, sizeof(gridpower_t));
	if (gp == NULL){
		return NULL;
	}

	gp->ipaddr = strdup(ipaddr);
	gp->port = port;

	gp->data_socket = bot_ssocket_create();

	retval = bot_ssocket_connect(gp->data_socket, ipaddr, gp->port);
	if (retval){
		goto error;
	}

	if (bot_ssocket_disable_nagle(gp->data_socket)){
		printf("Error couldn't set nagle!\n");
	}
	
	return gp;

error:
	gp_close(gp);
	return NULL;
}
gridpower_t * gp_open_url(const char *url){

	char *tmp = strdup(url);
	char *ipaddr = NULL;
	char *port = NULL;

	ipaddr = strtok(tmp, ":");
	port = strtok(NULL, ":");
	
	printf ("ipaddr = %s\n", ipaddr);
	printf ("port	= %s\n", port);

	gridpower_t *gp = gp_open(&ipaddr[0], atoi(&port[0]));
	free(tmp);
	return gp;
}
void gp_close(gridpower_t *gp){
	if (gp->ipaddr){
		free(gp->ipaddr);
	}
	if (gp->data_socket){
		bot_ssocket_destroy(gp->data_socket);
	}
	free(gp);
}
int	gp_get_fd(gridpower_t *gp){
	return bot_ssocket_get_fd(gp->data_socket);
}



