//gridpower.h

#ifndef __GRID_POWER__
#define __GRID_POWER__

typedef struct _gridpower_t gridpower_t;

gridpower_t * gp_open(const char *ipaddr, int port);
gridpower_t * gp_open_url(const char *url);
void gp_close(gridpower_t *gp);
int	gp_get_fd(gridpower_t *gp);


#endif
