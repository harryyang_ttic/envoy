//servo controller

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
//#include <glib.h>
#include <signal.h>
#include <sys/time.h>
#include <math.h>
#include <getopt.h>
#include <sys/select.h>
#include <inttypes.h>
#include <dirent.h>

#include "servo_controller.h"
#include "serial.h"
#include <lcm/lcm.h>
#include <lcmtypes/dynamixel_status_list_t.h>
#include <lcmtypes/dynamixel_cmd_list_t.h>
#include <dynamixel/dynamixel.h>


typedef struct _ServoParameter_t ServoParameter_t;
typedef struct _servo_node_t servo_node_t;


struct _ServoParameter_t {
	char * name;
	int address;
};

//we probably won't be using all of these, but this is good for testing
//if there is a more elegant way of find the size of this array (rather than counting) I would prefer that, so we don't have a chance of making mistakes
//NUM_OF_PARAMS <SOMETHING AWESOMELY SMART HERE>
static ServoParameter_t SERVO_STATUS_PARAMS[] = {
	{ "Goal Position",		DYNAMIXEL_CTL_GOAL_POSITION			},
	{ "Present Position",		DYNAMIXEL_CTL_PRESENT_POSITION			},
//	{ "Load", 			DYNAMIXEL_CTL_PRESENT_LOAD			},
	{ "Moving",			DYNAMIXEL_CTL_MOVING				},
	{ "Present Speed",		DYNAMIXEL_CTL_PRESENT_SPEED			},
//	{ "Torque Enabled", 		DYNAMIXEL_CTL_TORQUE_ENABLE			},
//	{ "CW margin",				DYNAMIXEL_CTL_CW_COMPLIANCE_MARGIN	},
//	{ "CCW margin",				DYNAMIXEL_CTL_CCW_COMPLIANCE_MARGIN },
//	{ "CW slope", 				DYNAMIXEL_CTL_CW_COMPLIANCE_SLOPE 	},
//	{ "CCW slope", 				DYNAMIXEL_CTL_CCW_COMPLIANCE_SLOPE	},
	{ "Moving Speed",			DYNAMIXEL_CTL_MOVING_SPEED			},
//	{ "Torque Limit", 			DYNAMIXEL_CTL_TORQUE_LIMIT			},
//	{ "Present Voltage", 		DYNAMIXEL_CTL_PRESENT_VOLTAGE		},
//	{ "Present Temp",			DYNAMIXEL_CTL_PRESENT_TEMP			},
//	{ "Lock",					DYNAMIXEL_CTL_LOCK					},
//	{ "Punch",					DYNAMIXEL_CTL_PUNCH					},
	{ NULL, 					0									},
};

/* name: read_servos_status
 *
 * description: 	reads the status of all of the servos
 *
 * parameters:
 * 	servos_status: 	populate this pointer with status data from the servos
 *	state:			hold serial filedescriptors
 * 
 * return:
 * 	0 	= success
 *	-1 	= fail
 */
int sc_read_telemetry(dynamixel_status_list_t *servos_status, 
		state_t * state){

	int status = 0;
	
	int64_t read_time = 0;
	int64_t end_time = 0;
	int num_of_servos = list_get_size(state->servo_list);
	//go through each of the sensors, and read the data
	for (int i = 0; i < num_of_servos; i++){
		for(int j = 0; SERVO_STATUS_PARAMS[j].name; j++){
			const ServoParameter_t *sparam = &SERVO_STATUS_PARAMS[j];
			unsigned short int value;
			servo_node_t * node = list_get_data_from_index(state->servo_list, i);	
			int fd = node->fd;
//			printf ("fd to read from %d\n", fd);
			servos_status->servos[i].servo_id = i;
			servos_status->servos[i].addresses[j] = sparam->address;
//			printf ("reading %s\n", SERVO_STATUS_PARAMS[j].name);
			
			read_time = _timestamp_now();
			status = dynamixel_read(fd, i,
				sparam->address, &value);
			end_time = _timestamp_now();
			//DEBUG TIMING DATA
			double dt_ms = (end_time - read_time) * 1e-3;
//			printf("Time for individual read = %.03f\n", dt_ms);

			servos_status->servos[i].values[j] = value;
			if (status != 0){
				printf ("Failed to read %s from servo number %d\n", 
					sparam->name, i);
				dynamixel_print_errors(status);
				if ((DYNAMIXEL_ERR_COMM_FLAG & status) != 0){
//					return status;
					return 0;
				}
			}
		}
	}
	servos_status->utime = _timestamp_now();

	return 0;
}


/* name: read_servos_status
 *
 * description: 	writes commands to each of the servos and executes the commands
 *
 * parameters:
 * 	servos_status: 	populate this pointer with status data from the servos
 *	state:			hold serial filedescriptors
 * 
 * return:
 * 	0 	= success
 *	-1 	= fail
 */

int sc_write_commands(const dynamixel_cmd_list_t *servos_command, state_t *state){

	int servo_id = -1;
	int num_of_servos = list_get_size(state->servo_list);

	//go through each of the sensors, and read the data
//	printf ("in %s, working on outputing commands\n", __func__);
//	printf ("Number of commands = %d\n", servos_command->ncommands);
	for (int i = 0; i < servos_command->ncommands; i++){
		if (i >= num_of_servos){
			break;
		}
//		printf ("Command index = %d\n", i);
		servo_node_t * node = list_get_data_from_index(state->servo_list, i);
		int fd = node->fd;
		servo_id = i;
		//if torque enable is 1, then write out torque value

		dynamixel_write(fd, servo_id, DYNAMIXEL_CTL_LED, servos_command->commands[i].torque_enable);
		if (servos_command->commands[i].torque_enable){
			printf ("Writing Torque\n");
			//write out torque
			dynamixel_write(fd, servo_id, DYNAMIXEL_CTL_TORQUE_ENABLE, 1);	
			uint16_t torque = servos_command->commands[i].max_torque;
			dynamixel_write(fd, servo_id, DYNAMIXEL_CTL_TORQUE_LIMIT, torque);
			//dynamixel_write(fd, servo_id, DYNAMIXEL_CTL_LED, servos_command->commands[i].torque_enable);
			printf ("Torque value %d\n", torque);
		}
		else {
			dynamixel_write(fd, servo_id, DYNAMIXEL_CTL_TORQUE_ENABLE, 0);
	//		dynamixel_write(fd, servo_id, DYNAMIXEL_CTL_LED, 0);
		}

		//write out goal position
		uint16_t goal_position = servos_command->commands[i].goal_position;
		dynamixel_reg_write(fd, servo_id, DYNAMIXEL_CTL_GOAL_POSITION, goal_position);
//		printf ("Goal Position = %d\n", goal_position);
		//write out max speed		

		uint16_t max_speed = servos_command->commands[i].max_speed;
		dynamixel_write(fd, servo_id, DYNAMIXEL_CTL_MOVING_SPEED, max_speed);
//		printf ("Speed value %d\n", max_speed);
//		//execute the command

		dynamixel_action(fd, servo_id);

	}

	return 0;
}

/* name: open_servos
 *
 * description: Opens all the servos
 *
 * parameters: 
 *	num_servos:		maximum number of servos to query for
 * 
 * return:
 * 	pointer to a list_head_t structure
 */

list_head_t * sc_open(int num_servos, int fd){
	printf ("%s\n", __func__);
	//get a list of all the tty/usb devices in the /dev directory	
	//char buffer [100];
	//int length = 0;

	list_head_t * servo_list = list_new("servo list");

	printf ("fd value = %d\n", fd);
	uint8_t responder_id;
	for (int j = 0; j < num_servos ; j++){
		printf ("sending ping...");
		int status = dynamixel_ping(fd, j, &responder_id);
		if (status != 0){
			printf ("time out\n");
		}
		else {
			printf ("response!\n");
			//put this data into the servo list	
			//if the list is too small, add empy items
			while (j >= list_get_size(servo_list)){
				printf ("servo index %d, is less than servo_count %d...", 
								j, 
								list_get_size(servo_list));
				printf ("Adding a new node to the servo_list\n");
				list_add(servo_list, "nothing", NULL);
			}
			servo_node_t * servo_node = calloc (1, sizeof(servo_node_t));
			if (servo_node == NULL){
				printf ("Error allocating space for servo_node\n");
				goto fail;
			}
			servo_node->fd = fd;
			//set the data to the servo_node that was just created
			list_set_data_by_index(servo_list, j, servo_node);
			//set the tag to the name of the device
			list_set_tag_by_index(
								servo_list, 
								"file descriptor",
								j);
		}
	}

	//destroy the file descriptor list
	return servo_list;

fail:
	//remove all items from the servo_list
	for (int i = 0; i < list_get_size(servo_list); i++){
		servo_node_t * servo_node = list_get_data_from_index(servo_list, i);
		free(servo_node);
	}
	list_destroy (servo_list);
	return NULL;
}
/* name: sc_characterize_read
 *
 * description:		characterizes the time it takes to read the servos	
 *					prints out the time it takes to read the servos in ms
 *
 * parameters:
 *	state:			hold serial filedescriptors
 * 
 * return:
 * 	0 	= success
 *	-1 	= fail
 */

int sc_characterize_read(state_t * state){
/*
	if (DEBUG) printf("%s: preparing to characterize read\n", __FUNCTION__);
	//sensor status structure
	short int addresses[NUM_OF_SERVOS][NUM_OF_PARAMS];
	int values[NUM_OF_SERVOS][NUM_OF_PARAMS]; 

	//populate the servo structure
	dynamixel_status_t servos[NUM_OF_SERVOS]; 

 	for (int i = 0; i < NUM_OF_SERVOS; i++){
		servos[i].num_values = NUM_OF_PARAMS;
		servos[i].addresses = addresses[i];
		servos[i].values = values[i];
	}
			
	dynamixel_status_list_t sensors_status = { 
		.utime = 0,
		.nservos = NUM_OF_SERVOS,
		.servos = servos 
	};

	int64_t start_time = _timestamp_now();
	if (DEBUG) printf("Preparing to read servo status\n");
	int read_status = read_servos_status(&sensors_status, state);
	if (read_status == 0){
 		int64_t end_time = _timestamp_now();
		double dt_ms = (end_time - start_time) * 1e-3;
		printf ("Time for status read = %.03f\n", dt_ms);
	}
	
*/
	return 0;
}

/* name: close_servos
 *
 * description: Closes all the servos
 *
 * parameters: Driver structure
 * 
 * return:
 * 	0 	= success
 *	-1 	= fail
 */
/*
int close_servos (state_t * state){
	if (DEBUG) printf("%s: preparing to close all servos\n", __FUNCTION__);
	//close all files
	for (int i = 0; i < NUM_OF_SERVOS; i++){
		for (int j = 0; j < i; j++){
			//check to see if we already closed the device
			if (strcmp(SERVO_MAP[j].device_name, SERVO_MAP[i].device_name) == 0){
				state->serial_fds[j] = state->serial_fds[i];
			}
		}
		if (state->serial_fds[i] > 0){
		//if (SERVO_MAP[i].fd > 0){
			serial_close(state->serial_fds[i]);
			state->serial_fds[i] = -1;
			state->servo_ids[i] = -1;
			//SERVO_MAP[i].fd = -1;
		}
	}
	return 0;	
}
*/
int sc_close (list_head_t * servo_list){
	printf ("in %s\n", __func__);
	//go through each node, and generate a unique file descriptor list
	list_head_t * fd_list = list_new("fd_list");

	for (int i = 0 ; i < list_get_size(servo_list); i++){
		//get the file descriptor from the list
		servo_node_t *servo_node = (servo_node_t *) list_get_data_from_index(
														servo_list, 
														i);
	
		int fd = servo_node->fd;
		printf ("found a file descriptor: %d\n", fd);
		if (list_get_size(fd_list) == 0){
			printf("found a unique file descriptor: %d\n", fd);
			list_add(fd_list, list_get_tag_from_index(servo_list, i), (void *)fd);
		}
		else {
			//compare it to all the file descriptors in the fd_list
			for (int j = 0; j < list_get_size(fd_list); j++){
				printf ("comparing previous file descriptors...");
				int fd_test = (int)list_get_data_from_index(fd_list, j);	
				if (fd == fd_test){
					printf ("found a duplicate file descriptor\n");
					//there is already an fd in here, break out of this inner loop
					break;
				}
				//if there is no other instances of that file descriptor put it in
				list_add(fd_list, list_get_tag_from_index(servo_list, i), (void *) fd);
				printf ("found a unique file descriptor %d\n", fd);
			}
		}
		//now there is an independent list of file descriptors
	}

	printf ("freeing all servo nodes\n");
	//remove, and free all items from the servo list
	for (int i = 0; i < list_get_size(servo_list); i++){
		servo_node_t * servo_node = list_get_data_from_index(servo_list, i);
		free(servo_node);
	}
	//close all the serial devices in the serial list
	printf ("freeing file descriptor list\n");
	printf ("fd_list size = %d\n", list_get_size(fd_list));
	for (int i = 0; i < list_get_size(fd_list) ; i++){
		printf ("freeing file descriptor %d\n", (int)list_get_data_from_index(fd_list, i));
		serial_close((int)list_get_data_from_index(fd_list, i));
	}
	list_destroy (fd_list);
	list_destroy (servo_list);

	return 0;
}
/*
int get_phy_address_from_index(int index, int *fd, int *servo_id){
	if (index >= NUM_OF_SERVOS){
		printf("Error attempting to access an index out of range");
		return -1;
	}
	*fd = SERVO_MAP[index].fd;
	*servo_id = SERVO_MAP[index].servo_id;
	return 0;
}
*/
