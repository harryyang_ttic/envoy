//main.h

#ifndef __MAIN_H__
#define __MAIN_H__

#include <lcm/lcm.h>
#include <sys/time.h>
#include <lcmtypes/dynamixel_cmd_list_t.h>
#include "list.h"

#define STATUS_CHANNEL "DYNAMIXEL_STATUS"
#define COMMAND_CHANNEL "DYNAMIXEL_COMMAND"
#define DEBUG 1
#define MS_DELAY 10

static int64_t _timestamp_now()
{
    struct timeval tv;
    gettimeofday (&tv, NULL);
    return (int64_t) tv.tv_sec * 1000000 + tv.tv_usec;
}


typedef struct _state_t state_t;
/*
typedef struct _ServoMap {
	const char *device_name;
	int baud;	
//	int fd;
//	uint8_t servo_id;

} ServoMap;

//I don't like the idea of this being a global variable,
//but I do like the idea that this one static array sets the servo count
static ServoMap SERVO_MAP[] = { 
	{"/dev/ttyUSB0",  1000000},
}; 
*/

//#define NUM_OF_SERVOS 1

struct _state_t {
    lcm_t *lcm;
    char *lcm_command_channel;
	char *lcm_status_channel;
	list_head_t * servo_list;
	//int serial_fds[NUM_OF_SERVOS];
	//int servo_ids[NUM_OF_SERVOS];
	int exit_flag;

	
//	dynamixel_cmd_list_t * commands;
};

#endif
