//sensor_conditioner.c
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "sensor_conditioner.h"
#include <dynamixel/dynamixel.h>

static double speed_conditioner (double value){
	return value;
}
static double torque_conditioner (double value){
	return value;
}
static double load_conditioner (double value){
	int load_val = (int) value;
	if (load_val > 1024){
		load_val -= 1024;
		load_val *= -1;
	}
	//bool signed_flag = (value && 1024);
	//load_val &= ~1024;
	//if (signed_flag){
	//	load_val *= -1;
	//}
	value = load_val;
	return value;
}
static double voltage_conditioner (double value){
	return value;
}
static double temperature_conditioner (double value){
	return value;
}
static double position_conditioner (double value){
	return value;
}

static sensor_data_t sensor_datas[] = {
	{ DYNAMIXEL_CTL_TORQUE_ENABLE, 			"Torque Enable", 		"", 	NULL},
	{ DYNAMIXEL_CTL_LED, 					"LED", 					"", 	NULL},
    { DYNAMIXEL_CTL_CW_COMPLIANCE_MARGIN, 	"L Pos Margin", 		"", 	NULL},
	{ DYNAMIXEL_CTL_CCW_COMPLIANCE_MARGIN,	"H Pos Margin",			"",		NULL},
    { DYNAMIXEL_CTL_CW_COMPLIANCE_SLOPE, 	"Compliance Slop", 		"", 	NULL},
	{ DYNAMIXEL_CTL_GOAL_POSITION, 			"Goal Position", 		"", 	position_conditioner},
    { DYNAMIXEL_CTL_MOVING_SPEED, 			"Moving Speed", 		"??", 	speed_conditioner},
    { DYNAMIXEL_CTL_TORQUE_LIMIT, 			"Torque Limit", 		"??",	torque_conditioner},
    { DYNAMIXEL_CTL_PRESENT_POSITION, 		"Position", 			"", 	position_conditioner},
	{ DYNAMIXEL_CTL_PRESENT_SPEED, 			"Speed", 				"??", 	speed_conditioner},
    { DYNAMIXEL_CTL_PRESENT_LOAD, 			"Load", 				"??", 	load_conditioner},
    { DYNAMIXEL_CTL_PRESENT_VOLTAGE, 		"Volts", 				"V", 	voltage_conditioner},
	{ DYNAMIXEL_CTL_PRESENT_TEMP, 			"Temperature", 			"C", 	temperature_conditioner},
	{ DYNAMIXEL_CTL_MOVING, 				"Moving (T/F)"			"",		NULL},
	{ DYNAMIXEL_CTL_LOCK,					"Lock (T/F)"			"",		NULL},
	{ DYNAMIXEL_CTL_PUNCH,					"Punch"					"", 	NULL},

	{ -1, "Unknown", "", NULL},
};
 
/* name: get_name_from_sensor_data
 *
 * description: returns the name from the sensor_data structure
 *
 * parameters:
 *	sensor_data: sensor_data structure
 *	
 *
 * return:
 *	dupllicated version of the sensor name
 *
*/

char * get_name_from_sensor_data(sensor_data_t * sensor_data){
	return strdup (sensor_data->name);
}
 
/* name: get_units_from_sensor_data
 *
 * description: returns the name of the units for the sensor data
 *
 * parameters:
 *	sensor_data: sensor_data structure
 *	
 *
 * return:
 *	dupllicated version of the unit name
 *
*/
char * get_units_from_sensor_data(sensor_data_t * sensor_data){
	if (sensor_data->unit == NULL){
		return strdup (" ");
	}
	return strdup (sensor_data->unit);
}
 
/* name: condition_signal
 *
 * description: returns the value that has been condition based on dynamixel
 * 	specification
 *	-it is safe to call this function even on a true/false falue
 *
 * parameters:
 *	sensor_data: sensor_data structure
 *	value: value read from the sensor
 *	
 *
 * return:
 *	the conditioned signal
 *
*/
double condition_signal(sensor_data_t * sensor_data, double value){
	if (sensor_data->conditioner_function == NULL){
		return value;
	}

	return sensor_data->conditioner_function(value);
}
 
/* name: get_sensor_data_from_address
 *
 * description: get the sensor data associated with the address
 *
 * parameters:
 *	address: address of the sensor data
 *	
 *
 * return:
 *	pointer to a sensor data structure, if the value is not recognize an "unknown"
 *	will 
 *
*/
sensor_data_t * get_sensor_data_from_address(int address){
	int i = 0;

	while (sensor_datas[i].address != -1){
		if (sensor_datas[i].address == address){
			break;
		}
		i++;
	}

	return &sensor_datas[i];
}

