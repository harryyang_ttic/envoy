#include "lcm_servo_controller.h"
#include <glib.h>
#include <stdio.h>
#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif
#include <lcm/lcm.h>
#include <lcmtypes/dynamixel_status_list_t.h>
#include <lcmtypes/dynamixel_cmd_list_t.h>
#include <dynamixel/dynamixel.h>
#include "main.h"

gboolean processing_lcm_data = FALSE;

void lcm_servo_status_handler(	const lcm_recv_buf_t *rbuf,
										const char * channel,
										const dynamixel_status_list_t *msg,
										void *user){
	handle_lcm_status_data(user,  msg);
}
static gboolean
on_lcm(GIOChannel *source, GIOCondition cond, void *user_data){
	lcm_state_t * state = (lcm_state_t *) user_data;
    if(0 != lcm_handle(state->lcm)){
		printf ("Error from LCM\n");
	}
    return TRUE;
}

lcm_state_t * lcm_servo_initialize (){

	printf ("Entered function %s\n", __func__);

	printf ("Allocating space for state_lcm_t...");	
	lcm_state_t *state = g_slice_new(lcm_state_t);
	if (state == NULL){
		printf ("Falied!\n");
		return NULL;
	}
	printf("Success!\n");
		
	//default to UDP
	char *lcm_provider = NULL;
	
	printf ("Starting LCM...");
	state->lcm = lcm_create(lcm_provider);

	if (state->lcm == NULL){
		printf("Failed!\n");
		return NULL;
	}

	//add a reference to the lcm channel for GTK
	GIOChannel * ioc = g_io_channel_unix_new (lcm_get_fileno (state->lcm));
	g_io_add_watch (ioc, G_IO_IN, (GIOFunc)on_lcm, state);
	printf ("Success!\n");
	return state;

}
int lcm_servo_destroy(lcm_state_t * state){
//void dynamixel_cmd_list_t_destroy(dynamixel_cmd_list_t *p);
	printf ("Entered function %s\n", __func__);
	lcm_servo_status_unsubscribe(state);
	lcm_destroy (state->lcm);
	
	return 0;
}
int lcm_servo_publish_command_structure(lcm_state_t * state, const char * name){
	int retval = 0;	
	
	//set all the values of the publish to 
	retval = dynamixel_cmd_list_t_publish(
							state->lcm,
							name,
							state->command_list);
	if (retval != 0){
		fprintf (stderr, "%s, Error: publishing to LCM", __func__);
	}
	return 0;
}
int lcm_servo_generate_command_structure (lcm_state_t * state, int num_of_servos){

	printf ("Allocating space for the command list...");
	state->command_list = calloc (1, sizeof(dynamixel_cmd_list_t));
	if (state->command_list == NULL){
		printf ("Failed!\n");
		return -1;
	}

	printf ("Success!\n");
	printf ("Allocating space for the commands...");
	dynamixel_cmd_t *commands = calloc(num_of_servos, sizeof(dynamixel_cmd_t));
	if (commands == NULL){
		printf ("Failed!\n");
		return -1;
	}
	printf ("Success!\n");
	for (int i = 0; i < num_of_servos; i++){
		commands[i].servo_id = i;
	}
	state->command_list->utime = 0;
	state->command_list->commands = commands;
	state->command_list->ncommands = num_of_servos;

	return 0;
}
int lcm_servo_destroy_command_structure (lcm_state_t *state){
	printf ("in %s\n", __func__);
	
	dynamixel_cmd_list_t * command_list = state->command_list;

	//free each of the commands
	free(command_list->commands);
	//free the controller
	free(command_list);
	state->command_list = NULL;
	return 0;
}

int lcm_servo_status_subscribe(lcm_state_t *state, char * channel_name, void * data){
	state->status_channel = channel_name;
	printf ("in %s\n", __func__);
	
	printf ("Subscribe to status...");
	//subscribe to the status channel
	state->status_subscription = 
		dynamixel_status_list_t_subscribe(
			state->lcm,
			state->status_channel,
			&lcm_servo_status_handler,
			data);
	if (state->status_subscription == NULL){
		printf("Failed!\n");
		return -1;
	}
	printf ("Success!\n");
	return 0;
}

int lcm_servo_status_unsubscribe(lcm_state_t *state){
	printf ("in %s\n", __func__);
	if (state->status_subscription != NULL){
		dynamixel_status_list_t_unsubscribe (
			state->lcm,
			state->status_subscription);
	}
	return 0;
}
