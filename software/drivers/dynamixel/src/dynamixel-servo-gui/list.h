//list.h

#ifndef __CSPND_LIST
#define __CSPND_LIST

#include <stdint.h>
#include <stdbool.h>


typedef struct _list_head_t list_head_t;
typedef struct _list_node_t list_node_t;

//list of pointers
struct _list_node_t
{
    list_node_t *prev;
    list_node_t *next;

    char * tag;
    void * data;
};

struct _list_head_t
{
    int count;
    list_node_t *start;
    list_node_t *end;
    char *name;
};



//initialize an empty list with the given name
list_head_t * initialize_list (char *name);
//add a node to the list with a tag (string), and data (void pointer)
bool list_add (list_head_t *lh, char * tag, void * data);
//remove a node in the list with a given index
bool list_remove (list_head_t *lh, unsigned int index);
//destroy an entire list
void destroy_list (list_head_t *lh);
//search the list for a tag, if found return the index, otherwise return -1
int get_index_from_tag (list_head_t *lh, char *tag);
//get the data from the list from an index, return NULL if error
void * get_data_from_index (list_head_t *lh, unsigned int index);
//get the data from the list from a tag, return NULL if error
void * get_data_from_tag (list_head_t *lh, char *tag);
//get index from comparing the data pointers
int get_index_from_data (list_head_t *lh, void * data);
//get the list size
int get_list_size(list_head_t *lh);

//set tag
bool set_tag_by_tag (list_head_t *lh, char *tagIn, char *tagSearch);
bool set_tag_by_index (list_head_t *lh, char *tag, unsigned int index);

//get tag
char * get_tag_from_index (list_head_t *lh, unsigned int index);

//set data
bool set_data_by_tag (list_head_t *lh, char *tag, void * data);
bool set_data_by_index (list_head_t *lh, unsigned int index, void * data);

#endif
