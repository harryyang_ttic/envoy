//config_parser.c

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "config_parser.h"
#include "list.h"
#include <glib.h>

/* name: parse_config_file
 *
 * description: parses the configuration data from a <file>.conf
 *
 * parameters:
 *	file_name : name of the file to parse
 *	
 *
 * return:
 *	reference to a configuration list
 *  NULL	= 	error
 *
*/
list_head_t * parse_config_file(char * file_name){
	printf ("in %s\n", __func__);
	list_head_t * config_list = initialize_list("configuration list");
	char buffer[LINE_SIZE];
	int retval = 0;

	int	servo_index = 0;
	char parameter[LINE_SIZE];

	FILE *fp = NULL;
	//open the file to read
	fp = fopen(file_name, "r");
	printf ("Openning configuration file...");
	if (fp == NULL){
		printf ("\nfile : %s was not found\n", file_name);
		return NULL;
	}
	printf ("Success!\n");

	while (fgets (buffer, LINE_SIZE, fp) != NULL){
		//we got a line
		//printf ("found a line\n");
		if (strchr(buffer, (char)'#')){
			//comment
			printf ("comment\n");
			continue;
		}
		if (strlen(&buffer[0]) < 2){
			//blank line
			continue;
		}
		retval = sscanf (&buffer[0], "%d %s", &servo_index, &parameter[0]); 
		if (retval < 2){
			printf ("Received less than 2 values foudn in the parse line\n");
			continue;
		}

		//put this value into the list
		//grow the list to the needed size
		while (get_list_size(config_list) < servo_index + 1){
			//adding a new servo list
			printf ("adding a new servo config list\n");
			list_head_t * servo_config_list = initialize_list("servo config list");
			if (servo_config_list == NULL){
				printf ("Error generating servo_config_list\n");
				return NULL;
			}
			if (!list_add(config_list, "servo config list", servo_config_list)){
				printf ("Failed to add to list\n");
			}
		}

		//get a point to the current servo list we need
		list_head_t * servo_config_list = get_data_from_index(
											config_list, 
											servo_index);
		if (servo_config_list == NULL){
			printf ("Error retrieving servo_config_list!\n");
			return NULL;
		}

		if (strcmp(parameter, NAME_STRING) == 0){
			printf ("Found servo %d name: ", servo_index);
			char name[20];
			retval = sscanf (
					&buffer[0], 
					"%d %s %s", 
					&servo_index, 
					&parameter[0], 
					&name[0]);
			if (retval < 3){
				printf ("didn't find all the parameters\n");
				continue;
			}
			printf ("%s\n", &name[0]);
			char * name_p = strdup(&name[0]);
			if (!list_add(servo_config_list, &parameter[0], name_p)){
				printf ("Failed to add to list\n");
			}
			
		}
		else if (strcmp(parameter, COUPLED_ENABLE_STRING) == 0){
			printf ("Found servo %d coupled enable = ", servo_index);
			char coupled_enabled[10];
			retval = sscanf (
				&buffer[0],
				"%d %s %s",
				&servo_index,
				&parameter[0],
				&coupled_enabled[0]);
			if (retval < 3){
				printf ("didn't find all the parameters\n");
				continue;
			}

			bool * enabled = calloc (1, sizeof(bool));
			*enabled = false;
			if (strcmp(&coupled_enabled[0], "true") == 0){
				*enabled = true;
				printf ("true\n");
			}
			else {
				printf ("false\n");
			}
			list_add(servo_config_list, &parameter[0], enabled);
		}
		else if (strcmp(parameter, COUPLED_WITH_STRING) == 0){
			printf ("Found servo %d is coupled with value: ", servo_index);
			int coupled_servo;
			retval = sscanf (
				&buffer[0],
				"%d %s %d",
				&servo_index,
				&parameter[0],
				&coupled_servo);
			if (retval < 3){
				printf ("didn't find all the parameters\n");
				continue;
			}

			int * coupled_servo_p = calloc (1, sizeof(int));
			*coupled_servo_p = coupled_servo;

			printf("%d\n", *coupled_servo_p);
			list_add(servo_config_list, &parameter[0], coupled_servo_p);
		}
		else if (strcmp(parameter, COUPLED_INVERT_STRING) == 0){
			printf ("Found servo %d is coupled inverted: ", servo_index);
			char invert[10];
			retval = sscanf (
				&buffer[0],
				"%d %s %s",
				&servo_index,
				&parameter[0],
				&invert[0]);
			if (retval < 3){
				printf ("didn't find all the parameters\n");
				continue;
			}

			bool * invert_p = calloc (1, sizeof(bool));
			if (invert_p == NULL){
				printf ("Error allocating space for invert_p\n");
			}
			*invert_p = false;
			if (strcmp(&invert[0], "true") == 0){
				*invert_p = true;
				printf ("true\n");
			}
			else{
				printf ("false\n");
			}
			list_add(servo_config_list, &parameter[0], invert_p);
			
		}
		else if (strcmp(parameter, MODEL_NAME_STRING) == 0){
			printf ("Found servo %d model name: ", servo_index);
			char model_name[LINE_SIZE];
			retval = sscanf (
				&buffer[0],
				"%d %s %s",
				&servo_index,
				&parameter[0],
				&model_name[0]);
			if (retval < 3){
				printf ("didn't find all the parameters\n");
				continue;
			}
			char *name = strdup(&model_name[0]);
			printf ("%s\n", name);
			list_add(servo_config_list, &parameter[0], name);
		}
		else if (strcmp(parameter, PARAMS_POSITION_STRING) == 0){
			printf ("Found servo %d position: ", servo_index);
			int min;
			int med;
			int max;

			retval = sscanf (
				&buffer[0],
				"%d %s %d %d %d",
				&servo_index,
				&parameter[0],
				&min,
				&med,
				&max);

			if (retval < 5){
				printf ("didn't find all the parameters\n");
				continue;
			}
			range_parameter_t * range = calloc(1, sizeof(range_parameter_t));
			range->min = min;
			range->med = med;
			range->max = max;

			printf ("%d - %d - %d\n", min, med, max);
			list_add(servo_config_list, &parameter[0], range);
		}
		else if (strcmp(parameter, PARAMS_SPEED_STRING) == 0){
			printf ("Found servo %d speed values: ", servo_index);
			int min;
			int med;
			int max;

			retval = sscanf (
				&buffer[0],
				"%d %s %d %d %d",
				&servo_index,
				&parameter[0],
				&min,
				&med,
				&max);
			if (retval < 5){
				printf ("didn't find all the parameters\n");
				continue;
			}

			range_parameter_t * range = calloc(1, sizeof(range_parameter_t));
			range->min = min;
			range->med = med;
			range->max = max;

			printf ("%d - %d - %d\n", min, med, max);
			list_add(servo_config_list, &parameter[0], range);

		}
		else if (strcmp(parameter, PARAMS_TORQUE_STRING) == 0){
			printf ("Found servo %d torque values: ", servo_index);
			int min;
			int med;
			int max;

			retval = sscanf (
				&buffer[0],
				"%d %s %d %d %d",
				&servo_index,
				&parameter[0],
				&min,
				&med,
				&max);
			if (retval < 5){
				printf ("didn't find all the parameters\n");
				continue;
			}

			range_parameter_t * range = calloc(1, sizeof(range_parameter_t));
			range->min = min;
			range->med = med;
			range->max = max;

			printf ("%d - %d - %d\n", min, med, max);
			list_add(servo_config_list, &parameter[0], range);

		}
		else {
			printf ("Unrecognized parse %d\n", servo_index);
		}

	}	
	//close file
	fclose(fp);

	return config_list;
}
/* name: destroy_config_parser
 *
 * description: destroy config file
 *
 * parameters:
 *	config_list : pointer to the config_structure
 *	
 *
 * return:
 *	
 *
*/
void destroy_config_list(list_head_t * config_list){
	printf ("in %s\n", __func__);
	while (get_list_size(config_list) > 0){
//		printf("found servo_list\n");
		list_head_t * servo_list = get_data_from_index(config_list, 0);
		
		while (get_list_size(servo_list) > 0){
//			printf ("found a parameter inside servo config list\n");
			void * param = get_data_from_index(servo_list, 0);
			free(param);
			list_remove(servo_list, 0);
		}
		list_remove(config_list, 0);
		destroy_list(servo_list);
	}
	destroy_list(config_list);
}
char * get_servo_name(list_head_t * config_list, int servo_index){
	printf ("in %s\n", __func__);
	//get the name of the servo
	list_head_t * servo_config_list = get_data_from_index(config_list, servo_index);
	return (char *) get_data_from_tag(servo_config_list, NAME_STRING); 
}
bool is_servo_coupled(list_head_t * config_list, int servo_index){
	printf ("in %s\n", __func__);
	list_head_t * servo_config_list = get_data_from_index(config_list, servo_index);
	bool * coupled = get_data_from_tag(servo_config_list, COUPLED_ENABLE_STRING);
	return *coupled;
}
int get_servo_couple(list_head_t * config_list, int servo_index){
	printf ("in %s\n", __func__);
	list_head_t * servo_config_list = get_data_from_index(config_list, servo_index);
	int * value = get_data_from_tag(servo_config_list, COUPLED_WITH_STRING);
	return *value;
}
bool is_servo_couple_invert(list_head_t * config_list, int servo_index){
	printf ("in %s\n", __func__);
	list_head_t * servo_config_list = get_data_from_index(config_list, servo_index);
	bool * invert = get_data_from_tag(servo_config_list, COUPLED_INVERT_STRING);
	return *invert;
}
char * get_servo_model_name(list_head_t * config_list, int servo_index){
	printf ("in %s\n", __func__);
	list_head_t * servo_config_list = get_data_from_index(config_list, servo_index);
	return (char *) get_data_from_tag(servo_config_list, MODEL_NAME_STRING);
}
int get_servo_max_position(list_head_t * config_list, int servo_index){
	printf ("in %s\n", __func__);
	list_head_t * servo_config_list = get_data_from_index(config_list, servo_index);
	range_parameter_t *range = get_data_from_tag(servo_config_list, PARAMS_POSITION_STRING);
	return range->max;
}
int get_servo_min_position(list_head_t * config_list, int servo_index){
	printf ("in %s\n", __func__);
	list_head_t * servo_config_list = get_data_from_index(config_list, servo_index);
	range_parameter_t *range = get_data_from_tag(servo_config_list, PARAMS_POSITION_STRING);
	return range->min;

}
int set_servo_mid_position(list_head_t * config_list, int servo_index, int mid_position){
	list_head_t * servo_config_list = get_data_from_index(config_list, servo_index);
	range_parameter_t * range = get_data_from_tag(servo_config_list, PARAMS_POSITION_STRING);
	range->med = mid_position;
	return 0;
}
int get_servo_mid_position(list_head_t * config_list, int servo_index){
	list_head_t * servo_config_list = get_data_from_index(config_list, servo_index);
	range_parameter_t *range = get_data_from_tag(servo_config_list, PARAMS_POSITION_STRING);
	return range->med;

}
int get_servo_max_speed(list_head_t * config_list, int servo_index){
	printf ("in %s\n", __func__);
	list_head_t * servo_config_list = get_data_from_index(config_list, servo_index);
	range_parameter_t *range = get_data_from_tag(servo_config_list, PARAMS_SPEED_STRING);
	return range->max;

}
int get_servo_min_speed(list_head_t * config_list, int servo_index){
	printf ("in %s\n", __func__);
	list_head_t * servo_config_list = get_data_from_index(config_list, servo_index);
	range_parameter_t *range = get_data_from_tag(servo_config_list, PARAMS_SPEED_STRING);
	return range->min;

}
int get_servo_mid_speed(list_head_t * config_list, int servo_index){
	printf ("in %s\n", __func__);
	list_head_t * servo_config_list = get_data_from_index(config_list, servo_index);
	range_parameter_t *range = get_data_from_tag(servo_config_list, PARAMS_SPEED_STRING);
	return range->med;

}
int get_servo_max_torque(list_head_t * config_list, int servo_index){
	printf ("in %s\n", __func__);
	list_head_t * servo_config_list = get_data_from_index(config_list, servo_index);
	range_parameter_t *range = get_data_from_tag(servo_config_list, PARAMS_TORQUE_STRING);
	return range->max;

}
int get_servo_min_torque(list_head_t * config_list, int servo_index){
	printf ("in %s\n", __func__);
	list_head_t * servo_config_list = get_data_from_index(config_list, servo_index);
	range_parameter_t *range = get_data_from_tag(servo_config_list, PARAMS_TORQUE_STRING);
	return range->min;

}
int get_servo_mid_torque(list_head_t * config_list, int servo_index){
	printf ("in %s\n", __func__);
	list_head_t * servo_config_list = get_data_from_index(config_list, servo_index);
	range_parameter_t *range = get_data_from_tag(servo_config_list, PARAMS_TORQUE_STRING);
	return range->med;

}
