//callbacks.h

#ifndef __CALLBACKS_H__
#define __CALLBACKS_H__

#include <gtk/gtk.h>
#include <glib.h>



G_MODULE_EXPORT void cb_hscale_value_changed (
												GtkRange * range,
												gpointer data);
G_MODULE_EXPORT void cb_torque_enabled_changed (
												GtkToggleButton * toggle_button,
												gpointer data);
G_MODULE_EXPORT void cb_listen_mode_toggled (
												GtkToggleToolButton * toggle_button,
												gpointer data);

#endif //__CALLBACKS_H__
