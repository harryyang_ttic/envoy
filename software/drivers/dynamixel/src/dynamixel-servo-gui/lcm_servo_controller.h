//lcm_servo_controller.h

#ifndef __LCM_SERVO_CONTROLLER_H__
#define __LCM_SERVO_CONTROLLER_H__

#include <lcm/lcm.h>
#include <lcmtypes/dynamixel_status_list_t.h>
#include <lcmtypes/dynamixel_cmd_list_t.h>


typedef struct _lcm_state_t lcm_state_t;

lcm_state_t * lcm_servo_initialize ();
int lcm_servo_generate_command_structure(lcm_state_t * state, int num_of_servos);
int lcm_servo_destroy_command_structure(lcm_state_t * state);
int lcm_servo_publish_command_structure(lcm_state_t * state, const char * channel_name);
int lcm_servo_destroy(lcm_state_t *state);
int lcm_servo_status_subscribe(lcm_state_t *state, char *channel_name, void * data);
int lcm_servo_status_unsubscribe(lcm_state_t *state);

struct _lcm_state_t {
	lcm_t *lcm;	
	dynamixel_status_list_t_subscription_t 	*status_subscription;
	dynamixel_cmd_list_t 					*command_list;
	char 									*status_channel;
};

#endif //__LCM_SERVO_CONTROLLER_H__
