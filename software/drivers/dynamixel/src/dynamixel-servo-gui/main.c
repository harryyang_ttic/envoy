
#include "gui_controller.h"
//#include "config_parser.h"
#include "main.h"
#include <stdio.h>
#include "lcm_servo_controller.h"
#include <dynamixel/dynamixel.h>

#ifndef BUILD_PATH
#error "Missing BUILD_PATH compiler flag"
#endif

//#define UI_FILE BUILD_PATH "/share/dynamixel-servo-gui/dynamixel-servo-gui.glade"


int initialize_controller (main_controller_t * main_controller);
int publish_commands(main_controller_t * controller);
int initialize_gui(gui_data_t * gui_data, dynamixel_status_list_t * status_list, list_head_t *config_list);



//callback for the main timer
gboolean main_timer_handler (gpointer data){

	main_controller_t * controller = (main_controller_t *) data;
	update_view(controller->gui_data);
	publish_commands(controller);

	return TRUE;
}

//main entry point
int main (int argc, char **argv){


	main_controller_t * controller = NULL; 

	controller = g_slice_new (main_controller_t);
	controller->gui_data = gui_initialize();

//	controller->config_list = parse_config_file(WHEELCHAIR_ARM_CONF_FILE);

	if (controller->gui_data == NULL){
		printf ("Error initializing GUI");
	}

	//initialize all the main controller comonents
	initialize_controller (controller);

	//lets get some LCM in here!
	controller->lcm_state =	lcm_servo_initialize();
	lcm_servo_status_subscribe(controller->lcm_state, STATUS_CHANNEL_NAME, controller);

/*
//FOR DEBUG ADD SERVOS
	add_servo(controller->gui_data, 0, 0, 0, 100);
	add_servo(controller->gui_data, 1, 300, 2, 10);
	add_servo(controller->gui_data, 2, 433, 12, 187);
	add_servo(controller->gui_data, 3, 482, 309, 223);
	add_servo(controller->gui_data, 4, 718, 388, 232);
	add_servo(controller->gui_data, 5, 684, 500, 603);
	add_servo(controller->gui_data, 6, 409, 408, 285);
//FOR DEBUG ADD SENSORS	
	add_sensor(controller->gui_data, -1);
	add_sensor(controller->gui_data, DYNAMIXEL_CTL_GOAL_POSITION);
	add_sensor(controller->gui_data, DYNAMIXEL_CTL_TORQUE_LIMIT);
	add_sensor(controller->gui_data, DYNAMIXEL_CTL_PRESENT_TEMP);
	add_sensor(controller->gui_data, DYNAMIXEL_CTL_PRESENT_LOAD);
	add_sensor(controller->gui_data, DYNAMIXEL_CTL_MOVING);
	add_sensor(controller->gui_data, DYNAMIXEL_CTL_PRESENT_LOAD);
*/
	lcm_servo_generate_command_structure(
		controller->lcm_state, 
		get_servo_count(controller->gui_data));	

	//setup the main timer interrupt
	g_timeout_add(DEFAULT_TIMEOUT_VALUE_MS, main_timer_handler, controller);

	//start the main loop
	gui_start(controller->gui_data);

	//lcm out
	lcm_servo_destroy(controller->lcm_state);

	//remove all the gui references
	gui_destroy(controller->gui_data);

	//free the configuration file data
//	destroy_config_list(controller->config_list);

	//free up allocated stuff
	g_slice_free (main_controller_t, controller);

	return 0;
}

int initialize_controller (main_controller_t * controller){
	//Set up the information model
	controller->data_available_timer 	= NULL;
	controller->read_first_status 		= FALSE; 
	
	controller->listen_mode 			= FALSE;
	controller->timeout_value 			= DEFAULT_TIMEOUT_VALUE_MS;
	return 0;
}

int publish_commands (main_controller_t *controller){

	gui_data_t * gui_data = controller->gui_data;
	int num_of_servos = get_servo_count(controller->gui_data);
	if (num_of_servos == 0){
		return -1;
	}
	dynamixel_cmd_list_t * command_list = controller->lcm_state->command_list;
	dynamixel_cmd_t * commands = command_list->commands;
	//populate the command structure

	//set the number of servos
	command_list->ncommands = num_of_servos;
	command_list->utime = _timestamp_now();

	//get all the command goal positions
	for (int i = 0; i < num_of_servos; i++){
		commands[i].servo_id = i;
		commands[i].torque_enable = get_torque_enable(gui_data, i); 
		commands[i].goal_position = get_goal_position(gui_data, i);
		commands[i].max_speed = get_max_speed(gui_data, i);
		commands[i].max_torque = get_max_torque(gui_data, i);
	}


	//pulish to LCM
	lcm_servo_publish_command_structure(
							controller->lcm_state, 
							COMMAND_CHANNEL_NAME);
	return 0;
}

void handle_lcm_status_data(void * data, const dynamixel_status_list_t * status_list){
	main_controller_t * controller = (main_controller_t *) data;
	gui_data_t * gui_data = controller->gui_data;
	//printf ("in %s\n", __func__);
	//reset the lcm watchdog
	if (controller->data_available_timer == NULL){
		controller->data_available_timer = g_timer_new();
	}
	g_timer_start(controller->data_available_timer);

	//check if there has been a change in the servo or sensor count
	if (check_for_change(controller, status_list)){
		printf ("Change in the servo, or sensor count reseting all servo displays\n");

		remove_all_servos(gui_data);

		for (int i = 0; i < status_list->nservos; i++){
			int goal_value = 0;
			int speed_value = 100;
			int torque_value = 0;
		
			dynamixel_status_t * status = &status_list->servos[i];
				
			for (int j = 0; j < status->num_values; j++){
				switch (status->addresses[j]){
					case DYNAMIXEL_CTL_GOAL_POSITION:
						goal_value = status->values[j];
						break;
					case DYNAMIXEL_CTL_MOVING_SPEED:
						speed_value = status->values[j];
						break;
					case DYNAMIXEL_CTL_TORQUE_LIMIT:
						torque_value = status->values[j];
						break;
					}
				}
				add_servo(gui_data, i, goal_value, speed_value, torque_value);
		}
	
		if (status_list->nservos == 0){
			printf ("There are no servos in the dynamixel_status_list structure\n");
			return;
		}
		for (int i = 0; i < status_list->servos[0].num_values; i++){
			add_sensor(gui_data, status_list->servos[0].addresses[i]);
		}

		set_servo_redraw(gui_data, FALSE);
		return;
	}
	set_servo_redraw(controller->gui_data, FALSE);

	int servo_count = get_servo_count(controller->gui_data);
	int sensor_count = get_sensor_count(controller->gui_data);
	for (int i = 0; i < servo_count; i++){
		for (int j = 0; j < sensor_count; j++){
			if (set_status_value(controller->gui_data, i, j, status_list->servos[i].addresses[j], status_list->servos[i].values[j]) == -1){
				printf ("Mismatch in addresses, require a redraw");
					set_servo_redraw(controller->gui_data, TRUE);
				return;
			}	
		}
	}
}

int initialize_gui(gui_data_t * gui_data, dynamixel_status_list_t * status_list, list_head_t * config_list){
	//reset all the windows
	printf("in %s\n", __func__);
	remove_all_servos(gui_data);

	for (int i = 0; i < status_list->nservos; i++){
		int goal_value = 0;
		int speed_value = 100;
		int torque_value = 0;
		
		dynamixel_status_t * status = &status_list->servos[i];
			
		for (int j = 0; j < status->num_values; j++){
			switch (status->addresses[j]){
				case DYNAMIXEL_CTL_GOAL_POSITION:
					goal_value = status->values[j];
					break;
				case DYNAMIXEL_CTL_MOVING_SPEED:
					speed_value = status->values[j];
					break;
				case DYNAMIXEL_CTL_TORQUE_LIMIT:
					torque_value = status->values[j];
					break;
			}
		}
		add_servo(gui_data, i, goal_value, speed_value, torque_value);
	}
	if (status_list->nservos == 0){
		printf ("There are no servos in the dynamixel_status_list structure\n");
		return -1;
	}
	for (int i = 0; i < status_list->servos[0].num_values; i++){
		add_sensor(gui_data, status_list->servos[0].addresses[i]);
	}

	set_servo_redraw(gui_data, FALSE);
	return 0;
}

gboolean check_for_change(main_controller_t * controller, const dynamixel_status_list_t * status_list){
	if (get_servo_redraw_request(controller->gui_data)){
		return TRUE;
	}
	//get the number of servos available	
	//check if there are the same number of servos
	if (status_list->nservos != controller->gui_data->servo_count){
		printf ("Number of servos in the status_list is different than the number of servos in the GUI\n");
		return TRUE;
	} 
	//checked if there are the same number of sensors
	if (controller->gui_data->servo_count > 0){
		servo_box_t * servo = g_ptr_array_index(controller->gui_data->servo_boxes, 0); 
		if (status_list->servos->num_values != servo->sensor_count){
			printf ("Number of status values in status_list is different than the number of sensors in the GUI\n");
			return TRUE;
		}
	}
	
	return FALSE;
}
