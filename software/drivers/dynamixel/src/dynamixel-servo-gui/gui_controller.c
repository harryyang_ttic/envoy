//gui_controller.c

#include "stdio.h"
#include "gui_controller.h"
#include "callbacks.h"
#include <dynamixel/dynamixel.h>

#ifndef BUILD_PATH
#error "Missing BUILD_PATH compiler flag"
#endif


/* name: gui_initialize
 *
 * description: creates a reference to the gui_structure and creates the GUI
 * 		from the GUI file
 *
 * parameters:
 *	
 *
 * return:
 *	reference to a gui_data_t
 *  NULL	= 	error
 *
*/
gui_data_t *gui_initialize(){
	gui_data_t 	*gui_data 					= NULL;	
	GtkBuilder 	*builder 					= NULL;
	GError 		*error 						= NULL;
	GtkToggleToolButton * 	toggle_button 	= NULL;

	printf ("in %s\n", __func__);
	
	//initialzie GTK+
	printf ("initializing GTK...\n");
	gtk_init (NULL, NULL);
	printf ("Success!\n");

	//create a new builder object
	printf ("getting a new builder...\n");
	builder = gtk_builder_new();
	printf ("Success!\n");

	//load the UI from a file, notify user if there is an error
	printf ("laoding the gui from a file...\n");
	if (!gtk_builder_add_from_file (builder, UI_FILE, &error)){
		g_warning ("%s", error->message);
		g_free(error);
		return NULL; 
	}
	printf ("Success!\n");

	printf ("allocating space for gui_data...");
	//allocate roome for the gui_data_t structure
	gui_data = g_slice_new(gui_data_t);
	if (gui_data == NULL){
		printf ("Failed\n");
		goto fail;
	}
	printf ("Success!\n");

	gui_data->servo_count = 0;

	//get references to needed gui components
	printf ("Getting main_window handle...");
	gui_data->main_window = GTK_WIDGET (
								gtk_builder_get_object (
										builder,
										MAIN_WINDOW_NAME));
	if (gui_data->main_window == NULL){
		printf ("Failed!\n");
		goto fail;
	}
	printf ("Success!\n");
	printf ("Getting the status_label handle...");
	gui_data->status_label = (GtkLabel *)
							GTK_WIDGET (
								gtk_builder_get_object (
										builder,
										STATUS_LABEL_NAME));
	if (gui_data->status_label == NULL){
		printf ("Failed!\n");
		goto fail;
	}
	printf ("Success!\n");
	printf ("Getting the receiving_data_label handle...");
	gui_data->receiving_data_label = (GtkLabel *)
							GTK_WIDGET (
								gtk_builder_get_object (
										builder,
										CONNECTION_LABEL_NAME));	
	if (gui_data->receiving_data_label == NULL){
		printf ("Failed!\n");
		goto fail;
	}
	printf ("Success!\n");
	printf ("Getting the servo_vbox...");
	gui_data->servo_vbox = (GtkVBox *)
							GTK_WIDGET (
								gtk_builder_get_object (
										builder,
										SERVO_VERTICAL_BOX_NAME));
	if (gui_data->servo_vbox == NULL){
		printf ("Failed!\n");
		goto fail;
	}
	printf ("Success!\n");

	printf ("Getting a handle to the toolbar toggle button...");
	toggle_button = (GtkToggleToolButton *)
							GTK_WIDGET (
								gtk_builder_get_object (
									builder,
									TOOL_LISTEN_TOGGLE_NAME));
	printf ("Success!\n");

	printf ("Getting a handle to the toolbar apply_configuration button...");
	gui_data->apply_configuration = (GtkToggleToolButton *)
							GTK_WIDGET (
								gtk_builder_get_object (
									builder,
									TOOL_APPLY_CONFIGURATION_NAME));

	printf ("Success!\n");

	printf ("Allocating space for GPtrArray...");
	gui_data->servo_redo = TRUE;
	//create an array pointer for all the future servos
	gui_data->servo_boxes = g_ptr_array_new (); 
	if (gui_data->servo_boxes == NULL){
		printf ("Failed!\n");
		goto fail;
	}
	printf ("Success!\n");

	//connect GTK signals
	gtk_builder_connect_signals(builder, NULL);

	//connect the signals to their respective callbacks
	g_signal_connect ( 	GTK_TOGGLE_TOOL_BUTTON (toggle_button),
						"toggled",
						(GCallback) cb_listen_mode_toggled,
						gui_data);


	//destry the builder
	g_object_unref (G_OBJECT(builder));

	//show the main window
	gtk_widget_show(gui_data->main_window);
	
	return gui_data;

fail:
	printf ("Freeing all resources...\n");
	if (gui_data != NULL){
		g_slice_free(gui_data_t, gui_data);
	}
	return NULL;
}
/* name: gui_start
 *
 * description: start the main gui loop
 *
 * parameters:
 *	gui_data: 	the main gui structure pointer
 *
 * return:
 *	0	=	success
 *  -1	= 	error
 *
*/
int gui_start(gui_data_t *gui_data){
	printf ("in %s\n", __func__);
	gtk_main();
	return 0;
}
/* name: gui_destroy
 *
 * description: destroy the GUI and all references to the GUI comoponents
 *
 * parameters:
 *	gui_data: 	the main gui structure pointer
 *
 * return:
 *	0	=	success
 *  -1	= 	error
 *
*/
int gui_destroy(gui_data_t * gui_data){
	printf ("in %s\n", __func__);

	if (gui_data == NULL){
		printf ("gui_data == NULL\n");
		return -1;
	}
	if (gui_data->servo_boxes == NULL){
		printf ("gui_data->servo_boxes == NULL\n");
	}

	//free up the allocatd gui_things here
	//go through each of ther servos in the array list, and destroy all references
	//free the data that each element is pointing too as well 
	printf ("Getting rid of array_list of servo boxes\n");
	
	g_ptr_array_free(gui_data->servo_boxes, FALSE);


	printf ("Freeing gui_data\n");
	g_slice_free(gui_data_t, gui_data);
	return -1;
}
/* name: set_receiving_data
 *
 * description: sets the label that says that we are receiving data
 *
 * parameters:
 *	gui_data: 	the main gui structure pointer
 *	receving_data: true/false indicating whether we are receiving data
 *
 * return:
 *
*/
void set_receiving_data(gui_data_t * gui_data, gboolean receiving){
	if (receiving){
		gtk_label_set_text(
			gui_data->receiving_data_label, 
			"receiving data");
	}
	else {
		gtk_label_set_text(
			gui_data->receiving_data_label,
			"no data");
	}
}
/* name: set_status_label
 *
 * description: sets the text of the status label
 *
 * parameters:
 *	gui_data: 	the main gui structure pointer
 *	status:		string for the label
 *
 * return:
 *
*/
void set_status_label(gui_data_t * gui_data, char * status){
	gtk_label_set_text (gui_data->status_label, status);
}
/* name: set_status_values
 *
 * description: puts all numeric data read from an LCM structure, and puts
 *		it into the gui
 *
 * parameters:
 *	gui_data: 	the main gui structure pointer
 *	dynamixel_status_list_t: reference to all the status parameters
 *
 * return:
 *	0	=	success
 *  -1	= 	error
 *
*/
int set_status_value(gui_data_t * gui_data, int servo_index, int sensor_index, int address, double value){
	//printf ("in %s\n", __func__);

	servo_box_t * servo = g_ptr_array_index(gui_data->servo_boxes, servo_index);
	status_box_t * sensor = g_ptr_array_index(servo->servo_status_array, sensor_index);
	if (sensor->sensor_data->address != address){
		printf ("Address of sensors don't match up!\n");
		printf ("LCM Status address = %d != GUI sensor %d", sensor->sensor_data->address, address);
		return -1;
	}
	sensor->value = condition_signal(sensor->sensor_data, value);	

	int length = 0;
	char buffer[ENTRY_BUFFER_LENGTH];

	length = g_snprintf(
					&buffer[0],
					ENTRY_BUFFER_LENGTH,
					"%f",
					sensor->value);
	//check to see if this is a position
	if (sensor->sensor_data->address == DYNAMIXEL_CTL_PRESENT_POSITION){
		set_servo_position_dynamic(gui_data, servo_index, TRUE);
		set_servo_dynamic_position(gui_data, servo_index, (int)value); 
	}
	//printf ("setting text\n");	
	gtk_entry_buffer_set_text(sensor->sensor_data_entry_buffer, &buffer[0], length);

	return 0;
}
/* name: add_servo
 *
 * description: adds a servo to the GUI, draws all components and gets
 *		references to all necessary parts
 *
 * parameters:
 *	gui_data: 	the main gui structure pointer
 *
 * return:
 *	0	=	success
 *  -1	= 	error
 *
*/
int add_servo(
				gui_data_t * gui_data, 
				int servo_index, 
				int goal_value, 
				int speed_value, 
				int torque_value){

	printf ("in %s\n", __func__);

	gchar			sensor_num_name[SENSOR_NUM_NAME_LENGTH];

	GtkHBox 		*main_servo_hbox 		= NULL;
	GtkVBox 		*servo_command_vbox 	= NULL;
	GtkLabel 		*servo_index_label 		= NULL;
	GtkTable 		*servo_command_table 	= NULL;
	GtkLabel 		*goal_name_label 		= NULL;
	GtkLabel 		*speed_name_label 		= NULL;
	GtkLabel 		*torque_name_label 		= NULL;
	GtkVBox 		*goal_command_vbox 		= NULL;
	GtkEntry 		*goal_command_entry 	= NULL;
	GtkHScale 		*goal_command_slider 	= NULL;
	GtkVBox 		*speed_command_vbox 	= NULL;
	GtkEntry 		*speed_command_entry 	= NULL;
	GtkHScale 		*speed_command_slider 	= NULL;
	GtkVBox 		*torque_command_vbox 	= NULL;
	GtkHBox			*torque_command_hbox	= NULL;
	GtkEntry 		*torque_command_entry 	= NULL;
	GtkHScale 		*torque_command_slider 	= NULL;
	GtkCheckButton 	*torque_command_check 	= NULL;
	GtkVBox 		*servo_status_main_vbox	= NULL;
	GtkVBox			*servo_status_name_vbox = NULL;
	GtkLabel 		*servo_name_label 		= NULL;
	GtkHBox			*servo_coupled_hbox		= NULL;
	GtkCheckButton	*servo_coupled			= NULL;
	GtkCheckButton 	*servo_coupled_invert	= NULL;
	GtkLabel		*servo_coupled_label	= NULL;
	GtkHBox			*servo_couple_name_hbox	= NULL;
	GtkEntry		*servo_coupled_with		= NULL;
	GtkTable		*servo_status_table		= NULL; 
	//GtkVBox 		*servo_status_vbox 		= NULL; 

	GtkAdjustment	*goal_slider_adjustment		= NULL;
	GtkAdjustment	*speed_slider_adjustment	= NULL;
	GtkAdjustment	*torque_slider_adjustment	= NULL;
	
	printf("Generating GUI components...");
	g_snprintf (
		&sensor_num_name[0],
		SENSOR_NUM_NAME_LENGTH,
		"Servo %d",
		gui_data->servo_count);
	
	printf ("Servo name: %s\n", &sensor_num_name[0]);

		//load default values
	goal_slider_adjustment = (GtkAdjustment *)gtk_adjustment_new (
															0.0,
															0.0,
															SERVO_MAX,
															1.0,
															10.0,
															10.0);

	speed_slider_adjustment = (GtkAdjustment *)gtk_adjustment_new (
															0.0,
															0.0,
															SERVO_MAX,
															1.0,
															10.0,
															10.0);

	torque_slider_adjustment = (GtkAdjustment *)gtk_adjustment_new (
															0.0,
															0.0,
															SERVO_MAX,
															1.0,
															10.0,
															10.0);

	//get a reference to the main_servo_hbox
	main_servo_hbox = (GtkHBox *)gtk_hbox_new			(FALSE, 2);
	servo_command_vbox = (GtkVBox *)gtk_vbox_new		(FALSE, 2);
	servo_index_label = (GtkLabel *)gtk_label_new		(&sensor_num_name[0]);
	//servo_index_label = (GtkLabel *)gtk_label_new		("Servo");
	servo_command_table = (GtkTable *)gtk_table_new		(2, 3, FALSE);
	goal_name_label = (GtkLabel *)gtk_label_new			("Goal Pos");
	speed_name_label = (GtkLabel *)gtk_label_new 		("Speed");
	torque_name_label = (GtkLabel *)gtk_label_new 		("Torque");
	goal_command_vbox = (GtkVBox *)gtk_vbox_new			(FALSE, 2);
	goal_command_entry = (GtkEntry *) gtk_entry_new		();
	goal_command_slider = (GtkHScale *)gtk_hscale_new 	(goal_slider_adjustment); 
	speed_command_vbox = (GtkVBox *)gtk_vbox_new		(FALSE, 2);
	speed_command_entry = (GtkEntry *) gtk_entry_new 	();
	speed_command_slider = (GtkHScale *)gtk_hscale_new	(speed_slider_adjustment);
	torque_command_vbox = (GtkVBox *)gtk_vbox_new		(FALSE, 2);
	torque_command_entry = (GtkEntry *) gtk_entry_new	();
	torque_command_hbox = (GtkHBox *) gtk_hbox_new		(FALSE, 2);
	torque_command_slider = (GtkHScale *)gtk_hscale_new	(torque_slider_adjustment);
	torque_command_check = (GtkCheckButton *) gtk_check_button_new();
	servo_status_main_vbox = (GtkVBox *)gtk_vbox_new	(FALSE, 2);
	servo_status_name_vbox = (GtkVBox *)gtk_vbox_new	(FALSE, 2);
	servo_name_label = (GtkLabel *)gtk_label_new 		("Servo Name");
	servo_coupled_hbox = (GtkHBox *)gtk_hbox_new		(FALSE, 2);
	servo_couple_name_hbox = (GtkHBox *)gtk_hbox_new	(FALSE, 2); 
	servo_coupled	= (GtkCheckButton *) 
			gtk_check_button_new_with_label				("Coupled");
	servo_coupled_invert = (GtkCheckButton *) 
			gtk_check_button_new_with_label				("Invert");
	servo_coupled_label = (GtkLabel *)gtk_label_new 	("Coupled with: ");
	servo_coupled_with = (GtkEntry *) gtk_entry_new		();
	//servo_status_vbox = (GtkVBox *)gtk_vbox_new			(FALSE, 2);
	servo_status_table = (GtkTable *)gtk_table_new		(3, 1, FALSE);
	printf ("Success!\n");

	printf ("Creating a new servo box...");
	//create a reference to a new servo box
	servo_box_t * servo_box = g_slice_new(servo_box_t);
	if (gui_data->servo_boxes == NULL){
		gui_data->servo_boxes = g_ptr_array_new();	
		printf("servo_boxes == NULL\n");
	}
	servo_box->servo_index = servo_index;
	g_ptr_array_add(gui_data->servo_boxes, servo_box);
	servo_box->sensor_count = 0;
	printf ("Success!\n");



	//put all the items in the right place
	printf ("Attempting to add items...\n");
	gtk_box_pack_start(	GTK_BOX(gui_data->servo_vbox), GTK_WIDGET(main_servo_hbox), TRUE, TRUE, 2);
	printf ("Added main_servo_hbox\n");
	gtk_box_pack_start( (GtkBox *)main_servo_hbox, (GtkWidget *)servo_command_vbox, TRUE, TRUE, 0);
	printf ("Added servo_command_vbox\n");
	gtk_box_pack_start(	(GtkBox *)servo_command_vbox, (GtkWidget *)servo_index_label, FALSE, TRUE,	0);
	printf ("Added servo_index_label\n");
	gtk_box_pack_start( (GtkBox *)servo_command_vbox, (GtkWidget *)servo_command_table, TRUE, TRUE, 0);
	printf ("Added servo_command_table\n");
	gtk_table_attach_defaults( servo_command_table, (GtkWidget *)goal_name_label, 0, 1, 0, 1);
	printf ("Added goal_name_label_to table\n");
	gtk_table_attach_defaults( servo_command_table, (GtkWidget *)speed_name_label, 0, 1, 1, 2);
	printf("Added speed_namel_label to table\n");
	gtk_table_attach_defaults( servo_command_table, (GtkWidget *)torque_name_label, 0, 1, 2, 3);
	printf("Added torque_name_label to table\n");
	gtk_table_attach_defaults( servo_command_table, (GtkWidget *)goal_command_vbox, 1, 2, 0, 1);
	printf("Added goal_command_vbox to table\n");
	gtk_table_attach_defaults( servo_command_table, (GtkWidget *)speed_command_vbox, 1, 2, 1, 2);
	printf("Added speed_command_vbox to table\n");
	gtk_table_attach_defaults( servo_command_table, (GtkWidget *)torque_command_vbox, 1, 2, 2, 3);
	printf("Added torque_command_vbox to table\n");
	gtk_box_pack_start( (GtkBox *)goal_command_vbox, (GtkWidget *)goal_command_entry, TRUE, TRUE, 0);
	printf("Added goal_command_entry to goal vbox\n");
	gtk_box_pack_start( (GtkBox *)goal_command_vbox, (GtkWidget *)goal_command_slider, TRUE, TRUE, 0);
	printf("Added goal_command_slider to goal vbox\n");
	gtk_box_pack_start( (GtkBox *)speed_command_vbox, (GtkWidget *)speed_command_entry, TRUE, TRUE, 0);
	printf("Added speed_command_entry to speed_command_vbox\n");
	gtk_box_pack_start( (GtkBox *)speed_command_vbox, (GtkWidget *)speed_command_slider, TRUE, TRUE, 0);
	printf("Added speed_command_slider to speed command vbox\n");
	gtk_box_pack_start( (GtkBox *)torque_command_vbox, (GtkWidget *)torque_command_entry, TRUE, TRUE, 0);
	printf("Added torque_command_entry to torque_command_vbox\n");
	gtk_box_pack_start( (GtkBox *)torque_command_vbox, (GtkWidget *)torque_command_hbox, TRUE, TRUE, 0);
	printf("Added torque_command_hbox to torque_command_vbox\n");
	gtk_box_pack_start( (GtkBox *)torque_command_hbox, (GtkWidget *)torque_command_slider, TRUE, TRUE, 0);
	printf("Added torque_command_slider to torque_command_hbox\n");
	gtk_box_pack_start( (GtkBox *)torque_command_hbox, (GtkWidget *)torque_command_check, FALSE, TRUE, 0);
	printf("Added torque_command_check to torque_command_hbox\n");

	//status
	gtk_box_pack_start( (GtkBox *)main_servo_hbox, (GtkWidget *)servo_status_main_vbox, TRUE, TRUE, 0);
	printf ("Added servo_status_main_vbox to main_servo_hbox\n");
	gtk_box_pack_start( (GtkBox *)servo_status_main_vbox, (GtkWidget *)servo_status_name_vbox, TRUE, TRUE, 0);
	printf ("Added servo_status_name_vbox to servo_status_main_servo_vbox\n");
	gtk_box_pack_start( (GtkBox *)servo_status_name_vbox, (GtkWidget *)servo_name_label, FALSE, TRUE, 0);
	printf ("Added servo_name_label to servo_status_name_vbox\n");
	//add the coupled hbox
	gtk_box_pack_start( (GtkBox *)servo_status_name_vbox, (GtkWidget *)servo_coupled_hbox, FALSE, TRUE,  0);
	printf ("Added coupled_hbox to servo_status_name_vbox\n");
	gtk_box_pack_start( (GtkBox *)servo_status_name_vbox, (GtkWidget *)servo_couple_name_hbox, FALSE, TRUE, 0);
	printf ("Added the label and entry hbox to servo_status_name_vbox\n");
	//add the coupled check box
	gtk_box_pack_start( (GtkBox *)servo_coupled_hbox, (GtkWidget *) servo_coupled, FALSE, TRUE,  0);
	printf ("Added servo coupled checkbox to servo_coupled_hbox\n");
	gtk_box_pack_start( (GtkBox *)servo_couple_name_hbox, (GtkWidget *) servo_coupled_label, FALSE, TRUE,  0);
	printf ("Added servo coupled label to servo_coupled_hbox\n");
	gtk_box_pack_start( (GtkBox *)servo_couple_name_hbox, (GtkWidget *) servo_coupled_with, FALSE, TRUE,  0);
	printf ("Added servo coupled with entry to servo_coupled_hbox\n");
	gtk_box_pack_start( (GtkBox *)servo_coupled_hbox, (GtkWidget *) servo_coupled_invert, FALSE, TRUE,  0);
	printf ("Added servo invert checkbox with servo coupled_hbox\n");
	gtk_box_pack_start( (GtkBox *)servo_status_name_vbox, (GtkWidget *)servo_status_table, TRUE, TRUE, 0);
	printf("Added servo_status_table to servo_status_main_vbos\n");



	gtk_entry_set_alignment(goal_command_entry, 1.0);
	gtk_entry_set_alignment(speed_command_entry, 1.0);
	gtk_entry_set_alignment(torque_command_entry, 1.0);

	//don't draw the values of the sliders
	gtk_scale_set_draw_value(GTK_SCALE(goal_command_slider), FALSE);
	gtk_scale_set_draw_value(GTK_SCALE(speed_command_slider), FALSE);
	gtk_scale_set_draw_value(GTK_SCALE(torque_command_slider), FALSE);

	//set up the buffers, and attach the references to important GTK_WIDGETS
	servo_box->servo_index_label = servo_index_label;
	servo_box->servo_name_label = servo_name_label;
	servo_box->main_servo_box = main_servo_hbox; 
	servo_box->servo_goal_entry_buffer = gtk_entry_buffer_new("0", 1);
	servo_box->servo_speed_entry_buffer = gtk_entry_buffer_new("0", 1);
	servo_box->servo_torque_entry_buffer = gtk_entry_buffer_new("0", 1);
	servo_box->coupled_with_entry_buffer = gtk_entry_buffer_new("0", 1);
	gtk_entry_set_buffer(goal_command_entry, servo_box->servo_goal_entry_buffer);
	gtk_entry_set_buffer(speed_command_entry, servo_box->servo_speed_entry_buffer);
	gtk_entry_set_buffer(torque_command_entry, servo_box->servo_torque_entry_buffer);
	gtk_entry_set_buffer(servo_coupled_with, servo_box->coupled_with_entry_buffer);
	//servo_box->servo_status_vbox = servo_status_vbox;
	servo_box->servo_status_table = servo_status_table;
	servo_box->servo_status_array = g_ptr_array_new();
	servo_box->torque_enable = FALSE;


	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(torque_command_check), FALSE); 
	gtk_entry_buffer_set_text(servo_box->servo_goal_entry_buffer, "0", 1);
	gtk_entry_buffer_set_text(servo_box->servo_speed_entry_buffer, "0", 1);
	gtk_entry_buffer_set_text(servo_box->servo_torque_entry_buffer, "0", 1);
	


	//attach signals
	g_signal_connect (	GTK_RANGE (goal_command_slider),
						"value-changed",
						(GCallback) cb_hscale_value_changed,
						servo_box->servo_goal_entry_buffer);
	g_signal_connect (	GTK_RANGE (speed_command_slider),
						"value-changed",
						(GCallback) cb_hscale_value_changed,
						servo_box->servo_speed_entry_buffer);
	g_signal_connect (	GTK_RANGE (torque_command_slider),
						"value-changed",
						(GCallback) cb_hscale_value_changed,
						servo_box->servo_torque_entry_buffer);
	g_signal_connect ( 	GTK_TOGGLE_BUTTON (torque_command_check),
						"toggled",
						(GCallback) cb_torque_enabled_changed,
						servo_box);


	servo_box->dynamic_position_flag = FALSE;
	gui_data->servo_count++;
	gtk_widget_show_all(GTK_WIDGET(gui_data->main_window));

	gtk_range_set_value(GTK_RANGE(goal_command_slider), goal_value);
	gtk_range_set_value(GTK_RANGE(speed_command_slider), speed_value);
	gtk_range_set_value(GTK_RANGE(torque_command_slider), torque_value);
	return 0;
}
/* name: set_servo_name
 *
 * description: sets the name of the servo, this is just to make it easier to
 *		recognize which servo is associated to a servo box
 *
 * parameters:
 *	gui_data: 	the main gui structure pointer
 *	gchar * :	name of the servo
 *
 * return:
 *	0	=	success
 *  -1	= 	error
 *
*/
int set_servo_name(gui_data_t * gui_data, gint index, gchar* name){
	printf ("in %s\n", __func__);
	servo_box_t *servo_box = (servo_box_t *)g_ptr_array_index(gui_data->servo_boxes, index);	
	if (servo_box == NULL){
		printf("Failed to get handle to the servo_box at index %d\n", index);
		return -1;
	}
	//set the text of the label
	gtk_label_set_text(servo_box->servo_name_label, name);

	return 0;
}
void remove_servo_status_data(gpointer data, gpointer user_data){
	status_box_t * status_box = (status_box_t *) data;
	status_box->sensor_data_entry_buffer = NULL;
}
/* name: remove_servo
 *
 * description: removes a servo from the gui, along with all references
 *
 * parameters:
 *	gui_data: 	the main gui structure pointer
 *	index: the index of the servo to be removed
 *
 * return:
 *	0	=	success
 *  -1	= 	error
 *
*/
int remove_servo(gui_data_t * gui_data, int index){
	printf ("in %s\n", __func__);
	servo_box_t * servo_box = (servo_box_t *) g_ptr_array_index(gui_data->servo_boxes, index);
	GtkHBox * hbox = servo_box->main_servo_box;
	GPtrArray * servo_status_array = servo_box->servo_status_array;
	if (servo_box == NULL){
		printf ("Failed to get a handle ot the servo box at array index %d\n", index);
		return -1;
	}

	//remove all references to the widget in the servo_box
	servo_box->main_servo_box = NULL;
	servo_box->servo_index_label = NULL;
	servo_box->servo_name_label = NULL;
	servo_box->servo_goal_entry_buffer = NULL;
	servo_box->servo_speed_entry_buffer = NULL;
	servo_box->servo_torque_entry_buffer = NULL;
	servo_box->servo_status_table = NULL;
	servo_box->coupled_with_entry_buffer = NULL;
	servo_box->coupled_enabled_cb = NULL;
	servo_box->coupled_invert_cb = NULL;
	//servo_box->servo_status_vbox = NULL;
	servo_box->servo_status_array = NULL;	

	//remove all references to all the status's
	g_ptr_array_foreach (servo_status_array, remove_servo_status_data, NULL);	
	g_ptr_array_free(servo_status_array, TRUE);

	//destroy the servo_box main reference, all the sub windows will be removed too
	gtk_container_remove(GTK_CONTAINER(gui_data->servo_vbox), GTK_WIDGET(hbox));
	gtk_widget_destroy(GTK_WIDGET(hbox));
	g_ptr_array_remove_index(gui_data->servo_boxes, index);
	g_slice_free(servo_box_t, servo_box);

	gui_data->servo_count--;
	return 0;
}
/* name: get_goal_position
 *
 * description: returns the goal position in the goal position for the servo
 *
 * parameters:
 *	gui_data: 	the main gui structure pointer
 *	index: the index of the servo to be read
 *
 * return:
 *	numeric value of the goal position for the servo
 *  -1	= 	error
 *
*/
gint get_goal_position(gui_data_t *gui_data, int index){
	gint value = 0;
	servo_box_t *servo_box = (servo_box_t *) g_ptr_array_index (gui_data->servo_boxes, index);
	GtkEntryBuffer * entry_buffer = servo_box->servo_goal_entry_buffer;
	
	const gchar * buffer = gtk_entry_buffer_get_text(entry_buffer);
	value = atoi(buffer);

	return value;
}
/* name: get_max_speed
 *
 * description: returns the numeric speed value for the servo
 *
 * parameters:
 *	gui_data: 	the main gui structure pointer
 *	index: the index of the servo to be read
 *
 * return:
 *	numeric value of the speed for the servo
 *  -1	= 	error
 *
*/

gint get_max_speed(gui_data_t *gui_data, int index){
	gint value = 0;
	servo_box_t *servo_box = (servo_box_t *) g_ptr_array_index (gui_data->servo_boxes, index);
	GtkEntryBuffer * entry_buffer = servo_box->servo_speed_entry_buffer;
	
	const gchar * buffer = gtk_entry_buffer_get_text(entry_buffer);
	value = atoi(buffer);

	return value;
}
/* name: get_torque_enable
 *
 * description: returns the whether or not the torque value is enabled
 *
 * parameters:
 *	gui_data: 	the main gui structure pointer
 *	index: the index of the servo to be read
 *
 * return:
 *	TRUE = torque is enabled
 *	FALSE = torque is not enbled
 *
*/

gboolean get_torque_enable(gui_data_t *gui_data, int index){
	servo_box_t *servo_box = (servo_box_t *)g_ptr_array_index(gui_data->servo_boxes, index);
	return servo_box->torque_enable;
}
/* name: get_max_torque
 *
 * description: returns numeric torque value
 *
 * parameters:
 *	gui_data: 	the main gui structure pointer
 *	index: the index of the servo to be read
 *
 * return:
 *	numeric value of the torque for the servo
 *  -1	= 	error
 *
*/

gint get_max_torque(gui_data_t *gui_data, int index){
	gint value = 0;
	servo_box_t *servo_box = (servo_box_t *) g_ptr_array_index (gui_data->servo_boxes, index);
	GtkEntryBuffer * entry_buffer = servo_box->servo_torque_entry_buffer;
	
	const gchar * buffer = gtk_entry_buffer_get_text(entry_buffer);
	value = atoi(buffer);

	return value;

}

void foreach_sensor_func(gpointer data, gpointer user_data){
	printf ("in %s\n", __func__);
	servo_box_t 	*servo_box 	=	(servo_box_t *)	data;		
	gint 	*address 			= 	(gint *) 		user_data;	
	guint	rows				= 0;
	guint 	columns				= 0;

	printf ("Analyzing servo_box...");
	if(servo_box->servo_status_array == NULL){
		servo_box->servo_status_array = g_ptr_array_new();
	}
	printf ("Success!\n");

	GtkTable * sensor_table			= servo_box->servo_status_table;
	//GtkVBox	 * sensor_vbox 			= servo_box->servo_status_vbox; 
	//GtkHBox	 * sensor_hbox			= NULL;
	GtkLabel * sensor_name_label 	= NULL;
	GtkEntry * sensor_entry			= NULL;
	GtkLabel * sensor_unit_label	= NULL;
	
	//setup the new status box
	status_box_t * status_box = g_slice_new(status_box_t);
	status_box->sensor_data_entry_buffer = gtk_entry_buffer_new("0", 1);
	status_box->sensor_data = get_sensor_data_from_address(*address);
	
	printf ("sensor name = %s\n", get_name_from_sensor_data(status_box->sensor_data));
	printf ("set got buffer...\n");
	//check if there is enough row
	gtk_table_get_size(sensor_table, &rows, &columns);

	if (servo_box->sensor_count > 0){
		
		//add a column
		columns++;
		gtk_table_resize(sensor_table, rows, columns);
	}

	printf ("instantiating all gui elementes...\n");
	//instantiate all the gui elements
	sensor_name_label = (GtkLabel *) gtk_label_new 	(
			get_name_from_sensor_data(status_box->sensor_data));
	printf ("got label...\n");
	sensor_entry = (GtkEntry *) gtk_entry_new 		();
	printf ("got entry...\n");
	sensor_unit_label = (GtkLabel *) gtk_label_new 	(
			get_units_from_sensor_data(status_box->sensor_data));
	printf ("got unit labels...\n");
	gtk_entry_set_buffer(sensor_entry, status_box->sensor_data_entry_buffer);
	printf ("got buffer...\n");


	gtk_entry_set_alignment(sensor_entry, 1.0);

	printf ("add to gui...\n");
	//add the views to the gui
	gtk_table_attach_defaults(
		sensor_table, 
		(GtkWidget *) sensor_name_label, 
		0, 
		1, 
		columns - 1, 
		columns);
	gtk_table_attach_defaults(
		sensor_table, 
		(GtkWidget *) sensor_entry, 
		1, 
		2, 
		columns - 1, 
		columns);
	gtk_table_attach_defaults(
		sensor_table, 
		(GtkWidget *) 
		sensor_unit_label, 
		2,
		3, 
		columns - 1, 
		columns);

	g_ptr_array_add(servo_box->servo_status_array, status_box);	
	servo_box->sensor_count++;

}
/* name: add_sensor
 *
 * description: adds all the gui functionality as well as the structure
 *
 * parameter:
 * 	gui_data: the main gui structure pointer
 *	address: the address of the sensor 
 *
 * return:
 *	0 = OK
 *	-1 = Error
 *
 */
int add_sensor(gui_data_t * gui_data, int address){
	printf ("in %s\n", __func__);

	//pass the sensor address with the foreach	
	g_ptr_array_foreach(
		gui_data->servo_boxes, 
		(GFunc)foreach_sensor_func, 
		&address);


	gtk_widget_show_all(GTK_WIDGET(gui_data->main_window));
 	return 0;
}
void foreach_remove_single_sensor (gpointer data, gpointer user_data){
	printf ("in %s\n", __func__);
	status_box_t *status_box	= 	(status_box_t *) data;
	servo_box_t *servo_box		= 	(servo_box_t *) user_data;
	guint rows					=	0;
	guint columns				=	0;

	gtk_table_get_size (servo_box->servo_status_table, &rows, &columns);
	columns--;
	
	//remove the reference to the handle
	status_box->sensor_data_entry_buffer = NULL;
	gtk_table_resize(servo_box->servo_status_table, rows, columns);
		
	//gtk_container_remove(
	//	GTK_CONTAINER(servo_box->servo_status_vbox), 
	//		GTK_WIDGET(status_box->sensor_hbox)
	//		); 
	//status_box->sensor_hbox = NULL;
}
void foreach_remove_sensor_func (gpointer data, gpointer user_data){
	printf ("in %s\n", __func__);
	servo_box_t 	*servo_box 	=	(servo_box_t *)	data;		
	gui_data_t		*gui_data	= 	(gui_data_t *) user_data;

	if (servo_box->sensor_count > 0){
		//go through each of the status function, and unreference handles
		g_ptr_array_foreach(
			servo_box->servo_status_array,
			(GFunc)foreach_remove_single_sensor,
			servo_box);
	}
	g_ptr_array_free (servo_box->servo_status_array, TRUE);
	servo_box->sensor_count = 0;
	servo_box->servo_index_label = NULL;
	servo_box->servo_name_label = NULL;
	servo_box->servo_goal_entry_buffer = NULL;
	servo_box->servo_speed_entry_buffer = NULL;
	servo_box->servo_torque_entry_buffer = NULL;
	//servo_box->servo_status_vbox = NULL;
	servo_box->servo_status_table = NULL; 
	gtk_container_remove(
		GTK_CONTAINER (gui_data->servo_vbox), 
			GTK_WIDGET (servo_box->main_servo_box));
	servo_box->main_servo_box = NULL;
}
/* name: get_servo_count
 *
 * description: returnss the number of servos that are currently being controlled
 *
 * parameter:
 * 	gui_data: the main gui structure pointer
 *
 * return:
 *	number of servos displayed
 *
 */

int get_servo_count(gui_data_t * gui_data){
	return gui_data->servo_count;
}

int get_sensor_count(gui_data_t * gui_data){
	if (gui_data->servo_count == 0){
		return 0;
	}
	servo_box_t *servo = (servo_box_t *)g_ptr_array_index(gui_data->servo_boxes, 0);
	return servo->sensor_count;
}
/* name: remove_all_sensor
 *
 * description: remove all the sensors
 *
 * parameter:
 * 	gui_data: the main gui structure pointer
 *
 * return:
 *	0 = OK
 *	-1 = Error
 *
 */
int remove_all_servos(gui_data_t * gui_data){
	printf ("in %s\n", __func__);
	g_ptr_array_foreach(
		gui_data->servo_boxes,
		(GFunc)foreach_remove_sensor_func,
		gui_data);
	g_ptr_array_free (gui_data->servo_boxes, TRUE);
	gui_data->servo_boxes = NULL;
	gui_data->servo_count = 0;
	//remove all references in vbox
	return 0;
}

void set_servo_redraw(gui_data_t * gui_data, gboolean enable){
	gui_data->servo_redo = enable;
}

gboolean get_servo_redraw_request(gui_data_t * gui_data){
	return gui_data->servo_redo;
}
gboolean is_servo_coupled_enabled (servo_box_t *servo_box){
	return servo_box->coupled;
}
gboolean is_servo_coupled_inverted_enabled (servo_box_t *servo_box){
	return servo_box->invert;
}
gint get_coupled_servo(servo_box_t *servo_box){
	return servo_box->coupled_index;
}
void update_sensor(gpointer data, gpointer user_data){
	status_box_t * status = (status_box_t *)data;
	char buffer[ENTRY_BUFFER_LENGTH];
	g_snprintf(
		&buffer[0],
		ENTRY_BUFFER_LENGTH,
		"%f",
		status->value);
}
void update_servo(gpointer data, gpointer user_data){
	servo_box_t *servo = (servo_box_t *)data;
	g_ptr_array_foreach(
		servo->servo_status_array,
		(GFunc) update_sensor,
		NULL);
}
void update_view(gui_data_t * gui_data){
	//go through each servo, and update the view
	g_ptr_array_foreach(
		gui_data->servo_boxes,
		(GFunc)update_servo,
		NULL);

//	gtk_widget_show_all(GTK_WIDGET(gui_data->main_window));

	//update the view
}

gboolean is_servo_position_dynamic(gui_data_t *gui_data, int index){
	servo_box_t *servo = (servo_box_t *)g_ptr_array_index(gui_data->servo_boxes, index);
	return servo->dynamic_position_flag; 
}
int set_servo_position_dynamic(gui_data_t *gui_data, int index, gboolean enable){
	servo_box_t *servo = (servo_box_t *)g_ptr_array_index(gui_data->servo_boxes, index);
	servo->dynamic_position_flag = enable;
	return 0;
}
int get_servo_dynamic_position(gui_data_t *gui_data, int index){
	servo_box_t *servo = (servo_box_t *)g_ptr_array_index(gui_data->servo_boxes, index);
	return servo->position_val;
}
int set_servo_dynamic_position(gui_data_t *gui_data, int index, int position){
	servo_box_t *servo = (servo_box_t *)g_ptr_array_index(gui_data->servo_boxes, index);
	servo->position_val = position;
	return 0;
}
