//sensor_conditioner.h

#ifndef __SENSOR_CONDITIONER_H__
#define __SENSOR_CONDITIONER_H__

typedef struct _sensor_data_t sensor_data_t;


sensor_data_t * get_sensor_data_from_address(int address);
char * get_name_from_sensor_data(sensor_data_t * sensor_data);
char * get_units_from_sensor_data(sensor_data_t * sensor_data);
double condition_signal(sensor_data_t * sensor_data, double value);

struct _sensor_data_t {
	int address;
	char * name;
	char * unit;
	double (*conditioner_function) (double value);
};


#endif //__SENSOR_CONDITIONER_H__
