#ifndef __MAIN_H__
#define __MAIN_H__
#include "lcm_servo_controller.h"
#include "gui_controller.h"
#include "list.h"
#include <gtk/gtk.h>
#include <glib.h>
#include <sys/time.h>
#include <lcm/lcm.h>
#include <lcmtypes/dynamixel_status_list_t.h>
#include <lcmtypes/dynamixel_status_t.h>
#include <lcmtypes/dynamixel_cmd_list_t.h>
#include <lcmtypes/dynamixel_cmd_t.h>

#define DEFAULT_TIMEOUT_VALUE_MS 100


#define COMMAND_CHANNEL_NAME "DYNAMIXEL_COMMAND"
#define STATUS_CHANNEL_NAME "DYNAMIXEL_STATUS"

#define MAX_SERVO_POS 800
#define NUM_OF_COMMANDS 4

#define WHEELCHAIR_ARM_CONF_FILE BUILD_PATH "/share/dynamixel-servo-gui/wheelchair_arm.conf"

typedef struct _main_controller_t main_controller_t;

static int64_t _timestamp_now()
{
    struct timeval tv;
    gettimeofday (&tv, NULL);
    return (int64_t) tv.tv_sec * 1000000 + tv.tv_usec;
}


struct _main_controller_t {
	GTimer 		*data_available_timer;
	gboolean 	read_first_status;
	gboolean 	listen_mode;
	gint 		timeout_value;
	gui_data_t 	*gui_data;
	lcm_state_t	*lcm_state;
//	list_head_t *config_list;
};

void handle_lcm_status_data(void * data, const dynamixel_status_list_t * status_list);
gboolean check_for_change(main_controller_t * controller, const dynamixel_status_list_t * status_list);

#endif //__MAIN_H__

