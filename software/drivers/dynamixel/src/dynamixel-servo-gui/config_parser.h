//config_parser.h
#include "list.h"
#include "stdbool.h"

#ifndef __CONFIG_PARSER__
#define __CONFIG_PARSER__

#define LINE_SIZE 100

#define NAME_STRING 			"NAME"
#define COUPLED_ENABLE_STRING	"COUPLED_ENABLED"
#define COUPLED_WITH_STRING		"COUPLED_WITH"
#define COUPLED_INVERT_STRING	"COUPLED_INVERT"
#define MODEL_NAME_STRING		"MODEL_NAME"
#define PARAMS_POSITION_STRING	"PARAMS_POSITION"
#define PARAMS_SPEED_STRING		"PARAMS_SPEED"
#define PARAMS_TORQUE_STRING	"PARAMS_TORQUE"

typedef struct _range_parameter_t range_parameter_t;

list_head_t * parse_config_file(char * file_name);
void destroy_config_list(list_head_t *config_list);

//servo name
char * get_servo_name(list_head_t * config_list, int servo_index);

//coupled data
bool is_servo_coupled(list_head_t * config_list, int servo_index);
int get_servo_couple(list_head_t * config_list, int servo_index);
bool is_servo_couple_invert(list_head_t * config_list, int servo_index);

//model number
char * get_servo_model_name(list_head_t * config_list, int servo_index);

//position
int set_servo_mid_position(list_head_t * config_list, int servo_index, int mid_position);
int get_servo_max_position(list_head_t * config_list, int servo_index);
int get_servo_mid_position(list_head_t * config_list, int servo_index);
int get_servo_min_position(list_head_t * config_list, int servo_index);

//speed
int get_servo_max_speed(list_head_t * config_list, int servo_index);
int get_servo_mid_speed(list_head_t * config_list, int servo_index);
int get_servo_min_speed(list_head_t * config_list, int servo_index);

//torque
int get_servo_max_torque(list_head_t * config_list, int servo_index);
int get_servo_mid_torque(list_head_t * config_list, int servo_index);
int get_servo_min_torque(list_head_t * config_list, int servo_index);


struct _range_parameter_t {
	int min;
	int med;
	int max;
};

#endif //__CONFIG_PARSER__
