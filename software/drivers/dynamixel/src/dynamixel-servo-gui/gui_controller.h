//gui_controller.h

#ifndef __GUI_CONTROLLER__
#define __GUI_CONTROLLER__

#include <gtk/gtk.h>
#include <glib.h>
#include <lcmtypes/dynamixel_status_list_t.h>
#include "sensor_conditioner.h"
#include "list.h"
//#include "config_parser.h"

#define MAIN_WINDOW_NAME "main_window"
#define SERVO_VERTICAL_BOX_NAME "servo_vertical_box"
#define STATUS_LABEL_NAME "status_label"
#define CONNECTION_LABEL_NAME "connection_label"
#define TOOL_LISTEN_TOGGLE_NAME "listen_mode_toolbutton"
#define TOOL_APPLY_CONFIGURATION_NAME "apply_configuration_toolbutton"
#define SERVO_MAX 800
#define ENTRY_BUFFER_LENGTH 8
#define SENSOR_NUM_NAME_LENGTH 12

#define UI_FILE BUILD_PATH "/share/dynamixel-servo-gui/dynamixel-servo-gui.glade"



typedef struct _gui_data_t gui_data_t;
typedef struct _servo_box_t servo_box_t;
typedef struct _status_box_t status_box_t;

struct _gui_data_t {
	//GUI
	GtkWidget 			*main_window;
	GtkLabel 			*status_label;
	GtkLabel			*receiving_data_label;
	GtkVBox				*servo_vbox;	
	GPtrArray 			*servo_boxes;
	GtkToggleToolButton *apply_configuration;

	gint 				servo_count;
	gboolean			servo_redo;
	gboolean			listen_mode;
};



struct _servo_box_t {
	//servo index
	gint			servo_index;
	//main handle to widget
	GtkHBox			*main_servo_box;
	//servo label
	GtkLabel 		*servo_index_label;
	//status name
	GtkLabel 		*servo_name_label;

	//goal entry
	GtkEntryBuffer 	*servo_goal_entry_buffer;
	//speed entry
	GtkEntryBuffer 	*servo_speed_entry_buffer;
	//torque entry
	GtkEntryBuffer 	*servo_torque_entry_buffer;
	//coupled with
	GtkEntryBuffer 	*coupled_with_entry_buffer;
	
	//coupled_enabled
	GtkCheckButton 	*coupled_enabled_cb;
	//coupeld invert
	GtkCheckButton	*coupled_invert_cb;

	//status table
	GtkTable			*servo_status_table;
	//status arraylist reference to sensor data
	GPtrArray 		*servo_status_array;	
	
	gboolean		torque_enable;
	gboolean		coupled;
	gboolean		invert;
	gint			coupled_index;

	gint			sensor_count;

	gboolean		dynamic_position_flag;
	gint			position_val;
};

struct _status_box_t {
	//entry
	GtkEntryBuffer 	*sensor_data_entry_buffer;
	sensor_data_t 	*sensor_data;
	double value;
};

gui_data_t * gui_initialize();
int gui_destroy(gui_data_t *gui_data);
int gui_start(gui_data_t *gui_data);
/*
int set_status_values(gui_data_t * gui_data, const dynamixel_status_list_t *status_list);
*/
int set_status_value(gui_data_t * gui_data, int servo_index, int sensor_index, int address, double value);
int add_servo(gui_data_t * gui_data, int servo_index, int goal_value, int speed_value, int torque_value);
int set_servo_name(gui_data_t *gui_data, gint index, gchar *name);
int remove_servo(gui_data_t * gui_data, int index);
void set_status_label(gui_data_t * gui_data, char * status);
int get_servo_count(gui_data_t * gui_data);
int get_sensor_count(gui_data_t * gui_data);

void set_servo_redraw(gui_data_t * gui_data, gboolean enable);
gboolean get_servo_redraw_request(gui_data_t * gui_data);
gboolean is_servo_coupled_enabled (servo_box_t *servo_box);
gboolean is_servo_coupled_inverted_enabled (servo_box_t *servo_box);
gint get_coupled_servo(servo_box_t *servo_box);
void update_view(gui_data_t *gui_data);

//sensor status
int add_sensor(gui_data_t *gui_data, int address);
int remove_all_servos(gui_data_t * gui_data);

//getting the data
gboolean get_torque_enable(gui_data_t * gui_data, int index);
int	get_goal_position(gui_data_t* gui_data, int index);
int get_max_speed(gui_data_t * gui_data, int index);
int get_max_torque(gui_data_t *gui_data, int index); 

int set_servo_position_dynamic(gui_data_t *gui_data, int index, gboolean enable);
gboolean is_servo_position_dynamic(gui_data_t *gui_data, int index);
int get_servo_dynamic_position(gui_data_t *gui_data, int index);
int set_servo_dynamic_position(gui_data_t *gui_data, int index, int position);
#endif //__GUI_CONTROLLER__
