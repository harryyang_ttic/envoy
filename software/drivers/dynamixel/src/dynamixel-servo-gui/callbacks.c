#include "callbacks.h"
#include "main.h"
#include "stdio.h"
#include <glib.h>
#include "gui_controller.h"
/*
G_MODULE_EXPORT gboolean cb_init_with_goal ( 	GtkWidget 	*widget,
												servoData 	*data){
	return (TRUE);
}
*/
G_MODULE_EXPORT void cb_listen_mode_toggled ( 	GtkToggleToolButton *toggle_button,
												gpointer data){
	
	printf ("in %s\n", __func__);
	gui_data_t *gui_data = (gui_data_t *) data;
	gboolean toggle = FALSE;
	toggle = gtk_toggle_tool_button_get_active (toggle_button);

	if (toggle){
		set_status_label(gui_data, "listen only");
	}
	else {
		set_status_label(gui_data, "listen and command");
	}
	gui_data->listen_mode = toggle;
}
G_MODULE_EXPORT void cb_hscale_value_changed (
												GtkRange * range,
												gpointer data){
	GtkEntryBuffer * gtk_entry_buffer = (GtkEntryBuffer *) data;
	int value = (int) gtk_range_get_value(range);
	gchar buffer[ENTRY_BUFFER_LENGTH];

	gint length = 0;
	GtkAdjustment * adj = gtk_range_get_adjustment(range);
	gint max_pos = (gint)gtk_adjustment_get_upper(adj);
	gint min_pos = (gint)gtk_adjustment_get_lower(adj);
	gint position = 0;
	if (value < min_pos){
		position = min_pos;
	}
	else if (value > max_pos){
		position = max_pos;
	}
	else {
		position = (gint) value;
	}
	

	length = g_snprintf(
		&buffer[0],
		ENTRY_BUFFER_LENGTH,
		"%d",
		position);

	gtk_entry_buffer_set_text(
		gtk_entry_buffer,
		buffer,
		length);

}

G_MODULE_EXPORT void cb_torque_enabled_changed (GtkToggleButton * toggle_button,
												gpointer data){
	printf ("in %s\n", __func__);
	servo_box_t * servo_box = (servo_box_t *) data;
	servo_box->torque_enable = gtk_toggle_button_get_active(toggle_button); 
}
