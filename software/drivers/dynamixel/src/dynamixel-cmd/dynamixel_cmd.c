/**
 * Utility program for sending a single command to one or more Dynamixel
 * servos.
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <math.h>
#include <getopt.h>
#include <assert.h>
#include <stdlib.h>

#include <dynamixel/dynamixel.h>

#include "serial.h"

typedef struct _State 
{
    int serial_fd;
} State;

typedef struct _ServoParameter 
{
    const char *cmd_id;
    const char *name;
    uint8_t address;
    uint8_t writable;
    const char *docstr;
} ServoParameter;

static ServoParameter SERVO_PARAMS[] = {
    { "model-number",       "Model Number",     DYNAMIXEL_CTL_MODEL_NUMBER,     0, NULL },
    { "firmware-version",   "Firmware Version", DYNAMIXEL_CTL_FIRMWARE_VERSION, 0, NULL },
    { "unit-id",            "Unit ID",          DYNAMIXEL_CTL_ID,               1, NULL },
    { "baud-rate",          "Baud Rate",        DYNAMIXEL_CTL_BAUD_RATE,        1, NULL },
    { "torque-enable",      "Torque Enable",    DYNAMIXEL_CTL_TORQUE_ENABLE,    1, NULL },
    { "led",                "LED",              DYNAMIXEL_CTL_LED,              1, NULL },
    { "goal-position",      "Goal Position",    DYNAMIXEL_CTL_GOAL_POSITION,    1, NULL },
    { "moving-speed",       "Moving Speed",     DYNAMIXEL_CTL_MOVING_SPEED,     1, NULL },
    { "torque-limit",       "Torque Limit",     DYNAMIXEL_CTL_TORQUE_LIMIT,     1, NULL },
    { "position",           "Present Position", DYNAMIXEL_CTL_PRESENT_POSITION, 0, NULL },
    { "present-speed",      "Present Speed",    DYNAMIXEL_CTL_PRESENT_SPEED,    0, NULL },
    { "present-voltage",    "Present Voltage",  DYNAMIXEL_CTL_PRESENT_VOLTAGE,  0, NULL },
    { "present-temperature","Present Temperature", DYNAMIXEL_CTL_PRESENT_TEMP,  0, NULL },
    { "return-delay", 		"Return Delay",		DYNAMIXEL_CTL_RETURN_DELAY_TIME, 1,NULL},
    //Added by Alec to experiment with endless turn (wheel mode)
    { "cw-angle-limit",     "Clockwise Angle Limit",            DYNAMIXEL_CTL_CW_ANGLE_LIMIT,    1, NULL },
    { "ccw-angle-limit",    "Counterclockwise Angle Limit",     DYNAMIXEL_CTL_CCW_ANGLE_LIMIT,   1, NULL },	    
    { NULL, NULL, 0, 0, NULL },
};

static void usage(void);

static int64_t _timestamp_now()
{
    struct timeval tv;
    gettimeofday (&tv, NULL);
    return (int64_t) tv.tv_sec * 1000000 + tv.tv_usec;
}

static int
cmd_ping(State *state, const char *cmd, int argc, char **argv) 
{
    if(argc < 1) {
        fprintf(stderr, "not enough arguments\n");
        return 1;
    }
    uint8_t unit_id = atoi(argv[0]);
    int64_t start_utime = _timestamp_now();
    uint8_t responder_id;
    int status = dynamixel_ping(state->serial_fd, unit_id, &responder_id);
    if(0 == status) {
        int64_t end_utime = _timestamp_now();
        double dt_ms = (end_utime - start_utime) * 1e-3;
        printf("Response received from unit %02X in %.03f ms\n", responder_id, dt_ms);
    } else {
        printf("Comm error.\n");
    }
    return status;
}
#define __ping_docstr \
"  ping <unit-id>\n" \
"      Ping one unit, or all units (id 254)"

static int
cmd_action(State *state, const char *cmd, int argc, char **argv) 
{
    if(argc < 1) {
        fprintf(stderr, "not enough arguments\n");
        return 1;
    }
    uint8_t unit_id = atoi(argv[0]);
    return dynamixel_action(state->serial_fd, unit_id);
}
#define __action_docstr \
"  action <unit-id>\n" \
"      Triggers execution of commands batched up with reg-write."

static int
cmd_reset(State *state, const char *cmd, int argc, char **argv) 
{
    if(argc < 1) {
        fprintf(stderr, "not enough arguments\n");
        return 1;
    }
    uint8_t unit_id = atoi(argv[0]);
    return dynamixel_reset(state->serial_fd, unit_id);
}
#define __reset_docstr \
"  reset <unit-id>\n" \
"      Reset servo to factory defaults."

static const ServoParameter *
get_servo_param_or_die(const char *param_name, int require_writable)
{
    for(int i=0; SERVO_PARAMS[i].name; i++) {
        ServoParameter *sparam = &SERVO_PARAMS[i];
        if(!strcmp(param_name, sparam->cmd_id)) {
            if(require_writable && !sparam->writable) {
                fprintf(stderr, "Parameter %s is not writable.\n", 
                        param_name);
                exit(1);
                return NULL;
            }
            return sparam;
        }
    }
    fprintf(stderr, "Invalid parameter\n");
    exit(1);
    return NULL;
}

static int
cmd_read(State *state, const char *cmd, int argc, char **argv) 
{
    if(argc < 2) {
        fprintf(stderr, "Insufficient arguments\n");
        return 1;
    }

    const ServoParameter *sparam = get_servo_param_or_die(argv[0], 0);
    uint8_t unit_id = atoi(argv[1]);

    int status;
    uint16_t val;
    status = dynamixel_read(state->serial_fd, unit_id, sparam->address, &val);
    if(0 != status) {
        printf("Failed to read %s.\n", sparam->name);
        dynamixel_print_errors(status);
    } else {
        printf("%s: %10d (0x%X)\n", sparam->name, val, val);
    }
    return status;
}
#define __read_docstr \
"  read <parameter> <unit-id>\n" \
"      Read a parameter from a single servo."

static int
cmd_write(State *state, const char *cmd, int argc, char **argv) 
{
    if(argc < 3) {
        fprintf(stderr, "Insufficient arguments\n");
        return 1;
    }
    const ServoParameter *sparam = get_servo_param_or_die(argv[0], 1);
    uint8_t unit_id = atoi(argv[1]);

    int status;
    uint16_t val = atoi(argv[2]);
    printf("setting 0x%02X to %d\n", sparam->address, val);
    status = dynamixel_write(state->serial_fd, unit_id, sparam->address, val);
    if(0 != status) {
        printf("Failed to write %s.\n", sparam->name);
        dynamixel_print_errors(status);
    }
    return status;
}
#define __write_docstr \
"  write <parameter> <unit-id> <value>\n" \
"      Write a parameter to a single servo."

static int
cmd_reg_write(State *state, const char *cmd, int argc, char **argv) 
{
    if(argc < 3) {
        fprintf(stderr, "Insufficient arguments\n");
        return 1;
    }
    const ServoParameter *sparam = get_servo_param_or_die(argv[0], 1);
    uint8_t unit_id = atoi(argv[1]);

    int status;
    uint16_t val = atoi(argv[2]);
    status = dynamixel_reg_write(state->serial_fd, unit_id, sparam->address, val);
    if(0 != status) {
        printf("Failed to write %s.\n", sparam->name);
        dynamixel_print_errors(status);
    }
    return status;
}
#define __reg_write_docstr \
"  reg-write <parameter> <unit-id> <value>\n" \
"      Write a parameter to a single servo, but delay execution until the\n" \
"      action command is given."

static int
cmd_sync_write(State *state, const char *cmd, int argc, char **argv) 
{
    if(argc < 3) {
        fprintf(stderr, "Insufficient arguments\n");
        return 1;
    }
    const ServoParameter *sparam = get_servo_param_or_die(argv[0], 1);

    int nservos = (argc-1) / 2;
    printf("# servos: %d\n", nservos);
    uint8_t unit_ids[nservos];
    uint16_t values[nservos];

    for(int i=0; i<nservos; i++) {
        unit_ids[i] = atoi(argv[1 + i*2]);
        values[i] = atoi(argv[1 + i*2 + 1]);
        printf("servo: %d value: %d\n", unit_ids[i], values[i]);
    }

    int status = dynamixel_sync_write(state->serial_fd, sparam->address, nservos, unit_ids, values);
    if(0 != status) {
        printf("Failed to write %s.\n", sparam->name);
        dynamixel_print_errors(status);
    }
    return status;
}
#define __sync_write_docstr \
"  sync-write <parameter> <unit-id-1> <value-1> ... \n" \
"      Write different values of the same parameter to many servos."

typedef struct _CmdHandler 
{
    const char *name;
    int (*handle) (State *state, const char *cmd, int argc, char **argv);
    const char *docstr;
} CmdHandler;

static CmdHandler CMD_HANDLERS[] = {
    { "ping",       cmd_ping,       __ping_docstr },
    { "read",       cmd_read,       __read_docstr },
    { "write",      cmd_write,      __write_docstr },
    { "reg-write",  cmd_reg_write,  __reg_write_docstr },
    { "action",     cmd_action,     __action_docstr },
    { "sync-write", cmd_sync_write, __sync_write_docstr },
    { "reset",      cmd_reset,      __reset_docstr },
    { NULL, NULL, NULL },
};

static void 
usage(void)
{
    fprintf(stderr, "usage: dynamixel-cmd [options] <cmd> <command_arguments...>\n"
            "\n"
            "Utility program for sending a single command to one or more Dynamixel\n"
            "servos.\n"
            "\n"
            "Options:\n"
            "  -h, --help             Shows this help text and exits\n"
            "  -d, --devicename <s>   Device to connect to (default: /dev/ttyUSB0)\n"
            "  -b, --baud <b>         Baud rate.  One of: (9600, 19200, 57600, 115200\n"
            "                         200000, 250000, 400000, 500000, 1000000)\n"
            "\n"
            "Commands:\n");
    for(int i=0; CMD_HANDLERS[i].name; i++) {
        fprintf(stderr, "\n"
                "%s\n", CMD_HANDLERS[i].docstr);
    }
    fprintf(stderr, "\nNOTE:  All values are in servo internal units.  Consult the\n"
                      "       Dynamixel servo manuals for the proper conversions.\n");
    fprintf(stderr, "\nParameters:\n");
    for(int i=0; SERVO_PARAMS[i].name; i++) {
        const ServoParameter *sparam = &SERVO_PARAMS[i];
        fprintf(stderr, ""
                "  %-25s %s\n", sparam->cmd_id,
                sparam->writable ? "read-write" : "read");
//                "    Read %s\n", SERVO_PARAMS[i].cmd_id, SERVO_PARAMS[i].name);
        if(SERVO_PARAMS[i].docstr) {
            fprintf(stderr, "    %s\n", SERVO_PARAMS[i].docstr);
        }
    }
    fprintf(stderr, "\n");
    exit(1);
}

int main(int argc, char *argv[])
{
    State *state = (State*) calloc(1, sizeof(State));
    setlinebuf (stdout);

    char *optstring = "hd:b:";
    int c;
    struct option long_opts[] = {
        {"help", no_argument, 0, 'h' },
        {"baud", required_argument, 0, 'b' },
        {"devicename", optional_argument, 0, 'd' },
        {0, 0, 0, 0}
    };

    // set input argument defaults
    int baud = 1000000;
    char *devicename = strdup("/dev/ttyUSB0");

    while ((c = getopt_long (argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
            case 'd':
                free(devicename);
                devicename = strdup (optarg);
                break;
            case 'b':
                baud = atoi(optarg);
                if(baud != 9600 && baud != 19200 && baud != 57600 &&
                   baud != 115200 && baud != 200000 && baud != 250000 &&
                   baud != 400000 && baud != 500000 && baud != 1000000 ) {
                    fprintf(stderr, "Invalid baud rate\n");
                    return 1;
                }
                break;
            case 'h':
            default:
                usage();
        }
    }

    // grab the remaining command line arguments
    if(optind > argc - 1) {
        usage();
    }
    const char *cmd_name = argv[optind];

    char **cmd_args = &argv[optind+1];
    int cmd_nargs = argc - optind - 1;

    // open up a serial connection
    state->serial_fd = serial_open(devicename, baud, 1);
    if (state->serial_fd < 0) {
        fprintf(stderr, "%s: Failed to open %s\n", __FUNCTION__, devicename);
        return state->serial_fd;
    }
    serial_setbaud(state->serial_fd, baud);

    int result = 1;

    // search for the desired command and execute it
    int found_cmd = 0;
    for(int i=0; CMD_HANDLERS[i].name && !found_cmd; i++) {
        const CmdHandler *cmd_handler = &CMD_HANDLERS[i];
        if(!strcmp(cmd_name, cmd_handler->name)) {
            result = cmd_handler->handle(state, cmd_name, cmd_nargs, cmd_args);
            found_cmd = 1;
        }
    }

    if(!found_cmd) {
        fprintf(stderr, "Unrecognized command %s\n", cmd_name);
        return 1;
    }

    serial_close(state->serial_fd);
    free(state);
    free(devicename);

    return result;
}
