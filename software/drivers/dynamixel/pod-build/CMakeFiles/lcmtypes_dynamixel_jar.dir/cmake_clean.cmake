FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/lcmtypes_dynamixel_jar"
  "lcmtypes_dynamixel.jar"
  "../lcmtypes/java/dynamixel/cmd_address_vals_t.class"
  "../lcmtypes/java/dynamixel/cmd_list_t.class"
  "../lcmtypes/java/dynamixel/status_list_t.class"
  "../lcmtypes/java/dynamixel/cmd_t.class"
  "../lcmtypes/java/dynamixel/status_t.class"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/lcmtypes_dynamixel_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
