# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build

# Include any dependencies generated for this target.
include src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/depend.make

# Include the progress variables for this target.
include src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/progress.make

# Include the compile flags for this target's objects.
include src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/flags.make

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/main.c.o: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/flags.make
src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/main.c.o: ../src/dynamixel-cm700/main.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/main.c.o"
	cd /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build/src/dynamixel-cm700 && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/dynamixel-cm700.dir/main.c.o   -c /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/src/dynamixel-cm700/main.c

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/main.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/dynamixel-cm700.dir/main.c.i"
	cd /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build/src/dynamixel-cm700 && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -E /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/src/dynamixel-cm700/main.c > CMakeFiles/dynamixel-cm700.dir/main.c.i

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/main.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/dynamixel-cm700.dir/main.c.s"
	cd /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build/src/dynamixel-cm700 && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -S /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/src/dynamixel-cm700/main.c -o CMakeFiles/dynamixel-cm700.dir/main.c.s

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/main.c.o.requires:
.PHONY : src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/main.c.o.requires

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/main.c.o.provides: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/main.c.o.requires
	$(MAKE) -f src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/build.make src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/main.c.o.provides.build
.PHONY : src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/main.c.o.provides

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/main.c.o.provides.build: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/main.c.o

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/serial.c.o: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/flags.make
src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/serial.c.o: ../src/dynamixel-cm700/serial.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/serial.c.o"
	cd /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build/src/dynamixel-cm700 && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/dynamixel-cm700.dir/serial.c.o   -c /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/src/dynamixel-cm700/serial.c

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/serial.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/dynamixel-cm700.dir/serial.c.i"
	cd /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build/src/dynamixel-cm700 && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -E /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/src/dynamixel-cm700/serial.c > CMakeFiles/dynamixel-cm700.dir/serial.c.i

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/serial.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/dynamixel-cm700.dir/serial.c.s"
	cd /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build/src/dynamixel-cm700 && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -S /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/src/dynamixel-cm700/serial.c -o CMakeFiles/dynamixel-cm700.dir/serial.c.s

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/serial.c.o.requires:
.PHONY : src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/serial.c.o.requires

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/serial.c.o.provides: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/serial.c.o.requires
	$(MAKE) -f src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/build.make src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/serial.c.o.provides.build
.PHONY : src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/serial.c.o.provides

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/serial.c.o.provides.build: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/serial.c.o

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/cm700.c.o: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/flags.make
src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/cm700.c.o: ../src/dynamixel-cm700/cm700.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build/CMakeFiles $(CMAKE_PROGRESS_3)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/cm700.c.o"
	cd /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build/src/dynamixel-cm700 && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/dynamixel-cm700.dir/cm700.c.o   -c /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/src/dynamixel-cm700/cm700.c

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/cm700.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/dynamixel-cm700.dir/cm700.c.i"
	cd /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build/src/dynamixel-cm700 && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -E /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/src/dynamixel-cm700/cm700.c > CMakeFiles/dynamixel-cm700.dir/cm700.c.i

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/cm700.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/dynamixel-cm700.dir/cm700.c.s"
	cd /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build/src/dynamixel-cm700 && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -S /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/src/dynamixel-cm700/cm700.c -o CMakeFiles/dynamixel-cm700.dir/cm700.c.s

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/cm700.c.o.requires:
.PHONY : src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/cm700.c.o.requires

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/cm700.c.o.provides: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/cm700.c.o.requires
	$(MAKE) -f src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/build.make src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/cm700.c.o.provides.build
.PHONY : src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/cm700.c.o.provides

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/cm700.c.o.provides.build: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/cm700.c.o

# Object files for target dynamixel-cm700
dynamixel__cm700_OBJECTS = \
"CMakeFiles/dynamixel-cm700.dir/main.c.o" \
"CMakeFiles/dynamixel-cm700.dir/serial.c.o" \
"CMakeFiles/dynamixel-cm700.dir/cm700.c.o"

# External object files for target dynamixel-cm700
dynamixel__cm700_EXTERNAL_OBJECTS =

bin/dynamixel-cm700: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/main.c.o
bin/dynamixel-cm700: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/serial.c.o
bin/dynamixel-cm700: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/cm700.c.o
bin/dynamixel-cm700: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/build.make
bin/dynamixel-cm700: lib/liblcmtypes_dynamixel.a
bin/dynamixel-cm700: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking C executable ../../bin/dynamixel-cm700"
	cd /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build/src/dynamixel-cm700 && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/dynamixel-cm700.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/build: bin/dynamixel-cm700
.PHONY : src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/build

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/requires: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/main.c.o.requires
src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/requires: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/serial.c.o.requires
src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/requires: src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/cm700.c.o.requires
.PHONY : src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/requires

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/clean:
	cd /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build/src/dynamixel-cm700 && $(CMAKE_COMMAND) -P CMakeFiles/dynamixel-cm700.dir/cmake_clean.cmake
.PHONY : src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/clean

src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/depend:
	cd /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/src/dynamixel-cm700 /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build/src/dynamixel-cm700 /home/harry/Documents/Robotics/envoy/software/drivers/dynamixel/pod-build/src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : src/dynamixel-cm700/CMakeFiles/dynamixel-cm700.dir/depend

