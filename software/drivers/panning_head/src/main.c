// file: main.c
//

#include <stdio.h>
#include <inttypes.h>
#include <glib.h>

#include <getopt.h>

#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif


#include <dynamixel/dynamixel.h>
#include <lcmtypes/dynamixel_cmd_list_t.h>
#include <lcmtypes/dynamixel_status_list_t.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/erlcm_servo_position_list_t.h>

#include <bot_param/param_client.h>
#include <bot_frames/bot_frames.h>

#define V_SPEED 30
#define H_SPEED 50//100
#define H_SPEED_LOW 50
#define H_SPEED_LOW_THRESHOLD_DEG 10.0
#define H_SPEED_HIGH_1_THRESHOLD_DEG 20.0
#define H_SPEED_HIGH_1 100
#define H_SPEED_HIGH_2 150
#define TICKS_TO_DEG (300.0 / 1024.0)
#define HORIZONTAL_SERVO_ID 1
#define VERTICAL_SERVO_ID   2
#define TICK_TOLLERANCE 4//2
#define VERTICAL_SERVO_SIGN -1
#define HORIZONTAL_SERVO_SIGN 1

//vertical servo flipped

//234 is horizontal zero 

//540 is vertical zero 

//#define H_OFFSET -90.0
//#define V_OFFSET 8.496

#define H_OFFSET_TICKS 218//240//234 
#define V_OFFSET_TICKS 540//237//214//540 

#define V_MAX_TICKS 700 
#define V_MIN_TICKS 300

#define H_MAX_TICKS 1023 
#define H_MIN_TICKS 0

#define DEFAULT_H_HEADING 0//90.0
#define DEFAULT_V_HEADING 0

#define VERTICAL_UPPER_LIMIT 20
#define VERTICAL_LOWER_LIMIT -20

#define HORIZONTAL_LOWER_LIMIT (0 - H_OFFSET_TICKS)* TICKS_TO_DEG//-30//-150
#define HORIZONTAL_UPPER_LIMIT (1023 - H_OFFSET_TICKS)* TICKS_TO_DEG

typedef struct _state_t state_t;

struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    guint timer_id_servo;

    BotFrames *bcf;

    double body_to_head_trans[3];

    erlcm_servo_position_list_t *last_head_cmd; 
    dynamixel_status_t last_h_servo_status;	
    dynamixel_status_t last_v_servo_status;	

    int64_t last_servo_utime;
    int64_t last_cmd_utime;

    double current_h_deg; 
    double current_v_deg; 

    gboolean verbose;

  gboolean usev; 
  gboolean useh;

    int arg_speed;
};

static double clamp(double val, double max, double min){
    double v = val;
    
    v = fmax(min, v);
    v = fmin(max, v);
    return v;
}

//
static int get_ticks_from_degrees(double angle, int offset){
    return angle / TICKS_TO_DEG  + offset; 
    //clamp(angle / 0.293  + H_OFFSET_TICKS, H_MAX_TICKS, H_MIN_TICKS); 
}

static double get_degrees_from_ticks(int ticks, int offset){
    return (ticks - offset)* TICKS_TO_DEG;     
    //clamp(angle / 0.293  + H_OFFSET_TICKS, H_MAX_TICKS, H_MIN_TICKS); 
}

static void move_to_position(state_t *self, int c_h_position, int c_v_position){

    if(self->last_head_cmd->dof < 2){
        fprintf(stderr,"Not enough dof in command\n");
        return;
    }

    int use_h = 0, use_v = 0;
    
    use_h = self->useh;
    use_v = self->usev;
    //ignoring the message use commands 
    
    if(self->last_head_cmd->command_type & 0x0001){
        use_h = self->useh && 1;
    }

    if(self->last_head_cmd->command_type & 0x0010){
        use_v = self->usev && 1;
    }

    if(!use_h && !use_v){
        return; 
    }

    int at_h = 0, at_v = 0;

    int h_position, v_position; 
    
    if(use_h){
        double h_normalized = bot_to_degrees(bot_mod2pi(bot_to_radians(self->last_head_cmd->values[0])));  


        if(h_normalized < -90){
            h_normalized += 360;
        }

        //fprintf(stderr,"Original : %f Compensated : %f\n", self->last_head_cmd->values[0], h_normalized);
                

        double h_act_heading = clamp(HORIZONTAL_SERVO_SIGN * /*self->last_head_cmd->values[0]*/ h_normalized , HORIZONTAL_UPPER_LIMIT, 
                                     HORIZONTAL_LOWER_LIMIT); 

        //this is problematic - if the heading is -170 for example 

        h_position = clamp((h_act_heading) / TICKS_TO_DEG  + H_OFFSET_TICKS, H_MAX_TICKS, H_MIN_TICKS) ; //which is it 0.29 or 3.3 ??


        //fprintf(stderr, "Last heard Command : %f  => %f H Position : %f\n", self->last_head_cmd->values[0], h_normalized, h_position);

        if(self->verbose){
            fprintf(stderr, " H :%.0f -> %d c : %d\n" , (h_act_heading) / TICKS_TO_DEG  + H_OFFSET_TICKS , h_position, c_h_position);
        }

        if(fabs(h_position - c_h_position) <= TICK_TOLLERANCE){
            //at goal - not moving         
            if(self->verbose){
                fprintf(stderr, "At h psotion - not moving\n");
            }
            at_h = 1;
        }
    }
    if(use_v){
        double v_normalized = bot_to_degrees(bot_mod2pi(bot_to_radians(self->last_head_cmd->values[1])));

        if(v_normalized < 0){
            v_normalized += 360;
        }
        double v_act_heading = clamp(VERTICAL_SERVO_SIGN * /*self->last_head_cmd->values[1] */ v_normalized, VERTICAL_UPPER_LIMIT, 
                                     VERTICAL_LOWER_LIMIT); 
        
        
        v_position = clamp((v_act_heading) / TICKS_TO_DEG + V_OFFSET_TICKS, V_MAX_TICKS, V_MIN_TICKS); //which is it 0.29 or 3.3 ??
        
        if(self->verbose){
            fprintf(stderr, "V : %f -> %d c : %d\n" , (v_act_heading) / TICKS_TO_DEG + V_OFFSET_TICKS, v_position, c_v_position);
        }
   
        
        if(fabs(v_position - c_v_position) <= TICK_TOLLERANCE){
            //at goal - not moving         
            //if(self->verbose){
            fprintf(stderr, "At v psotion - not moving\n");
            //}
            at_v = 1;
        }
    }
     
    if((use_v && use_h) && (at_h && at_v) || (use_v && !use_h) && (at_v) ||   (use_h && !use_v) && (at_h)){
      if(self->verbose){
        fprintf(stderr,"At goal - done\n");
      }
      return;
    }

    double h_velocity = H_SPEED_LOW;

    if(fabs(h_position - c_h_position) > H_SPEED_HIGH_1_THRESHOLD_DEG / TICKS_TO_DEG){
        h_velocity = H_SPEED_HIGH_2;
    }
    else if(fabs(h_position - c_h_position) > H_SPEED_LOW_THRESHOLD_DEG / TICKS_TO_DEG){
        h_velocity = H_SPEED_HIGH_1;
    }
    

    int no_servos = 0;

    dynamixel_cmd_t *c = NULL;

    //we might check on the last command and if its the same not publish 

    if(use_v && use_h){
        no_servos = 2;
        c = calloc(2, sizeof(dynamixel_cmd_t));
        
        c[0].servo_id = HORIZONTAL_SERVO_ID;
        c[0].torque_enable = 1;
        c[0].goal_position = h_position;
        c[0].max_speed = h_velocity;//self->arg_speed;
        c[0].max_torque = 983;
        
        c[1].servo_id = VERTICAL_SERVO_ID;
        c[1].torque_enable = 1;
        c[1].goal_position = v_position;
        c[1].max_speed = V_SPEED;//60;//self->arg_speed;
        c[1].max_torque = 983;
    }
    else if(use_v){
        no_servos = 1;
        c = calloc(1, sizeof(dynamixel_cmd_t));
        
        c[0].servo_id = VERTICAL_SERVO_ID;
        c[0].torque_enable = 1;
        c[0].goal_position = v_position;
        c[0].max_speed = V_SPEED;//60;//self->arg_speed;
        c[0].max_torque = 983;
    }
    else if(use_h){
        no_servos = 1;
        c = calloc(1, sizeof(dynamixel_cmd_t));
        
        c[0].servo_id = HORIZONTAL_SERVO_ID;
        c[0].torque_enable = 1;
        c[0].goal_position = h_position;
        c[0].max_speed = h_velocity;//self->arg_speed;
        c[0].max_torque = 983;
    }

    dynamixel_cmd_list_t c_list = {
     	.utime = bot_timestamp_now(),
     	.ncommands = no_servos,
     	.commands = c,
    };

    if (dynamixel_cmd_list_t_publish(self->lcm, "DYNAMIXEL_COMMAND", &c_list) != 0){
        printf ("command publish error\n");
    }

    free(c);
}

static void print_target(state_t *self){

    if(self->last_head_cmd->dof < 2){
        fprintf(stderr,"Not enough dof in command\n");
        return;
    }

    int use_h = 0, use_v = 0;
    
    use_h = self->useh;
    use_v = self->usev;
    //ignoring the message use commands 
    
    if(self->last_head_cmd->command_type & 0x0001){
        use_h = self->useh && 1;
    }

    if(self->last_head_cmd->command_type & 0x0010){
        use_v = self->usev && 1;
    }

    if(!use_h && !use_v){
        fprintf(stderr,"No valid command\n");
        return; 
    }

    int at_h = 0, at_v = 0;

    int h_position, v_position; 
    
    if(use_h){
        double h_act_heading = clamp(HORIZONTAL_SERVO_SIGN * self->last_head_cmd->values[0] , HORIZONTAL_UPPER_LIMIT, 
                                     HORIZONTAL_LOWER_LIMIT); 

        h_position = clamp((h_act_heading) / TICKS_TO_DEG  + H_OFFSET_TICKS, H_MAX_TICKS, H_MIN_TICKS) ; //which is it 0.29 or 3.3 ??

        if(self->verbose){
            //fprintf(stderr, " H :%.0f -> %d c : %d\n" , (h_act_heading) / TICKS_TO_DEG  + H_OFFSET_TICKS , h_position, c_h_position);
        }        
    }
    if(use_v){
        double v_act_heading = clamp(VERTICAL_SERVO_SIGN * self->last_head_cmd->values[1]  , VERTICAL_UPPER_LIMIT, 
                                     VERTICAL_LOWER_LIMIT); 
        
        
        v_position = clamp((v_act_heading) / TICKS_TO_DEG + V_OFFSET_TICKS, V_MAX_TICKS, V_MIN_TICKS); //which is it 0.29 or 3.3 ??
        
        if(self->verbose){
            //fprintf(stderr, "V : %f -> %d c : %d\n" , (v_act_heading) / TICKS_TO_DEG + V_OFFSET_TICKS, v_position, c_v_position);
        }
   
        
        /*if(fabs(v_position - c_v_position) <= TICK_TOLLERANCE){
            //at goal - not moving         
            if(self->verbose){
                fprintf(stderr, "At v psotion - not moving\n");
            }
            at_v = 1;
            }*/
    }
     
    if((use_v && use_h) && (at_h && at_v) || (use_v && !use_h) && (at_v) ||   (use_h && !use_v) && (at_h)){
      if(self->verbose){
        fprintf(stderr,"At goal - done\n");
      }
      return;
    }

    int no_servos = 0;

    dynamixel_cmd_t *c = NULL;

    if(use_v && use_h){
        no_servos = 2;
        c = calloc(2, sizeof(dynamixel_cmd_t));
        
        c[0].servo_id = HORIZONTAL_SERVO_ID;
        c[0].torque_enable = 1;
        c[0].goal_position = h_position;
        c[0].max_speed = H_SPEED;//self->arg_speed;
        c[0].max_torque = 983;
        
        c[1].servo_id = VERTICAL_SERVO_ID;
        c[1].torque_enable = 1;
        c[1].goal_position = v_position;
        c[1].max_speed = V_SPEED;//60;//self->arg_speed;
        c[1].max_torque = 983;
    }
    else if(use_v){
        no_servos = 1;
        c = calloc(1, sizeof(dynamixel_cmd_t));
        
        c[0].servo_id = VERTICAL_SERVO_ID;
        c[0].torque_enable = 1;
        c[0].goal_position = v_position;
        c[0].max_speed = V_SPEED;//60;//self->arg_speed;
        c[0].max_torque = 983;
    }
    else if(use_h){
        no_servos = 1;
        c = calloc(1, sizeof(dynamixel_cmd_t));
        
        c[0].servo_id = HORIZONTAL_SERVO_ID;
        c[0].torque_enable = 1;
        c[0].goal_position = h_position;
        c[0].max_speed = H_SPEED;//self->arg_speed;
        c[0].max_torque = 983;
    }

    dynamixel_cmd_list_t c_list = {
     	.utime = bot_timestamp_now(),
     	.ncommands = no_servos,
     	.commands = c,
    };

    if (dynamixel_cmd_list_t_publish(self->lcm, "DYNAMIXEL_COMMAND", &c_list) != 0){
        printf ("command publish error\n");
    }

    free(c);
}


void servo_update_handler(BotFrames *bot_frames, const char *frame, const char * relative_to, int64_t utime, void *user)
{
    //printf("link  %s->%s was updated, user = %p\n", frame, relative_to, user);
}

//80 Hz
static void on_servo(const lcm_recv_buf_t *rbuf, const char * channel, const dynamixel_status_list_t * msg, void * user){
    state_t *self = (state_t*) user;

    if(msg->nservos == 0){
        fprintf(stderr, "No servos in the list - returning\n");
        return;
    }

    self->last_servo_utime = msg->utime;
    //printf("Servo status received successfully at %"PRId64" \n",self->last_servo_utime);

    //cycle through the list and find the proper status 

    int h_pos = 0;
    int v_pos = 0;

    int h_found = 0; 
    int v_found = 0; 

    for(int i=0; i < msg->nservos; i++){
        if(HORIZONTAL_SERVO_ID == msg->servos[i].servo_id){
            h_pos = msg->servos[i].values[1];
            h_found = 1; 
        }
        
        else if(VERTICAL_SERVO_ID == msg->servos[i].servo_id){
            v_pos = msg->servos[i].values[1];
            v_found = 1; 
        }
    }

    if(h_found){
        self->current_h_deg = get_degrees_from_ticks(h_pos, H_OFFSET_TICKS);
    }

    if(v_found){
        self->current_v_deg = get_degrees_from_ticks(v_pos, V_OFFSET_TICKS);
    }
    
    bot_core_rigid_transform_t body_to_head;
    body_to_head.utime = msg->utime;
    body_to_head.trans[0] = self->body_to_head_trans[0];
    body_to_head.trans[1] = self->body_to_head_trans[1];
    body_to_head.trans[2] = self->body_to_head_trans[2];

    
    double rpy[] = { 0.0, 0.0, 0.0};
    rpy[1] = VERTICAL_SERVO_SIGN * self->current_v_deg * M_PI/180;
    rpy[2] = HORIZONTAL_SERVO_SIGN * self->current_h_deg * M_PI/180;
        
    double quat[4];
    bot_roll_pitch_yaw_to_quat (rpy, body_to_head.quat);

    bot_core_rigid_transform_t_publish (self->lcm, "BODY_TO_HEAD", &body_to_head);

    if((!v_found && self->usev) || (!h_found && self->useh)){
        fprintf(stderr,"One or more servo not found => h : %d v: %d\n", h_found, 
                v_found);
        return;
    }            
        
    move_to_position(self, h_pos, v_pos);    
	
    return;
}

//40 Hz
static void on_head_cmd(const lcm_recv_buf_t *rbuf, const char * channel, const erlcm_servo_position_list_t * msg, void * user){
    state_t *self = (state_t*) user;

    //fprintf(stderr,"Command Received\n");

    if(self->last_head_cmd != NULL){
        erlcm_servo_position_list_t_destroy(self->last_head_cmd);
    }

    self->last_head_cmd = erlcm_servo_position_list_t_copy(msg); 
	
    return;
}

//40 Hz
static void on_head_adjustment_cmd_broken(const lcm_recv_buf_t *rbuf, const char * channel, const erlcm_servo_position_list_t * msg, void * user){
    state_t *self = (state_t*) user;

    self->last_head_cmd->values[0] = self->current_h_deg + msg->values[0]; //+= msg->values[0]; 
    self->last_head_cmd->values[0] = fmax(fmin(HORIZONTAL_UPPER_LIMIT, self->last_head_cmd->values[0]), HORIZONTAL_LOWER_LIMIT); 
    //self->last_head_cmd->values[1] += msg->values[1]; 
    self->last_head_cmd->values[1] = self->current_v_deg + msg->values[1];//+= msg->values[1];
    self->last_head_cmd->values[1] = fmax(fmin(VERTICAL_UPPER_LIMIT, self->last_head_cmd->values[1]), VERTICAL_LOWER_LIMIT);

    return;
}


//40 Hz
static void on_head_adjustment_cmd(const lcm_recv_buf_t *rbuf, const char * channel, const erlcm_servo_position_list_t * msg, void * user){
    state_t *self = (state_t*) user;
    //fprintf(stderr,"heard : %, %f\n", self->last_head_cmd->values[0], self->last_head_cmd->values[1]);
    self->last_head_cmd->values[0] += msg->values[0]; 
    self->last_head_cmd->values[0] = fmax(fmin(HORIZONTAL_UPPER_LIMIT, self->last_head_cmd->values[0]), HORIZONTAL_LOWER_LIMIT); 
    //self->last_head_cmd->values[1] += msg->values[1]; 
	self->last_head_cmd->values[1] += msg->values[1];
    self->last_head_cmd->values[1] = fmax(fmin(VERTICAL_UPPER_LIMIT, self->last_head_cmd->values[1]), VERTICAL_LOWER_LIMIT);
    //fprintf(stderr,"heard now : %, %f\n", self->last_head_cmd->values[0], self->last_head_cmd->values[1]);
    return;
}




static gboolean on_servo_timeout (gpointer data) {
    state_t *self = (state_t*) data;
    //printf("TIMEOUT -- no servo status received\n");
    return TRUE;
}

static gboolean on_laser_timeout (gpointer data) {
    state_t *self = (state_t*) data;
    //printf("TIMEOUT -- no lidar data received\n");
    return TRUE;
}


static void usage() {
    fprintf (stderr, "usage: nodding-lidar [options]\n"
             "\n"
             "  -h, --help              Shows this help text and exits\n"
             "  -s, --side              Side angle (degrees)\n"
             "  -u, --up                Vertical angle (degrees)\n"
             "  -t, --turning-speed     Speed of motion (in counts from 1 to 1023)\n\n"
             "  -v, --verbose           Verbose\n\n"
             "  -V, --usev              Use Vertical\n"
             "  -H, --useh              Use Horizontal\n"
             "NOTE: the range of motion of the device is thresholded to 90 degrees above and below the horizontal plane due to hardware constraints.\n"
             );
    exit(1);
}

int main(int argc, char ** argv) {

    state_t *self = (state_t*) calloc(1, sizeof(state_t));

    //Default settings
    self->arg_speed = 100;
        
    self->lcm = bot_lcm_get_global (NULL);
    if(!self->lcm) {
        fprintf (stderr, "Error getting LCM\n");
        goto failed;
    }

    bot_glib_mainloop_attach_lcm(self->lcm);

    char *optstring = "hvs:u:t:HV";

    struct option long_opts[] = {
        {"help", no_argument, 0, 'h'},
        {"side", required_argument, 0, 's'},
        {"up", required_argument, 0, 'u'},
        {"turning-speed", required_argument, 0, 't'},
        {"verbose", no_argument, 0, 'v'},
        {"usev", no_argument, 0, 'V'},
        {"useh", no_argument, 0, 'H'},
        {0, 0, 0, 0}
    };	

    self->last_head_cmd = calloc(1, sizeof(erlcm_servo_position_list_t));

    self->last_head_cmd->dof = 2;
    self->last_head_cmd->values = calloc(self->last_head_cmd->dof , sizeof(double)); 

    self->last_head_cmd->values[0] = DEFAULT_H_HEADING;
    self->last_head_cmd->values[1] = DEFAULT_V_HEADING;
    
    self->last_head_cmd->command_type = 0x0011; 
    self->usev = FALSE;
    self->useh = TRUE;

    //we might need to command only one orientation (to leave the other as is) 

    char c;
    int override_failsafe = 0;
    while ((c = getopt_long (argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
        case 's':
            self->last_head_cmd->values[0] = atoi(optarg);
            self->useh = TRUE;
            break;
        case 'u':
            self->last_head_cmd->values[1] = atoi(optarg);
            self->usev = TRUE;
            break;
        case 'H':
          self->useh = TRUE;
          break;
        case 'V':
          self->usev = TRUE;
          break;
        case 't':
            self->arg_speed = atoi(optarg);
            if (self->arg_speed > 1023 && self->arg_speed < 0) {
                self->arg_speed = fmin(1023, self->arg_speed); 
                self->arg_speed = fmax(0, self->arg_speed); 
            }                            
            break;
        case 'v':
            self->verbose = TRUE;
            break;
        case 'h':
        default:
            usage();
        }
    }

    

    // Get BotFrames instance
    BotParam * param = bot_param_new_from_server(self->lcm, 0);
    if (!param) {
        fprintf (stderr, "Error getting BotParam. Are you running the server?\n");
        goto failed;
    }

    self->bcf = bot_frames_get_global(self->lcm, param);
    bot_frames_add_update_subscriber(self->bcf,servo_update_handler,NULL);

    // Get the fixed translation from body to head
    BotTrans body_to_head;
    bot_param_get_double_array (param, "coordinate_frames.HEAD.initial_transform.translation", &(self->body_to_head_trans[0]), 3);
    //bot_frames_get_trans (self->bcf, "head", "body", &body_to_head);
    //self->body_to_head_trans[0] = body_to_head.trans_vec[0];
    //self->body_to_head_trans[1] = body_to_head.trans_vec[1];
    //self->body_to_head_trans[2] = body_to_head.trans_vec[2];

    dynamixel_status_list_t_subscribe (self->lcm, "DYNAMIXEL_STATUS", &on_servo, self);
    if(argc == 1) {
        //usage();
        fprintf(stderr, "Passive mode - Only publishing frame updates\n");
        self->usev = FALSE;
        self->useh = FALSE;
    }        
    
    erlcm_servo_position_list_t_subscribe (self->lcm, "HEAD_POSITION", &on_head_cmd, self);
    erlcm_servo_position_list_t_subscribe (self->lcm, "HEAD_POSITION_ADJUSTMENT", &on_head_adjustment_cmd, self);
    
    self->mainloop = g_main_loop_new(NULL, FALSE);
	
    self->timer_id_servo=g_timeout_add(1000,on_servo_timeout,self);
    //self->timer_id_laser=g_timeout_add(1000,on_laser_timeout,self);
    g_main_loop_run(self->mainloop);
    return 0;

 failed:
    if (self)
        free(self);
    return -1;

}
