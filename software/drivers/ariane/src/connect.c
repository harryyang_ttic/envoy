/*
 * connect.c
 *
 * Connection establishment related functions.
 *
 * This file is part of libariane
 *
 * Copyright (C) 2009-2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include "internals.h"
#include <errno.h>
#include <assert.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/*
	If error occurs this would leak mem
*/
int ariane_create_sensordev(ariane_sensordev_t **dev) {
	ariane_sensordev_t *d;

	assert(dev);

	*dev = NULL;

	d = malloc(sizeof(ariane_sensordev_t));

	if (!d)
		return -errno;

	memset(d, 0, sizeof(ariane_sensordev_t));

	int err = pthread_mutex_init(&d->req_mutex, NULL);
	if (err)
		return -err;

	for (int i=0; i<ARIANE_MAX_CONCURRECT_REQUESTS; i++) {
		err = pthread_cond_init(&d->req_cond[i], NULL);
		if (err)
			return -err;
	}

	*dev = d;
	return 0;
}

int ariane_delete_sensordev(ariane_sensordev_t *dev) {
	assert(dev);

	int err = pthread_mutex_destroy(&dev->req_mutex);

	for (int i=0; i<ARIANE_MAX_CONCURRECT_REQUESTS; i++) {
		int err2 = pthread_cond_destroy(&dev->req_cond[i]);
		if (!err)
			err = err2;		// this way at least one of the errors get reported
	}

	free(dev);

	return -err;
}

// receiver threads start with this function
static void *receiver(void *void_dev) {
	assert(void_dev);
	ariane_sensordev_t *dev = (ariane_sensordev_t *)void_dev;

	/* Block signals - app should use signals not the receiver threads */
	sigset_t sigset;
	if (sigfillset(&sigset) || pthread_sigmask(SIG_BLOCK, &sigset, NULL)) {
		ariane_async_error(dev, -errno);
		return NULL;
	}

	dev->receiver(dev);
	
	return NULL;
}

int ariane_connect(ariane_sensordev_t *dev) {
	assert(dev && dev->receiver);

	int err = pthread_create(&dev->thread, NULL, receiver, dev);
	if (err)
		return -err;	// this is difficult case: who cleans up if pthread_create failed..
	
	err = ariane_sensordev_get_version_real(dev);
	if (err < 0) {
		dev->state = ARIANE_DEVICE_STATE_ERROR; // this makes the receiver eventually shut down
		return err;
	}

	dev->version = err;
	return 0;
}

int ariane_disconnect(ariane_sensordev_t *dev) {
	assert(dev && dev->state != ARIANE_DEVICE_STATE_SHUTDOWN);

	dev->state = ARIANE_DEVICE_STATE_SHUTDOWN;

	int err = pthread_join(dev->thread, NULL);
	if (err)
		return -err;

	return ariane_delete_sensordev(dev);
}
