/*
 * ariane-test.c
 *
 * Ariane test application.
 *
 * This file is part of libariane
 *
 * Copyright (C) 2009-2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#define ARIANE_I2C_DEBUG

#include <ariane/ariane.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <bot/bot_core.h>
#include <lcm/lcm.h>
#include <lcmtypes/wheelchair3d_lcmtypes.h>
#include <common3d/carmen3d_common.h>

typedef enum {
	MODE_NONE,
	MODE_FILTER = 'f',
	MODE_QUERY = 'q',
	MODE_SENSOR = 's',
	MODE_TORTURE = 't',
	MODE_STRESS = 'x',
	MODE_RESET = 'R',
	MODE_ACCEL = 'a',
	MODE_WRITE = 'w',
	MODE_WIKI = 'k',
	MODE_I2C = 'i'
} mode_t;

static void check_error(const char *s, int err);
static int query(unsigned int count);
static int wiki();
static int sense(const ariane_sid_t *sids, unsigned int nsids);
static int stress(bool defaults);
static int reset();
static int write_accel_offsets(const char *arg);
static int write_file(ariane_sid_t sid, const char *filename);
static int filter_test(ariane_filter_id_t fid, ariane_sid_t sid);
static int i2c_test();

static ariane_sid_t sidlist[ARIANE_MAX_SENSORS];
static unsigned int sidlist_len = 0;
static int ratelist[ARIANE_MAX_SENSORS];
static ariane_sensordev_t *dev = NULL;

static bool convert_units = false;

int main(int argc, char *argv[]) {
	printf("libariane test, Copyright (C) 2009 Nokia Corporation. All rights reserved.\n");

	int err = ariane_init();
	check_error("ariane_init", err);
	
	int c;
	char *modearg = NULL;
	mode_t mode = MODE_NONE;
	char *btaddr = NULL;
	bool overrun_stats = false, show_vbat = false, show_location = false;

	while ((c = getopt(argc, argv, "a:b:f:hikloqs:r:xtvw:RS:u")) != -1) {
		switch (c) {				
			case 'h':
			case '?':			
				printf("Usage: %s [OPTIONS]\n"
					"-h          Help.\n"
					"-a #,#,#    Write accelerometer offsets (3 values).\n"
					"-b addr     Use Bluetooth instead of USB.\n"
					"-q          Query subsystem.\n"
					"-k          Output in wiki format.\n"
					"-i          I2C passthrough test.\n"
					"-s sid      Sensing mode. SID is hex value.\n"
					"-f fid,sid  Filter test. FID and SID are hex values.\n"
					"-l          Print device location information.\n"
					"-w sid:file Write binary block from file.\n"
					"-r          Rate for sensor (example: -s SID -r #).\n"
					"-x          Stress test mode (default settings).\n"
					"-t          Torture mode (killer settings).\n"
					"-o          Print overrun stats before exit (subsys v0.19+).\n"
					"-u          Unit conversions (sensing mode).\n"
					"-v          Print battery level.\n"
					"-R          Reset request test.\n",
					argv[0]);
				return EXIT_SUCCESS;

			case 'b':
				btaddr = optarg;
				break;

			case 'l':
				show_location = true;
				break;

			case 'o':
				overrun_stats = true;
				break;

			case 'u':
				convert_units = true;
				break;

			case 'v':
				show_vbat = true;
				break;

			case MODE_QUERY:
			case MODE_SENSOR:
			case MODE_STRESS:
			case MODE_TORTURE:
			case MODE_RESET:
			case MODE_ACCEL:
			case MODE_WRITE:
			case MODE_FILTER:
			case MODE_WIKI:
			case MODE_I2C:
				if (mode != c && mode != MODE_NONE) {
					fprintf(stderr, "Unsupported combination of options.\n");
					return EXIT_FAILURE;
				}
				mode = c;
				modearg = optarg;

				if (mode == MODE_SENSOR) {
					if (optarg) {
						char *endp = NULL;
						sidlist[sidlist_len++] = strtol(optarg, &endp, 16);
						if (!endp || *endp) {
							fprintf(stderr, "Invalid SID.\n");
							return EXIT_FAILURE;
						}
					} else {
						fprintf(stderr, "SID required.\n");
						return EXIT_FAILURE;
					}
				}
				break;

			case 'r':
				if (mode == MODE_FILTER) {
					// just a quick hack to set filtered sensor rate
					
					sidlist_len = 1;
				} else if (!sidlist_len || mode != MODE_SENSOR) {
						fprintf(stderr, "Rate should be after SID (-s parameter).\n");
						return EXIT_FAILURE;
				}

				if (!optarg) {
					fprintf(stderr, "-r requires rate value.\n");
					return EXIT_FAILURE;
				} else {
					char *endp = NULL;
					ratelist[sidlist_len - 1] = strtol(optarg, &endp, 10);
					if (!endp || *endp || ratelist[sidlist_len - 1] <= 0) {
						fprintf(stderr, "Invalid rate.\n");
						return EXIT_FAILURE;
					}					
				}
				break;				
		}
	}

	if (btaddr) {
		err = ariane_bt_connect(btaddr, &dev);
		check_error("ariane_bt_connect", err);
	} else {
		err = ariane_usb_connect(&dev);
		check_error("ariane_usb_connect", err);
		fprintf(stderr, "Done Connecting\n");
	}

	uint16_t version = ariane_sensordev_get_version(dev);

	if (mode == MODE_WIKI) {
		printf("---++ Sensors in subsystem v%u.%.2u\n", (version >> 8), (version & 0xFF));
	} else {
		printf("Connected! Subsystem version %u.%.2u\n", (version >> 8), (version & 0xFF));
	}

	if (show_vbat) {
		if (version < 0x0015) {	// vbat parameter was added in v0.21
			printf("Battery level is not supported by the subsystem version.\n");
		} else {
			uint32_t vbat;
			
			int err = ariane_sensordev_get_sensor_parameter_value(dev, ARIANE_SID_SYS, ARIANE_SYS_PARAMID_VBAT, &vbat);
			check_error("ariane_sensordev_get_sensor_parameter_value", err);

			printf("Battery voltage: %u mV\n", vbat);
		}
	}
	
	if (show_location) {
		static const char *location_names[] = {
			"ARIANE_DEVICE_LOCATION_HEADSET",
			"ARIANE_DEVICE_LOCATION_WRIST",
			"ARIANE_DEVICE_LOCATION_HOST",
			"ARIANE_DEVICE_LOCATION_ANY"
		};
	
		ariane_device_location_t location = ariane_sensordev_get_location(dev);
		printf("Device location: %s (%d)\n", location_names[location], location);
	}

	switch (mode) {
		case MODE_NONE:
			printf("Doing nothing as requested.\n");
			break;
	
		case MODE_ACCEL:
			if (version < 0x0011) {
				fprintf(stderr, "Too old sensor subsystem.\n");
			} else {
				write_accel_offsets(modearg);
			}
			break;

		case MODE_QUERY:
			query(1);
			break;

		case MODE_I2C:
			if (version < 0x001B)
				fprintf(stderr, "Too old sensor subsystem. v0.27+ needed.\n");
			else
				i2c_test();
			break;

		case MODE_WIKI:
			wiki();
			break;
	
		case MODE_SENSOR:
			sense(sidlist, sidlist_len);
			break;
		
		case MODE_STRESS:
		case MODE_TORTURE:
			stress(mode == MODE_STRESS);
			break;
	
		case MODE_RESET:
			reset();
			break;

		case MODE_WRITE: {
			char *filename = strchr(modearg, ':');

			if (!filename) {
				fprintf(stderr, "Invalid argument. Use sid:filename.\n");
				break;
			}

			*filename++ = 0;

			char *endp = NULL;
			ariane_sid_t sid = strtol(modearg, &endp, 16);
			if (!endp || *endp) {
				fprintf(stderr, "Invalid SID.\n");
				break;
			}

			write_file(sid, filename);
			break;
		}

		case MODE_FILTER: {
			char *sidstr = strchr(modearg, ',');
			if (!sidstr) {
				fprintf(stderr, "Invalid argument. Use fid,sid.\n");
				break;
			}

			*sidstr++ = 0;

			char *endp = NULL;
			ariane_filter_id_t fid = strtol(modearg, &endp, 16);
			if (!endp || *endp) {
				fprintf(stderr, "Invalid FID.\n");
				break;
			}			
			
			endp = NULL;
			ariane_sid_t sid = strtol(sidstr, &endp, 16);
			if (!endp || *endp) {
				fprintf(stderr, "Invalid SID.\n");
				break;
			}

			filter_test(fid, sid);
			break;
		}
	}

	if (overrun_stats) {
		if (version < 0x0013)
			printf("Overrun statistics cannot be printed with this subsystem version.\n");
		else {
			uint32_t events, frames;

			err = ariane_sensordev_get_sensor_parameter_value(dev, ARIANE_SID_SYS, ARIANE_SYS_PARAMID_EVENTS_OVERRUN, &events);
			check_error("ariane_sensordev_get_sensor_parameter_value", err);

			err = ariane_sensordev_get_sensor_parameter_value(dev, ARIANE_SID_SYS, ARIANE_SYS_PARAMID_TX_OVERRUN, &frames);
			check_error("ariane_sensordev_get_sensor_parameter_value", err);
			
			printf("%u events lost.\n"
				   "%u frames lost.\n", events, frames);
		}
	}

	

	err = ariane_disconnect(dev);
	check_error("ariane_disconnect", err);

	return EXIT_SUCCESS;
}

static void print_error(const char *s, int err) {
	err = -err;

	if (err >= ARIANE_ERROR_GENERIC) {
		fprintf(stderr, "%s returned error 0x%X\n", s, err);
	} else {
		fprintf(stderr, "%s returned error %d : %s\n", s, err, strerror(err));
	}
}

static void check_error(const char *s, int err) {
	if (err >= 0)
		return;

	print_error(s, err);

	if (dev)
		ariane_disconnect(dev);

	exit(EXIT_FAILURE);
}

static int query(unsigned int count) {

	while (count--) {
		ariane_sid_t sids[ARIANE_MAX_SENSORS];

		int nsids = ariane_sensordev_get_sensors(dev, sids);
		check_error("ariane_sensordev_get_sensors", nsids);

		printf("Subsystem has %u sensors:\n", nsids);

		for (int i=0; i<nsids; i++) {
			printf("\tSID 0x%4X\n", sids[i]);
		}

		for (int i=0; i<nsids; i++) {
			printf("Sensor #%u ID 0x%X:\n", i, sids[i]);

			char descr[ARIANE_MAX_SENSOR_DESCR_LEN];
			int err = ariane_sensordev_get_sensor_description(dev, sids[i], descr);
			check_error("ariane_sensordev_get_sensor_description", err);

			printf("\tDescription: %s\n", descr);

			ariane_sensor_properties_t props;
			err = ariane_sensordev_get_sensor_properties(dev, sids[i], &props);

			if (err != -ENOENT) {
				check_error("ariane_sensordev_get_sensor_properties", err);

				printf("\tProperties:\n"
					"\t\tSample rate (hint): %u\n"
					"\t\tBits per sample: %u bits\n"
					"\t\tChannels: %u\n"
					"\t\tFlags: 0x%X\n",
					props.sample_rate,
					props.resolution_bits,
					props.sample_channels,
					props.flags);

				if (props.flags & ARIANE_SENSOR_FLAG_FILTER)
					printf("\t\t\tSENSOR_FLAG_FILTER\n");

				if (props.flags & ARIANE_SENSOR_FLAG_STATIC_DEPENDENCY)
					printf("\t\t\tSENSOR_FLAG_STATIC_DEPENDENCY\n");

				if (props.flags & ARIANE_SENSOR_FLAG_HW_MODE)
					printf("\t\t\tSENSOR_FLAG_HW_MODE\n");

				for (int chn=0; chn<props.sample_channels; chn++) {
					printf("\t\tScale (chn %u): %f\n"
						"\t\tOffset (chn %u): %f\n", 
						chn, props.conversions[chn].scale,
						chn, props.conversions[chn].offset);
				}
				
				if (props.name[0])
					printf("\t\tName: %s\n", props.name);
			
				if (props.min_value != props.max_value)
					printf("\t\tMeasurement range: %d to %d\n", props.min_value, props.max_value);
					
				if (props.rates[0]) {
					printf("\t\tRates (Hz): ");
					for (int r=0; r<ARIANE_MAX_SENSOR_RATES; r++) {
						if (props.rates[r])
							printf("%d ", props.rates[r]);							
					}
					printf("\n");
				}
			} // end of properties

			ariane_sid_t deps[ARIANE_MAX_DEPENDENCIES];
			int ndeps = ariane_sensordev_get_dependency_list(dev, sids[i], deps);
			check_error("ariane_sensordev_get_dependency_list", ndeps);

			if (ndeps) {
				if (props.flags & ARIANE_SENSOR_FLAG_FILTER)
					printf("\tThis filter depends on ");
				else if (props.flags & ARIANE_SENSOR_FLAG_HW_MODE)
					printf("\tShares HW resources with ");
				else
					printf("\tDepends on ");

				for (int j=0; j<ndeps; j++)
					printf("0x%X ", deps[j]);
				printf("\n");
			} else {
				printf("\tDependencies: none\n");
			}

			if (ndeps) {
				ndeps = ariane_sensordev_get_all_dependencies(dev, sids[i], deps);
				if (ndeps) {
					printf("\tAll dependencies: ");
					for (int j=0; j<ndeps; j++)
						printf("0x%X ", deps[j]);
					printf("\n");
				}
			}

			ariane_sensor_parameter_t *params = NULL;
			int nparams = ariane_sensordev_get_sensor_parameters(dev, sids[i], &params);
			check_error("ariane_sensordev_get_sensor_parameters", nparams);

			for (int j=0; j<nparams; j++) {
				printf("\tParameter 0x%X:\n", params[j].paramid);
				printf("\t\tDescription: %s\n", params[j].description);
		
				switch (params[j].paramid) {
					case ARIANE_PARAMID_SAMPLING_RATE:
						printf("\t\tType: ARIANE_PARAMID_SAMPLING_RATE\n");
						break;
		
					case ARIANE_PARAMID_DATA_BLOCK_SIZE:
						printf("\t\tType: DATA_BLOCK_SIZE_PARAMETER\n");
						break;
		
					case ARIANE_PARAMID_VERSION:
						printf("\t\tType: ARIANE_PARAMID_VERSION\n");
						break;
					
					default:
						printf("\t\tType: Sensor specific\n");
						break;
				}

				printf("\t\tFlags: 0x%X\n", params[j].flags);
		
				if (params[j].flags & ARIANE_SENSOR_PARAM_FLAG_READONLY)
					printf("\t\t\tARIANE_SENSOR_PARAM_FLAG_READONLY\n");
		
				if (params[j].flags & ARIANE_SENSOR_PARAM_FLAG_SIGNED)
					printf("\t\t\tARIANE_SENSOR_PARAM_FLAG_SIGNED\n");
		
				if (params[j].flags & ARIANE_SENSOR_PARAM_FLAG_RANGE) {
					printf("\t\t\tARIANE_SENSOR_PARAM_FLAG_RANGE\n");
		
					if (params[j].flags & ARIANE_SENSOR_PARAM_FLAG_SIGNED) {
						printf("\t\tRange: %d to %d\n", (int)params[j].min, (int)params[j].max);
					} else {
						printf("\t\tRange: %u to %u\n", params[j].min, params[j].max);
					}
				}

				uint32_t value;
				err = ariane_sensordev_get_sensor_parameter_value(dev, sids[i], params[j].paramid, &value);
				check_error("ariane_sensordev_get_sensor_parameter_value", err);

				if (params[j].flags & ARIANE_SENSOR_PARAM_FLAG_SIGNED)
					printf("\t\tValue: %d\n", value);
				else
					printf("\t\tValue: %u (0x%X)\n", value, value);
			} // end of parameter loop

			if (params)
				free(params);
			else
				printf("\tParameters: N/A\n");
		} // end of sensors loop

		ariane_filter_id_t fids[ARIANE_MAX_FILTERS];

		int nfilters = ariane_sensordev_get_filters(dev, fids);
		check_error("ariane_sensordev_get_filters", nfilters);

		printf("Subsystem has %u filters:\n", nfilters);

		for (int i=0; i<nfilters; i++) {
			printf("Filter #%u ID 0x%X:\n", i, fids[i]);

			char descr[ARIANE_MAX_SENSOR_DESCR_LEN];

			int err = ariane_sensordev_get_filter_description(dev, fids[i], descr);
			check_error("ariane_sensordev_get_filter_description", err);

			printf("\tDescription: %s\n", descr);
		}
	}

	return 0;
}

static const char * const mtype_names[] = {
	[ARIANE_MTYPE_ACCELERATION] = "Acceleration",
	[ARIANE_MTYPE_ANGULAR_RATE] = "Angular rate",
	[ARIANE_MTYPE_PRESSURE] = "Pressure",
	[ARIANE_MTYPE_TEMPERATURE] = "Temperature",
	[ARIANE_MTYPE_MAGNETIC_FIELD] = "Magnetic field",
	[ARIANE_MTYPE_VOLTAGE] = "Voltage",
	[ARIANE_MTYPE_TESTPATTERN] = "Test pattern",
	[ARIANE_MTYPE_KEYCODE] = "Key code",
	[ARIANE_MTYPE_ORIENTATION] = "Orientation",	
	[ARIANE_MTYPE_APPLICATION] = "Application specific",
	[ARIANE_MTYPE_SW_MEASUREMENT] = "SW measurement",
	[ARIANE_MTYPE_HAPTIC] = "Haptics",
	[ARIANE_MTYPE_SYSTEM] = "System"
};

static const char * const datafmt_names[] = {
	[ARIANE_EVENT_DATAFMT_XYZ16] = "3-axis, signed 16bit int",
	[ARIANE_EVENT_DATAFMT_U16] = "Unsigned 16bit int",
	[ARIANE_EVENT_DATAFMT_U32] = "Unsigned 32bit int",
	[ARIANE_EVENT_DATAFMT_KEY] = "Key code",
	[ARIANE_EVENT_DATAFMT_APPLICATION] = "Application",
	[ARIANE_EVENT_DATAFMT_NONE] = "None"
};

typedef struct {
	ariane_sid_t sid;
	const char *long_descr;
} sensor_long_descr_t;

/*
	This is very incorrect place for this information! TODO
*/
static const sensor_long_descr_t long_descr[] = {
	{0x010, "STMicroelectronics LIS331DLH accelerometer in 2G mode. "
			"Offsets can be written to subsystem using [[http://wikis.in.nokia.com/view/Ariane/LibAriane#Ariane_test][ariane-test]]. "
	},
	{0x011, "STMicroelectronics LIS331DLH accelerometer in 4G mode. "
			"Offsets can be written to subsystem using [[http://wikis.in.nokia.com/view/Ariane/LibAriane#Ariane_test][ariane-test]]. "
	},
	{0x012, "STMicroelectronics LIS331DLH accelerometer in 8G mode. "
			"Offsets can be written to subsystem using [[http://wikis.in.nokia.com/view/Ariane/LibAriane#Ariane_test][ariane-test]]. "
	},

	{0x110, "Epson gyroscope with simple dynamic offset correction that can be enabled using a sensor parameter."},

	{0x210, "High resolution barometer."},

	{0x310, "Temperature sensor implemented using a thermistor. Conversion from ADC value is done incorrectly!"},

	{0x410, "AK-8974 magnetometer. The component is the same as in some Nokia products."},

	{0x411, "HMC5843 magnetometer. Due to problems with AK-8974 some SensorBoxes have this component on a separate PWB."},

	{0x412, "Dynamic Offset Estimation for magnetometer. The implementation uses LibAK8974."},

	{0x610, "SW sensor for testing subsystem features. Predefined test pattern is produced."},
	{0x611, "SW sensor for testing subsystem features. Predefined test pattern is produced."},

	{0x710, "This sensor outputs key code when one of the buttons of SensorBox are pressed or released."},

	{0x711, "Simple tap detection implemented using accelerometer. "
			"Sensor parameters allow setting tap detection threshold, a filter factor and bit flag options. "
			"The bit flag options are documented in LibAriane "
			"[[http://projects.research.nokia.com/iadev/libariane/ariane-subsys_8h.html][API documentation]]. "
			"Double-tap activation can be enabled to prevent accidental tap detection. The tap sensor can generate "
			"tactile feedback to tap detection if the appropriate bit flag is set. With the double tap activation the "
			"tap detection is disabled using a timeout that can be set. The timeout time unit is milliseconds. "
			"The accelerometer signal is filtered using an exponential moving average filter that has a factor defined by "
			"the filter bits parameter: alpha = 1 / 2^(filter_bits).\n\n"
			"The key codes can be changed by writing the binary block of the sensor. The implementation stores the "
			"key codes to volatile memory."
	},

	{0x810, "This orientation algorithm, courtesy of Tom Ahola, implements sensor fusion of accelerometer, "
			"magnetometer and gyroscope. See OrientationSensors. "},

	{0x811, "This is classified as an orientation algorithm although it only integrates gyroscope signal without "
			"considering tilt etc. See OrientationSensors."},

	{0x812, "SmartCompass (LibAK8974) calculates compass direction with tilt compensation. "
			"See OrientationSensors. The yaw angle filter implemented in LibAK8974 can be enabled and disabled using "
			"the sensor parameter."
	},

	{0x813, "This sensor combines gyroscope with the SmartCompass. See OrientationSensors."},

	{0x8110, "This \"sensor\" allows controlling the linear vibration motor of SensorBox. "
			 "LibAriane provides simple convenience "
			 "[[http://projects.research.nokia.com/iadev/libariane/ariane-vibra_8h.html][API for actuator control]]. "
			 "BT and USB link latencies make generating very short pulses impossible so the actuator control "
			 "has <b>auto-stop time</b> parameter which defines length of vibration in 10 millisecond units. "
			 "The duty cycle divisor adjusts pulse width modulation ratio and can be used for limiting vibration strength. "
			 "Vibration drive frequency can also be set."
	},

	{0x8210, "The parameters of system sensor give information about system status and allow changing some system level settings."},

	{0, NULL}
};

static int wiki() {
	ariane_sid_t sids[ARIANE_MAX_SENSORS];

	int nsids = ariane_sensordev_get_sensors(dev, sids);
	check_error("ariane_sensordev_get_sensors", nsids);

	char descr[nsids][ARIANE_MAX_SENSOR_DESCR_LEN];

	printf("\n| *SID* | *Measurement type* | *Description* |\n");
	for (int i=0; i<nsids; i++) {
		int err = ariane_sensordev_get_sensor_description(dev, sids[i], descr[i]);
		check_error("ariane_sensordev_get_sensor_description", err);

		ariane_mtype_t mtype = ARIANE_SID_GET_MTYPE(sids[i]);
		printf("| 0x%X | %s | %s |\n", sids[i], mtype_names[mtype], descr[i]);
	}

	printf("\n");

	for (int i=0; i<nsids; i++) {
		/*
			First dependencies and properties
		*/

		ariane_sensor_properties_t props;
		int err = ariane_sensordev_get_sensor_properties(dev, sids[i], &props);

		printf("---+++ Sensor 0x%X: %s\n\n", sids[i], descr[i]);

		int j = 0;
		while (long_descr[j].sid) {
			if (long_descr[j].sid == sids[i]) {
				printf("%s\n\n", long_descr[j].long_descr);
				break;
			}
			j++;
		}

		if (err != -ENOENT) {

			printf("| *Sample rate (hint)* | %u |\n"
				   "| *Bits per sample* | %u |\n"
				   "| *Channels* | %u |\n\n", 
					props.sample_rate,
					props.resolution_bits,
					props.sample_channels);

			ariane_sid_t deps[ARIANE_MAX_DEPENDENCIES];
			int ndeps = ariane_sensordev_get_dependency_list(dev, sids[i], deps);

			if (props.flags & ARIANE_SENSOR_FLAG_FILTER) {
				printf("This sensor is a SW sensor (filter)");

				if (props.flags & ARIANE_SENSOR_FLAG_STATIC_DEPENDENCY)
					printf(" that uses ");
				else
					printf(" that does not use other sensors as input.\n");
			}

			if (props.flags & ARIANE_SENSOR_FLAG_HW_MODE)
				printf("This is sensor is HW mode of a physical sensor. It cannot be used at the same time with ");

			if (ndeps) {
				if (ndeps == 1)
					printf("SID 0x%X.\n\n", deps[0]);
				else {
					printf("SIDs ");
					for (int j=0; j<ndeps; j++)
						printf("0x%X ", deps[j]);

					printf(".\n\n");
				}
			}
		}

		/*
			Sensor parameters
		*/		
		ariane_sensor_parameter_t *params = NULL;
		int nparams = ariane_sensordev_get_sensor_parameters(dev, sids[i], &params);
		check_error("ariane_sensordev_get_sensor_parameters", nparams);


		if (nparams)
			printf("*Sensor parameters*\n"
				   "| *Parameter ID* | *Description* | *Type* | *Value* | *Min* | *Max* | *Access* |\n");

		for (int j=0; j<nparams; j++) {
			printf("| 0x%X | %s | ", params[j].paramid, params[j].description);
	
			switch (params[j].paramid) {
				case ARIANE_PARAMID_SAMPLING_RATE:
					printf("ARIANE_PARAMID_SAMPLING_RATE | ");
					break;
	
				case ARIANE_PARAMID_DATA_BLOCK_SIZE:
					printf("ARIANE_PARAMID_DATA_BLOCK_SIZE | ");
					break;
	
				case ARIANE_PARAMID_VERSION:
					printf("ARIANE_PARAMID_VERSION | ");
					break;
				
				default:
					printf("Sensor specific | ");
					break;
			}

			uint32_t value;
			err = ariane_sensordev_get_sensor_parameter_value(dev, sids[i], params[j].paramid, &value);
			check_error("ariane_sensordev_get_sensor_parameter_value", err);
			
			if (params[j].flags & ARIANE_SENSOR_PARAM_FLAG_SIGNED)
				printf("%d | ", (int)value);
			else
				printf("%u (0x%X) | ", value, value);

			if (params[j].flags & ARIANE_SENSOR_PARAM_FLAG_RANGE) {
				if (params[j].flags & ARIANE_SENSOR_PARAM_FLAG_SIGNED)
					printf("%d | %d | ", (int)params[j].min, (int)params[j].max);
				else
					printf("%u | %u | ", params[j].min, params[j].max);
			} else {
				printf(" | | ");
			}

			if (params[j].flags & ARIANE_SENSOR_PARAM_FLAG_READONLY)
				printf("Read-only |\n");
			else
				printf("Read/write |\n");
		}
		printf("\n");
	}

	printf("---++ Filters\n\n");

	ariane_filter_id_t fids[ARIANE_MAX_FILTERS];

	int nfilters = ariane_sensordev_get_filters(dev, fids);
	check_error("ariane_sensordev_get_filters", nfilters);

	printf("Filters are SW sensors that are not tied to any specific sensor. "
		   "When a filter is instantiated it becomes a SW sensor.\n\n"
		   "| *Filter ID* | *Description* |\n\n");

	for (int i=0; i<nfilters; i++) {
		char descr[ARIANE_MAX_SENSOR_DESCR_LEN];

		int err = ariane_sensordev_get_filter_description(dev, fids[i], descr);
		check_error("ariane_sensordev_get_filter_description", err);

		printf("| 0x%X | %s |\n", fids[i], descr);
	}

	return 0;
}

static volatile bool sigintcaught = false;

static void siginthandler(int s) {
	sigintcaught = true;
}

static int receive_sensor_data(const ariane_sid_t *sids, ariane_sensor_properties_t *props, int nsids) {
	printf("Press ctrl-c to stop..\n");

	sigintcaught = false;

    if (signal(SIGINT, siginthandler) == SIG_ERR)
		return -1;

	while (!sigintcaught) {
		ariane_event_t evt;
		ariane_get_event(true, &evt);

		ariane_mtype_t mtype = ARIANE_MTYPE_SYSTEM;
		if (evt.sensor) {
			mtype = ARIANE_SID_GET_MTYPE(evt.sensor->sid);
		}

		ariane_unit_conversion_t *conversions = NULL;
		
		if (convert_units && evt.sensor) {
			for (int i=0; i<nsids; i++)
				if (sids[i] == evt.sensor->sid) {
					conversions = props[i].conversions;
					break;
				}
		}

		switch (evt.datafmt) {
			case ARIANE_EVENT_DATAFMT_XYZ16:
				if (convert_units && conversions)
					printf("%u, %u, %f, %f, %f\n", evt.timestamp, mtype, 
						(evt.data.xyz.x - conversions[0].offset) * conversions[0].scale,
						(evt.data.xyz.y - conversions[1].offset) * conversions[1].scale,
						(evt.data.xyz.z - conversions[2].offset) * conversions[2].scale);
				else
					printf("%u, %u, %d, %d, %d\n", evt.timestamp, mtype, evt.data.xyz.x, evt.data.xyz.y, evt.data.xyz.z);
				break;

			case ARIANE_EVENT_DATAFMT_U16:
				if (convert_units && conversions) 
					printf("%u, %u, %f\n", evt.timestamp, mtype, (evt.data.u16 - conversions[0].offset) * conversions[0].scale);
				else
					printf("%u, %u, %u\n", evt.timestamp, mtype, evt.data.u16);
				break;

			case ARIANE_EVENT_DATAFMT_U32:
				if (convert_units && conversions) 
					printf("%u, %u, %f\n", evt.timestamp, mtype, (evt.data.u32 - conversions[0].offset) * conversions[0].scale);
				else
					printf("%u, %u, %u\n", evt.timestamp, mtype, evt.data.u32);
				break;
		
			case ARIANE_EVENT_DATAFMT_KEY:
				printf("%u, %u, %u, %s, %s\n", evt.timestamp, mtype, evt.data.key.key, 
					evt.data.key.pressed ? "pressed:true" : "pressed:false",
					evt.data.key.released ? "released:true" : "released:false");
				break;
		
			case ARIANE_EVENT_DATAFMT_APPLICATION:
				printf("%u, %u, ARIANE_EVENT_DATAFMT_APPLICATION\n", evt.timestamp, mtype);
				break;
		
			case ARIANE_EVENT_DATAFMT_ERROR:
				print_error("async error", evt.data.error);
				printf("Aborting..");
				return -1; 
				break;

			case ARIANE_EVENT_DATAFMT_NONE:
				printf("Bug!\n");
				break;
		}
	}

	return 0;
}


void pressure_event_handler(const ariane_event_t *event, void *param) {
  /*assert(queue && queue_size);

	pthread_mutex_lock(&queue_mutex);

	memcpy(&queue[queue_writei], event, sizeof(ariane_event_t));

	if (queue_readi == queue_writei) {
		// reader was too slow, queue_readi points to the oldest, so skip that to keep the order
		queue_readi++;
		if (queue_readi >= queue_size)
			queue_readi = 0;
	}

	if (queue_readi < 0)
		queue_readi = queue_writei;	// queue was empty

	queue_writei++;
	if (queue_writei >= queue_size)
		queue_writei = 0;

	pthread_cond_signal(&queue_cond);
	pthread_mutex_unlock(&queue_mutex);*/
  fprintf(stderr, "Msg sent\n"); 
}

static int sense(const ariane_sid_t *sids, unsigned int nsids) {
	ariane_sensor_t *sensors[nsids];

	int err = ariane_init_event_queue(16);
	check_error("ariane_init_event_queue", err);

	ariane_sensor_properties_t props[nsids];
	
	for (int i=0; i<nsids; i++) {
		err = ariane_sensordev_open(dev, 0, sids[i], ARIANE_SENSOR_ACCESS_MODE_CONTROL, &sensors[i]);		
		check_error("ariane_sensordev_open", err);

		if (err != ARIANE_SENSOR_ACCESS_MODE_CONTROL) {
			printf("Opening SID 0x%X resulted in access mode %u -> aborting..\n", sids[i], err);
			return -1;
		}

		if (ratelist[i]) {
			printf("Setting rate of SID 0x%X to %u\n", sids[i], ratelist[i]);
			err = ariane_sensor_set_sampling_rate(sensors[i], ratelist[i]);
			check_error("ariane_sensor_set_sampling_rate", err);
		}
		
		if (convert_units) {
			err = ariane_sensor_get_properties(sensors[i], &props[i]);
			check_error("ariane_sensor_get_properties", err);
		}
	}

	for (int i=0; i<nsids; i++) {
		err = ariane_sensor_start(sensors[i], pressure_event_handler, NULL);
		check_error("ariane_sensor_start", err);
	}

	if (convert_units) {
		if (receive_sensor_data(sids, props, nsids) < 0)
			return -1;
	} else {
		if (receive_sensor_data(NULL, NULL, 0) < 0)
			return -1;
	}

	printf("Shutting down..\n");

	for (int i=0; i<nsids; i++) {
		int err = ariane_sensor_stop(sensors[i]);
		check_error("ariane_sensor_stop", err);

		err = ariane_sensor_close(sensors[i]);
		check_error("ariane_sensor_close", err);
	}
		
	return 0;
}

static int filter_test(ariane_filter_id_t fid, ariane_sid_t src_sid) {
	ariane_sid_t sid;

	int err = ariane_sensordev_instantiate_filter(dev, fid, src_sid, &sid);
	check_error("ariane_sensordev_instantiate_filter", err);

	printf("Filter 0x%X instantiated using source SID 0x%X. Instance SID is 0x%X\n", fid, src_sid, sid);

	return sense(&sid, 1);
}

static int stress(bool defaults) {
	/* List of SIDS. Note that the location field is (currently!) ignored by the subsys so these work for all device types. */
	static const ariane_sid_t sids[] = {
		0x000,			/* Accelerometer (2G) */
		0x100,			/* Gyroscope */
		0x200,			/* Barometer */
		0x300,			/* Thermistor */
		0x400,			/* Magnetometer (not HMC!) */
		0x600,			/* Test pattern */
	};
	static unsigned int nsids = sizeof(sids) / sizeof(ariane_sid_t);
	/* Order of rate list must match sid list. Querying the max rates from subsys would be the right way to do this. */
	static int rates[] = {
		1000,
		1000,
		0,	// 0=don't set rate
		0,
		20,
		500	// test pattern max is 1kHz
	};
	
	if (!defaults)
		memcpy(ratelist, rates, nsids * sizeof(int));

	return sense(sids, nsids);
}

static int reset() {
	int err = ariane_sensordev_reset(dev);
	check_error("ariane_sensordev_reset", err);
	return 0;
}

static int write_block(ariane_sid_t sid, const uint8_t *data, unsigned int len) {
	ariane_sensor_t *sensor = NULL;

	int blk_size = ariane_sensordev_get_sensor_binary_block_size(dev, sid);
	check_error("ariane_sensordev_get_sensor_binary_block_size", blk_size);

	if (blk_size < len) {
		fprintf(stderr, "Block size is %u bytes but requested writing %u bytes!\n", blk_size, len);
		return -1;
	}

	int err = ariane_sensordev_open(dev, 0, sid, ARIANE_SENSOR_ACCESS_MODE_CONTROL, &sensor);
	check_error("ariane_sensordev_open", err);

	err = ariane_sensor_write_binary_block(sensor, 0, data, len);
	check_error("ariane_sensor_write_binary_block", err);

	err = ariane_sensor_close(sensor);
	check_error("ariane_sensor_close", err);

	return 0;
}

static int write_file(ariane_sid_t sid, const char *filename) {
	FILE *file = fopen(filename, "r");

	int len;
	if (!file || fseek(file, 0, SEEK_END) < 0 ||
		(len = ftell(file)) < 0 || fseek(file, 0, SEEK_SET) < 0) {
		perror(filename);
		return -1;
	}

	uint8_t buf[len];
	
	if (fread(buf, len, 1, file) != 1) {
		perror(filename);
		return -1;
	}

	fclose(file);

	return write_block(sid, buf, len);
}

static int write_accel_offsets(const char *arg) {
	if (!arg) {
		fprintf(stderr, "Offsets must be specified on command line.\n");
		return -1;
	}

	int16_t offsets[3];

	int n = sscanf(arg, "%hd,%hd,%hd", &offsets[0], &offsets[1], &offsets[2]);
	if (n != 3) {
		fprintf(stderr, "Invalid offsets\n");
		return -1;
	}

	printf("Storing offsets %d, %d, %d.\n", offsets[0], offsets[1], offsets[2]);

	uint8_t buf[sizeof(offsets)];
	uint8_t *p = buf;

	for (int i=0; i<3; i++) {
		// protocol byte order is not used for the binary block of the accelerometer
		*p++ = offsets[i] & 0xFF;
		*p++ = ((uint16_t)offsets[i]) >> 8;
	}

	return write_block(ARIANE_SID(0, ARIANE_DEVICE_LOCATION_ANY, ARIANE_MTYPE_ACCELERATION), buf, sizeof(buf));
}

#define I2C_ADDR_ACCEL 		0x18

static int i2c_test() {
	ariane_session_id_t session = ariane_get_session_id();
	
	uint8_t data[6];
	
	// Read register 0x0F from accelerometer
	int n = ariane_i2c_read(dev, session, I2C_ADDR_ACCEL, 0x0F, data, 1);
	check_error("ariane_i2c_req", n);
	
	if (n != 1 || data[0] != 0x32) {
		fprintf(stderr, "Unexpected response.\n");
		return-1;
	}
	
	// Write 0x2F to register 0x20: enables 100Hz mode
	data[0] = 0x2F;
	n = ariane_i2c_write(dev, session, I2C_ADDR_ACCEL, 0x20, data, 1);
	check_error("ariane_i2c_req", n);

	printf("Press ctrl-c to stop..\n");

	sigintcaught = false;

    if (signal(SIGINT, siginthandler) == SIG_ERR)
		return -1;

	while (!sigintcaught) {
		// 0x40 to get the chip in multibyte mode
		n = ariane_i2c_read(dev, session, I2C_ADDR_ACCEL, 0x28 | 0x80, data, 6);
		check_error("ariane_i2c_req", n);
		
		int16_t *d16 = (int16_t *)&data;
		
		printf("%d, %d, %d\n", d16[0], d16[1], d16[2]);
	}
	
	return 0;
}

