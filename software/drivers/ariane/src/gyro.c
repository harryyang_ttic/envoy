/*
 * gyro.c
 *
 * Simple example for reading RAW gyroscope values.
 *
 * This file is part of libariane
 *
 * Copyright (C) 2009-2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 *
 *	Compiling in Linux systems:
 *		gcc -std=gnu99 -lariane gyro.c -o gyro
 *
 *		Note that libariane-dev package must be installed (otherwise the
 *		header files of the library are not found).
 *
 *		Tab width is 4.
 *
 *		This example is for libariane 0.1-4 (and newer).
 *
 */

#include <ariane/ariane.h>		/* libariane applications must include ariane.h */
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
#include <lcmtypes/senlcm_imu_xyz_msg_t.h>

#include <stdio.h>				/* printf, fprintf */
#include <stdlib.h>				/* exit */
#include <string.h>				/* strerror */
#include <signal.h>

static volatile bool sigintcaught = false;
/*
	This function returns in case of an error. This is bad programming practice but in order to keep this
	example short this is used for error handling.
*/
static void check_error(const char *msg, int err);

static void siginthandler(int s) {
	sigintcaught = true;
}


static ariane_sensordev_t *dev = NULL;	// sensor device instance (single SensorBox / add-on / headset)

int main(int argc, char *argv[]) {
	/*
		Step1
			First initialize libariane then event queue is initialized. Note that the event queue is
			designed for simple example applications like this one. See step4 comments for more advanced
			example (application implements event handler).
	*/
	int err = ariane_init();
	check_error("ariane_init", err);

	err = ariane_init_event_queue(4);
	check_error("ariane_init_event_queue", err);

	/*
		Step2
			Connect to SensorBox (or add-on) over USB.
	*/
	err = ariane_usb_connect(&dev);
	check_error("ariane_usb_connect", err);

	/*
		Step3
			Open gyroscope in control mode.
			
			This could be done also like this (gyroscope SID is 0x110):
			err = ariane_sensordev_open(dev, 0, 0x110, ARIANE_SENSOR_ACCESS_MODE_CONTROL, &gyroscope);

			If integrated/accumulated gyroscope data was needed then try
			err = ariane_sensordev_open(dev, 0, 0x811, ARIANE_SENSOR_ACCESS_MODE_CONTROL, &gyroscope);

			The macro ARIANE_SID can construct SIDs:
			ARIANE_SID(0, ARIANE_DEVICE_LOCATION_WRIST, ARIANE_MTYPE_ANGULAR_RATE) is 0x110
	*/
	ariane_sensor_t *gyroscope = NULL;

	err = ariane_sensordev_open_by_type(dev, 0, ARIANE_MTYPE_ANGULAR_RATE, ARIANE_SENSOR_ACCESS_MODE_CONTROL, &gyroscope);
	check_error("ariane_sensordev_open_by_type", err);

	if (err != ARIANE_SENSOR_ACCESS_MODE_CONTROL) {
		ariane_disconnect(dev);
		fprintf(stderr, "Could not open sensor in control mode (already open?).");
		return EXIT_FAILURE;
	}

	/*
		Step4
			Start gyroscope. We use the built-in event handler of libariane. Alternatively
			this example could implement it's own event handler which could be like this:

				void application_event_handler(const ariane_event_t *event, void *param) {
					/ *
						Insert event processing here. Any lenghty operations should be done in
						another thread because the event handler is called from libariane thread (which
						handles data reception). See notes in ariane.h
					* /
				}

	*/
	err = ariane_sensor_start(gyroscope, ariane_event_handler, NULL);
	check_error("ariane_sensor_start", err);

	/*
		Step5
			Data is received using ariane_get_event() and printed to standard output.
	*/
        if (signal(SIGINT, siginthandler) == SIG_ERR) {
		perror("signal");
		ariane_disconnect(dev);
		return EXIT_FAILURE;
	}

    lcm_t *lcm = bot_lcm_get_global(NULL);
        
	//printf("Sensor started. 100 samples will be displayed before exiting.\n");
	//int n = 100;
	//while (n--) {
        while (!sigintcaught) {
		/*
			When the first parameter (the boolean parameter) of ariane_get_event is true,
			the function waits until an event becomes available. If set to false the function
			does not return until
		*/
		ariane_event_t event;
		ariane_get_event(true, &event);
		
		/*
			Event data format is ARIANE_EVENT_DATAFMT_XYZ16 for gyroscope. It is also possible
			that connection to sensor device is lost which we se as an event with
			data format ARIANE_EVENT_DATAFMT_ERROR.
		*/
		if (event.datafmt == ARIANE_EVENT_DATAFMT_XYZ16) {
            senlcm_imu_xyz_msg_t xyz;
            xyz.utime = bot_timestamp_now();
			xyz.x = (int)event.data.xyz.x;
            xyz.y = (int)event.data.xyz.y;
            xyz.z = (int)event.data.xyz.z;
			//printf("%d, %d, %d\n", (int)event.data.xyz.x, (int)event.data.xyz.y, (int)event.data.xyz.z);
            senlcm_imu_xyz_msg_t_publish(lcm, "IMU_STAT", &xyz);
		} else if (event.datafmt == ARIANE_EVENT_DATAFMT_ERROR) {
			fprintf(stderr, "Async error!\n");
			break;
		}
	}

	/*
		Step6
			It is important to stop and close sensors and disconnect sensor device properly.
	*/
	ariane_sensor_stop(gyroscope);
	ariane_sensor_close(gyroscope);
	ariane_disconnect(dev);
}

static void print_error(const char *s, int err) {
	err = -err;

	if (err >= ARIANE_ERROR_GENERIC) {
		fprintf(stderr, "%s returned error 0x%X\n", s, err);
	} else {
		fprintf(stderr, "%s returned error %d : %s\n", s, err, strerror(err));
	}
}

static void check_error(const char *s, int err) {
	if (err >= 0)
		return;

	print_error(s, err);

	if (dev)
		ariane_disconnect(dev);

	exit(EXIT_FAILURE);
}
