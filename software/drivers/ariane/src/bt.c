/*
 * bt.c
 *
 * Bluetooth support.
 *
 * This file is part of libariane
 *
 * Copyright (C) 2009-2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include "internals.h"
#include "slip.h"

#include <arpa/inet.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>


static void receiver(ariane_sensordev_t *dev) {	
	assert(dev->driver_data);
	int btsocket = *(int *)dev->driver_data;

	uint8_t rxbuf[ARIANE_MAX_MESSAGE_LEN];
	uint8_t decode_buf[ARIANE_MAX_MESSAGE_LEN];
	unsigned int rxpos = 0;
	bool sync = false;
	bool escmode = false;
	unsigned int frame_len = 0;

	while (dev->state == ARIANE_DEVICE_STATE_READY) {
		fd_set readfds;
		FD_SET(btsocket, &readfds);
		
		struct timeval timeout = {ARIANE_TIMEOUT, 0};

		int i = select(btsocket + 1, &readfds, NULL, NULL, &timeout);
		if (i < 0) {
			if (errno != EINTR)
				ariane_async_error(dev, -errno);
			continue;
		}

		if (i != 1)
			continue;

		ssize_t n;
		do {
			n = read(btsocket, rxbuf, sizeof(rxbuf));
		} while (n < 0 && errno == EINTR);		// the main app might get signals that libariane shouldn't care about

		if (n < 0) {
#ifndef NDEBUG
			perror("read() failed in bt receiver");
#endif
			ariane_async_error(dev, -errno);
			continue;
		}
#if 0
		printf("BT RX (rxpos %u, sync %u): ", rxpos, sync);
		for (int i=0; i<n; i++) {
			printf("0x%X ", rxbuf[i]);
		}
		printf("\n");
#endif
		/*
			SLIP decoding of received data. Note that subsystem adds 2-byte header to sensor protocol
			messages.

			The code is ugly but it has been tested in several apps so it works even if it's unreadable.
		*/
		for (int i=0; i<n; i++) {
			uint8_t data = rxbuf[i];

			if (!sync) {
				if (data == SLIP_END) {
					sync = true;
					rxpos = 0;
				}
			} else {
				switch (data) {
					case SLIP_END:
						if (rxpos > 4) {
							if (frame_len != rxpos - 2) {
#ifndef NDEBUG
								/*
									This is known problem with SensorBoxes. It seems that the BlueCore
									SW cannot keep up with the data flow and it misses bytes. Flow control should
									prevent that but..
								*/
								fprintf(stderr, "Overrun bug %u != %u\n", frame_len, rxpos - 2);
#endif
							} else {
								// frame successfully received
								ariane_receive(dev, decode_buf, frame_len);
							}
						}
						rxpos = 0;
						break;
	
					case SLIP_ESC:
						escmode = true;
						break;

					default:
						if (escmode) {
							escmode = false;
							if (data == SLIP_ESC_END)
								data = SLIP_END;
							else if (data == SLIP_ESC_ESC)
								data = SLIP_ESC;
							else {
								/* unexpected octet.. resync */
								sync = false;
							}
						}

						if (rxpos >= ARIANE_MAX_MESSAGE_LEN) {
							sync = false;
						} else {
							if (!rxpos) {
								// the first data byte in frame should be frame type - if it's not then resync
								if (data != 0x00)
									sync = false;
							} else if (rxpos == 1) {
								// the second byte is the frame length
								frame_len = data;
							} else {
								decode_buf[rxpos - 2] = data;
							}
							rxpos++;
						}
				} // switch
			} // else
		} // for
	}

	close(btsocket);

	if (dev->driver_data) {
		free(dev->driver_data);
		dev->driver_data = NULL;
	}
}

static int transmit(ariane_sensordev_t *dev, uint8_t *data, unsigned int len) {
	assert(dev && data);

	if (dev->state == ARIANE_DEVICE_STATE_ERROR)
		return ARIANE_ERROR_GENERIC;

	int btsocket = *(int *)dev->driver_data;

	uint8_t slip_buf[len * 2 + SLIP_FRAMING_OVERHEAD + 1];

	slip_buf[0] = SLIP_END;
	slip_buf[1] = 0;		// frame type code - used only with BT

	size_t nleft = slip_esc(SLIP_NO_EXTRA_END, data, slip_buf + 2, len) + 2;
	ssize_t n;
	data = slip_buf;

	while (nleft) {
		n = write(btsocket, data, nleft);
		if (n < 0 && errno != EINTR) {
			return -errno;
		}
		if (n > 0) {
			data += n;
			nleft -= n;
		}
	}

	return 0;
}


int ariane_bt_connect(const char *btaddr, ariane_sensordev_t **dev) {
	assert(dev && btaddr);

	struct sockaddr_rc addr = {.rc_family = AF_BLUETOOTH, .rc_channel = 1};
	if (str2ba(btaddr, &addr.rc_bdaddr))	// i have no idea what str2ba returns..
		return -EINVAL;

	int btsocket = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
    if (btsocket < 0) {
		return -errno;
	}

	int optval = 1;

	if (setsockopt(btsocket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval))) {
		int retval = -errno;
		close(btsocket);
		return retval;
	}

	if (connect(btsocket, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		int retval = -errno;
		close(btsocket);
		return retval;
	}

	int *driver_data = malloc(sizeof(int));
	if (!driver_data) {
		close(btsocket);
		return -errno;
	}
	*driver_data = btsocket;

	int err = ariane_create_sensordev(dev);
	if (err)
		return err;

	(*dev)->receiver = &receiver;
	(*dev)->transmit = &transmit;
	(*dev)->driver_data = driver_data;

	return ariane_connect(*dev);
}
