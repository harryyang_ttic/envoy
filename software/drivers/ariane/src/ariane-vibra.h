/**
  * @file ariane-vibra.h
  *
  * Linear vibration motor control for SensorBox and headset.
  *
  * This file is part of libariane
  *
  * Copyright (C) 2009-2010 Nokia Corporation and/or its subsidiary(-ies).
  *
  * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public License
  * version 2.1 as published by the Free Software Foundation.
  *
  * This library is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  * Lesser General Public License for more details.
  *
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
  * 02110-1301 USA
*/

#ifndef __ARIANE_VIBRA_H
#define __ARIANE_VIBRA_H

#include "ariane.h"

/** Haptic actuator type definition. It's represented as a sensor at lower levels. */
typedef ariane_sensor_t * ariane_vibra_t;

/**
  * Opens haptic actuator control.
  *
  * @param dev Sensor device to use.
  * @param session Session identifier. Use zero for automatic allocation. See #ariane_sensordev_open_sensor.
  * @param handle Actuator handle. Set before returning if operation was successful.
  * @param num Actuator number. Currently this is always zero because low level SW supports
  *            only one actuator. HW can drive two actuators.
*/
int ariane_vibra_open(ariane_sensordev_t *dev, ariane_session_id_t session, int num, ariane_vibra_t *handle);

/**
  * Close haptic actuator control.
  *
  * @param handle Actuator handle from #ariane_vibra_open call.
  * @return Error code.
*/
int ariane_vibra_close(ariane_vibra_t handle);

/**
  * Sets driving signal frequency.
  *
  * @param handle Actuator handle from #ariane_vibra_open call.
  * @param freq Frequency.
  * @return Error code.
*/
int ariane_vibra_set_freq(ariane_vibra_t handle, unsigned int freq);

/**
  * Get current driving frequency.
  *
  * @param handle Actuator handle from #ariane_vibra_open call.
  * @return Frequency. If an error occured negative value (error code) is returned.
*/
int ariane_vibra_get_freq(ariane_vibra_t handle);

/**
  * Start driving actuator.
  *
  * @param handle Actuator handle from #ariane_vibra_open call.
  * @return Error code.
*/
int ariane_vibra_start(ariane_vibra_t handle);

/**
  * Stop driving actuator.
  *
  * @param handle Actuator handle from #ariane_vibra_open call.
  * @return Error code.
*/
int ariane_vibra_stop(ariane_vibra_t handle);

#if 0
/*
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	Everything below this comment is a proposal for more advanced interface
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/

/** Vibration pattern structure. TBD */
struct ariane_vibra_pattern_struct;

/** Pattern structure type definition.*/
typedef struct ariane_vibra_pattern_struct ariane_vibra_pattern_t;

#define ARIANE_VIBRA_PATTERN_SLOTS		8		/**< Number of vibration pattern storage slots. */
#define ARIANE_VIBRA_PATTERN_SLOT_SIZE	32		/**< Size of vibration pattern slot (in bytes). */

/**
  *
  * @param slot Storage slot. TBD
  * @param pattern Vibration pattern.
  * @return Error code.
*/
int ariane_vibra_upload_pattern(ariane_vibra_t handle, unsigned int slot, ariane_vibra_pattern_t *pattern);

/**
  * "Plays" vibration pattern that was uploaded using #ariane_vibra_upload_pattern.
  *
  * @param slot Number of pattern storage slot. 
  * @return Error code.
*/
int ariane_vibra_play_pattern(ariane_vibra_t handle, unsigned int slot);

#endif

#endif
