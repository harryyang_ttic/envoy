/*
 * maccel.c
 *
 * Simple example for reading raw accelerometer values from several
 * SensorBoxes.
 *
 * This file is part of libariane
 *
 * Copyright (C) 2009-2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 *
 *	Compiling in Linux systems:
 *		gcc -std=gnu99 -lariane maccel.c -o maccel
 *
 *		Note that libariane-dev package must be installed (otherwise the
 *		header files of the library are not found).
 *
 *		Tab width is 4.
 *
 *		This example is for libariane 0.1-4 (and newer).
 *
 */

#include <ariane/ariane.h>		/* libariane applications must include ariane.h */

#include <stdio.h>				/* printf, fprintf */
#include <stdlib.h>				/* exit */
#include <string.h>				/* strerror */

/*
	This function returns in case of an error. This is bad programming practice but in order to keep this
	example short this is used for error handling.
*/
static void check_error(const char *msg, int err);


int main(int argc, char *argv[]) {
	if (argc < 2) {
		fprintf(stderr, "Usage: %s btaddress1 [btaddress2] [btaddress3] [...]\n"
						"Bluetooth addresses format: 00:00:00	:00:00:00\n", argv[0]);
		return EXIT_FAILURE;
	}

	int ndevices = argc - 1;

	int err = ariane_init();
	check_error("ariane_init", err);

	err = ariane_init_event_queue(32);
	check_error("ariane_init_event_queue", err);

	ariane_sensordev_t *devs[ndevices];

	for (int i=0; i<ndevices; i++) {
		err = ariane_bt_connect(argv[i + 1], &devs[i]);
		check_error("ariane_bt_connect", err);
	}
	
	ariane_sensor_t *accelerometers[ndevices];

	for (int i=0; i<ndevices; i++) {
		err = ariane_sensordev_open_by_type(devs[i], 0, ARIANE_MTYPE_ACCELERATION, ARIANE_SENSOR_ACCESS_MODE_CONTROL, &accelerometers[i]);
		check_error("ariane_sensordev_open_by_type", err);

		if (err != ARIANE_SENSOR_ACCESS_MODE_CONTROL) {
			ariane_disconnect(devs[i]);
			fprintf(stderr, "Could not open sensor in control mode (already open?). SensorBox %s\n", argv[i + 1]);
			return EXIT_FAILURE;
		}
	}

	for (int i=0; i<ndevices; i++) {
		err = ariane_sensor_start(accelerometers[i], ariane_event_handler, NULL);
		check_error("ariane_sensor_start", err);
	}

	int n = 5000;
	printf("Sensor started. %d samples will be displayed before exiting.\n", n);
	while (n--) {
		/*
			When the first parameter (the boolean parameter) of ariane_get_event is true,
			the function waits until an event becomes available. If set to false the function
			does not return until
		*/
		ariane_event_t event;
		ariane_get_event(true, &event);
		
		if (event.datafmt == ARIANE_EVENT_DATAFMT_XYZ16) {
			int src = -1;
			if (event.sensor) {
				for (int i=0; i<ndevices; i++)
					if (accelerometers[i] == event.sensor) {
						src = i;
						break;
					}
			}
			printf("%d, %d, %d, %d [src %d]\n", event.timestamp, 
					(int)event.data.xyz.x, (int)event.data.xyz.y, (int)event.data.xyz.z, src);
		} else if (event.datafmt == ARIANE_EVENT_DATAFMT_ERROR) {
			fprintf(stderr, "Async error!\n");
			break;
		}
	}

	for (int i=0; i<ndevices; i++) {
		ariane_sensor_stop(accelerometers[i]);
		ariane_sensor_close(accelerometers[i]);
		ariane_disconnect(devs[i]);
	}
}

static void print_error(const char *s, int err) {
	err = -err;

	if (err >= ARIANE_ERROR_GENERIC) {
		fprintf(stderr, "%s returned error 0x%X\n", s, err);
	} else {
		fprintf(stderr, "%s returned error %d : %s\n", s, err, strerror(err));
	}
}

static void check_error(const char *s, int err) {
	if (err >= 0)
		return;

	print_error(s, err);

	exit(EXIT_FAILURE);
}
