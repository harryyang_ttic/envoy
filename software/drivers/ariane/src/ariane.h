/**
  * @file ariane.h
  *
  * Ariane sensor access library main header. Include this file in
  * applications that use libariane.
  *
  * This file is part of libariane
  *
  * Copyright (C) 2009-2010 Nokia Corporation and/or its subsidiary(-ies).
  *
  * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public License
  * version 2.1 as published by the Free Software Foundation.
  *
  * This library is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  * Lesser General Public License for more details.
  *
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
  * 02110-1301 USA
  *
  *
  * POSIX threads are used by the library. The function calls are thread
  * safe unless otherwise stated but there are some limitations:
  * 1. The number of threads performing requests should be limited to 
  * #ARIANE_MAX_CONCURRECT_REQUESTS.
  * 2. Several threads should not access the same sensor at the same time.
  * It is recommended to have only one thread accessing libariane.
  *
  * @mainpage Libariane Documentation
  *
  * @section Introduction
  *
  * Libariane is a C library for using Ariane sensor devices (SensorBox and add-on).
  * Sensor framework uses libariane for accessing sensor devices over USB and Bluetooth
  * links. Libariane can also be used by applications that don't utilize the sensor
  * framework.
  *
  * @section Architecture
  * 
  * Libariane is allows accessing sensor subsystem using simple C interface. It handles
  * the differencies between USB and BT, protocol coding and many details to keep
  * applications simpler.
  *
  * Subsystem is the firmware running in the sensor devices. Processing of sensor data
  * and even application features can be implemented in the subsystem. The subsystem
  * provides higher abstraction layer for accessing the sensors.
  *
  * See Ariane Wiki pages for more information.
  *
  * @section API
  *
  * ariane.h is the libariane API.
  * ariane-subsys.h is the subsystem interface definition that libariane uses. Consider
  * it to be a part of libariane API.
  * 
  * @section usage Using libariane
  *
  * ariane-test.c in src directory shows how many features of the subsystem can be used
  * from a C application.
  *
  * Application should call ariane_init() before calling any other functions. Sensor
  * device connection is established using ariane_usb_connect() or ariane_bt_connect().
  * Connection should always be closed using ariane_disconnect().
  *
  * Majority of functions relating to controlling sensors and their parameters are
  * almost identical to the subsystem lower level interface. Consider taking a look at
  * the sensor subsystem wiki page.
  * 
  * Sensor data can be received either using event delivery mechanism or by calling
  * ariane_get_event(). The latter method requires calling ariane_init_event_queue() to
  * initialize queue for events. If application implements event handler then no
  * queueing is done by libariane. Currently the sensor framework uses the event handler
  * method while ariane-test application uses ariane_get_event().
  *
  * Most of functions report errors by returning negative value that is from errno.h or
  * one of the libariane specific error codes (see #ARIANE_ERROR_USB and other definitions
  * in ariane.h). errno variable is not set.
  *
  * @note The subsystem does not (usually) respond to requests that query sensors and sensor 
  * parameters that don't exist. Expect long delays when querying non-existing things and 
  * ETIMEDOUT errors. The right way is to query what the subsystem supports.
  *
  * POSIX threads are used by the library. The function calls are thread
  * safe unless otherwise stated but there are some limitations:
  * -# The number of threads performing requests should be limited to #ARIANE_MAX_CONCURRECT_REQUESTS.
  * -# Several threads should not access the same sensor at the same time.
  * -# is recommended to have only one thread accessing libariane.
  *
  * @note Copying the structs returned by libariane may cause problems because
  * libariane uses pointer comparisons. For example, the sensor structure pointer 
  * should be seen as a "handle". So don't copy but pass references.  
  * 
  * @section build Building libariane 
  *
  * If code is from SVN then do first "autoreconf -s -i --force". The source packages 
  * usually have that step done already.
  *
  * For Debian based systems (including Maemo Linux) use
  * "dpkg-buildpackage -rfakeroot" to build the installation packages.
  *
  * Other Linux systems should use "./configure && make && make install"
  *
  * @section Introduction
*/

#ifndef __ARIANE_H
#define __ARIANE_H

#include <stdint.h>
#include <stdbool.h>

#include "ariane-subsys.h"		// Subsystem interface definitions

/** Sensor device structure. */
typedef struct ariane_sensordev ariane_sensordev_t;

/** Event data format identifiers. */
typedef enum {
	/** 16bit signed X, Y, Z sample format. See #ariane_event_xyz16_t. */
	ARIANE_EVENT_DATAFMT_XYZ16,

	/** 16bit unsigned int sample format (uint16_t).*/
	ARIANE_EVENT_DATAFMT_U16,

	/** 32bit unsigned int sample format (uint32_t).*/
	ARIANE_EVENT_DATAFMT_U32,

	/** Key event format. See #ariane_event_key_t. */
	ARIANE_EVENT_DATAFMT_KEY,

	/** Application specific event format. See #ariane_event_key_t. */
	ARIANE_EVENT_DATAFMT_APPLICATION,

	/** Asynchronous error data format (int16_t). */
	ARIANE_EVENT_DATAFMT_ERROR,

	/** Data format for "sensors" that do not produce data. */
	ARIANE_EVENT_DATAFMT_NONE
} ariane_event_datafmt_t;

/** Sensor structure. */
typedef struct {
	/** Sensor identifier (SID). */
	ariane_sid_t sid;

	/**
	  * Sensor device that this sensor belongs to. Necessary to identify sensors uniquely. 
    */
	ariane_sensordev_t *dev;

	/** Session identifier. */
	ariane_session_id_t session;

	/** Access mode. */
	ariane_access_mode_t access_mode;

	/** Data format that this sensor uses. */
	ariane_event_datafmt_t datafmt;
} ariane_sensor_t;

/** Filter structure. */
typedef struct {
	/** Filter identifier. */
	ariane_filter_id_t fid;

	/** Sensor device that this filter belongs to. Necessary to identify filters uniquely. */
	ariane_sensordev_t *dev;
} ariane_filter_t;

/** 3-axis sensor data format. */
typedef struct {
	/** X coordinate (heading if orientation sensor). */
	int16_t x;

	/** Y coordinate (roll if orientation sensor). */
	int16_t y;

	/** Z coordinate (pitch if orientation sensor). */
	int16_t z;
} ariane_event_xyz16_t;

/** Key press/release event. */
typedef struct {
	/** Key code. */
	uint16_t key;
	
	/** True if key was pressed. Press and release event can be the same event (both can be true). */
	bool pressed;

	/** True if key was released. */
	bool released;
} ariane_event_key_t;

/** Event structure. */
typedef struct {
	/** Timestamp. The subsystem uses 1kHz timer for time stamping. */
	uint32_t timestamp;

	/** Sensor that produced this event. */
	const ariane_sensor_t *sensor;

	/** Data format. */
	ariane_event_datafmt_t datafmt;
	
	/** Event data. */
	union {
		/** See #ARIANE_EVENT_DATAFMT_XYZ16. */
		ariane_event_xyz16_t xyz;

		/** Unsigned 16bit integer. */
		uint16_t u16;

		/** Unsigned 32bit integer. */
		uint32_t u32;

		/** See #ARIANE_EVENT_DATAFMT_KEY. */
		ariane_event_key_t key;

		/**
		  * See #ARIANE_EVENT_DATAFMT_APPLICATION. Actual size can be calculated using the sensor
		  * properties: (resolution_bits + 7)/8 * sample_channels (TODO describe more precisely).
		*/
		uint8_t app[ARIANE_MAX_EVENT_SIZE];
		
		/**
		  * Asynchronous error event. See #ARIANE_EVENT_DATAFMT_ERROR. After receiving an error the sensor 
		  * device connection should no longer be used (close and reconnect).
		*/
		int error;
	} data;
} ariane_event_t;

/**
 * Unit conversion data structure.
 * Physical value y is calculated y = (x - offset) * scale.
 * where x is sample value. The unit is usually SI unit (documented separately in sensor documentation).
 *
*/
typedef struct {
	/** Scaling factor. */
	float scale;
	/** Offset. */
	float offset;
} ariane_unit_conversion_t;

/** Sensor properties. */
typedef struct {
	/** Default sampling rate in Hz. */
	uint16_t sample_rate;
	/** Bits per sample. */
	uint8_t resolution_bits;
	/** Number of channels. For example, 3-axis accelerometer has 3 channels. */
	uint8_t sample_channels;
	/** Bit flags. See #ARIANE_SENSOR_FLAG_FILTER etc. */
	uint16_t flags;
	/** Unit conversion coefficients. */
	ariane_unit_conversion_t conversions[ARIANE_MAX_CHANNELS];
	/** Sensor data format. */
	ariane_event_datafmt_t datafmt;
	/**
	  * Name of sensor. Shorter than the sensor description, inteded to be used for identifying sensors. 
	  * This is valid only with subsystem v0.29+. Applications should check if this is an empty string.
	*/
	char name[ARIANE_MAX_SENSOR_NAME_LEN];
	/**
	  * Measurement range hints. If values are equal then the information is unavailable. Subsys v0.29+
	  * support this but all sensors are not required to provide this information.
	*/ 
	int32_t min_value, max_value;
	/**
	  * List of availabe data rates. Sensors can have more available data rates than fit in the list. Supported
	  * by subsystem v0.29+. This information is optional: sensor implementations may omit it. Applications should
	  * check that rate is not zero (zero indicates unused rate field).
	*/
	uint16_t rates[ARIANE_MAX_SENSOR_RATES];
} ariane_sensor_properties_t;

/** Sensor parameter structure. */
typedef struct {
	/** Parameter (variable) identifier. */
	uint16_t paramid;
	/** Smallest valid value. Useful only for parameters that have a range of valid values. */
	uint32_t min;
	/** Greatest valid value. Useful only for parameters that have a range of valid values. */
	uint32_t max;
	/** Bit flags. See #ARIANE_SENSOR_PARAM_FLAG_SIGNED etc. */
	uint16_t flags;
	/** Parameter description. */
	char description[ARIANE_MAX_SENSOR_DESCR_LEN];
} ariane_sensor_parameter_t;

/** 
  * Event receiver callback function prototype.
  *
  * Rules for event handlers:
  * 1. Closing sensor from event handler function has (or may have) unpredictable results.
  *
  * 2. Event handler must execute quickly because it is called from the thread that receives
  * data from sensor devices. (this could be changed but adding more threads without real need
  * isn't wise). Never call anything that could block.
  *
  * 3. Spurious events might be possible and the handler should't break because of that.
*/
typedef void (*ariane_event_handler_t)(const ariane_event_t *event, void *param);

/**
  * Error codes. High values to avoid conflict with those defined in errno.h. 
  * Note that negation of this values is used when returned by a function defined in this file.
*/
enum {
	/** Generic error without specific meaning that hopefully is never returned. */
	ARIANE_ERROR_GENERIC = 0x1000,

	/** 
      * Generic libusb error. Libusb seems to return error codes from errno.h but the documentation just says
	  * negative values are errors. When libariane returns USB error you can (usually) get the original error from
	  * libusb using usb_error_errno and usb_strerror.
    */
	ARIANE_ERROR_USB,

	/** Returned when opening USB device failed. See also #ARIANE_ERROR_USB. */
	ARIANE_ERROR_USB_OPEN,

	/**
      * Returned when accessing USB device failed (usb_set_configuration or usb_claim_interface).
	  * Typically this results from insufficient user rights. See also #ARIANE_ERROR_USB.
    */
	ARIANE_ERROR_USB_ACCESS
};

#ifdef __cplusplus
extern "C" {
#endif

/**
  * Initialization.
  *
  * @return If successful, zero is returned. Currently returns always 0.
*/
int ariane_init();

/**
  * Connect to a sensor device using USB.
  *
  * TODO allow specifying which of the devices should be used (if there are several). First version just
  * takes the first found device
  *
  * This is not thread-safe.
  *
  * @param dev The pointer is initialized with device instance if operation was successful.
  * @return Error code. If no supported USB devices are connected this returns -ENODEV.
*/
int ariane_usb_connect(ariane_sensordev_t **dev);

/**
  * Connect to a sensor device using Bluetooth.
  *
  * This is not thread-safe.
  *
  * @param addr Bluetooth address.
  * @param dev The pointer is initialized with device instance if operation was successful.
  * @return Error code.
*/
int ariane_bt_connect(const char *addr, ariane_sensordev_t **dev);

/**
  * Disconnects sensor device and releases related resources.
  *
  * This is not thread-safe. Before callling disconnect application must verify that its threads
  * are not accessing any libariane funcs (with the same sensor dev!)
  *
  * Call this also after fatal errors to release the device and related data structures.
  *
*/
int ariane_disconnect(ariane_sensordev_t *dev);

/**
  * Returns new session (=client) identifier. The session identifiers identify clients that
  * can claim ownership of sensors (open them in control mode).
  *
  * @return New sesion identifier.
*/
ariane_session_id_t ariane_get_session_id();

/**
  * Receives events and stores them in a queue that can be accessed using ariane_get_event().
  * This is for applications that are not event driven (and don't implement event handler).
  * Remember to call ariane_init_event_queue() during initialization.
*/
void ariane_event_handler(const ariane_event_t *event, void *param);

/**
  * Initializes event queue with specific size. The queue is automatically initialized but if
  * this allows setting size different from the default is needed.
  *
  * This is not thread-safe.
  *
  * @param nevents Number of events, the size of the queue. Minimum is 1.
  *
*/
int ariane_init_event_queue(unsigned int nevents);

/**
  * Releases resources allocated for event queue.
  *
  * This is not thread-safe and application must make sure that nothing is using the queue
  * before calling this.
*/
void ariane_release_event_queue();

/**
  * Returns event from the event queue. See ariane_event_handler() for more details.
  * Caller must release the memory allocated for the event.
  *
  * @param wait Wait for event (indefinitely).
  * @param event Sufficient space for the event (caller allocates).
  * @return Returns true if event was stored, false otherwise.
*/
bool ariane_get_event(bool wait, ariane_event_t *event);

/**
  * Retrieves list of sensor identifiers from sensor device.
  *
  * @param dev Sensor device.
  * @param sensors Sensor ID list (return value). Minimum size is #ARIANE_MAX_SENSORS * sizeof(ariane_sid_t).
  *                Caller allocates the memory.
  * @return Returns the number of sensors found or error code (negative value).
*/
int ariane_sensordev_get_sensors(ariane_sensordev_t *dev, ariane_sid_t *sensors);

/**
  * Returns list of available filters.
  *
  * @param dev Sensor device.
  * @param filters Filter list (return value). Minimum size is #ARIANE_MAX_FILTERS * sizeof(ariane_filter_id_t).
  *                Caller allocates the memory.
  * @return Returns the number of filters found or error code (negative value).
*/
int ariane_sensordev_get_filters(ariane_sensordev_t *dev, ariane_filter_id_t *filters);

/**
  * Returns filter descrption.
  *
  * @param dev Sensor device.
  * @param fid Filter identifier.
  * @param descr Filter description (return value). Minimum size is ARIANE_MAX_SENSOR_DESCR_LEN.
  *              Caller allocates the memory.
*/
int ariane_sensordev_get_filter_description(ariane_sensordev_t *dev, ariane_filter_id_t fid, char *descr);

/**
  * Retrieves list of dependencies: a sensor can depend on other sensors. The type of dependency depends on
  * sensor flags (see #ARIANE_SENSOR_FLAG_STATIC_DEPENDENCY).
  *
  * @param dev Sensor device.
  * @param sid Sensor ID. 
  * @param sids Dependency list (return value). Minimum size is #ARIANE_MAX_DEPENDENCIES.
  * @return Returns number of dependencies (SIDs) or error code (negative value).
*/
int ariane_sensordev_get_dependency_list(ariane_sensordev_t *dev, ariane_sid_t sid, ariane_sid_t *sids);

/**
  * This function is similar to #ariane_sensordev_get_dependency_list but also indirect dependencies
  * are included (i.e. If the sensor A depends on sensor B and sensor B depends on C then both C and B
  * are in the dependency list).
  *
  * @param dev Sensor device.
  * @param sid Sensor ID. 
  * @param sids Dependency list (return value). Minimum size is #ARIANE_MAX_DEPENDENCIES.
  * @return Returns number of dependencies (SIDs) or error code (negative value).
*/
int ariane_sensordev_get_all_dependencies(ariane_sensordev_t *dev, ariane_sid_t sid, ariane_sid_t *sids);

/**
  * Opens sensor with specified SID. It is recommended to get the sensor list and use the
  * sensors from it.
  *
  * @param dev Sensor device.
  * @param session Session identifier. Use zero (=automatic) in most of applications. If you need to care
  *                about ownership of sensors then use #ariane_get_session_id to allocate a session identifier.
  * @param sid Sensor ID. 
  * @param sensor Pointer to sensor pointer. Used for returning the sensor structure. The memory allocated for
  *               the sensor is released when the sensor is closed.
  * @param amode Requested access mode (#ARIANE_SENSOR_ACCESS_MODE_CONTROL, #ARIANE_SENSOR_ACCESS_MODE_MONITOR or
  *              #ARIANE_SENSOR_ACCESS_MODE_ANY).
  * @return Access mode if successful. Otherwise negative value (error code) is returned. Note that
  *         #ARIANE_SENSOR_ACCESS_MODE_DENIED is returned also if the specified sensor ID is unknown.
*/
int ariane_sensordev_open_sensor(ariane_sensordev_t *dev, ariane_session_id_t session, ariane_sid_t sid, ariane_access_mode_t amode, ariane_sensor_t **sensor);

/** Backwards compatibility. */
#define ariane_sensordev_open		ariane_sensordev_open_sensor

/**
  * Opens sensor with specified measurement type. This function queries the sensor list and tries to
  * open matching sensors.
  *
  * @param dev Sensor device.
  * @param session Session identifier. Use zero (=automatic) in most of applications. If you need to care
  *                about ownership of sensors then use #ariane_get_session_id to allocate a session identifier.
  * @param type Measurement type.
  * @param amode Requested access mode (#ARIANE_SENSOR_ACCESS_MODE_CONTROL, #ARIANE_SENSOR_ACCESS_MODE_MONITOR or
  *              #ARIANE_SENSOR_ACCESS_MODE_ANY).
  * @param sensor Sensor structure (return value). Released when the sensor is closed.
  * @return Access mode if successful. Otherwise negative value (error code) is returned. If no sensors with matching
  * type are found then -NOENT is returned. 
*/
int ariane_sensordev_open_by_type(ariane_sensordev_t *dev, ariane_session_id_t session, ariane_mtype_t type, ariane_access_mode_t amode, ariane_sensor_t **sensor);

/**
  * Instantiates filter. Filter is a code module that processes input from any sensor. The source sensor is
  * specified when filter is instantiated. Instantiated filter is a regular sensor. The instance is deleted
  * when it is closed.
  *
  * @param dev Sensor device.
  * @param fid Filter identifier. Use ariane_sensordev_get_filters() to find available filters.
  * @param source_sid Identifies the sensor that is to be filtered.
  * @param new_sid Sensor ID of the filtered sensor. This is used to access the filtered sensor.
  * @return Error code.
*/
int ariane_sensordev_instantiate_filter(ariane_sensordev_t *dev, ariane_filter_id_t fid, ariane_sid_t source_sid, ariane_sid_t *new_sid);

/**
  * Sends reset command to the device. This is NOT thread-safe. Actually this is unsafe in any
  * ways and should not be used without considering the side effects. Note that the reset takes 
  * some time and the subsystem should not be accessed for a moment.
  *
  * @param dev Sensor device.
  * @return Error code.
*/
int ariane_sensordev_reset(ariane_sensordev_t *dev);

/**
  * Get subsystem version number.
  *
  * @return Version number: minor in bits 0-7, major in bits 8-15. This function never fails.
*/
uint16_t ariane_sensordev_get_version(ariane_sensordev_t *dev);

/**
  * Get subsystem location information.
  *
  * @return Location.
*/
ariane_device_location_t ariane_sensordev_get_location(ariane_sensordev_t *dev);

/**
  * Returns properties of a sensor.
  * 
  * @param sensor Sensor.
  * @param props Properties. Caller allocates.
  * @return Error code. Note that some sensors don't have properties. In that case -ENOENT is returned.
*/
int ariane_sensor_get_properties(ariane_sensor_t *sensor, ariane_sensor_properties_t *props);

/**
  * Returns properties of a sensor.
  * 
  * @param dev Sensor device.
  * @param sid sensor identifier.
  * @param props Properties. Caller allocates.
  * @return Error code. Note that some sensors don't have properties. In that case -ENOENT is returned.
*/
int ariane_sensordev_get_sensor_properties(ariane_sensordev_t *dev, ariane_sid_t sid, ariane_sensor_properties_t *props);

/**
  * Returns sensor description.
  *
  * @param sensor Sensor.
  * @param descr Description. Caller allocates #ARIANE_MAX_SENSOR_DESCR_LEN bytes.
  * @return Error code.
*/
int ariane_sensor_get_description(ariane_sensor_t *sensor, char *descr);

/**
  * Returns sensor description.
  *
  * @param dev Sensor device.
  * @param sid sensor identifier.
  * @param descr Description. Caller allocates #ARIANE_MAX_SENSOR_DESCR_LEN bytes.
  * @return Error code.
*/
int ariane_sensordev_get_sensor_description(ariane_sensordev_t *dev, ariane_sid_t sid, char *descr);

/**
  * Returns available parameters identifiers.
  *
  * @param sensor Sensor.
  * @param pids Parameter identifiers (return value). Caller allocates space for #ARIANE_MAX_PARAMETERS.
  * @return Returns number of parameter IDs or an error code (negative value).
*/
int ariane_sensor_get_parameter_ids(ariane_sensor_t *sensor, ariane_paramid_t *pids);

/**
  * Returns available parameters identifiers.
  *
  * @param dev Sensor device.
  * @param sid sensor identifier.
  * @param pids Parameter identifiers (return value). Caller allocates space for #ARIANE_MAX_PARAMETERS.
  * @return Returns number of parameter IDs or an error code (negative value).
*/
int ariane_sensordev_get_sensor_parameter_ids(ariane_sensordev_t *dev, ariane_sid_t sid, ariane_paramid_t *pids);

/**
  * Returns parameter descriptions.
  *
  * @param sensor Sensor.
  * @param params Parameters (return value). libariane allocates the memory and caller must release it (it's a single memory block 
  * allocated using malloc).
  * @return Number of parameters  or an error code (negative value).
*/
int ariane_sensor_get_parameters(ariane_sensor_t *sensor, ariane_sensor_parameter_t **params);

/**
  * Returns parameter descriptions. If sensor is already opened use #ariane_sensor_get_parameters.
  *
  * @param dev Sensor device.
  * @param sid sensor identifier.
  * @param params Parameters (return value). libariane allocates the memory and caller must release it (it's a single memory block 
  * allocated using malloc).
  * @return Number of parameters  or an error code (negative value).
*/
int ariane_sensordev_get_sensor_parameters(ariane_sensordev_t *dev, ariane_sid_t sid, ariane_sensor_parameter_t **params);

/**
  * Returns description of a sensor parameter.
  *
  * @param sensor Sensor.
  * @param pid Parameter identifier.
  * @param param Pointer to parameter description.
  * @return Error code.
*/
int ariane_sensor_get_parameter_description(ariane_sensor_t *sensor, ariane_paramid_t pid, ariane_sensor_parameter_t *param);

/**
  * Returns description of a sensor parameter.
  *
  * @param dev Sensor device.
  * @param sid sensor identifier.
  * @param pid Parameter identifier.
  * @param param Pointer to parameter description.
  * @return Error code.
*/
int ariane_sensordev_get_sensor_parameter_description(ariane_sensordev_t *dev, ariane_sid_t sid, ariane_paramid_t pid, ariane_sensor_parameter_t *param);

/**
  * Sets sensor parameter value. Sensor must be opened in control mode.
  *
  * @param sensor Sensor.
  * @param pid Parameter identifier.
  * @param value New value to be set. After successful return this is set to the actual value of the parameter.
  * @return Error code.
*/
int ariane_sensor_set_parameter_value(ariane_sensor_t *sensor, ariane_paramid_t pid, uint32_t *value);

/**
  * Gets sensor parameter value from subsystem.
  *
  * @param sensor Sensor.
  * @param pid Parameter identifier.
  * @param value Parameter value (set before returning if successful).
  * @return Error code.
*/
int ariane_sensor_get_parameter_value(ariane_sensor_t *sensor, ariane_paramid_t pid, uint32_t *value);

/**
  * Gets sensor parameter value from subsystem.
  *
  * @param dev Sensor device.
  * @param sid sensor identifier.
  * @param pid Parameter identifier.
  * @param value Parameter value (set before returning if successful).
  * @return Error code.
*/
int ariane_sensordev_get_sensor_parameter_value(ariane_sensordev_t *dev, ariane_sid_t sid, ariane_paramid_t pid, uint32_t *value);

/**
  * Convenience function for checking binary block size.
  *
  * @param sensor Sensor.
  * @return Size of binary block or an error code.
*/
int ariane_sensor_get_binary_block_size(ariane_sensor_t *sensor);

/**
  * Convenience function for checking binary block size.
  *
  * @param dev Sensor device.
  * @param sid sensor identifier.
  * @return Size of binary block or an error code.
*/
int ariane_sensordev_get_sensor_binary_block_size(ariane_sensordev_t *dev, ariane_sid_t sid);

/**
  * Convenience function for setting (current) sampling rate.
  *
  * @param sensor Sensor.
  * @param rate Frequency (Hz).
  * @return Rate that was accepted by the subsystem or an error code (negative value).
*/
int ariane_sensor_set_sampling_rate(ariane_sensor_t *sensor, unsigned int rate);

/**
  * Writes specified number of bytes to binary block of a sensor, starting from the specified offset.
  *
  * Use #ariane_sensordev_get_sensor_binary_block_size or #ariane_sensor_get_binary_block_size to
  * determine size of binary block.
  *
  * @param sensor Sensor.
  * @param offset Offset in the subsystem binary block.
  * @param data Data to write.
  * @param len Number of bytes to write.
  * @return Returns zero if successful, otherwise negative value on error.
*/
int ariane_sensor_write_binary_block(ariane_sensor_t *sensor, unsigned int offset, const uint8_t *data, unsigned int len);

/**
  * Reads specified number of bytes from binary block, that is associated with a sensor in the subsystem, starting
  * from the specified offset.
  *
  * Use #ariane_sensordev_get_sensor_binary_block_size or #ariane_sensor_get_binary_block_size to
  * determine size of binary block.
  *
  * @param sensor Sensor.
  * @param offset Offset in the subsystem binary block.
  * @param data Data buffer.
  * @param len Number of bytes to read.
  * @return Returns zero if successful, otherwise negative value on error.
*/
int ariane_sensor_read_binary_block(ariane_sensor_t *sensor, unsigned int offset, uint8_t *data, unsigned int len);

/**
  * Starts sensor. 
  *
  * @param sensor Sensor to use. Must remain valid until the sensor is stopped.
  * @param event_handler Event handler function pointer. Must remain valid until the
  * 					 sensor is stopped.
  * @param handler_param This parameter is passed to the event handler function when it is called.
*/
int ariane_sensor_start(ariane_sensor_t *sensor, ariane_event_handler_t event_handler, void *handler_param);

/**
  * Stops sensor.
  *
  * @param sensor Sensor.
  * @return Error code.
*/
int ariane_sensor_stop(ariane_sensor_t *sensor);

/**
  * Closes sensor. This releases the allocated resources. If the sensor has been started it
  * is important to call ariane_sensor_stop() before closing the sensor.
  *
  * @param sensor Sensor.
  * @return Error code.
*/
int ariane_sensor_close(ariane_sensor_t *sensor);

/**
  * Returns data format that is associated with the specified measurement type. Some data formats
  * don't have simple mapping.
  *
  * @param mtype Measurement type.
  * @return Data format. #ARIANE_EVENT_DATAFMT_NONE is returned if mapping cannot be done.
*/
ariane_event_datafmt_t ariane_mtype_to_datafmt(ariane_mtype_t mtype);

/** 
  * Associates SID with it's properties. Used only by #ariane_sensordev_get_all_sensor_properties (see its  
  * documentation). 
*/
typedef struct {
	/** Sensor ID. */
	ariane_sid_t sid;	
	/** Properties. */
	ariane_sensor_properties_t props;
} ariane_sensor_property_item_t;

/**
  * Retrieves sensor properties of all sensors. Caller releases allocated memory.
  *
  * @param props Pointer is initialized to point to newly allocated property structure before returning.
  * @return Number of sensors. Negative value is returned if an error occured.
*/
int ariane_sensordev_get_all_sensor_properties(ariane_sensordev_t *dev, ariane_sensor_property_item_t **props);

#ifdef ARIANE_I2C_DEBUG

/*
	Undocumented features that you probably shouldn't use.
*/

int ariane_i2c_read(ariane_sensordev_t *dev, ariane_session_id_t session, uint8_t addr, uint8_t reg, uint8_t *data, unsigned int len);
int ariane_i2c_write(ariane_sensordev_t *dev, ariane_session_id_t session, uint8_t addr, uint8_t reg, uint8_t *data, unsigned int len);
int ariane_i2c_req(ariane_sensordev_t *dev, ariane_session_id_t session, uint8_t addr, uint8_t reg, uint8_t *data, unsigned int len);

#endif

#ifdef __cplusplus
} // extern "C"
#endif

#endif
