/*
 * This file is part of libariane
 *
 * slip.h
 *
 * Copyright (C) 2003-2006, 2009 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */	

#ifndef SLIP_H
#define SLIP_H

#include <stdint.h>

#define SLIP_FRAMING_OVERHEAD	2		/* bytes, max */

/*
	Special characters
*/
#define SLIP_END				192		/* 0300, end of packet */
#define SLIP_ESC				219		/* 0333, byte stuffing */
#define SLIP_ESC_END			220		/* 0334, END data byte */
#define SLIP_ESC_ESC			221		/* 0335, ESC data byte */

/*
	Flags for slip_esc()
	
	These are useful for constructing an encoded packet in parts.
*/
#define SLIP_NO_EXTRA_END		0x01	/* no extra end-of-packet char */
#define SLIP_NO_END				0x02	/* no end-of-packet char */

/*
	Inserts necessary escape characters to data.

	flags
		SLIP_NO_EXTRA_END, SLIP_NO_END
	src
		raw packet data
	dst
		encoded packet data
	len
		number of src bytes
*/
uint16_t slip_esc(unsigned int flags, const uint8_t *src, uint8_t *dst, unsigned int len);

#endif
