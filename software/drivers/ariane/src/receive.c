/*
 * receive.c
 *
 * Frame reception from subsystem.
 *
 * This file is part of libariane
 *
 * Copyright (C) 2009-2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include "internals.h"
#include "bytecoding.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>

static int decode_xyz16(uint8_t *data, unsigned int data_len, ariane_event_t *evt) {
	if (data_len < 6)
		return -1;

	evt->datafmt = ARIANE_EVENT_DATAFMT_XYZ16;
	evt->data.xyz.x = decode_uint16(data);
	evt->data.xyz.y = decode_uint16(data + 2);
	evt->data.xyz.z = decode_uint16(data + 4);
	
	return 6;
}

static int decode_u32(uint8_t *data, unsigned int data_len, ariane_event_t *evt) {
	if (data_len < 4)
		return -1;

	evt->datafmt = ARIANE_EVENT_DATAFMT_U32;
	evt->data.u32 = decode_uint32(data);

	return 4;
}

static int decode_u16(uint8_t *data, unsigned int data_len, ariane_event_t *evt) {
	if (data_len < 2)
		return -1;

	evt->datafmt = ARIANE_EVENT_DATAFMT_U16;
	evt->data.u16 = decode_uint16(data);

	return 2;
}

static int decode_keycode(uint8_t *data, unsigned int data_len, ariane_event_t *evt) {
	if (data_len < 2)
		return -1;

	evt->datafmt = ARIANE_EVENT_DATAFMT_KEY;

	evt->data.key.pressed = *data & ARIANE_KEYFLAG_PRESSED ? true : false;
	evt->data.key.released = *data & ARIANE_KEYFLAG_RELEASED ? true : false;
	evt->data.key.key = data[1];

	return 2;
}

static int decode_app(uint8_t *data, unsigned int data_len, ariane_event_t *evt) {
	if (data_len < 1)
		return -1;

	unsigned int len = *data++;

	if (data_len < len + 1)
		return -1;

	evt->datafmt = ARIANE_EVENT_DATAFMT_APPLICATION;

	memcpy(evt->data.app, data, len);

	return len + 1;
}

typedef struct {
	unsigned int mtype;
	int (*decode)(uint8_t *data, unsigned int data_len, ariane_event_t *evt);
} decoder_t;

static const decoder_t decoders[] = {
        [ARIANE_MTYPE_ACCELERATION] =
        {ARIANE_MTYPE_ACCELERATION, decode_xyz16},
        [ARIANE_MTYPE_ANGULAR_RATE] =
        {ARIANE_MTYPE_ANGULAR_RATE, decode_xyz16},
        [ARIANE_MTYPE_PRESSURE] =
        {ARIANE_MTYPE_PRESSURE, decode_u32},
        [ARIANE_MTYPE_TEMPERATURE] =
        {ARIANE_MTYPE_TEMPERATURE, decode_u16},
        [ARIANE_MTYPE_MAGNETIC_FIELD] =
        {ARIANE_MTYPE_MAGNETIC_FIELD, decode_xyz16},
        [ARIANE_MTYPE_VOLTAGE] =
        {ARIANE_MTYPE_VOLTAGE, NULL},
        [ARIANE_MTYPE_TESTPATTERN] =
        {ARIANE_MTYPE_TESTPATTERN, decode_u16},
        [ARIANE_MTYPE_KEYCODE] =
        {ARIANE_MTYPE_KEYCODE, decode_keycode},
        [ARIANE_MTYPE_ORIENTATION] =
        {ARIANE_MTYPE_ORIENTATION, decode_xyz16},
		[ARIANE_MTYPE_APPLICATION] =
		{ARIANE_MTYPE_APPLICATION, decode_app}
};

void ariane_receive(ariane_sensordev_t *dev, uint8_t *data, unsigned int data_len) {
	assert(dev && data);
	
	if (data_len < ARIANE_MIN_MESSAGE_LEN) {
		// when debugging putting breakpoint here is a good idea
		return;
	}

	unsigned int idx = 0;

	ariane_rsp_t rsp = decode_uint16(data + idx);
	idx += 2;

	ariane_sid_t sid = decode_uint16(data + idx);
	idx += 2;

	ariane_mtype_t mtype = ARIANE_SID_GET_MTYPE(sid);
	
	dev->location = ARIANE_SID_GET_LOCATION(sid);

	if (rsp == ARIANE_RSP_SENSOR_DATA) {
		if (data_len - idx <= 4)
			return;
			
		dev->event.timestamp = decode_uint32(data + idx);
		idx += 4;

		assert(mtype <= ARIANE_MTYPE_APPLICATION && decoders[mtype].decode);

		int len = decoders[mtype].decode(data + idx, data_len - idx, &dev->event);
#ifdef NDEBUG
		if (len < 0)	// something went wrong but it could be just a corrupted data frame (which shouldn't happen)
			return;
#else
		assert(idx >= 0);
#endif
		idx += len;

		int err = ariane_deliver_event(dev, sid);
		assert(err >= 0);

		// Several data indication messages can be concatenated to single frame
		if (idx < data_len)
			ariane_receive(dev, data + idx, data_len - idx);

		return;
	}

	uint16_t session = decode_uint16(data + idx);
	idx += 2;

	// point to payload
	data = data + idx;
	data_len -= idx;

	pthread_mutex_lock(&dev->req_mutex);

	int rep_idx = -1;

	if (session < ARIANE_SESSIONID_FIRST + ARIANE_MAX_CONCURRECT_REQUESTS)
		rep_idx = session - ARIANE_SESSIONID_FIRST;
	else {
		for (int i=0, j=1; i<ARIANE_MAX_CONCURRECT_REQUESTS; i++, j <<= 1) {
			if ((dev->req_alloc_map & j) && dev->replies[i].session == session) {			
				rep_idx = i;
				break;
			}
		}
	}

	if (rep_idx < 0) {
		pthread_mutex_unlock(&dev->req_mutex);
#ifndef NDEBUG
		fprintf(stderr, "Unexpected msg %u sid 0x%X session %u\n", rsp, sid, session);
#endif
		return;
	}

	dev->replies[rep_idx].done = true;
	dev->replies[rep_idx].rsp = rsp;
	dev->replies[rep_idx].sid = sid;
	dev->replies[rep_idx].payload_len = data_len;
	memcpy((void *)&dev->replies[rep_idx].payload, data, data_len);
	
	pthread_cond_signal(&dev->req_cond[rep_idx]);

	pthread_mutex_unlock(&dev->req_mutex);
}
