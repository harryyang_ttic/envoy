/*
 * temp.c
 *
 * Simple temperature monitor SensorBox. Note that the temperature inside the
 * covers is significantly higher than real temperature of environment.
 *
 * This file is part of libariane
 *
 * Copyright (C) 2009-2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 *
 *	Compiling in Linux systems:
 *		gcc -std=gnu99 -lariane temp.c -o temp
 *
 *		Note that libariane-dev package must be installed (otherwise the
 *		header files of the library are not found).
 *
 *		Tab width is 4.
 *
 *		This example is for libariane 0.1-4 (and newer).
 *
 */

#include <ariane/ariane.h>		/* libariane applications must include ariane.h */

#include <stdio.h>				/* printf, fprintf */
#include <stdlib.h>				/* exit */
#include <string.h>				/* strerror */
#include <signal.h>

#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
#include <lcmtypes/senlcm_pressure_msg_t.h>

static void check_error(const char *msg, int err);

static ariane_sensordev_t *dev = NULL;	// sensor device instance (single SensorBox / add-on / headset)

static volatile bool sigintcaught = false;

static void siginthandler(int s) {
	sigintcaught = true;
}

int main(int argc, char *argv[]) {
	int err = ariane_init();
	check_error("ariane_init", err);

	err = ariane_init_event_queue(4);
	check_error("ariane_init_event_queue", err);

	if (argc <= 1) {
		err = ariane_usb_connect(&dev);
		check_error("ariane_usb_connect", err);
	} else {
		err = ariane_bt_connect(argv[1], &dev);
		check_error("ariane_bt_connect", err);	
	}

	ariane_sensor_t *temp = NULL, *baro = NULL;

	// 0x311 is the temperature sensor of the barometer module.
	err = ariane_sensordev_open_sensor(dev, 0, 0x311, ARIANE_SENSOR_ACCESS_MODE_CONTROL, &temp);
	if (err == ARIANE_SENSOR_ACCESS_MODE_CONTROL) {
		err = ariane_sensordev_open_sensor(dev, 0, 0x210, ARIANE_SENSOR_ACCESS_MODE_CONTROL, &baro);
	}
	
	check_error("ariane_sensordev_open_sensor", err);

	if (err != ARIANE_SENSOR_ACCESS_MODE_CONTROL) {
		ariane_disconnect(dev);
		fprintf(stderr, "Could not open sensor in control mode (already open?).");
		return EXIT_FAILURE;
	}

	ariane_sensor_properties_t temp_props;
	err = ariane_sensor_get_properties(temp, &temp_props);
	check_error("ariane_sensor_get_properties", err);

	ariane_sensor_properties_t baro_props;
	err = ariane_sensor_get_properties(baro, &baro_props);
	check_error("ariane_sensor_get_properties", err);

	err = ariane_sensor_start(temp, ariane_event_handler, NULL);
	check_error("ariane_sensor_start", err);

	err = ariane_sensor_start(baro, ariane_event_handler, NULL);
	check_error("ariane_sensor_start", err);

	if (signal(SIGINT, siginthandler) == SIG_ERR) {
		perror("signal");
		ariane_disconnect(dev);
		return EXIT_FAILURE;
	}

	lcm_t *lcm = bot_lcm_get_global(NULL);//globals_get_lcm();

	while (!sigintcaught) {
		ariane_event_t event;
		ariane_get_event(true, &event);
		
		if (event.datafmt == ARIANE_EVENT_DATAFMT_U16) {
			float t = (event.data.u16 - temp_props.conversions[0].offset) * temp_props.conversions[0].scale;
		
			printf("Temperature %.1f deg C [raw %d]\n", t, event.data.u16);
		} else if (event.datafmt == ARIANE_EVENT_DATAFMT_U32) {
			float p = (event.data.u32 - baro_props.conversions[0].offset) * baro_props.conversions[0].scale;
			
			printf("Pressure %.1f Pa [raw %d]\n", p, event.data.u32);

			senlcm_pressure_msg_t msg;
			msg.utime = bot_timestamp_now();
			msg.pressure = p;
			senlcm_pressure_msg_t_publish(lcm, "PRESSURE_STAT", &msg);

		} else if (event.datafmt == ARIANE_EVENT_DATAFMT_ERROR) {
			fprintf(stderr, "Async error!\n");
			break;
		}
	}

	ariane_sensor_stop(temp);
	ariane_sensor_stop(baro);
	ariane_sensor_close(temp);
	ariane_sensor_close(baro);
	ariane_disconnect(dev);
	
	fprintf(stderr,"Exiting\n");

	return EXIT_SUCCESS;
}

static void print_error(const char *s, int err) {
	err = -err;

	if (err >= ARIANE_ERROR_GENERIC) {
		fprintf(stderr, "%s returned error 0x%X\n", s, err);
	} else {
		fprintf(stderr, "%s returned error %d : %s\n", s, err, strerror(err));
	}
}

static void check_error(const char *s, int err) {
	if (err >= 0)
		return;

	print_error(s, err);

	if (dev)
		ariane_disconnect(dev);

	exit(EXIT_FAILURE);
}
