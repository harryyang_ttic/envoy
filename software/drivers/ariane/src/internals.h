/**
  * @file internals.h
  *
  * Internal header file. Put here things that clients shouldn't see.
  *
  * This file is part of libariane
  *
  * Copyright (C) 2009-2010 Nokia Corporation and/or its subsidiary(-ies).
  *
  * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public License
  * version 2.1 as published by the Free Software Foundation.
  *
  * This library is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  * Lesser General Public License for more details.
  *
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
  * 02110-1301 USA
*/

#ifndef __INTERNALS_H
#define __INTERNALS_H

#include "ariane.h"
#include <pthread.h>

/** Timeout in seconds for most of situations. */
#define ARIANE_TIMEOUT			1

/** Timeout (in milliseconds) for USB receiver. */
#define ARIANE_TIMEOUT_USB		500

/** Device state type. */
typedef enum {
	/** Device started. */
	ARIANE_DEVICE_STATE_READY,

	/** Communication with device terminated due to a error. */
	ARIANE_DEVICE_STATE_ERROR,

	/** Shut(ting) down. */
	ARIANE_DEVICE_STATE_SHUTDOWN
} ariane_sensordev_state_t;

/** Structure for decoded replies. */
typedef struct {
	/** Set to false when request is made, true when response is received. */
	bool done;

	/** Session (=client) ID. This is set when REQUEST IS MADE. */
	uint16_t session;

	/** Response code. */
	ariane_rsp_t rsp;

	/** SID. */
	ariane_sid_t sid;

	/** Length of payload. */
	unsigned int payload_len;

	/** Payload. */
	uint8_t payload[ARIANE_MAX_PAYLOAD_LEN];
} ariane_reply_t; 

/** Sensor device structure. */
struct ariane_sensordev {
	/** State of this device. */
	volatile ariane_sensordev_state_t state;

	/** Data receiving. Strange prototype because this is passed to pthread_create. dev is is ariane_sensordev_t pointer. */
	void (*receiver)(ariane_sensordev_t *dev);

	/** Transmits single protocol frame. */
	int (*transmit)(ariane_sensordev_t *dev, uint8_t *data, unsigned int len);

	/** Receiver thread. ariane_connect() creates the thread. */
	pthread_t thread;
	
	/**
      * Subsystem version. ariane_connect() initializes this. Receiver implementations should not use this because
      * this is initialized after starting receiver thread.
    */
	uint16_t version;
	
	/** Device location.  */
	ariane_device_location_t location;
	
	/** Request mutex. Protects the request/reply related variables in sensor device structure. */
	pthread_mutex_t req_mutex;
	
	/** Request condition variables. Receiver wakes up waiting thread. */
	pthread_cond_t req_cond[ARIANE_MAX_CONCURRECT_REQUESTS];
	
	/** Request allocation map. */
	volatile unsigned int req_alloc_map;

	/** Replies to requests. Some variables in the structs are initialized when request is made. */
	volatile ariane_reply_t replies[ARIANE_MAX_CONCURRECT_REQUESTS];
	
	/** Event that is being delivered. This is here to avoid the need to allocate memory in event delivery. */
	ariane_event_t event;

	/** Link "driver" data. */
	void *driver_data;
};

/** Macro for printing message when unimplemented function is used. */
#define TODO		fprintf(stderr, "%s in %s is incomplete!\n", __FUNCTION__, __FILE__);

/**
  * Non-link specific connection establishment. This creates the receiver thread
  * and checks subsystem version.
  * 
  * @param dev Sensor device structure. See ariane_create_sensordev().
*/
int ariane_connect(ariane_sensordev_t *dev);

/**
  * Receive data frame. Link "drivers" call this.
  *
  * @param dev Sensor device that produces the data.
  * @param data Protocol data frame.
  * @param data_len Length of data frame.
*/
void ariane_receive(ariane_sensordev_t *dev, uint8_t *data, unsigned int data_len);

/**
  * Create sensordev structure and initializes some of the variables.
  *
  * @param dev Pointer to a pointer..
  * @return Error code.
*/
int ariane_create_sensordev(ariane_sensordev_t **dev);

/**
  * Registers listener. #ariane_sensor_close calls this.
  *
  * @param sensor Sensor.
  * @param handler Event handler function.
  * @param handler_param Parameter that is passed to the event handler function.
  * @return Error code.
*/
int ariane_register_listener(ariane_sensor_t *sensor, ariane_event_handler_t handler, void *handler_param);

/**
  * Unregisters listener. #ariane_sensor_close calls this.
  *
  * @param sensor Sensor.
  * @return Error code.
*/
int ariane_unregister_listener(ariane_sensor_t *sensor);

/**
  * Delivers event. The event is in the device structure to avoid allocating memory (ugly but
  * efficient). The receiver thread doesn't know about open sensors so this function also
  * sets the sensor field in event structure.
  *
  * @param dev Sensor device that produced the event.
  * @param sid ID of source sensor.
  * @return Error code.
*/
int ariane_deliver_event(ariane_sensordev_t *dev, ariane_sid_t sid);

/**
  * Delivers error event to all listeners that listen to sensors from the specified device.
  * ariane_async_error() should usually be used when an asynchronous error occurs.
  *
  * @param dev Sensor device that malfunctioned.
  * @param event Initialized event structure. Sensor field may be NULL (it's set when listener
  * is called).
*/
void ariane_deliver_error(ariane_sensordev_t *dev, ariane_event_t *event);

/**
  * This is called when an error occurs in receiver thread. Device state is set to error state
  * and ariane_deliver_error() is called.
  *
  * @param dev Sensor device.
  * @param err Error code.
*/
void ariane_async_error(ariane_sensordev_t *dev, int err);

/**
  * Sends request to subsystem and waits for reply. Consider using #ariane_request_simple or
  * #ariane_request_sensor because they are easier to use.
  *
  * @param session Session ID, use 0 for automatic.
  * @param sid Sensor ID.
  * @param req Request type.
  * @param data Request data (payload).
  * @param data_len Length of request payload.
  * @param rep_data Reply payload. The buffer must be enough large for reply payload (max #ARIANE_MAX_PAYLOAD_LEN bytes).
  * @return If successful the number of bytes in reply is returned. On error, negative value is returned.
  *
*/
int ariane_request(ariane_sensordev_t *dev, ariane_session_id_t session, 
				   ariane_sid_t sid, ariane_req_t req, 
				   uint8_t *data, unsigned int data_len, 
				   uint8_t *rep_data);

/**
  * Calls #ariane_request.
*/
int ariane_request_sensor(const ariane_sensor_t *sensor, ariane_req_t req,  uint8_t *data, unsigned int data_len, uint8_t *rep_data);

/**
  * Calls #ariane_request using #ARIANE_SID_SYS as sensor ID.
*/
int ariane_request_simple(ariane_sensordev_t *dev, ariane_req_t req, uint8_t *data, unsigned int data_len, uint8_t *rep_data);


/**
  * Get subsystem version number (really).
  *
  * @return Version number: minor in bits 0-7, major in bits 8-15. If an error occured, negative value (error code) is returned.
*/
int ariane_sensordev_get_version_real(ariane_sensordev_t *dev);

#endif
