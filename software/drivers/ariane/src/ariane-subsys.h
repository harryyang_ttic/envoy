/**
  * @file ariane-subsys.h
  *
  * Sensor subsystem interface definitions.
  *
  * This file is part of libariane
  *
  * Copyright (C) 2009-2010 Nokia Corporation and/or its subsidiary(-ies).
  *
  * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public License
  * version 2.1 as published by the Free Software Foundation.
  *
  * This library is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  * Lesser General Public License for more details.
  *
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
  * 02110-1301 USA
  *
  * uint16_t is decoded like x = (p[0]<<8) | p[1];
  * uint32_t is decoded like x = (p[0]<<24) | (p[1]<<16) | (p[2]<<8) | p[3];
  * strings are ASCII valued with \0 after last character. Extra characters
  * after the zero are allowed.
  *
  * Message type is 16 bit uint16_t, the first field of all protocol messages.
  *
  * Sensor Identifier (SID) is a 16 bit uint16_t, the second field of all protocol messages.
  * SID contains following bit-fields:
  *
  * measurement_type = 0xff & (sid>>8);    enumbered below
  * location = (sid>>4) & 0x0f;            enumbered below
  * num = sid & 0x0f;                      sensor number in location & type 0,1,2,
  *
  * session_id is 16 bit integer and third field of all protocol messages (except SENSOR_DATA_IND)
  * which is used to identify client of sensor access
  * sensor subsystem returns same session_id in response message (*_rsp or *_nak)
  *
  * Reason codes as in errno.h when possible (meaning codes 0-32, in Linux often 
  * /usr/include/asm-generic/errno-base.h).
  * 
*/

#ifndef __ARIANE_SUBSYS_H
#define __ARIANE_SUBSYS_H

#include <stdint.h>		/* ISO C99 integer types */


/** Maximum payload size in bytes. See #ARIANE_MAX_MESSAGE_LEN. */
#define ARIANE_MAX_PAYLOAD_LEN		58

/** Maximum message length (payload + 6-byte header). */
#define ARIANE_MAX_MESSAGE_LEN		(ARIANE_MAX_PAYLOAD_LEN + 6)

/** 
  * Minimum message length. Mininum message is a 6-byte header or a data indication 
  * message (4-byte header + 2 byte data)
*/
#define ARIANE_MIN_MESSAGE_LEN		6

/** Maximum number of sensors. */
#define ARIANE_MAX_SENSORS			(ARIANE_MAX_PAYLOAD_LEN / 2)

/** Maximum number of filters. */
#define ARIANE_MAX_FILTERS			ARIANE_MAX_SENSORS

/** Maximum number of dependencies that a sensor can have. */
#define ARIANE_MAX_DEPENDENCIES		(ARIANE_MAX_SENSORS / 2)

/** Maximum number of parameters that a single sensor can have. */
#define ARIANE_MAX_PARAMETERS		ARIANE_MAX_SENSORS

/** Maximum length of sensor, sensor parameter and filter description. */
#define ARIANE_MAX_SENSOR_DESCR_LEN	32

/** Maximum length of sensor, sensor parameter and filter description. */
#define ARIANE_MAX_SENSOR_NAME_LEN	16

/** Maximum mumber of items in sampling rate list. */
#define ARIANE_MAX_SENSOR_RATES		4

/**
  * Maximum data block length for a single data block request (##ARIANE_REQ_SENSOR_READ_BINARY_BLOCK and 
  * #ARIANE_REQ_SENSOR_WRITE_BINARY_BLOCK. The total size of a data area in the subsystem is not limited.
*/
#define ARIANE_MAX_DATA_BLOCK_LEN	48

/**
  * Maximum number of concurrect requests. The host should never send more requests
  * before waiting for a response.
*/
#define ARIANE_MAX_CONCURRECT_REQUESTS	4

/** Maximum number of channels for a sensor. */
#define ARIANE_MAX_CHANNELS			3

/** Maximum size of single event in bytes. */
#define ARIANE_MAX_EVENT_SIZE		(ARIANE_MAX_CHANNELS * 4)

/** USB Vendor ID. */
#define ARIANE_USB_VENDORID			0x421

/** USB Product ID. */
#define ARIANE_USB_PRODUCTID		0xADD

/** USB endpoint IN. */
#define ARIANE_USB_EP_IN				1

/** USB endpoint OUT. */
#define ARIANE_USB_EP_OUT				2

/** Lowest protocol session (=client) ID. Values lower than this must not be used.*/
#define ARIANE_SESSIONID_FIRST		1

/** Highest protocol session (=client) ID. Values greater than this must not be used. */
#define ARIANE_SESSIONID_LAST		0x7FFF

/**
  * Type for request identifiers. Message numbers from host to sensor unit is 16 bits in protocol.
  * ARIANE_REQ_SENSOR_* requests relate to a sensor. ARIANE_REQ_SYSTEM requests target entire system.
*/ 
typedef enum {
	/** Open sensor request. */
	ARIANE_REQ_SENSOR_OPEN,
	/** Close sensor request. */	
	ARIANE_REQ_SENSOR_CLOSE,	
	/** Start sending sensor data. */	
	ARIANE_REQ_SENSOR_START, 
	/** Stop sending sensor data */
	ARIANE_REQ_SENSOR_STOP, 
	/** Get list of available sensor (SID) in device. */
	ARIANE_REQ_SYSTEM_GET_SENSORS, 
	/** get properties of a single sensor */
	ARIANE_REQ_SENSOR_GET_PROPERTIES, 
	/** Get human-readable description of a single sensor */
	ARIANE_REQ_SENSOR_GET_DESCRIPTION, 
	/** Get sensor parameters */
	ARIANE_REQ_SENSOR_GET_PARAMETERS, 
	/** Set sensor parameter */
	ARIANE_REQ_SENSOR_SET_PARAMETER,
	/** Get sensor parameter */
	ARIANE_REQ_SENSOR_GET_PARAMETER,
	/** Get sensor parameter description. */
	ARIANE_REQ_SENSOR_GET_PARAMETER_DESCRIPTION,
	/** Read configuration / setup tables */
	ARIANE_REQ_SENSOR_READ_BINARY_BLOCK,
	/** Write configuration / setup tables */
	ARIANE_REQ_SENSOR_WRITE_BINARY_BLOCK,
	/**
	  * Get list of sensors that the filter depends on or "sibbling" sensors
	  * when a sensor shares HW with another sensor.
	*/
	ARIANE_REQ_SENSOR_GET_DEPENDENCY_LIST,
	/** Get list of available filters request. */
	ARIANE_REQ_SYSTEM_GET_FILTERS,
	/** Instantiate filter request. */
	ARIANE_REQ_SYSTEM_INSTANTIATE_FILTER,
	/** Reset subsystem request. */
	ARIANE_REQ_SYSTEM_RESET,
	/** Get filter description. */
	ARIANE_REQ_SYSTEM_GET_FILTER_DESCRIPTION,
	/** Start sensor with response (#ARIANE_RSP_SENSOR_START). Only in subsys v0.10+ */
	ARIANE_REQ_SENSOR_START2,
	/** I2C read/write request. This was added for test/debug purposes in v0.27. */
	ARIANE_REQ_I2C,
	/** Get extended sensor properties request. V0.29+ */
	ARIANE_REQ_SENSOR_GET_EX_PROPERTIES
} ariane_req_t;

/** Response codes. There are historical reasons for having different codes for responses and replies.. */ 
typedef enum {
	/** Open sensor, response. */
	ARIANE_RSP_SENSOR_OPEN,
	/** return list of available sensor (SID) in device */
	ARIANE_RSP_SYSTEM_GET_SENSORS,
	/** Start sensor request rejected. This is a response to both ##ARIANE_REQ_SENSOR_START2 and
	  * #ARIANE_REQ_SENSOR_START, the latter responds only if the operation failed. 
	*/
	ARIANE_RSP_SENSOR_START,
	/** get properties of a single sensor */
	ARIANE_RSP_SENSOR_GET_PROPERTIES,
	/** Sensor sample data. Not really a response (unless considered to be a response to START req. */
	ARIANE_RSP_SENSOR_DATA,
	/** Get description of a single sensor */
	ARIANE_RSP_SENSOR_GET_DESCRIPTION,
	/** Get sensor parameters */
	ARIANE_RSP_SENSOR_GET_PARAMETERS,
	/** Set sensor parameter */
	ARIANE_RSP_SENSOR_SET_PARAMETER,
	/** Get sensor parameter */
	ARIANE_RSP_SENSOR_GET_PARAMETER,
	/** Get sensor parameter description. */
	ARIANE_RSP_SENSOR_GET_PARAMETER_DESCRIPTION,
	/** Read configuration / setup tables. */
	ARIANE_RSP_SENSOR_READ_BINARY_BLOCK,
	/** Write configuration / setup tables */
	ARIANE_RSP_SENSOR_WRITE_BINARY_BLOCK,
	/** Response contains list of SIDs that the filter depends on. */
	ARIANE_RSP_SENSOR_GET_DEPENDENCY_LIST,
	/** Response contains list of filter identifiers */
	ARIANE_RSP_SYSTEM_GET_FILTERS,
	/** Create filter instance */
	ARIANE_RSP_SYSTEM_INSTANTIATE_FILTER,
	/** Filter description. */
	ARIANE_RSP_SYSTEM_GET_FILTER_DESCRIPTION,
	/** I2C read/write response. This was added for test/debug purposes in v0.27. */
	ARIANE_RSP_I2C,
	/** Get extended sensor properties response. V0.29+ */
	ARIANE_RSP_SENSOR_GET_EX_PROPERTIES
} ariane_rsp_t;


/** Measurement types. 8 bits in protocol. */
typedef enum {
	/** Acceleration measurement. 16bit signed integer values. */
	ARIANE_MTYPE_ACCELERATION,
	/** Angular rate measurement. 16bit signed integer values(?) */
	ARIANE_MTYPE_ANGULAR_RATE,
	/** Air pressure measurement. */
	ARIANE_MTYPE_PRESSURE,
	/** Temperature measurement. */
	ARIANE_MTYPE_TEMPERATURE,
	/** Magnetic field measurement. */
	ARIANE_MTYPE_MAGNETIC_FIELD,
	/** Voltage measurement. */
	ARIANE_MTYPE_VOLTAGE,
	/** Test pattern event data format: 16bit unsigned integer. */
	ARIANE_MTYPE_TESTPATTERN,
	/** Key code "sensor". Data format: 16bit key code (TODO define them). */
	ARIANE_MTYPE_KEYCODE,
	/** Orientation measurement. Data format: 16bit signed integer values. */
	ARIANE_MTYPE_ORIENTATION,
	
	/**
	  * Application specific measurement. Used by algorithm modules that produce data in application
	  * specific format that is not supported by the subsystem / libariane / sensor framework. The
	  * first byte of data is the length of data (protocol level), applications use sensor properties
	  * to determine size.
	*/
	ARIANE_MTYPE_APPLICATION,
	
	/**
	  * Measurement type for SW modules that reuse the sensor interface for other purposes. 
	  * Starting and stopping sensors with this measurement type is meaningless and should not be
	  * done.
	  */
	ARIANE_MTYPE_SW_MEASUREMENT = 0x80,
	
	/**
	  * Measurement type for haptics module. Not a real sensor : it does not produce data.
	  * TODO define funtionality somewhere (=wiki?)
	  */
	ARIANE_MTYPE_HAPTIC,
		
	/**
	  * Measurement type for system settings module. Note that it is not a real sensor but
	  * allows querying system properties such as version number.
	  */
	ARIANE_MTYPE_SYSTEM
} ariane_mtype_t;

/**
  * Location types. In protocol 4bits are used for the location. The subsystem provides this
  * information but ignores the location bits in requests.
*/
typedef enum {
	/** Device integrated to a headset. */
	ARIANE_DEVICE_LOCATION_HEADSET,
	/** Wrist device (probably attached to left or right wrist) */
	ARIANE_DEVICE_LOCATION_WRIST,
	/** Location of host system (terminal device). */
	ARIANE_DEVICE_LOCATION_HOST,
	/** Undefined or unknown location. */
	ARIANE_DEVICE_LOCATION_ANY
} ariane_device_location_t;

/** Sensor identifier. Sensor number, location and measurement type are encoded in sid. */
typedef uint16_t ariane_sid_t;

/** Macro for constructing SIDs. */
#define ARIANE_SID(_num, _location, _type)	((_num & 0x0F) | ((_location & 0x0F) << 4) | (_type << 8))

/** SID of system sensor. Location field is set to #ARIANE_DEVICE_LOCATION_ANY so when comparing remember that
  * subsystem probably uses a different location field. In requests the subsystem ignores location. 
*/
#define ARIANE_SID_SYS						ARIANE_SID(0, ARIANE_DEVICE_LOCATION_ANY, ARIANE_MTYPE_SYSTEM)

/** Isolates number field of SID. */
#define ARIANE_SID_GET_NUM(_sid)       		(_sid & 0x0F)

/** Isolates measurement type field of SID. */
#define ARIANE_SID_GET_MTYPE(_sid)      	(ariane_mtype_t)(_sid >> 8)

/** Isolates location field of SID. */
#define ARIANE_SID_GET_LOCATION(_sid)      	(ariane_device_location_t)((_sid & 0xF0) >> 4)

/** Filter identifier type. TODO: enumeration for common types or something..? */
typedef uint16_t ariane_filter_id_t;

/** Session/client identifier type. */
typedef uint16_t ariane_session_id_t;

/** Sensor access mode. */
typedef enum {
	/** Access mode: control of sensor is allowed (setting properties etc). Only one client
	  * can open sensor in control mode.
	  */
	ARIANE_SENSOR_ACCESS_MODE_CONTROL,
	/** Access mode: monitor sensor. Sensor settings cannot be affected and sensor cannot
	  * be started or stopped really: the start subscribes to sensor data stream and stop
      * unsubscribes from it but the real start/stop is done only by the controlling client.. 
      * Essentially this is only useful when the sensor is already
	  * in use or if it is known that another client will later control it.
	  */
	ARIANE_SENSOR_ACCESS_MODE_MONITOR,
	/** Access mode: accept any mode. */
	ARIANE_SENSOR_ACCESS_MODE_ANY,
	/** Access to sensor denied in requested mode. Used as a return value. */	
	ARIANE_SENSOR_ACCESS_MODE_DENIED
} ariane_access_mode_t;

/** Parameter identifier type. */
typedef uint16_t ariane_paramid_t;

/* Well known parameter identifiers. */
enum {
	/** Parameter identifier: Sampling rate. */
	ARIANE_PARAMID_SAMPLING_RATE = 0x100,
	
	/** Parameter identifier: Binary block data length. */
	ARIANE_PARAMID_DATA_BLOCK_SIZE,
	
	/** Parameter identifier: Version number.*/
	ARIANE_PARAMID_VERSION,

	/**
      * Parameter identifier: Bit flags. Lower 16 bits are sensor specific, upper 16 bits are generic flags.
      * No generic flags have been defined yet.
      *
      * Some sensor specific flags are also defined (#ARIANE_TAPFLAG_ACTIVATION_DOUBLE_TAP etc).
     */
	ARIANE_PARAMID_FLAGS
};

/*
	The request and reply data structures (protocol level) were defined here. Due to
	various portability problems that could be caused by packed structs the definitions were
	removed.
	
	The data structures are documented in Ariane wiki.
*/

/*
	Don't confuse these with ARIANE_PARAMID_FLAGS which is something completely different. These flags
	are parameter flags field that is part of parameter description.
*/
#define ARIANE_SENSOR_PARAM_FLAG_SIGNED 	0x01 	/**< Parameter bit flag: values are with sign bit. */
#define ARIANE_SENSOR_PARAM_FLAG_RANGE 		0x02 	/**< Parameter bit flag: min and max values are valid. */
#define ARIANE_SENSOR_PARAM_FLAG_READONLY	0x04	/**< Parameter bit flag: parameter cannot be set. */

/** Sensor bit flag: Sensor is a filter (depends on other sensors).*/
#define ARIANE_SENSOR_FLAG_FILTER 				0x01	

/**
  * Sensor bit flag: Sensor has a static dependency list. Filters that depend on specific
  * sensors (SIDs) have this set. Dependency means that the sensor uses another sensor.
*/
#define ARIANE_SENSOR_FLAG_STATIC_DEPENDENCY 	0x02

/**
  * Sensor bit flag: Sensor is a HW mode of a physical sensor. This is set when a physical
  * sensor is visible as several virtual sensors that map to specific hardware modes.
  * Querying dependency list of the sensor returns the "sibbling" sensors.
*/
#define ARIANE_SENSOR_FLAG_HW_MODE 				0x04

/** Magic number for the reset request. */
#define ARIANE_RESET_MAGIC		0xDEAD
/** Constructs 16bit version number from major and minor version numbers. */
#define ARIANE_SUBSYS_VERSION(_major, _minor)		((_major << 8) | (_minor))

/** Key pressed flag. Specific to key code sensor event data. */
#define ARIANE_KEYFLAG_PRESSED		1
/** Key pressed flag. Specific to key code sensor event data. */
#define ARIANE_KEYFLAG_RELEASED		2

/** Tap sensor flag: activation by double tap. */
#define ARIANE_TAPFLAG_ACTIVATION_DOUBLE_TAP	0x01
/** Tap sensor flag: activation/deactivation feedback (vibration). */
#define ARIANE_TAPFLAG_ACTIVATION_FEEDBACK		0x02
/** Tap sensor flag: tap feedback (vibration). */
#define ARIANE_TAPFLAG_TAP_FEEDBACK				0x04

/** System sensor parameter ID: Events overrun counter (read-only). */
#define ARIANE_SYS_PARAMID_EVENTS_OVERRUN		0

/** System sensor parameter ID: TX overrun counter (read-only). */
#define ARIANE_SYS_PARAMID_TX_OVERRUN			1

/** System sensor parameter ID: Uptime (read-only). */
#define ARIANE_SYS_PARAMID_UP_TIME				2

/** System sensor parameter ID: TX buffering time limit. */
#define ARIANE_SYS_PARAMID_TX_BUF_TIME			3

/** System sensor parameter ID: TX retry limit. */
#define ARIANE_SYS_PARAMID_TX_RETRY_LIMIT		4

/** System sensor parameter ID: battery voltage (mV). */
#define ARIANE_SYS_PARAMID_VBAT					5

/** System sensor parameter ID: LED blinking enable/disable. Use this if the blinking annoys you. Subsys v0.29+ */
#define ARIANE_SYS_PARAMID_LED					6

#endif
