/*
 * request.c
 *
 * Requests to subsystem.
 *
 * This file is part of libariane
 *
 * Copyright (C) 2009-2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include "internals.h"
#include "bytecoding.h"

#include <arpa/inet.h>	// htons
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define TIMEOUT_START	500

static void release_req(ariane_sensordev_t *dev, int req_idx) {
	pthread_mutex_lock(&dev->req_mutex);
	dev->req_alloc_map ^= 1 << req_idx;
	pthread_mutex_unlock(&dev->req_mutex);
}

/*
	after 32k opens this will cause trouble. also this idiotically uses the
	same session ids for all devices. fix it if it bothers.
*/
ariane_session_id_t ariane_get_session_id() {
	static volatile ariane_session_id_t next_id = ARIANE_SESSIONID_FIRST + ARIANE_MAX_CONCURRECT_REQUESTS;
	static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

	pthread_mutex_lock(&mutex);
	ariane_session_id_t id = next_id;
	next_id++;
	if (next_id > ARIANE_SESSIONID_LAST) {
		next_id = ARIANE_SESSIONID_FIRST + ARIANE_MAX_CONCURRECT_REQUESTS;
	}
	pthread_mutex_unlock(&mutex);

	return id;
}


int ariane_request(ariane_sensordev_t *dev, ariane_session_id_t session, 
				   ariane_sid_t sid, ariane_req_t req, 
				   uint8_t *data, unsigned int data_len, 
				   uint8_t *rep_data) {
	assert(dev);
	
	int idx = -1;

	pthread_mutex_lock(&dev->req_mutex);

	// Find unused slot
	for (int i=0, j=1; i<ARIANE_MAX_CONCURRECT_REQUESTS; i++, j <<= 1)
		if (!(dev->req_alloc_map & j)) {
			dev->req_alloc_map |= j;
			idx = i;
			break;
		}

	pthread_mutex_unlock(&dev->req_mutex);
		
	if (idx < 0)
		return -EBUSY;

	if (!session)
		session = idx + ARIANE_SESSIONID_FIRST;

	uint8_t buf[ARIANE_MAX_MESSAGE_LEN];
	
	/*
		Header to buffer
	*/
	uint8_t *p = encode_uint16(buf, req);
	p = encode_uint16(p, sid);
	p = encode_uint16(p, session);

	if (data_len) {
		assert(data);
		memcpy(p, data, data_len);
		p += data_len;
	}

	// this is safe because we own the index
	dev->replies[idx].done = false;
	// session id is also used for mapping replies to requests, in addition to sensor ownership management.
	dev->replies[idx].session = session;

	/*
		Transmit request
	*/
	int err = dev->transmit(dev, buf, p - buf);
	if (err) {
		release_req(dev, idx);
		return err;
	}
	
	// Some requests don't have replies
	if (req == ARIANE_REQ_SYSTEM_RESET ||
		req == ARIANE_REQ_SENSOR_STOP ||
		req == ARIANE_REQ_SENSOR_CLOSE) {

		release_req(dev, idx);
		return 0;
	}

	pthread_mutex_lock(&dev->req_mutex);

	if (dev->replies[idx].done) {
		// wow, that was fast!
	} else {
		struct timespec ts;

		err = clock_gettime(CLOCK_REALTIME, &ts);
		if (!err) {
			/* Hack for old subsys */
			if (req == ARIANE_REQ_SENSOR_START) {
				ts.tv_nsec += TIMEOUT_START * 1000 * 1000;
				if (ts.tv_nsec >= 1000000000) {
					ts.tv_sec++;
					ts.tv_nsec -= 1000000000;
				}
			} else {
				ts.tv_sec += ARIANE_TIMEOUT;
			}
	
			err = pthread_cond_timedwait(&dev->req_cond[idx], &dev->req_mutex, &ts);
		} else {
			assert(errno);
			err = errno;
		}

		if (err) {
			pthread_mutex_unlock(&dev->req_mutex); // release_req locks again.. 

			release_req(dev, idx);

			/*
				The protocol was designed for "Ariane device adaptor" which worked differently.
				The start message is basically a design flaw from libarine perspective but it has to
				be supported because many devices have pre v0.10 software.
			*/
			if (err != ETIMEDOUT || req != ARIANE_REQ_SENSOR_START) {
				// this will make the receiver thread exit
				dev->state = ARIANE_DEVICE_STATE_ERROR;
			}
	
			return -err;
		}

		assert(dev->replies[idx].done);
	}

	pthread_mutex_unlock(&dev->req_mutex);

	/*
		This shouldn't be necessary but this could reveal problems that are otherwise difficult to debug.
	*/
	switch (req) {
		case ARIANE_REQ_SENSOR_OPEN:
			assert(dev->replies[idx].rsp == ARIANE_REQ_SENSOR_OPEN);
			break;

		case ARIANE_REQ_SENSOR_START:
			assert(dev->replies[idx].rsp == ARIANE_RSP_SENSOR_START);
			break;		

		case ARIANE_REQ_SYSTEM_GET_SENSORS: 
			assert(dev->replies[idx].rsp == ARIANE_RSP_SYSTEM_GET_SENSORS);
			break;		

		case ARIANE_REQ_SENSOR_GET_PROPERTIES: 
			assert(dev->replies[idx].rsp == ARIANE_RSP_SENSOR_GET_PROPERTIES);
			break;		

		case ARIANE_REQ_SENSOR_GET_DESCRIPTION: 
			assert(dev->replies[idx].rsp == ARIANE_RSP_SENSOR_GET_DESCRIPTION);
			break;		

		case ARIANE_REQ_SENSOR_GET_PARAMETERS: 
			assert(dev->replies[idx].rsp == ARIANE_RSP_SENSOR_GET_PARAMETERS);
			break;		
		
		case ARIANE_REQ_SENSOR_SET_PARAMETER:
			assert(dev->replies[idx].rsp == ARIANE_RSP_SENSOR_SET_PARAMETER);
			break;		

		case ARIANE_REQ_SENSOR_GET_PARAMETER:
			assert(dev->replies[idx].rsp == ARIANE_RSP_SENSOR_GET_PARAMETER);
			break;		

		case ARIANE_REQ_SENSOR_GET_PARAMETER_DESCRIPTION:
			assert(dev->replies[idx].rsp == ARIANE_RSP_SENSOR_GET_PARAMETER_DESCRIPTION);
			break;		

		case ARIANE_REQ_SENSOR_READ_BINARY_BLOCK:
			assert(dev->replies[idx].rsp == ARIANE_RSP_SENSOR_READ_BINARY_BLOCK);
			break;		

		case ARIANE_REQ_SENSOR_WRITE_BINARY_BLOCK:
			assert(dev->replies[idx].rsp == ARIANE_RSP_SENSOR_WRITE_BINARY_BLOCK);
			break;		

		case ARIANE_REQ_SENSOR_GET_DEPENDENCY_LIST:
			assert(dev->replies[idx].rsp == ARIANE_RSP_SENSOR_GET_DEPENDENCY_LIST);
			break;		

		case ARIANE_REQ_SYSTEM_GET_FILTERS:
			assert(dev->replies[idx].rsp == ARIANE_RSP_SYSTEM_GET_FILTERS);
			break;		

		case ARIANE_REQ_SYSTEM_INSTANTIATE_FILTER:
			assert(dev->replies[idx].rsp == ARIANE_RSP_SYSTEM_INSTANTIATE_FILTER);
			break;		

		case ARIANE_REQ_SYSTEM_GET_FILTER_DESCRIPTION:
			assert(dev->replies[idx].rsp == ARIANE_RSP_SYSTEM_GET_FILTER_DESCRIPTION);
			break;		

		case ARIANE_REQ_SENSOR_START2:
			assert(dev->replies[idx].rsp == ARIANE_RSP_SENSOR_START);
			break;		

		case ARIANE_REQ_SENSOR_GET_EX_PROPERTIES: 
			assert(dev->replies[idx].rsp == ARIANE_RSP_SENSOR_GET_EX_PROPERTIES);
			break;

		default:
#ifndef NDEBUG
			fprintf(stderr, "BUG! req %u rsp %u\n", req, dev->replies[idx].rsp);
#endif
			break;
	}

	unsigned int len = dev->replies[idx].payload_len;

	assert(rep_data || !len);

	if (len) {
		memcpy(rep_data, (void *)dev->replies[idx].payload, len);
	}

	release_req(dev, idx);

	return len;
}

int ariane_request_simple(ariane_sensordev_t *dev, ariane_req_t req, uint8_t *data, unsigned int data_len, uint8_t *rep_data) {
	return ariane_request(dev,
						  0,	// session
						  ARIANE_SID_SYS, req, data, data_len, rep_data);
}

int ariane_request_sensor(const ariane_sensor_t *sensor, ariane_req_t req, uint8_t *data, unsigned int data_len, uint8_t *rep_data) {
	return ariane_request(sensor->dev, sensor->session, sensor->sid, req, data, data_len, rep_data);
}

int ariane_sensordev_get_sensors(ariane_sensordev_t *dev, ariane_sid_t *sensors) {
	assert(sensors);

	int len = ariane_request_simple(dev, ARIANE_REQ_SYSTEM_GET_SENSORS, NULL, 0, (uint8_t *)sensors);
	if (len < 0)
		return len;

	int nsids = len / sizeof(ariane_sid_t);
	for (int i=0; i<nsids; i++)
		sensors[i] = ntohs(sensors[i]);

	return nsids;
}

int ariane_sensordev_get_filters(ariane_sensordev_t *dev, ariane_filter_id_t *filters) {
	assert(filters);

	int len = ariane_request_simple(dev, ARIANE_REQ_SYSTEM_GET_FILTERS, NULL, 0, (uint8_t *)filters);
	if (len < 0)
		return len;

	int nfilters = len / sizeof(ariane_filter_id_t);
	for (int i=0; i<nfilters; i++)
		filters[i] = ntohs(filters[i]);

	return nfilters;
}

int ariane_sensordev_get_filter_description(ariane_sensordev_t *dev, ariane_filter_id_t fid, char *descr) {
	assert(descr);

	uint8_t buf[ARIANE_MAX_SENSOR_DESCR_LEN + 2];

	encode_uint16(buf, fid);

	int len = ariane_request_simple(dev, ARIANE_REQ_SYSTEM_GET_FILTER_DESCRIPTION, buf, 2, buf);

	if (len < 0)
		return len;

	assert(len > 2);

	strncpy(descr, (char *)buf + 2, ARIANE_MAX_SENSOR_DESCR_LEN);
	descr[ARIANE_MAX_SENSOR_DESCR_LEN - 1] = 0;

	return len;
}

int ariane_sensordev_get_dependency_list(ariane_sensordev_t *dev, ariane_sid_t sid, ariane_sid_t *sids) {
	assert(sids);

	int len = ariane_request(dev, 0, sid, ARIANE_REQ_SENSOR_GET_DEPENDENCY_LIST, NULL, 0, (uint8_t *)sids);
	if (len < 0)
		return len;

	int nsids = len / sizeof(ariane_sid_t);
	for (int i=0; i<nsids; i++)
		sids[i] = htons(sids[i]);

	return nsids;
}


int ariane_sensordev_get_all_dependencies(ariane_sensordev_t *dev, ariane_sid_t sid, ariane_sid_t *sids) {	
	ariane_sid_t new_sids[ARIANE_MAX_DEPENDENCIES];

	int nsids = ariane_sensordev_get_dependency_list(dev, sid, sids);
	if (nsids <= 0)
		return nsids;

	for (int i=0; i<nsids; i++) {
		int n = ariane_sensordev_get_dependency_list(dev, sids[i], new_sids);
		if (n < 0)
			return n;

		for (int j=0; j<n; j++) {
			bool found = false;
			for (int k=0; k<nsids; k++) {
				if (new_sids[j] == sids[k]) {
					found = true;
					break;
				}
			}
			if (!found)
				sids[nsids++] = new_sids[j];
		}
	}

	return nsids;
}

// TODO move this to some other place?
ariane_event_datafmt_t ariane_mtype_to_datafmt(ariane_mtype_t mtype) {
	switch (mtype) {
		case ARIANE_MTYPE_ACCELERATION:
		case ARIANE_MTYPE_ANGULAR_RATE:
		case ARIANE_MTYPE_MAGNETIC_FIELD:
		case ARIANE_MTYPE_ORIENTATION:
			return ARIANE_EVENT_DATAFMT_XYZ16;

		case ARIANE_MTYPE_PRESSURE:
			return ARIANE_EVENT_DATAFMT_U32;

		case ARIANE_MTYPE_TEMPERATURE:
		case ARIANE_MTYPE_TESTPATTERN:
			return ARIANE_EVENT_DATAFMT_U16;

		case ARIANE_MTYPE_KEYCODE:
			return ARIANE_EVENT_DATAFMT_KEY;

		case ARIANE_MTYPE_APPLICATION:
			return ARIANE_EVENT_DATAFMT_APPLICATION;

		// This is here only because gcc would otherwise print warnings
		default:
			return ARIANE_EVENT_DATAFMT_NONE;
	}
}

int ariane_sensordev_open_sensor(ariane_sensordev_t *dev, ariane_session_id_t session, ariane_sid_t sid, ariane_access_mode_t amode, ariane_sensor_t **sensor) {
	assert(dev && sensor);

	uint16_t u16 = htons(amode);

	if (!session)
		session = ariane_get_session_id();

	int len = ariane_request(dev, session, sid, ARIANE_REQ_SENSOR_OPEN, (uint8_t *)&u16, 2, (uint8_t *)&u16);
	if (len < 0)
		return len;

	ariane_access_mode_t granted_amode = ntohs(u16);

	if (granted_amode == ARIANE_SENSOR_ACCESS_MODE_DENIED)
		return granted_amode;

	*sensor = malloc(sizeof(ariane_sensor_t));
	if (*sensor	== NULL)
		return -errno;

	(*sensor)->sid = sid;
	(*sensor)->dev = dev;
	(*sensor)->session = session;
	(*sensor)->access_mode = granted_amode;
	(*sensor)->datafmt = ariane_mtype_to_datafmt(ARIANE_SID_GET_MTYPE(sid));

	return granted_amode;
}

int ariane_sensordev_open_by_type(ariane_sensordev_t *dev, ariane_session_id_t session, ariane_mtype_t type, ariane_access_mode_t amode, ariane_sensor_t **sensor) {
	ariane_sid_t sids[ARIANE_MAX_SENSORS];

	int nsids = ariane_sensordev_get_sensors(dev, sids);
	if (nsids < 0)
		return nsids;

	for (int i=0; i<nsids; i++) {
		if (ARIANE_SID_GET_MTYPE(sids[i]) == type) {
			int retval = ariane_sensordev_open_sensor(dev, session, sids[i], amode, sensor);
			if (retval < 0)
				return retval;
	
			if (retval != ARIANE_SENSOR_ACCESS_MODE_DENIED) {
				return retval; // return access mode
			}
		}
	}

	return -ENOENT;
}

int ariane_sensordev_instantiate_filter(ariane_sensordev_t *dev, ariane_filter_id_t fid, ariane_sid_t source_sid, ariane_sid_t *new_sid) {
	uint8_t buf[6];

	encode_uint16(buf, fid);
	encode_uint16(buf + 2, source_sid);

	int len = ariane_request_simple(dev, ARIANE_REQ_SYSTEM_INSTANTIATE_FILTER, buf, 4, buf);
	if (len < 0)
		return len;

	assert(len == 6);

	if (decode_uint16(buf) != fid)
		return ARIANE_ERROR_GENERIC;

	*new_sid = decode_uint16(buf + 2);	
	int err = decode_uint16(buf + 4);		// the code should be from errno.h

	return -err;
}

int ariane_sensordev_reset(ariane_sensordev_t *dev) {
	uint16_t magic = htons(ARIANE_RESET_MAGIC);
	return ariane_request_simple(dev, ARIANE_REQ_SYSTEM_RESET, (uint8_t *)&magic, 2, NULL);
}

int ariane_sensordev_get_version_real(ariane_sensordev_t *dev) {
	ariane_sensor_t sensor = {ARIANE_SID_SYS, dev};

	uint32_t value;
	int err = ariane_sensor_get_parameter_value(&sensor, ARIANE_PARAMID_VERSION, &value);

	if (err)
		return err;
	else
		return value;
}

uint16_t ariane_sensordev_get_version(ariane_sensordev_t *dev) {
	assert(dev);
	return dev->version;
}

ariane_device_location_t ariane_sensordev_get_location(ariane_sensordev_t *dev) {
	assert(dev);
	return dev->location;
}

int ariane_sensor_get_properties(ariane_sensor_t *sensor, ariane_sensor_properties_t *props) {
	assert(sensor);
	return ariane_sensordev_get_sensor_properties(sensor->dev, sensor->sid, props);
}

// this is quite risky code
static float conv_float(uint8_t *buf) {
	assert(sizeof(float) == 4);

	float f;
	uint8_t *p = (uint8_t *)&f;
	p[0] = buf[3];
	p[1] = buf[2];
	p[2] = buf[1];
	p[3] = buf[0];
	
	return f;
}

int ariane_sensordev_get_sensor_properties(ariane_sensordev_t *dev, ariane_sid_t sid, ariane_sensor_properties_t *props) {
	assert(props);

	if (ARIANE_SID_GET_MTYPE(sid) >= ARIANE_MTYPE_SW_MEASUREMENT) {
		return -ENOENT;
	}

	uint8_t buf[ARIANE_MAX_PAYLOAD_LEN];
	
	int len = ariane_request(dev, 0, sid, ARIANE_REQ_SENSOR_GET_PROPERTIES, NULL, 0, buf);
	if (len < 0)
		return len;

	assert(len >= 10);

	props->sample_rate = decode_uint16(buf);
	props->resolution_bits = buf[2];
	props->sample_channels = buf[3];
	props->flags = decode_uint16(buf + 4);

	assert(props->sample_channels <= ARIANE_MAX_CHANNELS);

	assert(len == 6 + props->sample_channels * 8);

	uint8_t *p = buf + 6;

	for (int i=0; i<props->sample_channels; i++) {
		props->conversions[i].scale = conv_float(p);
		p += 4;

		props->conversions[i].offset = conv_float(p);
		p += 4;
	}

	props->datafmt = ariane_mtype_to_datafmt(ARIANE_SID_GET_MTYPE(sid));

	if (ariane_sensordev_get_version(dev) >= ARIANE_SUBSYS_VERSION(0, 29)) {
		// extended properties supported
	
		int len = ariane_request(dev, 0, sid, ARIANE_REQ_SENSOR_GET_EX_PROPERTIES, NULL, 0, buf);
		if (len < 0)
			return len;		
		
		assert(len == ARIANE_MAX_SENSOR_NAME_LEN + 2 * 4 + ARIANE_MAX_SENSOR_RATES * 2);
		
		p = buf;

		memcpy(props->name, p, ARIANE_MAX_SENSOR_NAME_LEN);
		p += ARIANE_MAX_SENSOR_NAME_LEN;
		
		props->min_value = decode_uint32(p);
		p += 4;
		props->max_value = decode_uint32(p);
		p += 4;

		for (int i=0; i<ARIANE_MAX_SENSOR_RATES; i++) {
			props->rates[i] = decode_uint16(p);
			p += 2;
		}
	} else {
		props->name[0] = 0;
		props->min_value = props->max_value = 0;
		memset(props->rates, 0, sizeof(props->rates));
	}

	return 0;
}


int ariane_sensor_get_description(ariane_sensor_t *sensor, char *descr) {
	return ariane_sensordev_get_sensor_description(sensor->dev, sensor->sid, descr);
}

int ariane_sensordev_get_sensor_description(ariane_sensordev_t *dev, ariane_sid_t sid, char *descr) {
	assert(descr);

	int len = ariane_request(dev,
						  	 0,	// session
						  	 sid, ARIANE_REQ_SENSOR_GET_DESCRIPTION, NULL, 0, (uint8_t *)descr);

	if (len < 0)
		return len;

	return 0;
}

int ariane_sensor_get_parameter_ids(ariane_sensor_t *sensor, ariane_paramid_t *pids) {
	assert(sensor);

	return ariane_sensordev_get_sensor_parameter_ids(sensor->dev, sensor->sid, pids);
}

int ariane_sensordev_get_sensor_parameter_ids(ariane_sensordev_t *dev, ariane_sid_t sid, ariane_paramid_t *pids) {
	assert(pids);

	int len = ariane_request(dev, 0, sid, ARIANE_REQ_SENSOR_GET_PARAMETERS, NULL, 0, (uint8_t *)pids);
	if (len <= 0)
		return len;

	len /= sizeof(ariane_paramid_t);

	for (int i=0; i<len; i++)
		pids[i] = ntohs(pids[i]);

	return len;	
}

int ariane_sensor_get_parameters(ariane_sensor_t *sensor, ariane_sensor_parameter_t **params) {
	return ariane_sensordev_get_sensor_parameters(sensor->dev, sensor->sid, params);
}

int ariane_sensordev_get_sensor_parameters(ariane_sensordev_t *dev, ariane_sid_t sid, ariane_sensor_parameter_t **params) {
	ariane_paramid_t pids[ARIANE_MAX_PARAMETERS];

	int npids = ariane_sensordev_get_sensor_parameter_ids(dev, sid, pids);

	if (npids <= 0)
		return npids;

	assert(npids <= ARIANE_MAX_PARAMETERS);

	*params = malloc(npids * sizeof(ariane_sensor_parameter_t));
	for (int i=0; i<npids; i++) {
		int retval = ariane_sensordev_get_sensor_parameter_description(dev, sid, pids[i], *params + i);
		if (retval < 0) {
			free(*params);
			return retval;
		}
	}

	return npids;
}

int ariane_sensordev_get_sensor_parameter_description(ariane_sensordev_t *dev, ariane_sid_t sid, ariane_paramid_t pid, ariane_sensor_parameter_t *param) {
	assert(param);	

	uint8_t buf[ARIANE_MAX_PAYLOAD_LEN];

	encode_uint16(buf, pid);

	int len = ariane_request(dev, 0, sid, ARIANE_REQ_SENSOR_GET_PARAMETER_DESCRIPTION, buf, 2, buf);
	if (len < 0)
		return len;
	
	assert(len > 12);

	param->paramid = decode_uint16(buf);
	param->min =  decode_uint32(buf + 2);
	param->max =  decode_uint32(buf + 6);
	param->flags =  decode_uint16(buf + 10);
	
	strncpy(param->description, (char *)(buf + 12), ARIANE_MAX_SENSOR_DESCR_LEN);
	param->description[ARIANE_MAX_SENSOR_DESCR_LEN - 1] = 0;	// it should be already null terminated but ..

	return 0;	
}

int ariane_sensor_get_parameter_description(ariane_sensor_t *sensor, ariane_paramid_t pid, ariane_sensor_parameter_t *param) {
	assert(sensor);
	return ariane_sensordev_get_sensor_parameter_description(sensor->dev, sensor->sid, pid, param);
}

int ariane_sensor_get_parameter_value(ariane_sensor_t *sensor, ariane_paramid_t pid, uint32_t *value) {
	return ariane_sensordev_get_sensor_parameter_value(sensor->dev, sensor->sid, pid, value);
}

int ariane_sensordev_get_sensor_parameter_value(ariane_sensordev_t *dev, ariane_sid_t sid, ariane_paramid_t pid, uint32_t *value) {
	assert(value);

	uint8_t buf[8];

	encode_uint16(buf, pid);

	int len = ariane_request(dev, 0, sid, ARIANE_REQ_SENSOR_GET_PARAMETER, buf, 2, buf);

	if (len < 0)
		return len;	

	assert(len == 8);
	assert(pid == decode_uint16(buf));	

	*value = decode_uint32(buf + 2);

	int err = decode_uint16(buf + 6);

	return -err;
}

int ariane_sensor_set_parameter_value(ariane_sensor_t *sensor, ariane_paramid_t pid, uint32_t *value) {
	assert(value);

	uint8_t buf[8];
	
	uint8_t *p = encode_uint16(buf, pid);
	p = encode_uint32(p, *value);

	int len = ariane_request_sensor(sensor, ARIANE_REQ_SENSOR_SET_PARAMETER, buf, p - buf, buf);
	if (len < 0)
		return len;

	assert(len == 8);
	assert(pid == decode_uint16(buf));	

	*value = decode_uint32(buf + 2);
	int err = decode_uint16(buf + 6);

	return -err;
}

int ariane_sensor_get_binary_block_size(ariane_sensor_t *sensor) {
	uint32_t value = 0;
	int err = ariane_sensor_get_parameter_value(sensor, ARIANE_PARAMID_DATA_BLOCK_SIZE, &value);
	if (err < 0)
		return err;

	return value;
}

int ariane_sensordev_get_sensor_binary_block_size(ariane_sensordev_t *dev, ariane_sid_t sid) {
	ariane_sensor_t sensor = {ARIANE_SID_SYS, dev};

	return ariane_sensor_get_binary_block_size(&sensor);
}

int ariane_sensor_set_sampling_rate(ariane_sensor_t *sensor, unsigned int rate) {
	uint32_t value = rate;

	int retval = ariane_sensor_set_parameter_value(sensor, ARIANE_PARAMID_SAMPLING_RATE, &value);
	if (retval < 0)
		return retval;

	return value;
}

int ariane_sensor_write_binary_block(ariane_sensor_t *sensor, unsigned int offset, const uint8_t *data, unsigned int len) {
	while (len) {
		uint8_t buf[ARIANE_MAX_PAYLOAD_LEN];
		uint8_t *pbuf = buf;

		unsigned int req_len = len > ARIANE_MAX_DATA_BLOCK_LEN ? ARIANE_MAX_DATA_BLOCK_LEN : len;

		pbuf = encode_uint16(pbuf, offset);
		pbuf = encode_uint16(pbuf, req_len);
		
		memcpy(pbuf, data, req_len);

		pbuf += req_len;
		len -= req_len;
		offset += req_len;
		data += req_len;

		int rep_len = ariane_request_sensor(sensor, ARIANE_REQ_SENSOR_WRITE_BINARY_BLOCK, buf, pbuf - buf, buf);
		if (rep_len < 0)
			return rep_len;

		assert(rep_len == 6);

		int err = decode_uint16(buf + 4);
		if (err)
			return -err;
	}

	return 0;
}

int ariane_sensor_read_binary_block(ariane_sensor_t *sensor, unsigned int offset, uint8_t *data, unsigned int len) {
	while (len) {
		uint8_t buf[ARIANE_MAX_PAYLOAD_LEN];
		uint8_t *pbuf = buf;

		unsigned int req_len = len > ARIANE_MAX_DATA_BLOCK_LEN ? ARIANE_MAX_DATA_BLOCK_LEN : len;

		pbuf = encode_uint16(pbuf, offset);
		pbuf = encode_uint16(pbuf, req_len);
		
		int rep_len = ariane_request_sensor(sensor, ARIANE_REQ_SENSOR_READ_BINARY_BLOCK, buf, pbuf - buf, buf);
		if (rep_len < 0)
			return rep_len;

		assert(rep_len == 6 + req_len);
		assert(offset == decode_uint16(buf));
		assert(req_len == decode_uint16(buf + 2));

		int err = decode_uint16(buf + 4);
		if (err)
			return -err;

		memcpy(data, buf + 6, req_len);
		len-= req_len;
		offset += req_len;
		data += req_len;
	}

	return 0;
}

int ariane_sensor_start(ariane_sensor_t *sensor, ariane_event_handler_t event_handler, void *param) {
	assert(event_handler);

	int err = ariane_register_listener(sensor, event_handler, param);
	if (err < 0)
		return -err;

	assert(sensor && sensor->dev);

	uint8_t buf[2];

	if (sensor->dev->version < ARIANE_SUBSYS_VERSION(0, 10)) {
		/*
			The first release of subsys (0.09) does not respond to successful start requests
			so time out is normal.
		*/
		err = ariane_request_sensor(sensor, ARIANE_REQ_SENSOR_START, NULL, 0, buf);

		if (err == -ETIMEDOUT)
			err = 0;
	} else {
		// subsys v0.10+
		err = ariane_request_sensor(sensor, ARIANE_REQ_SENSOR_START2, NULL, 0, buf);
	}

	if (err == 2) {	// 2 byte reply
		err = -(int)decode_uint16(buf);
	}

	if (err < 0)
		ariane_unregister_listener(sensor);

	return err;	
}

int ariane_sensor_stop(ariane_sensor_t *sensor) {
	ariane_unregister_listener(sensor);

	return ariane_request_sensor(sensor, ARIANE_REQ_SENSOR_STOP, NULL, 0, NULL);
}

int ariane_sensor_close(ariane_sensor_t *sensor) {
	ariane_unregister_listener(sensor);	// app should have called ariane_sensor_stop so this is unnecessary

	if (!sensor)
		return -EINVAL;

	int err = ariane_request_sensor(sensor, ARIANE_REQ_SENSOR_CLOSE, NULL, 0, NULL);
	
	free(sensor);

	return err;
}


int ariane_i2c_req(ariane_sensordev_t *dev, ariane_session_id_t session, uint8_t addr, uint8_t reg, uint8_t *data, unsigned int len) {
	if (!len || !data || len > ARIANE_MAX_DATA_BLOCK_LEN)
		return -EINVAL;

	uint8_t buf[ARIANE_MAX_DATA_BLOCK_LEN];

	buf[0] = addr;	// bit7 is used for read/write bit (different from I2C protocol!)
	buf[1] = reg;
	buf[2] = len;
		
	unsigned int rlen = 3;
	if (addr & 0x80) {
		memcpy(buf + rlen, data, len);
		rlen += len;
	}

	rlen = ariane_request(dev, session, ARIANE_SID_SYS, ARIANE_REQ_I2C, buf, rlen, buf);
	
	if (rlen < 0)
		return rlen;	

	assert(rlen >= 1);
		
	int err = buf[0];
	if (err)
		return -err;
	
	if (rlen > 1) {
		assert(rlen - 1 == len);
	
		memcpy(data, buf + 1, rlen - 1);
	}
	
	return len;
}

int ariane_sensordev_get_all_sensor_properties(ariane_sensordev_t *dev, ariane_sensor_property_item_t **props_pp) {
	if (!props_pp)
		return -EINVAL;

	ariane_sid_t sids[ARIANE_MAX_SENSORS];

	*props_pp = NULL;

	int nsids = ariane_sensordev_get_sensors(dev, sids);
	if (nsids < 0)
		return nsids;
	
	ariane_sensor_property_item_t *pi = malloc(sizeof(ariane_sensor_property_item_t) * nsids);
	if (!pi)
		return -errno;
		
	for (int i=0; i<nsids; i++) {
		pi[i].sid = sids[i];

		int err = ariane_sensordev_get_sensor_properties(dev, sids[i], &pi[i].props);
		
		if (err < 0 && err != -ENOENT) {
			free(pi);
			return err;
		}
		
	    if (err == -ENOENT) {
			memset(&pi[i].props, 0, sizeof(ariane_sensor_properties_t));
        }
	}
	
	*props_pp = pi;
	
	return nsids;
}

int ariane_i2c_read(ariane_sensordev_t *dev, ariane_session_id_t session, uint8_t addr, uint8_t reg, uint8_t *data, unsigned int len) {
	return ariane_i2c_req(dev, session, addr, reg, data, len);
}

int ariane_i2c_write(ariane_sensordev_t *dev, ariane_session_id_t session, uint8_t addr, uint8_t reg, uint8_t *data, unsigned int len) {
	return ariane_i2c_req(dev, session, addr | 0x80, reg, data, len);
}
