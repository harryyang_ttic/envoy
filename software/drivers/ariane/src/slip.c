/*
 * slip.c
 *
 * Based on RFC 1055.
 *
 * This file is part of libariane
 *
 * Copyright (C) 2003, 2009 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include "slip.h"

uint16_t slip_esc(unsigned int flags, const uint8_t *src, uint8_t *dst, unsigned int len) {
	uint8_t *dptr = dst;
	
	if (!(flags & SLIP_NO_EXTRA_END))
		*dptr++ = SLIP_END;
	
	while (len--) {
		switch (*src) {
			case SLIP_ESC:
				*dptr++ = SLIP_ESC;
				*dptr++ = SLIP_ESC_ESC;
				break;

			case SLIP_END:
				*dptr++ = SLIP_ESC;
				*dptr++ = SLIP_ESC_END;
				break;
			
			default:
				*dptr++ = *src;
				break;
		}
		
		src++;
	}

	if (!(flags & SLIP_NO_END))
		*dptr++ = SLIP_END;
	
	return (dptr - dst);
}


