/*
 * vibra.c
 *
 * This file is part of libariane
 *
 * Copyright (C) 2009-2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include "ariane-vibra.h"
#include <assert.h>
#include <stddef.h>

int ariane_vibra_open(ariane_sensordev_t *dev, ariane_session_id_t session, int num, ariane_vibra_t *handle) {
	ariane_sid_t sid = ARIANE_SID(num, ARIANE_DEVICE_LOCATION_ANY, ARIANE_MTYPE_HAPTIC);

	return ariane_sensordev_open_sensor(dev, session, sid, ARIANE_SENSOR_ACCESS_MODE_CONTROL, handle);
}

int ariane_vibra_close(ariane_vibra_t handle) {
	return ariane_sensor_close(handle);
}

int ariane_vibra_set_freq(ariane_vibra_t handle, unsigned int freq) {
	return ariane_sensor_set_sampling_rate(handle, freq);
}

int ariane_vibra_get_freq(ariane_vibra_t handle) {
	uint32_t value;
	int err = ariane_sensor_get_parameter_value(handle, ARIANE_PARAMID_SAMPLING_RATE, &value);
	if (err < 0)
		return err;

	return value;
}

static void dummy_event_handler(const ariane_event_t *event, void *param) {
}

int ariane_vibra_start(ariane_vibra_t handle) {
	return ariane_sensor_start(handle, dummy_event_handler, NULL);
}

int ariane_vibra_stop(ariane_vibra_t handle) {
	return ariane_sensor_stop(handle);
}

