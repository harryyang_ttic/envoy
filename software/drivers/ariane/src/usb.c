/*
 * usb.c
 *
 * USB support.
 *
 * This file is part of libariane
 *
 * Copyright (C) 2009-2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Jukka Jalkanen <jukka.jalkanen@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

//#define USB_RESET

#include "internals.h"
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <usb.h>

static void close_usb_handle(usb_dev_handle *usbhandle) {
	usb_release_interface(usbhandle, 0);
	usb_close(usbhandle);
}

static void receiver(ariane_sensordev_t *dev) {	
	usb_dev_handle *usbhandle = (usb_dev_handle *)dev->driver_data;
	assert(usbhandle);

	while (dev->state == ARIANE_DEVICE_STATE_READY) {
		uint8_t buf[ARIANE_MAX_MESSAGE_LEN];
		int retval = usb_interrupt_read(usbhandle, ARIANE_USB_EP_IN, (char *)buf, ARIANE_MAX_MESSAGE_LEN, ARIANE_TIMEOUT_USB);

		if (retval < 0)	{
			// time out here is not an error
			if (retval != -ETIMEDOUT) {
				ariane_async_error(dev, retval);
			}

			continue;
		}

		assert(retval >= ARIANE_MIN_MESSAGE_LEN);

		ariane_receive(dev, buf, retval);
	}

#ifdef USB_RESET
	/* 
		USB device doesn't get notified of our disconnect so it's best to reset it.
		Small delay is necessary after reset so that the test cases don't break: they 
		sometimes connect immediately after disconnect.
	*/
	if (ariane_sensordev_reset(dev) >= 0) {
		int err;
		do {
			err = usleep(999999);
		} while (err < 0 && errno == EINTR);
	}
#endif

	close_usb_handle(usbhandle);
	dev->driver_data = NULL;
}

static int transmit(ariane_sensordev_t *dev, uint8_t *data, unsigned int len) {
	assert(dev && data);

	if (dev->state == ARIANE_DEVICE_STATE_ERROR)
		return -ARIANE_ERROR_GENERIC;

	usb_dev_handle *usbhandle = (usb_dev_handle *)dev->driver_data;
	assert(usbhandle);

	int retval = usb_interrupt_write(usbhandle, ARIANE_USB_EP_OUT, (char *)data, len, ARIANE_TIMEOUT * 1000);

	/*
		libusb documentation says it returns <0 on error and nothing about short writes. This seems to work..
	*/
	if (retval != len)
		return -ARIANE_ERROR_USB;

	return 0;
}

int ariane_usb_connect(ariane_sensordev_t **sdev) {
	assert(sdev);

	usb_init();

	if (usb_find_busses() < 0) {
		return -ARIANE_ERROR_USB;
	}
	
	if (usb_find_devices() < 0) {
		return -ARIANE_ERROR_USB;
	}

	static struct usb_bus *busses, *bus;

	busses = usb_get_busses();

	for (bus=busses; bus; bus = bus->next) {
		struct usb_device *dev;

		for (dev=bus->devices; dev; dev = dev->next) {
			if (dev->descriptor.idVendor == ARIANE_USB_VENDORID &&
				dev->descriptor.idProduct == ARIANE_USB_PRODUCTID) {
				usb_dev_handle *usbhandle = usb_open(dev);
				if (!usbhandle)
					return -ARIANE_ERROR_USB_OPEN;

				if (usb_set_configuration(usbhandle, 1) < 0) {
					usb_close(usbhandle);
					return -ARIANE_ERROR_USB_ACCESS;
				}

				if (usb_claim_interface(usbhandle, 0) < 0) {
					usb_close(usbhandle);
					return -ARIANE_ERROR_USB_ACCESS;
				}
			
				int err = ariane_create_sensordev(sdev);

				if (err) {
					close_usb_handle(usbhandle);
					return err;
				}
			
				(*sdev)->receiver = &receiver;
				(*sdev)->transmit = &transmit;
				(*sdev)->driver_data = usbhandle;
			
				return ariane_connect(*sdev);
			}
		}
	}

	return -ENODEV;
}
