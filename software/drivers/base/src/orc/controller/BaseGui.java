package orc.controller;

import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import lcm.lcm.*;
import erlcm.*;
import orc.spy.*;

import orc.*;

/** A GUI for visualizing and interacting with the uOrc. **/
public class BaseGui
{
    JFrame jf;

    JDesktopPane jdp = new JDesktopPane();

    TextPanelWidget basicDisplay, qeiDisplay;
    ServoPanel servoPanels[];

    DigitalOutput digital_override;

    double rv_duty_cycle = 0.0;
    double tv_duty_cycle = 0.0;

    LCM lcm;
    Orc orc;

    public static void main(String args[])
    {
	try{
	    new BaseGui(args);
	}
	catch (IOException ex) {
	    System.out.println("LCM IO Exception: " + ex);
	}
    }

    public BaseGui(String args[])
	throws IOException
    {
        if (args.length > 0)
            orc = Orc.makeOrc(args[0]);
        else
            orc = Orc.makeOrc();

        jf = new JFrame("Wheelchair : "+orc.getAddress());
        jf.setLayout(new BorderLayout());
        jf.add(jdp, BorderLayout.CENTER);

        basicDisplay = new TextPanelWidget(new String[] {"Parameter", "Value"},
                                           new String[][] {{"uorc time", "0"}},
                                           new double[] {.4, .6});

	qeiDisplay = new TextPanelWidget(new String[] {"Port", "Position", "Velocity"},
                                         new String[][] {{"0", "0", "0"},
                                                         {"1", "0", "0"}},
                                         new double[] {.3, .5, .5});

        JPanel servoDisplay = new JPanel();
        servoDisplay.setLayout(new GridLayout(2,1));
        servoPanels = new ServoPanel[2];
        for (int i = 0; i < servoPanels.length; i++) {
            servoPanels[i] = new ServoPanel(i);
            JPanel jp = new JPanel();
            jp.setLayout(new BorderLayout());
	    if(i == 0){
		jp.add(new JLabel("Tv "), BorderLayout.WEST);
	    }
	    else{
		jp.add(new JLabel("Rv "), BorderLayout.WEST);
	    }
            jp.add(servoPanels[i], BorderLayout.CENTER);
            servoDisplay.add(jp);
        }

        jf.setSize(906,550);
        jf.setVisible(true);
        makeInternalFrame("Basic Status", basicDisplay, 300, 120);
        makeInternalFrame("Quadrature Decoders", qeiDisplay, 300, 120);
        makeInternalFrame("Joystick Simulation", servoDisplay, 300, 120);

	//might consider having a sampling from the simulated output to the voltage applied (by sensing the voltage)

	lcm = new LCM();
	
	digital_override = new DigitalOutput(orc, 0);

	digital_override.setValue(false);

	try {
	    Thread.sleep(100);
	}
	catch (InterruptedException ex) {
	}
	digital_override.setValue(true);

        new StatusPollThread().start();

        jf.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
		//add code that sets things to zero
		shutdown();
                System.exit(0);
            }});
    }

    int xpos = 0, ypos = 0;
    void makeInternalFrame(String name, JComponent jc, int width, int height)
    {
        JInternalFrame jif = new JInternalFrame(name, true, true, true, true);
        jif.setLayout(new BorderLayout());
        jif.add(jc, BorderLayout.CENTER);
        if (ypos+height >= jf.getHeight()) {
            ypos = 0;
            xpos += width;
        }
        jif.reshape(xpos, ypos, width, height);
        ypos += height;

        jif.setVisible(true);
        jdp.add(jif);
        jdp.setBackground(Color.blue);
    }

    class StatusPollThread extends Thread
    {
        StatusPollThread()
        {
            setDaemon(true);
        }

        public void run()
        {
            while(true) {
                try {
                    OrcStatus status = orc.getStatus();
                    orcStatus(orc, status);
                    Thread.sleep(20);
                } catch (Exception ex) {
                    System.out.println("Spy.StatusPollThread ex: "+ex);
                }
            }
        }
    }

    boolean firstStatus = true;

    int [] last = {0, 0};

    public void shutdown()
    {
	digital_override.setValue(false);
    }
    public void orcStatus(Orc orc, OrcStatus status)
    {
        // if uorc resets...
        if (status.utimeOrc < 2000000)
            firstStatus = true;

	orc_full_stat_msg_t msg = new orc_full_stat_msg_t();
		
	msg.utime = status.utimeOrc; 
	
	System.arraycopy(status.qeiPosition,0,msg.qei_position ,0,2);
	
        for (int i = 0; i < 2; i++) {
            qeiDisplay.values[i][1] = String.format("%6d", status.qeiPosition[i]);
	    if(last[i] != 0){
		if(status.qeiPosition[i] - last[i] < 0){
		    qeiDisplay.values[i][2] = String.format("%6d", -status.qeiVelocity[i]);
		    msg.qei_velocity[i] = -status.qeiVelocity[i];
		}
		else{
		    qeiDisplay.values[i][2] = String.format("%6d", status.qeiVelocity[i]);
		    msg.qei_velocity[i] = status.qeiVelocity[i];
		}
	    }
	    last[i] = status.qeiPosition[i];	    
        }
	
        basicDisplay.values[0][1] = String.format("%.6f", status.utimeOrc/1000000.0);


	msg.cmd_velocity[0] = tv_duty_cycle;
	msg.cmd_velocity[1] = rv_duty_cycle;

	lcm.publish ("BASE_FULL_STAT", msg); 

	firstStatus = false;

	qeiDisplay.repaint();
        basicDisplay.repaint();
    }

    class ServoPanel extends JPanel implements SmallSlider.Listener
    {
        SmallSlider slider = new SmallSlider(0, 1000, 560, 560, true);
        Servo servo;
	int port;

        ServoPanel(int port)
        {
	    this.port = port;
            slider.formatScale = 1;
            slider.formatString = "%.0f per one thousands";

            // pos/usecs don't matter, we specify pulse widths, not positions
            servo = new Servo(orc, port, 0, 0, 0, 0);
	    servo.setPWM(1500,0.565);
	    if(port == 0){
		tv_duty_cycle = 0.565;
	    }else{
		rv_duty_cycle = 0.565;
	    }

            setLayout(new BorderLayout());
            add(slider, BorderLayout.CENTER);

            slider.addListener(this);
        }

        public void goalValueChanged(SmallSlider slider, int goal)
        {
            //servo.setPulseWidth(goal);
	    double duty_cycle = (double)(goal/1000.0);
	    System.out.printf("Duty Cycle : %f\n", duty_cycle);
	    if(this.port == 0){
		tv_duty_cycle = duty_cycle;
	    }else{
		rv_duty_cycle = duty_cycle;
	    }
	    servo.setPWM(2000,duty_cycle);
        }
    }

    class TextPanelWidget extends JPanel
    {
        String panelName;
        String columnNames[];
        String values[][];
        double columnWidthWeight[];

        public TextPanelWidget(String columnNames[], String values[][],
                               double columnWidthWeight[])
        {
            this.columnNames = columnNames;
            this.columnWidthWeight = columnWidthWeight;
            this.values = values;
        }

        int getStringWidth(Graphics g, String s)
        {
            FontMetrics fm = g.getFontMetrics(g.getFont());
            return fm.stringWidth(s);
        }

        int getStringHeight(Graphics g, String s)
        {
            FontMetrics fm = g.getFontMetrics(g.getFont());
            return fm.getMaxAscent();
        }

        public Dimension getPreferredSize()
        {
            return new Dimension(100,100);
        }

        public Dimension getMinimumSize()
        {
            return getPreferredSize();
        }

        public Dimension getMaximumSize()
        {
            return new Dimension(1000,1000);
        }

        public void paint(Graphics _g)
        {
            Graphics2D g = (Graphics2D) _g;
            int width = getWidth(), height = getHeight();

            Font fb = new Font("Monospaced", Font.BOLD, 12);
            Font fp = new Font("Monospaced", Font.PLAIN, 12);
            g.setFont(fb);
            int textHeight = getStringHeight(g, "");

            ////////////////////////////////////////////
            // Draw backgrounds/outlines

            g.setColor(getBackground());
            g.fillRect(0, 0, width, height);

            g.setColor(Color.black);
            g.drawRect(0, 0, width-1, height-1);

            double totalColumnWidthWeight = 0;
            for (int i = 0; i < columnWidthWeight.length; i++)
                totalColumnWidthWeight += columnWidthWeight[i];

            int columnPos[] = new int[columnWidthWeight.length];
            int columnWidth[] = new int[columnWidthWeight.length];
            for (int i = 0; i < columnWidth.length; i++)
                columnWidth[i] = (int) (width * columnWidthWeight[i]/totalColumnWidthWeight);

            for (int i = 1; i < columnWidth.length; i++) {
                columnPos[i] = columnPos[i-1] + columnWidth[i-1];
            }

            ////////////////////////////////////////////
            // draw column headings
            g.setColor(Color.black);
            for (int i = 0; i < columnNames.length; i++) {
                String s = columnNames[i];
                g.drawString(s, columnPos[i]+columnWidth[i]/2 - getStringWidth(g, s)/2, textHeight*3/2);
            }

            ////////////////////////////////////////////
            // draw cells
            g.setFont(fp);
            for (int i = 0; i < values.length; i++) {
                for (int j = 0; j < columnNames.length; j++) {
                    String s = values[i][j];
                    if (s == null)
                        s = "";

                    g.drawString(s, columnPos[j]+columnWidth[j]/2 - getStringWidth(g, s)/2, textHeight*(3+i));
                }
            }

        }
    }
}
