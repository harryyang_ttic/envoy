package orc.controller;

import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;

import orc.*;

import java.io.*;

import lcm.lcm.*;
//import lcmtypes.*;
import erlcm.*;

/** Sits in between the base module and the orc**/
public class PortTester implements LCMSubscriber 
{
    Orc orc;
    Servo servo_tv, servo_rv;
    DigitalOutput digital_override; 
    LCM lcm;

    public static void main(String args[])
    {
	try {
	    new PortTester(args);
	}
	catch (IOException ex) {
	    System.out.println("LCM IO Exception: " + ex);
	}
    }

    public PortTester(String args[])
	throws IOException
    {
        if (args.length > 0)
            orc = Orc.makeOrc(args[0]);
        else
            orc = Orc.makeOrc();
	//Servo(Orc orc, int port, double pos0, int usec0, double pos1, int usec1)
	servo_rv = new Servo(orc, 8, 0, 0, 0, 0);
	servo_tv = new Servo(orc, 9, 0, 0, 0, 0);
	digital_override = new DigitalOutput(orc, 10);
	DigitalOutput digital_override1 = new DigitalOutput(orc, 1);
	DigitalOutput digital_override2 = new DigitalOutput(orc, 2);
	DigitalOutput digital_override3 = new DigitalOutput(orc, 3);
	DigitalOutput digital_override4 = new DigitalOutput(orc, 4);
	DigitalOutput digital_override5 = new DigitalOutput(orc, 5);
	DigitalOutput digital_override6 = new DigitalOutput(orc, 6);
	DigitalOutput digital_override7 = new DigitalOutput(orc, 7);
	/*for (int i = 9; i < 16; i++){
	    DigitalOutput d = new DigitalOutput(orc, i);
	    d.setValue(false);
	    }*/
	lcm = new LCM();
	lcm.subscribe("ORC_PWM_CMD", this);

	boolean v = false;
	
	/*digital_override.setValue(v); //works - seems to 
	digital_override1.setValue(v); //no
	digital_override2.setValue(v);//not working
	digital_override3.setValue(v);//no
	digital_override4.setValue(v);//not working 
	digital_override5.setValue(v);
	digital_override6.setValue(v);*/
	digital_override7.setValue(false);//not working
	servo_rv.setPulseWidth(0);
	servo_tv.setPulseWidth(700);
	new StatusPollThread().start();
    } 	

    class StatusPollThread extends Thread
    {
        StatusPollThread()
        {//since the daemon is not started - 
	    //this will cause it to exit after one loop - find out how the daemon can be started 
            //setDaemon(true);
        }

        public void run()
        {	    
            while(true) {
                try {
                    OrcStatus status = orc.getStatus();
		    System.out.printf(".");		   
                    orcStatus(orc, status);
                    Thread.sleep(20);  //might need to play with this 
                } catch (Exception ex) {
		    System.out.printf("Exception\n");
                    System.out.println("StatusPollThread ex: "+ex);
                }		
            }
        }
    }

    boolean firstStatus = true;

    double lastTime = 0.0;

    int last_left = 0, last_right = 0;

    public void orcStatus(Orc orc, OrcStatus status)
    {
        // if uorc resets...
        if (status.utimeOrc < 2000000)
            firstStatus = true;


	orc_stat_msg_t msg = new orc_stat_msg_t();
	
	msg.utime = status.utimeOrc; 
	
	System.arraycopy(status.qeiPosition,0,msg.qei_position ,0,2);

	if(last_left !=0){
	    if(status.qeiPosition[0] - last_left >=0){
		msg.qei_velocity[0] = status.qeiVelocity[0];
	    }
	    else{
		msg.qei_velocity[0] = -status.qeiVelocity[0];
	    }	    
	}
	if(last_right !=0){
	    if(status.qeiPosition[1] - last_right >=0){
		msg.qei_velocity[1] = status.qeiVelocity[1];
	    }
	    else{
		msg.qei_velocity[1] = -status.qeiVelocity[1];
	    }	    
	}
	
	last_left = status.qeiPosition[0];
	last_right = status.qeiPosition[1];
	//System.arraycopy(status.qeiVelocity,0,msg.qei_velocity ,0,2);
	//msg.qei_velocity = status.qeiVelocity;

	lcm.publish ("BASE_POS_STAT", msg); 

	// = new double[] { 1, 2, 3 };

	/*char x[50];*/
	/*System.out.printf("Frequency : %f\n", 1.0/(status.utimeOrc/1000000.0 - lastTime));

	System.out.printf("[%.6f] Encoder 0 Pos : %6d\n",status.utimeOrc/1000000.0, status.qeiPosition[0]);
	System.out.printf("[%.6f] Encoder 1 Pos : %6d\n",status.utimeOrc/1000000.0, status.qeiPosition[1]);

	System.out.printf("[%.6f] Encoder 0 Vel : %6d\n",status.utimeOrc/1000000.0, status.qeiVelocity[0]);
	System.out.printf("[%.6f] Encoder 1 Vel : %6d\n",status.utimeOrc/1000000.0, status.qeiVelocity[1]);*/
	
	lastTime = status.utimeOrc/1000000.0;

	//servo.setPulseWidth(700);

	firstStatus = false;
    }

    public void messageReceived(LCM lcm, String channel, LCMDataInputStream ins)
    {
	try {
	    if (channel.equals("ORC_PWM_CMD")) { //pwm velocity command
		//issue the command to the correct port 
		orc_pwm_msg_t cmd = new orc_pwm_msg_t(ins);

		int duration = (int) (cmd.value * 3750.0); 
		//make sure its within the range  
		duration = Math.max(Math.min(duration,3750),0);

		System.out.print("C");
		//max interval seems to be 3750 on the uOrc Spy 
		//tv - 0 ;  rv - 1; override - 2 ;
		if (cmd.port_id == 0){
		    //servo_tv.setPulseWidth(duration);
		    System.out.printf("T - : %d\n", duration);
		}
		else if (cmd.port_id == 1){
		    //servo_rv.setPulseWidth(duration);
		    //System.out.print("r");
		    System.out.printf("R - : %d\n", duration);
		}
		/*else if (cmd.port_id == 2){
		    servo_override.setPulseWidth(duration);
		    System.out.print("O");
		    }*/
	    }
	} catch (IOException ex) {
	    System.out.println("Exception: " + ex);
	}
    }

}

