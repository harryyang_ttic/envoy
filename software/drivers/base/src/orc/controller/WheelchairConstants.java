package orc.controller;
import java.lang.Math.*;

interface WheelchairConstants
{
    public static final int ORC_ROT_MOTOR_PORT = 1;
    public static final int ORC_TRANS_MOTOR_PORT = 0;
    public static final int ORC_JOYSTICK_OVERRIDE_PORT = 0; //slow port

    public static final int ORC_COMPUTER_CNTL = 1;
    public static final int ORC_JOYSTICK_CNTL = 0;


    public static final int ORC_LEFT_ENCODER_PORT =0;
    public static final int ORC_RIGHT_ENCODER_PORT = 1;
 
    public static final int ORC_LEFT_REVERSE_ENCODER = -1;
    public static final int ORC_RIGHT_REVERSE_ENCODER = -1;

    public static final double[] ORC_MAX_ROT_VEL = {0.51, 0.8 , 0.99 , 1.11, 1.21}; // Radians / seconds
    public static final double[] ORC_MAX_TRANS_VEL = {0.28, 0.55 , 0.84, 1.10 ,1.4}; // meters / seconds    

    //setting the last range as high - so that it captures high commands as well 
    public static final double[] ORC_BEST_ROT_RANGE = {0.4, 0.7 , 0.9 , 1.05, 3.0}; // Radians / seconds
    //public static final double[] ORC_BEST_ROT_RANGE = {0.0, 0.0 , 0.9 , 1.05, 3.0}; // Radians / seconds
    public static final double[] ORC_BEST_TRANS_RANGE = {0.25, 0.50 , 0.75, 1.0 ,2.0};//1.0 // meters / seconds
    //public static final double[] ORC_BEST_TRANS_RANGE = {0.0, 0.0 , 0.75, 1.0 ,2.0};//1.0 // meters / seconds
    
    
    public static final double ACCELERATION = 0.3;
    public static final double DECELERATION = 0.3;

    public static final double TV_ACCELERATION = 1.0;
    public static final double TV_DECELERATION = 1.0;

    public static final double RV_ACCELERATION = 5.0;
    public static final double RV_DECELERATION = 5.0; 
    
    //approx
    public static final double DELTA_T = 1/ 40.0; 

    public static final double[] ORC_TRANS_PWM_ZERO = {0.56,0.56,0.56,0.56,0.56};  // for the adjusted 
    public static final double[] ORC_ROT_PWM_ZERO  = {0.56,0.56,0.56,0.56,0.56};  // for the adjusted output -used to be 0.56 - compensation for forward motion
    public static final double[] ORC_ROT_PWM_ZERO_FORWARD  = {0.56,0.56,0.56,0.56,0.56};  // for the adjusted output -used to be 0.56 - compensation for forward motion
    //{0.545,0.545,0.545,0.545,0.545};
    public static final double[] ORC_ROT_PWM_ZERO_REVERSE  = {0.56,0.56,0.56,0.56,0.56};  // for the adjusted output -used to be 0.56 - compensation for forward motion//{0.575,0.575,0.575,0.575,0.575};
    public static final double[] ORC_TRANS_PWM_MAX = {0.85,0.85,0.85,0.85,0.85};  // for the adjusted 
    public static final double[] ORC_ROT_PWM_MAX = {0.85,0.85,0.85,0.85,0.85};  // for the adjusted output
    public static final double[] ORC_TRANS_PWM_MIN = {0.20,0.20,0.20,0.20,0.20};  // for the adjusted output
    public static final double[] ORC_ROT_PWM_MIN = {0.20,0.20,0.20,0.20,0.20};  // for the adjusted output
    public static final int[] ORC_ROT_REVERSE = {-1,-1,-1,-1,-1};
    public static final int[] ORC_TRANS_REVERSE = {1,1,1,1,1};


    //these worked for value at setting 3 
    /*public static final double T_ORC_P_GAIN = 0.2;//2.0 worked for translational//1.5; //20
    public static final double T_ORC_D_GAIN = 3.0;//0.5;  //5
    public static final double T_ORC_I_GAIN = 0;//0.5;//1.0;  //0*/

    //these worked for value at setting 4 

    //the P gains seem not to be enough - looking at the commanded velocities and the achieved ones - tune further and higher 
    //rise time is too small 

    //public static final double[] T_ORC_P_GAIN = {.013,0.01,0.01,.01,.01};//2.0 worked for translational//1.5; //20
    public static final double[] T_ORC_P_GAIN = {1.3, 1.0, 1.0, 1.0, 1.0};//2.0 worked for translational//1.5; //20
    //public static final double[] T_ORC_D_GAIN = {.01, 0.01, .01,.01,.01};//0.5;  //5
    public static final double[] T_ORC_D_GAIN = {1.0, 1.0, 1.0, 1.0, 1.0};//0.5;  //5
    //public static final double[] T_ORC_I_GAIN = {.006,0.002,.0025,.0025,.0025};//0.5;//1.0;  //0
    //public static final double[] T_ORC_I_GAIN = {0.6, 0.22, 0.25, 0.25, 0.25};//0.5;//1.0;  //0
    //public static final double[] T_ORC_I_GAIN = {0.1, 0.02, 0.025, 0.025, 0.025};//0.5;//1.0;  //0
    public static final double[] T_ORC_I_GAIN = {2.0, 2.0, 2.0, 2.0, 2.0};//0.5;//1.0;  //0


    //public static final double[] R_ORC_P_GAIN = {0.2, 0.2, 0.2, 0.2, 0.2};//{.005,0.005,0.005,0.005,0.005};//2.0 worked for translational//1.5; //20

    //public static final double[] R_ORC_D_GAIN = {2.0,2.0,2.0,2.0,2.0};//{.01,.01,.01,.01,.01};//0.5;  //5

    public static final double r_orc_p_gain = 0.2;
    public static final double r_orc_d_gain = 5.0;//3.0;
    public static final double r_orc_i_gain = 2.0;
    public static final double[] R_ORC_P_GAIN = {r_orc_p_gain, r_orc_p_gain, r_orc_p_gain, r_orc_p_gain, r_orc_p_gain};//{.005,0.005,0.005,0.005,0.005};//2.0 worked for translational//1.5; //20

    public static final double[] R_ORC_D_GAIN = {r_orc_d_gain, r_orc_d_gain, r_orc_d_gain, r_orc_d_gain, r_orc_d_gain};//{.01,.01,.01,.01,.01};//0.5;  //5


    public static final double[] R_ORC_I_GAIN = {r_orc_i_gain, r_orc_i_gain, r_orc_i_gain, r_orc_i_gain, r_orc_i_gain}; //{.006,.006,.006,.006,.006};  //0
    
    //this is not being used
    /*public static final double ORC_P_GAIN = 1.5; //20
    public static final double ORC_D_GAIN = 0.5;  //5
    public static final double ORC_I_GAIN = 0.0;  //0  
    */
    
    // Maximum difference between new and previous commands above which controller variables (e.g., integral term) reset.
    public static final double ORC_TRANS_RESET_THRESH = 0.05;
    public static final double ORC_ROT_RESET_THRESH = 0.05;

    public static final double ORC_WHEEL_DIAMETER = 0.255;//0.26; // meters
    public static final double ORC_WHEEL_BASE = 0.51;
    public static final double ORC_TICKS_PER_REVOLUTION = 137613.8; //double check this one
}