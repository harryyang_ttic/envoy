package orc.controller;

import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;

import orc.*;

import java.io.*;

import lcm.lcm.*;
import erlcm.*;

/** Sits in between the base module and the orc**/
public class Base implements LCMSubscriber 
{
    Orc orc;
    Servo servo_tv, servo_rv;
    DigitalOutput digital_override; 
    LCM lcm;
    double rv_duty_cycle = 0.0;
    double tv_duty_cycle = 0.0;

    public static void main(String args[])
    {
	try {
	    new Base(args);
	}
	catch (IOException ex) {
	    System.out.println("LCM IO Exception: " + ex);
	}
	catch (InterruptedException ex){
	    System.out.println("Thread Sleep Exception "+ ex);
	}
    }

    public Base(String args[])
	throws IOException, InterruptedException
    {
        if (args.length > 0)
            orc = Orc.makeOrc(args[0]);
        else
            orc = Orc.makeOrc();

	servo_tv = new Servo(orc, 0, 0, 0, 0, 0);
	servo_rv = new Servo(orc, 1, 0, 0, 0, 0);
	digital_override = new DigitalOutput(orc, 0);
	
	//set this to zero before we begin 
	digital_override.setValue(false);	
	Thread.sleep(100);
	//set the mean values // these should be reffered to from some single class - constants 
	servo_tv.setPWM(1500,0.565);
	Thread.sleep(100);
	servo_rv.setPWM(1500,0.565);
	Thread.sleep(100);
	digital_override.setValue(true);	

	lcm = new LCM();
	lcm.subscribe("ORC_PWM_CMD", this);
	
	digital_override.setValue(true);

	new StatusPollThread().start();
    } 	

    public void finalize() {
        System.out.println("Shutting Down Handler");
	digital_override.setValue(false);
    }

    class StatusPollThread extends Thread
    {
        StatusPollThread()
        {//since the daemon is not started - 
	    //this will cause it to exit after one loop - find out how the daemon can be started 
	    //this might work now that we have up to date firmware
            //setDaemon(true);
        }

        public void run()
        {	    
            while(true) {
                try {
                    OrcStatus status = orc.getStatus();
		    System.out.printf(".");		   
                    orcStatus(orc, status);
                    Thread.sleep(20);  //might need to play with this 
                } catch (Exception ex) {
		    System.out.printf("Exception\n");
                    System.out.println("StatusPollThread ex: "+ex);
                }		
            }
        }
    }

    boolean firstStatus = true;

    double lastTime = 0.0;

    int [] last = {0, 0};
    
    public void orcStatus(Orc orc, OrcStatus status)
    {
        // if uorc resets...
        if (status.utimeOrc < 2000000)
            firstStatus = true;

	orc_full_stat_msg_t msg = new orc_full_stat_msg_t();
		
	msg.utime = status.utimeOrc; 
	
	System.arraycopy(status.qeiPosition,0,msg.qei_position ,0,2);
	
        for (int i = 0; i < 2; i++) {
	    if(last[i] != 0){
		if(status.qeiPosition[i] - last[i] < 0){
		    msg.qei_velocity[i] = -status.qeiVelocity[i];
		}
		else{
		    msg.qei_velocity[i] = status.qeiVelocity[i];
		}
	    }
	    last[i] = status.qeiPosition[i];	    
        }
	
	msg.cmd_velocity[0] = tv_duty_cycle;
	msg.cmd_velocity[1] = rv_duty_cycle;

	lcm.publish ("BASE_FULL_STAT", msg); 

	firstStatus = false;
	lastTime = status.utimeOrc/1000000.0;
    }

    public void messageReceived(LCM lcm, String channel, LCMDataInputStream ins)
    {
	try {
	    if (channel.equals("ORC_PWM_CMD")) { //pwm velocity command
		//issue the command to the correct port 
		orc_pwm_msg_t cmd = new orc_pwm_msg_t(ins);

		System.out.print("C");

		//either make sure that the values are of the correct range, or make sure that the correct ones are sent

		if (cmd.port_id == 0){
		    tv_duty_cycle = cmd.value;
		    servo_tv.setPWM(1500,tv_duty_cycle);
		    System.out.printf("T - : %f\n", tv_duty_cycle);
		}
		else if (cmd.port_id == 1){
		    rv_duty_cycle = cmd.value;
		    servo_rv.setPWM(1500,rv_duty_cycle);
		    System.out.printf("R - : %f\n", rv_duty_cycle);
		}
	    }
	} catch (IOException ex) {
	    System.out.println("Exception: " + ex);
	}
    }    

}

