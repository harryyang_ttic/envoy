package orc.controller;

import java.awt.*;
import java.lang.Math;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import lcm.lcm.*;
import erlcm.*;
import bot_core.*;
import orc.spy.*;
import java.util.Date;
import lcm.spy.ChannelData;
import lcm.util.ParameterGUI;
import lcm.util.ParameterListener;
import lcm.util.ColorMapper;
import lcm.lcm.LCMDataInputStream;

import java.lang.Math.*;

import orc.*;

/** A GUI for visualizing and interacting with the uOrc. **/
public class WheelchairFullGUI implements LCMSubscriber, WheelchairConstants 
{

    JFrame jf;
    
    JDesktopPane jdp = new JDesktopPane();

    TextPanelWidget basicDisplay, qeiDisplay;
    ServoPanel servoPanels[];
    DigitalOutput vel_increase, vel_decrease;     
    
    RobotLaserPane robotFrame;
    Orc orc;
    Servo servo_tv, servo_rv;
    DigitalOutput digital_override; 
    LCM lcm;
    double rv_duty_cycle = 0.0;
    double tv_duty_cycle = 0.0;

    double prev_tv_request = 0.0;
    double prev_rv_request = 0.0;

    boolean shift_velocity = false;

    long systemTimeStart = 0;
    long orcTimeStart = 0; 
    ParameterGUI pg;

    // When the system starts, record the time in nano seconds as reported by
    // System.nanoTime(), which is relative to an arbitrary reference, and the corresponding
    // time in milliseconds since the epoch
    long systemStartTime_MSecSinceEpoch = 0;
    long systemStartTime_NSecReference = 0;

    int robot_state_last = robot_status_t.STATE_UNDEFINED;

    boolean verbose = false;
    boolean js_active = false;
    VelocityRange currentVelocity = VelocityRange.MAX_VEL_20;

    public enum VelocityRange {
        MAX_VEL_20,
        MAX_VEL_40,
        MAX_VEL_60,
        MAX_VEL_80,
        MAX_VEL_100;
        
        public static VelocityRange getLevelUp(VelocityRange current){
            if(current.equals(MAX_VEL_20)){
                return MAX_VEL_40;
            }
            if(current.equals(MAX_VEL_40)){
                return MAX_VEL_60;
            }
            if(current.equals(MAX_VEL_60)){
                return MAX_VEL_80;
            }
            if(current.equals(MAX_VEL_80)){
                return MAX_VEL_100;
            }
            else{
                return current;
            }
        }

        public static VelocityRange getLevelDown(VelocityRange current){
            if(current.equals(MAX_VEL_100)){
                return MAX_VEL_80;
            }
            if(current.equals(MAX_VEL_80)){
                return MAX_VEL_60;
            }
            if(current.equals(MAX_VEL_60)){
                return MAX_VEL_40;
            }
            if(current.equals(MAX_VEL_40)){
                return MAX_VEL_20;
            }
            else{
                return current;
            }
        }

        public static VelocityRange getLevel(int value){
            switch(value){
            case 0: return MAX_VEL_20; 
            case 1: return MAX_VEL_40; 
            case 2: return MAX_VEL_60; 
            case 3: return MAX_VEL_80; 
            case 4: return MAX_VEL_100; 
            default : return MAX_VEL_20; 
            }
        }

        public static byte getLevelInt(VelocityRange current){
            if(current.equals(MAX_VEL_100)){
                return 4;
            }
            if(current.equals(MAX_VEL_80)){
                return 3;
            }
            if(current.equals(MAX_VEL_60)){
                return 2;
            }
            if(current.equals(MAX_VEL_40)){
                return 1;
            }
            if(current.equals(MAX_VEL_20)){
                return 0;
            }
            else{
                return -1;
            }
        }
    }    
       

    public static void main(String args[])
    {
        try{
            new WheelchairFullGUI(args);
        }
        catch (IOException ex) {
            System.out.println("LCM IO Exception: " + ex);
        }
        catch (InterruptedException ex){
            System.out.println("Thread Sleep Exception "+ ex);
        }
    }

    public WheelchairFullGUI(String args[])
        throws IOException, InterruptedException
    {
        if (args.length > 0)
            orc = Orc.makeOrc(args[0]);
        else
            orc = Orc.makeOrc();

        jf = new JFrame("Wheelchair : "+orc.getAddress());
        jf.setLayout(new BorderLayout());
        jf.add(jdp, BorderLayout.CENTER);

        basicDisplay = new TextPanelWidget(new String[] {"Parameter", "Value"},
                                           new String[][] {{"uorc time", "0"}},
                                           new double[] {.4, .6});

        qeiDisplay = new TextPanelWidget(new String[] {"Port", "Position", "Velocity"},
                                         new String[][] {{"0", "0", "0"},
                                                         {"1", "0", "0"}},
                                         new double[] {.3, .5, .5});

        servo_tv = new Servo(orc, 0, 0, 0, 0, 0);
        servo_rv = new Servo(orc, 1, 0, 0, 0, 0);

        //ports to increase and decrease velocity

        vel_increase = new DigitalOutput(orc, 3); 
        vel_decrease = new DigitalOutput(orc, 2); 

        JPanel servoDisplay = new JPanel();
        servoDisplay.setLayout(new GridLayout(2,1));
        servoPanels = new ServoPanel[2];
        for (int i = 0; i < servoPanels.length; i++) {
            if(i==0){
                servoPanels[i] = new ServoPanel(i, servo_tv);
            }
            else if(i==1){
                servoPanels[i] = new ServoPanel(i, servo_rv);
            }
            JPanel jp = new JPanel();
            jp.setLayout(new BorderLayout());
            if(i == 0){
                jp.add(new JLabel("Tv "), BorderLayout.WEST);
            }
            else{
                jp.add(new JLabel("Rv "), BorderLayout.WEST);
            }
            jp.add(servoPanels[i], BorderLayout.CENTER);
            servoDisplay.add(jp);
        }

        jf.setSize(500,800);
        jf.setVisible(true);
        
        pg = new ParameterGUI();

        pg.addCheckBoxes("volume", "Show volume", true,
                         "set_goal", "Goal Click", false,
                         "velocity_stat", "Draw Velocity", true,
                         "velocity_cmd", "Draw Vel Cmd", true,
                         "rear_laser", "Show Rear Laser", true,
                         "pessimistic", "Pessimistic", false,
                         "normalized_intensities", "Intensity [0,1]", false,
                         "intensity", "Display intensity", false);

        //Add Buttons to Stop etc
        pg.addButtons("stop_robot","Stop Robot");
        lcm = new LCM();
        robotFrame = new  RobotLaserPane(pg, lcm);
    
        pg.addListener(new ParameterListener() {
                public void parameterChanged(String name)
                {
                    if(name.compareTo("stop_robot")==0){
                        robotFrame.stop_robot();
                    }
                }
            });
    
        makeRobotInternalFrame("RobotLaser", robotFrame, 500, 500);
        makeInternalFrame("Basic Status", basicDisplay, 300, 100);
        makeInternalFrame("Quadrature Decoders", qeiDisplay, 300, 120);
        makeInternalFrame("Joystick Simulation", servoDisplay, 300, 120);
    
    
        digital_override = new DigitalOutput(orc, 0);
    
        //set this to zero before we begin 
        digital_override.setValue(false);    
        Thread.sleep(20);
        //set the mean values // these should be reffered to from some single class - constants 
        servo_tv.setPWM(1500,0.565);
        Thread.sleep(20);
        servo_rv.setPWM(1500,0.565);
        Thread.sleep(20);
        digital_override.setValue(true);

        //Initialization step for the speed controller 
    
        vel_increase.setValue(true);
        Thread.sleep(200);
    
        //Loop this 4 times to make sure its initialized at 20 speed - then 
        //bring it up to the level needed
        for(int i = 0; i < 4; i++) {
            shiftVelDown();
        }


        //shift up to 80%
        for(int i = 0; i < 3; i++) {
            shiftVelUp();
        }

        /*vel_increase.setValue(false);
          Thread.sleep(200);*/
    
    
        // When the system starts, record the time in nano seconds as reported by
        // System.nanoTime(), which is relative to an arbitrary reference, and the corresponding
        // time in milliseconds since the epoch
        systemStartTime_MSecSinceEpoch = System.currentTimeMillis();
        systemStartTime_NSecReference = System.nanoTime();

        lcm.subscribe("ROBOT_VELOCITY_CMD",this);
        lcm.subscribe("SHIFT_VELOCITY_CMD",this);
        lcm.subscribe("ROBOT_VELOCITY_CMD_JS",this);
        lcm.subscribe("ROBOT_STATUS",this);

        digital_override.setValue(true);

        //adding shutdown hook
        Runtime rt = Runtime.getRuntime();

        rt.addShutdownHook(new Thread() {
                public void run() {
                    System.out.println("Shutting Down Handler");
                    digital_override.setValue(false);
                }
            });
    
        new StatusPollThread().start();

        new UpdateVelocityThread().start();


        jf.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    //add code that sets things to zero
                    shutdown();
                    System.exit(0);
                }});


    }


    private void resetErrors(){
        s_trans_error_prev = 0;
        trans_i_error = 0;
        s_rot_error_prev = 0;
        rot_i_error = 0;
    }

    private void resetIErrors(){
        trans_i_error = 0;
        rot_i_error = 0;
    }
    

    private void shiftVelDown() {
        try {
            vel_decrease.setValue(true);
            Thread.sleep(50);
            vel_decrease.setValue(false);
            Thread.sleep(50);
            vel_decrease.setValue(true);
            Thread.sleep(50);        
            //this is gurenteed to stop at 20%
            currentVelocity = VelocityRange.getLevelDown(currentVelocity);
            //reset values 
            resetErrors();
            //currentVelocity.printVelocity();
        } catch (InterruptedException e) {
            System.err.println("in shiftVelDown:" + e.getMessage());
        }
    }

    private void shiftVelUp() {
        try {
            vel_increase.setValue(true);
            Thread.sleep(50);
            vel_increase.setValue(false);
            Thread.sleep(50);
            vel_increase.setValue(true);
            Thread.sleep(50);
            //this is gurenteed to stop at 100%
            currentVelocity = VelocityRange.getLevelUp(currentVelocity);
            resetErrors();
            //currentVelocity.printVelocity();
        } catch (InterruptedException e) {
            System.err.println("in shiftVelUp:" + e.getMessage());
        }
    }

    private void shiftToVelocityLevel(VelocityRange target) {
        if(currentVelocity.ordinal() != target.ordinal()){
            System.out.printf("Not at the desired level\n");
            if(currentVelocity.ordinal() < target.ordinal()){         
                if(verbose)
                    System.out.printf("Shifting Up\n");
                while(currentVelocity.ordinal() < target.ordinal()){
                    shiftVelUp();             
                }
            }  
            else if(currentVelocity.ordinal() > target.ordinal()){    
                if(verbose)
                    System.out.printf("Shifting Down\n");
                while(currentVelocity.ordinal() > target.ordinal()){
                    shiftVelDown();             
                }
            }
            else{
                if(verbose)
                    System.out.printf("No Change\n");         
            }
        }
    }
    

    int xpos = 0, ypos = 0;
    void makeInternalFrame(String name, JComponent jc, int width, int height)
    {
        JInternalFrame jif = new JInternalFrame(name, true, true, true, true);
        jif.setLayout(new BorderLayout());
        jif.add(jc, BorderLayout.CENTER);
        if (ypos+height >= jf.getHeight()) {
            ypos = 0;
            xpos += width;
        }
        jif.reshape(xpos, ypos, width, height);
        ypos += height;

        jif.setVisible(true);
        jdp.add(jif);
        jdp.setBackground(Color.blue);
    }

    void makeRobotInternalFrame(String name, RobotLaserPane jc, int width, int height)
    {
        JInternalFrame jif = new JInternalFrame(name, true, true, true, true);
        jif.setLayout(new BorderLayout());
        jif.add(jc, BorderLayout.CENTER);
        jif.add(jc.pg.getPanel(), BorderLayout.SOUTH);
        if (ypos+height >= jf.getHeight()) {
            ypos = 0;
            xpos += width;
        }
        jif.reshape(xpos, ypos, width, height);
        ypos += height;

        jif.setVisible(true);
        jdp.add(jif);
        jdp.setBackground(Color.blue);
    }


    protected void finalize() throws Throwable {
        try {
            System.out.println("Shutting Down Handler");
            digital_override.setValue(false);
        
        } finally {
            super.finalize();
        }
    }   

    class StatusPollThread extends Thread
    {
        StatusPollThread()
        {
            setDaemon(true);
        }


        public void run()
        {        
            while(true) {
                try {
                    //System.out.printf("---------------------------------------------------\n");
                    OrcStatus status = orc.getStatus();
                    //System.out.printf("Updating Status");           
                    orcStatus(orc, status);

                    //maybe we should do this in two threads???
                    //setVelocity(current_tv_cmd, current_rv_cmd);
                    Thread.sleep(20);//was 20 //might need to play with this 
                } catch (Exception ex) {
                    System.out.printf("Exception\n");
                    System.out.println("StatusPollThread ex: "+ex);
                }        
            }
        }        
    }

    class UpdateVelocityThread extends Thread
    {
        UpdateVelocityThread()
        {
            setDaemon(true);
        }


        public void run()
        {        
            while(true) {
                try {                    
                    //System.out.printf("Setting Velocity");
                    //maybe we should do this in two threads???
                    // motion timeout
                    double delta_time = Math.abs(System.currentTimeMillis()/1000 - last_velocity_command_sec);
                    if (delta_time > 1) {
                        current_tv_cmd = 0.0;
                        current_rv_cmd = 0.0;
                    }
                    setVelocity(current_tv_cmd, current_rv_cmd);
                    Thread.sleep(20);//was 20 //might need to play with this 
                } catch (Exception ex) {
                    System.out.printf("Exception\n");
                    System.out.println("StatusPollThread ex: "+ex);
                }        
            }
        }        
    }


    public void shutdown()
    {
        digital_override.setValue(false);
    }

    public int setDeceleration(double deceleration){
        s_deceleration = deceleration;
        return 0;
    }

    public int setVelocity(double new_tv, double new_rv){
        //reset the d_error if there is a large sift in the desired velocity
        if (Math.abs(new_tv - s_desired_trans_velocity) > ORC_TRANS_RESET_THRESH) {
            s_trans_error_prev = 0.0;
            s_ignore_trans_d_term = 1;
            trans_i_error = 0.0;
            s_trans_time_millisec_prev = -1;
        }
        if (Math.abs(new_rv - s_desired_rot_velocity) > ORC_ROT_RESET_THRESH) {
            s_rot_error_prev = 0.0;
            s_ignore_trans_d_term = 1;
            rot_i_error = 0;
            s_rot_time_millisec_prev = -1;
        }
        s_desired_trans_velocity = new_tv;
        s_desired_rot_velocity = new_rv;

        this.commandVelocity(s_desired_trans_velocity, 
                             s_actual_trans_velocity, ORC_TRANS_MOTOR_PORT);
        this.commandVelocity(s_desired_rot_velocity, 
                             s_actual_rot_velocity, ORC_ROT_MOTOR_PORT);
        //this is not fully correct - fix 
        /*int tv_shift_up = 0;
          int rv_shift_up = 0;

          //call a function that decided whether we need to shift up/down and to what level 
    
        
          if( new_tv - ORC_MAX_TRANS_VEL[currentVelocity.ordinal()] > 0){
          tv_shift_up = 1;     
        
          }
          if(new_rv < 0 && (new_rv + ORC_MAX_ROT_VEL[currentVelocity.ordinal()]) < 0){
          rv_shift_up = 1;         
          }
          if(new_rv > 0 && (new_rv - ORC_MAX_ROT_VEL[currentVelocity.ordinal()]) > 0){
          rv_shift_up = 1;
          }
          if(rv_shift_up==1 || tv_shift_up ==1){
          shiftVelUp();
          System.out.printf("----------Shifting Up--------- %d : %f, %d : %f\n", tv_shift_up, ORC_MAX_TRANS_VEL[currentVelocity.ordinal()], rv_shift_up, ORC_MAX_ROT_VEL[currentVelocity.ordinal()]);
          }*/

        if(shift_velocity){
            VelocityRange target = makeVelocityShift(new_tv, new_rv);
            shiftToVelocityLevel(target);
        }
    
        return 0;
    }

    private VelocityRange makeVelocityShift(double tv, double rv){    
        int tv_range = -1, rv_range = -1; 

        if(tv < 0){
            for(int i=0; i < 5; i++){
                if(tv > - ORC_BEST_TRANS_RANGE[i]){
                    tv_range = i;
                    break;
                }
            }
        }
        else if(tv >0){
            for(int i=0; i < 5; i++){
                if(tv  < ORC_BEST_TRANS_RANGE[i]){
                    tv_range = i;
                    break;
                }
            }
        }
        else{
            tv_range = currentVelocity.ordinal();
        }

        if(rv < 0){
            for(int i=0; i < 5; i++){
                if(rv > - ORC_BEST_ROT_RANGE[i]){
                    rv_range = i;
                    break;
                }
            }
        }
        else if(rv >0){
            for(int i=0; i < 5; i++){
                if(rv  < ORC_BEST_ROT_RANGE[i]){
                    rv_range = i;
                    break;
                }
            }
        }    
        else{
            rv_range = currentVelocity.ordinal();
        }

        if(currentVelocity.ordinal() != tv_range || currentVelocity.ordinal() != rv_range){
            if(verbose)
                System.out.printf("+++++++++ Need to Shift +++++++++\n");
            int target_range = Math.max(tv_range, rv_range);
            //this selection criterion doesnt seem all that correct - need to test and think more 
            if(verbose)
                System.out.printf("Current Setting : %d Target : %d\n", currentVelocity.ordinal(), target_range);
            return VelocityRange.getLevel(target_range);
        }                
        return currentVelocity; 
    }


    public void commandVelocity(double desired_velocity, double current_velocity, 
                                int MOTION_PORT){

        double pwm_zero;
        double pTerm = 0.0, dTerm = 0.0, iTerm = 0.0, correction = 0.0;
        double velocity_error_prev = 0.0;
        double velocity_error = 0.0;
        double command_velocity;
        int ignore_d_term; 
        double i_error = 0;

        double p_gain, d_gain, i_gain;
        double dt_sec = 0;

        // get the values for the appropriate motor
        if (MOTION_PORT == ORC_TRANS_MOTOR_PORT) {
            pwm_zero = ORC_TRANS_PWM_ZERO[currentVelocity.ordinal()];
            velocity_error_prev = s_trans_error_prev;
            command_velocity = s_trans_command;
            ignore_d_term = s_ignore_trans_d_term;

            if (s_trans_time_millisec_prev > 0)
                dt_sec = (System.currentTimeMillis() - s_trans_time_millisec_prev)/1000;
            
            s_trans_time_millisec_prev = System.currentTimeMillis();

            i_error = trans_i_error;
        } else {
            pwm_zero = ORC_ROT_PWM_ZERO[currentVelocity.ordinal()];
            velocity_error_prev = s_rot_error_prev;
            command_velocity = s_rot_command;
            ignore_d_term = s_ignore_rot_d_term;

            if (s_rot_time_millisec_prev > 0)
                dt_sec = (System.currentTimeMillis() - s_rot_time_millisec_prev)/1000;
            
            s_rot_time_millisec_prev = System.currentTimeMillis();

            i_error = rot_i_error;
            //this will track the inital error also - not a good measure
        
        }

        if (dt_sec < 0) {
            System.out.printf("Error: dt since last control loop is negative!\n");
            dt_sec = 0;
        }

        // ------ PID COMPUTATION ------- //
        if (Math.abs(desired_velocity) > .001) {
            // compute the PID terms
            velocity_error = desired_velocity - (current_velocity);// - 0.1);
            if (MOTION_PORT == ORC_TRANS_MOTOR_PORT) {
                p_gain = T_ORC_P_GAIN[currentVelocity.ordinal()];
                d_gain = T_ORC_D_GAIN[currentVelocity.ordinal()];
                i_gain = T_ORC_I_GAIN[currentVelocity.ordinal()];
                trans_i_error  += velocity_error * dt_sec;
                i_error = trans_i_error;
            }
            else{
                p_gain = R_ORC_P_GAIN[currentVelocity.ordinal()];
                d_gain = R_ORC_D_GAIN[currentVelocity.ordinal()];
                i_gain = R_ORC_I_GAIN[currentVelocity.ordinal()];
                rot_i_error += velocity_error * dt_sec;
                i_error = rot_i_error;
            }
        

            pTerm = velocity_error * (double)p_gain;
            //iTerm = velocity_error_prev * (double)i_gain;
            iTerm =  i_error * (double)i_gain;

            // Cap the integral gain to prevent windup
            if (MOTION_PORT == ORC_TRANS_MOTOR_PORT) {
                if (Math.abs(iTerm) > 0.5 * ORC_MAX_TRANS_VEL[currentVelocity.ordinal()])
                    iTerm = Math.signum(iTerm) * 0.5 * ORC_MAX_TRANS_VEL[currentVelocity.ordinal()];
            }
            else {
                if (Math.abs(iTerm) > 0.5 * ORC_MAX_ROT_VEL[currentVelocity.ordinal()])
                    iTerm = Math.signum(iTerm) * 0.5 * ORC_MAX_ROT_VEL[currentVelocity.ordinal()];
            }
        

            //we should change how the iterm is being calculated - if we are thinking of 
            //using it 

            double d_v_error = 0;

            if (!(ignore_d_term==1)){
                d_v_error = velocity_error - velocity_error_prev;
                dTerm = ( d_v_error) * (double)d_gain;
            }
            else {
                dTerm = 0;
                ignore_d_term = 0;
            }

            // change command pwm accordingly -- ?? -- sign?
            correction = pTerm + iTerm + dTerm;

            //System.out.printf("Correction : P: %f, I: %f, D: %f => %f (ve %f)\n", pTerm , iTerm ,  dTerm, correction, velocity_error);


            //we use the correction + prev_control 
            if (MOTION_PORT == ORC_TRANS_MOTOR_PORT) {//TV
                command_velocity = correction + prev_tv_request;// *0.8;
            }
            else{//RV
                command_velocity = correction + prev_rv_request;// *0.8;
            }


            command_velocity = 0.9*desired_velocity + correction;
            //command_velocity += 0.8*desired_velocity;
            /*if(desired_velocity * command_velocity < 0){
              System.out.print("clamping to zero\n");
              command_velocity = 0;
              }*/

            /*if(desired_velocity > 0){
                if(command_velocity < 0.8 * desired_velocity){
                    command_velocity = 0.8 * desired_velocity; 
                }            
            }
            else if(desired_velocity < 0){
                if(command_velocity > 0.8 * desired_velocity){
                    command_velocity = 0.8 * desired_velocity; 
                }            
            }
            */
        
            if(verbose)
                System.out.printf("P: (e) %f, (s) %f D: (e) %f (s) %f => %f Command Vel: %f\n", velocity_error, 
                                  pTerm ,  d_v_error, dTerm , correction, command_velocity);
            // update values for the appropriate motor
            if (MOTION_PORT == ORC_TRANS_MOTOR_PORT) {
                s_trans_error_prev = velocity_error;
                s_ignore_trans_d_term = ignore_d_term;
                s_trans_command = command_velocity;
            } else {
                s_rot_error_prev = velocity_error;
                s_ignore_rot_d_term = ignore_d_term;
                s_rot_command = command_velocity;
            }
        
            // below minimum velocity; command zero and clear error
        } else { 
            command_velocity = 0.0;
            p_gain = -1;
            d_gain = -1;
            i_gain = -1;
            
            // update values for the appropriate motor
            if (MOTION_PORT == ORC_TRANS_MOTOR_PORT) {
                s_trans_error_prev = 0.0;
                s_ignore_trans_d_term = ignore_d_term;
                s_trans_command= command_velocity;
            } else {
                s_rot_error_prev = 0.0;
                s_ignore_rot_d_term = ignore_d_term;
                s_rot_command = command_velocity;
            }

            //this should reset the control command to zero - we just want to stop 

        }

        // ---------------- SET PWM -------------- //
        double[] command_input = this.commandToPWM(command_velocity, MOTION_PORT);//, desired_velocity);

        double pwm_input = command_input[1];
    
    
        if (MOTION_PORT == 0){
            tv_duty_cycle = pwm_input;
            servo_tv.setPWM(1500,tv_duty_cycle);
            //System.out.printf("T - : %f, Command (P) : %f (C) : %f\n", tv_duty_cycle, prev_tv_request,command_input[0] );
            //System.out.printf("TV => Desired : %f, Actual : %f , PWM: %f, Command (P) : %f (C) : %f\n", 
            //              desired_velocity, current_velocity, tv_duty_cycle, prev_tv_request, command_velocity );
            prev_tv_request = command_input[0];
        }
        else if (MOTION_PORT == 1){
            rv_duty_cycle = pwm_input;
            servo_rv.setPWM(1500,rv_duty_cycle);
            //System.out.printf("R => Desired : %f, Actual : %f , PWM: %f Command (P) : %f (C) : %f\n", desired_velocity, 
            //               current_velocity, rv_duty_cycle, prev_rv_request, command_velocity );
            prev_rv_request = command_input[0];
        }    

        // Publish the velocity control aux message
        erlcm.orc_velocity_control_aux_t aux = new erlcm.orc_velocity_control_aux_t();
        aux.utime = systemStartTime_MSecSinceEpoch * 1000 + (System.nanoTime() - systemStartTime_NSecReference)/1000;
        //aux.velocity_level = (byte) currentVelocity.ordinal();
        aux.velocity_level = -1;
        aux.v_desired = desired_velocity;
        aux.v_error = velocity_error;
        aux.v_error_prev = velocity_error_prev;
        aux.p_gain = p_gain;
        aux.d_gain = d_gain;
        aux.i_gain = i_gain;
        aux.p_correction = pTerm;
        aux.d_correction = dTerm;
        aux.i_correction = iTerm;
        aux.correction = correction;
        aux.command = command_velocity;
        aux.command_clamped = command_input[0];
        aux.pwm = pwm_input;

        if (MOTION_PORT == ORC_TRANS_MOTOR_PORT)
            lcm.publish ("ORC_TRANS_VEL_CONT_AUX", aux);
        else
            lcm.publish ("ORC_ROT_VEL_CONT_AUX", aux);
    }
    
    public int setAcceleration(double acceleration){
        s_acceleration = acceleration;
        return 0;
    }

    public int updateStatus(int left_ticks, int right_ticks,
                            int left_tick_vel, 
                            int right_tick_vel){

        s_time = (double)System.nanoTime()/1e6;
  
        s_left_range = 0.0;
        s_right_range = 0.0;

        int curr_left_tick = left_ticks; 
        int curr_right_tick = right_ticks; 
  
        int left_delta = this.computeDeltaTicksMod( curr_left_tick, s_left_tick ) 
            * ORC_LEFT_REVERSE_ENCODER ;
  
        int right_delta = this.computeDeltaTicksMod( curr_right_tick, s_right_tick ) 
            * ORC_RIGHT_REVERSE_ENCODER ;
    
        revs_left = revs_left + left_delta;
        revs_right = revs_right + right_delta;
    
        s_left_displacement = this.deltaTickToMetres( left_delta );
        s_right_displacement = this.deltaTickToMetres( right_delta ); 
        s_left_tick = curr_left_tick;
        s_right_tick = curr_right_tick;
    
        int left_delta_velocity = left_tick_vel;     
        int right_delta_velocity = right_tick_vel;
    
        //not sure how exactly this is calculated 

        s_left_velocity = this.deltaTickToMetres( left_delta_velocity ) * 60.0 /1.5 ;  
        s_right_velocity = this.deltaTickToMetres( right_delta_velocity ) * 60.0 /1.5 ; 
        s_actual_rot_velocity = (s_right_velocity - s_left_velocity)/ORC_WHEEL_BASE;
        s_actual_trans_velocity = ( s_right_velocity + s_left_velocity ) / 2 ;
     
        double displacement = ( s_left_displacement + s_right_displacement )/2;

        //not sure if this rotation calculation is correct either ???
        //double rotation = Math.atan2( s_right_displacement - s_left_displacement, 
        //                  ORC_WHEEL_BASE );
        double rotation = (s_right_displacement - s_left_displacement)/ORC_WHEEL_BASE;

        //r*theta
    
        s_x = s_x + displacement*Math.cos( s_theta );
        s_y = s_y + displacement*Math.sin( s_theta );
     
        s_theta = this.normalizeTheta( s_theta + rotation );

        /*
        // recommand the velocity
        this.commandVelocity(s_desired_trans_velocity, 
        s_actual_trans_velocity, ORC_TRANS_MOTOR_PORT);
        this.commandVelocity(s_desired_rot_velocity, 
        s_actual_rot_velocity, ORC_ROT_MOTOR_PORT);
        */
        return 0;
    }


    double lastTime = 0.0;

    int [] last = {0, 0};
    boolean firstStatus = true;

    //stuff needed to do the base related stuff
    double current_tv_cmd = 0.0, current_rv_cmd = 0.0;
    boolean moving = false;
    double current_acceleration = 0;
    double deceleration;
    double relative_wheelbase;
    double relative_wheelsize;

    double last_velocity_command_sec = 0;
    double last_motion_command = 0;
    double motion_timeout = 30;
    

    int s_ignore_trans_d_term = 1;
    int s_ignore_rot_d_term = 1;
    
    double s_acceleration;
    double trans_i_error;
    double rot_i_error;
    double s_deceleration;

    int revs_left = 0, revs_right = 0;

    double s_x = 0.0, s_y = 0.0, s_theta = 0.0;
    double s_left_range, s_right_range;
    double s_left_displacement, s_right_displacement;
    double s_time;
    int s_left_tick, s_right_tick;
    int s_left_tick_velocity, s_right_tick_velocity;
    double s_left_velocity, s_right_velocity;

    double s_trans_command = 0, s_rot_command = 0;
    double s_actual_trans_velocity, s_actual_rot_velocity;
    double s_desired_trans_velocity, s_desired_rot_velocity;
    double s_trans_error_prev = 0.0, s_rot_error_prev = 0.0;
    double s_trans_time_millisec_prev = -1, s_rot_time_millisec_prev = -1;


    //this reported tv is 1.5 times the one we obtain from the encoders
    double deltaTickToMetres(int delta_tick){
        double ticks2rev = 1.0 / ORC_TICKS_PER_REVOLUTION;
        double revolutions = (double)delta_tick * ticks2rev ;
        double radians = revolutions*2*Math.PI;
        double metres = radians*(ORC_WHEEL_DIAMETER/2.0);    
        return metres;

    }

    int computeDeltaTicksMod( int curr, int prev )
    {
        // compute the number of ticks traversed short and long way around
        // and pick the path with the minimal distance
        int abs_reg = Math.abs( curr - prev );

        //wrap around for a 32bit int 
        int abs_opp = 2147483647 - abs_reg;
        int actual_delta = Math.min( abs_reg, abs_opp );

        // give the angle the correct sign -- ccw is positive
        if( ( curr > prev && actual_delta == abs_reg ) || 
           ( curr < prev && actual_delta == abs_opp ) ){
            actual_delta = -actual_delta;
        }
  
        return actual_delta;
    }

    double normalizeTheta(double theta) 
    {
        double multiplier;

        while(true){
            if(theta > Math.PI){
                theta -= 2* Math.PI;
            }
            if(theta < - Math.PI){
                theta += 2 * Math.PI;
            }
            
            if (theta >= -Math.PI && theta < Math.PI)
                return theta;
        }
    
        /*if (theta >= -Math.PI && theta < Math.PI)
          return theta;

          //or use the while loop 
    
          multiplier = Math.floor(theta / (2*Math.PI));
          theta = theta - multiplier*2*Math.PI;
          if (theta >= Math.PI)
          theta -= 2*Math.PI;
          if (theta < -Math.PI)
          theta += 2*Math.PI;    
          return theta;*/
    } 

    int getIntegratedState(raw_odometry_msg_t s)
    /*(double *x_p, double *y_p,
      double *theta_p, double *tv_p,
      double *rv_p)*/
    {
        s.x = s_x;
        s.y = s_y;
        s.theta = s_theta;
        s.tv = (s_left_velocity+s_right_velocity)/2;
        s.rv = Math.atan2(s_right_velocity-s_left_velocity, ORC_WHEEL_BASE);  
        return 0;
    }

    /*  //this is where the compensation needs to happen - for the r_v
        double[] commandToPWM( double command_velocity, int port , double desired){

        double[] result = new double[2];
        // clamp command velocity
        if( port == ORC_TRANS_MOTOR_PORT ){
        if(desired >=0){ //prevents decelerating 
        if (command_velocity > ORC_MAX_TRANS_VEL){
        command_velocity = (double)ORC_MAX_TRANS_VEL;
        }
        if (command_velocity < 0){
        command_velocity = 0;
        }
        }
        if(desired <0){ //prevents decelerating 
        if (command_velocity < -ORC_MAX_TRANS_VEL){
        command_velocity = -(double)ORC_MAX_TRANS_VEL;
        }
        if (command_velocity > 0){
        command_velocity = 0;
        }
        }
        } else{
        if(desired >=0){ 
        if (command_velocity > ORC_MAX_ROT_VEL){
        command_velocity = (double)ORC_MAX_ROT_VEL;
        }
        if (command_velocity < 0){
        command_velocity = 0;
        }
        }
        if(desired < 0){
        if (command_velocity < -ORC_MAX_ROT_VEL){
        command_velocity = -(double)ORC_MAX_ROT_VEL;
        }
        if (command_velocity > 0){
        command_velocity = 0;
        }
        }
        }

        result[0] = command_velocity; //clamped velocity
    
        double pwm_input = 0;
        if ( port == ORC_ROT_MOTOR_PORT){
        command_velocity = command_velocity * ORC_ROT_REVERSE / ORC_MAX_ROT_VEL;
    
        // if we want to go backwards, the command velocity is negative
        if( command_velocity < 0 ){
        pwm_input = ORC_ROT_PWM_ZERO + 
        command_velocity * (ORC_ROT_PWM_ZERO - ORC_ROT_PWM_MIN);
        } else {
        pwm_input = ORC_ROT_PWM_ZERO + 
        command_velocity * (ORC_ROT_PWM_MAX - ORC_ROT_PWM_ZERO);
        }

        } else {
        command_velocity = command_velocity * ORC_TRANS_REVERSE / ORC_MAX_TRANS_VEL;

        // if we want to go backwards, the command velocity is negative
        if( command_velocity < 0 ){
        pwm_input = ORC_TRANS_PWM_ZERO + 
        command_velocity * (ORC_TRANS_PWM_ZERO - ORC_TRANS_PWM_MIN);
        } else {
        pwm_input = ORC_TRANS_PWM_ZERO + 
        command_velocity * (ORC_TRANS_PWM_MAX - ORC_TRANS_PWM_ZERO);
        }
        }
        //return pwm_input;
        result[1] = pwm_input;

        return result;
        }  */


    //this is where the compensation needs to happen - for the r_v
    double[] commandToPWM( double command_velocity, int port ){

        double[] result = new double[2];
        // clamp command velocity
        if( port == ORC_TRANS_MOTOR_PORT ){
            if (command_velocity > ORC_MAX_TRANS_VEL[currentVelocity.ordinal()]){
                command_velocity = (double)ORC_MAX_TRANS_VEL[currentVelocity.ordinal()];
            }
            if (command_velocity < -ORC_MAX_TRANS_VEL[currentVelocity.ordinal()]){
                command_velocity = -1.0 * (double)ORC_MAX_TRANS_VEL[currentVelocity.ordinal()];
            }
        } else{
            if (command_velocity > ORC_MAX_ROT_VEL[currentVelocity.ordinal()]){
                command_velocity = (double)ORC_MAX_ROT_VEL[currentVelocity.ordinal()];
            }
            if (command_velocity < -ORC_MAX_ROT_VEL[currentVelocity.ordinal()]){
                command_velocity = -1.0 * (double)ORC_MAX_ROT_VEL[currentVelocity.ordinal()];
            }
        }

        result[0] = command_velocity; //clamped velocity
    
        double pwm_input = 0;
        if ( port == ORC_ROT_MOTOR_PORT){
            command_velocity = command_velocity * ORC_ROT_REVERSE[currentVelocity.ordinal()] / ORC_MAX_ROT_VEL[currentVelocity.ordinal()];
    
            // if we want to go backwards, the command velocity is negative
            if( command_velocity < 0 ){

                //need to think about this a bit more --- seems like the deadzone in the middle 
                //might mess with things here
                pwm_input = ORC_ROT_PWM_ZERO[currentVelocity.ordinal()] + 
                    command_velocity * (ORC_ROT_PWM_ZERO[currentVelocity.ordinal()] - ORC_ROT_PWM_MIN[currentVelocity.ordinal()]);
            } else {
                pwm_input = ORC_ROT_PWM_ZERO[currentVelocity.ordinal()] + 
                    command_velocity * (ORC_ROT_PWM_MAX[currentVelocity.ordinal()] - ORC_ROT_PWM_ZERO[currentVelocity.ordinal()]);
            }
        
            if(current_rv_cmd ==0){
                if(current_tv_cmd >0){
                    pwm_input = ORC_ROT_PWM_ZERO_FORWARD[currentVelocity.ordinal()];
                }
                else if(current_tv_cmd < 0){
                    pwm_input = ORC_ROT_PWM_ZERO_REVERSE[currentVelocity.ordinal()];
                }
            }
        }        


        else {
            command_velocity = command_velocity * ORC_TRANS_REVERSE[currentVelocity.ordinal()] / ORC_MAX_TRANS_VEL[currentVelocity.ordinal()];

            // if we want to go backwards, the command velocity is negative
            if( command_velocity < 0 ){
                pwm_input = ORC_TRANS_PWM_ZERO[currentVelocity.ordinal()] + 
                    command_velocity * (ORC_TRANS_PWM_ZERO[currentVelocity.ordinal()] - ORC_TRANS_PWM_MIN[currentVelocity.ordinal()]);
            } else {
                pwm_input = ORC_TRANS_PWM_ZERO[currentVelocity.ordinal()] + 
                    command_velocity * (ORC_TRANS_PWM_MAX[currentVelocity.ordinal()] - ORC_TRANS_PWM_ZERO[currentVelocity.ordinal()]);
            }
        }
        //return pwm_input;
        result[1] = pwm_input;

        return result;
    }  

    
    public void orcStatus(Orc orc, OrcStatus status)
    {
        // if uorc resets...
        if (status.utimeOrc < 2000000){
            //assuming no time syncs happen
            systemTimeStart = (long)(System.currentTimeMillis() * 1000.0);
            orcTimeStart = status.utimeOrc; 
            firstStatus = true;
        }
    
        orc_full_stat_msg_t msg = new orc_full_stat_msg_t();
        
        //this is the utime from the orc  - not the system time 
        //Date currentDate = new Date();

        //long msec = currentDate.getTime();
    
        //msg.utime = (status.utimeOrc - orcTimeStart) + systemTimeStart; 
        // System.nanoTime(); //msec * 1000; //status.utimeOrc; 

        if (systemStartTime_NSecReference == 0) {
            systemStartTime_MSecSinceEpoch = System.currentTimeMillis();
            systemStartTime_NSecReference = System.nanoTime();
        }


        long elapsed_nano_milli = (long) ((System.nanoTime() - systemStartTime_NSecReference)/1000000.0);
        long elapsed_milli = (long) (System.currentTimeMillis() - systemStartTime_MSecSinceEpoch);

        if( Math.abs(elapsed_milli - elapsed_nano_milli) > 1.0){
            //update the reference times
            systemStartTime_MSecSinceEpoch = System.currentTimeMillis();
            systemStartTime_NSecReference = System.nanoTime();
        }

        //        System.out.print("Time Difference : "  + elapsed_milli  + " , "  + elapsed_nano_milli + "\n");
        // + "\n");

        //msg.utime = systemStartTime_MSecSinceEpoch * 1000 + (long) 

        msg.utime = systemStartTime_MSecSinceEpoch * 1000 + (long) ((System.nanoTime() - systemStartTime_NSecReference)/1000.0);

        System.arraycopy(status.qeiPosition,0,msg.qei_position ,0,2);
    
        for (int i = 0; i < 2; i++) {
            qeiDisplay.values[i][1] = String.format("%6d", status.qeiPosition[i]);
            if(last[i] != 0){
                if(status.qeiPosition[i] - last[i] < 0){
                    qeiDisplay.values[i][2] = String.format("%6d", -status.qeiVelocity[i]);
                    msg.qei_velocity[i] = -status.qeiVelocity[i];
                }
                else{
                    qeiDisplay.values[i][2] = String.format("%6d", status.qeiVelocity[i]);
                    msg.qei_velocity[i] = status.qeiVelocity[i];
                }
            }
            last[i] = status.qeiPosition[i];        
        }
    
        basicDisplay.values[0][1] = String.format("%.6f", status.utimeOrc/1000000.0);

        msg.cmd_velocity[0] = tv_duty_cycle;
        msg.cmd_velocity[1] = rv_duty_cycle;

        msg.velocity_level = VelocityRange.getLevelInt(currentVelocity);

        lcm.publish ("BASE_FULL_STAT", msg); 

        proximity_sensor_values_t prox_msg = new proximity_sensor_values_t();

        prox_msg.utime = msg.utime; 
        prox_msg.no_sensors = 8;
        prox_msg.value = new double[8];
        for (int i = 0; i < 8; i++) {
            //add the conversion function if needed         
            prox_msg.value[i] = status.analogInput[i] / 65535.0 * 5.0;
            //System.out.printf("%f\n",status.analogInput[i] / 65535.0 * 5.0);
        }

        // Publish a robot_state_command to transition to MANUAL if we are in run and the joystick has been moved
        if(robot_state_last == robot_status_t.STATE_RUN && prox_msg.value[2] < 0.2){ //joystick is being moved 
            //this sets the robot to inactive/manual mode
            robot_state_command_t state_cmd = new robot_state_command_t();
            state_cmd.utime = msg.utime; 
            state_cmd.state = robot_status_t.STATE_STOP;
            state_cmd.faults = robot_status_t.FAULT_MANUAL;
            state_cmd.fault_mask =  robot_status_t.FAULT_MASK_NO_CHANGE;
            state_cmd.sender = "orc-controller";
            state_cmd.comment  = "joystick motion detected";            
            lcm.publish("ROBOT_STATE_COMMAND", state_cmd);
            js_active = true;
        }
        
        if(prox_msg.value[2] > 3.0){
            js_active = false;
        }        

        lcm.publish ("ORC_SENSOR_STATUS", prox_msg); 
    
        orc_debug_stat_msg_t d_msg = new orc_debug_stat_msg_t();
        d_msg.utime = msg.utime;
        System.arraycopy(msg.qei_velocity,0,d_msg.qei_velocity ,0,2);
        System.arraycopy(status.qeiPosition,0,d_msg.qei_position ,0,2);
        System.arraycopy(msg.cmd_velocity,0,d_msg.pwm_value ,0,2);
        d_msg.s_desired_vel[0] = current_tv_cmd;
        d_msg.s_desired_vel[1] = current_rv_cmd;
        d_msg.s_actual[0] = s_actual_trans_velocity;
        d_msg.s_actual[1] = s_actual_rot_velocity;
        d_msg.command_velocity[0] = s_trans_command;
        d_msg.command_velocity[1] = s_rot_command;
    
        lcm.publish("BASE_DEBUG_STAT", d_msg); 

        //from drive_main
        double tv, rv;
        double displacement, rotation;
        int stat;

        //stop the chair if it has been too long since the last velocity command
        double delta_time = Math.abs(((double) msg.utime) / 1e6 - last_motion_command);
        if (moving && (delta_time > motion_timeout)) {
            System.out.printf("Error: Motion Timeout since it has been %.2f seconds since receiving the last velocity command\n", delta_time);
            moving = false;
            stat = this.setDeceleration(deceleration);
            stat = this.setVelocity(0.0,0.0);
        }
    
        int left_tick = msg.qei_position[0];
        int right_tick = msg.qei_position[1];
    
        int left_tick_velocity = msg.qei_velocity[0];
        int right_tick_velocity = msg.qei_velocity[1];
    
        raw_odometry_msg_t odom_msg = new raw_odometry_msg_t();

        odom_msg.utime = msg.utime;    
    
        stat = this.updateStatus(left_tick, right_tick,
                                 left_tick_velocity, 
                                 right_tick_velocity);

        
        stat = this.getIntegratedState(odom_msg);
        odom_msg.acceleration = 0.0;
    
        lcm.publish ("ODOMETRY", odom_msg); 

        firstStatus = false;
        lastTime =  msg.utime;    //status.utimeOrc/1000000.0;

        //maybe we dont need this either ///

        qeiDisplay.repaint();
        basicDisplay.repaint();
        robotFrame.repaint();
    }

    class ServoPanel extends JPanel implements SmallSlider.Listener
    {
        SmallSlider slider = new SmallSlider(0, 1000, 560, 560, true);
        Servo servo;
        int port;

        ServoPanel(int port, Servo servo)
        {
            this.port = port;
            slider.formatScale = 1;
            slider.formatString = "%.0f per one thousands";

            // pos/usecs don't matter, we specify pulse widths, not positions
            this.servo = servo;//new Servo(orc, port, 0, 0, 0, 0);
            this.servo.setPWM(1500,0.565);
            if(port == 0){
                tv_duty_cycle = 0.565;
            }else{
                rv_duty_cycle = 0.565;
            }

            setLayout(new BorderLayout());
            add(slider, BorderLayout.CENTER);

            slider.addListener(this);
        }

        public void goalValueChanged(SmallSlider slider, int goal)
        {
            //servo.setPulseWidth(goal);
            double duty_cycle = (double)(goal/1000.0);
            if(verbose)
                System.out.printf("Duty Cycle : %f\n", duty_cycle);
            if(this.port == 0){
                tv_duty_cycle = duty_cycle;
            }else{
                rv_duty_cycle = duty_cycle;
            }
            servo.setPWM(1500,duty_cycle);
        }
    }


    public void messageReceived(LCM lcm, String channel, LCMDataInputStream ins)
    {
        try {
            //we wont be using this now 
            
            if (channel.equals("ROBOT_STATUS")){
                erlcm.robot_status_t robot_status = new erlcm.robot_status_t(ins);

                robot_state_last = robot_status.state;
            }

            if (channel.equals("SHIFT_VELOCITY_CMD")){
                System.out.println("shifting velocity");
                erlcm.shift_velocity_msg_t vel_shift = new erlcm.shift_velocity_msg_t(ins);
        
                if (vel_shift.shift == erlcm.shift_velocity_msg_t.SHIFT_UP){//1) {
                    if(!currentVelocity.equals(VelocityRange.MAX_VEL_100)) {
                        shiftVelUp();
                        if(verbose)
                            System.out.println("vel mode shifted up to " + currentVelocity.toString());
                    }
                } else if(vel_shift.shift == erlcm.shift_velocity_msg_t.SHIFT_DOWN){
                    if (!currentVelocity.equals(VelocityRange.MAX_VEL_20)) {    
                        shiftVelDown();
                        if(verbose)
                            System.out.println("vel mode shifted down to " + currentVelocity.toString());
                    }
                }
            }
            
            //if (channel.equals("BASE_VELOCITY_CMD")) {
            if (channel.equals("ROBOT_VELOCITY_CMD") || 
                channel.equals("ROBOT_VELOCITY_CMD_JS")) {

                if(verbose)
                    System.out.println("Command Received =============");

                
                
                if(channel.equals("ROBOT_VELOCITY_CMD")){
                    shift_velocity = true;
                }else{
                    shift_velocity = false;
                }

                erlcm.velocity_msg_t v_cmd = new  erlcm.velocity_msg_t(ins);
                //System.out.printf("Velocity :%f, %f,%f\n",v_cmd.utime/1.0e6, v_cmd.tv, v_cmd.rv);
                double tv = v_cmd.tv;
                double rv = v_cmd.rv;
                int stat;

                if(tv == 0 && rv == 0){
                    if (moving){
                        stat = this.setDeceleration(deceleration);
                        moving = false;
                    }  
                }
                else if (!moving){
                    moving = true;        
                    current_acceleration = ACCELERATION;
                    stat = this.setAcceleration(current_acceleration);
                }

                // -- good place to clamp them 

                //*** Sachi - adding acceleration limits 
                current_tv_cmd = tv;
                current_rv_cmd = rv;
                last_velocity_command_sec = System.currentTimeMillis()/1000;
                //this doesnt work properly- because the velocity 
                //shifts around too much 
                //and also the acceleration curves are different for different settings 

                if(false){

                    if(tv != 0 || rv !=0){
                    
                        double delta_tv = current_tv_cmd - s_actual_trans_velocity; 
                        double delta_t_tv = Math.abs(delta_tv / TV_ACCELERATION); 
                        if(delta_t_tv > DELTA_T){
                            if(verbose)
                                System.out.printf("Too high a Trans velocity change - clamping\n");
                            if(delta_tv < 0){
                                current_tv_cmd = s_actual_trans_velocity - TV_ACCELERATION * DELTA_T; 
                            }
                            else{
                                current_tv_cmd = s_actual_trans_velocity + TV_ACCELERATION * DELTA_T; 
                            }
                        }
                    
                        double delta_rv = current_rv_cmd - s_actual_rot_velocity; 
                        double delta_t_rv = Math.abs(delta_rv / RV_ACCELERATION); 
                        if(delta_t_rv > DELTA_T){
                            if(verbose)
                                System.out.printf("Too high a Rot velocity change - clamping\n");
                            if(delta_rv < 0){
                                current_rv_cmd = s_actual_rot_velocity - RV_ACCELERATION * DELTA_T; 
                            }
                            else{
                                current_rv_cmd = s_actual_rot_velocity + RV_ACCELERATION * DELTA_T; 
                            }
                        }        
                    }    
                }    

                last_motion_command = ((double)v_cmd.utime)/1e6;
                //********* not sure if we should send this command right now?? 
                //System.out.printf("Setting Value (%f): (tv : %f, rv : %f\n", last_motion_command,current_tv_cmd, current_rv_cmd);
            }            
        } catch (IOException ex) {
            System.out.println("Exception: " + ex);
        }
    }
    
    class TextPanelWidget extends JPanel
    {
        String panelName;
        String columnNames[];
        String values[][];
        double columnWidthWeight[];

        public TextPanelWidget(String columnNames[], String values[][],
                               double columnWidthWeight[])
        {
            this.columnNames = columnNames;
            this.columnWidthWeight = columnWidthWeight;
            this.values = values;
        }

        int getStringWidth(Graphics g, String s)
        {
            FontMetrics fm = g.getFontMetrics(g.getFont());
            return fm.stringWidth(s);
        }

        int getStringHeight(Graphics g, String s)
        {
            FontMetrics fm = g.getFontMetrics(g.getFont());
            return fm.getMaxAscent();
        }

        public Dimension getPreferredSize()
        {
            return new Dimension(100,100);
        }

        public Dimension getMinimumSize()
        {
            return getPreferredSize();
        }

        public Dimension getMaximumSize()
        {
            return new Dimension(1000,1000);
        }

        public void paint(Graphics _g)
        {
            Graphics2D g = (Graphics2D) _g;
            int width = getWidth(), height = getHeight();

            Font fb = new Font("Monospaced", Font.BOLD, 12);
            Font fp = new Font("Monospaced", Font.PLAIN, 12);
            g.setFont(fb);
            int textHeight = getStringHeight(g, "");

            ////////////////////////////////////////////
            // Draw backgrounds/outlines

            g.setColor(getBackground());
            g.fillRect(0, 0, width, height);

            g.setColor(Color.black);
            g.drawRect(0, 0, width-1, height-1);

            double totalColumnWidthWeight = 0;
            for (int i = 0; i < columnWidthWeight.length; i++)
                totalColumnWidthWeight += columnWidthWeight[i];

            int columnPos[] = new int[columnWidthWeight.length];
            int columnWidth[] = new int[columnWidthWeight.length];
            for (int i = 0; i < columnWidth.length; i++)
                columnWidth[i] = (int) (width * columnWidthWeight[i]/totalColumnWidthWeight);

            for (int i = 1; i < columnWidth.length; i++) {
                columnPos[i] = columnPos[i-1] + columnWidth[i-1];
            }

            ////////////////////////////////////////////
            // draw column headings
            g.setColor(Color.black);
            for (int i = 0; i < columnNames.length; i++) {
                String s = columnNames[i];
                g.drawString(s, columnPos[i]+columnWidth[i]/2 - getStringWidth(g, s)/2, textHeight*3/2);
            }

            ////////////////////////////////////////////
            // draw cells
            g.setFont(fp);
            for (int i = 0; i < values.length; i++) {
                for (int j = 0; j < columnNames.length; j++) {
                    String s = values[i][j];
                    if (s == null)
                        s = "";

                    g.drawString(s, columnPos[j]+columnWidth[j]/2 - getStringWidth(g, s)/2, textHeight*(3+i));
                }
            }

        }
    }
}
