package orc.controller;

import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import lcm.lcm.*;
import erlcm.*;
import bot_core.*;
import orc.spy.*;

import lcm.spy.ChannelData;
import lcm.util.ParameterGUI;
import lcm.util.ParameterListener;
import lcm.util.ColorMapper;
import lcm.lcm.LCMDataInputStream;


import java.lang.Math.*;

import orc.*;

class RobotLaserPane extends JPanel implements MouseWheelListener, MouseListener, MouseMotionListener, KeyListener
    {
	boolean filledin = true;
	static final double MAX_ZOOM = 1024;
	static final double MIN_ZOOM = 4;
	static final double MAX_RANGE = 29.0;
	
	int i=1;
        //erlcm.robot_laser_t l;
	double tx, ty;
	AffineTransform T ;
	ParameterGUI pg;

	double goal_x = 0, goal_y = 0;

	erlcm.people_pos_msg_t people;
	erlcm.goal_heading_msg_t person_goal;
	erlcm.speech_cmd_t speech_cmd;
	erlcm.vector_msg_t vector_stat;
	erlcm.velocity_msg_t velocity_stat;
	erlcm.velocity_msg_t velocity_cmd;

	double b = 0.3; ///scale;  //half the breath
	double h1 = 0.5;///scale; // robot base size and height (a triangle)
	double h2 = 0.5;

	LCM lcm;

	double robot_theta = 0.0;
	boolean issue_vec = false;

        //erlcm.robot_laser_t rl;

	public RobotLaserPane(ParameterGUI pg, LCM lcm)
	{
	    this.pg = pg;
	    this.lcm = lcm;

	    addMouseWheelListener(this);
	    addMouseListener(this);
	    addMouseMotionListener(this);
	    addKeyListener(this);

	    setFocusable(true);
	    grabFocus();
	}

	public double getScale()
	{
	    return T.getScaleX();
	}

        /*public void setData(erlcm.robot_laser_t l)
	{
	    this.l = l;
	    repaint();
        }*/

	public void setPeopleData(erlcm.people_pos_msg_t p)
	{
	    this.people = p;
	}
	
        /*public void setRearLaserData(erlcm.robot_laser_t rl)
	{
	    this.rl = rl;
        }*/

	public void setVectorStatusData(erlcm.vector_msg_t v)
	{
	    this.vector_stat = v;
	}

	public void setVelocityCmdData(erlcm.velocity_msg_t v)
	{
	    this.velocity_cmd = v;
	}

	public void setVelocityData(erlcm.velocity_msg_t v)
	{
	    this.velocity_stat = v;
	}

	public void setPeopleGoalData(erlcm.goal_heading_msg_t g)
	{
	    this.person_goal = g;
	}

	public void setSpeechData(erlcm.speech_cmd_t s) //doubt this is needed
	{
	    this.speech_cmd = s;
	}

	public void bot_quat_to_roll_pitch_yaw(double q[], double rpy[]) 
	{
	    double roll_a = 2 * (q[0]*q[1] + q[2]*q[3]);
	    double roll_b = 1 - 2 * (q[1]*q[1] + q[2]*q[2]);
	    rpy[0] = Math.atan2 (roll_a, roll_b);
	    
	    double pitch_sin = 2 * (q[0]*q[2] - q[3]*q[1]);
	    rpy[1] = Math.asin (pitch_sin);
	    
	    double yaw_a = 2 * (q[0]*q[3] + q[1]*q[2]);
	    double yaw_b = 1 - 2 * (q[2]*q[2] + q[3]*q[3]);
	    rpy[2] = Math.atan2 (yaw_a, yaw_b);
	}
	
	public double normalize_theta(double theta) 
	{
	    double multiplier;
	    
	    if (theta >= -Math.PI && theta < Math.PI)
		return theta;
	    
	    multiplier = Math.floor(theta / (2*Math.PI));
	    theta = theta - multiplier*2*Math.PI;
	    if (theta >= Math.PI)
		theta = theta - 2*Math.PI;
	    if (theta < -Math.PI)
		theta = theta + 2*Math.PI;
	    
	    return theta;
	} 
	
	public void paint(Graphics gin)
	{
	    Graphics2D g = (Graphics2D) gin;

	    int width = getWidth(), height = getHeight();

	    if (T == null) {
		T = new AffineTransform();
		T.translate(width/2, height);
		T.scale(8, -8);
	    }

	    g.setColor(Color.black);
	    g.fillRect(0,0,width,height);

	    /*if (l==null)
		return;
        */
	    g.transform(T);
	    double scale = getScale();

	    double maxrange = 0;
        /*
	    bot_core.pose_t robot_pose = l.pose;
	    double pos[] = robot_pose.pos;
	    double quat[] = robot_pose.orientation;

	    double rpy[] =  new double[3];

	    bot_quat_to_roll_pitch_yaw(quat,rpy);

	    double laser_offset = pos[2]; //assumed that there is only x offset in robot frame
	    robot_theta = Math.PI/2 + rpy[2];

	    

	    double alpha = Math.PI/2 + rpy[2];
	    double x_offset = laser_offset*Math.cos(alpha);
	    double y_offset = laser_offset*Math.sin(alpha);
	    
	    // values for sick LMS291-S05
	    double min_intensity = 7500, max_intensity = 12000;

	    //for rear robot laser
	    
	    if (rl !=null){

	    //rear robot laser parameters

		bot_core.pose_t rear_robot_pose = rl.pose;
		double rear_pos[] = rear_robot_pose.pos;
		double rear_quat[] = rear_robot_pose.orientation;
		double rear_rpy[] =  new double[3];
		
		bot_quat_to_roll_pitch_yaw(rear_quat,rear_rpy);
		
		double rear_laser_offset = rear_pos[2];
		
		double rear_alpha = Math.PI/2 + rpy[2];
		
		double rear_x_offset = rear_laser_offset*Math.cos(alpha);
		double rear_y_offset = rear_laser_offset*Math.sin(alpha);

		bot_core.planar_lidar_t rearl = rl.laser;

		if (pg.gb("rear_laser")){
		    
		    g.setColor(new Color(0, 0, 100));

		    GeneralPath p = new GeneralPath();
		    p.moveTo(rear_x_offset,rear_y_offset);
		    for (int i = 0; i < rearl.nranges; i++) {
			if (rearl.ranges[i] > MAX_RANGE)
			    continue;
			
			if (rearl.ranges[i] < 100)
			    maxrange = Math.max(maxrange, rearl.ranges[i]);
			double theta = rearl.rad0+i*rearl.radstep+Math.PI/2 + rear_rpy[2];
			double x = rearl.ranges[i]*Math.cos(theta)+rear_x_offset;
			double y = rearl.ranges[i]*Math.sin(theta)+ rear_y_offset;
			p.lineTo((float) x, (float) y);
		    }
		    p.closePath();
		    g.fill(p);
		    
		    g.setStroke(new BasicStroke((float) (2.0/scale)));
		    
		    g.setColor(new Color(0, 0, 255));
		    g.draw(p);
		    
		    
		    
		    ColorMapper colormap = new ColorMapper(new int[] {0x0000ff,
								      0x008080,
								      0x808000,
								      0xff0000},
			min_intensity,
			max_intensity);
		    
		    {
			double r = 4.0/scale;
			if (!filledin)
			    r *= 2;
			
			for (int i = 0; i < rearl.nranges; i++) {
			    if (rearl.ranges[i] > MAX_RANGE)
				continue;
			    
			    if (!pg.gb("intensity") || rearl.nintensities != rearl.nranges) {
				g.setColor(Color.gray);
			    } else {
				g.setColor(new Color(colormap.map(rearl.intensities[i])));
			    }
			    
			    double theta = rearl.rad0 + i*rearl.radstep+Math.PI/2+rear_rpy[2];
			    
			    double x = rearl.ranges[i]*Math.cos(theta) + rear_x_offset;
			    double y = rearl.ranges[i]*Math.sin(theta)+  rear_y_offset;
			    
			    Ellipse2D e = new Ellipse2D.Double(x-r/2,y-r/2,r,r);
			    g.fill(e);
			}
		    }
		}
	    }



	    bot_core.planar_lidar_t pl = l.laser;

	    // draw the filled-in polygon of the laser scan
	    if (pg.gb("volume"))
		{
		    g.setColor(new Color(0, 0, 100));
		    GeneralPath p = new GeneralPath();
		    //p.moveTo(0,0);
		    p.moveTo(x_offset,y_offset);
		    for (int i = 0; i < pl.nranges; i++) {
			if (pl.ranges[i] > MAX_RANGE)
			    continue;
			if (pl.ranges[i] < 0.1)
			    continue;

			if (pl.ranges[i] < 100)
			    maxrange = Math.max(maxrange, pl.ranges[i]);
			double theta = pl.rad0+i*pl.radstep+Math.PI/2 + rpy[2];
			double x = pl.ranges[i]*Math.cos(theta)+x_offset;
			double y = pl.ranges[i]*Math.sin(theta)+ y_offset;
			p.lineTo((float) x, (float) y);
		    }
		    p.closePath();
		    g.fill(p);
		    
		    g.setStroke(new BasicStroke((float) (2.0/scale)));
		    
		    g.setColor(new Color(0, 0, 255));
		    g.draw(p);

		}

	    // draw the pessimistic polygon of the laser scan
	    if (pg.gb("pessimistic"))
		{
		    g.setColor(new Color(100, 0, 0));
		    GeneralPath p = new GeneralPath();
		    
		    p.moveTo(x_offset,y_offset);
		    //p.moveTo(0,0);
		    for (int i = 0; i < pl.nranges; i++) {
			if (pl.ranges[i] > MAX_RANGE) {
			    // count how many ranges are max_range
			    int max_count = 1;
			    for (int j = i+1; (j < pl.nranges) && (pl.ranges[j] > MAX_RANGE); j++)
				max_count++;
			    // if a person in black was in front of lidar how close could they be?
			    double min_pedestrian_width = 0.4;
			    // closest a person could get to obscure this many returns
			    double myrange = (min_pedestrian_width/2.0)/Math.tan(max_count*pl.radstep/2.0);
			    if (myrange < 100)
				maxrange = Math.max(maxrange, myrange);
			    for (int j=0; j<max_count;j++) {
				double theta = pl.rad0+(i+j)*pl.radstep+Math.PI/2 + rpy[2];
				double x = myrange*Math.cos(theta) + x_offset;
				double y = myrange*Math.sin(theta)+ y_offset;
				p.lineTo((float) x, (float) y);
			    }
			    // skip ahead
			    i+=max_count-1;
			}
			else {   
			    if (pl.ranges[i] < 100)
				maxrange = Math.max(maxrange, pl.ranges[i]);
			    double theta = pl.rad0+i*pl.radstep+Math.PI/2 + rpy[2];
			    double x = pl.ranges[i]*Math.cos(theta) + x_offset;
			    double y = pl.ranges[i]*Math.sin(theta)+ y_offset;
			    p.lineTo((float) x, (float) y);
			}
		    }
		    p.closePath();
		    g.fill(p);
		    
		    g.setStroke(new BasicStroke((float) (2.0/scale)));
		    
		    g.setColor(new Color(255, 0, 0));
		    g.draw(p);
		}
	    
	    ///////// draw laser returns as dots 

	    

	    if (pg.gb("normalized_intensities")) {
		min_intensity = 0;
		max_intensity = 1;
	    }

	    ColorMapper colormap = new ColorMapper(new int[] {0x0000ff,
							      0x008080,
							      0x808000,
							      0xff0000},
		min_intensity,
		max_intensity);
	    
	    {
		double r = 4.0/scale;
		if (!filledin)
		    r *= 2;

		for (int i = 0; i < pl.nranges; i++) {
		    if (pl.ranges[i] > MAX_RANGE)
			continue;
		    
		    if (!pg.gb("intensity") || pl.nintensities != pl.nranges) {
			g.setColor(Color.yellow);
		    } else {
			g.setColor(new Color(colormap.map(pl.intensities[i])));
		    }
		    
		    double theta = pl.rad0 + i*pl.radstep+Math.PI/2+rpy[2];
		    
		    double x = pl.ranges[i]*Math.cos(theta) + x_offset;
		    double y = pl.ranges[i]*Math.sin(theta)+  y_offset;

		    Ellipse2D e = new Ellipse2D.Double(x-r/2,y-r/2,r,r);
		    g.fill(e);
		}
	    }
	    
	    


	    ///////// draw reference rings
	    {
		g.setStroke(new BasicStroke((float) (1.0/scale))); // 1px
		g.setFont(g.getFont().deriveFont((float) (12.0/scale)).deriveFont(AffineTransform.getScaleInstance(1, -1)));

		int maxring = (int) Math.max(maxrange,Math.sqrt(width*width/4 + height*height)/scale);

		for (int i = 1; i < maxring; i++)
		    {
			double r = i;
			Ellipse2D e = new Ellipse2D.Double(-r,-r, 2*r, 2*r); //not sure whether we want to move this to 
			//center of the laser - or keep it at the robot center
			if (i%10==0) {
			    g.setColor(new Color(150,150,150));
			}
			else if (i%5==0) {
			    g.setColor(new Color(100,100,100));
			    //			    if (scale < 25)
			    //				continue;
			}
			else {
			    g.setColor(new Color(40,40,40));
			    if (scale <5)
				continue;
			}

			g.draw(e);
		    }

		// draw ring labels
		g.setColor(new Color(200,200,200));
		for (int i = 1; i < maxring; i++)
		    {
			double r = i;
			if (i%10==0 || (i%5==0 && maxring < 15))
			    g.drawString(""+i, (float) (r + 2/scale), (float) 0);
		    }			

	    }

	    //Draw velocity
	    if (pg.gb("velocity_stat")&&(velocity_stat != null)){//draw velocity stat
		
		g.setStroke(new BasicStroke((float) (2.0/scale)));
		double theta = rpy[2]+ Math.PI/2;
		
		double c = Math.cos(theta);
		double s = Math.sin(theta);
				
		double x1,x2,x3,x4,y1,y2,y3,y4;
		
		x1 = h1*c - b*s;
		y1 = h1*s + b*c;
		
		x2 = h1*c + b*s;
		y2 = h1*s - b*c;
		
		x3 = -h2*c + b*s;
		y3 = -h2*s - b*c;
		
		x4 = -h2*c - b*s;
		y4 = -h2*s + b*c;   
		
		if((Math.abs(velocity_stat.tv)<0.001) 
		   && (Math.abs(velocity_stat.rv)>0.0001)){ //rotating inplace
		    GeneralPath p = new GeneralPath();  		    
		    
		    p.moveTo((float)x1,(float)y1);
		    p.lineTo((float)x2,(float)y2);
		    p.lineTo((float)x3,(float)y3);
		    p.lineTo((float)x4,(float)y4);
		    
		    p.closePath();
		    g.setColor(Color.red);
		    g.draw(p); //draw line around robot
		}
		else if((Math.abs(velocity_stat.tv)>0.001) 
			&& (Math.abs(velocity_stat.rv)>0.001)){ 
		    
		    double top_left_x1=0.0,top_left_y1=0.0, top_left_x2=0.0,top_left_y2=0.0;		    
		    double r = 1.0/normalize_theta(velocity_stat.rv),r1,r2;

		    double xc1 = 0.0, yc1 = 0.0,xc2 = 0.0, yc2 = 0.0;

		    g.setColor(Color.red);

		    x1 = - b*0.8*s;
		    y1 = b*0.8*c;
		    
		    x2 = b*0.8*s;
		    y2 = - b*0.8*c;

		    r1 = Math.abs(r);
		    r2 = Math.abs(r)+ 2*b*0.8;

		    double start_angle=0, extent = 360;

		    if(r<0){
			xc1 = x2 + r1*Math.sin(theta);
			yc1 = y2 - r1*Math.cos(theta);

			xc2 = x1 + r2*Math.sin(theta);
			yc2 = y1 - r2*Math.cos(theta);	

			start_angle = 270 - Math.toDegrees(theta);
			extent = 90.0;
		    }
		    else{
			xc1 = x1 - r1*Math.sin(theta);
			yc1 = y1 + r1*Math.cos(theta);
			
			xc2 = x2 - r2*Math.sin(theta);
			yc2 = y2 + r2*Math.cos(theta);
			
			start_angle = 360 - Math.toDegrees(theta);
			extent = 90.0;
		    }
		    
		    top_left_x1 = xc1 - r1;
		    top_left_y1 = yc1 - r1;
		    
		    top_left_x2 = xc2 - r2;
		    top_left_y2= yc2 - r2;

		    //draw the arcs

		    Arc2D arc = new Arc2D.Double(top_left_x1,top_left_y1, 
						 r1*2,r1*2,start_angle,extent
						 ,Arc2D.OPEN);
		    g.draw(arc);

		    

		    arc = new Arc2D.Double(top_left_x2,top_left_y2, 
						 r2*2,r2*2,start_angle,extent
						 ,Arc2D.OPEN);
		    g.draw(arc);

		    	    
		}
		
		else if(Math.abs(velocity_stat.tv)>0.001) {

		    x1 = h1*c - b*0.8*s;
		    y1 = h1*s + b*0.8*c;
		    
		    x2 = h1*c + b*0.8*s;
		    y2 = h1*s - b*0.8*c;

		    x3 = (h1+4.0)*c - b*s;
		    y3 = (h1+4.0)*s + b*c;
		    
		    x4 = (h1+4.0)*c + b*s;
		    y4 = (h1+4.0)*s - b*c;

		    GeneralPath p = new GeneralPath();  
		    p.moveTo((float)x1,(float)y1);
		    p.lineTo((float)x3,(float)y3);
		    p.closePath();
		    g.setColor(Color.red);
		    g.draw(p); 

		    p = new GeneralPath();  
		    p.moveTo((float)x2,(float)y2);
		    p.lineTo((float)x4,(float)y4);
		    p.closePath();
		    g.setColor(Color.red);
		    g.draw(p);
		}	    
	    }  	

	    if (pg.gb("velocity_cmd")&&(velocity_cmd != null)){//draw velocity cmd
		
		g.setStroke(new BasicStroke((float) (2.0/scale)));
		double theta = rpy[2]+ Math.PI/2;
		
		double c = Math.cos(theta);
		double s = Math.sin(theta);
				
		double x1,x2,x3,x4,y1,y2,y3,y4;
		
		x1 = h1*c - b*s;
		y1 = h1*s + b*c;
		
		x2 = h1*c + b*s;
		y2 = h1*s - b*c;
		
		x3 = -h2*c + b*s;
		y3 = -h2*s - b*c;
		
		x4 = -h2*c - b*s;
		y4 = -h2*s + b*c;   
		
		if((Math.abs(velocity_cmd.tv)<0.001) 
		   && (Math.abs(velocity_cmd.rv)>0.0001)){ //rotating inplace
		    GeneralPath p = new GeneralPath();  		    
		    
		    p.moveTo((float)x1,(float)y1);
		    p.lineTo((float)x2,(float)y2);
		    p.lineTo((float)x3,(float)y3);
		    p.lineTo((float)x4,(float)y4);
		    
		    p.closePath();
		    g.setColor(Color.green);
		    g.draw(p); //draw line around robot
		}
		else if((Math.abs(velocity_cmd.tv)>0.001) 
			&& (Math.abs(velocity_cmd.rv)>0.001)){ 
		    
		    double top_left_x1=0.0,top_left_y1=0.0, top_left_x2=0.0,top_left_y2=0.0;		    
		    double r = 1.0/normalize_theta(velocity_cmd.rv),r1,r2;

		    double xc1 = 0.0, yc1 = 0.0,xc2 = 0.0, yc2 = 0.0;

		    g.setColor(Color.green);

		    x1 = - b*0.8*s;
		    y1 = b*0.8*c;
		    
		    x2 = b*0.8*s;
		    y2 = - b*0.8*c;

		    r1 = Math.abs(r);
		    r2 = Math.abs(r)+ 2*b*0.8;

		    double start_angle=0, extent = 360;

		    if(r<0){
			xc1 = x2 + r1*Math.sin(theta);
			yc1 = y2 - r1*Math.cos(theta);

			xc2 = x1 + r2*Math.sin(theta);
			yc2 = y1 - r2*Math.cos(theta);	

			start_angle = 270 - Math.toDegrees(theta);
			extent = 90.0;
		    }
		    else{
			xc1 = x1 - r1*Math.sin(theta);
			yc1 = y1 + r1*Math.cos(theta);
			
			xc2 = x2 - r2*Math.sin(theta);
			yc2 = y2 + r2*Math.cos(theta);
			
			start_angle = 360 - Math.toDegrees(theta);
			extent = 90.0;
		    }
		    
		    top_left_x1 = xc1 - r1;
		    top_left_y1 = yc1 - r1;
		    
		    top_left_x2 = xc2 - r2;
		    top_left_y2= yc2 - r2;

		    //draw the arcs

		    Arc2D arc = new Arc2D.Double(top_left_x1,top_left_y1, 
						 r1*2,r1*2,start_angle,extent
						 ,Arc2D.OPEN);
		    g.draw(arc);

		    

		    arc = new Arc2D.Double(top_left_x2,top_left_y2, 
						 r2*2,r2*2,start_angle,extent
						 ,Arc2D.OPEN);
		    g.draw(arc);

		    	    
		}
		
		else if(Math.abs(velocity_cmd.tv)>0.001) {

		    x1 = h1*c - b*0.8*s;
		    y1 = h1*s + b*0.8*c;
		    
		    x2 = h1*c + b*0.8*s;
		    y2 = h1*s - b*0.8*c;

		    x3 = (h1+4.0)*c - b*s;
		    y3 = (h1+4.0)*s + b*c;
		    
		    x4 = (h1+4.0)*c + b*s;
		    y4 = (h1+4.0)*s - b*c;

		    GeneralPath p = new GeneralPath();  
		    p.moveTo((float)x1,(float)y1);
		    p.lineTo((float)x3,(float)y3);
		    p.closePath();
		    g.setColor(Color.green);
		    g.draw(p); 

		    p = new GeneralPath();  
		    p.moveTo((float)x2,(float)y2);
		    p.lineTo((float)x4,(float)y4);
		    p.closePath();
		    g.setColor(Color.green);
		    g.draw(p);
		}	    
	    }
	    
	    
	    ///////// draw the robot
 	    {
		GeneralPath p = new GeneralPath();

		double theta = rpy[2]+ Math.PI/2;

		double c = Math.cos(theta);
		double s = Math.sin(theta);
		double x1,x2,x3,x4,y1,y2,y3,y4;
		x1 = h1*c - b*s;
		y1 = h1*s + b*c;

		x2 = h1*c + b*s;
		y2 = h1*s - b*c;
		
		x3 = -h2*c + b*s;
		y3 = -h2*s - b*c;

		x4 = -h2*c - b*s;
		y4 = -h2*s + b*c;

		p.moveTo((float)x1,(float)y1);
		p.lineTo((float)x2,(float)y2);
		p.lineTo((float)x3,(float)y3);
		p.lineTo((float)x4,(float)y4);
		
		p.closePath();
		g.setColor(Color.cyan);
		g.fill(p);

		p = new GeneralPath();
		
		p.moveTo((float)(x1+x2)/2,(float)(y1+y2)/2);
		p.lineTo((float)x3,(float)y3);
		p.lineTo((float)x4,(float)y4);
		
		p.closePath();
		g.setColor(Color.red);
		g.fill(p);
	    }

	    //draw vector command
	    {
		if(goal_x !=0.0 || goal_y !=0.0){
		    GeneralPath p = new GeneralPath();
		    p.moveTo(0,0);
		    p.lineTo((float)goal_x,(float)goal_y);
		    p.closePath();
		    g.setColor(Color.yellow);
		    //g.fill(p);
		    g.draw(p);
		    
		    g.setStroke(new BasicStroke((float) (3.0/scale))); // 1px
		    
		    double r = 0.2; //made scale invariant
		    Ellipse2D e = new Ellipse2D.Double(goal_x-r/2,goal_y-r/2,r,r);
		    g.fill(e);
		}
	    }

	    //draw people positions
	    {
		g.setStroke(new BasicStroke((float) (1.0/scale))); // 1px
		    
		double r = 0.2; //made scale invariant
		
        }
		*/
        
	}

	public void keyReleased(KeyEvent e) 
	{
	}

	public void keyPressed(KeyEvent e) 
	{
	    int amt = 8;
	    
	    //can add a stop key listener here

	    switch (e.getKeyCode())
		{
		case KeyEvent.VK_RIGHT:
		case KeyEvent.VK_KP_RIGHT:
		    pan(-amt, 0);
		    break;

		case KeyEvent.VK_LEFT:
		case KeyEvent.VK_KP_LEFT:
		    pan(amt, 0);
		    break;

		case KeyEvent.VK_UP:
		case KeyEvent.VK_KP_UP:
		    pan(0, amt);
		    break;

		case KeyEvent.VK_DOWN:
		case KeyEvent.VK_KP_DOWN:
		    pan(0, -amt);
		    break;
		}
	}

	public void keyTyped(KeyEvent e)
	{
	    //System.err.printf(""+e);
	    switch (e.getKeyChar())
		{
		case 'z':
		case '-':
		    zoom(.5, new Point(getWidth()/2, getHeight()/2));
		    break;

		case 'Z':
		case '+':
		    zoom(2, new Point(getWidth()/2, getHeight()/2));		
		    break;

		case 'S':
		case 's':
		    {
			//System.err.printf("Stopping\n");
			erlcm.speech_cmd_t speech_cmd = new  erlcm.speech_cmd_t();
			speech_cmd.utime = System.nanoTime()/1000;
			speech_cmd.cmd_type = "FOLLOWER";
			speech_cmd.cmd_property = "IDLE";
			//System.err.printf("Publishing\n");
			lcm.publish("PERSON_TRACKER",speech_cmd);
			//System.err.printf("Done\n");
			break;
		    }	
		case 'F':
		case 'f':
		    {
			erlcm.speech_cmd_t speech_cmd = new  erlcm.speech_cmd_t();
			speech_cmd.utime = System.nanoTime()/1000;
			speech_cmd.cmd_type = "FOLLOWER";
			speech_cmd.cmd_property = "START_FOLLOWING";
			lcm.publish("PERSON_TRACKER",speech_cmd);
			break;
		    }
		
		case 'L':
		case 'l':
		    {
			erlcm.speech_cmd_t speech_cmd = new  erlcm.speech_cmd_t();
			speech_cmd.utime = System.nanoTime()/1000;
			speech_cmd.cmd_type = "TRACKER";
			speech_cmd.cmd_property = "START_LOOKING";
			lcm.publish("PERSON_TRACKER",speech_cmd);
			break;
		    }

		default:
		    //		    System.out.println("key: "+e.getKeyChar());
		}

	    switch (e.getKeyCode())
		{
		default:
		    //		    System.out.println("key: "+e.getKeyCode());

		}
	}

	public void mouseMoved(MouseEvent e) {}
	public void mouseClicked(MouseEvent e) //throws java.awt.geom.NoninvertibleTransformException
	{		
	    if(pg.gb("set_goal")){
	    //if(0){
		Point p = e.getPoint();	
		

		Point2D.Double gui_point = new Point2D.Double((double)p.getX(),(double)p.getY());

		Point2D.Double user_point = new Point2D.Double();
		
		try{
		    T.inverseTransform(gui_point, user_point);

		    double x_act, y_act, x_goal, y_goal;
		    x_act = user_point.getX();
		    y_act = user_point.getY();

		    double c1 = Math.cos(robot_theta);
		    double s1 = Math.sin(robot_theta);

		    x_goal = x_act*c1 + y_act*s1;
		    y_goal = -x_act*s1 + y_act*c1;
		    
		    //x is to the front in the wheelchair coordinate system
		    //System.err.printf("Theta : %f Mouse Clicked (%f,%f) -> (%f,%f) => Goal (%f,%f)\n", robot_theta,p.getX(),p.getY(),
		    //	      user_point.getX(),user_point.getY(),x_goal,y_goal);


		    erlcm.vector_msg_t vector_cmd = new  erlcm.vector_msg_t();
		    
		    vector_cmd.utime = System.nanoTime()/1000;
		    vector_cmd.distance = Math.sqrt(Math.pow(x_goal,2.0)+Math.pow(y_goal,2.0));
		    vector_cmd.theta = Math.atan2(y_goal,x_goal);
		    
		    //System.err.printf("Goal : (%f,%f) Distance : %f, Theta : %f\n", 
		    //		      x_goal,y_goal,vector_cmd.distance,vector_cmd.theta);
		    
		    lcm.publish("ROBOT_VECTOR_CMD",vector_cmd);
		}
		catch (NoninvertibleTransformException inver_e){
		    System.err.printf("Can't inverse transform\n");
		}

	    }
	    // restore default view
	    if (e.getClickCount()==2) {
		T = null;

		repaint();
		filledin = !filledin;
	    }   
	}
	public void stop_robot(){
	    //System.out.println("\nStopping Robot");
	    erlcm.velocity_msg_t velocity_cmd = new  erlcm.velocity_msg_t();
		    
	    velocity_cmd.utime = System.nanoTime()/1000;
	    velocity_cmd.tv = 0.0;
	    velocity_cmd.rv = 0.0;
	    lcm.publish("ROBOT_VELOCITY_CMD",velocity_cmd);
	}

	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}

	Point dragBegin = null;
	public void mouseDragged(MouseEvent e)
	{
	    if(!pg.gb("set_goal")){
		Point p = e.getPoint();
		if (dragBegin != null) {
		    double tx = p.getX() - dragBegin.getX();
		    double ty = p.getY() - dragBegin.getY();

		    pan(tx, ty);
		}
		dragBegin = p;
	    }
	    else{
		issue_vec = true;
		//Might need to moderate how frequently we send this update to the robot
		Point p = e.getPoint();
		Point2D.Double gui_point = new Point2D.Double((double)p.getX(),(double)p.getY());

		Point2D.Double user_point = new Point2D.Double();
		
		try{
		    T.inverseTransform(gui_point, user_point);

		    double x_act, y_act, x_goal, y_goal;
		    x_act = user_point.getX();
		    y_act = user_point.getY();

		    double c1 = Math.cos(robot_theta);
		    double s1 = Math.sin(robot_theta);

		    x_goal = x_act*c1 + y_act*s1;
		    y_goal = -x_act*s1 + y_act*c1;
		    
		    goal_x = x_act;  //goal in user coordinate
		    goal_y = y_act;

		    erlcm.vector_msg_t vector_cmd = new  erlcm.vector_msg_t();
		    
		    vector_cmd.utime = System.nanoTime()/1000;
		    vector_cmd.distance = Math.sqrt(Math.pow(x_goal,2.0)+Math.pow(y_goal,2.0));
		    vector_cmd.theta = Math.atan2(y_goal,x_goal);
		    
		    lcm.publish("ROBOT_VECTOR_CMD",vector_cmd);
		}
		catch (NoninvertibleTransformException inver_e){
		    System.err.printf("Can't inverse transform\n");
		}
		repaint();

	    }
	}

	public void mousePressed(MouseEvent e) 
	{
	    dragBegin = e.getPoint();

	    
	}

	public void mouseReleased(MouseEvent e)
	{
	    dragBegin = null;
	    if(issue_vec == true){
		issue_vec = false;
		goal_x = 0;
		goal_y = 0;
		this.stop_robot();
	    }
	    	}

	void pan(double tx, double ty)
	{
		AffineTransform ST = AffineTransform.getTranslateInstance(tx, ty);
		T.preConcatenate(ST); // in pixel space
		repaint();
	}

	void zoom(double dscale, Point p)
	{
	    double newscale = getScale() * dscale;

	    if (newscale > MAX_ZOOM || newscale < MIN_ZOOM)
		return;

	    AffineTransform ST = new AffineTransform();
	    ST.translate(p.getX(), p.getY());
	    ST.scale(dscale, dscale);
	    ST.translate(-p.getX(), -p.getY());
	    
	    T.preConcatenate(ST);
	   
	    repaint();
	}

	public void mouseWheelMoved(MouseWheelEvent e)
	{
	    int amount=e.getWheelRotation();
	    double dscale = 1;

	    if (amount > 0)
		dscale = 0.5;
	    else
		dscale = 2;

	    zoom(dscale, e.getPoint());
	}
    }
