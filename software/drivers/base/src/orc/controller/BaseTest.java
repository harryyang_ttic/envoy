package orc.controller;

import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;

import orc.*;

import java.io.*;

import lcm.lcm.*;
//import lcmtypes.*;
import erlcm.*;

/** Sits in between the base module and the orc**/
public class BaseTest implements LCMSubscriber 
{
    Orc orc;
    Servo servo_tv, servo_rv;
    DigitalOutput digital_override; 
    LCM lcm;
    Motor motor;

    public static void main(String args[])
    {
	try {
	    new BaseTest(args);
	}
	catch (IOException ex) {
	    System.out.println("LCM IO Exception: " + ex);
	}		
    }

    public BaseTest(String args[])
	throws IOException
    {
        if (args.length > 0)
            orc = Orc.makeOrc(args[0]);
        else
            orc = Orc.makeOrc();
	//Servo(Orc orc, int port, double pos0, int usec0, double pos1, int usec1)
	//ports start at [0-7] slow & [8-15] fast for DigitalIO
	//ports range from [0-7] - these refer to the fast - for Servos
	
	servo_rv = new Servo(orc, 1, 0, 0, 0, 0);
	servo_tv = new Servo(orc, 0, 0, 0, 0, 0);
	digital_override = new DigitalOutput(orc, 0);
	/*for (int i = 9; i < 16; i++){
	    DigitalOutput d = new DigitalOutput(orc, i);
	    d.setValue(false);
	    }*/
	lcm = new LCM();
	lcm.subscribe("ORC_PWM_CMD", this);

	digital_override.setValue(false);

	try {
	    Thread.sleep(100);
	}
	catch (InterruptedException ex) {
	}

	/*motor = new Motor(orc, 0, false);

	  
	  motor.setPWM(0.5);*/
	    
	servo_rv.setPWM(2000, 0.55); //0.54-.55 is zero, i.e. 2.5V
	servo_tv.setPWM(2000, 0.75);
	//servo_tv.setPulseWidth(1000);
	/*while (true) {
	    try {
                digital_override.setValue(true);
                System.out.println("true");
                Thread.sleep(1000);
                digital_override.setValue(false);
                System.out.println("false");
                Thread.sleep(1000);
	    } catch (InterruptedException ex) {
	    }
	    }*/
	try {
	    Thread.sleep(100);
	}
	catch (InterruptedException ex) {
	}
	digital_override.setValue(true);
	new StatusPollThread().start();
    } 	

    class StatusPollThread extends Thread
    {
        StatusPollThread()
        {//since the daemon is not started - 
	    //this will cause it to exit after one loop - find out how the daemon can be started 
            //setDaemon(true);
        }

        public void run()
        {	    
            while(true) {
                try {
                    OrcStatus status = orc.getStatus();
		    //motor.setPWM(0.8);
		    System.out.printf(".");		   
                    orcStatus(orc, status);
                    Thread.sleep(20);  //might need to play with this 
                } catch (Exception ex) {
		    System.out.printf("Exception\n");
                    System.out.println("StatusPollThread ex: "+ex);
                }
            }
        }
    }

    boolean firstStatus = true;

    double lastTime = 0.0;

    int last_left = 0, last_right = 0;

    public void orcStatus(Orc orc, OrcStatus status)
    {
        // if uorc resets...
        if (status.utimeOrc < 2000000)
            firstStatus = true;


	orc_stat_msg_t msg = new orc_stat_msg_t();
	
	msg.utime = status.utimeOrc; 
	
	System.arraycopy(status.qeiPosition,0,msg.qei_position ,0,2);

	if(last_left !=0){
	    if(status.qeiPosition[0] - last_left >=0){
		msg.qei_velocity[0] = status.qeiVelocity[0];
	    }
	    else{
		msg.qei_velocity[0] = -status.qeiVelocity[0];
	    }	    
	}
	if(last_right !=0){
	    if(status.qeiPosition[1] - last_right >=0){
		msg.qei_velocity[1] = status.qeiVelocity[1];
	    }
	    else{
		msg.qei_velocity[1] = -status.qeiVelocity[1];
	    }	    
	}
	
	last_left = status.qeiPosition[0];
	last_right = status.qeiPosition[1];
	//System.arraycopy(status.qeiVelocity,0,msg.qei_velocity ,0,2);
	//msg.qei_velocity = status.qeiVelocity;

	lcm.publish ("BASE_POS_STAT", msg); 

	// = new double[] { 1, 2, 3 };

	/*char x[50];*/
	/*System.out.printf("Frequency : %f\n", 1.0/(status.utimeOrc/1000000.0 - lastTime));

	System.out.printf("[%.6f] Encoder 0 Pos : %6d\n",status.utimeOrc/1000000.0, status.qeiPosition[0]);
	System.out.printf("[%.6f] Encoder 1 Pos : %6d\n",status.utimeOrc/1000000.0, status.qeiPosition[1]);

	System.out.printf("[%.6f] Encoder 0 Vel : %6d\n",status.utimeOrc/1000000.0, status.qeiVelocity[0]);
	System.out.printf("[%.6f] Encoder 1 Vel : %6d\n",status.utimeOrc/1000000.0, status.qeiVelocity[1]);*/
	
	lastTime = status.utimeOrc/1000000.0;

	//servo.setPulseWidth(700);

	firstStatus = false;
    }

    public void messageReceived(LCM lcm, String channel, LCMDataInputStream ins)
    {
	try {
	    if (channel.equals("ORC_PWM_CMD")) { //pwm velocity command
		//issue the command to the correct port 
		orc_pwm_msg_t cmd = new orc_pwm_msg_t(ins);

		int duration = (int) (cmd.value * 3750.0); 
		//make sure its within the range  
		duration = Math.max(Math.min(duration,3750),0);

		System.out.print("C");
		//max interval seems to be 3750 on the uOrc Spy 
		//tv - 0 ;  rv - 1; override - 2 ;
		if (cmd.port_id == 0){
		    //servo_tv.setPulseWidth(duration);
		    System.out.printf("T - : %d\n", duration);
		}
		else if (cmd.port_id == 1){
		    //servo_rv.setPulseWidth(duration);
		    //System.out.print("r");
		    System.out.printf("R - : %d\n", duration);
		}
		/*else if (cmd.port_id == 2){
		    servo_override.setPulseWidth(duration);
		    System.out.print("O");
		    }*/
	    }
	} catch (IOException ex) {
	    System.out.println("Exception: " + ex);
	}
    }

}

