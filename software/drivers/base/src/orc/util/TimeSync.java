package orc.util;

import java.util.*;
import java.io.*;

/** An implementation of "A Passive Solution to the Sensor
 * Synchronization Problem" by Edwin Olson, 2010.
 **/
public class TimeSync
{
    long deviceTicks;
    double deviceTicksPerSecond;
    double R;
    double resetTime;

//    double p = -1, q;
    long p_ticks = -1, q_ticks;


    /** @param deviceTicksPerSecond How many ticks are there per second with the device's clock?
        @param R What is the maximum rate error between the two clocks?
        @param resetTime If the offset appears to change by more than
        resetTime, the clocks are resynchronized anew.
    **/
    public TimeSync(double deviceTicksPerSecond, double R, double resetTime)
    {
        this.deviceTicksPerSecond = deviceTicksPerSecond;
        this.R = R;
        this.resetTime = resetTime;
    }

    /** Call this whenever we observe the device's counter. **/
/*
// Reference implementation, which represents all times as
// 'doubles'. This can lead to precision issues.
    public synchronized void update(long hostUtime, long deviceTicks)
    {
        ////////////////////////////////////////////
        // Okay, now we're ready to do an update.

        double pi = deviceTicks / deviceTicksPerSecond;
        double qi = hostUtime / 1000000.0;

        if (p == -1 || Math.abs((pi-qi) - (p - q)) >= resetTime) {
            // first sync, or resynchronization
            p = pi;
            q = qi;
            return;
        }

        if (pi - qi >= p - q - Math.abs(R*(pi - p))) {
            p = pi;
            q = qi;
        }
    }

    public synchronized long toHostUtime(long deviceTicks)
    {
        double pi = deviceTicks / deviceTicksPerSecond;
        double Ai = p - q - Math.abs(R*(pi - p));

        double ti = pi - Ai;
        return (long) (ti*1000000.0);
    }
*/

    /* We can rewrite the update equations:
       pi - qi >= p - q - f(pi-p)

       as

       pi - p >= qi - q - f(pi-p)

       This form is superior, because we only need to be able to
       accurately represent changes in time for any single clock.
    **/
    public synchronized void update(long hostUtime, long deviceTicks)
    {
        long pi_ticks = deviceTicks;
        long qi_ticks = hostUtime;

        double dp = (pi_ticks - p_ticks) / deviceTicksPerSecond;
        double dq = (qi_ticks - q_ticks) / 1000000.0;

        if (p_ticks ==-1 || Math.abs(dp - dq) >= resetTime) {
            // first sync.
            p_ticks = pi_ticks;
            q_ticks = qi_ticks;
        }

        if (dp >= dq - Math.abs(R*dp)) {
            p_ticks = pi_ticks;
            q_ticks = qi_ticks;
        }
    }

    public synchronized long toHostUtime(long deviceTicks)
    {
        long pi_ticks = deviceTicks;

        double dp = (pi_ticks - p_ticks)/(1000000 / deviceTicksPerSecond);

        return ((long) dp) + q_ticks + ((long) Math.abs(R*dp));
    }

    public static void main(String args[])
    {
        try {
            // specify a file with host sensor timestamps on each line.
            BufferedReader ins = new BufferedReader(new FileReader(args[0]));
            String line;

            TimeSync ts = new TimeSync(1000000, 0.001, 0.5);

            while ((line = ins.readLine()) != null) {
                String toks[] = line.split("\\s+");
                long hostutime = Long.parseLong(toks[0]);
                long devutime = Long.parseLong(toks[1]);

                ts.update(hostutime, devutime);
                long predicthostutime = ts.toHostUtime(devutime);
                System.out.printf("%20d %20d %20d %20d\n", hostutime, devutime, predicthostutime, hostutime - predicthostutime);
            }
        } catch (IOException ex) {
            System.out.println("ex: "+ex);
        }

//        test(100000000, 0, 1000000, 0.001, 1000);
    }


    static void test(long htime, long dticks, double dTicksPerSecond, double R, long dt_usec)
    {
        TimeSync ts = new TimeSync(dTicksPerSecond, R, 5.0);
        Random r = new Random();

        for (int i = 0; i < 200; i++) {
            long latency = r.nextInt(100);
            ts.update(htime + latency, dticks);

            htime += dt_usec;
            dticks += (long) (dTicksPerSecond*dt_usec / 1000000.0);

            dticks += r.nextInt((int) (R*dTicksPerSecond*dt_usec / 1000000.0));

            System.out.printf("%16d %16d %16d %16d %d\n", htime, htime + latency, dticks, ts.toHostUtime(dticks), htime + latency - ts.toHostUtime(dticks));
        }
    }

}
