#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include "nodder_extractor.h"
#include <lcmtypes/erlcm_xyz_point_list_t.h>

typedef struct _state_t state_t;


struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    nodder_extractor_state_t *nodder; 
    int verbose;     
};

static void nodder_update_handler(int64_t utime, void *user)
{
  //fprintf(stderr," ++++++++ Callback Called\n");
  state_t *s = (state_t *)user;
  if(s->verbose)
    fprintf(stderr, "Called \n"); 
  xyz_point_list_t *ret = nodder_extract_new_points(s->nodder);
    
  if(ret != NULL){
    if(s->verbose)
      fprintf(stderr, "Size of Points : %d \n", ret->no_points);

    erlcm_xyz_point_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.no_points = ret->no_points;
    
    msg.points = (erlcm_xyz_point_t *)calloc(msg.no_points, sizeof(erlcm_xyz_point_t));

    for (size_t k = 0; k < ret->no_points; ++k){
      msg.points[k].xyz[0] = ret->points[k].xyz[0];//cloud_p->points[k].x; 
      msg.points[k].xyz[1] = ret->points[k].xyz[1]; 
      msg.points[k].xyz[2] = ret->points[k].xyz[2]; 
    }

    //publish
    erlcm_xyz_point_list_t_publish(s->lcm, "PCL_XYZ_LIST", &msg);
    free(msg.points);    

    destroy_xyz_list(ret);
  } 
}

//doesnt do anything right now - timeout function
gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;
    if(s->verbose)
        fprintf(stderr, "Called \n"); 
    xyz_point_list_t *ret = nodder_extract_new_points(s->nodder);
    
    if(ret != NULL){
        if(s->verbose)
            fprintf(stderr, "Size of Points : %d \n", ret->no_points);

        erlcm_xyz_point_list_t msg;
        msg.utime = bot_timestamp_now(); 
        msg.no_points = ret->no_points;
    
        msg.points = (erlcm_xyz_point_t *)calloc(msg.no_points, sizeof(erlcm_xyz_point_t));

        for (size_t k = 0; k < ret->no_points; ++k){
            msg.points[k].xyz[0] = ret->points[k].xyz[0];//cloud_p->points[k].x; 
            msg.points[k].xyz[1] = ret->points[k].xyz[1]; 
            msg.points[k].xyz[2] = ret->points[k].xyz[2]; 
        }

        //publish
        erlcm_xyz_point_list_t_publish(s->lcm, "PCL_XYZ_LIST", &msg);
        free(msg.points);    

        destroy_xyz_list(ret);
    } 
    //return true to keep running
    return TRUE;
}


void main(int argc, char **argv)
{
    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    state->lcm =  bot_lcm_get_global(NULL);

    state->nodder = nodder_extractor_init_full(state->lcm, 1, -40, 40, NULL, 
					       &nodder_update_handler, (void *) state);

    if(state->nodder== NULL){
        fprintf(stderr,"Issue getting collector\n");
        return;
    }

    state->mainloop = g_main_loop_new( NULL, FALSE );  

    const char *optstring = "vh";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "verbose", no_argument, 0, 'v' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    //usage(argv[0]);
            fprintf(stderr, "No help found - please add help\n");
	    break;
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
                state->verbose = 1;
		break;
	    }
        }
    }
  
    if (!state->mainloop){
	printf("Couldn't create main loop\n");
	return;
    }
    
    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);
    
    /* heart beat*/
    //g_timeout_add (100, heartbeat_cb, state);
    
    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
    
    bot_glib_mainloop_detach_lcm(state->lcm);
}
