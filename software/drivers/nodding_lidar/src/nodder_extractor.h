#ifndef __nodder_extractor_h__
#define __nodder_extractor_h__

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <point_types/point_types.h>
#include <lcmtypes/senlcm_nodder_status_t.h>

#define MAX_LASER_RETURNS 1081
#define MAX_RETURNS 1000

#ifdef __cplusplus
extern "C" {
#endif

  typedef void(on_collection_update_t)(int64_t utime, void *user);

  typedef struct {
    on_collection_update_t * callback_func;
    void * user;
  } on_update_handler_t;
  

    typedef struct _nodder_returns_t{
        int64_t utime;
        int no_returns; 
        bot_core_planar_lidar_t **returns;
    } nodder_returns_t;

    typedef struct _nodder_extractor_state_t nodder_extractor_state_t;
    struct _nodder_extractor_state_t {
        lcm_t *lcm;
        BotParam *param;
        BotFrames *frames;
        GMainLoop *mainloop;
        bot_core_pose_t *bot_pose_last;

        nodder_returns_t *lr;
        int64_t last_returned_time;
        int64_t last_collector_utime;
      
        on_update_handler_t *on_update;

        int have_data;
        
        //lets keep the laser collection in the GArray 
        GList    *pose_list;     
        GArray   *laser_returns;       //!< array of laser returns
        int64_t  utime_collection;
        int no_returns; 
        
        double start_angle; 
        double end_angle; 
        char *channel_name; 
        char *frame_name;

        double max_pitch; 
        double min_pitch;
        int going_up;
        //velodyne_calib_t *calib;
        //velodyne_laser_return_collector_t *collector;
        senlcm_nodder_status_t *last_status;
        //this is protected by the mutex
        //velodyne_laser_return_collection_t *lrc; 
    
        int64_t 	  last_nodder_data_utime;
        int64_t           last_pose_utime;
        GMutex *mutex;
        GMutex *mutex_lrc;
    };

    

    int nodder_push_scans(nodder_extractor_state_t *self, bot_core_planar_lidar_t *laser);
    
    void nodder_clear_scans(nodder_extractor_state_t *self);

    xyz_point_list_t * nodder_extract_points(nodder_extractor_state_t *self);

    xyz_point_list_t *nodder_extract_points_frame(nodder_extractor_state_t *self, char *frame);

    xyz_point_list_t *nodder_extract_points_frame_decimate(nodder_extractor_state_t *self, char *frame, int decimation_factor, double max_range);
    
  //nodder_extractor_state_t * nodder_extractor_init(lcm_t *lcm, );
  nodder_extractor_state_t * nodder_extractor_init(lcm_t *lcm, on_collection_update_t * callback, void *user);

  nodder_extractor_state_t * nodder_extractor_init_full(lcm_t *lcm, uint8_t whole_scan, double start_angle, double end_angle, char *channel, on_collection_update_t *callback, void *user); 

    //get points only if there are new ones 
    xyz_point_list_t *nodder_extract_new_points(nodder_extractor_state_t *self);
    
    xyz_point_list_t *nodder_extract_new_points_frame(nodder_extractor_state_t *self, char *frame);

  void nodder_extractor_destroy(nodder_extractor_state_t *self);

#ifdef __cplusplus
}
#endif

#endif
