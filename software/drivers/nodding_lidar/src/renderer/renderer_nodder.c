#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <gdk/gdkkeysyms.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <gsl/gsl_blas.h>

#include <bot_core/bot_core.h>
#include <bot_vis/viewer.h>
#include <bot_vis/gtk_util.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <er_common/path_util.h>

#include <nodder/nodder_extractor.h>

#define DTOR M_PI/180
#define RTOD 180/M_PI

#define MAX_REFRESH_RATE_USEC 30000 // about 1/30 of a second

#define RENDERER_NAME "Nodder"
#define NODDER_DATA_CIRC_SIZE 10
#define POSE_DATA_CIRC_SIZE 200 

#define PARAM_COLOR_MENU "Color"
#define PARAM_HISTORY_LENGTH "Scan Hist. Len."
#define PARAM_HISTORY_FREQUENCY "Scan Hist. Freq."
#define PARAM_POINT_SIZE "Point Size" 
#define PARAM_POINT_ALPHA "Point Alpha" 
#define GAMMA 0.1

typedef struct _rate_t {
    double current_hz;
    int64_t last_tick;
    int64_t tick_count;
} rate_t;

enum {
    COLOR_Z,
    COLOR_INTENSITY,
    COLOR_NONE,
};


typedef struct _pose_data_t pose_data_t;
struct _pose_data_t {
    double pose[6];
    double motion[6];
    int64_t utime;
};

typedef struct _RendererNodder RendererNodder;
struct _RendererNodder {
    BotRenderer renderer;
  BotPtrCircular   *nodder_data_circ;
    //pose_t *pose;

    lcm_t *lcm;
  
  nodder_extractor_state_t *nodder; 
  xyz_point_list_t *points; 
  BotParam *param;
  BotFrames *frames;
    int64_t last_collector_utime;

    int have_data;
    
    BotViewer         *viewer;
    BotGtkParamWidget *pw;   

    GMutex *mutex;
};

// convenience function to get the bot's position in the local frame
static int
frames_vehicle_pos_local (BotFrames *frames, double pos[3])
{
    double pos_body[3] = {0, 0, 0};
    return bot_frames_transform_vec (frames, "body", "local", pos_body, pos);
}

static void nodder_update_handler(int64_t utime, void *user)
{
  RendererNodder *s = (RendererNodder *)user;
  g_mutex_lock(s->mutex);
  xyz_point_list_t *points = nodder_extract_points_frame(s->nodder, "body");//nodder_extract_new_points(s->nodder);
  if(points){
    bot_ptr_circular_add (s->nodder_data_circ, points);
    //fprintf(stderr, "\npoints : %d\n", points->no_points);
  }
  g_mutex_unlock(s->mutex);
  bot_viewer_request_redraw (s->viewer);
} 

static void
renderer_nodder_destroy (BotRenderer *renderer)
{
    if (!renderer)
        return;

    RendererNodder *self = (RendererNodder *) renderer->user;
    if (!self)
        return;
    if (self->nodder_data_circ)
      bot_ptr_circular_destroy (self->nodder_data_circ);
    g_mutex_free (self->mutex);
    if(self->nodder)
      nodder_extractor_destroy(self->nodder);
    
    free (self);
}


static void 
renderer_nodder_draw (BotViewer *viewer, BotRenderer *renderer)
{
    RendererNodder *self = (RendererNodder*)renderer->user;
    g_assert(self);
    
    int colormode = bot_gtk_param_widget_get_enum(self->pw, PARAM_COLOR_MENU);
    if(colormode == COLOR_NONE)
        return;
    g_mutex_lock(self->mutex);
    if(!self->nodder_data_circ || bot_ptr_circular_size(self->nodder_data_circ) == 0){
      g_mutex_unlock(self->mutex);
      return;
    }

    double alpha = bot_gtk_param_widget_get_double(self->pw, PARAM_POINT_ALPHA);
    
    int hist_len = bot_gtk_param_widget_get_int (self->pw, PARAM_HISTORY_LENGTH);
 
    //fprintf(stderr,"Buffer Size : %d\n", bot_ptr_circular_size(self->nodder_data_circ));

    for (unsigned int cidx = 0;
         cidx < bot_ptr_circular_size(self->nodder_data_circ) && cidx < hist_len;
         cidx++) {

      glPushAttrib (GL_DEPTH_BUFFER_BIT | GL_POINT_BIT | GL_CURRENT_BIT);
      glEnable (GL_DEPTH_TEST);
      glEnable(GL_BLEND);
      glPointSize(bot_gtk_param_widget_get_double(self->pw, PARAM_POINT_SIZE));
      glBegin(GL_POINTS);
 
        
      xyz_point_list_t *points = bot_ptr_circular_index(self->nodder_data_circ, cidx);

      //copy and convert 
      double body_to_local[12];
      if (!bot_frames_get_trans_mat_3x4_with_utime (self->frames, "body",
                                                    "local",  points->utime,
                                                    body_to_local)) {
          fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
          g_mutex_unlock(self->mutex);
          return NULL;        
      }

      if(points==NULL){
	fprintf(stderr,"Error Points : Null \n");
	continue;
      }

      //fprintf(stderr, "Points : %p\n", (void *) points);

      //fprintf(stderr, "[%d] Point Size : %d\n", cidx, points->no_points);
      double pos_l[3];

      for (int i=0; i< points->no_points; i++){
	switch (colormode)  {
	case COLOR_INTENSITY: {
	  glColor4d(0.0, 1.0, 0.0, alpha);
	  break;
	}
	case COLOR_Z: {
	  double z =  points->points[i].xyz[2];// - bot_pos[2];
	  double Z_MIN = 0, Z_MAX = 3;
	  double z_norm_scale = 1 / (Z_MAX-Z_MIN);
	  double z_norm = (z - Z_MIN) * z_norm_scale;
	  glColor3fv(bot_color_util_jet(z_norm));
	  break;
	}
	default:
	  break;
	}           



        bot_vector_affine_transform_3x4_3d (body_to_local, points->points[i].xyz, pos_l);
           
	glVertex3d (pos_l[0], pos_l[1], pos_l[2]);
      
      }
      glEnd();
      glPopAttrib();
    }

    g_mutex_unlock(self->mutex);

    /*double bot_pos[3];
    if (!frames_vehicle_pos_local (self->frames, bot_pos))
        colormode = COLOR_INTENSITY; // Render intensity if there is no pose for height

    // draw the velodyne point cloud
    int hist_len = bot_gtk_param_widget_get_int (self->pw, PARAM_HISTORY_LENGTH);
    */
    
    /*for (unsigned int cidx = 0;
         cidx < bot_ptr_circular_size(self->velodyne_data_circ) && cidx < hist_len;
         cidx++) {
        
        velodyne_laser_return_collection_t *lrc = bot_ptr_circular_index(self->velodyne_data_circ, cidx);

        double sensor_to_local[12];

        if (!bot_frames_get_trans_mat_3x4 (self->frames, "VELODYNE",
                                           "local", 
                                           sensor_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            return;
            }
       

        glPushAttrib (GL_DEPTH_BUFFER_BIT | GL_POINT_BIT | GL_CURRENT_BIT);
        glEnable (GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glPointSize(bot_gtk_param_widget_get_double(self->pw, PARAM_POINT_SIZE));
        glBegin(GL_POINTS);
        
        double alpha = bot_gtk_param_widget_get_double(self->pw, PARAM_POINT_ALPHA);

        //fprintf(stderr,"LASER returns : %d\n", lrc->num_lr);

        int chunk_size = 32;// * 12; 

        for (int s = 0; s < lrc->num_lr; s++) {
            velodyne_laser_return_t *lr = &(lrc->laser_returns[s]);
            
            if(s % chunk_size == 0){
                //updated the sensor_to_local transform 
                if (!bot_frames_get_trans_mat_3x4_with_utime (self->frames, "VELODYNE",
                                                              "local", lr->utime,
                                                              sensor_to_local)) {
                    fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
                    return;
                }
            }
            
            
            //fprintf(stderr, "\t %d - %d : %f\n", lr->physical, lr->logical, lr->phi);
            double local_xyz[3];
            bot_vector_affine_transform_3x4_3d (sensor_to_local, lr->xyz, local_xyz);
           
            switch (colormode)  {
            case COLOR_INTENSITY: {
                double v = lr->intensity;
                glColor4d(v, v, v, alpha);
                break;
            }
            case COLOR_Z: {
                double z = local_xyz[2] - bot_pos[2];
                double Z_MIN = 0, Z_MAX = 3;
                double z_norm_scale = 1 / (Z_MAX-Z_MIN);
                double z_norm = (z - Z_MIN) * z_norm_scale;
                glColor3fv(bot_color_util_jet(z_norm));
                break;
            }
            default:
                break;
            }           
           
            glVertex3d (local_xyz[0], local_xyz[1], local_xyz[2]);
           
           
           
	    }
        
        glEnd();
        glPopAttrib();
	}*/
}

static void
on_param_widget_changed (BotGtkParamWidget *pw, const char *name, void *user)
{
    RendererNodder *self = user;
    bot_viewer_request_redraw (self->viewer);
}

static void
on_load_preferences (BotViewer *viewer, GKeyFile *keyfile, void *user_data)
{
    RendererNodder *self = user_data;
    bot_gtk_param_widget_load_from_key_file (self->pw, keyfile, self->renderer.name);
}

static void
on_save_preferences (BotViewer *viewer, GKeyFile *keyfile, void *user_data)
{
    RendererNodder *self = user_data;
    bot_gtk_param_widget_save_to_key_file (self->pw, keyfile, self->renderer.name);
}

void
circ_free_nodder_data(void *user, void *p) {
  xyz_point_list_t *np = (xyz_point_list_t *) p; 
  destroy_xyz_list(np);
}

static RendererNodder *
renderer_nodder_new (BotViewer *viewer, BotParam * param, lcm_t *_lcm)
{    
    RendererNodder *self = (RendererNodder*) calloc (1, sizeof (*self));

    self->viewer = viewer;

    BotRenderer *renderer = &self->renderer;
    renderer->draw = renderer_nodder_draw;
    renderer->destroy = renderer_nodder_destroy;
    renderer->widget = bot_gtk_param_widget_new();
    renderer->name = RENDERER_NAME;
    renderer->user = self;
    renderer->enabled = 1;

    self->lcm = _lcm;
    if (!self->lcm) {
        fprintf (stderr,"Error: setup_renderer_laser() failed to get global lcm object\n");
        renderer_nodder_destroy (renderer);
        return NULL;
    }


    self->param = param;
    if (!self->param) {
        fprintf (stderr,"Error: setup_renderer_laser() failed to get BotParam instance\n");
        renderer_nodder_destroy (renderer);
        return NULL;
    }

    self->frames = bot_frames_get_global (self->lcm, self->param);

    
    self->nodder = nodder_extractor_init_full(self->lcm, 1, -40, 40, NULL, 
			       &nodder_update_handler, (void *) self);

    self->nodder_data_circ = bot_ptr_circular_new (NODDER_DATA_CIRC_SIZE,
						   circ_free_nodder_data, self);

    self->mutex = g_mutex_new ();
    
    self->pw = BOT_GTK_PARAM_WIDGET (renderer->widget);

    gtk_widget_show_all (renderer->widget);
    g_signal_connect (G_OBJECT (self->pw), "changed", 
                      G_CALLBACK (on_param_widget_changed), self);
    g_signal_connect (G_OBJECT (viewer), "load-preferences", 
                      G_CALLBACK (on_load_preferences), self);
    g_signal_connect (G_OBJECT (viewer), "save-preferences",
                      G_CALLBACK (on_save_preferences), self);
    
    bot_gtk_param_widget_add_enum(self->pw, PARAM_COLOR_MENU, 
                                  BOT_GTK_PARAM_WIDGET_MENU, COLOR_Z,
                                  "Height", COLOR_Z,
                                  "Intensity", COLOR_INTENSITY,
                                  NULL);
    
    bot_gtk_param_widget_add_int(self->pw, PARAM_HISTORY_LENGTH, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 0,
                                 NODDER_DATA_CIRC_SIZE, 1, 1);
    
    
    bot_gtk_param_widget_add_double(self->pw, PARAM_HISTORY_FREQUENCY, 
                                    BOT_GTK_PARAM_WIDGET_SLIDER, 
                                    0.1, 10, 0.1, 0.5);
    
    bot_gtk_param_widget_add_double(self->pw, PARAM_POINT_SIZE, 
                                    BOT_GTK_PARAM_WIDGET_SLIDER, 
                                    0.5, 10, 0.5, 1.0);
    
    bot_gtk_param_widget_add_double(self->pw, PARAM_POINT_ALPHA, 
                                    BOT_GTK_PARAM_WIDGET_SLIDER, 
                                    0, 1, 0.5, 1.0);
    
    return self;
}

void
setup_renderer_nodder (BotViewer *viewer, int priority, BotParam * param, lcm_t *lcm)
{
    RendererNodder *self = renderer_nodder_new (viewer, param, lcm);
    bot_viewer_add_renderer (viewer, &self->renderer, priority);
}
