#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>

#include <gsl/gsl_blas.h>

#include <er_common/path_util.h>
#include <point_types/point_types.h>
#include <lcmtypes/senlcm_nodder_status_t.h>
#include <lcmtypes/dynamixel_status_list_t.h>
#include "nodder_extractor.h"



void
nodder_reset_collector (nodder_extractor_state_t *self) {
   
    if(self->no_returns>0){
        for (int i=0; i<(self->no_returns); i++) {
            bot_core_planar_lidar_t *laser = g_array_index(self->laser_returns, bot_core_planar_lidar_t *, i);
            bot_core_planar_lidar_t_destroy(laser);
      }
      
      self->max_pitch = -1000000.0;
      self->min_pitch = 1000000.0;
      self->no_returns = 0;
      //not sure if this clears the data in it 
      g_array_free (self->laser_returns, TRUE);
      self->laser_returns = g_array_new (FALSE, TRUE, sizeof (bot_core_planar_lidar_t *));
    }
}

void reset_return(nodder_returns_t *lr){
    if(lr){
        for(int i=0; i < lr->no_returns; i++)
            bot_core_planar_lidar_t_destroy(lr->returns[i]);
        free(lr->returns);
        lr->no_returns = 0;
        free(lr);
    }    
}

void nodder_extractor_destroy(nodder_extractor_state_t *self){
  if(self){
    reset_return(self->lr);
    g_mutex_free (self->mutex);
    g_mutex_free (self->mutex_lrc);
    if(self->on_update){
      free(self->on_update);
    }
    free(self->channel_name);
  }
  free(self);
}

nodder_returns_t *get_laser_return(nodder_extractor_state_t *self){
    if(self->no_returns==0)
        return NULL;
    nodder_returns_t *lr = (nodder_returns_t *) calloc(1, sizeof(nodder_returns_t));
    lr->no_returns = self->no_returns;
    lr->returns = (bot_core_planar_lidar_t **) calloc(lr->no_returns , sizeof(bot_core_planar_lidar_t *));
    
    for (int i=0; i<(self->no_returns); i++) {      
        lr->returns[i] = bot_core_planar_lidar_t_copy(g_array_index(self->laser_returns, bot_core_planar_lidar_t *, i));
    }
    lr->utime = lr->returns[0]->utime;
    return lr;
}


int nodder_push_scans(nodder_extractor_state_t *self, bot_core_planar_lidar_t *laser){
    //take the laser and add it to a data structure 
    g_mutex_lock (self->mutex_lrc);

    if(self->no_returns > MAX_RETURNS){
        fprintf(stderr, "Exceeded max size - restting\n");
        nodder_reset_collector (self);
    }

    double rpy_nodder[] = {0, 0, 0};
    double quat_nodder[4], quat_body_0[4];
    double rpy_body_0[3];
    bot_roll_pitch_yaw_to_quat (rpy_nodder, quat_nodder);
    BotTrans sensor_to_body;      
    
    if (!bot_frames_get_trans_with_utime (self->frames, "nodder", "body", laser->utime, &sensor_to_body))
        fprintf (stderr, "Angle check error - frame issue\n");
        
    bot_quat_mult (quat_body_0, sensor_to_body.rot_quat, quat_nodder);
        
    bot_quat_to_roll_pitch_yaw (quat_body_0, rpy_body_0);

    double pitch = -rpy_body_0[1]; 
    
    if(self->last_status){
        if(self->going_up){
            if(self->last_status->going_up == 0){
                g_mutex_lock (self->mutex);
                reset_return(self->lr);
                self->lr = get_laser_return(self);
                g_mutex_unlock (self->mutex);
                nodder_reset_collector (self);
                self->going_up = 0;

		//do the callback 
		if(self->on_update)
		  self->on_update->callback_func(laser->utime, self->on_update->user);
            }
        }
        if(self->going_up==0){
            if(self->last_status->going_up == 1){
                g_mutex_lock (self->mutex);
                reset_return(self->lr);
                self->lr = get_laser_return(self);
                g_mutex_unlock (self->mutex);
                nodder_reset_collector (self);
                self->going_up = 1;

		//do the callback 
		if(self->on_update)
		  self->on_update->callback_func(laser->utime, self->on_update->user);
            }
        }
    }
        
    g_array_append_val (self->laser_returns, laser);
    self->no_returns++;
    g_mutex_unlock (self->mutex_lrc);

    return 1;
}

xyz_point_list_t *nodder_extract_points(nodder_extractor_state_t *self){

    xyz_point_list_t *plist = nodder_extract_points_frame(self, "local");
    
    return plist;
}

xyz_point_list_t *nodder_extract_new_points(nodder_extractor_state_t *self){
  g_mutex_lock (self->mutex);
    if(self->lr==NULL){
        g_mutex_unlock (self->mutex);
        return NULL;
    }
    if(self->lr->no_returns >0){
        bot_core_planar_lidar_t *laser = self->lr->returns[0];
        if(self->last_returned_time == laser->utime){
            g_mutex_unlock (self->mutex);
            return NULL;
        }
    }
    g_mutex_unlock (self->mutex);
    xyz_point_list_t *plist = nodder_extract_points_frame(self, "local");
    
    return plist;
}

xyz_point_list_t *nodder_extract_points_frame_no_comp(nodder_extractor_state_t *self, char *frame){
    g_mutex_lock (self->mutex);
    if(self->lr==NULL){
        g_mutex_unlock (self->mutex);
        return NULL;
    }
    // copy the laser returns to the output
    if(self->lr->no_returns == 0){
        g_mutex_unlock (self->mutex);
        return NULL;
    }

   
    //maybe we need a more ordered data structure ?? 
    xyz_point_list_t *p_list = (xyz_point_list_t *) calloc(1, sizeof(xyz_point_list_t));
    p_list->points = (xyz_point_t *) calloc(self->lr->no_returns * MAX_LASER_RETURNS , sizeof(xyz_point_t)); 

    int s = 0;

    for (int i=0; i<(self->lr->no_returns); i++) {

        bot_core_planar_lidar_t *laser = self->lr->returns[i];

        if(i==0){
            p_list->utime = laser->utime;
        }
        //copy and convert 
        double sensor_to_local[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (self->frames, self->frame_name,
                                                      frame,  laser->utime,
                                                      sensor_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            g_mutex_unlock (self->mutex_lrc);
            return NULL;        
        }
        
        for(int j=0; j < laser->nranges; j++){
            
            double theta = laser->rad0 + 
                (double)j * laser->radstep;

            double xyz_sensor[3] = {laser->ranges[j] * cos(theta), laser->ranges[j]*sin(theta), 0}, xyz_local[3];

            if(theta < -M_PI/2 || theta > M_PI/2)
                continue;

            bot_vector_affine_transform_3x4_3d (sensor_to_local, xyz_sensor, p_list->points[s].xyz);
            s++; 
        }
    }

    p_list->no_points = s; 
    p_list->points = (xyz_point_t *) realloc(p_list->points , s * sizeof(xyz_point_t)); 

    self->last_returned_time = p_list->utime;

    g_mutex_unlock (self->mutex);
    
    return p_list;

}


xyz_point_list_t *nodder_extract_points_frame(nodder_extractor_state_t *self, char *frame){
    g_mutex_lock (self->mutex);
    if(self->lr==NULL){
        g_mutex_unlock (self->mutex);
        return NULL;
    }
    // copy the laser returns to the output
    if(self->lr->no_returns == 0){
        g_mutex_unlock (self->mutex);
        return NULL;
    }

   
    //maybe we need a more ordered data structure ?? 
    xyz_point_list_t *p_list = (xyz_point_list_t *) calloc(1, sizeof(xyz_point_list_t));
    p_list->points = (xyz_point_t *) calloc(self->lr->no_returns * MAX_LASER_RETURNS , sizeof(xyz_point_t)); 

    int s = 0;

    double local_to_frame[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (self->frames, "local",
                                                  frame,  self->lr->returns[0]->utime,
                                                  local_to_frame)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        g_mutex_unlock (self->mutex_lrc);
        return NULL;        
    }

    for (int i=0; i<(self->lr->no_returns); i++) {

        bot_core_planar_lidar_t *laser = self->lr->returns[i];

        if(i==0){
            p_list->utime = laser->utime;
        }
        //copy and convert 
        double sensor_to_local[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (self->frames, self->frame_name,
                                                      "local",  laser->utime,
                                                      sensor_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            g_mutex_unlock (self->mutex_lrc);
            return NULL;        
        }        
        
        for(int j=0; j < laser->nranges; j++){
            
            double theta = laser->rad0 + 
                (double)j * laser->radstep;

            double xyz_sensor[3] = {laser->ranges[j] * cos(theta), laser->ranges[j]*sin(theta), 0}, xyz_local[3];

            if(theta < -M_PI/2 || theta > M_PI/2)
                continue;

            bot_vector_affine_transform_3x4_3d (sensor_to_local, xyz_sensor, xyz_local);
            bot_vector_affine_transform_3x4_3d (local_to_frame, xyz_local, p_list->points[s].xyz);
            s++; 
        }
    }

    p_list->no_points = s; 
    p_list->points = (xyz_point_t *) realloc(p_list->points , s * sizeof(xyz_point_t)); 

    self->last_returned_time = p_list->utime;

    g_mutex_unlock (self->mutex);
    
    return p_list;

}

xyz_point_list_t *nodder_extract_points_frame_decimate(nodder_extractor_state_t *self, char *frame, int decimation_factor, double max_range){
    g_mutex_lock (self->mutex);
    if(self->lr==NULL){
        g_mutex_unlock (self->mutex);
        return NULL;
    }
    // copy the laser returns to the output
    if(self->lr->no_returns == 0){
        g_mutex_unlock (self->mutex);
        return NULL;
    }

   
    //maybe we need a more ordered data structure ?? 
    xyz_point_list_t *p_list = (xyz_point_list_t *) calloc(1, sizeof(xyz_point_list_t));
    p_list->points = (xyz_point_t *) calloc(self->lr->no_returns * MAX_LASER_RETURNS , sizeof(xyz_point_t)); 

    int s = 0;

    int c_ind = floor(self->lr->no_returns/2);

    double local_to_frame[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (self->frames, "local",
                                                  frame,  self->lr->returns[c_ind]->utime,
                                                  local_to_frame)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        g_mutex_unlock (self->mutex_lrc);
        return NULL;        
    }

    p_list->utime = self->lr->returns[c_ind]->utime;

    for (int i=0; i<(self->lr->no_returns); i++) {

        bot_core_planar_lidar_t *laser = self->lr->returns[i];

        //if(i!= c_ind)
        //  continue;
        /*if(i==0){
            p_list->utime = laser->utime;
        }
        else{
            continue;
            }*/
        //copy and convert 
        double sensor_to_local[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (self->frames, self->frame_name,
                                                      "local",  laser->utime,
                                                      sensor_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            g_mutex_unlock (self->mutex_lrc);
            return NULL;        
        }        
        
        for(int j=0; j < laser->nranges; j++){
            
            double theta = laser->rad0 + 
                (double)j * laser->radstep;
            
            double xyz_sensor[3] = {laser->ranges[j] * cos(theta), laser->ranges[j]*sin(theta), 0}, xyz_local[3];
            
            if(theta < -M_PI/2 || theta > M_PI/2 || laser->ranges[j] > max_range){
                continue;
            }
            
            bot_vector_affine_transform_3x4_3d (sensor_to_local, xyz_sensor, xyz_local);
            bot_vector_affine_transform_3x4_3d (local_to_frame, xyz_local, p_list->points[s].xyz);
            s++; 
            j+= decimation_factor;
        }
    }

    p_list->no_points = s; 
    p_list->points = (xyz_point_t *) realloc(p_list->points , s * sizeof(xyz_point_t)); 

    self->last_returned_time = p_list->utime;

    g_mutex_unlock (self->mutex);
    
    return p_list;

}


xyz_point_list_t *nodder_extract_new_points_frame(nodder_extractor_state_t *self, char *frame){
    g_mutex_lock (self->mutex);
    if(self->lr==NULL){
        g_mutex_unlock (self->mutex);
        return NULL;
    }
    if(self->lr->no_returns >0){
        bot_core_planar_lidar_t *laser = self->lr->returns[0];
        if(self->last_returned_time == laser->utime){
            g_mutex_unlock (self->mutex);
            return NULL;
        }
    }
    g_mutex_unlock (self->mutex);

    xyz_point_list_t *plist = nodder_extract_points_frame(self, frame);
    
    return plist;
}

xyz_point_list_t *nodder_extract_points_frame_comp(nodder_extractor_state_t *self, char *frame){
    g_mutex_lock (self->mutex);
    if(self->lr==NULL){
        g_mutex_unlock (self->mutex);
        return NULL;
    }
    // copy the laser returns to the output
    if(self->lr->no_returns == 0){
        g_mutex_unlock (self->mutex);
        return NULL;
    }

   
    //maybe we need a more ordered data structure ?? 
    xyz_point_list_t *p_list = (xyz_point_list_t *) calloc(1, sizeof(xyz_point_list_t));
    p_list->points = (xyz_point_t *) calloc(self->lr->no_returns * MAX_LASER_RETURNS , sizeof(xyz_point_t)); 

    int s = 0;

    double local_to_frame[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (self->frames, "local",
                                                  frame,  self->lr->returns[0]->utime, //need to be motion compensated - we do this trick
                                                  local_to_frame)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        g_mutex_unlock (self->mutex_lrc);
        return NULL;        
    }
    
    for (int i=0; i<(self->lr->no_returns); i++) {

        bot_core_planar_lidar_t *laser = self->lr->returns[i];

        if(i==0){
            p_list->utime = laser->utime;
        }
        //copy and convert 
        double sensor_to_local[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (self->frames, self->frame_name,
                                                      "local",  laser->utime,
                                                      sensor_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            g_mutex_unlock (self->mutex_lrc);
            return NULL;        
        }
        
        for(int j=0; j < laser->nranges; j++){
            
            double theta = laser->rad0 + 
                (double)j * laser->radstep;

            double xyz_sensor[3] = {laser->ranges[j] * cos(theta), laser->ranges[j]*sin(theta), 0}, xyz_local[3];

            bot_vector_affine_transform_3x4_3d (sensor_to_local, xyz_sensor, xyz_local);
            bot_vector_affine_transform_3x4_3d (local_to_frame, xyz_local, p_list->points[s].xyz); 
            s++; 
        }
    }

    p_list->no_points = s; 

    self->last_returned_time = p_list->utime;

    g_mutex_unlock (self->mutex);
    
    return p_list;

}

static void
on_laser (const lcm_recv_buf_t *rbuf, const char *channel,
          const bot_core_planar_lidar_t *msg, void *user)
{
    nodder_extractor_state_t *self = (nodder_extractor_state_t *)user;
    
    bot_core_planar_lidar_t *laser = bot_core_planar_lidar_t_copy(msg);
    //fprintf(stderr,".");
  
    if(!nodder_push_scans(self, laser)){
        fprintf(stderr, "Full Scan\n");
        //extract points 
    }   
    

}

static void
on_nodder_status (const lcm_recv_buf_t *rbuf, const char *channel,
          const senlcm_nodder_status_t *msg, void *user)
{
    nodder_extractor_state_t *self = (nodder_extractor_state_t *)user;

    if(self->last_status){
        senlcm_nodder_status_t_destroy(self->last_status);
    }
    self->last_status = senlcm_nodder_status_t_copy(msg);
    
    //might need a better check than this
    if(self->start_angle != msg->max_tilt || self->end_angle != msg->min_tilt){
        //destroy
        g_mutex_lock (self->mutex_lrc);
        nodder_reset_collector (self);
        g_mutex_unlock (self->mutex_lrc);
        fprintf(stderr, "Resetting collector sizes\n");
        self->start_angle = msg->max_tilt;
        self->end_angle = msg->min_tilt;
    }
}


nodder_extractor_state_t * nodder_extractor_init(lcm_t *lcm, on_collection_update_t *callback, void * user){
  nodder_extractor_init_full(lcm, 1, 0 ,2 * M_PI, NULL, callback, user);
}

nodder_extractor_state_t * nodder_extractor_init_full(lcm_t *lcm, uint8_t whole_scan, double start_angle, double end_angle, char *channel, on_collection_update_t *callback, void *user ){
    nodder_extractor_state_t *state = (nodder_extractor_state_t*) calloc(1, sizeof(nodder_extractor_state_t));
    state->lcm = lcm; 
    //do we need to attach it here also??
    bot_glib_mainloop_attach_lcm (state->lcm);
    state->param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->param);
    state->start_angle = start_angle;
    state->end_angle = end_angle;

    state->laser_returns = g_array_new (FALSE, TRUE, sizeof (bot_core_planar_lidar_t *));
    state->mutex = g_mutex_new ();
    state->mutex_lrc = g_mutex_new ();
    state->last_status = NULL;
    state->lr = NULL;
    state->on_update = NULL;
    if(callback!= NULL){
      state->on_update = (on_update_handler_t *)calloc(1, sizeof(on_update_handler_t));
      state->on_update->callback_func = callback;
      state->on_update->user = user;
    }

    fprintf(stderr, "Done\n");
    //we'll hardcode it for now - but should read the nodder channel and frame from the param
    if(channel == NULL){
        state->channel_name = strdup("NODDING_LASER");
    }
    else{
        state->channel_name = strdup(channel);
    }
    
    char key[1024];
    sprintf(key,"planar_lidars.%s.coord_frame" , state->channel_name);

    if (0 != bot_param_get_str(state->param, key, &state->frame_name)){
        fprintf(stderr, "%s - frame not defined\n", key);
        return NULL;
    }
    
    bot_core_planar_lidar_t_subscribe(state->lcm, state->channel_name, on_laser, state);   

    //dynamixel_status_list_t_subscribe (state->lcm, "DYNAMIXEL_STATUS", &on_servo, state);
    //bot_core_planar_lidar_t_subscribe(state->lcm, "SKIRT_FRONT", on_laser ,state);
    
    //this should be dynamically updated
    senlcm_nodder_status_t_subscribe(state->lcm, "NODDER_TILT_STATUS", on_nodder_status, state);

    return state; 
}


