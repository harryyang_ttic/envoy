// file: main.c
//

#include <stdio.h>
#include <inttypes.h>
#include <glib.h>
#include <getopt.h>

#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif

#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
#include <bot_param/param_client.h>
#include <bot_frames/bot_frames.h>

#include <dynamixel/dynamixel.h>
#include <lcmtypes/dynamixel_cmd_list_t.h>
#include <lcmtypes/dynamixel_status_list_t.h>
#include <lcmtypes/senlcm_nodder_status_t.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>


#define MIN_POSITION 300
#define CENTER_POSITION 512
#define MAX_POSITION 820
#define SERVO_ID 0
#define OFFSET (CENTER_POSITION - 512)
#define SIGN -1
#define TICKS_TO_DEGREES 0.29296875

typedef struct _state_t state_t;

struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    guint timer_id_servo;
    guint timer_id_laser;

    BotFrames *bcf;

    dynamixel_status_t last_servo_status;	
    bot_core_planar_lidar_t * last_laser_data;

    int64_t last_servo_utime;
    int64_t last_laser_utime;

    double arg_center;
    double arg_deviation;
    double arg_speed;
    int going_up;

    int servo_id;
   
};

int get_ticks(double degree);
double get_degrees(int ticks);

double get_min_position_degrees(state_t *self){
    int min_position = get_ticks(self->arg_center - self->arg_deviation);
    min_position = bot_clamp(min_position, MIN_POSITION, MAX_POSITION);
    return get_degrees(min_position);
}

double get_max_position_degrees(state_t *self){
    int max_position = get_ticks(self->arg_center + self->arg_deviation);
    max_position = bot_clamp(max_position, MIN_POSITION, MAX_POSITION);
    return get_degrees(max_position);
}

double get_degrees(int ticks){
    return SIGN * (ticks - CENTER_POSITION) * TICKS_TO_DEGREES; 
}

int get_ticks(double degree){
    return SIGN *(degree / TICKS_TO_DEGREES) + CENTER_POSITION;  //SIGN * (ticks - CENTER_POSITION) * TICKS_TO_DEGREES; 
}

int is_at_top(int current_tick, state_t *self){
    if(SIGN >0){
        if(current_tick >  get_ticks(self->arg_center)){
            return 1;
        }
    }
    else{
        if(current_tick < get_ticks(self->arg_center)){
            return 1;
        }
    }
    return 0; 
}

static void move_to_top(state_t * self) {

    //int max_position = (int) (self->arg_center+ SIGN * self->arg_deviation+150)/0.293;

    //max_position = max_position + OFFSET;

    int max_position = get_ticks(self->arg_center + self->arg_deviation); //(int) (self->arg_center+ SIGN * self->arg_deviation+150)/0.293;

    /*if(max_position > MAX_POSITION) {
        max_position = MAX_POSITION;
        printf("Max position thresholded to 90 degrees.");
        }*/

    self->going_up = 1;

    max_position = bot_clamp(max_position, MIN_POSITION, MAX_POSITION);

    dynamixel_cmd_t c = {
    	.servo_id = self->servo_id,
    	.torque_enable = 1,
    	.goal_position = max_position,
    	.max_speed = self->arg_speed,
    	.max_torque = 983,
    };

    dynamixel_cmd_list_t c_list = {
     	.utime = bot_timestamp_now(),
     	.ncommands = 1,
     	.commands = &c,
    };
  
    if (dynamixel_cmd_list_t_publish(self->lcm, "DYNAMIXEL_COMMAND", &c_list) != 0){
        //printf ("command publish error\n");
    }
};

static void move_to_bottom(state_t * self) {

    int min_position = get_ticks(self->arg_center - self->arg_deviation);//(int) (self->arg_center- SIGN * self->arg_deviation+150)/0.293;

    //min_position += OFFSET;

    /*if (min_position < MIN_POSITION) {
        min_position = MIN_POSITION;
        printf("Min position thresholded to negative 90 degrees.");
        }*/

    self->going_up = 0;

    min_position = bot_clamp(min_position, MIN_POSITION, MAX_POSITION);

    dynamixel_cmd_t c = {
    	.servo_id = self->servo_id,
    	.torque_enable = 1,
    	.goal_position = min_position,
    	.max_speed = self->arg_speed,
    	.max_torque = 983,
    };

    dynamixel_cmd_list_t c_list = {
        .utime = bot_timestamp_now(),
     	.ncommands = 1,
     	.commands = &c,
    };
  
    if (dynamixel_cmd_list_t_publish(self->lcm, "DYNAMIXEL_COMMAND", &c_list) != 0){
        //printf ("command publish error\n");
    }
}


void servo_update_handler(BotFrames *bot_frames, const char *frame, const char * relative_to, int64_t utime, void *user)
{
    //printf("link  %s->%s was updated, user = %p\n", frame, relative_to, user);
}

//80 Hz
static void on_servo(const lcm_recv_buf_t *rbuf, const char * channel, const dynamixel_status_list_t * msg, void * user){
    state_t *self = (state_t*) user;

    if(msg->nservos == 0){
        fprintf(stderr, "No servos in the list - returning\n");
        return;
    }

    self->last_servo_utime = msg->utime;
    //printf("Servo status received successfully at %"PRId64" \n",self->last_servo_utime);

    //cycle through the list and find the proper status 

    int valid_ind = -1; 

    for(int i=0; i < msg->nservos; i++){
        if(self->servo_id == msg->servos[i].servo_id){
            valid_ind = i;
            //fprintf(stderr, "ID : %d Servo index : %d\n", self->servo_id, valid_ind);
            break;
        }
    }

    if(valid_ind < 0){
      //fprintf(stderr,"Correct servo ID not found\n");
        return;
    }            
        
    self->last_servo_status = msg->servos[valid_ind];

    int i;
    for(i=0;i<self->last_servo_status.num_values;i++){
        //printf("Address: %"PRId16" Value: %"PRId32" \n",self->last_servo_status.addresses[i],self->last_servo_status.values[i]);
    }
    //Note: index 1 corresponds to present position. index 2 corresponds to moving boolean	

    int midpoint = get_ticks(self->arg_center);//(int) (self->arg_center+150)/0.293 + OFFSET;

    //fprintf(stderr, "Midpoint : %f => %d\n", self->arg_center, get_ticks(self->arg_center)); 
    
    //fprintf(stderr, "Top Point : %f => %d\n", self->arg_center + self->arg_deviation, get_ticks(self->arg_center + self->arg_deviation)); 
    
    //fprintf(stderr, "Bottom Point : %f => %d\n", self->arg_center - self->arg_deviation, 
    //get_ticks(self->arg_center - self->arg_deviation)); 
    
    //CASE where you are at 0 and not moving
    /*if(self->last_servo_status.values[1] < midpoint && self->last_servo_status.values[2] == 0){
        move_forward(self);
    }
    //CASE where you are at 1023 and not moving
    else if(self->last_servo_status.values[1] >= midpoint && self->last_servo_status.values[2] == 0) {
        move_backward(self);
        }*/

    if(self->last_servo_status.values[2] == 0){
      //fprintf(stderr, "Is at Top : %d\n", is_at_top(self->last_servo_status.values[1], self));
        if(is_at_top(self->last_servo_status.values[1], self)){
            //command to bottom 
            move_to_bottom(self);
        }
        else{
            move_to_top(self);
        }        
    }

    senlcm_nodder_status_t stat_msg; 
    stat_msg.utime = bot_timestamp_now();
    stat_msg.max_tilt = get_max_position_degrees(self);
    stat_msg.min_tilt = get_min_position_degrees(self);
    stat_msg.going_up = self->going_up;

    senlcm_nodder_status_t_publish(self->lcm, "NODDER_TILT_STATUS", &stat_msg);

    /*if(self->last_servo_status.values[1] < midpoint && self->last_servo_status.values[2] == 0){
        move_forward(self);
    }
    //CASE where you are at 1023 and not moving
    else if(self->last_servo_status.values[1] >= midpoint && self->last_servo_status.values[2] == 0) {
        move_backward(self);
        }*/

    //int16_t num_values i= self->last_servo_status.num_values;
    //int16_t addresses[&num_values] = self->last_servo_status.addresses;
    //int64_t values[&num_values] = self->last_servo_status.values;
    //printf("Number of values is %"PRId16" \n",self->last_servo_status.num_values);
	
	
    //Now, take the servo data and republish for renderer
      
    //bot_frames_add_update_subscriber(bcf,servo_update_handler,NULL);
	
    //BotTrans t;
    //bot_frames_get_trans(bcf, "nodder", "local", &t);
    bot_core_rigid_transform_t servo_msg;
    servo_msg.utime = bot_timestamp_now();
    servo_msg.trans[0] = 0; 
    servo_msg.trans[1] = 0;
    servo_msg.trans[2] = 0.0404;

    //fprintf(stderr, "Curent Degree %d => %f\n", self->last_servo_status.values[1], 
    //      get_degrees(self->last_servo_status.values[1])); 

    double pitch;
    pitch = - get_degrees(self->last_servo_status.values[1]);//(self->last_servo_status.values[1]-CENTER_POSITION)*0.0174532925*0.293;
    double rpy[3];
    rpy[0] = 0;
    rpy[1] = bot_to_radians(pitch); //Only variable component
    rpy[2] = 0;
	
    //fprintf (stdout, "pitch = %.2f\n", rpy[1]*180/M_PI);
    bot_roll_pitch_yaw_to_quat(rpy, servo_msg.quat);
	
    bot_core_rigid_transform_t_publish(self->lcm, "NODDER", &servo_msg);
	
    return;
}

//40 Hz
static void on_laser(const lcm_recv_buf_t *rbuf, const char * channel, const bot_core_planar_lidar_t * msg, void * user){
    state_t *self = (state_t*) user;
    self->last_laser_utime = bot_timestamp_now();
    //printf("Lidar data received successfully at %"PRId64" \n",self->last_laser_utime);
    self->last_laser_data = bot_core_planar_lidar_t_copy(msg);
	
    return;
}



static gboolean on_servo_timeout (gpointer data) {
    state_t *self = (state_t*) data;
    /*
    senlcm_nodder_status_t stat_msg; 
    stat_msg.utime = bot_timestamp_now();
    stat_msg.max_tilt = get_max_position_degrees(self);
    stat_msg.min_tilt = get_min_position_degrees(self);
    stat_msg.going_up = self->going_up;

    senlcm_nodder_status_t_publish(self->lcm, "NODDER_TILT_STATUS", &stat_msg);
    */
    //pulish status 
    //printf("TIMEOUT -- no servo status received\n");
    return TRUE;
}

static gboolean on_laser_timeout (gpointer data) {
    state_t *self = (state_t*) data;
    //printf("TIMEOUT -- no lidar data received\n");
    return TRUE;
}


static void usage() {
    fprintf (stderr, "usage: nodding-lidar [options]\n"
             "\n"
             "  -h, --help              Shows this help text and exits\n"
             "  -c, --center            Specify center pitch (degrees)\n"
             "  -d, --deviation         Specify range of motion from center (degrees)\n"
             "  -s, --speed             Specify moving-speed of the device (in counts from 1 to 1023)\n\n"
             "  -i, --id             Specify the servo id (int)\n\n"
             "NOTE: the range of motion of the device is thresholded to 90 degrees above and below the horizontal plane due to hardware constraints.\n"
             );
    exit(1);
}

int main(int argc, char ** argv) {

    state_t *self = (state_t*) calloc(1, sizeof(state_t));

    //Default settings
    self->arg_center = 0;
    self->arg_deviation = 15;
    self->arg_speed = 100;
        
    self->lcm = bot_lcm_get_global (NULL);
    if(!self->lcm) {
        fprintf (stderr, "Error getting LCM\n");
        goto failed;
    }
    bot_glib_mainloop_attach_lcm(self->lcm);

    char *optstring = "hc:d:s:i:";

    struct option long_opts[] = {
        {"help", no_argument, 0, 'h'},
        {"center", required_argument, 0, 'c'},
        {"deviation", required_argument, 0, 'd'},
        {"speed", required_argument, 0, 's'},
        {"id", required_argument, 0, 'i'},
        {0, 0, 0, 0}
    };	

    self->servo_id = SERVO_ID;

    char c;
    int override_failsafe = 0;
    while ((c = getopt_long (argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
        case 'c':
            self->arg_center = strtod(optarg, NULL);
            break;
        case 'd':
            self->arg_deviation = strtod(optarg, NULL);
            break;
        case 'i':
            self->servo_id = atoi(optarg);
            break;
        case 's':
            self->arg_speed = atoi(optarg);
            if (self->arg_speed <= 1023 && self->arg_speed >= 0) {
                /*
                  dynamixel_status_t speed_cmd;
                  speed_cmd.utime = bot_timestamp_now();
                  speed_cmd.servo_id = 0;
                  speed_cmd.num_values = 1;
                  speed_cmd.addresses[0] = 32;
                  speed_cmd.values[0] = self->arg_speed;
                            
                  dynamixel_status_list_t speed_cmd_list;
                  speed_cmd_list.utime = bot_timestamp_now();
                  speed_cmd_list.nservos = 1;
                  speed_cmd_list.servos = &speed_cmd;

                            
                  dynamixel_status_t speed_cmd = {
                  .utime = 0,
                  .servo_id = 0,
                  .num_values = 1,
                  .addresses = {32},
                  .values = {self->arg_speed},
                  };
                            
                  dynamixel_status_list_t speed_cmd_list = {
                  .utime = 0,
                  .nservos = 1,
                  .servos = &speed_cmd,
                  };
                            
                  if (dynamixel_status_list_t_publish(self->lcm, "DYNAMIXEL_STATUS", &speed_cmd_list) != 0) {
                  printf("Error communicating with servo. Please verify that the dynamixel-lcm process is running and the servo is properly connected\n");
                  } else {
                  printf("Success transmitting speed to servo.\n");
                  }
                */
            }
                            
            break;
        case 'h':
        default:
            usage();
        }
    }

    if(argc == 1) {
        usage();
    }
        
        

    // Get BotFrames instance
    BotParam * param = bot_param_new_from_server(self->lcm, 0);
    if (!param) {
        fprintf (stderr, "Error getting BotParam. Are you running the server?\n");
        goto failed;
    }

    self->bcf = bot_frames_get_global(self->lcm, param);
    bot_frames_add_update_subscriber(self->bcf,servo_update_handler,NULL);

    dynamixel_status_list_t_subscribe (self->lcm, "DYNAMIXEL_STATUS", &on_servo, self);
    bot_core_planar_lidar_t_subscribe (self->lcm, "HOKUYO_LIDAR", &on_laser, self);
    self->mainloop = g_main_loop_new(NULL, FALSE);
	
    self->timer_id_servo=g_timeout_add(1000,on_servo_timeout,self);
    self->timer_id_laser=g_timeout_add(1000,on_laser_timeout,self);
    g_main_loop_run(self->mainloop);
    return 0;

 failed:
    if (self)
        free(self);
    return -1;

}
