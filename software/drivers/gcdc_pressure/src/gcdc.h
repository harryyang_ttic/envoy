#ifndef _GCDC
#define _GCDC
#include <libusb-1.0/libusb.h>

#define GCDC_VID 0x10C4
#define GCDC_PID 0x84D1

#define GCDC_HID_INTERFACE 1
#define GCDC_HID_DATA_IN 0x83



#define SMBUS_ID 2
#define SMBUS_REPORT_COUNT 0x40

#define TIME_ID 1
#define TIME_REPORT_COUNT 0x13

#define GAIN_ID 3
#define GAIN_REPORT_COUNT 1

#define SELF_TEST_ID 5
#define SELF_TEST_REPORT_COUNT 1

#define ACCEL_ON_OFF_ID 7
#define ACCEL_ON_OFF_REPORT_COUNT 1

#define REPROGRAM_ID 19
#define REPROGRAM_REPORT_COUNT 3

//Unique ID sent to bootloader and required to put into a bootload state
#define UNIQUE_BL_ID    0x34

#define RESPONSE_REPORT 22

//#define DATA_REPORT 0x12
#define DATA_REPORT_COUNT 2*NUM_CHAN


#define OLD_TEMPERATURE_REPORT_ID 2
#define OLD_PRESSURE_REPORT_ID 4
#define TEMPERATURE_REPORT_ID 8
#define PRESSURE_REPORT_ID 10
#define PPS_TIME_REPORT_ID 6
#define ACCEL_REPORT_ID 0x12




#endif
