add_definitions(
  #-ggdb3 
    -O0 
    -std=gnu99
    )

set(gcdcTool_sources
    gcdcTool.c gcdcInterface.c ihex.c version.c firmwareUpdater.c pthreadUpdater.c 
    )

add_executable(er-gcdc-pressure ${gcdcTool_sources})

pods_use_pkg_config_packages(er-gcdc-pressure
    lcm 
    bot2-core 
    lcmtypes_er-lcmtypes 
)

target_link_libraries(er-gcdc-pressure
    -lusb-1.0  -lm
    )


pods_install_executables(er-gcdc-pressure)