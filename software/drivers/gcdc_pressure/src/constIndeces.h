
// the following are indeces into a lookup table.
// the values are used by both the 8051 embedded firmware and 
// the gcdcConfig.c tool which modfies the lookup table.

#define CONST_CRC 0			// the CRC for the next 510 bytes
#define CONST_USB_SERIAL 1		// offset for the serial number of the device
#define CONST_USB_MANUFACTURER 2	// usb manufacture string 
#define CONST_USB_PRODUCT 3		// usb product string,
#define CONST_TITLE 4			// title at the top of each file generated
#define CONST_DIR_NAME 5		// directory where data is stored
