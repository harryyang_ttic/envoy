
int firmwareUpdaterUpdateDevice(libusb_device_handle *devh1, char* filename);
int configPageWrite(libusb_device_handle *devh1, unsigned char* data);
int configPageRead(libusb_device_handle *devh1, unsigned char* data);
int configPageErase(libusb_device_handle *devh1);
int resetFirmware(libusb_device_handle *devh1);


struct configList {
  int length;
  int itemNumber;
  unsigned char* localCopy;
  struct configList* prev;
  struct configList* next;
};
                
struct configList* insertStringElement(struct configList* last, int itemNumber, char* string);
struct configList* insertU16Element(struct configList* last, int itemNumber, unsigned short data);
struct configList* getListRoot(struct configList* member);
unsigned char* configListToPage(unsigned char* dest, struct configList* list);
struct configList* pageToConfigList(unsigned char* src);
void dumpConfigList(struct configList* pList);
struct configList* mergeConfigLists(struct configList* a, struct configList* b);
unsigned int crc16(unsigned char* ptr, int cnt);
