/*
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <math.h>	// sqrt

#include <libusb-1.0/libusb.h>
#include "gcdc.h"
#include "gcdcInterface.h"
#include "firmwareUpdater.h"
#ifdef PTHREAD_UPDATER
#include "pthreadUpdater.h"
#endif

#include <libxml/xmlreader.h>
#include "constIndeces.h"

extern char* version_string;

int verbose_flag_1 = 0;
#define MAX_SERIAL_NUM 0x80
#define MAX_FILENAME 0x100
#define MAX_DEVICES 0x08

#define ST_HI 3

void usage()
{
	printf("gcdcTool - a tool for working with GCDC USB based devices\n");
	printf("\t for more information contact http://www.gcdataconcepts.com\n");
	printf("gcdcTool [--options] [file] \n");
	printf(" There is no default option, select desired action from list below\n");
	printf("\t--gcdcTool [--options] [arg] \n");
	printf("\t--verbose           provides internal diagnostics\n");
	printf("\t--version           outputs current version\n");
	printf("\t--brief             suppreses chatter\n");
	printf("\t--avail             lists the available device serial numbers\n");
	printf("\t--xml           reads the flash constans and outputs the results in the same format as the input document\n");
	printf("\t--read         reads the flash constants and displays the results\n");
	printf("\t--code filename convert the xml constants document to source format\n");
//	printf("\t--offy val          set the applied y offset\n");
	printf("\t--read              read the config information from the target device\n");
	printf("\t--erase             erase the config page (set to 0xff)\n");
	printf("\t--update filename   write the config information to the target device\n");
	printf("\t--help              this help info\n");
}


xmlChar* getTextInNode(xmlTextReaderPtr reader)
{
	xmlChar* value;
	int ret = xmlTextReaderRead(reader);
	while (ret == 1)
	{
		if( xmlTextReaderNodeType(reader)== XML_READER_TYPE_TEXT)
		{
			value = xmlTextReaderValue(reader);
			return(value);
		}
		if(xmlTextReaderNodeType(reader) == XML_READER_TYPE_END_ELEMENT)
			return(NULL);
		ret = xmlTextReaderRead(reader);
	}
	return(NULL);
}

struct configList* rootList = NULL;
struct configList* currentListItem = NULL;


void xmlInsertU16(xmlTextReaderPtr reader, int index)
{
	xmlChar* value = getTextInNode(reader);
	if(value)
	{
		int val = atoi((char*)value);
		currentListItem = insertU16Element(currentListItem, index, val);
		if(verbose_flag_1) printf("<%d>\n",val);
		xmlFree(value);
	}
}


void xmlInsertString(xmlTextReaderPtr reader, int index)
{
	xmlChar* value = getTextInNode(reader);
	if(value)
	{
		currentListItem = insertStringElement(currentListItem, index, (char*)value);
		if(verbose_flag_1) printf("<%s>\n",(char*)value);
		xmlFree(value);
	}
}

struct nameMapping {
int index;
char* name;
char* codeName;
};

struct nameMapping nameMap [] = {
{CONST_USB_SERIAL,"serialNum","defaultUsbSerial"},
{CONST_USB_MANUFACTURER,"manufacturer","defaultUsbManufacturer"},
{CONST_USB_PRODUCT,"product","defaultUsbProduct"},
{CONST_TITLE,"title","defaultTitle"},
{CONST_DIR_NAME,"dataDir","defaultDirName"},
{0,NULL}
};


char* xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<config lang=\"en-US\">\n";
char* xmlFooter = "</config>\n";

void dumpConfigListAsXml(struct configList* pList)
{
	int i;
	if(pList == NULL)
	{
		printf("dumpConfigList NULL config list\n");
		return;
	}
	printf("%s",xmlHeader);
	do
	{
		i=0;	
		while(nameMap[i].name !=NULL)
		{
			if(nameMap[i].index == pList->itemNumber)
			{
				printf("<%s>%s</%s>\n", nameMap[i].name, pList->localCopy, nameMap[i].name);
				break;
			}
			i++;
		}
		pList = pList->next;
	} 
	while (pList != NULL);
	printf("%s",xmlFooter);
}

char* cHeader = "// CCONST_PAGE must agree with the number used in Makefile for reloacation of the CCONST segment  \n\
// the start of the segment must be such that the size of this segment does \n\
// the size of this module is less than 0x200 so F800-0x200 = F600 \n\
#define CCONST_BASE 0xF600\n\
#pragma constseg CCONST   \n\
\n\
// SERIAL_NUMBER_PAGE must agree with the number used in Makefile for reloacation of the UPDATER segment\n\
// the start of the segment must be such that the size of this segment does not spill over into the\n\
// reserved area of the 'F340 or 0xFBFF\n\
// the size of this module is less than 0x200 so F800-200 = F600\n\
#define SERIAL_NUMBER_PAGE (0xf600/512)\n\
#pragma codeseg CCONST\n";
                                                            
void dumpConfigListAsCode(struct configList* pList, unsigned char* image)
{
	int i;
	if(pList == NULL)
	{
		printf("dumpConfigList NULL config list\n");
		return;
	}
	printf("%s",cHeader);
	printf("\n\n#define crcsum 0x%04x\n",crc16(image+2,510));
	printf("\nconst code char* code constTable[] = {(const char code*)crcsum, defaultUsbSerial, defaultUsbManufacturer, defaultUsbProduct, defaultTitle, defaultDirName};\n\n");
	do
	{
		i=0;	
		while(nameMap[i].name !=NULL)
		{
			if(nameMap[i].index == pList->itemNumber)
			{
				printf("const code char %s[] = \"%s\";\n", nameMap[i].codeName, pList->localCopy);
				break;
			}
			i++;
		}
		pList = pList->next;
	} 
	while (pList != NULL);
	printf("\n");
}
                                                            


void processNode(xmlTextReaderPtr reader) 
{
	/* handling of a node in the tree */
	xmlChar *name, *value;
   
	name = xmlTextReaderName(reader);
	if (name == NULL)
		name = xmlStrdup(BAD_CAST "--");

	if(xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT)
	{
		if(strcmp("serialNum", (char*)name) == 0)
		{
			if(verbose_flag_1) printf("Serial number: ");
			xmlInsertString(reader,CONST_USB_SERIAL);
		}
		else if(strcmp("manufacturer", (char*)name) == 0)
		{
			if(verbose_flag_1) printf("Manufacturer: ");
			xmlInsertString(reader,CONST_USB_MANUFACTURER);
		}
		else if(strcmp("product", (char*)name) == 0)
		{
			if(verbose_flag_1) printf("Product: ");
			xmlInsertString(reader,CONST_USB_PRODUCT);
		}
		else if(strcmp("title", (char*)name) == 0)
		{
			if(verbose_flag_1) printf("Title: ");
			xmlInsertString(reader,CONST_TITLE);
		}
		
		else if(strcmp("dataDir", (char*)name) == 0)
		{
			if(verbose_flag_1) printf("Data Dir: ");
			xmlInsertString(reader,CONST_DIR_NAME);
		}
// not currently implemented
//		else if(strcmp("xOffset", (char*)name) == 0)
//		{
//			if(verbose_flag_1) printf("X offsetElement, ");
//			xmlInsertU16(reader,6);
//		}
//		else if(strcmp("yOffset", (char*)name) == 0)
//		{
//			if(verbose_flag_1) printf("Y offsetElement, ");
//			xmlInsertU16(reader,7);
//		}
//		else if(strcmp("zOffset", (char*)name) == 0)
//		{
//			if(verbose_flag_1) printf("Z offsetElement, ");
//			xmlInsertU16(reader,8);
//		}
//		
//		else if(strcmp("xScale", (char*)name) == 0)
//		{
//			if(verbose_flag_1) printf("X Scale, ");
//			xmlInsertU16(reader,9);
//		}
//		else if(strcmp("yScale", (char*)name) == 0)
//		{
//			if(verbose_flag_1) printf("Y Scale, ");
//			xmlInsertU16(reader,10);
//		}
//		if(strcmp("zScale", (char*)name) == 0)
//		{
//			if(verbose_flag_1) printf("Z Scale, ");
//			xmlInsertU16(reader,11);
//		}

	}
	else if(xmlTextReaderNodeType(reader) == XML_READER_TYPE_SIGNIFICANT_WHITESPACE);
	else if(xmlTextReaderNodeType(reader) == XML_READER_TYPE_END_ELEMENT);
	else
	{
		value = xmlTextReaderValue(reader);

		printf("%d %d %s %d",xmlTextReaderDepth(reader),
			xmlTextReaderNodeType(reader),name, xmlTextReaderIsEmptyElement(reader));
	        if (value == NULL)
        		printf("\n");
		else 
		{
			printf(" %s\n", value);
			xmlFree(value);
		}
	}
	xmlFree(name);
	
}

    
int streamFile(char *filename) 
{
	xmlTextReaderPtr reader;
	int ret;

	reader = xmlNewTextReaderFilename(filename);
	if (reader != NULL)
	{
		ret = xmlTextReaderRead(reader);
		while (ret == 1)
		{
			processNode(reader);
			ret = xmlTextReaderRead(reader);
		}
		xmlFreeTextReader(reader);
		if (ret != 0) 
		{
			printf("%s : failed to parse\n", filename);
		}
	} 
	else 
	{
		printf("Unable to open %s\n", filename);
		return(-1);
	}
	return(ret);
}		


int main(int argc, char *argv[])
{
	int r = 1;
	int i;
	int c;
	char refSerialNum[MAX_SERIAL_NUM];
	char dutSerialNum[MAX_SERIAL_NUM];
	libusb_device_handle* devh[MAX_DEVICES]; 
	char filename[MAX_FILENAME];
	static int update_flag = 0;
	static int read_flag = 0;
	static int xml_flag =0;
	static int erase_flag=0;
	static int code_flag =0;
	libusb_device_handle* refDevh = NULL; 
	libusb_device_handle* dutDevh = NULL; 
	static int show_avail_flag = 0;
//	static int numTestSamples = 16;
	static int offsetTime = 0;
	
	char* serialNums[MAX_DEVICES];
	char temp[MAX_DEVICES][MAX_SERIAL_NUM];
	for(i=0;i<MAX_DEVICES;i++)
	{
		serialNums[i] = temp[i];
	}
	int numDevicesFound=0;

	refSerialNum[0] = '\0';
	dutSerialNum[0] = '\0';

	// Parse command-line options.
	while(1)
	{
		static struct option long_options[] =
	        {
	        /* These options set a flag. */
	        	{"verbose", no_argument,       &verbose_flag_1, 1},
	                {"xml",   no_argument,       &xml_flag, 1},
	                {"code",   required_argument, 0, 'c'},
	                {"read",   no_argument,       &read_flag, 1},
	                /* These options don't set a flag. We distinguish them by their indices. */
	                {"offx",  required_argument, 0, 'a'},
	                {"sernum",  required_argument, 0, 'b'},
	                {"update",  required_argument, 0, 'u'},
	                {"offset",  required_argument, 0, 'd'},
	                {"help",    no_argument, 0, '?'},
	                {"version",    no_argument, 0, 'v'},
	                {"erase",   no_argument,       &erase_flag, 1},
	                {0, 0, 0, 0}
		};

		int option_index = 0;
		c = getopt_long (argc, argv, "axr:d:f:",long_options, &option_index);
		/* Detect the end of the options. */
		if (c == -1)
		break;

		switch(c) 
		{
		case 0:
			/* If this option set a flag, do nothing else now. */
		        if (long_options[option_index].flag != 0)
		        break;
			printf ("option %s", long_options[option_index].name);
		        if (optarg)
		        	printf (" with arg %s", optarg);
			printf ("\n");
		        break;
		case 'b':
			strncpy(dutSerialNum,optarg, MAX_SERIAL_NUM);
			if(verbose_flag_1) printf("Device Serial Number <%s>\n",refSerialNum);
			break;
		case 'c':
			strncpy(filename,optarg, MAX_FILENAME);
			if(verbose_flag_1) printf("updating with <%s>\n",filename);
			code_flag = 1;
			break;
		case 'd':
			offsetTime = atoi(optarg);
			if(verbose_flag_1) printf("offset for x %d\n",offsetTime);
			break;
		case 'u':
			strncpy(filename,optarg, MAX_FILENAME);
			if(verbose_flag_1) printf("updating with <%s>\n",filename);
			update_flag = 1;
			break;
		case 'v':
			printf("gcdcTool Version: %s\n",version_string);
			printf("Copyright 2009, Gulf Coast Data Concepts, LLC\n");
			exit(0);
			break;
		case '?': /* getopt_long already printed an error message. */
			usage("Usage\n");
			exit(0);
			break;
		default:
			usage();
			abort ();
		}
	}
	/* Print any remaining command line arguments (not options). */
        if (optind < argc)
        {
        	printf("non-option ARGV-elements: ");
                while(optind < argc)
                	printf("%s ", argv[optind++]);
		printf("\n");
	}

	if(verbose_flag_1)
	{
		printf("gcdcTool VERSION %s\n",version_string);
	}

  	r = gcdcInterfaceInit(verbose_flag_1);
	if (r < 0) 
	{
		fprintf(stderr, "failed to initialise libusb\n");
		exit(1);
	}
	if(verbose_flag_1) printf("Interface initialized\n");

	numDevicesFound = gcdcInterfaceGetSerialNumbers(NULL, serialNums, MAX_DEVICES, MAX_SERIAL_NUM);
	if(numDevicesFound <1)
	{
		printf("Wasn't able to find devices %d\n",numDevicesFound);
		goto out;
	}
	if( numDevicesFound > MAX_DEVICES) numDevicesFound = MAX_DEVICES;

	if(show_avail_flag)
	{// display available serial numbers
		printf("Devices Found\n");
		for(i=0;i<numDevicesFound;i++)
		{
			printf("\tsn: <%s>\n",serialNums[i]);
		}
	}
	
	for(i=0;i< numDevicesFound;i++)
	{
		// open devices
		if(verbose_flag_1) printf("Connecting to <%s>\n",serialNums[i]);
		devh[i] = gcdcInterfaceConnectToSerialNumber(serialNums[i]);
		if(devh[i] == NULL) goto out;
		if(refSerialNum[0])
		{
			if(strncasecmp(serialNums[i],refSerialNum, MAX_SERIAL_NUM) == 0)
			{
				if(verbose_flag_1) printf("Reference device found\n");
				refDevh = devh[i];
			}
		}
		if(dutSerialNum[0])
		{
			if(strncasecmp(serialNums[i],dutSerialNum, MAX_SERIAL_NUM) == 0)
			{
				if(verbose_flag_1) printf("Expected device found\n");
				dutDevh = devh[i];
			}
		}
	}


	if(update_flag || code_flag)	// update unit
	{
		if(verbose_flag_1) printf("reading %s\n",filename);
		fflush(stdout);
		streamFile(filename);
		rootList = getListRoot(currentListItem);
	}


	if(erase_flag)
	{

		for(i=0;i<numDevicesFound;i++)
		{
			printf("\neraseing config page on Device %d \n",i);
			fflush(stdout);
			if(configPageErase(devh[i]))
			{
				printf("Error eraseing page\n");
				exit(-1);
			}
			resetFirmware(devh[i]);

		}
	}

	if(update_flag)	// update unit
	{
		int j;
		unsigned char* image;
		printf("updating firmware on %d devices\n",numDevicesFound);
		fflush(stdout);
		image = malloc(512);
    		
//		streamFile(filename);
		
		//make image of 512 byte sector of config data here

		for(i=0;i<numDevicesFound;i++)
		{
			printf("\nDevice %d \n",i);
			fflush(stdout);
			configPageRead(devh[i],image);
//printf("page read\n");
//			for(j=0;j<512;j++)
//			{
//				if(j%32 == 0) printf("\n0x%04x:  ",j);
//				printf("%02x ",(unsigned char)(image[j]));
//			}
//			printf("\n");
			struct configList* remade = pageToConfigList(image);
			dumpConfigList(remade);
			rootList = getListRoot(currentListItem);

			rootList = mergeConfigLists(remade, rootList);
			
			dumpConfigList(rootList);
			image = configListToPage(NULL,rootList);		

			if(verbose_flag_1)
			{
			printf("Image to write\n");
				for(j=0;j<512;j++)
				{
					if(j%32 == 0) printf("\n0x%04x:  ",j);
					printf("%02x ",(unsigned char)(image[j]));
				}
				printf("\n");
			}

			configPageWrite(devh[i], image);
			
			resetFirmware(devh[i]);
			        		
		}
		printf("\n");
	}

	if(read_flag || xml_flag)	// update unit
	{
		int j;
		unsigned char* image;
//		printf("reding config info %d devices\n",numDevicesFound);
		fflush(stdout);
		image = malloc(512);
    		
		for(i=0;i<numDevicesFound;i++)
		{
//			printf("\nDevice %d \n",i);
			fflush(stdout);

			configPageRead(devh[i],image);
			if(verbose_flag_1)
			{
				for( j=0; j<512; j++)
				{
					if( j%32 == 0) printf("\n0x%04x:  ",j);
					printf("%02x ",(unsigned char)(image[j]));
				}
				printf("\n");
			}
			
			struct configList* remade = pageToConfigList(image);
			if(xml_flag) dumpConfigListAsXml(remade);
			else dumpConfigList(remade);
			
			resetFirmware(devh[i]);
		}
//		printf("\n");
	}
	
	if(code_flag)
	{
		unsigned char* image = configListToPage(NULL,rootList);
		dumpConfigListAsCode(rootList, image);
	}
	

	for(i=0;i<numDevicesFound;i++)
	{
		gcdcInterfaceRelease(devh[i]);
	}


out:
	gcdcInterfaceDeinit();
	return r >= 0 ? r : -r;
}

