# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/harry/Documents/Robotics/envoy/software/controllers/waypoint_follower/src/waypoint_follower.c" "/home/harry/Documents/Robotics/envoy/software/controllers/waypoint_follower/pod-build/src/CMakeFiles/er-waypoint-follower.dir/waypoint_follower.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "_FILE_OFFSET_BITS=64"
  "_LARGEFILE64_SOURCE"
  "_LARGEFILE_SOURCE"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "/home/harry/Documents/Robotics/envoy/software/build/include"
  "/usr/include/glib-2.0"
  "/usr/lib/x86_64-linux-gnu/glib-2.0/include"
  "/usr/include/gtk-2.0"
  "/usr/lib/x86_64-linux-gnu/gtk-2.0/include"
  "/usr/include/atk-1.0"
  "/usr/include/cairo"
  "/usr/include/gdk-pixbuf-2.0"
  "/usr/include/pango-1.0"
  "/usr/include/gio-unix-2.0"
  "/usr/include/freetype2"
  "/usr/include/pixman-1"
  "/usr/include/libpng12"
  "/usr/include/harfbuzz"
  "/home/harry/Documents/Robotics/envoy/software/externals/../build/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
