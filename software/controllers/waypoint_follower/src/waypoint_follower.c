/* The navigator takes as input a set of waypoints published by the bavigator module
 * and laser scan returns and outputs controller commands.
 *
 * author: Sachi Hemachandra
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif


#include <lcm/lcm.h>

#include <bot_core/bot_core.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_frames/bot_frames.h>
#include <bot_lcmgl_client/lcmgl.h>

#include <lcmtypes/bot_core.h>
#include <lcmtypes/bot2_param.h>
#include <lcmtypes/er_lcmtypes.h>

#include <geom_utils/geometry.h>
#include <interfaces/robot3d_interface.h>
#include <interfaces/robot3d_interface.h>

#define LASER_MEMORY          10
#define LOOKAHEAD_DISTANCE     2.0
#define LONG_LOOKAHEAD_DISTANCE     30.0//4.0//4.0//2.0
#define MAX_AWAY_TRANSLATION_SPEED 0.3
#define MAX_TRANSLATION_SPEED  1.0//0.5//1.5//1.5//wheelchair base limts the top speed to 1.0 m/s and PI rad/s 
#define MAX_ROTATION_SPEED     1.5//1.0//2.0//2.0//5.0 is the one used in the wheelchair //1.0//.35 // in rad/sec.
#define UPDATE_RATE            0.05//0.1//0.05 // in seconds  -- ***maybe we might increase this 
#define SAFETY_RADIUS          0.3//0.4//0.5//0.35//0.3//.35 - old value

#define FORWARD_SCAN_DIST      1.0
#define BEST_SIDE_DIST         0.5

#define MASK_BUCKETS         360
#define TURN_IN_PLACE_DEG     80.0//60.0//60.0//90.0 //used to be 25 
#define TV_BIASING_THETA      110.0//80.0
#define CLAMP_THETA           40.0
#define ROTATE_IN_PLACE_V     2.0  //rad per sec

#define ROBOT_WIDTH            0.62
#define ROBOT_LENGTH           0.98
#define ROBOT_RADIUS           0.62

#define SIDE_PERSON_THRESHOLD  0.1
#define FORWARD_PERSON_DIST    2.0

#define MARGIN                 0.01 //might be worth reducing this one 

#define STOP_RADIUS            0.01//1.0//1.2
#define ROT_RADIUS             0.01//1.5 //2.0 - reduced this a bit

//parameters to use when keeping close
#define C_STOP_RADIUS          0.01//0.79 //0.6
#define C_ROT_RADIUS           0.01//0.8 //0.7
//#define C_SAFETY_RADIUS          0.3
//earlier values were too slow
#define C_MAX_TRANSLATION_SPEED  0.8//1.0
#define C_MAX_ROTATION_SPEED     1.2

struct laser_data
{
    bot_core_planar_lidar_t  *msg;    
    double   m[12];
};

struct laser_channel
{
    char      name[256];
    GQueue    *data;
    int       complained;
    int8_t    mask[MASK_BUCKETS];
};

enum FollowingState{
  FOLLOWING, //person is being tracked and followed - issuing navigation commands to the navigator 
  PAUSED,  
  WAITING, //told to follow but no person estimate received - so waiting till one comes
  IDLE //not following (can still have a track on the person)
};

enum FollowCommand{
  FOLLOW, 
  PAUSE,
  RESUME,
  STOP, 
};

enum TrackingState{
  VALID_HEADING, //person is within following distance
  TOO_CLOSE, //person is being tracked but is too close to the wheelchair
  LOST // system has lost track of the person 
};

typedef struct _State {
  lcm_t * lcm;
  //BotConf * config;
  BotParam * config;
  GHashTable *laser_hashtable;
  GQueue *channels;

  double goal_heading; // CCW in radians, 0 = straight forward
  int64_t last_utime;
  int64_t last_goal_heading_utime;
  gboolean estop;
  double *state;
  gboolean have_goal_heading;
  gboolean collision_det;
  gboolean too_small_gap;
  int keep_close;
  int small_gap;
   
  erlcm_people_pos_msg_t * person_msg;
  int verbose;
  int playback;
  enum TrackingState tracking_state;
  enum FollowingState following_state;
  double frontlaser_offset;
  double rearlaser_offset;
  double front_anglular_offset;
  double rear_anglular_offset;
  
  double kinect_front_offset;
  double kinect_side_offset;
  
  float draw_obs;
  int use_state_score;
  int nav_pause;
  int too_far;
  double robot_pos[3];
  double robot_heading;
  int turn_in_place;  
  //for drawing 
  bot_lcmgl_t *lcmgl_rot;
  bot_lcmgl_t *lcmgl_navigator;
  bot_lcmgl_t *lcmgl_obs;
  bot_lcmgl_t *lcmgl_footprint;
  bot_lcmgl_t *lcmgl_navigator_info;
  bot_lcmgl_t *lcmgl_scores;
  bot_lcmgl_t *lcmgl_goal;
  bot_lcmgl_t *lcmgl_waypoints;
  bot_lcmgl_t *lcmgl_arc;
  double end_goal[2];
  int use_waypoint;
  int have_nav_waypoint;
  double nav_waypoint[3];

  erlcm_kinect_range_msg_t *kinect_msg;
  
  erlcm_point_list_t *path;
  int tourguide_mode; //this module works only if we are not in tour guide mode
  int move_permission;
  gboolean use_theta;
  int started_goal_rotate;
} State;

void controller_update (State *self, int64_t utime);

struct laser_data *laser_data_copy (struct laser_data *ldata)
{
  struct laser_data *l = (struct laser_data*) calloc(1, sizeof(struct laser_data));
  for (int i=0;i<12;i++)
    l->m[i] = ldata->m[i];
  l->msg = bot_core_planar_lidar_t_copy (ldata->msg);
  return l;
}

inline int get_ind(int x,int count)
{
  if(x>=0 && x <= count-1)
    return x;
  else if(x >count-1)
    return x-(count);
  else
    return (count + x);
}


static inline double 
normalize_theta(double theta)
{
  int multiplier;
  
  if (theta >= -M_PI && theta < M_PI)
    return theta;
  
  multiplier = (int)(theta / (2*M_PI));
  theta = theta - multiplier*2*M_PI;
  if (theta >= M_PI)
    theta -= 2*M_PI;
  if (theta < -M_PI)
    theta += 2*M_PI;

  return theta;
}

//need to make this function better - seems to have some faults 
static inline int
geom_ray_rect_intersect_2d(const point2d_t *ray_start, const vec2d_t *ray_dir,
			   const point2d_t *obs_center, double h, double v, double alpha, 
			   double theta) 
{
  int collision = 0;
  if(fabs(ray_start->x - obs_center->x) < h/2 && 
     fabs(ray_start->y - obs_center->y) < v/2){
    collision = 1;
    //fprintf(stderr,"Collision 0\n");
    return 1;
  }
  //transform in to the new co-ordinate frame
  double x_n = obs_center->x * ray_dir->x +  obs_center->y * ray_dir->y;
  double y_n = -obs_center->x * ray_dir->y + obs_center->y * ray_dir->x;
  
  //if within the rectangle in the new co-ordinate frame
  if(fabs(x_n) < h/2 && fabs(y_n) < v/2){
    collision = 1;
    //fprintf(stderr,"Collision 1\n");
    return 1;
  }

  //check if within the arc of motion
  double obs_theta = atan2(obs_center->y, obs_center->x);
  int within = 0;
  //not sure if this does what is required 

  if(fabs(normalize_theta(obs_theta - (-alpha))) < theta){
    within = 1;    
  }
  else if(fabs(normalize_theta(obs_theta - alpha)) < theta){
    within = 1;
  }
  else if(fabs(normalize_theta(obs_theta - (M_PI - alpha))) < theta){
    within = 1;
  }
  else if(fabs(normalize_theta(obs_theta - (-M_PI + alpha))) < theta){ 
    within = 1;
  }
  if(within){    
    double obs_dist = hypot(obs_center->x, obs_center->y);
    double arc_dist = hypot(h/2, v/2);
    if(obs_dist < arc_dist){
      //fprintf(stderr,"Collision 2\n");
      //fprintf(stderr,"%f < %f\n", obs_dist, arc_dist);
      return 1;
    }
  }
  
  return 0;
}

static inline int
geom_ray_circle_intersect_2d(const point2d_t *ray_start, const vec2d_t *ray_dir,
        const point2d_t *circle_center, double radius, 
        double *t_closest, double *t_farthest)
{
    point2d_t c = {
        circle_center->x - ray_start->x,
        circle_center->y - ray_start->y,
    };
    double dsq = geom_vec_magnitude_squared_2d(ray_dir);
    double dot = geom_vec_vec_dot_2d(ray_dir, &c);
    double rsq = radius*radius;
    double det = dot*dot - dsq * (geom_vec_magnitude_squared_2d(&c) - rsq);
    if(det < 0) {
        return 0;
    } else if(fabs(det) < GEOM_EPSILON) {
        if(dot < 0)
            return 0;
        if(t_closest)
            *t_closest = dot / dsq;
        if(t_farthest)
            *t_farthest = dot / dsq;
        return 1;
    } else {
        double k = sqrt(det);
        double d1 = (dot - k) / dsq;
        double d2 = (dot + k) / dsq;
        int result = 0;
        if(d1 > 0) {
            if(t_closest)
                *t_closest = d1;
            result++;
        }
        if(d2 > 0) {
            if(result == 1) {
                if(t_farthest)
                    *t_farthest = d2;
            } else if(t_closest) {
                *t_closest = d2;
            }
            result++;
        }
        return result;
    }
}

//check for a collision if we rotate 
int 
check_rot_collision(pointlist2d_t **points, int nchannels, 
		    pointlist2d_t **rpoints, int nrchannels )
{
  double radius = ROBOT_RADIUS + MARGIN; //sqrt(pow(ROBOT_LENGTH/2,2) + pow(ROBOT_WIDTH/2,2))+ MARGIN;

  for (int lidx = 0; lidx < nchannels; lidx++) {
    pointlist2d_t *line = points[lidx];
    for (int i = 0; i < line->npoints; i++) {
      double x_r = line->points[i].x;
      double y_r = line->points[i].y;
      
      double p_r = sqrt(pow(x_r,2) + pow(y_r,2));

      if (p_r < radius){
	fprintf(stderr,"\t====[C] Collision (Rotation) => (%f,%f) => %f: Radius  :%f\n",x_r,y_r, p_r, radius);      
	return 1;
      }
    }
  }
  for (int lidx = 0; lidx < nrchannels; lidx++) {
    pointlist2d_t *line = rpoints[lidx];
    for (int i = 0; i < line->npoints; i++) {
      double x_r = line->points[i].x;
      double y_r = line->points[i].y;
      
      double p_r = sqrt(pow(x_r,2) + pow(y_r,2));

      if (p_r < radius){
	fprintf(stderr,"\t====[C] Collision (Rotation) => (%f,%f) => %f: Radius  :%f\n",x_r,y_r, p_r, radius);      
	return 1;
      }
    }
  }
  return 0;
}


int 
check_person_collision(erlcm_people_pos_msg_t * person_msg, double angle){
  /*int no_people = person_msg->num_people;
  int tour_ind = person_msg->followed_person;
  double c = cos(angle);
  double s = sin(angle);
  double side_t = SIDE_PERSON_THRESHOLD;
  double forward_t = FORWARD_PERSON_DIST;
  double width = ROBOT_WIDTH;

  for(int i=0; i < no_people; i++){
    if(i==tour_ind){
      continue;
    }
    else{
      //convert the person position to the new co-ordinate frame and then see if the 
      //person is within the new path
      double x = person_msg->people_pos[i].x;
      double y = person_msg->people_pos[i].y;

      double x_r = x*c + y*s;
      double y_r = -x*s + y*c;

      if((x_r < forward_t && x_r > 0) 
	 && (y_r < (width/2 + side_t)  && y_r > - (width/2 + side_t))){ 
	//with in the safety boundry	
	fprintf(stderr,"\t====[C] Collision (possible person) (%f,%f)\n ^^^^^^^^^",x_r,y_r);      
	return 1;
      }
    }
    }*/
  return 0;
}

//check if any of the points are within our region of occupation - suggesting a collision very soon 
int 
check_collision(pointlist2d_t **points, int nchannels, pointlist2d_t **rpoints, 
		int nrchannels,
		double *min_side_distance)
{
  double length = ROBOT_LENGTH + MARGIN;
  double width = ROBOT_WIDTH + MARGIN;

  double min_side_dist = HUGE;

  int collision_det = 0;

  double temp_side_dist;

  for (int lidx = 0; lidx < nchannels; lidx++) {
    pointlist2d_t *line = points[lidx];
    for (int i = 0; i < line->npoints; i++) {
      double x_r = line->points[i].x;
      double y_r = line->points[i].y;

      if((x_r < length/2 && x_r > -length/2) && (y_r < width/2 && y_r > -width/2)){ 
	//with in the safety boundry	
	fprintf(stderr,"\t====[C] Collision (Direct) => (%f,%f) \n",x_r,y_r);      
	collision_det = 1;
      }
      //calculate the min side distance
      if((x_r > -length/2) && (x_r < FORWARD_SCAN_DIST)){
	temp_side_dist = fabs(y_r);
	if(temp_side_dist < min_side_dist){
	  min_side_dist = temp_side_dist; 
	}	  
      }
    }
  }
  for (int lidx = 0; lidx < nrchannels; lidx++) {
    pointlist2d_t *line = rpoints[lidx];
    for (int i = 0; i < line->npoints; i++) {
      double x_r = line->points[i].x;
      double y_r = line->points[i].y;

      if((x_r < length/2 && x_r > -length/2) && (y_r < width/2 && y_r > -width/2)){ 
	//with in the safety boundry	
	fprintf(stderr,"\t====[C] Collision (Direct) => (%f,%f) \n",x_r,y_r);      
	collision_det = 1;
      }
      //calculate the min side distance
      if((x_r > -length/2) && (x_r < FORWARD_SCAN_DIST)){
	temp_side_dist = fabs(y_r);
	if(temp_side_dist < min_side_dist){
	  min_side_dist = temp_side_dist; 
	}	  
      }
    }
  }
  *min_side_distance = min_side_dist;

  return collision_det;
}

void laser_data_destroy (struct laser_data *ldata)
{
  bot_core_planar_lidar_t_destroy (ldata->msg);
}

//for printing status 
void print_following_state(State *s)
{
  if(s->following_state == FOLLOWING)
    fprintf(stderr," F-State : Following \n");
  if(s->following_state == PAUSED)
    fprintf(stderr," F-State : Paused \n");
  if(s->following_state == IDLE)
    fprintf(stderr," F-State : Stopped \n");  
  if(s->collision_det){
    fprintf(stderr," Collision - Detected \n");
  }
  else if (!s->collision_det){
    fprintf(stderr," Collision - Clear  \n");
  }
    
}

void print_tracking_state(State *s)
{
  if(s->tracking_state == VALID_HEADING)
    fprintf(stderr," T-State : Valid Heading \n");
  if(s->tracking_state == TOO_CLOSE)
    fprintf(stderr," T-State : Too Close\n");
  if(s->tracking_state == LOST)
    fprintf(stderr," T-State : Lost \n");
}

void print_status(State *s){
  print_following_state(s);
  print_tracking_state(s);  
}

//converting from local (robot) co-ordinates to global coordinates
void local_to_global(double lx, double ly, double *gx, double *gy, State *state){
  double s,c;
  bot_fasttrig_sincos(state->robot_heading,&s, &c);
  double x = state->robot_pos[0];
  double y = state->robot_pos[1];
  *gx = x + lx*c - ly*s;
  *gy = y + lx*s + ly*c;
  return;
}

void global_to_local(double gx, double gy, double *lx, double *ly, State *state){
  double s,c;
  bot_fasttrig_sincos(state->robot_heading,&s, &c);

  double dx = gx - state->robot_pos[0];
  double dy = gy - state->robot_pos[1];
  
  *lx = dx*c + dy*s;
  *ly = dy*c - dx*s;
  return;
}

void draw_arc(State *self, double tv, double rv){
  /* draw the obstacles */
  bot_lcmgl_t *lcmgl = self->lcmgl_arc;

  if (lcmgl) {
    if(rv !=0){
      double r = tv/rv; 
      lcmglPointSize(4);
      lcmglColor3f(0.5, 0, 0.0);

      double dt = M_PI/2/100; 
      double theta = 0;

      lcmglColor3f(1.0, 0, 0.0);
      lcmglBegin(GL_LINE_STRIP);
    
      for(int i=0; i < 100; i++){
	double x, y; 
	double gx, gy;
	if(r >0){
	  x = r * sin(theta);
	  y = r - r*cos(theta);	  
	}
	else{
	  x = fabs(r) * sin(theta); 
	  y = r * (1 - cos(theta)); 
	}

	local_to_global(x, y,&gx, &gy,self);
	theta +=dt;
	lcmglVertex3d(gx,gy, 0);
      }
      lcmglEnd();
    }
    else{//inf i.e. tv 
      //just draw forward 
      double gx, gy;
      local_to_global(0, 0,&gx, &gy,self);
      lcmglVertex3d(gx,gy, 0);
      local_to_global(1.0, 0,&gx, &gy,self);      
    }
    bot_lcmgl_switch_buffer (lcmgl);
  }
  return;
}

/*
 * Returns the rotational velocity
 */
double get_rot_vel(double angle_to_turn){
  double rv = 0;
  double used_theta = 0.0;

  //normalize the angle 
  angle_to_turn = bot_mod2pi(angle_to_turn) * 1.5;
  
  if (angle_to_turn < 0){
    used_theta = fmax(angle_to_turn, -bot_to_radians (CLAMP_THETA));
  }else{
    used_theta = fmin(angle_to_turn, bot_to_radians (CLAMP_THETA));
  }
  
  //rv = fmin(used_theta / bot_to_radians (80.0), 1.0) * MAX_ROTATION_SPEED;
  rv = fmin(used_theta * 1.0/ bot_to_radians (60.0), 1.0) * MAX_ROTATION_SPEED;

  //is it worth having a D term in here???

  //rv = fmin(max_allowed_rv, *rv);

  return rv;
}

double get_trans_vel_arc_with_obs_fixed_moderated(State *self, 
						  double best_heading, 
						  //double goal_pos[2], 
						  //double lookahead_distance,
						  double clear_dist, 
						  double avg_front_gap,
						  double p_dist, 
						  double heading_to_person, 
						  double *rv, double max_tv_vel, 
						  double side_dist)
{
  double tv = 0;  
  
  double p_projection_to_best_heading = p_dist * cos(bot_mod2pi(heading_to_person - best_heading)); 

  double avg_dist_front_best = (avg_front_gap + clear_dist)/2;

  double avg_free_dist = fmax(fmin(avg_dist_front_best, p_projection_to_best_heading),0);//p_dist; //
  
  double goal_pos[2] = {avg_free_dist * cos(best_heading), avg_free_dist * sin(best_heading)}; 
  
  double max_allowed_tv = max_tv_vel * fmin((side_dist / 0.6), 1.0) * fmin((avg_free_dist / 1.5), 1.0); 

  if(goal_pos[1] == 0){
    //this means straight motion 
    tv = max_tv_vel;
    return tv;
  }  

  fprintf(stderr,"Avg free dist : %f\n", avg_free_dist);

  //double r = pow(lookahead_distance,2)/ (2*goal_pos[1]);
  double r = pow(avg_free_dist,2)/ (2*goal_pos[1]);

  tv = fmax(0, fmin(r * (*rv), max_allowed_tv)); //max_tv_vel)); 

  //update the rv based on the tv that we obtained - should only reduce is 
  double old_rv = *rv; 

  *rv = tv / r; 

  fprintf(stderr, "Orignial : %f New : %f\n", old_rv, *rv); 
  
  return tv; 
}


//spoken responses 
void publish_speech_following_msg(char* property, State *s)
{
  erlcm_speech_cmd_t msg;
  msg.utime = bot_timestamp_now();
  msg.cmd_type = "FOLLOWER";
  msg.cmd_property = property;
  erlcm_speech_cmd_t_publish(s->lcm, "PERSON_TRACKER", &msg);
}

void clear_lcmgl(State *self)
{
  bot_lcmgl_t *lcmgl = self->lcmgl_rot;
  if(lcmgl)
    bot_lcmgl_switch_buffer (lcmgl);
  lcmgl = self->lcmgl_navigator;
  if(lcmgl)
    bot_lcmgl_switch_buffer (lcmgl);
  lcmgl = self->lcmgl_obs;
  if(lcmgl)
    bot_lcmgl_switch_buffer (lcmgl);
  lcmgl = self->lcmgl_footprint;
  if(lcmgl)
    bot_lcmgl_switch_buffer (lcmgl);
}

//following state for the wheelchair - this decides whether we follow a person or not 
void update_following_state_new(enum FollowCommand cmd, State *s)
{
  if(cmd == STOP){
    //should stop the wheelchair - not to be started until we have a follow command
    //lcm_carmen3d_robot_velocity_command(0.0, 0.0,s->lcm);

    if(s->following_state == FOLLOWING){
      fprintf(stderr,"Stopping Wheelchair\n");
    }
    else{
      //fprintf(stderr,"Chair is already stopped\n");
    }
    s->following_state = IDLE;
  }
  else if(cmd==FOLLOW){//check if we have a tracking of the person 
    if(s->tracking_state != LOST){//if person is tracked, start following            
      if(s->following_state == IDLE){//if we were idle before emit message
	fprintf(stderr," => Starting to Follow \n");
	//publish_speech_following_msg("FOLLOWING",s);
      }
      else if(s->following_state == PAUSED){//if we were idle before emit message
	fprintf(stderr," => Resuming following\n");
      }      
      s->following_state = FOLLOWING;
    }
    else{//we dont have an estimate of the person 
      //we should have a state that waits for a person location while being in the following state
      s->following_state = WAITING;
      fprintf(stderr,"Waiting for person estimate\n");
    }    
  }
  else if(cmd==PAUSE){//Pause the wheelchair 
    if(s->following_state == IDLE){
      //fprintf(stderr,"Chair Already Stopped");
    }
    else{
      fprintf(stderr," => Pausing Chair \n");
      if(s->following_state == WAITING){
	//publish_speech_following_msg("FOLLOWING",s);
      }
      s->following_state = PAUSED;
      fprintf(stderr,"**** Following State : %d\n", s->following_state);
      //we are too close to person position - pausing wheelchair
    }
    //lcm_carmen3d_robot_velocity_command(0.0, 0.0,s->lcm);
  }
  else if(cmd==RESUME){//check if we have a tracking of the person 
    if((s->following_state == PAUSED || s->following_state == WAITING) 
       && (s->tracking_state != LOST)){
      if(s->following_state == WAITING){
	//publish_speech_following_msg("FOLLOWING",s);
      }
      s->following_state = FOLLOWING;
      fprintf(stderr," => Resuming Following\n");
    }
  }
}


void nav_plan_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
		       const char * channel __attribute__((unused)), 
		       const erlcm_nav_plan_result_t * msg,
		       void * user  __attribute__((unused)))

{
  State *s = (State *)user;  
  static double last_wait_loc[2] = {.0,.0};
  static int64_t last_resume_msg_utime = 0;

  if(msg->plan_result == ERLCM_NAV_PLAN_RESULT_T_RESULT_NO_PLAN){
    fprintf(stderr,"+++++++++++++++++++++No valid path\n");
    if(s->nav_pause ==0){     
      fprintf(stderr,"Was not paused earlier \n");
      fprintf(stderr,"Dist from last pause : %f, (%f,%f)\n", hypot(last_wait_loc[0] - s->robot_pos[0], last_wait_loc[1] - s->robot_pos[1]), 
	      last_wait_loc[0], last_wait_loc[1]);
      if(s->have_goal_heading && ((s->move_permission ==1) && (hypot(last_wait_loc[0] - s->robot_pos[0], last_wait_loc[1] - s->robot_pos[1]) > 0.5))){
	publish_speech_following_msg("WAITING", s);
	fprintf(stderr,"Sending Waiting msg \n");
	
      }

      s->nav_pause = 1;
      last_wait_loc[0] = s->robot_pos[0];
      last_wait_loc[1] = s->robot_pos[1];
    }
  }
  else if(msg->plan_result == ERLCM_NAV_PLAN_RESULT_T_RESULT_PLAN_REACH_GOAL){
    fprintf(stderr,"+++++++++++++++++++++We have valid path => Goal : %f,%f\n", msg->navgoal_msg.goal.x, msg->navgoal_msg.goal.y);
    s->end_goal[0] = msg->navgoal_msg.goal.x;
    s->end_goal[1] = msg->navgoal_msg.goal.y;
    
    //check if we are supposed to go to the goal
    s->use_theta = msg->navgoal_msg.use_theta;
    if(s->use_theta == TRUE){
      fprintf(stderr,"Go to the exact goal heading\n");
    }
    else{
      fprintf(stderr,"--------Any Orientation \n");
    }

    if(s->nav_pause == 1){      
      if((msg->utime - last_resume_msg_utime) > 5e6){
	last_resume_msg_utime = msg->utime;
	//publish_speech_following_msg("RESUMING", s);
      }
    }
    s->nav_pause = 0;
    s->tracking_state = VALID_HEADING;
    update_following_state_new(FOLLOW,s);
    s->have_goal_heading = TRUE;
  }
  else if(msg->plan_result == ERLCM_NAV_PLAN_RESULT_T_RESULT_PLAN_AT_GOAL){
    fprintf(stderr,"+++++++++++++++++++++We are at the goal++++++++++++++++++\n");
  }  
}

void nav_waypoint_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
		       const char * channel __attribute__((unused)), 
		       const erlcm_waypoint_msg_t *msg,
		       void * user  __attribute__((unused)))

{
  fprintf(stderr,"@@@@@@@@@ Waypoint received\n");
  State *s = (State *)user;  
  s->have_nav_waypoint = 1;
  s->nav_waypoint[0] = msg->pos.x;
  s->nav_waypoint[1] = msg->pos.y;
  s->nav_waypoint[2] = msg->pos.yaw;
}

void nav_trajectory_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
		       const char * channel __attribute__((unused)), 
		       const erlcm_point_list_t * msg,
		       void * user  __attribute__((unused)))

{
  State *s = (State *)user;  
  
  fprintf(stderr,"T\n");
  
  if (s->path != NULL) {
    erlcm_point_list_t_destroy(s->path);
  }
  s->path = erlcm_point_list_t_copy(msg);
  
  if (s->path->num_points > 0){
    s->tracking_state = VALID_HEADING;

    //this needs to be changed to start when we command 
    //s->following_state == FOLLOWING;
    //update_following_state_new(FOLLOW,s);
    //s->have_goal_heading = TRUE;
  }
  else{
    s->have_goal_heading = FALSE;
  }
  //if (s->path->trajectory_length == 0)
  return;
}

//lcm handlers 
void mode_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
		  const char * channel __attribute__((unused)), 
		  const erlcm_tagged_node_t * msg,
		  void * user  __attribute__((unused)))
{
  State *s = (State *)user;  
  char* type = msg->type;
  char* new_mode = msg->label;
  if(!strcmp(type,"mode")){
    //mode has changed - 
    if(!strcmp(new_mode,"navigation")){//navigation mode - stop following guide
      s->tourguide_mode = 0;
      fprintf(stderr,"Navigation mode\n");
    }
    if(!strcmp(new_mode,"tourguide")){//tourguide mode
      s->tourguide_mode = 1;
      fprintf(stderr,"Tourguide mode\n");
    }
  }
}

void speech_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), 
		    const erlcm_speech_cmd_t * msg,
		    void * user  __attribute__((unused)))
{
  //these should be sent through a different channel  
  State *s = (State *)user;  
  char* cmd = msg->cmd_type;
  char* property = msg->cmd_property;

  if(strcmp(cmd,"FOLLOWER")==0){
    if(strcmp(property,"GO")==0){//guide has asked us to follow him
      //have permission to follow once we say go 
      //update_following_state_new(FOLLOW,s);
      s->move_permission = 1;
      s->started_goal_rotate = 0;
      fprintf(stderr,"Waiting for waypoints\n");
      //fprintf(stderr,"Speech Cmd : => Start Following\n");
    }
    else if(strcmp(property,"STOP")==0){//guide has asked us to stop following him
      update_following_state_new(STOP,s);
      s->have_goal_heading = FALSE;
      fprintf(stderr,"Speech Cmd : => Stop\n");
      s->move_permission = 0;
    }
  }
  if(strcmp(cmd,"TRACKER")==0){
    if(strcmp(property,"START_LOOKING")==0){//guide has reset tracking so we are stoping till we get a new tracking
      update_following_state_new(STOP,s);
      fprintf(stderr,"Speech Cmd : => Reset Tracking\n");
    }
  }
}

//keep an updated value of the robot pose
static void pose_handler(const lcm_recv_buf_t *rbuf, const char *channel, 
			 const bot_core_pose_t *msg, void *user_data )
{
  State *s = (State*) user_data;  
  double rpy[3];
  
  bot_quat_to_roll_pitch_yaw(msg->orientation, rpy);  
  s->robot_pos[0] = msg->pos[0];
  s->robot_pos[1] = msg->pos[1];
  s->robot_heading = rpy[2];  
  fprintf(stderr,"P: %f,%f\n",  s->robot_pos[0],  s->robot_pos[1]);
}


static void on_kinect_range(const lcm_recv_buf_t *rbuf, const char *channel, 
		      const erlcm_kinect_range_msg_t *msg, void *user_data )
{
  State *self = (State*) user_data;

  if(self->kinect_msg !=NULL){
    erlcm_kinect_range_msg_t_destroy(self->kinect_msg);
  }
  self->kinect_msg = erlcm_kinect_range_msg_t_copy(msg);
  fprintf(stderr,"K");
}

/* store laser data in memory
 */
static void on_laser(const lcm_recv_buf_t *rbuf, const char *channel, 
		      const bot_core_planar_lidar_t *msg, void *user_data )
{
  State *self = (State*) user_data;
  struct laser_channel *lchannel;
  lchannel = g_hash_table_lookup(self->laser_hashtable, channel);

  if (lchannel == NULL) {
    lchannel = (struct laser_channel*) calloc(1, sizeof(struct laser_channel));
    lchannel->data = g_queue_new ();
    lchannel->complained = 0;
    strcpy(lchannel->name, channel);

    // handle a mask
    char key[1024];
    sprintf(key, "planar_lidars.%s.mask", channel);
    char **masks = bot_param_get_str_array_alloc(self->config, key);
    int nmasked = 0;
    for (int j = 0; masks && masks[j]!=NULL && masks[j+1]!=NULL; j+=2) {
      double v0 = strtof(masks[j], NULL);
      double v1 = strtof(masks[j+1], NULL);

      // take small steps so that we won't skip a bucket due to a 
      // rounding area (hence the extra / 2.0)
      double theta_per_index = 2.0 * M_PI / MASK_BUCKETS;
      for (double t = v0; t <= v1; t += theta_per_index/2.0) {
	int idx = bot_theta_to_int(t, MASK_BUCKETS);            
	if(!lchannel->mask[idx]) {
	  nmasked++;
	}
	lchannel->mask[idx]=1;
      }
    }
    g_hash_table_insert(self->laser_hashtable, lchannel->name, lchannel);
    g_queue_push_head (self->channels, lchannel);
  }

  struct laser_data *ldata = (struct laser_data*) calloc(1, sizeof(struct laser_data));
  //need to append our local transformation vector here 

  /* push data in the queue */
  ldata->msg = bot_core_planar_lidar_t_copy(msg);

  g_queue_push_head (lchannel->data, ldata);

  if (g_queue_get_length (lchannel->data) > LASER_MEMORY) {
    struct laser_data *ld = (struct laser_data*)g_queue_pop_tail (lchannel->data);
    laser_data_destroy (ld);
  }

  /* process returns */
  if(!strcmp(channel,"SKIRT_FRONT")){
    controller_update (self, msg->utime);
  }
  return;
}

/*void people_pos_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), 
			const carmen3d_people_pos_msg_t * msg,
			void * user  __attribute__((unused)))
{
  State *s = (State *)user;
  if(s->person_msg!=NULL){
    carmen3d_people_pos_msg_t_destroy(s->person_msg);
  }
  s->person_msg = carmen3d_people_pos_msg_t_copy(msg);
  int ind = s->person_msg->followed_person;

  if(ind>=0){  //we have a valid person estimate
    double person[2] = {s->person_msg->people_pos[ind].x, s->person_msg->people_pos[ind].y};
    //check condition to start following a person
    double act_dist = hypot(person[0],person[1]);
    double act_angle = atan2(person[1],person[0]);
    s->goal_heading = act_angle;
    s->have_goal_heading = TRUE;
    s->last_goal_heading_utime = msg->utime;
    double used_stop_radius;
    if(!s->keep_close){
      used_stop_radius = STOP_RADIUS;
    }
    else{
      used_stop_radius = C_STOP_RADIUS;
    }
    if(act_dist >= used_stop_radius){//heading restriction removed from here
      s->tracking_state = VALID_HEADING;
      update_following_state_new(RESUME,s);
      if(s->following_state == FOLLOWING && act_dist >= 5.0){
	if(!s->too_far){ //we were within the range 
	  s->too_far = 1;
	  publish_speech_following_msg("SLOW_DOWN",s);  //warning to slow down 
	}	
      }
      else{
	s->too_far = 0;
      }
    }
    else{
      s->tracking_state = TOO_CLOSE;
      //we pause 
      update_following_state_new(PAUSE,s);
    }
  }
  else{
    if(s->tracking_state !=LOST){ //we had a track of the person before 
      //prevents the system from stoping before we get a new estimate
      s->tracking_state = LOST;
      s->keep_close = 0;
      publish_speech_following_msg("UNABLE_TO_FOLLOW_LOST",s); 
      update_following_state_new(STOP,s);
    }
    //we stop 

  }
  //print_status(s);
  }*/

/* computes intersection between a line segment and a point list
 */
double ray_cast_test(point2d_t *xy0, point2d_t *xy1, pointlist2d_t *points)
{
  if (!points)
    return HUGE;

  double closest_dist = HUGE;
  assert (xy0);
  assert (xy1);

  for (int pidx = 0; pidx+1 < points->npoints; pidx++) {
    point2d_t *p0 = &points->points[pidx];
    point2d_t *p1 = &points->points[pidx+1];
    assert (p0);
    assert (p1);

    point2d_t res;
    if (geom_line_seg_line_seg_intersect_2d(xy0, xy1, p0, p1, &res)) {
      closest_dist = fmin(closest_dist, sqrt(powf(res.x - xy0->x,2) +
					     powf(res.y - xy0->y,2)));
    }
  }
  return closest_dist;
}

void laser_returns_destroy (GQueue *channels)
{
  for (GList *iter=g_queue_peek_head_link (channels);iter;iter=iter->next) {
    struct laser_channel *lc = (struct laser_channel*)iter->data;
    for (GList *iterc=g_queue_peek_head_link (lc->data);iterc;iterc=iterc->next) {
      struct laser_data *ld = (struct laser_data*)iterc->data;
      laser_data_destroy (ld);
    }
    g_queue_free (lc->data);
  }
  g_queue_free (channels);
}

GQueue *laser_returns_copy (GQueue *channels)
{
  GQueue *q = g_queue_new ();

  for (GList *iter=g_queue_peek_head_link (channels);iter;iter=iter->next) {

    struct laser_channel *lc = (struct laser_channel*)iter->data;

    struct laser_channel *lchannel = (struct laser_channel*) calloc(1, sizeof(struct laser_channel));
    lchannel->data = g_queue_new ();
    lchannel->complained = lc->complained;
    strcpy(lchannel->name, lc->name);
    memcpy(lchannel->mask, lc->mask, MASK_BUCKETS * sizeof(int8_t));
    if (g_queue_is_empty (lc->data))
      continue;
    g_queue_push_head (lchannel->data, laser_data_copy (g_queue_peek_head (lc->data)));
    g_queue_push_head (q, lchannel);
  }
  return q;
}

void print_laser_returns (GQueue *channels, const char *filename)
{
  if (g_queue_is_empty (channels))
    return;

  for (GList *iter=g_queue_peek_head_link (channels);iter;iter=iter->next) {

    struct laser_channel *lc = (struct laser_channel*)iter->data;
    if (g_queue_is_empty (lc->data)) {
      fprintf (stderr, "laser data is empty.\n");
      continue;
    }
    struct laser_data *ldata = (struct laser_data*)g_queue_peek_head (lc->data);

    FILE *fp = fopen (filename, "w");

    for (int rangeidx = 0; rangeidx < ldata->msg->nranges; rangeidx++) {
      double range = ldata->msg->ranges[rangeidx];
      fprintf (fp, "%d %.3f\n", rangeidx, range);
    }
    fclose (fp);
  }
}

/* convert laser returns into a point list in the local frame
 */
pointlist2d_t **laser_to_local_points (State *s, GQueue *channels, int *npoints)
{
  int n = g_queue_get_length (channels); 
  if(s->kinect_msg !=NULL){
    fprintf(stdout,"Kinect Points\n");
    n++;
  }
  *npoints = n;

  if (g_queue_is_empty (channels))
    return NULL;

  pointlist2d_t **points = (pointlist2d_t**)malloc(n*sizeof(pointlist2d_t*));

  for (int ridx=0;ridx<n;ridx++)
    points[ridx] = NULL;

  int idx = 0;
  for (GList *iter=g_queue_peek_head_link (channels);iter;iter=iter->next) {
    struct laser_channel *lc = (struct laser_channel*)iter->data;
    
    if (g_queue_is_empty (lc->data)) {
      fprintf (stderr, "laser data is empty.\n");
      continue;
    }
    struct laser_data *ldata = (struct laser_data*)g_queue_peek_head (lc->data);

    double pos_offset = 0.0;
    double ang_offset = 0.0;

    if(!strcmp(lc->name,"SKIRT_FRONT")){
      pos_offset = s->frontlaser_offset;
      ang_offset = s->front_anglular_offset;
    }

    if(!strcmp(lc->name,"SKIRT_REAR")){
      pos_offset = s->rearlaser_offset;
      ang_offset = s->rear_anglular_offset;
    }
    
    double hit_buf[ldata->msg->nranges][2];
    int nhits = 0;

    for (int rangeidx = 0; rangeidx < ldata->msg->nranges; rangeidx++) {
      double range = ldata->msg->ranges[rangeidx];
      
      double theta = ldata->msg->rad0 + ldata->msg->radstep*rangeidx + ang_offset;

      // mask?
      int maskind = bot_theta_to_int(theta, MASK_BUCKETS);
      if(lc->mask[maskind]) {
	continue;
      }
      if(range < 0.02) { //already filters outliers by skipping these points 
	continue;
      }
      if(range >= 30.0) {
	continue;
      }

      double local_xyz[3];
      double s, c;
      bot_fasttrig_sincos(theta, &s, &c);

      //convert to local frame
      local_xyz[0] = range *c + pos_offset; 
      local_xyz[1] = range*s; 

      hit_buf[nhits][0] = local_xyz[0];
      hit_buf[nhits][1] = local_xyz[1];
      nhits++;
    }

    pointlist2d_t *pt = pointlist2d_new(nhits);
    for(int i=0; i<nhits; i++) {
      pt->points[i].x = hit_buf[i][0];
      pt->points[i].y = hit_buf[i][1];
    }
    points[idx] = pt;
    idx++;
  }
  
  //add the kinect points here also 
  if(s->kinect_msg !=NULL){
    pointlist2d_t *pt = pointlist2d_new(s->kinect_msg->no_readings);
    for(int i=0; i < s->kinect_msg->no_readings;i++){      
      pt->points[i].x = s->kinect_front_offset + s->kinect_msg->ranges[i] * cos(s->kinect_msg->thetas[i]);
      pt->points[i].y = s->kinect_side_offset + s->kinect_msg->ranges[i] * sin(s->kinect_msg->thetas[i]);
    }
    points[idx] = pt;
  }
  
  return points;
}

void save_data (double *v, int n, const char *filename)
{
  FILE *fp = fopen (filename, "w");
  for (int i=0;i<n;i++) {
    fprintf (fp, "%d %.4f\n", i, v[i]);
  }
  fclose (fp);
}

void draw_wheelchair(State *self){
  double pos[2] = {self->robot_pos[0], self->robot_pos[1]};
  double theta = self->robot_heading;

  bot_lcmgl_t *lcmgl = self->lcmgl_footprint;
  if(lcmgl){
    lcmglPointSize(15);
    if(self->following_state ==IDLE){
      lcmglColor3f(1, 0, 0);
    }
    else if(self->following_state ==PAUSED){
      lcmglColor3f(0, 0, 1);
    }
    else{
      lcmglColor3f(0, 1, 0); 
    }
    double s = sin(theta);
    double c = cos(theta);
    double xy1[2], xy2[2], xy3[2], xy4[2];
    double h = ROBOT_LENGTH/2 + MARGIN;
    double b = ROBOT_WIDTH/2 + MARGIN;

    xy1[0] = pos[0] + b*s + h*c;
    xy1[1] = pos[1] - b*c + h*s;

    xy2[0] = pos[0] + b*s - h*c;
    xy2[1] = pos[1] - b*c - h*s;

    xy3[0] = pos[0] - b*s - h*c;
    xy3[1] = pos[1] + b*c - h*s;
    
    xy4[0] = pos[0] - b*s + h*c;
    xy4[1] = pos[1] + b*c + h*s;

    lcmglBegin(GL_LINE_LOOP);
    lcmglVertex3d(xy1[0], xy1[1], 0);
    lcmglVertex3d(xy2[0], xy2[1], 0);
    lcmglVertex3d(xy3[0], xy3[1], 0);
    lcmglVertex3d(xy4[0], xy4[1], 0);    
    lcmglEnd();
    bot_lcmgl_switch_buffer (lcmgl);
    }
}



/* update controller commands
 */
int compute_lookahead_angle_average(int64_t utime, State *self, double lookahead_distance, double *angle, gboolean *turn_in_place, 
				    double *brange, double *dist_to_target, double *rv, double *tv)
{    
  static int stuck_msg_sent = 0;
  static int64_t stuck_utime = 0;
  static int64_t pause_utime = 0;
  int use_slowdown = 0;
  bot_lcmgl_t *lcmgl;
  fprintf(stderr,"---------------------------------------------------------\n");
  fprintf(stderr,"Goal Command Go to heading= %d\n", self->use_theta);
  static double prev_rv = 0.0, prev_tv = 0.0;
  double pos[3] = {.0, .0, .0};
  double bot_heading = 0.0;
  static int keep_close_msg = 0;

  static double last_small_gap_time = .0;
  static double small_gap_robot_pos[2] = {.0,.0};

  int collision_det = 0;
 
  double max_range = lookahead_distance;

  GQueue *channels = laser_returns_copy (self->channels);
  GTimer *timer = g_timer_new ();

  /* convert laser returns into point list */
  int nchannels=0;
  pointlist2d_t **points = laser_to_local_points (self, channels, &nchannels);

  //these contain the raw unflitered laser returns - for the front laser (contains the feet observations) 
  int nrchannels=0;
  pointlist2d_t **rpoints = NULL;

  if (!points) {
    fprintf (stderr, "failed to convert laser returns.\n");
    g_timer_destroy (timer);
    return -1;
  }

  /* draw the obstacles */
  lcmgl = self->lcmgl_obs;

  if (self->draw_obs && lcmgl) {
    lcmglPointSize(4);
    lcmglColor3f(0.5, 0, 0.0);
    //Drawing the obstacles
    for (int lidx = 0; lidx < nchannels; lidx++) {
      pointlist2d_t *line = points[lidx];
      lcmglBegin(GL_POINTS);
      double gx = .0, gy = .0;
      for (int pidx = 0; pidx < line->npoints; pidx++) {
	local_to_global(line->points[pidx].x, line->points[pidx].y,&gx, &gy,self);
	lcmglVertex3d(gx,gy, pos[2]);
      }
      lcmglEnd();
      lcmglColor3f(0.5, 0, 0.0);
      for (int pidx = 0; pidx < line->npoints; pidx++) {
	local_to_global(line->points[pidx].x, line->points[pidx].y,&gx, &gy,self);
	double xyz[3] = { gx, gy , 0 };
	lcmglCircle(xyz, SAFETY_RADIUS);
      }
    }
    bot_lcmgl_switch_buffer (lcmgl);
  }

  double min_side_dist = 0;
  // --------- check for collisions ---------------- //

  //maybe this can all be done once - rot and direct
  int proximity = check_collision(points, nchannels, rpoints, nrchannels,&min_side_dist);

  if(proximity){//possible collision - might be good to just publish 0 velocities 
    //update_following_state_new(STOP,self);
    //self->collision_det = 1;
    if(stuck_msg_sent==0 && ((double)(utime - stuck_utime))/1e6 > 60){
      publish_speech_following_msg("TOO_CLOSE", self);
      stuck_msg_sent =1;
      stuck_utime = utime;
    }
    collision_det = 1;
    fprintf(stderr,"============ Emergency Stop ============== \n");
    /* free points */
    for (int ridx=0;ridx<nchannels;ridx++) {
      if (points[ridx])
	pointlist2d_free (points[ridx]);
    }
    free (points);
    
    /* free channel copy */
    laser_returns_destroy (channels);

    lcmgl = self->lcmgl_navigator_info;
    if(lcmgl){
      double g_robot[3] = {.0,.0,.0};
      local_to_global(pos[0]+5.0, pos[1]+5.0,&g_robot[0], &g_robot[1],self);
      char seg_info[512];
      sprintf(seg_info,"Emergency Stop - Direct Collision Detected\n");
      fprintf(stderr,"Status : %s\n", seg_info);
      lcmglColor3f(1, 0, 0); 
      bot_lcmgl_text(lcmgl, g_robot, seg_info);
    }
    bot_lcmgl_switch_buffer (lcmgl);

    return 1; //collision - emergency stop 
  }
  else{
    stuck_msg_sent = 0;
  }

  // ---------------- Do scoring ---------------- //
  
  //span to check around the wheelchair
  double span = bot_to_radians (90.0); //360 seems like over kill 
  //No of buckets 
  double dtheta = bot_to_radians (5.0);//bucket size
  int nrays = 2*span/dtheta;
  double theta0 = -span;
  double thetas[nrays];
  double ranges[nrays];
  double f_ranges[nrays];
  double xys[nrays][2];

  double closest_pt_dist = INFINITY;
  point2d_t robot_pt = { pos[0], pos[1] };

  for (int lidx = 0; lidx < nchannels; lidx++) {
    pointlist2d_t *pts = points[lidx];
    for(int i=0; i<pts->npoints; i++) {
      double d = geom_point_point_distance_2d(&robot_pt, &pts->points[i]);
      closest_pt_dist = fmin(d, closest_pt_dist);
    }
  }
  double safety_radius;

  safety_radius = fmin(closest_pt_dist - 0.01, SAFETY_RADIUS);  
  
  int index_0 = 0; 

  for (int ridx=0;ridx<nrays;ridx++) {  
    double theta = theta0 + ridx * dtheta;

    if(theta==0){
      index_0 = ridx;
    }

    thetas[ridx] = theta;

    double s = sin (theta);
    double c = cos (theta);

    point2d_t ray_start = { pos[0], pos[1]};  //robot position
    point2d_t ray_dir = { c, s };

    double dist = max_range;
    double long_dist = LONG_LOOKAHEAD_DISTANCE;

    //get this from the config file 
    double alpha = atan2(0.6, 1.0);
    int poss_collision = 0;

    for (int lidx = 0; lidx < nchannels; lidx++) {
      pointlist2d_t *pts = points[lidx];
      for(int i=0; i<pts->npoints; i++) {
	double t = 0.0;
	//if the ray can lead to a collision find the distance of the colliding point (t)
	if(geom_ray_circle_intersect_2d(&ray_start, &ray_dir, 
					&pts->points[i], safety_radius, &t, NULL)) {
	  dist = fmin(dist, t);  //this finds the closest colliding point - for each rays 
	  //dist can never be more than the lookahead distance
	}
	if(geom_ray_rect_intersect_2d(&ray_start, &ray_dir, 
				      &pts->points[i], 1.0, 0.6, alpha, theta)){
	  //fprintf(stderr,"Collision From rotation\n");
	  //possible to skip this whole calculation
	  poss_collision = 1;
	  dist = 0;
	  //break;
	} 

	//might want to do away with this one - unless we need it
	if(geom_ray_circle_intersect_2d(&ray_start, &ray_dir, 
					&pts->points[i], 0.01, &t, NULL)) {
	  long_dist = fmin(long_dist,t);
	  //dist can never be more than the lookahead distance
	}
	
	
      }
    }
    ranges[ridx] = dist;
    f_ranges[ridx] = long_dist;
    xys[ridx][0] = pos[0] + c * dist;
    xys[ridx][1] = pos[1] + s * dist;
  }

  //check the reachability of the waypoints

  lcmgl = self->lcmgl_waypoints;
  int *wp_visibility = (int *)malloc(sizeof(int)*self->path->num_points);
  memset(wp_visibility,0,sizeof(int)*self->path->num_points);
  double *wp_dist = (double *)malloc(sizeof(double)*self->path->num_points);
  memset(wp_dist,0,sizeof(double)*self->path->num_points);
  double *wp_heading = (double *)malloc(sizeof(double)*self->path->num_points);
  memset(wp_heading,0,sizeof(double)*self->path->num_points);
  

  if(lcmgl){
    fprintf(stderr,"Drawing the path points : %d\n", self->path->num_points);
    for(int i=0; i < self->path->num_points; i++){
      double t_xyz[3] = {self->path->points[i].x, self->path->points[i].y, 0.2};
      double t_theta = self->path->points[i].yaw;
      double t_dx =  self->path->points[i].x-self->robot_pos[0];
      double t_dy =  self->path->points[i].y-self->robot_pos[1];
  
      double t_local[2] = {.0,.0};

      t_local[0] = t_dx*cos(self->robot_heading) + t_dy * sin(self->robot_heading);
      t_local[1] = -t_dx*sin(self->robot_heading) + t_dy * cos(self->robot_heading);
      lcmglColor3f(0.5, 0.5, 0.5);
      double t_dist = hypot(t_local[0], t_local[1]);
      double waypoint_heading = atan2(t_local[1], t_local[0]);	
      wp_dist[i] = t_dist;
      wp_heading[i] = waypoint_heading;
      if( t_dist < LONG_LOOKAHEAD_DISTANCE){	
	int angle_ind = (int)((waypoint_heading - theta0)/dtheta);
	//ideally we should check if the wp should be visible based on the map 
	if(t_dist - f_ranges[angle_ind] > 0.2){
	   wp_visibility[i] = 0;
	}
	else{
	   wp_visibility[i] = 1;
	}
      }
      if(t_dist < 2.0){
	if(wp_visibility[i] ==0){
	  //not reachable - draw in red
	  lcmglColor3f(0.5, 0.0, 0.0);
	}
	else{//reachable
	  lcmglColor3f(0.0, 0.0,1.0);
	}
      }
      else{
	lcmglColor3f(0.5, 0.5, 0.5);
      }
      if(i == self->path->num_points -1){
	lcmglColor3f(0.0, 1.0,1.0);
      }
      
      lcmglPointSize(3);	
      lcmglCircle(t_xyz, 0.2);
      //draw the heading
      lcmglPointSize(5);
      lcmglColor3f(0.0, 1.0, 1.0);
      lcmglBegin(GL_LINES);
      lcmglVertex3d(t_xyz[0],t_xyz[1],t_xyz[2]);
      lcmglVertex3d(t_xyz[0] + 0.5 *cos(t_theta) , t_xyz[1] + 0.5 *sin(t_theta), t_xyz[2]);
      lcmglEnd();      
    }
  }
  bot_lcmgl_switch_buffer (lcmgl);

  //if the closest waypoint is too close to the wheelchair pick the next one 
  double dist_to_wp = 0;
  
  int goal_ind = -1;
  int farthest_visible_ind = -1;
  if(self->use_waypoint && self->have_nav_waypoint){
    for(int i=0; i < self->path->num_points; i++){
      //match the waypoint 
      if((self->nav_waypoint[0] == self->path->points[i].x && self->nav_waypoint[1] == self->path->points[i].y) 
	 && self->nav_waypoint[2] == self->path->points[i].yaw){
	fprintf(stderr,"Found Goal waypoint\n");
	goal_ind = i;
	break;
      }
    }
  }
  else{
    for(int i=0; i < self->path->num_points; i++){
      dist_to_wp = hypot(self->path->points[i].x-self->robot_pos[0],self->path->points[i].y-self->robot_pos[1]);
      fprintf(stderr,"Dist to waypoint : %d => %f Visi: %d\n", i, dist_to_wp, wp_visibility[i]);
      if(wp_heading[i] > M_PI/2 || wp_heading[i] < - M_PI/2){
	fprintf(stderr,"Wp [%d] is behind the robot : %f\n", i, 180*wp_heading[i]/M_PI);
      }  
    
      //if(wp_visibility[i]){
      farthest_visible_ind = i;
	//}
      if(dist_to_wp > 1.0){// && wp_visibility[i]==1){
	goal_ind = i;
	fprintf(stderr,"Valid goal found\n");
	break;
      }
    }
  }

  double reach_thresh = 2.0;
  int total_wp = 0;
  int total_vis_wp = 0;
  for(int i=0; i < self->path->num_points; i++){
    //check how many wp that are within x m's of the robot are not reachable
    if(wp_dist[i] < reach_thresh){
      total_wp++;
      if(wp_visibility[i]){
	total_vis_wp++;
      }
    }
  }
  fprintf(stderr,"Total wp : %d => Visible wp : %d\n", total_wp, total_vis_wp);

  if(goal_ind <0){
    fprintf(stderr,"No waypoint found far as 1m\n");
    fprintf(stderr,"Farthest Visible Ind : %d\n", farthest_visible_ind);
    if(farthest_visible_ind <0){
      //issue - there was no valid goal - prob should pause and see if things change 
      goal_ind = 0;
    }
    else{//this might mean that we are close to the goal - if this is so - then we should move towards it
      goal_ind = farthest_visible_ind;
    }
  }
  if(goal_ind == self->path->num_points -1){
    fprintf(stderr,"----Current Goal is the final goal - slowing down\n");
    use_slowdown = 1;
  }  

  double dx =  self->path->points[goal_ind].x-self->robot_pos[0];
  double dy =  self->path->points[goal_ind].y-self->robot_pos[1];
  
  double person[2] = {.0,.0};

  person[0] = dx*cos(self->robot_heading) + dy * sin(self->robot_heading);
  person[1] = -dx*sin(self->robot_heading) + dy * cos(self->robot_heading);
  
  fprintf(stderr,"(%f,%f) =>Local Goal : %f,%f\n", dx, dy, person[0], person[1]);

  double g_goal[3] = {.0,.0,.0};
  local_to_global(person[0], person[1],&g_goal[0], &g_goal[1],self);

  lcmgl = self->lcmgl_goal;
  if(lcmgl){
    double t_xyz[3] = {self->path->points[goal_ind].x, self->path->points[goal_ind].y, 0.2};
    lcmglPointSize(5);
    lcmglColor3f(1.0, 0.0, 0.0);
    lcmglCircle(t_xyz, 0.1);
    //lcmglColor3f(0.5, 0, 0.5);
    //lcmglCircle(g_goal, 0.2);
  }
  bot_lcmgl_switch_buffer (lcmgl);

  //double person[2] = {self->path->points[0].x-self->robot_pos[0],self->path->points[0].y-self->robot_pos[1]};

  double act_dist = hypot(person[0],person[1]); 
  double act_angle = atan2(person[1],person[0]); 
  double heading_at_goal = bot_mod2pi(self->path->points[goal_ind].yaw - self->robot_heading);
  *dist_to_target = act_dist;
  
  double goal_heading = act_angle;//self->goal_heading;
  double goal_pt[2] = {pos[0] + cos (goal_heading) * max_range,
		       pos[1] + sin (goal_heading) * max_range};  //can even use our actual goal if we want
  
  double dist_to_end_goal = hypot(self->end_goal[0]-self->robot_pos[0],
				  self->end_goal[1]-self->robot_pos[1]);

  fprintf(stderr,"Dist to End Goal : %f\n", dist_to_end_goal);
  fprintf(stderr,"Heading Difference : %f\n", 180*heading_at_goal/M_PI);

  if(dist_to_end_goal < 0.25){
    //if we do not care about the goal heading - this is fine
    if(self->use_theta == FALSE){
      fprintf(stderr,"-----------------Goal Reached-----------------\n");
      update_following_state_new(STOP,self);
      self->have_goal_heading = FALSE;
      self->move_permission = 0;
      //should send message 
      erlcm_navigator_status_t nav_stat_msg;
      nav_stat_msg.utime = utime;
      nav_stat_msg.goal_reached = 1;
      nav_stat_msg.goal_x = self->end_goal[0];
      nav_stat_msg.goal_y = self->end_goal[1];
      erlcm_navigator_status_t_publish(self->lcm,"WAYPOINT_NAV_STATUS", &nav_stat_msg);
    }
    else{
      self->started_goal_rotate = 1;
      fprintf(stderr,"-----------------Goal Within reach - checking Orientation-----------\n");
      if(fabs(heading_at_goal) < bot_to_radians(15.0)){	
	fprintf(stderr,"Within waypoint heading Tollerance - stopping\n");
	update_following_state_new(STOP,self);
	self->have_goal_heading = FALSE;
	self->move_permission = 0;

	erlcm_navigator_status_t nav_stat_msg;
	nav_stat_msg.utime = utime;
	nav_stat_msg.goal_reached = 1;
	nav_stat_msg.goal_x = self->end_goal[0];
	nav_stat_msg.goal_y = self->end_goal[1];
	erlcm_navigator_status_t_publish(self->lcm,"WAYPOINT_NAV_STATUS", &nav_stat_msg);
      }
    }
  }

  if(self->started_goal_rotate && fabs(heading_at_goal) < bot_to_radians(15.0)){
    fprintf(stderr,"At goal\n");
    erlcm_navigator_status_t nav_stat_msg;
    nav_stat_msg.utime = utime;
    nav_stat_msg.goal_reached = 1;
    nav_stat_msg.goal_x = self->end_goal[0];
    nav_stat_msg.goal_y = self->end_goal[1];
    erlcm_navigator_status_t_publish(self->lcm,"WAYPOINT_NAV_STATUS", &nav_stat_msg);
    update_following_state_new(STOP,self);
    self->have_goal_heading = FALSE;
    self->move_permission = 0;
  }

  /* find the best ray */
  double best_range = 0;
  double best_dtheta = HUGE;
  
  /* compute score for each ray */
  double scores[nrays];

  double alpha = 1.0;

  int closest_p_ind = -1;
  double closest_dist = 1000.0;

  for (int ridx = 0; ridx < nrays; ridx++) {

    double dist_sq = powf(xys[ridx][0] - goal_pt[0],2) +
      powf(xys[ridx][1] - goal_pt[1],2);  

    if(closest_dist > dist_sq){
      closest_p_ind = ridx;
      closest_dist = dist_sq;
    }

    double score = ranges[ridx] / (1.0 + alpha * dist_sq);   
    scores[ridx] = score;
  }
  
  /* find best direction */

  double avg_scores[nrays];
  int lb = 2;//6;
  /*if(!self->keep_close){
    lb = 5;  //no of brackets to check around the current ray
  }
  else{
    lb = 5;//3;
    }*/
  for (int ridx=0;ridx<nrays;ridx++) {
    int temp_count = 0;
    float temp_avg_score = 0.0;
    for (int i= ridx-lb; i <= ridx+lb;i++){
      temp_count++;
      temp_avg_score += scores[get_ind(i,nrays)];
    }    
    avg_scores[ridx] = temp_avg_score/temp_count;
  }

  double best_score = -1000;
  int best_ridx = -1;
  
  for (int ridx=0;ridx<nrays;ridx++) {    
    if (best_score < avg_scores[ridx]) {//changed to take the average          
      best_ridx = ridx;
      best_score = avg_scores[ridx];      
    }
  }

  int max_width = 2;

  //scan to see how many good rays we have 
  double avg_ray_size = .0;
  double avg_ray_variance = .0;
  for (int i= best_ridx-max_width; i <= best_ridx+max_width;i++){
      avg_ray_size += ranges[get_ind(i,nrays)];
  } 
  avg_ray_size  = avg_ray_size / (2*max_width+1);
  for (int i= best_ridx-max_width; i <= best_ridx+max_width;i++){
    avg_ray_variance += pow(ranges[get_ind(i,nrays)]-avg_ray_size,2);
  }
  avg_ray_variance = avg_ray_variance / (2*max_width+1);

  if(avg_ray_variance > 0.1){
    fprintf(stderr,"Small Gap detected - Reducing the averaging\n");
    self->small_gap = 1;
    
    self->keep_close = 1;   

    last_small_gap_time = utime/1e6;
    small_gap_robot_pos[0] = self->robot_pos[0];
    small_gap_robot_pos[1] = self->robot_pos[1];
    
    //redoing the averaging with a smaller range
    lb = 1;
    for (int ridx=0;ridx<nrays;ridx++) {
      int temp_count = 0;
      float temp_avg_score = 0.0;
      for (int i= ridx-lb; i <= ridx+lb;i++){
	temp_count++;
	temp_avg_score += scores[get_ind(i,nrays)];
      }    
      avg_scores[ridx] = temp_avg_score/temp_count;
    }
    
    best_score = -1000;
    best_ridx = -1;
    
    for (int ridx=0;ridx<nrays;ridx++) {    
      if (best_score < avg_scores[ridx]) {//changed to take the average          
	best_ridx = ridx;
	best_score = avg_scores[ridx];      
      }
    }
  }
  else{
    //we remove the small gap condition once we are past the gap - but keep_close should be in effect 
    self->small_gap = 0;
    
  }

  //keep close detection
  int k_max_width = 8;

  //scan to see how many good rays we have 
  double k_avg_ray_size = .0;
  double k_avg_ray_variance = .0;
  double trav_dist = .0;
  
  for (int i= best_ridx-k_max_width; i <= best_ridx+k_max_width;i++){
      k_avg_ray_size += ranges[get_ind(i,nrays)];
  } 
  k_avg_ray_size  = k_avg_ray_size / (2*k_max_width+1);
  for (int i= best_ridx-k_max_width; i <= best_ridx+k_max_width;i++){
    k_avg_ray_variance += pow(ranges[get_ind(i,nrays)]-k_avg_ray_size,2);
  }
  k_avg_ray_variance = k_avg_ray_variance / (2*k_max_width+1);

  if(k_avg_ray_variance > 0.1){
    /*if(self->keep_close !=1){ //this is a new keep close - send speech message
      publish_speech_following_msg("KEEPING_CLOSE", self);
      }*/
    //self->keep_close = 1;
    last_small_gap_time = utime/1e6;
    small_gap_robot_pos[0] = self->robot_pos[0];
    small_gap_robot_pos[1] = self->robot_pos[1];
  }
  else{
    trav_dist = sqrt(pow(small_gap_robot_pos[0]-self->robot_pos[0],2) 
			    + pow(small_gap_robot_pos[1]-self->robot_pos[1],2));
    //if(utime/1e6 - last_small_gap_time > 30){//release the small gap hold only after 30s time
    if(trav_dist > 2.5){
      /*if(self->keep_close ==1){ //this is a new keep back - send speech message
	publish_speech_following_msg("KEEPING_BACK", self);
	}*/
      keep_close_msg = 0;
      self->keep_close = 0;
      
    }
  }

  if(self->draw_obs){//drawing the scores
    lcmgl = self->lcmgl_scores;
    if(lcmgl){
      for (int ridx=0;ridx<nrays;ridx++) {
	double score_g[3] = {.0,.0,.0};
	local_to_global(xys[ridx][0], xys[ridx][1],&score_g[0], &score_g[1],self);      
	char score_info[512];
	sprintf(score_info,"%.3f-%.3f", scores[ridx],avg_scores[ridx]);
	lcmglColor3f(1, 0, 0); 
	bot_lcmgl_text(lcmgl, score_g, score_info);
      }
    }
    bot_lcmgl_switch_buffer (lcmgl);
  }
  fprintf(stderr,"Best Score : %d,  %f\n", best_ridx, best_score);

  // ---------- Checking rotation crietrian ------------ //
  int draw_rotation_p = 0;

  double used_rot_radius;

  if(!self->keep_close){
    used_rot_radius = ROT_RADIUS;
  }
  else{
    used_rot_radius = C_ROT_RADIUS;
  }
  int collision = 0;
  
  if(act_dist < used_rot_radius && self->keep_close){//if we are within this - then we might not need to check the best ray -
    //we would like to have the system turned towards the user 
    fprintf(stderr,"\t+++  Within Personal Space\n");
    collision = check_rot_collision(points, nchannels, rpoints, nrchannels);
    //fprintf(stderr," ------ Collision : %d, Distnce : %f\n", collision, act_dist);
    draw_rotation_p = 1;
    if(!collision){
      *turn_in_place = TRUE;
      //changed to just turn to the best ray 
      self->turn_in_place = 1;
    }
    else{      
      //fprintf(stderr," ------ Rot Collision Detected, Distnce : %f\n", act_dist);
      *turn_in_place = FALSE; 
      //self->turn_in_place = 0;
    }
  }


  if(draw_rotation_p){
    lcmgl = self->lcmgl_rot;
    if(lcmgl){
      if(*turn_in_place){
	lcmglColor3f(0, 1, 0); 
      }
      else{
	lcmglColor3f(1, 0, 0); 
      }
      double rad = sqrt(pow(ROBOT_LENGTH/2,2)+ pow(ROBOT_WIDTH/2,2)) + MARGIN;
      double gx = .0, gy = .0;
      local_to_global(pos[0], pos[1],&gx, &gy,self);
      double xyz[3] = { gx, gy, 0 };
      lcmglCircle(xyz, rad);
      lcmglColor3f(1, 0, 0.5); 
      lcmglCircle(xyz,used_rot_radius);
      bot_lcmgl_switch_buffer (lcmgl);
    }    
  }
  else{
    lcmgl = self->lcmgl_rot;
    if(lcmgl)
      bot_lcmgl_switch_buffer (lcmgl);
  }

  best_dtheta = bot_mod2pi(thetas[best_ridx] - bot_heading); 
  //if(act_dist < 0.25 && self->use_theta == TRUE){
  //if(act_dist < 0.3 && self->use_theta == TRUE){
  if(self->started_goal_rotate){
    //pick the goal heading 
    best_dtheta = heading_at_goal; 
    goal_heading = heading_at_goal;
  }


  //the larger this is the more we should rotate - to get there quicker
  best_range = ranges[best_ridx];  //higher this is the faster we can move forward  

  lcmgl = self->lcmgl_navigator;

  /* draw the rays */
  if (lcmgl) {
    /* draw rays in light blue */
    double robot_gx = .0, robot_gy = .0;
    local_to_global(pos[0], pos[1],&robot_gx, &robot_gy,self);
    
    for (int ridx = 0; ridx < nrays; ridx++) {
      if (ridx == best_ridx) {
	lcmglLineWidth (3);
	lcmglColor3f(1, .5, .5);
      }
      else {
	lcmglLineWidth (1);
	lcmglColor3f(.3, .3, 1);
      }
      lcmglBegin(GL_LINES);

      double p_gx = .0, p_gy = .0;
      local_to_global(xys[ridx][0], xys[ridx][1] ,&p_gx, &p_gy,self);
      lcmglVertex3d(robot_gx, robot_gy, pos[2]);
      lcmglVertex3d(p_gx, p_gy, pos[2]);
      lcmglEnd();
    }
    lcmglColor3f(1, .5, .5);
    lcmglPointSize(8);
    lcmglBegin(GL_POINTS);

    double p_gx = .0, p_gy = .0;
    local_to_global(xys[best_ridx][0], xys[best_ridx][1] ,&p_gx, &p_gy,self);
    lcmglVertex3d(p_gx, p_gy, pos[2]);
    lcmglEnd();

    /* draw target direction as a yellow ray */
    lcmglColor3f(1, 1, 0);
    lcmglLineWidth(1);
    lcmglBegin(GL_LINES);

    lcmglVertex3d(robot_gx, robot_gy, pos[2]);
    local_to_global(goal_pt[0],goal_pt[1],&p_gx, &p_gy,self);
    lcmglVertex3d(p_gx, p_gy, pos[2]);
    lcmglEnd();

    bot_lcmgl_switch_buffer (lcmgl);
  } 
  
  if (angle)
    *angle = best_dtheta;

  if (brange)
    *brange = best_range;

  //we need to compute the gap between the best_ray and the goal_heading and not move if we 
  double angle_best_goal = best_dtheta; //bot_mod2pi(thetas[best_ridx] - goal_heading); 
  double angle_goal_robot = bot_mod2pi(goal_heading - bot_heading);   

    //wont try to turn in place if the angle is too small - prevents the wheelchair trying to move but failing
  
  //if(best_dtheta <= bot_to_radians(5.0)){ 
  /*if(best_dtheta <= bot_to_radians(5.0)){ 
    self->turn_in_place = 0;
    }*/
  if(angle_goal_robot <= bot_to_radians(15.0)){ 
    self->turn_in_place = 0;
  }

  
  /* turn in place */  
  int draw_rotation = 0;

  fprintf(stderr,"Angle Goal - to Robot : %f\n", angle_goal_robot/M_PI*180);
  
  if((act_dist >= used_rot_radius) && ((fabs(angle_goal_robot) > bot_to_radians (TURN_IN_PLACE_DEG)) 
				       || self->turn_in_place)){ 
    if(self->turn_in_place){
      fprintf(stderr,"Bacause of Prev Rot");
    }
    int collision = check_rot_collision(points, nchannels, rpoints, nrchannels);
    fprintf(stderr," ------ Collision : %d, Distnce : %f\n", collision, act_dist);
    draw_rotation = 1;
    if(!collision){
      *turn_in_place = TRUE;
      fprintf(stderr,"\t=+=+=+= Rotating In-Place =+=+=+=\n");
    }
    else{      
      *turn_in_place = FALSE;
      self->turn_in_place = 0;
    }
  }

  if(draw_rotation){
    lcmgl = self->lcmgl_rot;
    if(lcmgl){
      if(*turn_in_place){
	lcmglColor3f(0, 1, 0); 
      }
      else{
	lcmglColor3f(1, 0, 0); 
      }
      double rad = sqrt(pow(ROBOT_LENGTH/2,2)+ pow(ROBOT_WIDTH/2,2)) + MARGIN;
      double robot_gx = .0, robot_gy = .0;
      local_to_global(pos[0], pos[1],&robot_gx, &robot_gy,self);
      
      double xyz[3] = { robot_gx, robot_gy, 0 };
      lcmglCircle(xyz, rad);
      bot_lcmgl_switch_buffer (lcmgl);
    }
  }
  if (self->verbose) {
    double secs = g_timer_elapsed (timer, NULL);
    printf ("elapsed time: %.3f secs. (comp. rate: %.1f Hz)\n", secs, 1.0/secs);
  }

  /* free points */
  for (int ridx=0;ridx<nchannels;ridx++) {
    if (points[ridx])
      pointlist2d_free (points[ridx]);
  }
  free (points);

  /* free channel copy */
  laser_returns_destroy (channels);


  
  //if the wheelchair is facing the opposite way and there is no space to turn, the wheelchair needs to move forward 
  //and then do a turn 
  
  //also it might be better to have the wheelchair move forward and then do a turn if it can't turn in place

  //prevents the wheelchair from turning away from us because the gap is too small

  //----Original Too-Small gap detector-----//
  /*double small_gap_thresh = 90.0;
  if((fabs(angle_best_goal) > bot_to_radians(small_gap_thresh)) && (fabs(angle_goal_robot) < bot_to_radians(small_gap_thresh))){  //issue possible for it to try to turn to some side 
    // *****************reduce this a bit more - or find a way to detect an obstrcted path using the navigator - and then wait - either for an all clear signal (e.g. Xs of 
    //clear path 
    
    self->too_small_gap = 1;
  }  
  else{
    self->too_small_gap = 0;
    }*/

  double small_gap_thresh = 60.0;
  if((fabs(angle_best_goal) > bot_to_radians(small_gap_thresh)) && (fabs(angle_goal_robot) < bot_to_radians(small_gap_thresh))){  //issue possible for it to try to turn to some side 
    //*****************reduce this a bit more - or find a way to detect an obstrcted path using the navigator - and then wait - either for an all clear signal (e.g. Xs of 
    //clear path 
    fprintf(stderr,"+++++++ Too Small gap detected - Pausing\n");
    self->too_small_gap = 1;
    if(((double)utime - pause_utime)/1e6 > 120){
      pause_utime = utime;
      //publish waiting message
      //publish_speech_following_msg("WAITING", self);
    }
  }  
  else{
    self->too_small_gap = 0;
  }

  double used_tv_max;
  double used_rv_max;

  /*if(!self->keep_close){
    used_tv_max = MAX_TRANSLATION_SPEED;
    used_rv_max = MAX_ROTATION_SPEED;
  }
  else{
    used_tv_max = C_MAX_TRANSLATION_SPEED;
    used_rv_max = C_MAX_ROTATION_SPEED;
    }*/

  /*double drop_off_dist = 0.5; //was 1.0 need to check how this impacts *********
  if(self->keep_close){
    drop_off_dist = 1.5;
  }
  double used_dist;
  if(self->keep_close){
    used_dist = fmin(act_dist - used_rot_radius, drop_off_dist);
  }
  else{
    used_dist = fmin(act_dist - used_rot_radius + 0.2, drop_off_dist);
  }

  
  used_dist = fmax(.0, used_dist);
  //double used_dist = fmin(act_dist, drop_off_dist);
  
  fprintf(stderr,"Used Rot Radius : %f, Dist_from_rot : %f, Used Dist : %f\n", drop_off_dist, act_dist - used_rot_radius, used_dist);

  //this needs to change based on the keep close - needs to drop off to zero someone inside the rotate_in_place boundry
  //also needs to provide smooth acceleration and decelleration
  
  used_tv_max = MAX_TRANSLATION_SPEED; //used_dist/drop_off_dist * MAX_TRANSLATION_SPEED;
  used_rv_max = MAX_ROTATION_SPEED; //
  double max_allowed_tv;
  double max_allowed_rv; 
  if(act_dist > 1.5){
    max_allowed_tv = used_dist/drop_off_dist * MAX_TRANSLATION_SPEED;
    max_allowed_rv = used_dist/drop_off_dist * MAX_ROTATION_SPEED;  
  }
  else{
  max_allowed_tv = used_dist/drop_off_dist * C_MAX_TRANSLATION_SPEED;
    max_allowed_rv = used_dist/drop_off_dist * C_MAX_ROTATION_SPEED;  
    }*/

  double max_allowed_tv;
  double max_allowed_rv; 
  
  used_tv_max = MAX_TRANSLATION_SPEED; //used_dist/drop_off_dist * MAX_TRANSLATION_SPEED;
  used_rv_max = MAX_ROTATION_SPEED; 
  if(!use_slowdown){
    max_allowed_tv = used_tv_max;
    max_allowed_rv = used_rv_max;
  }
  else{
    double drop_off_dist = 1.0;
    double used_dist = fmin(act_dist , drop_off_dist);
    max_allowed_tv = used_dist/drop_off_dist * MAX_TRANSLATION_SPEED;
    //max_allowed_rv = used_dist/drop_off_dist * MAX_ROTATION_SPEED; 
  }
  // **************** Calculating tv and rv ****************** //

  //if within rot_radius and cant turn in place we do nothing
  if(((*dist_to_target < used_rot_radius) && (!*turn_in_place)) || collision_det){  
    fprintf(stderr,"Within Personal Space and cant turn - doing nothing\n");
    *rv = 0;
    *tv = 0;
  }
  //else if(self->collision_det){
  else if(collision_det){
    fprintf(stderr,"Collision Detected\n");
    *rv = 0;
    *tv = 0;
    //publish_speech_following_msg("TOO_CLOSE", self);
  }
  else if(self->too_small_gap){
    fprintf(stderr,"Small Gap Detected - Waiting till we have a clear path\n");
    //publish_speech_following_msg("SMALL_GAP", self);
    //update_following_state_new(STOP,self); //stopping the wheelchair 
    //update_following_state_new(PAUSE,self);
    *rv = 0;
    *tv = 0;
  }
  else{  //check if there is a person in our intended path 
    int person_collision = 0;//check_person_collision(self->person_msg, best_dtheta);
    
    if(person_collision){//stop wheelchair till the person is clear 
      fprintf(stderr,"Someone has crossed our path - Stopping till person moves");
      *rv = 0;
      *tv = 0;
    }
    else{
      fprintf(stderr, "Angle : %f Best to Robot : %f\n", *angle, angle_goal_robot);
      
      /*double used_theta = 0.0;
      if (*angle < 0){
	used_theta = fmax(*angle*1.5, -bot_to_radians (CLAMP_THETA));
      }else{
	used_theta = fmin(*angle*1.5, bot_to_radians (CLAMP_THETA));
      }
    
      fprintf(stderr,"Used theta : %f \n", 180*used_theta/M_PI);
      
      *rv = fmin(used_theta / bot_to_radians (60.0), 1.0) * MAX_ROTATION_SPEED;
      */
      *rv = get_rot_vel(*angle);

      double dt = 0;
      if (*turn_in_place)
	*tv = .0;
      else if(self->started_goal_rotate){
	*tv = 0;
      }
      else { 
	fprintf(stderr,"+++++++++++++++++++Caluclating Velocity\n");

	/*double avg_best_gap = 0;
	int temp_count = 0;
	int lr = 2; //not sure what the best value to use is

	for (int i= best_ridx-lr; i <= best_ridx+lr ;i++){
	  temp_count++;
	  avg_best_gap += ranges[get_ind(i,nrays)];
	}
	avg_best_gap = avg_best_gap / temp_count;

	//front gap - this is also relavent 
	double avg_front_gap = 0;
	temp_count = 0;
	  
	for (int i= index_0-lr; i <= index_0+lr ;i++){
	  temp_count++;
	  avg_front_gap += ranges[get_ind(i,nrays)];
	}
	avg_front_gap = avg_front_gap / temp_count; 

	double avg_free_dist = (avg_front_gap + avg_best_gap)/2 ;//lookahead_distance; 
	double goal_pos[2] = {avg_free_dist * cos(best_dtheta), avg_free_dist * sin(best_dtheta)}; 

	if(goal_pos[1] == 0){
	  //this means straight motion 
	  *tv = used_tv_max * fmin(avg_free_dist / 2.0, 1.0); 
	}  
	else{
	  //double r = pow(lookahead_distance,2)/ (2*goal_pos[1]);
	  double r = pow(avg_free_dist,2)/ (2*goal_pos[1]);

	  fprintf(stderr, " Goal : %f,%f : R : %f rv : %f Front : %f Best : %f\n", goal_pos[0], goal_pos[1], r, *rv, avg_best_gap, avg_front_gap);
	  
	  double safe_tv =  used_tv_max * fmin(avg_free_dist / 2.0, 1.0);

	  *tv = fmax(0, fmin(r * (*rv), safe_tv )); 
	  
	  *rv = *tv / r; 

	  draw_arc(self, *tv, *rv);
	}
	*/

	double avg_best_gap = 0;
	int temp_count = 0;
	int lr = 2; //not sure what the best value to use is

	for (int i= best_ridx-lr; i <= best_ridx+lr ;i++){
	  temp_count++;
	  avg_best_gap += ranges[get_ind(i,nrays)];
	}
	avg_best_gap = avg_best_gap / temp_count;

	//front gap - this is also relavent 
	double avg_front_gap = 0;
	temp_count = 0;
	  
	for (int i= index_0-lr; i <= index_0+lr ;i++){
	  temp_count++;
	  avg_front_gap += ranges[get_ind(i,nrays)];
	}
	avg_front_gap = avg_front_gap / temp_count; 
		
	*tv = get_trans_vel_arc_with_obs_fixed_moderated(self,best_dtheta, avg_best_gap, avg_front_gap,act_dist, act_angle, rv, max_allowed_tv, min_side_dist);
      }
    }
  }
  *tv = fmin(max_allowed_tv, *tv);
  *rv = fmin(max_allowed_rv, *rv);

  
  //average the tv and rv if there is a big rise or fall
  //smoothning threshold
  double smooth_thresh = 0.3; //used to be 0.5
  
  if( (prev_tv == .0) || fabs((*tv - prev_tv)/prev_tv) > smooth_thresh){//if there is a 50% or more change in the tv - average
    *tv = (*tv + prev_tv)/2;
    fprintf(stderr,"Smoothening tv Prev : %f, Current : %f\n", prev_tv, *tv);
  }

  if( (prev_rv == .0) || fabs((*rv - prev_rv)/prev_rv) > smooth_thresh){//if there is a 50% or more change in the rv - average
    *rv = (*rv + prev_rv)/2;
    fprintf(stderr,"Smoothening rv\n");
  }

  prev_rv = *rv;
  prev_tv = *tv;
  
  lcmgl = self->lcmgl_navigator_info;
  if(lcmgl){
    int rot_rad = 0;
    if(act_dist < used_rot_radius){
      rot_rad =1;
    }
    double g_robot[3] = {.0,.0,.0};
    local_to_global(pos[0]+5.0, pos[1]+5.0,&g_robot[0], &g_robot[1],self);
    char seg_info[512];
    if(!self->too_small_gap){
      /*sprintf(seg_info,"Dist to P: %f Max TV : %f, Max RV :%f\n G-V: %f K-V: %f Small Gap: %d \nKeep Close: %d Trav Dist : %f Side Dist: %f\n Within Rot:%d  \nT:%d, C:%d, RC:%d, V:(%f,%f)", 
	      act_dist, used_tv_max, used_rv_max, avg_ray_variance, k_avg_ray_variance, self->small_gap, self->keep_close, trav_dist,min_side_dist,
	      rot_rad,  *turn_in_place, proximity, collision, *tv,*rv);*/
      sprintf(seg_info,"Total wp : %d => Visible wp : %d \ntv:%f\trv:%f\tTurn-in-place:%d\n", 
	total_wp, total_vis_wp, *tv,*rv, *turn_in_place);
      
    }
    else{
      sprintf(seg_info,"Too Small gap - Pausing\n");
    }
    fprintf(stderr,"Status : %s\n", seg_info);
    lcmglColor3f(1, 0, 0); 
    bot_lcmgl_text(lcmgl, g_robot, seg_info);
  }
  bot_lcmgl_switch_buffer (lcmgl);

  free(wp_dist);
  free(wp_visibility);
  free(wp_heading);
  return 0;
}

/* update controller commands
 */
int compute_lookahead_angle_average_old(int64_t utime, State *self, double lookahead_distance, double *angle, gboolean *turn_in_place, 
				    double *brange, double *dist_to_target, double *rv, double *tv)
{    
  static int stuck_msg_sent = 0;
  static int64_t stuck_utime = 0;
  static int64_t pause_utime = 0;
  int use_slowdown = 0;
  bot_lcmgl_t *lcmgl;
  fprintf(stderr,"---------------------------------------------------------\n");
  fprintf(stderr,"Goal Command Go to heading= %d\n", self->use_theta);
  static double prev_rv = 0.0, prev_tv = 0.0;
  double pos[3] = {.0, .0, .0};
  double bot_heading = 0.0;
  static int keep_close_msg = 0;

  static double last_small_gap_time = .0;
  static double small_gap_robot_pos[2] = {.0,.0};

  int collision_det = 0;
 
  double max_range = lookahead_distance;

  GQueue *channels = laser_returns_copy (self->channels);
  GTimer *timer = g_timer_new ();

  /* convert laser returns into point list */
  int nchannels=0;
  pointlist2d_t **points = laser_to_local_points (self, channels, &nchannels);

  //these contain the raw unflitered laser returns - for the front laser (contains the feet observations) 
  int nrchannels=0;
  pointlist2d_t **rpoints = NULL;

  if (!points) {
    fprintf (stderr, "failed to convert laser returns.\n");
    g_timer_destroy (timer);
    return -1;
  }

  /* draw the obstacles */
  lcmgl = self->lcmgl_obs;

  if (self->draw_obs && lcmgl) {
    lcmglPointSize(4);
    lcmglColor3f(0.5, 0, 0.0);
    //Drawing the obstacles
    for (int lidx = 0; lidx < nchannels; lidx++) {
      pointlist2d_t *line = points[lidx];
      lcmglBegin(GL_POINTS);
      double gx = .0, gy = .0;
      for (int pidx = 0; pidx < line->npoints; pidx++) {
	local_to_global(line->points[pidx].x, line->points[pidx].y,&gx, &gy,self);
	lcmglVertex3d(gx,gy, pos[2]);
      }
      lcmglEnd();
      lcmglColor3f(0.5, 0, 0.0);
      for (int pidx = 0; pidx < line->npoints; pidx++) {
	local_to_global(line->points[pidx].x, line->points[pidx].y,&gx, &gy,self);
	double xyz[3] = { gx, gy , 0 };
	lcmglCircle(xyz, SAFETY_RADIUS);
      }
    }
    bot_lcmgl_switch_buffer (lcmgl);
  }

  double min_side_dist = 0;
  // --------- check for collisions ---------------- //

  //maybe this can all be done once - rot and direct
  int proximity = check_collision(points, nchannels, rpoints, nrchannels,&min_side_dist);

  if(proximity){//possible collision - might be good to just publish 0 velocities 
    //update_following_state_new(STOP,self);
    //self->collision_det = 1;
    if(stuck_msg_sent==0 && ((double)(utime - stuck_utime))/1e6 > 60){
      publish_speech_following_msg("TOO_CLOSE", self);
      stuck_msg_sent =1;
      stuck_utime = utime;
    }
    collision_det = 1;
    fprintf(stderr,"============ Emergency Stop ============== \n");
    /* free points */
    for (int ridx=0;ridx<nchannels;ridx++) {
      if (points[ridx])
	pointlist2d_free (points[ridx]);
    }
    free (points);
    
    /* free channel copy */
    laser_returns_destroy (channels);

    lcmgl = self->lcmgl_navigator_info;
    if(lcmgl){
      double g_robot[3] = {.0,.0,.0};
      local_to_global(pos[0]+5.0, pos[1]+5.0,&g_robot[0], &g_robot[1],self);
      char seg_info[512];
      sprintf(seg_info,"Emergency Stop - Direct Collision Detected\n");
      fprintf(stderr,"Status : %s\n", seg_info);
      lcmglColor3f(1, 0, 0); 
      bot_lcmgl_text(lcmgl, g_robot, seg_info);
    }
    bot_lcmgl_switch_buffer (lcmgl);

    return 1; //collision - emergency stop 
  }
  else{
    stuck_msg_sent = 0;
  }

  // ---------------- Do scoring ---------------- //
  
  //span to check around the wheelchair
  double span = bot_to_radians (180.0); //360 seems like over kill 
  //No of buckets 
  double dtheta = bot_to_radians (2.5);//bucket size
  int nrays = 2*span/dtheta;
  double theta0 = -span;
  double thetas[nrays];
  double ranges[nrays];
  double f_ranges[nrays];
  double xys[nrays][2];

  double closest_pt_dist = INFINITY;
  point2d_t robot_pt = { pos[0], pos[1] };

  for (int lidx = 0; lidx < nchannels; lidx++) {
    pointlist2d_t *pts = points[lidx];
    for(int i=0; i<pts->npoints; i++) {
      double d = geom_point_point_distance_2d(&robot_pt, &pts->points[i]);
      closest_pt_dist = fmin(d, closest_pt_dist);
    }
  }
  double safety_radius;

  safety_radius = fmin(closest_pt_dist - 0.01, SAFETY_RADIUS);  

  for (int ridx=0;ridx<nrays;ridx++) {  
    double theta = theta0 + ridx * dtheta;
    thetas[ridx] = theta;

    double s = sin (theta);
    double c = cos (theta);

    point2d_t ray_start = { pos[0], pos[1]};  //robot position
    point2d_t ray_dir = { c, s };

    double dist = max_range;
    double long_dist = LONG_LOOKAHEAD_DISTANCE;

    //get this from the config file 
    double alpha = atan2(0.6, 1.0);
    int poss_collision = 0;

    for (int lidx = 0; lidx < nchannels; lidx++) {
      pointlist2d_t *pts = points[lidx];
      for(int i=0; i<pts->npoints; i++) {
	double t = 0.0;
	//if the ray can lead to a collision find the distance of the colliding point (t)
	if(geom_ray_circle_intersect_2d(&ray_start, &ray_dir, 
					&pts->points[i], safety_radius, &t, NULL)) {
	  dist = fmin(dist, t);  //this finds the closest colliding point - for each rays 
	  //dist can never be more than the lookahead distance
	}
	if(geom_ray_rect_intersect_2d(&ray_start, &ray_dir, 
				      &pts->points[i], 1.0, 0.6, alpha, theta)){
	  //fprintf(stderr,"Collision From rotation\n");
	  //possible to skip this whole calculation
	  poss_collision = 1;
	  dist = 0;
	  //break;
	} 

	//might want to do away with this one - unless we need it
	if(geom_ray_circle_intersect_2d(&ray_start, &ray_dir, 
					&pts->points[i], 0.01, &t, NULL)) {
	  long_dist = fmin(long_dist,t);
	  //dist can never be more than the lookahead distance
	}
	
	
      }
    }
    ranges[ridx] = dist;
    f_ranges[ridx] = long_dist;
    xys[ridx][0] = pos[0] + c * dist;
    xys[ridx][1] = pos[1] + s * dist;
  }

  //check the reachability of the waypoints

  lcmgl = self->lcmgl_waypoints;
  int *wp_visibility = (int *)malloc(sizeof(int)*self->path->num_points);
  memset(wp_visibility,0,sizeof(int)*self->path->num_points);
  double *wp_dist = (double *)malloc(sizeof(double)*self->path->num_points);
  memset(wp_dist,0,sizeof(double)*self->path->num_points);
  double *wp_heading = (double *)malloc(sizeof(double)*self->path->num_points);
  memset(wp_heading,0,sizeof(double)*self->path->num_points);
  

  if(lcmgl){
    fprintf(stderr,"Drawing the path points : %d\n", self->path->num_points);
    for(int i=0; i < self->path->num_points; i++){
      double t_xyz[3] = {self->path->points[i].x, self->path->points[i].y, 0.2};
      double t_theta = self->path->points[i].yaw;
      double t_dx =  self->path->points[i].x-self->robot_pos[0];
      double t_dy =  self->path->points[i].y-self->robot_pos[1];
  
      double t_local[2] = {.0,.0};

      t_local[0] = t_dx*cos(self->robot_heading) + t_dy * sin(self->robot_heading);
      t_local[1] = -t_dx*sin(self->robot_heading) + t_dy * cos(self->robot_heading);
      lcmglColor3f(0.5, 0.5, 0.5);
      double t_dist = hypot(t_local[0], t_local[1]);
      double waypoint_heading = atan2(t_local[1], t_local[0]);	
      wp_dist[i] = t_dist;
      wp_heading[i] = waypoint_heading;
      if( t_dist < LONG_LOOKAHEAD_DISTANCE){	
	int angle_ind = (int)((waypoint_heading - theta0)/dtheta);
	//ideally we should check if the wp should be visible based on the map 
	if(t_dist - f_ranges[angle_ind] > 0.2){
	   wp_visibility[i] = 0;
	}
	else{
	   wp_visibility[i] = 1;
	}
      }
      if(t_dist < 2.0){
	if(wp_visibility[i] ==0){
	  //not reachable - draw in red
	  lcmglColor3f(0.5, 0.0, 0.0);
	}
	else{//reachable
	  lcmglColor3f(0.0, 0.0,1.0);
	}
      }
      else{
	lcmglColor3f(0.5, 0.5, 0.5);
      }
      if(i == self->path->num_points -1){
	lcmglColor3f(0.0, 1.0,1.0);
      }
      
      lcmglPointSize(3);	
      lcmglCircle(t_xyz, 0.2);
      //draw the heading
      lcmglPointSize(5);
      lcmglColor3f(0.0, 1.0, 1.0);
      lcmglBegin(GL_LINES);
      lcmglVertex3d(t_xyz[0],t_xyz[1],t_xyz[2]);
      lcmglVertex3d(t_xyz[0] + 0.5 *cos(t_theta) , t_xyz[1] + 0.5 *sin(t_theta), t_xyz[2]);
      lcmglEnd();      
    }
  }
  bot_lcmgl_switch_buffer (lcmgl);

  //if the closest waypoint is too close to the wheelchair pick the next one 
  double dist_to_wp = 0;
  
  int goal_ind = -1;
  int farthest_visible_ind = -1;
  if(self->use_waypoint && self->have_nav_waypoint){
    for(int i=0; i < self->path->num_points; i++){
      //match the waypoint 
      if((self->nav_waypoint[0] == self->path->points[i].x && self->nav_waypoint[1] == self->path->points[i].y) 
	 && self->nav_waypoint[2] == self->path->points[i].yaw){
	fprintf(stderr,"Found Goal waypoint\n");
	goal_ind = i;
	break;
      }
    }
  }
  else{
    for(int i=0; i < self->path->num_points; i++){
      dist_to_wp = hypot(self->path->points[i].x-self->robot_pos[0],self->path->points[i].y-self->robot_pos[1]);
      fprintf(stderr,"Dist to waypoint : %d => %f Visi: %d\n", i, dist_to_wp, wp_visibility[i]);
      if(wp_heading[i] > M_PI/2 || wp_heading[i] < - M_PI/2){
	fprintf(stderr,"Wp [%d] is behind the robot : %f\n", i, 180*wp_heading[i]/M_PI);
      }  
    
      //if(wp_visibility[i]){
      farthest_visible_ind = i;
	//}
      if(dist_to_wp > 1.0){// && wp_visibility[i]==1){
	goal_ind = i;
	fprintf(stderr,"Valid goal found\n");
	break;
      }
    }
  }

  double reach_thresh = 2.0;
  int total_wp = 0;
  int total_vis_wp = 0;
  for(int i=0; i < self->path->num_points; i++){
    //check how many wp that are within x m's of the robot are not reachable
    if(wp_dist[i] < reach_thresh){
      total_wp++;
      if(wp_visibility[i]){
	total_vis_wp++;
      }
    }
  }
  fprintf(stderr,"Total wp : %d => Visible wp : %d\n", total_wp, total_vis_wp);

  if(goal_ind <0){
    fprintf(stderr,"No waypoint found far as 1m\n");
    fprintf(stderr,"Farthest Visible Ind : %d\n", farthest_visible_ind);
    if(farthest_visible_ind <0){
      //issue - there was no valid goal - prob should pause and see if things change 
      goal_ind = 0;
    }
    else{//this might mean that we are close to the goal - if this is so - then we should move towards it
      goal_ind = farthest_visible_ind;
    }
  }
  if(goal_ind == self->path->num_points -1){
    fprintf(stderr,"----Current Goal is the final goal - slowing down\n");
    use_slowdown = 1;
  }
  

  double dx =  self->path->points[goal_ind].x-self->robot_pos[0];
  double dy =  self->path->points[goal_ind].y-self->robot_pos[1];
  
  double person[2] = {.0,.0};

  person[0] = dx*cos(self->robot_heading) + dy * sin(self->robot_heading);
  person[1] = -dx*sin(self->robot_heading) + dy * cos(self->robot_heading);
  
  fprintf(stderr,"(%f,%f) =>Local Goal : %f,%f\n", dx, dy, person[0], person[1]);

  double g_goal[3] = {.0,.0,.0};
  local_to_global(person[0], person[1],&g_goal[0], &g_goal[1],self);

  lcmgl = self->lcmgl_goal;
  if(lcmgl){
    double t_xyz[3] = {self->path->points[goal_ind].x, self->path->points[goal_ind].y, 0.2};
    lcmglPointSize(5);
    lcmglColor3f(1.0, 0.0, 0.0);
    lcmglCircle(t_xyz, 0.1);
    //lcmglColor3f(0.5, 0, 0.5);
    //lcmglCircle(g_goal, 0.2);
  }
  bot_lcmgl_switch_buffer (lcmgl);

  //double person[2] = {self->path->points[0].x-self->robot_pos[0],self->path->points[0].y-self->robot_pos[1]};

  double act_dist = hypot(person[0],person[1]); 
  double act_angle = atan2(person[1],person[0]); 
  double heading_at_goal = bot_mod2pi(self->path->points[goal_ind].yaw - self->robot_heading);
  *dist_to_target = act_dist;
  
  double goal_heading = act_angle;//self->goal_heading;
  double goal_pt[2] = {pos[0] + cos (goal_heading) * max_range,
		       pos[1] + sin (goal_heading) * max_range};  //can even use our actual goal if we want
  
  double dist_to_end_goal = hypot(self->end_goal[0]-self->robot_pos[0],
				  self->end_goal[1]-self->robot_pos[1]);

  fprintf(stderr,"Dist to End Goal : %f\n", dist_to_end_goal);
  fprintf(stderr,"Heading Difference : %f\n", 180*heading_at_goal/M_PI);

  if(dist_to_end_goal < 0.25){
    //if we do not care about the goal heading - this is fine
    if(self->use_theta == FALSE){
      fprintf(stderr,"-----------------Goal Reached-----------------\n");
      update_following_state_new(STOP,self);
      self->have_goal_heading = FALSE;
      self->move_permission = 0;
      //should send message 
      erlcm_navigator_status_t nav_stat_msg;
      nav_stat_msg.utime = utime;
      nav_stat_msg.goal_reached = 1;
      nav_stat_msg.goal_x = self->end_goal[0];
      nav_stat_msg.goal_y = self->end_goal[1];
      erlcm_navigator_status_t_publish(self->lcm,"WAYPOINT_NAV_STATUS", &nav_stat_msg);
    }
    else{
      self->started_goal_rotate = 1;
      fprintf(stderr,"-----------------Goal Within reach - checking Orientation-----------\n");
      if(fabs(heading_at_goal) < bot_to_radians(15.0)){	
	fprintf(stderr,"Within waypoint heading Tollerance - stopping\n");
	update_following_state_new(STOP,self);
	self->have_goal_heading = FALSE;
	self->move_permission = 0;

	erlcm_navigator_status_t nav_stat_msg;
	nav_stat_msg.utime = utime;
	nav_stat_msg.goal_reached = 1;
	nav_stat_msg.goal_x = self->end_goal[0];
	nav_stat_msg.goal_y = self->end_goal[1];
	erlcm_navigator_status_t_publish(self->lcm,"WAYPOINT_NAV_STATUS", &nav_stat_msg);
      }
    }
  }

  if(self->started_goal_rotate && fabs(heading_at_goal) < bot_to_radians(15.0)){
    fprintf(stderr,"At goal\n");
    erlcm_navigator_status_t nav_stat_msg;
    nav_stat_msg.utime = utime;
    nav_stat_msg.goal_reached = 1;
    nav_stat_msg.goal_x = self->end_goal[0];
    nav_stat_msg.goal_y = self->end_goal[1];
    erlcm_navigator_status_t_publish(self->lcm,"WAYPOINT_NAV_STATUS", &nav_stat_msg);
    update_following_state_new(STOP,self);
    self->have_goal_heading = FALSE;
    self->move_permission = 0;
  }

  /* find the best ray */
  double best_range = 0;
  double best_dtheta = HUGE;
  
  /* compute score for each ray */
  double scores[nrays];

  double alpha = 1.0;

  int closest_p_ind = -1;
  double closest_dist = 1000.0;

  for (int ridx = 0; ridx < nrays; ridx++) {

    double dist_sq = powf(xys[ridx][0] - goal_pt[0],2) +
      powf(xys[ridx][1] - goal_pt[1],2);  

    if(closest_dist > dist_sq){
      closest_p_ind = ridx;
      closest_dist = dist_sq;
    }

    double score = ranges[ridx] / (1.0 + alpha * dist_sq);   
    scores[ridx] = score;
  }
  
  /* find best direction */

  double avg_scores[nrays];
  int lb = 6;
  /*if(!self->keep_close){
    lb = 5;  //no of brackets to check around the current ray
  }
  else{
    lb = 5;//3;
    }*/
  for (int ridx=0;ridx<nrays;ridx++) {
    int temp_count = 0;
    float temp_avg_score = 0.0;
    for (int i= ridx-lb; i <= ridx+lb;i++){
      temp_count++;
      temp_avg_score += scores[get_ind(i,nrays)];
    }    
    avg_scores[ridx] = temp_avg_score/temp_count;
  }

  double best_score = -1000;
  int best_ridx = -1;
  
  for (int ridx=0;ridx<nrays;ridx++) {    
    if (best_score < avg_scores[ridx]) {//changed to take the average          
      best_ridx = ridx;
      best_score = avg_scores[ridx];      
    }
  }

  int max_width = 6;

  //scan to see how many good rays we have 
  double avg_ray_size = .0;
  double avg_ray_variance = .0;
  for (int i= best_ridx-max_width; i <= best_ridx+max_width;i++){
      avg_ray_size += ranges[get_ind(i,nrays)];
  } 
  avg_ray_size  = avg_ray_size / (2*max_width+1);
  for (int i= best_ridx-max_width; i <= best_ridx+max_width;i++){
    avg_ray_variance += pow(ranges[get_ind(i,nrays)]-avg_ray_size,2);
  }
  avg_ray_variance = avg_ray_variance / (2*max_width+1);

  if(avg_ray_variance > 0.1){
    fprintf(stderr,"Small Gap detected - Reducing the averaging\n");
    self->small_gap = 1;
    
    self->keep_close = 1;   

    last_small_gap_time = utime/1e6;
    small_gap_robot_pos[0] = self->robot_pos[0];
    small_gap_robot_pos[1] = self->robot_pos[1];
    
    //redoing the averaging with a smaller range
    lb = 2;
    for (int ridx=0;ridx<nrays;ridx++) {
      int temp_count = 0;
      float temp_avg_score = 0.0;
      for (int i= ridx-lb; i <= ridx+lb;i++){
	temp_count++;
	temp_avg_score += scores[get_ind(i,nrays)];
      }    
      avg_scores[ridx] = temp_avg_score/temp_count;
    }
    
    best_score = -1000;
    best_ridx = -1;
    
    for (int ridx=0;ridx<nrays;ridx++) {    
      if (best_score < avg_scores[ridx]) {//changed to take the average          
	best_ridx = ridx;
	best_score = avg_scores[ridx];      
      }
    }
  }
  else{
    //we remove the small gap condition once we are past the gap - but keep_close should be in effect 
    self->small_gap = 0;
    
  }

  //keep close detection
  int k_max_width = 8;

  //scan to see how many good rays we have 
  double k_avg_ray_size = .0;
  double k_avg_ray_variance = .0;
  double trav_dist = .0;
  
  for (int i= best_ridx-k_max_width; i <= best_ridx+k_max_width;i++){
      k_avg_ray_size += ranges[get_ind(i,nrays)];
  } 
  k_avg_ray_size  = k_avg_ray_size / (2*k_max_width+1);
  for (int i= best_ridx-k_max_width; i <= best_ridx+k_max_width;i++){
    k_avg_ray_variance += pow(ranges[get_ind(i,nrays)]-k_avg_ray_size,2);
  }
  k_avg_ray_variance = k_avg_ray_variance / (2*k_max_width+1);

  if(k_avg_ray_variance > 0.1){
    /*if(self->keep_close !=1){ //this is a new keep close - send speech message
      publish_speech_following_msg("KEEPING_CLOSE", self);
      }*/
    //self->keep_close = 1;
    last_small_gap_time = utime/1e6;
    small_gap_robot_pos[0] = self->robot_pos[0];
    small_gap_robot_pos[1] = self->robot_pos[1];
  }
  else{
    trav_dist = sqrt(pow(small_gap_robot_pos[0]-self->robot_pos[0],2) 
			    + pow(small_gap_robot_pos[1]-self->robot_pos[1],2));
    //if(utime/1e6 - last_small_gap_time > 30){//release the small gap hold only after 30s time
    if(trav_dist > 2.5){
      /*if(self->keep_close ==1){ //this is a new keep back - send speech message
	publish_speech_following_msg("KEEPING_BACK", self);
	}*/
      keep_close_msg = 0;
      self->keep_close = 0;
      
    }
  }

  if(self->draw_obs){//drawing the scores
    lcmgl = self->lcmgl_scores;
    if(lcmgl){
      for (int ridx=0;ridx<nrays;ridx++) {
	double score_g[3] = {.0,.0,.0};
	local_to_global(xys[ridx][0], xys[ridx][1],&score_g[0], &score_g[1],self);      
	char score_info[512];
	sprintf(score_info,"%.3f-%.3f", scores[ridx],avg_scores[ridx]);
	lcmglColor3f(1, 0, 0); 
	bot_lcmgl_text(lcmgl, score_g, score_info);
      }
    }
    bot_lcmgl_switch_buffer (lcmgl);
  }
  fprintf(stderr,"Best Score : %d,  %f\n", best_ridx, best_score);

  // ---------- Checking rotation crietrian ------------ //
  int draw_rotation_p = 0;

  double used_rot_radius;

  if(!self->keep_close){
    used_rot_radius = ROT_RADIUS;
  }
  else{
    used_rot_radius = C_ROT_RADIUS;
  }
  int collision = 0;
  
  if(act_dist < used_rot_radius && self->keep_close){//if we are within this - then we might not need to check the best ray -
    //we would like to have the system turned towards the user 
    fprintf(stderr,"\t+++  Within Personal Space\n");
    collision = check_rot_collision(points, nchannels, rpoints, nrchannels);
    //fprintf(stderr," ------ Collision : %d, Distnce : %f\n", collision, act_dist);
    draw_rotation_p = 1;
    if(!collision){
      *turn_in_place = TRUE;
      //changed to just turn to the best ray 
      self->turn_in_place = 1;
    }
    else{      
      //fprintf(stderr," ------ Rot Collision Detected, Distnce : %f\n", act_dist);
      *turn_in_place = FALSE; 
      //self->turn_in_place = 0;
    }
  }


  if(draw_rotation_p){
    lcmgl = self->lcmgl_rot;
    if(lcmgl){
      if(*turn_in_place){
	lcmglColor3f(0, 1, 0); 
      }
      else{
	lcmglColor3f(1, 0, 0); 
      }
      double rad = sqrt(pow(ROBOT_LENGTH/2,2)+ pow(ROBOT_WIDTH/2,2)) + MARGIN;
      double gx = .0, gy = .0;
      local_to_global(pos[0], pos[1],&gx, &gy,self);
      double xyz[3] = { gx, gy, 0 };
      lcmglCircle(xyz, rad);
      lcmglColor3f(1, 0, 0.5); 
      lcmglCircle(xyz,used_rot_radius);
      bot_lcmgl_switch_buffer (lcmgl);
    }    
  }
  else{
    lcmgl = self->lcmgl_rot;
    if(lcmgl)
      bot_lcmgl_switch_buffer (lcmgl);
  }

  best_dtheta = bot_mod2pi(thetas[best_ridx] - bot_heading); 
  //if(act_dist < 0.25 && self->use_theta == TRUE){
  //if(act_dist < 0.3 && self->use_theta == TRUE){
  if(self->started_goal_rotate){
    //pick the goal heading 
    best_dtheta = heading_at_goal; 
    goal_heading = heading_at_goal;
  }


  //the larger this is the more we should rotate - to get there quicker
  best_range = ranges[best_ridx];  //higher this is the faster we can move forward  

  lcmgl = self->lcmgl_navigator;

  /* draw the rays */
  if (lcmgl) {
    /* draw rays in light blue */
    double robot_gx = .0, robot_gy = .0;
    local_to_global(pos[0], pos[1],&robot_gx, &robot_gy,self);
    
    for (int ridx = 0; ridx < nrays; ridx++) {
      if (ridx == best_ridx) {
	lcmglLineWidth (3);
	lcmglColor3f(1, .5, .5);
      }
      else {
	lcmglLineWidth (1);
	lcmglColor3f(.3, .3, 1);
      }
      lcmglBegin(GL_LINES);

      double p_gx = .0, p_gy = .0;
      local_to_global(xys[ridx][0], xys[ridx][1] ,&p_gx, &p_gy,self);
      lcmglVertex3d(robot_gx, robot_gy, pos[2]);
      lcmglVertex3d(p_gx, p_gy, pos[2]);
      lcmglEnd();
    }
    lcmglColor3f(1, .5, .5);
    lcmglPointSize(8);
    lcmglBegin(GL_POINTS);

    double p_gx = .0, p_gy = .0;
    local_to_global(xys[best_ridx][0], xys[best_ridx][1] ,&p_gx, &p_gy,self);
    lcmglVertex3d(p_gx, p_gy, pos[2]);
    lcmglEnd();

    /* draw target direction as a yellow ray */
    lcmglColor3f(1, 1, 0);
    lcmglLineWidth(1);
    lcmglBegin(GL_LINES);

    lcmglVertex3d(robot_gx, robot_gy, pos[2]);
    local_to_global(goal_pt[0],goal_pt[1],&p_gx, &p_gy,self);
    lcmglVertex3d(p_gx, p_gy, pos[2]);
    lcmglEnd();

    bot_lcmgl_switch_buffer (lcmgl);
  } 
  
  if (angle)
    *angle = best_dtheta;

  if (brange)
    *brange = best_range;

  //we need to compute the gap between the best_ray and the goal_heading and not move if we 
  double angle_best_goal = best_dtheta; //bot_mod2pi(thetas[best_ridx] - goal_heading); 
  double angle_goal_robot = bot_mod2pi(goal_heading - bot_heading);   

    //wont try to turn in place if the angle is too small - prevents the wheelchair trying to move but failing
  
  //if(best_dtheta <= bot_to_radians(5.0)){ 
  /*if(best_dtheta <= bot_to_radians(5.0)){ 
    self->turn_in_place = 0;
    }*/
  if(angle_goal_robot <= bot_to_radians(15.0)){ 
    self->turn_in_place = 0;
  }

  
  /* turn in place */  
  int draw_rotation = 0;

  fprintf(stderr,"Angle Goal - to Robot : %f\n", angle_goal_robot/M_PI*180);
  
  if((act_dist >= used_rot_radius) && ((fabs(angle_goal_robot) > bot_to_radians (TURN_IN_PLACE_DEG)) 
				       || self->turn_in_place)){ 
    if(self->turn_in_place){
      fprintf(stderr,"Bacause of Prev Rot");
    }
    int collision = check_rot_collision(points, nchannels, rpoints, nrchannels);
    fprintf(stderr," ------ Collision : %d, Distnce : %f\n", collision, act_dist);
    draw_rotation = 1;
    if(!collision){
      *turn_in_place = TRUE;
      fprintf(stderr,"\t=+=+=+= Rotating In-Place =+=+=+=\n");
    }
    else{      
      *turn_in_place = FALSE;
      self->turn_in_place = 0;
    }
  }

  if(draw_rotation){
    lcmgl = self->lcmgl_rot;
    if(lcmgl){
      if(*turn_in_place){
	lcmglColor3f(0, 1, 0); 
      }
      else{
	lcmglColor3f(1, 0, 0); 
      }
      double rad = sqrt(pow(ROBOT_LENGTH/2,2)+ pow(ROBOT_WIDTH/2,2)) + MARGIN;
      double robot_gx = .0, robot_gy = .0;
      local_to_global(pos[0], pos[1],&robot_gx, &robot_gy,self);
      
      double xyz[3] = { robot_gx, robot_gy, 0 };
      lcmglCircle(xyz, rad);
      bot_lcmgl_switch_buffer (lcmgl);
    }
  }
  if (self->verbose) {
    double secs = g_timer_elapsed (timer, NULL);
    printf ("elapsed time: %.3f secs. (comp. rate: %.1f Hz)\n", secs, 1.0/secs);
  }

  /* free points */
  for (int ridx=0;ridx<nchannels;ridx++) {
    if (points[ridx])
      pointlist2d_free (points[ridx]);
  }
  free (points);

  /* free channel copy */
  laser_returns_destroy (channels);


  
  //if the wheelchair is facing the opposite way and there is no space to turn, the wheelchair needs to move forward 
  //and then do a turn 
  
  //also it might be better to have the wheelchair move forward and then do a turn if it can't turn in place

  //prevents the wheelchair from turning away from us because the gap is too small

  //----Original Too-Small gap detector-----//
  /*double small_gap_thresh = 90.0;
  if((fabs(angle_best_goal) > bot_to_radians(small_gap_thresh)) && (fabs(angle_goal_robot) < bot_to_radians(small_gap_thresh))){  //issue possible for it to try to turn to some side 
    // *****************reduce this a bit more - or find a way to detect an obstrcted path using the navigator - and then wait - either for an all clear signal (e.g. Xs of 
    //clear path 
    
    self->too_small_gap = 1;
  }  
  else{
    self->too_small_gap = 0;
    }*/

  double small_gap_thresh = 60.0;
  if((fabs(angle_best_goal) > bot_to_radians(small_gap_thresh)) && (fabs(angle_goal_robot) < bot_to_radians(small_gap_thresh))){  //issue possible for it to try to turn to some side 
    //*****************reduce this a bit more - or find a way to detect an obstrcted path using the navigator - and then wait - either for an all clear signal (e.g. Xs of 
    //clear path 
    fprintf(stderr,"+++++++ Too Small gap detected - Pausing\n");
    self->too_small_gap = 1;
    if(((double)utime - pause_utime)/1e6 > 120){
      pause_utime = utime;
      //publish waiting message
      //publish_speech_following_msg("WAITING", self);
    }
  }  
  else{
    self->too_small_gap = 0;
  }

  double used_tv_max;
  double used_rv_max;

  /*if(!self->keep_close){
    used_tv_max = MAX_TRANSLATION_SPEED;
    used_rv_max = MAX_ROTATION_SPEED;
  }
  else{
    used_tv_max = C_MAX_TRANSLATION_SPEED;
    used_rv_max = C_MAX_ROTATION_SPEED;
    }*/

  /*double drop_off_dist = 0.5; //was 1.0 need to check how this impacts *********
  if(self->keep_close){
    drop_off_dist = 1.5;
  }
  double used_dist;
  if(self->keep_close){
    used_dist = fmin(act_dist - used_rot_radius, drop_off_dist);
  }
  else{
    used_dist = fmin(act_dist - used_rot_radius + 0.2, drop_off_dist);
  }

  
  used_dist = fmax(.0, used_dist);
  //double used_dist = fmin(act_dist, drop_off_dist);
  
  fprintf(stderr,"Used Rot Radius : %f, Dist_from_rot : %f, Used Dist : %f\n", drop_off_dist, act_dist - used_rot_radius, used_dist);

  //this needs to change based on the keep close - needs to drop off to zero someone inside the rotate_in_place boundry
  //also needs to provide smooth acceleration and decelleration
  
  used_tv_max = MAX_TRANSLATION_SPEED; //used_dist/drop_off_dist * MAX_TRANSLATION_SPEED;
  used_rv_max = MAX_ROTATION_SPEED; //
  double max_allowed_tv;
  double max_allowed_rv; 
  if(act_dist > 1.5){
    max_allowed_tv = used_dist/drop_off_dist * MAX_TRANSLATION_SPEED;
    max_allowed_rv = used_dist/drop_off_dist * MAX_ROTATION_SPEED;  
  }
  else{
  max_allowed_tv = used_dist/drop_off_dist * C_MAX_TRANSLATION_SPEED;
    max_allowed_rv = used_dist/drop_off_dist * C_MAX_ROTATION_SPEED;  
    }*/

  double max_allowed_tv;
  double max_allowed_rv; 
  
  used_tv_max = MAX_TRANSLATION_SPEED; //used_dist/drop_off_dist * MAX_TRANSLATION_SPEED;
  used_rv_max = MAX_ROTATION_SPEED; 
  if(!use_slowdown){
    max_allowed_tv = used_tv_max;
    max_allowed_rv = used_rv_max;
  }
  else{
    double drop_off_dist = 1.0;
    double used_dist = fmin(act_dist , drop_off_dist);
    max_allowed_tv = used_dist/drop_off_dist * MAX_TRANSLATION_SPEED;
    //max_allowed_rv = used_dist/drop_off_dist * MAX_ROTATION_SPEED; 
  }
  // **************** Calculating tv and rv ****************** //

  //if within rot_radius and cant turn in place we do nothing
  if(((*dist_to_target < used_rot_radius) && (!*turn_in_place)) || collision_det){  
    fprintf(stderr,"Within Personal Space and cant turn - doing nothing\n");
    *rv = 0;
    *tv = 0;
  }
  //else if(self->collision_det){
  else if(collision_det){
    fprintf(stderr,"Collision Detected\n");
    *rv = 0;
    *tv = 0;
    //publish_speech_following_msg("TOO_CLOSE", self);
  }
  else if(self->too_small_gap){
    fprintf(stderr,"Small Gap Detected - Waiting till we have a clear path\n");
    //publish_speech_following_msg("SMALL_GAP", self);
    //update_following_state_new(STOP,self); //stopping the wheelchair 
    //update_following_state_new(PAUSE,self);
    *rv = 0;
    *tv = 0;
  }
  else{  //check if there is a person in our intended path 
    int person_collision = 0;//check_person_collision(self->person_msg, best_dtheta);
    
    if(person_collision){//stop wheelchair till the person is clear 
      fprintf(stderr,"Someone has crossed our path - Stopping till person moves");
      *rv = 0;
      *tv = 0;
    }
    else{
      double used_theta = 0.0;
      if (*angle < 0){
	used_theta = fmax(*angle, -bot_to_radians (CLAMP_THETA));
      }else{
	used_theta = fmin(*angle, bot_to_radians (CLAMP_THETA));
      }
    
      fprintf(stderr,"Used theta : %f \n", 180*used_theta/M_PI);
      ///***** why is this not here??? we should have a cap for the max rotating in place
      //(*turn_in_place){
      //*rv = ROTATE_IN_PLACE_V;
      //
      //se{
      //if(fabs(used_theta) < bot_to_radians(10.0)){
      //*rv = prev_rv/4;
      //}
      //else{
      *rv = fmin(used_theta / bot_to_radians (80.0), 1.0) * used_rv_max;
      //}
      //

      double dt = 0;
      if (*turn_in_place)
	*tv = .0;
      else if(self->started_goal_rotate){
	*tv = 0;
      }
      else { 
	double tv_ang = fabs(angle_goal_robot);

	if (tv_ang > bot_to_radians(TURN_IN_PLACE_DEG)){ //not sure which one i shuld use 
	  tv_ang = TV_BIASING_THETA;// - bot_to_radians(5.0);
	}
	dt = fabs (tv_ang -bot_to_radians (TV_BIASING_THETA));
	*tv = dt/bot_to_radians(TV_BIASING_THETA) * used_tv_max;
      
	double avg_best_gap = 0;
	int temp_count = 0;
	int lr = 3; //not sure what the best value to use is

	for (int i= best_ridx-lr; i <= best_ridx+lr ;i++){
	  temp_count++;
	  avg_best_gap += ranges[get_ind(i,nrays)];
	}
	avg_best_gap = avg_best_gap / temp_count;
      

	//*tv = (*tv) * best_range / max_range;
	
	if(!self->keep_close){
	  *tv = (*tv) * fmin(avg_best_gap / 1.0, 1.0);
	}
	*tv = (*tv) *  fmin(min_side_dist/ BEST_SIDE_DIST, 1.0);
	
	*tv = fmin(*tv,used_tv_max);

	//if(fabs(angle_best_goal) > bot_to_radians(100.0)){ 
	if(fabs(angle_best_goal) > bot_to_radians(90.0)){ 
	  //clamp the max speed that can be used when the wheelchair is going away from the person 
	  *tv = fmin(*tv,MAX_AWAY_TRANSLATION_SPEED);
	}      
	fprintf(stderr,"DT %f, TV : %f \n", dt, *tv);
      }
    }
  }
  *tv = fmin(max_allowed_tv, *tv);
  *rv = fmin(max_allowed_rv, *rv);

  
  //average the tv and rv if there is a big rise or fall
  //smoothning threshold
  double smooth_thresh = 0.3; //used to be 0.5
  
  if( (prev_tv == .0) || fabs((*tv - prev_tv)/prev_tv) > smooth_thresh){//if there is a 50% or more change in the tv - average
    *tv = (*tv + prev_tv)/2;
    fprintf(stderr,"Smoothening tv Prev : %f, Current : %f\n", prev_tv, *tv);
  }

  if( (prev_rv == .0) || fabs((*rv - prev_rv)/prev_rv) > smooth_thresh){//if there is a 50% or more change in the rv - average
    *rv = (*rv + prev_rv)/2;
    fprintf(stderr,"Smoothening rv\n");
  }

  prev_rv = *rv;
  prev_tv = *tv;
  
  lcmgl = self->lcmgl_navigator_info;
  if(lcmgl){
    int rot_rad = 0;
    if(act_dist < used_rot_radius){
      rot_rad =1;
    }
    double g_robot[3] = {.0,.0,.0};
    local_to_global(pos[0]+5.0, pos[1]+5.0,&g_robot[0], &g_robot[1],self);
    char seg_info[512];
    if(!self->too_small_gap){
      /*sprintf(seg_info,"Dist to P: %f Max TV : %f, Max RV :%f\n G-V: %f K-V: %f Small Gap: %d \nKeep Close: %d Trav Dist : %f Side Dist: %f\n Within Rot:%d  \nT:%d, C:%d, RC:%d, V:(%f,%f)", 
	      act_dist, used_tv_max, used_rv_max, avg_ray_variance, k_avg_ray_variance, self->small_gap, self->keep_close, trav_dist,min_side_dist,
	      rot_rad,  *turn_in_place, proximity, collision, *tv,*rv);*/
      sprintf(seg_info,"Total wp : %d => Visible wp : %d \ntv:%f\trv:%f\tTurn-in-place:%d\n", 
	total_wp, total_vis_wp, *tv,*rv, *turn_in_place);
      
    }
    else{
      sprintf(seg_info,"Too Small gap - Pausing\n");
    }
    fprintf(stderr,"Status : %s\n", seg_info);
    lcmglColor3f(1, 0, 0); 
    bot_lcmgl_text(lcmgl, g_robot, seg_info);
  }
  bot_lcmgl_switch_buffer (lcmgl);

  free(wp_dist);
  free(wp_visibility);
  free(wp_heading);
  return 0;
}

void controller_update(State *self, int64_t utime)
{  
  //fprintf(stderr,"-------------------------------------------------------------------------------\n");  
  draw_wheelchair(self);

  if(!self->tourguide_mode){ //work only when we are in the navigation mode 
    if (!self->have_goal_heading || (!self->move_permission) || (!self->playback && self->estop) || self->following_state ==IDLE || self->following_state == PAUSED){
      //should stop wheelchair - should be stopped by some other part of the code - but just in case 
      lcm_carmen3d_robot_velocity_command(0.0, 0.0,self->lcm);
      fprintf(stderr,"========== Not Following  = > Have Goal %d, EStop : %d Move Permission %d\n", self->have_goal_heading, (!self->playback && self->estop),self->move_permission );
      clear_lcmgl(self);

      bot_lcmgl_t *lcmgl = self->lcmgl_navigator_info;
      if(lcmgl){
	double g_robot[3] = {.0,.0,.0};
	local_to_global(5.0, 5.0,&g_robot[0], &g_robot[1],self);
	char seg_info[512];
	sprintf(seg_info,"Idle");
	lcmglColor3f(1, 0, 0); 
	bot_lcmgl_text(lcmgl, g_robot, seg_info);
      }
      bot_lcmgl_switch_buffer (lcmgl);
      return;    
    }
    else if((self->nav_pause==1) && self->have_goal_heading){
      fprintf(stderr,"Had a goal heading but now dont - pausing until status changes\n");
      lcm_carmen3d_robot_velocity_command(0.0, 0.0,self->lcm);
      bot_lcmgl_t *lcmgl = self->lcmgl_navigator_info;
      if(lcmgl){
	double g_robot[3] = {.0,.0,.0};
	local_to_global(5.0, 5.0,&g_robot[0], &g_robot[1],self);
	char seg_info[512];
	sprintf(seg_info,"Temp Obst - Waiting ");
	lcmglColor3f(1, 0, 0); 
	bot_lcmgl_text(lcmgl, g_robot, seg_info);
      }
      bot_lcmgl_switch_buffer (lcmgl);
    }
    
    else if (self->last_utime == 0 || fabs(utime - self->last_utime) > UPDATE_RATE * 1000000) {
      //fprintf(stderr," ----------------- Processing \n");
      double dtheta = .0;
      double best_range = .0;
      gboolean turn_in_place = FALSE;
      double dist_to_target = 0.0;
      double tv = 0.0, rv = 0.0;

      if (compute_lookahead_angle_average (utime, self, LOOKAHEAD_DISTANCE, &dtheta, 
					   &turn_in_place, &best_range, &dist_to_target, &rv, &tv) !=0){ 
	lcm_carmen3d_robot_velocity_command(0.0, 0.0,self->lcm);
	return;
      }     
      
      fprintf(stderr,"rv : %f, tv : %f Turn-In-Place : %d\n", rv, tv, turn_in_place);        

      //make sure that this doesnt go backwords
      tv = fmax(tv,0);
    
      lcm_carmen3d_robot_velocity_command(tv, rv,self->lcm);
      //fprintf(stderr,"Issued Velocity Command \n");
      self->last_utime = utime;
    }
    else{
      //fprintf(stderr,"Ignoring time step\n");
    }
  }
  
  return;
}


gboolean heartbeat_cb (gpointer data)
{
  State *s = (State*)data;
  s->estop = FALSE;
  return TRUE;
}

void read_parameters(State *self, double *fl_offset, double *rl_offset, 
		     double *fl_ang_offset, double *rl_ang_offset)
{
  BotParam *c =  self->config;
  BotFrames *frames = bot_frames_get_global (self->lcm, c);
  //char key[2048];
 
    // Find the position of the forward-facing LIDAR
    char *coord_frame;
    coord_frame = bot_param_get_planar_lidar_coord_frame (c, "SKIRT_FRONT");
    if (!coord_frame)
        fprintf (stderr, "\tError determining front laser coordinate frame\n");

    BotTrans front_laser_to_body;
    if (!bot_frames_get_trans (frames, coord_frame, "body", &front_laser_to_body))
        fprintf (stderr, "\tError determining front laser coordinate frame\n");
    else
        fprintf(stderr,"\tFront Laser Pos : (%f,%f,%f)\n",
                front_laser_to_body.trans_vec[0], front_laser_to_body.trans_vec[1], front_laser_to_body.trans_vec[2]);

    *fl_offset = front_laser_to_body.trans_vec[0];

    double front_rpy[3];
    bot_quat_to_roll_pitch_yaw (front_laser_to_body.rot_quat, front_rpy);
    *fl_ang_offset  = front_rpy[2];


    // Now for the rear laser
    coord_frame = NULL;
    coord_frame = bot_param_get_planar_lidar_coord_frame (c, "SKIRT_REAR");

    if (!coord_frame)
        fprintf (stderr, "\tError determining rear laser coordinate frame\n");

    BotTrans rear_laser_to_body;
    if (!bot_frames_get_trans (frames, coord_frame, "body", &rear_laser_to_body))
        fprintf (stderr, "\tError determining LIDAR coordinate frame\n");
    else
        fprintf(stderr,"\tRear Laser Pos : (%f,%f,%f)\n",
                rear_laser_to_body.trans_vec[0], rear_laser_to_body.trans_vec[1], rear_laser_to_body.trans_vec[2]);

    *rl_offset = rear_laser_to_body.trans_vec[0];

    double rear_rpy[3];
    bot_quat_to_roll_pitch_yaw (rear_laser_to_body.rot_quat, rear_rpy);
    *rl_ang_offset = rear_rpy[2];







  /* 
   * double position[3];
   * sprintf(key, "%s.%s.%s.position", "calibration", "planar_lidars", "SKIRT_FRONT");
   * if(3 != bot_param_get_double_array(c, key, position, 3)){
   *   fprintf(stderr,"\tError Reading Params\n");
   * }else{
   *   fprintf(stderr,"\tFront Laser Pos : (%f,%f,%f)\n",position[0], position[1], position[2]);
   * }
   * *fl_offset = position[0];
   *   
   * sprintf(key, "%s.%s.%s.position", "calibration", "planar_lidars", "SKIRT_REAR");
   * if(3 != bot_param_get_double_array(c, key, position, 3)){
   *   fprintf(stderr,"\tError Reading Params\n");
   * }else{
   *   fprintf(stderr,"\tRear Laser Pos : (%f,%f,%f)\n",position[0], position[1], position[2]);
   * }
   * *rl_offset = position[0];
   * 
   * 
   * double rpy[3];
   * sprintf(key, "%s.%s.%s.rpy", "calibration", "planar_lidars", "SKIRT_FRONT");
   * if(3 != bot_param_get_double_array(c, key, rpy, 3)){
   *   fprintf(stderr,"\tError Reading Params\n");
   * }else{
   *   fprintf(stderr,"\tFront Laser RPY : (%f,%f,%f)\n",rpy[0], rpy[1], rpy[2]);
   * }
   * *fl_ang_offset  = bot_to_radians(rpy[2]);
   * 
   * sprintf(key, "%s.%s.%s.rpy", "calibration", "planar_lidars", "SKIRT_REAR");
   * if(3 != bot_param_get_double_array(c, key, rpy, 3)){
   *   fprintf(stderr,"\tError Reading Params\n");
   * }else{
   *   fprintf(stderr,"\tRear Laser RPY : (%f,%f,%f)\n",rpy[0], rpy[1], rpy[2]);
   * }
   * *rl_ang_offset = bot_to_radians(rpy[2]);
   */
  
  /*sprintf(key, "%s.%s.%s.position", "calibration", "kinect", "KINECT_FORWARD");
  if(3 != bot_conf_get_double_array(c, key, position, 3)){
    fprintf(stderr,"\tError Reading Params\n");
  }else{
    fprintf(stderr,"\tKinect Pos : (%f,%f,%f)\n",position[0], position[1], position[2]);
  }*/
    self->kinect_front_offset = rear_laser_to_body.trans_vec[0];
  self->kinect_side_offset = rear_laser_to_body.trans_vec[1];
}

int
main (int argc, char ** argv)
{
  int c;
  State s;
  // so that redirected stdout won't be insanely buffered.
  setlinebuf(stdout);

  memset (&s, 0, sizeof (State));
  s.tourguide_mode = 1; 
  while ((c = getopt (argc, argv, "nhspvdw")) >= 0) {
    switch (c) {
    case 'v':
      s.verbose = 1;
      break;
    case 'n':
      s.tourguide_mode = 0;
      fprintf(stderr,"Initialized to navigation");
      break;
    case 'p':
      s.playback = 1;
      break;
    case 'd':
      s.draw_obs = 1;
      break;
    case 's':
      s.use_state_score = 1;
      break;
    case 'w':
      s.use_waypoint = 1;
      fprintf(stderr,"Using the given waypoint\n");
      break;
    case 'h':
    case '?':
      fprintf (stderr, "Usage: %s [-v] [-c] [-d] [-p]\n\
                        Options:\n\
                        -v     Be verbose\n\
                        -p     Playback Mode\n\
                        -d     Draw Obstacles\n\
                        -s     Use State Scores\n\
                        -h     This help message\n", argv[0]);
    return 1;
    }
  }
  //subscribing to carmen
  s.lcm = lcm_create(NULL);//globals_get_lcm();
  bot_glib_mainloop_attach_lcm (s.lcm);

  s.config = bot_param_new_from_server(s.lcm, 1);//globals_get_config();
  s.channels = g_queue_new ();
  s.nav_pause = -1;
  s.started_goal_rotate = 0;
  s.goal_heading = bot_to_radians (.0);
  s.laser_hashtable = g_hash_table_new (g_str_hash, g_str_equal);
  s.last_utime = 0;
  s.last_goal_heading_utime = 0;
  s.have_goal_heading = FALSE;
  s.estop = FALSE;
  s.collision_det = 0;
  s.state = NULL;
  s.person_msg = NULL;
  s.tracking_state = LOST;
  s.following_state = IDLE;
  s.too_far = 0;
  s.turn_in_place = 0;
  s.too_small_gap = 0;
  s.keep_close = 0;
  s.small_gap = 0;
  s.use_theta = FALSE;

  
  memset(&s.robot_pos,0, 3*sizeof(double)); //= {.0,.0,.0};
  s.robot_heading = .0;
  s.lcmgl_rot = bot_lcmgl_init (s.lcm, "ROT");
  s.lcmgl_navigator = bot_lcmgl_init (s.lcm, "NAVIGATOR");
  s.lcmgl_navigator_info = bot_lcmgl_init (s.lcm, "NAVIGATOR_INFO");
  s.lcmgl_obs = bot_lcmgl_init (s.lcm, "OBS");
  s.lcmgl_footprint = bot_lcmgl_init (s.lcm, "FOOTPRINT");
  s.lcmgl_scores = bot_lcmgl_init (s.lcm, "SCORE");
  s.lcmgl_goal = bot_lcmgl_init (s.lcm, "GOAL");
  s.lcmgl_waypoints = bot_lcmgl_init (s.lcm, "WAYPOINTS");
  s.lcmgl_arc = bot_lcmgl_init (s.lcm, "NAV_ARC");;
  s.path = NULL;
  s.kinect_msg = NULL;
  s.move_permission = 0;

  //set to actual -- ********
  //s.kinect_front_offset = 0.0;
  //s.kinect_side_offset = 0.0;

  memset(&s.end_goal,0, sizeof(double)*2);
  s.have_nav_waypoint = 0;
  memset(&s.nav_waypoint,0,sizeof(double)*3);
  read_parameters(&s,&s.frontlaser_offset, &s.rearlaser_offset, &s.front_anglular_offset, &s.rear_anglular_offset);

  //close connection to IPC once we have everything //does not need to be here but robot_interface has a lot of carmen stuff that needs to be takes out so 
  //seems to be complaining
  
  bot_core_planar_lidar_t_subscribe( s.lcm, "SKIRT_REAR", on_laser, &s );
  bot_core_planar_lidar_t_subscribe( s.lcm, "SKIRT_FRONT", on_laser, &s );
  erlcm_kinect_range_msg_t_subscribe(s.lcm, "KINECT_RANGE",on_kinect_range, &s);

  erlcm_speech_cmd_t_subscribe(s.lcm, "WAYPOINT_NAVIGATOR", speech_handler, &s);
  //erlcm_speech_cmd_t_subscribe(s.lcm, "TABLET_FOLLOWER", speech_handler, &s);
  if(s.use_waypoint){
    erlcm_waypoint_msg_t_subscribe(s.lcm,"WAYPOINT_GOAL_CMD",nav_waypoint_handler ,&s);
  }
  erlcm_point_list_t_subscribe(s.lcm, "NAV_PLAN", nav_trajectory_handler,&s);
  erlcm_nav_plan_result_t_subscribe(s.lcm, "NAV_PLAN_RESULT", nav_plan_handler,&s);
  erlcm_tagged_node_t_subscribe(s.lcm, "WHEELCHAIR_MODE", mode_handler, &s);
  bot_core_pose_t_subscribe(s.lcm,"POSE", pose_handler, &s);

  /* heart beat*/
  g_timeout_add_seconds (1, heartbeat_cb, &s);

  /* Main Loop */
  GMainLoop *mainloop = g_main_loop_new(NULL, FALSE);
  if (!mainloop) {
    fprintf(stderr,"Error: Failed to create the main loop\n");
    return -1;
  }

  bot_signal_pipe_glib_quit_on_kill (mainloop);
  /* sit and wait for messages */
  g_main_loop_run(mainloop);
  
  return 0;
}

