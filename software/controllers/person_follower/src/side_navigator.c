/* The navigator takes as input a goal heading in the robot's body frame 
 * and laser scan returns and outputs controller commands.
 *
 * author: Sachi Hemachandra
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif


#include <lcm/lcm.h>
#include <bot_core/bot_core.h>

#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_frames/bot_frames.h>
#include <lcmtypes/bot2_param.h>
#include <bot_lcmgl_client/lcmgl.h>

#include <geom_utils/geometry.h>

#include <lcmtypes/er_lcmtypes.h>
#include <interfaces/robot3d_interface.h>

#define LASER_MEMORY          10
#define LOOKAHEAD_DISTANCE     2.5//2.0
#define MAX_AWAY_TRANSLATION_SPEED 0.3

//these are prob too high as well //0.6 was used - can be a bit too slow
#define MAX_TRANSLATION_SPEED  0.8//1.0//1.5//1.5//wheelchair base limts the top speed to 1.0 m/s and PI rad/s 
#define FULL_MAX_TRANSLATION_SPEED  0.9//1.0
#define MAX_ROTATION_SPEED     1.2//1.2//2.0//2.0//5.0 is the one used in the wheelchair //1.0//.35 // in rad/sec.
#define MAX_FAST_TRANSLATION_SPEED 0.6//1.0//1.2 

#define UPDATE_RATE            0.1 //tested with 0.1 - fear that it might be a bit too slow// in seconds  -- ***maybe we might increase this 
#define SAFETY_RADIUS          0.3//0.4//0.5//0.35//0.3//.35 - old value

#define USED_SAFETY_RADIUS     0.4
#define VISIBILITY_RADIUS      0.3//0.15


#define FORWARD_SCAN_DIST      1.0
#define BEST_SIDE_DIST         0.5

#define MASK_BUCKETS         360
#define TURN_IN_PLACE_DEG     80.0//60.0//60.0//90.0 //used to be 25 
#define TV_BIASING_THETA      40.0//100.0//80.0//110.0//80.0
#define CLAMP_THETA           45.0//30.0
#define ROTATE_IN_PLACE_V     2.0  //rad per sec

#define ROBOT_WIDTH            0.62
#define ROBOT_LENGTH           0.98
#define ROBOT_RADIUS           0.62

#define SIDE_PERSON_THRESHOLD  0.1
#define FORWARD_PERSON_DIST    2.0

#define MARGIN                 0.01 //might be worth reducing this one 

#define STOP_RADIUS            0.6//1.0 - stop radius should be less - unless pretty close to hitting we should turn 
#define ROT_RADIUS             1.5 //2.0 - reduced this a bit

#define SIDE_FOLLOW_STOP_RADIUS 0.6

//parameters to use when keeping close
#define C_STOP_RADIUS          0.79 //0.6
#define C_ROT_RADIUS           0.8 //0.7
//#define C_SAFETY_RADIUS          0.3
//earlier values were too slow
#define C_MAX_TRANSLATION_SPEED  0.8//1.0
#define C_MAX_ROTATION_SPEED     1.2

#define BEAM_SKIP 3 //no of beams to skip in the laser 
#define BUCKET_SIZE            5.0 //2.5 - has a doubling effect on the processor usage - would need to test the benifit more 

//averaging at the person tracker - doing it here is not a gd idea
#define len_history 2//40//40 //not sure but this will prob fail if set to 1 
//double *heading_history;// = (double *) calloc(len_history, sizeof(double));
//double *temp_heading_history;// = (double *) calloc(len_history-1, sizeof(double));

#define MAX_JOYSTICK_TIMEOUT_USEC 200000

struct laser_data
{
    bot_core_planar_lidar_t  *msg;    
    double   m[12];
};

struct laser_channel
{
    char      name[256];
    GQueue    *data;
    int       complained;
    int8_t    mask[MASK_BUCKETS];
};

typedef struct _local_points_t{
    pointlist2d_t **points;
    pointlist2d_t **pf_points;
  
} local_points_t;

enum FollowingState{
    FOLLOWING, //person is being tracked and followed - issuing navigation commands to the navigator 
    //FOLLOWING_DIRECTION, 
    //FOLLOWING_SIDE, 
    PAUSED,  
    WAITING, //told to follow but no person estimate received - so waiting till one comes
    IDLE, //not following, commands velocity=0 (can still have a track on the person)
    OVERRIDDEN // valid only for joystick mode, subservient to joystick
};

enum FollowCommand{
    FOLLOW, 
    PAUSE,
    RESUME,
    STOP, 
    OVERRIDE,
    //GO_THROUGH_DOOR //to go through the door - halt person following and wait for 
    //door location - then travel through and wait for the person to come back with in a certain heading of you - or if elevator turn around 
};

enum TrackingState{
    VALID_HEADING, //person is within following distance
    TOO_CLOSE, //person is being tracked but is too close to the wheelchair
    LOST // system has lost track of the person 
};

typedef struct _State {
    lcm_t * lcm;
    BotParam * config;
    GHashTable *laser_hashtable;
    GQueue *channels;

    GHashTable *rlaser_hashtable;
    GQueue *rchannels;

    double goal_heading; // CCW in radians, 0 = straight forward
    int64_t last_utime;
    int64_t last_goal_heading_utime;
    gboolean estop;
    int shift_accel;
    double *state;
    gboolean have_goal_heading;
    gboolean collision_det;
    gboolean too_small_gap;
    int keep_close;
    int small_gap;
    int do_direction_only;

    erlcm_guide_info_t *guide_msg;
    double avg_guide_heading;

    double *heading_history;// = (double *) calloc(len_history, sizeof(double));
    double *temp_heading_history;
    int len;

    int verbose;
    int very_verbose;
    int playback;

    int subservient_to_joystick;
    erlcm_joystick_state_t *joystick_state_msg;

    enum TrackingState tracking_state;
    enum FollowingState following_state;
    double frontlaser_offset;
    double rearlaser_offset;
    double front_anglular_offset;
    double rear_anglular_offset;
    float draw_obs;
    int use_state_score;
    int too_far;
    double robot_pos[3];
    double robot_heading;
    int turn_in_place;  
    //for drawing 
    bot_lcmgl_t *lcmgl_rot;
    bot_lcmgl_t *lcmgl_navigator;
    bot_lcmgl_t *lcmgl_obs;
    bot_lcmgl_t *lcmgl_footprint;
    bot_lcmgl_t *lcmgl_navigator_info;
    bot_lcmgl_t *lcmgl_scores;
    bot_lcmgl_t *lcmgl_targets;
    bot_lcmgl_t *lcmgl_side_rays;
    bot_lcmgl_t *lcmgl_arc;
    int tourguide_mode;
    double previous_side_goal[2];
    int was_following_side;
    int target_side; 
} State;

void controller_update (State *self, int64_t utime);

void command_velocity(State *s, double tv, double rv){
    if(s->shift_accel)
        lcm_carmen3d_robot_velocity_command(tv, rv, s->lcm);
    else
        lcm_carmen3d_robot_velocity_const_accel_command(tv, rv,s->lcm);
}

struct laser_data *laser_data_copy (struct laser_data *ldata)
{
    struct laser_data *l = (struct laser_data*) calloc(1, sizeof(struct laser_data));
    for (int i=0;i<12;i++)
        l->m[i] = ldata->m[i];
    l->msg = bot_core_planar_lidar_t_copy (ldata->msg);
    return l;
}

inline int get_ind(int x,int count)
{
    if(x>=0 && x <= count-1)
        return x;
    else if(x >count-1)
        return x-(count);
    else
        return (count + x);
}

static inline double 
normalize_theta(double theta)
{
    int multiplier;
  
    if (theta >= -M_PI && theta < M_PI)
        return theta;
  
    multiplier = (int)(theta / (2*M_PI));
    theta = theta - multiplier*2*M_PI;
    if (theta >= M_PI)
        theta -= 2*M_PI;
    if (theta < -M_PI)
        theta += 2*M_PI;

    return theta;
}

//need to make this function better - seems to have some faults 
static inline int
geom_ray_rect_intersect_2d(const point2d_t *ray_start, const vec2d_t *ray_dir,
                           const point2d_t *obs_center, double h, double v, double alpha, 
                           double theta) 
{
    int collision = 0;
    if(fabs(ray_start->x - obs_center->x) < h/2 && 
       fabs(ray_start->y - obs_center->y) < v/2){
        collision = 1;
        return 1;
    }
    //transform in to the new co-ordinate frame
    double x_n = obs_center->x * ray_dir->x +  obs_center->y * ray_dir->y;
    double y_n = -obs_center->x * ray_dir->y + obs_center->y * ray_dir->x;
  
    //if within the rectangle in the new co-ordinate frame
    if(fabs(x_n) < h/2 && fabs(y_n) < v/2){
        collision = 1;
        return 1;
    }

    //check if within the arc of motion
    double obs_theta = atan2(obs_center->y, obs_center->x);
    int within = 0;
    //not sure if this does what is required 

    if(fabs(normalize_theta(obs_theta - (-alpha))) < theta){
        within = 1;    
    }
    else if(fabs(normalize_theta(obs_theta - alpha)) < theta){
        within = 1;
    }
    else if(fabs(normalize_theta(obs_theta - (M_PI - alpha))) < theta){
        within = 1;
    }
    else if(fabs(normalize_theta(obs_theta - (-M_PI + alpha))) < theta){ 
        within = 1;
    }
    if(within){    
        double obs_dist = hypot(obs_center->x, obs_center->y);
        double arc_dist = hypot(h/2, v/2);
        if(obs_dist < arc_dist){
            return 1;
        }
    }
  
    return 0;
}


static inline int
geom_ray_circle_intersect_2d(const point2d_t *ray_start, const vec2d_t *ray_dir,
                             const point2d_t *circle_center, double radius, 
                             double *t_closest, double *t_farthest)
{
    point2d_t c = {
        circle_center->x - ray_start->x,
        circle_center->y - ray_start->y,
    };
    double dsq = geom_vec_magnitude_squared_2d(ray_dir);
    double dot = geom_vec_vec_dot_2d(ray_dir, &c);
    double rsq = radius*radius;
    double det = dot*dot - dsq * (geom_vec_magnitude_squared_2d(&c) - rsq);
    if(det < 0) {
        return 0;
    } else if(fabs(det) < GEOM_EPSILON) {
        if(dot < 0)
            return 0;
        if(t_closest)
            *t_closest = dot / dsq;
        if(t_farthest)
            *t_farthest = dot / dsq;
        return 1;
    } else {
        double k = sqrt(det);
        double d1 = (dot - k) / dsq;
        double d2 = (dot + k) / dsq;
        int result = 0;
        if(d1 > 0) {
            if(t_closest)
                *t_closest = d1;
            result++;
        }
        if(d2 > 0) {
            if(result == 1) {
                if(t_farthest)
                    *t_farthest = d2;
            } else if(t_closest) {
                *t_closest = d2;
            }
            result++;
        }
        return result;
    }
}

//check for a collision if we rotate 
/*void  
  check_full_collisions(pointlist2d_t **points, int nchannels, 
  pointlist2d_t **rpoints, int nrchannels, 
  int *rot_collision, int *direct_collision, 
  double *min_side_distance)
  {

  //check for direct collisions
  double length = ROBOT_LENGTH + MARGIN;
  double width = ROBOT_WIDTH + MARGIN;

  double min_side_dist = HUGE;

  int collision_det = 0;

  double temp_side_dist;

  for (int lidx = 0; lidx < nchannels; lidx++) {
  pointlist2d_t *line = points[lidx];
  for (int i = 0; i < line->npoints; i++) {
  double x_r = line->points[i].x;
  double y_r = line->points[i].y;

  if((x_r < length/2 && x_r > -length/2) && (y_r < width/2 && y_r > -width/2)){ 
  //with in the safety boundry	
  fprintf(stderr,"\t====[C] Collision (Direct) => (%f,%f) \n",x_r,y_r);      
  *direct_collision = 1;
  }
  //calculate the min side distance
  if((x_r > -length/2) && (x_r < FORWARD_SCAN_DIST)){
  temp_side_dist = fabs(y_r);
  if(temp_side_dist < min_side_dist){
  min_side_dist = temp_side_dist; 
  }	  
  }
  }
  }
  for (int lidx = 0; lidx < nrchannels; lidx++) {
  pointlist2d_t *line = rpoints[lidx];
  for (int i = 0; i < line->npoints; i++) {
  double x_r = line->points[i].x;
  double y_r = line->points[i].y;

  if((x_r < length/2 && x_r > -length/2) && (y_r < width/2 && y_r > -width/2)){ 
  //with in the safety boundry	
  fprintf(stderr,"\t====[C] Collision (Direct) => (%f,%f) \n",x_r,y_r);      
  *direct_collision = 1;
  }
  //calculate the min side distance
  if((x_r > -length/2) && (x_r < FORWARD_SCAN_DIST)){
  temp_side_dist = fabs(y_r);
  if(temp_side_dist < min_side_dist){
  min_side_dist = temp_side_dist; 
  }	  
  }
  }
  }
  *min_side_distance = min_side_dist;


  //check for collisions if we rotate
  double radius = ROBOT_RADIUS + MARGIN; //sqrt(pow(ROBOT_LENGTH/2,2) + pow(ROBOT_WIDTH/2,2))+ MARGIN;

  for (int lidx = 0; lidx < nchannels; lidx++) {
  pointlist2d_t *line = points[lidx];
  for (int i = 0; i < line->npoints; i++) {
  double x_r = line->points[i].x;
  double y_r = line->points[i].y;
      
  double p_r = sqrt(pow(x_r,2) + pow(y_r,2));

  if (p_r < radius){
  fprintf(stderr,"\t====[C] Collision (Rotation) => (%f,%f) => %f: Radius  :%f\n",x_r,y_r, p_r, radius);      
  *rot_collision = 1;
  break;
  }
  }
  if(*rot_collision == 1)
  break;
  }
  if(*rot_collision == 0){
  for (int lidx = 0; lidx < nrchannels; lidx++) {
  pointlist2d_t *line = rpoints[lidx];
  for (int i = 0; i < line->npoints; i++) {
  double x_r = line->points[i].x;
  double y_r = line->points[i].y;
      
  double p_r = sqrt(pow(x_r,2) + pow(y_r,2));

  if (p_r < radius){
  fprintf(stderr,"\t====[C] Collision (Rotation) => (%f,%f) => %f: Radius  :%f\n",x_r,y_r, p_r, radius);      
  *rot_collision = 1;
  break;
  }
  }
  if(*rot_collision == 1)
  break;
  }
  }
  return;
  }
*/
//check for a collision if we rotate 
int 
check_rot_collision(pointlist2d_t **points, int nchannels, 
                    pointlist2d_t **rpoints, int nrchannels )
{
    double radius = ROBOT_RADIUS + MARGIN; //sqrt(pow(ROBOT_LENGTH/2,2) + pow(ROBOT_WIDTH/2,2))+ MARGIN;

    for (int lidx = 0; lidx < nchannels; lidx++) {
        pointlist2d_t *line = points[lidx];
        for (int i = 0; i < line->npoints; i++) {
            double x_r = line->points[i].x;
            double y_r = line->points[i].y;
      
            double p_r = sqrt(pow(x_r,2) + pow(y_r,2));

            if (p_r < radius){
                fprintf(stderr,"\t====[C] Collision (Rotation) => (%f,%f) => %f: Radius  :%f\n", x_r,y_r, p_r, radius);      
                return 1;
            }
        }
    }
    for (int lidx = 0; lidx < nrchannels; lidx++) {
        pointlist2d_t *line = rpoints[lidx];
        for (int i = 0; i < line->npoints; i++) {
            double x_r = line->points[i].x;
            double y_r = line->points[i].y;
      
            double p_r = sqrt(pow(x_r,2) + pow(y_r,2));

            if (p_r < radius){
                fprintf(stderr,"\t====[C] Collision (Rotation) => (%f,%f) => %f: Radius  :%f\n",x_r,y_r, p_r, radius);      
                return 1;
            }
        }
    }
    return 0;
}

double add_heading_obs(State *s, double heading, int clear){
    if(clear == 1){
        s->len = 0;
        return 0;
    }
  
    memcpy(s->temp_heading_history, s->heading_history, (len_history-1)*sizeof(double));
    memcpy(s->heading_history+1, s->temp_heading_history, (len_history-1)*sizeof(double));
    s->heading_history[0] = heading;
  
    s->len = fmin(s->len+1, len_history);

    double average = 0;
    
    for(int i=0; i < s->len; i++){
        average += s->heading_history[i];
    }
  
    if(s->len >0){
        average /= s->len;
        return average;
    }
    else{
        return 0;
    }
}

int 
check_person_collision(erlcm_people_pos_msg_t * person_msg, double angle){
    /*int no_people = person_msg->num_people;
      int tour_ind = person_msg->followed_person;
      double c = cos(angle);
      double s = sin(angle);
      double side_t = SIDE_PERSON_THRESHOLD;
      double forward_t = FORWARD_PERSON_DIST;
      double width = ROBOT_WIDTH;

      for(int i=0; i < no_people; i++){
      if(i==tour_ind){
      continue;
      }
      else{
      //convert the person position to the new co-ordinate frame and then see if the 
      //person is within the new path
      double x = person_msg->people_pos[i].x;
      double y = person_msg->people_pos[i].y;

      double x_r = x*c + y*s;
      double y_r = -x*s + y*c;

      if((x_r < forward_t && x_r > 0) 
      && (y_r < (width/2 + side_t)  && y_r > - (width/2 + side_t))){ 
      //with in the safety boundry	
      fprintf(stderr,"\t====[C] Collision (possible person) (%f,%f)\n ^^^^^^^^^",x_r,y_r);      
      return 1;
      }
      }
      }*/
    return 0;
}

//check if any of the points are within our region of occupation - suggesting a collision very soon 
int 
check_collision(pointlist2d_t **points, int nchannels, pointlist2d_t **rpoints, 
                int nrchannels,
                double *min_side_distance)
{
    double length = ROBOT_LENGTH + MARGIN;
    double width = ROBOT_WIDTH + MARGIN;

    double min_side_dist = HUGE;

    int collision_det = 0;

    double temp_side_dist;

    for (int lidx = 0; lidx < nchannels; lidx++) {
        pointlist2d_t *line = points[lidx];
        for (int i = 0; i < line->npoints; i++) {
            double x_r = line->points[i].x;
            double y_r = line->points[i].y;

            //this is wrong - the center point of the chair is not the middle of the length - slight difference 

            if((x_r < length/2 && x_r > -length/2) && (y_r < width/2 && y_r > -width/2)){ 
                //with in the safety boundry		
                if(!collision_det){
                    fprintf(stderr,"\t====[C] Collision (Direct) => (%f,%f) \n",x_r,y_r);      
                }
                collision_det = 1;
	
            }
            //calculate the min side distance
            if((x_r > -length/2) && (x_r < FORWARD_SCAN_DIST)){
                temp_side_dist = fabs(y_r);
                if(temp_side_dist < min_side_dist){
                    min_side_dist = temp_side_dist; 
                }	  
            }
        }
    }
    for (int lidx = 0; lidx < nrchannels; lidx++) {
        pointlist2d_t *line = rpoints[lidx];
        for (int i = 0; i < line->npoints; i++) {
            double x_r = line->points[i].x;
            double y_r = line->points[i].y;

            if((x_r < length/2 && x_r > -length/2) && (y_r < width/2 && y_r > -width/2)){ 
                //with in the safety boundry	
                fprintf(stderr,"\t====[C] Collision (Direct) => (%f,%f) \n",x_r,y_r);      
                collision_det = 1;
            }
            //calculate the min side distance
            if((x_r > -length/2) && (x_r < FORWARD_SCAN_DIST)){
                temp_side_dist = fabs(y_r);
                if(temp_side_dist < min_side_dist){
                    min_side_dist = temp_side_dist; 
                }	  
            }
        }
    }
    *min_side_distance = min_side_dist;

    return collision_det;
}

void laser_data_destroy (struct laser_data *ldata)
{
    bot_core_planar_lidar_t_destroy (ldata->msg);
}


//for printing status 
void print_following_state(State *s)
{
    if(s->following_state == FOLLOWING)
        fprintf(stderr," F-State : Following \n");
    if(s->following_state == PAUSED)
        fprintf(stderr," F-State : Paused \n");
    if(s->following_state == IDLE)
        fprintf(stderr," F-State : Stopped \n");  
    if(s->collision_det){
        fprintf(stderr," Collision - Detected \n");
    }
    else if (!s->collision_det){
        fprintf(stderr," Collision - Clear  \n");
    }
    
}

void print_tracking_state(State *s)
{
    if(s->tracking_state == VALID_HEADING)
        fprintf(stderr," T-State : Valid Heading \n");
    if(s->tracking_state == TOO_CLOSE)
        fprintf(stderr," T-State : Too Close\n");
    if(s->tracking_state == LOST)
        fprintf(stderr," T-State : Lost \n");
}

void print_status(State *s){
    print_following_state(s);
    print_tracking_state(s);  
}

//converting from local (robot) co-ordinates to global coordinates
void local_to_global(double lx, double ly, double *gx, double *gy, State *state){
    double s,c;
    bot_fasttrig_sincos(state->robot_heading,&s, &c);
    double x = state->robot_pos[0];
    double y = state->robot_pos[1];
    *gx = x + lx*c - ly*s;
    *gy = y + lx*s + ly*c;
    return;
}

void global_to_local(double gx, double gy, double *lx, double *ly, State *state){
    double s,c;
    bot_fasttrig_sincos(state->robot_heading,&s, &c);

    double dx = gx - state->robot_pos[0];
    double dy = gy - state->robot_pos[1];
  
    *lx = dx*c + dy*s;
    *ly = dy*c - dx*s;
    return;
}



//spoken responses 
void publish_speech_following_msg(char* property, State *s)
{
    erlcm_speech_cmd_t msg;
    msg.utime = bot_timestamp_now();
    msg.cmd_type = "FOLLOWER";
    msg.cmd_property = property;
    erlcm_speech_cmd_t_publish(s->lcm, "PERSON_TRACKER", &msg);
}

void clear_lcmgl(State *self)
{
    bot_lcmgl_t *lcmgl = self->lcmgl_rot;
    if(lcmgl)
        bot_lcmgl_switch_buffer (lcmgl);
    lcmgl = self->lcmgl_navigator;
    if(lcmgl)
        bot_lcmgl_switch_buffer (lcmgl);
    lcmgl = self->lcmgl_obs;
    if(lcmgl)
        bot_lcmgl_switch_buffer (lcmgl);
    lcmgl = self->lcmgl_footprint;
    if(lcmgl)
        bot_lcmgl_switch_buffer (lcmgl);
}

//following state for the wheelchair - this decides whether we follow a person or not 
void update_following_state_new(enum FollowCommand cmd, State *s)
{
    fprintf (stdout, "Current state = %d, commanded state = %d\n", s->following_state, cmd);
    if(cmd == STOP){
        //should stop the wheelchair - not to be started until we have a follow command

        if(s->following_state == FOLLOWING){
            fprintf(stderr,"Stopping Wheelchair\n");
            publish_speech_following_msg("STOPPED",s);
        }
        else if (s->following_state == OVERRIDDEN) {
            fprintf (stderr, "Transitioning from OVERRIDDEN to STOPPED\n");
            publish_speech_following_msg ("STOPPED", s);
        }
        else{
            publish_speech_following_msg("ALREADY_STOPPED",s);
            fprintf(stderr,"Chair is already stopped\n");
        }
        s->following_state = IDLE;
    }
    else if(cmd==FOLLOW){//check if we have a tracking of the person 
        if (s->following_state == OVERRIDDEN)
            fprintf (stderr, "Can't transition from OVERRIDDEN to FOLLOWING\n");
        else {
            if(s->tracking_state != LOST){//if person is tracked, start following            
                if(s->following_state == IDLE){//if we were idle before emit message
                    fprintf(stderr," => Starting to Follow \n");
                    publish_speech_following_msg("FOLLOWING",s);
                }
                else if(s->following_state == PAUSED){//if we were idle before emit message
                    fprintf(stderr," => Resuming following\n");
                }      
                s->following_state = FOLLOWING;
            }
            else if (s->following_state == OVERRIDDEN) {
                fprintf (stderr, "Can't transition from OVERRIDDEN to FOLLOWING\n");
            }
            else{//we dont have an estimate of the person 
                //we should have a state that waits for a person location while being in the following state
                s->following_state = WAITING;
                fprintf(stderr,"Waiting for person estimate\n");
                publish_speech_following_msg("WAITING",s); 
            }    
        }
    }
    else if(cmd==PAUSE){//Pause the wheelchair 
        if(s->following_state == IDLE){
        }
        else if (s->following_state == OVERRIDDEN)
            fprintf (stderr, "Can't transition from OVERRIDDEN to PAUSE\n");
        else{
            fprintf(stderr," => Pausing Chair \n");
            if(s->following_state == WAITING){
                publish_speech_following_msg("FOLLOWING",s);
            }
            s->following_state = PAUSED;
            fprintf(stderr,"**** Following State : %d\n", s->following_state);
        }
    }
    else if(cmd==RESUME){//check if we have a tracking of the person 
        if (s->following_state == OVERRIDDEN)
           fprintf (stderr, "Can't transition from OVERRIDDEN to FOLLOWING\n"); 
        else if((s->following_state == PAUSED || s->following_state == WAITING) 
           && (s->tracking_state != LOST)){
            if(s->following_state == WAITING){
                publish_speech_following_msg("FOLLOWING",s);
            }
            s->following_state = FOLLOWING;
            fprintf(stderr," => Resuming Following\n");
        }
    }    
    else if (cmd==OVERRIDE) {//Joystick override
        s->following_state = OVERRIDDEN;
        fprintf(stderr,"**** Following State : %d\n", s->following_state);
        publish_speech_following_msg("WAITING",s);
    }
}


//lcm handlers 
void mode_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                  const char * channel __attribute__((unused)), 
                  const erlcm_tagged_node_t * msg,
                  void * user  __attribute__((unused)))
{
    State *s = (State *)user;  
    char* type = msg->type;
    char* new_mode = msg->label;
    if(!strcmp(type,"mode")){
        //mode has changed - 
        if(!strcmp(new_mode,"navigation")){//navigation mode - stop following guide
            s->tourguide_mode = 0;
            fprintf(stderr,"Navigation mode\n");
        }
        if(!strcmp(new_mode,"tourguide")){//tourguide mode
            s->tourguide_mode = 1;
            fprintf(stderr,"Tourguide mode\n");
        }
    }
}

void speech_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), 
                    const erlcm_speech_cmd_t * msg,
                    void * user  __attribute__((unused)))
{
    State *s = (State *)user;  
    char* cmd = msg->cmd_type;
    char* property = msg->cmd_property;

    if(strcmp(cmd,"FOLLOWER")==0){
        if(strcmp(property,"START_FOLLOWING")==0){//guide has asked us to follow him
            update_following_state_new(FOLLOW,s);
            fprintf(stderr,"Speech Cmd : => Start Following\n");
        }
        else if(strcmp(property,"IDLE")==0){//guide has asked us to stop following him
            update_following_state_new(STOP,s);
            fprintf(stderr,"Speech Cmd : => Stop\n");
        }
        else if(strcmp(property,"KEEP_FARTHER")==0){//guide has asked us to keep our distance
            fprintf(stderr,"Keeping back\n");
            s->keep_close = 0;
        }
        else if(strcmp(property,"KEEP_CLOSER")==0){//guide has asked us to keep close 
            fprintf(stderr,"Keeping close\n");
            s->keep_close = 1;
        }
    }
    /*if(strcmp(cmd,"TRACKER")==0){
      if(strcmp(property,"START_LOOKING")==0){//guide has reset tracking so we are stoping till we get a new tracking
      update_following_state_new(STOP,s);
      fprintf(stderr,"Speech Cmd : => Reset Tracking\n");
      }
      }*/ //this is being handled by the tracker - we do not need to stop here
}

//keep an updated value of the robot pose
static void pose_handler(const lcm_recv_buf_t *rbuf, const char *channel, 
                         const bot_core_pose_t *msg, void *user_data )
{
    State *s = (State*) user_data;  
    double rpy[3];
    bot_quat_to_roll_pitch_yaw(msg->orientation, rpy);  
    s->robot_pos[0] = msg->pos[0];
    s->robot_pos[1] = msg->pos[1];
    s->robot_heading = rpy[2];  
}

static void joystick_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                 const char * channel __attribute__((unused)), 
                                 const erlcm_joystick_state_t * msg,
                                 void * user  __attribute__((unused)))
{  
    State *self = (State*) user;

    if (self->joystick_state_msg)
        erlcm_joystick_state_t_destroy (self->joystick_state_msg);

    self->joystick_state_msg = erlcm_joystick_state_t_copy (msg);
}


void draw_arc_broken(State *self, double tv, double rv){
    /* draw the obstacles */
    bot_lcmgl_t *lcmgl = self->lcmgl_arc;

    if (lcmgl) {
        double r_res = 0.4;

        double max_arc = 10.0;
        int max_r  = max_arc / r_res;
        
        for(int i= -max_r ; i < max_r; i++){
            double r = i * r_res; 
            lcmglPointSize(4);
            lcmglColor3f(0.5, 0, 0.0);

            double dt = M_PI/2/100; 
            double theta = 0;

            lcmglColor3f(1.0, 0, 0.0);
            lcmglBegin(GL_LINE_STRIP);
    
            for(int i=0; i < 100; i++){
                double x, y; 
                double gx, gy;
                if(r >0){
                    x = r * sin(theta);
                    y = r - r*cos(theta);	  
                }
                else{
                    x = fabs(r) * sin(theta); 
                    y = r * (1 - cos(theta)); 
                }

                local_to_global(x, y,&gx, &gy,self);
                theta +=dt;
                lcmglVertex3d(gx,gy, 0);
            }
            lcmglEnd();
        }
        double gx, gy;
        local_to_global(0, 0,&gx, &gy,self);
        lcmglVertex3d(gx,gy, 0);
        local_to_global(1.0, 0,&gx, &gy,self);      
        bot_lcmgl_switch_buffer (lcmgl);
    }
    return;
}

void draw_arc(State *self, double tv, double rv){
    // draw the obstacles 
    bot_lcmgl_t *lcmgl = self->lcmgl_arc;

    if (lcmgl) {
        if(rv !=0){
            double r = tv/rv; 
            lcmglPointSize(4);
            lcmglColor3f(0.5, 0, 0.0);

            double dt = M_PI/2/100; 
            double theta = 0;

            lcmglColor3f(1.0, 0, 0.0);
            lcmglBegin(GL_LINE_STRIP);
    
            for(int i=0; i < 100; i++){
                double x, y; 
                double gx, gy;
                if(r >0){
                    x = r * sin(theta);
                    y = r - r*cos(theta);	  
                }
                else{
                    x = fabs(r) * sin(theta); 
                    y = r * (1 - cos(theta)); 
                }

                local_to_global(x, y,&gx, &gy,self);
                theta +=dt;
                lcmglVertex3d(gx,gy, 0);
            }
            lcmglEnd();
        }
        else{//inf i.e. tv 
            //just draw forward 
            double gx, gy;
            local_to_global(0, 0,&gx, &gy,self);
            lcmglVertex3d(gx,gy, 0);
            local_to_global(1.0, 0,&gx, &gy,self);      
        }
        bot_lcmgl_switch_buffer (lcmgl);
    }
    return;
}

void draw_obs(State *self, pointlist2d_t **points, int nchannels){
    /* draw the obstacles */
    bot_lcmgl_t *lcmgl = self->lcmgl_obs;

    if (self->draw_obs && lcmgl) {
        lcmglPointSize(4);
        lcmglColor3f(0.5, 0, 0.0);
        //Drawing the obstacles
        for (int lidx = 0; lidx < nchannels; lidx++) {
            pointlist2d_t *line = points[lidx];
            lcmglBegin(GL_POINTS);
            double gx = .0, gy = .0;
            for (int pidx = 0; pidx < line->npoints; pidx++) {
                local_to_global(line->points[pidx].x, line->points[pidx].y,&gx, &gy,self);
                lcmglVertex3d(gx,gy, 0);
            }
            lcmglEnd();
            lcmglColor3f(0.5, 0, 0.0);
            for (int pidx = 0; pidx < line->npoints; pidx++) {
                local_to_global(line->points[pidx].x, line->points[pidx].y,&gx, &gy,self);
                double xyz[3] = { gx, gy , 0 };
                lcmglCircle(xyz, SAFETY_RADIUS);
            }
        }
        bot_lcmgl_switch_buffer (lcmgl);
    }
    return;
}

/*
 * Returns the rotational velocity
 */
double get_rot_vel(double angle_to_turn){
    double rv = 0;
    double used_theta = 0.0;

    //normalize the angle 
    angle_to_turn = bot_mod2pi(angle_to_turn) * 1.5;
  
    if (angle_to_turn < 0){
        used_theta = fmax(angle_to_turn, - bot_to_radians (CLAMP_THETA));
    }else{
        used_theta = fmin(angle_to_turn, bot_to_radians (CLAMP_THETA));
    }
  
    //rv = fmin(used_theta / bot_to_radians (80.0), 1.0) * MAX_ROTATION_SPEED;
    rv = fmin(used_theta * 1.0/ bot_to_radians (60.0), 1.0) * MAX_ROTATION_SPEED;

    //is it worth having a D term in here???

    //rv = fmin(max_allowed_rv, *rv);

    return rv;
}


/*
 * Returns the rotational velocity
 */
double get_trans_vel_arc_broken(State *self, 
                                double goal_pos[2], 
                                double goal_heading, 
                                double lookahead_distance,
                                double rv)
{
    double tv = 0;  

    double point_heading = atan2(goal_pos[1], goal_pos[0]);

    if(point_heading * goal_heading > 0){//both 
        double m = 0;

        if (self->verbose) {
            fprintf(stderr,"RV : %f\n", rv);
            fprintf(stderr," Target Heading : %f\n", bot_to_degrees(goal_heading));
        }

        if(fabs(goal_heading) < M_PI/2){
            m = -1 /  tan(goal_heading);
        }
        else{
            return 0;
        }
    
        double arc_radius = goal_pos[1] - m * goal_pos[0];

        //  arc_radius = abs(arc_radius);

        tv = arc_radius* rv;

        if (self->very_verbose) {
            fprintf(stderr,"Arc radius : %f\n", arc_radius);
            fprintf(stderr,"TV : %f\n", tv);
        }
    }

    return tv;
}


/*
 * Returns the rotational velocity
 */
void get_trans_rot_vel_arc(State *self, 
                           double target_pos[3],
                           double target_vel[2],
                           double *tv, 
                           double *rv, 
                           double max_tv, 
                           double max_rv)
{
  
    double ex = target_pos[0];
    double ey = target_pos[1]; 

    double gain_tv = 1.0;
    double gain_rv = 1.0;
    *tv = fmax(0,fmin(gain_tv * ex + target_vel[0] * cos(target_pos[2]), max_tv));

    //doesnt work - espcially the rv 
    *rv = fmin(fmax(-max_rv, gain_rv * sin(target_pos[2]) + target_vel[1] + ey *target_vel[0]), max_rv); 

    if (self->very_verbose) {
        fprintf(stderr,"New TV : %f\n", *tv);
        fprintf(stderr, "Comp 1: %f\tComp 2: %f\tComp 3: %f\n", gain_rv * sin(target_pos[2]), target_vel[1], ey *target_vel[0]);
        fprintf(stderr,"New RV : %f\n", *rv);
    }
}


/*
 * Returns the rotational velocity
 */
double get_trans_vel_arc_with_obs(State *self, 
                                  double best_heading, 
                                  //double goal_pos[2], 
                                  //double lookahead_distance,
                                  double clear_dist, 
                                  double avg_front_gap,
                                  double *rv, double max_tv_vel
                                  )
{
    double tv = 0;  

    double avg_free_dist = (avg_front_gap + clear_dist)/2;
  
    double goal_pos[2] = {avg_free_dist * cos(best_heading), avg_free_dist * sin(best_heading)}; 
  
    if(goal_pos[1] == 0){
        //this means straight motion 
        tv = max_tv_vel;
        return tv;
    }  



    //double r = pow(lookahead_distance,2)/ (2*goal_pos[1]);
    double r = pow(avg_free_dist,2)/ (2*goal_pos[1]);

    tv = fmax(0, fmin(r * (*rv), max_tv_vel)); 

    //update the rv based on the tv that we obtained - should only reduce is 
    double old_rv = *rv; 

    *rv = tv / r; 
    
    if (self->very_verbose) {
        fprintf(stderr,"Avg free dist : %f\n", avg_free_dist);
        fprintf(stderr, "Orignial : %f New : %f\n", old_rv, *rv); 
    }
  
    return tv; 
}

double get_trans_vel_arc_with_obs_fixed(State *self, 
                                        double best_heading, 
                                        //double goal_pos[2], 
                                        //double lookahead_distance,
                                        double clear_dist, 
                                        double avg_front_gap,
                                        double p_dist, 
                                        double heading_to_person, 
                                        double *rv, double max_tv_vel
                                        )
{
    double tv = 0;  

    double p_projection_to_best_heading = p_dist * cos(bot_mod2pi(heading_to_person - best_heading)); 

    double avg_dist_front_best = (avg_front_gap + clear_dist)/2;

    double avg_free_dist = fmax(fmin(avg_dist_front_best, p_projection_to_best_heading),0);//p_dist; //
  
    double goal_pos[2] = {avg_free_dist * cos(best_heading), avg_free_dist * sin(best_heading)}; 
  
    if(goal_pos[1] == 0){
        //this means straight motion 
        tv = max_tv_vel;
        return tv;
    }  

    //double r = pow(lookahead_distance,2)/ (2*goal_pos[1]);
    double r = pow(avg_free_dist,2)/ (2*goal_pos[1]);

    tv = fmax(0, fmin(r * (*rv), max_tv_vel)); 

    //update the rv based on the tv that we obtained - should only reduce is 
    double old_rv = *rv; 

    *rv = tv / r; 

    if (self->very_verbose) {
        fprintf(stderr,"Avg free dist : %f\n", avg_free_dist);
        fprintf(stderr, "Orignial : %f New : %f\n", old_rv, *rv); 
    }
  
    return tv; 
}

double get_trans_vel_arc_with_obs_fixed_moderated(State *self, 
                                                  double best_heading, 
                                                  //double goal_pos[2], 
                                                  //double lookahead_distance,
                                                  double clear_dist, 
                                                  double avg_front_gap,
                                                  double p_dist, 
                                                  double heading_to_person, 
                                                  double *rv, double max_tv_vel, 
                                                  double side_dist)
{
    double tv = 0;  
  
    double p_projection_to_best_heading = p_dist * cos(bot_mod2pi(heading_to_person - best_heading)); 

    double avg_dist_front_best = (avg_front_gap + clear_dist)/2;

    double avg_free_dist = fmax(fmin(avg_dist_front_best, p_projection_to_best_heading),0);//p_dist; //
  
    double goal_pos[2] = {avg_free_dist * cos(best_heading), avg_free_dist * sin(best_heading)}; 
  
    double max_allowed_tv = max_tv_vel * fmin((side_dist / 0.6), 1.0) * fmin((avg_free_dist / 1.5), 1.0); 

    if(goal_pos[1] == 0){
        //this means straight motion 
        tv = max_tv_vel;
        return tv;
    }  

    //double r = pow(lookahead_distance,2)/ (2*goal_pos[1]);
    double r = pow(avg_free_dist,2)/ (2*goal_pos[1]);

    tv = fmax(0, fmin(r * (*rv), max_allowed_tv)); //max_tv_vel)); 

    //update the rv based on the tv that we obtained - should only reduce is 
    double old_rv = *rv; 

    *rv = tv / r; 

    if (self->very_verbose) {
        fprintf(stderr,"Avg free dist : %f\n", avg_free_dist);
        fprintf(stderr, "Orignial : %f New : %f\n", old_rv, *rv); 
    }
  
    return tv; 
}

double get_trans_vel_arc_with_obs_direction(State *self, 
                                            double best_heading, 
                                            //double goal_pos[2], 
                                            //double lookahead_distance,
                                            double clear_dist, 
                                            double avg_front_gap,
                                            double p_dist, 
                                            double heading_to_person, 
                                            double *rv, double max_tv_vel, 
                                            double side_dist)
{
    double tv = 0;  
  
    double p_projection_to_best_heading = p_dist * cos(bot_mod2pi(heading_to_person - best_heading)); 

    double avg_dist_front_best = (avg_front_gap + clear_dist)/2;

    double avg_free_dist = fmax(fmin(avg_dist_front_best, p_projection_to_best_heading),0);//p_dist; //
  
    double goal_pos[2] = {avg_free_dist * cos(best_heading), avg_free_dist * sin(best_heading)}; 
  
    double max_allowed_tv = max_tv_vel * fmin((side_dist / 0.6), 1.0) * fmin((avg_free_dist / 1.5), 1.0); 

    if(goal_pos[1] == 0){
        //this means straight motion 
        tv = max_tv_vel;
        return tv;
    }  

    //double r = pow(lookahead_distance,2)/ (2*goal_pos[1]);
    double r = pow(avg_free_dist,2)/ (2*goal_pos[1]);

    tv = fmax(0, fmin(r * (*rv), max_allowed_tv)); //max_tv_vel)); 

    //update the rv based on the tv that we obtained - should only reduce is 
    double old_rv = *rv; 

    *rv = tv / r; 

    if (self->very_verbose) {
        fprintf(stderr,"Avg free dist : %f\n", avg_free_dist);
        fprintf(stderr, "Orignial : %f New : %f\n", old_rv, *rv); 
    }
    
    return tv; 
}


/*
 * Returns the rotational velocity
 */
double get_trans_vel_arc(State *self, 
                         double goal_pos[2], 
                         double lookahead_distance,
                         double *rv, double max_tv_vel)
{
    double tv = 0;  
  
    if(goal_pos[1] == 0){
        //this means straight motion 
        tv = max_tv_vel;
        return tv;
    }

    double r = pow(lookahead_distance,2)/ (2*goal_pos[1]);

    tv = fmax(0, fmin(r * (*rv), max_tv_vel)); 

    //update the rv based on the tv that we obtained - should only reduce is 
    double old_rv = *rv; 

    *rv = tv / r; 

    if (self->very_verbose)
        fprintf(stderr, "Orignial : %f New : %f\n", old_rv, *rv); 
  
    return tv; 
}


/*
 * Returns the rotational velocity
 */
double get_rot_vel_arc(State *self, 
                       double goal_pos[2], 
                       double lookahead_distance,
                       double tv)
{
    double rv = 0;  
  
    if(goal_pos[1] == 0){
        //this means straight motion 
        return 0;
    }

    double r = pow(lookahead_distance,2)/ (2*goal_pos[1]);

    if(r == 0){
        //should actually be higher 
        return 0;//MAX_ROTATION_SPEED
    }

    rv = fmax(-MAX_ROTATION_SPEED, fmin(tv/ r, MAX_ROTATION_SPEED)); 
    return rv; 
}

/*
 * Returns the rotational velocity
 */
double get_trans_vel(State *self, double angle_goal_robot, 
                     double dist_to_person, double avg_best_gap, 
                     double min_side_dist, double angle_best_goal, double max_allowed_tv){
    double tv = 0;
    double tv_ang = fabs(angle_goal_robot);

    if (tv_ang > bot_to_radians(TURN_IN_PLACE_DEG)){ //not sure which one i shuld use 
        tv_ang = TV_BIASING_THETA;// - bot_to_radians(5.0);
    }


    //double dt = fabs (tv_ang -bot_to_radians (TV_BIASING_THETA));
    double dt = fabs(fmin(fabs(tv_ang),bot_to_radians (TV_BIASING_THETA)) - bot_to_radians (TV_BIASING_THETA));

    tv = dt/bot_to_radians(TV_BIASING_THETA) * max_allowed_tv;      
    if (self->very_verbose)
        fprintf(stderr,"Biasing Angle : %f, After DT : %f", bot_to_degrees(tv_ang), tv);
    // *tv = (*tv) * best_range / max_range;
	
    //if(!self->keep_close){
    tv = (tv) * fmin(avg_best_gap / 1.0, 1.0);
    if (self->very_verbose)
        fprintf(stderr,"\tBest Gap Rat : %f", fmin(avg_best_gap / 1.0, 1.0));
    //}

    //********* thought we removed this 
    tv = (tv) *  fmin(min_side_dist/ BEST_SIDE_DIST, 1.0);

    if (self->very_verbose)
        fprintf(stderr,"\tSide Dist Rat : %f", fmin(min_side_dist/ BEST_SIDE_DIST, 1.0));
	
    tv = fmin(tv, max_allowed_tv);
  
    //if(fabs(angle_best_goal) > bot_to_radians(100.0)){ 
    if(fabs(angle_best_goal) > bot_to_radians(90.0)){ 
        //clamp the max speed that can be used when the wheelchair is going away from the person 
        tv = fmin(tv,MAX_AWAY_TRANSLATION_SPEED);
    }      

    /*if(angle_best_goal < bot_to_radians(10) && angle_goal_robot < bot_to_radians(10)){
    //choose the tv a different way
    double dist_p = dist_to_person - 1.5;
    double gain_p = 1.0;
    if(dist_p > 1.5){
    tv = fmin(dist_p*gain_p , 1.0) * MAX_FAST_TRANSLATION_SPEED; 
    fprintf(stderr,"\n+++++++++++++++++++++++++++++++\n");
    }

    }*/

    if (self->very_verbose)
        fprintf(stderr,"Actual TV : %f\n", tv);
    return tv;

}

/*
 * Returns the rotational velocity
 */
double get_trans_vel_orig(State *self, double angle_goal_robot, 
                          double dist_to_person, double avg_best_gap, 
                          double min_side_dist, double angle_best_goal){
    double tv = 0;
    double tv_ang = fabs(angle_goal_robot);

    if (tv_ang > bot_to_radians(TURN_IN_PLACE_DEG)){ //not sure which one i shuld use 
        tv_ang = TV_BIASING_THETA;// - bot_to_radians(5.0);
    }
    double dt = fabs (tv_ang -bot_to_radians (TV_BIASING_THETA));
    tv = dt/bot_to_radians(TV_BIASING_THETA) * MAX_TRANSLATION_SPEED;      

    // *tv = (*tv) * best_range / max_range;
	
    //if(!self->keep_close){
    tv = (tv) * fmin(avg_best_gap / 1.0, 1.0);
    //}
    tv = (tv) *  fmin(min_side_dist/ BEST_SIDE_DIST, 1.0);
	
    tv = fmin(tv, MAX_TRANSLATION_SPEED);

    //if(fabs(angle_best_goal) > bot_to_radians(100.0)){ 
    if(fabs(angle_best_goal) > bot_to_radians(90.0)){ 
        //clamp the max speed that can be used when the wheelchair is going away from the person 
        tv = fmin(tv,MAX_AWAY_TRANSLATION_SPEED);
    }      
    return tv;

}

/*
 * Draw emergency stop text
 */
void draw_text_navigator_msg(State *self, double pos[3], char *seg_info)
{
    bot_lcmgl_t *lcmgl = self->lcmgl_navigator_info;
    if(lcmgl){
        double g_robot[3] = {.0,.0,.0};
        local_to_global(pos[0]+5.0, pos[1]+5.0,&g_robot[0], &g_robot[1],self);
        //char seg_info[512];
        //sprintf(seg_info,"Emergency Stop - Direct Collision Detected\n");
        if (self->very_verbose)
            fprintf(stderr,"Status : %s\n", seg_info);
        lcmglColor3f(1, 0, 0); 
        bot_lcmgl_text(lcmgl, g_robot, seg_info);
    }
    bot_lcmgl_switch_buffer (lcmgl);
}


/*
 * Draw emergency stop text
 */
void draw_emergency_navigator_msg(State *self, double pos[3])
{
    bot_lcmgl_t *lcmgl = self->lcmgl_navigator_info;
    if(lcmgl){
        double g_robot[3] = {.0,.0,.0};
        local_to_global(pos[0]+5.0, pos[1]+5.0,&g_robot[0], &g_robot[1],self);
        char seg_info[512];
        sprintf(seg_info,"Emergency Stop - Direct Collision Detected\n");
        if (self->very_verbose)
            fprintf(stderr,"Status : %s\n", seg_info);
        lcmglColor3f(1, 0, 0); 
        bot_lcmgl_text(lcmgl, g_robot, seg_info);   
    }  
    bot_lcmgl_switch_buffer (lcmgl);
    //clear the old targets 
    lcmgl = self->lcmgl_targets;
    bot_lcmgl_switch_buffer (lcmgl);
}

/*
  returns 1 if we should check side by side 
*/

int check_side_by_side(int was_following_side, double dist_to_person, double heading_to_person, 
                       double person_heading, double person_velocity, int verbose){

    if (verbose)
        fprintf(stderr,"Person Heading : %f\n", bot_to_degrees(person_heading));
    //person heading is which way the person is facing 
    double catchup_distance = 2.0;
    //switch_back distance is larger - this decides when we should swtich back from side-by-side => direction following 
    double switch_back_distance = 2.2; //2.5 

    //hmm we should not reject based on velocity here - this will cause it to stop side following when we stop 

    //we should not go from direction to side by side unless there is significant velocity 

    if(!was_following_side && person_velocity < 0.1){
        if (verbose)
            fprintf(stderr," + Not checking : person not moving and robot was direction following\n");
        return 0;
    }

    else if(!was_following_side){//was doing direction following 
        if(((dist_to_person < catchup_distance)// && heading_to_person < bot_to_radians(45)) 
            && fabs(person_heading) < bot_to_radians(30))
           && person_velocity > 0.2) {
            if (verbose)
                fprintf(stderr," + Was not following side and withing catch distance \n");
            return 1;
        }
    }
    else{//was doing side by side 
        if(dist_to_person < switch_back_distance && fabs(person_heading) < bot_to_radians(120)){//90)){
            return 1;
        }
    }
    return 0;
}

/*
  checks the person's personal space and returns 1 if the robot is within -
  forcing the robot to turn-in-place only
*/
int check_in_personal_space_half_circle(double dist_to_person, double heading_to_person, 
                                        double person_heading, double p_pos[2], 
                                        double *dist_from_edge){

    //check if chair is infront or behind the person 
    double beta = bot_mod2pi(M_PI - heading_to_person + person_heading);
  
    double R;
    //if in the forward portion of the person 
    if(fabs(beta ) < bot_to_radians(90)){
        R = 1.5;
    }
    else{
        R = 1.0;
    }

    *dist_from_edge = fmax(0,dist_to_person - R);

    if(dist_to_person < R){    
        return 1;
    }
    else{
        return 0;
    }
}

//I think this is the one that is prob setting the speed to 0 - fix (prob when the speed is high and then the person stops - component cause of the speed causes the dist to fire 
int check_in_personal_space_from_velocity(double dist_to_person, double heading_to_person, 
                                          double person_heading, double p_pos[2], 
                                          double personal_zone_radius,
                                          double *dist_from_edge){
    double R = 1.2;
    //if in the forward portion of the person 
    /*if(fabs(beta ) < bot_to_radians(120)){
        R = fmax(personal_zone_radius, 1.2);
    }
    else{
        R = fmax(personal_zone_radius, 0.8); //should be 0.8 - -changed for the direction following demo 
    }

    *dist_from_edge = fmax(0,dist_to_person - R);*/

    *dist_from_edge = fmax(0,dist_to_person - R);

    if(dist_to_person < R){    
        return 1;
    }
    else{
        return 0;
    }
}


/*int check_in_personal_space_from_velocity(double dist_to_person, double heading_to_person, 
                                          double person_heading, double p_pos[2], 
                                          double personal_zone_radius,
                                          double *dist_from_edge){

    //check if chair is infront or behind the person 
    double beta = bot_mod2pi(M_PI - heading_to_person + person_heading);
  
    double R;
    //if in the forward portion of the person 
    if(fabs(beta ) < bot_to_radians(120)){
        R = fmax(personal_zone_radius, 1.2);
    }
    else{
        R = fmax(personal_zone_radius, 0.8); //should be 0.8 - -changed for the direction following demo 
    }

    *dist_from_edge = fmax(0,dist_to_person - R);

    if(dist_to_person < R){    
        return 1;
    }
    else{
        return 0;
    }
}*/

int check_in_personal_space_ellipse(double dist_to_person, double heading_to_person, 
                                    double person_heading, double p_pos[2]){

    //check if chair is infront or behind the person 
    double beta = bot_mod2pi(M_PI - heading_to_person + person_heading);
  
    double a, b, c; //a is the major axis 
    //if in the forward portion of the person 
    if(fabs(beta ) < bot_to_radians(90)){
        a = 1.5; 
        b = 1.0;
    }
    else{
        a = 1.0;
        b = 1.0;
    }

    c = sqrt(pow(a,2) - pow(b,2));
    double xy1[2] = {p_pos[0] + c * cos(person_heading), p_pos[1] + c * sin(person_heading)};
    double xy2[2] = {p_pos[0] - c * cos(person_heading), p_pos[1] - c * sin(person_heading)};
  
    double d1 = hypot(xy1[0], xy1[1]);
    double d2 = hypot(xy2[0], xy2[1]);

    if(d1 + d2 < 2 * a){//within the ellipse 
        return 1;
    }

    return 0;
}

/*
 * Draw emergency stop text
 */
void draw_paused_navigator_msg(State *self, double pos[3])
{
    bot_lcmgl_t *lcmgl = self->lcmgl_navigator_info;
    if(lcmgl){
        double g_robot[3] = {.0,.0,.0};
        local_to_global(pos[0]+5.0, pos[1]+5.0,&g_robot[0], &g_robot[1],self);
        char seg_info[512];
        sprintf(seg_info,"Within Pausing Radius \n");
        lcmglColor3f(1, 0, 0); 
        bot_lcmgl_text(lcmgl, g_robot, seg_info);
    }
    bot_lcmgl_switch_buffer (lcmgl);
}

/*
 * Draws the scores for each ray - function broken 
 */
void draw_obs_scores(State *self, double **xys, double *scores, double *avg_scores, 
                     int nrays)
{
    bot_lcmgl_t *lcmgl = self->lcmgl_scores;
    if(lcmgl){
        for (int ridx=0;ridx<nrays;ridx++) {
            double score_g[3] = {.0,.0,.0};
            local_to_global(xys[ridx][0], xys[ridx][1],&score_g[0], &score_g[1],self);      
            char score_info[512];
            sprintf(score_info,"%.3f-%.3f", scores[ridx],avg_scores[ridx]);
            lcmglColor3f(1, 0, 0); 
            bot_lcmgl_text(lcmgl, score_g, score_info);
        }
    }
    bot_lcmgl_switch_buffer (lcmgl);
}

void draw_rotation(State *self, int can_rotation, int clear){
    double pos[3] = {.0,.0,.0};
    bot_lcmgl_t *lcmgl = self->lcmgl_rot;
    if(lcmgl){
        if(!clear){
            if(can_rotation){
                lcmglColor3f(0, 1, 0); 
            }
            else{
                lcmglColor3f(1, 0, 0); 
            }
            double rad = sqrt(pow(ROBOT_LENGTH/2,2)+ pow(ROBOT_WIDTH/2,2)) + MARGIN;
            double gx = .0, gy = .0;
            local_to_global(pos[0], pos[1],&gx, &gy,self);
            double xyz[3] = { gx, gy, 0 };
            lcmglCircle(xyz, rad);
            lcmglColor3f(1, 0, 0.5); 
            lcmglCircle(xyz,ROT_RADIUS);
        }
        bot_lcmgl_switch_buffer (lcmgl);
    }
}
//broken - not sure how to pass the rays
void draw_rays(State *self, int nrays, int best_ridx, double** xys, double goal_pt[2]){
    double pos[3] = {.0,.0,.0};
    bot_lcmgl_t *lcmgl = self->lcmgl_navigator;

    /* draw the rays */
    if (lcmgl) {
        /* draw rays in light blue */
        double robot_gx = .0, robot_gy = .0;
        local_to_global(pos[0], pos[1],&robot_gx, &robot_gy,self);
    
        for (int ridx = 0; ridx < nrays; ridx++) {
            if (ridx == best_ridx) {
                lcmglLineWidth (3);
                lcmglColor3f(1, .5, .5);
            }
            else {
                lcmglLineWidth (1);
                lcmglColor3f(.3, .3, 1);
            }
            lcmglBegin(GL_LINES);

            double p_gx = .0, p_gy = .0;
            local_to_global(xys[ridx][0], xys[ridx][1] ,&p_gx, &p_gy,self);
            lcmglVertex3d(robot_gx, robot_gy, pos[2]);
            lcmglVertex3d(p_gx, p_gy, pos[2]);
            lcmglEnd();
        }
        lcmglColor3f(1, .5, .5);
        lcmglPointSize(8);
        lcmglBegin(GL_POINTS);

        double p_gx = .0, p_gy = .0;
        local_to_global(xys[best_ridx][0], xys[best_ridx][1] ,&p_gx, &p_gy,self);
        lcmglVertex3d(p_gx, p_gy, pos[2]);
        lcmglEnd();

        /* draw target direction as a yellow ray */
        lcmglColor3f(1, 1, 0);
        lcmglLineWidth(1);
        lcmglBegin(GL_LINES);

        lcmglVertex3d(robot_gx, robot_gy, pos[2]);
        local_to_global(goal_pt[0],goal_pt[1],&p_gx, &p_gy,self);
        lcmglVertex3d(p_gx, p_gy, pos[2]);
        lcmglEnd();

        bot_lcmgl_switch_buffer (lcmgl);
    }

}


static void on_laser(const lcm_recv_buf_t *rbuf, const char *channel, 
                     const bot_core_planar_lidar_t *msg, void *user_data )
{
    State *self = (State*) user_data;
    struct laser_channel *lchannel;

    lchannel = g_hash_table_lookup(self->laser_hashtable, channel);
  
    if (lchannel == NULL) {
        lchannel = (struct laser_channel*) calloc(1, sizeof(struct laser_channel));
        lchannel->data = g_queue_new ();
        lchannel->complained = 0;
        strcpy(lchannel->name, channel);

        // handle a mask
        char key[1024];
        sprintf(key, "planar_lidars.%s.mask", channel);
        char **masks = bot_param_get_str_array_alloc(self->config, key);
        int nmasked = 0;
        for (int j = 0; masks && masks[j]!=NULL && masks[j+1]!=NULL; j+=2) {
            double v0 = strtof(masks[j], NULL);
            double v1 = strtof(masks[j+1], NULL);

            // take small steps so that we won't skip a bucket due to a 
            // rounding area (hence the extra / 2.0)
            double theta_per_index = 2.0 * M_PI / MASK_BUCKETS;
            for (double t = v0; t <= v1; t += theta_per_index/2.0) {
                int idx = bot_theta_to_int(t, MASK_BUCKETS);            
                if(!lchannel->mask[idx]) {
                    nmasked++;
                }
                lchannel->mask[idx]=1;
            }
        }
        g_hash_table_insert(self->laser_hashtable, lchannel->name, lchannel);
        g_queue_push_head (self->channels, lchannel);    
    }

    struct laser_data *ldata = (struct laser_data*) calloc(1, sizeof(struct laser_data));
    //need to append our local transformation vector here 

    /* push data in the queue */
    ldata->msg = bot_core_planar_lidar_t_copy(msg);

    g_queue_push_head (lchannel->data, ldata);

    if (g_queue_get_length (lchannel->data) > LASER_MEMORY) {
        struct laser_data *ld = (struct laser_data*)g_queue_pop_tail (lchannel->data);
        laser_data_destroy (ld);
    }

    /* process returns */
    if(!strcmp(channel,"SKIRT_FRONT")){
        controller_update (self, msg->utime);
    }
    return;
}


void guide_pos_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), 
                       const erlcm_guide_info_t * msg,
                       void * user  __attribute__((unused)))
{
    State *s = (State *)user;
    if(s->guide_msg!=NULL){
        erlcm_guide_info_t_destroy(s->guide_msg);
    }
    s->guide_msg = erlcm_guide_info_t_copy(msg);
    //int ind = s->person_msg->followed_person;

    if(s->guide_msg->tracking_state==1){  //we have a valid person estimate    
        double person[2] = {s->guide_msg->pos[0], s->guide_msg->pos[1]};
        //check condition to start following a person
        double act_angle = atan2(person[1],person[0]);

        //Error : This is technically wrong - these headings are as of the current robot frame 
        //this frame changes based on robot motion 
        s->avg_guide_heading = s->guide_msg->person_heading;//add_heading_obs(s, s->guide_msg->person_heading,0);
        s->goal_heading = act_angle;
        s->have_goal_heading = TRUE;
        s->last_goal_heading_utime = msg->utime;
        double used_stop_radius;

        if(!s->keep_close){
            used_stop_radius = STOP_RADIUS;
        }
        else{
            used_stop_radius = C_STOP_RADIUS;
        }
    
        /*if(act_dist >= used_stop_radius){//heading restriction removed from here
          s->tracking_state = VALID_HEADING;
          update_following_state_new(RESUME,s);
          if(s->following_state == FOLLOWING && act_dist >= 5.0){
          if(!s->too_far){ //we were within the range 
          s->too_far = 1;
          publish_speech_following_msg("SLOW_DOWN",s);  //warning to slow down 
          }	
          }
          else{
          s->too_far = 0;
          }*/
        //we should prob not do this here - should be in the control loop
    
        s->tracking_state = VALID_HEADING;
        //update_following_state_new(RESUME,s);

        /*if(act_dist >= used_stop_radius){//heading restriction removed from here
          s->tracking_state = VALID_HEADING;
          update_following_state_new(RESUME,s);
          if(s->following_state == FOLLOWING && act_dist >= 5.0){
          if(!s->too_far){ //we were within the range 
          s->too_far = 1;
          publish_speech_following_msg("SLOW_DOWN",s);  //warning to slow down 
          }	
          }
          else{
          s->too_far = 0;
          }    
          }
          else{
          s->tracking_state = TOO_CLOSE;
          //we pause 
          update_following_state_new(PAUSE,s);
          }*/
    }
 
    else{
        s->avg_guide_heading = s->guide_msg->person_heading;//add_heading_obs(s, 0,1);
        if(s->tracking_state !=LOST){ //we had a track of the person before 
            //prevents the system from stoping before we get a new estimate
            s->tracking_state = LOST;
            s->keep_close = 0;
            //publish_speech_following_msg("UNABLE_TO_FOLLOW_LOST",s); 
            update_following_state_new(STOP,s);
        }
        //we stop 

    }
    //print_status(s);
}

/*void people_pos_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), 
  const carmen3d_people_pos_msg_t * msg,
  void * user  __attribute__((unused)))
  {
  State *s = (State *)user;
  if(s->person_msg!=NULL){
  carmen3d_people_pos_msg_t_destroy(s->person_msg);
  }
  s->person_msg = carmen3d_people_pos_msg_t_copy(msg);
  int ind = s->person_msg->followed_person;

  if(ind>=0){  //we have a valid person estimate
  double person[2] = {s->person_msg->people_pos[ind].x, s->person_msg->people_pos[ind].y};
  //check condition to start following a person
  double act_dist = hypot(person[0],person[1]);
  double act_angle = atan2(person[1],person[0]);
  s->goal_heading = act_angle;
  s->have_goal_heading = TRUE;
  s->last_goal_heading_utime = msg->utime;
  double used_stop_radius;
  if(!s->keep_close){
  used_stop_radius = STOP_RADIUS;
  }
  else{
  used_stop_radius = C_STOP_RADIUS;
  }
  if(act_dist >= used_stop_radius){//heading restriction removed from here
  s->tracking_state = VALID_HEADING;
  update_following_state_new(RESUME,s);
  if(s->following_state == FOLLOWING && act_dist >= 5.0){
  if(!s->too_far){ //we were within the range 
  s->too_far = 1;
  publish_speech_following_msg("SLOW_DOWN",s);  //warning to slow down 
  }	
  }
  else{
  s->too_far = 0;
  }
  }
  else{
  s->tracking_state = TOO_CLOSE;
  //we pause 
  update_following_state_new(PAUSE,s);
  }
  }
  else{
  if(s->tracking_state !=LOST){ //we had a track of the person before 
  //prevents the system from stoping before we get a new estimate
  s->tracking_state = LOST;
  s->keep_close = 0;
  //publish_speech_following_msg("UNABLE_TO_FOLLOW_LOST",s); 
  update_following_state_new(STOP,s);
  }
  //we stop 

  }
  //print_status(s);
  }*/

/* computes intersection between a line segment and a point list
 */
double ray_cast_test(point2d_t *xy0, point2d_t *xy1, pointlist2d_t *points)
{
    if (!points)
        return HUGE;

    double closest_dist = HUGE;
    assert (xy0);
    assert (xy1);

    for (int pidx = 0; pidx+1 < points->npoints; pidx++) {
        point2d_t *p0 = &points->points[pidx];
        point2d_t *p1 = &points->points[pidx+1];
        assert (p0);
        assert (p1);

        point2d_t res;
        if (geom_line_seg_line_seg_intersect_2d(xy0, xy1, p0, p1, &res)) {
            closest_dist = fmin(closest_dist, sqrt(powf(res.x - xy0->x,2) +
                                                   powf(res.y - xy0->y,2)));
        }
    }
    return closest_dist;
}

void laser_returns_destroy (GQueue *channels)
{
    for (GList *iter=g_queue_peek_head_link (channels);iter;iter=iter->next) {
        struct laser_channel *lc = (struct laser_channel*)iter->data;
        for (GList *iterc=g_queue_peek_head_link (lc->data);iterc;iterc=iterc->next) {
            struct laser_data *ld = (struct laser_data*)iterc->data;
            laser_data_destroy (ld);
        }
        g_queue_free (lc->data);
    }
    g_queue_free (channels);
}

void free_points(pointlist2d_t **points, int nchannels){
    /* free points */
    for (int ridx=0;ridx<nchannels;ridx++) {
        if (points[ridx])
            pointlist2d_free (points[ridx]);
    }
    free (points);
    /* free channel copy */
    //laser_returns_destroy (channels);
}

GQueue *laser_returns_copy (GQueue *channels)
{
    GQueue *q = g_queue_new ();

    for (GList *iter=g_queue_peek_head_link (channels);iter;iter=iter->next) {

        struct laser_channel *lc = (struct laser_channel*)iter->data;

        struct laser_channel *lchannel = (struct laser_channel*) calloc(1, sizeof(struct laser_channel));
        lchannel->data = g_queue_new ();
        lchannel->complained = lc->complained;
        strcpy(lchannel->name, lc->name);
        memcpy(lchannel->mask, lc->mask, MASK_BUCKETS * sizeof(int8_t));
        if (g_queue_is_empty (lc->data))
            continue;
        g_queue_push_head (lchannel->data, laser_data_copy (g_queue_peek_head (lc->data)));
        g_queue_push_head (q, lchannel);
    }
    return q;
}

void print_laser_returns (GQueue *channels, const char *filename)
{
    if (g_queue_is_empty (channels))
        return;

    for (GList *iter=g_queue_peek_head_link (channels);iter;iter=iter->next) {

        struct laser_channel *lc = (struct laser_channel*)iter->data;
        if (g_queue_is_empty (lc->data)) {
            fprintf (stderr, "laser data is empty.\n");
            continue;
        }
        struct laser_data *ldata = (struct laser_data*)g_queue_peek_head (lc->data);

        FILE *fp = fopen (filename, "w");

        for (int rangeidx = 0; rangeidx < ldata->msg->nranges; rangeidx++) {
            double range = ldata->msg->ranges[rangeidx];
            fprintf (fp, "%d %.3f\n", rangeidx, range);
        }
        fclose (fp);
    }
}

/* convert laser returns into a point list in the local frame
 */
pointlist2d_t **laser_to_local_points (State *s, GQueue *channels, int *npoints, int filter_person)
{
    int n = g_queue_get_length (channels); 
    *npoints = n;

    double person[2] = {s->guide_msg->pos[0], s->guide_msg->pos[1]};
    double p_heading = s->guide_msg->person_heading;
    if (g_queue_is_empty (channels))
        return NULL;

    pointlist2d_t **points = (pointlist2d_t**)malloc(n*sizeof(pointlist2d_t*));
    double dist = 0;

    for (int ridx=0;ridx<n;ridx++)
        points[ridx] = NULL;

    int idx = 0;
    for (GList *iter=g_queue_peek_head_link (channels);iter;iter=iter->next) {
        struct laser_channel *lc = (struct laser_channel*)iter->data;
    
        if (g_queue_is_empty (lc->data)) {
            fprintf (stderr, "laser data is empty.\n");
            continue;
        }
        struct laser_data *ldata = (struct laser_data*)g_queue_peek_head (lc->data);

        double pos_offset = 0.0;
        double ang_offset = 0.0;

        if(!strcmp(lc->name,"SKIRT_FRONT")){
            pos_offset = s->frontlaser_offset;
            ang_offset = s->front_anglular_offset;
        }

        if(!strcmp(lc->name,"SKIRT_REAR")){
            pos_offset = s->rearlaser_offset;
            ang_offset = s->rear_anglular_offset;
        }
    
        double hit_buf[ldata->msg->nranges][2];
        int nhits = 0;

        //we do beam skips 
        for (int rangeidx = 0; rangeidx < ldata->msg->nranges; rangeidx+=BEAM_SKIP) {
            double range = ldata->msg->ranges[rangeidx];
      
            double theta = ldata->msg->rad0 + ldata->msg->radstep*rangeidx + ang_offset;

            // mask?
            int maskind = bot_theta_to_int(theta, MASK_BUCKETS);
            if(lc->mask[maskind]) {
                continue;
            }
            if(range < 0.02) { //already filters outliers by skipping these points 
                rangeidx-= BEAM_SKIP-1; //bring back one because otherwise we will skip the next one 
                continue;
            }
            if(range >= 30.0) {
                rangeidx-=BEAM_SKIP-1; //bring back one because otherwise we will skip the next one 
                continue;
            }

            double local_xyz[3];
            double s, c;
            bot_fasttrig_sincos(theta, &s, &c);

            //convert to local frame
            local_xyz[0] = range *c + pos_offset; 
            local_xyz[1] = range*s; 

            dist = hypot(local_xyz[0] - person[0], 
                         local_xyz[1] - person[1]);
      
            if(filter_person && dist < 0.5){
                continue;
            }

            hit_buf[nhits][0] = local_xyz[0];
            hit_buf[nhits][1] = local_xyz[1];
            nhits++;
        }    

        //add points along the person's trajectory 
        if(!filter_person){//fill obstacles along the projected trajectory 
            double c = cos(p_heading);
            double s = sin(p_heading);

            double l = 0;
            for(int i=0;i<4;i++){
                l+=0.2;
                hit_buf[nhits][0] = person[0] + l * c;
                hit_buf[nhits][1] = person[1] + l * s;
                nhits++;
            }
        }
    

        pointlist2d_t *pt;
        if(!filter_person){
            pt = pointlist2d_new(nhits+1);
        }
        else{
            pt = pointlist2d_new(nhits);
        }
        for(int i=0; i<nhits; i++) {
            pt->points[i].x = hit_buf[i][0];
            pt->points[i].y = hit_buf[i][1];
        }
        //we add an adition point for the person location 
        if(!filter_person){
            pt->points[nhits].x = person[0];
            pt->points[nhits].y = person[1];
        }
        points[idx] = pt;
        idx++;
    }
    return points;
}

/* convert laser returns into a point list in the local frame
 */
local_points_t laser_to_local_points_broken(State *s, GQueue *channels, int *npoints)
{
    //we return set of points with and without the person observations 

    int n = g_queue_get_length (channels); 
    *npoints = n;

    double person[2] = {s->guide_msg->pos[0], s->guide_msg->pos[1]};
    local_points_t lp;

  

    if (g_queue_is_empty (channels)){
        lp.points = NULL;
        lp.pf_points = NULL;
        return lp;
    }


    lp.points = (pointlist2d_t**)malloc(n*sizeof(pointlist2d_t*));
    lp.pf_points = (pointlist2d_t**)malloc(n*sizeof(pointlist2d_t*));
    /*act_points = (pointlist2d_t**)malloc(n*sizeof(pointlist2d_t*));
      fprintf(stderr,"Allocated Points\n");
      person_free_points = (pointlist2d_t**)malloc(n*sizeof(pointlist2d_t*));
      fprintf(stderr,"Allocated PF Points\n");
    */
    for (int ridx=0;ridx<n;ridx++){
        lp.points[ridx] = NULL;
        lp.pf_points[ridx] = NULL;
    }

    int idx = 0;
    for (GList *iter=g_queue_peek_head_link (channels);iter;iter=iter->next) {
        struct laser_channel *lc = (struct laser_channel*)iter->data;
    
        if (g_queue_is_empty (lc->data)) {
            fprintf (stderr, "laser data is empty.\n");
            continue;
        }
        struct laser_data *ldata = (struct laser_data*)g_queue_peek_head (lc->data);

        double pos_offset = 0.0;
        double ang_offset = 0.0;

        //this does not support side offsets - but ok for now 

        if(!strcmp(lc->name,"SKIRT_FRONT")){
            pos_offset = s->frontlaser_offset;
            ang_offset = s->front_anglular_offset;
        }

        if(!strcmp(lc->name,"SKIRT_REAR")){
            pos_offset = s->rearlaser_offset;
            ang_offset = s->rear_anglular_offset;
        }
    
        double hit_buf[ldata->msg->nranges][2];    
        int nhits = 0;
        double pf_hit_buf[ldata->msg->nranges][2]; //person free points
        int pf_nhits = 0; //person free 

        //we do beam skips 
        for (int rangeidx = 0; rangeidx < ldata->msg->nranges; rangeidx+=BEAM_SKIP) {
            double range = ldata->msg->ranges[rangeidx];
      
            double theta = ldata->msg->rad0 + ldata->msg->radstep*rangeidx + ang_offset;

            // mask?
            int maskind = bot_theta_to_int(theta, MASK_BUCKETS);
            if(lc->mask[maskind]) {
                continue;
            }
            if(range < 0.02) { //already filters outliers by skipping these points 
                rangeidx-= BEAM_SKIP-1; //bring back one because otherwise we will skip the next one 
                continue;
            }
            if(range >= 30.0) {
                rangeidx-=BEAM_SKIP-1; //bring back one because otherwise we will skip the next one 
                continue;
            }

            double local_xyz[3];
            double s, c;
            bot_fasttrig_sincos(theta, &s, &c);

            //convert to local frame
            local_xyz[0] = range *c + pos_offset; 
            local_xyz[1] = range*s; 

            hit_buf[nhits][0] = local_xyz[0];
            hit_buf[nhits][1] = local_xyz[1];
            nhits++;

            if(hypot(local_xyz[0] - person[0], local_xyz[1] - person[1]) > 0.4){ //these do not come from legs 
                pf_hit_buf[pf_nhits][0] = local_xyz[0];
                pf_hit_buf[pf_nhits][1] = local_xyz[1];
                pf_nhits++;
            }
        }

        if (s->very_verbose)
            fprintf(stderr,"Basic Points : %d PF Points : %d \n", nhits, pf_nhits);

        pointlist2d_t *pt = pointlist2d_new(nhits);
        for(int i=0; i<nhits; i++) {
            pt->points[i].x = hit_buf[i][0];
            pt->points[i].y = hit_buf[i][1];
        }   
    
        lp.points[idx] = pt;

        pointlist2d_t *pf_pt = pointlist2d_new(pf_nhits);
        for(int i=0; i<nhits; i++) {
            pf_pt->points[i].x = pf_hit_buf[i][0];
            pf_pt->points[i].y = pf_hit_buf[i][1];
        }

        lp.pf_points[idx] = pf_pt;
        idx++;
    }  
    return lp;
}

/* convert laser returns into a point list in the local frame
 */
pointlist2d_t **rlaser_to_local_points (State *s, GQueue *channels, int *npoints)
{
    int n = g_queue_get_length (channels); 
    *npoints = n;

    if (g_queue_is_empty (channels))
        return NULL;

    pointlist2d_t **points = (pointlist2d_t**)malloc(n*sizeof(pointlist2d_t*));

    for (int ridx=0;ridx<n;ridx++)
        points[ridx] = NULL;

    int idx = 0;
    for (GList *iter=g_queue_peek_head_link (channels);iter;iter=iter->next) {
        struct laser_channel *lc = (struct laser_channel*)iter->data;
    
        if (g_queue_is_empty (lc->data)) {
            fprintf (stderr, "laser data is empty.\n");
            continue;
        }
        struct laser_data *ldata = (struct laser_data*)g_queue_peek_head (lc->data);

        double pos_offset = 0.0;
        double ang_offset = 0.0;

        if(!strcmp(lc->name,"SKIRT_FRONT")){
            pos_offset = s->frontlaser_offset;
            ang_offset = s->front_anglular_offset;
        }

        if(!strcmp(lc->name,"SKIRT_REAR")){
            pos_offset = s->rearlaser_offset;
            ang_offset = s->rear_anglular_offset;
        }
    
        double hit_buf[ldata->msg->nranges][2];
        int nhits = 0;

        for (int rangeidx = 0; rangeidx < ldata->msg->nranges; rangeidx++) {
            double range = ldata->msg->ranges[rangeidx];
            double theta = ldata->msg->rad0 + ldata->msg->radstep*rangeidx + ang_offset;
            int maskind = bot_theta_to_int(theta, MASK_BUCKETS);

            if(lc->mask[maskind]) {
                continue;
            }
            if(range < 0.02) { //already filters outliers by skipping these points 
                continue;
            }
            if(range >= 30.0) {
                continue;
            }

            double local_xyz[3];
            double s, c;
            bot_fasttrig_sincos(theta, &s, &c);

            //convert to local frame
            local_xyz[0] = range *c + pos_offset; 
            local_xyz[1] = range*s; 

            hit_buf[nhits][0] = local_xyz[0];
            hit_buf[nhits][1] = local_xyz[1];
            nhits++;
        }

        pointlist2d_t *pt = pointlist2d_new(nhits);
        for(int i=0; i<nhits; i++) {
            pt->points[i].x = hit_buf[i][0];
            pt->points[i].y = hit_buf[i][1];
        }
        points[idx] = pt;
        idx++;
    }
    return points;
}

void save_data (double *v, int n, const char *filename)
{
    FILE *fp = fopen (filename, "w");
    for (int i=0;i<n;i++) {
        fprintf (fp, "%d %.4f\n", i, v[i]);
    }
    fclose (fp);
}

void draw_wheelchair(State *self){
    double pos[2] = {self->robot_pos[0], self->robot_pos[1]};
    double theta = self->robot_heading;

    bot_lcmgl_t *lcmgl = self->lcmgl_footprint;
    if(lcmgl){
        lcmglPointSize(15);
        if(self->following_state ==IDLE){
            lcmglColor3f(1, 0, 0);
        }
        else if(self->following_state ==PAUSED){
            lcmglColor3f(0, 0, 1);
        }
        else{
            lcmglColor3f(0, 1, 0); 
        }
        double s = sin(theta);
        double c = cos(theta);
        double xy1[2], xy2[2], xy3[2], xy4[2];
        double h = ROBOT_LENGTH/2 + MARGIN;
        double b = ROBOT_WIDTH/2 + MARGIN;

        xy1[0] = pos[0] + b*s + h*c;
        xy1[1] = pos[1] - b*c + h*s;

        xy2[0] = pos[0] + b*s - h*c;
        xy2[1] = pos[1] - b*c - h*s;

        xy3[0] = pos[0] - b*s - h*c;
        xy3[1] = pos[1] + b*c - h*s;
    
        xy4[0] = pos[0] - b*s + h*c;
        xy4[1] = pos[1] + b*c + h*s;

        lcmglBegin(GL_LINE_LOOP);
        lcmglVertex3d(xy1[0], xy1[1], 0);
        lcmglVertex3d(xy2[0], xy2[1], 0);
        lcmglVertex3d(xy3[0], xy3[1], 0);
        lcmglVertex3d(xy4[0], xy4[1], 0);    
        lcmglEnd();
        bot_lcmgl_switch_buffer (lcmgl);
    }
}

double get_ray_dist(double target_pos[2], double pos[2], pointlist2d_t **points, int nchannels, double safety_radius)
{
    double theta = atan2(target_pos[1] - pos[1], target_pos[0]- pos[0]);
 
    double s = sin (theta);
    double c = cos (theta);

    // ***not sure if this is the right check - because especially for the forward position - it is likely to be covered by 
    //the person 

    point2d_t ray_start = { pos[0], pos[1]};  //robot position
    point2d_t ray_dir = { c, s };

    double dist = 20.0;//max_range;

    int poss_collision = 0;
    //get this from the config file
    //double alpha = atan2(0.6, 1.0);

    //double safety_radius = 0.4;//0.6;

    //--------------------------------------------------------------------------------//
    //Find maximum collision free distance for each ray

    //search for each ray for each point - for an intersection 
    for (int lidx = 0; lidx < nchannels; lidx++){
        pointlist2d_t *pts = points[lidx];
        if(poss_collision){
            break;
        }
        for(int i=0; i<pts->npoints; i++) {
            double t = 0.0;
            //if the ray can lead to a collision find the distance of the colliding point (t)
            if(geom_ray_circle_intersect_2d(&ray_start, &ray_dir, 
                                            &pts->points[i], safety_radius, &t, NULL)){
                dist = fmin(dist, t);  //this finds the closest colliding point - for each rays 
                //dist can never be more than the lookahead distance
            }
            //not sure if this following check is entirely correct - need to check it more thourghly
            /*if(geom_ray_rect_intersect_2d(&ray_start, &ray_dir, 
              &pts->points[i], 1.0, 0.6, alpha, theta)){
              //possible to skip this whole calculation
              poss_collision = 1;
              dist = 0;
              break;
              } */
        }
    
        //check for person as well here - with a higher radius - because person is not a point 
    
    }
      
    return dist;
}

//check if there are obstacles near by and is also reachable from person 

//might be better to check with actual footprint 
int is_empty_and_reachable(double radius, double pos[2], double side_pos[2], double person_pos[2], pointlist2d_t **points, int nchannels, double *side_gap, int verbose)
{
    int poss_collision = 0;
    //get this from the config file

    for (int lidx = 0; lidx < nchannels; lidx++){
        pointlist2d_t *pts = points[lidx];
        if(poss_collision){
            break;
        }
        for(int i=0; i<pts->npoints; i++) {
            double t = 0.0;
            t = hypot(pts->points[i].x - pos[0], pts->points[i].y - pos[1]);
            if(t <= radius){
                if (verbose)
                    fprintf(stderr, "Occupied Closest Point : %f [%f,%f]\n", t, pts->points[i].x, pts->points[i].y);
                return 0;
            }
        }
    }

    double ray_dist;
    double act_dist;
    //need to also check if we can move in that direction (from side pos - to projection)
  

    /*ray_dist = get_ray_dist(pos, side_pos, points, nchannels, USED_SAFETY_RADIUS );

      act_dist = hypot(pos[0] - side_pos[0], pos[1] - side_pos[1]);
  
      if(ray_dist - act_dist < -0.5 ){
      fprintf(stderr,"Rejected - path from side to projection not clear (R:%f A: %f)\n", ray_dist, act_dist);
      return 0;
      }*/

    //get ray dist from person position to side 
    ray_dist = get_ray_dist(side_pos, person_pos, points, nchannels, VISIBILITY_RADIUS);
    act_dist = hypot(person_pos[0] - side_pos[0], person_pos[1] - side_pos[1]);

    *side_gap = ray_dist; //lets return the side distance as well 

    if(ray_dist - act_dist < -0.5 ){
        if (verbose)
            fprintf(stderr,"Rejected - path from person to side  not clear (R:%f A: %f)\n", ray_dist, act_dist);
        return 0;
    }

    return 1;
}

//check if there are obstacles near by and is also reachable from person 

//might be better to check with actual footprint 
int is_empty_and_reachable_complete(double radius, double side_forward_pos[2], 
                                    double side_pos[2], double person_pos[2], 
                                    double person_forward_pos[2], 
                                    pointlist2d_t **points, int nchannels, 
                                    double *side_gap, 
                                    double *side_forward_dist)
{
    int poss_collision = 0;
    //get this from the config file

    /*for (int lidx = 0; lidx < nchannels; lidx++){
      pointlist2d_t *pts = points[lidx];
      if(poss_collision){
      break;
      }
      for(int i=0; i<pts->npoints; i++) {
      double t = 0.0;
      t = hypot(pts->points[i].x - side_forward_pos[0], pts->points[i].y - side_forward_pos[1]);
      if(t <= radius){
      fprintf(stderr, "Occupied Closest Point : %f [%f,%f]\n", t, pts->points[i].x, pts->points[i].y);
      return 0;
      }
      }
      }*/

    int is_side_reachable = 0; //is the side reachable from the person 
    int is_projection_reachable_from_side = 0; //if the side projection and the side position are reachable //we might get away without this 
    int is_projection_reachable_from_person = 0; //if the person projection and the side projection can reach eachother 
    int is_reachable = 1;

    double ray_dist;
    double act_dist;
    //need to also check if we can move in that direction (from side pos - to projection)
  

    /*ray_dist = get_ray_dist(pos, side_pos, points, nchannels, USED_SAFETY_RADIUS );

      act_dist = hypot(pos[0] - side_pos[0], pos[1] - side_pos[1]);
  
      if(ray_dist - act_dist < -0.5 ){
      fprintf(stderr,"Rejected - path from side to projection not clear (R:%f A: %f)\n", ray_dist, act_dist);
      return 0;
      }*/

    //get ray dist from person position to side 
    ray_dist = get_ray_dist(side_pos, person_pos, points, nchannels, VISIBILITY_RADIUS);
    act_dist = hypot(person_pos[0] - side_pos[0], person_pos[1] - side_pos[1]);

    if(ray_dist >= act_dist){
        is_side_reachable = 1;
    } 
    else{
        is_reachable = 0;
    }

    *side_gap = ray_dist; //lets return the side distance as well 

    ray_dist = get_ray_dist(side_forward_pos, side_pos, points, nchannels, VISIBILITY_RADIUS);
    *side_forward_dist = ray_dist;  
    act_dist = hypot(side_forward_pos[0] - side_pos[0], side_forward_pos[1] - side_pos[1]);

    if(ray_dist >= act_dist){
        is_projection_reachable_from_side = 1;    
    }
    else{
        is_reachable = 0;
    }

    ray_dist = get_ray_dist(side_forward_pos, person_forward_pos, points, nchannels, VISIBILITY_RADIUS);
    act_dist = hypot(side_forward_pos[0] - person_forward_pos[0], side_forward_pos[1] - person_forward_pos[1]);

    if(ray_dist >= act_dist){
        is_projection_reachable_from_person = 1;
    }
    else{
        is_reachable = 0;
    }
  
    return is_reachable;
  
}

//might be better to check with actual footprint 
double get_front_gap(double side_proj_pos1[2], double side_proj_pos2[2], 
                     double person_proj_pos[2], pointlist2d_t **points, int nchannels, int verbose)
{
    int poss_collision = 0;
    //get this from the config file

  
    //need to also check if we can move in that direction (from side pos - to projection)
    double ray_dist1 = get_ray_dist(side_proj_pos1, person_proj_pos,  points, nchannels, USED_SAFETY_RADIUS );

    double ray_dist2 = get_ray_dist(side_proj_pos2, person_proj_pos, points, nchannels, USED_SAFETY_RADIUS );

    if (verbose)
        fprintf(stderr,"\nSide 1: %f Side 2: %f\t Total Gap : %f\n\n", ray_dist1, ray_dist2, ray_dist1 + ray_dist2);

    return ray_dist1 + ray_dist2;
}


//check if there are obstacles near by 

//might be better to check with actual footprint 
int is_empty(double radius, double pos[2], double side_pos[2], pointlist2d_t **points, int nchannels, int verbose)
{
    int poss_collision = 0;
    //get this from the config file

    for (int lidx = 0; lidx < nchannels; lidx++){
        pointlist2d_t *pts = points[lidx];
        if(poss_collision){
            break;
        }
        for(int i=0; i<pts->npoints; i++) {
            double t = 0.0;
            t = hypot(pts->points[i].x - pos[0], pts->points[i].y - pos[1]);
            if(t <= radius){
                if (verbose)
                    fprintf(stderr, "Closest Point : %f [%f,%f]\n", t, pts->points[i].x, pts->points[i].y);
                return 0;
            }
        }
    }

    //need to also check if we can move in that direction (from side pos - to projection)
    double ray_dist = get_ray_dist(side_pos, pos, points, nchannels, USED_SAFETY_RADIUS);

    double act_dist = hypot(pos[0] - side_pos[0], pos[1] - side_pos[1]);
  
    if(ray_dist < act_dist){
        if (verbose)
            fprintf(stderr,"Rejected - path from side to projection not clear\n");
        return 0;
    }
  

    return 1;
}


//side by side following method 
int calculate_velocity_command(double utime, State *self, double lookahead_distance, double *angle, gboolean *turn_in_place, 
                                 double *brange, double *dist_to_target, double *rv, double *tv)
{    
    int very_verbose = self->very_verbose;
    if(very_verbose)
        fprintf(stderr,"----------------------------SIDE-BY-SIDE-----------------------------------\n");
    static double prev_rv = 0.0, prev_tv = 0.0;
    bot_lcmgl_t * lcmgl; //for drawing 
    double pos[3] = {.0, .0, .0};
    double bot_heading = 0.0;
    //if a stuck message was sent 
    static int stuck_msg_sent = 0;

    int suggested_action = 0; //0 is direction following, 1 is side by side

    static double prev_time = 0;

    int collision_det = 0;

    //person location - body frame 
    double person[2] = {self->guide_msg->pos[0], self->guide_msg->pos[1]};

    //personal zone radius
  
    //since there is already feedback from the closing velocity on the robot velocity 
    //having this only makes the chair stop as the radius grows and then start moving again
    //this is because the human can stop very suddenly 
    double personal_zone_radius = 1.0;//0;//fmax(1.0, self->guide_msg->closing_velocity * 2.0);

    double p_distance = hypot(person[0], person[1]);

    double act_dist = hypot(person[0], person[1]);

    //person heading (direction of velocity - not the actual direction the person is facing) - assumed to be stable (leg tracker is a bit unstable)
    double p_heading = self->guide_msg->person_heading;

    //direction of the person from the robot frame 
    double heading_to_person = atan2(person[1], person[0]);
  
    //this is no longer an averaged heading 
    p_heading =  self->avg_guide_heading;
    //add_heading_obs(self->guide_msg->person_heading, 0);
  
    GQueue *channels = laser_returns_copy (self->channels);
    GTimer *timer = g_timer_new ();

    //might want to delete these guys - seems like calloc'ed memory ?? 

    /* convert laser returns into point list */
    int nchannels=0;

    pointlist2d_t **points = laser_to_local_points (self, channels, &nchannels, 0);//, points, pf_points);
    pointlist2d_t **pf_points = laser_to_local_points (self, channels, &nchannels, 1);

    //these contain the raw unflitered laser returns - for the front laser (contains the feet observations) 
    //used to check if we can saftly rotate 
    GQueue *rchannels = laser_returns_copy (self->rchannels);
    int nrchannels = 0;
    pointlist2d_t **rpoints = NULL;

    if (!points) {
        fprintf (stderr, "failed to convert laser returns.\n");
        g_timer_destroy (timer);
        return -1;
    }

    /* draw the obstacles */
    //draw_obs(self, points, nchannels);
    draw_obs(self, pf_points, nchannels);
  
    double min_side_dist = 0;
    // --------- check for collisions ---------------- //
    int proximity = check_collision(points, nchannels, rpoints, nrchannels, &min_side_dist);

    if(proximity){//possible collision - returning 
        /*if(stuck_msg_sent==0){
          publish_speech_following_msg("TOO_CLOSE", self);
          stuck_msg_sent =1;
          }*/
        collision_det = 1;

        if(collision_det){
            fprintf(stderr,"Collision Detected\n");
            publish_speech_following_msg("TOO_CLOSE", self);
        }
        
        fprintf(stderr,"============ Emergency Stop ============== \n");

        /* free points */
        free_points(points, nchannels);
        free_points(pf_points, nchannels);
        free_points(rpoints, nrchannels);
        laser_returns_destroy (channels);
        laser_returns_destroy (rchannels);

        lcmgl = self->lcmgl_navigator;
        bot_lcmgl_switch_buffer (lcmgl);
    
        draw_emergency_navigator_msg(self, pos);

        return 1; //collision - emergency stop 
    }
    else{
        stuck_msg_sent = 0;
    }

    /*
      Overall Algorithm 
      -----------------

      + Check new mode (depends on the previous mode) 
      + Select new target - based on the current mode and velocity
      + Select actual target moderated on obstacles (the person observations are removed if direction following - kept is side by side) 
      + Convert to velocity 
    */
  
    prev_time = utime; 

    double goal_heading;
    //distance to target
    if (self->verbose)
        fprintf(stderr,"\n Mode : Following Direction\n\n");
    *dist_to_target = act_dist;
    goal_heading =atan2(person[1], person[0]);

    act_dist = *dist_to_target; //set the actual distance - to be our target distance 

    if (self->verbose)
        fprintf(stderr,"Distance to Target : %f\n", act_dist);  

    //do the calculation up to a look ahead 
    double max_range = lookahead_distance;
    //double goal_heading = //self->goal_heading;
    double goal_pt[2] = {pos[0] + cos (goal_heading) * max_range,
                         pos[1] + sin (goal_heading) * max_range};  
  
    double angle_goal_robot = bot_mod2pi(goal_heading - bot_heading);   

    //set only rv if we are withing rot-radius - we have the collision check before to stop the robot in case of collision

    //check in the half circle model 
    double dist_from_edge = 0;
    //int in_personal_space = check_in_personal_space_half_circle(act_dist, heading_to_person, p_heading, 
    //							      person, 
    //							      &dist_from_edge);
    int in_personal_space = check_in_personal_space_from_velocity(act_dist, heading_to_person, p_heading, 
                                                                  person, 
                                                                  personal_zone_radius,
                                                                  &dist_from_edge);
  

    if (self->verbose)
        fprintf(stderr,"In personal Space : %d\n", in_personal_space);

    if( in_personal_space || 
       (fabs(angle_goal_robot) > bot_to_radians (TURN_IN_PLACE_DEG))
       || self->turn_in_place){
        //we are doing it here twice
        *tv = 0;
        *rv = 0;
        if (self->verbose)
            fprintf(stderr,"\t+++  Trying to turn in place\n");
        int turn_angle = 0;
        if(fabs(angle_goal_robot) > bot_to_radians (TURN_IN_PLACE_DEG)){
            turn_angle = 1;
        }
        if (self->verbose)
            fprintf(stderr,"Was Turn-in-place:%d\tPersonal Space : %d\tTurn-angle:%d\n",self->turn_in_place, 
                    in_personal_space,turn_angle );

        int collision = check_rot_collision(points, nchannels, rpoints, nrchannels);
    
        if(collision){//possible collision if we rotate - so doing nothing
            *rv = 0;
            char seg_info[512];
            sprintf(seg_info,"Within-Rotational space - but unable to turn - Collision\n");
            draw_text_navigator_msg(self, pos, seg_info);
      
            /* free points */
            free_points(points, nchannels);
            free_points(pf_points, nchannels);
            free_points(rpoints, nrchannels);
            laser_returns_destroy (channels);
            laser_returns_destroy (rchannels);
      
            lcmgl = self->lcmgl_navigator;
            bot_lcmgl_switch_buffer (lcmgl);

            *turn_in_place = FALSE;
            self->turn_in_place = 0;
            draw_rotation(self, self->turn_in_place, 0);
            return 0;
        }

        else if(fabs(angle_goal_robot) <= bot_to_radians(10.0)){
            *turn_in_place = FALSE;
            self->turn_in_place = 0;
            *rv = 0;

            /* free points */
            free_points(points, nchannels);
            free_points(pf_points, nchannels);
            free_points(rpoints, nrchannels);
            laser_returns_destroy (channels);
            laser_returns_destroy (rchannels);

            lcmgl = self->lcmgl_navigator;
            bot_lcmgl_switch_buffer (lcmgl);

            char seg_info[512];
            sprintf(seg_info,"Within-Rotational space - But too small an angle to turn\n");
            draw_text_navigator_msg(self, pos, seg_info);
            draw_rotation(self, self->turn_in_place, 0);
            return 0;
        }
    

        //ok to rotate in place

        /* free points */
        free_points(points, nchannels);
        free_points(pf_points, nchannels);
        free_points(rpoints, nrchannels);
        laser_returns_destroy (channels);
        laser_returns_destroy (rchannels);
   
    
        *turn_in_place = TRUE;
        *rv = get_rot_vel(angle_goal_robot);
        self->turn_in_place = 1;

        lcmgl = self->lcmgl_navigator_info;
        char seg_info[512];
        sprintf(seg_info,"Within Personal Space: Turning in place Tv: %f, Rv: %f\n", 
                *tv, *rv);
        draw_text_navigator_msg(self, pos, seg_info);
        draw_rotation(self, self->turn_in_place, 0);    
        return 0;
    }
  
    draw_rotation(self, self->turn_in_place, 1);

    //person is over the rot-radius - we need to score and pick the best heading 

    // ---------- Checking rotation crietrian ------------ //
    int draw_rotation_p = 0;

    double used_rot_radius;

    if(!self->keep_close){
        used_rot_radius = ROT_RADIUS;
    }
    else{
        used_rot_radius = C_ROT_RADIUS;
    }
    int collision = 0;

    // ---------------- Do scoring ---------------- //
  
    //we can just check around the person - span can at worst be 90 - around the person 

    //span to check around the wheelchair
    double span = bot_to_radians (90.0);//(180.0); //360 seems like over kill 
    //No of buckets 
    double dtheta = bot_to_radians(BUCKET_SIZE); //(2.5);//bucket size
    //double dtheta = bot_to_radians (5.0);//bucket size
    int nrays = 2*span/dtheta;
    //we span our search around the current heading of the person
    double theta0 = goal_heading-span;
    int index_0 = 0;
    double thetas[nrays];
    double ranges[nrays];
    double xys[nrays][2];

    //------------------------------------------------------------------------//
    //Should switch what laser returns we are using 


    //not sure why we calculate this  
    double closest_pt_dist = INFINITY;
    point2d_t robot_pt = { pos[0], pos[1] };
    for (int lidx = 0; lidx < nchannels; lidx++) {
        pointlist2d_t *pts;
        if(self->was_following_side)
            pts = points[lidx];
        else
            pts = pf_points[lidx];
        //we can skip the rear 
    
        for(int i=0; i<pts->npoints; i++) {
            double d = geom_point_point_distance_2d(&robot_pt, &pts->points[i]);
            closest_pt_dist = fmin(d, closest_pt_dist);
        }
    }

    double safety_radius;

    safety_radius = fmin(closest_pt_dist - 0.01, SAFETY_RADIUS);  
    //min of the safety distance and the closest of the points

    for (int ridx=0;ridx<nrays;ridx++) {  
        double theta = theta0 + ridx * dtheta;
        thetas[ridx] = theta;
        if(theta==0){
            index_0 = ridx;
        }
        double s = sin (theta);
        double c = cos (theta);

        point2d_t ray_start = { pos[0], pos[1]};  //robot position
        point2d_t ray_dir = { c, s };

        double dist = max_range;

        int poss_collision = 0;
        //get this from the config file
        double alpha = atan2(0.6, 1.0);

        //--------------------------------------------------------------------------------//
        //Find maximum collision free distance for each ray

        //search for each ray for each point - for an intersection 
        for (int lidx = 0; lidx < nchannels; lidx++){
            pointlist2d_t *pts;
            if(self->was_following_side)
                pts = points[lidx];
            else
                pts = pf_points[lidx];

            if(poss_collision){
                break;
            }
            for(int i=0; i<pts->npoints; i++) {
                double t = 0.0;
                //if the ray can lead to a collision find the distance of the colliding point (t)
                if(geom_ray_circle_intersect_2d(&ray_start, &ray_dir, 
                                                &pts->points[i], safety_radius, &t, NULL)){
                    dist = fmin(dist, t);  //this finds the closest colliding point - for each rays 
                    //dist can never be more than the lookahead distance
                }
                //not sure if this following check is entirely correct - need to check it more thourghly

                //*****Sachi removed - not in bot-core - introduced in common3d - which we are not using here 
                if(geom_ray_rect_intersect_2d(&ray_start, &ray_dir, 
                                              &pts->points[i], 1.0, 0.6, alpha, theta)){
	    
                    //possible to skip this whole calculation
                    poss_collision = 1;
                    dist = 0;
                    break;
                }
            }


            if(self->was_following_side){//check for person here as well 
                double t = 0.0;	
	
                double c1 = cos(p_heading);
                double s1 = sin(p_heading);

                double l = 0;
                int no_iter = 15;//LOOKAHEAD_DISTANCE / 0.2;

                for(int i=0;i<no_iter;i++){
                    point2d_t person_pt = {person[0] + l * c1, person[1] + l * s1};
                    l+=0.2;

                    double person_radius = 0.1; 
                    if(geom_ray_circle_intersect_2d(&ray_start, &ray_dir, 
                                                    &person_pt, safety_radius + person_radius , &t, NULL)){
                        dist = fmin(dist, t);
                    }	  
                }	
            }	
        }
        ranges[ridx] = dist;
        xys[ridx][0] = pos[0] + c * dist;
        xys[ridx][1] = pos[1] + s * dist;
    }
  
    /* find the best ray */
    double best_range = 0;
    double best_dtheta = HUGE;
  
    /* compute score for each ray */
    double scores[nrays];

    double alpha = 1.0;

    int closest_p_ind = -1;
    double closest_dist = 1000.0;

    //--------------------------------------------------------------------------------//
    //Score each ray based on the maximum collision free distance and how far it is from 
    //the goal

    for (int ridx = 0; ridx < nrays; ridx++) {

        double dist_sq = powf(xys[ridx][0] - goal_pt[0],2) +
            powf(xys[ridx][1] - goal_pt[1],2);  

        if(closest_dist > dist_sq){
            closest_p_ind = ridx;
            closest_dist = dist_sq;
        }

        double score = ranges[ridx] / (1.0 + alpha * dist_sq);   
        scores[ridx] = score;
    }
  
    /* find best direction */

    double avg_scores[nrays];
    int lb = 2; //reduced the averaging as we reduced the beam resolution 
    if(BUCKET_SIZE == 2.5){
        lb = 5;
    }

    //--------------------------------------------------------------------------------//
    // Average the scores - we pick the best average score - to prevent corner cutting   

    for (int ridx=0;ridx<nrays;ridx++) {
        int temp_count = 0;
        float temp_avg_score = 0.0;
        for (int i= ridx-lb; i <= ridx+lb;i++){
            temp_count++;
            temp_avg_score += scores[get_ind(i,nrays)];
        }    
        avg_scores[ridx] = temp_avg_score/temp_count;
    }

    double best_score = -1000;
    int best_ridx = -1;
  
    for (int ridx=0;ridx<nrays;ridx++) {    
        if (best_score < avg_scores[ridx]) {//changed to take the average          
            best_ridx = ridx;
            best_score = avg_scores[ridx];      
        }
    }
  
    if(self->draw_obs){//drawing the scores
        //draw_obs_scores(self, xys, scores, avg_scores, nrays);
        lcmgl = self->lcmgl_scores;
        if(lcmgl){
            for (int ridx=0;ridx<nrays;ridx++) {
                double score_g[3] = {.0,.0,.0};
                local_to_global(xys[ridx][0], xys[ridx][1],&score_g[0], &score_g[1],self);      
                char score_info[512];
                sprintf(score_info,"%.3f-%.3f", scores[ridx],avg_scores[ridx]);
                lcmglColor3f(1, 0, 0); 
                bot_lcmgl_text(lcmgl, score_g, score_info);
            }
        }
        bot_lcmgl_switch_buffer (lcmgl);
    }

#ifdef PRINT_OP
    fprintf(stderr,"Best Score : %d,  %f\n", best_ridx, best_score);
#endif
  
    best_dtheta = bot_mod2pi(thetas[best_ridx] - bot_heading); 

    //the larger this is the more we should rotate - to get there quicker
    best_range = ranges[best_ridx];  //higher this is the faster we can move forward  

    lcmgl = self->lcmgl_navigator;

    // draw the rays 
    if (lcmgl) {
        // draw rays in light blue
        double robot_gx = .0, robot_gy = .0;
        local_to_global(pos[0], pos[1],&robot_gx, &robot_gy,self);
    
        for (int ridx = 0; ridx < nrays; ridx++) {
            if (ridx == best_ridx) {
                lcmglLineWidth (3);
                lcmglColor3f(1, .5, .5);
            }
            else {
                lcmglLineWidth (1);
                lcmglColor3f(.3, .3, 1);
            }
            lcmglBegin(GL_LINES);

            double p_gx = .0, p_gy = .0;
            local_to_global(xys[ridx][0], xys[ridx][1] ,&p_gx, &p_gy,self);
            lcmglVertex3d(robot_gx, robot_gy, pos[2]);
            lcmglVertex3d(p_gx, p_gy, pos[2]);
            lcmglEnd();
        }
        lcmglColor3f(1, .5, .5);
        lcmglPointSize(8);
        lcmglBegin(GL_POINTS);

        double p_gx = .0, p_gy = .0;
        local_to_global(xys[best_ridx][0], xys[best_ridx][1] ,&p_gx, &p_gy,self);
        lcmglVertex3d(p_gx, p_gy, pos[2]);
        lcmglEnd();

        // draw target direction as a yellow ray 
        lcmglColor3f(1, 1, 0);
        lcmglLineWidth(1);
        lcmglBegin(GL_LINES);

        lcmglVertex3d(robot_gx, robot_gy, pos[2]);
        local_to_global(goal_pt[0],goal_pt[1],&p_gx, &p_gy,self);
        lcmglVertex3d(p_gx, p_gy, pos[2]);
        lcmglEnd();

        bot_lcmgl_switch_buffer (lcmgl);
    }
 
  
    if (angle)
        *angle = best_dtheta;

    if (brange)
        *brange = best_range;

    //we need to compute the gap between the best_ray and the goal_heading and not move if we 
    double angle_best_goal = bot_mod2pi(thetas[best_ridx] - goal_heading); 

    /*break out from turn in place once the person is close enough */
    //wont try to turn in place if the angle is too small - prevents the wheelchair trying to move but failing

    if(fabs(angle_goal_robot) <= bot_to_radians(10.0)){  //was 15.0 - but now our dynamics are better 
        self->turn_in_place = 0;
    }
  
    /* free points */
    free_points(points, nchannels);
    free_points(pf_points, nchannels);
    free_points(rpoints, nrchannels);
    laser_returns_destroy (channels);
    laser_returns_destroy (rchannels);
  
  
    //if the wheelchair is facing the opposite way and there is no space to turn, the wheelchair needs to move forward 
    //and then do a turn 
  
    //also it might be better to have the wheelchair move forward and then do a turn if it can't turn in place

    //prevents the wheelchair from turning away from us because the gap is too small
    if((fabs(angle_best_goal) > bot_to_radians(90.0)) && (fabs(angle_goal_robot) < bot_to_radians(90.0))){  //issue possible for it to try to turn to some side 
        self->too_small_gap = 1;
    }  
    else{
        self->too_small_gap = 0;
    }

    double used_tv_max;
    double used_rv_max;

    double drop_off_dist = 0.5; //was 1.0 need to check how this impacts *********
    if(self->keep_close){
        drop_off_dist = 1.5;
    }
    double used_dist;
    if(self->keep_close){    
        used_dist = fmin(act_dist - used_rot_radius, drop_off_dist);
    }
    else{
        if(self->was_following_side){
            used_dist = fmin(act_dist - used_rot_radius + 0.1, drop_off_dist);
        }
        else{
            used_dist = fmin(dist_from_edge + 0.1, drop_off_dist);
        }
    }
  
    used_dist = fmax(.0, used_dist);
 
#ifdef PRINT_OP  
    fprintf(stderr,"Used Rot Radius : %f, Dist_from_rot : %f, Used Dist : %f\n", drop_off_dist, act_dist - used_rot_radius, used_dist);
#endif
    //this needs to change based on the keep close - needs to drop off to zero someone inside the rotate_in_place boundry
    //also needs to provide smooth acceleration and decelleration
  
    used_tv_max = MAX_TRANSLATION_SPEED; //used_dist/drop_off_dist * MAX_TRANSLATION_SPEED;
    used_rv_max = MAX_ROTATION_SPEED; //

    //we can use this but - this controller needs to be distance based - or match the person's velocity somehow
    double max_allowed_tv = MAX_FAST_TRANSLATION_SPEED;//MAX_TRANSLATION_SPEED;
    double max_allowed_rv = MAX_ROTATION_SPEED; 
  
 
    //this max tv velocity should be -Approach_velocity + Gain * (distance from personal_zone)
    double P_gain = 1.0;//1.2;
    double D_gain = 0.0; 
    double closing_v  = -self->guide_msg->closing_velocity;
    if(fabs(closing_v) < 0.2){
        closing_v = 0;
    }
    
    //hmm - should we do this - stop the robot if the person has stopped??
    if(dist_from_edge < 0.1 && self->guide_msg->vel_mag < 0.1){
        max_allowed_tv = 0; 
    }
    else{
        //self->guide_msg->closing_velocity can drop pretty rapidly - if the person stops while the robot is moving 
        //max_allowed_tv = fmin(fmax(-self->guide_msg->closing_velocity + P_gain * dist_from_edge,0), FULL_MAX_TRANSLATION_SPEED) ;
        
        max_allowed_tv = fmin(fmax(-self->guide_msg->closing_velocity * D_gain + P_gain * dist_from_edge,0), FULL_MAX_TRANSLATION_SPEED) ;
    }
    //double p_vel_component = cos(p_heading) * self->guide_msg->vel_mag; 
    //max_allowed_tv = fmin(fmax(p_vel_component + P_gain * dist_from_edge,0), FULL_MAX_TRANSLATION_SPEED) ;
    
    if (self->very_verbose)
        fprintf(stderr," Escaping Velocity : %f Error Component : %f Max Allowed : %f\n", -self->guide_msg->closing_velocity, P_gain * dist_from_edge, max_allowed_tv);
    
    // **************** Calculating tv and rv ****************** //

    //if within rot_radius and cant turn in place we do nothing
    //only hitting this if we are doing direction following
    
    if(self->too_small_gap){
        if (self->verbose)
            fprintf(stderr,"Small Gap Detected\n");
        publish_speech_following_msg("SMALL_GAP", self);
        update_following_state_new(STOP,self); //stopping the wheelchair 
    }
    else{  //check if there is a person in our intended path 
        int person_collision = 0;//check_person_collision(self->person_msg, best_dtheta);
    
        if(person_collision){//stop wheelchair till the person is clear 
            if (self->verbose)
                fprintf(stderr,"Someone has crossed our path - Stopping till person moves");
            *rv = 0;
            *tv = 0;
        }
        else{
            *rv = get_rot_vel(*angle);
      
            if (*turn_in_place)
                *tv = .0;
            else { 
                double avg_best_gap = 0;
                int temp_count = 0;
                int lr = 2; //not sure what the best value to use is

                for (int i= best_ridx-lr; i <= best_ridx+lr ;i++){
                    temp_count++;
                    avg_best_gap += ranges[get_ind(i,nrays)];
                }
                avg_best_gap = avg_best_gap / temp_count;

                //front gap - this is also relavent 
                double avg_front_gap = 0;
                temp_count = 0;
	  
                for (int i= index_0-lr; i <= index_0+lr ;i++){
                    temp_count++;
                    avg_front_gap += ranges[get_ind(i,nrays)];
                }
                avg_front_gap = avg_front_gap / temp_count; 
       
                //this has broken the side-by-side following 
                *tv = get_trans_vel_arc_with_obs_direction(self,best_dtheta, avg_best_gap, avg_front_gap, p_distance, heading_to_person, rv, max_allowed_tv, min_side_dist);	
                //get_trans_vel_arc_with_obs_fixed(self,best_dtheta, avg_best_gap, avg_front_gap, p_distance, heading_to_person, rv, max_allowed_tv);	

                if(self->guide_msg->vel_mag == 0 && *tv< 0.1){
                    *tv = 0.0;
                }
	  
                //*tv = max_allowed_tv; 
                //*rv = get_rot_vel_arc(self, g_pos, t_dist, *tv); 
	
                //draw_arc(self, )
	
#ifdef PRINT_OP
                fprintf(stderr,"DT %f, TV : %f \n", dt, *tv);
#endif
            }
        }
    }

    draw_arc(self, *tv, *rv);
  
    prev_rv = *rv;
    prev_tv = *tv;
  
    lcmgl = self->lcmgl_navigator_info;
    if(lcmgl){
        int rot_rad = 0;
        if(act_dist < used_rot_radius){
            rot_rad =1;
        }
        double g_robot[3] = {.0,.0,.0};
        local_to_global(pos[0]+5.0, pos[1]+5.0,&g_robot[0], &g_robot[1],self);
        char seg_info[512];

        sprintf(seg_info,"Following Sides : %d \nAct Dist : %f, Turn In place : %d, Collision : %d, Tv : %f, RV : %f", 
                self->was_following_side, act_dist, *turn_in_place, collision, *tv,*rv);
    
#ifdef PRINT_OP
        fprintf(stderr,"Status : %s\n", seg_info);
#endif
        lcmglColor3f(1, 0, 0); 
        bot_lcmgl_text(lcmgl, g_robot, seg_info);
    }
    bot_lcmgl_switch_buffer (lcmgl);

    return 0;
  
}

//side by side following method 
int calculate_velocity_command_side(double utime, State *self, double lookahead_distance, double *angle, gboolean *turn_in_place, 
                                 double *brange, double *dist_to_target, double *rv, double *tv)
{    
    int very_verbose = self->very_verbose;
    if(very_verbose)
        fprintf(stderr,"----------------------------SIDE-BY-SIDE-----------------------------------\n");
    static double prev_rv = 0.0, prev_tv = 0.0;
    bot_lcmgl_t * lcmgl; //for drawing 
    double pos[3] = {.0, .0, .0};
    double bot_heading = 0.0;
    //if a stuck message was sent 
    static int stuck_msg_sent = 0;

    static double time_of_last_side_fail = 0; //the last time when the robot switched from side to direction 

    //keep track of the time of the first side clear - so we switch to side following - only if side has been clear for ? 0.5 s and 
    //vice versa - prob a lower threashold for switching from side to direction following 
    static double time_side_1_clear = 0;
    static double time_side_2_clear = 0;
    static double time_side_1_unclear = 0;
    static double time_side_2_unclear = 0;

    static double time_direction = 0;

    int suggested_action = 0; //0 is direction following, 1 is side by side
    int side_1_clear = 0;
    int side_2_clear = 0;

    static double prev_time = 0;

    int collision_det = 0;

    //person location - robot frame 
    double person[2] = {self->guide_msg->pos[0], self->guide_msg->pos[1]};

    //personal zone radius - this is now a variable distance based on the 
    //relative velocity of the chair to the person 
    //we will keep this at a 1.0 minimum
  
    //since there is already feedback from the closing velocity on the robot velocity 
    //having this only makes the chair stop as the radius grows and then start moving again
    //this is because the human can stop very suddenly 
    double personal_zone_radius = 0;//fmax(1.0, self->guide_msg->closing_velocity * 2.0);

    double p_distance = hypot(person[0], person[1]);

    double act_dist = hypot(person[0], person[1]);

    //person heading (direction of velocity - not the actual direction the person is facing) - assumed to be stable (leg tracker is a bit unstable)
    double p_heading = self->guide_msg->person_heading;

    //direction of the person from the robot frame 
    double heading_to_person = atan2(person[1], person[0]);
  
    //this is no longer an averaged heading 
    p_heading =  self->avg_guide_heading;
    //add_heading_obs(self->guide_msg->person_heading, 0);
  
    //for the two side locations 
    double target_side_pos[2] = {.0,.0};
    //the projected position that we pick as our actual target 
    double target_proj_pos[2] = {.0,.0};

    GQueue *channels = laser_returns_copy (self->channels);
    GTimer *timer = g_timer_new ();

    /* convert laser returns into point list */
    int nchannels=0;

    pointlist2d_t **points = laser_to_local_points (self, channels, &nchannels, 0);//, points, pf_points);
    pointlist2d_t **pf_points = laser_to_local_points (self, channels, &nchannels, 1);

    //these contain the raw unflitered laser returns - for the front laser (contains the feet observations) 
    //used to check if we can saftly rotate 
    GQueue *rchannels = laser_returns_copy (self->rchannels);
    int nrchannels = 0;
    pointlist2d_t **rpoints = NULL;

    if (!points) {
        fprintf (stderr, "failed to convert laser returns.\n");
        g_timer_destroy (timer);
        return -1;
    }

    /* draw the obstacles */
    //draw_obs(self, points, nchannels);
    draw_obs(self, pf_points, nchannels);
  
    double min_side_dist = 0;
    // --------- check for collisions ---------------- //
    int proximity = check_collision(points, nchannels, rpoints, nrchannels, &min_side_dist);

    if(proximity){//possible collision - returning 
        /*if(stuck_msg_sent==0){
          publish_speech_following_msg("TOO_CLOSE", self);
          stuck_msg_sent =1;
          }*/
        collision_det = 1;
        
        fprintf(stderr,"============ Emergency Stop ============== \n");

        /* free points */
        free_points(points, nchannels);
        free_points(pf_points, nchannels);
        free_points(rpoints, nrchannels);
        laser_returns_destroy (channels);
        laser_returns_destroy (rchannels);

        lcmgl = self->lcmgl_navigator;
        bot_lcmgl_switch_buffer (lcmgl);
    
        draw_emergency_navigator_msg(self, pos);

        return 1; //collision - emergency stop 
    }
    else{
        stuck_msg_sent = 0;
    }

    //---------------------------------------------------------------------------------------------------//
    //---------------Check to see if side following should be done---------------------------------------//

    //*************************************************************************************************//
    // This part contains the logic that decides the type of following we should do and the intended 
    // location of the goal 
    //*************************************************************************************************//

    /* Logic : 
     
       Distance from person: > threshold: (different thresholds - based on previous state)
     
       - Unable to catch up - do direction following

       if was direction_following: 
       if angle to person > threshold - do direction following 

       //threshold should be higher than our rotation distance - otherwise will always stop before trying to catch up 

       Person within catching distance (this means that given the correct controls, the robot:

       -Current Mode: 
     
       +++ Direction Following : 
     
       Check both side locations and projection in to the future 
       -Either that or wait for n number of all-clears to switch to side by side following 
     
       --- If atleast one side is reachable, switch over to side-by-side; set the goal to be infront of the side position
       (to make the wheelchair match velocity with the person) 
     
       --- If neither side is reachable - continue direction following 
     
       +++ Side-by-side following :

       Check if the side that we were following is free and the projection is free 
       project n(seconds) to the future

       => If reachable - set this as the target (the projection or some part there of)

       => If not reachable - switch to direction following - which should automatically cause the robot to fall back and follow 
       behind // but if the robot is side by side already - should prob slow down before trying to turn (since the heading is high
       - it will try to turn in place)
	 
       +++ No motion : Need to figure out the best stratergy for this situation - should we face the guide in this case? or simply go 
       wait in the side-by-side location? - would the side by side location even be valid then? - maybe we should time out
	
    */


    /*
      Overall Algorithm 
      -----------------

      + Check new mode (depends on the previous mode) 
      + Select new target - based on the current mode and velocity
      + Select actual target moderated on obstacles (the person observations are removed if direction following - kept is side by side) 
      + Convert to velocity 
    */
  
    
    //checking if we are within the catch up distance and reasonable heading of the person 
    //if((!self->was_following_side && act_dist < catchup_distance)//we were doing direction following and are with in catch up distance 
    //  || (self->was_following_side && act_dist < switch_back_distance)){ // the person is within the catch up distance - we check if we should chenge mode 
    //caluclating the various needed locations 

    //angles to the side (from the person's current heading)
    double angles_to_side = bot_to_radians(90);//M_PI/2; //can be greater causing the chair to follow the person at an angle  

    //distance to the sides 
    double side_target_dist = 0.8 / cos(angles_to_side - M_PI/2); 

    double dist_behind_person = 0.8 * tan(angles_to_side - M_PI/2); 

    //actual angles to side points 
    double p1_alpha = bot_mod2pi(p_heading + angles_to_side); //left
    double p2_alpha = bot_mod2pi(p_heading - angles_to_side); //right
    
    double side_1[2] = {.0,.0};
    double side_1_forward[2] = {.0,.0};
    double side_1_forward_check[2] = {.0,.0};
    double side_2[2] = {.0,.0};
    double side_2_forward[2] = {.0,.0};
    double side_2_forward_check[2] = {.0,.0};

    //side-by-side target positions 
    side_1[0] = person[0]+side_target_dist * cos(p1_alpha);
    side_1[1] = person[1]+side_target_dist * sin(p1_alpha);

    side_2[0] = person[0]+side_target_dist * cos(p2_alpha);
    side_2[1] = person[1]+side_target_dist * sin(p2_alpha);  

    //lets check if these two points are reachable 
    double side_1_ray = get_ray_dist(side_1, pos, points, nchannels, USED_SAFETY_RADIUS);
    double side_2_ray = get_ray_dist(side_2, pos, points, nchannels, USED_SAFETY_RADIUS);

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //distance to side targets 
    double side_1_act = hypot(side_1[0],side_1[1]); 
    double side_2_act = hypot(side_2[0],side_2[1]); 

    if(very_verbose){
        fprintf(stderr,"\t Target 1 (act) : %f, (ray) : %f\n", side_1_act, side_1_ray);
        fprintf(stderr,"\t Target 2 (act) : %f, (ray) : %f\n", side_2_act, side_2_ray);
    }

    //forward project our targets in time  - lets try for 3s distance 
    
    //3s might be too much for this - although the target at 3s ahead seems to keep the chair at good velocity
    double projection_time = 2.0;//3.0;
    double far_projection_time = 4.0;//3.0;
    //we put a max here - but not sure how this will effect the controller 
    double far_projection_distance = 0;
    double projection_distance = 0;
    if(self->was_following_side){
        //if following distance - look somewhat ahead 
        projection_distance = 0.5;//1.5;//fmin(self->guide_msg->vel_mag * projection_time, 1.2);
    }
    else{
        //if not following distance - look further ahead 
        projection_distance = 2.5;//fmin(self->guide_msg->vel_mag * far_projection_time, 2.5);
    }

    //double close_projection_distance = fmin(self->guide_msg->vel_mag * projection_time, 1.0);
    far_projection_distance = fmin(self->guide_msg->vel_mag * far_projection_time, 2.0);

  
    double c = cos(p_heading);
    double s = sin(p_heading);

    double p_projection[2] = {.0,.0};

    //person projection 

    p_projection[0] = person[0] + projection_distance * c;
    p_projection[1] = person[1] + projection_distance * s;
  
    //side-by-side forward target positions 
    side_1_forward[0] = side_1[0] + far_projection_distance * c;
    side_1_forward[1] = side_1[1] + far_projection_distance * s;

    //side-by-side forward target positions 
    side_1_forward_check[0] = side_1[0] + projection_distance * c;
    side_1_forward_check[1] = side_1[1] + projection_distance * s;

    double side_1_heading = atan2(side_1_forward[1], side_1_forward[0]);

    side_2_forward[0] = side_2[0] + far_projection_distance * c;
    side_2_forward[1] = side_2[1] + far_projection_distance * s;

    side_2_forward_check[0] = side_2[0] + projection_distance * c;
    side_2_forward_check[1] = side_2[1] + projection_distance * s;

    double side_2_heading = atan2(side_2_forward[1], side_2_forward[0]);

    double side1_gap = 0, side2_gap = 0;

    //double front_gap = get_front_gap(side_1_forward_check, side_2_forward_check, p_projection, pf_points, nchannels, self->very_verbose);

    //we now have a mode that forces it to do direction following only 
    if(!self->do_direction_only && check_side_by_side(self->was_following_side,act_dist, heading_to_person, p_heading, self->guide_msg->vel_mag, self->very_verbose)){ 
 
        //int side_1_forward_empty = is_empty_and_reachable(0.3, side_1_forward, side_1, person, pf_points, nchannels, &side1_gap);
        //int side_2_forward_empty = is_empty_and_reachable(0.3, side_2_forward, side_2, person, pf_points, nchannels, &side2_gap);

        //use these to decide to follow sides 

        double side_1_forward_dist = 0;
        double side_2_forward_dist = 0;
    
        int side_1_forward_empty = is_empty_and_reachable_complete(0.3, side_1_forward, side_1, person, p_projection, pf_points, 
                                                                   nchannels, &side1_gap, &side_1_forward_dist);
        int side_2_forward_empty = is_empty_and_reachable_complete(0.3, side_2_forward, side_2, person, p_projection, pf_points, 
                                                                   nchannels, &side2_gap, &side_2_forward_dist);
    

        lcmgl = self->lcmgl_side_rays;
        if(lcmgl){
            //draw the two side rays 
            double side_1_ray[2] = {.0,.0};
            double side_2_ray[2] = {.0,.0};

            side_1_ray[0] = person[0]+side1_gap * cos(p1_alpha);
            side_1_ray[1] = person[1]+side1_gap * sin(p1_alpha);

            side_2_ray[0] = person[0]+side2_gap * cos(p2_alpha);
            side_2_ray[1] = person[1]+side2_gap * sin(p2_alpha);  

            double xyz[3] = {.0,.0,.0};
            //draw the sides and forward locations 
            lcmglLineWidth (5);
            lcmglColor3f(1.0, .0, .0); 

      
            lcmglBegin(GL_LINES);
            local_to_global(person[0], person[1],&xyz[0], &xyz[1],self);  
            lcmglVertex3d(xyz[0], xyz[1], xyz[2]);
            local_to_global(side_1_ray[0], side_1_ray[1],&xyz[0], &xyz[1],self);  
            lcmglVertex3d(xyz[0], xyz[1], xyz[2]);
            lcmglEnd();

            lcmglBegin(GL_LINES);
            local_to_global(person[0], person[1],&xyz[0], &xyz[1],self);  
            lcmglVertex3d(xyz[0], xyz[1], xyz[2]);
            local_to_global(side_2_ray[0], side_2_ray[1],&xyz[0], &xyz[1],self);  
            lcmglVertex3d(xyz[0], xyz[1], xyz[2]);
            lcmglEnd();

            bot_lcmgl_switch_buffer (lcmgl);
        }
    
        /*int side_1_forward_empty = 0;
          int side_2_forward_empty = 0;
    
          if(front_gap > 3.0){
          side_1_forward_empty = 1;
          side_2_forward_empty = 1;
          }*/

        /*int side_1_forward_empty = is_empty_and_reachable(0.3, side_1_forward_check, side_1, person, pf_points, nchannels);
          int side_2_forward_empty = is_empty_and_reachable(0.3, side_2_forward_check, side_2, person, pf_points, nchannels);*/

    
        if(very_verbose){
            fprintf(stderr,"Forward projections Empty : %d , %d \n", side_1_forward_empty, side_2_forward_empty);
            
            fprintf(stderr,"==== Person Heading (deg): %f\n" , p_heading * 180 / M_PI);
        }

        if(fabs(p_heading) > bot_to_radians(30) && !self->was_following_side){//if we were doing direction following and at too high an angle - do direction following 
            suggested_action = 0;
            if (very_verbose)
                fprintf(stderr, "Was direction following - still at too large an angle from the person direction\n");
        }

        else if(fabs(p_heading) > bot_to_radians(45) && self->was_following_side){//if we were doing side following and at too high an angle - do direction following 
            suggested_action = 0;
        }

        else if(side_1_ray < side_1_act && side_2_ray < side_2_act){
            if (very_verbose)
                fprintf(stderr,"== Both targets un-reachable\n");
            suggested_action = 0;
        }
    
        if((side_1_ray >= side_1_act && fabs(side_1_heading) < bot_to_radians(90)) && side_1_forward_empty){
            suggested_action = 1;
            side_1_clear = 1;
        }

        if((side_2_ray >= side_2_act && fabs(side_2_heading) < bot_to_radians(90)) && side_2_forward_empty){
            suggested_action = 1;
            side_2_clear = 1;
        }
    }
    else{
        suggested_action = -1; //-1 action means - immediately switch    
        lcmgl = self->lcmgl_side_rays;
        bot_lcmgl_switch_buffer (lcmgl);
    }
  
    double dt = utime - prev_time;

    if(suggested_action == 1){
        time_direction = 0; //reset time direction 
        if(side_1_clear == 0){ //reset side 1 clear 
            //time_side_1 = 0;
            time_side_1_clear = 0;
            time_side_1_unclear +=dt;
        }
        else{
            time_side_1_unclear = 0;
            time_side_1_clear +=dt;
        }
        if(side_2_clear == 0){ //reset side 1 clear 
            time_side_2_clear = 0;
            time_side_2_unclear +=dt;
        }
        else{
            time_side_2_unclear = 0;
            time_side_2_clear +=dt;
        }
    }
    else if(suggested_action == 0){
        time_direction += dt;
        time_side_2_clear = 0;
        time_side_2_unclear += dt;
        time_side_1_clear = 0;
        time_side_1_unclear +=dt;
    }
    else if(suggested_action == -1){
        time_direction = 10e6;
        time_side_2_clear = 0;
        time_side_2_unclear = 10e6;
        time_side_1_clear = 0;
        time_side_1_unclear = 10e6;
    }
  
    prev_time = utime; 

    if (very_verbose) {
        fprintf(stderr, "Suggested Action : %d , Side 1 Clear : %d Side 2 Clear : %d\n", suggested_action, 
                side_1_clear, side_2_clear); 
        
        fprintf(stderr, "D Clear Time : %f, S_1 : %f S_2 : %f\n", time_direction, time_side_1_clear, time_side_2_clear); 
        fprintf(stderr, "D Unclear Time : %f, S_1 : %f S_2 : %f\n", time_direction, time_side_1_unclear, time_side_2_unclear); 
    }

    double stop_side_time_threshold = 1e6 / 2.0; 
    double start_side_time_threshold = 1e6; 
    double mode_switch_threshold = 10;//10; //in seconds 

    if (self->verbose)
        fprintf(stderr,"++++++ Time since last fail : %f\n", (utime - time_of_last_side_fail)/1e6);
  
    //we should check the clear distance on that side 

    if(self->was_following_side){//was following side by side 
        if(self->target_side == 1){ //if was following side 1
            if(time_side_1_clear < stop_side_time_threshold && time_side_1_unclear >= stop_side_time_threshold){ //side 1 is no longer clear - switch to direction 
                self->was_following_side = 0;
                self->target_side = -1;	
                time_of_last_side_fail = utime; 
            }    
            else{//keep following the same side 
                target_side_pos[0] = side_1[0];
                target_side_pos[1] = side_1[1];
	
                target_proj_pos[0] = side_1_forward[0];
                target_proj_pos[1] = side_1_forward[1];

                self->was_following_side = 1; 
                self->target_side = 1;
            }
        }
        if(self->target_side == 2){ //if was following side 1
            if(time_side_2_clear < stop_side_time_threshold && time_side_2_unclear >= stop_side_time_threshold){ //side 1 is no longer clear - switch to direction 
                self->was_following_side = 0;
                self->target_side = -1;	
                time_of_last_side_fail = utime; 
            }    
            else{//keep following the same side 
                target_side_pos[0] = side_2[0];
                target_side_pos[1] = side_2[1];
	    
                target_proj_pos[0] = side_2_forward[0];
                target_proj_pos[1] = side_2_forward[1];

                self->was_following_side = 1; 
                self->target_side = 2;
            }
        }
    }
    else{// if(self->guide_msg->vel_mag > 0.1) {//only transition from direction to following when the person is moving 
        if (self->very_verbose) {
            fprintf(stderr, "Dist to Sides : %f,%f Side gaps : %f,%f Angles to Side Forward : %f, %f\n", 
                    side_1_act, side_2_act, side1_gap, side2_gap, bot_to_degrees(side_1_heading), 
                    bot_to_degrees(side_2_heading));
        }

        if((time_side_1_clear > start_side_time_threshold && time_side_2_clear > start_side_time_threshold)
           && (utime - time_of_last_side_fail)/1e6 > mode_switch_threshold ){ 
            //we do not switch to side by side until at least 30s after the last switch - to supress too many 
            //switches back and forth 

            //self->was_following_side = 1;

            //side gap is more important - as this gives us a better chance of keep following for longer time 
            if(fabs(side1_gap - side2_gap) > 0.5){ //there is a signifcant difference in the side gap size 
                //we pick the best side gap - assuming we are at a close enough heading to it 
                if (self->very_verbose)
                    fprintf(stderr, "Significant Difference in the side spaces \n");

                if((side1_gap > side2_gap) && (fabs(side_1_heading) < bot_to_radians(45))){
                    if (self->very_verbose)
                        fprintf(stderr, "Selected Side 1 since it has more space\n");
                    target_side_pos[0] = side_1[0];
                    target_side_pos[1] = side_1[1];
	
                    target_proj_pos[0] = side_1_forward[0];
                    target_proj_pos[1] = side_1_forward[1];
                    self->was_following_side = 1;
                    self->target_side = 1;
                }
                else if(fabs(side_2_heading) < bot_to_radians(45)){
                    if (self->very_verbose)
                        fprintf(stderr, "Selected Side 2 since it has more space\n");
                    target_side_pos[0] = side_2[0];
                    target_side_pos[1] = side_2[1];
	    
                    target_proj_pos[0] = side_2_forward[0];
                    target_proj_pos[1] = side_2_forward[1];
                    if (self->very_verbose)
                        fprintf(stderr,"Side 2 empty Selecting Side 2\n");
                    self->was_following_side = 1;
                    self->target_side = 2;
                }
                else{
                    self->was_following_side = 0;
                    self->target_side = -1;
                }	
            }

            //we should switch back to distance 
            //pick the closest side - if its heading is not too much 
            //otherwise pick the longer side - if the heading is not too much 
            else if(fabs(side_1_act - side_2_act) < 0.5){ //if the distances are close enough 
                if (self->very_verbose)
                    fprintf(stderr, "One side is more closer than the other\n");
                //pick the one with the closer heading 
                if((fabs(side_1_heading) < fabs(side_2_heading)) && (fabs(side_1_heading) < bot_to_radians(45))){
                    if (self->very_verbose)
                        fprintf(stderr, "Selected Side 1 since it is closer\n");
                    target_side_pos[0] = side_1[0];
                    target_side_pos[1] = side_1[1];
	
                    target_proj_pos[0] = side_1_forward[0];
                    target_proj_pos[1] = side_1_forward[1];
                    self->was_following_side = 1;
                    self->target_side = 1;
                }
                else if(fabs(side_2_heading) < bot_to_radians(45)){
                    if (self->very_verbose)
                        fprintf(stderr, "Selected Side 2 since it is closer\n");
                    target_side_pos[0] = side_2[0];
                    target_side_pos[1] = side_2[1];
	    
                    target_proj_pos[0] = side_2_forward[0];
                    target_proj_pos[1] = side_2_forward[1];
                    self->was_following_side = 1;
                    self->target_side = 2;
                }	
                else{
                    self->was_following_side = 0;
                    self->target_side = -1;
                }
            }
            else{//pick the closer target
                //if(fabs(side_1_heading) < fabs(side_2_heading)){
                if((side_1_act < side_2_act) && (fabs(side_1_heading) < bot_to_radians(30))){
                    target_side_pos[0] = side_1[0];
                    target_side_pos[1] = side_1[1];
	
                    target_proj_pos[0] = side_1_forward[0];
                    target_proj_pos[1] = side_1_forward[1];
                    self->was_following_side = 1;
                    self->target_side = 1;
                }
                else if((side_2_act < side_1_act) && (fabs(side_2_heading) < bot_to_radians(30))){
                    target_side_pos[0] = side_2[0];
                    target_side_pos[1] = side_2[1];
	    
                    target_proj_pos[0] = side_2_forward[0];
                    target_proj_pos[1] = side_2_forward[1];
                    if (self->very_verbose)
                        fprintf(stderr,"Side 2 empty Selecting Side 2\n");
                    self->was_following_side = 1;
                    self->target_side = 2;
                }      
                else{
                    self->was_following_side = 0;
                    self->target_side = -1;
                }
            }
        }
        else if(time_side_1_clear > start_side_time_threshold && (utime - time_of_last_side_fail)/1e6 > mode_switch_threshold ){
            if(fabs(side_1_heading) < bot_to_radians(30)){
                target_side_pos[0] = side_1[0];
                target_side_pos[1] = side_1[1];
	
                target_proj_pos[0] = side_1_forward[0];
                target_proj_pos[1] = side_1_forward[1];
                self->was_following_side = 1;
                self->target_side = 1;
            }
            else{
                self->was_following_side = 0;
                self->target_side = -1;
            }
        }
        else if(time_side_2_clear > start_side_time_threshold && (utime - time_of_last_side_fail)/1e6 > mode_switch_threshold){
            if(fabs(side_2_heading) < bot_to_radians(30)){
                target_side_pos[0] = side_2[0];
                target_side_pos[1] = side_2[1];
	    
                target_proj_pos[0] = side_2_forward[0];
                target_proj_pos[1] = side_2_forward[1];
                if (self->very_verbose)
                    fprintf(stderr,"Side 2 empty Selecting Side 2\n");
                self->was_following_side = 1;
                self->target_side = 2;
            }
            else{
                self->was_following_side = 0;
                self->target_side = -1;
            }
        }
        else{
            self->was_following_side = 0;
            self->target_side = -1;
        }
    }
 
    lcmgl = self->lcmgl_targets;
    if(lcmgl){
        double xyz[3] = {.0,.0,.0};
        //draw the sides and forward locations 
        local_to_global(side_1[0], side_1[1],&xyz[0], &xyz[1],self);  
        lcmglColor3f(1, 0, 0); 
        lcmglCircle(xyz, 0.2);
        local_to_global(side_1_forward[0], side_1_forward[1],&xyz[0], &xyz[1],self);  
        lcmglCircle(xyz, 0.2);

        local_to_global(side_2[0], side_2[1],&xyz[0], &xyz[1],self);  
        lcmglColor3f(1, 0, 1.0); 
        lcmglCircle(xyz, 0.2);
        local_to_global(side_2_forward[0], side_2_forward[1],&xyz[0], &xyz[1],self);  
        lcmglCircle(xyz, 0.2);

        //lets draw the targets 
        if(self->was_following_side){
            //fprintf(stderr,"Side 1 (%f,%f), Side 2 (%f,%f) => Target : %f,%f\n", side_1[0], side_1[1],
            //	side_2[0], side_2[1], target_side_pos[0], target_side_pos[1]);
            //fprintf(stderr,"Proj Side 1 (%f,%f), Side 2 (%f,%f) => Target : %f,%f\n", side_1_forward[0], side_1_forward[1],
            //	side_2_forward[0], side_2_forward[1], target_proj_pos[0], target_proj_pos[1]);

            lcmglLineWidth (5);
            local_to_global(target_side_pos[0], target_side_pos[1],&xyz[0], &xyz[1],self);  
            lcmglColor3f(.0, 1.0, 1.0); 
            lcmglCircle(xyz, 0.3);
            local_to_global(target_proj_pos[0], target_proj_pos[1],&xyz[0], &xyz[1],self);  
            lcmglCircle(xyz, 0.3);
        }
    }

    bot_lcmgl_switch_buffer (lcmgl);
    //return 0;

    double goal_heading;
    //distance to target
    if(self->was_following_side){
        if (self->verbose)
            fprintf(stderr,"\n Mode : Following Side : %d\n\n", self->target_side );

        *dist_to_target = hypot(target_proj_pos[0], target_proj_pos[1]);
        goal_heading =atan2(target_proj_pos[1], target_proj_pos[0]);
    }
    else{
        if (self->verbose)
            fprintf(stderr,"\n Mode : Following Direction\n\n");
        *dist_to_target = act_dist;
        goal_heading =atan2(person[1], person[0]);
    }

    act_dist = *dist_to_target; //set the actual distance - to be our target distance 

    if (self->very_verbose)
        fprintf(stderr,"Distance to Target : %f\n", act_dist);  

    //do the calculation up to a look ahead 
    double max_range = lookahead_distance;
    //double goal_heading = //self->goal_heading;
    double goal_pt[2] = {pos[0] + cos (goal_heading) * max_range,
                         pos[1] + sin (goal_heading) * max_range};  
  
    double angle_goal_robot = bot_mod2pi(goal_heading - bot_heading);   

    //set only rv if we are withing rot-radius - we have the collision check before to stop the robot in case of collision

    //exit if we are within the radius - we have the collision check before to stop the robot in case of collision
  
    if((self->was_following_side && p_distance <= SIDE_FOLLOW_STOP_RADIUS)
       || (!self->was_following_side && p_distance <= STOP_RADIUS)){
        *rv = 0;
        *tv = 0;
        if (self->very_verbose)
            fprintf(stderr,"Within Stopping Radius\n");

        lcmgl = self->lcmgl_navigator;
        bot_lcmgl_switch_buffer (lcmgl);
    
        /* free points */
        free_points(points, nchannels);
        free_points(pf_points, nchannels);
        free_points(rpoints, nrchannels);
        laser_returns_destroy (channels);
        laser_returns_destroy (rchannels);

        lcmgl = self->lcmgl_navigator_info;
        draw_paused_navigator_msg(self, pos);
    
        return 0;
    }


    //check if this is wrong ????????
    //we should use two turn in place radii

    /*if((!self->was_following_side && (act_dist <= ROT_RADIUS || 
      ((act_dist >= ROT_RADIUS) 
      && (fabs(angle_goal_robot) > bot_to_radians (TURN_IN_PLACE_DEG)))))
      || (self->was_following_side && (fabs(angle_goal_robot) > bot_to_radians (TURN_IN_PLACE_DEG)))
      || self->turn_in_place){*/

    //check in the half circle model 
    double dist_from_edge = 0;
    //int in_personal_space = check_in_personal_space_half_circle(act_dist, heading_to_person, p_heading, 
    //							      person, 
    //							      &dist_from_edge);
    int in_personal_space = check_in_personal_space_from_velocity(act_dist, heading_to_person, p_heading, 
                                                                  person, 
                                                                  personal_zone_radius,
                                                                  &dist_from_edge);
  

    if (self->very_verbose)
        fprintf(stderr,"In personal Space : %d\n", in_personal_space);

    if((!self->was_following_side && ( in_personal_space || // (act_dist <= ROT_RADIUS || 
                                      ((fabs(angle_goal_robot) > bot_to_radians (TURN_IN_PLACE_DEG)))))
       || (self->was_following_side && (fabs(angle_goal_robot) > bot_to_radians (TURN_IN_PLACE_DEG)))
       || self->turn_in_place){
        //we are doing it here twice
        *tv = 0;
        *rv = 0;
        if (self->very_verbose)
            fprintf(stderr,"\t+++  Trying to turn in place\n");
        int turn_angle = 0;
        if(fabs(angle_goal_robot) > bot_to_radians (TURN_IN_PLACE_DEG)){
            turn_angle = 1;
        }
        if (self->very_verbose) {
            fprintf(stderr,"Was Turn-in-place:%d\tFollowing Side: %d\tPersonal Space : %d\tTurn-angle:%d\n",self->turn_in_place, 
                    self->was_following_side,
                    in_personal_space,turn_angle );
        }


        //we also do not rotate in place if the person is moving at a good rate - this prevents 
        //half motion of rotate in place before following again 
        double projection_time = 3.0;
        double p_projection_distance = fmin(self->guide_msg->vel_mag * projection_time, 1.5);
        double c = cos(p_heading);
        double s = sin(p_heading);

        double person_projection[2] = {.0,.0};

        person_projection[0] = person[0] + p_projection_distance * c;
        person_projection[1] = person[1] + p_projection_distance * s;

        double heading_to_proj = atan2(person_projection[1], person_projection[0]);

        if (self->very_verbose)
            fprintf(stderr,"Heading to Person projection : %f\n", bot_to_degrees(heading_to_proj));// / M_PI*180);

        //************* this might be wrong - need to check 
        if(fabs(heading_to_proj) < bot_to_radians(60) && projection_distance > 0.1) {//the person is moving forward - should just stop and wait
            if (self->very_verbose)
                fprintf(stderr,"Direction following - person moving forward - waiting\n");
            *turn_in_place = FALSE;
            self->turn_in_place = 0;
            *rv = 0;

            lcmgl = self->lcmgl_navigator;
            bot_lcmgl_switch_buffer (lcmgl);

            /* free points */
            free_points(points, nchannels);
            free_points(pf_points, nchannels);
            free_points(rpoints, nrchannels);
            laser_returns_destroy (channels);
            laser_returns_destroy (rchannels);

            char seg_info[512];
            sprintf(seg_info,"Within-Rotational space - But person moving - waiting\n");
            draw_text_navigator_msg(self, pos, seg_info);
            draw_rotation(self, self->turn_in_place, 0);
            return 0;
        }

    
        int collision = check_rot_collision(points, nchannels, rpoints, nrchannels);
    
        if(collision){//possible collision if we rotate - so doing nothing
            *rv = 0;
            char seg_info[512];
            sprintf(seg_info,"Within-Rotational space - but unable to turn - Collision\n");
            draw_text_navigator_msg(self, pos, seg_info);
      
            /* free points */
            free_points(points, nchannels);
            free_points(pf_points, nchannels);
            free_points(rpoints, nrchannels);
            laser_returns_destroy (channels);
            laser_returns_destroy (rchannels);
      
            lcmgl = self->lcmgl_navigator;
            bot_lcmgl_switch_buffer (lcmgl);

            *turn_in_place = FALSE;
            self->turn_in_place = 0;
            draw_rotation(self, self->turn_in_place, 0);
            return 0;
        }

        else if(fabs(angle_goal_robot) <= bot_to_radians(10.0)){
            *turn_in_place = FALSE;
            self->turn_in_place = 0;
            *rv = 0;

            /* free points */
            free_points(points, nchannels);
            free_points(pf_points, nchannels);
            free_points(rpoints, nrchannels);
            laser_returns_destroy (channels);
            laser_returns_destroy (rchannels);

            lcmgl = self->lcmgl_navigator;
            bot_lcmgl_switch_buffer (lcmgl);

            char seg_info[512];
            sprintf(seg_info,"Within-Rotational space - But too small an angle to turn\n");
            draw_text_navigator_msg(self, pos, seg_info);
            draw_rotation(self, self->turn_in_place, 0);
            return 0;
        }
    

        //ok to rotate in place

        /* free points */
        free_points(points, nchannels);
        free_points(pf_points, nchannels);
        free_points(rpoints, nrchannels);
        laser_returns_destroy (channels);
        laser_returns_destroy (rchannels);
   
    
        *turn_in_place = TRUE;
        *rv = get_rot_vel(angle_goal_robot);
        self->turn_in_place = 1;

        lcmgl = self->lcmgl_navigator_info;
        char seg_info[512];
        sprintf(seg_info,"Within Personal Space: Turning in place Tv: %f, Rv: %f\n", 
                *tv, *rv);
        draw_text_navigator_msg(self, pos, seg_info);
        draw_rotation(self, self->turn_in_place, 0);    
        return 0;
    }
  
    draw_rotation(self, self->turn_in_place, 1);

    //person is over the rot-radius - we need to score and pick the best heading 

    // ---------- Checking rotation crietrian ------------ //
    int draw_rotation_p = 0;

    double used_rot_radius;

    if(!self->keep_close){
        used_rot_radius = ROT_RADIUS;
    }
    else{
        used_rot_radius = C_ROT_RADIUS;
    }
    int collision = 0;

    // ---------------- Do scoring ---------------- //
  
    //we can just check around the person - span can at worst be 90 - around the person 

    //span to check around the wheelchair
    double span = bot_to_radians (90.0);//(180.0); //360 seems like over kill 
    //No of buckets 
    double dtheta = bot_to_radians(BUCKET_SIZE); //(2.5);//bucket size
    //double dtheta = bot_to_radians (5.0);//bucket size
    int nrays = 2*span/dtheta;
    //we span our search around the current heading of the person
    double theta0 = goal_heading-span;
    int index_0 = 0;
    double thetas[nrays];
    double ranges[nrays];
    double xys[nrays][2];

    //------------------------------------------------------------------------//
    //Should switch what laser returns we are using 


    //not sure why we calculate this  
    double closest_pt_dist = INFINITY;
    point2d_t robot_pt = { pos[0], pos[1] };
    for (int lidx = 0; lidx < nchannels; lidx++) {
        pointlist2d_t *pts;
        if(self->was_following_side)
            pts = points[lidx];
        else
            pts = pf_points[lidx];
        //we can skip the rear 
    
        for(int i=0; i<pts->npoints; i++) {
            double d = geom_point_point_distance_2d(&robot_pt, &pts->points[i]);
            closest_pt_dist = fmin(d, closest_pt_dist);
        }
    }

    double safety_radius;

    safety_radius = fmin(closest_pt_dist - 0.01, SAFETY_RADIUS);  
    //min of the safety distance and the closest of the points

    for (int ridx=0;ridx<nrays;ridx++) {  
        double theta = theta0 + ridx * dtheta;
        thetas[ridx] = theta;
        if(theta==0){
            index_0 = ridx;
        }
        double s = sin (theta);
        double c = cos (theta);

        point2d_t ray_start = { pos[0], pos[1]};  //robot position
        point2d_t ray_dir = { c, s };

        double dist = max_range;

        int poss_collision = 0;
        //get this from the config file
        double alpha = atan2(0.6, 1.0);

        //--------------------------------------------------------------------------------//
        //Find maximum collision free distance for each ray

        //search for each ray for each point - for an intersection 
        for (int lidx = 0; lidx < nchannels; lidx++){
            pointlist2d_t *pts;
            if(self->was_following_side)
                pts = points[lidx];
            else
                pts = pf_points[lidx];

            if(poss_collision){
                break;
            }
            for(int i=0; i<pts->npoints; i++) {
                double t = 0.0;
                //if the ray can lead to a collision find the distance of the colliding point (t)
                if(geom_ray_circle_intersect_2d(&ray_start, &ray_dir, 
                                                &pts->points[i], safety_radius, &t, NULL)){
                    dist = fmin(dist, t);  //this finds the closest colliding point - for each rays 
                    //dist can never be more than the lookahead distance
                }
                //not sure if this following check is entirely correct - need to check it more thourghly

                //*****Sachi removed - not in bot-core - introduced in common3d - which we are not using here 
                if(geom_ray_rect_intersect_2d(&ray_start, &ray_dir, 
                                              &pts->points[i], 1.0, 0.6, alpha, theta)){
	    
                    //possible to skip this whole calculation
                    poss_collision = 1;
                    dist = 0;
                    break;
                }
            }


            if(self->was_following_side){//check for person here as well 
                double t = 0.0;	
	
                double c1 = cos(p_heading);
                double s1 = sin(p_heading);

                double l = 0;
                int no_iter = 15;//LOOKAHEAD_DISTANCE / 0.2;

                for(int i=0;i<no_iter;i++){
                    point2d_t person_pt = {person[0] + l * c1, person[1] + l * s1};
                    l+=0.2;

                    double person_radius = 0.1; 
                    if(geom_ray_circle_intersect_2d(&ray_start, &ray_dir, 
                                                    &person_pt, safety_radius + person_radius , &t, NULL)){
                        dist = fmin(dist, t);
                    }	  
                }	
            }	
        }
        ranges[ridx] = dist;
        xys[ridx][0] = pos[0] + c * dist;
        xys[ridx][1] = pos[1] + s * dist;
    }
  
    /* find the best ray */
    double best_range = 0;
    double best_dtheta = HUGE;
  
    /* compute score for each ray */
    double scores[nrays];

    double alpha = 1.0;

    int closest_p_ind = -1;
    double closest_dist = 1000.0;

    //--------------------------------------------------------------------------------//
    //Score each ray based on the maximum collision free distance and how far it is from 
    //the goal

    for (int ridx = 0; ridx < nrays; ridx++) {

        double dist_sq = powf(xys[ridx][0] - goal_pt[0],2) +
            powf(xys[ridx][1] - goal_pt[1],2);  

        if(closest_dist > dist_sq){
            closest_p_ind = ridx;
            closest_dist = dist_sq;
        }

        double score = ranges[ridx] / (1.0 + alpha * dist_sq);   
        scores[ridx] = score;
    }
  
    /* find best direction */

    double avg_scores[nrays];
    int lb = 2; //reduced the averaging as we reduced the beam resolution 
    if(BUCKET_SIZE == 2.5){
        lb = 5;
    }

    //--------------------------------------------------------------------------------//
    // Average the scores - we pick the best average score - to prevent corner cutting   

    for (int ridx=0;ridx<nrays;ridx++) {
        int temp_count = 0;
        float temp_avg_score = 0.0;
        for (int i= ridx-lb; i <= ridx+lb;i++){
            temp_count++;
            temp_avg_score += scores[get_ind(i,nrays)];
        }    
        avg_scores[ridx] = temp_avg_score/temp_count;
    }

    double best_score = -1000;
    int best_ridx = -1;
  
    for (int ridx=0;ridx<nrays;ridx++) {    
        if (best_score < avg_scores[ridx]) {//changed to take the average          
            best_ridx = ridx;
            best_score = avg_scores[ridx];      
        }
    }
  
    if(self->draw_obs){//drawing the scores
        //draw_obs_scores(self, xys, scores, avg_scores, nrays);
        lcmgl = self->lcmgl_scores;
        if(lcmgl){
            for (int ridx=0;ridx<nrays;ridx++) {
                double score_g[3] = {.0,.0,.0};
                local_to_global(xys[ridx][0], xys[ridx][1],&score_g[0], &score_g[1],self);      
                char score_info[512];
                sprintf(score_info,"%.3f-%.3f", scores[ridx],avg_scores[ridx]);
                lcmglColor3f(1, 0, 0); 
                bot_lcmgl_text(lcmgl, score_g, score_info);
            }
        }
        bot_lcmgl_switch_buffer (lcmgl);
    }

#ifdef PRINT_OP
    fprintf(stderr,"Best Score : %d,  %f\n", best_ridx, best_score);
#endif
  
    best_dtheta = bot_mod2pi(thetas[best_ridx] - bot_heading); 

    //the larger this is the more we should rotate - to get there quicker
    best_range = ranges[best_ridx];  //higher this is the faster we can move forward  

    lcmgl = self->lcmgl_navigator;

    // draw the rays 
    if (lcmgl) {
        // draw rays in light blue
        double robot_gx = .0, robot_gy = .0;
        local_to_global(pos[0], pos[1],&robot_gx, &robot_gy,self);
    
        for (int ridx = 0; ridx < nrays; ridx++) {
            if (ridx == best_ridx) {
                lcmglLineWidth (3);
                lcmglColor3f(1, .5, .5);
            }
            else {
                lcmglLineWidth (1);
                lcmglColor3f(.3, .3, 1);
            }
            lcmglBegin(GL_LINES);

            double p_gx = .0, p_gy = .0;
            local_to_global(xys[ridx][0], xys[ridx][1] ,&p_gx, &p_gy,self);
            lcmglVertex3d(robot_gx, robot_gy, pos[2]);
            lcmglVertex3d(p_gx, p_gy, pos[2]);
            lcmglEnd();
        }
        lcmglColor3f(1, .5, .5);
        lcmglPointSize(8);
        lcmglBegin(GL_POINTS);

        double p_gx = .0, p_gy = .0;
        local_to_global(xys[best_ridx][0], xys[best_ridx][1] ,&p_gx, &p_gy,self);
        lcmglVertex3d(p_gx, p_gy, pos[2]);
        lcmglEnd();

        // draw target direction as a yellow ray 
        lcmglColor3f(1, 1, 0);
        lcmglLineWidth(1);
        lcmglBegin(GL_LINES);

        lcmglVertex3d(robot_gx, robot_gy, pos[2]);
        local_to_global(goal_pt[0],goal_pt[1],&p_gx, &p_gy,self);
        lcmglVertex3d(p_gx, p_gy, pos[2]);
        lcmglEnd();

        bot_lcmgl_switch_buffer (lcmgl);
    }
 
  
    if (angle)
        *angle = best_dtheta;

    if (brange)
        *brange = best_range;

    //we need to compute the gap between the best_ray and the goal_heading and not move if we 
    double angle_best_goal = bot_mod2pi(thetas[best_ridx] - goal_heading); 

    /*break out from turn in place once the person is close enough */
    //wont try to turn in place if the angle is too small - prevents the wheelchair trying to move but failing

    if(fabs(angle_goal_robot) <= bot_to_radians(10.0)){  //was 15.0 - but now our dynamics are better 
        self->turn_in_place = 0;
    }
  
    /* free points */
    free_points(points, nchannels);
    free_points(pf_points, nchannels);
    free_points(rpoints, nrchannels);
    laser_returns_destroy (channels);
    laser_returns_destroy (rchannels);
  
  
    //if the wheelchair is facing the opposite way and there is no space to turn, the wheelchair needs to move forward 
    //and then do a turn 
  
    //also it might be better to have the wheelchair move forward and then do a turn if it can't turn in place

    //prevents the wheelchair from turning away from us because the gap is too small
    if((fabs(angle_best_goal) > bot_to_radians(90.0)) && (fabs(angle_goal_robot) < bot_to_radians(90.0))){  //issue possible for it to try to turn to some side 
        self->too_small_gap = 1;
    }  
    else{
        self->too_small_gap = 0;
    }


    double used_tv_max;
    double used_rv_max;

    double drop_off_dist = 0.5; //was 1.0 need to check how this impacts *********
    if(self->keep_close){
        drop_off_dist = 1.5;
    }
    double used_dist;
    if(self->keep_close){    
        used_dist = fmin(act_dist - used_rot_radius, drop_off_dist);
    }
    else{
        if(self->was_following_side){
            used_dist = fmin(act_dist - used_rot_radius + 0.1, drop_off_dist);
        }
        else{
            used_dist = fmin(dist_from_edge + 0.1, drop_off_dist);
        }
    }
  
    used_dist = fmax(.0, used_dist);
    //double used_dist = fmin(act_dist, drop_off_dist);

#ifdef PRINT_OP  
    fprintf(stderr,"Used Rot Radius : %f, Dist_from_rot : %f, Used Dist : %f\n", drop_off_dist, act_dist - used_rot_radius, used_dist);
#endif
    //this needs to change based on the keep close - needs to drop off to zero someone inside the rotate_in_place boundry
    //also needs to provide smooth acceleration and decelleration
  
    used_tv_max = MAX_TRANSLATION_SPEED; //used_dist/drop_off_dist * MAX_TRANSLATION_SPEED;
    used_rv_max = MAX_ROTATION_SPEED; //

    //we can use this but - this controller needs to be distance based - or match the person's velocity somehow
    double max_allowed_tv = MAX_FAST_TRANSLATION_SPEED;//MAX_TRANSLATION_SPEED;
    double max_allowed_rv = MAX_ROTATION_SPEED; 
  
    //********* Check here - this is where the velocity is calculated 

    if(!self->was_following_side){
        //change this 

        //this max tv velocity should be -Approach_velocity + Gain * (distance from personal_zone)
        double P_gain = 1.2;
        double closing_v  =-self->guide_msg->closing_velocity;
        if(fabs(closing_v) < 0.2){
            closing_v = 0;
        }

        if(dist_from_edge < 0.4 && self->guide_msg->vel_mag < 0.1){
            max_allowed_tv = 0; 
        }
        else{
            max_allowed_tv = fmin(fmax(-self->guide_msg->closing_velocity + P_gain * dist_from_edge,0), FULL_MAX_TRANSLATION_SPEED) ;
        }
        //double p_vel_component = cos(p_heading) * self->guide_msg->vel_mag; 
        //max_allowed_tv = fmin(fmax(p_vel_component + P_gain * dist_from_edge,0), FULL_MAX_TRANSLATION_SPEED) ;

        if (self->very_verbose)
            fprintf(stderr," Escaping Velocity : %f Error Component : %f Max Allowed : %f\n", -self->guide_msg->closing_velocity, P_gain * dist_from_edge, max_allowed_tv);
    
    }
    else{
        //calculate the v_x and v_y based on the distances and velocities 
        //then convert to tv and rv to match a 1s motion

        double forward_error = target_side_pos[0] * cos(p_heading) +  target_side_pos[1] * sin(p_heading) - dist_behind_person - 0.4;; 
        double side_error = - target_side_pos[0] * sin (p_heading) + target_side_pos[1] * cos(p_heading);
        if(self->target_side == 1){
            if (self->very_verbose)
                fprintf(stderr,"left\n");
            side_error = - side_error; 
        }

        double f_gain = 1.0;
        double s_gain = 1.0; 

        /*//velocities in the person's frame of reference 
          double vx_p = self->guide_msg->vel_mag + f_gain * forward_error;  
          double vy_p = side_error * s_gain;
    
    
    
          fprintf(stderr, " Person frame : vx : %f\tvy : %f\n", vx_p, vy_p);

          double vx = vx_p;//vx_p * cos(p_heading) + vy_p * sin(p_heading); 
          double vy = vy_p;//-vx_p * sin(p_heading) + vy_p * cos(p_heading); 

          fprintf(stderr, " Robot frame : vx : %f\tvy : %f\n", vx, vy);

          double r_1 = (pow(vx, 2) + pow(vy,2)) / (2 * vy);

          fprintf(stderr," Radius : %f\n", r_1); 

          double rv_1 = asin( vx / r_1);
          fprintf(stderr, "RV : %f\n", rv_1);
    
          double tv_1 = rv_1 * r_1; 

          *tv = tv_1;
          *rv = rv_1;*/

        if (self->very_verbose) {
            fprintf(stderr, "Side target %f,%f\n", target_side_pos[0], target_side_pos[1]);
            fprintf(stderr,"Side error : %f\n" , side_error); 
        }

        max_allowed_tv = forward_error * f_gain + self->guide_msg->vel_mag + side_error * s_gain; 

        //added a simple feedback - for the max tv 
        if (self->very_verbose) {
            fprintf(stderr," Person Velocity : %f Forward Error Component : %f Side Error : %f Max Allowed : %f\n", self->guide_msg->vel_mag
                    , f_gain * forward_error, side_error * s_gain,   max_allowed_tv);
        }
        max_allowed_tv = fmin(fmax(max_allowed_tv,0), FULL_MAX_TRANSLATION_SPEED);
    }
    /*else{
    //setting the max velocity as the error in distance (from the side 
    //position plus the person's velocity

    double gain = 1;

    //double error = p_distance * cos(heading_to_person);    
    
    //this is the error from the robot center - our robot is larger 
    //double error = person[0] * cos(p_heading) +  person[1] * sin(p_heading) 
    //- dist_behind_person;
    double forward_error = person[0] * cos(p_heading) +  person[1] * sin(p_heading) 
    - dist_behind_person - 0.4;

    //this is one error - this is not enough 

    //double side_error  =  -( person[0] - projection_distance) * sin(p_heading) +  person[1] * cos(p_heading) - 0.8;
    //double side_error  =  -(p_projection[0] - projection_distance) * sin(p_heading) +  p_projection[1] * cos(p_heading) - 0.8; 
    //-if we are using this we need to project the robot forward as well 
    double heading_diff = 0;
    
    if(self->target_side == 1){
    heading_diff = -p_heading;
    }
    else{
    heading_diff = p_heading;
    }
    //this is not exactly correct
    double side_error  =  fmin(fmax(heading_diff / M_PI, -1), 1); 

    fprintf(stderr, "Proj : %f,%f  Dist : %f\n",p_projection[0], p_projection[1], 
    -(p_projection[0] - projection_distance) * sin(p_heading) +  p_projection[1] * cos(p_heading));

    //fprintf(stderr, "Proj : %f,%f  Dist : %f\n", person[0], person[1], 
    //	    -person[0] * sin(p_heading) +  person[1] * cos(p_heading));
    
    double side_gain   = 10; 
    

    double vx = forward_error * gain + self->guide_msg->vel_mag; 
    max_allowed_tv = vx; 
    //double vy = side_error * side_gain; 

    //max_allowed_tv = vx * cos(p_heading) + vy*sin(p_heading);

    max_allowed_tv = fmin(fmax(max_allowed_tv,0), FULL_MAX_TRANSLATION_SPEED);

    //added a simple feedback - for the max tv 
    fprintf(stderr," Person Velocity : %f Forward Error Component : %f Side Error : %f Max Allowed : %f\n", self->guide_msg->vel_mag
    , gain * forward_error, side_error * side_gain,   max_allowed_tv);

    //max_allowed_tv = fmin(fmax(forward_error * gain + self->guide_msg->vel_mag,0), FULL_MAX_TRANSLATION_SPEED);
    }*/

    // **************** Calculating tv and rv ****************** //

    //if within rot_radius and cant turn in place we do nothing
    //only hitting this if we are doing direction following
    /*if((((*dist_to_target < used_rot_radius) && (!*turn_in_place)) && !self->was_following_side)
      || collision_det){  
      fprintf(stderr,"Within Personal Space and cant turn - doing nothing\n");
      *rv = 0;
      *tv = 0;
      }*/
    //else if(self->collision_det){
    if(collision_det){
        fprintf(stderr,"Collision Detected\n");
        publish_speech_following_msg("TOO_CLOSE", self);
    }
    else if(self->too_small_gap){
        if (self->very_verbose)
            fprintf(stderr,"Small Gap Detected\n");
        publish_speech_following_msg("SMALL_GAP", self);
        update_following_state_new(STOP,self); //stopping the wheelchair 
    }
    else{  //check if there is a person in our intended path 
        int person_collision = 0;//check_person_collision(self->person_msg, best_dtheta);
    
        if(person_collision){//stop wheelchair till the person is clear 
            fprintf(stderr,"Someone has crossed our path - Stopping till person moves");
            *rv = 0;
            *tv = 0;
        }
        else{
            //if(!self->was_following_side){
            *rv = get_rot_vel(*angle);
      
            if (*turn_in_place)
                *tv = .0;
            else { 
                double avg_best_gap = 0;
                int temp_count = 0;
                int lr = 2; //not sure what the best value to use is

                for (int i= best_ridx-lr; i <= best_ridx+lr ;i++){
                    temp_count++;
                    avg_best_gap += ranges[get_ind(i,nrays)];
                }
                avg_best_gap = avg_best_gap / temp_count;

                //front gap - this is also relavent 
                double avg_front_gap = 0;
                temp_count = 0;
	  
                for (int i= index_0-lr; i <= index_0+lr ;i++){
                    temp_count++;
                    avg_front_gap += ranges[get_ind(i,nrays)];
                }
                avg_front_gap = avg_front_gap / temp_count; 
                //fprintf(stderr,"Front gap : %f\t Best Gap : %f\n", 

                /*if(self->was_following_side){
                  double target_pos[3] = {person[0], person[1], p_heading};
                  double target_vel[2] = {self->guide_msg->vel_mag, 0};
	  
                  double t_dist = hypot(target_side_pos[0], target_side_pos[1]); 
                  //for now we use this 
                  double g_pos[2] = {t_dist * cos(best_dtheta), t_dist * sin(best_dtheta)}; 

                  *tv = get_trans_vel_arc(self, g_pos, p_heading, *rv); 
                  //get_trans_rot_vel_arc(self, target_pos, target_vel, tv, rv, FULL_MAX_TRANSLATION_SPEED,MAX_ROTATION_SPEED);
                  }
                  else{
                  double g_pos[2] =  {0,0};
                  *tv = get_trans_vel_arc(self, g_pos, p_heading, *rv); 

                  //this is the origninal equation used to extract the tv 	
                  // *tv = get_trans_vel(self, angle_goal_robot, act_dist, avg_best_gap, min_side_dist, angle_best_goal, max_allowed_tv);
                  }
                */
                //this is the origninal equation used to extract the tv 	
                //*tv = get_trans_vel(self, angle_goal_robot, act_dist, avg_best_gap, min_side_dist, angle_best_goal, max_allowed_tv);
                //double t_dist = 2.0;	
                //double g_pos[2] = {t_dist * cos(best_dtheta), t_dist * sin(best_dtheta)}; 
                //this function extracts the tv based on the lookahead distance and the chosen rv 
                //*tv = get_trans_vel_arc(self, g_pos, t_dist, avg_best_gap, rv, max_allowed_tv);	
                //*tv = get_trans_vel_arc_with_obs(self, g_pos, t_dist, avg_best_gap, avg_front_gap, rv, max_allowed_tv);	
                //*tv = get_trans_vel_arc_with_obs(self,best_dtheta, avg_best_gap, avg_front_gap, rv, max_allowed_tv);	

                //p_distance
                //this has broken the side-by-side following 
                *tv = get_trans_vel_arc_with_obs_fixed_moderated(self,best_dtheta, avg_best_gap, avg_front_gap, p_distance, heading_to_person, rv, max_allowed_tv, min_side_dist);	
                //get_trans_vel_arc_with_obs_fixed(self,best_dtheta, avg_best_gap, avg_front_gap, p_distance, heading_to_person, rv, max_allowed_tv);	

                if(self->guide_msg->vel_mag == 0 && *tv< 0.1){
                    *tv = 0.0;
                }
	  
                //*tv = max_allowed_tv; 
                //*rv = get_rot_vel_arc(self, g_pos, t_dist, *tv); 
	
                //draw_arc(self, )
	
#ifdef PRINT_OP
                fprintf(stderr,"DT %f, TV : %f \n", dt, *tv);
#endif
            }
            /*}
              else{

              }*/
        }
    }

    draw_arc(self, *tv, *rv);
  
    prev_rv = *rv;
    prev_tv = *tv;
  
    lcmgl = self->lcmgl_navigator_info;
    if(lcmgl){
        int rot_rad = 0;
        if(act_dist < used_rot_radius){
            rot_rad =1;
        }
        double g_robot[3] = {.0,.0,.0};
        local_to_global(pos[0]+5.0, pos[1]+5.0,&g_robot[0], &g_robot[1],self);
        char seg_info[512];

        sprintf(seg_info,"Following Sides : %d \nAct Dist : %f, Turn In place : %d, Collision : %d, Tv : %f, RV : %f", 
                self->was_following_side, act_dist, *turn_in_place, collision, *tv,*rv);
    
#ifdef PRINT_OP
        fprintf(stderr,"Status : %s\n", seg_info);
#endif
        lcmglColor3f(1, 0, 0); 
        bot_lcmgl_text(lcmgl, g_robot, seg_info);
    }
    bot_lcmgl_switch_buffer (lcmgl);

    return 0;
  
}

void controller_update(State *self, int64_t utime)
{  
    if(self->tourguide_mode){
        draw_wheelchair(self);
        
        // Don't do anything if we are in OVERRIDDEN mode
        if (self->following_state == OVERRIDDEN)
            return;

        if (!self->have_goal_heading || (!self->playback && self->estop) || self->following_state ==IDLE || self->following_state == PAUSED){
            //should stop wheelchair - should be stopped by some other part of the code - but just in case 
            command_velocity(self, 0,0);
            char seg_info[512];
            sprintf(seg_info,".");
            double pos[3] = {.0,.0,.0};
            draw_text_navigator_msg(self,pos , seg_info);

            clear_lcmgl(self);
            return;    
        }
    
        else if (self->last_utime == 0 || fabs(utime - self->last_utime) > UPDATE_RATE * 1000000) {
            double dtheta = .0;
            double best_range = .0;
            gboolean turn_in_place = FALSE;
            double dist_to_target = 0.0;
            double tv = 0.0, rv = 0.0;

            if(self->do_direction_only){
                
                if ( calculate_velocity_command(utime, self, LOOKAHEAD_DISTANCE, &dtheta, 
                                                &turn_in_place, &best_range, &dist_to_target, &rv, &tv) !=0) { 
                    command_velocity(self, 0,0);
                    return;
                }     
                
            }
            else{
                if ( calculate_velocity_command_side(utime, self, LOOKAHEAD_DISTANCE, &dtheta, 
                                                &turn_in_place, &best_range, &dist_to_target, &rv, &tv) !=0) { 
                    command_velocity(self, 0,0);
                    return;
                }     
                
            }
            if (self->very_verbose)
                fprintf(stderr,"rv : %f, tv : %f Turn-In-Place : %d\n", rv, tv, turn_in_place);
            //make sure that this doesnt go backwords
            tv = fmax(tv,0);
            command_velocity(self, tv, rv);
            self->last_utime = utime;
        }
        else{//skipping time step
        }
    }
    return;
}


gboolean heartbeat_cb (gpointer data)
{
    State *s = (State*)data;
 
    fprintf (stdout, "following_state = %d\n", s->following_state);
   //this is relavent only for person following - not for door traversals
    if(s->tourguide_mode){   
    
        /* stop sending commands if no goal received 
         * in a while */
        int64_t dt = bot_timestamp_now () - s->last_goal_heading_utime;
        int max_secs = 1;

        if (dt > max_secs * 1000000) {    
	  if (s->following_state != OVERRIDDEN) {
            printf ("no goal received in the last %d seconds.\n", max_secs);
            s->estop = TRUE; 
            //giving the stop command 
            command_velocity(s, 0,0);
	  }
	  else{
	     s->estop = FALSE;
	  }
        } 
        else {
            s->estop = FALSE;
        }    
    }
    
    // Transition to OVERRIDDEN depending on joystick state
    if (s->subservient_to_joystick) {
        int transition_to_idle = 0;
        int transition_to_overridden = 0;

        // 1) If we haven't heard from the joystick in > MAX_JOYSTICK_TIMEOUT_USEC --> Transition to IDLE
        // 2) Else If the L1 button is not depressed                               --> Transition to OVERRIDDEN
        // 3) Else If the L1 button is depressed and we were in OVERRIDDEN           --> Transition to IDLE (or previous)
        // 4)                                    we are NOT in OVERRIDDEN            --> No transition

        if (!s->joystick_state_msg)
            transition_to_idle = 1;
        else {
            int64_t dt = abs(bot_timestamp_now() - s->joystick_state_msg->utime);

            if (dt > MAX_JOYSTICK_TIMEOUT_USEC) {
                fprintf (stdout, "Haven't heard from the joystick in %.2f seconds. Transitioning to IDLE\n",
                         dt/1E6);
                transition_to_idle = 1;
            }
            else {
                if (s->joystick_state_msg->left_up_down[0]) {
                    if (s->following_state == OVERRIDDEN) {
                        fprintf (stdout, "L1 button was depressed. Transitioning from OVERRIDDEN to IDLE\n");
                        transition_to_idle = 1;
                    }
                    // else do nothing
                }
                else {
                    if (s->following_state != OVERRIDDEN) {
                        fprintf (stdout, "L1 button was released. Transitioning to OVERRIDDEN mode!\n");
                        transition_to_overridden = 1;
                    }
                    // else do nothing
                }
            }
        }

        if (transition_to_idle && transition_to_overridden) {
            fprintf (stderr, "Should not be trying to transition to IDLE and OVERRIDDEN, choosing IDLE\n");
            transition_to_overridden = 0;
        }

        if (transition_to_idle)
            update_following_state_new (STOP,s);
        else if (transition_to_overridden){
            fprintf (stdout, "Updating to OVERRIDDEN\n");
            update_following_state_new (OVERRIDDEN,s);
        }
    }

    return TRUE;
}

void read_parameters(State *self, double *fl_offset, double *rl_offset, 
                     double *fl_ang_offset, double *rl_ang_offset)
{
    BotParam *c =  self->config;
    BotFrames *frames = bot_frames_get_global (self->lcm, c);

    //char key[2048];
 
    // Find the position of the forward-facing LIDAR
    char *coord_frame;
    coord_frame = bot_param_get_planar_lidar_coord_frame (c, "SKIRT_FRONT");
    if (!coord_frame)
        fprintf (stderr, "\tError determining front laser coordinate frame\n");

    BotTrans front_laser_to_body;
    if (!bot_frames_get_trans (frames, coord_frame, "body", &front_laser_to_body))
        fprintf (stderr, "\tError determining front laser coordinate frame\n");
    else
        fprintf(stderr,"\tFront Laser Pos : (%f,%f,%f)\n",
                front_laser_to_body.trans_vec[0], front_laser_to_body.trans_vec[1], front_laser_to_body.trans_vec[2]);

    *fl_offset = front_laser_to_body.trans_vec[0];

    double front_rpy[3];
    bot_quat_to_roll_pitch_yaw (front_laser_to_body.rot_quat, front_rpy);
    *fl_ang_offset  = front_rpy[2];


    // Now for the rear laser
    coord_frame = NULL;
    coord_frame = bot_param_get_planar_lidar_coord_frame (c, "SKIRT_REAR");

    if (!coord_frame)
        fprintf (stderr, "\tError determining rear laser coordinate frame\n");

    BotTrans rear_laser_to_body;
    if (!bot_frames_get_trans (frames, coord_frame, "body", &rear_laser_to_body))
        fprintf (stderr, "\tError determining LIDAR coordinate frame\n");
    else
        fprintf(stderr,"\tRear Laser Pos : (%f,%f,%f)\n",
                rear_laser_to_body.trans_vec[0], rear_laser_to_body.trans_vec[1], rear_laser_to_body.trans_vec[2]);

    *rl_offset = rear_laser_to_body.trans_vec[0];

    double rear_rpy[3];
    bot_quat_to_roll_pitch_yaw (rear_laser_to_body.rot_quat, rear_rpy);
    *rl_ang_offset = rear_rpy[2];





    /* 
     * double position[3];
     * sprintf(key, "%s.%s.%s.position", "calibration", "planar_lidars", "SKIRT_FRONT");
     * if(3 != bot_param_get_double_array(c, key, position, 3)){
     *   fprintf(stderr,"\tError Reading Params\n");
     * }else{
     *   fprintf(stderr,"\tFront Laser Pos : (%f,%f,%f)\n",position[0], position[1], position[2]);
     * }
     * *fl_offset = position[0];
     *   
     * sprintf(key, "%s.%s.%s.position", "calibration", "planar_lidars", "SKIRT_REAR");
     * if(3 != bot_param_get_double_array(c, key, position, 3)){
     *   fprintf(stderr,"\tError Reading Params\n");
     * }else{
     *   fprintf(stderr,"\tRear Laser Pos : (%f,%f,%f)\n",position[0], position[1], position[2]);
     * }
     * *rl_offset = position[0];
     * 
     * 
     * double rpy[3];
     * sprintf(key, "%s.%s.%s.rpy", "calibration", "planar_lidars", "SKIRT_FRONT");
     * if(3 != bot_param_get_double_array(c, key, rpy, 3)){
     *   fprintf(stderr,"\tError Reading Params\n");
     * }else{
     *   fprintf(stderr,"\tFront Laser RPY : (%f,%f,%f)\n",rpy[0], rpy[1], rpy[2]);
     * }
     * *fl_ang_offset  = bot_to_radians(rpy[2]);
     * 
     * sprintf(key, "%s.%s.%s.rpy", "calibration", "planar_lidars", "SKIRT_REAR");
     * if(3 != bot_param_get_double_array(c, key, rpy, 3)){
     *   fprintf(stderr,"\tError Reading Params\n");
     * }else{
     *   fprintf(stderr,"\tRear Laser RPY : (%f,%f,%f)\n",rpy[0], rpy[1], rpy[2]);
     * }
     * *rl_ang_offset = bot_to_radians(rpy[2]);
     */
}

static void usage (int argc, char ** argv)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "Person Follower: Follows person (surprised?)\n"
             "\n"
             "    -v   verbose output\n"
             "    -V   very verbose (DEBUG)\n"
             "    -c   no velocity shift\n"
             "    -p   playback mode\n"
             "    -d   direction-following only\n"
             "    -s   use state scores\n"
             "    -j   subservient to joystick (L1 button must remain depressed)\n"
             "    -h   print this help and exit"
             "\n\n",
             argv[0]);
}

int
main (int argc, char ** argv)
{
    int c;
    State s;

    // so that redirected stdout won't be insanely buffered.
    setlinebuf(stdout);

    memset (&s, 0, sizeof (State));
    
    s.shift_accel = 1;

    while ((c = getopt (argc, argv, "hspvVodcj")) >= 0) {
        switch (c) {
            case 'v':
                s.verbose = 1;
                break;
            case 'V':
                s.very_verbose = 1;
                s.verbose = 1;
                break;
            case 'c':
                s.shift_accel = 0;
            case 'p':
                s.playback = 1;
                break;
            case 'o':
                s.draw_obs = 1;
                break;
            case 'd':
                s.do_direction_only = 1;
                break;
            case 's':
                s.use_state_score = 1;
                break;
            case 'j':
                s.subservient_to_joystick = 1;
                break;
            case 'h':
            case '?':
                usage (argc, argv);
                return 1;
        }
    }
    
    s.lcm = lcm_create(NULL);//globals_get_lcm();
    bot_glib_mainloop_attach_lcm (s.lcm);

    s.config = bot_param_new_from_server(s.lcm, 1);//globals_get_config();
    s.channels = g_queue_new ();
    s.rchannels = g_queue_new ();
    s.goal_heading = bot_to_radians (.0);
    s.laser_hashtable = g_hash_table_new (g_str_hash, g_str_equal);
    s.rlaser_hashtable = g_hash_table_new (g_str_hash, g_str_equal);
    s.last_utime = 0;
    s.last_goal_heading_utime = 0;
    s.have_goal_heading = FALSE;
    s.estop = FALSE;
    s.collision_det = 0;
    s.state = NULL;
    s.guide_msg = NULL;
    s.tracking_state = LOST;
    s.following_state = IDLE;
    s.too_far = 0;
    s.turn_in_place = 0;
    s.too_small_gap = 0;
    s.keep_close = 0;
    s.small_gap = 0;
    s.previous_side_goal[0] = .0;
    s.previous_side_goal[1] = .0;  
    s.was_following_side = 0;
    s.target_side = -1;
    memset(&s.robot_pos,0, 3*sizeof(double)); //= {.0,.0,.0};
    s.robot_heading = .0;
    s.lcmgl_rot = bot_lcmgl_init (s.lcm, "ROT");
    s.lcmgl_navigator = bot_lcmgl_init (s.lcm, "NAVIGATOR");
    s.lcmgl_navigator_info = bot_lcmgl_init (s.lcm, "NAVIGATOR_INFO");
    s.lcmgl_obs = bot_lcmgl_init (s.lcm, "OBS");
    s.lcmgl_footprint = bot_lcmgl_init (s.lcm, "FOOTPRINT");
    s.lcmgl_scores = bot_lcmgl_init (s.lcm, "SCORE");
    s.lcmgl_targets = bot_lcmgl_init (s.lcm, "TARGETS");
    s.lcmgl_side_rays = bot_lcmgl_init (s.lcm, "SIDE_RAYS");
    s.lcmgl_arc = bot_lcmgl_init (s.lcm, "NAV_ARC");;
    s.tourguide_mode = 1; //we assume that this will start in TG mode
    s.avg_guide_heading = 0;
    s.heading_history = (double *) calloc(len_history, sizeof(double));
    s.temp_heading_history = (double *) calloc(len_history-1, sizeof(double));
    s.len = 0;
    read_parameters(&s,&s.frontlaser_offset, &s.rearlaser_offset, &s.front_anglular_offset, &s.rear_anglular_offset);

    //close connection to IPC once we have everything //does not need to be here but robot_interface has a lot of carmen stuff that needs to be takes out so 
    //seems to be complaining

    //not using the top laser for this 
    bot_core_planar_lidar_t_subscribe( s.lcm, "SKIRT_FRONT", on_laser, &s );
  
    fprintf (stdout, "Initial following state is %d\n", s.following_state);

    //we supersead the people list with the guide pos
    //carmen3d_people_pos_msg_t_subscribe(s.lcm, "PEOPLE_LIST",people_pos_handler, &s);  //robot orientation  
    erlcm_guide_info_t_subscribe(s.lcm, "GUIDE_POS",guide_pos_handler, &s); 

    erlcm_speech_cmd_t_subscribe(s.lcm, "PERSON_TRACKER", speech_handler, &s);
    erlcm_speech_cmd_t_subscribe(s.lcm, "TABLET_FOLLOWER", speech_handler, &s);

    bot_core_pose_t_subscribe(s.lcm,"POSE", pose_handler, &s);

    erlcm_tagged_node_t_subscribe(s.lcm, "WHEELCHAIR_MODE", mode_handler, &s);

    if (s.subservient_to_joystick)
        erlcm_joystick_state_t_subscribe (s.lcm, "PS3_JS_CMD", joystick_handler, &s);


    /* heart beat*/
    g_timeout_add_seconds (1, heartbeat_cb, &s);

    /* Main Loop */
    GMainLoop *mainloop = g_main_loop_new(NULL, FALSE);
    if (!mainloop) {
        fprintf(stderr,"Error: Failed to create the main loop\n");
        return -1;
    }

    bot_signal_pipe_glib_quit_on_kill (mainloop);
    /* sit and wait for messages */
    g_main_loop_run(mainloop);
  
    return 0;
}

