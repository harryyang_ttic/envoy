add_definitions(
  #-ggdb3 
    -std=gnu99
    -g 
    -D_FILE_OFFSET_BITS=64 
    -D_LARGEFILE_SOURCE 
    -D_LARGEFILE64_SOURCE 
    -Wall 
    -Wno-unused-parameter 
    -Wno-format-zero-length
    )

include_directories(
    ${GLIB2_INCLUDE_DIRS}
    ${GTK2_INCLUDE_DIRS}
    ${LCM_INCLUDE_DIRS}
    )

set(side_follower3d_sources
  side_navigator.c
  )

add_executable(er-person-follower ${side_follower3d_sources})

pods_use_pkg_config_packages(er-person-follower 
    lcm bot2-core 
    lcmtypes_er-lcmtypes
    bot2-param-client
    bot2-lcmgl-client
    lcmtypes_bot2-core
    bot2-frames
    robot3d_interface  
  )

pods_install_executables(er-person-follower)