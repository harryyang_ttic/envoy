#!/usr/bin/python
from festival import Festival
import sys, os
import pygtk, gtk, gobject
pygtk.require('2.0')
if gtk.pygtk_version < (2,3,90):
   print "PyGtk 2.3.90 or later required for this example"
   raise SystemExit

import pygst
pygst.require("0.10")
import gst
import string
import platform
from time import *
import pango
import cPickle
import math
from sys import argv

sys.path.append( '../../build/lib/python2.6/site-packages/' )

import lcm
from erlcm.speech_cmd_t import speech_cmd_t
from erlcm.robot_laser_t import robot_laser_t
from erlcm.pose_t import pose_t
from erlcm.proximity_warning_msg_t import proximity_warning_msg_t
from erlcm.navigator_goal_msg_t import navigator_goal_msg_t
from erlcm.navigator_floor_goal_msg_t import navigator_floor_goal_msg_t
from erlcm.point_t import point_t
from erlcm.mission_control_msg_t import mission_control_msg_t
#Pose nodes from ISAM SLAM
from erlcm.node_list_t import node_list_t
from erlcm.place_list_t import place_list_t
from erlcm.map_request_msg_t import map_request_msg_t
from erlcm.tagged_node_t import tagged_node_t
from erlcm.tagged_node_list_t import tagged_node_list_t
from erlcm.floor_change_msg_t import floor_change_msg_t
from erlcm.map_request_msg_t import map_request_msg_t
from erlcm.navigator_status_t import navigator_status_t
from erlcm.elevator_list_t import elevator_list_t
from erlcm.elevator_goal_list_t import elevator_goal_list_t
from erlcm.elevator_goal_node_t import elevator_goal_node_t
from erlcm.floor_map_msg_t import floor_map_msg_t
from erlcm.goal_cost_querry_t import goal_cost_querry_t
from erlcm.point_t import point_t
from erlcm.cost_result_t import cost_result_t
import threading, thread

class lcm_listener(threading.Thread):
    def __init__(self,speaker, callback, floor_callback, multi_nav_handler, floor_update_handler, places_update_handler, cost_result_callback):
        threading.Thread.__init__(self)
        self.callback = callback
        self.floor_callback = floor_callback
        self.places_update_handler = places_update_handler
        self.multi_nav_callback = multi_nav_handler
        self.cost_result_callback = cost_result_callback
	self.speaker = speaker
        self.floor_update_handler = floor_update_handler
        self.lc = lcm.LCM()
        self.subscription_speech = self.lc.subscribe("PERSON_TRACKER", self.speech_handler)
        self.subscription_speech1 = self.lc.subscribe("MULTI_LOCALIZER", self.speech_handler)
        self.subscription_speech2 = self.lc.subscribe("TABLET_FOLLOWER", self.speech_handler)
        self.subscription_nav_status = self.lc.subscribe("WAYPOINT_NAV_STATUS", self.nav_status_handler)
        self.subscription_pose = self.lc.subscribe("POSE", self.pose_handler)
        self.subscription_prox_war = self.lc.subscribe( "PROXIMITY_WARN",self.proximity_handler)
        #self.subscription_place_list = self.lc.subscribe( "TAGGED_PLACE_LIST",self.node_list_handler)
        self.subscription_elevator_list = self.lc.subscribe( "ELEVATOR_LIST",self.elevator_list_handler)
        self.subscription_elevator_list = self.lc.subscribe( "FINAL_ELEVATOR_LIST",self.elevator_list_handler)
        self.subscription_floor_map = self.lc.subscribe( "FLOOR_MAP_STATUS",self.floor_map_handler)
        self.subscription_cost_result = self.lc.subscribe( "COST_QUERRY_RESULT",self.cost_result_handler)

        self.cost_querry_queue = []
        self.cost_querry_mode = 'none' #'general' - at start up , 'to_portals' - when getting cost from robot to elevators
        
        #self.elevator_cost_msg = []
        #self.places_cost
        
        #"COST_QUERRY_RESULT"
        self.goal_cost_map = {}
        #the structure of this is 
        #floor[key:no] 
        #----elevator[key:id]
        #---------places [name,x,y,cost]        

        self.node_list = None

        self.elevator_places = []
        self.nav_places = []
        tmp_place =  {'id': 0, 'name':'meeting_room','x':25.0,'y': -6.68,'theta': 1.374,'floor_no': 3, 'floor_ind': 0}
        self.nav_places.append(tmp_place)
        self.floor_map = []
	self._running = True        

        #Indicators for whether any messages were recived yet
        self.speech_msgs_rec = 0
        self.laser_msgs_rec = 0
        self.pose_msgs_rec = 0    
        self.collision = 0

        self.last_slowdown_spoken = 0.0
        self.last_stuck_spoken = 0.0
        self.last_pose_msg = None
        
    def run(self):
        print "Started LCM Listener"
        try:
            while self._running:
                self.lc.handle()
        except KeyboardInterrupt:
            pass

    def pose_handler(self,channel, data):
       msg = pose_t.decode(data)
       #self.callback(msg)
       #threadsafe call to gtk
       #gobject.idle_add(self.callback, msg)
       self.last_pose_msg = msg
       self.pose_msgs_rec = 1 

    def check_cost_to_elevators(self, curr_floor_no):
       if(self.get_pose_point_msg() !=None):
          cost_q_msg = goal_cost_querry_t()
          cost_q_msg.utime = int(time() * 1000000)
          cost_q_msg.floor_no = int(curr_floor_no)
          cost_q_msg.portal = self.get_pose_point_msg()
          elevators = self.get_elevator_msg_on_floor(curr_floor_no)
          #elevators = self.get_comb_elevators_on_floor(i)
          cost_q_msg.num_goals = len(elevators)
          cost_q_msg.goals = elevators
          self.cost_querry_mode = 'to_portals'          
          self.lc.publish("COST_QUERRY", cost_q_msg.encode())

       else:
          print "Err: Do not have a pose message - can not querry"
          
    def cost_result_handler(self, channel, data):
       msg = cost_result_t.decode(data)
       if(self.cost_querry_mode == 'general'):
          print "Querried Floor :" , msg.querry.floor_no 
                  
          prev_querry = self.cost_querry_queue.pop(0)
          
          #basic check to see if we are getting response to what we asked
          if(msg.querry.utime == prev_querry['msg'].utime):
             print "Sucess " 
          
          #print prev_querry['places']
          print "Elevator : " , prev_querry['elevator']
          print "Costs : " 
          
          #costs = []
          costs = {}
          for i in range(len(prev_querry['places'])):
             print "\t", prev_querry['places'][i] , " : " , msg.costs[i]
             t_place = prev_querry['places'][i]
             temp_cost =  {'id': t_place['id'], 'name': t_place['name'],'x': t_place['x'],'y': t_place['y'],\
                              'theta':t_place['theta'],'floor_no':t_place['floor_no'], 'floor_ind': t_place['floor_ind'],\
                              'cost':msg.costs[i]}
             costs[t_place['id']] = temp_cost
             #costs.append(temp_cost)
          #add this to the cost matrix
          q_elevator = prev_querry['elevator']

          if not (msg.querry.floor_no in self.goal_cost_map):
             print "New floor"
             self.goal_cost_map[msg.querry.floor_no] = {}
             if not (q_elevator['id'] in self.goal_cost_map[msg.querry.floor_no]):
                self.goal_cost_map[msg.querry.floor_no][q_elevator['id']] = {}
             self.goal_cost_map[msg.querry.floor_no][q_elevator['id']]['places'] = costs
             self.goal_cost_map[msg.querry.floor_no][q_elevator['id']]['elevator'] = prev_querry['elevator']
          else:
             print "++++++Known Floor"
             if not (q_elevator['id'] in self.goal_cost_map[msg.querry.floor_no]):
                self.goal_cost_map[msg.querry.floor_no][q_elevator['id']] = {}
             self.goal_cost_map[msg.querry.floor_no][q_elevator['id']]['places'] = costs
             self.goal_cost_map[msg.querry.floor_no][q_elevator['id']]['elevator'] = prev_querry['elevator']
          #
          #for i in msg.costs: 
          #   print "\t" , i
          if(len(self.cost_querry_queue) > 0):
             #print len(self.cost_querry_queue) 
             #print "No of Goals : ", self.cost_querry_queue[0].num_goals
             #for i in self.cost_querry_queue[0].goals:
             #   print i.x, i.y#self.cost_querry_queue[0].goals
             print "Sending General querry"
             self.lc.publish("COST_QUERRY", self.cost_querry_queue[0]['msg'].encode())             
          else:
             print "Done with querry"
             print "Cost Map : " 
             for i in self.goal_cost_map.keys():
                print "============================================================================================================"
                print "Floor No : " , i
                for j in self.goal_cost_map[i].keys():
                   print "-----------------------------------------------------------------------------------------------------------"
                   print "Elevator ID : " , j 
                   c_ele = self.goal_cost_map[i][j]['elevator']
                   print "\tPos : (", c_ele['x'], ",", c_ele['y'],") = Door : (", c_ele['door_x'], "," , c_ele['door_y'] , ")"
                   print 
                   for l in self.goal_cost_map[i][j]['places'].keys():
                      k = self.goal_cost_map[i][j]['places'][l]
                      print "\t\tID : " , k['id'] , " Name : " , k['name'], " =>(", k['x'], ",", k['y'], ") = Cost :" , k['cost']   
             self.cost_querry_mode = 'none'
       elif(self.cost_querry_mode == 'to_portals'):
          c_elevators  = self.get_elevators_on_floor(msg.querry.floor_no)
          print  c_elevators
          print "Got the costs to elevators" 
          print "Costs : " 
          for i in msg.costs: 
             print "\t" , i
          self.cost_querry_mode = 'none'

          costs_to_ele = {}
          #for i in msg.costs:
          for i in range(len(c_elevators)):
             t_ele = c_elevators[i]
             costs_to_ele[t_ele['id']] = {'cost':msg.costs[i], 'elevator':t_ele}

          #self.cost_result_callback(costs_to_ele)
          #threadsafe call to gtk
          gobject.idle_add(self.cost_result_callback, costs_to_ele)

    def get_costs_from_elevators(self,floor_no, elevator_id, place_id):
       cost = -1 #10000000
       if(floor_no in self.goal_cost_map.keys()):
          if(elevator_id in self.goal_cost_map[floor_no].keys()):
             if(place_id in self.goal_cost_map[floor_no][elevator_id]['places'].keys()):
                cost = self.goal_cost_map[floor_no][elevator_id]['places'][place_id]['cost']
       return cost               
 
    def get_places_on_floor(self,floor_no):
       #should we just return the message???
       places = []
       for i in self.nav_places:
          if(floor_no == i['floor_no']):
             places.append(i)
       return places

    def get_comb_places_on_floor_msg(self,floor_no):
       #should we just return the message???
       place_msg = []
       places = []
       for i in self.nav_places:
          if(floor_no == i['floor_no']):
             places.append(i)
             msg = point_t()
             msg.x = i['x']
             msg.y = i['y']
             place_msg.append(msg)
       comb_place = {'places': places, 'msg': place_msg}
       #return place_msg
       return comb_place

    def get_places_on_floor_msg(self,floor_no):
       #should we just return the message???
       place_msg = []
       for i in self.nav_places:
          if(floor_no == i['floor_no']):
             msg = point_t()
             msg.x = i['x']
             msg.y = i['y']
             place_msg.append(msg)

       return place_msg

    def get_elevator_msg_on_floor(self,floor_no):
       elevator_msg = []
       for i in self.elevator_places:
          ele_floor_info = i['floors']
          for j in ele_floor_info.keys():
             if(floor_no == ele_floor_info[j]['floor_no']):
                print "Elevator on Floor : " , floor_no
                msg = point_t()
                msg.x = ele_floor_info[j]['x']
                msg.y = ele_floor_info[j]['y']
                elevator_msg.append(msg)
       return elevator_msg

    def get_elevators_on_floor(self,floor_no):
       elevators = []
       for i in self.elevator_places:
          ele_floor_info = i['floors']
          for j in ele_floor_info.keys():
             if(floor_no == ele_floor_info[j]['floor_no']):
                elevators.append(ele_floor_info[j])
       return elevators

    def get_comb_elevators_on_floor(self,floor_no):
       comb_msg = []
       #elevator_msg = []
       #elevators = []
       for i in self.elevator_places:
          ele_floor_info = i['floors']
          for j in ele_floor_info.keys():
             if(floor_no == ele_floor_info[j]['floor_no']):
                #elevators.append(ele_floor_info[j])
                print "Elevator on Floor : " , floor_no
                msg = point_t()
                msg.x = ele_floor_info[j]['x']
                msg.y = ele_floor_info[j]['y']
                #elevator_msg.append(msg)
                comb_msg.append({'msg':msg, 'elevator':ele_floor_info[j]})
       return comb_msg#{'msg': elevator_msg, 'elevators': elevators}
       #return elevator_msg
       
    def nav_status_handler(self,channel, data):
       print "++++++++++++ Goal Reached message received++++++"
       msg = navigator_status_t.decode(data)
       #self.callback(msg)
       #self.multi_nav_callback(msg.goal_reached, msg.goal_x, msg.goal_y)
       param_dict = [msg.goal_reached, msg.goal_x, msg.goal_y]
       gobject.idle_add(self.callback, msg)
       gobject.idle_add(self.multi_nav_callback, param_dict)
       
       

    def proximity_handler(self,channel, data):
       msg = proximity_warning_msg_t.decode(data)
       #self.callback(msg)
       gobject.idle_add(self.callback, msg)
       if(msg.warning ==1):
          if(self.collision ==0):
             self.speaker.say("collision")
             self.collision =1          
       else:
          self.speaker.say("cleared")
          self.collision =0        

    def node_list_handler(self,channel, data):
       self.nav_places = []
       print "Tagged Location List received"
       msg = tagged_node_list_t.decode(data)
       
       #cost_querry = goal_cost_querry_t()
       if msg.place_count ==0:
          print "No places received" #Filling with fake set of places
          
       else:
          count = 0
          for i in msg.places:
             tmp_place =  {'id': count, 'name':i.label,'x':i.x,'y':i.y,'theta':i.theta,'floor_no':i.floor_no, 'floor_ind':i.floor_index}
             self.nav_places.append(tmp_place)
             print "ID : " , tmp_place['id'], "Name : " , i.label , "," , "(",i.x,",",i.y,")-", i.theta," => Floor ", i.floor_no 
             count +=1
       #self.callback(msg)
       #send cost querry 

       #check if we have the elevators - then send the querry 
       if(len(self.elevator_places) > 0):
          print "Elevators already received - Time to querry costs"             
             
       #self.places_update_handler(self.nav_places)
       gobject.idle_add(self.places_update_handler, self.nav_places)

    def floor_map_handler(self, channel, data):
       print "Floor Map Received"
       msg = floor_map_msg_t.decode(data)
       if(msg.no_floors > 0):
          self.floor_map = []
          for i in msg.floor_no:
             self.floor_map.append(i)
       print "Floors " 
       print self.floor_map
       print "Current Floor Ind : " , msg.curr_floor_ind
       #self.floor_update_handler(self.floor_map)
       gobject.idle_add(self.floor_update_handler, self.floor_map)

    def get_pose_point_msg(self):
       if(self.last_pose_msg !=None):
          msg = point_t()          
          msg.x = self.last_pose_msg.pos[0] 
          msg.y = self.last_pose_msg.pos[1]
          return msg
       else:
          return None
       
    def elevator_list_handler(self,channel, data):
       self.elevator_places = []

       temp_ele = []
       msg = elevator_list_t.decode(data)
       if msg.count ==0:
          print "No elevators received"
       else:                   
          for i in msg.elevators:
             tmp_place =  {'x':i.pos[0],'y':i.pos[1],'door_x':i.door_pos[0],'door_y':i.door_pos[1],'floor_no':i.floor_no, 'floor_ind':i.floor_ind}
             temp_ele.append(tmp_place)
             print "Elevator : (",i.pos[0],",",i.pos[1],") = Door (",i.door_pos[0],",",i.door_pos[1] ,") => Floor ", i.floor_no 

       while(len(temp_ele)>0):
          if(len(self.elevator_places)==0):
             #add the first elevator
             curr_ele_floor = temp_ele.pop(0)
             curr_ele = {}
             curr_ele['x'] = curr_ele_floor['x']
             curr_ele['y'] = curr_ele_floor['y']
             curr_ele['floors'] = {curr_ele_floor['floor_no']: curr_ele_floor}
             self.elevator_places.append(curr_ele)

          #append the closest elevator to the list if close enough
          j= 0
          min_dist = 1000
          closest_ind = -1
          for i in range(len(self.elevator_places)):
             print "====elevator==== "
             print self.elevator_places[i]
             print "----Floor----" 
             print temp_ele[j]
             t_dist =  math.sqrt(pow(self.elevator_places[i]['x']-temp_ele[j]['x'],2) + pow(self.elevator_places[i]['y']-temp_ele[j]['y'],2))
             print "Dist :  " , t_dist
             if(min_dist > t_dist):
                closest_ind = i
                min_dist = t_dist

          if(min_dist < 1.0):
             #add as the same elevator
             print temp_ele[j]
             print "Found the same elevator on a different floor - adding : " , closest_ind
             self.elevator_places[closest_ind]['x'] = (self.elevator_places[closest_ind]['x'] * len(self.elevator_places[closest_ind]['floors'].keys()) \
                                                          + temp_ele[j]['x']) / (len(self.elevator_places[closest_ind]['floors'].keys())+1)
             self.elevator_places[closest_ind]['y'] = (self.elevator_places[closest_ind]['y'] * len(self.elevator_places[closest_ind]['floors'].keys()) \
                                                          + temp_ele[j]['y']) / (len(self.elevator_places[closest_ind]['floors'].keys())+1)

             self.elevator_places[closest_ind]['floors'][temp_ele[j]['floor_no']] = temp_ele[j] 
             temp_ele.pop(j) 
             print "=====Combined elevators===="

          elif(len(temp_ele)>0):
             curr_ele_floor = temp_ele.pop(j)
             curr_ele = {}
             curr_ele['x'] = curr_ele_floor['x']
             curr_ele['y'] = curr_ele_floor['y']
             curr_ele['floors'] = {curr_ele_floor['floor_no']: curr_ele_floor}
             self.elevator_places.append(curr_ele)
             print "Added a new elevator"


       no_ele = 0
       for i in self.elevator_places:
          print "Elevator " , (no_ele +1), " : " , i['x'], ",", i['y']
          
          ele_floor_info = i['floors']
          for j in ele_floor_info.keys():
             ele_floor_info[j]['id'] = no_ele
             print "\tID : " , ele_floor_info[j]['id'], " Floor :" , ele_floor_info[j]['floor_no'] ," = (", ele_floor_info[j]['x'], ",", ele_floor_info[j]['y'], ") = Door (", ele_floor_info[j]['door_x'] , "," , ele_floor_info[j]['door_y'], ")"
          no_ele +=1

       print "---------------------------"
       
       #check if we have places - then send the querry 
       if(len(self.nav_places) > 0):
          print "Both elevators and goals received - time to querry costs" 
          
          self.cost_querry_queue = []
       #for each floor for each elevator - querry for goals in the same floor 
          
          
          print self.floor_map
          for i in self.floor_map:
             #elevators = self.get_elevators_on_floor(i)
             elevators = self.get_comb_elevators_on_floor(i)
             #place_msg = self.get_places_on_floor_msg(i)
             place_msg = self.get_comb_places_on_floor_msg(i)

             print "Floor : " , i
             print "\tNo of Elevators" ,  len(elevators)
             print "\tNo of Places " , len(place_msg)
             
             for j in elevators: 
                if(len(place_msg['msg'])>0):
                   cost_q_msg = goal_cost_querry_t()
                   cost_q_msg.utime = int(time() * 1000000)
                   cost_q_msg.floor_no = int(i)
                   cost_q_msg.portal = j['msg']
                   cost_q_msg.num_goals = len(place_msg['msg'])
                   cost_q_msg.goals = place_msg['msg']
                   self.cost_querry_queue.append({'msg':cost_q_msg, 'places': place_msg['places'], 'elevator':j['elevator']})
                else:
                   print "No places on this floor"
          if(len(self.cost_querry_queue) > 0):
             print "Sending first querry"
             self.lc.publish("COST_QUERRY", self.cost_querry_queue[0]['msg'].encode())
             self.cost_querry_mode = 'general'

       #self.callback(msg)
       gobject.idle_add(self.callback, msg)

    def speech_handler(self,channel, data):
       msg = speech_cmd_t.decode(data)
       print "Message Received"
       #self.callback(msg)
       gobject.idle_add(self.callback, msg)
       self.last_speech_msg = msg
       self.speech_msgs_rec = 1
       print("Received message on channel \"%s\"" % channel)
       print("   timestamp   = %s" % str(msg.utime))       
       print("   command    = %s" % str(msg.cmd_type))

       #tracking messages published by person_tracker
       if((msg.cmd_type == "TRACKER") and (msg.cmd_property == "LOST")):
          self.speaker.say("lost")       
       if((msg.cmd_type == "TRACKER") and (msg.cmd_property == "FOUND")):
          self.speaker.say("found")
       if((msg.cmd_type == "TRACKER") and (msg.cmd_property == "LOOKING")):
          print "Looking"
          self.speaker.say("looking")
       if((msg.cmd_type == "TRACKER") and (msg.cmd_property == "SEE_YES")):
          self.speaker.say("yes")
       if((msg.cmd_type == "TRACKER") and (msg.cmd_property == "SEE_NO")):
          self.speaker.say("no")
       if((msg.cmd_type == "TRACKER") and (msg.cmd_property == "SEE_LOOKING")):
          self.speaker.say("no still looking")
       if((msg.cmd_type == "FOLLOWER") and (msg.cmd_property == "FOLLOWING")):
          self.speaker.say("following")
       if((msg.cmd_type == "FOLLOWER") and (msg.cmd_property == "KEEPING_CLOSE")):
          self.speaker.say("keeping close")
       if((msg.cmd_type == "FOLLOWER") and (msg.cmd_property == "KEEPING_BACK")):
          self.speaker.say("keeping back")
       if((msg.cmd_type == "FOLLOWER") and (msg.cmd_property == "SLOW_DOWN")):
          if((msg.utime/1e6 - self.last_slowdown_spoken) > 60): #prevents this message being uttered too often 
             self.last_slowdown_spoken = msg.utime/1e6
             self.speaker.say("slow down")
       if((msg.cmd_type == "FOLLOWER") and (msg.cmd_property == "TOO_CLOSE")):
          if((msg.utime/1e6 - self.last_stuck_spoken) > 60): #prevents this message being uttered too often 
             self.last_stuck_spoken = msg.utime/1e6
             self.speaker.say("stuck")
       if((msg.cmd_type == "FOLLOWER") and (msg.cmd_property == "SMALL_GAP")):
          self.speaker.say("too narrow")
       if((msg.cmd_type == "FOLLOWER") and (msg.cmd_property == "STOPPED")):
          self.speaker.say("stopping")
       if((msg.cmd_type == "FOLLOWER") and (msg.cmd_property == "ALREADY_STOPPED")):
          self.speaker.say("stopped")

       if((msg.cmd_type == "FOLLOWER") and (msg.cmd_property == "UNABLE_TO_FOLLOW_LOST")):
          self.speaker.say("cant, i dont see you")
       if((msg.cmd_type == "FOLLOWER") and (msg.cmd_property == "WAITING")):
          self.speaker.say("i dont see you")
       if((msg.cmd_type == "ELEVATOR") and (msg.cmd_property == "ELEVATOR_CLOSED")):
          print "Elevator Detected"
          self.speaker.say("are we going to a different floor")
       if((msg.cmd_type == "LOCALIZER") and (msg.cmd_property == "LOST")):
          self.speaker.say("i'm lost, please drive around so i can locate myself")
       if((msg.cmd_type == "LOCALIZER") and (msg.cmd_property == "FOUND")):
          self.speaker.say("i know where i am")
       if((msg.cmd_type == "LOCALIZER") and (msg.cmd_property == "RESTART")):
          self.speaker.say("still lost, looking again")

       if((msg.cmd_type == "FLOOR_CHANGE") and (msg.cmd_property == "CHANGE")):
          print "Floor Change Detected"          
          #self.floor_callback("querry",msg.cmd_property)   
          gobject.idle_add(self.floor_callback,["querry",msg.cmd_property])
          #should set off a floor querry

       if(msg.cmd_type == "FLOOR_AUTO_CHANGE"): 
          print "Automatic Floor Change Detected"
          #self.floor_callback("auto_change",msg.cmd_property)                      
          gobject.idle_add(self.floor_callback,["auto_change",msg.cmd_property])

       if(msg.cmd_type == "FLOOR_REVISIT"):
          print "Possible Floor Revisit Detected"
          suggested_floor = msg.cmd_property         
          #this suggestion should be kept in memory if we suggest the floor no
          #self.floor_callback("suggestion",msg.cmd_property)   
          gobject.idle_add(self.floor_callback,["suggestion",msg.cmd_property])
          print("Recieved")
          self.time = msg.utime
       

    def get_last_speech_msg(self):
       print "Speech Message"
       if self.speech_msgs_rec == 1:
          return self.last_speech_msg
       else:
          return "None"

    def get_last_laser_msg(self):
       if self.laser_msgs_rec == 1:
          return self.last_laser_msg
       else:
          return "None"

    def get_last_pose_msg(self):  #Also check how corrections to poses is done - when map is changed
       if self.pose_msgs_rec == 1:
          return self.last_pose_msg
       else:
          return "None"

    def get_pose_at_time(self):
       if self.pose_msgs_rec == 1:
          return self.last_pose_msg  #Need to implement pose interpolation
       else:
          print "No messages received"
          return "None"

    def send_speech_msg(self,cmd, prop):
       msg = speech_cmd_t()
       msg.utime = int(time() * 1000000)
       msg.cmd_type = cmd
       msg.cmd_property = prop
       self.lc.publish("PERSON_TRACKER", msg.encode())
       print "Speech Msg Sent"

    def send_navigator_msg(self,cmd, prop):
       msg = speech_cmd_t()
       msg.utime = int(time() * 1000000)
       msg.cmd_type = cmd
       msg.cmd_property = prop
       self.lc.publish("WAYPOINT_NAVIGATOR", msg.encode())
       print "Speech Msg Sent"

    def check_floor_status(self):
       msg = map_request_msg_t()
       msg.utime = int(time() * 1000000)
       msg.requesting_prog = "DM"
       print "Asking Floor"
       self.lc.publish("FLOOR_STATUS_REQUEST", msg.encode())

    def send_possible_tag_msg(self,tag):
       msg =  tagged_node_t()
       msg.utime = int(tag['time'] * 1000000)
       msg.type = tag['type']
       msg.label = tag['label']
       msg.dir = tag['dir']
       msg.view = tag['view']
       msg.pos = tag['pos']
       msg.prop = tag['prop']
       msg.pron = tag['pron']
       self.lc.publish("SPEECH_TAG", msg.encode())
       print "Sent Possible Tagging event"

    def send_confirm_tag_msg(self,tag_time, name):
       msg =  tagged_node_t()
       msg.utime = int(tag_time * 1000000)
       msg.type = 'confirm'
       msg.label = name
       self.lc.publish("SPEECH_TAG", msg.encode())
       print "Sent Confirmation"

    def send_wheelchair_mode(self,mode):
       msg =  tagged_node_t()
       msg.utime = int(time() * 1000000)
       msg.type = 'mode'
       msg.label = mode
       self.lc.publish("WHEELCHAIR_MODE", msg.encode())
       print "Sent Mode Change"

    def send_place_request_msg(self):
       msg = map_request_msg_t()
       msg.utime = int(time() * 1000000)
       msg.requesting_prog = "DM"
       self.lc.publish("PLACE_REQUEST_CHANNEL", msg.encode())
       print "Requesting Placelist"

    def send_goal_msg_old(self,goal, use_theta):
       print "Message Goal :" , goal
    
       msg = navigator_goal_msg_t()
       msg.utime = int(time() * 1000000)
       msg.nonce = 0.0
       #select use theta - for the elevator controller 
       msg.use_theta = 1 #use_theta

       goal_pos = point_t()
       goal_pos.x = goal['x']
       goal_pos.y = goal['y']
       goal_pos.z = 0.0
       #if(use_theta ==1):
       goal_pos.yaw = goal['theta']
       #else:
       #   goal_pos.yaw = 0.0
       goal_pos.pitch = 0.0
       goal_pos.roll = 0.0

       msg.goal = goal_pos

       miss_msg = mission_control_msg_t()
       miss_msg.utime = int(time() * 1000000)
       miss_msg.type = 4
       self.lc.publish("MISSION_CONTROL",miss_msg.encode())
       self.lc.publish("NAV_GOAL", msg.encode())

       self.send_navigator_msg("FOLLOWER", "GO")

       print "Goal Msg Sent"

    def send_goal_msg(self,goal, use_theta):
       print "Message Goal :" , goal
       
       msg = navigator_floor_goal_msg_t()
       msg.goal_msg = navigator_goal_msg_t()
       msg.goal_msg.utime = int(time() * 1000000)
       msg.goal_msg.nonce = 0.0
       #select use theta - for the elevator controller 
       msg.goal_msg.use_theta = 1#use_theta
       msg.floor_no = goal['floor_no']
       goal_pos = point_t()
       goal_pos.x = goal['x']
       goal_pos.y = goal['y']
       goal_pos.z = 0.0
       #if(use_theta ==1):
       goal_pos.yaw = goal['theta']
       #else:
       #   goal_pos.yaw = 0.0
       goal_pos.pitch = 0.0
       goal_pos.roll = 0.0

       msg.goal_msg.goal = goal_pos

       miss_msg = mission_control_msg_t()
       miss_msg.utime = int(time() * 1000000)
       miss_msg.type = 4
       self.lc.publish("MISSION_CONTROL",miss_msg.encode())
       self.lc.publish("NAV_FLOOR_GOAL", msg.encode())

       self.send_navigator_msg("FOLLOWER", "GO")

       print "Goal Msg Sent"

    def send_elevator_goal_msg(self,goal, use_theta): 
       print "Message Goal :" , goal
       if(len(goal['pos'])==0):
          print "No valid elevator location given"
          return
       elif(len(goal['pos'])==1): 
          print "Single elevator - sending a normal goal point"
          msg = navigator_goal_msg_t()
          msg.utime = int(time() * 1000000)
          msg.nonce = 0.0
          #select use theta - for the elevator controller 
          msg.use_theta = use_theta

          goal_pos = point_t()
          #not sure how to do this 

          goal_pos.x = goal['pos'][0]['x']
          goal_pos.y = goal['pos'][0]['y']
          goal_pos.z = 0.0
          if(use_theta ==1):
             goal_pos.yaw = goal['theta']
          else:
             goal_pos.yaw = 0.0
          goal_pos.pitch = 0.0
          goal_pos.roll = 0.0

          msg.goal = goal_pos          
          
       if(len(goal['pos'])>=1):
          print "Multiple Elevators near eachother"
          ele_msg = elevator_goal_list_t()
          ele_msg.utime = int(time() * 1000000)
          ele_msg.count = len(goal['pos'])          
          ele_list = []
          for i in goal['pos']:
             ele_node = elevator_goal_node_t()
             ele_node.pos = [i['x'], i['y']]
             ele_node.door_pos = [i['door_x'],i['door_y']]
             ele_list.append(ele_node)
          ele_msg.elevators = ele_list
          print "No of elevators", ele_msg.count
          print ele_msg.elevators

          print "Full message " 
          print ele_msg
          self.lc.publish("ELEVATOR_NAV_GOAL", ele_msg.encode())
       miss_msg = mission_control_msg_t()
       miss_msg.utime = int(time() * 1000000)
       miss_msg.type = 4
       self.lc.publish("MISSION_CONTROL",miss_msg.encode())
       self.send_navigator_msg("FOLLOWER", "GO")

       print "Goal Msg Sent"

    def send_stop_msg(self):       
       miss_msg = mission_control_msg_t()
       miss_msg.utime = int(time() * 1000000)
       miss_msg.type = 5
       self.lc.publish("MISSION_CONTROL",miss_msg.encode())
       self.send_navigator_msg("FOLLOWER", "STOP")
       return ""

    def send_floor_change_msg(self,current_floor_no):
       floor_msg = floor_change_msg_t()
       floor_msg.utime = int(time() * 1000000)
       floor_msg.floor_no = current_floor_no
       self.lc.publish("FLOOR_STAUS",floor_msg.encode())    
        
    def stop(self):
       self.lc.unsubscribe(self.subscription_speech)
       self.lc.unsubscribe(self.subscription_pose)
       self.lc.unsubscribe(self.subscription_speech1)
       self.lc.unsubscribe(self.subscription_speech2)
       self.lc.unsubscribe(self.subscription_nav_status)
       self.lc.unsubscribe(self.subscription_prox_war)
       self.lc.unsubscribe(self.subscription_place_list)
       print "Unsubscribed all listeners"
       thread.exit()
       print "Exiting Thread"


class GTK_Main:
	
	def __init__(self, speaker, mode, current_floor):

                #----------Speech Synthesizer-----------------------
		self.speaker = speaker

                #----------System info
		release = platform.release()

                #----------Floor Changes----------------------------

                if(current_floor !=None):
                   self.current_floor_no = current_floor
                else:
                   self.current_floor_no = -1

                print "Current Floor is : " , self.current_floor_no 

                self.poss_new_floor_no = -1

                #-----------Speech Processing-----------------------

                #----Keywords
                
		#add keywords that you want extracted - for keyword extraction
		self.keywords = ['CMD','LOC','DIR','KEYWORD', 'PRON' , 'PROP', 'LABEL','VIEW','POS','PREFIX','SUFFIX']  

                #----------Speech Acquisition #Depricated in the current implementation 
		self.device = 'usb'  #options = bluetooth, usb, default

                self.player_made = 0

                #-----Speech Recognizer Parameters 
		#Floor Files to load - changes which vocab is loaded 
		self.current_floor = 'stata_floor_3' #'tbh' #'stata_floor_8' 
		
                #-----Grammar Files to load
                self.tour_config_file = "../../../../data/recog/" + self.current_floor + ".cfg"
		self.nav_config_file = "../../../../data/recog/navigation.cfg"  #might be generated at the end of the tour

                self.mode_config_file = "../../../../data/recog/mode_select.cfg"

                self.confirm_config_file = "../../../../data/recog/confirmation.cfg"
                self.wakeup_config_file = "../../../../data/recog/wakeup.cfg"

                self.new_floor_config_file = "../../../../data/new_floor.cfg"

                

		#----------Speech Detector Parameters
		self.threshold = -55.0
		self.minspeech = 100000
		self.prespeech = 500000 
                self.holdover = 500000

                #-----------Dialog Manager Parameters
                self.DM_mode = None
                self.next_DM_mode = None
                self.curr_config_file = self.mode_config_file
                
                #----Logging Parameters
                self.logging_mode = 1#1#0  #0 - No logging of audio files

                self.tag_file =  open('../../../../data/logs/wheelchair.log', 'a')
                self.listening = 0

		#Audio Logs
		if self.logging_mode == 1:		
                   self.last_log_name = 'logs/last_file.log'	
                   try: 
		      #file exists so we start from the next position
                      a = open(self.last_log_name,'r')
                      self.sink_count = int(a.read()) + 1
                      a.close()
                      
                   except IOError, e:
		      #No previous logs - so start the logs at 0
                      self.sink_count = 0

		else: 
                   print "== Not Logging Audio ==" 

		#Places Logs	
		self.placelist_filename = "logs/" + self.current_floor + ".log"

                #Place Locations (current navigatable targets
                self.placelocations_filename = "/home/sachih/wheelchair/maps/locations/" + self.current_floor + "_map_locations.log"
                
		#---Dialog manager Parameters

		#Will start at the inactive state and the keyword will set it to active state
		self.state = "inactive"  #whether the DM is listening or not

		self.dialog_state = 'init' #Dialog State 

		self.mode = 'recording' #Listener State 

                ##might need a current action state
		

                #---TG Mode Stuff
		#Last spoken placename
                self.last_placename  = {'prop':'none','label':'none', 'view': 'none', 'pos':'none',\
                                           'dir' : 'none', 'time':0.0}

                #---Nav Mode Stuff
                self.last_goal  = {'name':'none','x':0,'y':0,'floor_no':-1,'pron': 'none'}
                
                self.navigation_mode = "none" #Whether multi-floor or single floor - might not need to make this destinction now
                #waypoint queue
                self.multi_floor_waypoints = []
                self.multi_current_goal = None #We should print this - the current goal - and maybe the goal list 
                
		#-----------------GUI Parameters----------------------------------

		#Main window 
		self.width = 700
		self.height = 500

		window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		window.set_title("Wheelchair DM")
		window.set_default_size(self.width, self.height) 
		window.connect("destroy", self.destroy, "WM destroy")

		#Notebook - for tabs		
		self.notebook = gtk.Notebook()
		self.notebook.set_tab_pos(gtk.POS_TOP)
		self.notebook.show()
		self.show_tabs = True
		self.show_border = True

		window.add(self.notebook)

		#####################################################################
		#Main tab
		
		results_vbox =  gtk.VBox()		
		results_label = gtk.Label("Dialog Manager")
		
		#add dialog manager tab 
		self.notebook.append_page(results_vbox, results_label)

		#Main Section 
		
		main_hbox = gtk.HBox()
		results_vbox.pack_start(main_hbox)

		#Speech Command buttons
		
		cmd_frame = gtk.Frame("Dialog Manager")
		cmd_vbox =  gtk.VBox()
		cmd_frame.add(cmd_vbox)

		main_hbox.pack_start(cmd_frame)                

		#Current Status Button - Listening/Sleep/Not Listening
		self.buttonLabelSize = pango.FontDescription('Sans 20')
		self.btnStartStop = gtk.Button()
		self.start_label = gtk.Label()
		self.start_label.set_markup('Not Listening')
		self.start_label.modify_font(self.buttonLabelSize)
		self.btnStartStop.add(self.start_label)
		self.btnStartStop.connect("clicked", self.start_stop)
		self.btnStartStop.set_size_request(self.width/2,40)
		
                button_hbox = gtk.HBox()
                cmd_vbox.pack_start(button_hbox)
                dm_button_vbox = gtk.VBox()

                button_hbox.pack_start(dm_button_vbox)
		dm_button_vbox.pack_start(self.btnStartStop)
                
                #Mode Display - Displays the Current DM - Tourguide/Navigation
                buttonLabelSize = pango.FontDescription('Sans 24')
		self.btnModeDisp = gtk.Button()
		self.mode_label = gtk.Label()
		self.mode_label.modify_font(buttonLabelSize)
		self.btnModeDisp.add(self.mode_label)
                self.btnModeDisp.connect("clicked",self.change_mode_handler)
		self.btnModeDisp.set_size_request(self.width/2,40)
		dm_button_vbox.pack_start(self.btnModeDisp)
                
		#Dialog Manager Status Button
                cur_result_hbox = gtk.HBox()
		cur_results_frame = gtk.Frame("Recognition List")
		cur_results_frame.add(cur_result_hbox)
                
                #Current Recognition result
                recog_results_frame = gtk.Frame("Current Recog")
		self.recog_label = gtk.Label()
		self.recog_label.set_markup('None\n')
		self.recogLabelSize = pango.FontDescription('Sans 11')
		self.recog_label.set_line_wrap(1)
                self.recog_label.set_size_request(self.width/2,75)

		self.recog_label.modify_font(self.recogLabelSize)
		recog_results_frame.add(self.recog_label)

                #Full Recognition List
		scrollWin = gtk.ScrolledWindow()
		scrollWin.set_size_request(self.width/2,self.height/2)
		cur_result_hbox.pack_start(scrollWin)

		#Text Buffer for current recognition list
		self.result = gtk.TextBuffer()
		resultView = gtk.TextView(self.result)
		resultView.set_justification(gtk.JUSTIFY_LEFT)
		scrollWin.add(resultView)

                action_hbox=  gtk.HBox()
                #Current Action - i.e. speech output
                action_results_frame = gtk.Frame("Current Action")
		self.action_label = gtk.Label()
		self.action_label.set_markup('--No action--\n')
		self.actionLabelSize = pango.FontDescription('Sans 11')
		self.action_label.set_line_wrap(1)
                self.action_label.set_size_request(self.width/4,50)

		self.action_label.modify_font(self.actionLabelSize)
		action_results_frame.add(self.action_label)
                
                #Current State of the Dialog Manager 
                dialog_state_frame = gtk.Frame("Dialog State")
		self.dialog_state_label = gtk.Label()
                self.dialog_state_label.set_justify(gtk.JUSTIFY_CENTER)
		self.dialog_state_label.set_markup(self.dialog_state)
		dialogLabelSize = pango.FontDescription('Sans 11')
		self.dialog_state_label.set_line_wrap(1)
                self.dialog_state_label.set_size_request(self.width/4,50)

		self.dialog_state_label.modify_font(dialogLabelSize)
		dialog_state_frame.add(self.dialog_state_label)

                #Navigation Information
                navigation_state_frame = gtk.Frame("Navigation State")
		self.navigation_state_label = gtk.Label()
		self.navigation_state_label.set_markup("No Goal")
		navigationLabelSize = pango.FontDescription('Sans 11')
		self.navigation_state_label.set_line_wrap(1)
                self.navigation_state_label.set_size_request(self.width/2,75)

		self.navigation_state_label.modify_font(navigationLabelSize)
                self.navigation_state_label.set_justify(gtk.JUSTIFY_LEFT)
		navigation_state_frame.add(self.navigation_state_label)
                
                action_hbox.pack_start(dialog_state_frame)
                action_hbox.pack_start(action_results_frame)

                #DM Information 
                cmd_vbox.pack_start(action_hbox)
                cmd_vbox.pack_start(navigation_state_frame)
                cmd_vbox.pack_start(recog_results_frame)                
                cmd_vbox.pack_start(cur_results_frame)

                #---------Additional Debug Information-------------#
                
                #-----LCM Message Disp
		lcm_cmd_frame = gtk.Frame("LCM Messaging")
                lcm_result_vbox = gtk.VBox()
                lcm_cmd_frame.add(lcm_result_vbox)

                cmd_list_vbox = gtk.VBox()
                cmd_list_vbox.pack_start(lcm_cmd_frame)
		

                lcm_nav_frame = gtk.Frame("Navigation")
                lcm_nav_vbox = gtk.VBox()
                lcm_nav_frame.add(lcm_nav_vbox)
                cmd_list_vbox.pack_start(lcm_nav_frame)

                #-------Quick Command Buttons

                button_hbox.pack_start(cmd_list_vbox)

                #LCM Messages
		lcm_infront_button = gtk.Button("I am infront of you")
		lcm_infront_button.set_size_request(self.width/2,20)
		lcm_infront_button.connect("clicked", self.send_infront_msg)

                
                lcm_follow_button = gtk.Button("Start following me")
		lcm_follow_button.set_size_request(self.width/2,20)
		lcm_follow_button.connect("clicked", self.send_follow_msg)

                lcm_stop_button = gtk.Button("Stop following me")
		lcm_stop_button.set_size_request(self.width/2,20)
		lcm_stop_button.connect("clicked", self.send_stop_msg)
	
                lcm_locate_button = gtk.Button("Do you see me?")
		lcm_locate_button.set_size_request(self.width/2,20)
		lcm_locate_button.connect("clicked", self.send_locate_msg)

                floor_hbox = gtk.HBox()
                self.floor_combobox = gtk.combo_box_new_text()
                #------ Select Floors
                #In Tourguide mode this should have a list of floors 
                liststore = gtk.ListStore(str)
                cell = gtk.CellRendererText()
                self.floor_combobox.pack_start(cell)
                self.floor_combobox.add_attribute(cell, 'text', 0)
                self.floor_combobox.set_wrap_width(1)
                self.connection_floor = self.floor_combobox.connect('changed', self.changed_floor_cb)
                self.floor_combobox.set_model(liststore)
                
                lcm_resend_floor_button = gtk.Button("Resend")
		lcm_resend_floor_button.set_size_request(self.width/8,20)
		lcm_resend_floor_button.connect("clicked", self.resend_floor_msg)

                floor_hbox.pack_start(self.floor_combobox)
                floor_hbox.pack_start(lcm_resend_floor_button)

                #------Goal List
                place_hbox = gtk.HBox()
                self.place_combobox = gtk.combo_box_new_text()

                liststore = gtk.ListStore(str)
                cell = gtk.CellRendererText()
                self.place_combobox.pack_start(cell)
                self.place_combobox.add_attribute(cell, 'text', 0)
                self.place_combobox.set_wrap_width(1)
                liststore.append(['Select Goal'])
                liststore.append(['None'])
                self.place_combobox.set_model(liststore)
                self.place_combobox.set_active(0)
                self.connection_place = self.place_combobox.connect('changed', self.changed_place_cb)

                lcm_resend_goal_button = gtk.Button("Resend")
		lcm_resend_goal_button.set_size_request(self.width/8,20)
		lcm_resend_goal_button.connect("clicked", self.resend_goal_msg)
                
                place_hbox.pack_start(self.place_combobox)
                place_hbox.pack_start(lcm_resend_goal_button)

                lcm_result_vbox.pack_start(lcm_infront_button)
                lcm_result_vbox.pack_start(lcm_follow_button)
                lcm_result_vbox.pack_start(lcm_stop_button)
                lcm_nav_vbox.pack_start(place_hbox)
                lcm_nav_vbox.pack_start(floor_hbox)#self.floor_combobox)
		
		
		######################################################################
		#Option Tab
		
		options_label = gtk.Label("Options")
		options_vbox = gtk.VBox()
		self.notebook.append_page(options_vbox, options_label)

		#Log Options
		log_vbox = gtk.VBox()
		log_frame = gtk.Frame("Places Log File Options")
		log_frame.add(log_vbox)
		options_vbox.pack_start(log_frame)

		#Log File name
		log_name_hbox = gtk.HBox()
		log_vbox.pack_start(log_name_hbox)

		logfile_name_label = gtk.Label("Log File Location :")
		log_name_hbox.pack_start(logfile_name_label)

		self.logfile_label = gtk.Label()
		self.logfile_label.set_markup(self.placelist_filename)
		log_name_hbox.pack_start(self.logfile_label)				

		#Choose new file
		log_file_hbox = gtk.HBox()
		log_vbox.pack_start(log_file_hbox)

		self.clear_label = gtk.Label()
		self.clear_label.set_markup('Clear Places')

		clearListButton = gtk.Button()
		clearListButton.add(self.clear_label)
		clearListButton.set_size_request(self.width/2,50)
		clearListButton.connect("clicked", self.clear_list)

		log_file_hbox.pack_start(clearListButton)

		chooseFileButton = gtk.Button("Choose File")
		chooseFileButton.set_size_request(self.width/2,50)
		chooseFileButton.connect("clicked", self.choose_file)

		log_file_hbox.pack_start(chooseFileButton)

                ################
                
                #Place Log
		places_vbox = gtk.VBox()
		places_frame = gtk.Frame("Current Places File Options")
		places_frame.add(places_vbox)
		options_vbox.pack_start(places_frame)

		#Places Log File name
		places_name_hbox = gtk.HBox()
		places_vbox.pack_start(places_name_hbox)

		placesfile_name_label = gtk.Label("Places File Location :")
		places_name_hbox.pack_start(placesfile_name_label)

		self.placesfile_label = gtk.Label()
		self.placesfile_label.set_markup(self.placelocations_filename)
		places_name_hbox.pack_start(self.placesfile_label)				

		#Choose new file
		places_file_hbox = gtk.HBox()
		places_vbox.pack_start(places_file_hbox)

                chooseFileButton = gtk.Button("Choose Locations File")
		chooseFileButton.set_size_request(self.width,25)
		chooseFileButton.connect("clicked", self.choose_locations_file)

		places_file_hbox.pack_start(chooseFileButton)

                ################               

		
		sound_hbox = gtk.HBox()
		sound_frame = gtk.Frame("Sound Input Options")
		sound_frame.add(sound_hbox)
		options_vbox.pack_start(sound_frame)

		#Audio File logging 
		log_audio_vbox = gtk.VBox()
		log_audio_frame = gtk.Frame("Audio File Logging Options")
		log_audio_frame.add(log_audio_vbox)
		options_vbox.pack_start(log_audio_frame)
		
		button = gtk.CheckButton("Log Audio Files")
                #gtk.RadioButton(None, "Log Audio Files")
		button.connect("toggled", self.log_data)
		log_audio_vbox.pack_start(button, True, True, 0)
		button.show()

		#Hardware selection 
		
		button = gtk.RadioButton(None, "main mic")
		button.connect("toggled", self.change_input, "default")
		sound_hbox.pack_start(button, True, True, 0)
		button.show()
		
		button = gtk.RadioButton(button, "usb mic")
		button.connect("toggled", self.change_input, "usb")		
		sound_hbox.pack_start(button, True, True, 0)
		button.show()
		
		button = gtk.RadioButton(button, "bluetooth")
		button.connect("toggled", self.change_input, "bluetooth")
		sound_hbox.pack_start(button, True, True, 0)
                button.set_active(True)
		button.show()		

		vocab_hbox = gtk.HBox()
		vocab_frame = gtk.Frame("Vocabulary Options")
		vocab_frame.add(vocab_hbox)
		options_vbox.pack_start(vocab_frame)

		button = gtk.RadioButton(None, "Stata Fl:1")
		button.connect("toggled", self.change_floor, "stata_floor_1")
		vocab_hbox.pack_start(button, True, True, 0)
		button.show()
		
		self.toggle_button = gtk.RadioButton(button, "Stata Fl:3")
		self.toggle_button.connect("toggled", self.change_floor, "stata_floor_3")		
		vocab_hbox.pack_start(self.toggle_button, True, True, 0)
		self.toggle_button.show()
		
		self.toggle_button_8 = gtk.RadioButton(self.toggle_button, "Stata Fl:8")
		self.toggle_button_8.connect("toggled", self.change_floor, "stata_floor_8")
		vocab_hbox.pack_start(self.toggle_button_8, True, True, 0)
		self.toggle_button_8.show()		

		self.toggle_button_tbh = gtk.RadioButton(self.toggle_button_8, "TBH")
		self.toggle_button_tbh.connect("toggled", self.change_floor, "tbh")
		vocab_hbox.pack_start(self.toggle_button_tbh, True, True, 0)
		button.show()

		button = gtk.RadioButton(self.toggle_button_tbh, "New")
		button.connect("toggled", self.change_floor, "new")
		vocab_hbox.pack_start(button, True, True, 0)
		button.show()

                #Debug Tab		
		debug_label = gtk.Label("Debug")
		debug_vbox = gtk.VBox()
		self.notebook.append_page(debug_vbox, debug_label)

                lcm_results_frame = gtk.Frame("LCM Message Result")
		self.lcm_label = gtk.Label()
		self.lcm_label.set_markup('\tNone\n')
		self.lcmLabelSize = pango.FontDescription('Sans 8')
		self.lcm_label.set_line_wrap(1)
                self.lcm_label.set_size_request(self.width/2,250)
                self.lcm_label.set_justify(gtk.JUSTIFY_LEFT)

		self.lcm_label.modify_font(self.lcmLabelSize)
                lcm_results_frame.add(self.lcm_label)
                debug_vbox.pack_start(lcm_results_frame)

                #Places List - Tagged Places		
		places_hbox = gtk.HBox()
		places_frame = gtk.Frame("Tagged Places List")
		places_frame.add(places_hbox)

                debug_vbox.pack_start(places_frame)

		#Made scrollable
		scrollWin1 = gtk.ScrolledWindow()
		scrollWin1.set_size_request(self.width,self.height/2)
		places_hbox.pack_start(scrollWin1)

		#Text Buffer for all places in the list
		self.placebuf = gtk.TextBuffer()
		resultView1 = gtk.TextView(self.placebuf)
		resultView1.set_justification(gtk.JUSTIFY_LEFT)
		scrollWin1.add(resultView1)

                #Locations List		
		nav_locs_hbox = gtk.HBox()
		nav_locs_frame = gtk.Frame("Navigatable Places List")
		nav_locs_frame.add(nav_locs_hbox)

                debug_vbox.pack_start(nav_locs_frame)

		#Made scrollable
		scrollWin1 = gtk.ScrolledWindow()
		scrollWin1.set_size_request(self.width,self.height/2)
		nav_locs_hbox.pack_start(scrollWin1)

		#Text Buffer for all places in the list
		self.nav_locs_buf = gtk.TextBuffer()
		resultView1 = gtk.TextView(self.nav_locs_buf)
		resultView1.set_justification(gtk.JUSTIFY_LEFT)
		scrollWin1.add(resultView1)
                
		window.show_all()
                
		self.get_colors(window)
		self.set_color('offline')
                self.load_place_file()
                print "Changing DM Mode"
                
                self.disp = []
                self.background = lcm_listener(self.speaker,self.update_label,
                                               self.handle_lcm, self.multi_navigation_handler, 
                                               self.floor_update_handler, self.places_update_handler , 
                                               self.cost_result_handler)
                self.background.check_floor_status()
                #starting listening
                self.change_DM_mode(mode)
                self.start_listening()
		self.background.start()
                #self.toggle_button_8.set_active(True)
                
		return

        #=================================================================================#
        #======================Helper Functions===========================================#

        #--------GUI Element Handlers------------
        def get_colors(self,window):		
		self.gc = window.get_style().fg_gc[gtk.STATE_NORMAL]
		self.colormap = self.gc.get_colormap()
		self.white = self.colormap.alloc_color('white')
		self.black = self.colormap.alloc_color('black')
		self.magenta = self.colormap.alloc_color('magenta')
		self.green = self.colormap.alloc_color('green')
		self.gray = self.colormap.alloc_color('gray')
		self.red = self.colormap.alloc_color('red')
		self.lightgray = self.colormap.alloc_color(60000,60000,60000)
		self.pink = self.colormap.alloc_color('pink')
		self.blue = self.colormap.alloc_color('blue')
		self.orange = self.colormap.alloc_color('orange')
		self.yellow = self.colormap.alloc_color('yellow') 

        #### Status Updates on the GUI ####
        
        #Updates the display of the current place loactions (spoken tagged locations)
        def update_placelist_display(self):
           list_text = ""
           count = 0
           for place in self.places_list:
              count = count + 1
              list_text = list_text + str(count) + " | Time : " + str(place['time']) + " | " + \
                  "Name : " + place['label'] + " | Type : " + place['type'] + "\n"
           self.placebuf.set_text(list_text)

        #Updates the last lcm message log 
        def update_label(self,msg): 
           #print "____Message Handler Called_____"
           #Might consider making this a queue
           type = str(msg.__class__)
           msg_info = {'type': type[type.rfind(".")+1:type.rfind("\'")]}
           self.disp.insert(0,msg_info)
           if (len(self.disp) >5):
              self.disp.pop()
           disp = ""
           for i in self.disp:
              disp = "Type : " +  i['type'] + "\n" 

        #Sets up audio data logging
        def log_data(self,widget):
           if widget.get_active():
              self.logging_mode = 1
              print "Logging Audio Files"
           else:
              self.logging_mode = 0
              print "Not Logging Audio Files"

        #Formats Room name for speech synthesis
        def format_location(self,location):
		
           dash_point = location.find("-")
           
           building_name = location[0:location.find("-")]
           
           room_number = location[location.find("-")+1:len(location)]
           
           room_name = ""
           for i in room_number:
              room_name = room_name + i + ","
              
           full_room_name = building_name + " dash " + room_name
           
           return full_room_name

        def change_floor(self,widget,data=None):
           if widget.get_active():
              if data == "new":
                 print "Adding a new Vocabulary" 
                 self.choose_vocab_file(widget)
              else:
                 print "Floor Changed to : " , data
                 self.placelist_filename = "../../../../data/logs/" + data + ".log"
                 self.tour_config_file = "../../../../data/recog/" + data + ".cfg"
                 self.curr_config_file = self.tour_config_file
                 self.current_floor = data
                 self.load_place_file()
                 self.logfile_label.set_markup(self.placelist_filename)
                 self.stop_listening()
                 self.start_listening()


        #Changing the listening State

        def set_color(self, state):

		if state == 'offline':
			self.btnStartStop.modify_bg(gtk.STATE_NORMAL,self.white)
			self.btnStartStop.modify_bg(gtk.STATE_PRELIGHT,self.white)
			self.btnStartStop.modify_bg(gtk.STATE_ACTIVE,self.white)
			self.start_label.set_text("Not Listening")

                elif state == 'sleep':
			self.btnStartStop.modify_bg(gtk.STATE_NORMAL,self.gray)
			self.btnStartStop.modify_bg(gtk.STATE_PRELIGHT,self.gray)
			self.btnStartStop.modify_bg(gtk.STATE_ACTIVE,self.gray)
			self.start_label.set_text("Sleeping")

		elif state == 'listening':

			self.btnStartStop.modify_bg(gtk.STATE_NORMAL,self.green)
			self.btnStartStop.modify_bg(gtk.STATE_PRELIGHT,self.green)
			self.btnStartStop.modify_bg(gtk.STATE_ACTIVE,self.green)
			self.start_label.set_text("Listening")			

		elif state == 'heard':
			
			self.btnStartStop.modify_bg(gtk.STATE_NORMAL,self.orange)
			self.btnStartStop.modify_bg(gtk.STATE_PRELIGHT,self.orange)
			self.btnStartStop.modify_bg(gtk.STATE_ACTIVE,self.orange)
			self.start_label.set_text("Heard")

                elif state == 'processing':
			
			self.btnStartStop.modify_bg(gtk.STATE_NORMAL,self.red)
			self.btnStartStop.modify_bg(gtk.STATE_PRELIGHT,self.red)
			self.btnStartStop.modify_bg(gtk.STATE_ACTIVE,self.red)
			self.start_label.set_text("Processing")

		print "Changed Color " 

        #### Logging Related ####
	
        #Clears the Placelist Log
	def clear_list(self,window):
		print "Clearing List"
		try:
			place_log = open(self.placelist_filename,'w')			
			place_log .close()
			self.places_list = []
			print "Placelist File cleared"
			
		except IOError, e:
			self.places_list = []
			print "Unable to load from file "

        #Loads Tagged location file                 
        def load_place_file(self):  #Tagged File for the tour 
		try:
			place_log = open(self.placelist_filename,'r')
			self.places_list = cPickle.load(place_log)	
			print "File Location " , self.placelist_filename
			print self.places_list
			place_log.close()
			
		except IOError, e:

			self.places_list = []
			print "Unable to load from file "

		except EOFError,e:
			self.places_list = []
			print "Unable to load from file "

		self.update_placelist_display()


        #load places file - Should be defunct x
        def load_map_place_file(self):   #This should be removed 
                print "Map Place locations"
                print self.placelocations_filename
                try:
                   
                   place_log = open(self.placelocations_filename,'r')
                   self.map_places_list = cPickle.load(place_log)	
                   print "File Location " , self.placelocations_filename
                   print self.map_places_list
                   place_log.close()
			
		except IOError, e:

                   self.map_places_list = []
                   print "Unable to load from file "
                   
		except EOFError,e:
                   self.map_places_list = []
                   print "Unable to load from file "
        
        #### File Chooser dialogs ####
        
        #Should be defunct as we are setting the map server to give this out
        def choose_locations_file(self,window):
           print "Choose new locations file"

        #Choose new file to log the speech uttrances 
	def choose_file(self,window):
		print "Choose new log file"
		dialog = gtk.FileChooserDialog("Open..",
					       None,
					       gtk.FILE_CHOOSER_ACTION_SAVE,
					       (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
						gtk.STOCK_OPEN, gtk.RESPONSE_OK))
		dialog.set_default_response(gtk.RESPONSE_OK)
		
		filter = gtk.FileFilter()
		filter.set_name("All files")
		filter.add_pattern("*")
		dialog.add_filter(filter)
		response = dialog.run()
		if response == gtk.RESPONSE_OK:
			print dialog.get_filename(), 'selected'
			if os.access(dialog.get_filename(), os.F_OK):
				print "File Exists : " , os.access(dialog.get_filename(), os.F_OK)
				overwrite_dig = gtk.Dialog(title="File Exists")
				overwrite_button = overwrite_dig.add_button("OVERWRITE", gtk.RESPONSE_OK)
				append_button = overwrite_dig.add_button("APPEND", gtk.RESPONSE_APPLY)
				cancel_button = overwrite_dig.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)

				cancel_button.grab_default()
				over_res = overwrite_dig.run()

				self.update_log(over_res,dialog.get_filename())
				overwrite_dig.destroy()

		elif response == gtk.RESPONSE_CANCEL:
			print 'Closed, no files selected'
		dialog.destroy()

        
	
               

	def choose_vocab_file(self,window):
		print "Choose new vocab file"
		dialog = gtk.FileChooserDialog("Open..",
					       None,
					       gtk.FILE_CHOOSER_ACTION_OPEN,
					       (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
						gtk.STOCK_OPEN, gtk.RESPONSE_OK))
		dialog.set_default_response(gtk.RESPONSE_OK)
		
		filter = gtk.FileFilter()
		filter.set_name("All files")
		filter.add_pattern("*.jsgf")
		dialog.add_filter(filter)
		response = dialog.run()
		if response == gtk.RESPONSE_OK:
			file_name = dialog.get_filename()

			name_start = file_name.rfind("/")
			name_end = file_name.rfind(".jsgf")
			file_name_frag = file_name[name_start+1:name_end]
			print "New JSGF File " , file_name_frag 
			
			self.placelist_filename = "../../../../data/logs/" + file_name_frag + ".log"
			self.tour_config_file = file_name[0:name_end] + ".cfg"
			print self.tour_config_file
                        self.stop_listening()
                        self.curr_config_file = self.tour_config_file
                        self.start_listening()
			
		elif response == gtk.RESPONSE_CANCEL:
			print 'Closed, no files selected'
		dialog.destroy()	

	def update_log(self,response,file_name):
		if response == gtk.RESPONSE_OK:
			print "Overwrite File"
		elif response == gtk.RESPONSE_APPLY:
			print "Append"
		elif response == gtk.RESPONSE_CANCEL:
			print "Canceled"			
			
	
                #### GUI Button Event Handlers

        #Changes the type of input 
        def change_input(self,widget,data=None):
           if widget.get_active():
              self.device = data
              print "Listening Device Changed to : " , data

        #Start/Stop Button for Listener           
	def start_stop(self, w):
                if self.listening==0:
                        self.start_listening()
		else:
                        self.stop_listening()			

        #Change Mode Button for Listener           
	def change_mode_handler(self, w):
           print "Change Mode Handler"
           if (self.DM_mode == "tourguide"):
              self.change_DM_mode("navigation")
           elif (self.DM_mode == "navigation"):
              self.change_DM_mode("tourguide")
           
	def destroy(self, widget, data=None):
                self.stop_listening()
		print "Exiting"
		self.background.stop()
		self.background.join()
		print "Done"
		gtk.main_quit()


        #### Speech Command Parsing Helper Functions 

        # Parse command and get keywords
	def get_command(self, result, key):		
		found = result.find(key)		
		if (found >=0):
			start_loc = result.find("=",found)
			end_loc = result.find("]",start_loc)
			return result[start_loc+1:end_loc]
		else:
			return 'none'

        #=================================================================================#
        #======================LCM =======================================================#


        #--------Background LCM message senders
        def send_lcm_speech_msg(self,window):
           self.background.send_speech_msg("FOLLOW","PERSON")
                
        def send_infront_msg(self,window):
           self.background.send_speech_msg("TRACKER","START_LOOKING")

        def send_floor_change_msg(self, window):
           self.background.send_floor_change_msg(self.current_floor_no)
           
        def send_follow_msg(self,window):
           self.background.send_speech_msg("FOLLOWER","START_FOLLOWING")

        def send_stop_msg(self,window):
           self.background.send_speech_msg("FOLLOWER","IDLE")

        def send_locate_msg(self,window):
           self.background.send_speech_msg("TRACKER","STATUS")

        #LCM navigation messages 
        def send_goal_msg(self,goal, use_theta):
           print "Message Goal :" , goal
           if(goal['location'] == 'elevator'):
              print "Special Goal location - checking for multiple possible co-ordinates"
              if(len(goal['pos'])==1):
                 print "Single elevator found - normal operation"
              else:
                 print "Multiple elevators found - special operation"
              self.background.send_elevator_goal_msg(goal,use_theta)
           else:
              self.background.send_goal_msg(goal,use_theta)

        def send_stop(self):
           self.dialog_state = 'init'
           print "Stop Called" 
           self.background.send_stop_msg()
           #Should return this only if we are moving
           if((self.DM_mode == "navigation" and len(self.multi_floor_waypoints)>0) or self.DM_mode == 'tourguide'):
              #Clearing the waypoints 
              self.multi_floor_waypoints = []
              return "stopping"
           else:
              print "Not Moving"
              return ""     

        def handle_lcm(self,param_dict):#mode, suggested_floor): 
            mode = param_dict[0]
            suggested_floor = param_dict[1]
            print "Handler Called" 
            #if(suggested_floor =='0'):
            #   suggested_floor = 'ground'
            if(mode=="suggestion"):
               self.poss_new_floor_no = int(suggested_floor)
               self.dialog_state = 'floor_change' 
               self.curr_config_file = self.confirm_config_file
               self.speaker.say("are we back on floor " + suggested_floor)
            elif(mode=='querry'):
               self.speaker.say("which floor are we on")
               self.dialog_state = 'floor_change_querry' 
               self.curr_config_file = self.new_floor_config_file

            elif(mode =='auto_change'):
               print "Automatic floor change "
               print "Suggested Floor " , suggested_floor
               self.current_floor_no = int(suggested_floor)
               if(self.current_floor_no !=-1):
                  self.multi_navigation_floor_handler()
               else:
                  self.speaker.say("which floor are we on")
                                  
            if(mode !='auto_change' or (mode =='auto_change' and self.current_floor_no ==-1)):
               if(self.mode == 'recording'):
                  print "Already Listening - stopping recorder"
                  self.recorder.set_state(gst.STATE_NULL)

               self.minspeech = 25000
               self.mode = 'recording'
               #Make recorder and restart the listening process
               self.make_recorder()
               #start recorder 
               self.recorder.set_state(gst.STATE_PLAYING)
               self.start_label.set_text("Listening")				
               print "Listening"
               self.set_color('listening')
        

        #=================================================================================#
        #======================Navigation=================================================#
        def cost_result_handler(self, cost_to_elevators):
           print "Costs received"
           print cost_to_elevators
           print "Goal ID : " , self.last_goal['id']
           print "Goal Floor No " , self.last_goal['floor_no']

           speech_output = ""

           min_cost_elevator_ind = -1
           min_cost = 1000000
           
           cost_mat = {}

           for i in cost_to_elevators.keys():
              cost = self.background.get_costs_from_elevators(self.last_goal['floor_no'], i, self.last_goal['id'])
              if(cost == -1):
                 print "Elevator [", i,"] does not travel to the goal floor" 
              else:
                 print "Cost from elevator : " , i , " : " , cost

                 total_cost = cost_to_elevators[i]['cost'] + cost
                 cost_mat[i] = total_cost
                 print "Total Cost : "  , total_cost
                 if(min_cost > total_cost):
                    min_cost = total_cost
                    min_cost_elevator_ind = i

           print "Closest Elevator : " , min_cost_elevator_ind
           if(min_cost_elevator_ind < 0):
              self.speaker.say("goal is unreachable")  
              print "Goal Unreachable"
              return
           
           print "------Valid Path Found------"
           
           closest_ele = {}
           #add this valid elevator 
           valid_elevators = [cost_to_elevators[min_cost_elevator_ind]['elevator']]

           closest_ele['x'] = cost_to_elevators[min_cost_elevator_ind]['elevator']['x']
           closest_ele['y'] = cost_to_elevators[min_cost_elevator_ind]['elevator']['y']

           ###################################################################
           #####  New way to handle elevators ####
           #search around the closest elevator 
           
           valid_elevator_count = 1
           for i in cost_mat.keys():
              if(i==min_cost_elevator_ind):
                 print "This is the closest elevator"
              else:
                 temp_ele = cost_to_elevators[i]['elevator']
                 print temp_ele
                 #ideally should search dist from all elevators 
                 dist = math.sqrt(pow(temp_ele['x'] - closest_ele['x'],2) + \
                                     pow(temp_ele['y'] - closest_ele['y'],2))
                 print "Dist from Closest Elevator : " , dist 
                 if(dist < 10.0): 
                    print "Close enough to the closest elevator - considering both"
                    valid_elevator_count += 1
                    valid_elevators.append(temp_ele)
           ###################################################################

           self.multi_floor_waypoints = []

           '''self.multi_floor_waypoints = []

           ele_controller = None

           no_ele = 0
           valid_elevator_count = 0 #valid elevator is an elevator that is present on both floors 

           valid_elevators = []

           for i in self.background.elevator_places: #check through the elevators 
              print "Elevator " , (no_ele +1), " : " , i['x'], ",", i['y']
              no_ele +=1
              ele_floor_info = i['floors']


              accessible_floors = set(ele_floor_info.keys())

              if(accessible_floors.intersection([self.last_goal['floor_no']]) and \
                    accessible_floors.intersection([self.current_floor_no])):
                 print "Elevator connects both current floor and the goal floor"
                 valid_elevator_count += 1

                 print "Appending valid elevator"
                 print ele_floor_info[self.current_floor_no]
                 valid_elevators.append(ele_floor_info[self.current_floor_no])'''

           print "---------Valid elevator set " 
           print valid_elevators

           if(valid_elevator_count==1):
              print "Found only one usable elevator - setting this as the intermediate goal"

           if(valid_elevator_count >1):
              print "Found more than one valid elevator"

           if(valid_elevator_count >=1):               

              dist_controller = 15.0
              #ideally the elevator controller should be attached to the elevator info

              for j in self.background.nav_places:
                 if(j['name']=='elevator_controller' and j['floor_no'] == self.current_floor_no):
                    #check the distance and pick the closest controller - if its within a certain radius 
                    for i in valid_elevators:
                       temp_dist = math.sqrt(pow(j['x'] - i['x'],2) + pow(j['y'] - i['y'],2))
                       print "\t\t===Dist to elevator Controller : ", temp_dist
                       if(dist_controller > temp_dist):
                          dist_controller = temp_dist
                          ele_controller = j

              if(ele_controller != None):
                 print "Found an elevator controller - Adding"
                 waypoint = {'location': 'elevator_controller', 'floor_no':  ele_controller['floor_no'], \
                                'x':ele_controller['x'],'y':ele_controller['y'],\
                                'theta':ele_controller['theta']}
                 self.multi_floor_waypoints.append(waypoint)

              #the way this is added depends on the no of valid elevators
              waypoint = {'location': 'elevator', 'floor_no': self.current_floor_no , 'pos':[]}
              for i in valid_elevators:
                 print "Adding elevators"
                 print i
                 waypoint['pos'].append({'x':i['x'], 'y':i['y'], 'door_x':i['door_x'], 'door_y':i['door_y']})

              print waypoint                                       

              self.multi_floor_waypoints.append(waypoint)


           #adding the last waypoint to be the actual goal
           last_waypoint = {'location': self.last_goal['location'], 'floor_no': self.last_goal['floor_no'],\
                               'x':self.last_goal['x'],'y':self.last_goal['y']}
           self.multi_floor_waypoints.append(last_waypoint)

           for i in self.multi_floor_waypoints:
              print "Waypoint : ", i

           if(valid_elevator_count >=1):
              if(ele_controller != None):
                 speech_output += " going to the elevator controller first"   
                 print "Going to elevator controller first"
                 print self.multi_floor_waypoints[0]
                 self.send_goal_msg(self.multi_floor_waypoints[0], 1)
                 self.navigation_mode = "multi_floor"
              else:
                 speech_output += " going to the elevator first"   
                 print "Going to elevator first"
                 self.send_goal_msg(self.multi_floor_waypoints[0], 0)
                 self.navigation_mode = "multi_floor"

           else:
              speech_output += " I can not find an elevator on this floor"
           #self.speaker.say(speech_output)
           self.update_navigation_list()
           return speech_output

        #this should not be on pose - but on goal reached 
        def multi_navigation_handler(self,param_dict):
           goal_reached = param_dict[0]
           goal_x = param_dict[1]
           goal_y = param_dict[2]
           if(len(self.multi_floor_waypoints) == 0):
              return
           
           print "Multi nav handler called" 
           print "X:", goal_x, ", Y:", goal_y
           print "Current Goal"
           print self.multi_floor_waypoints[0]

           print "All waypoints"
           print self.multi_floor_waypoints

           dist = 1000

           if(self.multi_floor_waypoints[0]['location'] == 'elevator'):
           #this will mess up when there are multiple elevators 
              for i in self.multi_floor_waypoints[0]['pos']:
                 temp_dist = math.sqrt(pow(goal_x - i['x'],2) + \
                                  pow(goal_y - i['y'],2))
                 if(temp_dist < dist):
                    dist = temp_dist 
              print "Min Distance from an elevator " , dist
           else:
              dist = math.sqrt(pow(goal_x - self.multi_floor_waypoints[0]['x'],2) + \
                                  pow(goal_y - self.multi_floor_waypoints[0]['y'],2))
           if(goal_reached==1 and dist < 0.2):
              if(self.navigation_mode == "multi_floor" and len(self.multi_floor_waypoints) > 0):
                 if(len(self.multi_floor_waypoints) ==1):
                    print "Arrived at the goal"
                    self.speaker.say("we are at the "+ self.multi_floor_waypoints[0]['location'])
                    self.multi_floor_waypoints.pop(0)
                    self.navigation_mode = []

                 else:
                    print "Arrived at " , self.multi_floor_waypoints[0]['location']
                    if(self.multi_floor_waypoints[0]['location'] == 'elevator_controller'):
                       #ideally we should wait for a goal reached message from the navigator
                       self.multi_floor_waypoints.pop(0)
                       self.send_goal_msg(self.multi_floor_waypoints[0],0)
                       self.speaker.say("called an elevator, going to the elevator")
                    elif(self.multi_floor_waypoints[0]['location'] == 'elevator'):
                       print "Waiting until we reach the correct floor"
                       self.multi_floor_waypoints.pop(0)
                       self.speaker.say("we are at the elevator, waiting until we reach floor " + str(self.multi_floor_waypoints[0]['floor_no']))
                    else:
                       self.multi_floor_waypoints.pop(0)                       
                       self.send_goal_msg(self.multi_floor_waypoints[0],0)
                       self.speaker.say("We are at the "  + self.multi_floor_waypoints[0]['location'] + ", i do not know what to do")
                       
              else:
                 if(len(self.multi_floor_waypoints) ==1):
                    print "Arrived at the goal"
                    self.speaker.say("we are at the "+ self.multi_floor_waypoints[0]['location'])
                    self.multi_floor_waypoints.pop(0)
                    self.navigation_mode = []

              self.update_navigation_list()

        def multi_navigation_floor_handler(self):
           if(self.navigation_mode == "multi_floor" and len(self.multi_floor_waypoints) > 0):
              print self.multi_floor_waypoints
              #check if we are on the target floor 
              if(self.multi_floor_waypoints[0]['location'] == 'elevator'):
                 #the elevator waypoint was not removed from the stack 
                 #check if its this elevator that we are in
                 if(self.background.last_pose_msg !=None):
                    dist = 1000

                    if(self.multi_floor_waypoints[0]['location'] == 'elevator'):
                       
                       for i in self.multi_floor_waypoints[0]['pos']:
                          temp_dist = math.sqrt(pow(self.background.last_pose_msg.pos[0] - i['x'],2) + \
                                                   pow(self.background.last_pose_msg.pos[1] - i['y'],2))
                          if(temp_dist < dist):
                             dist = temp_dist 
                       print "Min Distance from an elevator " , dist
                    else:
                       dist = math.sqrt(pow(self.background.last_pose_msg.pos[0] - self.multi_floor_waypoints[0]['x'],2) + \
                                           pow(self.background.last_pose_msg.pos[1] - self.multi_floor_waypoints[0]['y'],2))

                    
                 #print "We are at the elevator - waiting for the correct floor"
                    if(dist < 10.0):
                       self.multi_floor_waypoints.pop(0)
                       #self.speaker.say("we are at the elevator waiting until we reach floor " + str(self.multi_floor_waypoints[0]['floor_no']))
              if(self.multi_floor_waypoints[0]['floor_no'] == self.current_floor_no):
                 self.send_goal_msg(self.multi_floor_waypoints[0],0)
                 print "We are on the target floor" 
                 print "Exiting elevator and going to the " + self.multi_floor_waypoints[0]['location']

                 self.speaker.say("We are on floor " + str(self.current_floor_no) + ", exiting elevator and going to the " + self.multi_floor_waypoints[0]['location'])
           else:
              self.speaker.say("We are on floor " + str(self.current_floor_no))

           self.update_navigation_list()

        #-----------Navigation Place list handler

        def places_update_handler(self,places):
           print "+++++++Received Places+++++"
           model = self.place_combobox.get_model()
           index = self.place_combobox.get_active()
           self.place_combobox.disconnect(self.connection_place)
           model.clear()
           for i in self.background.nav_places:
              model.append([i['name'] + " : Fl" + str(i['floor_no'])])
           self.place_combobox.set_active(0)
           self.connection_place = self.place_combobox.connect('changed', self.changed_place_cb)

        #---------Update the navigation waypoints
        def update_navigation_list(self):
           print "Update Navigation List"
           nav_list = ""
           if(len(self.multi_floor_waypoints)==0):
              self.navigation_state_label.set_markup("No Goal")
           else:
              #print self.multi_floor_waypoints
              for i in self.multi_floor_waypoints:
                 nav_list += i['location'] + " = Floor : " + str(i['floor_no']) + "\n"
           
              self.navigation_state_label.set_markup(nav_list)

        #--------Place Command Handler
        def changed_place_cb(self,window):
           print "Called"
           model = self.place_combobox.get_model()
           index = self.place_combobox.get_active()
           print "Current Floor : " , self.current_floor_no
           if index > -1:
              print self.background.nav_places[index]['name']
              print model[index][0], 'selected'
              #Set this as the last goal
              if(self.background.last_pose_msg !=None):
                 #why not use the actual place ????
                 self.last_goal['location'] = self.background.nav_places[index]['name']
                 self.last_goal['x'] = self.background.nav_places[index]['x']
                 self.last_goal['y'] = self.background.nav_places[index]['y']
                 self.last_goal['floor_no'] = self.background.nav_places[index]['floor_no']
                 self.last_goal['pron'] = self.background.nav_places[index]['name']
                 self.last_goal['id'] = self.background.nav_places[index]['id']
                 self.goal_confirmation()
              else:
                 print " ++++++ No poses received"
                 speech_output = "I do not know where I am"
                 self.speaker.say(speech_output)

           return

        #=================================================================================#
        #======================Floor Change===============================================#

        #---------------Floor Update Handler

        def floor_update_handler(self,floors):
           if(self.DM_mode =='navigation'):
              print "+++++++Received Floors+++++"
              model = self.floor_combobox.get_model()
              index = self.floor_combobox.get_active()
              self.floor_combobox.disconnect(self.connection_floor)
              model.clear()
              for i in self.background.floor_map:
                 model.append(["Fl: " + str(i)])
              self.floor_combobox.set_active(0)
              self.connection_floor = self.floor_combobox.connect('changed', self.changed_floor_cb)
           elif(self.DM_mode =='tourguide'):
              print "In tourguide Mode" 

        #-----------------Seed Floor No's

        def seed_floors(self,no_floors):
           print "+++++++Seeding Floors+++++"
           model = self.floor_combobox.get_model()
           index = self.floor_combobox.get_active()
           self.floor_combobox.disconnect(self.connection_floor)
           model.clear()
           for i in range(no_floors):
              model.append(["Fl: " + str(i)])
           self.floor_combobox.set_active(0)
           self.connection_floor = self.floor_combobox.connect('changed', self.changed_floor_cb)

        #------Change Floor No--------

        def resend_floor_msg(self,window):
           print "Called"
           model = self.floor_combobox.get_model()
           index = self.floor_combobox.get_active()
           if index > -1:
              print model[index][0], 'selected'   
              if(self.DM_mode == 'navigation'):
                 if(self.background.floor_map[index] !=-1):
                    print "Selected Floor : " , self.background.floor_map[index]
                    self.current_floor_no =  self.background.floor_map[index]
                    self.background.send_floor_change_msg(self.current_floor_no)

        def resend_goal_msg(self,window):
           print "Called"
           model = self.place_combobox.get_model()
           index = self.place_combobox.get_active()
           print "Current Floor : " , self.current_floor_no
           if index > -1:
              print self.background.nav_places[index]['name']
              print model[index][0], 'selected'
              #Set this as the last goal
              if(self.background.last_pose_msg !=None):
                 #why not use the actual place ????
                 self.last_goal['location'] = self.background.nav_places[index]['name']
                 self.last_goal['x'] = self.background.nav_places[index]['x']
                 self.last_goal['y'] = self.background.nav_places[index]['y']
                 self.last_goal['floor_no'] = self.background.nav_places[index]['floor_no']
                 self.last_goal['pron'] = self.background.nav_places[index]['name']
                 self.last_goal['id'] = self.background.nav_places[index]['id']
                 self.goal_confirmation()
              else:
                 print " ++++++ No poses received"
                 speech_output = "I do not know where I am"
                 self.speaker.say(speech_output)

           return

        def changed_floor_cb(self,window):
           print "Called"
           model = self.floor_combobox.get_model()
           index = self.floor_combobox.get_active()
           if index > -1:
              print model[index][0], 'selected'   
              if(self.DM_mode == 'navigation'):
                 if(self.background.floor_map[index] !=-1):
                    print "Selected Floor : " , self.background.floor_map[index]
                    self.current_floor_no =  self.background.floor_map[index]
                    self.background.send_floor_change_msg(self.current_floor_no)
                    self.multi_navigation_floor_handler()
                    #floor name gets said - bacause of the map_server
                 else:
                    self.speaker.say("which floor are we on")
              elif(self.DM_mode == 'tourguide'):
                 print "In TG Mode"
                 floor_str = model[index][0]
                 start_ind = floor_str.find(":")
                 print "Floor No is :" + floor_str[start_ind+2:]
                 self.current_floor_no = int(floor_str[start_ind+2:])
                 self.background.send_floor_change_msg(self.current_floor_no)
                 speech_output = "we are on floor " + str(self.current_floor_no)
                 self.speaker.say(speech_output)
              #Send Floor Change message here
           return

        #=======================================================================================================#
        #=====================Dialog Manager Mode ==============================================================#
        

        #Changes between Mode selection/Tourguide and Navigation mode
        def change_DM_mode(self,mode):

           speech_output = "" 
           if (self.DM_mode == mode): #We have nothing to do
              print "Nothing to do"
              return
           else:
              print "Doing mode change"
              if self.listening ==1:
                 if self.logging_mode ==0:
                    self.stop_listening()
                 self.DM_mode =  mode
                 self.dialog_state = 'init'
                 self.background.send_wheelchair_mode(self.DM_mode)
                 if mode == "tourguide":
                    self.seed_floors(5) #reseed the floor list  
                    #ask which floor we are on 
                    if(self.current_floor_no ==-1):
                       print "Current Floor Unknown"
                       speech_output = "Which floor are we on"
                    self.btnModeDisp.modify_bg(gtk.STATE_NORMAL,self.yellow)
                    self.btnModeDisp.modify_bg(gtk.STATE_PRELIGHT,self.yellow)
                    self.btnModeDisp.modify_bg(gtk.STATE_ACTIVE,self.yellow)
                    self.mode_label.set_text("Tourguide Mode")
                    self.curr_config_file = self.tour_config_file                     
                 
                 elif mode == 'navigation':           
                    self.btnModeDisp.modify_bg(gtk.STATE_NORMAL,self.red)
                    self.btnModeDisp.modify_bg(gtk.STATE_PRELIGHT,self.red)
                    self.btnModeDisp.modify_bg(gtk.STATE_ACTIVE,self.red)
                    self.mode_label.set_text("Navigation Mode")
                    self.curr_config_file = self.nav_config_file
                    print "Sending place request"
                    self.background.check_floor_status()
                    self.background.send_place_request_msg()
                    #check floor status 
                    #ask floor status if we dont have a floor 

           
                 elif mode == 'mode_select':           
                    self.btnModeDisp.modify_bg(gtk.STATE_NORMAL,self.gray)
                    self.btnModeDisp.modify_bg(gtk.STATE_PRELIGHT,self.gray)
                    self.btnModeDisp.modify_bg(gtk.STATE_ACTIVE,self.gray)
                    self.mode_label.set_text("Startup")
                    self.curr_config_file = self.mode_config_file

                 if self.logging_mode ==0:
                    self.start_listening()

              else:
                 self.DM_mode =  mode
                 self.dialog_state = 'init'
                 self.background.send_wheelchair_mode(self.DM_mode)
                 if mode == "tourguide":
                    self.seed_floors(5) 
                    self.btnModeDisp.modify_bg(gtk.STATE_NORMAL,self.yellow)
                    self.btnModeDisp.modify_bg(gtk.STATE_PRELIGHT,self.yellow)
                    self.btnModeDisp.modify_bg(gtk.STATE_ACTIVE,self.yellow)
                    self.mode_label.set_text("Tourguide Mode")
                    self.curr_config_file = self.tour_config_file 
                 
                 elif mode == 'navigation':           
                    self.btnModeDisp.modify_bg(gtk.STATE_NORMAL,self.red)
                    self.btnModeDisp.modify_bg(gtk.STATE_PRELIGHT,self.red)
                    self.btnModeDisp.modify_bg(gtk.STATE_ACTIVE,self.red)
                    self.mode_label.set_text("Navigation Mode")
                    print "Sending place request"
                    self.background.send_place_request_msg()
                    self.curr_config_file = self.nav_config_file
           
                 elif mode == 'mode_select':           
                    self.btnModeDisp.modify_bg(gtk.STATE_NORMAL,self.gray)
                    self.btnModeDisp.modify_bg(gtk.STATE_PRELIGHT,self.gray)
                    self.btnModeDisp.modify_bg(gtk.STATE_ACTIVE,self.gray)
                    self.mode_label.set_text("Startup")
                    self.curr_config_file = self.mode_config_file

           return speech_output

        #====================================================================================================#
        #======================== Speech Listener Handlers ==================================================#

        #--------------GST Message Handling----------------#

	def on_message(self, bus, message):
           
           t = message.type

           if t == gst.MESSAGE_EOS:

                  if self.logging_mode==1:

                          if self.mode == 'recording':
                                  self.recorder.set_state(gst.STATE_NULL)					
                          else:
                                  self.player.set_state(gst.STATE_NULL)
                  else:
                          self.recorder.set_state(gst.STATE_NULL)

                  self.start_label.set_text("Not Listening")

                  print "End of Stream"
                  self.set_color('offline')


           elif t == gst.MESSAGE_ERROR:

                  if self.logging_mode==1:
                          if self.mode == 'recording':
                                  self.recorder.set_state(gst.STATE_NULL)
                          else:
                                  self.player.set_state(gst.STATE_NULL)
                  else:
                          self.recorder.set_state(gst.STATE_NULL)

                  self.start_label.set_text("Not Listening")
                  print "Error Discovered"
                  err, debug = message.parse_error()
                  print "Error: %s" % err, debug
                  self.set_color('offline')

           elif t == gst.MESSAGE_APPLICATION:

             #Dealing with the SAD and SUMMIT Messages
             print "Dialog State : " , self.dialog_state

             if self.logging_mode ==1:  #When audio logging is enabled
                #In playback mode when the saved audio segment is played back with the summit sink at the end
                if self.mode == 'playback' and message.structure.has_field("items") and len( message.structure["items"])>0 :  

                   self.recog_label.set_markup('-----have results-----')
                   #We have a valid set of recognitions
                   self.player.set_state(gst.STATE_NULL)	
                   self.mode = 'processing'
                   #file_name = "log_" + str(self.sink_count) + ".raw"					
                   recognition = {'type': 'recognition', 'info': {'file_name':'none',\
                                                                     'results':message.structure['items']}, 'time': time()}

                   #Update the Displayed recognition list
                   results_text = ""
                   count = 0
                   for rec in message.structure['items']: 
                      count = count + 1
                      results_text = results_text + str(count)+ " : " + rec + "\n"
                   self.result.set_text(results_text)
                   self.btnStartStop.modify_bg(gtk.STATE_NORMAL,self.white)
                   print results_text

                   #Parse uttrance
                   parsed = self.parse_uttarance(message.structure['items'])
                   print "Parsed"
                   print parsed

                   #Take Action
                   rec_time = time()
                   self.take_action(parsed, rec_time)

                elif self.mode == 'recording' and self.player_made ==0 and \
                       not ( message.structure.keys()[0] == 'start_timestamp'):
                   #An end of speech is detected 
                   self.set_color('processing')
                   self.recog_label.set_markup('-----waiting for recognition results-----')
                   print "Processing"
                   #stop the recorder and start playback of the last speech segment saved
                   self.recorder.set_state(gst.STATE_NULL)

                   #handing things over to the recognizer
                   self.make_player()
                   self.player.set_state(gst.STATE_PLAYING)
                   self.mode = 'playback'

                elif self.mode == 'recording' and self.player_made ==0 and \
                       ( message.structure.keys()[0] == 'start_timestamp'):
                   #Start of speech heard 
                   self.set_color('heard')
                   print "Heard"                         

             else:
                #Audio logging off
                if message.structure.has_field("items") and len( message.structure["items"])>0 and \
                       not ( message.structure.keys()[0] == 'start_timestamp'):  
                   #Valid recognition result
                   rec_time = time()
                   recognition = {'type': 'recognition', 'info': {'file_name':'none',\
                                                                     'results':message.structure['items']}, 'time': rec_time}
                   #Save to recognition tag file with time stamps
                   cPickle.dump(recognition,self.tag_file)

                   #Update the display with the current list
                   results_text = ""
                   count = 0
                   for rec in message.structure['items']: 
                      count = count + 1
                      results_text = results_text + str(count)+ " : " + rec + "\n"
                   self.result.set_text(results_text)						

                   #Parse result
                   parsed = self.parse_uttarance(message.structure['items'])
                   print parsed
                   print "Parsed"

                   #Take action 
                   self.take_action(parsed, rec_time)

                   if self.state == 'active':
                      print "Listening"
                      self.set_color('listening')


                elif message.structure.keys()[0] == 'start_timestamp':
                   print "Heard"
                   if self.state == 'active':
                      self.set_color('heard')
                      self.result.set_text('-------------------')
                            
        #------------------ GST Functions --------------------------#
	def make_recorder(self):
		# Define the GStreamer Source Element -- will need to be alter to fit hardware
		print "Making Recorder"

		if self.device == 'bluetooth': #options = bluetooth, usb, default
			device_name = 'btheadset'
		elif self.device == 'usb':
			device_name = 'hw:1'
		else:
			device_name= 'hw:0'
		
		if self.logging_mode==1:

			self.player_made = 0
						
			file_name = "../../../../data/logs/log_" + str(self.sink_count) + ".raw"

                        #we will make use of pulssrc
			#self.recorder = gst.parse_launch('alsasrc name=source ! queue ! audioconvert'
                        self.recorder = gst.parse_launch('pulsesrc name=source '
                                                         + 'device=alsa_input.usb-Logitech_Logitech_Wireless_Headset_H760-00-H760.analog-mono '
                                                         + '! queue ! audioconvert'
                                                         + '! audioresample ! ' 
                                                         + 'audio/x-raw-int, rate = 8000, channels=1, width=16, depth=16 ! ' 
							 + ' SAD name =sad ! queue ! filesink name=filesink location='
							 + file_name)
                        print 'alsasrc name=source ! queue ! audioconvert'\
								 + '! audioresample ! ' + \
							 'audio/x-raw-int, rate = 8000, channels=1, width=16, depth=16 ! ' \
							 + ' SAD name =sad ! queue ! filesink name=filesink location='\
							 + file_name

		else:
                        #self.recorder = gst.parse_launch('alsasrc name=source ! queue ! audioconvert'
                   self.recorder = gst.parse_launch('pulsesrc name=source '
                                                    + 'device=alsa_input.usb-Logitech_Logitech_Wireless_Headset_H760-00-H760.analog-mono '
                                                    + '! queue ! audioconvert'
                                                    + '! audioresample ! ' 
                                                    + 'audio/x-raw-int, rate = 8000, channels=1, width=16, depth=16 ! ' 
                                                    + ' SAD name =sad ! queue ! summitsink config=' + self.curr_config_file)

                   print 'Recorder : alsasrc name=source ! queue ! audioconvert'\
                       + '! audioresample ! ' + \
                       'audio/x-raw-int, rate = 8000, channels=1, width=16, depth=16 ! ' \
                       + ' SAD name =sad ! queue ! summitsink config=' + self.curr_config_file

                print "Recorder " , self.recorder

		sad = self.recorder.get_by_name("sad") 

		sad.set_property("enable_noise_est", True)
		sad.set_property("threshold", self.threshold)
		sad.set_property("minspeech", self.minspeech)
		sad.set_property("prespeech",self.prespeech)
                sad.set_property("holdover",self.holdover)
                sad.set_property('framelength', 32000)
                print sad.get_property('framelength')
		bus = self.recorder.get_bus()
		bus.add_signal_watch()
		bus.connect("message", self.on_message)

	def make_player(self):
		
		#Used only if logging is enabled
		print "Making Player"
		self.player_made = 1

		file_name = "../../../../data/logs/log_" + str(self.sink_count) + ".raw"

		output = 'filesrc name=source location=' + file_name + ' ! queue ! audio/x-raw-int, rate = 8000, channels=1, width=16, depth=16 ! queue ! '  + ' summitsink config=' + self.curr_config_file
		    
		#print output			       
                self.player = gst.parse_launch(output)
		#self.player = gst.parse_launch('filesrc name=source location=' + file_name + 
                #                               ' ! queue ! ' + 
	#				       ' summitsink config=' + self.curr_config_file)	

                print 'Player : filesrc name=source location=' + file_name + \
                                               ' ! queue ! ' + \
					       ' summitsink config=' + self.curr_config_file
		
		self.sink_count =  self.sink_count + 1

		bus = self.player.get_bus()
		bus.add_signal_watch()
		bus.connect("message", self.on_message)
        
        #Starts and stops listener
        def start_listening(self):
           if self.listening ==0:
              self.listening = 1
              self.make_recorder()	
              self.state = 'active'
              self.start_label.set_text("Listening")				
              self.recorder.set_state(gst.STATE_PLAYING)
              print "Started Listening"
              self.set_color('listening')
              
        def stop_listening(self):
           if self.listening ==1:  # we are currently listening
              if self.logging_mode ==1:
                 if self.mode == 'playback':
                    self.player.get_by_name("source").set_state(gst.STATE_READY)
                 elif self.mode == 'recording':
                    self.recorder.get_by_name("source").set_state(gst.STATE_READY)
                 else:
                    print "Processing"
              else:
                 self.recorder.set_state(gst.STATE_NULL)

              self.listening =0
           print "Set to ready"
           self.set_color('offline')
           self.state = "inactive"
                 
        # Parse Uttarance
        def parse_uttarance(self, uttarance):		
           #Should return the actual name as well 
           
           print "Parsing Uttarance New"
           size = len(uttarance)
           output = {}
	   #have this buit automatically so that we can add commands when we want to 
           
           N = 1
           
           for recognized in uttarance[0:N] :
              print recognized
              self.recog_label.set_markup(recognized)			
              for key in self.keywords:
                 output[key.lower()] = (self.get_command(recognized,key)).lower()
              return output
           return output

        #### Possible Response Actions ####

        #Ask direction of orientation related tagging (Tourguide
        def ask_direction(self):
		speech_output = "Is it on the " + self.last_placename['dir']
		self.speaker.say(speech_output)

        def add_to_placelist(self):
	
		new_place = {}

		new_place['prop'] = self.last_placename['prop']
		new_place['label'] = self.last_placename['label'] 
		new_place['dir']= self.last_placename['dir'] 
		new_place['time'] = self.last_placename['time']
		new_place['type']= self.last_placename['type'] 

                #self.background.send_confirm_tag_msg(self.last_placename['time'],"SLAM_CONFIRM",self.last_placename['label'])
                self.background.send_confirm_tag_msg(self.last_placename['time'],self.last_placename['label'])

		self.places_list.append(new_place)
		
		self.update_placelist_display()
		
		print 'Current Placelist ' , self.places_list

		try:
			place_log = open(self.placelist_filename,'w')
			cPickle.dump(self.places_list,place_log)						
			place_log.close()
			print "Saved File to " , self.placelist_filename
			#print self.places_list
			
		except IOError, e:
			
			print "Unable to save to file "
						

        def ask_confirmation_mode(self,new_mode):
           if new_mode == "navigation":
              speech_output = "Are we done with the tour"
           elif new_mode == "tourguide":
              speech_output = "Are we going on a tour"
           #self.speaker.say(speech_output)
           return speech_output

        def ask_floor_confirmation(self,floor_no):
           speech_output = "are we on the " + floor_no + " floor" 
           return speech_output
           #self.speaker.say(speech_output)

	def ask_confirmation(self,pron, prop, prefix, suffix, view, pos):
           words = pron.split('_')  #split the pronunciation
           phrase = ''
              
           for i in words:
              phrase = phrase + ' ' + i
                 
           if prefix !='none':
              temp_pre =''
              pre = prefix.split('_')
              print pre
              for i in pre:
                 if i == 'my':
                    temp_pre = temp_pre + ' ' + 'your'
                 else:
                    temp_pre = temp_pre + ' ' + i
              phrase = temp_pre + ' ' + phrase
           if suffix !='none':
              phrase = phrase + suffix
              
           if pos =="none":
              pos = "at"           
           if view == "none":
              view = "both"

           if view =="guide":
              speech_output = "are you " + pos + " " + phrase
           elif view =="both":
              speech_output = "are we " + pos + " " + phrase
           elif view =="wheelchair":
              speech_output = "am i " + pos + " " + phrase
              
           #speech_output = "Are we at " + phrase
           #self.speaker.say(speech_output)
           return speech_output

	def confirmation(self):
           speech_output = "Adding"        
           #self.speaker.say(speech_output)
           return speech_output
           
        def floor_confirmation(self):
           speech_output = "Changing floor"        
           #self.speaker.say(speech_output)
           return speech_output

        def goal_confirmation(self):
           print "Goal Confirmed"

           print "Goal Floor " , self.last_goal['floor_no'], " Current Floor No : " , \
               self.current_floor_no
           
           speech_output = ""
           if(self.current_floor_no == -1):
              speech_output = "i do not know which floor i am on"
              print "Unknown Floor"
              return speech_output
           if(self.last_goal['floor_no'] == self.current_floor_no):
              #since there are no portals - then we can either get there or not - no need to 
              #querry costs 
              print "Same floor Goal => Going directly to the location"
              speech_output = "Going to " + self.last_goal['pron']       
              #self.speaker.say(speech_output)
              #send the goal
              #this currently sends the goal to the navigator directly - some module needs to moderate this - either here 
              #or somewhere else
              self.multi_floor_waypoints = []
              last_waypoint = {'location': self.last_goal['location'], 'floor_no': self.last_goal['floor_no'],\
                                  'x':self.last_goal['x'],'y':self.last_goal['y']}
              self.multi_floor_waypoints.append(last_waypoint)
              
              self.send_goal_msg(self.last_goal,0)
              return speech_output
           else:
              print "Location on a different floor" 
              print self.last_goal
              print "Goal ID : " , self.last_goal['id']
              speech_output = "We need to go to floor " + str(self.last_goal['floor_no'])
              
              #now we do need cost calculation
              
              #self.last_goal['pron'] + " is on floor " + str(self.last_goal['floor_no'])

              #search for a path - comprising of the elevator and the new goal 
              #the goals can be switched - i.e. from the elevator to the actual location 
              #once the floor auto-change has occured - to the correct floor

              #we should proceed with this only when we have the costs 
              self.background.check_cost_to_elevators(self.current_floor_no)
              return speech_output
              ######-------- we hold off here and wait till we get the costs to elevators ------####
              '''print "Waiting until the costs are received"
              self.multi_floor_waypoints = []

              #find the distance between the current position and the avaiable elevators
              

              #elevator_found = 0
              ele_controller = None

              no_ele = 0
              valid_elevator_count = 0 #valid elevator is an elevator that is present on both floors 

              valid_elevators = []

              for i in self.background.elevator_places: #check through the elevators 
                 print "Elevator " , (no_ele +1), " : " , i['x'], ",", i['y']
                 no_ele +=1
                 ele_floor_info = i['floors']


                 accessible_floors = set(ele_floor_info.keys())

                 if(accessible_floors.intersection([self.last_goal['floor_no']]) and \
                       accessible_floors.intersection([self.current_floor_no])):
                    print "Elevator connects both current floor and the goal floor"
                    valid_elevator_count += 1

                    print "Appending valid elevator"
                    print ele_floor_info[self.current_floor_no]
                    valid_elevators.append(ele_floor_info[self.current_floor_no])
              
              print "---------Valid elevator set " 
              print valid_elevators
      
              if(valid_elevator_count==1):
                 print "Found only one usable elevator - setting this as the intermediate goal"

              if(valid_elevator_count >1):
                 print "Found more than one valid elevator"

              if(valid_elevator_count >=1):               
                 
                 dist_controller = 15.0
                 #ideally the elevator controller should be attached to the elevator info

                 for j in self.background.nav_places:
                    if(j['name']=='elevator_controller' and j['floor_no'] == self.current_floor_no):
                       #check the distance and pick the closest controller - if its within a certain radius 
                       for i in valid_elevators:
                          temp_dist = math.sqrt(pow(j['x'] - i['x'],2) + pow(j['y'] - i['y'],2))
                          print "\t\t===Dist to elevator Controller : ", temp_dist
                          if(dist_controller > temp_dist):
                             dist_controller = temp_dist
                             ele_controller = j

                 if(ele_controller != None):
                    print "Found an elevator controller"
                    waypoint = {'location': 'elevator_controller', 'floor_no':  ele_controller['floor_no'], 'x':ele_controller['x'],'y':ele_controller['y'],
                                'theta':ele_controller['theta']}
                    self.multi_floor_waypoints.append(waypoint)

                 #the was this is added depends on the no of valid elevators
                 waypoint = {'location': 'elevator', 'floor_no': self.current_floor_no , 'pos':[]}
                 for i in valid_elevators:
                    waypoint['pos'].append({'x':i['x'], 'y':i['y'], 'door_x':i['door_x'], 'door_y':i['door_y']})
                    
                 print waypoint                                       

                 self.multi_floor_waypoints.append(waypoint)
                 
                           
              #adding the last waypoint to be the actual goal
              last_waypoint = {'location': self.last_goal['location'], 'floor_no': self.last_goal['floor_no'],\
                                  'x':self.last_goal['x'],'y':self.last_goal['y']}
              self.multi_floor_waypoints.append(last_waypoint)
              
              if(valid_elevator_count >=1):
                 if(ele_controller != None):
                    speech_output += " going to the elevator controller first"   
                    print "Going to elevator controller first"
                    self.send_goal_msg(self.multi_floor_waypoints[0], 1)
                    self.navigation_mode = "multi_floor"
                 else:
                    speech_output += " going to the elevator first"   
                    print "Going to elevator first"
                    self.send_goal_msg(self.multi_floor_waypoints[0], 0)
                    self.navigation_mode = "multi_floor"
                 
              else:
                 speech_output += " I can not find an elevator on this floor"
              #self.speaker.say(speech_output)
              self.update_navigation_list()
              return speech_output'''

              

	def confirmation_no_direction(self):
           speech_output = "Adding without direction"
           #self.speaker.say(speech_output)
           return speech_output

	def rejection(self):
           speech_output = "Ignoring"
           #self.speaker.say(speech_output)
           return speech_output


	def do_Low_Level(self,lowLevel):
           speech_output = ""
           if(lowLevel == "stop"):
                 self.wheelchair.command_stop()
                 speech_output = "I am stopping"			                         

           elif(lowLevel == "left"):
                 self.wheelchair.command_vector(0.75,math.pi/2)
                 speech_output = "I am going left"


           elif(lowLevel == "right"):
                 self.wheelchair.command_vector(0.75,-math.pi/2)
                 speech_output = "I am going right"


           elif(lowLevel == "forward"):
                 self.wheelchair.command_vector(0.75,0)
                 speech_output = " I am going forward"

           #if (speech_output !=""):
           # self.speaker.say(speech_output)
           return speech_output

	def do_Place_Name(self,goal, pron):

		words = pron.split('_')
		
		phrase = ''
		
		for i in words:
			phrase = phrase + ' ' + i


		self.wheelchair.set_goal_name(goal)
		self.wheelchair.command_go()
		speech_output = "I am going to " + phrase
		self.speaker.say(speech_output)


	def do_Turn(self,direction):
           if(direction == "left"):
              #self.wheelchair.command_vector(0,math.pi/2)
              print "Not supported"
           elif(direction == "right"):
              #self.wheelchair.command_vector(0,-math.pi/2)
              print "Not supported"
           elif(direction == "back"):
              #self.wheelchair.command_vector(0,math.pi)
              print "Not supported"

           return ""

        def do_Tagging_Action(self,parsed,rec_time):
           if(parsed['label'] !='none'):
              self.dialog_state = 'place_name'           
              self.last_placename['prop']  = parsed['prop']
              self.last_placename['label'] = parsed['label']
              self.last_placename['dir']  = parsed['dir']
              self.last_placename['type'] = parsed['cmd']
              self.last_placename['view'] = parsed['view'] # who's point of view this tagging is
              self.last_placename['pos'] = parsed['pos']   #whether we are inside/facing the location/objects
              self.last_placename['pron'] = parsed['pron']
              self.last_placename['time'] = rec_time
              self.background.send_possible_tag_msg(self.last_placename)
              speech_output = self.ask_confirmation(parsed['pron'], parsed['prop'], parsed['prefix'], parsed['suffix'], parsed['view'], parsed['pos'])
              return speech_output
           else:
              self.dialog_state = 'init' 
              return ""

        def do_Floor_Change_Action(self,parsed,rec_time):
           speech_output = ""
           if(parsed['prop']!='none'):             
              self.poss_new_floor_no = int(parsed['prop'])

              try: 
                 if(len(self.background.floor_map)>0 and self.DM_mode == 'navigation'):
                    if(self.background.floor_map.index(self.poss_new_floor_no) >=0):
                       print "Known Floor"
              except ValueError, e:
                 print "Unknown Floor"
                 self.dialog_state = 'init'
                 return speech_output

              if(self.current_floor_no != self.poss_new_floor_no):
                 print "Possible new floor"
                 self.dialog_state = 'floor_change' 
                 speech_output = self.ask_floor_confirmation(parsed['pron'])
              else:
                 self.background.send_floor_change_msg(self.poss_new_floor_no)
                 speech_output = "we are on floor "+ str(self.poss_new_floor_no)
                 #self.speaker.say(speech_output)
                 self.dialog_state = 'init'

           return speech_output
                 

        def do_Door_Action(self,parsed,rec_time):
           self.dialog_state = 'door_traversal' 
           speech_output = "do you want me to go through the door"
           self.speaker.say(speech_output)

        def do_Elevator_Action(self,parsed,rec_time):
           self.dialog_state = 'elevator_traversal' 
           speech_output = "do you want me to go through the elevator"
           self.speaker.say(speech_output)
              
        def do_Confirm(self,parsed,rec_time):
           speech_output = ""
           print " Confirmation - Dialog Mode "  , self.dialog_state
           if (parsed['prop']=='yes'):
              #if self.dialog_state == 'place_name' and self.last_placename['dir'] !='none':
              #   self.ask_direction()
              #   self.dialog_state = 'place_dir'

              if self.dialog_state == 'place_name':
                 print "Adding Place"
                 self.dialog_state = 'init'
                 #if self.last_placename['label']!='none':
                 self.add_to_placelist()
                 speech_output = self.confirmation()

              elif (self.dialog_state == 'floor_change' or self.dialog_state == 'suggested_floor_change'):
                 print "Changing Floor"
                 self.dialog_state = 'init'
                 self.current_floor_no = self.poss_new_floor_no
                 self.background.send_floor_change_msg(self.poss_new_floor_no)
                 speech_output = self.floor_confirmation()
                 
              elif self.dialog_state == 'place_dir':
                 print "Adding Place"
                 #if self.last_placename['label']!='none':
                 self.dialog_state = 'init'
                 self.add_to_placelist()
                 speech_output = self.confirmation()

              elif self.dialog_state == 'goto_place':
                 if(self.last_goal['location'] != 'none'):
                    print "Going to location"
                    self.dialog_state = 'init'
                    speech_output = self.goal_confirmation()

              elif self.dialog_state == 'mode_change':
                 speech_output = "changing to " + self.next_DM_mode
                 #self.speaker.say(speech_output)                      
                 speech_output += ", " + self.change_DM_mode(self.next_DM_mode)
                 self.next_DM_mode = None
                 self.dialog_state = 'init'

              #elif self.dialog_state == 'door_traversal':
              #   speech_output = "going through the door"
              #   self.background.send_speech_msg("FOLLOWER","GO_THROUGH_DOOR")
              #   self.speaker.say(speech_output)                    
              #   self.dialog_state = 'init'


              #elif self.dialog_state == 'elevator_traversal':
              #   speech_output = "going through the elevator"
              #   self.background.send_speech_msg("FOLLOWER","GO_INTO_ELEVATOR")
              #   self.speaker.say(speech_output)                                  
              #   self.dialog_state = "init"
              
              else:
                 self.dialog_state = 'init'
                 #self.rejection()

           #elif self.dialog_state == 'place_dir':
           #   self.last_placename['dir'] ='none'
           #   self.add_to_placelist()
           #   self.dialog_state = 'init'
           #   self.confirmation_no_direction()
           else:
              if self.dialog_state != 'init':
                 self.dialog_state = 'init'
                 speech_output = self.rejection()
           return speech_output

        def do_Follower_Action(self,parsed,rec_time):
           #speech_output = ""
           print "Command : " , parsed['cmd'] , " Property : " , parsed['prop']
           #Going through door not implemented 
           #if parsed['prop'] == "go_through_door":
           #   self.do_Door_Action(parsed,rec_time)              
           #elif parsed['prop'] == "go_into_elevator":
           #   self.do_Elevator_Action(parsed,rec_time)     
           #else :
           #   if parsed['prop'] == "keep_closer":
           #      speech_output = "coming close"
           #   if parsed['prop'] == "keep_farther":
           #      speech_output = "keeping back"
           #   if(speech_output !=""):
           #      self.speaker.say(speech_output)
           #      print speech_output
           self.background.send_speech_msg(parsed['cmd'].upper(),parsed['prop'].upper())
           print "Sending person tracker/follower message"
           return ""

        def do_Go_To_Location(self,parsed,rec_time):
           #this is where the reasoning needs to happen - for multi-floor traversals 
           speech_output = ""
           
           if(self.current_floor_no == -1):
              speech_output = "i do not know which floor i am on"
              self.dialog_state = 'init'
              return speech_output
           
           print self.background.nav_places
           map_places = set()
           for i in self.background.nav_places:
              map_places.add(i['name'])

           if parsed['loc'] != 'none' and map_places.intersection(set([parsed['loc']])): 
           #check if there is such a location 
              words = parsed['pron'].split('_')
              phrase = ''                    
              for i in words:
                 phrase = phrase + ' ' + i

              loc_same_floor = -1
              loc_on_dif_floor = -1
              dif_floor_no = -1
              same_floor_match = []
              other_floor_match = []

              print "Current Floor : " , self.current_floor_no


              temp_match = None
              #first search through the places on the current floor 
              for i in self.background.nav_places:
                 if(i['name']== parsed['loc']):
                    self.dialog_state = 'goto_place'

                    print "Found Possible match"
                    print "Goal is (" , i['x'], ",",i['y'], ") = Floor : ", i['floor_no']
                    if(self.current_floor_no == i['floor_no']):
                       #speech_output = "I am going to " + phrase
                       #self.speaker.say(speech_output)
                       print "On the same floor"
                       
                       if(self.background.last_pose_msg !=None):
                          print self.background.last_pose_msg.pos

                          #this method is wrong - ideally should querry 
                          #the navigator for the two paths and compare the distances 

                          #self.send_goal_msg(i) #- send this and get back the distance metric

                          dist_to_goal = math.sqrt(pow(self.background.last_pose_msg.pos[0] - i['x'],2) + \
                              pow(self.background.last_pose_msg.pos[1] - i['y'],2))
                          print dist_to_goal
                          #if(dist_to_loc > dist_to_goal):
                          #same_floor_match[0] = i
                          temp_match = {'name':i['name'],'dist':dist_to_goal,'x': i['x'], 'theta': i['theta'],                                       'y':i['y'], 'floor_no':i['floor_no'], 'id':i['id']}
                          same_floor_match.append(temp_match)
                          loc_same_floor = 1
                          
                       else:
                          print " ++++++ No poses received"
                          speech_output = "I do not know where I am"
                          self.dialog_state = 'init'
                          #print "Issued Command"
                    else: #there is a matching location on a different floor 
                       if(self.background.last_pose_msg !=None):
                          temp_match = {'name':i['name'],'dist':-1,'x': i['x'],
                                        'y':i['y'], 'floor_no':i['floor_no'], 'id':i['id']}
                          other_floor_match.append(temp_match)
                          loc_on_dif_floor = 1
                          dif_floor_no = i['floor_no']
                       else:
                          print " ++++++ No poses received"
                          speech_output = "I do not know where I am"
                          self.dialog_state = 'init'

              if(loc_same_floor==1):
                 #querry the user - pick the closest location 
                 #ideally this should have more information attached to it - e.g. gates side exit 
                 min_dist = 1000
                 closest_match = []
                 for i in same_floor_match:
                    if(min_dist > i['dist']):
                       min_dist = i['dist']
                       #if(len(closest_match)>0):
                       #   closest_match.pop()
                       closest_match = i
                 
                 self.last_goal['location'] = closest_match['name']
                 self.last_goal['x'] = closest_match['x']
                 self.last_goal['y'] = closest_match['y']
                 self.last_goal['theta'] = closest_match['theta']
                 self.last_goal['floor_no'] = self.current_floor_no
                 self.last_goal['pron'] = phrase
                 self.last_goal['id'] = closest_match['id']
                 self.dialog_state = 'goto_place'
                 speech_output = "do you want me to go to " + phrase
                 print "Speech output " , speech_output
                 #self.speaker.say(speech_output)

              elif(loc_on_dif_floor and temp_match !=None):
                 #speech_output = phrase + " is not on this floor"
                 speech_output = phrase + " is on floor " +  str(dif_floor_no) + " Did you mean this place" 
                 print "Speech output " , speech_output

                 self.last_goal['location'] = temp_match['name']
                 self.last_goal['x'] = temp_match['x']
                 self.last_goal['y'] = temp_match['y']
                 self.last_goal['floor_no'] = temp_match['floor_no']
                 self.last_goal['pron'] = phrase
                 self.last_goal['id'] = temp_match['id']
                 self.dialog_state = 'goto_place'
                 
           elif parsed['loc'] != 'none':
              print "location is not known in the map"
              words = parsed['pron'].split('_')
              phrase = ''                    
              for i in words:
                 phrase = phrase + ' ' + i
              speech_output = "I dont know the location of the " + phrase
              self.dialog_state = 'init'
           
           else:
              self.dialog_state = 'init'
              
           print "Returning Speech Output"
           return speech_output
           

        def take_action(self, parsed, rec_time):    
           speech_output = ""
           if parsed['cmd']== 'none':  
              print "No valid command"
              if(self.state == 'inactive'):
                 self.dialog_state = 'init'     
                 print "resetting state"
              else:
                 print "Still in deaf mode"

          #Wake Up/Sleep Modes 
           if parsed['cmd']=='active':
              print "=== Listening Mode ==="
              if (parsed['prop']=='yes' and parsed['keyword'] == 'computer'):  
                  self.state = 'active'
                  self.set_color('listening')
                  self.dialog_state = 'init'
                  speech_output = "Listening"

              elif parsed['prop']=='no' and parsed['keyword'] == 'computer':  
                 if(self.state != 'inactive'):
                    self.state = 'inactive'
                    self.set_color('sleep')
                    speech_output = "sleeping"
                    #this should have a specific grammer - for waking up 
                    self.dialog_state = 'wakeup'

          #Active Commands (Tour/Nav mode)
           elif (self.state == 'active' and parsed['cmd']!= 'none'):
              print self.DM_mode
              print "=== Listening Mode ==="
              if parsed['cmd']=="change_mode"  and parsed['keyword'] == 'computer':      
                 print "Mode Change" 
                 self.next_DM_mode = parsed['prop']
                 speech_output = self.ask_confirmation_mode(parsed['prop'])
                 self.dialog_state = 'mode_change'

             #### Tourguide Mode commands ####
              elif(self.DM_mode == "tourguide"): 
                 print "---- Tourguide Mode ----"
                #Place Tagging related Commands
                 if (parsed['cmd']=='place') or (parsed['cmd']=='object'):
                    print "Tagging Location"
                    speech_output = self.do_Tagging_Action(parsed,rec_time)

                #Confirmation Commands for labeling
                 elif (parsed['cmd']=='confirm'):
                    print "Confirmation"
                    speech_output = self.do_Confirm(parsed,rec_time)

                ### Person Follower/Tracker based commands ###
                 elif ((parsed['cmd']=='follower') or(parsed['cmd']=='tracker')):
                    print "Person Following"
                    speech_output = self.do_Follower_Action(parsed,rec_time)

                 elif (parsed['cmd']=='floor_change' and parsed['keyword'] == 'computer'):
                    print "Floor Change"
                    speech_output = self.do_Floor_Change_Action(parsed,rec_time)
                    print "Floor Change"

              elif(self.DM_mode == "navigation"):
                 print "---- Navigation Mode ----"
                 if(parsed['cmd']=='confirm'):
                    print "Confirm"
                    speech_output = self.do_Confirm(parsed,rec_time)   
                    
                 elif (parsed['cmd']=='goto'):
                    print "Go to Location"
                    speech_output = self.do_Go_To_Location(parsed,rec_time)

                 elif (parsed['cmd']=='turn'):				
                    print "Turn command"
                    speech_output = self.do_Turn(parsed['dir'])
                    print "Issued Command"
                    
                 elif (parsed['cmd']=='go_lowlevel'):				
                    print "Low level command"
                    print "Low level command - Not responding"
                    
                 elif (parsed['cmd']=='floor_change' and parsed['keyword'] == 'computer'):
                    print "Floor change"
                    speech_output = self.do_Floor_Change_Action(parsed,rec_time)
                    
                 elif parsed['cmd']=='stop':
                    print "Stop motion command"
                    speech_output = self.send_stop()


           print "-------------Done taking action---------------"

           if self.logging_mode ==1:
             print "In logging mode - now we should start up the listener"
             #Change mode back to recording
             if(self.mode == 'recording'):
                print "Already Listening - stopping recorder"
                self.recorder.set_state(gst.STATE_NULL)

             if(self.state == 'inactive'):
                   self.curr_config_file = self.wakeup_config_file
             elif(self.dialog_state == 'place_name' or  self.dialog_state == 'floor_change' or\
                   self.dialog_state == 'place_dir' or  self.dialog_state == 'goto_place' or\
                   self.dialog_state == 'mode_change'):
                self.curr_config_file = self.confirm_config_file
                self.minspeech = 25000

             elif(self.dialog_state == 'floor_change_querry'):
                self.minspeech = 100000
                self.curr_config_file = self.new_floor_config_file
             elif(self.dialog_state == 'init'):
                self.minspeech = 100000
                if(self.DM_mode == "tourguide"):
                   self.curr_config_file = self.tour_config_file
                elif(self.DM_mode == "navigation"):
                   self.curr_config_file = self.nav_config_file   

             self.mode = 'recording'
             #Make recorder and restart the listening process
             self.make_recorder()
             #start recorder 
             self.recorder.set_state(gst.STATE_PLAYING)
             self.start_label.set_text("Listening")				
             print "Listening"
             if(self.state != 'inactive'):
                self.set_color('listening')
             else:
                self.set_color('sleep')

             self.action_label.set_markup(speech_output)
             self.dialog_state_label.set_markup(self.dialog_state)
             self.speaker.say(speech_output)

             

                #Handles taking action based on the recognized uttrance
        def take_action_1(self, parsed, rec_time):    
             #No command spotted 
             if parsed['cmd']== 'none':  
                print "No valid command"
                if(self.dialog_state != 'wakeup'):
                   self.dialog_state = 'init'     
                   print "resetting state"
                else:
                   print "Still in deaf mode"

             #Wake Up/Sleep Modes 
             if parsed['cmd']=='active':
                if parsed['prop']=='yes' and parsed['keyword'] == 'computer':                      
                   self.state = 'active'
                   self.set_color('listening')
                   self.dialog_state = 'init'
                   self.speaker.say("Listening")

                elif parsed['prop']=='no' and parsed['keyword'] == 'computer':  
                   if(self.state != 'inactive'):
                      self.state = 'inactive'
                      self.set_color('sleep')
                      self.speaker.say("sleeping")
                      #this should have a specific grammer - for waking up 
                      self.dialog_state = 'wakeup'

             #Active Commands (Tour/Nav mode)
             elif (self.state == 'active' and parsed['cmd']!= 'none'):
                print self.DM_mode

                if parsed['cmd']=="change_mode"  and parsed['keyword'] == 'computer':            
                   self.next_DM_mode = parsed['prop']
                   self.ask_confirmation_mode(parsed['prop'])
                   self.dialog_state = 'mode_change'

                #### Tourguide Mode commands ####
                elif(self.DM_mode == "tourguide"): 
                   print "---- Tourguide Mode ----"
                   #Place Tagging related Commands
                   if (parsed['cmd']=='place') or (parsed['cmd']=='object'):
                      self.do_Tagging_Action(parsed,rec_time)

                   #Confirmation Commands for labeling
                   elif (parsed['cmd']=='confirm'):
                      self.do_Confirm(parsed,rec_time)

                   ### Person Follower/Tracker based commands ###
                   elif ((parsed['cmd']=='follower') or(parsed['cmd']=='tracker')):
                      self.do_Follower_Action(parsed,rec_time)

                   elif (parsed['cmd']=='floor_change'):
                      self.do_Floor_Change_Action(parsed,rec_time)
                      print "Floor Change"

                elif(self.DM_mode == "navigation"):
                   #print parsed
                   print "---- Navigation Mode ----"
                   if(parsed['cmd']=='confirm'):
                      self.do_Confirm(parsed,rec_time)   
                   
                   elif (parsed['cmd']=='goto'):
                      print "Heard Navigation Command"
                      self.do_Go_To_Location(parsed,rec_time)

                   elif (parsed['cmd']=='turn'):				
                      self.do_Turn(parsed['dir'])
                      print "Issued Command"

                   elif (parsed['cmd']=='go_lowlevel'):				
                      #self.do_Low_Level(parsed['dir'])
                      print "Low level command - Not responding"

                   elif (parsed['cmd']=='floor_change'):
                      self.do_Floor_Change_Action(parsed,rec_time)
                      print "Floor Change"

                   elif parsed['cmd']=='stop':
                      self.send_stop()
                      print "Stopping"                

                print "-------------Done taking action---------------"
                
             if self.logging_mode ==1:
                print "In logging mode - now we should start up the listener"
                #Change mode back to recording
                if(self.mode == 'recording'):
                   print "Already Listening - stopping recorder"
                   self.recorder.set_state(gst.STATE_NULL)
                  
                if(self.state == 'inactive'):
                   self.curr_config_file = self.wakeup_config_file
                elif(self.dialog_state == 'place_name' or  self.dialog_state == 'floor_change' or\
                      self.dialog_state == 'place_dir' or  self.dialog_state == 'goto_place' or\
                      self.dialog_state == 'mode_change'):
                   self.curr_config_file = self.confirm_config_file
                   self.minspeech = 25000

                elif(self.dialog_state == 'floor_change_querry'):
                   self.minspeech = 100000
                   self.curr_config_file = self.new_floor_config_file
                elif(self.dialog_state == 'init'):
                   self.minspeech = 100000
                   if(self.DM_mode == "tourguide"):
                      self.curr_config_file = self.tour_config_file
                   elif(self.DM_mode == "navigation"):
                      self.curr_config_file = self.nav_config_file   

                self.mode = 'recording'
                #Make recorder and restart the listening process
                self.make_recorder()
                #start recorder 
                self.recorder.set_state(gst.STATE_PLAYING)
                self.start_label.set_text("Listening")				
                print "Listening"
                self.set_color('listening')



#====================================================================================================#

if __name__ == "__main__":
   speaker = Festival()
   if len(argv) >=2:
      if(argv[1] == 'n'):
         print "------Navigation Mode------"
         if len(argv) >=3:
            #tour_dialog_manager(speaker,"navigation",arg[2])
             GTK_Main(speaker,"navigation", int(argv[2]))
         else:
            #tour_dialog_manager(speaker,"navigation",None)
            GTK_Main(speaker,"navgation", None)
      else:
         print "------Tourguide Mode--------"
         tour_dialog_manager(speaker,"tourguide",None)
   else:
      print "------Tourguide-------"
      GTK_Main(speaker,"tourguide", None)
   #GTK_Main(speaker,"navigation",None)
   #GTK_Main(speaker,"navigation", 3)#"normally -"tourguide"
   gtk.gdk.threads_init()
   gtk.main()

