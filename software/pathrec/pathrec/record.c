#include<stdio.h>
#include<lcm/lcm.h>
#include<lcmtypes/er_lcmtypes.h>
#include<bot_core/bot_core.h>
//#include<er_common/lcm_utils.h>
//#include <bot_lcmgl_client/lcmgl.h>
//#include <er_lcmtypes/lcm_channel_names.h>
//#include <bot_frames/bot_frames.h>
//#include <bot_param/param_util.h>

void publish_pose()
{
    lcm_t* lcm=bot_lcm_get_global(NULL);

    static bot_core_pose_t slam_pose;
    slam_pose.pos[0]=1;
    slam_pose.pos[1]=1;

    bot_core_pose_t_publish(lcm,"POSE", &slam_pose);
}

void publish_goal()
{
    erlcm_point_t goal;
    goal.x=2.94;
    goal.y=1.97;

    lcm_t* lcm=bot_lcm_get_global(NULL);
    erlcm_navigator_floor_goal_msg_t msg;
    msg.goal_msg.goal = goal;
    msg.goal_msg.use_theta = 0;//0;
    msg.goal_msg.utime = bot_timestamp_now();
    msg.goal_msg.nonce = random();
    msg.goal_msg.sender = ERLCM_NAVIGATOR_GOAL_MSG_T_SENDER_WAYPOINT_TOOL;
    msg.floor_no = 2;
    erlcm_navigator_floor_goal_msg_t_publish(lcm,"NAV_GOAL_FLOOR",&msg);

}

int main(int argc, char** argv)
{
   // publish_pose();
    publish_goal();
}
