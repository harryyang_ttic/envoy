#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>
#include <getopt.h>
#include <glib.h>

#include <bot_core/bot_core.h>
#include <bot_param/param_client.h>
#include <bot_frames/bot_frames.h>

#include <lcmtypes/bot_core.h>
#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/xsens_ins_t.h>

#define IMU_LCM_HAS_SERIAL 0 // Does the IMU LCM message contain serial numbers?
#define MAX_DELTA_TIME 2

// Used if we are publishing the pose
#define PUBLISH_GLOBAL_TO_LOCAL_HZ 10.0

#define PUBLISH_POSE_HZ 50.0

#define NOMINAL_IMU_HZ 100.0
#define NOMINAL_ODOM_HZ 45.0

#define MAX_UTIME_DIFFERENCE 300000

#define WARN_UTIME_DIFFERENCE 50000

#define ORC_TICKS_PER_REVOLUTION 130305.52438480203//137613.8
#define ORC_WHEEL_DIAMETER 0.255
#define ORC_WHEELBASE 0.51

#if DEBUG
#define dbg(...) do { fprintf(stderr, "[%s:%d] ", __FILE__, __LINE__); \
                      fprintf(stderr, __VA_ARGS__); } while(0)
#else
#define dbg(...) 
#endif


#ifndef SQUARE
#define SQUARE(a) ( (a)*(a) )
#endif

typedef struct _state_t {
    lcm_t * lcm;
    
    GMainLoop *mainloop;
    
    erlcm_raw_odometry_msg_t *odom_last;
    erlcm_orc_debug_stat_msg_t *raw_last;
    int64_t odom_prev_utime;
    
    xsens_ins_t *imu_last;
    int64_t imu_prev_utime;

    int64_t pose_last_utime;
    int64_t last_global_to_local_utime;
    int64_t pose_publish_last_utime;

    // keep track of time of last message according to actual (clock) time
    int64_t odom_last_received_utime;
    int64_t imu_last_received_utime;

    bot_core_pose_t *pose;
    BotTrans imu_to_body_trans;

    double heading;
    int64_t heading_utime;

    int odom_only;
    int publish_pose_channel_message;

    uint8_t calibrate;
    int8_t fixup;
    uint8_t auto_calibrate;

    uint32_t consecutive_still;
    uint32_t calibrate_still_count;
    uint8_t is_still;

    uint8_t calibration_done;
    int calib_count;
    double gyro_calib[3];
    double gyro_calib_sum[3];
    double accel_calib[3];
    double accel_calib_sum[3];
    int64_t first_imu_time;

    double gyro_calib_param[3];
    double accel_calib_param[3];

    int64_t last_imu_time;
    double last_q[4];
    
    double sum_yaw, native_yaw;

    uint8_t calib_announce;
    uint8_t fixup_announce;

    int verbose;
    int transform;

    int error_flag_odom;    
    int error_flag_imu;

    guint status_timer_id;
    
    int odom_stopped;

} state_t;

const double forward_vec[3] = { 1, 0, 0 };
const double imu_to_body_default[4] = { 1, 0, 0, 0 };

static void
gyro_calibrate (const xsens_ins_t * imu, state_t * self)
{
    int i;
    if (self->calib_count == 0) {
        self->first_imu_time = imu->utime;
        for (i = 0; i < 3; i++)
            self->gyro_calib_sum[i] = 0.0;
        for (i = 0; i < 3; i++)
            self->accel_calib_sum[i] = 0.0;
    }
    for (i = 0; i < 3; i++)
        self->gyro_calib_sum[i] += imu->gyro[i];
    for (i = 0; i < 3; i++)
        self->accel_calib_sum[i] += imu->accel[i];
    self->calib_count++;

    /* After 5 seconds, finish up */
    if (imu->utime - self->first_imu_time > 5000000) {
        for (i = 0; i < 3; i++)
            self->gyro_calib[i] = self->gyro_calib_sum[i] / self->calib_count;
        printf ("Calibrated biases: %f %f %f\n", self->gyro_calib[0],
                self->gyro_calib[1], self->gyro_calib[2]);
        for (i = 0; i < 3; i++)
            self->accel_calib[i] = self->accel_calib_sum[i] / self->calib_count;
        printf ("Acceleration: %f %f %f\n", self->accel_calib[0],
                self->accel_calib[1], self->accel_calib[2]);
        self->calibrate = 0;
        self->calibration_done = 1;
        self->calib_announce = 1;
        self->calibrate_still_count = 0;
        self->calib_count = 0;
    }
}

static double
angle_clamp (double theta)
{
    if (theta > 4*M_PI || theta < -4*M_PI)
        fprintf (stderr, "Warning: theta (%f) is more than 2pi out of range\n",
                theta);
    if (theta > 2*M_PI)
        return theta - 2*M_PI;
    if (theta < -2*M_PI)
        return theta + 2*M_PI;
    return theta;
}

static void
imu_fixup_yaw (state_t * self)
{
    if (self->last_imu_time == 0) {
        self->last_imu_time = self->imu_last->utime;
        memcpy (self->last_q, self->imu_last->quat, 4 * sizeof (double));
        self->sum_yaw = 0.0;
        self->native_yaw = 0.0;
        return;
    }

    double q[4], fixup[4], rate[3];
    int64_t dt = self->imu_last->utime - self->last_imu_time;

    // Rotate the angular velocity vector into the global frame
    // and integrate the global frame's yaw rate (the global frame's
    // yaw rate is always in the plane perpendicular to gravity).
    memcpy (rate, self->imu_last->gyro, 3 * sizeof (double));
    bot_quat_rotate (self->imu_last->quat, rate);
    if (self->consecutive_still < 30) {
        /* Only integrate yaw when we are moving */
        self->sum_yaw += rate[2] * dt / 1000000.0;
        self->sum_yaw = angle_clamp (self->sum_yaw);
    }

    // Compute q, the quaternion that rotates the previous orientation
    // into the current orientation.
    self->last_q[1] = -self->last_q[1];
    self->last_q[2] = -self->last_q[2];
    self->last_q[3] = -self->last_q[3];
    bot_quat_mult (q, self->imu_last->quat, self->last_q);

    // Compute the yaw component of q and integrate it.  Note that
    // this is not degenerate unlike most Euler angles because q is
    // guaranteed to be a very small rotation.
    self->native_yaw += atan2 (2*(q[1]*q[2] + q[0]*q[3]),
            (q[0]*q[0] + q[1]*q[1] - q[2]*q[2] - q[3]*q[3]));
    self->native_yaw = angle_clamp (self->native_yaw);

    // Compute a fixup quaternion that corrects for the difference
    // in yaw between sum_yaw and native_yaw in the global frame,
    // and apply the fixup to the orientation quaternion.
    fixup[0] = cos ((self->sum_yaw - self->native_yaw) / 2);
    fixup[1] = 0;
    fixup[2] = 0;
    fixup[3] = sin ((self->sum_yaw - self->native_yaw) / 2);
    bot_quat_mult (q, fixup, self->imu_last->quat);
    if (self->verbose) {
        printf ("fixup yaw: old %8.4f new %8.4f, quat: %8.4f %8.4f %8.4f %8.4f\n",
                self->native_yaw * 180.0 / M_PI,
                self->sum_yaw * 180.0 / M_PI,
                q[0], q[1], q[2], q[3]);
    }

    self->last_imu_time = self->imu_last->utime;
    memcpy (self->last_q, self->imu_last->quat, 4 * sizeof (double));

    memcpy (self->imu_last->quat, q, 4 * sizeof (double));

}

static int
emit_pose (state_t *self)
{
    if (self->error_flag_odom || (!self->odom_only && self->error_flag_imu))
        return 0;

    // Return if we don't have an odometry reading yet
    if (!self->odom_last || !self->odom_prev_utime){
        return 0;
    }    

    if (!self->odom_only) {
        if (!self->imu_last || !self->odom_prev_utime ){
            return 0;
        }
        
        // Monitor for any descrepancy between odom and imu utimes
        if (fabs (self->imu_last->utime - self->odom_last->utime) > WARN_UTIME_DIFFERENCE)
            fprintf (stderr, "Warning: speed utime differs from IMU utime by "
                     "%"PRId64" us\n", self->imu_last->utime - self->odom_last->utime);
    }

    int64_t now = bot_timestamp_now();
    double dt_sec = ((double) (now - self->pose_last_utime)) * 1e-6;
    if (dt_sec < 0) {
        fprintf (stderr, "Negative jump in time of %.2f seconds detected. Skipping pose integration\n", dt_sec);
        self->pose_last_utime = now;
        return -1;
    }
    else if (dt_sec > MAX_DELTA_TIME) {
        fprintf (stderr, "Jump in time of %.2f seconds. Skipping pose integration\n", dt_sec);
        self->pose_last_utime = now;
        return -1;
    }
    
    if (self->odom_only) {
        self->pose->rotation_rate[0] = 0;
        self->pose->rotation_rate[1] = 0;
        self->pose->rotation_rate[2] = self->odom_last->rv;
    }
    else {
        if(!self->odom_stopped){
            self->pose->rotation_rate[0] = self->imu_last->gyro[0];
            self->pose->rotation_rate[1] = self->imu_last->gyro[1];
            self->pose->rotation_rate[2] = self->imu_last->gyro[2];
            bot_quat_rotate (self->imu_to_body_trans.rot_quat, self->pose->rotation_rate);
        }
        else{
            self->pose->rotation_rate[0] = 0;
            self->pose->rotation_rate[1] = 0;
            self->pose->rotation_rate[2] = 0;
        }
        // Rotate the angular velocities from the IMU frame to the local frame        
    }

    // Compute the heading as integrated yaw rate, but use the roll and pitch computed by the IMU
    //int64_t now = bot_timestamp_now();
    //double dt_odom = ((double) (self->odom_last->utime - self->odom_prev_utime)) * 1e-9;//* 1e-6;

    // The current estimate of the vehicle's RPY in the local frame
    double rpy_body_to_local[3];
    bot_quat_to_roll_pitch_yaw (self->pose->orientation, rpy_body_to_local);

    if (self->odom_only) {
        rpy_body_to_local[0] = 0;
        rpy_body_to_local[1] = 0;
        rpy_body_to_local[2] += self->pose->rotation_rate[2] * dt_sec;    
    }
    else {
        // Compute the vehicle orientation in the local frame as measured by the IMU
        if(!self->odom_stopped){
            double q[4];
            bot_quat_mult(q, self->imu_to_body_trans.rot_quat, self->imu_last->quat);
            double rpy_per_imu[3];
            bot_quat_to_roll_pitch_yaw (q, rpy_per_imu);
            
            //double dt_imu = ((double) (self->imu_last->utime - self->imu_prev_utime)) * 1e-6;
            rpy_body_to_local[0] = rpy_per_imu[0];
            rpy_body_to_local[1] = rpy_per_imu[1];
            rpy_body_to_local[2] += self->pose->rotation_rate[2] * dt_sec;
        }
    }
    

    bot_mod2pi (rpy_body_to_local[2]);
    
    // The new estimate of the vehicle orientation
    bot_roll_pitch_yaw_to_quat (rpy_body_to_local, self->pose->orientation);

    // Integrate the forward velocity assuming zero roll and pitch
    double rpy_body_to_local_level[] = {0, 0, rpy_body_to_local[2]};
    double q_body_to_local_level[4];
    bot_roll_pitch_yaw_to_quat (rpy_body_to_local_level, q_body_to_local_level);
    double vel[] = {self->odom_last->tv, 0, 0};
    bot_quat_rotate (q_body_to_local_level, vel);

    self->pose->vel[0] = vel[0];
    self->pose->vel[1] = vel[1];
    self->pose->vel[2] = vel[2];
    
    self->pose->pos[0] += self->pose->vel[0] * dt_sec;
    self->pose->pos[1] += self->pose->vel[1] * dt_sec;
    self->pose->pos[2] += self->pose->vel[2] * dt_sec;
    
    if (self->odom_only)
        self->pose->utime = self->odom_last->utime;
    else 
        self->pose->utime = bot_min (self->imu_last->utime, self->odom_last->utime);

    // If we are publishing the pose message, also publish the GLOBAL_TO_LOCAL transformation
    if (self->publish_pose_channel_message) {
        int64_t utime_now = bot_timestamp_now();
        if ((utime_now - self->pose_publish_last_utime) > 1e6/PUBLISH_POSE_HZ) {
            self->pose_publish_last_utime = bot_timestamp_now();
            bot_core_pose_t_publish( self->lcm, "POSE", self->pose );
        }        
    }
    else{
        int64_t utime_now = bot_timestamp_now();
        if ((utime_now - self->pose_publish_last_utime) > 1e6/PUBLISH_POSE_HZ) {
            self->pose_publish_last_utime = bot_timestamp_now();
            bot_core_pose_t_publish( self->lcm, "POSE_DEADRECKON", self->pose );
        }
    }
    if(self->transform){
        bot_core_rigid_transform_t global_to_local;
        global_to_local.trans[0] = 0;
        global_to_local.trans[1] = 0;
        global_to_local.trans[2] = 0;
        global_to_local.quat[0] = 1;
        global_to_local.quat[1] = 0;
        global_to_local.quat[2] = 0;
        global_to_local.quat[3] = 0;
        
        global_to_local.utime = self->pose->utime;

        int64_t utime_now = bot_timestamp_now();
        if ((utime_now - self->last_global_to_local_utime) > 1e6/PUBLISH_GLOBAL_TO_LOCAL_HZ) {
            bot_core_rigid_transform_t_publish (self->lcm, "GLOBAL_TO_LOCAL", &global_to_local);
            self->last_global_to_local_utime = utime_now;
        }
    }

    self->pose_last_utime = now;

    return 0;
}


double get_ticks_to_meters_normal(int ticks){
    return ticks / ORC_TICKS_PER_REVOLUTION * 2 * M_PI * (ORC_WHEEL_DIAMETER /2);
}

int get_ticks_difference(int ticks_c, int ticks_p){
    int abs_reg = fabs(ticks_c - ticks_p);
    int abs_opp = 2147483647 - abs_reg;

    return fmin(abs_reg, abs_opp); 
}

double get_ticks_to_meters(int ticks_c, int ticks_p){
    int abs_reg = fabs(ticks_c - ticks_p);
    int abs_opp = 2147483647 - abs_reg;

    int actual_delta = fmin(abs_reg, abs_opp); 

    int used_delta = 0;

    //no wraparound 
    if(abs_reg < abs_opp)
        used_delta = ticks_c - ticks_p;

    else{ //wrapped around
        if(ticks_c - ticks_p > 0)
            used_delta = -abs_opp;
        else{
            used_delta = abs_opp;
        }
    }

    return used_delta / ORC_TICKS_PER_REVOLUTION * 2 * M_PI * (ORC_WHEEL_DIAMETER /2);
}

static void
on_raw_odometry_update(const lcm_recv_buf_t *rbuf, const char *channel,
                   const erlcm_orc_debug_stat_msg_t *msg, void *user)
{
    state_t *self = (state_t*) user;
    if(self->raw_last == NULL){
        self->raw_last = erlcm_orc_debug_stat_msg_t_copy(msg);
        return;
    }

    if(self->odom_last == NULL){
        self->odom_last = (erlcm_raw_odometry_msg_t *) calloc(1,sizeof(erlcm_raw_odometry_msg_t));
    }

    static int ticks_right = 0; 
    static int ticks_left = 0;
        
    double dl = get_ticks_to_meters(msg->qei_position[0], self->raw_last->qei_position[0]);
    double dr = get_ticks_to_meters(msg->qei_position[1], self->raw_last->qei_position[1]);
    ticks_left  += get_ticks_difference(msg->qei_position[0], self->raw_last->qei_position[0]);
    ticks_right += get_ticks_difference(msg->qei_position[1], self->raw_last->qei_position[1]);

    double dt = (msg->utime - self->raw_last->utime) /1.0e6;

    double tv = (dl + dr)/2/dt;

    double rv = (dr - dl) / ORC_WHEELBASE / dt;

    double dx = (dl + dr)/2; 

    double tv_orc = (get_ticks_to_meters_normal(msg->qei_velocity[0]) + get_ticks_to_meters_normal(msg->qei_velocity[1]))/2;

    double rv_orc = (- get_ticks_to_meters_normal(msg->qei_velocity[0]) + get_ticks_to_meters_normal(msg->qei_velocity[1]))/ ORC_WHEELBASE;

    if(tv_orc == 0 && rv_orc == 0){
        self->odom_stopped = 1;
    }
    else{
        self->odom_stopped = 0;
    }
    
    /*fprintf(stderr,"Left vel - from ticks: %f Normal : %f\n", dl, get_ticks_to_meters_normal(msg->qei_velocity[0]));
    fprintf(stderr,"Right vel - from ticks: %f Normal : %f\n", dr, get_ticks_to_meters_normal(msg->qei_velocity[1]));
    
    fprintf(stderr, "++++ Ticks left : %d Ticks right : %d => Avg : %d\n", ticks_left, ticks_right, (ticks_right + ticks_left)/2);

    fprintf(stderr, "Left : %f Right : %f dt : %f => TV : %f RV : %f\n", 
    dl, dr, dt, tv, rv);*/

    self->odom_last->utime = msg->utime;
    self->odom_last->tv = tv;
    self->odom_last->rv = rv;
    
    self->odom_last->x += dx * cos(self->odom_last->theta);
    self->odom_last->y += dx * sin(self->odom_last->theta);
    self->odom_last->theta += (dr - dl) / ORC_WHEELBASE; 

    self->odom_prev_utime = msg->utime;

    /*if (self->odom_last) {
        
        erlcm_raw_odometry_msg_t_destroy (self->odom_last);
        }*/

    
    if(self->raw_last){
        erlcm_orc_debug_stat_msg_t_destroy(self->raw_last);       
    }
    self->raw_last = erlcm_orc_debug_stat_msg_t_copy(msg);
    
    if (fabs(tv) < 1e-6)
        self->is_still = 1;
    else
        self->is_still = 0;

    self->odom_last_received_utime = bot_timestamp_now ();
    emit_pose (self);
}

static void
on_imu (const lcm_recv_buf_t *rbuf, const char *channel, 
        const xsens_ins_t *imu, void *user )
{
    state_t *self = (state_t *) user;
    xsens_ins_t imu_out;
    int i;
    int force_fixup = 0;

    /* Calibrate at startup if the user requested it */
    if (self->calibrate) {
        gyro_calibrate (imu, self);
        //return;
    }

    if (self->is_still) {
        self->consecutive_still++;
        self->calibrate_still_count++;

        /* Wait 3 seconds after stopping and then start a calibration */
        if (self->auto_calibrate && self->calibrate_still_count > 300)
            gyro_calibrate (imu, self);
    }
    else {
        self->calibrate_still_count = 0;
        self->consecutive_still = 0;
        self->calib_count = 0;
    }


    if (self->imu_last) {
        self->imu_prev_utime = self->imu_last->utime;
        xsens_ins_t_destroy (self->imu_last);
    }

    self->imu_last = xsens_ins_t_copy (imu);


    if (self->calibration_done) {
        if (!self->calib_announce) {
            printf ("Using calibrated gyro biases\n");
            self->calib_announce = 1;
        }
        for (i = 0; i < 3; i++){
            if(self->calibrate){
                 self->imu_last->gyro[i] -= self->gyro_calib_param[i];
            }
            else{
                self->imu_last->gyro[i] -= self->gyro_calib[i];
            }
        }
    }
    /* If the device is a known IMU, subtract some reasonable calibration
     * values */
#if IMU_LCM_HAS_SERIAL
    else if (imu->imu_serial_number == 0x00300395) {
        if (!self->calib_announce) {
            fprintf (stdout, "Using known XSens gyro biases\n");
            self->calib_announce = 1;
        }
        self->imu_last->gyro[0] -= -0.006896;
        self->imu_last->gyro[1] -= -0.004279;
        self->imu_last->gyro[2] -= -0.002147;
    }
    else if (imu->imu_serial_number == 0x00300396) {
        if (!self->calib_announce) {
            fprintf (stdout, "Using known XSens gyro biases\n");
            self->calib_announce = 1;
        }
        self->imu_last->gyro[0] -= 0.002637;
        self->imu_last->gyro[1] -= -0.002170;
        self->imu_last->gyro[2] -= -0.004832;
    }

    if (imu->imu_serial_number == 0x00300395 ||
            imu->imu_serial_number == 0x00300396) {
        if (self->fixup == -1)
            force_fixup = 1;
    }
#endif
    if (self->fixup == 1 || force_fixup) {
        if (!self->fixup_announce) {
            printf ("Performing yaw fixup\n");
            self->fixup_announce = 1;
        }
        imu_fixup_yaw (self);
    }

    self->imu_last_received_utime = bot_timestamp_now();

    emit_pose(self);
}


static gboolean
on_status_timer (gpointer data)
{
    state_t *self = (state_t *) data;

    // Compare current clock against time of last imu and odom messages
    int64_t now = bot_timestamp_now();

    if (self->odom_last_received_utime > 0 && fabs (now - self->odom_last_received_utime) > 5*1E6/NOMINAL_ODOM_HZ) {
        fprintf (stderr, "Error: It has been %.4f seconds since last odometry message was received\n",
                 ((float) (now - self->odom_last_received_utime))/1E6);
        self->error_flag_odom = 1;
        // TODO: Publish fault
    }
    else
        self->error_flag_odom = 0;

    if (self->imu_last_received_utime > 0 && !self->odom_only && (fabs (now - self->imu_last_received_utime) > 5*1E6/NOMINAL_IMU_HZ)) {
        fprintf (stderr, "Error: It has been %.4f seconds since last IMU message was received\n",
                 ((float) (now - self->imu_last_received_utime))/1E6);
        self->error_flag_imu = 1;
        // TODO: Publish fault
    }
    else
        self->error_flag_imu = 0;

    return TRUE;
}




static void
usage (int argc, char ** argv)
{
    fprintf (stderr, "Usage: %s [OPTIONS]\n"
             "Dead-reckoning module...\n"
             "\n"
             "Options:\n"
             "    -o, --odometry-only       Use only odometry and not IMU.\n"     
             "    -t, --publish-transform   Publish global to local transform\n"     
             "    -p, --publish-pose        Publish pose on POSE channel rather than POSE_DEADRECKON\n"
             "    -c, --calibrate           Calibrate the IMU's gyro biases for 5 seconds at startup\n"
             "    -a, --always-calibrate    Auto-calibrate IMU biases any time the vehicle is stationary\n"
             "    -f N, --fixup=N           Enable the fixup of the IMU's quaternion in the yaw direction by\n"
             "                               integrating the gyro's ourselves.  N=1 On, N=0 Off (default)\n"
             "    -v     Be verbose\n"
             "    -h     This help message\n", argv[0]);
};


int
main (int argc, char ** argv)
{

    // so that redirected stdout won't be insanely buffered.
    setlinebuf(stdout);


    state_t *self = (state_t *) calloc (1, sizeof (state_t));
    int c;

    self->fixup = -1;
    char *optstring = "hvcf:opta";
    struct option long_opts[] = {
        { "help", no_argument, NULL, 'h' },
        { "verbose", no_argument, NULL, 'v' },
        { "odometry-only", no_argument, NULL, 'o' },
        { "publish-pose", no_argument, NULL, 'p' },
        { "publish-transform", no_argument, NULL, 't' },
        { "calibrate", no_argument, NULL, 'c' },
        { "always-calibrate", no_argument, NULL, 'a' },
        { "fixup", required_argument, NULL, 'f' },
        { 0, 0, 0, 0 }
    };

    while ((c = getopt_long (argc, argv, optstring, long_opts, 0 )) >= 0) {
        switch (c) {
        case 'v':
            self->verbose = 1;
            break;
        case 't':
            self->transform = 1;
            break;
        case 'c':
            self->calibrate = 1;
            break;
        case 'a':
            self->auto_calibrate = 1;
            break;
        case 'o':
            self->odom_only = 1;
            break;
        case 'f':
            self->fixup = atoi (optarg);
            break;
        case 'p':
            self->publish_pose_channel_message = 1;
            break;
        case 'h':
        case '?':
        default:
            usage(argc, argv);
            return 1;
        }
    }

    fprintf(stderr,"Publishing Pose : %d\n", self->publish_pose_channel_message);

    // Allocate the initial pose and set the heading to 0 degrees.
    self->pose = (bot_core_pose_t *) calloc (1, sizeof (bot_core_pose_t));
    double rpy[3] = {0,0,0};
    
    bot_roll_pitch_yaw_to_quat(rpy, self->pose->orientation);
    self->raw_last = NULL;
    self->lcm = bot_lcm_get_global (NULL);

    self->odom_last = (erlcm_raw_odometry_msg_t *) calloc(1, sizeof(erlcm_raw_odometry_msg_t));

    self->odom_stopped = 0;

    // Get the transformation from the IMU to body
    BotParam *param = bot_param_new_from_server (self->lcm, 0);
    BotFrames *frames = bot_frames_get_global (self->lcm, param);

    if (bot_frames_get_trans (frames, "imu", "body", &(self->imu_to_body_trans)) == 0) {
        fprintf (stderr, "Error getting transformation from IMU to BODY via BotFrames\n");
        return -1;
    }
    
    bot_param_get_double_array (param, "calibration.imu.gyro_biases", (self->gyro_calib_param) , 3);

    bot_param_get_double_array (param, "calibration.imu.accel_biases", (self->accel_calib_param) , 3);

    fprintf(stderr, "Gyro Config bians : %f,%f,%f\n", self->gyro_calib_param[0], self->gyro_calib_param[1], self->gyro_calib_param[2]);
      
    xsens_ins_t_subscribe( self->lcm, "IMU", on_imu, self);
    
    erlcm_orc_debug_stat_msg_t_subscribe (self->lcm, "BASE_DEBUG_STAT", on_raw_odometry_update, self);

    /* Main Loop */
    self->mainloop = g_main_loop_new(NULL, FALSE);
    if (!self->mainloop) {
        fprintf(stderr,"Error: Failed to create the main loop\n");
        return -1;
    }

    self->status_timer_id = g_timeout_add (100, on_status_timer, self);
            
    // attach LCM to the mainloop
    bot_glib_mainloop_attach_lcm (self->lcm);
    

    /* sit and wait for messages */
    g_main_loop_run(self->mainloop);

    if (self->pose)
        free (self->pose);

    free (self);
    return 0;
}
