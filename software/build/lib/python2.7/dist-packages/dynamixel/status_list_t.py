"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

import dynamixel.status_t

class status_list_t(object):
    __slots__ = ["utime", "nservos", "servos"]

    def __init__(self):
        self.utime = 0
        self.nservos = 0
        self.servos = []

    def encode(self):
        buf = BytesIO()
        buf.write(status_list_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">qh", self.utime, self.nservos))
        for i0 in range(self.nservos):
            assert self.servos[i0]._get_packed_fingerprint() == dynamixel.status_t._get_packed_fingerprint()
            self.servos[i0]._encode_one(buf)

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != status_list_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return status_list_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = status_list_t()
        self.utime, self.nservos = struct.unpack(">qh", buf.read(10))
        self.servos = []
        for i0 in range(self.nservos):
            self.servos.append(dynamixel.status_t._decode_one(buf))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if status_list_t in parents: return 0
        newparents = parents + [status_list_t]
        tmphash = (0xce4b6435fcf10328+ dynamixel.status_t._get_hash_recursive(newparents)) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if status_list_t._packed_fingerprint is None:
            status_list_t._packed_fingerprint = struct.pack(">Q", status_list_t._get_hash_recursive([]))
        return status_list_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

