"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

class cmd_t(object):
    __slots__ = ["servo_id", "torque_enable", "goal_position", "max_speed", "max_torque"]

    def __init__(self):
        self.servo_id = 0
        self.torque_enable = 0
        self.goal_position = 0
        self.max_speed = 0
        self.max_torque = 0

    def encode(self):
        buf = BytesIO()
        buf.write(cmd_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">hbiii", self.servo_id, self.torque_enable, self.goal_position, self.max_speed, self.max_torque))

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != cmd_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return cmd_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = cmd_t()
        self.servo_id, self.torque_enable, self.goal_position, self.max_speed, self.max_torque = struct.unpack(">hbiii", buf.read(15))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if cmd_t in parents: return 0
        tmphash = (0xb21cd897bb47f770) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if cmd_t._packed_fingerprint is None:
            cmd_t._packed_fingerprint = struct.pack(">Q", cmd_t._get_hash_recursive([]))
        return cmd_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

