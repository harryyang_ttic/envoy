"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

class bot_state_t(object):
    __slots__ = ["utime", "prev_state", "new_state", "action", "speech_response", "have_person", "connected_to_cloud", "current_location", "current_floor", "current_goal", "final_goal"]

    def __init__(self):
        self.utime = 0
        self.prev_state = ""
        self.new_state = ""
        self.action = ""
        self.speech_response = ""
        self.have_person = 0
        self.connected_to_cloud = 0
        self.current_location = ""
        self.current_floor = 0
        self.current_goal = ""
        self.final_goal = ""

    def encode(self):
        buf = BytesIO()
        buf.write(bot_state_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">q", self.utime))
        __prev_state_encoded = self.prev_state.encode('utf-8')
        buf.write(struct.pack('>I', len(__prev_state_encoded)+1))
        buf.write(__prev_state_encoded)
        buf.write(b"\0")
        __new_state_encoded = self.new_state.encode('utf-8')
        buf.write(struct.pack('>I', len(__new_state_encoded)+1))
        buf.write(__new_state_encoded)
        buf.write(b"\0")
        __action_encoded = self.action.encode('utf-8')
        buf.write(struct.pack('>I', len(__action_encoded)+1))
        buf.write(__action_encoded)
        buf.write(b"\0")
        __speech_response_encoded = self.speech_response.encode('utf-8')
        buf.write(struct.pack('>I', len(__speech_response_encoded)+1))
        buf.write(__speech_response_encoded)
        buf.write(b"\0")
        buf.write(struct.pack(">hh", self.have_person, self.connected_to_cloud))
        __current_location_encoded = self.current_location.encode('utf-8')
        buf.write(struct.pack('>I', len(__current_location_encoded)+1))
        buf.write(__current_location_encoded)
        buf.write(b"\0")
        buf.write(struct.pack(">h", self.current_floor))
        __current_goal_encoded = self.current_goal.encode('utf-8')
        buf.write(struct.pack('>I', len(__current_goal_encoded)+1))
        buf.write(__current_goal_encoded)
        buf.write(b"\0")
        __final_goal_encoded = self.final_goal.encode('utf-8')
        buf.write(struct.pack('>I', len(__final_goal_encoded)+1))
        buf.write(__final_goal_encoded)
        buf.write(b"\0")

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != bot_state_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return bot_state_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = bot_state_t()
        self.utime = struct.unpack(">q", buf.read(8))[0]
        __prev_state_len = struct.unpack('>I', buf.read(4))[0]
        self.prev_state = buf.read(__prev_state_len)[:-1].decode('utf-8', 'replace')
        __new_state_len = struct.unpack('>I', buf.read(4))[0]
        self.new_state = buf.read(__new_state_len)[:-1].decode('utf-8', 'replace')
        __action_len = struct.unpack('>I', buf.read(4))[0]
        self.action = buf.read(__action_len)[:-1].decode('utf-8', 'replace')
        __speech_response_len = struct.unpack('>I', buf.read(4))[0]
        self.speech_response = buf.read(__speech_response_len)[:-1].decode('utf-8', 'replace')
        self.have_person, self.connected_to_cloud = struct.unpack(">hh", buf.read(4))
        __current_location_len = struct.unpack('>I', buf.read(4))[0]
        self.current_location = buf.read(__current_location_len)[:-1].decode('utf-8', 'replace')
        self.current_floor = struct.unpack(">h", buf.read(2))[0]
        __current_goal_len = struct.unpack('>I', buf.read(4))[0]
        self.current_goal = buf.read(__current_goal_len)[:-1].decode('utf-8', 'replace')
        __final_goal_len = struct.unpack('>I', buf.read(4))[0]
        self.final_goal = buf.read(__final_goal_len)[:-1].decode('utf-8', 'replace')
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if bot_state_t in parents: return 0
        tmphash = (0x7987550c738c453c) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if bot_state_t._packed_fingerprint is None:
            bot_state_t._packed_fingerprint = struct.pack(">Q", bot_state_t._get_hash_recursive([]))
        return bot_state_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

