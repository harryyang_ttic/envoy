"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

import erlcm.rect_list_t

import erlcm.track_list_t

class obstacle_list_t(object):
    __slots__ = ["rects", "tracks"]

    def __init__(self):
        self.rects = erlcm.rect_list_t()
        self.tracks = erlcm.track_list_t()

    def encode(self):
        buf = BytesIO()
        buf.write(obstacle_list_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        assert self.rects._get_packed_fingerprint() == erlcm.rect_list_t._get_packed_fingerprint()
        self.rects._encode_one(buf)
        assert self.tracks._get_packed_fingerprint() == erlcm.track_list_t._get_packed_fingerprint()
        self.tracks._encode_one(buf)

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != obstacle_list_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return obstacle_list_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = obstacle_list_t()
        self.rects = erlcm.rect_list_t._decode_one(buf)
        self.tracks = erlcm.track_list_t._decode_one(buf)
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if obstacle_list_t in parents: return 0
        newparents = parents + [obstacle_list_t]
        tmphash = (0x980a8d3d59cbba6+ erlcm.rect_list_t._get_hash_recursive(newparents)+ erlcm.track_list_t._get_hash_recursive(newparents)) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if obstacle_list_t._packed_fingerprint is None:
            obstacle_list_t._packed_fingerprint = struct.pack(">Q", obstacle_list_t._get_hash_recursive([]))
        return obstacle_list_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

