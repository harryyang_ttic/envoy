"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

import erlcm.euler_t

import erlcm.position_t

class imu_msg_t(object):
    __slots__ = ["acc", "att", "z", "zdot", "timestamp"]

    def __init__(self):
        self.acc = erlcm.position_t()
        self.att = erlcm.euler_t()
        self.z = 0.0
        self.zdot = 0.0
        self.timestamp = 0.0

    def encode(self):
        buf = BytesIO()
        buf.write(imu_msg_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        assert self.acc._get_packed_fingerprint() == erlcm.position_t._get_packed_fingerprint()
        self.acc._encode_one(buf)
        assert self.att._get_packed_fingerprint() == erlcm.euler_t._get_packed_fingerprint()
        self.att._encode_one(buf)
        buf.write(struct.pack(">ffd", self.z, self.zdot, self.timestamp))

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != imu_msg_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return imu_msg_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = imu_msg_t()
        self.acc = erlcm.position_t._decode_one(buf)
        self.att = erlcm.euler_t._decode_one(buf)
        self.z, self.zdot, self.timestamp = struct.unpack(">ffd", buf.read(16))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if imu_msg_t in parents: return 0
        newparents = parents + [imu_msg_t]
        tmphash = (0xc3bc27256518078+ erlcm.position_t._get_hash_recursive(newparents)+ erlcm.euler_t._get_hash_recursive(newparents)) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if imu_msg_t._packed_fingerprint is None:
            imu_msg_t._packed_fingerprint = struct.pack(">Q", imu_msg_t._get_hash_recursive([]))
        return imu_msg_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

