"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

class map_loc_t(object):
    __slots__ = ["utime", "map_loc", "placelist_loc"]

    def __init__(self):
        self.utime = 0
        self.map_loc = ""
        self.placelist_loc = ""

    def encode(self):
        buf = BytesIO()
        buf.write(map_loc_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">q", self.utime))
        __map_loc_encoded = self.map_loc.encode('utf-8')
        buf.write(struct.pack('>I', len(__map_loc_encoded)+1))
        buf.write(__map_loc_encoded)
        buf.write(b"\0")
        __placelist_loc_encoded = self.placelist_loc.encode('utf-8')
        buf.write(struct.pack('>I', len(__placelist_loc_encoded)+1))
        buf.write(__placelist_loc_encoded)
        buf.write(b"\0")

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != map_loc_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return map_loc_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = map_loc_t()
        self.utime = struct.unpack(">q", buf.read(8))[0]
        __map_loc_len = struct.unpack('>I', buf.read(4))[0]
        self.map_loc = buf.read(__map_loc_len)[:-1].decode('utf-8', 'replace')
        __placelist_loc_len = struct.unpack('>I', buf.read(4))[0]
        self.placelist_loc = buf.read(__placelist_loc_len)[:-1].decode('utf-8', 'replace')
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if map_loc_t in parents: return 0
        tmphash = (0x44a92baa22c0c50b) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if map_loc_t._packed_fingerprint is None:
            map_loc_t._packed_fingerprint = struct.pack(">Q", map_loc_t._get_hash_recursive([]))
        return map_loc_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

