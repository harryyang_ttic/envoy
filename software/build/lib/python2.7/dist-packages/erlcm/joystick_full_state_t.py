"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

class joystick_full_state_t(object):
    __slots__ = ["utime", "count", "values"]

    def __init__(self):
        self.utime = 0
        self.count = 0
        self.values = []

    def encode(self):
        buf = BytesIO()
        buf.write(joystick_full_state_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">qh", self.utime, self.count))
        buf.write(struct.pack('>%dh' % self.count, *self.values[:self.count]))

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != joystick_full_state_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return joystick_full_state_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = joystick_full_state_t()
        self.utime, self.count = struct.unpack(">qh", buf.read(10))
        self.values = struct.unpack('>%dh' % self.count, buf.read(self.count * 2))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if joystick_full_state_t in parents: return 0
        tmphash = (0x41e55c0302b46c38) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if joystick_full_state_t._packed_fingerprint is None:
            joystick_full_state_t._packed_fingerprint = struct.pack(">Q", joystick_full_state_t._get_hash_recursive([]))
        return joystick_full_state_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

