#ifndef __full_laser_h__
#define __full_laser_h__

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>

#ifdef __cplusplus
extern "C" {
#endif

    typedef void(on_full_laser_update_t)(int64_t utime, bot_core_planar_lidar_t *laser, void *user);

    typedef struct {
        on_full_laser_update_t * callback_func;
        void * user;
    } on_full_laser_update_handler_t;

    typedef struct _full_laser_state_t full_laser_state_t;
    struct _full_laser_state_t {
        lcm_t *lcm;
        BotParam *param;
        BotFrames *frames;
        GMainLoop *mainloop;
        bot_core_planar_lidar_t *last_front_laser;
        bot_core_planar_lidar_t *last_rear_laser;
        bot_core_planar_lidar_t *full_laser;
        int64_t last_laser_utime;
        GMutex *mutex;
        int no_bins; 
        float *readings;
        on_full_laser_update_handler_t *on_update;
    };

    full_laser_state_t * full_laser_init(lcm_t *lcm, int no_bins, on_full_laser_update_t *callback, void *user);

#ifdef __cplusplus
}
#endif

#endif
