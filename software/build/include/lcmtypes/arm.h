#ifndef __lcmtypes_arm_h__
#define __lcmtypes_arm_h__

#include "arm_cmd_trace_status_t.h"
#include "arm_cmd_joint_status_list_t.h"
#include "arm_cmd_circle_trace_t.h"
#include "arm_cmd_joint_list_t.h"
#include "arm_cmd_qs_t.h"
#include "arm_cmd_qs_list_t.h"
#include "arm_cmd_joint_draw.h"
#include "arm_cmd_grasp_plan_t.h"
#include "arm_cmd_ef_pose_t.h"
#include "arm_cmd_joint_status_t.h"
#include "arm_cmd_object_manipulation_cmd_t.h"
#include "arm_cmd_joint_t.h"
#include "arm_cmd_ik_t.h"

#endif
