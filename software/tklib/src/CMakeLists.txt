add_definitions(
#    -ggdb3 
    -O0 
    -std=c99
    -c -pipe -Wall -W   -DQT_SHARED -DQT_NO_DEBUG -DQT_THREAD_SUPPORT  -fPIC -fpic -g
    #-std=gnu99
    -Dbool=char -Dtrue=1 -Dfalse=0 #-Dand=&& 
    )

# Create a shared library libhello.so with a single source file
add_library(tklib SHARED
    avs.c             
#    carmen_subscribe.c  
    EKF2D.c        
    gsl_python.c        
    kmeans.c  
    math3d.c            
    probability.c 
    quaternion.c
    #spatial_features.c
    box_window.c      
    carmen_util.c       
    gaussian.c
    gsl_utilities.c
    line.c
    nearest_neighbor.c
    procrustes.cpp
    #simulator.c
    spline.cpp
    #carmen_publish.c 
    #crf_utils.c
    gridmapping.cpp
    hurdle_extractor.c
    math2d.c
#    noise_models.c
    python_swig_utils.c
    #spatial_feature_extractor.c
    tklib_log_gridmap.cpp    
    
)

# make the header public
# install it to include/hello
pods_install_headers(avs.h 
  #carmen_publish.h
  #crf_utils.h
  gaussian.h
  gsl_utilities.h
  line.h
  named_enum.h
  probability.h
  quaternion.h
  #spatial_features.h
  box_window.h
  #carmen_subscribe.h
  datatypes.h
  gridmapping.hpp
  hurdle_extractor.h
  math2d.h
  nearest_neighbor.h
  procrustes.hpp
  #simulator.h
  spline.h
  #carmen_messages.h
  carmen_util.h
  EKF2D.h
  gsl_python.h
  kmeans.h
  math3d.h
#  noise_models.hpp
  python_swig_utils.h
  #spatial_feature_extractor.h
  tklib_log_gridmap.hpp
  DESTINATION tklib)

pods_use_pkg_config_packages(tklib lcm bot2-core lcmtypes_er-lcmtypes er-common map3d_interface)

include_directories(
    ${GLIB2_INCLUDE_DIRS}
    ${GTK2_INCLUDE_DIRS}
    ${LCM_INCLUDE_DIRS}
    )

message(GLIB ${GLIB2_INCLUDE_DIRS})

# make the library public
pods_install_libraries(tklib)

# uncomment these lines to link against another library via pkg-config
#set(REQUIRED_PACKAGES pkg_a pkg_b)
#pods_use_pkg_config_packages(hello ${REQUIRED_PACKAGES})

# create a pkg-config file for the library, to make it easier for other
# software to use.
pods_install_pkg_config_file(tklib
    CFLAGS
    LIBS -ltklib
    REQUIRES ${REQUIRED_PACKAGES}
    VERSION 0.0.1)
