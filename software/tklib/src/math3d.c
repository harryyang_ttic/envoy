#include "math3d.h"
#include "math2d.h"

void math3d_prism_free(math3d_prism_t p)
{
  gsl_matrix_free(p.points_xy);
}

math3d_prism_t math3d_prism_init()
{
  math3d_prism_t result;
  result.points_xy = 0;
  result.initialized = 1;
  return result;
}


bool math3d_higher_than(math3d_prism_t p1, math3d_prism_t p2) 
{
  if (p1.z_end > p2.z_end) {
    return true;
  } else {
    return false;
  }
}

bool math3d_starts_higher_than(math3d_prism_t p1, math3d_prism_t p2) 
{
  if (p1.z_start > p2.z_start) {
    return true;
  } else {
    return false;
  }
}

bool math3d_supports(math3d_prism_t p1, math3d_prism_t p2)
{
  if (math3d_starts_higher_than(p2, p1) &&
      math2d_overlaps(p1.points_xy, p2.points_xy)) {
    return true;
  } else {
    return false;
  }
}



bool math3d_intersects(math3d_prism_t p1, math3d_prism_t p2)
{
  if (math2d_range_overlaps(p1.z_start, p1.z_end, p2.z_start, p2.z_end) && 
      math2d_overlaps(p1.points_xy, p2.points_xy)) {
    return true;
  } else {
    return false;
  }
}
