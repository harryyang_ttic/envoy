#ifndef CRF_UTILS_H
#define CRF_UTILS_H

#include "spatial_feature_extractor.h"
#include <vector>
#include <string>

using namespace std;

#ifdef __cplusplus
extern "C" {
#endif


  /* gets the name of all the features */
vector<string> crf_extract_features_names_observation(vector<string> figure, 
						      vector<string> verb, 
						      vector<string> spatial_relation, 
						      vector<string> landmark);

vector<string> crf_extract_features_names_transition(vector<string> figure, 
						     vector<string> verb, 
						     vector<string> spatial_relation, 
						     vector<string> landmark);

  /* gets the value of all the features */
gsl_vector* crf_extract_features_values_observation(vector<string> figure, 
						    vector<string> verb, 
						    vector<string> spatial_relation, 
						    vector<string> landmark, 
						    gsl_matrix* fig1_xyth, 
						    gsl_matrix* gnd1_xy, 
						    bool perform_scaling);


gsl_vector* crf_extract_features_values_transition(vector<string> figure, 
						   vector<string> verb, 
						   vector<string> spatial_relation, 
						   vector<string> landmark, 
						   gsl_matrix* fig1_xyth, 
						   gsl_matrix* fig2_xyth, 
						   bool perform_scaling);


/* Factor the features */
vector<string> crf_extract_features_names_transition_factored(vector<string> verb);

vector<string> crf_extract_features_names_observation_factored(vector<string> figure, 
							       vector<string> verb, 
							       vector<string> spatial_relation, 
							       vector<string> landmark);
  
gsl_vector* crf_extract_features_values_transition_factored(vector<string> verb, 
							    gsl_matrix* fig1_xyth, 
							    gsl_matrix* fig2_xyth, 
							    bool perform_scaling);

gsl_vector* crf_extract_features_values_observation_factored(vector<string> figure, 
							     vector<string> verb, 
							     vector<string> spatial_relation, 
							     vector<string> landmark, 
							     gsl_matrix* fig1_xyth, 
							     gsl_matrix* gnd1_xy, 
							     bool perform_scaling);

  /*********************************************/
  /*             Internal Function            */
vector<string> crf_get_word_features(vector<string> tokens, 
				     vector<string> names, string prefix);

/*ESDC Functions*/
gsl_vector* flu_path_polygon(vector<string> relation, 
			     gsl_matrix* fig1_xyth, 
		             gsl_matrix* gnd1_xy, 
			     bool perform_scaling);

vector<string> flu_path_polygon_names(vector<string> relation);


gsl_vector* flu_polygon(vector<string> figure, 
			gsl_matrix* gnd1_xy, 
			bool perform_scaling);

vector<string> flu_polygon_names(vector<string> figure);

gsl_vector* flu_polygon_polygon(vector<string> relation, 
		   	        gsl_matrix* fig1_xy, 
				gsl_matrix* fig2_xy, 
				bool perform_scaling);

vector<string> flu_polygon_polygon_names(vector<string> relation);

#ifdef __cplusplus
}
#endif

#endif


