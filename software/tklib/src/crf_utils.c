#include "crf_utils.h"

//get the word features
vector<string> crf_get_word_features(vector<string> tokens, 
				     vector<string> names, string prefix){
  
  vector<string> ret_names;
  
  string str = "";
  for(unsigned int k=0; k<tokens.size(); k++){
    for(unsigned int i=0; i<names.size(); i++){
      //print prefix+w+"_"+name,
      ret_names.push_back(str+prefix+tokens[k]+"_"+names[i]);
    }
  }
  
  return ret_names;
}


vector<string> crf_extract_features_names_observation(vector<string> figure, 
						      vector<string> verb, 
						      vector<string> spatial_relation, 
						      vector<string> landmark){
  /* compute the low-level features names */
  vector<string> names = sfe_f_path_l_polygon_names();
  
  /* cross the words with the feature names */
  vector<string> f_names = crf_get_word_features(figure, names, "f_");
  vector<string> v_names = crf_get_word_features(verb, names, "v_");
  vector<string> sr_names = crf_get_word_features(spatial_relation, names, "sr_");
  vector<string> l_names = crf_get_word_features(landmark, names, "l_");

  /* create the return feature names */
  vector<string> ret_names;
  
  for(unsigned int k=0; k<f_names.size(); k++)
    ret_names.push_back(f_names[k]);
  for(unsigned int k=0; k<v_names.size(); k++)
    ret_names.push_back(v_names[k]);
  for(unsigned int k=0; k<sr_names.size(); k++)
    ret_names.push_back(sr_names[k]);
  for(unsigned int k=0; k<l_names.size(); k++)
    ret_names.push_back(l_names[k]);

  return ret_names;
}

gsl_vector* crf_extract_features_values_observation(vector<string> figure, 
						    vector<string> verb, 
						    vector<string> spatial_relation, 
						    vector<string> landmark, 
						    gsl_matrix* fig1_xyth, 
						    gsl_matrix* gnd1_xy, 
						    bool perform_scaling){
  int num_words = figure.size()+verb.size()+spatial_relation.size()+landmark.size();

  if(num_words == 0) {
    return NULL;
  }
  
    
  gsl_vector * feats = sfe_extract_f_path_l_polygon(fig1_xyth, gnd1_xy, perform_scaling);  
  
  gsl_vector* ret_vec = gsl_vector_calloc(num_words*feats->size);
  for(int i=0; i<num_words; i++){
    gsl_vector_memcpy(&gsl_vector_subvector(ret_vec, feats->size*i, feats->size).vector, 
		      feats);
  }
  gsl_vector_free(feats);
  return ret_vec;
}




gsl_vector* crf_extract_features_values_observation_factored(vector<string> figure, 
							     vector<string> verb, 
							     vector<string> spatial_relation, 
							     vector<string> landmark, 
							     gsl_matrix* fig1_xyth, 
							     gsl_matrix* gnd1_xy, 
							     bool perform_scaling){
  if(figure.size() == 0 && verb.size() == 0 
     && spatial_relation.size() == 0 && landmark.size() == 0)
    return NULL;
  
  /* compute the low-level features for each component*/
  gsl_vector* feats_fig;  gsl_vector* feats_landmark;
  gsl_vector* feats_verb;  gsl_vector* feats_relation;
  
  gsl_vector* feats_fig_tmp = sfe_polygon_features(gnd1_xy);
  gsl_vector* feats_landmark_tmp = sfe_polygon_features(gnd1_xy);
  gsl_vector* feats_verb_tmp = sfe_path_features(fig1_xyth);
  
  //tklib_matrix_printf(fig1_xyth);
  //tklib_matrix_printf(gnd1_xy);
  gsl_vector* feats_relation_tmp = sfe_path_polygon_features(fig1_xyth, gnd1_xy);
  
  if(perform_scaling){
    gsl_matrix_view fig1_xy_p;
    gsl_matrix* fig1_xy = NULL;
    
    if(fig1_xyth != NULL){
      fig1_xy_p = gsl_matrix_submatrix(fig1_xyth, 0, 0, 2, fig1_xyth->size2);
      fig1_xy = &fig1_xy_p.matrix;
    }

    vector<string> names_fig = sfe_polygon_names();
    gsl_vector* mask_fig = sfe_get_normalization_mask(names_fig);

    assert(mask_fig->size == feats_fig_tmp->size);
    feats_fig = sfe_normalize_feature_vector(fig1_xy, gnd1_xy, mask_fig, feats_fig_tmp);

    vector<string> names_landmark = sfe_polygon_names();
    gsl_vector* mask_landmark = sfe_get_normalization_mask(names_landmark);

    assert(mask_landmark->size == feats_landmark_tmp->size);
    feats_landmark = sfe_normalize_feature_vector(fig1_xy, gnd1_xy, mask_landmark, feats_landmark_tmp);

    vector<string> names_verb = sfe_path_names();
    gsl_vector* mask_verb = sfe_get_normalization_mask(names_verb);
    assert(mask_verb->size == feats_verb_tmp->size);
    feats_verb = sfe_normalize_feature_vector(fig1_xy, gnd1_xy, mask_verb, feats_verb_tmp);

    vector<string> names_relation = sfe_path_polygon_names();
    gsl_vector* mask_relation = sfe_get_normalization_mask(names_relation);
    assert(mask_relation->size == feats_relation_tmp->size);
    feats_relation = sfe_normalize_feature_vector(fig1_xy, gnd1_xy, mask_relation, feats_relation_tmp);
    
    gsl_vector_free(feats_fig_tmp);
    gsl_vector_free(feats_landmark_tmp);
    gsl_vector_free(feats_verb_tmp);
    gsl_vector_free(feats_relation_tmp);
    gsl_vector_free(mask_fig);
    gsl_vector_free(mask_landmark);
    gsl_vector_free(mask_verb);
    gsl_vector_free(mask_relation);
  }
  else{
    feats_fig=feats_fig_tmp;
    feats_landmark=feats_landmark_tmp;
    feats_relation=feats_relation_tmp;
    feats_verb=feats_verb_tmp;
  }
  int num_features = feats_fig->size*figure.size()+feats_landmark->size*landmark.size()
    +feats_verb->size*verb.size()+feats_relation->size*spatial_relation.size();
  gsl_vector* ret_vec = gsl_vector_calloc(num_features);
  
  size_t i;  int curr_index = 0;
  for(i=0; i<figure.size(); i++){
    gsl_vector_memcpy(&gsl_vector_subvector(ret_vec, feats_fig->size*i, feats_fig->size).vector, feats_fig);
  }
  curr_index = feats_fig->size*figure.size();
  
  
  for(i=0; i<spatial_relation.size(); i++){
    gsl_vector_memcpy(&gsl_vector_subvector(ret_vec, feats_relation->size*i+curr_index,
					    feats_relation->size).vector, feats_relation);
  }
  curr_index += feats_relation->size*spatial_relation.size();


  for(i=0; i<verb.size(); i++){
    gsl_vector_memcpy(&gsl_vector_subvector(ret_vec, feats_verb->size*i+curr_index, 
					    feats_verb->size).vector, feats_verb);
  }
  curr_index += feats_verb->size*verb.size();

  
  for(i=0; i<landmark.size(); i++){
    gsl_vector_memcpy(&gsl_vector_subvector(ret_vec,feats_landmark->size*i+curr_index, 
					    feats_landmark->size).vector, feats_landmark);
  }

  gsl_vector_free(feats_fig);
  gsl_vector_free(feats_landmark);
  gsl_vector_free(feats_verb);
  gsl_vector_free(feats_relation);
  
  return ret_vec;
}


vector<string> crf_extract_features_names_observation_factored(vector<string> figure, 
							       vector<string> verb, 
							       vector<string> spatial_relation, 
							       vector<string> landmark){

  /* cross the words with the feature names */
  vector<string> names_fig = sfe_polygon_names();
  vector<string> f_names = crf_get_word_features(figure, names_fig, "f_");

  vector<string> names_verb = sfe_path_names();
  vector<string> v_names = crf_get_word_features(verb, names_verb, "v_");
  
  vector<string> names_relation = sfe_path_polygon_names();
  vector<string> sr_names = crf_get_word_features(spatial_relation, names_relation, "sr_");

  vector<string> names_landmark = sfe_polygon_names();
  vector<string> l_names = crf_get_word_features(landmark, names_landmark, "l_");
  
  /* create the return feature names */
  vector<string> ret_names;
  
  for(unsigned int k=0; k<f_names.size(); k++)
    ret_names.push_back(f_names[k]);
  for(unsigned int k=0; k<v_names.size(); k++)
    ret_names.push_back(v_names[k]);
  for(unsigned int k=0; k<sr_names.size(); k++)
    ret_names.push_back(sr_names[k]);
  for(unsigned int k=0; k<l_names.size(); k++)
    ret_names.push_back(l_names[k]);

  return ret_names;
}




vector<string> crf_extract_features_names_transition(vector<string> figure, 
						     vector<string> verb, 
						     vector<string> spatial_relation, 
						     vector<string> landmark){
  /* compute the low-level features names */
  vector<string> names = sfe_path_path_names();
  
  /* cross the words with the feature names */
  vector<string> f_names = crf_get_word_features(figure, names, "f_");
  vector<string> v_names = crf_get_word_features(verb, names, "v_");
  vector<string> sr_names = crf_get_word_features(spatial_relation, names, "sr_");
  vector<string> l_names = crf_get_word_features(landmark, names, "l_");

  /* create the return feature names */
  vector<string> ret_names;
  
  for(unsigned int k=0; k<f_names.size(); k++)
    ret_names.push_back(f_names[k]);
  for(unsigned int k=0; k<v_names.size(); k++)
    ret_names.push_back(v_names[k]);
  for(unsigned int k=0; k<sr_names.size(); k++)
    ret_names.push_back(sr_names[k]);
  for(unsigned int k=0; k<l_names.size(); k++)
    ret_names.push_back(l_names[k]);

  return ret_names;
}

gsl_vector* crf_extract_features_values_transition(vector<string> figure, 
						   vector<string> verb, 
						   vector<string> spatial_relation, 
						   vector<string> landmark, 
						   gsl_matrix* fig1_xyth, 
						   gsl_matrix* fig2_xyth, 
						   bool perform_scaling){
  
  int num_words = figure.size()+verb.size()+spatial_relation.size()+landmark.size();

  /* compute the low-level features and normalize if necessary */
  gsl_vector* feats;
  //printf("computing features\n");
  gsl_vector* feats_tmp = sfe_extract_path_path(fig1_xyth, fig2_xyth);
  
  if(perform_scaling){
    //printf("scaling\n");
    vector<string> all_names = sfe_path_path_names();
    gsl_vector* mask = sfe_get_normalization_mask(all_names);

    //printf("normalize feature vector: %d, %d\n", mask->size, feats_tmp->size);

    gsl_matrix_view fig1_xy_p;
    gsl_matrix* fig1_xy = NULL;
    
    if(fig1_xyth != NULL){
      fig1_xy_p = gsl_matrix_submatrix(fig1_xyth, 0, 0, 2, fig1_xyth->size2);
      fig1_xy = &fig1_xy_p.matrix;
    }

    gsl_matrix_view fig2_xy_p;
    gsl_matrix* fig2_xy = NULL;
    
    if(fig2_xyth != NULL){
      fig2_xy_p = gsl_matrix_submatrix(fig2_xyth, 0, 0, 2, fig2_xyth->size2);
      fig2_xy = &fig2_xy_p.matrix;
    }
    
    //gsl_matrix_view fig1_xy = gsl_matrix_submatrix(fig1_xyth, 0, 0, 2, fig1_xyth->size2);
    //gsl_matrix_view fig2_xy = gsl_matrix_submatrix(fig2_xyth, 0, 0, 2, fig2_xyth->size2);
    
    feats = sfe_normalize_feature_vector(fig1_xy, fig2_xy, mask, feats_tmp);
    
    gsl_vector_free(feats_tmp);
    gsl_vector_free(mask);
  }
  else
    feats=feats_tmp;

  //printf("word features\n");
  /*  copy according to the word features  */
  gsl_vector* ret_vec = gsl_vector_calloc(num_words*feats->size);
  for(int i=0; i<num_words; i++){
    gsl_vector_memcpy(&gsl_vector_subvector(ret_vec, feats->size*i, feats->size).vector, 
		      feats);
  }
  
  gsl_vector_free(feats);
  return ret_vec;
}

vector<string> crf_extract_features_names_transition_factored(vector<string> verb){
  /* compute the low-level features names */
  vector<string> names = sfe_path_path_names();
  
  /* cross the words with the feature names */
  vector<string> v_names = crf_get_word_features(verb, names, "v_");
  
  return v_names;
}


gsl_vector* crf_extract_features_values_transition_factored(vector<string> verb, 
							    gsl_matrix* fig1_xyth, 
							    gsl_matrix* fig2_xyth, 
							    bool perform_scaling){
  
  int num_words = verb.size();
  if(num_words == 0)
    return NULL;


  /* compute the low-level features and normalize if necessary */
  gsl_vector* feats;
  //printf("computing features\n");
  gsl_vector* feats_tmp = sfe_extract_path_path(fig1_xyth, fig2_xyth);
  
  if(perform_scaling){
    //printf("scaling\n");
    vector<string> all_names = sfe_path_path_names();
    gsl_vector* mask = sfe_get_normalization_mask(all_names);

    //printf("normalize feature vector: %d, %d\n", mask->size, feats_tmp->size);

    gsl_matrix_view fig1_xy_p;
    gsl_matrix* fig1_xy = NULL;
    
    if(fig1_xyth != NULL){
      fig1_xy_p = gsl_matrix_submatrix(fig1_xyth, 0, 0, 2, fig1_xyth->size2);
      fig1_xy = &fig1_xy_p.matrix;
    }

    gsl_matrix_view fig2_xy_p;
    gsl_matrix* fig2_xy = NULL;
    
    if(fig2_xyth != NULL){
      fig2_xy_p = gsl_matrix_submatrix(fig2_xyth, 0, 0, 2, fig2_xyth->size2);
      fig2_xy = &fig2_xy_p.matrix;
    }
    
    //gsl_matrix_view fig1_xy = gsl_matrix_submatrix(fig1_xyth, 0, 0, 2, fig1_xyth->size2);
    //gsl_matrix_view fig2_xy = gsl_matrix_submatrix(fig2_xyth, 0, 0, 2, fig2_xyth->size2);
    assert(mask->size == feats_tmp->size);
    feats = sfe_normalize_feature_vector(fig1_xy, fig2_xy, mask, feats_tmp);
    
    gsl_vector_free(feats_tmp);
    gsl_vector_free(mask);
  }
  else
    feats=feats_tmp;

  //printf("word features\n");
  /*  copy according to the word features  */
  gsl_vector* ret_vec = gsl_vector_calloc(num_words*feats->size);
  for(int i=0; i<num_words; i++){
    gsl_vector_memcpy(&gsl_vector_subvector(ret_vec, feats->size*i, feats->size).vector, 
		      feats);
  }
  
  gsl_vector_free(feats);
  return ret_vec;
}

///////////////////////////////////////////////////////////////////////////////
//
// Modified Features for ESDCs
//
///////////////////////////////////////////////////////////////////////////////

gsl_vector* flu_path_polygon(vector<string> relation, 
			     gsl_matrix* fig1_xyth, 
		             gsl_matrix* gnd1_xy, 
			     bool perform_scaling){
  if(relation.size() == 0)
    return NULL;
  
  gsl_vector* feats_relation;
  gsl_vector* feats_path;
  
  gsl_vector* feats_path_tmp = sfe_path_features(fig1_xyth);
  gsl_vector* feats_relation_tmp = sfe_path_polygon_features(fig1_xyth, gnd1_xy);
  
  if(perform_scaling){
    gsl_matrix_view fig1_xy_p;
    gsl_matrix* fig1_xy = NULL;
    
    if(fig1_xyth != NULL){
      fig1_xy_p = gsl_matrix_submatrix(fig1_xyth, 0, 0, 2, fig1_xyth->size2);
      fig1_xy = &fig1_xy_p.matrix;
    }

    vector<string> names_path = sfe_path_names();
    gsl_vector* mask_path = sfe_get_normalization_mask(names_path);
    assert(mask_path->size == feats_path_tmp->size);
    feats_path = sfe_normalize_feature_vector(fig1_xy, gnd1_xy, mask_path, feats_path_tmp);

    vector<string> names_relation = sfe_path_polygon_names();
    gsl_vector* mask_relation = sfe_get_normalization_mask(names_relation);
    assert(mask_relation->size == feats_relation_tmp->size);
    feats_relation = sfe_normalize_feature_vector(fig1_xy, gnd1_xy, mask_relation, feats_relation_tmp);
    
    gsl_vector_free(feats_path_tmp);
    gsl_vector_free(feats_relation_tmp);
    gsl_vector_free(mask_path);
    gsl_vector_free(mask_relation);
  }
  else{
    feats_relation=feats_relation_tmp;
    feats_path=feats_path_tmp;
  }
  int num_features = feats_path->size*relation.size()+feats_relation->size*relation.size();
  gsl_vector* ret_vec = gsl_vector_calloc(num_features);
  
  size_t i;  int curr_index = 0;

  for(i=0; i<relation.size(); i++){
    gsl_vector_memcpy(&gsl_vector_subvector(ret_vec, feats_relation->size*i+curr_index,
					    feats_relation->size).vector, feats_relation);
  }
  curr_index += feats_relation->size*relation.size();

  for(i=0; i<relation.size(); i++){
    gsl_vector_memcpy(&gsl_vector_subvector(ret_vec, feats_path->size*i+curr_index, 
					    feats_path->size).vector, feats_path);
  }

  gsl_vector_free(feats_path);
  gsl_vector_free(feats_relation);
  
  return ret_vec;
}


vector<string> flu_path_polygon_names(vector<string> relation)
{
  vector<string> names_path = sfe_path_names();
  vector<string> p_names = crf_get_word_features(relation, names_path, "r_");
  
  vector<string> names_relation = sfe_path_polygon_names();
  vector<string> r_names = crf_get_word_features(relation, names_relation, "r_");
 
  /* create the return feature names */
  vector<string> ret_names;
  
  for(unsigned int k=0; k<p_names.size(); k++)
    ret_names.push_back(p_names[k]);
  for(unsigned int k=0; k<r_names.size(); k++)
    ret_names.push_back(r_names[k]);

  return ret_names;
}

gsl_vector* flu_polygon(vector<string> figure, 
			gsl_matrix* gnd1_xy, 
			bool perform_scaling){
  if(figure.size() == 0)
    return NULL;
  
  /* compute the low-level features for each component*/
  gsl_vector* feats_fig;  
  gsl_vector* feats_fig_tmp = sfe_polygon_features(gnd1_xy);
  
  if(perform_scaling){

    vector<string> names_fig = sfe_polygon_names();
    gsl_vector* mask_fig = sfe_get_normalization_mask(names_fig);

    assert(mask_fig->size == feats_fig_tmp->size);
    feats_fig = sfe_normalize_feature_vector(NULL, gnd1_xy, mask_fig, feats_fig_tmp);

    gsl_vector_free(feats_fig_tmp);
    gsl_vector_free(mask_fig);
  }
  else{
    feats_fig=feats_fig_tmp;
  }
  int num_features = feats_fig->size*figure.size();
  gsl_vector* ret_vec = gsl_vector_calloc(num_features);
  
  size_t i;
  for(i=0; i<figure.size(); i++){
    gsl_vector_memcpy(&gsl_vector_subvector(ret_vec, feats_fig->size*i, feats_fig->size).vector, feats_fig);
  }
  
  gsl_vector_free(feats_fig);
  
  return ret_vec;
}

vector<string> flu_polygon_names(vector<string> figure)
{
  /* cross the words with the feature names */
  vector<string> names_fig = sfe_polygon_names();
  vector<string> f_names = crf_get_word_features(figure, names_fig, "f_");
  
  /* create the return feature names */
  vector<string> ret_names;
  
  for(unsigned int k=0; k<f_names.size(); k++)
    ret_names.push_back(f_names[k]);

  return ret_names;
}

gsl_vector* flu_polygon_polygon(vector<string> relation, 
		   	        gsl_matrix* figure_xy, 
				gsl_matrix* landmark_xy, 
				bool perform_scaling){
  if(relation.size() == 0)
    return NULL;
  
  /* compute the low-level features for each component*/
  gsl_vector* feats_relation;
  
  int num_features = feats_relation->size*relation.size();
  gsl_vector* ret_vec = gsl_vector_calloc(num_features);
  
  size_t i;  int curr_index = 0;
  for(i=0; i<relation.size(); i++){
    gsl_vector_memcpy(&gsl_vector_subvector(ret_vec, feats_relation->size*i+curr_index,
					    feats_relation->size).vector, feats_relation);
  }

  gsl_vector_free(feats_relation);
  
  return ret_vec;
}


vector<string> flu_polygon_polygon_names(vector<string> relation)
{
  /* cross the words with the feature names */
  vector<string> names_relation = sfe_f_polygon_l_polygon_names();
  vector<string> r_names = crf_get_word_features(relation, names_relation, "r_");

  /* create the return feature names */
  vector<string> ret_names;
  
  for(unsigned int k=0; k<r_names.size(); k++)
    ret_names.push_back(r_names[k]);

  return ret_names;
}
