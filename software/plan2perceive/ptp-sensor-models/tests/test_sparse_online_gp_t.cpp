
#include <ptp-sensor-models/sparse_online_gp_t.hpp>
#include <ptp-common-test/test.hpp>
#include <iostream>


using namespace ptp;
using namespace ptp::sensormodels;

//=======================================================================

int test_compile()
{
  start_of_test;

  // create a new gp
  sparse_gp_hyperparameters_t hyp;
  hyp.scale = 2;
  hyp.sigma = 1.2;
  hyp.capacity = 0;
  sparse_online_gp_t gp0( hyp );
  
  // add some data
  for( size_t i = 0; i < 10; ++ i ) {
    dist_and_orientation_t x( i, deg(0).from( angle_t::X_AXIS ) );
    detection_and_location_t dx( i, x );
    gp0.add( dx );
  }
  
  // predict something
  dist_and_orientation_t x0( 1.5, deg(0).from( angle_t::X_AXIS ) );
  mean_variance_t mv0 = gp0.predict( x0 );

  // print prediction
  std::cout << "Predicted, p(x) ~ N(" << mv0.mean << "," << mv0.var << ")" << std::endl;

  end_of_test;
}

//=======================================================================

int main( int argc, char** argv )
{
  run_test( test_compile() );

  return 0;
}

//=======================================================================
