
#include <ptp-sensor-models/sensor_models_t.hpp>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_randist.h>
#include <bot_core/rand_util.h>
#include <string>
#include <vector>
#include <set>
#include <fstream>
#include <iostream>
#include <sstream>


using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::sensormodels;


// Description:
// Function which loads a set of correlated labeled trainig data
// as a vector of detection and locations
std::vector<detection_and_location_t>
load_correlated_training_data_text()
{
  std::string filename = "/home/velezj/projects/object_detector_pose_planning/envoy/software/plan2perceive/training-data/master-logs/floor_3_and_1_lcmlog-2011-09-28.00-subset.master-log";

  // The known same object detection from the text master log
  int min_det_index = 590;
  int max_det_index = 781;
  
  // Load the file and read it line by line, creating a detection
  // out of those lines we want
  std::vector<detection_and_location_t> dets;
  std::ifstream fin( filename.c_str() );
  while( fin ) {
    int id;
    double det_value, x, y, w, h, dist, angle;
    int dummy, label;
    fin >> id >> det_value >> x >> y >> w >> h >> dist >> angle >> dummy >> label;

    if( id < min_det_index ||
	id > max_det_index )
      continue;
    
    // normalize the angle/dist
    dist = fabs( dist );
    while( angle > M_PI )
      angle -= M_PI;
    while( angle < M_PI )
      angle += M_PI;
    angle = fabs( angle );

    // create det angle
    dist_and_orientation_t dao( dist, rad(angle).from(angle_t::X_AXIS) );
    detection_and_location_t dx( det_value, dao );
    
    // add to vector
    dets.push_back( dx );
  }

  std::cout << " ..loaded " << dets.size() << " training data points" << std::endl;

  return dets;
}


// Description:
// Function which loads a set of correlated labeled trainig data
// as a vector of detection and locations
std::vector<detection_and_location_t>
load_correlated_training_data_door()
{
  std::string filename = "/home/velezj/projects/object_detector_pose_planning/envoy/software/plan2perceive/training-data/master-logs/doors/master_log.txt.updated_detscore.mapped";

  // The known same object detection from the text master log
  //int min_det_index = 590;
  //int max_det_index = 781;
  std::set<int> indices;
  indices.insert( 105 );
  indices.insert( 106 );
  indices.insert( 107 );
  indices.insert( 108 );
  indices.insert( 113 );
  indices.insert( 118 );
  indices.insert( 119 );
  indices.insert( 123 );
  indices.insert( 124 );
  indices.insert( 125 );
  indices.insert( 126 );
  indices.insert( 127 );
  indices.insert( 128 );
  indices.insert( 163 );
  indices.insert( 164 );  
  indices.insert( 165 );
  indices.insert( 467 );
  indices.insert( 468 );
  indices.insert( 469 );
  indices.insert( 470 );
  indices.insert( 471 );
  indices.insert( 472 );
  indices.insert( 473 );
  indices.insert( 474 );
  indices.insert( 475 );
  indices.insert( 475 );
  indices.insert( 476 );
  indices.insert( 477 );
  indices.insert( 478 );
  indices.insert( 479 );
  indices.insert( 480 );
  indices.insert( 481 );
  indices.insert( 482 );
  indices.insert( 483 );
  indices.insert( 484 );
  indices.insert( 485 );
  indices.insert( 486 );
  indices.insert( 487 );
  indices.insert( 809 );
  indices.insert( 810 );
  indices.insert( 815 );
  indices.insert( 816 );
  indices.insert( 817 );
  indices.insert( 820 );
  indices.insert( 821 );
  indices.insert( 822 );
  indices.insert( 826 );
  indices.insert( 827 );
  indices.insert( 828 );
  indices.insert( 1066 );
  indices.insert( 1067 );
  indices.insert( 1698 );
  indices.insert( 1699 );
  indices.insert( 1700 );
  indices.insert( 1701 );
  indices.insert( 1702 );
  indices.insert( 1703 );
  indices.insert( 1704 );
  indices.insert( 1705 );
  indices.insert( 1706 );
  indices.insert( 1707 );
  indices.insert( 1708 );

  // indices.insert( 166 );
  // indices.insert( 167 );
  // indices.insert( 168 );
  // indices.insert( 169 );
  // indices.insert( 189 );
  // indices.insert( 194 );
  // indices.insert( 197 );
  // indices.insert( 198 );
  // indices.insert( 199 );
  // indices.insert( 204 );
  // indices.insert( 205 );
  // indices.insert( 206 );
  // indices.insert( 207 );
  // indices.insert( 210 );
  // indices.insert( 212 );
  // indices.insert( 215 );
  // indices.insert( 264 );
  // indices.insert( 267 );
  // indices.insert( 268 );
  // indices.insert( 269 );
  // indices.insert( 271 );
  // indices.insert( 275 );
  // indices.insert( 278 );
  // indices.insert( 279 );
  // indices.insert( 282 );
  // indices.insert( 283 );
  // indices.insert( 284 );
  // indices.insert( 285 );
  // indices.insert( 286 );
  // indices.insert( 287 );
  // indices.insert( 288 );
  // indices.insert( 294 );
  // indices.insert( 295 );
  // indices.insert( 296 );
  // indices.insert( 443 );
  // indices.insert( 444 );
  // indices.insert( 445 );
  // indices.insert( 446 );
  // indices.insert( 447 );
  // indices.insert( 448 );
  // indices.insert( 449 );
  // indices.insert( 450 );
  // indices.insert( 451 );
  // indices.insert( 452 );
  // indices.insert( 453 );
  // indices.insert( 457 );
  // indices.insert( 458 );
  // indices.insert( 460 );
  // indices.insert( 461 );
  // indices.insert( 462 );
  // indices.insert( 463 );
  // indices.insert( 464 );
  // indices.insert( 465 );
  // indices.insert( 466 );

  // Load the file and read it line by line, creating a detection
  // out of those lines we want
  std::vector<detection_and_location_t> dets;
  std::ifstream fin( filename.c_str() );
  while( fin ) {
    int id;
    double det_value, x, y, w, h;
    std::string dist_str;
    double angle;
    double dummy;
    int label;
    fin >> id >> det_value >> x >> y >> w >> h >> dist_str >> angle >> dummy >> label;

    if( indices.find(id) == indices.end() ) {
      //std::cout << "   skipped '" << id << "' ..." << std::endl;
      continue;
    }

    // skip any Nan dist
    if( dist_str == "NaN" ) {
      //std::cout << "   skipped NAN '" << id << "' ..." << std::endl;
      continue;
    }
    
    double dist;
    std::istringstream iss( dist_str );
    iss >> dist;
    
    // normalize the angle/dist
    dist = fabs( dist );
    while( angle > M_PI )
      angle -= M_PI;
    while( angle < M_PI )
      angle += M_PI;
    angle = fabs( angle );

    // create det angle
    dist_and_orientation_t dao( dist, rad(angle).from(angle_t::X_AXIS) );
    detection_and_location_t dx( det_value, dao );
    
    // add to vector
    dets.push_back( dx );
  }

  std::cout << " ..loaded " << dets.size() << " training data points" << std::endl;

  return dets;
}


// Description:
// The parameter structure for the minimzer function
struct param_t
{
  std::vector<detection_and_location_t>* training_set;
  gsl_permutation* permutation;
  size_t hold_out_start_index;
};


// Description:
// INitialize the parameter structure given
std::vector<detection_and_location_t> _training_set;
gsl_permutation *_permutation = NULL;
size_t _hold_out_start_index;
param_t create_parameters_text()
{
  
  // Ok, load the training set
  if( _training_set.empty() )
    _training_set = load_correlated_training_data_text();
  
  // initialize the permutation'
  if( !_permutation ) { 
    _permutation = gsl_permutation_calloc( _training_set.size() );
  }
  
  // Initialzie the hold out index
  double hold_out_percent = 0.2;
  _hold_out_start_index = _training_set.size() * ( 1 - hold_out_percent );
  

  param_t params;
  params.training_set = &_training_set;
  params.permutation = _permutation;
  params.hold_out_start_index = _hold_out_start_index;
  
  return params;
}
param_t create_parameters_door()
{
  
  // Ok, load the training set
  if( _training_set.empty() )
    _training_set = load_correlated_training_data_door();
  
  // initialize the permutation'
  if( !_permutation ) { 
    _permutation = gsl_permutation_calloc( _training_set.size() );
  }
  
  // Initialzie the hold out index
  double hold_out_percent = 0.2;
  _hold_out_start_index = _training_set.size() * ( 1 - hold_out_percent );
  

  param_t params;
  params.training_set = &_training_set;
  params.permutation = _permutation;
  params.hold_out_start_index = _hold_out_start_index;
  
  return params;
}


// Description:
// This is the sunction we want to minimize
// It is in the GSL minimizer function format
// The parameters is the entire vector of correlated
// training data as a pointer.
// This will be permuted and then added to oiur sensor model
// The likelihood of all the data will then be 
double model_likelihood_function_text( const gsl_vector* v,
				       void* params )
{
  double b = gsl_vector_get( v, 0 );
  param_t *p = (param_t*)params;

  // ignore variables that are negative
  if( b < 0 )
    return 0; // this is the maximum of the function
  
  // Ok, create a sensor models
  sensor_models_t models;
  
  // Add all of the training set to the model in the given
  // permutation order
  for( int i = 0; i < p->hold_out_start_index; ++i ) {
    int index = gsl_permutation_get( p->permutation, i );
    detection_and_location_t dx = p->training_set->at( index );
    if( i == 0 ) {
      models.add_new_object( 1, dx );
      models.object(0)->param_b = b;
    } else {
      models.object(0)->update( dx );
    }
  }

  // Now, report the summed likelihood of the hold out set
  double lik_sum = 0;
  for( int i = p->hold_out_start_index; i < p->training_set->size(); ++i ) {
    int index = gsl_permutation_get( p->permutation, i );
    detection_and_location_t dx = p->training_set->at( index );
    mean_variance_t mv = models.object(0)->query_no_update( dx.location, 1 );
    double lik = gsl_ran_gaussian_pdf( dx.detection_value - mv.mean, mv.var );
    lik_sum += lik;
  }

  // go to the next permutation for the next call
  gsl_permutation_next( p->permutation );

  // return the negative sum likelihood as the thing to minimize
  return -lik_sum;
}
double model_likelihood_function_door( const gsl_vector* v,
				       void* params )
{
  double b = gsl_vector_get( v, 0 );
  param_t *p = (param_t*)params;

  // ignore variables that are negative
  if( b < 0 )
    return 0; // this is the maximum of the function
  
  // Ok, create a sensor models
  sensor_models_t models;
  
  // Add all of the training set to the model in the given
  // permutation order
  for( int i = 0; i < p->hold_out_start_index; ++i ) {
    int index = gsl_permutation_get( p->permutation, i );
    detection_and_location_t dx = p->training_set->at( index );
    if( i == 0 ) {
      models.add_new_object( 0, dx );
      models.object(0)->param_b = b;
    } else {
      models.object(0)->update( dx );
    }
  }

  // Now, report the summed likelihood of the hold out set
  double lik_sum = 0;
  for( int i = p->hold_out_start_index; i < p->training_set->size(); ++i ) {
    int index = gsl_permutation_get( p->permutation, i );
    detection_and_location_t dx = p->training_set->at( index );
    mean_variance_t mv = models.object(0)->query_no_update( dx.location, 1 );
    double lik = gsl_ran_gaussian_pdf( dx.detection_value - mv.mean, mv.var );
    lik_sum += lik;
  }

  // go to the next permutation for the next call
  gsl_permutation_next( p->permutation );

  // return the negative sum likelihood as the thing to minimize
  return -lik_sum;
}



int main( int argc, char** argv )
{
  std::cout << "starting system ...." << std::endl;

  // initialize the globals to something sane
  _permutation = NULL;


  // { // TEXT
    
  //   if( _permutation ) {
  //     gsl_permutation_free( _permutation );
  //     _permutation = NULL;
  //   }

  //   // Ok, first creat an output file
  //   std::ofstream fout( "text_learned_mixture.ssv" );
    
  //   // keep track of the smallest and largest var found in 
  //   // minimizations
  //   double min_var = 0.01;
  //   double max_var = 100;
    
  //   // keep track of minimzation value found
  //   double min_min = std::numeric_limits<double>::infinity();
  //   double max_min = -std::numeric_limits<double>::infinity();
    
  //   // Start the loop
  //   size_t num_minimizations = 0;
  //   while( num_minimizations < 300 ) {
      
  //     std::cout << "TEXT minimization # " << num_minimizations << "  var range=(" << min_var << "," << max_var << "), min range=(" << min_min << "," << max_min << ")" << std::endl;
      
  //     ++num_minimizations;
      
  //     // Now create the GSL minimizer
  //     const gsl_multimin_fminimizer_type *T = 
  // 	gsl_multimin_fminimizer_nmsimplex2;
  //     gsl_multimin_fminimizer *s = NULL;
  //     gsl_vector *ss, *x;
  //     gsl_multimin_function minex_func;
      
  //     int iter = 0;
  //     int status;
  //     double size;
  //     double last_var = -1;
  //     double last_min = 1;
  //     double last_size = 0;
      
  //     /* Starting point */
  //     double starting_var = min_var + ( max_var - min_var ) * bot_randf();
  //     x = gsl_vector_alloc (1);
  //     gsl_vector_set (x, 0, starting_var);
  //     std::cout << "   starting variable: " << starting_var << std::endl;
      
  //     /* Set initial step sizes to 1 */
  //     ss = gsl_vector_alloc (1);
  //     gsl_vector_set_all (ss, 1.0);
      
  //     /* Initialize method and iterate */
  //     param_t params = create_parameters_text();
  //     minex_func.n = 1;
  //     minex_func.f = &model_likelihood_function_text;
  //     minex_func.params = (void*)&params;
      
  //     s = gsl_multimin_fminimizer_alloc (T, 1);
  //     gsl_multimin_fminimizer_set (s, &minex_func, x, ss);
      
  //     do
  // 	{
  // 	  iter++;
  // 	  status = gsl_multimin_fminimizer_iterate(s);
	  
  // 	  if (status) 
  // 	    break;
	  
  // 	  size = gsl_multimin_fminimizer_size (s);
  // 	  status = gsl_multimin_test_size (size, 1e-5);
	  
  // 	  if (status == GSL_SUCCESS)
  // 	    {
  // 	      double min_val = s->fval;
  // 	      double min_size = size;
  // 	      double var = gsl_vector_get( s->x, 0 );
	      
  // 	      std::cout << "  ** MIN lik=" << min_val << ", var=" << var << "  (size=" << size << std::endl;
	      
  // 	      fout << "1" << " " << num_minimizations << " " << "1" << " " << starting_var << " " << min_val << " " << var << " " << size << " " << iter << std::endl;
	      
  // 	      // update var range and min range
  // 	      if( var < min_var )
  // 		min_var = var;
  // 	      if( var > max_var )
  // 		max_var = var;
  // 	      if( min_var < 0 )
  // 		min_var = 0;
  // 	      if( max_var < 0 )
  // 		max_var = 0;
  // 	      if( min_val < min_min )
  // 		min_min = min_val;
  // 	      if( min_val > max_min )
  // 		max_min = min_val;
	      
  // 	    }
	  
	  
  // 	  last_var = gsl_vector_get( s->x, 0 );
  // 	  last_min = s->fval;
  // 	  last_size = size;
	  
  // 	  if( iter % 5 == 0 ) {
  // 	    printf ("        %5d Var: %8.4f f() = %7.3f size = %.3f\n", 
  // 		    iter,
  // 		    gsl_vector_get (s->x, 0), 
  // 		    s->fval, size);
  // 	  }
	  
  // 	}
  //     while (status == GSL_CONTINUE && iter < 3000);
      
  //     if( status != GSL_SUCCESS ) {
  // 	std::cout << "   ## var=" << last_var << ", min=" << last_min << "  broken minimization, status = " << status << std::endl;
  // 	fout  << "1" << " " << num_minimizations << " " << "0" << " " << starting_var << " " << last_min << " " << last_var << " " << last_size << " " << iter << std::endl;
	
  //     }
      
  //     gsl_vector_free(x);
  //     gsl_vector_free(ss);
  //     gsl_multimin_fminimizer_free (s);

  //   }

  // }


  //=================== DOOR ====================================


  { // DOOR

    // clear parmaters
    _training_set.clear();

    if( _permutation ) {
      gsl_permutation_free( _permutation );
      _permutation = NULL;
    }

    // Ok, first creat an output file
    std::ofstream fout( "door_learned_mixture.ssv" );
    
    // keep track of the smallest and largest var found in 
    // minimizations
    double min_var = 0.01;
    double max_var = 100;
    
    // keep track of minimzation value found
    double min_min = std::numeric_limits<double>::infinity();
    double max_min = -std::numeric_limits<double>::infinity();
    
    // Start the loop
    size_t num_minimizations = 0;
    while( num_minimizations < 300 ) {
      
      std::cout << "DOOR minimization # " << num_minimizations << "  var range=(" << min_var << "," << max_var << "), min range=(" << min_min << "," << max_min << ")" << std::endl;
      
      ++num_minimizations;
      
      // Now create the GSL minimizer
      const gsl_multimin_fminimizer_type *T = 
	gsl_multimin_fminimizer_nmsimplex2;
      gsl_multimin_fminimizer *s = NULL;
      gsl_vector *ss, *x;
      gsl_multimin_function minex_func;
      
      int iter = 0;
      int status;
      double size;
      double last_var = -1;
      double last_min = 1;
      double last_size = 0;
      
      /* Starting point */
      double starting_var = min_var + ( max_var - min_var ) * bot_randf();
      x = gsl_vector_alloc (1);
      gsl_vector_set (x, 0, starting_var);
      std::cout << "   starting variable: " << starting_var << std::endl;
      
      /* Set initial step sizes to 1 */
      ss = gsl_vector_alloc (1);
      gsl_vector_set_all (ss, 1.0);
      
      /* Initialize method and iterate */
      param_t params = create_parameters_door();
      minex_func.n = 1;
      minex_func.f = &model_likelihood_function_door;
      minex_func.params = (void*)&params;
      
      s = gsl_multimin_fminimizer_alloc (T, 1);
      gsl_multimin_fminimizer_set (s, &minex_func, x, ss);
      
      do
	{
	  iter++;
	  status = gsl_multimin_fminimizer_iterate(s);
	  
	  if (status) 
	    break;
	  
	  size = gsl_multimin_fminimizer_size (s);
	  status = gsl_multimin_test_size (size, 1e-5);
	  
	  if (status == GSL_SUCCESS)
	    {
	      double min_val = s->fval;
	      double min_size = size;
	      double var = gsl_vector_get( s->x, 0 );
	      
	      std::cout << "  ** MIN lik=" << min_val << ", var=" << var << "  (size=" << size << std::endl;
	      
	      fout << "0" << " " << num_minimizations << " " << "1" << " " << starting_var << " " << min_val << " " << var << " " << size << " " << iter << std::endl;
	      
	      // update var range and min range
	      if( var < min_var )
		min_var = var;
	      if( var > max_var )
		max_var = var;
	      if( min_var < 0 )
		min_var = 0;
	      if( max_var < 0 )
		max_var = 0;
	      if( min_val < min_min )
		min_min = min_val;
	      if( min_val > max_min )
		max_min = min_val;
	      
	    }
	  
	  
	  last_var = gsl_vector_get( s->x, 0 );
	  last_min = s->fval;
	  last_size = size;
	  
	  if( iter % 5 == 0 ) {
	    printf ("        %5d Var: %8.4f f() = %7.3f size = %.3f\n", 
		    iter,
		    gsl_vector_get (s->x, 0), 
		    s->fval, size);
	  }
	  
	}
      while (status == GSL_CONTINUE && iter < 3000);
      
      if( status != GSL_SUCCESS ) {
	std::cout << "   ## var=" << last_var << ", min=" << last_min << "  broken minimization, status = " << status << std::endl;
	fout  << "0" << " " << num_minimizations << " " << "0" << " " << starting_var << " " << last_min << " " << last_var << " " << last_size << " " << iter << std::endl;
	
      }
      
      gsl_vector_free(x);
      gsl_vector_free(ss);
      gsl_multimin_fminimizer_free (s);

      // clear parmaters
      _training_set.clear();
    }

  }


}
