

#include "sparse_online_gp_t.hpp"


using namespace ptp;
using namespace ptp::sensormodels;
using namespace sogp;


//========================================================

sparse_online_gp_t::sparse_online_gp_t( const sparse_online_gp_t& a )
{
  if( a._sogp ) {
    _sogp = boost::shared_ptr<SOGP>( new SOGP( a._sogp.get() ) );
  }
}

//========================================================

sparse_online_gp_t::sparse_online_gp_t( const sparse_gp_hyperparameters_t& hyp )
{
  // creat an RDFKernel structure and parametrs, then acreate
  // the internal SOGP gp object.
  RBFKernel kern(hyp.sigma);
  kern.setA( hyp.scale );
  SOGPParams params(&kern);
  //params.s20=hyp.sigma;
  params.capacity=hyp.capacity;
  _sogp = boost::shared_ptr<SOGP>( new SOGP( params ) );
}

//========================================================

int sparse_online_gp_t::size() const
{
  if( _sogp )
    return _sogp->size();
  return -1;
}

//========================================================

mean_variance_t sparse_online_gp_t::predict( const dist_and_orientation_t& x ) const
{
  // special case if no data
  if( _sogp->size() < 1 ) {
    mean_variance_t special_mv;
    special_mv.mean = 0;
    special_mv.var = 0.0000001;
    return special_mv;
  }


  // create a matrix from the giuven x
  sogp::ColumnVector input( 2 );
  input( 1 ) = x.dist;
  input( 2 ) = x.orientation.from( angle_t::X_AXIS ).radians();
  
  // Get back the prediction from the internal GP
  double prediction_sigma;
  sogp::Matrix prediction_mean = _sogp->predict( input, prediction_sigma );
  
  // Convert from row vector to a mean for us
  mean_variance_t res;
  res.mean = prediction_mean( 1,1 );
  res.var = prediction_sigma;
  
  return res;
}

//========================================================

void sparse_online_gp_t::add( const detection_and_location_t& dx )
{
  // Create a column vector for both the location and the detection
  sogp::ColumnVector input( 2 ), output( 1 );
  input(1) = dx.location.dist;
  input(2) = dx.location.orientation.from( angle_t::X_AXIS ).radians();
  output(1) = dx.detection_value;
  
  // now add to our internal GP
  _sogp->add( input, output );
}

//========================================================
//========================================================
