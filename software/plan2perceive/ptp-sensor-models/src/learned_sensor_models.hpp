
#if !defined( __PTP_SENSORMODELS_learned_sensor_models_HPP__ )
#define __PTP_SENSORMODELS_learned_sensor_models_HPP__

#include "sensor_model_t.hpp"

namespace ptp {
  namespace sensormodels {


    //================================================================
    
    // Description:
    // This is a static class whci h allows access to the traned and learned
    // sensor models in the system.
    // The basic call is the static create_new_sensor_model which returns
    // a sensor_model_tp with the data for the given object type
    // specified.
    class learned_sensor_models
    {
    public:

      // Description:
      // Returns a new sensor model object for the given object type.
      // This will have the training hyperparameters for the given type.
      static sensor_model_tp create_new_sensor_model( const int& object_type );
    };

    //================================================================

  }
}

#endif

