
#if !defined( __PTP_SENSORMODELS_sparse_online_gp_HPP__ )
#define __PTP_SENSORMODELS_sparse_online_gp_HPP__

#include "sensor_model_t.hpp"
#include <sogp/SOGP.h>

namespace ptp {
  namespace sensormodels {


    //=================================================================

    // Description:
    // The hyperparameters for our GP
    struct sparse_gp_hyperparameters_t
    {
      float scale;
      float sigma;
      size_t capacity; // the max number of data to hold (hence the 'sparse')
    };

    //=================================================================

    // Description:
    // A Sparse Gaussian Process.
    // This code wraps up a library in a useful way for our
    // sensor models
    class sparse_online_gp_t
    {
    public:

      // Description:
      // Creates a new gp with no parameter.
      // this is really here but SHOULD NOT BE USED
      sparse_online_gp_t() { }

      // Desccription:
      // Clonning copy constructor which COPIES the internal
      // GP ( so it will be a DIFFERENT POINTET )
      sparse_online_gp_t( const sparse_online_gp_t& a );


      // Description:
      // Creates a new gp with teh given hyperparameters
      sparse_online_gp_t( const sparse_gp_hyperparameters_t& p );

      // Description:
      // Gets the prediction for a particular location
      mean_variance_t predict( const dist_and_orientation_t& x ) const;
      
      // Description:
      // Adds the given y=x training pair to this GP
      void add( const detection_and_location_t& dx );

      // Description:
      // Returns the size of this GP ( whic his the number of data used! )
      int size() const;

    protected:

      // Description:
      // The underlying sparse GP
      // This is mutable since our internal GP does not have const methods.
      mutable boost::shared_ptr<sogp::SOGP> _sogp;
      
    };
    

    //=================================================================

  }
}

#endif

