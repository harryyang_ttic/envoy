
#include "sensor_models_t.hpp"
#include "learned_sensor_models.hpp"


using namespace ptp;
using namespace ptp::sensormodels;


//========================================================================

sensor_model_tp sensor_models_t::object( const int& object_id ) const
{
  return _models[ object_id ];
}

//========================================================================

int sensor_models_t::add_new_object( const int& object_type_id,
				     const detection_and_location_t& dx )
{
  
  // Ok, first create a new sensor model of the correct type
  sensor_model_tp model 
    = learned_sensor_models::create_new_sensor_model( object_type_id );
  
  // Now update the model wit hteh detection
  model->update( dx );
  
  // Store the model and return it's index as teh id
  _models.push_back( model );
  
  return _models.size() - 1;
}

//========================================================================

sensor_models_t sensor_models_t::clone() const
{
  // just copy over the models
  sensor_models_t copy;
  for( size_t i = 0; i < _models.size(); ++i ) {
    copy._models.push_back( _models[i]->clone() );
  }
  
  return copy;
}

//========================================================================
//========================================================================
//========================================================================
//========================================================================
//========================================================================
//========================================================================
//========================================================================
