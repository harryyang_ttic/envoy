
#include "sensor_model_t.hpp"

namespace ptp {
  namespace sensormodels {

    std::ostream& operator<< ( std::ostream& os, const mean_variance_t& mv )
    {
      os << "N(" << mv.mean << "," << mv.var << ")";
      return os;
    }

  }
}
