
// THIS FILE WILL BE AUTO GENERATED, THIS IS A DU<MY TEMPLATE

#include <vector>
#include "../sensor_model_t.hpp"
#include "../sparse_online_gp_t.hpp"
#include <iostream>
#include <sstream>
#include <fstream>

using namespace ptp;
using namespace ptp::sensormodels;
using namespace ptp::common;


// A vector of the known trained models, indexed by type
std::vector< sensor_model_tp > TRAINED_base_sensor_models;


// The trained sensor models, each subclassed from base sensor_model_t
// and each having the data in this file.


class text_sensor_t 
  : public sensor_model_t
{
public:

  text_sensor_t() 
  {
    param_b = 5.4;
#include "text_model_constructor.cpp"

    std::cout << "Sensor model: GP_IND0(" << gp_ind0.size() 
	      << ") GP_IND1(" << gp_ind1.size() 
	      << ") GP_CORR0(" << gp_corr0.size()
	      << ") GP_CORR1(" << gp_corr1.size() << ")" << std::endl;
  }
 

  mean_variance_t query_no_update( const dist_and_orientation_t& x, const float& object_exists_p ) const
  {
    mean_variance_t mv_ind0 = gp_ind0.predict( x );
    mean_variance_t mv_ind1 = gp_ind1.predict( x );
    mean_variance_t mv_corr0 = gp_corr0.predict( x );
    mean_variance_t mv_corr1 = gp_corr1.predict( x );
    float y = object_exists_p;
    if( y < 0 )
      y = 0;
    if( y > 1 )
      y = 1;
    float corr_sigma_2 = y*y*mv_corr1.var*mv_corr1.var + (1-y)*(1-y)*mv_corr0.var*mv_corr0.var;
    float a = 1 - exp( -corr_sigma_2 * param_b );
    
    mean_variance_t mv;
    mv.mean = a*( y*mv_ind1.mean + (1-y)*mv_ind0.mean ) + (1-a)*( y*mv_corr1.mean + (1-y)*mv_corr0.mean );
    float ind_sigma_2 = y*y*mv_ind1.var*mv_ind1.var + (1-y)*(1-y)*mv_ind0.var*mv_ind0.var;
    mv.var = sqrt( a*a*ind_sigma_2 + (1-a)*(1-a)*corr_sigma_2 );
    
    return mv;
  }


  void update( const detection_and_location_t& dx )
  {
    // only update the correlation model
    gp_corr0.add( dx );
    gp_corr1.add( dx );
  }


math_vector_t gradient_of_theta_about( const float& state,
				       const dist_and_orientation_t& x,
				       const bool& use_independent_model_only) const
  {
    float y = state;
    if( y < 0 )
      y = 0;
    if( y > 1 )
      y = 1;
    
    mean_variance_t mv_ind0 = gp_ind0.predict( x );
    mean_variance_t mv_ind1 = gp_ind1.predict( x );
    mean_variance_t mv_corr0 = gp_corr0.predict( x );
    mean_variance_t mv_corr1 = gp_corr1.predict( x );
    

    float mu_ind0 = mv_ind0.mean;
    float mu_ind1 = mv_ind1.mean;
    float mu_corr0 = mv_corr0.mean;
    float mu_corr1 = mv_corr1.mean;
    float s_ind0 = mv_ind0.var;
    float s_ind1 = mv_ind1.var;
    float s_corr0 = mv_corr0.var;
    float s_corr1 = mv_corr1.var;
    float corr_sigma_2 = y*y*mv_corr1.var*mv_corr1.var + (1-y)*(1-y)*mv_corr0.var*mv_corr0.var;
    float a = 1- exp( -corr_sigma_2 * param_b );

    if( use_independent_model_only ) {
      a = 1;
    }
    
    // Ok, here's the derivative according to mathematica
    double dtheta1 =
      ((1 - a)*((mu_corr0) - (mu_corr1)) + a*((mu_ind0) - (mu_ind1))) /
      ( pow(a,2) *pow( pow(s_ind0,2)*pow(y , 2) + pow(s_ind1, 2)*pow(1 - y , 2), 2) +
	pow(1 - a, 2)*pow( pow(s_corr0, 2)*pow(y , 2) + pow(s_corr1, 2)*pow(1 - y ,2), 2)) -
      (((1 - a)*((mu_corr0)*y + (mu_corr1)*(1 - y )) +
	a*((mu_ind0)*y + (mu_ind1)*(1 - y )))*
       (2*pow(a , 2)*(2*pow(s_ind0 , 2)*y - 2*pow(s_ind1, 2)*(1 - y ))*
	(pow(s_ind0, 2)*pow(y , 2) + pow(s_ind1, 2)*pow(1 - y , 2)) +
	2*pow(1 - a, 2)*(2*pow(s_corr0, 2)*y - 2*pow(s_corr1, 2)*(1 - y ))*
	(pow(s_corr0, 2)*pow(y , 2) + pow(s_corr1, 2)*pow(1 - y , 2)))) / 
      pow(pow(a , 2)*pow(pow(s_ind0, 2)*pow(y , 2) + pow(s_ind1, 2)*pow(1 - y , 2), 2) +
	  pow(1 - a, 2)*pow(pow(s_corr0, 2)*pow(y , 2) + pow(s_corr1, 2)*pow(1 - y , 2), 2), 2);

    double dtheta2 =
      (2*pow(1 - a, 2)*(2*y *pow(s_corr0, 2) - 2*(1 - y )*pow(s_corr1, 2))*
       (pow(y , 2)*pow(s_corr0, 2) + pow(1 - y , 2)*pow(s_corr1, 2)) +
       2*pow(a , 2)*(2*y *pow(s_ind0, 2) - 2*(1 - y )*pow(s_ind1, 2))*
       (pow(y , 2)*pow(s_ind0, 2) + pow(1 - y , 2)*pow(s_ind1,2))) / 
      (2*pow(pow(1 - a, 2)*pow(pow(y , 2)*pow(s_corr0, 2) + pow(1 - y , 2)*pow(s_corr1, 2 ) , 2) +
	     pow(a , 2)*pow(pow(y , 2)*pow(s_ind0, 2) + pow(1 - y , 2)*pow(s_ind1, 2), 2) ,2));
      
 
    math_vector_t d(2);
    d(0) = dtheta1;
    d(1) = dtheta2;

    return d;
  }

  void query_separate( const dist_and_orientation_t& x, const float& exists, mean_variance_t& mv_ind, mean_variance_t& mv_corr, float& prob_indep ) const
  {
    mean_variance_t mv_ind0 = gp_ind0.predict( x );
    mean_variance_t mv_ind1 = gp_ind1.predict( x );
    mean_variance_t mv_corr0 = gp_corr0.predict( x );
    mean_variance_t mv_corr1 = gp_corr1.predict( x );
    float y = exists;
    if( y < 0 )
      y = 0;
    if( y > 1 )
      y = 1;
    float corr_sigma_2 = y*y*mv_corr1.var*mv_corr1.var + (1-y)*(1-y)*mv_corr0.var*mv_corr0.var;
    float a = 1 - exp( -corr_sigma_2 * param_b );
    prob_indep = a;
    
    mv_corr.mean = y*mv_corr1.mean + (1-y)*mv_corr0.mean;
    mv_corr.var = sqrt(corr_sigma_2);

    float ind_sigma_2 = y*y*mv_ind1.var*mv_ind1.var + (1-y)*(1-y)*mv_ind0.var*mv_ind0.var;
    mv_ind.mean = y*mv_ind1.mean + (1-y)*mv_ind0.mean;
    mv_ind.var = sqrt( ind_sigma_2 );
  }


  text_sensor_t( const text_sensor_t* a )
    : gp_ind0( a->gp_ind0 ),
      gp_ind1( a->gp_ind1 ),
      gp_corr0( a->gp_corr0 ),
      gp_corr1( a->gp_corr1 )
  { param_b = a->param_b; }


  sensor_model_tp clone() const
  {
    boost::shared_ptr<text_sensor_t> c( new text_sensor_t( this ) );
    return c;
  }

  
  void dump_gps( const std::string& prefix ) const
  {
    float min_x = 0;
    float max_x = 6;
    float min_y = 0;
    float max_y = 6;
    size_t num_x = 60;
    size_t num_y = 60;
    float step_x = (max_x - min_x) / num_x;
    float step_y = (max_y - min_y) / num_y;
    dist_and_orientation_t dao( 0, rad(0).from(angle_t::X_AXIS) );
    //std::cout << "querying field..." << std::endl;
    
    // the ind0 model
    std::cout << "ind0 model size= " << gp_ind0.size() << std::endl;
    matrix_t field( num_y, num_x );
    for( size_t i = 0; i < num_y; ++i ) {
      for( size_t j = 0; j < num_x; ++j ) {
	float x = j*step_x;
	float y = i*step_y;
	dao.dist = sqrt( x*x + y*y );
	float r = atan2( y, x );
	dao.orientation = rad(r).from(angle_t::X_AXIS);
	mean_variance_t mv = gp_ind0.predict( dao );
	field( i, j ) = mv.mean;
      }
    }
    std::ostringstream oss;
    oss << prefix << "gp_ind0.ssv";
    std::ofstream fout( oss.str().c_str() );
    fout << field;
    fout.close();
    std::cout << "dumped '" << oss.str() << "'" << std::endl;
    
    // the ind1 model
    //matrix_t field( num_y, num_x );
    std::cout << "ind1 model size= " << gp_ind1.size() << std::endl;
    for( size_t i = 0; i < num_y; ++i ) {
      for( size_t j = 0; j < num_x; ++j ) {
	float x = j*step_x;
	float y = i*step_y;
	dao.dist = sqrt( x*x + y*y );
	float r = atan2( y, x );
	dao.orientation = rad(r).from(angle_t::X_AXIS);
	mean_variance_t mv = gp_ind1.predict( dao );
	field( i, j ) = mv.mean;
      }
    }
    oss.str("");
    oss << prefix << "gp_ind1.ssv";
    fout.open( oss.str().c_str() );
    fout << field;
    fout.close();
    std::cout << "dumped '" << oss.str() << "'" << std::endl;
    

    // the corr0 model
    //matrix_t field( num_y, num_x );
    std::cout << "corr0 model size= " << gp_corr0.size() << std::endl;
    for( size_t i = 0; i < num_y; ++i ) {
      for( size_t j = 0; j < num_x; ++j ) {
	float x = j*step_x;
	float y = i*step_y;
	dao.dist = sqrt( x*x + y*y );
	float r = atan2( y, x );
	dao.orientation = rad(r).from(angle_t::X_AXIS);
	mean_variance_t mv = gp_corr0.predict( dao );
	field( i, j ) = mv.mean;
      }
    }
    oss.str("");
    oss << prefix << "gp_corr0.ssv";
    fout.open( oss.str().c_str() );
    fout << field;
    fout.close();
    std::cout << "dumped '" << oss.str() << "'" << std::endl;
    
    // the corr1 model
    //matrix_t field( num_y, num_x );
    std::cout << "corr1 model size= " << gp_corr1.size() << std::endl;
    for( size_t i = 0; i < num_y; ++i ) {
      for( size_t j = 0; j < num_x; ++j ) {
	float x = j*step_x;
	float y = i*step_y;
	dao.dist = sqrt( x*x + y*y );
	float r = atan2( y, x );
	dao.orientation = rad(r).from(angle_t::X_AXIS);
	mean_variance_t mv = gp_corr1.predict( dao );
	field( i, j ) = mv.mean;
      }
    }
    oss.str("");
    oss << prefix << "gp_corr1.ssv";
    fout.open( oss.str().c_str() );
    fout << field;
    fout.close();
    std::cout << "dumped '" << oss.str() << "'" << std::endl;
    
  }


private:
  sparse_online_gp_t gp_ind0;
  sparse_online_gp_t gp_ind1;
  sparse_online_gp_t gp_corr0;
  sparse_online_gp_t gp_corr1;
};



class door_sensor_t 
  : public sensor_model_t
{
public:

  door_sensor_t() 
  {
    param_b = 6.1;
#include "door_model_constructor.cpp"

    std::cout << "Door Sensor model: GP_IND0(" << gp_ind0.size() 
	      << ") GP_IND1(" << gp_ind1.size() 
	      << ") GP_CORR0(" << gp_corr0.size()
	      << ") GP_CORR1(" << gp_corr1.size() << ")" << std::endl;
  }
 

  mean_variance_t query_no_update( const dist_and_orientation_t& x, const float& object_exists_p ) const
  {
    mean_variance_t mv_ind0 = gp_ind0.predict( x );
    mean_variance_t mv_ind1 = gp_ind1.predict( x );
    mean_variance_t mv_corr0 = gp_corr0.predict( x );
    mean_variance_t mv_corr1 = gp_corr1.predict( x );
    float y = object_exists_p;
    if( y < 0 )
      y = 0;
    if( y > 1 )
      y = 1;
    float corr_sigma_2 = y*y*mv_corr1.var*mv_corr1.var + (1-y)*(1-y)*mv_corr0.var*mv_corr0.var;
    float a = 1 - exp( -corr_sigma_2 * param_b );
    
    mean_variance_t mv;
    mv.mean = a*( y*mv_ind1.mean + (1-y)*mv_ind0.mean ) + (1-a)*( y*mv_corr1.mean + (1-y)*mv_corr0.mean );
    float ind_sigma_2 = y*y*mv_ind1.var*mv_ind1.var + (1-y)*(1-y)*mv_ind0.var*mv_ind0.var;
    mv.var = sqrt( a*a*ind_sigma_2 + (1-a)*(1-a)*corr_sigma_2 );
    
    return mv;
  }


  void update( const detection_and_location_t& dx )
  {
    // only update the correlation model
    gp_corr0.add( dx );
    gp_corr1.add( dx );
  }


math_vector_t gradient_of_theta_about( const float& state,
				       const dist_and_orientation_t& x,
				       const bool& use_independent_model_only) const
  {
    float y = state;
    if( y < 0 )
      y = 0;
    if( y > 1 )
      y = 1;
    
    mean_variance_t mv_ind0 = gp_ind0.predict( x );
    mean_variance_t mv_ind1 = gp_ind1.predict( x );
    mean_variance_t mv_corr0 = gp_corr0.predict( x );
    mean_variance_t mv_corr1 = gp_corr1.predict( x );
    

    float mu_ind0 = mv_ind0.mean;
    float mu_ind1 = mv_ind1.mean;
    float mu_corr0 = mv_corr0.mean;
    float mu_corr1 = mv_corr1.mean;
    float s_ind0 = mv_ind0.var;
    float s_ind1 = mv_ind1.var;
    float s_corr0 = mv_corr0.var;
    float s_corr1 = mv_corr1.var;
    float corr_sigma_2 = y*y*mv_corr1.var*mv_corr1.var + (1-y)*(1-y)*mv_corr0.var*mv_corr0.var;
    float a = 1- exp( -corr_sigma_2 * param_b );

    if( use_independent_model_only ) {
      a = 1;
    }
    
    // Ok, here's the derivative according to mathematica
    double dtheta1 =
      ((1 - a)*((mu_corr0) - (mu_corr1)) + a*((mu_ind0) - (mu_ind1))) /
      ( pow(a,2) *pow( pow(s_ind0,2)*pow(y , 2) + pow(s_ind1, 2)*pow(1 - y , 2), 2) +
	pow(1 - a, 2)*pow( pow(s_corr0, 2)*pow(y , 2) + pow(s_corr1, 2)*pow(1 - y ,2), 2)) -
      (((1 - a)*((mu_corr0)*y + (mu_corr1)*(1 - y )) +
	a*((mu_ind0)*y + (mu_ind1)*(1 - y )))*
       (2*pow(a , 2)*(2*pow(s_ind0 , 2)*y - 2*pow(s_ind1, 2)*(1 - y ))*
	(pow(s_ind0, 2)*pow(y , 2) + pow(s_ind1, 2)*pow(1 - y , 2)) +
	2*pow(1 - a, 2)*(2*pow(s_corr0, 2)*y - 2*pow(s_corr1, 2)*(1 - y ))*
	(pow(s_corr0, 2)*pow(y , 2) + pow(s_corr1, 2)*pow(1 - y , 2)))) / 
      pow(pow(a , 2)*pow(pow(s_ind0, 2)*pow(y , 2) + pow(s_ind1, 2)*pow(1 - y , 2), 2) +
	  pow(1 - a, 2)*pow(pow(s_corr0, 2)*pow(y , 2) + pow(s_corr1, 2)*pow(1 - y , 2), 2), 2);

    double dtheta2 =
      (2*pow(1 - a, 2)*(2*y *pow(s_corr0, 2) - 2*(1 - y )*pow(s_corr1, 2))*
       (pow(y , 2)*pow(s_corr0, 2) + pow(1 - y , 2)*pow(s_corr1, 2)) +
       2*pow(a , 2)*(2*y *pow(s_ind0, 2) - 2*(1 - y )*pow(s_ind1, 2))*
       (pow(y , 2)*pow(s_ind0, 2) + pow(1 - y , 2)*pow(s_ind1,2))) / 
      (2*pow(pow(1 - a, 2)*pow(pow(y , 2)*pow(s_corr0, 2) + pow(1 - y , 2)*pow(s_corr1, 2 ) , 2) +
	     pow(a , 2)*pow(pow(y , 2)*pow(s_ind0, 2) + pow(1 - y , 2)*pow(s_ind1, 2), 2) ,2));
      
 
    math_vector_t d(2);
    d(0) = dtheta1;
    d(1) = dtheta2;

    return d;
  }

  void query_separate( const dist_and_orientation_t& x, const float& exists, mean_variance_t& mv_ind, mean_variance_t& mv_corr, float& prob_indep ) const
  {
    mean_variance_t mv_ind0 = gp_ind0.predict( x );
    mean_variance_t mv_ind1 = gp_ind1.predict( x );
    mean_variance_t mv_corr0 = gp_corr0.predict( x );
    mean_variance_t mv_corr1 = gp_corr1.predict( x );
    float y = exists;
    if( y < 0 )
      y = 0;
    if( y > 1 )
      y = 1;
    float corr_sigma_2 = y*y*mv_corr1.var*mv_corr1.var + (1-y)*(1-y)*mv_corr0.var*mv_corr0.var;
    float a = 1 - exp( -corr_sigma_2 * param_b );
    prob_indep = a;
    
    mv_corr.mean = y*mv_corr1.mean + (1-y)*mv_corr0.mean;
    mv_corr.var = sqrt(corr_sigma_2);

    float ind_sigma_2 = y*y*mv_ind1.var*mv_ind1.var + (1-y)*(1-y)*mv_ind0.var*mv_ind0.var;
    mv_ind.mean = y*mv_ind1.mean + (1-y)*mv_ind0.mean;
    mv_ind.var = sqrt( ind_sigma_2 );
  }


  door_sensor_t( const door_sensor_t* a )
    : gp_ind0( a->gp_ind0 ),
      gp_ind1( a->gp_ind1 ),
      gp_corr0( a->gp_corr0 ),
      gp_corr1( a->gp_corr1 )
  { param_b = a->param_b; }


  sensor_model_tp clone() const
  {
    boost::shared_ptr<door_sensor_t> c( new door_sensor_t( this ) );
    return c;
  }

  
  void dump_gps( const std::string& prefix ) const
  {
    float min_x = 0;
    float max_x = 6;
    float min_y = 0;
    float max_y = 10;
    size_t num_x = 100;
    size_t num_y = 100;
    float step_x = (max_x - min_x) / num_x;
    float step_y = (max_y - min_y) / num_y;
    dist_and_orientation_t dao( 0, rad(0).from(angle_t::X_AXIS) );
    //std::cout << "querying field..." << std::endl;
    
    // the ind0 model
    std::cout << "ind0 model size= " << gp_ind0.size() << std::endl;
    matrix_t field( num_y, num_x );
    matrix_t var( num_y, num_x );
    for( size_t i = 0; i < num_y; ++i ) {
      for( size_t j = 0; j < num_x; ++j ) {
	float x = j*step_x;
	float y = i*step_y;
	dao.dist = sqrt( x*x + y*y );
	float r = atan2( y, x );
	dao.orientation = rad(r).from(angle_t::X_AXIS);
	mean_variance_t mv = gp_ind0.predict( dao );
	field( i, j ) = mv.mean;
	var( i, j ) = mv.var;
      }
    }
    std::ostringstream oss;
    oss << prefix << "gp_ind0.ssv";
    std::ofstream fout( oss.str().c_str() );
    fout << field;
    fout.close();
    oss.str("");
    oss << prefix << "gp_ind0_var.ssv";
    fout.open( oss.str().c_str() );
    fout << var;
    fout.close();
    std::cout << "dumped '" << oss.str() << "'" << std::endl;
    
    // the ind1 model
    //matrix_t field( num_y, num_x );
    std::cout << "ind1 model size= " << gp_ind1.size() << std::endl;
    for( size_t i = 0; i < num_y; ++i ) {
      for( size_t j = 0; j < num_x; ++j ) {
	float x = j*step_x;
	float y = i*step_y;
	dao.dist = sqrt( x*x + y*y );
	float r = atan2( y, x );
	dao.orientation = rad(r).from(angle_t::X_AXIS);
	mean_variance_t mv = gp_ind1.predict( dao );
	field( i, j ) = mv.mean;
	var( i, j ) = mv.var;
      }
    }
    oss.str("");
    oss << prefix << "gp_ind1.ssv";
    fout.open( oss.str().c_str() );
    fout << field;
    fout.close();
    oss.str("");
    oss << prefix << "gp_ind1_var.ssv";
    fout.open( oss.str().c_str() );
    fout << var;
    fout.close();
    std::cout << "dumped '" << oss.str() << "'" << std::endl;
    

    // the corr0 model
    //matrix_t field( num_y, num_x );
    std::cout << "corr0 model size= " << gp_corr0.size() << std::endl;
    for( size_t i = 0; i < num_y; ++i ) {
      for( size_t j = 0; j < num_x; ++j ) {
	float x = j*step_x;
	float y = i*step_y;
	dao.dist = sqrt( x*x + y*y );
	float r = atan2( y, x );
	dao.orientation = rad(r).from(angle_t::X_AXIS);
	mean_variance_t mv = gp_corr0.predict( dao );
	field( i, j ) = mv.mean;
	var( i, j ) = mv.var;
      }
    }
    oss.str("");
    oss << prefix << "gp_corr0.ssv";
    fout.open( oss.str().c_str() );
    fout << field;
    fout.close();
    oss.str("");
    oss << prefix << "gp_corr0_var.ssv";
    fout.open( oss.str().c_str() );
    fout << var;
    fout.close();
    std::cout << "dumped '" << oss.str() << "'" << std::endl;
    
    // the corr1 model
    //matrix_t field( num_y, num_x );
    std::cout << "corr1 model size= " << gp_corr1.size() << std::endl;
    for( size_t i = 0; i < num_y; ++i ) {
      for( size_t j = 0; j < num_x; ++j ) {
	float x = j*step_x;
	float y = i*step_y;
	dao.dist = sqrt( x*x + y*y );
	float r = atan2( y, x );
	dao.orientation = rad(r).from(angle_t::X_AXIS);
	mean_variance_t mv = gp_corr1.predict( dao );
	field( i, j ) = mv.mean;
	var( i, j ) = mv.var;
      }
    }
    oss.str("");
    oss << prefix << "gp_corr1.ssv";
    fout.open( oss.str().c_str() );
    fout << field;
    fout.close();
    oss.str("");
    oss << prefix << "gp_corr1_var.ssv";
    fout.open( oss.str().c_str() );
    fout << var;
    fout.close();
    std::cout << "dumped '" << oss.str() << "'" << std::endl;
    
  }


private:
  sparse_online_gp_t gp_ind0;
  sparse_online_gp_t gp_ind1;
  sparse_online_gp_t gp_corr0;
  sparse_online_gp_t gp_corr1;
};


class dummy_sensor_t 
  : public sensor_model_t
{
public:
  
  mean_variance_t query_no_update( const dist_and_orientation_t& x, const float& object_exists_p ) const
  {
    mean_variance_t mv;
    float mean_exists = 10 - x.dist;
    float mean_no = 0.3;
    float var_exists = 2;
    float var_no = 1;
    mv.mean = object_exists_p * mean_exists + (1-object_exists_p)*mean_no;
    mv.var = pow( object_exists_p, 2 ) * pow(var_exists,2)
      + pow( 1-object_exists_p, 2 ) * pow(var_no,2);
    return mv;
  }

  void update( const detection_and_location_t& dx )
  {
    // do nothing to update
  }

  math_vector_t gradient_of_theta_about( const float& state,
					 const dist_and_orientation_t& x,
					 const bool& use_independent_model_only) const
  {
    float y = state;

    float mean_exists = 10 - x.dist;
    float mean_no = 0.3;
    float var_exists = 2;
    float var_no = 1;

    float mu_ind0 = mean_no;
    float mu_ind1 = mean_exists;
    float mu_corr0 = mean_no;
    float mu_corr1 = mean_exists;
    float s_ind0 = var_no;
    float s_ind1 = var_exists;
    float s_corr0 = var_no;
    float s_corr1 = var_exists;
    float a = 0.5;

    if( use_independent_model_only )
      a = 1;

    // Ok, here's the derivative according to mathematica
    double dtheta1 =
      ((1 - a)*((mu_corr0) - (mu_corr1)) + a*((mu_ind0) - (mu_ind1))) /
      ( pow(a,2) *pow( pow(s_ind0,2)*pow(y , 2) + pow(s_ind1, 2)*pow(1 - y , 2), 2) +
	pow(1 - a, 2)*pow( pow(s_corr0, 2)*pow(y , 2) + pow(s_corr1, 2)*pow(1 - y ,2), 2)) -
      (((1 - a)*((mu_corr0)*y + (mu_corr1)*(1 - y )) +
	a*((mu_ind0)*y + (mu_ind1)*(1 - y )))*
       (2*pow(a , 2)*(2*pow(s_ind0 , 2)*y - 2*pow(s_ind1, 2)*(1 - y ))*
	(pow(s_ind0, 2)*pow(y , 2) + pow(s_ind1, 2)*pow(1 - y , 2)) +
	2*pow(1 - a, 2)*(2*pow(s_corr0, 2)*y - 2*pow(s_corr1, 2)*(1 - y ))*
	(pow(s_corr0, 2)*pow(y , 2) + pow(s_corr1, 2)*pow(1 - y , 2)))) / 
      pow(pow(a , 2)*pow(pow(s_ind0, 2)*pow(y , 2) + pow(s_ind1, 2)*pow(1 - y , 2), 2) +
	  pow(1 - a, 2)*pow(pow(s_corr0, 2)*pow(y , 2) + pow(s_corr1, 2)*pow(1 - y , 2), 2), 2);

    double dtheta2 =
      (2*pow(1 - a, 2)*(2*y *pow(s_corr0, 2) - 2*(1 - y )*pow(s_corr1, 2))*
       (pow(y , 2)*pow(s_corr0, 2) + pow(1 - y , 2)*pow(s_corr1, 2)) +
       2*pow(a , 2)*(2*y *pow(s_ind0, 2) - 2*(1 - y )*pow(s_ind1, 2))*
       (pow(y , 2)*pow(s_ind0, 2) + pow(1 - y , 2)*pow(s_ind1,2))) / 
      (2*pow(pow(1 - a, 2)*pow(pow(y , 2)*pow(s_corr0, 2) + pow(1 - y , 2)*pow(s_corr1, 2 ) , 2) +
	     pow(a , 2)*pow(pow(y , 2)*pow(s_ind0, 2) + pow(1 - y , 2)*pow(s_ind1, 2), 2) ,2));
      
 
    math_vector_t d(2);
    d(0) = dtheta1;
    d(1) = dtheta2;

    return d;
  }

  void query_separate( const dist_and_orientation_t& x, const float& exists, mean_variance_t& mv_ind, mean_variance_t& mv_corr, float& prob_indep ) const {
    prob_indep = 1;
  }




protected:
  
  sensor_model_tp clone() const
  {
    return sensor_model_tp( new dummy_sensor_t() );
  }

};



// The INIT
void TRAINED_sensor_models_INIT() {
  if( TRAINED_base_sensor_models.empty() ) {
    // add dummy sensors for both type 0 sensors
    //TRAINED_base_sensor_models.push_back( sensor_model_tp( new dummy_sensor_t()));
 
    // Add the door sensor GP model
    TRAINED_base_sensor_models.push_back( sensor_model_tp( new door_sensor_t()));
    
    // Add the text sensor GP model
    TRAINED_base_sensor_models.push_back( sensor_model_tp( new text_sensor_t()));
  }
}
