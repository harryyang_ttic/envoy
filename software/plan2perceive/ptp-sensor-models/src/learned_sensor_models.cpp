
#include "learned_sensor_models.hpp"

// Include our TRAINING generated models
#include "training/TRAINED_sensor_models.cpp"


using namespace ptp;
using namespace ptp::sensormodels;


//=============================================================

sensor_model_tp learned_sensor_models::create_new_sensor_model( const int& object_type )
{
  
  // initialize the trained models ( if needed )
  TRAINED_sensor_models_INIT();

  // Query for the trained sensor model of that type
  // and clone it
  sensor_model_tp model = TRAINED_base_sensor_models[object_type]->clone();
  
  return model;

}

//=============================================================
