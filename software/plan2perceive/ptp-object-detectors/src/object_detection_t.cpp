
#include "object_detection_t.hpp"
#include <ptp-common-coordinates/coordinate_math.hpp>
#include <ptp-common-coordinates/translation_manifold_t.hpp>
#include <ptp-common-coordinates/rotation_manifold_t.hpp>
#include <bot_core/bot_core.h>


using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::detectors;


//================================================================

object_detection_t::object_detection_t( const ptplcm_object_detection_t& lcm_det,
					const manifold_tp& world_manifold )
  : _lcm_det( lcm_det )
{
  double robot_rpy[3];
  bot_quat_to_roll_pitch_yaw(lcm_det.orientation_of_robot_when_taken, robot_rpy);
  double robot_yaw = robot_rpy[2];
  
  _manifold_when_taken = 
    create_object_manifold( coordinate( lcm_det.position_of_robot_when_taken[0],
					lcm_det.position_of_robot_when_taken[1],
					lcm_det.position_of_robot_when_taken[2] ).on( world_manifold ),
			    rad( robot_yaw ).from(angle_t::X_AXIS ),
			    world_manifold );
			    
  coordinate_t pos = 
    coordinate( lcm_det.position_in_detector_frame[0],
		lcm_det.position_in_detector_frame[1],
		lcm_det.position_in_detector_frame[2] ).on( _manifold_when_taken );
  _object_manifold = 
    create_object_manifold( pos, 
			    rad( lcm_det.orientation_from_detector_plane ).from(angle_t::X_AXIS),
			    _manifold_when_taken );

  //std::cout << "Detection obj-manifold=" << *_object_manifold << std::endl;

}

//================================================================

float object_detection_t::get_detection_value() const
{
  return _lcm_det.detector_value;
}

//================================================================

dist_and_orientation_t object_detection_t::get_dist_and_orientation( const manifold_tp& robot_manifold ) const
{
  return calculate_dist_and_orientation_from_object( robot_manifold, _object_manifold );
}

//================================================================


namespace ptp {
  namespace detectors {

    //================================================================
    
    manifold_tp create_object_manifold( const coordinate_t& position_on_ref,
					const angle_t& orientation_from_ref,
					const manifold_tp& ref )
    {
      
      // First, translate the origin of ref to the new origin
      // ( the object's location )
      manifold_tp tref = translate( position_on_ref, ref );
      
      // Now deal with the added rotation of the orientation
      manifold_tp rtref = rotate(line_simplex( coordinate( 0,0,0 ),
					       coordinate( 0,0,1 ),
					       tref),
				 orientation_from_ref,
				 tref );
      return rtref;
    }

    //================================================================
    
    dist_and_orientation_t 
    calculate_dist_and_orientation( const manifold_tp& target,
				    const manifold_tp& source )
    {

      //std::cout << "det::dist-orient : target=" << *target << ", source= " << *source << std::endl;
      
      // First, we want to see the distance between the origins of the 
      // two manifold in the source's frame
      coordinate_t target_origin_in_source =
	coordinate( 0,0,0 ).on( target ).on( source );

      float dist = 
	sqrt( raw_coordinate_t::l2_norm( target_origin_in_source.raw_coordinate(),
					 coordinate( 0,0,0 ) ) );

      // Ok, now we want to see the rotation between the x axis of the
      // target and source ( in the source frame )
      // Notice we only use the first segment of the curve
      // curve_t target_axis_curve_in_source = 
      // 	basis_t( coordinate( 0,0,0 ).on( target ),
      // 		 coordinate( 1,0,0 ).on( target ) ).on( source );
      // basis_t target_axis_in_source = target_axis_curve_in_source.first_segment();
      coordinate_t target_x_axis_point_in_source =
	coordinate( 1,0,0 ).on( target ).on( source );
      basis_t target_axis_in_source = 
	basis_t( target_origin_in_source,
		 target_x_axis_point_in_source );
      
      target_axis_in_source = target_axis_in_source.center_on( coordinate( 0,0,0 ).on( source ) );
      
      // calculate the angle between the two axis
      // We implicitly use the source x axis = (1,0,0)
      // HACK: we assume that the vectors are both on the x,y plane
      angle_t axis_angle = rad( atan2( target_axis_in_source.direction()[1],
				       target_axis_in_source.direction()[0]) ).from( angle_t::X_AXIS );
      
      
      // return the distance and nagle
      return dist_and_orientation_t( dist, axis_angle );
      

    }


    //================================================================
    
    dist_and_orientation_t 
    calculate_dist_and_orientation_from_object( const manifold_tp& view_man,
						const manifold_tp& obj_man )
    {

      //std::cout << "det::dist-orient : target=" << *target << ", source= " << *source << std::endl;
      
      // First, we want to see the distance between the origins of the 
      // two manifold in the source's frame
      coordinate_t target_origin_in_source =
	coordinate( 0,0,0 ).on( view_man ).on( obj_man );

      float dist = 
	sqrt( raw_coordinate_t::l2_norm( target_origin_in_source.raw_coordinate(),
					 coordinate( 0,0,0 ) ) );

      // Ok, now we want to see the rotation between the x axis of the
      // target and source ( in the source frame )
      // Notice we only use the first segment of the curve
      // curve_t target_axis_curve_in_source = 
      // 	basis_t( coordinate( 0,0,0 ).on( target ),
      // 		 coordinate( 1,0,0 ).on( target ) ).on( source );
      // basis_t target_axis_in_source = target_axis_curve_in_source.first_segment();
      coordinate_t target_x_axis_point_in_source =
	coordinate( 1,0,0 ).on( view_man ).on( obj_man );
      basis_t target_axis_in_source = 
	basis_t( target_origin_in_source,
		 target_x_axis_point_in_source );
      
      target_axis_in_source = target_axis_in_source.center_on( coordinate( 0,0,0 ).on( obj_man ) );
      
      // // calculate the angle between the two axis
      // // We implicitly use the source x axis = (1,0,0)
      // // Notice that we add 180 degre since we want the object manifold to
      // // be seen rather than looking with the manifold
      // // HACK: we assume that the vectors are both on the x,y plane
      // angle_t axis_angle = rad( atan2( target_axis_in_source.direction()[1],
      // 				       target_axis_in_source.direction()[0])
      // 				- M_PI ).from( angle_t::X_AXIS );

      
      // Calculate the angle between the source manifold and the
      // target location ( not the targe view ) since this is
      // for objects as the source
      angle_t axis_angle = rad( atan2( target_origin_in_source[1],
				       target_origin_in_source[0] ) ).from( angle_t::X_AXIS );
      
      
      // return the distance and nagle
      return dist_and_orientation_t( dist, axis_angle );
      

    }


    //================================================================


  }
}


