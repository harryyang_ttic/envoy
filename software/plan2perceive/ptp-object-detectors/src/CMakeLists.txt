
add_library( ptp-object-detectors SHARED
  common.hpp
  object_detection_t.hpp
  object_detection_t.cpp
  )

pods_install_headers(
  common.hpp
  object_detection_t.hpp
  DESTINATION ptp-object-detectors
  )

pods_use_pkg_config_packages( ptp-object-detectors ptp-common-coordinates bot2-core )

pods_install_libraries( ptp-object-detectors )

pods_install_pkg_config_file( ptp-object-detectors LIBS -lptp-object-detectors -llcmtypes_ptp-object-detectors REQUIRES ptp-common-coordinates bot2-core )
