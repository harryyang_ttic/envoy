
#if !defined( __PTP_DETECTORS_object_detection_t_HPP__ )
#define __PTP_DETECTORS_object_detection_t_HPP__


#include "common.hpp"
#include <lcmtypes/ptplcm_object_detection_t.h>
#include <ptp-common-coordinates/manifold.hpp>


namespace ptp {
  namespace detectors {

    using namespace ptp::coordinates;

    //===================================================================

    // Description:
    // A object detection.
    // This is an internal representation that can be converted to/from
    // the LCM messages but includes features such as manifolds.
    class object_detection_t
    {
    public:

      // Description:
      // Creates a detection from an lcm message and the current
      // manifold for the robot when it was taken
      object_detection_t( const ptplcm_object_detection_t& lcm_det,
			  const manifold_tp& robot_manifold );

      // Description:
      // Returns the distance and orientation for this detection
      // from the given robot manifold.
      dist_and_orientation_t 
      get_dist_and_orientation( const manifold_tp& robot_manifold ) const;

      // Description:
      // Returns the detection value
      float get_detection_value() const;

      // Description:
      // Returns a dection and location structure
      detection_and_location_t 
      get_detection_and_location( const manifold_tp& robot ) const
      { 
	return detection_and_location_t( get_detection_value(),
					 get_dist_and_orientation(robot) ); 
      }

      // Description:
      // Returns the object class type
      int get_object_type() const
      { return _lcm_det.object_class; }

      // Description:
      // Get taken manifold 
      // ( the manifold for the robot when the detection wastaken )
      manifold_tp get_manifold_when_taken() const
      { return _manifold_when_taken; }

      // Description:
      // Returns the object's manifold
      // ( where the object facesthe x axis )
      manifold_tp get_object_manifold() const
      { return _object_manifold; }

      // Description:
      // Get the LCM message for this
      const ptplcm_object_detection_t inner_lcm_detection() const
      { return _lcm_det; }
      
    protected:

      // Description:
      // The LCM message structure
      ptplcm_object_detection_t _lcm_det;

      // Description:
      // The robot manifold when the detection was taken
      manifold_tp _manifold_when_taken;

      // Description:
      // The object's manifold
      manifold_tp _object_manifold;
      

    };

    //===================================================================

    // Description:
    // Returns a new manifold for the given object which is
    // at a given position ( relative to given manifold ) and 
    // which has a certain orientation ( relative to the x of given manifld )
    manifold_tp create_object_manifold( const coordinate_t& position_on_ref,
					const angle_t& orientation_from_ref,
					const manifold_tp& ref );

    //===================================================================

    // Description:
    // Returnsthe distance from source to target manifold, 
    // as well as the orientation of the target manifold in the
    // source's frame
    dist_and_orientation_t 
    calculate_dist_and_orientation( const manifold_tp& target,
				    const manifold_tp& source );

    // Descrioption:
    // Returns the distance and prientation from the target manifold
    // ( with teh x axis as the fowards  direction ) and the 
    // given OBJECT manifold.
    // This is similar to above but take case of having orientation 
    // looking AT object rather than orietnation of the object.
    dist_and_orientation_t
    calculate_dist_and_orientation_from_object( const manifold_tp& view_man,
						const manifold_tp& obj_man );
    
    //===================================================================



  }
}

#endif

