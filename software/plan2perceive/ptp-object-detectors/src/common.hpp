
#if !defined( __PTP_DETECTORS_common_HPP__ )
#define __PTP_DETECTORS_common_HPP__


#include <ptp-common-coordinates/angle.hpp>

namespace ptp {
  namespace detectors {


    using namespace ptp::coordinates;

    //===============================================================

    // Description:
    // A simple structure which encodes a distance nad orientation of object
    struct dist_and_orientation_t
    {
      float dist;
      angle_t orientation;
      dist_and_orientation_t( const float& d, const angle_t& o )
	: dist(d), orientation(o)
      {}
    };


    //===============================================================

    // Description:
    // A detection from object detector
    struct detection_and_location_t 
    {
      float detection_value;
      dist_and_orientation_t location;
      detection_and_location_t( const float& det,
				const dist_and_orientation_t& x )
	: detection_value(det),
	  location(x)
      {}
    };

    //===============================================================

    

  }
}

#endif

