#include <string>
#include <vector>
#include <iostream>
#include <stdio.h>

#include <GL/gl.h>
#include <glib.h>
#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <ptp-object-detectors/common.hpp>
#include <ptp-sensor-models/sensor_models_t.hpp>
#include <lcmtypes/ptplcm_object_detection_set_t.h>
#include <lcmtypes/bot_core_pose_t.h>
#include <er_lcmtypes/lcm_channel_names.h>
#include <lcmtypes/ptplcm_object_initial_beliefs_t.h>

#define BUILD_ROOT "/home/velezj/projects/object_detector_pose_planning/envoy/software/plan2perceive/"

#ifndef BUILD_ROOT
#error "BUILD_ROOT undefined"
#endif

// simulate the amount of time it takes for an object detector to run
//#define DETECTOR_DELAY_MS 5000
// FOR best SIM RUNS so far #define DETECTOR_DELAY_MS 2000
#define DETECTOR_DELAY_MS 6000
#define FOV_MIN_ANGLE (-60 * M_PI / 180)
#define FOV_MAX_ANGLE (60 * M_PI / 180)
#define FOV_MIN_RANGE (0)
#define FOV_MAX_RANGE (9)


using namespace std;
using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::detectors;
using namespace ptp::sensormodels;
//using namespace ptp::planner;

#define dbg(...) fprintf(stderr, __VA_ARGS__)

struct carmen_point_t
{
  float x;
  float y;
  float theta;
};

struct StoredObject
{
  int id;
  double x;
  double y;
  double z;
  double theta;
  string obj_class;
  int obj_type;
  bool seen_once;
  mean_variance_t belief;

  StoredObject(int id, double x, double y, double z, double theta, string obj_class, int obj_type, mean_variance_t bel ) {
    this->id = id;
    this->x = x;
    this->y = y;
    this->z = z;
    this->theta = theta;
    this->obj_class = obj_class;
    this->obj_type = obj_type;
    this->seen_once = false;
    this->belief = bel;
  }
};

struct SimulatedDetection
{
  double relative_x;
  double relative_y;
  double relative_theta;
  double score;
  int32_t object_type;
  bool was_correlated_to_history;
  int32_t obj_id;

  SimulatedDetection(double rx, double ry, double rtheta, double score, int32_t object_type, int32_t oid) :
    relative_x(rx),
    relative_y(ry),
    relative_theta(rtheta),
    score(score),
    object_type(object_type),
    was_correlated_to_history(false),
    obj_id(oid)
  {}

  
};

struct DelayedTransmission
{
  ptplcm_object_detection_set_t* msg;
  lcm_t* lcm;
  int delay_ms;
  DelayedTransmission(const ptplcm_object_detection_set_t* msg, lcm_t* lcm, 
      int delay_ms) {
    this->msg = ptplcm_object_detection_set_t_copy(msg);
    this->lcm = lcm;
    this->delay_ms = delay_ms;
    g_timeout_add(delay_ms, DelayedTransmission::delayedTransmit, this);
  }
  ~DelayedTransmission() {
    delete this->msg;
  }
  static gboolean delayedTransmit(void* user_data) {
    DelayedTransmission* self = static_cast<DelayedTransmission*>(user_data);
    self->msg->utime = self->msg->source_time + self->delay_ms*1000;
    dbg("publishing %d object detection%s\n", 
        self->msg->size, (self->msg->size==1 ? "" : "s"));
    ptplcm_object_detection_set_t_publish( self->lcm, "DETECTOR_OUTPUT_CHANNEL", self->msg );
    delete self;
    return FALSE;
  }
};

void 
ptp_robot_dist_theta_relative_to_object(const carmen_point_t *obj_xyt,
    double robot_x, double robot_y,
    double *dist,
    double *theta)
{
  double dx = obj_xyt->x - robot_x;
  double dy = obj_xyt->y - robot_y;
  // assume that the robot is pointing at the object?
  double rtheta = atan2(dy, dx);
  //  double rtheta = rpoint->theta;
  double otheta = obj_xyt->theta;
  double dtheta = otheta - bot_mod2pi_ref(otheta, rtheta);

  *dist = sqrt(dx*dx + dy*dy);
  *theta = dtheta;
}

void
ptp_object_position_from_detection(const carmen_point_t* obj_relative_xyt,
    const carmen_point_t* detector_position,
    carmen_point_t* object_xyt)
{
  double costheta = cos(detector_position->theta);
  double sintheta = sin(detector_position->theta);

  double tx = obj_relative_xyt->x;
  double ty = obj_relative_xyt->y;

  object_xyt->x = tx * costheta - ty * sintheta + detector_position->x;
  object_xyt->y = tx * sintheta + ty * costheta + detector_position->y;
  object_xyt->theta = detector_position->theta + obj_relative_xyt->theta;
}

void
ptp_object_position_relative_to_robot(const carmen_point_t* object_xyt,
    const carmen_point_t* detector_position,
    carmen_point_t* obj_relative_xyt)
{
  double ty = object_xyt->y - detector_position->y;
  double tx = object_xyt->x - detector_position->x;
  double costheta = cos(detector_position->theta);
  double sintheta = sin(detector_position->theta);
  obj_relative_xyt->x =  costheta * tx + sintheta * ty;
  obj_relative_xyt->y = -sintheta * tx + costheta * ty;
  obj_relative_xyt->theta = object_xyt->theta - detector_position->theta;
}

char**
ptp_get_map_names(void)
{
  int n = 0;
  char** result = (char**) malloc(sizeof(char**)*(n+1));

  char* maps_dir = g_strdup_printf("%s/data/maps", BUILD_ROOT);

  GError* err;
  GDir* dir = g_dir_open(maps_dir, 0, &err);
  if(!dir) {
    fprintf(stderr, "Error listing maps: %s\n", err->message);
    g_error_free(err);
    return result;
  }
  for(const gchar* entry=g_dir_read_name(dir); entry; entry=g_dir_read_name(dir)) {
    char* sub_dir_name = g_strdup_printf("%s/%s", maps_dir, entry);
    if(g_file_test(sub_dir_name, G_FILE_TEST_IS_DIR) && !g_str_has_prefix(entry, ".")) {
      result[n] = g_strdup(entry);
      n++;
      result = (char**) realloc(result, sizeof(char**)*n);
    }
    g_free(sub_dir_name);
  }
  g_dir_close(dir);
  g_free(maps_dir);

  return result;
}

void 
ptp_print_map_names(void)
{
  char** map_names = ptp_get_map_names();
  for(int i=0; map_names[i]; i++) {
    printf("    %s\n", map_names[i]);
  }
  g_strfreev(map_names);
}

// Return true ( heads ) for flipping a biased-towards-heads coin
bool flip( const double& bias ) 
{
  return ( (static_cast<double>(rand()) / RAND_MAX) < bias );
}

bool
loadStoredObjects(const char* map_name, vector<StoredObject>* output,
	 	  sensor_models_t& models)
{
  output->clear();

  char* fname = g_strdup_printf("%s/data/maps/%s/sim_objects.txt", BUILD_ROOT, map_name);
  char* contents = NULL;
  gsize content_len = -1;
  GError* err = NULL;
  if(! g_file_get_contents(fname, &contents, &content_len, &err)) {
    fprintf(stderr, "Error loading sim objects: %s\n", err->message);
    g_error_free(err);
    return false;
  }
  g_free(fname);

  char** lines = g_strsplit(contents, "\n", 0);
  g_free(contents);
  for(int lineno=0; lines[lineno]; lineno++) {
    char* line = lines[lineno];
    g_strstrip(line);
    int linelen = strlen(line);
    if(0 == linelen || line[0] == '#') {
      continue;
    }

    double x, y, z, theta_deg;
    char* object_class = (char*) malloc(linelen);
    int object_type;
    double bel_mean, bel_var;

    int nelem = sscanf(line, "%lf %lf %lf %lf %lf %lf %s %d", &x, &y, &z, &theta_deg, &bel_mean, &bel_var, object_class, &object_type);
    if(nelem != 8) {
      fprintf(stderr, "File error at line %d\n", lineno);
      continue;
    }

    printf("%3d: %f %f %f %f %f %f %s %d\n", lineno, x, y, z, theta_deg, bel_mean, bel_var, object_class, object_type);

    // create a new sensor model
    dist_and_orientation_t dao( 20, rad(0).from(angle_t::X_AXIS) );
    detection_and_location_t dx( 0, dao );
    models.add_new_object( object_type, dx );
    mean_variance_t mv;
    mv.mean = bel_mean;
    mv.var = bel_var;

    StoredObject obj(output->size(), x, y, z, theta_deg * M_PI / 180, object_class, object_type, mv);
    free(object_class);
    output->push_back(obj);
  }

  return true;
}

class ObjectDetectorSimulator
{
  public:
  ObjectDetectorSimulator
  (lcm_t* lcm, const char* mapname, int initially_see_object);
    ~ObjectDetectorSimulator();

    inline int getNumObjects() { return _objects.size(); }

    void runDetector();

    void draw();

    inline double dist( const SimulatedDetection&a, const SimulatedDetection& b );

  SimulatedDetection ensure_correlation_from_history( const SimulatedDetection& det, const vector<SimulatedDetection>& history, const float& prob_independent );

    static void onPose(const lcm_recv_buf_t* rbuf, const char* channel, 
        const bot_core_pose_t* msg, void* user);
    static gboolean runDetectorStatic(void* user_data);


  static void onInitialBeliefsPing( const lcm_recv_buf_t* rbuf,
				    const char* channel,
				    const ptplcm_object_initial_beliefs_t* msg,
				    void* user );

  private:
    lcm_t* _lcm;
    bot_lcmgl_t* _lcmgl;
    bot_core_pose_t* _last_pose;
    vector<StoredObject> _objects;
    string _mapname;
  int _initially_see_object;

    double _fov_min_angle;
    double _fov_max_angle;
    double _fov_min_range;
    double _fov_max_range;

  sensor_models_t _models;

  vector< vector<SimulatedDetection> > _detection_history;
};

ObjectDetectorSimulator::ObjectDetectorSimulator(lcm_t* lcm, const char* mapname, int initially_see_object) : 
  _lcm(lcm),
  _last_pose(NULL),
  _mapname(mapname),
  _initially_see_object( initially_see_object )
{
  loadStoredObjects(mapname, &_objects, _models);

  _lcmgl = bot_lcmgl_init(_lcm, "sim-objects");
  bot_lcmgl_switch_buffer(_lcmgl);

  _fov_min_angle = FOV_MIN_ANGLE;
  _fov_max_angle = FOV_MAX_ANGLE;
  _fov_min_range = FOV_MIN_RANGE;
  _fov_max_range = FOV_MAX_RANGE;


  for( size_t i = 0; i < _objects.size(); ++i ) {
    _detection_history.push_back( vector<SimulatedDetection>() );
  }

  bot_core_pose_t_subscribe(lcm, POSE_CHANNEL, ObjectDetectorSimulator::onPose, this);
  ptplcm_object_initial_beliefs_t_subscribe( lcm, "PTP_INITIAL_BELIEFS", ObjectDetectorSimulator::onInitialBeliefsPing, this );

  g_timeout_add(1000, ObjectDetectorSimulator::runDetectorStatic, this);
}

ObjectDetectorSimulator::~ObjectDetectorSimulator()
{
  if(_last_pose)
    bot_core_pose_t_destroy(_last_pose);
  bot_lcmgl_destroy(_lcmgl);
}

void ObjectDetectorSimulator::onPose(const lcm_recv_buf_t* rbuf, const char* channel, 
    const bot_core_pose_t* msg, void* user)
{
  ObjectDetectorSimulator* self = static_cast<ObjectDetectorSimulator*> (user);
  if(self->_last_pose)
    bot_core_pose_t_destroy(self->_last_pose);
  self->_last_pose = bot_core_pose_t_copy(msg);
}

void ObjectDetectorSimulator::onInitialBeliefsPing( const lcm_recv_buf_t* rbuf,
						    const char* channel,
						    const ptplcm_object_initial_beliefs_t* msg,
						    void* user )
{
  // Ok, we will ping back our initial beliefs if it's a ping message
  if( msg->size == 0 ) {

    ObjectDetectorSimulator* self = static_cast<ObjectDetectorSimulator*> (user);
    
    // This is a ping message, reply with beliefs
    ptplcm_object_initial_beliefs_t reply;
    reply.utime = bot_timestamp_now();
    reply.size = self->_objects.size();
    reply.object_types = new int32_t[ reply.size ];
    reply.object_poses = new ptplcm_point_t[ reply.size ];
    reply.object_belief_means = new double[ reply.size ];
    reply.object_belief_variances = new double[ reply.size ];
    for( size_t i = 0; i < reply.size; ++i ) {
      reply.object_types[i] = self->_objects[i].obj_type;
      reply.object_belief_means[i] = self->_objects[i].belief.mean;
      reply.object_belief_variances[i] = self->_objects[i].belief.var;
      reply.object_poses[i].x = self->_objects[i].x;
      reply.object_poses[i].y = self->_objects[i].y;
      reply.object_poses[i].z = self->_objects[i].z;
      reply.object_poses[i].yaw = self->_objects[i].theta;
    }
    ptplcm_object_initial_beliefs_t_publish( self->_lcm, "PTP_INITIAL_BELIEFS", &reply );

    std::cout << "  !initial belief ping!" << std::endl;
    
    delete[] reply.object_types;
    delete[] reply.object_poses;
    delete[] reply.object_belief_means;
    delete[] reply.object_belief_variances;
    
  }
  
}

gboolean
ObjectDetectorSimulator::runDetectorStatic(void* user_data)
{
    ObjectDetectorSimulator* self = static_cast<ObjectDetectorSimulator*>(user_data);
    self->runDetector();
    return FALSE;
}

void ObjectDetectorSimulator::runDetector()
{
  if(!_last_pose)
    return;

  double robot_rpy[3];
  bot_quat_to_roll_pitch_yaw(_last_pose->orientation, robot_rpy);
  double robot_yaw = robot_rpy[2];

  vector<SimulatedDetection> detections;

  // find objects in range.
  for(int obj_ind = 0; obj_ind<_objects.size(); obj_ind++) {
    StoredObject& obj = _objects[obj_ind];

    carmen_point_t obj_xyt = { obj.x, obj.y, obj.theta };
    carmen_point_t robot_xyt = { _last_pose->pos[0], _last_pose->pos[1], robot_yaw };

    // robot pose relative to object
    carmen_point_t obj_relative_xyt;
    ptp_object_position_relative_to_robot(&obj_xyt, &robot_xyt, &obj_relative_xyt);

    double dist = sqrt(obj_relative_xyt.x*obj_relative_xyt.x + 
            obj_relative_xyt.y*obj_relative_xyt.y);
    double theta = atan2(obj_relative_xyt.y, obj_relative_xyt.x);

    // robot in object coordinate
    double temp_theta_r = atan2( robot_xyt.y - obj_xyt.y,
				 robot_xyt.x - obj_xyt.x );
    double rtheta_in_obj =  temp_theta_r - obj_xyt.theta;

    // is the object in the sensor field of view?
    if( _initially_see_object == 0 ) {
      obj.seen_once = true; // do not automatically 'see' the object the 
                            // first time it compes into range
    }

    // if(dist < _fov_min_range || dist > _fov_max_range ||
    //    ( obj.seen_once == true && ( theta < _fov_min_angle || theta > _fov_max_angle ) ) ) {
    //     printf("Object %3d not in sensor fov (%6.2f, %6.2f)\n", obj.id, dist, theta);
    //     // out of detector FOV.
    //     continue;
    // }
    if( fabs( dist ) > _fov_max_range ||
	theta < _fov_min_angle ||
	theta > _fov_max_angle ||
	rtheta_in_obj > 66 * M_PI / 180 ||
	rtheta_in_obj < -66 * M_PI / 180 ) {
   
      printf("Object %3d not in sensor fov (%6.2f, %6.2f [%4.1f])\n", obj.id, dist, theta * 180 / M_PI, rtheta_in_obj * 180 / M_PI);
      continue;
    }

    // in field of view.  Now recompute the angle to the object, taking the
    // object's orientation into account.
    double obj_relative_theta;
    ptp_robot_dist_theta_relative_to_object(&obj_xyt, robot_xyt.x, robot_xyt.y, &dist, 
            &obj_relative_theta);

    // sample a detection
    bool object_exists = false;
    if( obj.obj_class == "no" )
      object_exists = false;
    else if ( obj.obj_class == "yes" )
      object_exists = true;
    else
      BOOST_THROW_EXCEPTION( exception_base() );
    dist_and_orientation_t dao( dist, rad(obj_relative_theta).from(angle_t::X_AXIS) );
    float prob_independent;
    mean_variance_t mv_ind, mv_corr;
    _models.object( obj.id )->query_separate( dao, object_exists ? 1 : 0, mv_ind, mv_corr, prob_independent );
    float score = 0;
    bool was_correlated_to_history = false;

    // for debug query the full model as well
    mean_variance_t mv_full = _models.object( obj.id )->query_no_update( dao, object_exists ? 1 : 0 );


    // If we are correlated, generate from correlation model only
    if( flip( 1 - prob_independent ) ) {
      score = bot_gauss_rand( mv_corr.mean, mv_corr.var );
      was_correlated_to_history = true;
    } else {
      score = bot_gauss_rand( mv_ind.mean, mv_ind.var );
      was_correlated_to_history = false;
    }


    SimulatedDetection det(obj_relative_xyt.x, obj_relative_xyt.y, obj_relative_xyt.theta,
			   score, obj.obj_type, obj.id);
    det.was_correlated_to_history = was_correlated_to_history;

    // if correlated take previous historical value
    //if( det.was_correlated_to_history ) {
    //  det = ensure_correlation_from_history( det, _detection_history[obj_ind], 0.0 );
    //}

    // compute likelihood according to models
    double det_lik_full = 1.0 / sqrt( 2 * M_PI * mv_full.var * mv_full.var ) * exp( - ( ( det.score - mv_full.mean ) * ( det.score - mv_full.mean ) ) / ( 2 * mv_full.var * mv_full.var ) );
    double det_lik_ind = 1.0 / sqrt( 2 * M_PI * mv_ind.var * mv_ind.var ) * exp( - ( ( det.score - mv_ind.mean ) * ( det.score - mv_ind.mean ) ) / ( 2 * mv_ind.var * mv_ind.var ) );
    double det_lik_corr = 1.0 / sqrt( 2 * M_PI * mv_corr.var * mv_corr.var ) * exp( - ( ( det.score - mv_corr.mean ) * ( det.score - mv_corr.mean ) ) / ( 2 * mv_corr.var * mv_corr.var ) );
        


    // Now, UPDATE our model with the detection
    detection_and_location_t dx( det.score, dao );
    _models.object( obj.id )->update( dx );
    
    // store detection for later sending and for history
    detections.push_back(det);
    _detection_history[obj_ind].push_back(det);

    obj.seen_once = true;

    printf("Object %3d (%4s %d): (%4.2f, %4.2f) -> %3.2f (pind=%f, %s  {i:%4.3f,%4.3f} {c:%4.3f,%4.3f})\n", obj.id, obj.obj_class.c_str(), obj.obj_type, dist, obj_relative_theta, det.score, prob_independent, ( det.was_correlated_to_history ? "*corr*" : "ind"), mv_ind.mean, mv_ind.var, mv_corr.mean, mv_corr.var );
    printf("   full_model={%4.3f, %4.3f}, lik: full=%4.3f, ind=%4.3f, cor=%4.3f\n",
	   mv_full.mean, mv_full.var, det_lik_full, det_lik_ind, det_lik_corr );
  }

  // publish message
  ptplcm_object_detection_set_t msg;
  memset(&msg, 0, sizeof(msg));
  msg.utime = _last_pose->utime; // this gets overwritten later, at transmit time
  msg.source_time = msg.utime;

  msg.size = detections.size();
  msg.detections = new ptplcm_object_detection_t[ msg.size ];
  for(int det_index=0; det_index<msg.size; det_index++) {
    msg.detections[det_index].utime = msg.utime;
    msg.detections[det_index].source_utime = msg.source_time;
    msg.detections[det_index].object_class = detections[det_index].object_type;
    msg.detections[det_index].object_local_id = detections[det_index].obj_id;
    msg.detections[det_index].detector_value = detections[det_index].score;
    msg.detections[det_index].position_in_detector_frame[0] = detections[det_index].relative_x;
    msg.detections[det_index].position_in_detector_frame[1] = detections[det_index].relative_y;
    msg.detections[det_index].position_in_detector_frame[2] = 0;
    msg.detections[det_index].distance_from_detector = sqrt( detections[det_index].relative_x * detections[det_index].relative_x + detections[det_index].relative_y * detections[det_index].relative_y );
    msg.detections[det_index].orientation_from_detector_plane = detections[det_index].relative_theta;
    msg.detections[det_index].has_image_patch = false;
    msg.detections[det_index].has_disparity_patch = false;
    msg.detections[det_index].has_point_cloud = false;
    msg.detections[det_index].position_of_robot_when_taken[0] = _last_pose->pos[0];
    msg.detections[det_index].position_of_robot_when_taken[1] = _last_pose->pos[1];
    msg.detections[det_index].position_of_robot_when_taken[2] = _last_pose->pos[2];
    msg.detections[det_index].orientation_of_robot_when_taken[0] = _last_pose->orientation[0];
    msg.detections[det_index].orientation_of_robot_when_taken[1] = _last_pose->orientation[1];
    msg.detections[det_index].orientation_of_robot_when_taken[2] = _last_pose->orientation[2];
    msg.detections[det_index].orientation_of_robot_when_taken[3] = _last_pose->orientation[3];
    msg.detections[det_index].detector_info.position_in_robot_body_frame[0] = 0;
    msg.detections[det_index].detector_info.position_in_robot_body_frame[1] = 0;
    msg.detections[det_index].detector_info.position_in_robot_body_frame[2] = 0;
    msg.detections[det_index].detector_info.look_at_axis_in_robot_body_frame[0] = 1;
    msg.detections[det_index].detector_info.look_at_axis_in_robot_body_frame[1] = 0;
    msg.detections[det_index].detector_info.look_at_axis_in_robot_body_frame[2] = 0;
    msg.detections[det_index].detector_info.angle_about_look_at_axis = 0;
    msg.detections[det_index].detector_info.fov_left_radians = FOV_MAX_ANGLE;
    msg.detections[det_index].detector_info.fov_right_radians = FOV_MIN_ANGLE;
    msg.detections[det_index].detector_info.fov_bottom_radians = 0;
    msg.detections[det_index].detector_info.fov_top_radians = 0;
    msg.detections[det_index].detector_info.fov_min_distance = FOV_MIN_RANGE;
    msg.detections[det_index].detector_info.fov_max_distance = FOV_MAX_RANGE;
  }

  // transmit the message later
  new DelayedTransmission(&msg, _lcm, DETECTOR_DELAY_MS);

  // set a timeout to call the detector again later.
  g_timeout_add(DETECTOR_DELAY_MS, ObjectDetectorSimulator::runDetectorStatic, this);

  delete[] msg.detections;

  draw();
}


double ObjectDetectorSimulator::dist
( const SimulatedDetection& a, const SimulatedDetection& b )
{
  return sqrt( (a.relative_x - b.relative_x ) * (a.relative_x - b.relative_x)
	       + (a.relative_y - b.relative_y) * (a.relative_y - b.relative_y));
}


SimulatedDetection ObjectDetectorSimulator::ensure_correlation_from_history
( const SimulatedDetection& det, const vector<SimulatedDetection>& history,
  const float& prob_independet)
{
  // Ok, find closest previous detection
  if( history.size() < 1 )
    return det;
  SimulatedDetection prev = history[0];
  for( size_t i = 1; i < history.size(); ++i ) {
    if( dist( det, history[i]) < dist( det, prev ) ) {
      prev = history[i];
    } 
  }

  // now we flip a biased coin to see if we return the previous detector value
  // or the new ( sampled ) detector score
  // This acts like a mixture of fully correlated and uncorrelated by d_max

  SimulatedDetection corr_det = det;
  if( flip( 1 - prob_independet ) ) {
    corr_det.score = prev.score;
    corr_det.was_correlated_to_history = true;
  }
  return corr_det;
}


static void drawDoor(bot_lcmgl_t* lcmgl)
{
//  bot_lcmgl_box(lcmgl, double xyz[3], float size[3]);
  double xyz[] = { 0, 0, 0 };
  double sz[] = { 0.1, 1 };
//  bot_lcmgl_circle(lcmgl, xyz, 1);
  bot_lcmgl_rect(lcmgl, xyz, sz, 1);

  double knob_xyz[] = { -0.05, 0.45, 0 };
  bot_lcmgl_disk(lcmgl, knob_xyz, 0, 0.05);
}

static void drawSign(bot_lcmgl_t* lcmgl)
{
  bot_lcmgl_begin( lcmgl , GL_QUADS );
  bot_lcmgl_vertex2f( lcmgl, 0.2, 0.1 );
  bot_lcmgl_vertex2f( lcmgl, -0.2, 0.5 );
  bot_lcmgl_vertex2f( lcmgl, -0.2, -0.5 );
  bot_lcmgl_vertex2f( lcmgl, 0.2, -0.1 );
  bot_lcmgl_end( lcmgl );
}

static void drawDetection(bot_lcmgl_t* lcmgl, const SimulatedDetection& det)
{
  if( det.was_correlated_to_history ) {
    bot_lcmgl_color3f( lcmgl, 1, 0, 0 );
  } else {
    bot_lcmgl_color3f( lcmgl, 0, 0, 1 );
  }
  double xyz[] = { 0, 0, 0 };
  double sz[] = { 0.05, 0.05 };
  bot_lcmgl_rect( lcmgl, xyz, sz, 1 );
}

void ObjectDetectorSimulator::draw()
{
  for(int obj_ind = 0; obj_ind<_objects.size(); obj_ind++) {
    const StoredObject& obj = _objects[obj_ind];
    if(obj.obj_class == "yes")
        bot_lcmgl_color3f(_lcmgl, 0, 1, 0);
    else
        bot_lcmgl_color3f(_lcmgl, 1, 0, 0);
    bot_lcmgl_push_matrix(_lcmgl);
    bot_lcmgl_translated(_lcmgl, obj.x, obj.y, obj.z);
    bot_lcmgl_rotated(_lcmgl, obj.theta * 180/M_PI, 0, 0, 1);
    if( obj.obj_type == 0 )
      drawDoor( _lcmgl );
    else
      drawSign( _lcmgl );
    
    // Uncomment below to draw history of detection colored by whether
    // they were correlated or not
    /*
    for( size_t det_ind = 0; det_ind < _detection_history[obj_ind].size(); ++det_ind ) {
      drawDetection( _lcmgl, _detection_history[obj_ind][det_ind] );
    }
    */
    
    bot_lcmgl_pop_matrix(_lcmgl);
  }

  bot_lcmgl_switch_buffer(_lcmgl);
}

int main(int argc, char** argv)
{
  if(argc < 2) {
    fprintf(stderr, "usage: sim_objects <map_name>\n");
    fprintf(stderr, "\n");
    fprintf(stderr, "Available maps:\n");
    ptp_print_map_names();
    return 1;
  }

  const char* mapname = argv[1];
  int initially_see_object = 0;
  if( argc > 2 ) {
    initially_see_object = atoi( argv[2] );
  }

  setlinebuf(stdout);

  GMainLoop* mainloop = g_main_loop_new(NULL, FALSE);
  lcm_t* lcm = lcm_create(NULL);
  ObjectDetectorSimulator* sim = new ObjectDetectorSimulator(lcm, mapname, initially_see_object);
  if(!sim->getNumObjects()) {
    fprintf(stderr, "Warning:  No stored objects...\n");
  }

  bot_glib_mainloop_attach_lcm(lcm);
  bot_signal_pipe_glib_quit_on_kill(mainloop);
  g_main_loop_run(mainloop);
  g_main_loop_unref(mainloop);

  return 0;
}
