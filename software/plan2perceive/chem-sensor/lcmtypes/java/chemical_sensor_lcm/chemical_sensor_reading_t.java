/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package chemical_sensor_lcm;
 
import java.io.*;
import java.nio.*;
import java.util.*;
import lcm.lcm.*;
 
public final class chemical_sensor_reading_t implements lcm.lcm.LCMEncodable
{
    public long utime;
    public short sensor1_resistance;
    public short sensor2_resistance;
    public short sensor3_resistance;
 
    public chemical_sensor_reading_t()
    {
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0xfaa8ebcf6e6bc1caL;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class>());
    }
 
    public static long _hashRecursive(ArrayList<Class> classes)
    {
        if (classes.contains(chemical_sensor_lcm.chemical_sensor_reading_t.class))
            return 0L;
 
        classes.add(chemical_sensor_lcm.chemical_sensor_reading_t.class);
        long hash = LCM_FINGERPRINT_BASE
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeLong(this.utime); 
 
        outs.writeShort(this.sensor1_resistance); 
 
        outs.writeShort(this.sensor2_resistance); 
 
        outs.writeShort(this.sensor3_resistance); 
 
    }
 
    public chemical_sensor_reading_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public chemical_sensor_reading_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static chemical_sensor_lcm.chemical_sensor_reading_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        chemical_sensor_lcm.chemical_sensor_reading_t o = new chemical_sensor_lcm.chemical_sensor_reading_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.utime = ins.readLong();
 
        this.sensor1_resistance = ins.readShort();
 
        this.sensor2_resistance = ins.readShort();
 
        this.sensor3_resistance = ins.readShort();
 
    }
 
    public chemical_sensor_lcm.chemical_sensor_reading_t copy()
    {
        chemical_sensor_lcm.chemical_sensor_reading_t outobj = new chemical_sensor_lcm.chemical_sensor_reading_t();
        outobj.utime = this.utime;
 
        outobj.sensor1_resistance = this.sensor1_resistance;
 
        outobj.sensor2_resistance = this.sensor2_resistance;
 
        outobj.sensor3_resistance = this.sensor3_resistance;
 
        return outobj;
    }
 
}

