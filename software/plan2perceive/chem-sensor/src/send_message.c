// sends a sample chemical_sensor_reading_t LCM messages on the channel "CHEMICAL_SENSOR"

#include <stdio.h>
#include <lcm/lcm.h>

#include <lcmtypes/chemical_sensor_lcm_chemical_sensor_reading_t.h>

static void send_message(lcm_t * lcm) {
    chemical_sensor_lcm_chemical_sensor_reading_t my_data = {
        .utime = 0,
        .sensor1_resistance = 10,
        .sensor2_resistance = 20,
        .sensor3_resistance = 30
    };

    chemical_sensor_lcm_chemical_sensor_reading_t_publish(lcm, "CHEMICAL_SENSOR", &my_data);
}

int main(int argc, char ** argv) {
    lcm_t * lcm;

    lcm = lcm_create(NULL);
    if(!lcm)
        return 1;

    send_message(lcm);

    lcm_destroy(lcm);
    return 0;
}
