// Listens for chemical_sensor_reading_t LCM messages on the channel
// "CHEMICAL_SENSOR"

#include <stdio.h>
#include <inttypes.h>
#include <lcm/lcm.h>
#include <lcmtypes/chemical_sensor_lcm_chemical_sensor_reading_t.h>

static void
my_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
        const chemical_sensor_lcm_chemical_sensor_reading_t * msg, void * user)
{
    int i;
    printf("Received message on channel \"%s\":\n", channel);
    printf("  utime   = %"PRId64"\n", msg->utime);
    printf("  sensor1_resistance    = %i\n", msg->sensor1_resistance);
    printf("  sensor2_resistance    = %i\n", msg->sensor2_resistance);
    printf("  sensor3_resistance    = %i\n", msg->sensor3_resistance);
    printf("\n");
}

int main(int argc, char ** argv) {
    lcm_t * lcm;

    lcm = lcm_create(NULL);
    if(!lcm)  
      return 1;

    chemical_sensor_lcm_chemical_sensor_reading_t_subscription_t * sub =
        chemical_sensor_lcm_chemical_sensor_reading_t_subscribe(lcm, "CHEMICAL_SENSOR", &my_handler, NULL);

    while(1)
      lcm_handle(lcm);

    chemical_sensor_lcm_chemical_sensor_reading_t_unsubscribe(lcm, sub);
    lcm_destroy(lcm);
    return 0;
}
