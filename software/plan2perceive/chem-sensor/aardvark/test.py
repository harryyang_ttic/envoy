from aardvark_py import *
from neem import *

import time

# put in try finally to make sure we close the port
try:
    handle = configure_aardvark()
    
    # set chamber temperature to and pump voltage
    set_chamber_temp_and_pump_voltage(handle, 40, 5)
    
    # set heater tensions
    set_heater_tensions(handle, 2.38, 5.1, 5.1)

    for i in range(1, 30):
        print_status(*read_status(handle))
        print_readings(read_sensors(handle))
        time.sleep(1)
    
    # shut down furnace, pump 
    set_chamber_temp_and_pump_voltage(handle, 0, 0)
    
finally:                 
    # close the aardvark connection
    aa_close(handle)
