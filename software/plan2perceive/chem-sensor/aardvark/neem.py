from aardvark_py import *
import time

# Finds an Aardvark adapter connected to the system, and configures it for I2C
# communction. Returns the handle for adapter.
def configure_aardvark():
    # find the aardvark adapters connected to the system
    num, ports = aa_find_devices(10)
    if num < 1:
        raise Exception('No Aardvark devices found.')


    print ports

    # get the port number
    port = ports[0]

    # get a handle for the aardvark
    handle = aa_open(port)
    if (handle <= 0):
        raise Exception('Unable to open Aardvark device on port {0}: {1}'.format(
                port, aa_status_string(handle)))

    try:
        # configure for I2C
        ret = aa_configure(handle, AA_CONFIG_SPI_I2C)
        if ret < 0:
            raise Exception('Error configuring Aardvark device on port ' +
                            '{0} for I2C: {1}'.format(
                    port, aa_status_string(ret)))

        ret = aa_i2c_bitrate(handle, 40)

        # NOT SURE IF NEED THIS
        # Enable the I2C bus pullup resistors (2.2k resistors).
        # This command is only effective on v2.0 hardware or greater.
        # The pullup resistors on the v1.02 hardware are enabled by default.
        ret = aa_i2c_pullup(handle, AA_I2C_PULLUP_BOTH)
        if ret < 0:
            raise Exception('Error enabling I2C pullup for Aardvark device ' +
                            'on port {0}: {1}'.format(
                    port, aa_status_string(ret)))

        print 'Connected on port {0}'.format(port)

        # return the handle
        return handle
    
    # Make sure handle is closed
    except Exception as e:
        aa_close(handle)
        raise e

# Register addresses on the NEEM sensor
STATUS_ADDRESS = 0x52 # for writing chamber temp and pump voltage, and reading
                      # those values as well as status and hygrometry
SENSOR_ADDRESS = 0x57 # for writing heater tension and reading sensor
                      # resistances

def set_chamber_temp_and_pump_voltage(handle, chamber_temp, pump_voltage):
    # build array to write
    data_out = array_u08(2)
    data_out[0] = chamber_temp * 2
    data_out[1] = pump_voltage * 20

    # write data
    ret = aa_i2c_write(handle, STATUS_ADDRESS, AA_I2C_NO_FLAGS, data_out)
    if ret < 0:
        raise Exception('Error writing chamber temp and pump volatge: {0}'.format(
                aa_status_string(ret)))

    print 'Chamber temp set to {0} C and pump voltage set to {1} V'.format(
        chamber_temp, pump_voltage)

def set_heater_tensions(handle, sensor_1_voltage, sensor_2_voltage, sensor_3_voltage):
    if sensor_1_voltage > 2.39:
        raise Exception('The voltage for sensor 1 cannot be larger than ' +
                        '2.39 volts or it might damage the sensor')

    # define function to convert voltage to written value
    def volts_to_value(voltage):
        val = int(round(57.72 * voltage - 50.95))

        # coerce to proper range
        if val < 0:
            val = 0
        elif val > 255:
            val = 255
            
        return val

    # build data structure to write
    data_out = array_u08(4)
    data_out[0] = 0x06
    data_out[1] = volts_to_value(sensor_1_voltage)
    data_out[2] = volts_to_value(sensor_2_voltage)
    data_out[3] = volts_to_value(sensor_3_voltage)

    # write data
    ret = aa_i2c_write(handle, SENSOR_ADDRESS, AA_I2C_NO_FLAGS, data_out)
    if ret < 0:
        raise Exception('Error writing sensor tensions: {0}'.format(
                aa_status_string(ret)))

    def print_success(sensor_num, voltage):
        print 'Sensor {0} tension set to {1} V'.format(sensor_num, voltage)

    for (num, voltage) in [(1, sensor_1_voltage), (2, sensor_2_voltage),
                           (3, sensor_3_voltage)]:
        print_success(num, voltage)

# Returns current status of the sensor and returns a
# (status_code, room_temp, pump_voltage, hygrometry) tuple
def read_status(handle):
    ret, data_in = aa_i2c_read(handle, STATUS_ADDRESS, AA_I2C_NO_FLAGS, 4)
    if ret < 0:
        raise Exception('Error reading sensor status: {0}'.format(
                aa_status_string(ret)))

    # convert numbers
    status_code = data_in[0]
    room_temp = data_in[1] / 2.0
    pump_voltage = data_in[2] / 20.0
    hygrometry = data_in[3] / 100.0

    return (status_code, room_temp, pump_voltage, hygrometry)

def print_status(status_code, room_temp, pump_voltage, hygrometry):
    pump_status, temp_status = convert_status(status_code)
    print '{0}\n{1}\nRoom Temperature: {2} C\nPump Voltage: {3}\nHygrometry: {4}'.format(
        pump_status, temp_status, room_temp, pump_voltage, hygrometry)

# Given a status code for the temperature and pump status, returns text for the status.
# Returns tuple of the form (pump_status, temp_status)
def convert_status(status_code):
    # get pump status
    if status_code % 2 == 1:
        pump_status = 'Pump OK'
    elif (status_code / 2) % 1 == 1:
        pump_status = 'Too much pump current!!!'
    else:
        pump_status = 'No loading or pump stopped'

    # get temperature status
    if (status_code / 4) % 2 == 1:
        temp_status = 'Not OK: temp outside +/- .5 degrees C'
    else:
        temp_status = 'Temp OK'

    return pump_status, temp_status

# Returns a seq of sensor resistances in ohms
def read_sensors(handle):
    # Send preliminare write signal
    data_out = array_u08(1)
    data_out[0] = 0x00



    (ret, num_written, data_in, num_read) = aa_i2c_write_read(handle, SENSOR_ADDRESS, AA_I2C_NO_FLAGS, data_out, array_u08(6))
    if ret < 0:
        raise Exception('Error reading sensor resistances: {0}'.format(
                aa_status_string(ret)))

    sensor_readings = []
    for i in range(3):
        # add the two bytes
        try:
            value = (data_in[2*i] << 8) + data_in[2*i + 1]

            # convert to ohms
            resistance = value * 20
            
        except IndexError:
            print('Error reading resistance {0}, setting value to -1')
            resistance = -1

        # append to the sensor_readings list
        sensor_readings.append(resistance)

    return sensor_readings

def print_readings(sensor_readings):
    for i in range(len(sensor_readings)):
        print 'Sensor {0}: {1} Ohms'.format(str(i + 1), sensor_readings[i])
