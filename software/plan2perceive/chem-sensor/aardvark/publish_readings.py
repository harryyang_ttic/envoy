import lcm
import neem
import time
from aardvark_py import aa_close

# import the chemical sensor lcm type
import sys
sys.path.append('../../../build/lib/python2.6/site-packages/chemical_sensor_lcm')
from chemical_sensor_reading_t import chemical_sensor_reading_t

if __name__ == '__main__':
    # Initialize LCM instance
    lc = lcm.LCM()

    # Connect to Aardvark
    handle = neem.configure_aardvark()

    # Start the chem sensor
    neem.set_chamber_temp_and_pump_voltage(handle, 40, 5)
    neem.set_heater_tensions(handle, 2.38, 5.1, 5.1)

    # Get and publish readings indefinitely
    try:
        while True:
            time.sleep(1)

            (status_code, chamber_temp, pump_voltage, hydrometry) = neem.read_status(handle)

            neem.print_status(status_code, chamber_temp, pump_voltage, hydrometry)
            
            # read the sensor
            sensor_resistances = neem.read_sensors(handle)

            # create a lcm message, populate it
            msg = chemical_sensor_reading_t()
            msg.utime = int(time.time() * 1000)
            msg.sensor1_resistance = sensor_resistances[0]
            msg.sensor2_resistance = sensor_resistances[1]        
            msg.sensor3_resistance = sensor_resistances[2]
            msg.chamber_temperature = chamber_temp
            msg.pump_voltage = pump_voltage
            msg.hydrometry = hydrometry

            print msg.utime, msg.sensor1_resistance, msg.sensor2_resistance, msg.sensor3_resistance
            
            lc.publish('CHEMICAL_SENSOR', msg.encode())
    except KeyboardInterrupt:
        pass
    finally:
        neem.set_chamber_temp_and_pump_voltage(handle, 0, 0)
        aa_close(handle)
