from aardvark_py import *
from neem import *

try:
    handle = configure_aardvark()

    # shut down furnace, pump 
    set_chamber_temp_and_pump_voltage(handle, 0, 0)
    
finally:                 
    # close the aardvark connection
    aa_close(handle)
