import lcm
import time

from exlcm import example_t

lc = lcm.LCM()

msg = example_t()
msg.reading = int(time.time() * 1000000)

lc.publish("NEEM_READING", msg.encode())
