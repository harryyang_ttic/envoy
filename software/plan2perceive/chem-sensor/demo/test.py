import sys
sys.path.append('../../../externals/libbot2/trunk/bot2-lcmgl/python/src')

import bot_lcmgl.data_t as data_t
from bot_lcmgl.lcmgl import *

import array
import math
import lcm
lcm = lcm.LCM()
a = array.array('B')
width = 100
height = 100
g = lcmgl('lcmgl_texture', lcm)
for y in range(height):
    for x in range(width):
        v = math.sin(x / 5.) + math.cos(y / 5.)
        a.append(int(v * 50 + 127))
img_data = a.tostring()
print len(img_data)
tex_id = g.texture2d(img_data, width, height, LCMGL_LUMINANCE, LCMGL_COMPRESS_NONE)
g.glColor3f(0, 0, 1)
g.textureDrawQuad(tex_id, 
        (-10, 10, 0),
        (-10, -10, 0),
        (10, -10, 0),
        (10, 10, 0))
g.switch_buffer()

import time
g = lcmgl('lcmgl_test', lcm)
for i in range(90):
    g.glLineWidth(1 + i / 25.0)
    g.glColor3f(1, 1, 0)

    px, py = 0, 0
    nx, ny = 0, 0

    g.glBegin(GL_LINES)
    for j in range(i):
        nx = 0.1 * j * math.cos(j / 5.)
        ny = 0.1 * j * math.sin(j / 5.)
        g.glVertex3d(px, py, 0)
        g.glVertex3d(nx, ny, 0)
        px, py = nx, ny
    g.glEnd()

    g.glEnable(GL_LIGHTING)
    g.glMaterialf(GL_FRONT, GL_AMBIENT, 0, 0.7, 0, 1);
    g.glMaterialf(GL_FRONT, GL_DIFFUSE, 0, 0.7, 0, 1);

    g.sphere(nx, ny, 0, 1, 10, 10)

    g.switch_buffer()
    time.sleep(0.05)
