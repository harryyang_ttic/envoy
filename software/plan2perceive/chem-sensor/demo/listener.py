import lcm
import sys
sys.path.append('../../../externals/libbot2/bot2-core/lcmtypes/python/bot_core')
sys.path.append('../lcmtypes/python/chemical_sensor_lcm')

from pose_t import pose_t
from chemical_sensor_reading_t import chemical_sensor_reading_t
from threading import Lock

pose_lock = Lock()
pose = [0,0,0]

def pose_handler(channel, data):
    msg = pose_t.decode(data)
#    print("Received message on channel \"%s\"" % channel)
    with pose_lock:
        global pose
        pose = msg.pos
#    print("")

def reading_handler(channel, data):
    msg = chemical_sensor_reading_t.decode(data)
    print("Received message on channel \"%s\"" % channel)
    with pose_lock:
        print("   pos0    = %s" % str(pose[0]))
    print("   pos1    = %s" % str(pose[1]))
    print("   pos2    = %s" % str(pose[2]))
    print("   utime   = %s" % str(msg.utime))
    print("   res     = %s %s %s" % ( str(msg.sensor1_resistance)
                                     , str(msg.sensor2_resistance),
                                      str(msg.sensor3_resistance) ) )
    print msg
    with pose_lock:
        f = open('TestData','a')
        f.write('%d %f %f %f %f %f %f\n' % ( msg.utime,pose[0],pose[1],pose[2],msg.sensor1_resistance,msg.sensor2_resistance,msg.sensor3_resistance ))
        f.close()
    print("")

lc = lcm.LCM()
subscription = lc.subscribe("POSE", pose_handler)
subscription2 = lc.subscribe("CHEMICAL_SENSOR",reading_handler)

try:
    while True:
        lc.handle()
except KeyboardInterrupt:
    pass

lc.unsubscribe(subscription)
lc.unsubscribe(subscription2)
