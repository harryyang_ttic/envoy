procman file: er_wheelchair.cfg
start:
   param_server
   map_server (234)
   all core
   laser_0
   laser_1
   localizer
   viewer

 ./scan-matcher -l SKIRT_FRONT -p

to run publish_readings.py must use sudo
also, must set the LCM broadcast address for the root user
so run:
$ sudo su root
$ export LCM_DEFAULT_URL=udpm://239.255.12.67:6712?ttl=1
$ python publish_readings.py
