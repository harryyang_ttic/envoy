import lcm
import time

# import the chemical sensor lcm type
import sys
sys.path.append('../lcmtypes/python/chemical_sensor_lcm')
from chemical_sensor_reading_t import chemical_sensor_reading_t

# Initialize LCM instance
lc = lcm.LCM()

# Get and publish readings indefinitely
try:
    while True:
        time.sleep(1)

            # create a lcm message, populate it
        msg = chemical_sensor_reading_t()
        msg.utime = int(time.time() * 1000)
        msg.sensor1_resistance = 1000
        print("   resistance    = %s" % str(msg.sensor1_resistance))
        msg.sensor2_resistance = 20        
        msg.sensor3_resistance = 30

        lc.publish('CHEMICAL_SENSOR', msg.encode())
except KeyboardInterrupt:
    pass
