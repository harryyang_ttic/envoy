FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/lcmtypes_chem-sensor_jar"
  "lcmtypes_chem-sensor.jar"
  "../lcmtypes/java/chemical_sensor_lcm/chemical_sensor_reading_t.class"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/lcmtypes_chem-sensor_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
