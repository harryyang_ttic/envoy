FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/pyc_files"
  "../lcmtypes/python/chemical_sensor_lcm/__init__.pyc"
  "../lcmtypes/python/chemical_sensor_lcm/chemical_sensor_reading_t.pyc"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/pyc_files.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
