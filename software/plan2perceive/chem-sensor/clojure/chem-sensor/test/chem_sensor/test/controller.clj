(ns chem-sensor.test.controller
  (:use [chem-sensor.controller])
  (:use [clojure.test]))

(deftest dist-test
  (is (== 9 (dist {:x 1 :y 1 :z 1} {:x 2 :y 5 :z 9}))))

(deftest analyze-sections-test
  (is (= [(/ Math/PI 4) 1 2] [(/ (* 3 Math/PI) 4) 2 2] [(/ (* 5 Math/PI) 4) 3 2] [(/ (* 7 Math/PI) 4) 4 2] (analyze-sections {:x 0 :y 0 :z 0 :v 0} [{:x 3 :y 4 :z 0 :v 0} {:x 4 :y 3 :z 0 :v 10} {:x (- 3) :y 4 :z 0 :v 5} {:x (- 4) :y 3 :z 0 :v 15} {:x (- 3) :y (- 4) :z 0 :v 10} {:x (- 4) :y (- 3) :z 0 :v 20} {:x 3 :y (- 4) :z 0 :v 15} {:x 4 :y (- 3) :z 0 :v 25} ] 4 10) )))