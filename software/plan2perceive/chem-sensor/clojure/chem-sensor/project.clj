(defproject chem-sensor "1.0.0-SNAPSHOT"
  :description "FIXME: write description"
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [incanter "1.3.0" :exclusions [incanter/incanter-mongodb]]
                 [log4j/log4j "1.2.16" :exclusions [java.mail/mail
                                                    javax.jms/jms
                                                    com.sun.jdmk/jmxtools
                                                    com.sun.jmx/jmxri]]
                 [org.clojure/tools.logging "0.2.4"]
                 [me.raynes/conch "0.4.0"]
                 [org.clojure/tools.cli "0.2.2"]]
  :local-repo "local-m2"
  :resource-paths ["lcm-jars/*" "resources"]
  :main chem-sensor.controller
  :min-lein-version "2.0.0")