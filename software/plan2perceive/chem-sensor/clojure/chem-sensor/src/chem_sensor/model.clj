(ns chem-sensor.model
  (:require [chem-sensor.config :as config]
            [incanter.stats :as stats])
  (:use [incanter.core]))

(def height-offset
  (ref (:height-offset (config/read-tag :model-params))))

(defn set-height-offset [k]
  (dosync (alter height-offset (fn [x] k))))

(defn measurement
  "Takes a state vector and generates a concentration according to
  q * exp[-|x_r - x_s|^2/v^2] + k."
  [x_robot x_source]
  (let [dist-sq (apply + (pow (minus x_robot x_source) 2)) ; distance squared from robot to source
        model-params (config/read-tag :gaussian-model-params)
        q (:amplitude-coefficient model-params)
        v (:width-coefficient model-params)
        k @height-offset]
    (+ k (* q (exp (minus (/ dist-sq (pow v 2))))))))

(defn consolidate-readings
  "Takes a seq of readings and manipulates them into a single number to
  pass to the estimator"
  [readings]
  (Math/log (/ (reduce + 0 (map #(/ %) readings)) (count readings))))

(defn likelihood
  "Returns the likelihood that the source is at position x given a concentration
   measurement at x_robot. x, x_robot are in the form [x y z]"
  [x readings x_robot]
  (let [conc-ideal (measurement x_robot x)
        meas-noise (:measurement-noise (config/read-tag :gaussian-model-params))
        conc-meas (consolidate-readings readings)]
    (stats/pdf-normal conc-meas :mean conc-ideal :sd meas-noise)))

(defn meas->readings
  "Converts a concentration to a reading seq"
  [meas]
  [(/ (Math/exp meas))])

(defn make-noisy [readings x_robot x_source]
  (let [conc (consolidate-readings readings)
        noise (:measurement-noise (config/read-tag :gaussian-model-params))
        conc-new (stats/sample-normal 1 :mean conc :sd noise)]
    (meas->readings conc-new)))