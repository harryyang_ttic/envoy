(ns chem-sensor.mt-model
  (:require [chem-sensor.config :as config]
            [incanter.stats :as stats])
  (:use [incanter.core]))

(defn distance
  [x_robot x_source]
  (Math/sqrt (apply + (pow (minus x_robot x_source) 2))))

(defn measurement
  [x_robot x_source]
  (let [dist (distance x_robot x_source)
        model-params (config/read-tag :mt-model-params)]
    (if (< dist (:peak-radius model-params))
      (:max-reading-variance model-params)
      (:min-reading-variance model-params))))

(defn measurement-noise
  "Calculates the predicted measurement noise as a function of distance"
  [x_robot x_source]
  (let [dist (distance x_robot x_source)
        model-params (config/read-tag :mt-model-params)
        max-var (:max-variance-noise model-params)]
    (if (< dist (:peak-radius model-params))
      max-var
      (let [min-var (:min-variance-noise model-params)
            sigma (:variance-noise-width model-params)]
        (+ (* (- max-var min-var)
              (Math/exp (- (pow (/ dist sigma) 2))))
           min-var)))))

(defn consolidate-readings
  "Takes a seq of readings and manipulates them into a single number to
  pass to the estimator"
  [readings]
  (stats/variance readings))

(defn likelihood
  "Returns the likelihood that the source is at position x given a concentration
   measurement at x_robot. x, x_robot are in the form [x y z]"
  [x readings x_robot]
  (let [meas-pred (measurement x_robot x)
        meas-noise (measurement-noise x_robot x)
        meas (consolidate-readings readings)]
    (stats/pdf-normal meas :mean meas-pred :sd meas-noise)))

(defn meas->readings
  [meas]
  (stats/sample-normal 30 :mean 3000 :sd (Math/sqrt meas)))

(defn make-noisy [meas x_robot x_source]
  (let [noise (measurement-noise x_robot x_source)]
    (stats/sample-normal 1 :mean meas :sd noise)))