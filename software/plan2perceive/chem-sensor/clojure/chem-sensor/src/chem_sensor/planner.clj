(ns chem-sensor.planner
  (:require [chem-sensor.config :as config]
            [chem-sensor.particle-filter :as estimator])
  (:use [incanter.core]))

(defn directional-goal-pose
  "Returns a new goal pose based on the current estimate. Goal in form of
   (x y z roll pitch yaw). Goal set in the x-y plane in the direction of
   the source estimate, with some stochasticity added. (Parameters in xml
   config file). The stochasticity is multiplied by the attempt number."
  [bot-pose estimate attempt] 
  (let [robot-pose (:pos bot-pose)
        x_r [(:x robot-pose) (:y robot-pose) (:z robot-pose)]
        x_s estimate
        theta (Math/atan2
               (- (second x_s) (second x_r))
               (- (first x_s) (first x_r)))
        planner-params (config/read-tag :planner-params)
        dist-percentage (:movement-percentage planner-params)
        dist (Math/sqrt (reduce + (pow (minus x_r x_s) 2)))
        move-dist (max (* dist-percentage dist)
                       (:minimum-movement-distance planner-params))
        eta (* attempt (:stochasticity planner-params))
        movement-theta (+ theta (- (/ eta 2) (rand eta)))
        x_goal [(+ (first x_r) (* move-dist (cos movement-theta)))
                (+ (second x_r) (* move-dist (sin movement-theta)))
                (last x_r)]
        theta_goal (Math/atan2
                    (- (second x_s) (second x_goal))
                    (- (first x_s) (first x_goal)))]
    (flatten [x_goal 0.0 0.0 theta_goal])))

(defn goal-pose
  [bot-pose estimate attempt]
  (let [stochasticity (:stochasticity (config/read-tag :planner-params))]
    (if (> stochasticity 0)
     (flatten [(estimator/sample-particle-pose) 0.0 0.0 0.0])
     (directional-goal-pose bot-pose estimate attempt))))

