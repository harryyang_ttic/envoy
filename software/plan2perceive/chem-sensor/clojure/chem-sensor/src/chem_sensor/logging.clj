(ns chem-sensor.logging
  (:require [chem-sensor.conversions :as convert]
            [chem-sensor.config :as config]
            [chem-sensor.model :as model]
            [chem-sensor.particle-filter :as estimator]
            [clojure.string :as string]
            [clojure.java.io :as io]
            [conch.core :as sh]
            [clojure.tools.logging :as log])
  (:import [chemical_sensor_lcm chemical_sensor_reading_t]
           [lcm.lcm LCM]
           [lcm.lcm LCMSubscriber]
           [java.text SimpleDateFormat]
           [java.util Date]))

(def date-format (SimpleDateFormat. "yyyy-MM-dd_HH-mm-ss"))

(defn time-string []
  (.format date-format (Date.)))

(defn tab-delimited-ln [& args]
  (str (string/join "\t" (flatten args)) "\n"))

(def log-folder (ref nil))

(defn set-log-folder
  "Sets the log-folder, making sure it exists"
  [folder]
  (let [folder (io/file folder)]
    (io/make-parents (io/file folder "test"))
    (dosync (ref-set log-folder folder))))

(defn estimator-log-file [folder]
  (io/file folder "estimator.log"))

(defn start-kalman-filter-log [folder]
  (let [file (estimator-log-file folder)]
    (spit file (tab-delimited-ln "time" "x_r" "y_r" "z_r" "q_r_i" "q_r_j" "q_r_k" "q_r_s" "mu_x" "mu_y" "my_z" "sig_xx" "sig_xy" "sig_xz" "sig_yz" "sig_yy" "sig_yz" "sig_zx" "sig_zy" "sig_zz" "meas" "readings"))))

(defn start-particle-filter-log [folder]
  (let [file (estimator-log-file folder)]
    (spit file (tab-delimited-ln "time" "x_r" "y_r" "z_r"
                                 "q_r_i" "q_r_j" "q_r_k" "q_r_s"
                                 "mu_x" "mu_y" "my_z"
                                 "meas" "readings"))))

(defn start-estimator-log [folder]
  (start-particle-filter-log folder))

(defn append-estimate [bot-pose estimate readings]
  (let [file (estimator-log-file @log-folder)
        m_p (:pos bot-pose)
        pose [(:x m_p) (:y m_p) (:z m_p)]
        m_q (:orientation bot-pose)
        quat [(:i m_q) (:j m_q) (:k m_q) (:s m_q)]
        time (:utime bot-pose)
        meas (model/consolidate-readings readings)]
    (spit file (tab-delimited-ln time pose quat estimate meas readings)
          :append true)))

(defn readings-log-file [folder]
  (io/file folder "readings.log"))

(defn start-readings-log [folder]
  (let [file (readings-log-file folder)]
    (spit file (tab-delimited-ln "time" "x_r" "y_r" "z_r" "q_r_i" "q_r_j" "q_r_k" "q_r_s" "sensor1" "sensor2" "sensor3" "chamber_temp" "pump_voltage" "hydrometry"))))

(defn append-reading [bot-pose reading]
  (let [file (readings-log-file @log-folder)
        m_p (:pos bot-pose)
        pose [(:x m_p) (:y m_p) (:z m_p)]
        m_q (:orientation bot-pose)
        quat [(:i m_q) (:j m_q) (:k m_q) (:s m_q)]]
    (spit file (tab-delimited-ln (:utime reading) pose quat (:sensor1 reading)
                                 (:sensor2 reading) (:sensor3 reading)
                                 (:chamber_temp reading) (:pump_voltage reading)
                                 (:hydrometry reading))
          :append true)))

(def lcm-logger (ref nil))

(defn start-lcm-logger
  "Starts a lcm logger process, sets the lcm-logger ref to be the process"
  [folder]
  (let [log-file (io/file folder "lcmlog.log")
        proc (sh/proc "lcm-logger" (.getAbsolutePath log-file))]
    (log/info "Started lcm-logger.")
    (dosync (ref-set lcm-logger proc))))

(defn stop-lcm-logger []
  (sh/destroy @lcm-logger)
  (log/info "Killed lcm-logger"))

(defn init-logging
  "Initializes logging, and returns the folder where files is being logged"
  []
  (set-log-folder (io/file (config/read-tag :root-log-folder) (time-string)))
  (io/copy config/config-file (io/file @log-folder (.getName config/config-file)))
  (start-estimator-log @log-folder)
  (start-readings-log @log-folder)
  (start-lcm-logger @log-folder))

(defn print-particles [folder particles]
  (spit (io/file folder "particles.log")
        (apply str (for [p particles]
                     (tab-delimited-ln (:x p) (:y p) (map double (:p p)))))))

(defn stop-logging
  "Stops logging processes"
  []
  (print-particles @log-folder @estimator/particles)
  (stop-lcm-logger))