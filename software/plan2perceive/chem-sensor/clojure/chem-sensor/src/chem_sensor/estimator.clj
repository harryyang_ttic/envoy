(ns chem-sensor.estimator
  (:require [chem-sensor.config :as config]
            [chem-sensor.logging :as logging]
            [chem-sensor.model :as modle]
            [clojure.tools.logging :as log]
            [clojure.string :as str])
  (:use [incanter.core]
        [chem-sensor.kalman-filter]))

;; The state estimate of the chemsensor system is described by the position
;; vector of the source location

;; estimate is a seq where the first component is the state vector and the
;; second component is the covariance matrix

(def estimate (ref nil))

(def robot-pose (ref nil))

(def update-time (ref nil))

(defn log-estimator-state [bot-pose meas meas-pred readings]
  (let [est @estimate
        [mu sig] est]
    (log/infof "Estimate: [%s] Trace of covariance: %.4f"
               (str/join " " (map #(format "%.2f" (float %)) mu))
               (float (trace sig)))
    (logging/append-estimate @update-time bot-pose est meas meas-pred readings)))

(defn update-pose
  "Updates robot-pose with a map in the form of a pose_t map"
  [bot-pose]
  (let [pose (:pos bot-pose)
        time (:utime bot-pose)]
    (dosync
     (ref-set robot-pose [(:x pose) (:y pose) (:z pose)])
     (ref-set update-time time))))

;; small value so entries are non-zero
(def eps 1e-3)

(defn generate-init-estimate
  "Generates an initial estimate from room parameters"
  []
  (let [room-params (config/read-tag (keyword (config/read-tag :room)))]
    [[(:center-x room-params) (:center-y room-params) 0.0]
     (matrix [[(:length-x room-params) 0 0]
              [0 (:length-y room-params) 0]
              [0 0 eps]])]))

(defn state-fn
  "Predicts the new state of the system from a command and the previous state.
  Because movement of the robot will not move the source, we simply return the
  source"
  [command state]
  state)

(defn meas-fn
  "Predicts a sensor reading based on the state. 
  TODO: mabe work with a concentration value that is not between 0 and 1"
  [state]
  (matrix [(model/concentration @robot-pose state)]))

(defn state-jacobian 
  "Jacobian for the state function."
  [command state]
  (matrix [[1 0 0]
           [0 1 0]
           [0 0 1]]))

(defn meas-jacobian
  "Jacobian matrix for the measurement function"
  [state]
  (let [x_r @robot-pose ; robot pose
        x_s state ; source pose
        concentration-value (model/concentration x_r x_s)
        model-params (config/read-tag :gaussian-model-params)
        v (:width-coefficient model-params)]
    (mult
     2 concentration-value (/ (pow v 2))
     (matrix [[(- (first x_r) (first x_s)) (- (second x_r) (second x_s)) (- (last x_r) (last x_s))]]))))

(def EKF (ref nil))

(defn set-EKF
  "Sets the EKF function for the estimator"
  [state-fn meas-fn state-jacobian meas-jacobian state-cov meas-cov]
  (dosync (ref-set EKF (generate-EKF state-fn meas-fn state-jacobian meas-jacobian state-cov meas-cov))))

(defn init-estimator
  "Initializes the estimator with a bot-pose in the form of a pose_t map and a timestring containing the trial start time"
  [bot-pose init-estimate process-noise meas-noise]
  (dosync
   (update-pose bot-pose)
   (ref-set estimate init-estimate)
   (set-EKF state-fn meas-fn state-jacobian meas-jacobian
            (matrix [[process-noise 0 0]
                     [0 process-noise 0]
                     [0 0 eps]])
            (matrix [meas-noise])))
  (log-estimator-state bot-pose nil nil [])
  @estimate)



(defn update-estimate
  "Given a the robot pose and an average measurement, updates the estimate; logs everything"
  [bot-pose readings]
  (update-pose bot-pose) ; updates the pose to the position of the current measurement
  (let [[mu sig] @estimate
        meas (model/consolidate-readings readings)
        [[new-mu new-sig] meas-pred] (@EKF mu sig @robot-pose (matrix [meas]))]
    (dosync
     (ref-set estimate [new-mu new-sig]))
    (log-estimator-state bot-pose meas meas-pred readings)
    [new-mu new-sig]))