(ns chem-sensor.simulation
  (:require [chem-sensor.particle-filter :as estimator]
            [chem-sensor.config :as config]
            [chem-sensor.logging :as logging]
            [chem-sensor.model :as model]
            [incanter.charts :as charts]
            [chem-sensor.planner :as planner]
            [incanter.stats :as stats])
  (:use [incanter.core]))

(defn random-pose
  "Gives a random pose where the robot can operate"
  []
  (let [room-params (config/read-tag (keyword (config/read-tag :room)))
        pos (fn [center length] (+ (- center (/ length 2)) (rand length)))]
    [(pos (:center-x room-params) (:length-x room-params))
     (pos (:center-y room-params) (:length-x room-params))
     0]))

(def source-pose
  (let [pose-map (config/read-tag :chem-pose)]
    [(:x pose-map)
     (:y pose-map)
     (:z pose-map)]))

(defn bot-pose-map [[x y z]]
  "Mimic a bot-pose-map as would be converted from a pose_t lcm message"
  {:pos {:x x
         :y y
         :z z}
   :orientation {:i 0
                 :j 0
                 :k 0
                 :s 1}
   :utime 0})

(def bot-pose (ref (random-pose)))

(defn set-bot-pose [robot-pose]
  (dosync (ref-set bot-pose (bot-pose-map robot-pose))))

(defn run [times]
  (estimator/init)
  (config/init)
  (println "Initialized")
  (set-bot-pose (random-pose))
  (logging/set-log-folder (clojure.java.io/file (config/read-tag :root-log-folder) "simulations"))
  (logging/start-estimator-log @logging/log-folder)
  (doseq [i (range times)]
    (let [robot-pose (planner/goal-pose @bot-pose (estimator/best-estimate) 1)
          conc (model/measurement robot-pose source-pose)
          bot-pose (bot-pose-map robot-pose)
          readings (model/make-noisy (model/meas->readings conc) robot-pose source-pose)
          estimate (estimator/update-estimate bot-pose readings)]
      (println "at" i)
      (set-bot-pose robot-pose)
      (logging/append-estimate bot-pose estimate readings)))
  (logging/print-particles @logging/log-folder
                           @estimator/particles))