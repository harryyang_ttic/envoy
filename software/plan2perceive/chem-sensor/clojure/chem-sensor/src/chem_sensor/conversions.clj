(ns chem-sensor.conversions
  (:import [chemical_sensor_lcm chemical_sensor_reading_t]
           [bot_core pose_t]
           [erlcm point_t]
           [erlcm navigator_goal_msg_t]
           [erlcm navigator_floor_goal_msg_t]))

(defn chem_sensor_reading_t->m [reading]
  (zipmap [:utime :sensor1 :sensor2 :sensor3 :chamber_temp :pump_voltage :hydrometry]
          [(.utime reading)
           (.sensor1_resistance reading)
           (.sensor2_resistance reading)
           (.sensor3_resistance reading)
           (.chamber_temperature reading)
           (.pump_voltage reading)
           (.hydrometry reading)]))

(defn m->chem_sensor_reading_t [m]
  (let [obj (chemical_sensor_reading_t.)]
    (do
      (set! (.utime obj) (:utime m))
      (set! (.sensor1_resistance obj) (:sensor1 m))
      (set! (.sensor2_resistance obj) (:sensor2 m))
      (set! (.sensor3_resistance obj) (:sensor3 m))
      (set! (.chamber_temperature obj) (:chamber_temp m))
      (set! (.pump_voltage obj) (:pump_voltage m))
      (set! (.hydrometry obj) (:hydrometry m))
      obj)))

; better way to do it, but can't quite get it working  (doseq [x ['utime 'sensor1 'sensor2 'sensor3]] (set! (. reading x) ((keyword x) m)))

(defn pose_t->m [x]
  (zipmap [:utime :pos :vel :orientation :rotation_rate :accel]
          [(.utime x)
           (zipmap [:x :y :z] (seq (.pos x)))
           (zipmap [:x :y :z] (seq (.vel x)))
           (zipmap [:i :j :k :s] (seq (.orientation x)))
           (zipmap [:roll :pitch :yaw] (seq (.rotation_rate x)))
           (zipmap [:x :y :z] (seq (.accel x)))]))

 (defmacro set-keys! [pose m k klst]
  `(set! (. ~pose ~(symbol (name k)))
         (double-array (map #(% (~k ~m)) ~klst))))

(defmacro m->pose_t [m]
  (let [pose (pose_t.)]
    `(set! (.utime ~pose) (:utime ~m))
    `(set-keys! ~pose ~m :pos [:x :y :z])
    `(set-keys! ~pose ~m :vel [:x :y :z])
    `(set-keys! ~pose ~m :oreintation [:i :j :k :s])
    `(set-keys! ~pose ~m :rotation_rate [:roll :pitch :yaw])
    `(set-keys! ~pose ~m :accel [:x :y :z])   
    pose))

(defn pose-map->vec
  "Takes a map representing a pose_t and returns a [x y z] vec"
  [pose-map]
  (let [pose (:pos pose-map)]
    [(:x pose) (:y pose) (:z pose)]))

(defn point_t->m [x]
  (zipmap
   [:x :y :z :yaw :pitch :roll]
   [(.x x)
    (.y x)
    (.z x)
    (.yaw x)
    (.pitch x)
    (.roll x)]))

(defn m->point_t [m]
  (let [obj (point_t.)]
    (set! (.x obj) (:x m))
    (set! (.y obj) (:y m))
    (set! (.z obj) (:z m))
    (set! (.yaw obj) (:yaw m))
    (set! (.pitch obj) (:pitch m))
    (set! (.roll obj) (:roll m))
    obj))

(defn navigator_goal_msg_t->m [x]
  (zipmap
   [:utime :goal :vel :yaw_dir :use_theta :sender :nonce]
   [(.utime x)
    (point_t->m (.goal x))
    (.velocity x)
    (.yaw_direction x)
    (.use_theta x)
    (.sender x)
    (.nonce x)]))

(defn m->navigator_goal_msg_t [m]
  "Convert a map with the fields :utime, :goal, :vel, :yaw_dir, :use_theta, :sender, and :nonce to a navigator_goal_msg_t"
  (let [x (navigator_goal_msg_t.)]
    (set! (.utime x) (:utime m))
    (set! (.goal x) (m->point_t (:goal m)))
    (set! (.velocity x) (:vel m))
    (set! (.yaw_direction x) (:yaw_dir m))
    (set! (.use_theta x) (:use_theta m))
    (set! (.sender x) (:sender m))
    (set! (.nonce x) (:nonce m))
    x))

(defn navigator_floor_goal_msg_t->m [x]
  (zipmap
   [:goal_msg :floor_no]
   [(navigator_goal_msg_t->m (.goal_msg x))
    (.floor_no x)]))

(defn m->navigator_floor_goal_msg_t [m]
  (let [x (navigator_floor_goal_msg_t.)]
    (set! (.goal_msg x) (m->navigator_goal_msg_t (:goal_msg m)))
    (set! (.floor_no x) (:floor_no m))
    x))

(comment ;;Trying to make it work with a macro...should check out source for -> and ->>
  (defn map->pose_t [m]
   (let [pose (pose_t.)]
     (do
       (set! (.utime pose) (:utime m))
       (map (fn [[x y]] (set-keys! pose m x y)) [[:pos [:x :y :z]]
                                                [:vel [:x :y :z]]
                                                [:orientation [:i :j :k :s]]
                                                [:rotation_rate [:roll :pitch :yaw]]
                                                [:accel [:x :y :z]]])))))