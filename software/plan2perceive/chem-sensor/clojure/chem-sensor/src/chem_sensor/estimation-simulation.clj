(ns chem-sensor.estimation-simulation
  (:require [chem-sensor.particle_filter :as estimator]
            [chem-sensor.config :as config]
            [chem-sensor.logging :as logging]
            [incanter.charts :as charts]
            [chem-sensor.planner :as planner])
  (:use [incanter.core]))

(defn random-pose
  "Gives a random pose where the robot can operate"
  []
  (let [room-params (config/read-tag (keyword (config/read-tag :room)))
        pos (fn [center length] (+ (- center (/ length 2)) (rand length)))]
    [(pos (:center-x room-params) (:length-x room-params))
     (pos (:center-y room-params) (:length-x room-params))
     0]))

(def source-pose (random-pose))

(def history (agent []))

(defn add-estimate [e]
  (send history (fn [x] (conj x e))))

(defn bot-pose-map [robot-pose]
  "Mimic a bot-pose-map as would be converted from a pose_t lcm message"
  {:pos {:x (first robot-pose)
         :y (second robot-pose)
         :z (last robot-pose)}
   :orientation {:i 0
                 :j 0
                 :k 0
                 :s 1}
   :utime 0})

(defn bot-pose-map->robot-pose
  [bot-pose]
  (let [pose (:pos bot-pose)]
    [(:x pose) (:y pose) (:z pose)]))

(defn conc->readings
  "Converts a concentration to a reading seq"
  [conc]
  [(/ (Math/exp conc))])

(def bot-pose (ref nil))

(defn set-bot-pose [robot-pose]
  (dosync (ref-set bot-pose (bot-pose-map robot-pose))))

(defn run-estimator [times process-noise meas-noise]
  (print (bot-pose-map->robot-pose @bot-pose))
  (add-estimate (estimator/init-estimator @bot-pose
                                          [source-pose
                                           (matrix [[12.0 0 0]
                                                    [0 12.0 0]
                                                    [0 0 0.0001]])]
                                          process-noise
                                          meas-noise))
  (doseq [i (range (dec times))]
    (let [robot-pose (planner/goal-pose @bot-pose @estimator/estimate 1)
          conc (estimator/concentration robot-pose source-pose)]
      (set-bot-pose robot-pose)
      (add-estimate (estimator/update-estimate (bot-pose-map robot-pose)
                                               (conc->readings conc))))))

(defn sq-dist [x y]
  (->
   (minus x y)
   (pow 2)
   (sum)))

(defn plot []
  (let [c (charts/xy-plot [] [] :x-axis "Reading Number" :y-axis "Dist from source (red); Trace of cov (blue)")]
    (charts/add-lines c (range (count @history))
                      (map #(sq-dist source-pose (first %1)) @history))
    (charts/add-lines c (range (count @history))
                       (map #(trace (second %1)) @history))
    (view c)))

(defn run [times process-noise meas-noise]
  (send history (fn [x] []))
  (logging/set-log-folder (clojure.java.io/file (config/read-tag :root-log-folder) "simulations"))
  (logging/start-estimator-log @logging/log-folder)
  (set-bot-pose (random-pose))
  (run-estimator times process-noise meas-noise)
  (plot))