(ns chem-sensor.controller
  (:gen-class)
  (:require [chem-sensor.conversions :as convert]
            [chem-sensor.config :as config]
            [chem-sensor.particle-filter :as estimator]
            [chem-sensor.planner :as planner]
            [chem-sensor.logging :as logging]
            [chem-sensor.model :as model]
            [clojure.tools.logging :as log]
            [clojure.string :as str]
            [clojure.tools.cli :only [cli]])
  (:import [bot_core pose_t]
           [chemical_sensor_lcm chemical_sensor_reading_t]
           [lcm.lcm LCM]
           [lcm.lcm LCMSubscriber]))

(def lcm (LCM/getSingleton))

(def bot-pose (agent nil
                     :error-mode :continue))

(def reading-num (ref -1))

(defn set-reading-num [num]
  (dosync
   (ref-set reading-num num)))

(def readings (agent {-1 []} :error-mode :continue))

(defn publish-goal 
  "Takes a goal-pose ([x y z roll pitch yaw] seq), and a velocity
   to move towards that goal at"
  [goal-pose vel]
  (.publish lcm "NAV_GOAL_FLOOR"
            (convert/m->navigator_floor_goal_msg_t
             (let [time (System/currentTimeMillis)
                   [x y z roll pitch yaw] goal-pose]
               {:goal_msg {:goal {:x x
                                  :y y
                                  :z z
                                  :roll roll
                                  :pitch pitch
                                  :yaw yaw}
                           :utime time
                           :vel vel
                           :yaw_dir 0
                           :use_theta true
                           :sender 5              ; "SENDER_USAR"
                           :nonce (rand-int Integer/MAX_VALUE)}
                :floor_no 2}))))

(defn- start-updating-pose []
  "Instantiates a listener that updates bot-pose with each new message"
  (.subscribe lcm "POSE"
              (proxy [LCMSubscriber] []
                (messageReceived [lcm channel ins]
                  (try
                    (send bot-pose
                          (fn [x] (convert/pose_t->m (pose_t. ins))))
                    (catch Exception e
                      (log/error e "Error updating model bot-pose")))))))

(defn- start-updating-readings []
  "Instantiates a listener that updates measurements with each new message"
  (.subscribe lcm config/chem-channel
              (proxy [LCMSubscriber] []
                (messageReceived [lcm channel ins]
                  (let [reading (convert/chem_sensor_reading_t->m
                                 (chemical_sensor_reading_t. ins))
                        reading-num @reading-num]
                    (if (or (>= reading-num 0) (= -2 reading-num))
                      (try
                        (send readings
                              (fn [x]
                                (update-in x [reading-num]
                                           (fn [coll]
                                             (conj coll reading)))))
                        (catch Exception e
                          (log/error e "Error updating model readings"))))
                    (logging/append-reading @bot-pose reading))))))

(defn sensor2-readings
  "Returns a seq of all the sensor2 readings collected for the reading-num nun"
  [num]
  (let [readings-list (@readings num)]
    (map #(:sensor2 %1) readings-list)))

(defn calibrate-sensor
  []
  (set-reading-num -2)
  (log/info "Calibrating sensor")
  (Thread/sleep (:calibration-time (config/read-tag :controller-params)))
  (set-reading-num -1)
  (let [offset (model/consolidate-readings (sensor2-readings -2))]
    (log/infof "Model height offset set to %s" (str offset))
    (spit (clojure.java.io/file @logging/log-folder "offset") (str offset))
    (model/set-height-offset offset)))

(defn message-published? 
  "Returns a promise that will deliver true if a message comes over the LCM channel lcm-channel, and deliver false if timeout (in millis) elapses and a message has not been published on that channel yet"
  [lcm-channel timeout]
  (let [published (promise)]
    (.subscribe lcm lcm-channel
                (proxy [LCMSubscriber] []
                  (messageReceived [lcm channel ins]
                    (deliver published true))))
    (future 
      (Thread/sleep timeout)
      (deliver published false))
    published))

(defn bot-at-goal?
  "Returns a promise that will deliver true if a message comes over the WAYPOINT_STATUS LCM channel, and deliver false if timeout (in millis) elapses without a message being published on that channel"
  [timeout]
  ;; a erlcm_speech_cmd_t message comes over this channel
  (message-published? "WAYPOINT_STATUS" timeout))

(defn robot-moving-to-goal? 
  "Returns a promise that will deliver true if a message comes over the NAV_PLAN LCM channel, and deliver false if timeout (in millis) elapses and the robot has not published message on this channel yet"
  [timeout]
  ;; a erlcm_point_list_t message comes over this channel
  (message-published? "NAV_PLAN" timeout))

(defn chemical-localized? [estimate]
  "Decides if the chemical is localzed based on the passed estimate"
  (comment (let [source (config/read-tag :chem-pose)
         r (:localization-distance (config/read-tag :planner-params))
         x-diff (- (:x source) (first mu))
         y-diff (- (:y source) (second mu))
         sign (fn [x] (if (>= x 0) 1 -1))
         test (fn [x var] (< (Math/abs (+ x (* 2 (sign x) (Math/sqrt var)))) r))]))
  ;; TODO: actually implement. For now just returns false
  false)

(defn take-readings
  "Takes the nth reading.  Waits the time passed in planner-params for the air
   to settle and the takes readings for the time passed in planner-params.
   Returns the new readings."
  [n planner-params]
  (log/info "Waiting for air to settle.")
  (Thread/sleep (:air-settling-time planner-params))
  (set-reading-num n)
  (log/info "Taking readings.")
  (Thread/sleep (:readings-time planner-params)) ; collect readings
  (set-reading-num -1) ; stop taking readings
  (sensor2-readings n))

(defn move-to-planner-goal [n attempt planner-params]
  "Blocks until the robot moves to a goal the planner sets. The attempt for
   a goal is passed to the planner."
  (let [goal-pose (planner/goal-pose @bot-pose (estimator/best-estimate) attempt)
        ;; valid-goal evaluates to false if a NAV_PLAN message hasn't been
        ;; published before the timeout elapses
        valid-goal (robot-moving-to-goal? (:nav-plan-timeout planner-params))]
    ;; set the goal to move to
    (publish-goal goal-pose (:velocity planner-params))
    (if (not @valid-goal)
      ;; if it isn't a valid goal, replan the location to go to
      (do
        (log/info "Invalid goal. Trying another.")
        (move-to-planner-goal n (inc attempt) planner-params))
      ;; otherwise, wait for the robot to get to the goal
      (do
        (log/infof "Goal set to [%s]"
                   (str/join " " (map #(format "%.4f" (float %)) goal-pose)))
        (if (deref (bot-at-goal? (:reach-goal-timeout planner-params)))
          (log/info "Reached goal.")
          ;; if it doesn't get to the goal by the timeout, plan a new location
          (do
            (log/info "Timed out while waiting to reach goal. Trying another.")
            (move-to-planner-goal n (inc attempt) planner-params)))))))

(defn wait-for-manual-goal [planner-params]
  "Blocks until the robot has reached a set goal."
  (log/info "Waiting for robot to reach a manually set goal.")
  (if (deref (bot-at-goal? (:manual-reach-goal-timeout planner-params)))
    (log/info "Reached a manually set goal.")
    (wait-for-manual-goal planner-params)))

(defn take-next-reading
  "Takes a reading at a new location.  Location either manually set or set by
   the planner.  Returns the new readings"
  [n planner-params manual-goal?]
  (if manual-goal?
    (wait-for-manual-goal planner-params)
    (move-to-planner-goal n 1 planner-params))
  (take-readings n planner-params))

(defn run-robot [planner-params manual-goals?]
  "Runs the control loop for the robot until the chemical has been localized."
  (loop [n 0]
    (let [readings (take-next-reading n planner-params manual-goals?)
          pose-map @bot-pose
          estimate (estimator/update-estimate pose-map readings)]
      (logging/append-estimate pose-map estimate readings)
      (logging/print-particles @logging/log-folder @estimator/particles)
      (if (not (chemical-localized? estimate))
        (recur (inc n))
        (log/info "Chemical localized.")))))

(defn shutdown-cleanup
  "Runs the function f when the JVM is shutdown, i.e."
  [f]
  (.addShutdownHook (Runtime/getRuntime) (Thread. f)))

(defn setup-cleanup
  "Sets up the cleanup function for the system"
  []
  (logging/stop-logging))

(defn start-controller [manual-goals?]
  (let [planner-params (config/read-tag :planner-params)
        model-params (config/read-tag :gaussian-model-params)]
    (logging/init-logging)
    (start-updating-pose)
    (log/info "Waiting for pose messages...")
    (while (not @bot-pose)
      (Thread/sleep 100)) ; wait for the bot-pose to start updating, i.e. be non-nil
    (log/info "Pose messages recieved.")
    (start-updating-readings)
    (calibrate-sensor)
    (estimator/init)
    (log/info "Starting controller")
    (run-robot planner-params manual-goals?)))

;; Note: update with tools.cli
(defn -main [& args]
  (config/init)
  (start-controller (> (count args) 0)))
  (comment (let [[opts args banner] (cli args
                                 ["-m" "--[no-]manual" :default false])]
     (start-controller (:manual opts))))