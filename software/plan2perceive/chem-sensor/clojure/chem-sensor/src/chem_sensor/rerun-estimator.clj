(ns chem-sensor.rerun-estimator
  (:require [chem-sensor.estimator :as estimator]
            [chem-sensor.logging :as logging]
            [chem-sensor.config :as config]
            [incanter.charts :as charts]
            [clojure.string :as string])
  (:use [incanter.core]
        [chem-sensor.kalman-filter]))

;; can change: process noise, measurement noise, initial position
;; rerun with readings, get new

(defn parse-line [line-tokens full-line?]
  (let [read-nth (fn [n] (read-string (nth line-tokens n)))
        read-vec (fn [start len]
                   (map read-string (subvec line-tokens start (+ start len))))] 
    {:utime (read-nth 0)
     :pos (read-vec 1 3)
     :quat (read-vec 4 4)
     :mu (read-vec 8 3)
     :sig [(read-vec 11 3) (read-vec 14 3) (read-vec 17 3)]
     :meas (if full-line? (read-nth 20) nil)
     :readings (if full-line? (map read-string (subvec line-tokens 21)) [])}))

(defn read-estimator-log [file]
  (let [lines (map #(string/split % #"\t")
                   (nthrest (string/split-lines (slurp file)) 1))]
    (concat [(parse-line (first lines) false)] (map #(parse-line % true) (rest lines)))))

(defn bot-pose-map [line-map]
  "Mimic a bot-pose-map as would be converted from a pose_t lcm message"
  (let [robot-pose (:pos line-map)
        time (:utime line-map)
        quat (:quat line-map)]
    {:utime time
     :pos {:x (first robot-pose)
           :y (second robot-pose)
           :z (last robot-pose)}
     :orientation {:i (first quat)
                   :j (second quat)
                   :k (nth quat 2)
                   :s (last quat)}}))

(def history (agent []))

(defn add-estimate [e]
  (send history (fn [x] (conj x e))))

(defn run-estimator [first-map reading-maps process-noise meas-noise init-mu init-cov]
  (estimator/init-estimator (bot-pose-map first-map)
                            [init-mu init-cov]
                            process-noise meas-noise)
  (doseq [line-map reading-maps]
    (let [bot-pose (bot-pose-map line-map)]
      (add-estimate (estimator/update-estimate bot-pose (:readings line-map))))))

(defn run [file process-noise meas-noise init-mu init-cov filter-readings-fn]
  (let [file (clojure.java.io/file file)
        line-maps (read-estimator-log file)
        first-map (first line-maps)]
    (logging/set-log-folder (clojure.java.io/file (.getParent file) "estimator-rerun"))
    (logging/start-estimator-log @logging/log-folder)
    (run-estimator first-map (filter-readings-fn (rest line-maps)) process-noise meas-noise init-mu init-cov)))

(defn test []
  (let [log-file (clojure.java.io/file "/home/troyastorino/UROP/data/62x/2012-11-06_17-48-43/estimator.log")]
    (run log-file 100.0 0.00001 (matrix [18.0 -7.0 0.0]) (matrix [[10 0 0]
                                                                  [0 10 0]
                                                                  [0 0 0.01]]))))
