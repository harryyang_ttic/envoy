(ns chem-sensor.simulator
  (:use [chem-sensor.config :only [read-tag chem-channel]])
  (:require [chem-sensor.conversions :as convert]
        [chem-sensor.model :as model])
  (:import [chemical_sensor_lcm chemical_sensor_reading_t]
           [bot_core pose_t]
           [lcm.lcm LCM]
           [lcm.lcm LCMSubscriber]))

(defn publish-reading [m lcm]
  "Publishes a chemsensor reading as a LCM message."
  (.publish lcm chem-channel (convert/m->chem_sensor_reading_t m)))

(defn simulate-reading [model]
  "Given a chemical dispersion model, simulates a sensor reading."
  (let [m @model/bot-pose
        pose (:pos m)
        v (* 1e12 (model (:x pose) (:y pose) (:z pose)))]
    {:utime (System/currentTimeMillis)
     :sensor1 (int (* 1000 (rand)))
     :sensor2 (int (* 1000 (rand)))
     :sensor3 (int (* 1000 (rand)))}))

(defn simulate-sensor [interval]
  "Publish simulated sensor readings at specified interval, where interval is specified in milliseconds."
  (let [thread
        (Thread. (fn []
                   (let [lcm (LCM/getSingleton)
                         chem-pos (read-tag :chem-pose)
                         params (read-tag :model-params)]
                     (model/update-pose)
                     (while true
                       (try
                         (publish-reading
                          (simulate-reading
                           (apply model/gaussian-emission-model
                                  chem-pos
                                  (map #(eval %)
                                       [(:wind-vel params)
                                        (:vapor-pressure params)
                                        (:molar-mass params)
                                        (:source-surface-area params)
                                        (:temperature params)])))
                          lcm)
                         (catch Exception e
                           (println "Sensor simulation error: " e)))
                       (Thread/sleep interval)))))]
    (.start thread)
    thread))