(ns chem-sensor.particle-filter
  (:require [chem-sensor.config :as config]
            [chem-sensor.model :as model]
            [chem-sensor.conversions :as convert])
  (:use [incanter.core]))

(defn linnums
  "Returns a seq of num numbers where mid is the middle number and rnge is
   the range of the numbers"
  [mid rnge num]
  (let [step (/ rnge num)]
    (range (- mid (/ rnge 2)) (+ mid (/ rnge 2)) step)))

(defn gen-particles []
   (let [n (:sqrt-num-particles (config/read-tag :particle-filter-params))
         room-params (config/read-tag (keyword (config/read-tag :room)))
         center-x (:center-x room-params)
         center-y (:center-y room-params)
         dist-from-center (fn [x y]
                            (Math/sqrt
                             (apply + (pow (minus [center-x center-y] [x y]) 2))))
         r (:radius room-params)
         x-vals (linnums center-x (* 2 r) n)
         y-vals (linnums center-y (* 2 r) n)
         init-points (for [x x-vals
                           y y-vals]
                       {:x x
                        :y y})
         points (filter #(< (dist-from-center (:x %) (:y %)) r) init-points)
         num (count points)]
     (apply vector
            (map #(assoc % :p [(float (/ num))]) points))))

(def particles (ref (gen-particles)))

(defn init []
  (dosync
   (ref-set particles (gen-particles))))

(defn particle-posterior [particle x_robot readings]
  "Returns the unnormalized posterior for a particle"
  (let [x_particle [(:x particle) (:y particle) 0]
        likelihood (model/likelihood x_particle readings x_robot)
        prior (last (:p particle))]
    (* likelihood prior)))

(defn best-estimate
  []
  (let [prob (fn [particle] (last (:p particle)))]
    (reduce (fn [acc particle]
               (plus acc (mult (matrix [(:x particle) (:y particle) 0])
                               (prob particle))))
             (matrix [0 0 0])
             @particles)))

(defn update-estimate [bot-pose readings]
  (let [x_robot (convert/pose-map->vec bot-pose)
        particle-posteriors (doall (map #(particle-posterior % x_robot readings)
                                        @particles))
        prob-mass (apply + particle-posteriors)]
    (dosync
     (alter particles
            (partial map (fn [particle]
                           (let [posterior (/ (particle-posterior particle x_robot readings)
                                              prob-mass)]
                             (update-in particle [:p] conj posterior)))))))
  (best-estimate))

(defn sample-particle-pose
  "Returns a sample particle from the particle distribution. Does a cdf of the
   particles"
  []
  (let [particles @particles
        val (rand)
        prob (fn [particle] (last (:p particle)))]
    (loop [acc 0 i 0]
      (let [particle (nth particles i nil)]
        (if (not particle)
          (print "i:" i " acc:" acc))
        (let [p-val (+ acc (prob particle))]
          (if (> p-val val)
            [(:x particle) (:y particle) 0]
            (recur p-val (inc i))))))))

(defn probability-mass-in-circle
  "Finds the probability mass of particles within a circle"
  [radius [x y]]
  (let [dist (fn [xs ys] (Math/sqrt (apply + (pow (minus [x y] [xs ys]) 2))))
        particles @particles]
    (reduce (fn [acc particle]
              (+ acc
                 (if (< (dist (:x particle) (:y particle)) radius)
                   (last (:p particle))
                   0)))
            0 particles)))