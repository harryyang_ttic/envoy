(ns chem-sensor.config
  (:require [clojure.xml :as xml]))

(def tags (agent {}))

(defn init
  []
  (send tags (fn [x] {})))

(def config-file (clojure.java.io/file "config.xml"))

(defn- read-xml-content [coll]
  (if (> (count coll) 1)
    (zipmap (map #(:tag %) coll)
            (map #(read-xml-content (:content %)) coll))
    (let [item (first coll)]
      (if (= (type item) java.lang.String)
        (read-string (first coll))
        {(:tag item) (read-xml-content (:content item))}))))

(defn read-tag [key]
  "Returns the content of the tag as a map from the config file"
  (let [val (key @tags)]
    (if val
      val
      (let [value (first (for [m (xml-seq (xml/parse config-file))
                             :when (= key (:tag m))]
                         (read-xml-content (:content m))))]
        (send tags #(assoc % key value))
        value))))

(let [m (read-tag :channels)]
  (def chem-channel (str (:chem-sensor m))))
