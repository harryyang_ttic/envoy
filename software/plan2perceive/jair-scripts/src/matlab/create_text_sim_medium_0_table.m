
% read in teh analysis
d_p = dlmread( '/home/velezj/projects/object_detector_pose_planning/data/experiments/simulation/text-medium-0/text-medium-0.analysis' );
d_r07 = dlmread( '/home/velezj/projects/object_detector_pose_planning/data/experiments/simulation/random-0.7-text-medium-0/random-0.7-text-medium-0.analysis' );
d_g07 = dlmread( '/home/velezj/projects/object_detector_pose_planning/data/experiments/simulation/greedy-0.7-text-medium-0/greedy-0.7-text-medium-0.analysis' );
d_rtbss = dlmread( '/home/velezj/projects/object_detector_pose_planning/data/experiments/simulation/rtbss-text-medium-0/rtbss-text-medium-0.analysis' );


% filter out things that were too long
d_p = d_p( d_p( :, 5 ) < ( 14 * 60 * 1000000 ), : );
d_r07 = d_r07( d_r07( :, 5 ) < ( 14 * 60 * 1000000 ), : );
d_g07 = d_g07( d_g07( :, 5 ) < ( 14 * 60 * 1000000 ), : );
d_rtbss = d_rtbss( d_rtbss( :, 5 ) < ( 14 * 60 * 1000000 ), : );


% choose a cap size
cap = min( [ 40 size(d_p,1) size(d_r07,1) size(d_g07,1) size(d_rtbss,1) ] );
d_p = d_p( 1:cap, : );
d_r07 = d_r07( 1:cap, : );
d_g07 = d_g07( 1:cap, : );
d_rtbss = d_rtbss( 1:cap, : );


% get num for later use
num_d_p = size( d_p, 1 );
num_d_r07 = size( d_r07, 1 );
num_d_g07 = size( d_g07, 1 );
num_d_rtbss = size( d_rtbss, 1 );

% calculate precisoin vectors
zidx = ( d_p(:,8) + d_p(:,9) ) == 0;
d_p_precision = d_p( :,8 ) ./ ( d_p( :,8 ) + d_p( :,9 ) );
d_p_precision( zidx ) = 0;
num_d_p_precision = size( d_p_precision, 1 );

zidx = ( d_r07(:,8) + d_r07(:,9) ) == 0;
d_r07_precision = d_r07( :,8 ) ./ ( d_r07(:,8) + d_r07( :,9 ) );
d_r07_precision( zidx ) = 0;
num_d_r07_precision = size( d_r07_precision, 1 );

zidx = ( d_g07(:,8) + d_g07(:,9) ) == 0;
d_g07_precision = d_g07( :,8 ) ./ ( d_g07(:,8) + d_g07( :,9 ) );
d_g07_precision( zidx ) = 0;
num_d_g07_precision = size( d_g07_precision, 1 );

zidx = ( d_rtbss(:,8) + d_rtbss(:,9) ) == 0;
d_rtbss_precision = d_rtbss( :,8 ) ./ ( d_rtbss(:,8) + d_rtbss( :,9 ) );
d_rtbss_precision( zidx ) = 0;
num_d_rtbss_precision = size( d_rtbss_precision, 1 );


% compute precison mean and stderr
p_precision_mean = mean( d_p_precision )
p_precision_stderr = std( d_p_precision ) / sqrt( num_d_p_precision )
r07_precision_mean = mean( d_r07_precision )
r07_precision_stderr = std( d_r07_precision ) / sqrt( num_d_r07_precision )
g07_precision_mean = mean( d_g07_precision )
g07_precision_stderr = std( d_g07_precision ) / sqrt( num_d_g07_precision )
rtbss_precision_mean = mean( d_rtbss_precision )
rtbss_precision_stderr = std( d_rtbss_precision ) / sqrt( num_d_rtbss_precision )


% calculate recall
zidx = ( d_p( :,8 ) + d_p( :,10 ) ) == 0;
d_p_recall = d_p( :,8 ) ./ ( d_p(:,8) + d_p(:,10) );
d_p_recall( zidx ) = 0;
num_d_p_recall = size( d_p_recall, 1 );

zidx = ( d_r07(:,8) + d_r07(:,10) ) == 0;
d_r07_recall = d_r07(:,8) ./ ( d_r07(:,8) + d_r07(:,10) );
d_r07_recall( zidx ) = 0;
num_d_r07_recall = size( d_r07_recall, 1 );

zidx = ( d_g07(:,8) + d_g07(:,10) ) == 0;
d_g07_recall = d_g07(:,8) ./ ( d_g07(:,8) + d_g07(:,10) );
d_g07_recall( zidx ) = 0;
num_d_g07_recall = size( d_g07_recall, 1 );

zidx = ( d_rtbss(:,8) + d_rtbss(:,10) ) == 0;
d_rtbss_recall = d_rtbss(:,8) ./ ( d_rtbss(:,8) + d_rtbss(:,10) );
d_rtbss_recall( zidx ) = 0;
num_d_rtbss_recall = size( d_rtbss_recall, 1 );


% compute recall mean and stderr
d_p_recall_mean = mean( d_p_recall )
d_p_recall_stderr = std( d_p_recall ) / sqrt( num_d_p_recall )
d_r07_recall_mean = mean( d_r07_recall )
d_r07_recall_stderr = std( d_r07_recall ) / sqrt( num_d_r07_recall )
d_g07_recall_mean = mean( d_g07_recall )
d_g07_recall_stderr = std( d_g07_recall ) / sqrt( num_d_g07_recall )
d_rtbss_recall_mean = mean( d_rtbss_recall )
d_rtbss_recall_stderr = std( d_rtbss_recall ) / sqrt( num_d_rtbss_recall )


% compute path length mean and stderr
d_p_pathlength_mean = mean( d_p(:,4) )
d_p_pathlength_stderr = std( d_p(:,4) ) / size( d_p(:,4), 1 )
d_r07_pathlength_mean = mean( d_r07(:,4) )
d_r07_pathlength_stderr = std( d_r07(:,4) ) / size( d_r07( :,4) , 1 )
d_g07_pathlength_mean = mean( d_g07(:,4) )
d_g07_pathlength_stderr = std( d_g07(:,4) ) / size( d_g07( :,4) , 1 )
d_rtbss_pathlength_mean = mean( d_rtbss(:,4) )
d_rtbss_pathlength_stderr = std( d_rtbss(:,4) ) / size( d_rtbss( :,4) , 1 )


