
% the output table name
latex_table_filename = '/home/velezj/projects/object_detector_pose_planning/data/experiments/simulation/result_tables_generated/text-small-0-depth5.tex'

% read in teh analysis
d_p = dlmread( '/home/velezj/projects/object_detector_pose_planning/data/experiments/simulation/text-small-0/text-small-0.analysis' );
d_r07 = dlmread( '/home/velezj/projects/object_detector_pose_planning/data/experiments/simulation/random-0.7-text-small-0/random-0.7-text-small-0.analysis' );
d_g07 = dlmread( '/home/velezj/projects/object_detector_pose_planning/data/experiments/simulation/greedy-0.7-text-small-0/greedy-0.7-text-small-0.analysis' );
d_rtbss = dlmread( '/home/velezj/projects/object_detector_pose_planning/data/experiments/simulation/rtbss-text-small-0-depth5/rtbss-text-small-0-depth5.analysis' );


% choose a cap size
cap = min( [ 40 size(d_p,1) size(d_r07,1) size(d_g07,1) size(d_rtbss,1) ] );
d_p = d_p( 1:cap, : );
d_r07 = d_r07( 1:cap, : );
d_g07 = d_g07( 1:cap, : );
d_rtbss = d_rtbss( 1:cap, : );


% get num for later use
num_d_p = size( d_p, 1 );
num_d_r07 = size( d_r07, 1 );
num_d_g07 = size( d_g07, 1 );
num_d_rtbss = size( d_rtbss, 1 );

% calculate precisoin vectors
zidx = ( d_p(:,8) + d_p(:,9) ) == 0;
d_p_precision = d_p( :,8 ) ./ ( d_p( :,8 ) + d_p( :,9 ) );
d_p_precision( zidx ) = 0;
num_d_p_precision = size( d_p_precision, 1 );

zidx = ( d_r07(:,8) + d_r07(:,9) ) == 0;
d_r07_precision = d_r07( :,8 ) ./ ( d_r07(:,8) + d_r07( :,9 ) );
d_r07_precision( zidx ) = 0;
num_d_r07_precision = size( d_r07_precision, 1 );

zidx = ( d_g07(:,8) + d_g07(:,9) ) == 0;
d_g07_precision = d_g07( :,8 ) ./ ( d_g07(:,8) + d_g07( :,9 ) );
d_g07_precision( zidx ) = 0;
num_d_g07_precision = size( d_g07_precision, 1 );

zidx = ( d_rtbss(:,8) + d_rtbss(:,9) ) == 0;
d_rtbss_precision = d_rtbss( :,8 ) ./ ( d_rtbss(:,8) + d_rtbss( :,9 ) );
d_rtbss_precision( zidx ) = 0;
num_d_rtbss_precision = size( d_rtbss_precision, 1 );


% compute precison mean and stderr
p_precision_mean = mean( d_p_precision )
p_precision_stderr = std( d_p_precision ) / sqrt( num_d_p_precision )
r07_precision_mean = mean( d_r07_precision )
r07_precision_stderr = std( d_r07_precision ) / sqrt( num_d_r07_precision )
g07_precision_mean = mean( d_g07_precision )
g07_precision_stderr = std( d_g07_precision ) / sqrt( num_d_g07_precision )
rtbss_precision_mean = mean( d_rtbss_precision )
rtbss_precision_stderr = std( d_rtbss_precision ) / sqrt( num_d_rtbss_precision )


% calculate recall
zidx = ( d_p( :,8 ) + d_p( :,10 ) ) == 0;
d_p_recall = d_p( :,8 ) ./ ( d_p(:,8) + d_p(:,10) );
d_p_recall( zidx ) = 0;
num_d_p_recall = size( d_p_recall, 1 );

zidx = ( d_r07(:,8) + d_r07(:,10) ) == 0;
d_r07_recall = d_r07(:,8) ./ ( d_r07(:,8) + d_r07(:,10) );
d_r07_recall( zidx ) = 0;
num_d_r07_recall = size( d_r07_recall, 1 );

zidx = ( d_g07(:,8) + d_g07(:,10) ) == 0;
d_g07_recall = d_g07(:,8) ./ ( d_g07(:,8) + d_g07(:,10) );
d_g07_recall( zidx ) = 0;
num_d_g07_recall = size( d_g07_recall, 1 );

zidx = ( d_rtbss(:,8) + d_rtbss(:,10) ) == 0;
d_rtbss_recall = d_rtbss(:,8) ./ ( d_rtbss(:,8) + d_rtbss(:,10) );
d_rtbss_recall( zidx ) = 0;
num_d_rtbss_recall = size( d_rtbss_recall, 1 );


% compute recall mean and stderr
p_recall_mean = mean( d_p_recall )
p_recall_stderr = std( d_p_recall ) / sqrt( num_d_p_recall )
r07_recall_mean = mean( d_r07_recall )
r07_recall_stderr = std( d_r07_recall ) / sqrt( num_d_r07_recall )
g07_recall_mean = mean( d_g07_recall )
g07_recall_stderr = std( d_g07_recall ) / sqrt( num_d_g07_recall )
rtbss_recall_mean = mean( d_rtbss_recall )
rtbss_recall_stderr = std( d_rtbss_recall ) / sqrt( num_d_rtbss_recall )


% compute path length mean and stderr
p_pathlength_mean = mean( d_p(:,4) )
p_pathlength_stderr = std( d_p(:,4) ) / size( d_p(:,4), 1 )
r07_pathlength_mean = mean( d_r07(:,4) )
r07_pathlength_stderr = std( d_r07(:,4) ) / size( d_r07( :,4) , 1 )
g07_pathlength_mean = mean( d_g07(:,4) )
g07_pathlength_stderr = std( d_g07(:,4) ) / size( d_g07( :,4) , 1 )
rtbss_pathlength_mean = mean( d_rtbss(:,4) )
rtbss_pathlength_stderr = std( d_rtbss(:,4) ) / size( d_rtbss( :,4) , 1 )


% write out the latex table with results
tf = fopen( latex_table_filename, 'w' );
fprintf( tf, '\\begin{tabular}{ | l || l | l | l | l | } \n' );
fprintf( tf, '\\hline \n');
fprintf( tf, '{\\em Average} & Random$_{\\beta = 0.7}$ & Greedy$_{\\beta=0.7}$ & Planned & RTBSS \\\\ \\hline \n' );
fprintf( tf, 'Precision & %0.2f $\\pm$%0.2f & %0.2f $\\pm$%0.2f & %0.2f $\\pm$%0.2f & %0.2f $\\pm$%0.2f \\\\ \n', ...
	     r07_precision_mean, r07_precision_stderr, ...
	     g07_precision_mean, g07_precision_stderr, ...
	     p_precision_mean, p_precision_stderr, ...
	     rtbss_precision_mean, rtbss_precision_stderr );
fprintf( tf, 'Recall & %0.2f $\\pm$%0.2f & %0.2f $\\pm$%0.2f & %0.2f $\\pm$%0.2f & %0.2f $\\pm$%0.2f \\\\ \n', ...
	     r07_recall_mean, r07_recall_stderr, ...
	     g07_recall_mean, g07_recall_stderr, ...
	     p_recall_mean, p_recall_stderr, ...
	     rtbss_recall_mean, rtbss_recall_stderr);
fprintf( tf, 'Path Length (m)  & %0.2f $\\pm$%0.2f & %0.2f $\\pm$%0.2f & %0.2f $\\pm$%0.2f & %0.2f $\\pm$%0.2f \\\\ \\hline \n', ...
	     r07_pathlength_mean, r07_pathlength_stderr, ...
	     g07_pathlength_mean, g07_pathlength_stderr, ...
	     p_pathlength_mean, p_pathlength_stderr, ...
	     rtbss_pathlength_mean, rtbss_pathlength_stderr);
fprintf( tf, 'Total Trials & %d & %d & %d & %d \\\\ \\hline \n', ...
	     num_d_r07, ...
	     num_d_g07, ...
	     num_d_p, ...
	     num_d_rtbss);
fprintf( tf, '\\end{tabular} \n' );
fclose( tf );
