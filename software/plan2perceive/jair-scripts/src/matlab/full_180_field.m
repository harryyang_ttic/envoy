function [f] = full_180_field( p )

    [r c] = size( p );
    f = nan * ones( r + r - 1, c );
    f(1:r, 1:c) = p( r:-1:1, 1:c );
    f(r+1:r+r - 1, 1:c) = p( 2:r, 1:c );

end