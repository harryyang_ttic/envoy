#!/usr/bin/env python

import sys
import time
import subprocess
import shutil

import gobject
import gtk

import lcm
import bot_procman
import bot_procman.sheriff_config
import bot_procman.sheriff_gtk

import ptplcm
import bot_core
import erlcm.navigator_goal_msg_t
import erlcm.point_t
import ptplcm.object_initial_beliefs_t
import ptplcm.point_t

def now():
    return int(time.time() * 1000000)

class Experiment(object):
    def __init__(self, experiment_id):
        self.experiment_id = experiment_id
        self.procman_config_filename = "/home/velezj/projects/object_detector_pose_planning/envoy/software/plan2perceive/data/procman/ptp-sim-text-multi-0.cfg"
        self.procman_config = bot_procman.sheriff_config.config_from_filename( self.procman_config_filename )
        
        self.lc = lcm.LCM()
        gobject.io_add_watch(self.lc, gobject.IO_IN, self.lc_handle)
        self.gui = bot_procman.sheriff_gtk.SheriffGtk(self.lc)
        self.gui.on_spawn_deputy_activate()

        self.sheriff = self.gui.sheriff

        self.ptp_planner = None

        self.started_timestamp = None

    def start_logging( self ):
        # ok, start a new logger process
        self.lcm_logger_proc = subprocess.Popen( ['lcm-logger', '-i', self.experiment_id ] )

        print "started logging lcm messages"

    def stop_logging(self):
        
        # terminate the logging process ( if any )
        if self.lcm_logger_proc is not None:
            self.lcm_logger_proc.kill()

    def send_goal( self ):
        
        print "sendin initial beliefs ping"
        msg = ptplcm.object_initial_beliefs_t()
        msg.utime = now()
        msg.size = 0
        self.lc.publish( "PTP_INITIAL_BELIEFS", msg.encode() );

        print "sending goal to ptp-planner"
        msg = erlcm.navigator_goal_msg_t.navigator_goal_msg_t()
        msg.utime = now()
        msg.goal = erlcm.point_t.point_t()
        msg.goal.x = 0
        msg.goal.y = -14
        msg.velocity = 0.07
        msg.sender = erlcm.navigator_goal_msg_t.navigator_goal_msg_t.SENDER_USAR;
        msg.nonce = int(time.time())
        
        self.lc.publish( "PTP_PLANNER_GOAL", msg.encode() )

    def check_for_done( self ):
                
        # check if planner is finished, in which case we are too
        if self.ptp_planner.status() == bot_procman.sheriff.STOPPED_OK:
            
            # stop the trial
            self.stop_experiment()
            
        
        # Timeout
        if now() - self.started_timestamp > 15 * 60 * 1000000 :
            self.stop_experiment()

            
        # re-add ourselves to check again in 1 second
        gobject.timeout_add(1000, self.check_for_done )


    def stop_experiment( self ):
        
        # stop every command
        for cmd in self.sheriff.get_all_commands():
            self.sheriff.stop_command( cmd )

        # stop the logging
        self.stop_logging()
        
        # cleanup any sherrif things
        self.gui.cleanup()

        # and stop loop
        gtk.main_quit()
        

    def start_experiment(self):

        # started timestmap
        self.started_timestamp = now()

        # first thing is to start logging
        self.start_logging()

        # start the config grouped commands first        
        for cmd in self.sheriff.get_all_commands():
            if cmd.group.strip() == "config":
                self.sheriff.start_command( cmd )
            if cmd.nickname.strip() == "ptp-planner":
                self.ptp_planner = cmd

        self.sheriff.send_orders()


        # start all other command
        for cmd in self.sheriff.get_all_commands():
            if cmd.group.strip() != "config":
                self.sheriff.start_command( cmd )

        self.sheriff.send_orders()

        # wait a bit before publishing the map and sending the takeoff command
        gobject.timeout_add(2000, self.send_goal)

    def run(self):
        gobject.timeout_add(1000, lambda: self.gui.sheriff.load_config(self.procman_config))
        gobject.timeout_add(3000, self.start_experiment)
        gobject.timeout_add(6000, self.check_for_done )
        try:
            gtk.main()
        except KeyboardInterrupt:
            print("Exiting")
        self.gui.cleanup()


    def lc_handle(self, *a):
        
        try:
            self.lc.handle()
        except Exception:
            traceback.print_exc()
        return True


if __name__ == "__main__":
    exp_id = '/home/velezj/projects/object_detector_pose_planning/data/experiments/simulation/text-multi-0/lcmlog'
    if len(sys.argv) > 1:
        exp_id = sys.argv[1]
    exp = Experiment(exp_id)
    exp.run()


        
