
import sys
import os


##
## Calls the ptp-analitics scripts on a directory of experiments

if len(sys.argv) < 2:
    print "Usage: " + sys.argv[0] + " experiment-dir"
    exit(-1)

base_dir = sys.argv[1]
file_set = os.listdir( base_dir )
sp = base_dir.split( "/" )
base_name = "default"
if len( sp ) > 0 :
    base_name = sp[-1]
if len( base_name ) < 1 and len(sp) > 1:
    base_name = base_dir.split( "/" )[-2]

# loop over files, running the analitics
for f in file_set:
    
    cmd = "./ptp-sim-multi-object-analysis %s %s" % ( base_dir + "/" + f, base_dir + "/" + base_name + ".analysis" )
    #print cmd + "\n"
    os.system( cmd )

