
#include <ptp-fit/plane_fit.hpp>
#include <ptp-common-coordinates/identity_manifold_t.hpp>
#include <ptp-common-test/test.hpp>
#include <iostream>
#include <stdlib.h>

using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::fit;

//=====================================================================

int test_compile()
{
  start_of_test;

  // create base manifold
  manifold_tp base( new identity_manifold_t( manifold_t::BASE_MANIFOLD ) );

  // Create a point cloud and fit a plane to it
  point_cloud_tp pc1( new point_cloud_t() );
  pc1->add( coordinate( 0,0,0 ).on( base ) );
  pc1->add( coordinate( 1,0,0 ).on( base ) );
  pc1->add( coordinate( 0,1,0 ).on( base ) );
  
  // create some ransac parameters
  plane_fit_ransac_parameters_t param1;
  param1.percent_to_subsample = 0.5;
  param1.min_samples = 3;
  param1.percent_which_must_agree = 0.75;
  param1.percent_of_max_distance_which_is_agree = 0.5;
  param1.max_agree_distance = 0.5;
  param1.min_agree_distance = 0.1;
  param1.max_mean_distance = 0.2;
  param1.max_iterations = 20;
  
  // fit a plane to the clouse
  plane_tp p1 = find_best_plane_fit( pc1, param1 );
  
  end_of_test;
}

//=====================================================================

int test_fit()
{

  start_of_test;

  // create base manifold
  manifold_tp base( new identity_manifold_t( manifold_t::BASE_MANIFOLD ) );

  // Create a point cloud and fit a plane to it
  point_cloud_tp pc1( new point_cloud_t() );
  pc1->add( coordinate( 0,0,0 ).on( base ) );
  pc1->add( coordinate( 1,0,0 ).on( base ) );
  pc1->add( coordinate( 0,1,0 ).on( base ) );
  
  // create some ransac parameters
  plane_fit_ransac_parameters_t param1;
  param1.percent_to_subsample = 0.5;
  param1.min_samples = 3;
  param1.percent_which_must_agree = 0.75;
  param1.percent_of_max_distance_which_is_agree = 0.5;
  param1.max_agree_distance = 0.1;
  param1.min_agree_distance = 0.1;
  param1.max_mean_distance = 0.15;
  param1.max_iterations = 20;
  
  // fit a plane to the clouse
  plane_tp p1 = find_best_plane_fit( pc1, param1 );
  //std::cout << "Plane p1: " << std::endl;
  //std::cout << (*p1) << std::endl;

  // creat the true normal
  simplex_tp n1_true = line_simplex( coordinate( 0.3333333333, 0.333333333, 0 ),
				     coordinate( 0.3333333333, 0.333333333, 1 ),
				     base );

  // Make sure the plane is what we want it to be
  test_assert( float_equal( n1_true, p1->normal_simplex() ) );

  
  end_of_test;

}

//=====================================================================

int test_fit_noise()
{

  start_of_test;

  // create base manifold
  manifold_tp base( new identity_manifold_t( manifold_t::BASE_MANIFOLD ) );

  // Create a point cloud and fit a plane to it
  point_cloud_tp pc2( new point_cloud_t() );
  pc2->add( coordinate( 0,0,0 ).on( base ) );
  pc2->add( coordinate( 1,0,0 ).on( base ) );
  pc2->add( coordinate( 0,1,0 ).on( base ) );
  pc2->add( coordinate( -1,-2,0 ).on( base ) );
  pc2->add( coordinate( 2,1.5,1 ).on( base ) );
  
  // create some ransac parameters
  plane_fit_ransac_parameters_t param2;
  param2.percent_to_subsample = 0.5;
  param2.min_samples = 3;
  param2.percent_which_must_agree = 4.0/5.0;
  param2.percent_of_max_distance_which_is_agree = 0.2;
  param2.max_agree_distance = 0.01;
  param2.min_agree_distance = 0.01;
  param2.max_mean_distance = 0.25;
  param2.max_iterations = 50;
  
  // fit a plane to the clouse
  plane_tp p2 = find_best_plane_fit( pc2, param2 );
  //std::cout << "Plane p2: " << std::endl;
  //std::cout << (*p2) << std::endl;

  // creat the true normal and the plane's normal as origin centered basis
  basis_t n2_true = basis_from_simplex( line_simplex( coordinate( 0, 0, 0 ),
						      coordinate( 0, 0, 1 ),
						      base ) );
  basis_t n2 = basis_t( coordinate(0,0,0).on(base),
			basis_from_simplex( p2->normal_simplex() ).origin_centered_direction() );

  // Make sure the plane is what we want it to be
  test_assert( float_equal( n2, n2_true ) );

  
  end_of_test;

}

//=====================================================================

int main( int argc, char** argv )
{

  run_test( test_compile() );
  run_test( test_fit() );
  run_test( test_fit_noise() );

  return 0;
}
