
#include <ptp-fit/sequential_monte_carlo_SIR_ml.hpp>
#include <ptp-common-test/test.hpp>

using namespace ptp;
using namespace ptp::common;
using namespace ptp::distributions;
using namespace ptp::fit;



//==============================================================

// Description:
// A simple likelihood function
// Notice it is just a function and need not sum to 1
double lik_01( const math_vector_t& a )
{
  assert( a.size() == 1 );
  if( a(0) > 2.0 )
    return 0;
  if( a(0) < -2.0 )
    return 0;
  return ( (a(0) + 2) / 4.0 );
}

//==============================================================


int test_compile()
{
  start_of_test;

  // create a prior over floats ( as gaussian )
  boost::shared_ptr<discrete_distribution_t<math_vector_t> > prior( new discrete_distribution_t<math_vector_t>() );
  math_vector_t v0(1); v0(0) = 0;
  math_vector_t v1(1); v1(0) = 1;
  prior->set( v0, 0.5 );
  prior->set( v1, 0.5 );

  // Create teh parameters
  typedef sequential_monte_carlo_SIR_ml_t<math_vector_t> smc_t;
  float trans_sigma = 1;
  float rthresh = 100;
  smc_t::params_t params( prior, trans_sigma, lik_01, rthresh, 1000 );

  // Run for a few hundred iterations
  smc_t::termination_conditions_t term;
  term.max_iterations = 100;
  smc_t smc(params);
  smc.run_iterations( term );

  // get the posterior
  smc_t::distribution_tp posterior = smc.current_posterior();
  smc.print_particles( std::cout );


  end_of_test;
}

//==============================================================

int main( int argc, char** argv )
{

  run_test(test_compile());
  
  return 0;
}
