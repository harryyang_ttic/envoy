
#include "plane_fit.hpp"
#include <ptp-common-matrix-math/svd.hpp>
#include <ptp-common-coordinates/matrix_util.hpp>
#include <algorithm>
#include <iostream>


namespace ptp {
  namespace fit {

    using namespace ptp::common;
    using namespace ptp::coordinates;
    using namespace ptp::matrixmath;

    //==================================================================

    // Description:
    // Fit a plane to a set of points using least-squares
    plane_tp fit_plane( const point_cloud_tp& points )
    {
      // Check the we have at least 3 points
      if( points->size() < 3 ) {
	BOOST_THROW_EXCEPTION( fit::exceptions::not_enough_samples_error() );
      }
      
      // Build up a matrix of the points centered at the origin
      coordinate_t centroid = points->centroid();
      point_cloud_tp centered_points = points->center_on( centroid );
      matrix_t m = centered_points->as_row_matrix();
      
      // Now, compute the SVD for this matrix
      matrix_t v, u;
      math_vector_t s;
      bool res = svd( m, u, v, s );
      
      // If tehre was a problem, then the points are degenerate
      if( res == false ) {
	BOOST_THROW_EXCEPTION( fit::exceptions::degenerate_error() );
      }
      
      // Now, we know the plane goes thorugh the centroid and
      // the normal is the eigevector with the smallest eigen value
      int idx = find_min_epsilon_idx( s, 0 );
      //raw_coordinate_t normal_point = raw_coordinate_from_math_vector( ublas::row( v, idx ) );
      raw_coordinate_t normal_point = raw_coordinate_from_math_vector( v.col( idx ) );

      // Now, get the first non-centroid point to be the arbritary direction
      // of the x axis for the plane
      coordinate_t x_axis_point = centered_points->at(0);
      
      // create a new plane with the nromal and origin as centroid and return
      simplex_tp normal = line_simplex( centroid.raw_coordinate(),
					centroid.raw_coordinate() + 
					normal_point,
					points->manifold() );
      simplex_tp x_axis = line_simplex( centroid.raw_coordinate(),
					( centroid + x_axis_point ).raw_coordinate(),
					points->manifold() );
      return plane_tp( new plane_t( normal, x_axis ) );
    }

    //==================================================================

    plane_tp find_best_plane_fit( const point_cloud_tp& points,
				  const plane_fit_ransac_parameters_t& param )
    {
      // Check the we have at least 3 points
      if( points->size() < 3 ) {
	BOOST_THROW_EXCEPTION( fit::exceptions::not_enough_samples_error() );
      }
      
      // Compute the actual number of samples to use per iteration
      size_t num_samples = param.percent_to_subsample * points->size();
      if( num_samples < param.min_samples ) {
	num_samples = std::min( points->size(), (int)param.min_samples );
      }

      // Initialize iteration counter and the best plane found so far
      plane_tp best_plane;
      float percent_agreement_for_best_plane = 0;
      float mean_distance_for_best_plane = 0;
      int iteration = 0;
      
      // While we do not have aggrement, and iterations are left
      // We want to sample some points from the point cloud, fit 
      // a plane with those points, and then check the aggreement
      while( ( percent_agreement_for_best_plane < param.percent_which_must_agree  ||
	       mean_distance_for_best_plane > param.max_mean_distance ) &&
	     iteration < param.max_iterations ) {
	
	// increment iteration number
	++iteration;

	// debug
	//std::cout << "  Fit-Plane: Iteration [" << iteration << "]: ";
	
	// Fit a plane to the samples
	point_cloud_tp samples = points->choose( num_samples );
	plane_tp plane;
	try {
	  plane = fit_plane( samples );
	} catch ( fit::exceptions::degenerate_error& e ) { 
	  
	  // If we are usign all points an it's degenerate, throw
	  // this upwards, else go on to the next iteration
	  if( num_samples == points->size() ) {
	    throw e;
	  } else {
	    continue;
	  }

	}
	
	// Find the distance ( error ) for all the points
	std::vector<float> errors;
	float error_sum = 0;
	for( size_t i = 0; 
	     i < points->size();
	     ++i ) {
	  curve_t error_curve = plane->shortest_curve_to_plane( points->at(i) );
	  errors.push_back( error_curve.arc_length() );
	  error_sum += errors[i];
	}
	float max_error = *std::max_element( errors.begin(), errors.end() );
	float mean_error = error_sum / points->size();
	
	// Tally agreement
	float agreement_percent = 0;
	float total_error = 0;
	float agreement_threshold = param.percent_of_max_distance_which_is_agree * max_error;
	agreement_threshold = std::max( agreement_threshold, param.min_agree_distance );
	agreement_threshold = std::min( agreement_threshold, param.max_agree_distance );
	for( size_t i = 0; i < errors.size(); ++i ) {
	  float error = errors[i];
	  total_error += error;
	  if( error <= agreement_threshold ) {
	    agreement_percent += 1.0/points->size();
	  }
	}

	// debug
	//std::cout << "e=" << total_error << ", m=" << mean_error << ", ma=" << agreement_threshold << ", a=" << agreement_percent << std::endl;
	
	// keep best plane so far
	if( agreement_percent >= percent_agreement_for_best_plane ) {
	  best_plane = plane;
	  percent_agreement_for_best_plane = agreement_percent;
	  mean_distance_for_best_plane = mean_error;
	}
      }

      // Return the best found ( it either is above agreement percent or
      // iterations were reached )
      return best_plane;
    }

    //==================================================================

  }
}
