
#if !defined( __PTP_FIT_plane_fit_HPP__ )
#define __PTP_FIT_plane_fit_HPP__

#include <ptp-common-coordinates/plane_t.hpp>
#include <ptp-common-coordinates/point_cloud_t.hpp>

namespace ptp {
  namespace fit {

    using namespace ptp::common;
    using namespace ptp::coordinates;

    //==================================================================

    namespace exceptions {
    
      // Description:
      // Not enough sampels to fit something
      struct not_enough_samples_error : public virtual exception_base {};
      
      // Description:
      // The points are degenerate
      struct degenerate_error : public virtual exception_base {};
      
    }
    
    //==================================================================
    
    // Description:
    // The parameters for a plane fitting algorithm
    struct plane_fit_ransac_parameters_t
    {
      float percent_to_subsample;
      size_t min_samples;
      float percent_which_must_agree;
      float percent_of_max_distance_which_is_agree;
      float max_agree_distance;
      float min_agree_distance;
      float max_mean_distance;
      size_t max_iterations;
    };

    //==================================================================

    // Description:
    // Finds the 'best' fitting plane to a set of coordinates.
    // The best fit according to the orthogonal distance from the
    // found plane in a least-squares sense.
    plane_tp find_best_plane_fit( const point_cloud_tp& points,
				  const plane_fit_ransac_parameters_t& param );

    //==================================================================


  }
}

#endif

