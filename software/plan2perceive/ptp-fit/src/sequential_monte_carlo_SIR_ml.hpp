
#if !defined( __PTP_FIT_sequential_monte_carlo_SIR_HPP__ )
#define __PTP_FIT_sequential_monte_carlo_SIR_HPP__

#include <ptp-common-distributions/discrete_distribution_t.hpp>
#include <ptp-common-distributions/multivariate_normal_distribution_t.hpp>
#include <boost/function.hpp>

namespace ptp {
  namespace fit {

    using namespace ptp;
    using namespace ptp::common;
    using namespace ptp::distributions;

    //==================================================================

    // Description:
    // Performs a sequential monte carlo (particle filter) set of steps
    // using the Sequential-Importance_sampling (SIR) algorithm.
    // This results in a posterior distribution.
    // We will want to maximie the likelihood
    //
    // Things needed are:
    //    An initial proposal distribution which gives use the prior over X
    //    a transition sigma (dynamics model) for particles
    //    a likelihood function of p(X)
    //    a resapling threshold
    //    a termination condition
    //
    // The elements X must be a math vector type.
    template< class X>
    class sequential_monte_carlo_SIR_ml_t
    {
    public:  // public types
      
      typedef boost::shared_ptr<distribution_t<X> > distribution_tp;
      typedef boost::function1< double, const X& > likelihood_function_t;

      //--------------------------------------------------

    public: // inner structures

      //--------------------------------------------------
      
      // Description:
      // The parameters for this fitting method
      struct params_t {
	distribution_tp prior_proposal_distribution;
	float transition_sigma;
	likelihood_function_t likelihood_function;
	float resampling_threshold_as_effective_particles;
	size_t particle_pool_size;
	params_t( const distribution_tp& prior,
		  const float& trans,
		  const likelihood_function_t& lik,
		  const float& resmaple_thresh,
		  const size_t& pool )
	  : prior_proposal_distribution( prior ),
	    transition_sigma( trans ),
	    likelihood_function( lik ),
	    resampling_threshold_as_effective_particles( resmaple_thresh ),
	    particle_pool_size( pool )
	{}
      };
      
      //--------------------------------------------------
      
      // Description:
      // Termination criteria for this fitting algorithm
      struct termination_conditions_t {
	boost::optional<size_t> max_iterations;
	boost::optional<double> min_change_in_weights;
      };

      //--------------------------------------------------

      // Description:
      // A particfle, which is just an X and a weight
      struct particle_t {
	X x;
	float w;
      };

      //--------------------------------------------------
      

    public:

      //--------------------------------------------------
      
      // Description:
      // Create a new object with parameters
      sequential_monte_carlo_SIR_ml_t( const params_t& p )
	: _params( p )
      {
	set_params( p );
      }

      //--------------------------------------------------

      // Description:
      // Sets the parameters for this object
      void set_params( const params_t& p )
      { 
	_params = p; 
	reset();
      }

      //--------------------------------------------------

      // Description:
      // Returns the current posterior distribution 
      distribution_tp current_posterior() const
      { 
	return particle_distribution( _particles );
      }


      //--------------------------------------------------

      // Description:
      // Runs the monte carlo filter until the given termination criterion
      // is met
      void run_iterations( const termination_conditions_t& term )
      {
	_term = term;

	// draw the first set of particles
	_particles = draw_initial_particles( _params.particle_pool_size );
	
	do {
	  run_single_iteration();
	} while( is_done( _term ) == false );
      }

      //--------------------------------------------------

      // Description:
      // Resets the internal state
      void reset()
      {
	_iteration = 0;
	_particles.clear();
	_largest_weight_change = 0;
      }

      //--------------------------------------------------
      
      // Description:
      // Print the current set of particles to the stream
      void print_particles( std::ostream& os ) const
      {
	for( size_t i = 0; i < _particles.size(); ++i ) {
	  os << _particles[i].x << " " << _particles[i].w << std::endl;
	}
      }

      //--------------------------------------------------

    protected:

      //--------------------------------------------------

      // Description:
      // Runs a single step of the filter.
      // This will update the internal particles ( the internal 
      // proposal distribution ) and possible resample particles.
      void run_single_iteration()
      {
	std::vector<particle_t> particles = draw_particles( _params.particle_pool_size );
	update_particle_weights( particles, _particles, _params.likelihood_function );
	normalize_weights( particles );
	float effective_size = calculate_effective_size( particles );
	if( effective_size < _params.resampling_threshold_as_effective_particles ) {
	  _particles = resample( particles );
	} else {
	  _particles = particles;
	}
	
	++_iteration;
      }

      //--------------------------------------------------
      
      // Description:
      // Returns true iff the given termination conditions have been met
      bool is_done( const termination_conditions_t& term ) const
      {
	if( term.max_iterations &&
	    _iteration >= (*term.max_iterations) )
	  return true;
	if( term.min_change_in_weights &&
	    (*term.min_change_in_weights >= _largest_weight_change ) )
	  return true;
	return false;
      }

      //--------------------------------------------------

      // Description:
      // Draw a number of initial particles
      std::vector<particle_t> draw_initial_particles( const size_t& n ) const
      {
	// we will just draw samples from our prior
	std::vector<particle_t> particles;
	particles.reserve( n );
	for( size_t i = 0; i < n; ++i ) {
	  particle_t p;
	  p.x = _params.prior_proposal_distribution->sample();
	  p.w = _params.prior_proposal_distribution->p( p.x ).get_raw_probability_t().as_float();
	  particles.push_back( p );
	}
	normalize_weights( particles );
	return particles;
      }

      //--------------------------------------------------

      // Description:
      // Normalizes the weights of the given aprticles to
      // sum to 1, This changes teh given particle set
      void normalize_weights( std::vector<particle_t>& particles ) const
      {
	// compute weight sum
	double sum = 0;
	for( size_t i = 0; i < particles.size(); ++i ) {
	  sum += particles[i].w;
	}
	// normalize
	for( size_t i = 0; i < particles.size(); ++i ) {
	  particles[i].w /= sum;
	}
      }

      //--------------------------------------------------

      // Description:
      // Draw N particles from the current proposal distribution
      // The 'weights' of the particles returned is MEANINGLESS
      // so the update weights must be called first!
      std::vector<particle_t> draw_particles( const size_t& n ) const
      {
	// teh resultign particles
	std::vector<particle_t> drawn_particles;
	drawn_particles.reserve( n );

	// Create a discrete distribution with the particles
	distribution_tp pdistribution = particle_distribution( _particles );
	
	// Now we want to sample from those particles
	// And for each sample we want to shift it by the 
	// transition distribution (a normal with a sigma parameter and 0 mean)
	for( size_t i = 0; i < n; ++i ) {
	  X orig = pdistribution->sample();
	  multivariate_normal_distribution_t<X> tdistribution( orig, _params.transition_sigma );
	  X sample_x = tdistribution.sample();
	  particle_t particle;
	  particle.x = sample_x;
	  particle.w = tdistribution.p( sample_x ).get_raw_probability_t().as_float();
	  drawn_particles.push_back( particle );
	}

	// return the aprticles
	return drawn_particles;
      }

      //--------------------------------------------------

      // Description:
      // Updates the weight of particles based on old weights
      // This assumes that particles are indexed that same between
      // old and new
      void update_particle_weights( std::vector<particle_t>& particles,
				    const std::vector<particle_t>& old,
				    const likelihood_function_t& lik ) const
      {
	double smallest_delta = std::numeric_limits<double>::infinity();
	for( size_t i = 0; i < particles.size(); ++i ) {
	  double new_weight = lik( particles[i].x ) * old[i].w;
	  if( abs(particles[i].w - new_weight) < smallest_delta ) {
	    smallest_delta = abs( particles[i].w - new_weight );
	  }
	  particles[i].w = new_weight;
	}
      }

      //--------------------------------------------------

      // Description:
      // Resamples the particles, returns a new set 
      std::vector<particle_t> resample( const std::vector<particle_t>& p ) const
      {
	std::vector< particle_t > samples;
	samples.reserve( p.size() );
	
	// create a new discrete distribution with the particles
	distribution_tp pdist = particle_distribution( p );
	
	// Sample from this distribution to get new particles
	double sample_weight = 1.0 / p.size();
	for( size_t i = 0; i < p.size(); ++i ) {
	  particle_t s;
	  s.x = pdist->sample();
	  s.w = sample_weight;
	  samples.push_back( s );
	}

	// return the new samples
	return samples;
      }

      //--------------------------------------------------

      // Description:
      // Returns a deiscrete distribution of X according to the
      // particles given
      distribution_tp particle_distribution( const std::vector<particle_t>& p ) const
      {
	boost::shared_ptr<discrete_distribution_t<X> > dist( new discrete_distribution_t<X>() );
	for( size_t i = 0; i < p.size(); ++i ) {
	  dist->set( p[i].x, p[i].w );
	}
	return dist;
      }

      //--------------------------------------------------

      // Description:
      // Returns the effective wieght
      float calculate_effective_size( const std::vector<particle_t>& p )
      {
	double sum = 0;
	for( size_t i = 0; i < p.size(); ++i ) {
	  sum += ( p[i].w * p[i].w );
	}
	return 1.0 / sum;
      }

      //--------------------------------------------------



      // Description:
      // The parameters
      params_t _params;
      
      // Descripiton:
      // THe termination criteria
      termination_conditions_t _term;

      // Description:
      // The current set of particles
      std::vector< particle_t > _particles;

      // Description:
      // The largest weight change in particles this iteration
      float _largest_weight_change;

      // Description:
      // The current iteration
      size_t _iteration;
      
    };

    //==================================================================
    //==================================================================
    //==================================================================

  }
}

#endif

