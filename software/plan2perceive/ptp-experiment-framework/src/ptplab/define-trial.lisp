
(in-package #:ptplab)



;;; Description:
;;; A high-level interface for defining a new trial for an experiment
;;; and adding it to the experiment.
;;; The config-options keyword is a list of 
;;; (config-key value) pairs, where teh key is a structured name
(defun add-trial (&key
		  (experiment *experiment*)
		  (name (concatenate 'string
				     "trial-"
				     (write-to-string (length (trials-of *experiment*)))))
		  (execute-command 
		   (merge-pathnames #p"execute" 
				    (base-data-path-of (specification-of experiment))))
		  (execute-command-args nil)
		  (config-options nil)
		  (config-pathname
		   (merge-pathnames
		    (make-pathname 
		     :directory (list :relative 
				      name)
		     :name "config"
		     :type "xml")
		    (base-data-path-of (specification-of experiment)))))
  
  ;; Now create a trial
  (let ((trial 
	 (make-instance 'trial-t
			:name name
			:experiment experiment
			:execute-command execute-command
			:execute-command-args execute-command-args
			:config-pathname config-pathname
			:config-options config-options)))
    
    ;; add the trial to the experiment and reutrn trial
    (push trial (trials-of experiment))
    trial))
  