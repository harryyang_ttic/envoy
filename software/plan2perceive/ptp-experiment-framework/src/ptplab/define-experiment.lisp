
(in-package #:ptplab)

;;; Description:
;;; The default base path for defining exsperiment.
;;; By defautl, define-experiment will append the name to this
;;; path to create the experiment path.
(defvar *experiment-base-path* (truename #p"~/experiments/") )


;;; Description:
;;; Global list of all defined experiments
(defvar *all-defined-experiments* 'nil)


;;; Description:
;;; The current experiment
(defparameter *experiment* nil)


;;; Description:
;;; Hihg-level way of creating an experiment.
;;; This will set teh *experiment* current experiment ot the
;;; jsut defined one
(defmacro define-experiment (name &key 
			     (path (merge-pathnames 
				    (make-pathname
				     :directory (list :relative name))
				    (force-directory *experiment-base-path*)))
			     (group "experiment") (tags nil) (repos nil))
  ;; first, define a package which is the experiment group
  ;; If already defined, this does not do anything
  (let* ((name-string (if (typep name 'string) name (symbol-name name)))
	 (name-symbol (intern (string-upcase name-string)))
	 (group-normed-symbol (intern (string-upcase group)))
	 (pkg (if (find-package group-normed-symbol)
		  (find-package group-normed-symbol)
		  (make-package group-normed-symbol 
				:use (list (find-package 'common-lisp)
					   (find-package 'ptplab))))))
    `(let* ((*package* (find-package ',group-normed-symbol)))
       (let ((new-exp
	      (defvar ,(intern 
			(symbol-name name-symbol)
			pkg)
		(make-instance 'experiment-t
			       :specification (make-instance 'experiment-specification-t
							     :name ,name-string
							     :group ,group
							     :tags ,tags
							     :code-repositories ,repos
							     :base-data-path ,path
							     :config-pathname 
							     ,(merge-pathnames #p"config.xml" path))))))
	 (push new-exp *all-defined-experiments*)
	 (setf *experiment* (symbol-value new-exp))
	 new-exp))))



