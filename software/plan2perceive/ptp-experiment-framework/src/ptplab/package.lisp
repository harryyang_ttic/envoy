;;;; package.lisp

(defpackage #:ptplab
  (:use #:cl)
  (:shadowing-import-from #:hu.dwim.defclass-star
			  #:defclass*))


(declaim (optimize (debug 3) (safety 3)))
