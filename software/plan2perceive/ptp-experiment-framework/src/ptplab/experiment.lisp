
(in-package #:ptplab)



;;; Description:
;;; This class represents a particular experiment.
;;; This includes bot hthe specification for the experiment as well
;;; as any results and nalysis for it.
(defclass* experiment-t ()
  ((specification :type experiment-specification-t
		  :documentation "The specification for this experiment. This contains all things needed to actually run the experiment")
   (trials nil :documentation "A list of epxeriment trials, or nil. This will be filled in as the experiment is run (so as more trials are completed)")
   (analysis nil :documentation "A list of experiment analysis. Will be filled in as the analysis are done.")))


;;; Descrioption:
;;; A specification for a particular experiment.
;;; This includes all of the things need to actually run the experiment
;;; This includes 
;;;    a set of structured tags, 
;;;    a location for config
;;;    a primary prefix for output data files
;;;    a primary prefix for input data files
;;;    a primary prefix for analysis files
;;;    a primary prefix for figures/plots/media files
;;;    secondary prefixes for project archive
;;;    a code revision number(s)
;;;    a set of code repository locations which this depends on
;;;    a structured group name (places project in hierarchy of groups )
;;;    a unique identifier (within group)
(defclass* experiment-specification-t ()
  ((tags () 
	 :documentation "list of strucutred tags for this experiment")
   (config-pathname
    :documentation "the path to the configuration file for this experiment")
   (base-data-path
    :documentation "primary prefix path for all paths for this experiment")
   (output-data-path #p"output/"
    :documentation "primary prefix path for output data for this experiment. 
                    This is relative to the base-data-path.")
   (input-data-path #p"input/"
    :documentation "primary prefix path of input data for this experiment.
                    This is relaticve to base-data-path.")
   (analysis-data-path #p"analysis/"
    :documentation "primary prefix path for analysis data for this experiment.
                    This is rek\lative to base-data-path.")
   (type-based-data-paths nil
			  :documentation "list of type-specific data path prefixes for experiment.
                    These are *not* relative to base-data-path.
                    Currently these are *not implemented*.")
   (archive-paths nil
		  :documentation "list of secondary archival paths for this experiment.
                    These are *not* relative to base-data-path.")
   (code-repositories
    :documentation "list of repository-specification-t structures to all 
                    code repos this experiment depends on")
   (group
    :documentation "structured name for the group this experiment belongs to")
   (name
    :documentation "unique identifier (within group) for this experiment")))


;;; Descrioption:
;;; A particular trial for an experiment. This includes any data output files,
;;; analyusis, and input file locations (rather than prefixes).  Also has
;;; a unique identifier (within experiment)
;;;
;;; Note: trials are linked from experiemnt-t and link to their experiment-t
(defclass* trial-t ()
  ((name :documentation "Uniqeu identifier (within experiemnt) for this trial.")
   (experiment :documentation "The experiment-t this trial belongs to.")
   (config-pathname :documentation "The location of  the config for this trial.")
   (config-options 
    nil
    :documentation "Lsit of (config-key value) paris with additional 
                    configuration options
                    specific to this trial")
   (command-line-config-values 
    nil
    :documentation "List of (config-key config-value) pairs
                    which are given in the command line for trial.")
   (execute-command
    ""
    :documentation "The command to execute when running trial, as a string")
   (execute-command-args
    nil
    :documentation "List of argument to the execute-command")
   (output-pathnames 
    nil
    :documentation "List of the output files for this trial.
                    This will be filled in after the trial is finished.")
   (input-pathnames
    nil
    :documentation "List of the input files for this trial.
                    This will be filled in after the trial is finished")
   (analysis-pathnames
    nil
    :documentation "List of analysis files for this trial.
                    Will be filled in as the analysis are run.")))



;;; Description:
;;; Builds and returns the data paths for input/.outpout/analysis for 
;;; a trial
(defmethod output-data-path-of ((trial trial-t))
  (let ((spec (specification-of (experiment-of trial))))
    (merge-pathnames 
     (merge-pathnames 
      (make-pathname :directory (list :relative 
				      (name-of trial)))
      (force-directory (output-data-path-of spec)))
     (base-data-path-of spec))))

(defmethod input-data-path-of ((trial trial-t))
  (let ((spec (specification-of (experiment-of trial))))
    (merge-pathnames 
     (merge-pathnames 
      (make-pathname :directory (list :relative 
				      (name-of trial)))
      (force-directory (input-data-path-of spec)))
     (base-data-path-of spec))))

(defmethod analysis-data-path-of ((trial trial-t))
  (let ((spec (specification-of (experiment-of trial))))
    (merge-pathnames 
     (merge-pathnames 
      (make-pathname :directory (list :relative 
				      (name-of trial)))
      (force-directory (analysis-data-path-of spec)))
     (base-data-path-of spec))))

;;; Description:
;;; A particular analysis for an entire experiment (not a single trial).
;;; Includes any input and output files, a unique name (within experiment)
;;; as well as a config and a link back to the parent experiment-t.
(defclass* analysis-t ()
  ((name :documentation "Unique name (within experiment) for this analysis")
   (experiment :documentation "The experiment-t this analysis belong to.")
   (config-pathname :documentation "The location of config for this analysis.")
   (output-pathnames 
    :documentation "List of the output files for this analysis.
                    This will be filled in after the trial is finished.")
   (input-pathnames
    :documentation "List of the input files for this analysis.
                    This will be filled in after the trial is finished")))
   



  
;;; Description:
;;; The namespace url for our PTP
(defvar *ptp-namespace-url* "http://svn.csail.mit.edu/velezj-repo/ptp")


;;; Description:
;;; The generic function which creates and writes out a config XML file
;;; for a given object
(defgeneric create-and-write-config (obj))


;;; Description:
;;; Macro which binds the namespace map for xpath
(defmacro with-namespaces-for-xpath (namespace-specs &body body)
  (let ((quoted-specs (mapcar #'(lambda (x) (cons 'list x)) namespace-specs)))
    `(let* ((libxml2.xpath:*default-ns-map*
	    (append (list ,@quoted-specs) libxml2.xpath:*default-ns-map*)))
	   ,@body)))
     


;;; Description:
;;; Creats ans writes config XML for a particular trial.
;;; This uses the trial's experiment-t->experiment-specification-t to
;;; fill in the config parameters.
(defmethod create-and-write-config ((trial trial-t))
  (write-config-for-trial trial))
(defun write-config-for-trial (trial)
  (declare (type trial-t trial))
  (let* ((exp (experiment-of trial))
	 (spec (specification-of exp)))
    ;; Create a config xml structure for the trial
    (libxml2.tree:with-object 
	(trial-config-xml 
	 (xfactory:with-document-factory 
	     ((PTP *ptp-namespace-url* "ptp"))
	   (PTP "trial"
		(PTP "name"
		     (xfactory:text (name-of trial)))
		(PTP "execute-command"
		     (xfactory:text (execute-command-of trial)))
		(PTP "execute-command-args"
		     (dolist (arg (execute-command-args-of trial))
		       (PTP "execute-command-arg"
			    (xfactory:text arg))))
		(PTP "output-data-path"
		     (xfactory:text 
		      (namestring (parse-namestring 
				   (concatenate 'string 
						(namestring (base-data-path-of spec))
						"/"
						(namestring (output-data-path-of spec))
						"/"
						(name-of trial)
						"/")))))
		(PTP "input-data-path"
		     (xfactory:text
		      (namestring (parse-namestring 
				   (concatenate 'string 
						(namestring (base-data-path-of spec))
						"/"
						(namestring (input-data-path-of spec))
						"/"
						(name-of trial)
						"/")))))
		(PTP "analysis-data-path"
		     (xfactory:text
		      (namestring (parse-namestring 
				   (concatenate 'string 
						(namestring (base-data-path-of spec))
						"/"
						(namestring (analysis-data-path-of spec))
						"/"
						(name-of trial)
						"/")))))
		(PTP "input-pathnames"
		     (dolist (in-pathname (input-pathnames-of trial))
		       (PTP "input-pathname"
			    (xfactory:text (namestring in-pathname)))))
		(PTP "analysis-pathnames"
		     (dolist (analysis-pathname (analysis-pathnames-of trial))
		       (PTP "analysis-pathname"
			    (xfactory:text (namestring analysis-pathname)))))
		(PTP "output-pathnames"
		     (dolist (out-pathname (output-pathnames-of trial))
		       (PTP "output-pathname"
			    (xfactory:text (namestring out-pathname)))))))) 
      
      ;; create the XML node for the config-options for this trial
      ;; by prasing the (key vaklue) list and creating xml from
      ;; the keys (which are structured names)
      (libxml2.tree:with-object (config-options-xml (libxml2.tree:make-document))
	(let ((xml-nodes 
	       (mapcar 
		#'(lambda (option)
		    (destructuring-bind (key val) option
		      (multiple-value-bind (root leaf)
			  (structured-name-to-xml key)
			(setf (libxml2.tree:text-content leaf) val)
			root)))
		(config-options-of trial))))
	  (dolist (node xml-nodes nil)
	    (libxml2.tree:append-child config-options-xml node)))
	
	;; now, read in the config for the experiment
	(libxml2.tree:with-parse-document 
	    (exp-config (truename (config-pathname-of (specification-of (experiment-of trial)))))
	  ;; merge the three configs (just concatenate for now)
	  (with-namespaces-for-xpath (("ptp" *ptp-namespace-url*))
	    (let ((exp-config-root-node 
		   (libxml2.xpath:find-single-node exp-config "/ptp:config"))
		  (trial-config-subnodes 
		   (libxml2.xpath:find-list trial-config-xml "/*"))
		  (trial-config-option-nodes 
		   (libxml2.xpath:find-list config-options-xml "/*")))
	      (dolist (node (append trial-config-subnodes 
				    trial-config-option-nodes) nil)
		(libxml2.tree:append-child exp-config-root-node 
					   (libxml2.tree:copy node))))
	    ;; Now write the xml to the config pathname specified in trial
	    (ensure-directories-exist (config-pathname-of trial))
	    (let ((path (merge-pathnames (file-namestring (config-pathname-of trial))
					 (truename (directory-namestring 
						    (config-pathname-of trial))))))
	      (libxml2.tree:serialize exp-config path :pretty-print t))
	    ;; return the xml strings
	    (list (libxml2.tree:serialize exp-config :to-string :pretty-print t)
		  (libxml2.tree:serialize trial-config-xml :to-string :pretty-print t)
		  (libxml2.tree:serialize config-options-xml :to-string :pretty-print t))))))))



;;; Description:
;;; Creates and writes teh config XML file for a given experiment.
(defmethod create-and-write-config ((exp experiment-t))
  (let ((spec (specification-of exp)))
    ;; Create a config xml structure for the experiment
    (libxml2.tree:with-object 
	(config-xml 
	 (xfactory:with-document-factory 
	     ((PTP *ptp-namespace-url* "ptp"))
	   (PTP "config"
		(PTP "experiment"
		     (PTP "tags"
			  (dolist (tag (tags-of spec))
			    (PTP "tag"
				 (xfactory:text tag))))
		     (PTP "config-pathname"
			  (xfactory:text (namestring (config-pathname-of spec))))
		     (PTP "base-data-path"
			  (xfactory:text (namestring (base-data-path-of spec))))
		     (PTP "output-data-path"
			  (xfactory:text 
			   (namestring (parse-namestring 
					(concatenate 'string 
						     (namestring (base-data-path-of spec))
						     "/"
						     (namestring (output-data-path-of spec)))))))
		     (PTP "input-data-path"
			  (xfactory:text 
			   (namestring (parse-namestring 
					(concatenate 'string 
						     (namestring (base-data-path-of spec))
						     "/"
						     (namestring (input-data-path-of spec)))))))
		     (PTP "analysis-data-path"
			  (xfactory:text 
			   (namestring (parse-namestring 
					(concatenate 'string 
						     (namestring (base-data-path-of spec))
						     "/"
						     (namestring (analysis-data-path-of spec)))))))
		     (PTP "archive-paths"
			  (dolist (p (archive-paths-of spec))
			    (PTP "archive-pathname" 
				 (xfactory:text (namestring p)))))
		     (PTP "repositories"
			  (dolist (r (code-repositories-of spec))
			    (PTP "repository"
				 (PTP "name"
				      (xfactory:text (name-of r)))
				 (PTP "pathanme"
				      (xfactory:text (namestring (pathname-of r))))
				 (PTP "url"
				      (xfactory:text (url-of r)))
				 (PTP "revision"
				      (xfactory:text (revision-of r)))
				 (PTP "experiment-branch-path"
				      (xfactory:text (namestring (experiment-branch-path-of r))))
				 (PTP "experiment-branch-url-prefix"
				      (xfactory:text (experiment-branch-url-prefix-of r))))))
		     (PTP "group"
			  (xfactory:text (group-of spec)))
		     (PTP "name"
			  (xfactory:text (name-of spec)))))))
      ;; write the config XML to the configpathname
      (ensure-directories-exist (config-pathname-of spec))
      (libxml2.tree:serialize config-xml 
			      (merge-pathnames (file-namestring (config-pathname-of spec))
					       (truename (directory-namestring
							  (config-pathname-of spec)))) :pretty-print t )
      ;; return the xml as a string
      (libxml2.tree:serialize config-xml :to-string :pretty-print t))))
    
							    
	  

;;; Parametre for debuging
(defparameter *repo-1*
  (make-instance 'ptplab::repository-specification-t
		 :name "repo-1"
		 :pathname #p"/home/velezj/projects/repo_1"
		 :url "https://svn.csail.mit.edu/velezj-repo/repo_1/trunk"
		 :revision "1212"
		 :experiment-branch-path #p"/home/velezj/experiments/experiment-branches/repo_1"
		 :experiment-branch-url-prefix "https://svn.csail.mit.edu/velezj-repo/experiment-branches/repo_1"))

(defparameter *repo-2*
  (make-instance 'ptplab::repository-specification-t
		 :name "repo-2"
		 :pathname #p"/home/velezj/projects/repo_2"
		 :url "https://svn.csail.mit.edu/velezj-repo/repo_2"
		 :revision "1212"
		 :experiment-branch-path #p"/home/velezj/experiments/experiment-branches/repo_2"
		 :experiment-branch-url-prefix "https://svn.csail.mit.edu/velezj-repo/experiment-branches/repo_2"))


(defparameter *exp-spec* 
  (make-instance 'ptplab::experiment-specification-t 
		 :name "test-exp"
		 :group "test.experiments"
		 :tags (list "tag1" "tag3")
		 :config-pathname #p"/home/velezj/experiments/test-exp/config.xml"
		 :base-data-path #p"/home/velezj/experiments/test-exp"
		 :output-data-path #p"output"
		 :input-data-path #p"input"
		 :analysis-data-path #p"analysis"
		 :code-repositories (list *repo-1* *repo-2*)))

(defparameter *exp*
  (make-instance 'experiment-t
		 :specification *exp-spec*
		 :trials nil
		 :analysis nil))

(defparameter *trial-1*
  (make-instance 'trial-t
		 :name "trial-1"
		 :experiment *exp*
		 :config-pathname #p"/home/velezj/experiments/test-exp/trial-1/config.xml"
		 :input-pathnames (list #p"/home/velezj/experiments/test-exp/trial-1/input/inpout-file-1.txt"
					#p"/home/velezj/experiments/test-exp/trial-1/input/input-file-2.txt")
		 :output-pathnames (list #p"/home/velezj/experiments/test-exp/trial-1/output/output-file.txt")
		 :analysis-pathnames (list #p"/home/velezj/experiments/test-exp/trial-1/analysis/analysis-file.txt")
		 :config-options
		 (xfactory:with-document-factory ((PTP *ptp-namespace-url* "ptp"))
		   (PTP "option-A" 
			(xfactory:text "value-a")))))


