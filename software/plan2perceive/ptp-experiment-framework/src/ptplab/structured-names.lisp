
(in-package #:ptplab)


;;; Description:
;;; The default sturcutred name delimiter
(defvar *structured-name-delimiter-regex* "[.]")

;;; Description:
;;; Takes a structured name (containing delimited regions) and 
;;; split it into a lsit of token
(defun split-structured-name (str &key (delimiter-regex *structured-name-delimiter-regex*) (limit nil))
  (cl-ppcre:split delimiter-regex str :limit limit))


;;; Description
;;; Create a tree from the sturcutred name.
;;; This will return a list of lists, with each list contaning as
;;; it's first element the strucutred name region for that level.
(defun structured-name-to-tree (str &key (delimiter-regex *structured-name-delimiter-regex*) (limit nil))
  ;; stop if we have reached out limit
  (if (and (not (null limit))
	   (<= limit 1))
      (list str)
      ;; get the next sturcutred reagion ( and the rest )
      (let ((regions 
	     (split-structured-name str 
				    :delimiter-regex delimiter-regex 
				    :limit 2)))
	;; if there is no more regions, jsut return this one
	(if (< (length regions) 2)
	    regions
	    ;; otherwise recurse and build up tree
	    (list (car regions)
		  (structured-name-to-tree (cadr regions) 
					   :delimiter-regex delimiter-regex 
					   :limit (if (null limit) 
						      limit 
						      (- limit 1))))))))



;;; Description:
;;; Creates an XML tree from a structured name.
;;; Returns both the parent (top) node and the last (leaf) nodes
(defun structured-name-to-xml (str &key 
			       (delimiter-regex *structured-name-delimiter-regex*) 
			       (limit nil)
			       (namespace-url *ptp-namespace-url*))
  (let ((regions (split-structured-name str :delimiter-regex delimiter-regex :limit limit)))
    ;; special cases for where there are less than 2 regions
    (case (length regions)
      (0 (values nil nil))
      (1 (let ((root (libxml2.tree:make-element (car regions) namespace-url)))
	   (values root root)))
      (otherwise
       (let* ((root (libxml2.tree:make-element (car regions) namespace-url))
	      (cur-node root))
	 (dolist (region (cdr regions) nil)
	   (setf cur-node 
		 (libxml2.tree:append-child cur-node
					    (libxml2.tree:make-element region))))
	 (values root cur-node))))))
    
