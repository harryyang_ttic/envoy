

(in-package #:ptplab)


;;; Descripotion:
;;; A small type for the status of a repository entry
(deftype repository-entry-status-t 
    () 
  '(member 
    :added 
    :conflicted
    :deleted
    :ignored
    :modified
    :replaced
    :unversioned
    :missing
    :obstructed
    :versioned
    :unknown))

					

;;; Description:
;;; This class represnets the status of a simgle entry in a repository.
;;; It mainly includes a path to the entry, and name, and a status
(defclass* repository-entry-t ()
  ((name :documentation "The name for an entry (usually last part of path)")
   (pathname :documentation "The path to the entry")
   (revision nil :documentation "The revision number for the entry.")
   (status :type repository-entry-status-t :documentation "The status of the entry")))


;;; Description:
;;; This class represents a particular repostitory and state of the repository
;;; (aka revision)
(defclass* repository-info-t ()
  ((name :documentation "The name for this repository (usually last part of path)")
   (pathname :documentation "The path to the repository. This is the local path.")
   (url :documentation "The url for this repository. This is the global access path")
   (root :documentation "The repository root")
   (revision :documentation "The revision of hte repository")
   (unique-id :documentation "For some repositories, the unique id")))


;;; Description:
;;; A specification for a repository.
;;; Nearly the same as repository-info-t but semmantically different.
;;; Also includes the location to put any temporary branch check-ins
(defclass* repository-specification-t ()
  ((name 
    :documentation "The name for this repository (usually last part of path)")
   (pathname 
    :documentation "The path to the repository. This is the local path.")
   (url 
    :documentation "The url for this repository. 
                    This is the global access path")
   (revision :documentation "The revision of hte repository")
   (experiment-branch-path
    :documentation "The path prefix to use for local temporary check-ins.")
   (experiment-branch-url-prefix
    :documentation "The url to use as a prefix for any temporary checkings 
                    needed by the system." )))



;;; Description:
;;; Returns information on a particular svn repository at the given location.
;;; This is the revision at the time of the call, not the HEAD.
;;; Returns a repository-info-t instance.
(defun svn-repo-get-info (repo-pathname)
  ;;; call svn info, storing result in string
  (let ((res-string-xml (make-array '(0) :element-type 'base-char
				    :fill-pointer 0 :adjustable t))
	(repo-truename (cl-fad:file-exists-p repo-pathname)))
    ;; if location ois invalid, return nil
    (if (null repo-truename)
	nil
	;; otherwise make the call to svn info
	(multiple-value-bind (prog-status exit-code-r) 
	    (with-output-to-string (s res-string-xml)
	      (external-program:run "svn" 
				    (list "info" repo-truename "--xml")
				    :output s
				    :error :output))
	  ;; return the info from the xml
	  (libxml2.tree:with-parse-document (doc res-string-xml)
	    (make-instance 'repository-info-t 
			   :name (xpath:find-string doc "//entry/@path")
			   :pathname (xpath:find-string doc "//entry/@path")
			   :url (xpath:find-string doc "//entry/url")
			   :root (xpath:find-string doc "//entry/repository/root")
			   :unique-id (xpath:find-string doc "//entry/repository/uuid")
			   :revision (xpath:find-string doc "//entry/@revision")))))))
	      
    

;;; Description:
;;; Returns any modified or not-commited files for an svn repo
;;; at the given location
;;; Returns multiple values:
;;;   Second, a boolean whether the given repo is completely commited
;;;   First, the set of uncommited files (result from svn status call)
(defun svn-repo-check-status (repo-pathname)
  ;;; call the svn status program, storing result in string
  (let ((res-string (make-array '(0) :element-type 'base-char
				:fill-pointer 0 :adjustable t))
	(repo-truename (cl-fad:file-exists-p repo-pathname)))
    ;; if the repository directory/filename does not exist,
    ;; return nil
    (if (null repo-truename)
	(values nil nil)
	;; Otherwise call svn status
	(progn 
	  (let* ((exit-code nil)
		 (res-string 
		  (with-output-to-string (s)
		    (multiple-value-bind (prog-status exit-code-r) 
			(external-program:run "svn" 
					      (list "status" repo-truename)
					      :output s
					      :error :output)
		      (setf exit-code exit-code-r)))))
	    ;; Now parse the entries (but without revisions)
	    ;; and then add the repository revision to them
	    (let* ((entries (parse-svn-status-output res-string))
		   (info (svn-repo-get-info repo-pathname))
		   (revision (revision-of info)))
	      ;; return entries and exit code
	      (values 
	       (mapcar #'(lambda (x) (setf (revision-of x) revision) x)
		       entries)
	       exit-code)))))))



;;; Description:
;;; Given the output of an 'svn status' command, parses the results and
;;; returns a list of repository-entry-t objects
(defun parse-svn-status-output (svn-status-string)
  (let ((lines (split-sequence:split-sequence #\Newline svn-status-string :remove-empty-subseqs t )))
    (mapcar #'(lambda (x) (parse-svn-entry-status-line x)) lines)))


;;; Description:
;;; Parse a single line for an svn status call into a repository-entry-t
(defun parse-svn-entry-status-line (line)
  ;;; if this is an empty line, return nil
  (if ( < (length line) 8)
      nil
      ;; First, get the first character and create a status from it
      (let* ((status (case (char line 0)
		       (" " (case (char line 1)
			      (#\Space :versioned)
			      (#\M :modified)
			      (#\C :conflicted)
			      (otherwise :unknown)))
		       (#\A :added)
		       (#\C :conflicted)
		       (#\D :deleted)
		       (#\I :ignored)
		       (#\M :modified)
		       (#\R :replaced)
		       (#\X :unversioned)
		       (#\? :unversioned)
		       (#\! :missing)
		       (#\~ :obstructed)
		       (otherwise :unknown)))
	     ;; Now just parse the full path
	     (pathname (parse-namestring 
			(string-trim 
			 '(#\Space #\Tab #\Newline) 
			 (subseq line 7))))
	     ;; Use the name poart of pathname as teh name for entry
	     (name (pathname-name pathname)))
	;; Return a new repository-entry-t
	(make-instance 'repository-entry-t
		       :status status
		       :pathname pathname
		       :name name))))



;;; Description:
;;; Updates the given repository specification with information from the
;;; repository it points to.
(defun update-repository-specification (repo-spec)
  (declare (type repository-specification-t repo-spec))
  ;; read the info for the path
  (let ((repo-info (svn-repo-get-info (pathname-of repo-spec))))
    ;; update the fields of the specification
    (setf (revision-of repo-spec) (revision-of repo-info))
    (setf (url-of repo-spec) (url-of repo-info))))


;;; Description:
;;; Add a file to the svn repo.
;;; This does *not* commit the add, just queues it up.
(defun svn-repo-add-file (repo-pathname file-pathname)
  ;; First, cd into the repository directory
  (sb-posix:chdir repo-pathname)
  ;; Now call 'svn add' with the file and the message
  (multiple-value-bind (prog-status exit-code stdout stderr)
      (run-and-get-output "svn" 
			  (list "add" file-pathname)
			  :print-output t
			  :print-error t)
    (if (= exit-code 0)
	t
	nil)))
    

;;; Description:
;;; Commit any changes to the repository given.
(defun svn-repo-commit (repo-pathname message)
  (multiple-value-bind (prog-status exit-code stdout stderr)
      (run-and-get-output "svn"
			  (list "commit" "-m" message (truename repo-pathname))
			  :print-output t
			  :print-error t)
    (= exit-code 0)))


;;; Description:
;;; Given a repository pathname and a branch url, switches the repo
;;; to be a new branch with the given url.
(defun svn-repo-make-branch-for-local-mods (repo-pathname repo-url branch-url &key (message ""))
  ;; Ok, create the branch
  (multiple-value-bind (prog-status exit-code stdout stderr)
      (run-and-get-output "svn"
			  (list "-m" message
				"cp" 
				repo-url
				branch-url)
			  :print-output t
			  :print-error t)
    ;; stop if error
    (if (not (= exit-code 0))
	nil
	;; Now cd into the repo and switch
	(progn (sb-posix:chdir repo-pathname)
	       (multiple-value-bind (prog-status exit-code stdout stderr)
		   (run-and-get-output "svn"
				       (list "switch" branch-url)
				       :print-output t
				       :print-error t)
		 (= exit-code 0))))))



;;; Description:
;;; Ensures that the repostiroy with the given specification is current.
;;; This will make a branch and commit any changes/local-mods of the 
;;; repository.
;;; This *will* change the specification given to update it's data
;;; if the repository is changed.
(defun ensure-svn-repository-is-current (repo-spec 
					 &key 
					 (commit-identifier "automatic commit ptp-experiment-framework"))
  (declare (type repository-specification-t repo-spec))
  (let ((local-entries (svn-repo-check-status (pathname-of repo-spec))))
    ;; if no local modifications, do nothing
    (if (null local-entries)
	t
	;; otherwise, we need to add/commit the changes into the experimental
	;; branch
	(let ((repo-pathname (pathname-of repo-spec))
	      (repo-url (url-of repo-spec))
	      (branch-url (experiment-branch-url-prefix-of repo-spec)))
	  ;; switch local repo into new branch
	  (format t "creating branch at: ~A~%" branch-url)
	  (svn-repo-make-branch-for-local-mods repo-pathname 
					       repo-url 
					       branch-url 
					       :message 
					       (concatenate 'string
							    "Auto-Branching: "
							    commit-identifier))
	  ;; add any files needed
	  (dolist (e local-entries)
	    (if (eql (status-of e) :unversioned)
		(progn
		  (format t "adding '~A' to branch~%" (truename (pathname-of e)))
		  (svn-repo-add-file repo-pathname (pathname-of e)))))
	  ;; commit the entire repo
	  (svn-repo-commit repo-pathname commit-identifier)
	  ;; update the given spec
	  (update-repository-specification repo-spec)))))
