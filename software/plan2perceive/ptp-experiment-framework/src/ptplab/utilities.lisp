
(in-package #:ptplab)


;;; Description:
;;; Given an object, "explodes" it by printtin out it's class, it's slots
;;; and then exxplodes each of the slot values
(defun explode (obj &key (stream t) (indent "") (prefix "") )
  (if (null obj)
      (format stream "~A~ANIL~%" indent prefix)
      ;;; Do something special if obj is not an object instance per say
      (if (not (or (subtypep (type-of obj) 'standard-object)
		   (subtypep (type-of obj) 'class)
		   (subtypep (type-of obj) 'structure-class)
		   (subtypep (type-of obj) 'list)))
	  ;; For non instances we just print them out
	  (format stream "~A~A~A~%" indent prefix obj)
	  ;; for sequences we explode each element
	  (if (subtypep (type-of obj) 'list)
	      (let ((ctr 0))
		(dolist (elt obj)
		  (explode elt :stream stream 
			   :prefix prefix
			   :indent (concatenate 'string 
						indent (format '() "~A." ctr)))
		  (setf ctr (1+ ctr))))
	      ;; Otehrwise we print out the class and slots (exploded)
	      ;; first get the slot names
	      (let* ((class (moptilities:get-class obj))
		     (slot-names (moptilities:slot-names obj)))
		(progn 
		  ;; print out the class name
		  (format stream "~A~A[~A]~%" indent prefix (moptilities:class-name-of obj))
		  ;; for each slot, print out it's name and then 
		  ;; explode it's value with new indentation
		  (dolist (slot-name slot-names)
		    (let* ((str (format '() "~A~A~A~%" indent prefix slot-name))
			   (value (slot-value obj slot-name))
			   (local-new-indent 
			    (concatenate 'string 
					 indent 
					 (make-string (length str) 
						      :initial-element #\Space )))
			   (new-indent 
			    (concatenate 'string
					 indent
					 (make-string 4
						      :initial-element #\Space))))
		      (format stream str)
		      (explode value :stream stream :indent new-indent :prefix prefix)))))))))
  


;;; Description:
;;; Run an external program and return a quad tuple (mutliple values)
;;; of status, exitcode, output, and erroroutput
(defun run-and-get-output (program args 
			   &key 
			   input if-input-does-not-exist
			   environment replace-environment-p
			   (print-output nil)
			   (print-error nil))
  (let ((out (make-string-output-stream))
	(err (make-string-output-stream)))
    (multiple-value-bind (prog-status exit-code)
	(external-program:run program args
			      :input input
			      :if-input-does-not-exist if-input-does-not-exist
			      :output out
			      :error err
			      :environment environment
			      :replace-environment-p replace-environment-p)
      (let ((out-string (get-output-stream-string out))
	    (err-string (get-output-stream-string err)))
	(if print-error
	    (format t "Program stdout: ~A~%" out-string))
	(if print-error
	    (format t "Program stderr: ~A~%" err-string))
	(values prog-status exit-code 
		(get-output-stream-string out)
		(get-output-stream-string err))))))



			   
			   
;;; Description:
;;; Given a pathanme, return a pathname that forces the given pathname to 
;;; be a directory (so if it is a file will make it into a directory structure)
(defun force-directory (path-spec)
  (if (null (pathname-name path-spec))
      path-spec
      (merge-pathnames
       (make-pathname
	:directory (list :relative (file-namestring path-spec)))
	(make-pathname
	 :host (pathname-host path-spec)
	 :device (pathname-device path-spec)
	 :version (pathname-version path-spec)
	 :directory (pathname-directory path-spec)))))