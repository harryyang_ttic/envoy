
import sys
import os


if len( sys.argv ) < 4:
    print "usage: " + sys.argv[0] + " master_log meta_file output_dir"
    exit(-1)


master_log_filename = sys.argv[1]
meta_filename = sys.argv[2]
output_dir = sys.argv[3]

base_image_dir = "/home/velezj/projects/object_detector_pose_planning/data/door-detector/data-Nov2010/bb_set_full/Images/";

size = "300x300"

# make the output dir
try :
    os.makedirs( output_dir )
except :
    pass

# create a meta dictionary
meta = {}
fmeta = open( meta_filename )
for line in fmeta:
    toks = line.split()
    meta[ toks[0] ] = toks[1]

# read in every line from master log
# scale picture and copy to output directory
flog = open( master_log_filename)
for line in flog :
    
    # split 
    toks = line.split()
    
    # build picutre name
    orig_name = base_image_dir + "log_image_" + meta[ toks[0] ] + ".jpg"
    
    # build the result picture name
    result_name = output_dir + toks[0] + ".jpg"
    
    # draw and scale the iamge using ImageMagik
    cmd = "convert %s -stroke red -strokewidth 3 -fill none -draw \"rectangle %s,%s %s,%s\" -resize %s %s" % ( orig_name, toks[2], toks[3], toks[4], toks[5], size, result_name )
    print cmd + "\n"
    os.system( cmd )

