
import sys
import os


if len(sys.argv) < 4:
    print "Usage: " + sys.argv[0] + " <master_log> <meta_log> <result_log>"
    exit(-1)

base_log_filename = sys.argv[1]
meta_log_filename = sys.argv[2]
result_filename = sys.argv[3]

### read in the meta file
meta = {}
fmeta = open( meta_log_filename )
for line in fmeta:
    toks = line.split()
    meta[ toks[0] ] = toks[2]

## Read in the master log and write out lines wfused with label from meta
fout = open( result_filename, "w" )
flog = open( base_log_filename )
for line in flog:
    toks = line.split()
    print "Split Line: " + str(toks)
    if toks[0] in meta:
        toks[9] = meta[ toks[0] ]
    fout.write( toks[0] + " " + toks[1] + " " + toks[2] + " " +
                toks[3] + " " + toks[4] + " " + toks[5] + " " +
                toks[6] + " " + toks[7] + " " + toks[8] + " " +
                toks[9] + "\n" );


