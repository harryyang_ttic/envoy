function [hyp0 hyp1 x0 y0 x1 y1] = train_correlated_gps()

  master_log = '/home/velezj/projects/object_detector_pose_planning/envoy/software/plan2perceive/training-data/master-logs/doors/fused.master-log';
  d = dlmread( master_log );

  % filter data to those we want
  door1 = [166:169 189 194 197:199 204:207 210 212 215 264 267:269 271 275 278 279 282:288 294:296 443:453 457 458 460:466 ];
  door0 = [105:108 113 118:119 123:128 163:165 467:487 809 810 815:817 820:822 826:828 1066 1067 1698:1708];
  d = d( [door0 door1], : );
  
  d = d( ~isnan( sum(d,2) ), : );

  
  % Normalize depth to be positive, angles to go 0-180 degre
  d( :, 7 ) = abs( d(:, 7 ) );
  for i=1:size(d,1)
    while( d(i,8) > pi )
      d(i,8) = d(i,8) - pi;
    end
    while( d(i,8) < -pi )
      d(i,8) = d(i,8) + pi;
    end
    d( i, 8 ) = abs( d(i,8) );
    %if( d(i,8) > pi/2 )
    %  d(i,8) = pi - d(i,8);
    %end
  end

  % get rid of angle graeter than pi/4
  %d = d( d(:,8) < pi/4, : );
  
  % Split data by label
  d0 = d( d(:,10) == 0, : );
  d1 = d( d(:,10) == 1, : );
  
  % construct and minize gp likelihood parameters
  mean0 = @meanZero;
  mean1 = @meanZero;
  cov0 = @covSEiso;
  cov1 = @covSEiso;
  lik0 = @likGauss;
  lik1 = @likGauss;
  hyp0.cov = [0;0]; hyp0.lik = log(0.1);
  hyp1.cov = [0;0]; hyp1.lik = log(0.1);
  
  % minize gp likelihood 
  x0 = d0(:,7:8);
  y0 = d0(:,2);
  hyp0 = minimize( hyp0, @gp, -100, @infExact, mean0, cov0, lik0, x0, y0 );
  x1 = d1(:,7:8);
  y1 = d1(:,2);
  hyp1 = minimize( hyp1, @gp, -100, @infExact, mean1, cov1, lik1, x1, y1 );
  
end
