
function create_cpp_model_file( filename, ind_hyp0, ind_hyp1, corr_hyp0, corr_hyp1, ind_x0, ind_y0, ind_x1, ind_y1 )

  fd = fopen( filename, 'w' );
  
  % create four gps and their hyper parameters
  fprintf( fd, 'sparse_gp_hyperparameters_t ind_hyp0;\n' );
  fprintf( fd, 'sparse_gp_hyperparameters_t ind_hyp1;\n' );
  fprintf( fd, 'sparse_gp_hyperparameters_t corr_hyp0;\n' );
  fprintf( fd, 'sparse_gp_hyperparameters_t corr_hyp1;\n' );
  fprintf( fd, 'ind_hyp0.sigma = %f;\n', (exp(ind_hyp0.cov(1)))^2 );
  fprintf( fd, 'ind_hyp0.scale = %f;\n', (exp(ind_hyp0.cov(2)))^2 );
  fprintf( fd, 'ind_hyp0.capacity = 500;\n' );
  fprintf( fd, 'ind_hyp1.sigma = %f;\n', (exp(ind_hyp1.cov(1)))^2 );
  fprintf( fd, 'ind_hyp1.scale = %f;\n', (exp(ind_hyp1.cov(2)))^2 );
  fprintf( fd, 'ind_hyp1.capacity = 500;\n' );
  fprintf( fd, 'corr_hyp0.sigma = %f;\n', (exp(corr_hyp0.cov(1)))^2 );
  fprintf( fd, 'corr_hyp0.scale = %f;\n', (exp(corr_hyp0.cov(2)))^2 );
  fprintf( fd, 'corr_hyp0.capacity = 500;\n' );
  fprintf( fd, 'corr_hyp1.sigma = %f;\n', (exp(corr_hyp1.cov(1)))^2 );
  fprintf( fd, 'corr_hyp1.scale = %f;\n', (exp(corr_hyp1.cov(2)))^2 );
  fprintf( fd, 'corr_hyp1.capacity = 500;\n' );
  fprintf( fd, '\n\n//// THE DATA\n\n' );
  fprintf( fd, 'dist_and_orientation_t dao( 0, rad(0).from( angle_t::X_AXIS ) );\n' );
  fprintf( fd, 'detection_and_location_t dx( 0, dao );\n' );

  % the data for each GP
  fprintf( fd, 'gp_ind0 = sparse_online_gp_t( ind_hyp0 );\n' );
  for i=1:size( ind_x0, 1 )
    fprintf( fd, 'dx.detection_value = %f;\n', ind_y0(i) );
    fprintf( fd, 'dx.location.dist = %f;\n', ind_x0(i,1) );
    fprintf( fd, 'dx.location.orientation = rad(%f).from(angle_t::X_AXIS);\n', ind_x0(i,2) );
    fprintf( fd, 'gp_ind0.add( dx );\n' );
  end 
  fprintf( fd, 'gp_ind1 = sparse_online_gp_t( ind_hyp1 );\n' );
  for i=1:size( ind_x1, 1 )
    fprintf( fd, 'dx.detection_value = %f;\n', ind_y1(i) );
    fprintf( fd, 'dx.location.dist = %f;\n', ind_x1(i,1) );
    fprintf( fd, 'dx.location.orientation = rad(%f).from(angle_t::X_AXIS);\n', ind_x1(i,2) );
    fprintf( fd, 'gp_ind1.add( dx );\n' );
  end
  fprintf( fd, 'gp_corr0 = sparse_online_gp_t( corr_hyp0 );\n' ); 
  fprintf( fd, 'gp_corr1 = sparse_online_gp_t( corr_hyp1 );\n' );
  
						
  fclose( fd );
	  
end