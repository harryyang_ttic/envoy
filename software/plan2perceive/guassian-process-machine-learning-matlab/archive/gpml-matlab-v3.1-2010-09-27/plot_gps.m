function plot_gps( hyp, x, y )

  % make a dense set of samples
  z = [];
  depths = linspace( 0, 6, 50 );
  angles = linspace( 0, pi, 50 );
  zmap = [];
  
  for d=1:length(depths)
    for a=1:length(angles)
      z( end+1, : ) = [ depths(d) angles(a) ];
      zmap( end+1, : ) = [d a];
    end
  end

  % compute mean and s2 functions
  [m s2] = gp( hyp, @infExact, @meanZero, @covSEiso, @likGauss, x, y, z );

  % map mean to matrix
  mmat = [];
  for i=1:size(zmap,1)
    mmat( zmap(i,1), zmap(i,2) ) = m(i);
  end
  
  % plot the mean as a surface
  surf( depths, angles, mmat );

end
