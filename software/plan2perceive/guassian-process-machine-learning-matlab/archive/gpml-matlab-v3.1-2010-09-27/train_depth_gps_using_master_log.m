
function [hyp0 hyp1] = train_depth_gps_using_master_log( master_log )

  
  d = dlmread( master_log );
  
% Normalize depth to be positive, angles to go 0-180 degre
  d( :, 7 ) = abs( d(:, 7 ) );
for i=1:size(d,1)
	while( d(i,8) > pi )
	  d(i,8) = d(i,8) - pi;
    end
    while( d(i,8) < -pi )
      d(i,8) = d(i,8) + pi;
    end
    d( i, 8 ) = abs( d(i,8) );
  end
  
  % Split data by label
  d0 = d( d(:,10) == 0, : );
d1 = d( d(:,10) == 1, : );
  
  % construct and minize gp likelihood parameters
  mean0 = @meanZero;
mean1 = @meanZero;
cov0 = @covSEiso;
cov1 = @covSEiso;
lik0 = @likGauss;
lik1 = @likGauss;
hyp0.cov = [0;0]; hyp0.lik = log(0.1);
hyp1.cov = [0;0]; hyp1.lik = log(0.1);

  % minize gp likelihood 
  x0 = d0(:,7);
y0 = d0(:,2);
hyp0 = minimize( hyp0, @gp, -100, @infExact, mean0, cov0, lik0, x0, y0 );
x1 = d1(:,7);
y1 = d1(:,2);
hyp1 = minimize( hyp1, @gp, -100, @infExact, mean1, cov1, lik1, x1, y1 );

end
