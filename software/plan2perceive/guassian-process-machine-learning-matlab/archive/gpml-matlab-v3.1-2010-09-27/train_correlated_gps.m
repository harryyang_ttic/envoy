function [hyp0 hyp1 x0 y0 x1 y1] = train_correlated_gps()

  master_log = '/home/velezj/projects/object_detector_pose_planning/envoy/software/plan2perceive/training-data/master-logs/floor_3_and_1_lcmlog-2011-09-28.00-subset.master-log';
  d = dlmread( master_log );

  % filter data to those we want
  notext0 = [ 35 43 44 46 47 49:51 53:56 61 67 73 74 79 80 85:88 93 98 99 105 106 109 110 117 118 132 146 155 172 188 189 210 211 232 252 276 277 296 318 319 338 353 362 363 369 370 380 381 390 391 394 400 404 411 417 422 427 428 433 434 440 441 447 455 460 461 466 468 470 475 476 483 484 489 492 493 495 496 498 499 501 502:507 509:513 515 516 518 520 522 524 527 529 532 534 536 539 543 545 547 ];

  text0 = [ 1078:1717 2254:3641 ];
  d = d( [notext0 text0], : );

  
  % Normalize depth to be positive, angles to go 0-180 degre
  d( :, 7 ) = abs( d(:, 7 ) );
  for i=1:size(d,1)
    while( d(i,8) > pi )
      d(i,8) = d(i,8) - pi;
    end
    while( d(i,8) < -pi )
      d(i,8) = d(i,8) + pi;
    end
    d( i, 8 ) = abs( d(i,8) );
    %if( d(i,8) > pi/2 )
    %  d(i,8) = pi - d(i,8);
    %end
  end

  % get rid of angle graeter than pi/4
  %d = d( d(:,8) < pi/4, : );
  
  % Split data by label
  d0 = d( d(:,10) == 0, : );
  d1 = d( d(:,10) == 1, : );
  
  % construct and minize gp likelihood parameters
  mean0 = @meanZero;
  mean1 = @meanZero;
  cov0 = @covSEiso;
  cov1 = @covSEiso;
  lik0 = @likGauss;
  lik1 = @likGauss;
  hyp0.cov = [0;0]; hyp0.lik = log(0.1);
  hyp1.cov = [0;0]; hyp1.lik = log(0.1);
  
  % minize gp likelihood 
  x0 = d0(:,7:8);
  y0 = d0(:,2);
  hyp0 = minimize( hyp0, @gp, -100, @infExact, mean0, cov0, lik0, x0, y0 );
  x1 = d1(:,7:8);
  y1 = d1(:,2);
  hyp1 = minimize( hyp1, @gp, -100, @infExact, mean1, cov1, lik1, x1, y1 );
  
end
