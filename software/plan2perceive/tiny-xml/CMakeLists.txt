cmake_minimum_required(VERSION 2.6.0)

# pull in the pods macros. See cmake/pods.cmake for documentation
set(POD_NAME tiny-xml)
include(cmake/pods.cmake)


set( CMAKE_C_FLAGS_DEBUG "-O0 -g3 -ggdb3" )
set( CMAKE_CXX_FLAGS_DEBUG "-O0 -g3 -ggdb3" )


#tell cmake to build these subdirectories
add_subdirectory(src)
#add_subdirectory(src/test)


