
#include <ptp-common-coordinates/plane_t.hpp>
#include <ptp-common-coordinates/identity_manifold_t.hpp>
#include <ptp-common-test/test.hpp>


using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;

//==================================================================

int test_compile()
{
  start_of_test;

  // Create a base manifold and a plane on it
  manifold_tp m_base( new identity_manifold_t( manifold_t::BASE_MANIFOLD ) );
  simplex_tp n1 = line_simplex( coordinate( 0, 0, 0 ),
				coordinate( 0, 0, 1 ),
				m_base );
  simplex_tp x1 = line_simplex( coordinate( 0, 0, 0 ),
				coordinate( 1, 0, 0 ),
				m_base );
  plane_t p1( n1, x1 );
  
  
  
  end_of_test;
}

//==================================================================

int test_coordinates()
{
  start_of_test;

  // Create a base manifold and a plane on it
  manifold_tp m_base( new identity_manifold_t( manifold_t::BASE_MANIFOLD ) );
  simplex_tp n1 = line_simplex( coordinate( 0, 0, 0 ),
				coordinate( 0, 0, 1 ),
				m_base );
  simplex_tp x1 = line_simplex( coordinate( 0, 0, 0 ),
				coordinate( 1, 0, 0 ),
				m_base );
  simplex_tp x2 = line_simplex( coordinate( 0, 0, 0 ),
				coordinate( 0, 1, 0 ),
				m_base );
  plane_tp p1( new plane_t( n1, x1 ) );
  plane_tp p2( new plane_t( n1, x2 ) );
  
  // Check some points in the plane and base manifolds
  coordinate_t c1 = coordinate( 1, 0 ).on( p1 );
  coordinate_t c2 = coordinate( 1, 0 ).on( p2 );
  coordinate_t c3 = coordinate( 0, 1 ).on( p1 );
  //std::cout << "C1          : " << c1 << std::endl;
  //std::cout << "C1 on base  : " << c1.on( m_base ) << std::endl;
  //std::cout << "C1 anon base: " << coordinate( 1, 0, 0 ).on( m_base ) << std::endl;
  //std::cout << "C2:           " << c2 << std::endl;
  //std::cout << "C2 on base:   " << c2.on( m_base ) << std::endl;
  //std::cout << "C2 anon base: " << coordinate( 0, 1, 0 ).on( m_base ) << std::endl;
  //std::cout << "C3:           " << c2 << std::endl;
  //std::cout << "C3 on base:   " << c2.on( m_base ) << std::endl;
  //std::cout << "C3 anon base: " << coordinate( 0, 1, 0 ).on( m_base ) << std::endl;
  test_assert( float_equal( c1.on( m_base ), coordinate( 1, 0, 0 ).on( m_base ) ) );
  test_assert( float_equal( c2.on( m_base ), coordinate( 0, 1, 0 ).on( m_base ) ) );
  test_assert( float_equal( c3.on( m_base ), coordinate( 0, 1, 0 ).on( m_base ) ) );

  // Check off-origin planes
  coordinate_t o3 = coordinate( 0, 1, 0 ).on( m_base );
  simplex_tp n3 = line_simplex( o3.raw_coordinate(),
				coordinate( 0, 2, 0 ),
				m_base );
  simplex_tp x3 = line_simplex( o3.raw_coordinate(),
				coordinate( 1, 1, 0 ),
				m_base);
  plane_tp p3( new plane_t( n3, x3 ) );
  coordinate_t c4 = coordinate( 1, 0 ).on( p3 );
  coordinate_t c5 = coordinate( 0, -1 ).on( p3 );
  //std::cout << "C4:           " << c4 << std::endl;
  //std::cout << "C4 on base:   " << c4.on( m_base ) << std::endl;
  //std::cout << "C4 anon base: " << coordinate( 1, 1, 0 ).on( m_base ) << std::endl;
  //std::cout << "C5:           " << c5 << std::endl;
  //std::cout << "C5 on base:   " << c5.on( m_base ) << std::endl;
  //std::cout << "C5 anon base: " << coordinate( 0, 1, 1 ).on( m_base ) << std::endl;
  test_assert( float_equal( c4.on( m_base ), coordinate( 1, 1, 0 ).on( m_base ) ) );
  test_assert( float_equal( c5.on( m_base ), coordinate( 0, 1, 1 ).on( m_base ) ) );

  // Check non-axis aligned planes
  coordinate_t o4 = coordinate( 0, 0, 1 ).on( m_base );
  simplex_tp n4 = line_simplex( o4.raw_coordinate(),
				coordinate( 0, tan(45*M_PI/180), 0 ),
				m_base );
  simplex_tp x4 = line_simplex( o4.raw_coordinate(),
				coordinate( 0, tan(45*M_PI/180), 2 ),
				m_base );
  plane_tp p4( new plane_t( n4, x4 ) );
  float sqr2 = sqrt( 2 );
  coordinate_t c6 = coordinate( 1, 0 ).on( p4 );
  coordinate_t c7 = coordinate( 0, 0.1 ).on( p4 );
  coordinate_t c6_true = 1*coordinate( 0, tan(45*M_PI/180), 2 ).on(m_base);
  coordinate_t c7_true = o4 + 0.1*sqr2*coordinate( 1, 0, 0 ).on(m_base);
  //std::cout << "C6:           " << c6 << std::endl;
  //std::cout << "C6 on base:   " << c6.on( m_base ) << std::endl;
  //std::cout << "C6 true:      " << c6_true << std::endl;
  //std::cout << "C7:           " << c7 << std::endl;
  //std::cout << "C7 on base:   " << c7.on( m_base ) << std::endl;
  //std::cout << "C7 true:      " << c7_true << std::endl;
  test_assert( float_equal( c6.on( m_base ), c6_true ) );
  test_assert( float_equal( c7.on( m_base ), c7_true ) );
  
  
  end_of_test;
}

//==================================================================

int test_inverse_coordinates()
{
  start_of_test;

  // Create a base manifold and a plane on it
  manifold_tp m_base( new identity_manifold_t( manifold_t::BASE_MANIFOLD ) );
  simplex_tp n1 = line_simplex( coordinate( 0, 0, 0 ),
				coordinate( 0, 0, 1 ),
				m_base );
  simplex_tp x1 = line_simplex( coordinate( 0, 0, 0 ),
				coordinate( 1, 0, 0 ),
				m_base );
  simplex_tp x2 = line_simplex( coordinate( 0, 0, 0 ),
				coordinate( 0, 1, 0 ),
				m_base );
  plane_tp p1( new plane_t( n1, x1 ) );
  plane_tp p2( new plane_t( n1, x2 ) );

  // Create a coordinate in the manifold and then
  // get teh plane coordinate for it
  coordinate_t c8 = coordinate( 1, 0, 0 ).on( m_base );
  coordinate_t c8_plane = c8.on( p1 );
  coordinate_t c8_plane_true = coordinate( 1, 0 ).on( p1 );
  //std::cout << "C8:              " << c8 << std::endl;
  //std::cout << "C8 (plane):      " << c8_plane << std::endl;
  //std::cout << "C8 (plane true): " << c8_plane_true << std::endl;
  test_assert( float_equal( c8_plane, c8_plane_true ) );

  end_of_test;
}

//==================================================================

int main( int argc, char** argv )
{

  run_test( test_compile() );
  run_test( test_coordinates() );
  run_test( test_inverse_coordinates() );

  return 0;
}
