
#include <ptp-common-coordinates/angle_coverage.hpp>
#include <ptp-common/common.hpp>
#include <ptp-common-test/test.hpp>
#include <iostream>


using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;


//====================================================================

int test_compile()
{
  start_of_test

  // Create a new conservative angle t
  conservative_angle_coverage_t c1( 0.2 );
  optimistic_angle_coverage_t o1( 0.4 );

  // try out operatoirs
  c1 + c1;
  c1 - c1;
  c1 * c1;
  c1 / c1;
  c1 < c1;
  c1 > c1;
  c1 == c1;
  o1 + o1;
  o1 - o1;
  o1 * o1;
  o1 / o1;
  o1 < o1;
  o1 > o1;
  o1 == o1;
  
  // mixed operators
  c1 + o1;
  c1 - o1;
  c1 * o1;
  c1 / o1;
  c1 < o1;
  c1 > o1;
  c1 == o1;
  o1 + c1;
  o1 - c1;
  o1 * c1;
  o1 / c1;
  o1 < c1;
  o1 > c1;
  o1 == c1;
  
  // conversion to angle_coverage_t
  angle_coverage_t a1 = c1;
  angle_coverage_t a2 = o1;
  
  test_assert( c1 < o1 );
  test_assert( c1 + o1 < optimistic_angle_coverage_t(1.0) );

  end_of_test
}


//====================================================================

int main( int argc, char** argv )
{

  run_test( test_compile() );

  return 0;
}
