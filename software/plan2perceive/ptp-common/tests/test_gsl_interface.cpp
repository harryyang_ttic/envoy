
#include <ptp-common-interfaces/gsl_interface.hpp>
#include <ptp-common-test/test.hpp>
#include <iostream>

using namespace ptp;
using namespace ptp::common;
using namespace ptp::interfaces;

//=======================================================================

int test_convert()
{
  start_of_test;

  // Create a matrix and a math_vector, convert to
  // gsl versions, and print all
  matrix_t m1( 2, 3 );
  math_vector_t v1( 2 );
  float ctr = 0;
  for( size_t i = 0; i < m1.rows(); ++i ) {
    for( size_t j = 0; j < m1.cols(); ++j ) {
      m1( i,j ) = ctr;
      ++ctr;
    }
  }
  for( size_t i = 0; i < v1.size(); ++i ) {
    v1( i ) = ctr;
    ++ctr;
  }
  gsl_vector_tp gv1 = gsl::create_gsl_vector( v1 );
  gsl_matrix_tp gm1 = gsl::create_gsl_matrix( m1 );
  
  std::cout << m1 << std::endl;
  std::cout << v1 << std::endl;
  gsl_matrix_fprintf( stdout, gm1.get(), "%f" );
  fprintf( stdout, "\n" );
  gsl_vector_fprintf( stdout, gv1.get(), "%f" );
  fprintf( stdout, "\n" );

  end_of_test;
}

//=======================================================================

int main( int argc, char** argv )
{

  run_test( test_convert() );

  return 0;
}
