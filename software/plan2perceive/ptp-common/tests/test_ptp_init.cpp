
#include <ptp-common-framework/init.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <ptp-common-test/test.hpp>
#include <iostream>
#include <fstream>


using namespace ptp;
using namespace ptp::common;
using namespace ptp::common::framework;
using namespace boost::property_tree::xml_parser;


//================================================================

int test_init(int argc, char** argv )
{
  start_of_test;

  // simply initialzie the ptp framework with given command line
  ptp_init( argc, argv );
  
  // Now print out hte properties
  //std::cout << "TESTING INIT: " << std::endl;
  write_xml( std::cout, ptp_config() );

  end_of_test;
}


//================================================================

int test_streams(int argc, char** argv )
{
  start_of_test;

  // simply initialzie the ptp framework with given command line
  ptp_init( argc, argv );

  // now get some streams
  ostream out = ptp_output_file_stream( "out.txt" );
  ostream deb = ptp_debug_file_stream( "debug.txt" );
  ostream aout = ptp_analysis_file_stream( "analysis.txt" );
  istream in = ptp_input_file_stream( "input.txt" );

  // wite some stuff
  out << "test out";
  deb << "test debug ";
  aout << "analysis: ";
  
  
  // Now print out hte properties
  //std::cout << "TESTING STREAMS:" << std::endl;
  write_xml( std::cout, ptp_config() );
  std::ofstream fout( ptp_config<std::string>( "ptp:config.ptp:self-pathname", "config.xml").c_str());
  write_xml( fout , ptp_config());

  end_of_test;
}

//================================================================

int main( int argc, char** argv )
{

  //run_test( test_init(argc,argv) );
  run_test( test_streams(argc, argv) );

  return 0;
}
