
#include <ptp-common-coordinates/angle.hpp>
#include <ptp-common/common.hpp>
#include <ptp-common-test/test.hpp>
#include <iostream>


using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;


int test_compile()
{
  start_of_test

  angle_t a1 = deg( 45 ).from( angle_t::X_AXIS );
  angle_t a2 = deg( 45 ).from( angle_t::X_AXIS );
  angle_t a3 = deg( 45 ).from( a1 );
  angle_t a4 = a3 - a1;
  angle_t a5 = a3 - a2;
  angle_t a6 = deg( 180 ).from( angle_t::X_AXIS ) - a3;
  angle_t a7 = rad( -M_PI ).from( a3 ) + a1;
  
  test_assert( a4 == deg( 45 ).from( angle_t::X_AXIS ) );
  test_assert( a5 == deg( 45 ).from( angle_t::X_AXIS ) );
  test_assert( a6 == deg( 90 ).from( angle_t::X_AXIS ) );
  test_assert( float_equal( a7, deg( -45 ).from( angle_t::X_AXIS ), deg( 1e-6 ).from( angle_t::X_AXIS ) ) );
  
  
  end_of_test
}




int main( int argc, char** argv )
{

  run_test(test_compile());
 
  return 0;
}
