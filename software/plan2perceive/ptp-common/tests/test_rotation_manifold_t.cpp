
#include <ptp-common-coordinates/rotation_manifold_t.hpp>
#include <ptp-common-coordinates/identity_manifold_t.hpp>
#include <ptp-common-test/test.hpp>
#include <iostream>


using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;

//============================================================

int test_rotation()
{
  start_of_test;

  //  Create a base manifold
  manifold_tp base = manifold_tp( new identity_manifold_t( manifold_t::BASE_MANIFOLD ) );

  // Ok, now create a rotated manifold
  manifold_tp r1 = rotate( line_simplex( coordinate( 0,0,0 ),
					 coordinate( 0,0,1 ),
					 base ),
			   deg(90).from(angle_t::X_AXIS),
			   base );
  
  // Make sure axis of rotation are correct
  coordinate_t c1 = coordinate( 1,0,0 ).on( r1 );
  coordinate_t c1_true = coordinate( 0,1,0 ).on( base );
  coordinate_t c1_base = c1.on( base );
  coordinate_t c2 = coordinate( 0,1,0 ).on( r1 );
  coordinate_t c2_true = coordinate( -1,0,0 ).on( base );
  coordinate_t c2_base = c2.on( base );
  coordinate_t c3 = coordinate( 0,0,1 ).on( r1 );
  coordinate_t c3_true = coordinate( 0,0,1 ).on( base );
  coordinate_t c3_base = c3.on( base );
  test_assert( float_equal( c1_true, c1_base ) );
  test_assert( float_equal( c2_true, c2_base ) );
  test_assert( float_equal( c3_true, c3_base ) );
  
  // Make sure axis of base are coorect in rotated
  coordinate_t c4 = coordinate( 1,0,0 ).on( base );
  coordinate_t c4_true = coordinate( 0,-1,0 ).on( r1 );
  coordinate_t c4_r = c4.on( r1 );
  coordinate_t c5 = coordinate( 0,1,0 ).on( base );
  coordinate_t c5_true = coordinate( 1,0,0 ).on( r1 );
  coordinate_t c5_r = c5.on( r1 );
  coordinate_t c6 = coordinate( 0,0,1 ).on( base );
  coordinate_t c6_true = coordinate( 0,0,1 ).on( r1 );
  coordinate_t c6_r = c6.on( r1 );
  test_assert( float_equal( c4_true, c4_r ) );
  test_assert( float_equal( c5_true, c5_r ) );
  test_assert( float_equal( c6_true, c6_r ) );


  end_of_test;
}

//============================================================

int main( int argc, char** argv )
{

  run_test( test_rotation() );

  return 0;
}
