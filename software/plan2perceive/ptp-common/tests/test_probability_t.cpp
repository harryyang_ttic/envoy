
#include <ptp-common/probability.hpp>
#include <ptp-common/common.hpp>
#include <ptp-common-test/test.hpp>
#include <iostream>


using namespace ptp;
using namespace ptp::common;




//=======================================================================

// Description:
// Test the probability classes to ensure they compile
int test_compile_raw()
{
  start_of_test
  raw_probability_t p1( 0.2 );
  raw_probability_t p2( p1 );
  raw_log_probability_t lp1( log(0.5) );
  raw_log_probability_t lp2( lp1 );
  raw_probability_t p3( lp1.as_probability() );
  raw_log_probability_t lp3( p3 );
  raw_probability_t p4( 0.9 );
  
  std::cout << "P : " << p1 << " " << p2 << " " << p3 << std::endl;
  std::cout << "LP: " << lp1 << " " << lp2 << " " << lp3 << std::endl;
  
  p1 + p2;
  p1 - p2;
  p1 * p2;
  p1 / p2;
  
  lp1 + lp2;
  lp1 - lp2;
  lp1 * lp2;
  lp1 / lp2;
  
  p1 + 0.15f;
  p1 - 0.15f;
  p1 * 0.5f;
  p1 / 0.5f;
  
  lp1 + 0.15f;
  lp1 - 0.15f;
  lp1 * 0.5f;
  lp1 / 0.5f;
  
  p1 + lp1;
  p1 - lp1;
  p1 * lp1;
  p1 / lp1;
  
  lp1 + p1;
  lp1 - p1;
  lp1 * p1;
  lp1 / p4; // divide by something that still keeps it a probability

  end_of_test
}

//=======================================================================

int test_raw_normal_to_log()
{
  start_of_test
  raw_probability_t p1( 0.1f );
  raw_log_probability_t lp1( p1 );
  raw_probability_t p2 = lp1.as_probability();
  
  // check consistentcy
  test_assert( float_equal( p1, p2 ) );
  
  // check some absolute values
  test_assert( float_equal( lp1.as_float(), log( p1.as_float() ) ) );
  
  end_of_test
}

//=======================================================================

int test_probability_t()
{
  start_of_test
  probability_t p1( 0.1, false );
  // the next constructor takes in a normal proability
  // but internally uses log implementation, hence 0.1 *is* the probability
  // not exp(0.1)
  probability_t p2( 0.1, true ); 
  

  test_assert( float_equal(p1 + p2, raw_probability_t( 0.1 + 0.1 ) ) );
  test_assert( float_equal(p1 - p2, raw_probability_t( 0.1 - 0.1 ) ) );
  test_assert( float_equal(p1 * p2, raw_probability_t( 0.1 * 0.1 ) ) );
  test_assert( float_equal(p1 / p2, raw_probability_t( 0.1 / 0.1 ) ) );

  test_assert( float_equal(p1 + p2, raw_log_probability_t( log( 0.1 + 0.1 ) ) ) );
  test_assert( float_equal(p1 - p2, raw_log_probability_t( log( 0.1 - 0.1 ) ) ) );
  test_assert( float_equal(p1 * p2, raw_log_probability_t( log( 0.1 * 0.1 ) ) ) );
  test_assert( float_equal(p1 / p2, raw_log_probability_t( log( 0.1 / 0.1 ) ) ) );

  test_assert( float_equal(p2 + p1, raw_probability_t( 0.1 + 0.1 ) ) );
  test_assert( float_equal(p2 - p1, raw_probability_t( 0.1 - 0.1 ) ) );
  test_assert( float_equal(p2 * p1, raw_probability_t( 0.1 * 0.1 ) ) );
  test_assert( float_equal(p2 / p1, raw_probability_t( 0.1 / 0.1 ) ) );

  test_assert( float_equal(p2 + p1, raw_log_probability_t( log( 0.1 + 0.1 ) ) ) );
  test_assert( float_equal(p2 - p1, raw_log_probability_t( log( 0.1 - 0.1 ) ) ) );
  test_assert( float_equal(p2 * p1, raw_log_probability_t( log( 0.1 * 0.1 ) ) ) );
  test_assert( float_equal(p2 / p1, raw_log_probability_t( log( 0.1 / 0.1 ) ) ) );

  end_of_test
}

//=======================================================================

void make_too_small( const raw_probability_t& orig )
{
  raw_probability_t p(orig);
  for( size_t ctr = 0; ctr < 100; ++ctr ) {
    p *= raw_probability_t(0.1);
  }
}

void make_too_large( const raw_probability_t& orig ) 
{
  raw_probability_t p(orig);
  for( size_t ctr = 0; ctr < 10; ++ctr ) {
    p += raw_probability_t(0.100001);
  }
}

int test_raw_probability_too_small_error()
{
  start_of_test

  raw_probability_t p( 0.1 );

  // check that we throw exception when too small
  test_exception( make_too_small(p) , exceptions::probability_is_too_small_error );

  // check that we throw exception when too large
  test_exception( make_too_large(p), exceptions::probability_is_greater_than_one_error );

  end_of_test
}

//=======================================================================

int test_probability_expression_t()
{
  start_of_test
    
    // OK, we want to make sure that in-flight computations do *not*
    // generate any exceptions
    
  probability_t p1( 0.1, false );
  probability_t p2( 0.1, true );
  
  // make sure that unnormalized probabilities are not accepted
  test_exception( probability_t ptemp = p1 + 10.0, exceptions::probability_is_greater_than_one_error );
  test_exception( probability_t ptemp = p2 + 10.0, exceptions::probability_is_greater_than_one_error );
  
  // make sure that in-the-middle computation *can* be unnormalized
  test_assert( float_equal( static_cast<probability_t>((p1 + 15.0) / 16.0 ), raw_probability_t( 15.1 / 16.0 ) ) ) ;
  test_assert( float_equal( static_cast<probability_t>((p2 - 15) / 16.0 ), raw_probability_t( 15.1 / 16.0 ) ) ) ;
    

  end_of_test
}

//=======================================================================

int main( int argc, char** argv ) 
{

  try {

  run_test(test_compile_raw());
  run_test(test_raw_normal_to_log());
  run_test(test_probability_t());
  run_test(test_raw_probability_too_small_error());
  run_test(test_probability_expression_t());

  } catch (...) {
    std::cout << "TOPLEVEL: " << boost::current_exception_diagnostic_information() << std::endl;
    throw;
  }
  
  return 0;

}
