
#include <ptp-common-coordinates/manifold.hpp>
#include <ptp-common-coordinates/identity_manifold_t.hpp>
#include <ptp-common-test/test.hpp>
#include <iostream>


using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;


//===================================================================

int test_compile()
{
  start_of_test

  // Create a manifold, a coordinate on it
  manifold_tp m1( new identity_manifold_t( manifold_t::BASE_MANIFOLD ) );
  std::vector<float> cdata;
  cdata.push_back( 1.0 );
  cdata.push_back( 2.0 );
  coordinate_t c1 = raw_coordinate_t( cdata ).on( m1 );


  end_of_test
}

//===================================================================

int test_base_manifold()
{
  start_of_test

  // create two manifold, both BASE
  manifold_tp m1( new identity_manifold_t( manifold_t::BASE_MANIFOLD ) );
  manifold_tp m2( new identity_manifold_t( manifold_t::BASE_MANIFOLD ) );
  
  // initial coordinate
  std::vector<float> cdata1;
  cdata1.push_back( 1.0 );
  cdata1.push_back( 2.0 );
  coordinate_t c1 = raw_coordinate_t( cdata1 ).on( m1 );
  
  // second coordinate
  std::vector<float> cdata2;
  cdata2.push_back( 1.0 );
  cdata2.push_back( 2.0 );
  coordinate_t c2 = raw_coordinate_t( cdata2 ).on( m2 );

  // Get the first coordinate on the second coordinate
  coordinate_t c3 = c1.on( m2 );
  test_assert( c3 == c2 );

  // test dimensionality
  test_assert( dimensionality( c1 ) == 2 );
  

  // print stuff out
  std::cout << "M1: " << m1 << std::endl;
  std::cout << "M2: " << m2 << std::endl;
  std::cout << "c1: " << c1 << std::endl;
  std::cout << "c2: " << c2 << std::endl;
  std::cout << "c3: " << c3 << std::endl;
  
  // Print more complicated stuff
  std::cout << "Bundle Set M1(c1): " << m1->basis_bundle_set( c1 ) << std::endl;
  std::cout << "Bundle Set M2(c2): " << m2->basis_bundle_set( c2 ) << std::endl;
  std::cout << "Bundle Set M2(c3): " << m2->basis_bundle_set( c3 ) << std::endl;

  // Trigger exception
  test_exception( m1->basis_bundle_set( c2 ), coordinates::exceptions::reference_manifolds_differ_error );

  end_of_test
}

//===================================================================

int main( int argc, char** argv ) 
{

  run_test(test_compile());
  run_test(test_base_manifold());

  return 0;
}
