
#include <iostream>
#include <iomanip>
#include <math.h>

using namespace std;

void test_atan2( double x, double y, double eps )
{
  double xe = ( fabs(x) < eps ? 0 : x );
  double ye = ( fabs(y) < eps ? 0 : y );
  float xef = ( fabs((float)x) < eps ? 0 : (float)x );
  float yef = ( fabs((float)y) < eps ? 0 : (float)y );
  //std::cout << "x: " << setw(15) << x << " y: " << setw(15) << y << " e: " << eps << endl;
  //std::cout << "   " << "atan2(d,d)     = " << atan2(x,y) << endl;
  //std::cout << "   " << "atan2(f,f)     = " << atan2((float)x,(float)y) << endl;
  //std::cout << "   " << "atan2(<e,<e)   = " << atan2(xe,ye) << endl;
  //std::cout << "   " << "atan2(<ef,<ef) = " << atan2(xef,yef) << endl;

  cout << x << " " << y << " " << eps << " ";
  cout << (float)x << " " << (float)y << " ";
  cout << xe << " " << ye << " ";
  cout << xef << " " << yef << " ";
  cout << atan2(x,y) << " ";
  cout << atan2( (float)x, (float)y ) << " ";
  cout << atan2( xe, ye ) << " ";
  cout << atan2( xef, yef );
  cout << endl;
}

int main( int argc, char** argv )
{

  // setup stream
  cout.precision( 20 );
  cout.setf( ios::scientific, ios::floatfield );

  for( double k = 1e-40; k < 1e-2; k *= 1e2 ) {
    for( double ep = 1e-40; ep < 1e-6; ep *= 1e2 ) {
      test_atan2(  k,  k, ep );
      test_atan2( -k,  k, ep );
      test_atan2( -k, -k, ep );
      test_atan2(  k, -k, ep );
    }
  }

  return 0;
}
