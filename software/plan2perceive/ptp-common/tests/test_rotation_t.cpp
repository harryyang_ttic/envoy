
#include <ptp-common-coordinates/rotation_t.hpp>
#include <ptp-common-coordinates/identity_manifold_t.hpp>
#include <ptp-common-test/test.hpp>

using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;

//========================================================================


//========================================================================

int test_compile()
{
  start_of_test

  // create a mnifold
  manifold_tp m1( new identity_manifold_t( manifold_t::BASE_MANIFOLD ) );

  // create a simplex
  std::vector<coordinate_t> s_points;
  std::vector<float> a_points;
  a_points.push_back( 0 );
  a_points.push_back( 0 );
  a_points.push_back( 0 );
  std::vector<float> b_points;
  b_points.push_back( 1 );
  b_points.push_back( 0 );
  b_points.push_back( 0 );
  s_points.push_back( coordinate_t( raw_coordinate_t( a_points ), m1 ) );
  s_points.push_back( coordinate_t( raw_coordinate_t( b_points ), m1 ) );
  simplex_tp s1( new simplex_t( s_points, m1 ) );

  // Ok, now create a rotation
  rotation_t rot1( s1, deg( 45 ).from( angle_t::X_AXIS ) );

  end_of_test
}

//========================================================================

int test_2d_rotations()
{
  start_of_test

  // create a manifold
  manifold_tp m1( new identity_manifold_t( manifold_t::BASE_MANIFOLD ) );

  // create a simplex
  std::vector<coordinate_t> s_points;
  s_points.push_back( coordinate_t( coordinate( 0,0 ), m1 ) );
  simplex_tp s1( new simplex_t( s_points, m1 ) );
  s_points.clear();
  s_points.push_back( coordinate_t( coordinate( 0.5, 0 ), m1 ) );
  simplex_tp s2( new simplex_t( s_points, m1 ) );

  // Ok, now create a rotations
  rotation_t rot1( s1, deg(  90 ).from( angle_t::X_AXIS ) );
  rotation_t rot2( s1, deg( 180 ).from( angle_t::X_AXIS ) );
  rotation_t rot3( s1, deg( 270 ).from( angle_t::X_AXIS ) );
  rotation_t rot4( s1, deg( 360 ).from( angle_t::X_AXIS ) );
  rotation_t rot5( s1, deg( -45 ).from( angle_t::X_AXIS ) );
  rotation_t rot6( s2, deg(  90 ).from( angle_t::X_AXIS ) );
  
  // create a coordinate and it's wanted rotated verions
  // and test whether the rotation works
  coordinate_t c1 = coordinate_t( coordinate( 1, 0 ), m1 );
  coordinate_t c1_true = coordinate_t( coordinate( 0, 1 ), m1 );
  coordinate_t c1_rot = rot1.rotate_coordinate( c1 );
  //std::cout << "C1        = " << c1 << std::endl;
  //std::cout << "C1 (rot)  = " << c1_rot << std::endl;
  //std::cout << "C1 (true) = " << c1_true << std::endl;
  test_assert( float_equal(c1_true,c1_rot) );

  coordinate_t c2 = coordinate_t( coordinate( 1, 0 ), m1 );
  coordinate_t c2_true = coordinate_t( coordinate( -1, 0 ), m1 );
  coordinate_t c2_rot = rot2.rotate_coordinate( c2 );
  test_assert( float_equal(c2_true,c2_rot) );

  coordinate_t c3 = coordinate_t( coordinate( 1, 0 ), m1 );
  coordinate_t c3_true = coordinate_t( coordinate( 0, -1 ), m1 );
  coordinate_t c3_rot = rot3.rotate_coordinate( c3 );
  test_assert( float_equal(c3_true,c3_rot) );

  coordinate_t c4 = coordinate_t( coordinate( 1, 0 ), m1 );
  coordinate_t c4_true = coordinate_t( coordinate( 1, 0 ), m1 );
  coordinate_t c4_rot = rot4.rotate_coordinate( c4 );
  test_assert( float_equal(c4_true,c4_rot) );

  coordinate_t c5 = coordinate_t( coordinate( 1, 0 ), m1 );
  coordinate_t c5_true = coordinate_t( coordinate( cos(-45*M_PI/180), sin(-45*M_PI/180) ), m1 );
  coordinate_t c5_rot = rot5.rotate_coordinate( c5 );
  test_assert( float_equal(c5_true,c5_rot) );


  coordinate_t c6 = coordinate_t( coordinate( 1, 0 ), m1 );
  coordinate_t c6_true = coordinate_t( coordinate( 0, 0.5 ), m1 );
  coordinate_t c6_rot = rot6.rotate_coordinate( c6 );
  test_assert( float_equal(c6_true,c6_rot) );


  end_of_test
}

//========================================================================

int test_3d_z_rotations()
{
  start_of_test

  // create a manifold
  manifold_tp m1( new identity_manifold_t( manifold_t::BASE_MANIFOLD ) );

  // create a simplex
  std::vector<coordinate_t> s_points;
  s_points.push_back( coordinate_t( coordinate( 0,0,0 ), m1 ) );
  s_points.push_back( coordinate_t( coordinate( 0,0,1 ), m1 ) );
  simplex_tp s1( new simplex_t( s_points, m1 ) );
  s_points.clear();
  s_points.push_back( coordinate_t( coordinate( 0.5,0,0 ), m1 ) );
  s_points.push_back( coordinate_t( coordinate( 0.5,0,1 ), m1 ) );
  simplex_tp s2( new simplex_t( s_points, m1 ) );

  // Ok, now create a rotations
  rotation_t rot1( s1, deg(  90 ).from( angle_t::X_AXIS ) );
  rotation_t rot2( s1, deg( 180 ).from( angle_t::X_AXIS ) );
  rotation_t rot3( s1, deg( 270 ).from( angle_t::X_AXIS ) );
  rotation_t rot4( s1, deg( 360 ).from( angle_t::X_AXIS ) );
  rotation_t rot5( s1, deg( -45 ).from( angle_t::X_AXIS ) );
  rotation_t rot6( s2, deg(  90 ).from( angle_t::X_AXIS ) );
  
  // create a coordinate and it's wanted rotated verions
  // and test whether the rotation works
  coordinate_t c1 = coordinate_t( coordinate( 1, 0, 0 ), m1 );
  coordinate_t c1_true = coordinate_t( coordinate( 0, 1, 0 ), m1 );
  coordinate_t c1_rot = rot1.rotate_coordinate( c1 );
  test_assert( float_equal(c1_true,c1_rot) );

  coordinate_t c2 = coordinate_t( coordinate( 1, 0, 0 ), m1 );
  coordinate_t c2_true = coordinate_t( coordinate( -1, 0, 0 ), m1 );
  coordinate_t c2_rot = rot2.rotate_coordinate( c2 );
  test_assert( float_equal(c2_true,c2_rot) );

  coordinate_t c3 = coordinate_t( coordinate( 1, 0, 0 ), m1 );
  coordinate_t c3_true = coordinate_t( coordinate( 0, -1, 0 ), m1 );
  coordinate_t c3_rot = rot3.rotate_coordinate( c3 );
  test_assert( float_equal(c3_true,c3_rot) );

  coordinate_t c4 = coordinate_t( coordinate( 1, 0, 0 ), m1 );
  coordinate_t c4_true = coordinate_t( coordinate( 1, 0, 0 ), m1 );
  coordinate_t c4_rot = rot4.rotate_coordinate( c4 );
  test_assert( float_equal(c4_true,c4_rot) );

  coordinate_t c5 = coordinate_t( coordinate( 1, 0, 0 ), m1 );
  coordinate_t c5_true = coordinate_t( coordinate( cos(-45*M_PI/180), sin(-45*M_PI/180), 0 ), m1 );
  coordinate_t c5_rot = rot5.rotate_coordinate( c5 );
  test_assert( float_equal(c5_true,c5_rot) );


  coordinate_t c6 = coordinate_t( coordinate( 1, 0, 0 ), m1 );
  coordinate_t c6_true = coordinate_t( coordinate( 0, 0.5, 0 ), m1 );
  coordinate_t c6_rot = rot6.rotate_coordinate( c6 );
  test_assert( float_equal(c6_true,c6_rot) );

  end_of_test
}

//========================================================================

int test_3d_x_rotations()
{
  start_of_test

  // create a manifold
  manifold_tp m1( new identity_manifold_t( manifold_t::BASE_MANIFOLD ) );

  // create a simplex
  std::vector<coordinate_t> s_points;
  s_points.push_back( coordinate_t( coordinate( 0,0,0 ), m1 ) );
  s_points.push_back( coordinate_t( coordinate( 1,0,0 ), m1 ) );
  simplex_tp s1( new simplex_t( s_points, m1 ) );
  s_points.clear();
  s_points.push_back( coordinate_t( coordinate( 0,0,0.5 ), m1 ) );
  s_points.push_back( coordinate_t( coordinate( 1,0,0.5 ), m1 ) );
  simplex_tp s2( new simplex_t( s_points, m1 ) );

  // Ok, now create a rotations
  rotation_t rot1( s1, deg(  90 ).from( angle_t::X_AXIS ) );
  rotation_t rot2( s1, deg( 180 ).from( angle_t::X_AXIS ) );
  rotation_t rot3( s1, deg( 270 ).from( angle_t::X_AXIS ) );
  rotation_t rot4( s1, deg( 360 ).from( angle_t::X_AXIS ) );
  rotation_t rot5( s1, deg( -45 ).from( angle_t::X_AXIS ) );
  rotation_t rot6( s2, deg(  90 ).from( angle_t::X_AXIS ) );
  
  // create a coordinate and it's wanted rotated verions
  // and test whether the rotation works
  coordinate_t c1 = coordinate_t( coordinate( 0, 1, 0 ), m1 );
  coordinate_t c1_true = coordinate_t( coordinate( 0, 0, 1 ), m1 );
  coordinate_t c1_rot = rot1.rotate_coordinate( c1 );
  test_assert( float_equal(c1_true,c1_rot) );

  coordinate_t c2 = coordinate_t( coordinate( 0, 1, 0 ), m1 );
  coordinate_t c2_true = coordinate_t( coordinate( 0, -1, 0 ), m1 );
  coordinate_t c2_rot = rot2.rotate_coordinate( c2 );
  test_assert( float_equal(c2_true,c2_rot) );

  coordinate_t c3 = coordinate_t( coordinate( 0, 1, 0 ), m1 );
  coordinate_t c3_true = coordinate_t( coordinate( 0, 0, -1 ), m1 );
  coordinate_t c3_rot = rot3.rotate_coordinate( c3 );
  test_assert( float_equal(c3_true,c3_rot) );

  coordinate_t c4 = coordinate_t( coordinate( 0, 1, 0 ), m1 );
  coordinate_t c4_true = coordinate_t( coordinate( 0, 1, 0 ), m1 );
  coordinate_t c4_rot = rot4.rotate_coordinate( c4 );
  test_assert( float_equal(c4_true,c4_rot) );

  coordinate_t c5 = coordinate_t( coordinate( 0, 1, 0 ), m1 );
  coordinate_t c5_true = coordinate_t( coordinate( 0, cos(-45*M_PI/180), sin(-45*M_PI/180) ), m1 );
  coordinate_t c5_rot = rot5.rotate_coordinate( c5 );
  test_assert( float_equal(c5_true,c5_rot) );


  coordinate_t c6 = coordinate_t( coordinate( 0, 0, 0 ), m1 );
  coordinate_t c6_true = coordinate_t( coordinate( 0, 0.5, 0 ), m1 );
  coordinate_t c6_rot = rot6.rotate_coordinate( c6 );
  test_assert( float_equal(c6_true,c6_rot) );

  end_of_test
}


//========================================================================

int main( int argc, char** argv )
{

  run_test( test_compile() );
  run_test( test_2d_rotations() );
  run_test( test_3d_z_rotations() );
  run_test( test_3d_x_rotations() );
  return 0;
}
