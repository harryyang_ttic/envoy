
#include <ptp-common/reals.hpp>
#include <ptp-common-test/test.hpp>
#include <iostream>

using namespace ptp;
using namespace ptp::common;

//=====================================================================

int test_compile()
{
  start_of_test;

  realf_t r1( 0 );
  realf_t r2;
  realf_t r3( 1e-10f );
  reald_t r4( 1e-10f );
  realf_t r5( 1e-10 );
  reald_t r6( 1e-10 );
  realf_t r7( 1 );
  reald_t r8( 1 );
  
  
  
  // test operators
  r1 + r2;
  r1 - r2;
  r1 * r2;
  r1 / r2;

  //r1 + r4;
  //r1 - r4;
  //r1 * r4;
  //r1 / r4;

  //r4 + r1;
  //r4 - r1;
  //r4 * r1;
  //r4 / r1;

  r1 + 0.1f;
  r1 - 0.1f;
  r1 * 0.1f;
  r1 / 0.1f;

  r1 + 0.1;
  r1 - 0.1;
  r1 * 0.1;
  r1 / 0.1;
  r1 = 1;

  r1 == r2;
  r1 < r2;
  r1 == 1;
  r1 == 1.0f;
  r1 == 1.0;
  r1 < 1;
  r1 < 1.0f;
  r1 < 1.0;

  std::cout << r1 << std::endl;

  end_of_test;
}


//=====================================================================

int test_capping()
{
  start_of_test;

  realf_t r1( 1 );
  realf_t r2( 1e-6 );
  realf_t r3( 1e-10 );
  reald_t r4( 1e-10 );
  
  test_assert( r1 == 1 );
  test_assert( r2 == 1e-6 );
  test_assert( r3 == 0 );
  test_assert( r4 == 1e-10 );

  end_of_test;
}

//=====================================================================

int main( int argc, char** argv )
{

  run_test( test_compile() );
  run_test( test_capping() );

  return 0;
}
