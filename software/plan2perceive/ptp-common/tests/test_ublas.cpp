
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <iostream>

using namespace boost::numeric::ublas;


int main( int argc, char** argv )
{
  
  // Create a 3x3 matrix
  matrix<float> m( 3, 3 );
  
  // Assign stuff to it
  for( int i = 0; i < m.size1(); ++i ) {
    for( int j = 0; j < m.size2(); ++j ) {
      m( i,j ) = i * j + (j - i);
    }
  }
  
}
