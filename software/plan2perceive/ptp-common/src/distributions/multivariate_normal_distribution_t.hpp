
#if !defined( __PTP_DISTRIBUTION_multivariate_normal_distribution_t_HPP__ )
#define __PTP_DISTRIBUTION_multivariate_normal_distribution_t_HPP__

#include <ptp-common/distribution.hpp>
#include <ptp-common/matrix.hpp>

namespace ptp {
  namespace distributions {

    using namespace ptp::common;
    
    //================================================================

    // Description:
    // A multivariate gaussian (normal) distribution.
    // These are defined by a mean and covariance matrix.
    //
    // The Elements must be math_vector_t or of type Matrix<> with
    // a single filled dimension
    template< class X >
    class multivariate_normal_distribution_t
      : public distribution_t< X >
    {
    public: // types
      
      typedef X domain_type_t;
      
    public:

      //------------------------------------------------
      
      // Description:
      // Creates a new normal with given mean and covariance matrix
      multivariate_normal_distribution_t( const X& mean,
					  const matrix_t& cov )
      {
	init( mean, cov );
      }

      //------------------------------------------------
      
      // Description:
      // Creates a new normal with given mean and the same
      // variance for each dimension ( so covariance ends up being
      // I * sigma^2 )
      multivariate_normal_distribution_t( const X& mean,
					  const float& sigma )
      {
	matrix_t cov = matrix_t::Identity( dimensionality(mean), dimensionality(mean) );
	init( mean, cov * (sigma*sigma) );
      }

      //------------------------------------------------

      // Description:
      // Returns the probability of a given sample
      probability_t operator() ( const X& x ) const
      {
	math_vector_t diff = ( x - _mean );
	//double ex = -0.5 * diff.dot(  _cov_ldlt.solve(diff) );
	double ex = -0.5 * ( diff.dot ( _cov_inv * diff ) );
	return _constant * exp(ex);
      }

      //------------------------------------------------
      
      // Description:
      // Samples from this gaussina
      X sample() const
      {
	// first sample a set of unit guassians
	X unit_normals = sample_unit_normals( dimensionality(_mean) );
	
	// now compute the affine transform using covariance cholesky decomp
	X unit_trans = _cov_ldlt.matrixL() * unit_normals;
	
	// add the mean vector and return sample
	X sample_vec = unit_trans + _mean;
	
	return sample_vec;
      }

      //------------------------------------------------

    protected:

      //------------------------------------------------
      
      // Description:
      // The mean
      X _mean;
      
      // Description:
      // The covariance
      matrix_t _cov;

      // Description:
      // The inverse covariance
      matrix_t _cov_inv;

      // Description:
      // The covariance cholesky decomposition
      LDLT<matrix_t> _cov_ldlt;
      
      // Description:
      // The normalizing constant from covariance
      double _constant;


      //------------------------------------------------
      
      // Description:
      // Initialize this gaussian with given mean, cov
      void init( const X& mean, const matrix_t& cov )
      {
	_mean = mean;
	_cov = cov;
	size_t n = dimensionality(mean);
	
	
	// compute the constant normalization for distribution
	double det = _cov.determinant();
	double p1 = pow( 2* M_PI, - (double)n / 2.0 );
	double p2 = pow( det, -0.5 );
	_constant = p1 * p2;
	
	
	// compute the cholesky decomposition of covaraiance
	_cov_ldlt = cov.ldlt();

	// compute inverse
	_cov_inv = cov.inverse();
      }

      //------------------------------------------------

      // Description:
      // Returns a vector with sampled unit normals for each
      // element
      X sample_unit_normals( const size_t& n ) const
      {

	// Just sample independet unit nromals
	X vec(n);
	for( size_t i = 0; i < n; ++i ) {
	  vec(i) = gsl_ran_ugaussian( static_distributions_gsl_rng() );
	}

	// return the unit normal samples
	return vec;
      }

      //------------------------------------------------
      

    };

    //================================================================
    //================================================================
    
  }
}


#endif

