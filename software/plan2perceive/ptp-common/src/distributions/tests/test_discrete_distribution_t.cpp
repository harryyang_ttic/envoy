
#include <ptp-common-distributions/discrete_distribution_t.hpp>
#include <ptp-common-test/test.hpp>
#include <iostream>

using namespace ptp;
using namespace ptp::common;
using namespace ptp::distributions;


//========================================================================

int test_compile()
{
  start_of_test;

  // Create a new discrete distribution with two integer elements
  discrete_distribution_t<int> d0;
  d0.set( 0, 2 );
  d0.set( 1, 2 );
  
  // Make sure probabilities are 0.5 for each
  test_assert( float_equal( d0.p( 0 ), raw_probability_t( 0.5 ) ) );
  test_assert( float_equal( d0.p( 1 ), raw_probability_t( 0.5 ) ) );
  
  // Mkae sure elements out of distribution have 0 probability
  test_assert( float_equal( d0.p( 100 ), raw_probability_t( 0 ) ) );
  
  // Sample a bunch of values, make sure they are similar
  // distributed like 0.5
  int count_0 = 0, count_1 = 0;
  for( size_t i = 0; i < 100; ++i ) {
    int s = d0.sample();
    test_assert( s == 0 || s == 1 );
    if( s == 0 )
      ++count_0;
    else
      ++count_1;
  }
  double sample_p_0 = (double)count_0 / ( count_0 + count_1 );
  test_assert( float_equal( sample_p_0, 0.5, 0.1 ) );

  end_of_test;
}

//========================================================================

int main( int argc, char** argv )
{
  run_test( test_compile() );
  
  return 0;
}
