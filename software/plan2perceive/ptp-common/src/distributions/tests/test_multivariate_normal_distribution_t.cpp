
#include <ptp-common/matrix.hpp>
#include <ptp-common-distributions/multivariate_normal_distribution_t.hpp>
#include <ptp-common-test/test.hpp>


using namespace ptp;
using namespace ptp::common;
using namespace ptp::distributions;

int test_compile()
{
  start_of_test;

  math_vector_t mean( 2 );
  mean( 0 ) = 1;
  mean( 1 ) = 10;
  matrix_t cov( 2, 2 );
  cov( 0,0 ) = cov( 1,1 ) = 1;
  cov( 0,1 ) = cov( 1,0 ) = 0.2;
  multivariate_normal_distribution_t<math_vector_t> d0( mean, cov );
  
  // take the probability of a vector
  math_vector_t x( 2 );
  x(0) = 1.4;
  x(1) = 1.4;
  probability_t p0 = d0.p( x );
  std::cout << "X: " << x.transpose() << "   p(x)=" << p0 << std::endl;
  
  // Take some samples
  for( size_t i = 0; i < 100; ++i ) {
    math_vector_t s = d0.sample();
    std::cout << "Sample: " << s.transpose() << "  p(s)= " << d0.p(s) << std::endl;
  }
 
  end_of_test;
}


int main( int argc, char** argv )
{
  run_test(test_compile());
  
  return 0;
}
