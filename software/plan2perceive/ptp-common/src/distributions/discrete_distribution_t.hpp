
#if !defined( __PTP_DISTRIBUTIONS_discreete_distribution_HPP__ )
#define __PTP_DISTRIBUTIONS_discreete_distribution_HPP__

#include <ptp-common/distribution.hpp>
#include <boost/unordered_map.hpp>
#include <vector>

namespace ptp {
  namespace distributions {

    using namespace ptp::common;
    
    //================================================================

    // Description:
    // A general discrete distribution, where you initialize it with
    // elements or can add element->weight mappings as you go
    //
    // Note: this IS NOT THREAD SAFE!
    template< class X >
    class discrete_distribution_t
      : public distribution_t< X >
    {
    public: // types
      
      typedef X domain_type_t;
      
    public:

      //---------------------------------------------------------

      // Description:
      // Create a new distribution no elements
      discrete_distribution_t()
	: _is_normalized( true ),
	  _data(),
	  _index_map(),
	  _gsl_discrete( NULL )
      {}

      //---------------------------------------------------------

      // Description:
      // Copy distribution
      discrete_distribution_t( const discrete_distribution_t<X>& d )
	: _is_normalized( false ),
	  _data( d._data ),
	  _index_map( d._index_map ),
	  _gsl_discrete( NULL )
      {}

      //---------------------------------------------------------
      
      // Description:
      // Free any resources ( especially the GSL ones )
      ~discrete_distribution_t()
      {
	if( _gsl_discrete ) {
	  gsl_ran_discrete_free( _gsl_discrete );
	}
      }

      //---------------------------------------------------------
      
      // Description:
      // Sets the weight for a particular element.
      // If it is a new element it gets added to the distribution
      void set( const X& x, const double& weight )
      {
	_data[ x ] = weight;
	_is_normalized = false;
      }

      //---------------------------------------------------------

      // Description:
      // Returns the probability oif a given lement.
      // Elements not in distribution are assumed to have zero prob.
      probability_t operator() ( const X& x ) const
      {
	normalize_and_update_internal();
	typename boost::unordered_map<domain_type_t,double>::const_iterator i
	  = _data.find( x );
	if( i == _data.end() )
	  return 0;
	return i->second;
      }

      //---------------------------------------------------------

      // Description:
      // Returns a sample froim this distribution
      X sample() const
      {
	normalize_and_update_internal();
	size_t idx = gsl_ran_discrete( static_distributions_gsl_rng(),
				       _gsl_discrete );
	return _index_map[ idx ];
      }

      //---------------------------------------------------------

    protected:

      //---------------------------------------------------------

      // Description:
      // Normlaize the internal representation to have a prob which
      // sums to 1 for all elements, and create internal structures
      void normalize_and_update_internal() const
      {
	// flag denoting whether we need to re-create GSL dist
	bool recreate_gsl_dist = (_gsl_discrete == NULL);

	// First, if everything is already normalized, skip
	if( !_is_normalized ) {

	  // we're changing weights, so recreate the gsl structures
	  recreate_gsl_dist = true;
	  
	  // Get the sum of all the weights
	  double sum = 0;
	  typename boost::unordered_map<domain_type_t,double>::const_iterator citer;
	  for( citer = _data.begin(); citer != _data.end(); ++citer ) {
	    sum += citer->second;
	  }

	  // Update the weights
	  typename boost::unordered_map<domain_type_t,double>::iterator iter;
	  for( iter = _data.begin(); iter != _data.end(); ++iter ) {
	    iter->second /= sum;
	  }
	  
	}
	
	// recreate the gsl structure if we need to
	if( recreate_gsl_dist )
	  create_gsl_distribution();

      }

      //---------------------------------------------------------
      
      void create_gsl_distribution() const
      {
	// First, we enumerate our data lement and store their mapping
	// into indexes as a vecotr and store the weights as array for GSL
	double *weights = new double[ _data.size() ];
	_index_map.clear();
	_index_map.reserve( _data.size() );
	size_t idx = 0;
	typename boost::unordered_map<domain_type_t,double>::const_iterator iter;
	for( iter = _data.begin(); iter != _data.end(); ++iter, ++idx ) {
	  _index_map.push_back( iter->first );
	  weights[idx] = iter->second;
	}

	// Now create a gsl_discrete_t ( release previous if any )
	if( _gsl_discrete ) {
	  gsl_ran_discrete_free( _gsl_discrete );
	}
	_gsl_discrete = gsl_ran_discrete_preproc( _data.size(), weights );
	
	// delete and free the weights array
	delete[] weights;
      }
      
      //---------------------------------------------------------
      
      // Description:
      // Flag denoting whetehr the distribution is normalized or not
      // This is an internal state and because we will normalize lazyly,
      // we need to change this in a cost memebr, hence the mutable
      mutable bool _is_normalized;

      // Description:
      // The elements and weights mapping
      // This is mutable since we change it when normalizing ( lazily )
      // inside of p() and sample( ), but this is not a conceptual
      // change hence those methods are const.
      mutable boost::unordered_map< domain_type_t, double > _data;

      // Description:
      // Mapping from a GSL index into an element
      // This will be created lazily and so it is mutable
      mutable std::vector<domain_type_t> _index_map;
	
      // Description:
      // THe GSL distribution structure
      // This will be created lazily and hence it is mutable
      mutable gsl_ran_discrete_t* _gsl_discrete;

      
    };

    //================================================================

  }
}

#endif

