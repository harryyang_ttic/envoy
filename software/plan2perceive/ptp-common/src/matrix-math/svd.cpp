
#include "svd.hpp"


namespace ptp {
  namespace matrixmath {


    //==================================================================

    bool svd( const matrix_t& a, matrix_t& u, matrix_t& v, math_vector_t& s )
    {
      // create a jacobi svd and ask for full u and V computation
      JacobiSVD<matrix_t> svd( a, ComputeFullU | ComputeFullV );
      
      // assign the resutls
      u = svd.matrixU();
      v = svd.matrixV();
      s = svd.singularValues();
      
      return true;
    }

    //==================================================================


  }
}

