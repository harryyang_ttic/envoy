
#if !defined( __PTP_MATRIXMATH_svd_HPP__ )
#define __PTP_MATRIXMATH_svd_HPP__

#include <ptp-common/matrix.hpp>

namespace ptp {
  namespace matrixmath {

    using namespace common;

    //=============================================================

    // Description:
    // Computes the Singular Value Decomposition of the given
    // matrix
    bool svd( const matrix_t& a,
	      matrix_t& u,
	      matrix_t& v, 
	      math_vector_t& s );

    //=============================================================

  }
}

#endif

