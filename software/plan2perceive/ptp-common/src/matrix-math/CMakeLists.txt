

## The ptp-common-matrix-math library
add_library( ptp-common-matrix-math SHARED
  svd.hpp
  svd.cpp
  )

pods_use_pkg_config_packages( ptp-common-matrix-math ptp-common-interfaces gsl boost ptp-common )

pods_install_headers(
  svd.hpp
  DESTINATION ptp-common-matrix-math
  )

pods_install_libraries( ptp-common-matrix-math )
pods_install_pkg_config_file( ptp-common-matrix-math LIBS -lptp-common-matrix-math REQUIRES boost gsl ptp-common ptp-common-interfaces )

