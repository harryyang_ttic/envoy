
#if !defined( __PTP_COORDINATES_rotation_manifold_t_HPP__ )
#define __PTP_COORDINATES_rotation_manifold_t_HPP__

#include "manifold.hpp"
#include "rotation_t.hpp"

namespace ptp {
  namespace coordinates {

    //===================================================================

    // Description:
    // A rotation_manifold is simple a manifold which is a rotation,
    // about the a given simplex, of the refence manifold
    // A such, a rotation_manifold is flat in on the reference manifold
    // and has the same dimensionality as the reference manifold
    class rotation_manifold_t
      : public manifold_t
    {
    public:

      // Description:
      // Create a new rotation manifold with reference ref and
      // a rotation on ref given.
      // The rotation specifies how to go from the reference manifold
      // to this manifold ( hence ref_to_this )
      rotation_manifold_t( const rotation_tp& rot_ref_to_this,
			   const manifold_tp& ref,
			   const std::string& name );
      rotation_manifold_t( const rotation_tp& rot_ref_to_this,
			   const manifold_tp& ref );


      // Description:
      // Returns the rotation for this manifold, The rotation is
      // on the reference manifold
      rotation_tp rotation_form_reference_to_this() const
      { return _rotation_ref_to_this; }
      
      // Description:
      // Returns the rotation from this manifold to the refenece,
      // The rotation is on the reference
      rotation_tp rotation_from_this_to_reference() const
      { return _rotation_this_to_ref; }

      // Description:
      // Returns the basis bundle for a particular basis.
      // Note that the given basis is in the reference of THIS manifold
      virtual basis_bundle_t basis_bundle( const basis_t& basis_on_this_manifold ) const;

      // Description:
      // Return the inverted basis bundle for a particular basis of
      // the REFERENCE manifold.
      // Note: the given basis in on the REFERENCE manifold
      virtual basis_bundle_t inverted_basis_bundle( const basis_t& basis_on_reference_manifold ) const;
      
      // Description:
      // The basis bundle vectors for the manifold.
      // Note: that these basis are for a particular point on the manifold
      //       and that they are defined in BOTH the coordinate frame of the
      //       reference manifold and this manifold,
      //       but the given point is in THIS manifold
      //virtual basis_bundle_set_t basis_bundle_set( const coordinate_t& point_in_this_manifold ) const;

      // Description:
      // The inverted basis bundle vectors for the manifold.
      // Note: that these basis are for a particular point on the manifold
      //       and that they are defined in BOTH the coordinate frame of the
      //       reference manifold and this manifold,
      //       but the given point is in THE REFERENCE manifold
      //virtual basis_bundle_set_t inverted_basis_bundle_set( const coordinate_t& point_in_reference_manifold ) const;


    protected:
      
      // Description:
      // The rotation, on the reference manifold, to go form
      // the reference to this, or vice versa
      rotation_tp _rotation_ref_to_this;
      rotation_tp _rotation_this_to_ref;

      // Description:
      // Builds and store the inverse rotation from the 
      // rotation_ref_to_this.
      void build_inverse_rotation();
      
    };

    // Description:
    // Smart pointer type
    typedef boost::shared_ptr< const rotation_manifold_t > rotation_manifold_tp;

    //===================================================================

    // Description:
    // Helper function to create a rotated manifold from a base
    rotation_manifold_tp rotate( const simplex_tp& axis,
				 const angle_t& angle,
				 const manifold_tp& base );
    rotation_manifold_tp rotate( const rotation_tp& r,
				 const manifold_tp& base );

    //===================================================================

  }
}

#endif

