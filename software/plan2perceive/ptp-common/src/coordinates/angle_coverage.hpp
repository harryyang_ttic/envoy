
#if !defined( __PTP_COORDINATES_angle_coverage_HPP__ )
#define __PTP_COORDINATES_angle_coverage_HPP__

#include <boost/operators.hpp>
#include <iosfwd>

namespace ptp {
  namespace coordinates  {


    //====================================================================

    // Descripiton:
    // A value between 0 and 1 representing the coverage of 
    // a particular area/space/room.  In particular,
    // this is the coverage in terms of "angle"
    // This is a superclass which has particular subclasses
    // for types of angle coverages and coverage proxys
    class angle_coverage_t
      : boost::ordered_field_operators< angle_coverage_t >
    {
    public:

      // Description:
      // the coverage value
      float _value;

      // Description:
      // Returns the coverage as a float value
      float as_float() const { return _value; }
      
      // Description:
      // Operatators for arithmetic
      angle_coverage_t operator+= ( const angle_coverage_t& a );
      angle_coverage_t operator-= ( const angle_coverage_t& a );
      angle_coverage_t operator*= ( const angle_coverage_t& a );
      angle_coverage_t operator/= ( const angle_coverage_t& a );

      // Description:
      // Operators for comparison
      bool operator<  ( const angle_coverage_t& a ) const;
      bool operator== ( const angle_coverage_t& a ) const;

      // Descripitn:
      // IO operators
      friend std::ostream& operator<< (std::ostream& os, const angle_coverage_t& a );
      
    protected:

      // Description:
      // Only subclasses and friend can create angle_coverage_t
      angle_coverage_t() { };
      
      // Description:
      // Creates a new coverage from a float
      angle_coverage_t( const float& a )
	: _value( a ) 
      {}

    private:
      
    };


    //====================================================================

    // Description:
    // An angle coverage proxy that is alway conservative
    class conservative_angle_coverage_t
      : public angle_coverage_t
    {
    public:
      
      // Description:
      // Create a new conservative coverage from a float
      conservative_angle_coverage_t( const float& a )
	: angle_coverage_t( a )
      {}

    };

    //====================================================================

    // Description:
    // An angle coverage proxy that is alway optimistic
    class optimistic_angle_coverage_t
      : public angle_coverage_t
    {
    public:
      
      // Description:
      // Create a new conservative coverage from a float
      optimistic_angle_coverage_t( const float& a )
	: angle_coverage_t( a )
      {}

    };

    
    //====================================================================


  }
}

#endif

