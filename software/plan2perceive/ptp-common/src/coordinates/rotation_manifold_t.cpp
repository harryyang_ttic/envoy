
#include "rotation_manifold_t.hpp"

using namespace ptp;
using namespace ptp::coordinates;

//==============================================================

rotation_manifold_t::rotation_manifold_t( const rotation_tp& rot_ref_to_this,
					  const manifold_tp& ref,
					  const std::string& name )
  : _rotation_ref_to_this( rot_ref_to_this )
{
  this->_reference = ref;
  this->_name = name;
  build_inverse_rotation();
}

//==============================================================

rotation_manifold_t::rotation_manifold_t( const rotation_tp& rot_ref_to_this,
					  const manifold_tp& ref )
  : _rotation_ref_to_this( rot_ref_to_this )
{
  this->_reference = ref;
  this->_name = "r." + ref->name();
  build_inverse_rotation();
}

//==============================================================

void rotation_manifold_t::build_inverse_rotation()
{
  _rotation_this_to_ref = 
    rotation_tp( new rotation_t( _rotation_ref_to_this->simplex(),
				 - _rotation_ref_to_this->angle_about_simplex() ) );
}

//==============================================================

basis_bundle_t rotation_manifold_t::basis_bundle( const basis_t& basis_on_this_manifold ) const
{
  // Check that the point is on this manifold
  if( basis_on_this_manifold.manifold() != shared_from_this() )
    BOOST_THROW_EXCEPTION( exceptions::coordinate_not_in_manifold_error() );
  
  // Ok, we know that the point is a rotation vesrion from the 
  // reference to this, so 'lift' the basis onto the refernece
  // and undo to rotation
  basis_t rotated_basis_on_ref = basis_on_this_manifold.lift( reference() );
  basis_t basis_on_ref( _rotation_ref_to_this->rotate_coordinate( rotated_basis_on_ref.origin() ),
			_rotation_ref_to_this->rotate_coordinate( rotated_basis_on_ref.direction() ) );
  
  // Return a bundle
  return basis_bundle_t( basis_on_this_manifold,
			 curve_t( basis_on_ref ) );
}

//==============================================================

basis_bundle_t rotation_manifold_t::inverted_basis_bundle( const basis_t& basis_on_reference_manifold ) const
{
  // Check that the point is on the reference
  if( basis_on_reference_manifold.manifold() != reference() )
    BOOST_THROW_EXCEPTION( exceptions::coordinate_not_in_manifold_error() );
  
  // Ok, we know that the basis on this is just a rotation of the 
  // basis on the reference ( given ), so rotate the basis
  // the 'lift' onto this manifold
  basis_t basis_on_this = 
    basis_t( _rotation_this_to_ref->rotate_coordinate( basis_on_reference_manifold.origin() ),
	     _rotation_this_to_ref->rotate_coordinate( basis_on_reference_manifold.direction() ) ).lift( shared_from_this() );
  
  // return a bundle
  return basis_bundle_t( basis_on_reference_manifold,
			 curve_t( basis_on_this ) );
}

//==============================================================

namespace ptp {
  namespace coordinates {

    rotation_manifold_tp rotate( const simplex_tp& axis,
				 const angle_t& angle,
				 const manifold_tp& base )
    {
      rotation_tp r( new rotation_t( axis, angle ) );
      return rotate( r, base );
    }

    rotation_manifold_tp rotate( const rotation_tp& r,
				 const manifold_tp& base )
    {
      return rotation_manifold_tp( new rotation_manifold_t( r, base ) );
    }
  }
}

//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================
