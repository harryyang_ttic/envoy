

#if !defined ( __PTP_COORDINATES_manifold_HPP__ )
#define __PTP_COORDINATES_manifold_HPP__


#include <vector>
#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/operators.hpp>
#include <string>

#include <ptp-common/common.hpp>
#include <ptp-common/dimensionality.hpp>

namespace ptp {
  namespace coordinates {

    using namespace ptp::common;

    //=====================================================================

    // Description:
    // Forward declarations
    class raw_coordinate_t;
    class coordinate_t;
    class manifold_t;
    class basis_t;
    //class basis_set_t;
    class basis_bundle_t;
    //class basis_bundle_set_t;
    class curve_t;

    typedef boost::shared_ptr<const manifold_t> manifold_tp;

    //=========================================================================

    // Description:
    // These are some exceptions that may happen when using manifolds
    namespace exceptions {
      
      
      // Description:
      // An implementation error has happened
      struct implementation_error : public virtual exception_base {};

      // Descripiton:
      // Invalid manifold was given
      struct invalid_manifold_error : public virtual exception_base {};

      // Description:
      // The coordinates have different manifolds
      struct reference_manifolds_differ_error : public virtual invalid_manifold_error {};

      // Description:
      // The manifold is NULL when it should not be
      struct null_manifold_error : public virtual invalid_manifold_error {};

      // Description:
      // Two manifolds do not overlap when they should.
      // This is usually teh case when a particular embeding cannot be feasable
      // while trying to generate inverted basis bundles
      struct manifolds_do_not_overlap_error : public virtual invalid_manifold_error {};

      // Description:
      // The coordinate is not in the correct manifold
      struct coordinate_not_in_manifold_error : public virtual exception_base {};

      // Description:
      // The curve offset is outside of hte domain
      struct domain_error : public virtual exception_base {};

      // Description:
      // Exception thrown when a particular coordiante has an invalid dim
      // for a function
      struct invalid_dimensionality_error : public virtual exception_base {};

      // Description:
      // The dimensionality of two element do not agree
      struct dimensionalities_do_not_agree_error : public virtual invalid_dimensionality_error {};

            
    }

    //=====================================================================

    // Description:
    // A basis bundle set is an ordered set of basis bundles
    // used to represent a full embedding of a manifold onto 
    // another at a particular point.
    typedef std::vector<basis_bundle_t> basis_bundle_set_t;
    

    // Description:
    // Returns the coordinate of the origin of the bais bundle set
    // in the reference of the grounding ( reference ) manifold
    coordinate_t get_grounding_origin( const basis_bundle_set_t& bundle_set );

    //=====================================================================

    // Description:
    // A manifold represents a consistent set of rules for a coordinate
    // system, which lives in some other higher-dimensional space
    // This is meant as a base class, do NOT explicitly create an instance
    // of this class ( that is why the static member is there, as this
    // class referes to the BASE menifold that holds all others. )
    class manifold_t
      : public boost::enable_shared_from_this< manifold_t >
    {
    public:

      // Description:
      // The base manifold which encompases all n-dimensional space
      static manifold_tp BASE_MANIFOLD;

    public:


      // Description:
      // Returns the manifold this manifold lives in
      manifold_tp reference() const;


      // Description:
      // Returns the name for this manifold.
      // It is not guaranteed to be non-empty ("") or to be unique.
      // But it may be useful in debugging
      virtual std::string name() const { return _name; }

      // Description:
      // Returns the basis bundle for a particular basis.
      // Note that the given basis is in the reference of THIS manifold
      virtual basis_bundle_t basis_bundle( const basis_t& basis_on_this_manifold ) const;

      // Description:
      // Return the inverted basis bundle for a particular basis of
      // the REFERENCE manifold.
      // Note: the given basis in on the REFERENCE manifold
      virtual basis_bundle_t inverted_basis_bundle( const basis_t& basis_on_reference_manifold ) const;
      
      // Description:
      // The basis bundle vectors for the manifold.
      // Note: that these basis are for a particular point on the manifold
      //       and that they are defined in BOTH the coordinate frame of the
      //       reference manifold and this manifold,
      //       but the given point is in THIS manifold
      virtual basis_bundle_set_t basis_bundle_set( const coordinate_t& point_in_this_manifold ) const;

      // Description:
      // The inverted basis bundle vectors for the manifold.
      // Note: that these basis are for a particular point on the manifold
      //       and that they are defined in BOTH the coordinate frame of the
      //       reference manifold and this manifold,
      //       but the given point is in THE REFERENCE manifold
      virtual basis_bundle_set_t inverted_basis_bundle_set( const coordinate_t& point_in_reference_manifold ) const;
      


      // Description:
      // Returns true iff the two manifolds are the same
      bool operator== (const manifold_t& a ) const;
      bool operator!= (const manifold_t& a ) const
      { return !( (*this) == a ); }

      
      // Description:
      // IO Operators
      friend std::ostream& operator<< ( std::ostream& os, const manifold_t& a );
      


    protected:

      // Description:
      // Returns the set of normal axis-aligned basis for a
      // manifold, the basis are centered around the given origin
      // and extend unit distance away.
      // Note: the manifold of the origin will be used as the manifold
      //       for the returned basis
      std::vector<basis_t> create_axis_aligned_basis( const coordinate_t& origin, const float& unit ) const;


      // Description:
      // The manifold this manifold lives in
      manifold_tp _reference;

      // Description:
      // A string identifying this manifold.
      // This may be set upoon construction by subclasses.
      std::string _name;

      // Description:
      // Creates a new manifold with given name
      manifold_t( const std::string& n )
	: _name( n )
      {}

      // Description:
      // Createa a new manifold with a default name 
      manifold_t()
      {}

    private:
    };

    
    // Descripiton:
    // IO output for shared pointer manifolds
    std::ostream& operator<< ( std::ostream& os,const manifold_tp& a );
    

    //=====================================================================

    // Description:
    // An ungrounded coordinate.
    // This is an abstract version which computes at run-time,
    // there are also specific compile-time coordinates with a 
    // known dimensionality
    class raw_coordinate_t
      : boost::additive< raw_coordinate_t >,
	boost::multiplicative< raw_coordinate_t, float >,
	boost::partially_ordered< raw_coordinate_t >
    {
    public:

      // Description:
      // Creates a new coordinate from a set of floats
      raw_coordinate_t( const std::vector<float>& data );

      // Description:
      // Ground a particular raw coordinate onto a manifold
      // This returns a new, grounded coordinate
      coordinate_t on( const manifold_tp& man ) const;


      // Description:
      // Gets the particular dimensionality point for this coordinate
      float operator[] ( const int& dim ) const;
      float& operator[] ( const int& dim );


      // Description:
      // The dimensionality of this coordinate
      int dimensionality() const;
      

      // Description:
      // Returns the L2 norm between the given two raw coordinates
      static double l2_norm( const raw_coordinate_t& a, const raw_coordinate_t& b );

      // Description:
      // Operators for arithmetic.
      // Assumes Eucledian metric space
      raw_coordinate_t operator+= (const raw_coordinate_t& a );
      raw_coordinate_t operator-= (const raw_coordinate_t& a );
      raw_coordinate_t operator*= (const float& a );
      raw_coordinate_t operator/= (const float& a );

      // Description:
      // Ordering operators, assumes lexographical ordering on dimensions
      bool operator== ( const raw_coordinate_t& a ) const;
      bool operator< ( const raw_coordinate_t& a ) const;


      // Description:
      // Friends
      friend class coordinate_t;

      // Description:
      // IO Operators
      friend std::ostream& operator<< ( std::ostream& os, const raw_coordinate_t& a );
      
      
    protected:

      // Description:
      // The actual coordinate points
      std::vector<float> _data;
      
    private:
    };

    // Description:
    // Helper functions to create raw coordinates
    raw_coordinate_t coordinate( const float& a );
    raw_coordinate_t coordinate( const float& a, const float& b );
    raw_coordinate_t coordinate( const float& a, const float& b, const float& c );


    //=====================================================================
    
    // Description:
    // A grounded coordinate on a manifold.
    // This is the class most often used
    class coordinate_t
      : boost::additive< coordinate_t >,
	boost::multiplicative< coordinate_t, float >,
	boost::partially_ordered< coordinate_t >,
        boost::equality_comparable<coordinate_t>			      
    {
    public:

      // Description:
      // The coordinate point itself
      raw_coordinate_t _coordinate;
      
      // Description:
      // The gounding manifold for the coordinate
      manifold_tp _manifold;

      // Description:
      // Construct a new coordinate from a raw coordinate and 
      // a manifold
      coordinate_t( const raw_coordinate_t& coord, const manifold_tp& man );

      // Description:
      // Construct an empty coordiante in a NULL manifold
      // This is not a valid coordinate
      coordinate_t();

      // Description:
      // Returns the dimensionality
      int dimensionality() const;

      // Descripton:
      // Returns the manifold the coordinate is in
      manifold_tp manifold() const
      { return _manifold; }

      // Description:
      // Returns the raw coordinate
      raw_coordinate_t raw_coordinate() const
      { return _coordinate; }
      
      // Description:
      // Returns the particular dimension point
      // In the coordinate frame of the grounding manifold
      float operator[] ( const int& dim ) const;
      float& operator[] ( const int& dim );

      // Description:
      // Returns this coordinate grounded in a different manifold.
      // If the coordinate cannot exists inside the given manifold,
      // an exception is thrown
      coordinate_t on( const manifold_tp& man ) const;
      

      // Description:
      // Returns this coordinate grounded in a different manifold
      // Notatice that this is a 'project' operation, so that is the
      // given manifold cannot emcompass this cordinate, it is projected 
      // to the 'nearest' point on the new manifold
      coordinate_t project_onto ( const manifold_tp& man ) const;


      // Description:
      // Lift a particular coordinate into a given manifold.
      // Lifting means that we IGNORE the current manifold and set
      // the given manifold as our manifold.
      // THIS IS AN EXTREMELY DANGEROUS OPERATION.
      coordinate_t lift( const manifold_tp& man ) const;
      

      // Description:
      // Operators for arithmetic
      coordinate_t operator+= (const coordinate_t& a );
      coordinate_t operator-= (const coordinate_t& a );
      coordinate_t operator*= (const float& a );
      coordinate_t operator/= (const float& a );

      // Description:
      // Operators for ordering. assumes lexicographical on dimensions
      bool operator== ( const coordinate_t& a ) const;
      bool operator<  ( const coordinate_t& a ) const;

      // Description:
      // IO Operators
      friend std::ostream& operator<< ( std::ostream& os, const coordinate_t& a );
      
      
    protected:


      // Description:
      // Rebase this coordinate to have the new given manifold
      // as it's grounding
      void rereference( const manifold_tp& man );

    private:
    };


    //=====================================================================


    // Description:
    // A basis is a particular direction on a manifold, along with
    // the length in that direction that one can travel on the manifold.
    // As such, a basis have an origina coordinate and a last coordinate
    // which encodeds both the direction and length of a straight segment
    // in that direction.
    class basis_t
    {
    public:

      // Description:
      // Create a new basis
      basis_t( const coordinate_t& origin, const coordinate_t& dir );

      // Description:
      // The origin
      coordinate_t _origin;

      // Description:
      // The direction/length coordinate
      coordinate_t _direction;

      // Description:
      // Returns the origin coordinate for this basis_t
      coordinate_t origin() const { return _origin; }

      // Description:
      // Returns the direction for this basis
      coordinate_t direction() const { return _direction; }

      // Description:
      // Returns the direction of the basis centered on
      // the oriigin. This essentially returns dir - origin vector
      // as a coordinate
      coordinate_t origin_centered_direction() const
      { return direction() - origin(); }

      // Description:
      // returns the arc-length of this basis line
      float arc_length() const;

      // Description:
      // Returns the manifold for a particular basis
      manifold_tp manifold() const;

      // Description:
      // Returns the dimensionality of the origin and direction
      int dimensionality() const
      { return _origin.dimensionality(); }

      // Description:
      // Translate both origin and riection by given corodinate
      basis_t translate( const coordinate_t& t ) const;

      // Description:
      // returns new basis with the origin of the basis be the given coordiante
      // and the direction length and direction be that of this basis
      basis_t center_on( const coordinate_t& orig ) const;
      
      // Description:
      // Return this basis grounded to the given manifold.
      // Note: if the given manifold cannot retain the basis,
      //       an exception is thrown
      curve_t on( const manifold_tp& man ) const;

      
      // Description:
      // Lift a particular basis into a given manifold.
      // Lifting means that we IGNORE the current manifold and set
      // the given manifold as our manifold.
      // THIS IS AN EXTREMELY DANGEROUS OPERATION.
      basis_t lift( const manifold_tp& man ) const;

      // Description:
      // IO Operators
      friend std::ostream& operator<< ( std::ostream& os, const basis_t& a );
      

    protected:

    private:
    };

    
    //=====================================================================
    
    // Description:
    // A curve is a function in a particular manifold.
    // The curve always goes from (0,1) range.
    // The curve is represented as an ordered set of linear
    // motions in a particular manifold.
    class curve_t
    {
    public:

      // Description:
      // The manifold the curve lines in
      manifold_tp _manifold;

      // Description:
      // The linear pieces of the curve
      std::vector<basis_t> _data;
      
      // Description:
      // The curves representing this curve
      std::vector<curve_t> _curves;
      
      // Description:
      // Bool saying whether we are using curves or not
      bool _compound_curve;

      // Description:
      // The total arclength of the curve
      float _total_arc_length;


      // Description:
      // Creates a curve from a set of linear basis motions
      // on the same reference manifold ( given, or assumed all same )
      //curve_t( const std::vector<basis_t>& pieces, const manifold_t& manifold );
      curve_t( const std::vector<basis_t>& pieces );
      curve_t( const std::vector<curve_t>& curves );
      curve_t( const basis_t& single_basis );


      
      // Description:
      // Returns the arc length of this full curve in the manifold
      // of reference.
      float arc_length() const;


      // Description:
      // Returns the manifold that this curve is on
      manifold_tp manifold() const 
      { return _manifold; }

      
      // Description:
      // Returns the single point along the curve in the frame of
      // the curve's manifold
      // The offset must be between 0 and 1
      coordinate_t at( const float& offset ) const;

      // Description:
      // Returns the points, in curve coordinates ( 0,1 ) where
      // the piecewise linear motions of the curve change
      std::vector<float> changepoints() const;
      
      // Description:
      // Returns a segment of the curve as a set of linear basis
      std::vector<basis_t> segment( const float& start, const float& end ) const;

      // Description:
      // Returns only the first segment of this curve
      basis_t first_segment() const;

      
      // Description:
      // Returns this curve on the given manifold.
      // If the given manifold cannot represent this curve, and exception is
      // thrown
      curve_t on( const manifold_tp& man ) const;

      // Description:
      // Change the reference manifold for a curve
      void rereference( const manifold_tp& man );

      
      // Description:
      // Lift a particular curve into a given manifold.
      // Lifting means that we IGNORE the current manifold and set
      // the given manifold as our manifold.
      // THIS IS AN EXTREMELY DANGEROUS OPERATION.
      curve_t lift( const manifold_tp& man ) const;

      // Description:
      // IO Operators
      friend std::ostream& operator<< ( std::ostream& os, const curve_t& a );
      

    protected:

    private:
    };

    //=====================================================================

    // Description:
    // A basis bundle is a pair of basis, one for a manifold A embeded
    // in manifold B, and another for a manifold B.  Both origins of
    // the basis are the same point, just in their respective manifold
    // coordinate frames.
    // Basis bundles are what allows us to transfer from one manifold
    // to another and in fact define the embedding itself at a single point
    // in a single direction.
    class basis_bundle_t
    {
    public:

      // Description:
      // Create a new basis bundle with given basis
      // and grounding curve
      basis_bundle_t( const basis_t& basis, const curve_t& curve );

      // Description:
      // The embeded manifold basis
      basis_t _embeded_basis;
      
      // Description:
      // The reference manifold basis, as a curve now because
      // it need not be linear.
      // The curve itself is the length of the embeded basis above.
      // Note that the curve domain is (0,1), where 1 is the 
      // length of the embeded basis and 0 is the origin.
      curve_t _reference_curve;

      
      // Description:
      // Lift a particular bundle into a given pair of manifolds.
      // Lifting means that we IGNORE the current manifold and set
      // the given manifold as our manifold.
      // THIS IS AN EXTREMELY DANGEROUS OPERATION.
      basis_bundle_t lift( const manifold_tp& basis_man, const manifold_tp& curve_man ) const;

      // Description:
      // IO Operators
      friend std::ostream& operator<< ( std::ostream& os, const basis_bundle_t& a );
      

    protected:
    private:
    };

    //=====================================================================

    // Description:
    // A basis set is just an ordered set of basis used to
    // represent an axis system
    typedef std::vector<basis_t> basis_set_t;

    //=====================================================================

    // Descripiton:
    // IO Operators
    std::ostream& operator<< ( std::ostream& os, const basis_set_t& a );
    std::ostream& operator<< ( std::ostream& os, const basis_bundle_set_t& a );

    //=====================================================================

  }
}



namespace ptp {
  namespace common {

    namespace cn = ptp::coordinates;

    //=====================================================================
    
    
    // Description:
    // Test whether two coordinates are equal in the sense that they are
    // on the same manifodl and their values are within epsilon away
    // ( as an l2_norm of difference )
    bool float_equal( const cn::coordinate_t& a, 
		      const cn::coordinate_t& b, 
		      const float& epsilon = 1e-6 );
    
    // Description:
    // Tests whetehr two basis are the same, with fudge factor
    bool float_equal( const cn::basis_t& a, 
		      const cn::basis_t& b, 
		      const float& eps = 1e-6 );
    
    //=====================================================================
    
    // Description:
    // Useful functions for debug
    void print( const cn::coordinate_t& a );
    void print( const cn::raw_coordinate_t& a );
    void print( const cn::manifold_t&a );
    void print( const cn::basis_t& a );
    void print( const cn::curve_t& a );
    void print( const cn::basis_set_t& a );
    void print( const cn::basis_bundle_t& a );
    void print( const cn::basis_bundle_set_t& a );
    
    // The dimensionality functions for coordiantes etc
    template<>
    size_t dimensionality( const cn::coordinate_t& a );
    template<>
    size_t dimensionality( const cn::raw_coordinate_t& a );
    template<>
    size_t dimensionality( const cn::basis_t& a );
        
  }
}

#endif

