
#if !defined( __PTP_COORDINATES_simplex_t_HPP__ )
#define __PTP_COORDINATES_simplex_t_HPP__


#include "manifold.hpp"

namespace ptp {
  namespace coordinates {


    //============================================================

    // Description:
    // forward declaration
    class simplex_t;
    typedef boost::shared_ptr<const simplex_t> simplex_tp;


    //============================================================

    // Description:
    // These are some exceptions that may happen when using simplexes
    namespace exceptions {
      
      // Description:
      // Not enough points in a set / simplex
      struct not_enough_points_error : public virtual exception_base {};
            
    }

    //============================================================

    // Description:
    // A simplex is a region on a manifold that contains less 
    // dimensions than the manifold.  In particular, the coordinate 
    // frames of simplexes are the same as the manifolds but 
    // projected down into the reduce dimensions.
    //
    // A simplex on an N-dimensional manifold or S dimensions
    // is defined by (S+1) N-dimensional coordinates.
    // E.G. a plane in 3D is defined by 3 3D points.
    class simplex_t
    {
    public:

      // Description:
      // Creates a new simplex on the given manifold with the given
      // points.  The points must exctly define a simplex ( so 
      // no extra points! )
      simplex_t( const std::vector<coordinate_t>& points, const manifold_tp& man );


      // Description:
      // Returns the manifold for this simplex
      manifold_tp manifold() const
      { return _manifold; }

      // Description:
      // Returns the points defininng a simplex
      std::vector<coordinate_t> points() const
      { return _points; }

      // Description:
      // Returns the diumensionality of the simplex
      int dimensionality() const
      { return _dimensionality; }


    protected:


      // Description:
      // The coordinates defining hte simplex
      std::vector<coordinate_t> _points;

      // Description:
      // The manifold this simplex is on
      manifold_tp _manifold;

      // Description:
      // The dimensionality of this simplex
      int _dimensionality;

      // Description:
      // Initialize the object with a set of poitns and a manifold.
      // This may throw an exception if the manifold and points are
      // not consistent.
      void init( const std::vector<coordinate_t>& points, const manifold_tp& man );

      // Description:
      // IO operators
      friend std::ostream& operator<< ( std::ostream& os, const simplex_t& s);

      // Description:
      // equality testing with fudge factor
      friend bool float_equal( const simplex_tp& a, const simplex_tp& b, const float& eps );
      
    private:
    };


    //============================================================

    // Description:
    // Functions to create some common simplexes without
    // using the std::vector constructor
    simplex_tp point_simplex( const raw_coordinate_t& a, const manifold_tp& man );
    simplex_tp line_simplex( const raw_coordinate_t& a, const raw_coordinate_t& b, const manifold_tp& man );
    simplex_tp plane_simplex( const raw_coordinate_t& a, const raw_coordinate_t& b, const raw_coordinate_t& c, const manifold_tp& man );

    //============================================================

    // Descripton:
    // Convert to/from basis_t and simplex_tp.
    // These are only to be use wwith line simplexes ( dim = 1 )
    // and will throw exceptions otherwise
    basis_t basis_from_simplex( const simplex_tp& a );
    simplex_tp simplex_from_basis( const basis_t& a );

    //============================================================

  }
}


namespace ptp {
  namespace common {

    //============================================================
    
    // Description:
    // Tests is two simplexes are equal, with epsilon fudge factor
    bool float_equal( const coordinates::simplex_tp& a, const coordinates::simplex_tp& b, const float& eps=1e-6 );

    //============================================================

  }
}

#endif

