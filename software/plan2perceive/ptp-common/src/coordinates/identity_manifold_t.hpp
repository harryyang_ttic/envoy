
#if !defined( __PTP_COORDINATES_identity_manifold_t_HPP__ )
#define __PTP_COORDINATES_identity_manifold_t_HPP__

#include "manifold.hpp"

namespace ptp {
  namespace coordinates {


    //==================================================================

    // Description:
    // A simply manifold which takes in the frames of it's reference
    // manifold.
    // This is really here just to test things out
    class identity_manifold_t
      : public manifold_t
    {
    public:
      
      // Description:
      // Creates a new identity manifold from given reference
      identity_manifold_t( const manifold_tp& ref )
      { 
	this->_reference = ref;
	this->_name = "i." + ref->name();
      }

      // Description:
      // Returns the basis bundle for a particular basis.
      // Note that the given basis is in the reference of THIS manifold
      virtual basis_bundle_t basis_bundle( const basis_t& basis_on_this_manifold ) const;

      // Description:
      // Return the inverted basis bundle for a particular basis of
      // the REFERENCE manifold.
      // Note: the given basis in on the REFERENCE manifold
      virtual basis_bundle_t inverted_basis_bundle( const basis_t& basis_on_reference_manifold ) const;
      
      // Description:
      // The basis bundle vectors for the manifold.
      // Note: that these basis are for a particular point on the manifold
      //       and that they are defined in BOTH the coordinate frame of the
      //       reference manifold and this manifold,
      //       but the given point is in THIS manifold
      virtual basis_bundle_set_t basis_bundle_set( const coordinate_t& point_in_this_manifold ) const;

      // Description:
      // The inverted basis bundle vectors for the manifold.
      // Note: that these basis are for a particular point on the manifold
      //       and that they are defined in BOTH the coordinate frame of the
      //       reference manifold and this manifold,
      //       but the given point is in THE REFERENCE manifold
      virtual basis_bundle_set_t inverted_basis_bundle_set( const coordinate_t& point_in_reference_manifold ) const;
      


      // Description:
      // Returns true iff the two manifolds are the same
      bool operator== (const identity_manifold_t& a ) const;
      bool operator!= (const identity_manifold_t& a ) const
      { return !( (*this) == a ); }

      // Description:
      // IO operators
      friend std::ostream& operator<< ( std::ostream& os, const identity_manifold_t& a );

    };

    //==================================================================

  }
}

#endif

