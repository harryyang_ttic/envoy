
#if !defined( __PTP_COORDINATES_translation_manifold_t_HPP__ )
#define __PTP_COORDINATES_translation_manifold_t_HPP__

#include "manifold.hpp"

namespace ptp {
  namespace coordinates {

    //=================================================================

    // Descritption:
    // A translation_manifold_t is a manifold which is a translation of
    // it's reference manifold. The manifold in falt in the reference 
    // refernece manifold and has the same number of dimensions and
    // scaling.
    class translation_manifold_t
      : public manifold_t
    {
    public:

      // Description:
      // Create a new translation manifold with given translation t
      // and refernece manifold
      translation_manifold_t( const coordinate_t& t,
			      const manifold_tp& ref,
			      const std::string& name );
      translation_manifold_t( const coordinate_t& t,
			      const manifold_tp& ref );

      // Description:
      // Returns the translation factor, in the reference manifold
      coordinate_t translation() const
      { return _translation; }
      
      // Description:
      // Returns the basis bundle for a particular basis.
      // Note that the given basis is in the reference of THIS manifold
      virtual basis_bundle_t basis_bundle( const basis_t& basis_on_this_manifold ) const;

      // Description:
      // Return the inverted basis bundle for a particular basis of
      // the REFERENCE manifold.
      // Note: the given basis in on the REFERENCE manifold
      virtual basis_bundle_t inverted_basis_bundle( const basis_t& basis_on_reference_manifold ) const;
      
      // Description:
      // The basis bundle vectors for the manifold.
      // Note: that these basis are for a particular point on the manifold
      //       and that they are defined in BOTH the coordinate frame of the
      //       reference manifold and this manifold,
      //       but the given point is in THIS manifold
      //virtual basis_bundle_set_t basis_bundle_set( const coordinate_t& point_in_this_manifold ) const;

      // Description:
      // The inverted basis bundle vectors for the manifold.
      // Note: that these basis are for a particular point on the manifold
      //       and that they are defined in BOTH the coordinate frame of the
      //       reference manifold and this manifold,
      //       but the given point is in THE REFERENCE manifold
      //virtual basis_bundle_set_t inverted_basis_bundle_set( const coordinate_t& point_in_reference_manifold ) const;

      
      // Description:
      // IO Operators
      friend std::ostream& operator<< ( std::ostream& os, const translation_manifold_t& t );
      
    protected:

      // Description:
      // The translation factor, in the reference manifold
      coordinate_t _translation;

    };

    // Description:
    // Pointer type
    typedef boost::shared_ptr<const translation_manifold_t> translation_manifold_tp;

    //=================================================================

    // Description:
    // Helper function to create a translation manifold from a
    // base manifold pointer
    translation_manifold_tp translate( const coordinate_t& t,
				       const manifold_tp& m );

    //=================================================================

  }
}

#endif

