
#include "plane_t.hpp"
#include "matrix_util.hpp"
#include "coordinate_math.hpp"

using namespace ptp;
using namespace coordinates;


//=========================================================================

plane_t::plane_t( const simplex_tp& normal,
		  const simplex_tp& x_axis,
		  const std::string& name )
  : _normal_simplex( normal ), 
    _plane_x_axis_on_reference( x_axis ),
    _thickness( DEFAULT_PLANE_THICKNESS )
{
  this->_reference = normal->manifold();
  this->_name = name;
  build_rotations_and_axis();
}

//=========================================================================

plane_t::plane_t( const simplex_tp& normal,
		  const simplex_tp& x_axis )
  : _normal_simplex( normal ),
    _plane_x_axis_on_reference( x_axis ),
    _thickness( DEFAULT_PLANE_THICKNESS )
{
  this->_reference = normal->manifold();
  this->_name = "p." + reference()->name();
  build_rotations_and_axis();
}

//=========================================================================

void plane_t::build_rotations_and_axis()
{

  // First, make sure that  the normal and axis simplexes live on 
  // the same manifold
  if( _normal_simplex->manifold() != _plane_x_axis_on_reference->manifold() ) {
    BOOST_THROW_EXCEPTION( exceptions::reference_manifolds_differ_error() );
  }

  // Ok, start with the non-rotated plane's X axis and move it to the
  // plane's origin
  basis_t x_axis_orig_basis = basis_from_simplex( _plane_x_axis_on_reference );
  _plane_x_axis_on_reference = 
    line_simplex( origin_in_reference().raw_coordinate(),
		  (x_axis_orig_basis.origin_centered_direction() + origin_in_reference()).raw_coordinate(),
		  reference() );
  
  // Now calculate the y axis as the cross product
  basis_t normal_basis = basis_from_simplex( _normal_simplex );
  basis_t x_axis_basis = basis_from_simplex( _plane_x_axis_on_reference );
  basis_t y_axis_basis = cross( normal_basis,
				x_axis_basis );

  // Now, normalize the new y axis to have the same 'unit' length as
  // the x axis
  y_axis_basis = normalize_length( y_axis_basis, x_axis_basis.arc_length() );
  
  _plane_y_axis_on_reference = simplex_from_basis( y_axis_basis );
}

//=========================================================================

curve_t plane_t::shortest_curve_to_plane( const coordinate_t& point_in_reference_manifold ) const
{
  
  // Ok, compute the intersection of the normal start at the given point
  // and the plane itself
  
  // First make sure that the point is in the reference
  if( point_in_reference_manifold.manifold() != reference() ) {
    BOOST_THROW_EXCEPTION( exceptions::reference_manifolds_differ_error() );
  }

  // Ok, now make vectors out of the plane origin, normal
  // as well as the given point
  math_vector_t plane_origin = math_vector_from_raw_coordinate( origin_in_reference().raw_coordinate() );
  math_vector_t plane_normal = math_vector_from_raw_coordinate( basis_from_simplex( _normal_simplex ).origin_centered_direction().raw_coordinate() );
  math_vector_t point = math_vector_from_raw_coordinate( point_in_reference_manifold.raw_coordinate() );
  
  // Ok, now the shortest vector to the plane is
  // n . (point - origin)   ( where . is the dot product )
  float distance = plane_normal.dot( point - plane_origin );
  math_vector_t shortest_vector = plane_normal * distance;
  
  // Ok, create a coordinate out of the vector
  coordinate_t dir = raw_coordinate_from_math_vector( shortest_vector ).on( reference() );
  
  // return the curve
  return curve_t( basis_t( point_in_reference_manifold, 
			   point_in_reference_manifold + dir ) );
}

//=========================================================================

basis_bundle_t plane_t::basis_bundle( const basis_t& basis_on_this_manifold ) const
{
  
  // Ok, given a basis ( or line ) in the plane, we need to get back to 
  // out reference manifold coordinates ( a curve ).
  
  // Since a plane is *not* curved, the result of our basis on the reference
  // manifold will in fact be a line itself.

  // Make sure that the basis has excatly 2 dimensional coordinates
  if( basis_on_this_manifold.dimensionality() != 2 ) {
    BOOST_THROW_EXCEPTION( exceptions::invalid_dimensionality_error() );
  }

  // get basis from axis simplexes
  basis_t x_axis_basis = basis_from_simplex( _plane_x_axis_on_reference );
  basis_t y_axis_basis = basis_from_simplex( _plane_y_axis_on_reference );

  // Go from the plane coordinate into the raference using the unit
  // axis vectors
  coordinate_t x_axis_centered_dir = x_axis_basis.origin_centered_direction();
  coordinate_t y_axis_centered_dir = y_axis_basis.origin_centered_direction();
  coordinate_t basis_origin_on_ref = 
    origin_in_reference() +
    basis_on_this_manifold.origin()[0] * x_axis_centered_dir +
    basis_on_this_manifold.origin()[1] * y_axis_centered_dir;
  coordinate_t basis_dir_on_ref = 
    origin_in_reference() + 
    basis_on_this_manifold.direction()[0] * x_axis_centered_dir +
    basis_on_this_manifold.direction()[1] * y_axis_centered_dir;
 
  // Ok, now return a new bundle with the basis linked
  return basis_bundle_t( basis_on_this_manifold, curve_t( basis_t( basis_origin_on_ref, basis_dir_on_ref ) ) );

}

//=========================================================================

basis_bundle_t plane_t::inverted_basis_bundle( const basis_t& basis_on_reference_manifold ) const
{

  // Ok, check if both points of basis in reference lie on the plane
  curve_t curve_origin_to_plane = shortest_curve_to_plane( basis_on_reference_manifold.origin() );
  curve_t curve_direction_to_plane = shortest_curve_to_plane( basis_on_reference_manifold.direction() );
  if( curve_direction_to_plane.arc_length() > _thickness ||
      curve_origin_to_plane.arc_length() > _thickness ) {
    BOOST_THROW_EXCEPTION( exceptions::manifolds_do_not_overlap_error() );
  }

  // Ok, now 'snap' the basis in the reference onto the plane ( but still
  // in the reference manifold )
  coordinate_t basis_origin = curve_origin_to_plane.at( 1.0 );
  coordinate_t basis_dir = curve_direction_to_plane.at( 1.0 );
  basis_t snapped_basis( basis_origin, basis_dir );

  // Get teh axis basis
  basis_t x_axis_basis = basis_from_simplex( _plane_x_axis_on_reference );
  basis_t y_axis_basis = basis_from_simplex( _plane_y_axis_on_reference );

  // Ok, take the snapped basis and ceter it at the plane origin
  // to be 0
  basis_t centered_basis = basis_t( snapped_basis.origin() - origin_in_reference(),
				    snapped_basis.direction() - origin_in_reference() );
  
  // Ok, now take the centered origin and switch into a 
  // 2dimensional coordinate with the length of the origin
  // basis projected onto the x,y axis basis
  basis_t centered_basis_origin = basis_t( origin_in_reference(),
					   centered_basis.origin() );
  float ox = dot( centered_basis_origin, x_axis_basis ) / x_axis_basis.arc_length();
  float oy = dot( centered_basis_origin, y_axis_basis ) / y_axis_basis.arc_length();

  // Similarly, get the projection of the cetenred direction coordinate
  basis_t centered_basis_dir = basis_t( origin_in_reference(),
					centered_basis.direction() );
  float dx = dot( centered_basis_dir, x_axis_basis ) / x_axis_basis.arc_length();
  float dy = dot( centered_basis_dir, y_axis_basis ) / y_axis_basis.arc_length();

  // create a new basis which is now in hte plane reference
  basis_t plane_basis( coordinate( ox, oy ).on( shared_from_this() ),
		       coordinate( dx, dy ).on( shared_from_this() ) );

  
  // Ok, create a curve
  curve_t plane_curve( plane_basis );
  
  // return the bundle
  return basis_bundle_t( basis_on_reference_manifold, plane_curve );
}

//=====================================================================

basis_bundle_set_t plane_t::inverted_basis_bundle_set( const coordinate_t& point_in_reference_manifold ) const
{

  // First, make sure the point is on reference manifold
  if( _reference && point_in_reference_manifold._manifold != _reference ) {
    BOOST_THROW_EXCEPTION( exceptions::reference_manifolds_differ_error() );
  }

  // Now, get a set of normal basis in reference manifold
  // These are in fact the x and y axis in the reference frame
  // Note: center them on the wanted coordiante
  std::vector<basis_t> ref_axis;
  ref_axis.push_back( basis_from_simplex( _plane_x_axis_on_reference ).center_on( point_in_reference_manifold ) );
  ref_axis.push_back( basis_from_simplex( _plane_y_axis_on_reference ).center_on( point_in_reference_manifold ) );
  
  // For each basis, get the corresponding bundle and return the set
  basis_bundle_set_t bundle_set;
  for( size_t i = 0; i < ref_axis.size(); ++i ) {
    basis_bundle_t bundle = this->inverted_basis_bundle( ref_axis[i] );
    bundle_set.push_back( bundle );
  }
  
  return bundle_set;
}


//=========================================================================

bool plane_t::operator== ( const plane_t& a ) const
{
  return 
    _normal_simplex == a._normal_simplex &&
    _plane_y_axis_on_reference == a._plane_y_axis_on_reference &&
    _plane_x_axis_on_reference == a._plane_x_axis_on_reference;
}

//=========================================================================

namespace ptp {
  namespace coordinates {

    std::ostream& operator<< ( std::ostream& os, const plane_t& a ) 
    {
      os << "P::" << (*a._normal_simplex) << "/x=" << *(a._plane_x_axis_on_reference);
      return os;
    }

  }
}

//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
