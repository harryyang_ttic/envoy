
#include "point_cloud_t.hpp"
#include "matrix_util.hpp"
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <iostream>
#include <cmath>


using namespace ptp;
using namespace ptp::coordinates;


//=====================================================================

// Description:
// A global random number generate
namespace ptp {
  namespace coordinates {

    const gsl_rng_type* _point_cloud_rng_T;
    gsl_rng* _point_cloud_rng = NULL;

  }
}

//=====================================================================

point_cloud_t::point_cloud_t()
{
}

//=====================================================================

int point_cloud_t::size() const
{
  return _points.size();
}

//=====================================================================

int point_cloud_t::dimensionality() const
{
  if( size() < 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::point_cloud_empty_error() );
  }
  return _points[0].dimensionality();
}

//=====================================================================

manifold_tp point_cloud_t::manifold() const
{
  if( size() < 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::point_cloud_empty_error() );
  }
  return _points[0].manifold();
}

//=====================================================================

coordinate_t point_cloud_t::at( const int& idx ) const
{
  return _points[idx];
}

//=====================================================================

coordinate_t& point_cloud_t::at_ref( const int& idx )
{
  return _points[idx];
}

//=====================================================================

void point_cloud_t::add( const coordinate_t& a )
{
  // Make sure the manifold and dimension are consistent if
  // we are not the first point
  if( size() > 0 ) {  
    if( manifold() != a.manifold() ) {
      BOOST_THROW_EXCEPTION( exceptions::coordinate_not_in_manifold_error() );
    }
    if( dimensionality() != a.dimensionality() ) {
      BOOST_THROW_EXCEPTION( exceptions::invalid_dimensionality_error() );
    }
  }

  // Check that the point is 'valid', so non NaN or Infinities
  bool valid = true;
  for( size_t i = 0; i < a.dimensionality(); ++i ) {
    if( std::isnan( a[i] ) || std::isinf( a[i] ) ) {
      valid = false;
      break;
    }
  }
  
  // add the point if valid
  if( valid ) {
    _points.push_back( a );
  }
}

//=====================================================================

point_cloud_tp point_cloud_t::choose( const int& n ) const
{
  // Make sure n > 0 && n < size
  if( n > size() || n < 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::domain_error() );
  }
  if( size() < 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::point_cloud_empty_error() );
  }
  
  // create a random number generate and state if needed
  if( _point_cloud_rng == NULL ) {
    gsl_rng_env_setup(); // uses the environment variable GSL_RNG_TYPE
    _point_cloud_rng_T = gsl_rng_default;
    _point_cloud_rng = gsl_rng_alloc ( _point_cloud_rng_T );
  }
  
  // create an array of the indices
  int* index_array = new int[ size() ];
  int* chosen_array = new int[ n ];
  for( size_t i = 0; i < size(); ++i ) {
    index_array[i] = i;
  }
  
  // sample indices
  int res = gsl_ran_choose( _point_cloud_rng, chosen_array, n, index_array, size(), sizeof(int) );

  // throw exception is we cannot sample
  if( res != 0 ) {
    BOOST_THROW_EXCEPTION( internal_error() );
  }
  
  // Now, create a new point cloud with the chosen points
  point_cloud_tp chosen( new point_cloud_t() );
  for( size_t i = 0; i < n; ++i ) {
    chosen->add( this->at( chosen_array[i] ) );
  }

  // debug
  // std::cout << "chose: ";
  // for( size_t i = 0; i < n; ++i ) {
  //   std::cout << chosen_array[i];
  //   if( i != n-1 ) 
  //     std::cout << ",";
  // }
  // std::cout << std::endl;
  
  // free resources
  delete[] index_array;
  delete[] chosen_array;
  
  // return 
  return chosen;
}

//=====================================================================

coordinate_t point_cloud_t::centroid() const
{
  if( size() < 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::point_cloud_empty_error() );
  }
  
  coordinate_t c( at(0) );
  for( size_t i = 1; i < size(); ++i ) {
    c += at(i);
  }
  c /= static_cast<float>( size() );
  
  return c;
}

//=====================================================================

point_cloud_tp point_cloud_t::center_on( const coordinate_t& a ) const
{
  // make sure we are not empty
  if( size() < 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::point_cloud_empty_error() );
  }
  
  // make sure the given coordiante is on the right manifold
  if( a.manifold() != manifold() ) {
    BOOST_THROW_EXCEPTION( exceptions::coordinate_not_in_manifold_error() );
  }
  
  // Create a new point cloud and subtract the given corodiante
  // from all points, adding result to new cloud
  point_cloud_tp cloud( new point_cloud_t() );
  for( size_t i = 0; i < size(); ++i ) {
    cloud->add( this->at(i) - a );
  }
  
  return cloud;
}

//=====================================================================

matrix_t point_cloud_t::as_row_matrix() const
{
  // make sure cloud is not empty
  if( size() < 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::point_cloud_empty_error() );
  }
  
  // Create a matrix whoose rows will be the coordinates
  matrix_t m( size(), dimensionality() );
  
  // fill the matrix
  for( size_t i = 0; i < size(); ++i ) {
    m.row( i ) = math_vector_from_raw_coordinate( at(i).raw_coordinate() );
  }
  
  // reutrn the matrix
  return m;
}

//=====================================================================

void point_cloud_t::save_ssv( const std::string& filename ) const
{
  matrix_t m = as_row_matrix();
  save_matrix_as_ssv( m, filename );
}


//=====================================================================

mapped_point_cloud_t::mapped_point_cloud_t()
{
}

//=====================================================================

int mapped_point_cloud_t::size() const
{
  return _points.size();
}

//=====================================================================

int mapped_point_cloud_t::dimensionality() const
{
  if( size() < 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::point_cloud_empty_error() );
  }
  return _points[0].dimensionality();
}

//=====================================================================

int mapped_point_cloud_t::mapping_dimensionality() const
{
  if( size() < 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::point_cloud_empty_error() );
  }
  return _mapping_points[0].dimensionality();
}

//=====================================================================

manifold_tp mapped_point_cloud_t::manifold() const
{
  if( size() < 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::point_cloud_empty_error() );
  }
  return _points[0].manifold();
}

//=====================================================================

manifold_tp mapped_point_cloud_t::mapping_manifold() const
{
  if( size() < 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::point_cloud_empty_error() );
  }
  return _mapping_points[0].manifold();
}

//=====================================================================

coordinate_t mapped_point_cloud_t::at( const int& idx ) const
{
  return _points[idx];
}

//=====================================================================

coordinate_t& mapped_point_cloud_t::at_ref( const int& idx )
{
  return _points[idx];
}

//=====================================================================

coordinate_t mapped_point_cloud_t::mapping_at( const int& idx ) const
{
  return _mapping_points[idx];
}

//=====================================================================

coordinate_t& mapped_point_cloud_t::mapping_at_ref( const int& idx )
{
  return _mapping_points[idx];
}

//=====================================================================

void mapped_point_cloud_t::add( const coordinate_t& mapping,
				const coordinate_t& a )
{
  // Make sure the manifold and dimension are consistent if
  // we are not the first point
  if( size() > 0 ) {  
    if( manifold() != a.manifold() ) {
      BOOST_THROW_EXCEPTION( exceptions::coordinate_not_in_manifold_error() );
    }
    if( mapping_manifold() != mapping.manifold() ) {
      BOOST_THROW_EXCEPTION( exceptions::coordinate_not_in_manifold_error() );
    }
    if( dimensionality() != a.dimensionality() ) {
      BOOST_THROW_EXCEPTION( exceptions::invalid_dimensionality_error() );
    }
    if( mapping_dimensionality() != mapping.dimensionality() ) {
      BOOST_THROW_EXCEPTION( exceptions::invalid_dimensionality_error() );
    }
    
  }

  // Check that the point is 'valid', so non NaN or Infinities
  bool valid = true;
  for( size_t i = 0; i < a.dimensionality(); ++i ) {
    if( std::isnan( a[i] ) || std::isinf( a[i] ) ) {
      valid = false;
      break;
    }
  }
  for( size_t i = 0; i < mapping.dimensionality(); ++i ) {
    if( std::isnan( mapping[i] ) || std::isinf( mapping[i] ) ) {
      valid = false;
      break;
    }
  }
  
  // add the point if valid
  if( valid ) {
    _points.push_back( a );
    _mapping_points.push_back( mapping );
  }
}

//=====================================================================

point_cloud_tp mapped_point_cloud_t::points() const
{
  point_cloud_tp p( new point_cloud_t() );
  for( size_t i = 0; i < size(); ++i ) {
    p->add( this->at(i) );
  }
  return p;
}

//=====================================================================

point_cloud_tp mapped_point_cloud_t::mapping_points() const
{
  point_cloud_tp p( new point_cloud_t() );
  for( size_t i = 0; i < size(); ++i ) {
    p->add( this->mapping_at(i) );
  }
  return p;
}

//=====================================================================

mapped_point_cloud_tp mapped_point_cloud_t::choose( const int& n ) const
{
  // Make sure n > 0 && n < size
  if( n > size() || n < 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::domain_error() );
  }
  if( size() < 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::point_cloud_empty_error() );
  }
  
  // create a random number generate and state if needed
  if( _point_cloud_rng == NULL ) {
    gsl_rng_env_setup(); // uses the environment variable GSL_RNG_TYPE
    _point_cloud_rng_T = gsl_rng_default;
    _point_cloud_rng = gsl_rng_alloc ( _point_cloud_rng_T );
  }
  
  // create an array of the indices
  int* index_array = new int[ size() ];
  int* chosen_array = new int[ n ];
  for( size_t i = 0; i < size(); ++i ) {
    index_array[i] = i;
  }
  
  // sample indices
  int res = gsl_ran_choose( _point_cloud_rng, chosen_array, n, index_array, size(), sizeof(int) );

  // throw exception is we cannot sample
  if( res != 0 ) {
    BOOST_THROW_EXCEPTION( internal_error() );
  }
  
  // Now, create a new point cloud with the chosen points
  mapped_point_cloud_tp chosen( new mapped_point_cloud_t() );
  for( size_t i = 0; i < n; ++i ) {
    chosen->add( this->mapping_at( chosen_array[i] ),
		 this->at( chosen_array[i] ) );
  }

  // debug
  // std::cout << "chose: ";
  // for( size_t i = 0; i < n; ++i ) {
  //   std::cout << chosen_array[i];
  //   if( i != n-1 ) 
  //     std::cout << ",";
  // }
  // std::cout << std::endl;
  
  // free resources
  delete[] index_array;
  delete[] chosen_array;
  
  // return 
  return chosen;
}

//=====================================================================

coordinate_t mapped_point_cloud_t::centroid() const
{
  if( size() < 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::point_cloud_empty_error() );
  }
  
  coordinate_t c( at(0) );
  for( size_t i = 1; i < size(); ++i ) {
    c += at(i);
  }
  c /= static_cast<float>( size() );
  
  return c;
}

//=====================================================================

mapped_point_cloud_tp mapped_point_cloud_t::center_on( const coordinate_t& a ) const
{
  // make sure we are not empty
  if( size() < 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::point_cloud_empty_error() );
  }
  
  // make sure the given coordiante is on the right manifold
  if( a.manifold() != manifold() ) {
    BOOST_THROW_EXCEPTION( exceptions::coordinate_not_in_manifold_error() );
  }
  
  // Create a new point cloud and subtract the given corodiante
  // from all points, adding result to new cloud
  mapped_point_cloud_tp cloud( new mapped_point_cloud_t() );
  for( size_t i = 0; i < size(); ++i ) {
    cloud->add( this->mapping_at(i), this->at(i) - a );
  }
  
  return cloud;
}

//=====================================================================

matrix_t mapped_point_cloud_t::as_row_matrix() const
{
  // make sure cloud is not empty
  if( size() < 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::point_cloud_empty_error() );
  }
  
  // Create a matrix whoose rows will be the coordinates
  matrix_t m( size(), dimensionality() );
  
  // fill the matrix
  for( size_t i = 0; i < size(); ++i ) {
    m.row( i ) = math_vector_from_raw_coordinate( at(i).raw_coordinate() );
  }
  
  // reutrn the matrix
  return m;
}

//=====================================================================

void mapped_point_cloud_t::save_ssv( const std::string& filename ) const
{
  matrix_t m = as_row_matrix();
  save_matrix_as_ssv( m, filename );
}


//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
