
#if !defined( __PTP_COORDINATES_plane_t_HPP__ )
#define __PTP_COORDINATES_plane_t_HPP__

#include "manifold.hpp"
#include "simplex_t.hpp"
#include "rotation_t.hpp"

#define DEFAULT_PLANE_THICKNESS 1e-6

namespace ptp {
  namespace coordinates {


    //==================================================================

    // Description:
    // A flat 2D manifold
    class plane_t 
      : public manifold_t
    {
    public:


      // Description:
      // Create a new plane wit hthe given normal and 
      // given x axis ( on reference manifold ) for orientation
      // Also, give the plane the given name
      plane_t( const simplex_tp& normal,
	       const simplex_tp& x_axis,
	       const std::string& name );

      // Description:
      // Create a new plane with given normal
      // and given x axsi ( on reference ) for orientation
      plane_t( const simplex_tp& normal,
	       const simplex_tp& x_axis );

      
      // Description:
      // Returns the origina of the plane coordinates in the
      // referene manifold frame
      coordinate_t origin_in_reference() const
      { return _normal_simplex->points()[0]; }

      // Description:
      // Returns the normal simplex to this palne
      simplex_tp normal_simplex() const
      { return _normal_simplex; }


      // Description:
      // Returns the x axis simplex ( in reference manifold )
      simplex_tp x_axis_simplex() const 
      { return _plane_x_axis_on_reference; }
      
      // Description:
      // Returns teh y axis simplex ( in reference manifold )
      simplex_tp y_axis_simplex() const
      { return _plane_y_axis_on_reference; }

      // Description:
      // Returns the basis bundle for a particular basis.
      // Note that the given basis is in the reference of THIS manifold
      virtual basis_bundle_t basis_bundle( const basis_t& basis_on_this_manifold ) const;

      // Description:
      // Return the inverted basis bundle for a particular basis of
      // the REFERENCE manifold.
      // Note: the given basis in on the REFERENCE manifold
      virtual basis_bundle_t inverted_basis_bundle( const basis_t& basis_on_reference_manifold ) const;
      
      // Description:
      // The basis bundle vectors for the manifold.
      // Note: that these basis are for a particular point on the manifold
      //       and that they are defined in BOTH the coordinate frame of the
      //       reference manifold and this manifold,
      //       but the given point is in THIS manifold
      //virtual basis_bundle_set_t basis_bundle_set( const coordinate_t& point_in_this_manifold ) const;

      // Description:
      // The inverted basis bundle vectors for the manifold.
      // Note: that these basis are for a particular point on the manifold
      //       and that they are defined in BOTH the coordinate frame of the
      //       reference manifold and this manifold,
      //       but the given point is in THE REFERENCE manifold
      virtual basis_bundle_set_t inverted_basis_bundle_set( const coordinate_t& point_in_reference_manifold ) const;
      


      // Description:
      // Returns the shortest curve from given coordinate in the reference
      // manifold and the plane.
      // This curve is in the refernece manifold frame.
      curve_t shortest_curve_to_plane( const coordinate_t& point_in_refenrece_manifold ) const;

      // Description:
      // Returns true iff the two manifolds are the same
      bool operator== (const plane_t& a ) const;
      bool operator!= (const plane_t& a ) const
      { return !( (*this) == a ); }

      // Description:
      // IO operators
      friend std::ostream& operator<< ( std::ostream& os, const plane_t& a );

      

    protected:

      // Description:
      // The normal vector, in the reference manifold, for the plane
      // This includes the origina of the plane
      simplex_tp _normal_simplex;
      
      // Description:
      // The 'thickness' of the plane.
      // While mathematical planes are exactly 2 dimensional, for
      // computers we need a fudge epsilon factor to allow points to
      // be very close to the actual plane after computations and such.
      float _thickness;

      // Description:
      // The axis of the plane in the reference manifold
      simplex_tp _plane_x_axis_on_reference;
      simplex_tp _plane_y_axis_on_reference;
      

      // Description:
      // Build the rotations based on the simple and
      // angle as well as the axis
      void build_rotations_and_axis();

      // Description:
      // Returns the coordinate of the x axis, in the reference manifold
      // without the origin
      coordinate_t _x_axis_coordinate() const
      { return _plane_x_axis_on_reference->points()[1]; }

      // Description:
      // Returns the coordinate of they axis, in the reference manifold
      // without the origin
      coordinate_t _y_axis_coordinate() const
      { return _plane_y_axis_on_reference->points()[1]; }


    private:
    };

    // Description:
    // Smart pointer type
    typedef boost::shared_ptr<const plane_t> plane_tp;

    //==================================================================


  }
}

#endif

