
#if !defined( __PTP_COORDINATES_rotation_t_HPP__ )
#define __PTP_COORDINATES_rotation_t_HPP__

#include "simplex_t.hpp"
#include "angle.hpp"
#include <ptp-common/matrix.hpp>

namespace ptp {
  namespace coordinates {

    using namespace ptp::common;

    //=====================================================================

    // Description:
    // An N-Dimensional rotation.
    // A rotation represents an angular rotation about an (N-2) dimensional
    // simplex inside of an N-dimensional manifold.
    // For example: rotation about a point ( 0-dim simplex ) 
    //              in a plane ( 2-dim manifold )
    // For example: rotation about an axis ( 1-dim simplex )
    //              in 3d.
    class rotation_t
    {
    public:

      // Description:
      // Creates a new rotation about the given simplex of magnitude
      // given by the angle
      rotation_t( const simplex_tp& simplex, const angle_t& angle );

      // Description:
      // Return the simplex
      simplex_tp simplex() const
      { return _simplex; }

      // Description:
      // Retursn the angle about simplex
      angle_t angle_about_simplex() const
      { return _angle; }

      // Description:
      // Returns the manifold this rotation lives in ( the manifold 
      // for the simplex given )
      manifold_tp manifold() const
      { return _simplex->manifold(); }

      // Description:
      // Returns the dimensionality on which this rotation 
      // expectes coordinates
      int dimensionality() const
      { return _dimensionality; }
      
      // Description:
      // Applies this rotation to a coordinate on the
      // manifold this rotation lives in
      coordinate_t rotate_coordinate( const coordinate_t& c ) const;



    protected:

      // Description:
      // The simplex which is 'constant' in the rotation
      // ( such as the point for 2D rotations, or the axis for 3D )
      simplex_tp _simplex;

      // Description:
      // The dimensionality expected for coordinates for this rotation
      int _dimensionality;

      // Description:
      // The angle for the rotation about the simplex
      angle_t _angle;

      // Description:
      // The generalized rotation matrix.
      // This is an NxN matrix, where N is the dimensionality of 
      // the manifold we are in
      matrix_t _matrix;

      // Description:
      // The translation matrices use to center coordinates 
      // before toration
      math_vector_t _pre_translation_vector;
      math_vector_t _post_translation_vector;

      // Description:
      // Initialize with a simplex and angle
      void init( const simplex_tp& simplex, const angle_t& angle );
      
      // Description:
      // Generates teh rotation matrix for the given simplex and angle
      // Along with translation matrices
      matrix_t generate_generalized_rotation_matrix( const simplex_tp& simplex, const angle_t& angle, math_vector_t& pre_translation, math_vector_t& post_translation ) const;

      // Description:
      // Returns a main rotation matrix for the given axis pairs.
      // A main rotation matrix is a rotation of theta on a given
      // plane from Xa to Xb vectors.
      matrix_t main_rotation_matrix( const int& xa, const int& xb, const angle_t& theta ) const;
      
      
    private:
    };

    // Description:
    // A shared pointer to a rotation
    typedef boost::shared_ptr<const rotation_t> rotation_tp;

    //=====================================================================

  }
}

#endif

