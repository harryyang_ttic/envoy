
#include "matrix_util.hpp"
#include <iostream>
#include <fstream>


namespace ptp {
  namespace coordinates {


    //==================================================================
    
    matrix_t create_tiled_col_matrix( const matrix_t& m, const int& col )
    {
      
      matrix_t t( m.rows(), m.cols() );
      for( int i = 0; i < m.rows(); ++i ) {
	for( int j = 0; j < m.cols(); ++j ) {
	  t(i,j) = m( i, col );
	}
      }
      
      return t;
    }

    //==================================================================
    
    matrix_t create_tiled_row_matrix( const matrix_t& m, const int& row )
    {
      
      matrix_t t( m.rows(), m.cols() );
      for( int i = 0; i < m.rows(); ++i ) {
	for( int j = 0; j < m.cols(); ++j ) {
	  t(i,j) = m( row, j );
	}
      }
      
      return t;
    }
    
    
    //==================================================================
    
    math_vector_t math_vector_from_raw_coordinate( const raw_coordinate_t& a )
    {
      math_vector_t v( a.dimensionality() );
      for( size_t i = 0; i < a.dimensionality(); ++i ) {
	v(i) = a[i];
      }
      return v;
    }
    
    //==================================================================
    
    raw_coordinate_t raw_coordinate_from_math_vector( const math_vector_t& a )
    {
      std::vector<float> d;
      for( size_t i = 0; i < a.size(); ++i ) {
	d.push_back( a(i) );
      }
      return raw_coordinate_t( d );
    }
    
    //==================================================================
    
    matrix_t create_matrix_from_simplex_points_col( const simplex_tp& simplex )
    {
      
      // We just use the coordinates of the simplex as columns and
      // glue them together to create a matrix
      matrix_t m( simplex->points()[0].dimensionality(), simplex->points().size() );
      
      for( size_t i = 0; i < simplex->points().size(); ++i ) {
	m.col( i ) = math_vector_from_raw_coordinate( simplex->points()[i].raw_coordinate() );
      }
      
      return m;
    }

    //==================================================================

    int find_min_epsilon_idx( const math_vector_t& a, const float& eps )
    {
      float min_element = 0;
      int idx = -1;
      for( int i = 0; i < a.size(); ++i ) {
	if( a(i) >= eps && ( a(i) < min_element || idx < 0 ) ) {
	  min_element = a(i);
	  idx = i;
	}
      }
      
      return idx;
    }
    
    //==================================================================
    
    void print_matrix( const matrix_t& m )
    {
      std::cout << "{ ";
      for( size_t i = 0; i < m.rows(); ++i ) {
	for( size_t j = 0; j < m.cols(); ++j ) {
	  std::cout << m(i,j) << "  ";
	}
	std::cout << std::endl << "  ";
      }
      std::cout << "}" << std::endl;
    }

    //==================================================================
    
    
    void print_vector( const math_vector_t& v ) 
    {
      std::cout << "[ ";
      for( size_t i = 0; i < v.size(); ++i ) {
	std::cout << v(i) << "  ";
      }
      std::cout << "]" << std::endl;
    }
    
    //==================================================================

    void save_matrix_as_ssv( const matrix_t& m, const std::string& filename )
    {
      
      std::ofstream fout( filename.c_str() );
      for( size_t i = 0; i < m.rows(); ++i ) {
	for( size_t j = 0; j < m.cols(); ++j ) {
	  fout << m(i,j);
	  if( j != m.cols() -1 )
	    fout << " ";
	}
	fout << std::endl;
      }
      fout << std::endl;
      fout.close();
    }

    //==================================================================
    

  }
}
