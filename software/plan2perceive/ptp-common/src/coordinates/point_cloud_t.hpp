
#if !defined( __PTP_COORDINATES_point_cloud_t_HPP__ )
#define __PTP_COORDINATES_point_cloud_t_HPP__

#include "manifold.hpp"
#include <ptp-common/matrix.hpp>

namespace ptp {
  namespace coordinates {

    using namespace ptp::common;

    //===============================================================

    // Description:
    // Foward declarations
    class point_cloud_t;
    class mapped_point_cloud_t;
    typedef boost::shared_ptr<point_cloud_t> point_cloud_tp;
    typedef boost::shared_ptr<mapped_point_cloud_t> mapped_point_cloud_tp;

    //===============================================================

    namespace exceptions {

      // Description:
      // Some functions require non-empty point clouds
      struct point_cloud_empty_error : public virtual exception_base {};

    }

    //===============================================================

    // Description:
    // A point cloud is a set of coordinate_t with the same
    // manifold.
    // There are some useful methods to calculate things and
    // to apply operation on all the points.
    // Also, point clouds allow us to pass pointers to them
    // and so less copying is done
    class point_cloud_t
    {
    public:

      // Description:
      // Create a new empty point cloud
      point_cloud_t();
      
      // Description:
      // Returns the size of a point cloud
      int size() const;
      
      // Description:
      // Returns the dimensionality of the points in the cloud
      int dimensionality() const;
      
      // Description:
      // Returns the manifold for teh cloud
      manifold_tp manifold() const;

      // Description:
      // Add a point to this point cloud.
      // This will only add VALID points, so coordinates which have 
      // NEITHER Infinity or NaN values!
      void add( const coordinate_t& a );

      // Description:
      // Returns the point at a given index in this cloud
      coordinate_t  at( const int& idx ) const;
      coordinate_t& at_ref( const int& idx );
      
      // Description:
      // Returns a new point cloud with points from this ckoud chosen
      // uniformaly at random ( sampled without replacement )
      point_cloud_tp choose( const int& n ) const;
      
      // Description:
      // Returns the centroid of the point cloud
      coordinate_t centroid() const;

      // Description:
      // Centeres the point in this cloud to the given 
      // point.  It essentially subtracts the given point from
      // all points in the cloud
      point_cloud_tp center_on( const coordinate_t& a ) const;

      // Description:
      // Returns a new matrix which represents the points in
      // this cloud. The rows of the matrix are the coordinates.
      matrix_t as_row_matrix() const;

      // Description:
      // Save a point cloud as a Space-Separated-Values (.ssv) file
      // Each line is a point, with spaces between the coordinate dims
      void save_ssv( const std::string& filename ) const;
      
    protected:

      // Description:
      // The vector of coordiantes in this cloud
      std::vector<coordinate_t> _points;
      
      
    };
    

    //===============================================================

    // Description:
    // A mapped point cloud is a set of coordinate_t with the same
    // manifold that have a paired other coordiante_t (the mapping).
    // There are some useful methods to calculate things and
    // to apply operation on all the points.
    // Also, point clouds allow us to pass pointers to them
    // and so less copying is done
    class mapped_point_cloud_t
    {
    public:

      // Description:
      // Create a new empty point cloud
      mapped_point_cloud_t();
      
      // Description:
      // Returns the size of a point cloud
      int size() const;
      
      // Description:
      // Returns the dimensionality of the points in the cloud
      int dimensionality() const;
      
      // Description:
      // Retursn the dimensionality of mapping points in the cloud
      int mapping_dimensionality() const;
      
      // Description:
      // Returns the manifold for the cloud (not the mapping)
      manifold_tp manifold() const;
      manifold_tp mapping_manifold() const;

      // Description:
      // Add a point to this point cloud.
      // The first coordinate is the mapping coordinate, the second
      // is the coordinate it maps to.
      // This will only add VALID points, so coordinates which have 
      // NEITHER Infinity or NaN values!
      void add( const coordinate_t& mapping, const coordinate_t& a );

      // Description:
      // Returns the point at a given index in this cloud
      coordinate_t  at( const int& idx ) const;
      coordinate_t& at_ref( const int& idx );

      // Description:
      // Returns the mapping coordinate at a given index
      coordinate_t mapping_at( const int& idx ) const;
      coordinate_t& mapping_at_ref( const int& idx );

      // Description:
      // Returns the points in this mapped point cloud ( the non mapping points)
      // this is a COPY
      point_cloud_tp points() const;
      
      // Description:
      // Rtursn the mapping points in this point cloud
      // This is a COPY
      point_cloud_tp mapping_points() const;
      
      // Description:
      // Returns a new point cloud with points from this ckoud chosen
      // uniformaly at random ( sampled without replacement )
      mapped_point_cloud_tp choose( const int& n ) const;
      
      // Description:
      // Returns the centroid of the point cloud
      coordinate_t centroid() const;

      // Description:
      // Centeres the point in this cloud to the given 
      // point.  It essentially subtracts the given point from
      // all points in the cloud
      mapped_point_cloud_tp center_on( const coordinate_t& a ) const;

      // Description:
      // Returns a new matrix which represents the points in
      // this cloud. The rows of the matrix are the coordinates.
      matrix_t as_row_matrix() const;

      // Description:
      // Save a point cloud as a Space-Separated-Values (.ssv) file
      // Each line is a point, with spaces between the coordinate dims
      void save_ssv( const std::string& filename ) const;
      
    protected:

      // Description:
      // The vector of coordiantes in this cloud
      std::vector<coordinate_t> _points;
      
      // Description:
      // THe vector of mapping coordinates in this cloud
      std::vector<coordinate_t> _mapping_points;
      
    };


    //===============================================================

  }
}

#endif

