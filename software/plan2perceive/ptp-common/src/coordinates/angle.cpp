
#include "angle.hpp"


using namespace ptp;
using namespace coordinates;


//=======================================================================

// Description:
// Static initialization
angle_t angle_t::X_AXIS( 0, true );

//=======================================================================

float raw_angle_t::deg_to_rad( const float& a )
{
  return a * M_PI / 180.0;
}

//=======================================================================

float raw_angle_t::rad_to_deg( const float& a )
{
  return a * 180.0 / M_PI;
}

//=======================================================================

float raw_angle_t::degree_value() const
{
  if( _using_radians ) {
    return rad_to_deg( _value );
  } else {
    return _value;
  }
}

//=======================================================================

float raw_angle_t::radian_value() const
{
  if( _using_radians ) {
    return _value;
  } else {
    return deg_to_rad( _value );
  }
}

//=======================================================================

angle_t raw_angle_t::from( const angle_t& a )
{
  return angle_t( _value, _using_radians, boost::shared_ptr<angle_t>(new angle_t(a)) );
}

//=======================================================================

namespace ptp {
  namespace coordinates {

    raw_angle_t deg( const float& a )
    {
      return raw_angle_t( a, false );
    }
    

    raw_angle_t rad( const float& a )
    {
      return raw_angle_t( a, true );
    }
  }
}

//=======================================================================

bool angle_t::is_basis() const
{
  return static_cast<bool>(this->_reference);
}

//=======================================================================

angle_t angle_t::from( const angle_t& a ) const
{
  if( this->_reference &&
      (*this->_reference) == a )
    return *this;

  // We are both BASIS, so we are in the same reference frame
  // by definition
  if( !this->_reference && !a._reference ) {
    float new_val = 0;
    if( _using_radians ) {
      new_val = _value - a.radians();
    } else {
      new_val = _value - a.degrees();
    }
    return angle_t( new_val, _using_radians, _reference );
  }
  
  // Spacial case, this object has a basis reference
  // so we can do the math once the other angle hase been
  // grounded to the basis
  if( !this->_reference ) {
    angle_t x_a = a.from( angle_t::X_AXIS );
    float new_val = 0;
    if( _using_radians ) {
      new_val = _value - x_a.radians();
    } else {
      new_val = _value - x_a.degrees();
    }
    return angle_t( new_val, _using_radians, boost::shared_ptr<angle_t>(new angle_t(a)) );

  } else {

 
    // Simply add the differences in the referenes to the value and
    // reference to the correct basis
    angle_t copy = *this;
    copy.rereference( a );
    return copy;
  
  }
}

//=======================================================================

angle_t angle_t::rereference( const angle_t& a )
{
  // check if we already have the reference
  if( this->_reference && 
      (*this->_reference) == a )
    return *this;

  // Special case when this is a basis
  // to compute the new relative value
  if( !this->_reference ) {
    
    // get the other angle in ters of the basis
    angle_t x_a = a.from( angle_t::X_AXIS );
    
    // Now, compute the difference in the values
    float new_value = 0;
    if( _using_radians ) {
      new_value = _value - x_a.radians();
    } else {
      new_value = _value - x_a.degrees();
    }
    
    // set the new value
    _value = new_value;

    
  } else {
  
    // calculate difference in the references
    // we know that ht result will be in the reference of the first argument
    // which is the 'a' parameter to this function
    angle_t diff_ref = a - (*this->_reference);
    
    // add to the value of this angle the difference in the referneces
    //unsafe_add_to_value( diff_ref.as_raw_angle() );
    unsafe_subtract_to_value( diff_ref.as_raw_angle() );
    
  }


  // set the new reference angle
  this->_reference = boost::shared_ptr<angle_t>(new angle_t(a));

  // return the now change this
  return *this;
}

//=======================================================================

raw_angle_t angle_t::as_raw_angle() const
{
  if( _using_radians )
    return rad( _value );
  return deg( _value );
}

//=======================================================================

void angle_t::unsafe_add_to_value( const raw_angle_t& a )
{
  if( _using_radians ) {
    _value += a.radian_value();
  } else {
    _value += a.degree_value();
  }
}

//=======================================================================

void angle_t::unsafe_subtract_to_value( const raw_angle_t& a )
{
  if( _using_radians ) {
    _value -= a.radian_value();
  } else {
    _value -= a.degree_value();
  }
}

//=======================================================================

float angle_t::degrees() const
{
  return this->as_raw_angle().degree_value();
}

//=======================================================================

float angle_t::radians() const
{
  return this->as_raw_angle().radian_value();
}

//=======================================================================

angle_t angle_t::ensure_zero_centered()
{
  float half_circle_value = 0;
  if( _using_radians ) {
    half_circle_value = M_PI;
  } else {
    half_circle_value = 180;
  }
  
  while( _value < -half_circle_value ) {
    _value += 2 * half_circle_value;
  } 
  while( _value > half_circle_value ) {
    _value -= 2 * half_circle_value;
  }

  return *this;
}

//=======================================================================

angle_t angle_t::ensure_positive()
{
  float circle_radius = 0;
  if( _using_radians ) {
    circle_radius = 2 * M_PI;
  } else {
    circle_radius = 360;
  }
  while( _value > circle_radius ) {
    _value -= circle_radius;
  }
  while( _value < 0 ) {
    _value += circle_radius;
  }
  return *this;
}

//=======================================================================

angle_t angle_t::operator+= ( const angle_t& a )
{
  angle_t grounded_a;
  if( this->_reference ) {
    grounded_a = a.from( (*this->_reference) );
  } else {
    grounded_a = a.from( angle_t::X_AXIS );
  }
  if( _using_radians ) {
    _value += grounded_a.radians();
  } else {
    _value += grounded_a.degrees();
  }

  // since we have the same reference, apply operator to reference
  if( _reference && (*_reference != angle_t::X_AXIS) ) {
    _reference = 
      boost::shared_ptr<angle_t>( new angle_t( (*this->_reference) + (*grounded_a._reference) ) );
  } else {
  }
  
  return *this;
}

//=======================================================================

angle_t angle_t::operator-= ( const angle_t& a )
{
  angle_t grounded_a;
  if( this->_reference ) {
    grounded_a = a.from( (*this->_reference) );
  } else {
    grounded_a = a.from( angle_t::X_AXIS );
  }
  if( _using_radians ) {
    _value -= grounded_a.radians();
  } else {
    _value -= grounded_a.degrees();
  }

  // since we have the same reference, apply operator to reference
  if( _reference && (*_reference != angle_t::X_AXIS) ) {
    _reference = 
      boost::shared_ptr<angle_t>( new angle_t( (*this->_reference) - (*grounded_a._reference) ) );
  } else {
  }
  

  return *this;
}

//=======================================================================

angle_t angle_t::operator*= ( const angle_t& a )
{
  angle_t grounded_a;
  if( this->_reference ) {
    grounded_a = a.from( (*this->_reference) );
  } else {
    grounded_a = a.from( angle_t::X_AXIS );
  }
  if( _using_radians ) {
    _value *= grounded_a.radians();
  } else {
    _value *= grounded_a.degrees();
  }

    // since we have the same reference, apply operator to reference
  if( _reference  && (*_reference != angle_t::X_AXIS) ) {
    _reference = 
      boost::shared_ptr<angle_t>( new angle_t( (*this->_reference) * (*grounded_a._reference) ) );
  } else {
  }

  return *this;
}

//=======================================================================

angle_t angle_t::operator/= ( const angle_t& a )
{
  angle_t grounded_a;
  if( this->_reference ) {
    grounded_a = a.from( (*this->_reference) );
  } else {
    grounded_a = a.from( angle_t::X_AXIS );
  }
  if( _using_radians ) {
    _value /= grounded_a.radians();
  } else {
    _value /= grounded_a.degrees();
  }

  // since we have the same reference, apply operator to reference
  if( _reference && (*_reference != angle_t::X_AXIS) ) {
    _reference = 
      boost::shared_ptr<angle_t>( new angle_t( (*this->_reference) / (*grounded_a._reference) ) );
  } else {
  }

  return *this;
}

//=======================================================================

bool angle_t::operator< ( const angle_t& a ) const
{
  angle_t grounded_a;
  angle_t positive_this;
  if( this->_reference ) {
    grounded_a = a.from( (*this->_reference) );
    positive_this = this->from( (*this->_reference) );
  } else {
    grounded_a = a.from( angle_t::X_AXIS );
    positive_this = this->from( angle_t::X_AXIS );
  }
  grounded_a.ensure_positive();
  positive_this.ensure_positive();
  return positive_this._value < grounded_a._value;
}

//=======================================================================

bool angle_t::operator== ( const angle_t& a ) const
{
  angle_t grounded_a;
  angle_t positive_this;
  if( this->_reference ) {
    grounded_a = a.from( (*this->_reference) );
    positive_this = this->from( (*this->_reference) );
  } else {
    grounded_a = a.from( angle_t::X_AXIS );
    positive_this = this->from( angle_t::X_AXIS );
  }
  grounded_a.ensure_positive();
  positive_this.ensure_positive();
  return positive_this._value == grounded_a._value;
}

//=======================================================================

namespace ptp {
  namespace coordinates {

    angle_t operator- ( const angle_t& a )
    {
      return angle_t( -a._value, a._using_radians, a._reference );
    }
    
  }
}

//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
