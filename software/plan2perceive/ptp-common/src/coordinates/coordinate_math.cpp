
#include "coordinate_math.hpp"

namespace ptp {
  namespace coordinates {

    using namespace ptp::common;

    //====================================================================

    basis_t cross( const basis_t& a, const basis_t& b )
    {
      if( a.dimensionality() != 3 )
	BOOST_THROW_EXCEPTION( exceptions::invalid_dimensionality_error() );
      if( b.dimensionality() != 3 )
	BOOST_THROW_EXCEPTION( exceptions::invalid_dimensionality_error() );
      if( a.manifold() != b.manifold() )
	BOOST_THROW_EXCEPTION( exceptions::reference_manifolds_differ_error() );
      if( a.origin() != b.origin() ) 
	BOOST_THROW_EXCEPTION( exceptions::basis_origins_differ_error() );
      
      coordinate_t c_norm( a.direction() );
      coordinate_t a_norm = a.origin_centered_direction();
      coordinate_t b_norm = b.origin_centered_direction();
      c_norm[0] = a_norm[1]*b_norm[2] - a_norm[2]*b_norm[1];
      c_norm[1] = a_norm[2]*b_norm[0] - a_norm[0]*b_norm[2];
      c_norm[2] = a_norm[0]*b_norm[1] - a_norm[1]*b_norm[0];
      
      return basis_t( a.origin(), c_norm + a.origin() );
    }

    //====================================================================
    
    float dot( const basis_t& a, const basis_t& b )
    {
      if( a.dimensionality() != b.dimensionality() )
	BOOST_THROW_EXCEPTION( exceptions::dimensionalities_do_not_agree_error() );
      if( a.manifold() != b.manifold() )
	BOOST_THROW_EXCEPTION( exceptions::reference_manifolds_differ_error() );
      if( a.origin() != b.origin() ) 
	BOOST_THROW_EXCEPTION( exceptions::basis_origins_differ_error() );
      
      
      float p;
      coordinate_t a_norm = a.origin_centered_direction();
      coordinate_t b_norm = b.origin_centered_direction();
      for( size_t i = 0; i < a.dimensionality(); ++i ) {
	p += a_norm[i] * b_norm[i];
      }
      return p;
    }

    //====================================================================

    basis_t normalize_length( const basis_t& a, const float& d )
    {
      return basis_t( a.origin(), 
		      a.origin() + d * a.origin_centered_direction() / a.arc_length() );
    }

    //====================================================================

  }
}
