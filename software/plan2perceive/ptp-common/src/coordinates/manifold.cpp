
#include "manifold.hpp"
#include <iostream>

using namespace ptp;
using namespace coordinates;

//=====================================================================

// Description:
// Static initialization
manifold_tp manifold_t::BASE_MANIFOLD = boost::shared_ptr<manifold_t>( new manifold_t("BASE") );

//=====================================================================

raw_coordinate_t::raw_coordinate_t( const std::vector<float>& data )
  : _data( data )
{
}

//=====================================================================

int raw_coordinate_t::dimensionality() const
{
  return _data.size();
}

//=====================================================================

float raw_coordinate_t::operator[] ( const int& dim ) const
{
  return _data[dim];
}

//=====================================================================

float& raw_coordinate_t::operator[] ( const int& dim )
{
  return _data[dim];
}

//=====================================================================

coordinate_t raw_coordinate_t::on( const manifold_tp& man ) const
{
  return coordinate_t( *this, man );
}

//=====================================================================

double raw_coordinate_t::l2_norm( const raw_coordinate_t& a, const raw_coordinate_t& b )
{
  assert( a.dimensionality() == b.dimensionality() );
  double sum = 0;
  for( int i = 0; i < a.dimensionality(); ++i ) {
    sum += ( b._data[i] - a._data[i] ) * ( b._data[i] - a._data[i] );
  }
  return sum;
}

//=====================================================================

raw_coordinate_t raw_coordinate_t::operator+= ( const raw_coordinate_t& a )
{
  assert( dimensionality() == a.dimensionality() );
  for( int i = 0; i < dimensionality(); ++i ) {
    _data[i] += a._data[i];
  }
  return *this;
}

//=====================================================================

raw_coordinate_t raw_coordinate_t::operator-= ( const raw_coordinate_t& a )
{
  assert( dimensionality() == a.dimensionality() );
  for( int i = 0; i < dimensionality(); ++i ) {
    _data[i] -= a._data[i];
  }
  return *this;
}

//=====================================================================

raw_coordinate_t raw_coordinate_t::operator*= ( const float& a )
{
  for( int i = 0; i < dimensionality(); ++i ) {
    _data[i] *= a;
  }
  return *this;
}

//=====================================================================

raw_coordinate_t raw_coordinate_t::operator/= ( const float& a )
{
  for( int i = 0; i < dimensionality(); ++i ) {
    _data[i] /= a;
  }
  return *this;
}

//=====================================================================

bool raw_coordinate_t::operator== ( const raw_coordinate_t& a ) const
{
  if( dimensionality() != a.dimensionality() )
    return false;
  for( size_t i = 0; i < dimensionality(); ++i ) {
    if( _data[i] != a._data[i] )
      return false;
  }
  return true;
}

//=====================================================================

bool raw_coordinate_t::operator< ( const raw_coordinate_t& a ) const
{
  if( dimensionality() != a.dimensionality() )
    return dimensionality() < a.dimensionality();
  for( size_t i = 0; i < dimensionality(); ++i ) {
    if( _data[i] >= a._data[i] )
      return false;
  }
  return true;
}

//=====================================================================

namespace ptp {
  namespace coordinates {

    raw_coordinate_t coordinate( const float& a ) {
      std::vector<float> d;
      d.push_back( a );
      return raw_coordinate_t(d);
    }

    raw_coordinate_t coordinate( const float& a, const float& b ) {
      std::vector<float> d;
      d.push_back( a );
      d.push_back( b );
      return raw_coordinate_t(d);
    }
    
    raw_coordinate_t coordinate( const float& a, const float& b, const float& c ) {
      std::vector<float> d;
      d.push_back( a );
      d.push_back( b );
      d.push_back( c );
      return raw_coordinate_t(d);
    }

  }
}

//=====================================================================

coordinate_t::coordinate_t( const raw_coordinate_t& coord, const manifold_tp& man )
  : _coordinate( coord ), _manifold( man )
{
}

//=====================================================================

coordinate_t::coordinate_t()
  : _coordinate( std::vector<float>() ), _manifold()
{
}

//=====================================================================

int coordinate_t::dimensionality() const
{
  return _coordinate.dimensionality();
}

//=====================================================================

float coordinate_t::operator[] ( const int& dim ) const 
{
  return _coordinate[ dim ];
}

//=====================================================================

float& coordinate_t::operator[] ( const int& dim )
{
  return _coordinate[ dim ];
}

//=====================================================================

coordinate_t coordinate_t::operator+= ( const coordinate_t& a )
{
  assert( this->_manifold == a._manifold );
  if( this->_manifold != a._manifold ) {
    BOOST_THROW_EXCEPTION( exceptions::reference_manifolds_differ_error() );
  }
  _coordinate += a._coordinate;
  return *this;
}

//=====================================================================

coordinate_t coordinate_t::operator-= ( const coordinate_t& a )
{
  assert( this->_manifold == a._manifold );
  if( this->_manifold != a._manifold ) {
    BOOST_THROW_EXCEPTION( exceptions::reference_manifolds_differ_error() );
  }
  _coordinate -= a._coordinate;
  return *this;
}

//=====================================================================

coordinate_t coordinate_t::operator*= ( const float& a )
{
  _coordinate *= a;
  return *this;
}

//=====================================================================

coordinate_t coordinate_t::operator/= ( const float& a )
{
  _coordinate /= a;
  return *this;
}

//=====================================================================

bool coordinate_t::operator== ( const coordinate_t& a ) const
{
  return 
    ( _coordinate == a._coordinate ) && 
    ( _manifold == a._manifold );
}

//=====================================================================

bool coordinate_t::operator< ( const coordinate_t& a ) const
{
  return _coordinate < a.on( _manifold )._coordinate;
}

//=====================================================================

coordinate_t coordinate_t::on( const manifold_tp& man ) const
{
  
  // TODO:
  // This needs some checking for edge cases
  
  coordinate_t copy( *this );
  copy.rereference( man );
  return copy;
}

//=====================================================================

coordinate_t coordinate_t::project_onto( const manifold_tp& man ) const
{
  // TODO:
  // This REALLY needs some work
  return this->on( man );
}

//=====================================================================

coordinate_t coordinate_t::lift( const manifold_tp& man ) const
{
  return coordinate_t( _coordinate, man );
}

//=====================================================================

void coordinate_t::rereference( const manifold_tp& man )
{
  
  // This is the heart of a coordinate transformation, where
  // we take this point which is in the frame of reference of
  // manifold A and turn it into the same point, but now in 
  // the frame of reference of manifold B ( given as man ).

  // Make sure we have a manifold
  assert( _manifold );
  if( _manifold == false || man == false ) {
    BOOST_THROW_EXCEPTION( exceptions::null_manifold_error() );
  }
  
  // First, check if the references are already the same
  // and if so we are done
  if( _manifold == man ) {
    return;
  }

  // If the basis manifold for this coordinate is the BASE manifold,
  // try to get at this coordinate using the reference of the given
  // manifold
  if( _manifold == manifold_t::BASE_MANIFOLD ) {

    // Ok, get the point in the reference of the manifold
    coordinate_t this_in_new_ref = this->on( man->reference() );
    
    // Now, get the inverted basis bundle to allow us to go from
    // the point in the reference manifold to the point in the
    // wanted ( embedded ) manifold
    basis_bundle_set_t inverted_basis_bundle_set = man->inverted_basis_bundle_set( this_in_new_ref );
    
    // Ok, use the inverted basis to get the wanted coordinate and return it
    coordinate_t this_in_man = get_grounding_origin( inverted_basis_bundle_set );
    //return this_in_man;
    (*this) = this_in_man;

  } else {
    
    // First grab the basis bundle about this point
    basis_bundle_set_t basis_bundle_set = _manifold->basis_bundle_set( *this );

    // Now compute this point in the reference of our manifold ( 1-level down )
    coordinate_t this_in_manifold_reference = get_grounding_origin( basis_bundle_set );

    // Now get this new base point in the given manifold (which is what we want)
    this_in_manifold_reference.rereference( man );

    //return this_in_manifold_reference;
    (*this) = this_in_manifold_reference;
  }

}

//=====================================================================

basis_t::basis_t( const coordinate_t& origin, const coordinate_t& dir )
  : _origin( origin ), _direction( dir )
{
}

//=====================================================================

float basis_t::arc_length() const
{
  return sqrt( raw_coordinate_t::l2_norm( _origin._coordinate, _direction._coordinate ) );
}

//=====================================================================

manifold_tp basis_t::manifold() const
{
  return _origin._manifold;
}

//=====================================================================

basis_t basis_t::translate( const coordinate_t& t ) const
{
  return basis_t( origin() + t,
		  direction() + t );
}

//=====================================================================

basis_t basis_t::center_on( const coordinate_t& orig ) const
{
  return basis_t( orig,
		  orig + origin_centered_direction() );
}

//=====================================================================

curve_t basis_t::on( const manifold_tp& man ) const
{
  // Use curve_t interface
  curve_t curve( *this );
  return curve.on( man );
}

//=====================================================================

basis_t basis_t::lift( const manifold_tp& man ) const
{
  return basis_t( _origin.lift( man ), _direction.lift( man ) );
}

//=====================================================================

basis_bundle_t::basis_bundle_t( const basis_t& basis, const curve_t& curve )
  : _embeded_basis( basis ), _reference_curve( curve )
{
}

//=====================================================================

basis_bundle_t basis_bundle_t::lift( const manifold_tp& basis_man, const manifold_tp& curve_man ) const
{
  return basis_bundle_t( _embeded_basis.lift( basis_man ), _reference_curve.lift( curve_man ) );
}

//=====================================================================

curve_t::curve_t( const std::vector<basis_t>& pieces )
  : _data(pieces), _compound_curve( false )
{
  // check that manifolds for all pieces are the SAME
  for( int i = 1; i < pieces.size(); ++i ) {
    if( pieces[i]._origin._manifold != pieces[i-1]._origin._manifold ) {
      BOOST_THROW_EXCEPTION( exceptions::reference_manifolds_differ_error() );
    }
  }
  
  // First, set the manifold of the curve to hte manifold of the  
  // basis
  _manifold = pieces[0]._origin._manifold;
 
  // Now, compute the total arc length by summing the distances of
  // the basis
  _total_arc_length = 0;
  for( size_t i = 0; i < _data.size(); ++i ) {
    _total_arc_length += _data[i].arc_length();
  }
}


//=====================================================================

curve_t::curve_t( const std::vector<curve_t>& curves )
  : _curves( curves ), _compound_curve( true )
{
  // check that manifolds for all pieces are the SAME
  for( int i = 1; i < _curves.size(); ++i ) {
    if( _curves[i]._manifold != _curves[i-1]._manifold ) {
      BOOST_THROW_EXCEPTION( exceptions::reference_manifolds_differ_error() );
    }
  }
  
  // First, set the manifold of the curve to hte manifold of the  
  // basis
  _manifold = _curves[0]._manifold;
 
  // Now, compute the total arc length by summing the distances of
  // the basis
  _total_arc_length = 0;
  for( size_t i = 0; i < _data.size(); ++i ) {
    _total_arc_length += _curves[i].arc_length();
  }
}

//=====================================================================

curve_t::curve_t( const basis_t& basis )
  : _compound_curve( false )
{
  
  // First, set the manifold of the curve to hte manifold of the  
  // basis
  _manifold = basis._origin._manifold;

  // add the basis to hte se of basis
  _data.push_back( basis );
 
  // Now, compute the total arc length
  _total_arc_length = basis.arc_length();
}

//=====================================================================

float curve_t::arc_length() const
{
  return _total_arc_length;
}

//=====================================================================

coordinate_t curve_t::at( const float& offset ) const
{
  assert( offset >= 0 );
  assert( offset <= 1 );
  if( offset < 0 || offset > 1 ) {
    BOOST_THROW_EXCEPTION( exceptions::domain_error() );
  }

  // If we are not a compound curve, return the point
  // in the line segments, else recurse to return point
  // in the right sub-curve
  if( _compound_curve == false ) {

    
    // Find the basis which 'contains' the wanted point linearly
    int basis_idx = 0;
    float offset_left = offset;
    for( ; basis_idx < _data.size()-1; ++basis_idx ) {
      float offset_of_basis = _data[basis_idx].arc_length();
      if( offset_of_basis > 0 ) {
	offset_of_basis /= _total_arc_length;
      }
      if( offset_of_basis > offset_left ) {
	// this is the right basis, break loop
	break;
      }
      offset_left -= offset_of_basis;
    }
    
    // Ok, now that we have the right basis and offset left, move along
    // the basis line with the wanted distance
    // Make sure to take care of zero length curves
    float wanted_distance = offset_left * this->arc_length();
    float wanted_fraction = 0;
    if( _data[basis_idx].arc_length() > 0 ) {
      wanted_fraction = wanted_distance / _data[basis_idx].arc_length();
    }
    coordinate_t point = _data[basis_idx]._origin + _data[basis_idx]._direction * wanted_fraction;
    
    return point;
  
  } else {
  
    // find the curve which contains the wanted point
    int curve_idx = 0;
    float offset_left = offset;
    for( ; curve_idx < _curves.size()-1; ++curve_idx ) {
      float offset_of_curve = _curves[curve_idx].arc_length();
      if( offset_of_curve > 0 ) {
	offset_of_curve /= _total_arc_length;
      }
      if( offset_of_curve > offset_left ) {
	// this is the curve we want which contains the point
	break;
      }
      offset_left -= offset_of_curve;
    }

    // Now that we have the subcurve, query it
    float wanted_fraction = 0;
    if( _curves[curve_idx].arc_length() > 0 ) {
      wanted_fraction = ( offset_left * _total_arc_length ) / _curves[curve_idx].arc_length();
    }
    return _curves[curve_idx].at( wanted_fraction );
    
  }
}

//=====================================================================

std::vector<float> curve_t::changepoints() const
{
  std::vector<float> points;
  
  if( _compound_curve == false ) {
    points.push_back( 0 );
    float sum = 0;
    for( size_t i = 0; i < _data.size(); ++i ) {
      sum += _data[i].arc_length();
      points.push_back( sum / arc_length() );
    }
    points.push_back( 1.0 );
  } else {
    for( size_t i = 0; i < _curves.size(); ++i ) {
      std::vector<float> sub_points = _curves[i].changepoints();
      points.insert( points.end(), sub_points.begin(), sub_points.end() );
    }
  }
    
  return points;
}

//=====================================================================

std::vector<basis_t> curve_t::segment( const float& start, const float& end ) const
{
  std::vector<basis_t> seg;

  if( _compound_curve == false ) {

    // Ok, find the first and last basis we need
    int first_idx = 0;
    int last_idx = 0;
    float start_left = start;
    float end_left = end;
    float start_frac = 0;
    float end_frac = 0;
    for( size_t i = 0; i < _data.size(); ++i ) {
      if( end_left > 0 ) {
	last_idx = i;
	end_frac = end_left;
      }
      if( start_left > 0 ) {
	first_idx = i;
	start_frac = start_left;
      }
      float al = _data[i].arc_length();
      start_left -= al;
      end_left -= al;
    }

    // Now that we have the indices, we need to get the partials for 
    // the start and end

    // Get the partial staring basis by adding to the origin
    // of the starting basis
    basis_t partial_start = _data[ first_idx ];
    float fraction = start_frac * arc_length() / _data[first_idx].arc_length();
    partial_start._origin += (partial_start._direction * fraction);
    
    // Get the partial ending basis by subtracting from the 
    // direction vector
    basis_t partial_end = _data[ last_idx ];
    fraction = end_frac * arc_length() / _data[last_idx].arc_length();
    partial_end._direction *= fraction;

    // Ok, now add the partial start, the intermediate basis, and the 
    // partial end
    seg.push_back( partial_start );
    for( size_t i = first_idx + 1; i < last_idx; ++i ) {
      seg.push_back( _data[ i ] );
    }
    seg.push_back( partial_end );

  } else {

    // Ok, find the first and last curves we need
    int first_idx = 0;
    int last_idx = 0;
    float start_left = start;
    float end_left = end;
    float start_frac = 0;
    float end_frac = 0;
    for( size_t i = 0; i < _curves.size(); ++i ) {
      if( end_left > 0 ) {
	last_idx = i;
	end_frac = end_left;
      }
      if( start_left > 0 ) {
	first_idx = i;
	start_frac = start_left;
      }
      float al = _curves[i].arc_length();
      start_left -= al;
      end_left -= al;
    }

    // Now that we have the indices, we need to get the partials for 
    // the start and end

    // Get the partial staring curve segment
    float fraction = start_frac * arc_length() / _curves[first_idx].arc_length();
    std::vector<basis_t> start_seg = _curves[first_idx].segment( fraction, 1.0 );
    
    // Get the partial ending curve segment
    fraction = end_frac * arc_length() / _curves[last_idx].arc_length();
    std::vector<basis_t> end_seg = _curves[last_idx].segment( 0.0, fraction );

    // Ok, now add the partial start, the intermediate curves, and the 
    // partial end
    seg.insert( seg.end(), start_seg.begin(), start_seg.end() );
    for( size_t i = first_idx + 1; i < last_idx; ++i ) {
      std::vector<basis_t> sub_seg = _curves[i].segment(0.0, 1.0);
      seg.insert( seg.end(), sub_seg.begin(), sub_seg.end() );
    }
    seg.insert( seg.end(), end_seg.begin(), end_seg.end() );
    
  }

  // return the segments
  return seg;

}

//=====================================================================

basis_t curve_t::first_segment() const
{
  if( _compound_curve == false ) {
    
    // Easy, jsut return the firs basis
    return _data[0];
  } else {
  
    // Return the first segment of the first curve
    return _curves[0].first_segment();
  }
}

//=====================================================================

curve_t curve_t::on( const manifold_tp& man ) const
{
  curve_t copy( *this );
  copy.rereference( man );
  return copy;
}

//=====================================================================

curve_t curve_t::lift( const manifold_tp& man ) const
{
  // Simply lift each sub-curve or basis and return new curve_t object
  if( _compound_curve ) {
    std::vector<curve_t> curves;
    for( size_t i = 0; i < _curves.size(); ++i ) {
      curves.push_back( _curves[i].lift( man ) );
    }
    return curve_t( curves );
  } else {
    std::vector<basis_t> basis;
    for( size_t i = 0; i < _data.size(); ++i ) {
      basis.push_back( _data[i].lift( man ) );
    }
    return curve_t( basis );
  }
}

//=====================================================================

void curve_t::rereference( const manifold_tp& man )
{

  // Make sure the given manifold and this one are non null
  if( _manifold == false || man == false ) {
    BOOST_THROW_EXCEPTION( exceptions::null_manifold_error() );
  }

  // First, check if we already have the manifold, and if so
  // we are set
  if( _manifold == man ) {
    return;
  }
  
  
  // Here, we simply call .on( man ) for subcurves we have
  // we have, since this implicitly changes the manifold they are in
  if( _compound_curve ) {
    
    std::vector<curve_t> new_curves;
    for( size_t i = 0; i < _curves.size(); ++i ) {
      new_curves.push_back( _curves[i].on( man ) );
    }
    (*this) = curve_t( new_curves );
  }

  // Ok, we need to check if we are at the base manifold ourselves,
  // in which case we must go down into the wanted manifold's
  // reference then back up using hte inverse bundles
  // Note: at this point, the wanted manifold is NOT the base manifold
  //       otherwise the equality check at the top would have
  //       been true and we would not be here, so weare guaranteed
  //       to have a non-base manifold as our wanted base, which means
  //       is must have a reference manifold
  if( _manifold == manifold_t::BASE_MANIFOLD ) {

    // Ok, get the basis in the reference of the wanted manifold
    std::vector<curve_t> curves_on_ref;
    for( size_t i = 0; i < _data.size(); ++i ) {
      curves_on_ref.push_back( _data[i].on( man->reference() ) );
    }

    // Now that we have the curve in the reference, bring it "up" to
    // the wanted manifold by using hte inverse bundles
    std::vector<curve_t> bundle_curves;
    for( size_t i = 0; i < curves_on_ref.size(); ++i ) {
      
      // Get the reference curves as a set of basis
      std::vector<basis_t> basis_of_ref = curves_on_ref[i].segment(0,1);
      
      // Change each basis of reference into a curve on the manifold
      // using the inverted bundle
      for( size_t k = 0; k < basis_of_ref.size(); ++k ) {
	basis_bundle_t inverted_bundle = man->reference()->inverted_basis_bundle( basis_of_ref[k] );
	bundle_curves.push_back( inverted_bundle._reference_curve );
      }
    }

    // Ok, now just wrap up the curves into a curve object
    (*this) = curve_t( bundle_curves );
    

  } else {

    // Ok, for basis, we need to be a little more specific.
    // Here, we need to go down one level to our reference manifold
    // using ourbundles, which give use curves, we when need to get
    // these curves into the manifold we want and return
    std::vector< curve_t > bundle_curves;
    for( size_t i = 0; i < _data.size(); ++i ) {
      //basis_bundle_t bundle = _manifold->reference()->basis_bundle( _data[i] );
      basis_bundle_t bundle = _manifold->basis_bundle( _data[i] );
      bundle_curves.push_back( bundle._reference_curve.on( man ) );
    }
    (*this) = curve_t( bundle_curves );
  }
  
}

//=====================================================================

coordinate_t ptp::coordinates::get_grounding_origin( const basis_bundle_set_t& bundle )
{
  return bundle[0]._reference_curve.at(0.0);
}

//=====================================================================

manifold_tp manifold_t::reference() const
{
  // Here, the BASE manifold ( or one wihtout a refernce )
  // always returns the BASE manifold as reference
  if( _reference )
    return _reference;
  return manifold_t::BASE_MANIFOLD;
}

//=====================================================================

basis_bundle_t manifold_t::basis_bundle( const basis_t& basis_on_this_manifold ) const
{
  // This is the implementation for BASE manifold only, make sure that
  // we are only called by a base manifold
  if( _reference ) {
    BOOST_THROW_EXCEPTION( exceptions::implementation_error() );
  }

  // Make sure give basis in in fact on this manifold
  if( (*basis_on_this_manifold.manifold()) != (*this) ) {
    BOOST_THROW_EXCEPTION( exceptions::invalid_manifold_error() );
  }

  // Ok, since we are the base manifold, our basis bundle is itself the
  // basis :-)
  return basis_bundle_t( basis_on_this_manifold, curve_t( basis_on_this_manifold ) );
}

//=====================================================================

basis_bundle_t manifold_t::inverted_basis_bundle( const basis_t& basis_on_reference_manifold ) const
{
  // This is the implementation for BASE manifold only, make sure that
  // we are only called by a base manifold
  if( _reference ) {
    BOOST_THROW_EXCEPTION( exceptions::implementation_error() );
  }

  // Make sure give basis in in fact on the reference manifold
  if( _reference && basis_on_reference_manifold.manifold() != _reference ) {
    BOOST_THROW_EXCEPTION( exceptions::invalid_manifold_error() );
  } else if ( _reference == false && (*basis_on_reference_manifold.manifold()) != (*this) ) {
    BOOST_THROW_EXCEPTION( exceptions::invalid_manifold_error() );
  }

  // Since we are the base manifold, the inverted bundle for a basis is the
  // basis itself :-)
  return basis_bundle_t( basis_on_reference_manifold, curve_t( basis_on_reference_manifold ) );
}

//=====================================================================

std::vector<basis_t> manifold_t::create_axis_aligned_basis( const coordinate_t& origin, const float& unit ) const
{
  std::vector<basis_t> axis;
  
  // zero out the coordinate
  coordinate_t zero( origin );
  for( size_t i = 0; i < zero.dimensionality(); ++i ) {
    zero[i] = 0;
  }
  
  // Now, for each dimension, create a 'unit' long dimension verctor 
  // and add to set of axis
  for( size_t i = 0; i < zero.dimensionality(); ++i ) {
    coordinate_t coord( zero );
    coord[i] = unit;
    axis.push_back( basis_t( origin, coord + origin ) );
  }

  return axis;
}

//=====================================================================

basis_bundle_set_t manifold_t::basis_bundle_set( const coordinate_t& point_in_this_manifold ) const
{

  // First, make sure the point is on this manifold
  if( (*point_in_this_manifold._manifold) != (*this) ) {
    BOOST_THROW_EXCEPTION( exceptions::reference_manifolds_differ_error() );
  }

  // Now, get a set of normal basis in this manifold
  std::vector<basis_t> axis = create_axis_aligned_basis( point_in_this_manifold, 1.0 );
  
  // For each basis, get the corresponding bundle and return the set
  basis_bundle_set_t bundle_set;
  for( size_t i = 0; i < axis.size(); ++i ) {
    bundle_set.push_back( this->basis_bundle( axis[i] ) );
  }
  
  return bundle_set;
}

//=====================================================================

basis_bundle_set_t manifold_t::inverted_basis_bundle_set( const coordinate_t& point_in_reference_manifold ) const
{

  // First, make sure the point is on reference manifold
  if( _reference && point_in_reference_manifold._manifold != _reference ) {
    BOOST_THROW_EXCEPTION( exceptions::reference_manifolds_differ_error() );
  } else if( _reference == false && (*point_in_reference_manifold._manifold) != (*this) ) {
    BOOST_THROW_EXCEPTION( exceptions::reference_manifolds_differ_error() );
  }

  // Now, get a set of normal basis in reference manifold
  std::vector<basis_t> ref_axis = create_axis_aligned_basis( point_in_reference_manifold, 1.0 );
  
  // For each basis, get the corresponding bundle and return the set
  basis_bundle_set_t bundle_set;
  for( size_t i = 0; i < ref_axis.size(); ++i ) {
    bundle_set.push_back( this->inverted_basis_bundle( ref_axis[i] ) );
  }
  
  return bundle_set;
}

//=====================================================================

bool manifold_t::operator== ( const manifold_t& a ) const
{
  return _reference == a._reference;
}

//=====================================================================

namespace ptp {
  namespace common {

    //=====================================================================
    
    // The dimensionality functions for coordiantes etc
    template<>
    size_t dimensionality( const cn::coordinate_t& a ) { return a.dimensionality(); }
    template<>
    size_t dimensionality( const cn::raw_coordinate_t& a ) { return a.dimensionality(); }
    template<>
    size_t dimensionality( const cn::basis_t& a ) { return a.dimensionality(); }
    
    //=====================================================================


  }
}

//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================

// Description:
// IO Operators for sevevral things
namespace ptp {
  namespace coordinates {


    //----------------------------------------------------------------

    std::ostream& operator<< ( std::ostream& os, const raw_coordinate_t& a )
    {
      os << "(";
      for( size_t i = 0; i < a.dimensionality(); ++i ) {
	os << a[i];
	if( i != a.dimensionality() - 1 ) {
	  os << ",";
	}
      }
      os << ")";
      return os;
    }

    //----------------------------------------------------------------

    std::ostream& operator<< ( std::ostream& os, const coordinate_t& a )
    {
      os << a._coordinate << " on " << a.manifold();
      return os;
    }

    //----------------------------------------------------------------

    std::ostream& operator<< ( std::ostream& os, const basis_t& a )
    {
      os << "|" << a._origin._coordinate << " to " << a._direction._coordinate << " on " << a.manifold() << "|";
      return os;
    }

    //----------------------------------------------------------------
    
    std::ostream& operator<< ( std::ostream& os, const curve_t& a )
    {
      if( a._compound_curve )
	os << "[ compound curve ";
      else
	os << "[ curve ";
      if( a._compound_curve ) {
	for( size_t i = 0; i < a._curves.size(); ++i ) {
	  os << a._curves[i];
	  if( i != a._curves.size() - 1 )
	    os << ":";
	}
      } else {
	for( size_t i = 0; i < a._data.size(); ++i ) {
	  os << a._data[i];
	  if( i != a._data.size() - 1 ) {
	    os << ":";
	  }
	}
      }
      os << " on " << a.manifold();
      os << "]";
      return os;
    }
    
    //----------------------------------------------------------------

    std::ostream& operator<< ( std::ostream& os, const basis_bundle_t& a ) 
    {
      os << "{ bundle " << a._embeded_basis << " " << a._reference_curve << "}";
      return os;
    }

    //----------------------------------------------------------------

    std::ostream& operator<< ( std::ostream& os, const manifold_t& a )
    {
      os << a.name();
      return os;
    }
    
    //----------------------------------------------------------------

    std::ostream& operator<< ( std::ostream& os, const manifold_tp& a )
    {
      if( a ) {
	os << (*a);
      }
      os << "@" << std::hex << (unsigned long)((void*)(a.get()));
      os << std::dec;
      return os;
    }

    //----------------------------------------------------------------

    std::ostream& operator<< ( std::ostream& os, const basis_set_t& a )
    {
      os << "[";
      for( size_t i = 0; i < a.size(); ++i ) {
	os << a[i];
	if( i != a.size() - 1 )
	  os << ",";
      }
      os << "]";
      return os;
    }

    //----------------------------------------------------------------

    std::ostream& operator<< ( std::ostream& os, const basis_bundle_set_t& a )
    {
      os << "[";
      for( size_t i = 0; i < a.size(); ++i ) {
	os << a[i];
	if( i != a.size() - 1 )
	  os << ",";
      }
      os << "]";
      return os;
    }

    //----------------------------------------------------------------



  }
}

//=====================================================================

namespace ptp {
  namespace common {

    using namespace ptp::coordinates;
  
    bool float_equal( const coordinate_t& a, const coordinate_t& b, const float& epsilon ) {
      if( a.manifold() != b.manifold() )
	return false;
      return sqrt( raw_coordinate_t::l2_norm( a.raw_coordinate(), b.raw_coordinate() ) ) < epsilon;
    }

    bool float_equal( const basis_t& a, const basis_t& b, const float& eps )
    {
      return 
	float_equal( a.origin(), b.origin() ) &&
	float_equal( a.direction(), b.direction() );
    }

  }
   
}

//=====================================================================

namespace ptp {
  namespace common {

    using namespace ptp::coordinates;
  
    void print( const coordinate_t& a )
    { std::cout << a << std::endl; }
    void print( const raw_coordinate_t& a )
    { std::cout << a << std::endl; }
    void print( const manifold_t&a )
    { std::cout << a << std::endl; }
    void print( const basis_t& a )
    { std::cout << a << std::endl; }
    void print( const curve_t& a )
    { std::cout << a << std::endl; }
    void print( const basis_set_t& a )
    { std::cout << a << std::endl; }
    void print( const basis_bundle_t& a )
    { std::cout << a << std::endl; }
    void print( const basis_bundle_set_t& a )
    { std::cout << a << std::endl; }

  }
 }

//=====================================================================
