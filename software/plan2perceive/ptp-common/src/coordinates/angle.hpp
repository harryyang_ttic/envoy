

#if !defined( __PTP_COORDINATES_angle_HPP__ )
#define __PTP_COORDINATES_angle_HPP__


#include <boost/operators.hpp>
#include <boost/optional.hpp>
#include <boost/shared_ptr.hpp>
#define _USE_MATH_DEFINES
#include <math.h>

namespace ptp {
  namespace coordinates {

    //=======================================================================

    // Description:
    // Forward declarations
    class raw_angle_t;
    class angle_t;


    //=======================================================================

    // Description:
    // An abstract angle that cannot be safely queryed for a 
    // grounded value
    class raw_angle_t
    {
    public:

      // Description:
      // The raw value
      float _value;
      
      // Description:
      // Whether the value is in degrees or radians
      bool _using_radians;
      
      
      // Description:
      // Returns a grounded angle representing this angle from
      // a given reference angle
      angle_t from( const angle_t& ref );

      // Description:
      // convert to/from degrees/radians
      static float rad_to_deg( const float& a );
      static float deg_to_rad( const float& a );

      // Description:
      // Allow use to use protected methods in angle_t
      friend class angle_t;
      friend raw_angle_t deg(const float& a);
      friend raw_angle_t rad(const float& a);

    protected:

      // Description:
      // Returns the value in degress/radians
      float degree_value() const;
      float radian_value() const;
      
      
      // Description:
      // Creates a new raw angle with given value
      raw_angle_t( const float& a, const bool& use_radians )
	: _value(a), _using_radians( use_radians )
      {}

    private:

      // Description:
      // Private constructor, use the deg() and rad() functions to create
      // raw_angle_t objects
      raw_angle_t() { }

    };

     
    //=======================================================================
    
    // Description:
    // Fanctory functions for raw_angle_t
    // These are the main ways of initializing angles
    raw_angle_t deg( const float& a );
    raw_angle_t rad( const float& a );
    

    //=======================================================================

    // Description:
    // A grounded angle value.
    // Computations can be safely done with any other grounded angle
    // values.
    class angle_t 
      : boost::ordered_field_operators< angle_t >
    {

    public:
      
      // Description:
      // Base angle references
      static angle_t X_AXIS;
      

    public:
      
      // Description:
      // The value of the angle
      float _value;
      
      // Description:
      // Whether we are in radians or not
      bool _using_radians;
      
      // Description:
      // The reference angle
      boost::shared_ptr<angle_t> _reference;


      // Description:
      // returns true iff this is a baiss angle
      bool is_basis() const;

      // Description:
      // Returns the reference angle
      boost::shared_ptr<angle_t> reference() const
      { return _reference; }
      
      // Description:
      // Returns a new angle with the given reference angle.
      // The reference of the new angle is that given 
      // and the value will be computed off the reference.
      angle_t from( const angle_t& ref ) const;

      // Description:
      // Changes the reference for the angle, recomputing the
      // value based on the given new reference.
      angle_t rereference( const angle_t& new_ref );

      
      // Description:
      // Returns the value of the angle, in radians or degrees , from the 
      // reference angle for this object
      float degrees() const;
      float radians() const;
      

      // Description:
      // Operators used for angle arithmetic
      angle_t operator+= (const angle_t& a);
      angle_t operator-= (const angle_t& a);
      angle_t operator*= (const angle_t& a);
      angle_t operator/= (const angle_t& a);

      // Description:
      // Comparison operators
      bool operator< (const angle_t& a) const;
      bool operator== (const angle_t& a) const;
      
      
      // Description:
      // Ensure that the angle is centered ( 0 ) around reference
      // ( so it will be between -pi and pi, or -180 and 180 )
      angle_t ensure_zero_centered();

      // Description:
      // Ensure that the angle is positive
      // ( so it will be between 0 and 2pi, or 0 and 360 )
      angle_t ensure_positive();

      // Description:
      // We are friend with the raw_angle_t class
      friend class raw_angle_t;
      friend angle_t operator- ( const angle_t& a );

    protected:

      // Description:
      // Create a new angle with given value and reference
      angle_t( const float& value, const bool& use_radians,
	       const boost::shared_ptr<angle_t>& ref )
	: _value( value ), _using_radians( use_radians ), _reference( ref )
      {}

      // Description:
      // Create a new anglet with *no* refernece.
      // Use this only for BASIS angles
      angle_t( const float& value, const bool& use_radians )
	: _value( value ), _using_radians( use_radians )
      {}

      // Description:
      // ungrounds this angle into a raw_angle_t
      raw_angle_t as_raw_angle() const;
      
      // Description:
      // Adds/Subs the given raw_angle_t to the value of this angle
      // This is "unsafe" since it ignores references ( raw angles
      // are not grounded! )
      void unsafe_add_to_value( const raw_angle_t& a );
      void unsafe_subtract_to_value( const raw_angle_t& a );

    private:

      // Description:
      // Private constructor, use the static basis angles if you want
      // a angle_t, otherwise ground (using .from()) a raw_angle_t
      // using the deg() or rad()
      angle_t() { }

    };

    //=======================================================================

  }
}

#endif

