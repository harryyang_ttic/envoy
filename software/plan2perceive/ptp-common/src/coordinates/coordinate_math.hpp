
#if !defined ( __PTP_COORDINATES_coordinate_math_HPP__ )
#define __PTP_COORDINATES_coordinate_math_HPP__

#include "manifold.hpp"

namespace ptp {
  namespace coordinates {


    //===================================================================

    namespace exceptions {


      // Description:
      // A basis_t is invalid, in general it means that the programmer 
      // gave a function an incorrect basi
      struct invalid_basis_error : public virtual exception_base {};
      
      // Description:
      // The origins of two basis are not the same and they should be
      struct basis_origins_differ_error : public virtual invalid_basis_error {};
    }

    //===================================================================

    // Description:
    // Treats two basis as vector. The originas of the basis must be
    // the same otherwise an exception is thrown.
    // Note: throws exception if these are not 3D coordinates
    basis_t cross( const basis_t& a, const basis_t& b );

    //===================================================================

    // Description:
    // Returns the dot product of the two vectors,
    // the origins of the basis must match
    float dot( const basis_t&a, const basis_t& b );

    //===================================================================


    // Description:
    // Returns a new basis with same origin but a longer/shporter
    // direction coordinate so that the arc length is d
    basis_t normalize_length( const basis_t& a, const float& d );

    //===================================================================


  }
}

#endif

