
#if !defined( __PTP_COORDINATES_matrix_util_HPP__ )
#define __PTP_COORDINATES_marix_util_HPP__

#include <ptp-common/matrix.hpp>
#include "manifold.hpp"
#include "simplex_t.hpp"

namespace ptp {
  namespace coordinates {
    
    using namespace ptp::common;

    //====================================================================

    // Description:
    // Convert To/From raw coordinates and math vector
    // Description:
    math_vector_t math_vector_from_raw_coordinate( const raw_coordinate_t& c );
    raw_coordinate_t raw_coordinate_from_math_vector( const math_vector_t& v );

    //====================================================================

    // Description:
    // Create a matrix out of the coordinates of a given simplex
    // The points are columns in the matrix
    matrix_t create_matrix_from_simplex_points_col( const simplex_tp& simplex );  

    //====================================================================
    
    // Description:
    // Returns a matrix with the given row tiled down N columns
    // or given column tiled across N rows
    matrix_t create_tiled_row_matrix( const matrix_t& m, const int& row );
    matrix_t create_tiled_col_matrix( const matrix_t& m, const int& col );

    //====================================================================

    // Description:
    // Given a math vector, returns the index of the smallest
    // element not less that the given threshold
    int find_min_epsilon_idx( const math_vector_t& a, const float& eps );
    
    //====================================================================

    // Description:
    // Print matrices to standard output
    void print_matrix( const matrix_t& a );
    void print_vector( const math_vector_t& a );

    //====================================================================

    // Descripiton:
    // Save a matrix as a Space-Separeted_Value file
    void save_matrix_as_ssv( const matrix_t& m, const std::string& filename );
    
    //==================================================================
    
  }
}

#endif

