
#include "identity_manifold_t.hpp"

using namespace ptp;
using namespace coordinates;


//=======================================================================

basis_bundle_t identity_manifold_t::basis_bundle( const basis_t& basis_on_this_manifold ) const
{

  // Make sure give basis in in fact on this manifold
  if( (*basis_on_this_manifold.manifold()) != (*this) ) {
    BOOST_THROW_EXCEPTION( exceptions::invalid_manifold_error() );
  }

  // Ok, create the 'same' basis in the reference manifold, since we are
  // the identity manifold we live it it's space ( exactly in the space )
  basis_t basis_on_reference = basis_on_this_manifold.lift( reference() );
  
  // Now, get the basus bundle from reference
  basis_bundle_t bundle_for_ref = reference()->basis_bundle( basis_on_reference );
  
  // Ok, now lift the bundle onto this manifold
  return bundle_for_ref.lift( shared_from_this(), reference() );
  
}


//=======================================================================

basis_bundle_t identity_manifold_t::inverted_basis_bundle( const basis_t& basis_on_reference_manifold ) const
{

  // Make sure give basis in in fact on this manifold
  if( basis_on_reference_manifold.manifold() != reference() ) {
    BOOST_THROW_EXCEPTION( exceptions::invalid_manifold_error() );
  }

  // Ok, create the 'same' basis in the reference manifold, since we are
  // the identity manifold we live it it's space ( exactly in the space )
  basis_t basis_on_reference = basis_on_reference_manifold.lift( reference()->reference() );
  
  // Now, get the basus bundle from reference
  basis_bundle_t inverted_bundle_for_ref = reference()->inverted_basis_bundle( basis_on_reference );
  
  // Ok, now lift the bundle onto this manifold
  return inverted_bundle_for_ref.lift( reference(), shared_from_this() );
  
}

//=======================================================================

basis_bundle_set_t identity_manifold_t::basis_bundle_set( const coordinate_t& point_in_this_manifold ) const
{
 
  // Make sure coordinate is on this manifold
  if( point_in_this_manifold.manifold() != shared_from_this() ) {
    BOOST_THROW_EXCEPTION( exceptions::reference_manifolds_differ_error() );
  }

  // Create same coordinate in the reference manifodl since we are 
  // an identity manifold
  coordinate_t point_on_reference = point_in_this_manifold.lift( reference() );
  
  // Get the basis bundle from reference manifold
  basis_bundle_set_t bundle_set_on_reference = reference()->basis_bundle_set( point_on_reference );
  
  // Create new bundle set with each bundle lifted to this manifold
  basis_bundle_set_t bundle_set;
  for( size_t i = 0; i < bundle_set_on_reference.size(); ++i ) {
    bundle_set.push_back( bundle_set_on_reference[i].lift( shared_from_this(), reference() ) );
  }
  
  return bundle_set;

}

//=======================================================================

basis_bundle_set_t identity_manifold_t::inverted_basis_bundle_set( const coordinate_t& point_in_reference_manifold ) const
{
  
  // make sure point is on reference manifold
  if( point_in_reference_manifold.manifold() != reference() ) {
    BOOST_THROW_EXCEPTION( exceptions::reference_manifolds_differ_error() );
  }

  // Get the point in the reference's reference
  coordinate_t point_in_ref = point_in_reference_manifold.lift( reference()->reference() );
  
  // get the inverted bundle from reference
  basis_bundle_set_t inverted_bundle_on_reference = reference()->inverted_basis_bundle_set( point_in_ref );
  
  // Lift each bundle in set onto this manifold
  basis_bundle_set_t inverted_bundle;
  for( size_t i = 0; i < inverted_bundle_on_reference.size(); ++i ) {
    inverted_bundle.push_back( inverted_bundle_on_reference[i].lift( reference(), shared_from_this() ) );
  }
  
  return inverted_bundle;
}

//=======================================================================

bool identity_manifold_t::operator== ( const identity_manifold_t& a ) const
{
  return reference() == a.reference();
}

//=======================================================================

namespace ptp {
  namespace coordinates {

    std::ostream& operator<< ( std::ostream& os, const identity_manifold_t& a )
    {
      os << "I" << (*a.reference());
      return os;
    }

  }
}

//=======================================================================
