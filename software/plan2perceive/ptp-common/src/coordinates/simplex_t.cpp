
#include "simplex_t.hpp"
#include "coordinate_math.hpp"

using namespace ptp;
using namespace ptp::coordinates;


//==================================================================

simplex_t::simplex_t( const std::vector<coordinate_t>& points, const manifold_tp& man )
{
  init( points, man );
}

//==================================================================

void simplex_t::init( const std::vector<coordinate_t>& points, const manifold_tp& man )
{

  // Check that we have at least 1 points
  assert( points.empty() == false );
  if( points.empty() ) {
    BOOST_THROW_EXCEPTION( exceptions::not_enough_points_error() );
  }

  // Ok, first check that the coordinates are
  // all on the manifold
  for( size_t i = 0; i < points.size(); ++i ) {
    if( points[i].manifold() != man ) {
      BOOST_THROW_EXCEPTION( exceptions::coordinate_not_in_manifold_error() );
    }
  }

  // Ok, now we set the dimensionality of the manifold to be 
  // the number of points - 1
  _dimensionality = points.size() - 1;

  // Store the points and the manifold
  _points = points;
  _manifold = man;
}

//==================================================================

namespace ptp {
  namespace coordinates {

    simplex_tp point_simplex( const raw_coordinate_t& a, const manifold_tp& man )
    {
      std::vector<coordinate_t> d;
      d.push_back( a.on(man) );
      return simplex_tp( new simplex_t( d, man ) );
    }

    simplex_tp line_simplex( const raw_coordinate_t& a, const raw_coordinate_t& b, const manifold_tp& man )
    {
      std::vector<coordinate_t> d;
      d.push_back( a.on(man) );
      d.push_back( b.on(man) );
      return simplex_tp( new simplex_t( d, man ) );
    }

    simplex_tp point_simplex( const raw_coordinate_t& a, const raw_coordinate_t& b, const raw_coordinate_t& c, const manifold_tp& man )
    {
      std::vector<coordinate_t> d;
      d.push_back( a.on(man) );
      d.push_back( b.on(man) );
      d.push_back( c.on(man) );      
      return simplex_tp( new simplex_t( d, man ) );
    }

  }
}

//==================================================================

namespace ptp {
  namespace coordinates {

    std::ostream& operator<< ( std::ostream& os, const simplex_t& a )
    {
      os << "s(" << a.points().size() - 1 << "," << a.dimensionality() << ")";
      os << "<";
      for( size_t i = 0; i < a.points().size(); ++i )  {
      	os << a.points()[i];
      	if( i != a.points().size() - 1 ) 
      	  os << ",";
      }
      os << ">";
      return os;
    }
   
  }
}

//==================================================================

namespace ptp {
  namespace coordinates {

    basis_t basis_from_simplex( const simplex_tp& a )
    {
      // make sure the simplex is a line
      if( a->dimensionality() != 1 ) {
	BOOST_THROW_EXCEPTION( exceptions::invalid_dimensionality_error() );
      }
      
      return basis_t( a->points()[0], a->points()[1] );
    }

    simplex_tp simplex_from_basis( const basis_t& a ) 
    {
      return line_simplex( a.origin().raw_coordinate(),
			   a.direction().raw_coordinate(),
			   a.manifold() );
    }
 
  }
}

//==================================================================

namespace ptp {
  namespace common {

    using namespace ptp::coordinates;

    bool float_equal( const simplex_tp& a, const simplex_tp& b, const float& eps )
    {
      if( a->manifold() != b->manifold() )
	return false;
      if( a->dimensionality() != b->dimensionality() )
	return false;
      if( a->points().size() != b->points().size() )
	return false;
      for( size_t i = 0; i < a->points().size(); ++i ) {
	if( float_equal( a->points()[i], b->points()[i], eps ) == false )
	  return false;
      }
      return true;
    }
  
  }
}

//==================================================================
//==================================================================
