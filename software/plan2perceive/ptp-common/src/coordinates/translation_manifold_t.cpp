
#include "translation_manifold_t.hpp"

using namespace ptp;
using namespace coordinates;

//========================================================================

translation_manifold_t::translation_manifold_t( const coordinate_t& t,
						const manifold_tp& ref,
						const std::string& name )
  : _translation( t )
{
  this->_reference = ref;
  this->_name = name;
}

//========================================================================

translation_manifold_t::translation_manifold_t( const coordinate_t& t,
						const manifold_tp& ref )
  : _translation( t )
{
  this->_reference = ref;
  this->_name = "t." + ref->name();
}

//========================================================================

basis_bundle_t translation_manifold_t::basis_bundle( const basis_t& basis_on_this_manifold ) const
{
  // First check that the basis given is on this manifold
  if( basis_on_this_manifold.manifold() != shared_from_this() )
    BOOST_THROW_EXCEPTION( exceptions::coordinate_not_in_manifold_error() );
  
  // Ok, we are just a translation of the reference, so to go back to
  // the refenrece we simply add our translation
  // Since we are the same dimensionality as reference, 'lift' the given basis
  // onto the reference, then subtract
  basis_t translated_basis_on_ref = basis_on_this_manifold.lift( reference() );
  basis_t basis_on_ref = basis_t( translated_basis_on_ref.origin() + _translation,
				  translated_basis_on_ref.direction() + _translation );
  
  // Return a new undle
  return basis_bundle_t( basis_on_this_manifold,
			 curve_t( basis_on_ref ) );
}

//========================================================================

basis_bundle_t translation_manifold_t::inverted_basis_bundle( const basis_t& basis_on_reference_manifold ) const
{
  // Check the the basis is on the refenrence
  if( basis_on_reference_manifold.manifold() != reference() )
    BOOST_THROW_EXCEPTION( exceptions::coordinate_not_in_manifold_error() );
  
  // Since we are just a translation of basis, we just subtract our translation
  // vector and then 'lift' the coordinate onto ourselves
  basis_t basis_on_this = 
    basis_t( basis_on_reference_manifold.origin() - _translation,
	     basis_on_reference_manifold.direction() - _translation ).lift( shared_from_this() );
  
  // Retursn a bundle
  return basis_bundle_t( basis_on_reference_manifold,
			 curve_t( basis_on_this ) );
}

//========================================================================

namespace ptp {
  namespace coordinates {

    std::ostream& operator<< ( std::ostream& os, const translation_manifold_t& t ) 
    {
      os << "T::" << t._translation;
      return os;
    }
    
  }
}

//========================================================================

namespace ptp {
  namespace coordinates {

    translation_manifold_tp translate( const coordinate_t& t,
				       const manifold_tp& base )
    {
      return translation_manifold_tp( new translation_manifold_t( t, base ) );
    }

  }
}

//========================================================================
//========================================================================
//========================================================================
//========================================================================
//========================================================================
//========================================================================
