
#if !defined( __PTP_COMMON_test_HPP__ )
#define __PTP_COMMON_test_HPP__


#include <boost/exception/all.hpp>

// Description:
// Useful macros and functions for a small test framework


//=====================================================================

// Description:
// Test macros for building up tests and running them

#define run_test(x) do { int res = x; if(res) { std::cout << "FAILED: " #x << std::endl; } } while(0)

#define start_of_test					\
  int result = 0;					\
  try { 

#define end_of_test						\
  } catch (boost::exception& e) {				\
    std::cout << "    TestEX: " <<				\
      boost::diagnostic_information(e) << std::endl;		\
    result = 1;							\
  } catch (...) {						\
    result = 1;							\
  }								\
  return result;

#define test_assert(x) do {						\
    try {								\
      if( (x) == false )						\
	{								\
	  std::cout << "  boink: " #x << std::endl;			\
	  result = 1;							\
	}								\
    } catch (boost::exception& e ) {					\
      std::cout << "  boink: " #x << std::endl;				\
      std::cout << "    caught exception: " <<				\
	boost::diagnostic_information(e) << std::endl;			\
      result = 1;							\
    }									\
  } while(0)

#define test_exception(x, exception)					\
  do {									\
    bool exception_thrown = false;					\
    try {								\
      x;								\
    } catch ( exception & e ) {						\
      exception_thrown = true;						\
    } catch ( ... ) {							\
      result = 1;							\
      std::cout << "  boink: "						\
		<< boost::current_exception_diagnostic_information()	\
		<< std::endl;						\
    }									\
    if( exception_thrown == false ) {					\
      std::cout << "  boink: expected exception '"			\
		<< #exception << "' not thrown"				\
		<< std::endl;						\
      result = 1;							\
    }									\
  } while(0)



#endif

