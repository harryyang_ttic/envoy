
#if !defined( __PTP_COMMON_FRAMEWORK_globals_HPP__ )
#define __PTP_COMMON_FRAMEWORK_globals_HPP__


#include <ptp-common/common.hpp>
#include <boost/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/filesystem.hpp>
#include "streams.hpp"


namespace ptp {
  namespace common {
    namespace framework {

      //==================================================================
      
      namespace exceptions {	
	
      }

      //==================================================================

      // Description:
      // Returns a boost PropertyTree representing the entire configuration
      boost::property_tree::ptree& ptp_config();

      //==================================================================

      // Description:
      // Fetch a configuration value.
      // Throws exception if the key does not exist
      template <class ValueT>
      ValueT ptp_config( const std::string& key )
      {
	return ptp_config().get<ValueT>( key );
      }

      //==================================================================

      // Description:
      // Fetch a configuration value, with default if not found
      template< class ValueT >
      ValueT ptp_config( const std::string& key, const ValueT& def )
      {
	return ptp_config().get<ValueT>( key, def );
      }

      //==================================================================

      // Description:
      // Fetch a configuration value.
      // Returns a boost::optional with result so will not throw
      // exception if key not found.
      template< class ValueT >
      boost::optional<ValueT> ptp_config_optional( const std::string& key )
      {
	return ptp_config().get_optional<ValueT>( key );
      }

      //==================================================================

      // Description:
      // Returns a stream for use with the ptp framework.
      // Each stream has a name that maps to a file.
      // Use this for *ALL* file stream you want to create/use!!
      ostream ptp_output_file_stream( const std::string& name );
      istream ptp_input_file_stream( const std::string& name );
      ostream ptp_analysis_file_stream( const std::string& name );
      ostream ptp_debug_file_stream( const std::string& name );
      
      // Description:
      // Returns a filename for an output file for the script.
      // Use this API for *any* files you want
      boost::filesystem::path
      ptp_output_filename( const std::string& name );
      boost::filesystem::path
      ptp_input_filename( const std::string& name );
      boost::filesystem::path
      ptp_analysis_filename( const std::string& name );
      boost::filesystem::path 
      ptp_debug_filename( const std::string& name );
      

      //==================================================================
      

    }
  }
}

#endif

