
#include "init.hpp"
#include <boost/program_options.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <iostream>

using namespace ptp;
using namespace ptp::common;
using namespace ptp::common::framework;
using namespace boost;
using namespace boost::property_tree::xml_parser;
using boost::property_tree::ptree;
namespace po = boost::program_options;


//=======================================================================

namespace ptp {  
  namespace common { 
    namespace framework {
      
      void ptp_init( int argc, char** argv )
      {
	
	// setup the program options for the ptp framework
	po::options_description desc("ptp framework options");
	desc.add_options()
	  ( "help", "produce help message.")
	  ( "config,c", po::value<std::string>(), "path to ptp configuration file");
	
	// Parse the command line, allowing for unrecognized options which will
	// be used as overwrites for the config file
	po::parsed_options parsed = 
	  po::command_line_parser(argc,argv).options(desc).allow_unregistered().run();
	
	// get a mapping of the options
	po::variables_map vm;
	po::store( parsed, vm );
	po::notify(vm);
	
	// handle help
	if( vm.count("help") ) {
	  std::cout << desc << std::endl;
	  exit(1);
	}
	
	// Now, open the configuration file and read into our ptp_config
	// property_tree
	if( vm.count( "config") ) {
	  read_xml( vm["config"].as<std::string>(), ptp_config() );

	  // add a self reference to the config file inside hte config
	  ptp_config().put( "ptp:config.ptp:self-pathname", vm["config"].as<std::string>() );
	}
	
	// Ok, now update the config with each unrecognized command line option
	for( size_t i = 0; i < parsed.options.size(); ++i ) {

	  //std::cout << "processing option: " << parsed.options[i].string_key << std::endl;

	  if( parsed.options[i].unregistered == false )
	    continue;
	  
	  // make sure we have a string key
	  // and that we have a value associated with it
	  if( parsed.options[i].string_key.size() == 0 &&
	      i+1 < parsed.options.size() ) {
	    // TODO: add the parse index for option into the exception
	    BOOST_THROW_EXCEPTION( framework::exceptions::config_entry_error() );
	  }
	  
	  // put a new key into property map with each value of the option
	  for( size_t j = 0; j < parsed.options[i+1].value.size(); ++j ) {
	    ptp_config().put( parsed.options[i].string_key, parsed.options[i+1].value[j] );
	    //std::cout << "Command line Overwrite: [" << parsed.options[i].string_key << "] = " << parsed.options[i+1].value[j] << std::endl;
	  }
	}
      }
      
    }
  }
}

//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
