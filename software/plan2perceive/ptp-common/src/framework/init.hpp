
#if !defined( __PTP_COMMON_FRAMEWORK_init_HPP__ )
#define __PTP_COMMON_FRAMEWORK_init_HPP__

#include "globals.hpp"

namespace ptp {
  namespace common {
    namespace framework {

      //==================================================================
      
      namespace exceptions {
	
	struct init_error : public virtual exception_base {};

	struct config_entry_error : public virtual init_error {};
	
      }

      //==================================================================
      
      // Description:
      // Initializes the Plan2Perceive (PTP) framework, taking in the command 
      // line arguments to the program.
      //
      // The main argument is the path to a config file.  However, the
      // framwork allows you to override *all* config file variables by
      // command line, to the point that if you do not supply a config file
      // you can just cupply the configuration entries in the command line.
      // This includes multiple values for configuration keys (a-la gcc -I).
      void ptp_init( int argc, char** argv );

    }
  }
}

#endif


