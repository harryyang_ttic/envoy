
#include "globals.hpp"
#include <boost/property_tree/xml_parser.hpp>
#include <fstream>


using namespace ptp;
using namespace ptp::common;
using namespace ptp::common::framework;
using namespace boost;
using namespace boost::property_tree::xml_parser;
namespace fs = boost::filesystem;
using boost::property_tree::ptree;



//=======================================================================

// Description:
// Global property tree with configuration for ptp framework.
// Eventually this may not be global, but for right now it is.
static ptree g_ptp_config;

//=======================================================================

namespace ptp {
  namespace common {
    namespace framework {

      //=======================================================================

      ptree& ptp_config()
      {
	return g_ptp_config;
      }

      //=======================================================================
      
      ostream ptp_output_file_stream( const std::string& name )
      { 
	fs::path path = ptp_output_filename( name );
	ptp_config().add( "ptp:config.ptp:trial.ptp:output-pathnames.ptp:output-pathname", path.string());
	return ostream( new std::ofstream( path.c_str() ) );
      }

      //=======================================================================

      istream ptp_input_file_stream( const std::string& name )
      {
	fs::path path = ptp_input_filename( name );
	ptp_config().add( "ptp:config.ptp:trial.ptp:input-pathnames.ptp:input-pathname", path.string());
	return istream( new std::ifstream( path.c_str() ) );
      }

      //=======================================================================

      ostream ptp_analysis_file_stream( const std::string& name )
      {
	fs::path path = ptp_analysis_filename( name );
	ptp_config().add("ptp:config.ptp:trial.ptp:analysis-pathnames.ptp:analysis-pathname", path.string());
	return ostream( new std::ofstream( path.c_str() ) );
      }

      //=======================================================================

      ostream ptp_debug_file_stream( const std::string& name )
      {
	fs::path path = ptp_debug_filename( name );
	ptp_config().add( "ptp:config.ptp:trial.ptp:output-pathnames.ptp:output-pathname", path.string());
	return ostream( new std::ofstream( path.c_str() ) );
      }

      //=======================================================================

      fs::path ptp_output_filename( const std::string& name )
      {
	return fs::path(ptp_config<std::string>( "ptp:config.ptp:trial.ptp:output-data-path", ".") 
			+ "/" +
			name);
      }

      //=======================================================================

      fs::path ptp_input_filename( const std::string& name )
      {
	return fs::path(ptp_config<std::string>( "ptp:config.ptp:trial.ptp:input-data-path", "." ) + "/" +
			name);
      }

      //=======================================================================

      fs::path ptp_analysis_filename( const std::string& name )
      {
	return fs::path(ptp_config<std::string>( "ptp:config.ptp:trial.ptp:analysis-data-path", "." )  + "/" +
			name);
      }

      //=======================================================================

      fs::path ptp_debug_filename( const std::string& name )
      {
	return fs::path(ptp_config<std::string>( "ptp:config.ptp:trial.ptp:output-data-path", "." ) + "/" +
			name);
      }
      
      //=======================================================================


    }
  }
}

//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
//=======================================================================
