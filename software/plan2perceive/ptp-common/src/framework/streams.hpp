
#if !defined( __PTP_COMMON_streams_HPP__ )
#define __PTP_COMMON_streams_HPP__


#include <ptp-common/common.hpp>

namespace ptp {
  namespace common {


    //====================================================================

    // Description:
    // This is a simple class which represents an std::istream
    // but can be moved around.
    // It internally keeps a shared pointer to the std::istream.
    class istream
    {
    public:

      // Description:
      // Creates a new stream from an std::stream pointer
      istream( std::istream* s )
	: _inner( s )
      {}
      istream( const istream&s )
	: _inner( s._inner )
      {}
      istream( const boost::shared_ptr<std::istream>& s )
	: _inner( s )
      {}

      // Description:
      // Returns the inner stream pointer;
      boost::shared_ptr<std::istream> inner() const {
	return _inner;
      }
      boost::shared_ptr<std::istream> inner() {
	return _inner;
      }

    protected:
      
      boost::shared_ptr< std::istream> _inner;

    };


    template< class T >
    istream& operator>> ( istream& s, T& x )
    {
      (*s.inner()) >> x;
      return s;
    }

    //=====================================================================

    // Description:
    // This is a simple class which represents an std::ostream
    // but can be moved around.
    // It internally keeps a shared pointer to the std::ostream.
    class ostream
    {
    public:

      // Description:
      // Creates a new stream from an std::stream pointer
      ostream( std::ostream* s )
	: _inner( s )
      {}
      ostream( const ostream&s )
	: _inner( s._inner )
      {}
      ostream( const boost::shared_ptr<std::ostream>& s )
	: _inner( s )
      {}

      // Description:
      // Returns the inner stream pointer;
      boost::shared_ptr<std::ostream> inner() const {
	return _inner;
      }
      boost::shared_ptr<std::ostream> inner() {
	return _inner;
      }

    protected:
      
      boost::shared_ptr< std::ostream> _inner;

    };


    template< class T >
    ostream& operator<< ( ostream& s, const  T& x )
    {
      (*s.inner()) << x;
      return s;
    }


  }
}


#endif

