
#include "matrix.hpp"
#include <boost/functional/hash.hpp>


namespace ptp {
  namespace common {

    template< >
    size_t dimensionality( const math_vector_t& a )
    {
      return a.size();
    }

    bool operator> ( const realf_t& a, const int& b ) {
      return a._value > b;
    }

    size_t hash_value( const math_vector_t& a ) 
    {
      size_t seed = 0;
      for( size_t i = 0; i < a.size(); ++i ) {
	boost::hash_combine( seed, a(i) );
      }
      return seed;
    }

    size_t hash_value( const matrix_t& a ) 
    {
      size_t seed = 0;
      for( size_t i = 0; i < a.rows(); ++i ) {
	for( size_t j = 0; j < a.cols(); ++j ) {
	  boost::hash_combine( seed, a(i,j) );
	}
      }
      return seed;
    }


  }
}
