
#if !defined( __PTP_COMMON_reals_HPP__ )
#define __PTP_COMMON_reals_HPP__

#include <boost/operators.hpp>
#include <math.h>

namespace ptp {
  namespace common {

    //=================================================================

    // Description:
    // A number which has a policy for capping around zero
    template< class T, int EPSILON_EXPONENT >
    class policy_number_t
      : boost::ordered_field_operators<policy_number_t<T,EPSILON_EXPONENT> >,
      boost::ordered_field_operators<policy_number_t<T,EPSILON_EXPONENT>, T >
    {
    public:

      // Description:
      // A typedef for self for typing convenience
      typedef policy_number_t<T,EPSILON_EXPONENT> self_t;

      // Description:
      // The given exponenet
      static const int EPSILON_EXP = EPSILON_EXPONENT;

      // Description:
      // The type for the value
      typedef T value_type;

      // Description:
      // The number itself
      T _value;
      
      // Description:
      // The epsilon, this is a class specific number
      static T EPSILON;
      
      // Description:
      // Create a policy number from a regular T
      policy_number_t( const T& v )
	: _value(v)
      {
	apply_policy();
      }
      
      // Description:
      // Create a default policy number
      policy_number_t() 
	: _value()
      {
	apply_policy();
      }


      // Description:
      // Copy constructor for all policy numbers
      template< class T2, int E >
      policy_number_t( const policy_number_t<T2,E>& a )
	: _value( a._value )
      {
	apply_policy();
      }

      // Description:
      // Cast between this and T values
      operator T () 
      {
	return _value;
      }
      operator T () const
      {
	return _value;
      }

      // Description:
      // Assignemtn operator from T
      self_t& operator= ( const T& a ) {
	_value = a;
	apply_policy();
	return *this;
      }
      template< class T2, int E >
      self_t& operator= ( const policy_number_t<T2,E>& a ) {
	_value = a._value;
	apply_policy();
	return *this;
      }
	

      // Description:
      // All the usual arithmatic applies
      self_t operator+= ( const self_t& a ) {
	_value += a._value;
	apply_policy();
	return *this;
      }
      self_t operator-= ( const self_t& a ) {
	_value -= a._value;
	apply_policy();
	return *this;
      }
      self_t operator*= ( const self_t& a ) {
	_value *= a._value;
	apply_policy();
	return *this;
      }
      self_t operator/= ( const self_t& a ) {
	_value /= a._value;
	apply_policy();
	return *this;
      }
      self_t operator+= ( const T& a ) {
	_value += a;
	apply_policy();
	return *this;
      }
      self_t operator-= ( const T& a ) {
	_value -= a;
	apply_policy();
	return *this;
      }
      self_t operator*= ( const T& a ) {
	_value *= a;
	apply_policy();
	return *this;
      }
      self_t operator/= ( const T& a ) {
	_value /= a;
	apply_policy();
	return *this;
      }


      // Description:
      // Comparison operators
      bool operator== ( const self_t& a ) const
      {
	return _value == a._value;
      }
      bool operator< ( const self_t& a ) const 
      {
	return _value < a._value;
      }
      bool operator== ( const T& a ) const
      {
	return _value == a;
      }
      bool operator< ( const T& a ) const
      {
	return _value < a;
      }



    protected:

      // Description:
      // This applies the policy for this number to the current
      // value in this number, changing it according to the policy
      // For now, we simple cap the number down to zero if
      // it's absolute value is smaller that the given epsilon
      void apply_policy()
      {
	if( _value < EPSILON && _value > -EPSILON ) {
	  _value = 0;
	}
      }

    };

    //=================================================================

    // Description:
    // Useful type instantiations of policy numbers
    typedef policy_number_t<float ,-8 > realf_t;
    typedef policy_number_t<double,-16> reald_t;
    
    //=================================================================

    template< class T, int E > 
    typename policy_number_t<T,E>::value_type policy_number_t<T,E>::EPSILON = pow( 10, policy_number_t<T,E>::EPSILON_EXP );
    
    //=================================================================

  }
}

#endif

