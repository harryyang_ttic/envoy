
#include "probability.hpp"


using namespace ptp;
using namespace ptp::common;


//=========================================================================

// Description:
// This macro is used within the raw probability classes to handle
// basic exception when dealing with probability and binary operators
//
// Here, we take in the two operands and the operator as well as
// a predicate thunk for checking for an identity operation
// We check that the operation changes teh values ( else we are too
// close to zero ) and the the probability result is still within the
// 0-1 range
//
// Note: the operators are assumed to be of the op= variety with the
//       first operand having the resulting value of the computation
//
// Note: the operands are assumed to be floats
#define raw_assigning_operator_with_basic_checks( a, b, op, ident_thunk ) \
  do {									\
    try {								\
      float old_a = (a);						\
      float old_b = (b);						\
      (a) op (b);							\
      float new_a = (a);						\
      if( (ident_thunk) == false ) {					\
	if( new_a == old_a ) {						\
	  BOOST_THROW_EXCEPTION( exceptions::probability_is_too_small_error() ); \
	}								\
      }									\
      if( new_a < 0 ) {							\
	BOOST_THROW_EXCEPTION( exceptions::probability_is_negative_error() ); \
      }									\
      if( new_a > 1 ) {							\
	BOOST_THROW_EXCEPTION( exceptions::probability_is_greater_than_one_error() ); \
      }									\
    } catch ( boost::exception& e ) {					\
      e << exceptions::probability_t_info( probability_t(*this) );	\
      throw;								\
    }									\
  } while(0)



//=========================================================================

// Description:
// This macro is used within the raw log probability classes to handle
// basic exception when dealing with probability and binary operators
//
// Here, we take in the two operands and the operator as well as
// a predicate thunk for checking for an identity operation
// We check that the operation changes teh values ( else we are too
// close to zero ) and the the probability result is still within the
// 0-1 range
//
// Note: the operators are assumed to be of the op= variety with the
//       first operand having the resulting value of the computation
//
// Note: the operands are assumed to be floats
#define raw_log_assigning_operator_with_basic_checks( a, b, op, ident_thunk ) \
  do {									\
    try {								\
      float old_a = (a);						\
      float old_b = (b);						\
      (a) op (b);							\
      float new_a = (a);						\
      if( (ident_thunk) == false ) {					\
	if( new_a == old_a ) {						\
	  BOOST_THROW_EXCEPTION( exceptions::probability_is_too_small_error() ); \
	}								\
      }									\
      if( new_a > 0 ) {							\
	BOOST_THROW_EXCEPTION( exceptions::probability_is_greater_than_one_error() ); \
      }									\
    } catch ( boost::exception& e ) {					\
      e << exceptions::probability_t_info( probability_t(*this) );	\
      throw;								\
    }									\
  } while(0)


//=========================================================================

// Description:
// Performs an assignment operation for a probability expression.
// Takes care of checking for too-small values
//
// Note: the operators are assumed to be of the op= variety with the
//       first operand having the resulting value of the computation
//
// Note: the operands are assumed to be doubles
#define expression_assigning_operator_with_basic_checks( a, b, op, ident_thunk ) \
  do {									\
    try {								\
      double old_a = (a);						\
      double old_b = (b);						\
      (a) op (b);							\
      double new_a = (a);						\
      if( (ident_thunk) == false ) {					\
	if( new_a == old_a ) {						\
	  BOOST_THROW_EXCEPTION( exceptions::probability_is_too_small_error() ); \
	}								\
      }									\
    } catch ( boost::exception& e ) {					\
      e << exceptions::probability_t_info( probability_t(*this) );	\
      throw;								\
    }									\
  } while(0)


//=========================================================================


// Description:
// Creates an expression from a probability value
probability_expression_t::probability_expression_t( const raw_probability_t& p )
  : _value( p.as_float() ), _using_log_space( false )
{
}

probability_expression_t::probability_expression_t( const raw_log_probability_t& p )
  : _value( p.as_float() ), _using_log_space( true )
{
}

probability_expression_t::probability_expression_t( const probability_t& p )
  : _using_log_space( p._using_log_space )
{
  if( _using_log_space ) {
    _value = p.get_raw_log_probability_t().as_float();
  } else {
    _value = p.get_raw_probability_t().as_float();
  }
}


// Description:
// Automatically cast into a probability
probability_expression_t::operator raw_probability_t()
{
  return probability_t( _value, _using_log_space ).get_raw_probability_t();
}

probability_expression_t::operator raw_log_probability_t()
{
  return probability_t( _value, _using_log_space ).get_raw_log_probability_t();
}

probability_expression_t::operator probability_t()
{
  return probability_t( _value, _using_log_space );
}


// Description:
// Operators need for arithmetic
probability_expression_t 
probability_expression_t::operator+= (const probability_expression_t& p )
{
  // take the space of this as the space to work in
  if( !this->_using_log_space ) {
    expression_assigning_operator_with_basic_checks( _value,
						     p.get_normal_value(),
						     +=,
						     p.get_normal_value() == 0.0 );
  } else {
    
    // Ok, we're going to use the following log identity:
    //  log( a + c ) = log(a) + log( 1 + b^(log(c) - log(a)) )
    // where b is the base of the logs
    //
    // Now, if a > c, then we get:
    //  log( a + c ) = log(a) + log( 1 + (c / a) )
    float log_a = _value;
    float log_c = p.get_log_value();
    //_value = ( log_a + log( 1.0f + exp( log_c - log_a ) ) );
    expression_assigning_operator_with_basic_checks( _value,
						     ( log_a + log( 1.0f + exp( log_c - log_a ) ) ),
						     =,
						     p.get_normal_value() == 0.0);
    
  }
  return *this;
}

probability_expression_t 
probability_expression_t::operator-= (const probability_expression_t& p )
{
  // take the space of this as the space to work in
  if( !this->_using_log_space ) {
    expression_assigning_operator_with_basic_checks( _value,
						     p.get_normal_value(),
						     -=,
						     p.get_normal_value() == 0.0 );
  } else {
    
    // Ok, we're going to use the following log identity:
    //  log( a + c ) = log(a) + log( 1 - b^(log(c) - log(a)) )
    // where b is the base of the logs
    //
    // Now, if a > c, then we get:
    //  log( a + c ) = log(a) + log( 1 - (c / a) )
    float log_a = _value;
    float log_c = p.get_log_value();
    //_value = ( log_a + log( 1.0f - exp( log_c - log_a ) ) );
    expression_assigning_operator_with_basic_checks( _value,
						     ( log_a + log( 1.0f - exp( log_c - log_a ) ) ),
						     =,
						     p.get_normal_value() == 0.0);
    
  }
  return *this;
}

probability_expression_t 
probability_expression_t::operator*= (const probability_expression_t& p )
{
  if( !this->_using_log_space ) {
    expression_assigning_operator_with_basic_checks( _value,
						     p.get_normal_value(),
						     *=,
						     p.get_normal_value() == 1.0);
  } else {
    expression_assigning_operator_with_basic_checks( _value,
						     p.get_log_value(),
						     +=,
						     p.get_log_value() == 0.0);
  }
  return *this;
}

probability_expression_t 
probability_expression_t::operator/= (const probability_expression_t& p )
{
  if( !this->_using_log_space ) {
    expression_assigning_operator_with_basic_checks( _value,
						     p.get_normal_value(),
						     /=,
						     p.get_normal_value() == 1.0);
  } else {
    expression_assigning_operator_with_basic_checks( _value,
						     p.get_log_value(),
						     -=,
						     p.get_log_value() == 0.0);
  }
  return *this;
}


probability_expression_t
probability_expression_t::operator+= (const float& v )
{
  expression_assigning_operator_with_basic_checks( _value, v, +=,
						   v == 0.0 );
  return *this;
}

probability_expression_t
probability_expression_t::operator-= (const float& v )
{
  expression_assigning_operator_with_basic_checks( _value, v, -=,
						   v == 0.0 );
  return *this;
}

probability_expression_t
probability_expression_t::operator*= (const float& v )
{
  expression_assigning_operator_with_basic_checks( _value, v, *=,
						   v == 1.0 );
  return *this;
}

probability_expression_t
probability_expression_t::operator/= (const float& v )
{
  expression_assigning_operator_with_basic_checks( _value, v, /=,
						   v == 1.0 );
  return *this;
}


// Description:
// Operators for ordering
bool probability_expression_t::operator< (const probability_expression_t& p ) const
{
  if( !this->_using_log_space ) {
    return _value < p.get_normal_value();
  } else {
    return _value < p.get_log_value();
  }
}

bool probability_expression_t::operator== (const probability_expression_t& p ) const
{
  if( !this->_using_log_space ) {
    return _value == p.get_normal_value();
  } else {
    return _value == p.get_log_value();
  }
}




//=========================================================================

// Description:
// Validates a probability, throws exceptions if not valid
void raw_probability_t::validate()
{
  if( _prob < 0.0f )
    BOOST_THROW_EXCEPTION( exceptions::probability_is_negative_error() );
  if( _prob > 1.0f )
    BOOST_THROW_EXCEPTION( exceptions::probability_is_greater_than_one_error() );
}

//=========================================================================


namespace ptp {
  namespace common {

// Description:
// Derived raw_probability_t operators
probability_expression_t operator+ ( const raw_probability_t& a,
				     const probability_expression_t& b )
{
  probability_expression_t temp(a);
  temp += b;
  return temp;
}
probability_expression_t operator- ( const raw_probability_t& a,
				     const probability_expression_t& b )
{
  probability_expression_t temp(a);
  temp -= b;
  return temp;
}
probability_expression_t operator* ( const raw_probability_t& a,
				     const probability_expression_t& b )
{
  probability_expression_t temp(a);
  temp *= b;
  return temp;
}
probability_expression_t operator/ ( const raw_probability_t& a,
				     const probability_expression_t& b )
{
  probability_expression_t temp(a);
  temp /= b;
  return temp;
}
probability_expression_t operator+ ( const raw_probability_t& a,
				     const float& b )
{
  probability_expression_t temp(a);
  temp += b;
  return temp;
}
probability_expression_t operator- ( const raw_probability_t& a,
				     const float& b )
{
  probability_expression_t temp(a);
  temp -= b;
  return temp;
}
probability_expression_t operator* ( const raw_probability_t& a,
				     const float& b )
{
  probability_expression_t temp(a);
  temp *= b;
  return temp;
}
probability_expression_t operator/ ( const raw_probability_t& a,
				     const float& b )
{
  probability_expression_t temp(a);
  temp /= b;
  return temp;
}

  }
}


//=========================================================================

// Description:
// Validates the log probability. throws exception if not valid
void raw_log_probability_t::validate()
{
  if( _log_prob > 0 )
    BOOST_THROW_EXCEPTION( exceptions::probability_is_greater_than_one_error() );
}


//=========================================================================


namespace ptp {
  namespace common {

// Description:
// Derived raw_log_probability_t operators
probability_expression_t operator+ ( const raw_log_probability_t& a,
				     const probability_expression_t& b )
{
  probability_expression_t temp(a);
  temp += b;
  return temp;
}
probability_expression_t operator- ( const raw_log_probability_t& a,
				     const probability_expression_t& b )
{
  probability_expression_t temp(a);
  temp -= b;
  return temp;
}
probability_expression_t operator* ( const raw_log_probability_t& a,
				     const probability_expression_t& b )
{
  probability_expression_t temp(a);
  temp *= b;
  return temp;
}
probability_expression_t operator/ ( const raw_log_probability_t& a,
				     const probability_expression_t& b )
{
  probability_expression_t temp(a);
  temp /= b;
  return temp;
}
probability_expression_t operator+ ( const raw_log_probability_t& a,
				     const float& b )
{
  probability_expression_t temp(a);
  temp += b;
  return temp;
}
probability_expression_t operator- ( const raw_log_probability_t& a,
				     const float& b )
{
  probability_expression_t temp(a);
  temp -= b;
  return temp;
}
probability_expression_t operator* ( const raw_log_probability_t& a,
				     const float& b )
{
  probability_expression_t temp(a);
  temp *= b;
  return temp;
}
probability_expression_t operator/ ( const raw_log_probability_t& a,
				     const float& b )
{
  probability_expression_t temp(a);
  temp /= b;
  return temp;
}

  }
}


//=========================================================================


namespace ptp {
  namespace common {

// Description:
// Derived probability_t operators
probability_expression_t operator+ ( const probability_t& a,
				     const probability_expression_t& b )
{
  probability_expression_t temp(a);
  temp += b;
  return temp;
}
probability_expression_t operator- ( const probability_t& a,
				     const probability_expression_t& b )
{
  probability_expression_t temp(a);
  temp -= b;
  return temp;
}
probability_expression_t operator* ( const probability_t& a,
				     const probability_expression_t& b )
{
  probability_expression_t temp(a);
  temp *= b;
  return temp;
}
probability_expression_t operator/ ( const probability_t& a,
				     const probability_expression_t& b )
{
  probability_expression_t temp(a);
  temp /= b;
  return temp;
}
probability_expression_t operator+ ( const probability_t& a,
				     const float& b )
{
  probability_expression_t temp(a);
  temp += b;
  return temp;
}
probability_expression_t operator- ( const probability_t& a,
				     const float& b )
{
  probability_expression_t temp(a);
  temp -= b;
  return temp;
}
probability_expression_t operator* ( const probability_t& a,
				     const float& b )
{
  probability_expression_t temp(a);
  temp *= b;
  return temp;
}
probability_expression_t operator/ ( const probability_t& a,
				     const float& b )
{
  probability_expression_t temp(a);
  temp /= b;
  return temp;
}

  }
}


//=========================================================================


namespace ptp {
  namespace common {
    
    // Description:
    // Output operator implementation
    std::ostream& operator<< (std::ostream& os, const raw_probability_t& p )
    {
      return ( os << p._prob );
    }
    
    // Description:
    // Output operator implementation
    std::ostream& operator<< (std::ostream& os, const raw_log_probability_t& lp )
    {
      return ( os << lp._log_prob );
    }
    
    // Description:
    // Prints an probability to output stream
    std::ostream& operator<< (std::ostream& os, const probability_t& p )
    {
      os << "(" << p.get_raw_probability_t() << "," << p.get_raw_log_probability_t() << ")";
      return os;
    }

    // Description:
    // Prints an probability expression to output stream
    std::ostream& operator<< (std::ostream& os, const probability_expression_t& p )
    {
      if( p._using_log_space ) {
	os << "L";
      }
      os << p._value;
      return os;
    }

  }
}

//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
//=========================================================================
