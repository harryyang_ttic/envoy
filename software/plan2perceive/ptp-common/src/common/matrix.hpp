
#if !defined( __PTP_COMMON_matrix_HPP__ )
#define __PTP_COMMON_matrix_HPP__

#include <eigen3/Eigen/Dense>
#include "reals.hpp"
#include "common.hpp"
#include "dimensionality.hpp"

namespace ptp {
  namespace common {

    //====================================================================

    // Description:
    // Exceptions used by matrices
    namespace exceptions {

      // Description:
      // Failed to invert a matrix
      struct matrix_invert_error : public virtual exception_base {};
      
    }


    //====================================================================

    // Description:
    // Bring in the eigne namespace
    using namespace Eigen;

    //====================================================================
    
    // Description:
    // The matrix type, using ublas matrix
    typedef Matrix<double,Eigen::Dynamic,Eigen::Dynamic> matrix_t;
    
    // Description:
    // Smart pointer to a matrix
    typedef boost::shared_ptr<matrix_t> matrix_tp;


    //====================================================================

    // Description:
    // A mathematical vector type
    typedef Matrix<double, Eigen::Dynamic, 1> math_vector_t;
    typedef boost::shared_ptr<math_vector_t> math_vector_tp;

    //====================================================================

    // Description:
    // The dimensionality of vector types ( and matrix types )
    template<>
    size_t dimensionality( const math_vector_t& a );

    //====================================================================

    // Description:
    // A hash function for matrices and vectors
    size_t hash_value( const math_vector_t& a ) ;
    size_t hash_value( const matrix_t& a );

    //====================================================================

    
  }
}


//====================================================================

// Description:
// Things needed for a new scalar type by Eigne    
namespace Eigen {
  using namespace ptp;
  using namespace ptp::common;

  template<> struct NumTraits<realf_t> 
  {
    typedef realf_t Real;
    typedef realf_t NonInteger;
    typedef realf_t Nested;
    enum {
      IsComplex = 0,
      IsInteger = 0,
      IsSigned = 1,
      RequireInitialization = 0,
      ReadCost = 1,
      AddCost = 1,
      MulCost = 1
    };

    static realf_t epsilon() { return realf_t::EPSILON; }
    static realf_t dummy_precision() { return realf_t::EPSILON; }
    static realf_t highest() { return NumTraits<float>::highest(); }
    static realf_t lowest() { return NumTraits<float>::lowest(); }
  };
}


// Description:
// Internal Functions required for Eigne for scalar types
namespace ptp {
  namespace common {


    // Description:
    // These operators must be EXPLICITLY created for Eigen to work
    // .... grrrr these should not behere and should be auto picked up
    bool operator> ( const realf_t& a, const int& b );


    //namespace Eigen {
  namespace internal {

    namespace pc = ptp::common;

    inline pc::realf_t conj(const pc::realf_t& x)  { return x; }
    inline pc::realf_t real(const pc::realf_t& x)  { return x; }
    inline pc::realf_t imag(const pc::realf_t&)    { return 0.; }
    inline pc::realf_t abs(const pc::realf_t&  x)  { return fabs(x._value); }
    inline pc::realf_t abs2(const pc::realf_t& x)  { return x*x; }
    inline pc::realf_t sqrt(const pc::realf_t& x)  { return sqrt(x._value); }
    inline pc::realf_t exp(const pc::realf_t&  x)  { return exp(x._value); }
    inline pc::realf_t log(const pc::realf_t&  x)  { return log(x._value); }
    inline pc::realf_t sin(const pc::realf_t&  x)  { return sin(x._value); }
    inline pc::realf_t cos(const pc::realf_t&  x)  { return cos(x._value); }
    inline pc::realf_t pow(const pc::realf_t& x, pc::realf_t y)  { return pow(x._value, y._value); }

  }
    //}

  }
}

//====================================================================

#endif

