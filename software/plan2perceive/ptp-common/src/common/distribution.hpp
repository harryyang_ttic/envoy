
#if !defined( __PTP_COMMON_distribution_HPP__ )
#define __PTP_COMMON_distribution_HPP__


#include "common.hpp"
#include "dimensionality.hpp"
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>



namespace ptp {
  namespace common {

    //==================================================================

    // Description:
    // The traits of a distibution.
    // Most udeful for the range of elements type
    template<class D>
    struct distribution_traits
    {
      typedef typename D::domain_type_t domain_type_t;
      typedef domain_type_t mean_type_t;
      typedef double variance_type_t;
    };

    //==================================================================

    // Description:
    // A distribution over elements of a certain type
    template<class X>
    class distribution_t
    {
    public:

      typedef X domain_type_t;
      
      // Description:
      // Returns the probability of given X
      virtual probability_t operator() ( const X& x ) const = 0;
      probability_t p( const X& x ) const
      { return this->operator()( x ); }

      
      // Description:
      // Returns an element sampled from this distribution
      virtual X sample() const = 0;
    };

    //==================================================================

    // Description:
    // Computes the mean of a distributiuon
    template< class D>
    typename distribution_traits<D>::mean_type_t
    mean( const boost::shared_ptr<D>& dist )
    {
      BOOST_STATIC_ASSERT_MSG( sizeof(D) == 0,
			       "mean not implemented for thsi distribution");
    }

    //==================================================================

    // Description:
    // Returns the global GSL rng structure
    gsl_rng* static_distributions_gsl_rng();

    //==================================================================

    

  }
}

#endif

