
#if !defined( __PTP_COMMON_dimensionality_HPP__ )
#define __PTP_COMMON_dimensionality_HPP__

#include "common.hpp"

namespace ptp {
  namespace common {

    //==================================================================

    namespace exceptions {

      struct unknown_dimensionality_error : public virtual exception_base {};

    }

    //==================================================================

    // Description:
    // Templated function which returns the dimensionality of a 
    // particular object given to it
    template< class T >
    size_t dimensionality( const T& a )
    {
      return 1;
    }

    template< class T >
    size_t dimensionality( const boost::shared_ptr<T>& a )
    {
      if( a == false )
	BOOST_THROW_EXCEPTION( common::exceptions::unknown_dimensionality_error() );
      return dimensionality<T>( *a );
    }

      //==================================================================

  }
}

#endif

