
#if !defined (__PTP_COMMON_probability_HPP__ )
#define __PTP_COMMON_probability_HPP__

#include <math.h>
#include <boost/operators.hpp>
#include <boost/optional.hpp>
#include <boost/exception/all.hpp>
#include <exception>
#include <iosfwd>


//=========================================================================

// Description:
// A base exception that is both std and boost compatible
class exception_base : public virtual std::exception, public virtual boost::exception {};

// Description:
// An exception that means we don't know what the hell jsut happened
class internal_error : public virtual exception_base {};


//=========================================================================

namespace ptp {
  namespace common {

    //=========================================================================

    // Description:
    // Forward declarations
    class raw_probability_t;
    class raw_log_probability_t;
    class probability_t;
    class probability_expression_t;

    //=========================================================================

    // Description:
    // These are some exceptions that may happen when using probabilitiues
    namespace exceptions {
      
      // Description:
      // The probability has become negative
      struct probability_is_negative_error : public virtual exception_base {};
      
      // Descripiton:
      // The proabability is larger than 1
      struct probability_is_greater_than_one_error : public virtual exception_base {};
      
      // Description:
      // The probability has become too small to handle
      struct probability_is_too_small_error : public virtual exception_base {};

      // Description:
      // Add a raw_probability_t object to an exception
      typedef boost::error_info<struct tag_raw_probability_t, raw_probability_t > raw_probability_t_info;
      
      // Description:
      // Add a raw_log_probability_t object to an exception
      typedef boost::error_info<struct tag_raw_log_probability_t, raw_log_probability_t > raw_log_probability_t_info;
      
      // Description:
      // Add a probability_t object to an exception
      typedef boost::error_info<struct tag_probability_t, probability_t > probability_t_info;

            
    }

    
    //=========================================================================
    
    // Description:
    // A probability expression is a not-quite completed computation for
    // a probability value.  In effect, exressions are *not* enforced to
    // be normalized, nor non-negative, but are still check to see if
    // they are too small.
    //
    // An expression can be casted ( implicitly ) into a probability,
    // raw probability, or raw log probability.  These in turn do enforce
    // normal probability constraints ( non-negative, capped at 1 ).
    //
    // Any computation done with probabilities should result in an expression
    // until it is store in a probabiity object ( at the end of the computation,
    // when the value is once again a valid probability ).
    //
    // All probability, raw_probability, and raw_log_probability objects
    // return expression when operators are involved.
    class probability_expression_t
      : boost::ordered_field_operators< probability_expression_t >,
	boost::ordered_field_operators< probability_expression_t, float >
    {
    public:

      // Description:
      // the current value for a computation.
      // Note: this is a double to allow for temporary values with
      //       higher precision than a resulting probability value
      double _value;

      // Description:
      // Flag stating whether we are in log space of not
      bool _using_log_space;
      
      // Description:
      // Creates an expression from a probability value
      probability_expression_t( const raw_probability_t& p );
      probability_expression_t( const raw_log_probability_t& p );
      probability_expression_t( const probability_t& p );

      // Description:
      // Automatically cast into a probability
      operator raw_probability_t();
      operator raw_log_probability_t();
      operator probability_t();
      

      // Description:
      // Operators need for arithmetic
      probability_expression_t operator+= (const probability_expression_t& p );
      probability_expression_t operator-= (const probability_expression_t& p );
      probability_expression_t operator*= (const probability_expression_t& p );
      probability_expression_t operator/= (const probability_expression_t& p );
      probability_expression_t operator+= (const float& v );
      probability_expression_t operator-= (const float& v );
      probability_expression_t operator*= (const float& v );
      probability_expression_t operator/= (const float& v );
      

      // Description:
      // Operators for ordering
      bool operator< (const probability_expression_t& p ) const;
      bool operator== (const probability_expression_t& p ) const;
      

      // Description:
      // Returns the value in normal probability space
      inline double get_normal_value() const 
      { if( _using_log_space ) return exp( _value ); return _value; }
      
      // Description:
      // Returns the value in log space
      inline double get_log_value() const
      { if( _using_log_space ) return _value; return log( _value ); }
      

      // Description:
      // IO operators
      friend std::ostream& operator<< (std::ostream& os, const probability_expression_t& p );
      

    protected:


    private:
    };


    //=========================================================================

    // Description:
    // A probability value in normal probability space (normalized)
    struct raw_probability_t
    {
      
      // Description:
      // The floating point precision representation of the 
      // probability
      float _prob;
      
      // Description:
      // Convert to/from float and a probability
      raw_probability_t( const float& p ) : _prob( p ) { validate(); }
      float as_float() const { return _prob; }

      // Description:
      // Validates this probability to ensure that it is a valid
      // normalized probability
      void validate();

      
      // Description:
      // Operators needed for arithmatic
      probability_expression_t operator+= (const probability_expression_t& p )
      { (*this) = probability_expression_t(*this) += p; }
      probability_expression_t operator-= (const probability_expression_t& p )
      { (*this) = probability_expression_t(*this) -= p; }
      probability_expression_t operator*= (const probability_expression_t& p )
      { (*this) = probability_expression_t(*this) *= p; }
      probability_expression_t operator/= (const probability_expression_t& p )
      { (*this) = probability_expression_t(*this) /= p; }
      
      // Operators for comparing
      bool operator< (const raw_probability_t& p ) const
      {
	return this->_prob < p._prob;
      }
      bool operator== (const raw_probability_t& p ) const
      {
	return this->_prob == p._prob;
      }
      
      

      // Description:
      // IO operators
      friend std::ostream& operator<< (std::ostream& os, const raw_probability_t& p );
      
    }; 

    
    // Description:
    // Operators derived from those above
    probability_expression_t operator+ ( const raw_probability_t& a,
					 const probability_expression_t& b );
    probability_expression_t operator- ( const raw_probability_t& a,
					 const probability_expression_t& b );
    probability_expression_t operator* ( const raw_probability_t& a,
					 const probability_expression_t& b );
    probability_expression_t operator/ ( const raw_probability_t& a,
					 const probability_expression_t& b );
    probability_expression_t operator+ ( const raw_probability_t& a,
					 const float& b );
    probability_expression_t operator- ( const raw_probability_t& a,
					 const float& b );
    probability_expression_t operator* ( const raw_probability_t& a,
					 const float& b );
    probability_expression_t operator/ ( const raw_probability_t& a,
					 const float& b );
    


    //=========================================================================

    // Description:
    // A normalized probability in log space
    struct raw_log_probability_t 
    {
						
      // Description:
      // The log(p) probability
      float _log_prob;
      

      // Description:
      // Convert to/from a float
      raw_log_probability_t( const float& lp ) : _log_prob( lp ) { validate(); }
      float as_float() const { return this->_log_prob; }
      

      // Description:
      // Convert from raw_probability_t to raw_log_probability_t
      raw_log_probability_t( const raw_probability_t& p )
	: _log_prob( log( p._prob ) ) { }
      
      
      // Description:
      // Convert from raw_log_probability_t to raw_probability_t
      raw_probability_t as_probability() const { return exp( this->_log_prob ); }
      operator raw_probability_t() const { return this->as_probability(); }


      // Description:
      // Validates this probability to ensure that it is a valid
      // normalized log probability
      void validate();


      // Description:
      // Operators needed for arithmatic
      probability_expression_t operator+= (const probability_expression_t& p )
      { (*this) = probability_expression_t(*this) += p; }
      probability_expression_t operator-= (const probability_expression_t& p )
      { (*this) = probability_expression_t(*this) -= p; }
      probability_expression_t operator*= (const probability_expression_t& p )
      { (*this) = probability_expression_t(*this) *= p; }
      probability_expression_t operator/= (const probability_expression_t& p )
      { (*this) = probability_expression_t(*this) /= p; }

      // Operators for comparing
      bool operator< (const raw_log_probability_t& p ) const
      {
	return this->_log_prob < p._log_prob;
      }
      bool operator== (const raw_log_probability_t& p ) const
      {
	return this->_log_prob == p._log_prob;
      }
      
      // Description:
      // IO operators
      friend std::ostream& operator<< (std::ostream& os, const raw_probability_t& p );
      
    };

    
    // Description:
    // Operators derived from those above
    probability_expression_t operator+ ( const raw_log_probability_t& a,
					 const probability_expression_t& b );
    probability_expression_t operator- ( const raw_log_probability_t& a,
					 const probability_expression_t& b );
    probability_expression_t operator* ( const raw_log_probability_t& a,
					 const probability_expression_t& b );
    probability_expression_t operator/ ( const raw_log_probability_t& a,
					 const probability_expression_t& b );
    probability_expression_t operator+ ( const raw_log_probability_t& a,
					 const float& b );
    probability_expression_t operator- ( const raw_log_probability_t& a,
					 const float& b );
    probability_expression_t operator* ( const raw_log_probability_t& a,
					 const float& b );
    probability_expression_t operator/ ( const raw_log_probability_t& a,
					 const float& b );
    


    //=========================================================================
    
    // Description:
    // This is a probability class which deals in the ideal and abstract
    // sense of a probability, but can internally use either
    // raw_probability_t or raw_log_probability_t as it's representation
    //
    // In particular: operators +, -, *, / always act as if a 
    // raw_probability_t object even if internally it is a raw_log_probability_t
    struct probability_t
    {
      
      // Description:
      // the internal representation being used flag
      bool _using_log_space;
      
      // Description:
      // The itnernal representations.
      // Note: if there is a value, it is consistent
      //       (so if both values exists, both are consistent )
      boost::optional< raw_probability_t > _probability;
      boost::optional< raw_log_probability_t > _log_probability;
      
      
      // Description:
      // Creates a new probability from a raw_probability_t
      // with optional flag to use log space ( false by default )
      // or not
      // Note: the given float is assumed to *be* a probability
      //       in normal space, not log, regardless of optional flag
      probability_t( const raw_probability_t& p, 
		     const bool use_log_space = true )
	: _using_log_space( use_log_space )
      {
	if( _using_log_space ) {
	  _log_probability.reset( static_cast<raw_log_probability_t>(p) );
	} else {
	  _probability.reset( p );
	}
      }

      // Description:
      // Creates a new probability from a raw_log_probability_t
      // with optional flag to use log space ( true by default )
      //
      // Note: the given log probability is assumed to be a log probability
      probability_t( const raw_log_probability_t& p,
		     const bool use_log_space = true )
	: _using_log_space( use_log_space )
      {
	if( _using_log_space ) {
	  _log_probability.reset( p );
	} else {
	  _probability.reset( p.as_probability() );
	}
      }

      // Description:
      // Creates a new probability from a float
      // with optional flag to use log space ( false by default )
      // or not
      // Note: the given float is assumed to *be* a probability
      //       in normal space, not log, regardless of optional flag
      probability_t( const float& p, 
		     const bool use_log_space = false )
	: _using_log_space( use_log_space )
      {
	if( _using_log_space ) {
	  _log_probability.reset( raw_log_probability_t(raw_probability_t(p)) );
	} else {
	  _probability.reset( raw_probability_t(p) );
	}
      }

      // Description:
      // Creates a new probability from an expression
      // We will keep the same space as the expresison.
      // Note: this will check for a valid probability
      probability_t( const probability_expression_t& v )
	: _using_log_space( v._using_log_space )
      {
	if( _using_log_space ) {
	  _log_probability.reset( v.get_log_value() );
	  _log_probability->validate();
	} else {
	  _probability.reset( v.get_normal_value() );
	  _probability->validate();
	}
      }


      // Description:
      // Returns the current probability value
      raw_probability_t get_raw_probability_t() const
      {
	if( !_probability )
	  return _log_probability->as_probability();
	return *_probability;
      }

      // Description:
      // Returns the current log probability value
      raw_log_probability_t get_raw_log_probability_t() const
      {
	if( !_log_probability ) {
	  return (raw_log_probability_t)(*_probability);
	} else {
	  return *_log_probability;
	}
      }


      // Description:
      // Operators needed for arithmetic
      probability_expression_t operator+= (const probability_expression_t& p )
      {	probability_expression_t(*this) += p; }
      probability_expression_t operator-= (const probability_expression_t& p )
      {	probability_expression_t(*this) -= p; }
      probability_expression_t operator*= (const probability_expression_t& p )
      {	probability_expression_t(*this) *= p; }
      probability_expression_t operator/= (const probability_expression_t& p )
      {	probability_expression_t(*this) /= p; }


      // Description:
      // Compare two probabilities
      bool operator< (const probability_t& p ) const
      {
	// work in the space of this object
	if( !this->_using_log_space) {
	  return this->_probability < p.get_raw_probability_t();
	} else {
	  return this->_log_probability < p.get_raw_log_probability_t();
	}
      }

      // Description:
      // Twpo probabilites are equal if their probability values are equal
      bool operator== (const probability_t& p ) const
      {
	// work in the space of this object
	if( !this->_using_log_space) {
	  return this->_probability == p.get_raw_probability_t();
	} else {
	  return this->_log_probability == p.get_raw_log_probability_t();
	}
      }


      // Description:
      // IO operators
      friend std::ostream& operator<< (std::ostream& os, const probability_t& p );

    };


    // Description:
    // Operators derived from those above
    probability_expression_t operator+ ( const probability_t& a,
					 const probability_expression_t& b );
    probability_expression_t operator- ( const probability_t& a,
					 const probability_expression_t& b );
    probability_expression_t operator* ( const probability_t& a,
					 const probability_expression_t& b );
    probability_expression_t operator/ ( const probability_t& a,
					 const probability_expression_t& b );
    probability_expression_t operator+ ( const probability_t& a,
					 const float& b );
    probability_expression_t operator- ( const probability_t& a,
					 const float& b );
    probability_expression_t operator* ( const probability_t& a,
					 const float& b );
    probability_expression_t operator/ ( const probability_t& a,
					 const float& b );



    //=========================================================================


  }
}

#endif

