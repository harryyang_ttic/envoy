
#include "distribution.hpp"


// Description:
// A global random number generator for distributions
namespace ptp {
  namespace common {

    const gsl_rng_type* _distributions_rng_T;
    gsl_rng* _distributions_rng = NULL;

    
    gsl_rng* static_distributions_gsl_rng()
    {
      if( _distributions_rng == NULL ) {
	gsl_rng_env_setup(); // uses the environment variable GSL_RNG_TYPE
	_distributions_rng_T = gsl_rng_default;
	_distributions_rng = gsl_rng_alloc ( _distributions_rng_T );
      }
      return _distributions_rng;
    }

  }
}
