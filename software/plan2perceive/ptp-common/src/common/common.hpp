
#if !defined( __PTP_COMMON_common_HPP__ )
#define __PTP_COMMON_common_HPP__



#include "probability.hpp"
#include <boost/shared_ptr.hpp>
#include <iostream>


namespace ptp {
  namespace common {
  
    // Description:
    // Returns whetehr two floats are equal, with a fudge factor
    static bool float_equal( const float& a, 
			     const float& b, 
			     const float& epsilon = 1e-6) 
    {
      return abs( a - b ) < epsilon;
    }

    // Description:
    // Returns whetehr two probs are equal, with a fudge factor
    static bool float_equal( const raw_probability_t& a, 
			     const raw_probability_t& b, 
			     const float& epsilon = 1e-6) 
    {
      return float_equal( a.as_float(), b.as_float(), epsilon );
    }

    // Description:
    // Returns whetehr two probs are equal, with a fudge factor
    static bool float_equal( const raw_log_probability_t& a, 
			     const raw_log_probability_t& b, 
			     const float& epsilon = 1e-6) 
    {
      return float_equal( a.as_float(), b.as_float(), epsilon );
    }
    
    // Description:
    // Returns whetehr two probs are equal, with a fudge factor
    static bool float_equal( const probability_t& a, 
			     const probability_t& b, 
			     const float& epsilon = 1e-6) 
    {
      return float_equal( a.get_raw_probability_t().as_float(), 
			  b.get_raw_probability_t().as_float(), 
			  epsilon );
    }

    // Description:
    // Returns whetehr two probs are equal, with a fudge factor
    static bool float_equal( const probability_t& a, 
			     const raw_probability_t& b, 
			     const float& epsilon = 1e-6) 
    {
      return float_equal( a.get_raw_probability_t().as_float(), 
			  b.as_float(), 
			  epsilon );
    }

    // Description:
    // Returns whetehr two probs are equal, with a fudge factor
    static bool float_equal( const raw_probability_t& a, 
			     const probability_t& b, 
			     const float& epsilon = 1e-6) 
    {
      return float_equal( a.as_float(), 
			  b.get_raw_probability_t().as_float(), 
			  epsilon );
    }

    // Description:
    // Returns whetehr two probs are equal, with a fudge factor
    static bool float_equal( const probability_t& a, 
			     const raw_log_probability_t& b, 
			     const float& epsilon = 1e-6) 
    {
      return float_equal( a.get_raw_log_probability_t().as_float(), 
			  b.as_float(), 
			  epsilon );
    }

    // Description:
    // Returns whetehr two probs are equal, with a fudge factor
    static bool float_equal( const raw_log_probability_t& a, 
			     const probability_t& b, 
			     const float& epsilon = 1e-6) 
    {
      return float_equal( a.as_float(), 
			  b.get_raw_log_probability_t().as_float(), 
			  epsilon );
    }

    // Description:
    // Returns whetehr two probs are equal, with fidge factor
    template< class T >
    static bool float_equal( const probability_expression_t& a,
			     const T& b,
			     const float& epsilon = 1e-6 )
    {
      return float_equal( static_cast<probability_t>(a), b, epsilon );
    }
    template< class T >
    static bool float_equal( const T& a,
			     const probability_expression_t& b,
			     const float& epsilon = 1e-6 )
    {
      return float_equal( a, static_cast<probability_t>(b), epsilon );
    }


    // Description:
    // general 'float equal' for objects
    template< class T, class U >
    bool float_equal( const T& a, const T& b, const U& epsilon )
    {
      if( a > b )
	return (a - b) < epsilon;
      return (b - a) < epsilon;
    }

  }

}


#endif
