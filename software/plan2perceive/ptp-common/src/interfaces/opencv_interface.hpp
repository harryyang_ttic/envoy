
#if !defined ( __PTP_INTERFACES_OPENCV_opencv_interface_HPP__ )
#define __PTP_INTERFACES_OPENCV_opencv_interface_HPP__

#include <cv.h>
#include <cxmisc.h>
#include <cvaux.h>
#include <highgui.h>
#include <ptp-common/matrix.hpp>
#include <string>

namespace ptp {
  namespace interfaces {

    using namespace ptp::common;

    //==========================================================

    namespace exceptions {

      // Descripton:
      // An opencv matrix type is wrong, not implemented yet,
      // or unknown
      struct matrix_type_error : public virtual exception_base {};
      
    }

    //==========================================================

    // Description:
    // A shared pointer to a cv mat
    //typedef boost::shared_ptr<CvMat> cv_mat_tp;

    //==========================================================

    // Description:
    // A deleter for a smart pointer of CvMat type.
    //void cv_mat_free( CvMat* m );

    //==========================================================
    
    namespace opencv {

      // Description:
      // Creates and returns a copy of the given old cv matrix
      cv::Mat copy( CvMat* m );

      // Description:
      // Creates a matrix_t pointer COPY of the given cv matrix
      matrix_tp to_matrix( const cv::Mat& a );
      
      // Description:
      // Converts from a matrix to an cv mat
      // This is a COPY
      cv::Mat create_cv_matrix( const matrix_tp& a );


      // Description:
      // Loads an image and returns a new matrix view of it.
      // It converts the image from rgb to gray
      cv::Mat load_image_gray( const std::string& filename );

      // Description:
      // Save the given image
      void save_image( const std::string& filename, const cv::Mat& a );
    
    }

    //==========================================================
    //==========================================================
    //==========================================================

  }
}

#endif

