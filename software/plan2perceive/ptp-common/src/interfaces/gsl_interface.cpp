
#include "gsl_interface.hpp"


namespace ptp {
  namespace interfaces {
    namespace gsl {

      //=================================================================

      gsl_vector_tp create_gsl_vector( const math_vector_t& a )
      {
	// First, allocate the vector
	gsl_vector_tp v( gsl_vector_alloc( a.size() ),
			 &gsl_vector_free );
	
	// Now fill in the values
	for( size_t i = 0; i < a.size(); ++i ) {
	  gsl_vector_set( v.get() , i, a(i) );
	}
	
	return v;
      }

      //=================================================================

      math_vector_t to_math_vector( gsl_vector_tp& a )
      {
	math_vector_t v( a->size );
	for( size_t i = 0; i < a->size; ++i ) {
	  v( i ) = gsl_vector_get( a.get(), i );
	}
	return v;
      }

      //=================================================================

      math_vector_t to_math_vector( gsl_vector* a )
      {
	math_vector_t v( a->size );
	for( size_t i = 0; i < a->size; ++i ) {
	  v( i ) = gsl_vector_get( a, i );
	}
	return v;
      }
      
      //=================================================================
      
      gsl_matrix_tp create_gsl_matrix( const matrix_t& a )
      {
	// Allocate matrix
	gsl_matrix_tp m( gsl_matrix_alloc( a.rows(), a.cols() ),
			 &gsl_matrix_free );
	
	// fill matrix with values
	for( size_t i = 0; i < a.rows(); ++i ) {
	  for( size_t j = 0; j < a.cols(); ++j ) {
	    gsl_matrix_set( m.get(), i, j, a(i,j) );
	  }
	}
	
	return m;
      }
      
      //=================================================================
      
      matrix_t to_matrix( gsl_matrix_tp& a )
      {
	matrix_t m( a->size1, a->size2 );
	for( size_t i = 0; i < a->size1; ++i ) {
	  for( size_t j = 0; j < a->size2; ++j ) {
	    m(i,j) = gsl_matrix_get( a.get(), i, j );
	  }
	}
	return m;
      }

      //=================================================================
      
      matrix_t to_matrix( gsl_matrix* a )
      {
	matrix_t m( a->size1, a->size2 );
	for( size_t i = 0; i < a->size1; ++i ) {
	  for( size_t j = 0; j < a->size2; ++j ) {
	    m(i,j) = gsl_matrix_get( a, i, j );
	  }
	}
	return m;
      }


      //=================================================================
      //=================================================================
      //=================================================================
      //=================================================================
      //=================================================================
      //=================================================================
      //=================================================================
      //=================================================================
      //=================================================================
      //=================================================================
      //=================================================================
      //=================================================================
      //=================================================================
      //=================================================================
      

    }
  }
}
