
#include "opencv_interface.hpp"


namespace ptp {
  namespace interfaces {

    //==========================================================

    // void cv_mat_free( CvMat* m )
    // {
    //   cvReleaseMat( &m );
    // }
    
    //==========================================================


    namespace opencv {

      //==========================================================

      cv::Mat copy( CvMat* m )
      {
	cv::Mat c( m->rows, m->cols, CV_MAT_TYPE( m->type ) );
	for( size_t i = 0 ; i < m->rows; ++i ) {
	  for( size_t j = 0; j < m->cols; ++j ) {

	    switch( CV_MAT_TYPE( m->type ) ) {
	    case CV_8UC1:
	      c.at<unsigned char>( i,j ) = cvGet2D( m, i, j ).val[0];
	      break;
	    case CV_16UC1:
	      c.at<unsigned short>( i,j ) = cvGet2D( m, i, j ).val[0];
	      break;
	    case CV_16SC1:
	      c.at<short>( i,j ) = cvGet2D( m, i, j ).val[0];
	      break;
	    case CV_32FC1:
	      c.at<float>( i,j ) = cvGet2D( m, i, j ).val[0];
	      break;
	    case CV_64FC1:
	      c.at<double>( i,j ) = cvGet2D( m, i, j ).val[0];
	      break;
	    default:
	      BOOST_THROW_EXCEPTION( exceptions::matrix_type_error() );
	    } 
	  
	  }
	}
	
	return c;
	  
      }

      //==========================================================

      matrix_tp to_matrix( const cv::Mat& a )
      {
	matrix_tp m( new matrix_t( a.rows, a.cols ) );
	for( size_t i = 0; i < m->rows(); ++i ) {
	  for( size_t j = 0; j < m->cols(); ++j ) {
	    switch( a.type() ) {
	    case CV_8UC1:
	      (*m)(i,j) = a.at<unsigned char>( i, j );
	      break;
	    case CV_16UC1:
	      (*m)(i,j) = a.at<unsigned short>( i, j );
	      break;
	    case CV_16SC1:
	      (*m)(i,j) = a.at<short>( i, j );
	      break;
	    case CV_32FC1:
	      (*m)(i,j) = a.at<float>( i, j );
	      break;
	    case CV_64FC1:
	      (*m)(i,j) = a.at<double>( i, j );
	      break;
	    default:
	      BOOST_THROW_EXCEPTION( exceptions::matrix_type_error() );
	    }
	  }
	}
	return m;
      }

      //==========================================================

      cv::Mat create_cv_matrix( const matrix_tp& a )
      {
	cv::Mat m( a->rows(), a->cols(), CV_32FC1 );
	for( size_t i = 0; i < a->rows(); ++i ) {
	  for( size_t j = 0; j < a->cols(); ++j ) {
	    m.at<float>( i, j ) = (*a)(i,j);
	  }
	}
	return m;
      }

      //==========================================================

      // cv_mat_tp create_cv_matrix( const int& rows, const int& cols, const int& type )
      // {
      // 	return cv_mat_tp( cvCreateMat( rows, cols, type ), &cv_mat_free );
      // }

      //==========================================================

      cv::Mat load_image_gray( const std::string& filename )
      {
	IplImage *stereo_image = cvLoadImage( filename.c_str() );
	cv::Mat m( stereo_image );
	cv::Mat mgray;
	cv::cvtColor( m, mgray, CV_RGB2GRAY );
	cvReleaseImage( &stereo_image );
	return mgray;
      }
      
      //==========================================================
      
      void save_image( const std::string& filename, const cv::Mat& a )
      {
	CvMat m = a;
	cvSaveImage( filename.c_str(), &m );
      }
      
      //==========================================================


    }
  }
}
