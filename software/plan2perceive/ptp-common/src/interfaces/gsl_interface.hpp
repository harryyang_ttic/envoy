
#if !defined( __PTP_INTERFACES_GSL_gsl_interface_HPP__ )
#define __PTP_INTERFACES_GSL_gsl_interface_HPP__

#include <ptp-common/matrix.hpp>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>


namespace ptp {
  namespace interfaces {

    using namespace ptp::common;
      
    //=================================================================
    
    // Description:
    // Shared pointer types for gsl vector and matrix classes
    typedef boost::shared_ptr<gsl_vector> gsl_vector_tp;
    typedef boost::shared_ptr<gsl_matrix> gsl_matrix_tp;
    
    
    //=================================================================
    
    
    namespace gsl {
      

      //=================================================================

      // Description:
      // Creates a new gsl vector from a math_vector
      // Note: this COPIES the data
      gsl_vector_tp create_gsl_vector( const math_vector_t& a );
      math_vector_t to_math_vector( gsl_vector_tp& a );
      math_vector_t to_math_vector( gsl_vector* a );

      //=================================================================

      // Description:
      // Creates a new gsl matrix from a matrix_t
      // Note: this COPIES the data
      gsl_matrix_tp create_gsl_matrix( const matrix_t& a );
      matrix_t to_matrix( gsl_matrix_tp& a );
      matrix_t to_matrix( gsl_matrix* a );

      //=================================================================
      //=================================================================
      
    }


  }
}


#endif

