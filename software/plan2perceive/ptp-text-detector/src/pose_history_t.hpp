
#if !defined( __PTP_TEXTDETECTOR_pose_history_t_HPP__ )
#define __PTP_TEXTDETECTOR_pose_history_t_HPP__


#include <lcmtypes/bot_core_pose_t.h>
#include <boost/shared_ptr.hpp>
#include <vector>


namespace ptp {
  namespace detectors {    
    namespace textdetector {
      

      //===============================================================
      
      // Description:
      // A pose and a time together
      struct pose_frame_t {
	bot_core_pose_t pose;
	int64_t time;
      };
      typedef boost::shared_ptr<pose_frame_t> pose_frame_tp;
      
      //===============================================================

      //Description:
      // A finite history of poses ( with times )
      class pose_history_t
      {
      public:

	// Description:
	// Create a history with given capacity
	pose_history_t( const int& cap )
	  : _capacity( cap ), _end_index(0), _history( cap )
	{}

	// Description:
	// Add a pose with a time
	void add( const bot_core_pose_t& pose, 
		  const int64_t& t );

	// Description:
	// Get the closest pose at a given time in history
	pose_frame_tp find_closest( const int64_t& t ) const;
	
      protected:

	// Description:
	// THe capacity
	int _capacity;
	
	// Description:
	// The current end index
	size_t _end_index;
	
	// Description:
	// The history
	std::vector<pose_frame_tp> _history;
      };
      
      //===============================================================

    }
  }
}

#endif

