
#if !defined( __PTP_TEXTDETECTOR_text_detection_t_HPP__ )
#define __PTP_TEXTDETECTOR_text_detection_t_HPP__

#include <leget/TextSpotter.h>
#include <ptp-stereo/stereo_image_t.hpp>
#include <lcmtypes/ptplcm_object_detection_t.h>
#include <vector>

namespace ptp {
  namespace detectors {


    //===============================================================
    
    namespace textdetector {

      using namespace ptp::coordinates;
      using namespace ptp::stereo;

      //===============================================================

      // Description:
      // A detection from our text spotter
      class text_detection_t
      {
      public:
	
	// Description:
	// The detected word from Leget
	leget::Word leget_word;
	
	// Description:
	// The patch image and point cloud for it
	cv::Mat image_patch;
	cv::Mat disparity_patch;
	mapped_point_cloud_tp point_cloud;
	
	text_detection_t()
	  : leget_word( "~~", -1, "~~", -1, cv::Rect() )
	{}
      };
      
      // Description:
      // SHared pointer type
      typedef boost::shared_ptr<text_detection_t> text_detection_tp;
      
      // Description:
      // List type is just a vector
      typedef std::vector<text_detection_tp> text_detection_list_t;
      
    }



    //===============================================================
    
    // Descriptions:
    // Function to convert to/from lcm structures
    namespace lcm {

      using namespace ptp::detectors::textdetector;
      
      // Description:
      // Create lcm message with copy of detection
      ptplcm_object_detection_t* create_lcm_detection( const text_detection_tp& a );
      
      // Description:
      // Create a detection with copy of lcm message
      text_detection_tp create_detection( const ptplcm_object_detection_t* msg );
      
    }
    
    //===============================================================
    
    
  }
}


#endif


