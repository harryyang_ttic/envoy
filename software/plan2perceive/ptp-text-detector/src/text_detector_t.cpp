
#include "text_detector_t.hpp"


using namespace ptp;
using namespace ptp::detectors;
using namespace ptp::detectors::textdetector;
using namespace ptp::stereo;


//=====================================================================

text_detector_params_t::text_detector_params_t()
  : vote_threshold( 500.0 ),
    min_area_threshold( 150 ),
    alpha( 20.0 ),
    num_scales( 15 ),
    base_window_width( 9 ),
    base_window_height( 5 ),
    x_step( 4 ),
    y_step( 2 ),
    rescale_size_x( 1024 ),
    rescale_size_y( 768 ),
    threshold( 0.2 )
{}

//=====================================================================

text_detector_t::text_detector_t()
{
  set_params( text_detector_params_t() );
}

//=====================================================================

void text_detector_t::set_params( const text_detector_params_t& p )
{
  _params = p;

  // create a threshold set from the single threshold
  leget::TextSpotter::ThresholdSet thresh_set;
  thresh_set.insert( p.threshold );

  // create a new TextSpotter when parameters change
  // _text_spotter = boost::shared_ptr<leget::TextSpotter>
  //   ( new leget::TextSpotter( p.vote_threshold,
  // 			      thresh_set,
  // 			      p.min_area_threshold,
  // 			      p.alpha ) );
  _text_spotter = boost::shared_ptr<leget::TextSpotter>
    ( new leget::TextSpotter() );
  _text_spotter->setFilterWords( false );
  _text_spotter->setRelativeVoteThresholds( thresh_set );
}

//=====================================================================

// Descripiton:
// Draw a text region, word, and probability onto image
void annotate(cv::Mat &img, 
              const std::string& str, 
              const cv::Rect& rect) 
{
    cv::Scalar colour(0);
    colour[2] = 135;
    
    cv::Point pt1, pt2;
    pt1.x = rect.x;
    pt1.y = rect.y;
    pt2.x = rect.x + rect.width;
    pt2.y = rect.y + rect.height;
    
    cv::rectangle(img, pt1, pt2, colour, 2);
    
    
    int nFontFace = cv::FONT_HERSHEY_PLAIN;
    double dfFontScale = 1.5;
    int nThickness = 2;
    int nBaseline = 0;
    cv::Size textSize = cv::getTextSize(str, nFontFace, dfFontScale, nThickness, &nBaseline);
    nBaseline += nThickness;
    
    // center the text
    //Point textOrg((img.cols - textSize.width)/2,
    //              (img.rows + textSize.height)/2);
    cv::Point rectOrg(rect.x, rect.y);
    cv::Point textOrg(rect.x, rect.y - 5);
    
    
    // draw the box
    //rectangle(img, textOrg + cv::Point(0, nBaseline), textOrg + cv::Point(textSize.width, -textSize.height), colour, CV_FILLED);
    cv::rectangle(img, rectOrg, rectOrg + cv::Point(textSize.width, -(textSize.height+12) ), colour, CV_FILLED);
    
    // write the text itself
    cv::putText(img, str, textOrg, nFontFace, dfFontScale, cv::Scalar::all(255), nThickness, 1);
}


//=====================================================================


text_detection_list_t text_detector_t::process( const stereo_image_t& img ) const
{

  // The actual work

  std::cout << "searching to text" << std::endl;
  
  // Get the left image ( is already grayscale ) as cv matrix
  cv::Mat cv_gray_image = img.cv_left_image();


  leget::TextSpotter spotter;
  spotter.setFilterWords( false );
  leget::TextSpotter::ThresholdSet relThresholds;
  relThresholds.insert( _params.threshold );
  spotter.setRelativeVoteThresholds(relThresholds);
  
  // call 'detect' then 'indentifyAndParse'
  // _text_spotter->detect( cv_gray_image, 
  // 			 _params.base_window_width,
  // 			 _params.base_window_height,
  // 			 _params.x_step,
  // 			 _params.y_step,
  // 			 _params.num_scales );
  // _text_spotter->identifyAndParse( cv_gray_image );
  spotter.detect( cv_gray_image, 
		  _params.base_window_width,
		  _params.base_window_height,
		  _params.x_step,
		  _params.y_step,
		  _params.num_scales );
  spotter.identifyAndParse( cv_gray_image );
  spotter.checkSpelling();
	  
  
  // get the detection words
  //leget::TextSpotter::ProofedWords pw = _text_spotter->getProofedWords();
  leget::TextSpotter::ProofedWords pw = spotter.getProofedWords();

  // copy image to write onto for debug
  cv::Mat img_clone = cv_gray_image.clone();
  
  // Create list of detection objects
  text_detection_list_t dets;
  for( leget::TextSpotter::ProofedWords::const_iterator i = pw.begin(); 
       i != pw.end(); ++i ) {
    text_detection_tp d = text_detection_tp( new text_detection_t( ) );
    d->leget_word = (*i);
    cv::Rect rect;
    i->getBoundingRect( rect );
    d->image_patch = cv_gray_image( rect );
    d->disparity_patch = img.disparity()( rect );
    d->point_cloud = 
      img.mapped_point_cloud_for_rectangle( rect.x, 
					    rect.y,
					    rect.x + rect.width,
					    rect.y + rect.height);
    dets.push_back( d );

    // draw detection rectangle onto clone
    std::stringstream ssFullLabel;
    ssFullLabel.precision(2);
    ssFullLabel << i->getCorrectedLabel() << " [p=" << i->getPosterior() << "] ";
    annotate( img_clone, ssFullLabel.str(), rect );
  }


  // save the debug file
  cv::imwrite( "anotated_text_regions.jpg", img_clone );
  

  // reutrn the detections
  return dets;

}

//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
