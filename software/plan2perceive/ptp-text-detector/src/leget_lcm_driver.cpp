
#include <ptp-text-detector/text_detector_t.hpp>
#include <lcm/lcm.h>
#include <lcmtypes/ptplcm_object_detection_set_t.h>
#include <lcmtypes/camlcm_image_t.h>
#include <ptp-stereo/stereo_util.hpp>
#include <ptp-common-coordinates/identity_manifold_t.hpp>
#include <ptp-fit/plane_fit.hpp>
#include <bot_core/timestamp.h>
#include "pose_history_t.hpp"
#include <iostream>
#include <fstream>

using namespace ptp;
using namespace ptp::coordinates;
using namespace ptp::stereo;
using namespace ptp::interfaces;
using namespace ptp::detectors::textdetector;
using namespace ptp::fit;


// global variables
text_detector_t detector;
lcm_t *lcm = NULL;
stereo_engine_tp engine;
manifold_tp cam_manifold;
int32_t next_object_id = 0;
pose_history_t _history( 2000 );



void handle_pose(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
		 const char * channel __attribute__((unused)), 
		 const bot_core_pose_t * msg, 
		 void * user) 
{
  _history.add( *msg, msg->utime );
  std::cout << ".";
  std::cout.flush();
}


ptplcm_object_detection_t
create_detection( const text_detection_tp& det,
		  const int64_t& image_utime,
		  const stereo_image_t& image,
		  const pose_frame_tp& pose )
{
  
  point_cloud_tp points = det->point_cloud->points();

  // Compute a plane from the point cloud
  plane_fit_ransac_parameters_t ransac_param;
  ransac_param.percent_to_subsample = 0.2;
  ransac_param.min_samples = 4;
  ransac_param.percent_which_must_agree = 0.75;
  ransac_param.percent_of_max_distance_which_is_agree = 0.2;
  ransac_param.max_agree_distance = 0.25;
  ransac_param.min_agree_distance = 0.05;
  ransac_param.max_mean_distance = 0.15;
  ransac_param.max_iterations = std::min( std::max( points->size() / 200, 50 ), 100 );
  std::cout << "Fitting plane ( samples= " << (ransac_param.percent_to_subsample * points->size() ) << " max iters= " << ransac_param.max_iterations << ").." << std::endl;
  plane_tp plane = find_best_plane_fit( points, ransac_param );
  
  // Ok, now compute the center of the plane on the camera manifold
  coordinate_t center = plane->origin_in_reference().on( cam_manifold );

  // get teh normal to the plane, centered at 0,0,0 ( so a direction )
  coordinate_t normal = basis_from_simplex( plane->normal_simplex() ).origin_centered_direction();
  
  // Calculate the distance and angle to this point
  basis_t center_basis = basis_t( coordinate( 0,0,0 ).on( cam_manifold ),
				  center );
  float distance = center[2];
  double orientation = atan2( normal[0], normal[2] );



  
  // create and populate the ptplcm_object_detection_t
  ptplcm_object_detection_t msg;
  msg.utime = bot_timestamp_now();
  msg.source_utime = image_utime;
  msg.object_class = 1; // always text
  msg.object_local_id = next_object_id++;
  msg.detector_value = det->leget_word.getPosterior();
  coordinate_t centroid = det->point_cloud->centroid().on( cam_manifold );
  msg.position_in_detector_frame[0] = centroid[2];
  msg.position_in_detector_frame[1] = centroid[0];
  msg.position_in_detector_frame[2] = centroid[1];
  msg.distance_from_detector = distance;
  msg.orientation_from_detector_plane = orientation;
  //msg.local_map_utime;
  cv::Rect rect;
  det->leget_word.getBoundingRect( rect );
  msg.image_region_x = rect.x;
  msg.image_region_y = rect.y;
  msg.image_region_width = rect.width;
  msg.image_region_height = rect.height;
  msg.has_image_patch = false;
  msg.has_disparity_patch = false;
  msg.has_point_cloud = false;
  msg.position_of_robot_when_taken[0] = pose->pose.pos[0];
  msg.position_of_robot_when_taken[1] = pose->pose.pos[1];
  msg.position_of_robot_when_taken[2] = pose->pose.pos[2];
  msg.orientation_of_robot_when_taken[0] = pose->pose.orientation[0];
  msg.orientation_of_robot_when_taken[1] = pose->pose.orientation[1];
  msg.orientation_of_robot_when_taken[2] = pose->pose.orientation[2];
  msg.orientation_of_robot_when_taken[3] = pose->pose.orientation[3];
  msg.detector_info.position_in_robot_body_frame[0] = 0;
  msg.detector_info.position_in_robot_body_frame[1] = 0;
  msg.detector_info.position_in_robot_body_frame[2] = 0;
  msg.detector_info.look_at_axis_in_robot_body_frame[0] = 1;
  msg.detector_info.look_at_axis_in_robot_body_frame[1] = 0;
  msg.detector_info.look_at_axis_in_robot_body_frame[2] = 0;
  msg.detector_info.angle_about_look_at_axis = 0;
  msg.detector_info.fov_left_radians = deg(-45).from(angle_t::X_AXIS).radians();
  msg.detector_info.fov_right_radians = deg(45).from(angle_t::X_AXIS).radians();
  msg.detector_info.fov_bottom_radians = deg(-45).from(angle_t::X_AXIS).radians();
  msg.detector_info.fov_top_radians = deg(45).from(angle_t::X_AXIS).radians();
  msg.detector_info.fov_min_distance = 0.01;
  msg.detector_info.fov_max_distance = 6;
  msg.detector_info.id = 1; // this is the text detector


  return msg;
}

void image_handler( const lcm_recv_buf_t *rbuf, 
		    const char *channel, const camlcm_image_t *msg, void *user)
{
  // get the pose of the robot at the time of the image
  pose_frame_tp pose = _history.find_closest( msg->utime );
  
  // If the difference is too great, ignore this image
  bool HACK = true;
  if( HACK == false ) {
    if( !pose )
      return;
    if( fabs( pose->time - msg->utime ) > 1 * 1000000 ) {
      std::cout << "skipping image (pose tdiff=" << (pose->time - msg->utime ) << ")" << std::endl;
      return;
    }
  } else {
    pose = pose_frame_tp( new pose_frame_t() );
    pose->pose.pos[0] = 0;
    pose->pose.pos[1] = 0;
    pose->pose.pos[2] = 0;
    pose->pose.orientation[0] = 0;
    pose->pose.orientation[1] = 0;
    pose->pose.orientation[2] = 0;
    pose->pose.orientation[3] = 0;
    pose->time = msg->utime;
  }

  // Ok, write image to jpeg file and read it
  std::ofstream fout( "temp-image-file.jpg" );
  fout.write( (char*)msg->data, msg->size );
  fout.close();

  // read and roate the image and write back to file
  // IplImage *roti = cvLoadImage( "temp-image-file.jpg" );
  // cv::Mat rotim = cv::Mat( roti );
  // cv::Mat rot = rotim.clone();
  // cv::Point2f center_point( rot.cols / 2.0f, rot.rows / 2.0f );
  // cv::warpAffine( rotim, rot, cv::getRotationMatrix2D( center_point, 180, 1.0 ), rot.size() );
  // cv::imwrite( "temp-image-file.jpg", rot );
  // cvReleaseImage( &roti );
      
  cv::Mat left, right;
  load_and_break_stereo_image_gray( "temp-image-file.jpg", left, right );
  stereo_image_t stereo_image( left, right, engine, cam_manifold );
  
  // run a text detection with image
  text_detection_list_t dets =
    detector.process( stereo_image );
  
  // publish the detections
  ptplcm_object_detection_set_t det_set;
  det_set.source_time = msg->utime;
  det_set.size = dets.size();
  det_set.detections = new ptplcm_object_detection_t[ det_set.size ];
  for( size_t i = 0; i < dets.size(); ++i ) {
    det_set.detections[i] = create_detection( dets[i], msg->utime, stereo_image, pose );
    std::cout << "   Det[" << i << "] d=" 
	      << det_set.detections[i].distance_from_detector 
	      << " o=" 
	      << det_set.detections[i].orientation_from_detector_plane 
	      << " det-loc=" 
	      << det_set.detections[i].position_in_detector_frame[0] << ","
	      << det_set.detections[i].position_in_detector_frame[1] << ","
	      << det_set.detections[i].position_in_detector_frame[2]
	      << std::endl;
  }
  det_set.utime = bot_timestamp_now();
  
  // publish the detection set
  ptplcm_object_detection_set_t_publish( lcm, "DETECTOR_OUTPUT_CHANNEL", &det_set );
  std::cout << "Detected " << dets.size() << " Text Regions" << std::endl;
  
  // free resxsources
  delete[] det_set.detections;
}
		    

int main( int argc, char** argv )
{

  // create the global lcm
  lcm = lcm_create( NULL );

  // Create the parameters for the text detector and 
  // set them
  text_detector_params_t params;
  params.vote_threshold = 500;
  params.min_area_threshold = 150;
  params.alpha = 20;
  params.num_scales = 10;
  params.base_window_width = 8;
  params.base_window_height = 5;
  params.x_step = 4;
  params.y_step = 2;
  params.rescale_size_x = 1024;
  params.rescale_size_y = 768;
  params.threshold = 0.0;
  detector.set_params( params );

  // creat the stereo engine
  engine = stereo_engine_tp( new stereo_engine_t() );
  engine->load( "/home/velezj/projects/object_detector_pose_planning/data/ptp/stereo-calibration-full-2.xml" );
  engine->set_block_matching_param( 1024/8, 0, 5 );

  // Create the camera's manifold
  // (We are lazy and just use the identity manifold here )
  cam_manifold = manifold_tp( new identity_manifold_t( manifold_t::BASE_MANIFOLD ) );
  
  // Hook into the lcm messages
  camlcm_image_t_subscription_t *img_sub 
    = camlcm_image_t_subscribe( lcm, "CAMLCM_IMAGE", &image_handler, NULL );
  camlcm_image_t_subscription_set_queue_capacity( img_sub, 1 );
  bot_core_pose_t_subscription_t *pos_sub 
    = bot_core_pose_t_subscribe( lcm, "POSE", &handle_pose, NULL );
  bot_core_pose_t_subscription_set_queue_capacity( pos_sub, 1 );
  
  // entere lcm loop
  while( lcm_handle( lcm ) == 0 ) {
    ; // keep handling
  }

  return 0;
}
