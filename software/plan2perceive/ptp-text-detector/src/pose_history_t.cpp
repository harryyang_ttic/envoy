
#include "pose_history_t.hpp"
#include <limits>
#include <math.h>


using namespace ptp;
using namespace ptp::detectors::textdetector;

//====================================================================

void pose_history_t::add( const bot_core_pose_t& pose,
			  const int64_t& t )
{
  pose_frame_tp f( new pose_frame_t() );
  f->pose = pose;
  f->time = t;
  
  _history[ _end_index ] = f;
  ++_end_index;
  if( _end_index >= _capacity )
    _end_index = 0;
}

//====================================================================

pose_frame_tp pose_history_t::find_closest( const int64_t& t ) const
{
  double tdiff = std::numeric_limits<double>::infinity();
  pose_frame_tp f;
  for( size_t i = 0; i < _history.size(); ++i ) {
    if( _history[i] ) {
      double d = fabs(_history[i]->time - t );
      if( d < tdiff ) {
	tdiff = d;
	f = _history[i];
      }
    }
  }

  return f;
}

//====================================================================

//====================================================================
