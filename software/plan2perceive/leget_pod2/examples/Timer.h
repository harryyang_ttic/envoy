// --------------------------------------------------------------
/** @file Timer.h
 ** @brief A timer class to provide some basic profiling information.
 **
 ** A timer class to provide some basic profiling information for use in spot_text.
 **
 ** @author Ingmar Posner
 ** @date 01/04/2011
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#ifndef __LEGET_TIMER_H__
#define __LEGET_TIMER_H__

#include <stdio.h>
#include <sys/time.h>
#include <string>

class Timer
{
public:

	// --------------------------------------------------------------
	/**
	 * @brief Constructor.
	 * Construct and start a silent timer.
	 */
	// --------------------------------------------------------------
	Timer()
    {
        _bVerbose = false;
        
        // start timer
        start();
    }

    
	// --------------------------------------------------------------
    /**
     * @brief Constructor.
     * Construct and start a verbose timer.
     * @param sTag	The tag to print with the timing information.
     */
	// --------------------------------------------------------------
	Timer(std::string sTag)
    {
        _sTag = sTag;
        _bVerbose = true;
        
        // start timer
        start();
    }

    
	// --------------------------------------------------------------
	/**
     * @ brief Stop the timer.
     * Stop the timer and obtain the timing information
     * @return The time elapsed in ms since start was called.
     */
	// --------------------------------------------------------------
	double stop()
    {
        gettimeofday(&_timeVal,NULL);
        double dfStop = _timeVal.tv_sec + _timeVal.tv_usec / 1000000.0;
        
        double _dfElapsed = (dfStop - _dfStart)*1000;
        
        if (_bVerbose)
            printf("%s: %.2f [ms]\n", _sTag.c_str(), _dfElapsed);
        
        return _dfElapsed;
    }
    

private:
    
	// --------------------------------------------------------------
	/**
     * @brief Start a timer.
     * Set start time.
     */
	// --------------------------------------------------------------
	void start()
    {
        gettimeofday(&_timeVal, NULL);
        _dfStart = _timeVal.tv_sec + _timeVal.tv_usec / 1000000.0;
    }
    
	// -----------------
    // ---- Members ----
	// -----------------
    std::string _sTag;		/**< The tag to be printed with the timing information. */
    double      _dfStart;	/**< The start time. */
    double      _dfElapsed; /**< The elapsed time. */
	timeval     _timeVal;	/**< Time information container. */
    bool        _bVerbose;  /**< Housekeeping of verbosity. */
};


#endif // __LEGET_TIMER_H__

