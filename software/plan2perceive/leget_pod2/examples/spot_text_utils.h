// --------------------------------------------------------------
/** @file spot_text_utils.h
 ** @brief Spot_text annotation utilities.
 **
 ** Annotation and output utilities for spot_text.
 **
 ** @author Ingmar Posner
 ** @date 24/03/2011
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#ifndef __SPOT_TEXT_UTILS_H__
#define __SPOT_TEXT_UTILS_H__

# define BOOST_FILESYSTEM_VERSION 2
#include <boost/filesystem.hpp>
#include "leget/TextSpotter.h"

namespace fs = boost::filesystem;


// --------------------------------------------------------------
/**
 * @brief Annotate with corrected text.
 * Annotate an image with the spelling-corrected detections produced by leget.
 * This function produces a directory of images.
 * @param opBasePath	The output base path.
 * @param img			The image to annotate.
 * @param proofedWords	The container of spelling-corrected detections provided by leget.
 * @param postThresh	The threshold on the word-posterior below which detections are not used for annotation. [0.0]
 */
// --------------------------------------------------------------
void annotateWithCorrectedText(const fs::path& opBasePath, 
                               cv::Mat &img,
                               const leget::TextSpotter::ProofedWords& proofedWords,
                               const double postThresh=0.0);

// --------------------------------------------------------------
/**
 * @brief Annotate with raw OCR output.
 * Annotate an image with all the raw OCR output associated with the detections produced by leget.
 * This function produces a directory of images.
 * @param opBasePath	The output base path.
 * @param img			The image to annotate.
 * @param ocrWords		The container of raw OCR output associated with detections provided by leget.
 */
// --------------------------------------------------------------
void annotateWithRawOcr(const fs::path& opBasePath, 
                        const cv::Mat& img, 
                        const leget::TextSpotter::OcrWords& ocrWords);


// --------------------------------------------------------------
/**
 * @brief Annotate with putative text regions.
 * Annotate an image with all the putative text regions found by leget.
 * @param opBasePath 	The output base path.
 * @param img			The image to annotate.
 * @param rois			The container of putative text regions provided by leget.
 */
// --------------------------------------------------------------
void annotateWithPutativeTextRegions(const fs::path& opBasePath, 
                                     const cv::Mat &img, 
                                     const leget::TextSpotter::TextBlocks& rois);


// --------------------------------------------------------------
/**
 * @brief Annotate with cascade detections.
 * Annotate an image with all the cascade detections found by as part of the leget pipeline.
 * @param opBasePath	The output base path.
 * @param img			The image to annotate.
 * @param detections	The container of cascade detections provided by leget.
 */
// --------------------------------------------------------------
void annotateWithCascadeDetections(const fs::path& opBasePath, 
                                   const cv::Mat &img, 
                                   const leget::TextSpotter::Detections& detections);


// --------------------------------------------------------------
/**
 * @brief A helper function to draw the actual annotation.
 * Takes a text string and a rectangle location and dimensions and produces the actual image annotation.
 * @param img	The image to annotate.
 * @param str	The string annotation.
 * @param rect	The box annotation associated with the string.
 */
// --------------------------------------------------------------
void annotate(cv::Mat &img,
		      const std::string& str,
		      const cv::Rect& rect);


/**
 * @brief Dump leget output to xml.
 * Produce an xml file of detected text for further processing. Note that using anything other than an xml format is
 * difficult since the ocr output can comprise any character - hence simple output delimiting is tricky.
 * @param opBasePath 	The output base path.
 * @param words			The corrected words (this encompasses also the raw ocr output and confidences!).
 * @param fullImagePath	The full image path.
 * @param xResolution	The x-resolution at which the image was processed.
 * @param yResolution	The y-resolution at which the image was processed.
 */
void dumpXml(const fs::path& opBasePath,
			 const leget::TextSpotter::ProofedWords& words,
			 const fs::path& fullImagePath,
			 const unsigned int& xResolution,
			 const unsigned int& yResolution);


/**
 * @brief Dump the voting array to a text file.
 * Write the voting array to a text file so that it can be analyzed using third-party software such as Matlab or Python.
 * @param opBasePath	The output base path.
 * @param mat			The voting array.
 */
void dumpVotesToText(const fs::path& opBasePath,
					 const leget::TextSpotter::VotesArray &mat);



#endif // __SPOT_TEXT_UTILS_H__
