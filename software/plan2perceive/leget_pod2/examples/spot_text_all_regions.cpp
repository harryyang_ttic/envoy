// --------------------------------------------------------------
/** @file spot_text.cpp
 ** @brief A command line tool for spotting text. Also a usage example for libleget.
 **
 ** This command line tool showcases the basic functionality of libleget. It takes an image and some basic command line switches
 ** and produces annotated output. Output from every stage of the leget pipeline is shown: cascade detections and putative text regions
 ** are shown in single images. For clarity, text annotations for raw OCR output as well as the corrected system output are provided in separate sub-directories
 ** with one annotation per image.
 **
 ** The default values for the command line parameters are those found to work well on a variety of data. However, the command line options
 ** are not exhaustive in terms of versatility. See the main Leget header TextSpotter.h for other system parameters.
 **
 ** @author Ingmar Posner
 ** @date 01/04/2011
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

# define BOOST_FILESYSTEM_VERSION 2

#include <iostream>
#include <highgui.h>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include "leget/TextSpotter.h"
#include "Timer.h"
#include "spot_text_utils.h"
//#include "stereovision.h"
//#include <ptp-stereo/stereo_engine_t.hpp>

namespace po = boost::program_options;
namespace fs = boost::filesystem;
//using namespace ptp;
//using namespace ptp::stereo;


int main(int argc, char* argv[])
{
    
    // ===========================
    // Parse command line options
    // ===========================
    
    // default values 
    fs::path ipPath;
    fs::path opBasePath(".");
    unsigned int numScales = 10;
    unsigned int winBaseWidth = 30;
    unsigned int winBaseHeight = 15;
    unsigned int xStep = 4;
    unsigned int yStep = 2;    
    cv::Size maxDims;
    maxDims.width = 1024;
    maxDims.height = 768;
    double postThresh = 0.0;
    double singleRelThresh = 0.4;
    bool useRelThreshGrid = false;
    bool flipImageAboutXAxis = false;
    bool cropBottomHalf = false;
    
    try 
    {
        po::options_description desc("This program takes an image and uses the Leget library to produce a variety of annotated output as well as an xml file containing the annotations. \nAllowed options");
        desc.add_options()
        ("help", "produce help message")
        ("input,i", po::value<std::string>(), "The image to process.")
        ("output,o", po::value<std::string>(), "The base path for the output. The path can be relative or absolute. [pwd]")
        ("xStep", po::value<unsigned int>(), "Sliding window x-step in pixels. [4]")
        ("yStep", po::value<unsigned int>(), "Sliding window y-step in pixels. [2]")
        ("numScales", po::value<unsigned int>(), "Number of scales to run detector over. [10]")
        ("baseWinWidth", po::value<unsigned int>(), "Width in pixels of smallest sliding window. [30]")
        ("baseWinHeight", po::value<unsigned int>(), "Height in pixels of smallest sliding window. [15]")
        ("maxImWidth", po::value<unsigned int>(), "The maximum image width (in pixels) to process. [1024]")
        ("maxImHeight", po::value<unsigned int>(), "The maximum image height (in pixels) to process. [768]")
        ("postThresh", po::value<double>(), "The threshold applied to the word posterior (for displaying only!). [0.0]")
        ("singleRelThresh", po::value<double>(), "Uses only a single relative threshold provided as argument. [0.4]")
        ("useRelThreshGrid", po::value<bool>()->zero_tokens(), "Switch to overwrite single relative threshold setting to apply a grid of thresholds and automatically select the best one. [false]")
	  ("flipImageAboutXAxis", po::value<bool>()->zero_tokens(), "Flip image about the x axis before processing")
	  ("cropBottomHalf", po::value<bool>()->zero_tokens(), "Crop the bottom half of the image (useful for stereo images")
        ;
        
        po::variables_map vm;        
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);    
        
        if (vm.empty())
        {
            std::cout << desc << "\n";
            return 1;
        }
        
        if (vm.count("help")) 
        {
            std::cout << desc << "\n";
            return 1;
        }
        
        if (vm.count("input")) 
        {
            ipPath = fs::system_complete( fs::path( vm["input"].as<std::string>() ) ); 
        }

        
        if (vm.count("output")) 
        {
            opBasePath = fs::system_complete( fs::path( vm["output"].as<std::string>() ) ); 
        }
        
        if (vm.count("xStep")) 
        {
            xStep = vm["xStep"].as<unsigned int>(); 
        }
        
        if (vm.count("yStep")) 
        {
            yStep = vm["yStep"].as<unsigned int>(); 
        }
        
        if (vm.count("numScales")) 
        {
            numScales = vm["numScales"].as<unsigned int>(); 
        }
        
        if (vm.count("baseWinWidth")) 
        {
            winBaseWidth = vm["baseWinWidth"].as<unsigned int>(); 
        }
        
        if (vm.count("baseWinHeight")) 
        {
            winBaseHeight = vm["baseWinHeight"].as<unsigned int>(); 
        }
        
        if (vm.count("maxImWidth")) 
        {
        	maxDims.width = vm["maxImWidth"].as<unsigned int>();
        }
        
        if (vm.count("maxImHeight")) 
        {
        	maxDims.height = vm["maxImHeight"].as<unsigned int>();
        }
        
        if (vm.count("postThresh"))
        {
        	postThresh = vm["postThresh"].as<double>();
        }

        if (vm.count("singleRelThresh"))
        {
        	singleRelThresh = vm["singleRelThresh"].as<double>();
        }

        if (vm.count("useRelThreshGrid"))
        {
        	useRelThreshGrid = true;
        }
	
	if (vm.count("flipImageAboutXAxis"))
        {
        	flipImageAboutXAxis = true;
        }
	
	if (vm.count("cropBottomHalf")){
	  cropBottomHalf = true;
	}
    }
    catch(...) 
    {
        std::cerr << "error: parsing command line options" << std::endl;
        return 1;
    }


    // ===========================
    // Get the image
    // ===========================
    
    // check that the image exists
    if( !fs::exists(ipPath) )
    {
        std::cout << "Could not find image file: " << ipPath.string() << std::endl;
        return 1;
    }
    
    // load the image
    Timer t1("Loading image");
    std::string imageFile = ipPath.string();
    IplImage* img = cvLoadImage(imageFile.c_str());
    if(!img)
    {
        std::cout << "Could not load image file: " << imageFile << std::endl;
        return 1;
    }
    cv::Mat imgMat(img);
    t1.stop();
    

    // vector of rotations to use
    std::vector<double> rot_degree_vector;
    rot_degree_vector.push_back( 0.0 );
    

    // ===========================
    // flip if required
    // ===========================
    
    if( flipImageAboutXAxis ) {
      //rot_degree_vector.push_back( 180 );
      cv::Mat temp;
      cv::warpAffine( imgMat, temp, 
		      cv::getRotationMatrix2D( cv::Point2f( imgMat.cols / 2.0f, imgMat.rows / 2.0f ), 180, 1.0 ), imgMat.size() );
      imgMat = temp;
    }

    // ===========================
    // crop bottom half if wanted
    // ===========================
    
    if( cropBottomHalf ) {
      imgMat = imgMat( cv::Range(0, imgMat.rows / 2), cv::Range::all() );
    }

    // ===========================
    // Resize if required
    // ===========================
    
    // resize to maximum only if required
    double heightScaleFactor = static_cast<double>(maxDims.height) / static_cast<double>(imgMat.rows);
    double widthScaleFactor = static_cast<double>(maxDims.width) / static_cast<double>(imgMat.cols);
    double requiredScaling = std::min(heightScaleFactor, widthScaleFactor);
    cv::Mat resizedImg;
    if( requiredScaling < 1.0)
    {
    	cv::Size newSize;
    	newSize.width = floor(requiredScaling * static_cast<double>(imgMat.cols));
    	newSize.height = floor(requiredScaling * static_cast<double>(imgMat.rows));
    	cv::resize(imgMat, resizedImg, newSize, 0, 0);
    }
    else
    {
    	resizedImg = imgMat;
    }


    // ===========================
    // THE MEAT: Perform TextSpotting
    // ===========================
    
    try
    {
        // make yourself a spotter
        // NOTE: here we are using the leget system defaults - see leget/TextSpotter.h header for details!
        leget::TextSpotter spotter;
	spotter.setFilterWords( false );
        
        // let's do a cunning thing and set a _range_ of relative thresholds which leget will work through and select the best for us.
        // this takes more time (for identifyAndParse(...)) than using a single threshold (the leget default is set to 0.4 only, which performs pretty well all on its own)
        // but can produce better results. We do it here mainly to showcase the capability of leget to do it...
        leget::TextSpotter::ThresholdSet relThresholds;
        if( useRelThreshGrid )
        {
        	relThresholds.insert(0.1);
        	relThresholds.insert(0.2);
        	relThresholds.insert(0.3);
        	relThresholds.insert(0.4);
        	relThresholds.insert(0.5);
        	relThresholds.insert(0.6);
        	relThresholds.insert(0.7);
        	relThresholds.insert(0.8);
        	relThresholds.insert(0.9);
        	spotter.setRelativeVoteThresholds(relThresholds);
        }
        else
        {
        	relThresholds.insert(singleRelThresh);
        	spotter.setRelativeVoteThresholds(relThresholds);
        }


        // produce some user-friendly output
        std::cout << "------------------------------------------------" << std::endl;
        std::cout << "Using the following settings:" << std::endl;
        std::cout << "input image: " << ipPath.string() << std::endl;
        std::cout << "output directory: " << opBasePath.string() << std::endl;
        std::cout << "window base width: " << winBaseWidth << " pix" << std::endl;
        std::cout << "window base height: " << winBaseHeight << " pix" <<  std::endl;
        std::cout << "number of scales to apply: " << numScales << std::endl;
        std::cout << "x-step: " << xStep << std::endl;
        std::cout << "y-step: " << yStep << std::endl;
        std::cout << "max image size before scaling is applied: [" << maxDims.width << " x " << maxDims.height << "] pix" << std::endl;
        std::cout << "processing image at size: [" << resizedImg.cols << " x " << resizedImg.rows << "] pix" << std::endl;
        std::cout << "word-posterior threshold (display only): " << postThresh << std::endl;
        std::cout << "using relative threshold(s): [";
        leget::TextSpotter::ThresholdSet::const_iterator citRelThreshs;
        for( citRelThreshs = relThresholds.begin(); citRelThreshs != relThresholds.end(); ++citRelThreshs )
        	std::cout << " " << *citRelThreshs;
        std::cout << " ]" << std::endl;
	std::cout << "rotations to try: [";
	for( size_t i = 0; i < rot_degree_vector.size(); ++i ) {
	  std::cout << " " << rot_degree_vector[i];
	}
	std::cout << " ]" << std::endl;
        std::cout << "------------------------------------------------" << std::endl;

        // NOTE: We'll perform spotting here step-by-step to provide timing information.
        //       Being able to do this also means you can forego the spelling correction if you don't need it.
        //       An alternative to the following 'step-by-step' approach is the 'all-in-one' SPOT function:
        //       spotter.spot(resizedImg, winBaseWidth, winBaseHeight, xStep, yStep, numScales);
        //       See leget/TextSpotter.h for details.
        
        Timer t2("*** Spotting Total ***");
        
        // convert to grayscale as input to pipeline (spot does this for you!)
        cv::Mat imGrayOrig;
	cv::Mat imGrayRot;
        cv::cvtColor(resizedImg, imGrayOrig, CV_RGB2GRAY);
	cv::Point2f center_point( resizedImg.cols / 2.0f, resizedImg.rows / 2.0f );


	// get some variable that will be incrementally filled 
	// with ortated image results
	leget::TextSpotter::Detections detections;
	leget::TextSpotter::TextBlocks blocks;
	leget::TextSpotter::OcrWords ocrWords;
	leget::TextSpotter::ProofedWords proofedWords;
        
	for( size_t rot_index = 0; rot_index < rot_degree_vector.size(); ++rot_index) {

	  // Rotate by wanted rotation
	  cv::warpAffine( imGrayOrig, imGrayRot, cv::getRotationMatrix2D( center_point, rot_degree_vector[rot_index], 1.0 ), imGrayOrig.size() );


	  Timer t3("Detection");
	  spotter.detect(imGrayRot, winBaseWidth, winBaseHeight, xStep, yStep, numScales);
	  t3.stop();
	  
	  Timer t4("Identify and parse text regions");
	  spotter.identifyAndParse(imGrayRot);
	  t4.stop();
	  
	  Timer t5("Check spelling");
	  spotter.checkSpelling();
	  t5.stop();
	  
	  // stop total timer
	  t2.stop();

	  // add to results
	  leget::TextSpotter::Detections d = spotter.getDetections();
	  detections.insert( detections.end(), d.begin(), d.end() );
	  leget::TextSpotter::TextBlocks b = spotter.getTextRegions();
	  blocks.insert( blocks.end(), b.begin(), b.end() );
	  leget::TextSpotter::OcrWords o = spotter.getOcrOutput();
	  ocrWords.insert( ocrWords.end(), o.begin(), o.end() );
	  leget::TextSpotter::ProofedWords p = spotter.getProofedWords();
	  proofedWords.insert( proofedWords.end(), p.begin(), p.end() );
	}
        
        
        // ===========================
        // Produce some output
        // ===========================
        
        // ... the raw detections
        cv::Mat detectionIm = resizedImg.clone();
        annotateWithCascadeDetections(opBasePath, detectionIm, detections);
        
        // ... the text rois
        cv::Mat roiIm = resizedImg.clone();
        annotateWithPutativeTextRegions(opBasePath, roiIm, blocks);
        
        // ... the raw ocr output
        annotateWithRawOcr(opBasePath, resizedImg , ocrWords);
        
        // ... the corrected annotations
        annotateWithCorrectedText(opBasePath, resizedImg, proofedWords, postThresh);
        
        // ... dump information about all words found to xml
        dumpXml(opBasePath, proofedWords, ipPath, resizedImg.cols, resizedImg.rows);

        // ... dump voting array
        leget::TextSpotter::VotesArray votes = spotter.getVotes();
        dumpVotesToText(opBasePath, votes);


    }
    catch (leget::error::Exception &e)
    {
        e.debugPrint();
        return 1;
    }
    
    // ===========================
    // Do housekeeping
    // ===========================
    
    // free the image
    cvReleaseImage(&img);
    
}
