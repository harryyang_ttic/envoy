// --------------------------------------------------------------
/** @file spot_text_utils.cpp
 ** @brief Implementation of spot_text annotation and output utilities.
 **
 ** Annotation utilities for spot_text.
 **
 ** @author Ingmar Posner 
 ** @date 24/03/2011
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#include <fstream>
#include <boost/lexical_cast.hpp>
#include "spot_text_utils.h"
#include <tiny-xml/tinyxml.h>


// ===================================================================
void annotateWithCorrectedText(const fs::path& opBasePath,
                               cv::Mat& img,
                               const leget::TextSpotter::ProofedWords& proofedWords,
                               const double dfPostThresh) 
{
    fs::path correctedPath = opBasePath / "corrected";
    
    // first, let's check whether the directory exists ...
    if( fs::exists(correctedPath) )
    {
        // ... if it does, remove all old data   
        fs::remove_all(correctedPath);
    }
    // and recreate the empty dir
    boost::filesystem::create_directories(correctedPath);
    
    unsigned int index = 0;
    leget::TextSpotter::ProofedWords::const_iterator citWords;
    leget::TextSpotter::ProofedWords::const_iterator citTextEnd = proofedWords.end();
    for (citWords = proofedWords.begin(); citWords != citTextEnd; ++citWords, ++index)
    {
        // clone the image so we can show individual annotations
        cv::Mat clonedImg = img.clone();
        
        std::stringstream ssFullLabel;
        ssFullLabel.precision(2);
        if (citWords->getPosterior() < dfPostThresh)
            continue;
        
        // make the annotation
        ssFullLabel << citWords->getCorrectedLabel() << " [p=" << citWords->getPosterior() << "] ";
        std::string sText = ssFullLabel.str();
        
        // perform annotation
        cv::Rect rect;
        citWords->getBoundingRect(rect);
        annotate(clonedImg, sText, rect);
        
        // write the image
        fs::path imgPath = correctedPath / (boost::lexical_cast<std::string>(index) + ".jpg");
        std::cout << "writing " << imgPath.string() << std::endl;
        cv::imwrite(imgPath.string(), clonedImg); 
    }
}


// ===================================================================
void annotateWithRawOcr(const fs::path& opBasePath, 
                        const cv::Mat& img,
                        const leget::TextSpotter::OcrWords& ocrWords) 
{
    fs::path rawOcrPath = opBasePath / "rawOcr";
    
    // first, let's check whether the directory exists ...
    if( fs::exists(rawOcrPath) )
    {
        // ... if it does, remove all old data   
        fs::remove_all(rawOcrPath);
    }
    // and recreate the empty dir
    boost::filesystem::create_directories(rawOcrPath);
    
    unsigned int index = 0;
    leget::TextSpotter::OcrWords::const_iterator citWords;
    leget::TextSpotter::OcrWords::const_iterator citTextEnd = ocrWords.end();
    for (citWords = ocrWords.begin(); citWords != citTextEnd; ++citWords, ++index)
    {
        // clone the image so we can show individual annotations
        cv::Mat clonedImg = img.clone();
        
        std::stringstream ssFullLabel;
        ssFullLabel.precision(2);
        
        // make the annotation
        ssFullLabel << citWords->_word << " [" << citWords->_confidence << "] ";
        std::string sText = ssFullLabel.str();
        
        // perform annotation
        annotate(clonedImg, sText, citWords->_boundingBox);
        
        // write the image
        fs::path imgPath = rawOcrPath / (boost::lexical_cast<std::string>(index) + ".jpg");
        std::cout << "writing " << imgPath.string() << std::endl;
        cv::imwrite(imgPath.string(), clonedImg); 
    }
}


// ===================================================================
void annotateWithPutativeTextRegions(const fs::path& opBasePath, 
                                     const cv::Mat& img, 
                                     const leget::TextSpotter::TextBlocks& rois)  
{
    // clone the image so we can show individual annotations
    cv::Mat clonedImg = img.clone();
    
    leget::TextSpotter::TextBlocks::const_iterator citRoi;
    leget::TextSpotter::TextBlocks::const_iterator citRoiEnd = rois.end();
    cv::Point_<unsigned int> pt1, pt2;
    cv::Scalar_<unsigned int> colour;
    colour[2] = 255;
    std::cout << "Drawing " << rois.size() << " putative text regions." << std::endl;
    for (citRoi = rois.begin(); citRoi != citRoiEnd; ++citRoi)
    {
        pt1.x = citRoi->_roi.x;
        pt1.y = citRoi->_roi.y;
        pt2.x = citRoi->_roi.x + citRoi->_roi.width;
        pt2.y = citRoi->_roi.y + citRoi->_roi.height;
        
        cv::rectangle(clonedImg, pt1, pt2, colour, 2); 
    }
    
    // check output path exists - otherwise create it
    if( !fs::exists(opBasePath) )
    {
        fs::create_directories(opBasePath);
    }
    
    // write to file 
    fs::path imgPath = opBasePath / "textRegions.jpg";
    std::cout << "writing " << imgPath.string() << std::endl;
    cv::imwrite(imgPath.string(), clonedImg); 
}


// ===================================================================
void annotateWithCascadeDetections(const fs::path& opBasePath, 
                                   const cv::Mat &img, 
                                   const leget::TextSpotter::Detections& detections) 
{
    // clone the image so we can show individual annotations
    cv::Mat clonedImg = img.clone();
    
    leget::TextSpotter::Detections::const_iterator citRect;
    leget::TextSpotter::Detections::const_iterator citRectEnd = detections.end();
    cv::Point_<unsigned int> pt1, pt2;
    cv::Scalar_<unsigned int> colour;
    colour[2] = 255;
    std::cout << "Drawing " << detections.size() << " cascade detections." << std::endl;
    for (citRect = detections.begin(); citRect != citRectEnd; ++citRect)
    {
        pt1.x = citRect->_rect.x;
        pt1.y = citRect->_rect.y;
        pt2.x = citRect->_rect.x + citRect->_rect.width;
        pt2.y = citRect->_rect.y + citRect->_rect.height;
        
        cv::rectangle(clonedImg, pt1, pt2, colour, 2); 
    }
    
    // check output path exists - otherwise create it
    if( !fs::exists(opBasePath) )
    {
        fs::create_directories(opBasePath);
    }

    // write to file 
    fs::path imgPath = opBasePath / "detections.jpg";
    std::cout << "writing " << imgPath.string() << std::endl;
    cv::imwrite(imgPath.string(), clonedImg); 


    //DEBUG
    // dump scale and score info
    std::ofstream osFile;
    osFile.open((opBasePath / "scaleScoreInfo.txt").string().c_str());
    for (citRect = detections.begin(); citRect != citRectEnd; ++citRect)
    {
    	osFile << citRect->_scale << " " << citRect->_score << std::endl;
    }
    osFile.close();

}


// ===================================================================
void annotate(cv::Mat &img, 
              const std::string& str, 
              const cv::Rect& rect) 
{
    cv::Scalar colour(0);
    colour[2] = 135;
    
    cv::Point pt1, pt2;
    pt1.x = rect.x;
    pt1.y = rect.y;
    pt2.x = rect.x + rect.width;
    pt2.y = rect.y + rect.height;
    
    cv::rectangle(img, pt1, pt2, colour, 2);
    
    
    int nFontFace = cv::FONT_HERSHEY_PLAIN;
    double dfFontScale = 1.5;
    int nThickness = 2;
    int nBaseline = 0;
    cv::Size textSize = cv::getTextSize(str, nFontFace, dfFontScale, nThickness, &nBaseline);
    nBaseline += nThickness;
    
    // center the text
    //Point textOrg((img.cols - textSize.width)/2,
    //              (img.rows + textSize.height)/2);
    cv::Point rectOrg(rect.x, rect.y);
    cv::Point textOrg(rect.x, rect.y - 5);
    
    
    // draw the box
    //rectangle(img, textOrg + cv::Point(0, nBaseline), textOrg + cv::Point(textSize.width, -textSize.height), colour, CV_FILLED);
    rectangle(img, rectOrg, rectOrg + cv::Point(textSize.width, -(textSize.height+12) ), colour, CV_FILLED);
    
    // write the text itself
    putText(img, str, textOrg, nFontFace, dfFontScale, cv::Scalar::all(255), nThickness, 1);
}




// ===================================================================
void dumpXml(const fs::path& opBasePath,
			 const leget::TextSpotter::ProofedWords& words,
			 const fs::path& fullImagePath,
			 const unsigned int& xResolution,
			 const unsigned int& yResolution)
{

	std::string sTemp;

	TiXmlDocument doc;
	// the opening declaration
	TiXmlDeclaration * pDecl = new TiXmlDeclaration("1.0", "", "");
	doc.LinkEndChild(pDecl);

	// the imageName tag
	TiXmlElement * pImageNameTag = new TiXmlElement("image");
	sTemp = fullImagePath.filename();
	TiXmlText * pImageNameText = new TiXmlText(sTemp.c_str());
	pImageNameTag->LinkEndChild(pImageNameText);
	doc.LinkEndChild(pImageNameTag);

	// the x_resolution tag
	TiXmlElement * pXResolutionTag = new TiXmlElement("x_resolution");
	sTemp = boost::lexical_cast<std::string>(xResolution);
	TiXmlText * pXResolutionText = new TiXmlText(sTemp.c_str());
	pXResolutionTag->LinkEndChild(pXResolutionText);
	doc.LinkEndChild(pXResolutionTag);

	// the y_resolution tag
	TiXmlElement * pYResolutionTag = new TiXmlElement("y_resolution");
	sTemp = boost::lexical_cast<std::string>(yResolution);
	TiXmlText * pYResolutionText = new TiXmlText(sTemp.c_str());
	pYResolutionTag->LinkEndChild(pYResolutionText);
	doc.LinkEndChild(pYResolutionTag);

	// tagged rectangles
	leget::TextSpotter::ProofedWords::const_iterator citProofed;
	leget::TextSpotter::ProofedWords::const_iterator citProofedEnd = words.end();
	for(citProofed = words.begin(); citProofed != citProofedEnd; ++citProofed)
	{
		TiXmlElement * pTaggedRect = new TiXmlElement("rect_roi");

		// the bounding box
		cv::Rect boundingBox;
		citProofed->getBoundingRect(boundingBox);

		// the x tag
		TiXmlElement * pXPositionTag = new TiXmlElement("x");
		sTemp = boost::lexical_cast<std::string>(boundingBox.x);
		TiXmlText * pXPositionText = new TiXmlText(sTemp.c_str());
		pXPositionTag->LinkEndChild(pXPositionText);
		pTaggedRect->LinkEndChild(pXPositionTag);

		// the y tag
		TiXmlElement * pYPositionTag = new TiXmlElement("y");
		sTemp = boost::lexical_cast<std::string>(boundingBox.y);
		TiXmlText * pYPositionText = new TiXmlText(sTemp.c_str());
		pYPositionTag->LinkEndChild(pYPositionText);
		pTaggedRect->LinkEndChild(pYPositionTag);

		// the width tag
		TiXmlElement * pWidthPositionTag = new TiXmlElement("width");
		sTemp = boost::lexical_cast<std::string>(boundingBox.width);
		TiXmlText * pWidthPositionText = new TiXmlText(sTemp.c_str());
		pWidthPositionTag->LinkEndChild(pWidthPositionText);
		pTaggedRect->LinkEndChild(pWidthPositionTag);

		// the height tag
		TiXmlElement * pHeightPositionTag = new TiXmlElement("height");
		sTemp = boost::lexical_cast<std::string>(boundingBox.height);
		TiXmlText * pHeightPositionText = new TiXmlText(sTemp.c_str());
		pHeightPositionTag->LinkEndChild(pHeightPositionText);
		pTaggedRect->LinkEndChild(pHeightPositionTag);

		// the raw ocr string
		TiXmlElement * pRawOcrStringTag = new TiXmlElement("raw_ocr_string");
		sTemp = citProofed->getRawLabel();
		TiXmlText * pRawOcrStringText = new TiXmlText(sTemp.c_str());
		pRawOcrStringTag->LinkEndChild(pRawOcrStringText);
		pTaggedRect->LinkEndChild(pRawOcrStringTag);

		// the ocr confidence
		TiXmlElement * pRawOcrConfidenceTag = new TiXmlElement("ocr_confidence");
		sTemp = boost::lexical_cast<std::string>(citProofed->getOCRConfidence());
		TiXmlText * pRawOcrConfText = new TiXmlText(sTemp.c_str());
		pRawOcrConfidenceTag->LinkEndChild(pRawOcrConfText);
		pTaggedRect->LinkEndChild(pRawOcrConfidenceTag);

		// the corrected string
		TiXmlElement * pCorrectedStringTag = new TiXmlElement("tag");
		sTemp = citProofed->getCorrectedLabel();
		TiXmlText * pCorrectedStringText = new TiXmlText(sTemp.c_str());
		pCorrectedStringTag->LinkEndChild(pCorrectedStringText);
		pTaggedRect->LinkEndChild(pCorrectedStringTag);

		// the word posterior
		TiXmlElement * pWordPostTag = new TiXmlElement("word_posterior");
		sTemp = boost::lexical_cast<std::string>(citProofed->getPosterior());
		TiXmlText * pWordPostText = new TiXmlText(sTemp.c_str());
		pWordPostTag->LinkEndChild(pWordPostText);
		pTaggedRect->LinkEndChild(pWordPostTag);


		// complete tagged rectangle
		doc.LinkEndChild(pTaggedRect);
	}

    // write the xml file
	std::string fName = (opBasePath / "wordDetections.xml").string();
	doc.SaveFile( fName.c_str() );
}




//===================================================================
void dumpVotesToText(const fs::path& opBasePath,
					 const leget::TextSpotter::VotesArray &mat)
{

	std::string fName = (opBasePath / "votesMat.txt").string();

	std::ofstream osFile;
    osFile.open(fName.c_str());
    for (int nX = 0; nX < mat.rows; ++nX)
    {
        for (int nY = 0; nY < mat.cols; ++nY)
        {
            osFile << mat.at<float>(nX, nY) << ", ";
        }
        osFile << std::endl;
    }
    osFile.close();
}


