// --------------------------------------------------------------
/** @file TextSpotter.h
 ** @brief The main api for libleget.
 **
 ** This is the main api for the leget text-spotting library.
 **
 ** NOTE: Leget depends on the tesseract OCR engine, which is NOT THREAD-SAFE!
 **       Consequently, leget is not thread-safe either (though some parts are trivially
 **       parallelisable - this is future work).
 **
 ** @author Ingmar Posner
 ** @date 25/6/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------


#ifndef __LEGET_TEXT_SPOTTER_H__
#define __LEGET_TEXT_SPOTTER_H__

#include <cv.h>
#include <highgui.h>
#include "tesseract/baseapi.h"
#include "leget/boosting/BoostedCascade.h"
#include "leget/misc/Detection.h"
#include "leget/misc/Word.h"
#include "leget/misc/SpellChecker.h"
#include "leget/misc/TextSpottingUtils.h"
#include "leget/misc/Exceptions.h"


namespace leget
{

	// --------------------------------------------------------------
    /** @brief The main text-spotting class.
     * Make yourself one of these to perform text-spotting.
     */
    // --------------------------------------------------------------
	class TextSpotter
    {
        
    public:
        
        // local typedefs 
        typedef std::vector< std::vector<cv::Point> >   Contours;
        typedef std::set<double>                        ThresholdSet;
        typedef cv::Mat			                        VotesArray;
        typedef std::deque<Detection>                   Detections;
        typedef std::deque<TextRoi>                     TextBlocks; 
        typedef std::deque<OcrWord>                     OcrWords;
        typedef std::deque<Word>                        ProofedWords;
        
        
        // ************************************************
        // ***************** CON-/DESTRUCT ****************
        // ************************************************
        // --------------------------------------------------------------
        /**
         * @brief Constructor.
         * Instantiates a TextSpotter object. The default values provided for the system parameters
         * should provide a decent first shot.
         * @param absVoteThresh		The absolute vote threshold applied when rejecting spurious cascade detections. [20.0]
         * @param relVoteThreshs	The relative (to local maximum) vote threshold applied when segmenting text regions.
         *							When more than one is provided all are tried and the best result is used. [0.4]
         * @param minAreaThresh		The minimum area covered by a text region. Note that tesseract dislikes images less than 15px high. [150]
         * @param alpha				The sensitivity of the spell checker. [20.0]
         * @param pageSegMode		The tesseract page-segmentation mode. [PSM_AUTO]
         * @param cascadeLocation	The directory where the cascade detector lives (i.e. the Cascade.Clf file). [The one shipped with Leget]
         * @param dictionaryFile	The file where the dictionary lives. [The one shipped with Leget]
         */
        // --------------------------------------------------------------
        TextSpotter(const double& absVoteThresh=500.0,
                    const ThresholdSet& relVoteThreshs=ThresholdSet(), 
                    const int& minAreaThresh=150,
                    const double& alpha=20.0,
                    const tesseract::PageSegMode pageSegMode=tesseract::PSM_AUTO,
                    const std::string& cascadeLocation=DEFAULT_CASCADE,
                    const std::string& dictionaryFile=DEFAULT_DICTIONARY) 
	  : _cascade(cascadeLocation),
	  _absoluteVoteThreshold(absVoteThresh),
	  _relativeVoteThresholds(relVoteThreshs), 
	  _minAreaThreshold(minAreaThresh), 
	  _alpha(alpha),
	  _pageSegMode(pageSegMode),
	  _filter_words( false )
	    {
        	// set default for relativeVoteThresholds
            if (_relativeVoteThresholds.empty())
            {
                _relativeVoteThresholds.insert(0.4);
            }
            
            // ****************************
            // set up TESSERACT
            // ****************************
            
            // Start tesseract. Returns zero on success and -1 on failure.
            // The datapath must be the name of the data directory (no ending /) or
            // some other file in which the data directory resides (for instance argv[0].)
            // The language is (usually) an ISO 639-3 string or NULL will default to eng.
            if(_ocrEngine.Init(NULL, "eng") != 0)
            {
                std::cerr << "FATAL ERROR: TextSpotter::TextSpotter() - Failed to initialise Tesseract OCR engine..." << std::endl;
                exit(1);
            }
            
            // Set the current page segmentation mode. Tesseract defaults to PSM_SINGLE_BLOCK.
            // NOTE: tesseract::PSM_AUTO causes Tesseract to "adapt" - so results are decent but somewhat stochastic
            //       tesseract::PSM_SINGLE_BLOCK gives less good result but is repeatable. Use this when using multiple thresholds.
            _ocrEngine.SetPageSegMode(_pageSegMode);
            
            
            // ****************************
            // initialize spell checker
            // ****************************
            try 
            {
                _spellChecker.initialise(dictionaryFile, _alpha);
            }
            catch (error::IOError& e)
            {
                e.debugPrint();
                exit(1);
            }
        }
        

        // --------------------------------------------------------------
        /**
         * @brief Destructor.
         * Takes care of shutting down the tesseract OCR engine.
         */
        // --------------------------------------------------------------
        ~TextSpotter()
        {
            // End tesseract.
            _ocrEngine.End();
        }
        
        
        // *******************************
        // ********** UTILITIES **********
        // *******************************

        // --------------------------------------------------------------
        /**
         * @brief All-in-one full-on text-spotting.
         * This function performs detection, parsing and spelling correction all in one.
         * It also converts any input image to gray-scale.
         * @param img			The image to operate on (can be colour).
         * @param winWidth		The width (px) of the smallest sliding window of the cascade detector.
         * @param winHeight 	The height (px) of the smallest sliding window of the cascade detector.
         * @param dx			The x-step size (px) of the sliding window classifier.
         * @param dy			The y-step size (px) of the sliding window classifier.
         * @param numScales		The number of scales to process the image at. [10]
         * @param scaleFactor	The scale factor. [1.3]
         */
        // --------------------------------------------------------------
        void spot(const cv::Mat &img, 
                  const unsigned int &winWidth, 
                  const unsigned int &winHeight,
                  const unsigned int &dx, 
                  const unsigned int &dy, 
                  const unsigned int numScales=10, 
                  const double scaleFactor=1.3);
        
        
        // --------------------------------------------------------------
        /**
         * @brief Perform detection only using the boosted cascade.
         * This guy only runs the boosted cascade classifier and stores the rectangles deemed to
         * contain text for all scales. Upon calling this _detections will contain valid results.
         * @param img			The _gray-scale_ image to operate on.
         * @param winWidth		The width (px) of the smallest sliding window of the cascade detector.
         * @param winHeight 	The height (px) of the smallest sliding window of the cascade detector.
         * @param dx			The x-step size (px) of the sliding window classifier.
         * @param dy			The y-step size (px) of the sliding window classifier.
         * @param numScales		The number of scales to process the image at. [10]
         * @param scaleFactor	The scale factor. [1.3]
        */
        // --------------------------------------------------------------
        void detect(const cv::Mat &img, 
                    const unsigned int &winWidth, 
                    const unsigned int &winHeight,
                    const unsigned int &dx, 
                    const unsigned int &dy, 
                    const unsigned int numScales=10, 
                    const double scaleFactor=1.3);
        
        
        // --------------------------------------------------------------
        /**
         * @brief Identify text regions and apply OCR.
         * Counts the votes cast by the cascade detections and thresholds (absolute and relative)
         * to determine the putative text regions. These are then passed to the OCR engine to get the
         * raw OCR strings, confidences and word bounding boxes.
         * This function must be called after detect(). Upon calling it _votes, _detectedTextRois
         * and _rawOcrWords contain valid output.
         * @param img	The _gray-scale_ image to operate on.
         */
        // --------------------------------------------------------------
        void identifyAndParse(const cv::Mat& img);
        

        // --------------------------------------------------------------
        /**
         * @brief Perform spell-checking of the raw ocr words.
         * Perform spell-checking of the raw ocr words.
         * This function must be called after identifyAndParse(). Upon calling it _correctedWords
         * contains valid output.
         */
        // --------------------------------------------------------------
        void checkSpelling();
        
        

        // *******************************
        // ********** ACCESSORS **********
        // *******************************

	// --------------------------------------------------------------
        /**
         * @brief Set On/Off word filtering for results.
         * Set whertehr to filter words ( true ) or not.
         * @param f	Are we filtering words.
         */
        // --------------------------------------------------------------
        inline void setFilterWords(const bool& f)
        {
            _filter_words = f;
        }
        


        // --------------------------------------------------------------
        /**
         * @brief Absolute vote threshold accessor.
         * Set absolute vote threshold.
         * @param thresh	The absolute vote threshold.
         */
        // --------------------------------------------------------------
        inline void setAbsoluteVoteThreshold(const double& thresh)
        {
            _absoluteVoteThreshold = thresh;
        }
        

        // --------------------------------------------------------------
        /**
         * @brief Absolute vote threshold accessor.
         * Get the absolute vote threshold currently used by the spotter object.
         * @return The absolute vote threshold.
         */
        // --------------------------------------------------------------
        inline double getAbsoluteVoteThreshold() const
        {
            return _absoluteVoteThreshold;
        }
        

        // --------------------------------------------------------------
        /**
         * @brief Relative vote threshold accessor.
         * Set the relative vote thresholds for local non-maximal suppression.
         * Note that the set size can be greater than one! If so, leget will go through them all
         * for every image and chose the one it deems best according to metric based on OCR confidences.
         * @param threshs	The set of relative vote thresholds (numbers between 0.0 and 1.0).
         */
        // --------------------------------------------------------------
        inline void setRelativeVoteThresholds(const ThresholdSet& threshs)
        {
            _relativeVoteThresholds = threshs;
        }
        

        // --------------------------------------------------------------
        /**
         * @brief Relative vote threshold accessor.
         * Get the set of relative vote thresholds.
         * @return The set of relative vote thresholds.
         */
        // --------------------------------------------------------------
        // get local non-maximal suppression threshold
        inline ThresholdSet getRelativeVoteThresholds() const
        {
            return _relativeVoteThresholds;
        }
        

        // --------------------------------------------------------------
        /**
         * @brief Minimum area threshold accessor.
         * Set the minimum area size for putative text regions.
         * @param thresh The minimum area threshold.
         */
        // --------------------------------------------------------------
        inline void setMinAreaThreshold(const int& thresh)
        {
            _minAreaThreshold = thresh;
        }


        // --------------------------------------------------------------
        /**
         * @brief Minimum area threshold accessor.
         * Get the minimum area size for putative text regions.
         * @return The minimum area threshold.
         */
        // --------------------------------------------------------------
        inline int getMinAreaThreshold() const
        {
            return _minAreaThreshold;
        }
        

        // --------------------------------------------------------------
        /**
         * @brief Spell-check sensitivity accessor.
         * Set the sensitivity of the spell-checker. This is the parameter for the
         * 'sensor model' - as it were.
         * @param alpha The spell-check sensitivity.
         */
        // --------------------------------------------------------------
        inline void setAlpha(const double& alpha)
        {
            _alpha = alpha;
        }


        // --------------------------------------------------------------
        /**
         * @brief Spell-check sensitivity accessor.
         * Get the sensitivity of the spell-checker.
         * @return The spell-check sensitivity.
         */
        // --------------------------------------------------------------
        inline double getAlpha() const
        {
            return _alpha;
        }
        

        // --------------------------------------------------------------
        /**
         * @ brief Results accessor: detector votes.
         * Get the voting array obtained when counting detections per pixel.
         * @return The voting array.
         */
        // --------------------------------------------------------------
        inline const VotesArray& getVotes() const
        {
            const VotesArray& rVotes = _votes;
            return rVotes;
        }
        

        // --------------------------------------------------------------
        /**
         * @ brief Results accessor: cascade detections.
         * Get the individual detections (at all scales) of the boosted cascade classifier.
         * @return The cascade detections.
         */
        // --------------------------------------------------------------
        inline const Detections& getDetections() const
        {
            const Detections& rDetections = _detections;
            return rDetections;
        }
        

        // --------------------------------------------------------------
        /**
         * @ brief Results accessor: putative text regions.
         * Get the putative text regions distilled from the voting array.
         * @return The putative text regions.
         */
        // --------------------------------------------------------------
        inline const TextBlocks& getTextRegions() const
        {
            const TextBlocks& rRois = _detectedTextRois;
            return rRois;
        }
        

        // --------------------------------------------------------------
        /**
         * @ brief Results accessor: raw ocr output.
         * Get the raw OCR parsings (including word boundaries and detection confidences) obtained
         * for all putative text regions.
         * @return The raw OCR output.
         */
        // --------------------------------------------------------------
        inline const OcrWords& getOcrOutput() const
        {
            const OcrWords& rOcrWords = _rawOcrWords;
            return rOcrWords;
        }
        

        // --------------------------------------------------------------
        /**
         * @ brief Results accessor: final (spell-checked) detections.
         * Get the final, spell-checked detections including word boundaries and estimated
         * word-posterior values.
         * @return The final, proofed detections.
         */
        // --------------------------------------------------------------
        inline const ProofedWords& getProofedWords() const
        {
            const ProofedWords& rCorrectedWords = _correctedWords;
            return rCorrectedWords;
        }
        
        
    private:
        
        /**  */
        // --------------------------------------------------------------
        /**
         * @brief Use the OCR engine to parse the detected text regions.
         * Extract text strings, word boundaries and detection confidences from the putative text regions.
         * @param img				The image to process.
         * @param detectedTextRois	The putative text regions.
         * @param rawOcrWords		The container to hold the parsings.
         */
        // --------------------------------------------------------------
        void parseWords(const cv::Mat& img, 
                        const TextBlocks& detectedTextRois,
                        OcrWords& rawOcrWords);
        

        // --------------------------------------------------------------
        /**
         * @brief Performs the relative thresholding to identify putative text regions.
         * Takes a single relative threshold and extracts corresponding putative text regions from the votes array.
         * @param contours				The contours delineating the areas of the voting array to consider for thresholding.
         * @param relativeThreshold		The relative threshold to be applied.
         * @param detectedTextRois		The extracted putative text regions.
         */
        // --------------------------------------------------------------
        void identifyTextRegions(const Contours& contours,
                                 const double& relativeThreshold, 
                                 TextBlocks& detectedTextRois) const;
        

        // --------------------------------------------------------------
        /**
         * @brief Count the votes cast by the cascade classifier.
         * Create a voting array of same size as original image and count the votes cast by the cascade classifier.
         * @param img	The image being processed.
         */
        // --------------------------------------------------------------
        void countVotes(const cv::Mat& img);
        

        // --------------------------------------------------------------
        /**
         * @brief Compute the feature channels used by the cascade classifier.
         * The cascade classifier is currently based on three feature types: x-gradient, y-gradient and gradient magnitude.
         * The individual channels from which these features are extracted by the cascade feature generator are calculated here.
         * @param imGray	The gray-scale image to be processed.
         * @param channels	The resulting feature channels.
         */
        // --------------------------------------------------------------
        void computeFeatureChannels(const cv::Mat& imGray, 
                                    features::FeatureGenerator::ChannelSet& channels) const;

        
        //DEBUG
        void dumpMat(const std::string sFName, const leget::TextSpotter::VotesArray &mat);


        // ******************************************
        // ***************** MEMBERS ****************
        // ******************************************
        
        // system components
        boosting::BoostedCascade    _cascade;       /**< The boosted cascade used to focus ocr efforts. */
        tesseract::TessBaseAPI      _ocrEngine;     /**< The Tesseract OCR engine. */
        static SpellChecker         _spellChecker;  /**< The spell checker. */
        
        // system parameters
        double                  _absoluteVoteThreshold;    /**< Min. number of votes required for a pixel to represent text. */
        ThresholdSet            _relativeVoteThresholds;   /**< Local non-maximal suppression: threshold relative to local maximum. */
        int                     _minAreaThreshold;         /**< The min. area a text roi needs to span. */
        double                  _alpha;                    /**< The sensitivity of the sensor model (i.e. the spell-checker). */
        tesseract::PageSegMode  _pageSegMode;              /**< The page segmentation mode for tesseract. Recommended: PSM_AUTO or         
                                                                PSM_SINGLE_BLOCK. */
        std::string             _cascadeLocation;          /**< The directory where the cascade classifier lives. */
        std::string             _dictionaryFile;           /**< The file where the dictionary lives. */

      bool _filter_words;  /**< are we filtering words for detections. */
                                                 
        
        // containers for intermediate and final results
        VotesArray              _votes;             /**< The matrix containg the detector votes for every pixel. */
        Detections              _detections;        /**< The detections from the boosted cascade. */
        TextBlocks              _detectedTextRois;  /**< The putative blacks of text identified from the cascade output. */
        OcrWords                _rawOcrWords;       /**< The raw OCR detections split into words. */
        ProofedWords            _correctedWords;    /**< The corrected word list. */        
    };
    
}

#endif //__LEGET_TEXT_SPOTTER_H__
