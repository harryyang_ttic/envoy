// --------------------------------------------------------------
/** @file HaarFeature.h
 **
 ** @author Ingmar Posner
 ** @date 13/6/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#ifndef __LEGET_HAARGEOMETRY_H__
#define __LEGET_HAARGEOMETRY_H__

#include <deque>
#include <boost/shared_ptr.hpp>
#include "HaarBlock.h"


// --------------------------------------------------------------
/** HaarGeometry holds the basic haar geometry consisting of HaarBlocks.
 **/
// --------------------------------------------------------------
template <typename FeatureType>
class HaarGeometry
{
public:
    
    typedef typename std::deque< boost::shared_ptr<FeatureType> > BlockList;
    
    typedef typename std::deque< boost::shared_ptr<FeatureType> >::const_iterator ConstIterator;
    typedef typename std::deque< boost::shared_ptr<FeatureType> >::iterator Iterator;
    
    // ************************************************
	// ***************** CON-/DESTRUCT ****************
	// ************************************************
	HaarGeometry(const boost::shared_ptr<FeatureType> &pBlock)
    {
        AddBlock(pBlock);
    }
    
    // ****************************************
    // ***************** UTILS ****************
    // ****************************************
    
    /** Add a block. */
    inline void AddBlock(const boost::shared_ptr<FeatureType> pBlock)
    {
        _blocks.push_back(pBlock);
    }
    
    /** Get number of blocks. */
    inline unsigned int GetNumBlocks() const {return _blocks.size();}
    
    /** Print contents of object. */
    inline void Print() const
    {
        std::cout << "**** [HAAR-GEOMETRY] ****" << std::endl;
        std::cout << "The Haar-geometry consists of " << _blocks.size() << " blocks:" << std::endl;
        typename BlockList::const_iterator citBlocks;
        unsigned int uIndex = 0;
        for (citBlocks = _blocks.begin(); citBlocks != _blocks.end(); ++citBlocks, ++uIndex)
            (*citBlocks)->Print(uIndex);
        std::cout << "************************** " << std::endl;
    }
    
    inline ConstIterator ConstBegin() {return _blocks.begin();}
    inline ConstIterator ConstEnd() {return _blocks.end();}    
    
private:    
    // ******************************************
    // ***************** MEMBERS ****************
    // ******************************************
    BlockList _blocks;
};


#endif // __LEGET_HAARGEOMETRY_H__

