// --------------------------------------------------------------
/** @file HaarFeature.h
 **
 ** @author Ingmar Posner
 ** @date 13/6/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#ifndef __LEGET_HAARFEATURE_H__
#define __LEGET_HAARFEATURE_H__

#include "leget/misc/Exceptions.h"
#include "leget/features/HaarGeometry.h"

namespace leget
{
    namespace features
    {
        
        // --------------------------------------------------------------
        /** A base class for all features. 
         **/
        // --------------------------------------------------------------
        class Feature
        {
        public:
            virtual double compute(const std::pair< cv::Mat_<double>, cv::Mat_<double> > &rois)=0;
        };
        
        
        // --------------------------------------------------------------
        /** A template class for Haar features using different evaluation metrics. 
         **/
        // --------------------------------------------------------------
        template <typename FeatureType>
        class HaarFeature : public Feature 
        {
            
        public:
            
            // ************************************************
            // ***************** CON-/DESTRUCT ****************
            // ************************************************
            /** Populate from array of blocks and coeffs. */
            HaarFeature(const boost::shared_ptr< HaarGeometry<FeatureType> > pGeometry, const cv::Mat_<double> &coeffs)
            {
                // set the geometry
                SetGeometry(pGeometry);
                
                // set coefficients
                SetCoefficients(coeffs);
            }
            
            
            // ****************************************
            // ***************** UTILS ****************
            // ****************************************
            
            /** Add HaarGeometry. */
            inline void SetGeometry(const boost::shared_ptr< HaarGeometry<FeatureType> > pGeometry) 
            {
                _pGeometry = pGeometry;
            }
            
            /** Add block coefficients. */
            inline void SetCoefficients(const cv::Mat_<double> &coeffs) 
            {
                cv::Size coeffsSize = coeffs.size();
                unsigned int uNumCoeffs = std::max(coeffsSize.width, coeffsSize.height);
                if (uNumCoeffs != _pGeometry->GetNumBlocks())
                    throw error::Exception("The number of coefficients needs to match the number of blocks!");
                
                _blockCoeffs = coeffs;
            }
            
            // --------------------------------------------------------------
            /**
             ** HaarFeature::Compute() compute the feature for a given channel and roi.
             ** @param[in]     pChannel the channel to compute the feature on.
             ** @param[in]     roi the region of interest to calculate the feature for.
             ** @return a double containing the feature value.
             ** @note ASSUMPTION: rois are of the same size!
             ** @note We are forced here to use Double images(!) becasue of opencv.
             ** @todo Cache previously calculated block values?
             */
            // --------------------------------------------------------------
            virtual double compute(const std::pair< cv::Mat_<double>, cv::Mat_<double> > &rois)
            {
                // set up blocks
                _roiSize = rois.first.size();
                typename HaarGeometry<FeatureType>::ConstIterator citBlocks;
                typename HaarGeometry<FeatureType>::ConstIterator citBlockEnd = _pGeometry->ConstEnd();
                for (citBlocks = _pGeometry->ConstBegin(); citBlocks != citBlockEnd; ++citBlocks)
                {
                    (*citBlocks)->SetRelativeDims(_roiSize.width, _roiSize.height);
                }
                
                // calculate block values
                _blockVals.create(_pGeometry->GetNumBlocks(), 1);
                double * pBlockValData = _blockVals.ptr<double>();
                unsigned int uIndex = 0;
                for (citBlocks = _pGeometry->ConstBegin(); citBlocks != citBlockEnd; ++citBlocks, ++uIndex)
                {
                    pBlockValData[uIndex] = (*citBlocks)->computeMetric(rois);
                }
                
                
                // ... and apply coefficients
                _coeffsApplied = _blockVals.mul(_blockCoeffs);
                _cvValue = cv::sum(_coeffsApplied);
                return _cvValue[0];
            }
            
            
            
            // --------------------------------------------------------------
            /**
             ** HaarFeature::Print() prints the contents of the current object.
             */
            // --------------------------------------------------------------
            inline void print() const
            {
                std::cout << "**** [HaarFeature] ****" << std::endl;
                std::cout << "Type: " << typeid(HaarGeometry<FeatureType>).name() << std::endl;
                _pGeometry->Print();
                
                std::cout << "Block coefficients: [ ";
                //unsigned char uIndex = 0;
                const double *pCoeffs = _blockCoeffs.ptr<double>();
                cv::Size coeffsSize = _blockCoeffs.size();
                unsigned int uNumCoeffs = std::max(coeffsSize.width, coeffsSize.height);
                for (unsigned int uI = 0; uI < uNumCoeffs; ++uI)
                    std::cout << pCoeffs[uI] << " ";
                std::cout << "]" << std::endl;
                std::cout << "**********************" << std::endl;
            }
            
            
        private:
            
            // ******************************************
            // ***************** MEMBERS ****************
            // ******************************************
            boost::shared_ptr< HaarGeometry<FeatureType> >     _pGeometry;
            cv::Mat_<double>                                    _blockCoeffs;
            
            // some vars for easy caching and to avoid multiple const/dest calls.
            cv::Size _roiSize;
            cv::Mat_<double> _blockVals;
            cv::Mat_<double> _coeffsApplied;
            cv::Scalar _cvValue;
        };
        
        
        /** A type for Haar features based on block means. */
        typedef HaarFeature<MeanBlock> HaarMeanFeature;
        
        /** A type for Haar features based on block variances. */
        typedef HaarFeature<VarianceBlock> HaarVarianceFeature;
        
    }
}

#endif // __LEGET_HAARFEATURE_H__

