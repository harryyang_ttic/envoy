// --------------------------------------------------------------
/** @file FeatureSet.h
 **
 **
 ** @author Ingmar Posner
 ** @date 10/02/2011
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#ifndef __LEGET_FEATURESET_H__
#define __LEGET_FEATURESET_H__

#include "leget/features/HaarFeature.h"

namespace leget
{
    namespace features
    {
        // --------------------------------------------------------------
        /** A container for sets of features. **/
        // --------------------------------------------------------------
        class FeatureSet
        {
        private:
            typedef std::deque< boost::shared_ptr<Feature> > FeatureContainer;
            
        public:
            // ************************************************
            // ***************** CON-/DESTRUCT ****************
            // ************************************************
            FeatureSet()
            {}
            
            // ********************************************
            // ***************** ACCESSORS ****************
            // ********************************************
            inline Feature& operator[](unsigned int id)
            {
                Feature& rFeature = *(_features[id]);
                return rFeature;
            }
            
            // ********************************************
            // ***************** ITERATORS ****************
            // ********************************************
            typedef FeatureContainer::const_iterator ConstIterator;
            
            inline ConstIterator begin() const { return _features.begin(); };
            inline ConstIterator end() const { return _features.end(); };
            
            // ****************************************
            // ***************** UTILS ****************
            // ****************************************
            void load();

            
        private:
            
            template<typename FeatureType>
            void tempLoadHelper(FeatureContainer& features);
            
            
            // ******************************************
            // ***************** MEMBERS ****************
            // ******************************************
            
            FeatureContainer _features; /**< The feature set. */
        };
        
    }
}

#endif // __LEGET_FEATURESET_H__
