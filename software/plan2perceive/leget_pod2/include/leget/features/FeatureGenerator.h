// --------------------------------------------------------------
/** @file FeatureGenerator.h
 **
 ** @author Ingmar Posner
 ** @date 14/6/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------


#ifndef __LEGET_FEATURE_GENERATOR_H__
#define __LEGET_FEATURE_GENERATOR_H__

#include <iostream>
#include <utility>
#include <deque>
#include <set>
#include <map>
#include <cv.h>
#include <boost/shared_ptr.hpp>
#include "leget/boosting/Datum.h"
#include "leget/features/FeatureSet.h"
#include "leget/misc/TextSpottingUtils.h"

namespace leget
{
    namespace features
    {
        
        // --------------------------------------------------------------
        /** FeatureChannel provides a container for the integral images of a single 
         ** feature channel. 
         **
         **/
        // --------------------------------------------------------------
        class FeatureChannel
        {
        public:
            
            // ************************************************
            // ***************** CON-/DESTRUCT ****************
            // ************************************************
            
            // --------------------------------------------------------------
            /** 
             ** Constructor. Takes a single channel base image (e.g. gradient, gray-scale, etc)
             ** and makes an integral image of both the original and the element-wise square of the original.
             ** These are then stored and available on demand for further processing.
             **
             ** @param[in]     channelImage the image to derive the feature channel from.
             ** @brief Constructor.
             */
            // --------------------------------------------------------------
            FeatureChannel(const cv::Mat_<double> &channelImage)
            {
                cv::Mat_<double> intImage, intSqrImage;
                cv::integral(channelImage, intImage, intSqrImage, CV_64F);
                _channelPair = std::make_pair(intImage, intSqrImage);
            }
            
            /**Accessor: get the pair of integral images back. */
            inline const std::pair< cv::Mat_<double>, cv::Mat_<double> > & getChannelPair() const 
            {
                const std::pair< cv::Mat_<double>, cv::Mat_<double> > & refPair = _channelPair;
                return refPair;
            }
            
            
        private:
            
            // ******************************************
            // ***************** MEMBERS ****************
            // ******************************************
            
            /**A pair of integral images. One from the original image, the other from the squared pixel values of the original. */
            std::pair< cv::Mat_<double>, cv::Mat_<double> > _channelPair; 
        };
        
        
        
        // --------------------------------------------------------------
        /** FeatureGenerator provides a container for the integral images of a single 
         ** feature channel. 
         **
         **/
        // --------------------------------------------------------------
        class FeatureGenerator
        {
        public:
            
            // useful typedefs
            typedef std::deque<boost::shared_ptr<FeatureChannel> > ChannelSet;
            typedef std::pair< boost::shared_ptr<FeatureChannel>, boost::shared_ptr<Feature> > ChannelFeaturePair;
            typedef std::map<unsigned int, ChannelFeaturePair> IdFeatureMap;
            
            // a container for the parameters of a sliding window
            struct Window
            {
                // constructor
                Window() : _x(0), _y(0), _w(0), _h(0)
                {}
                
                unsigned int _x; // x location
                unsigned int _y; // y location
                unsigned int _w; // width
                unsigned int _h; // height
            };
            
            
            // ************************************************
            // ***************** CON-/DESTRUCT ****************
            // ************************************************
            
            // --------------------------------------------------------------
            /** 
             ** Constructor. 
             **
             ** @param[in]     
             ** @brief Constructor.
             */
            // --------------------------------------------------------------
            FeatureGenerator(const Window& window)
            {
                setRoi(window);
            }
            
            // ****************************************
            // ***************** UTILS ****************
            // ****************************************
            
            /** Make feature id -> <channel, feature> pair. */
            static void makeIdFeatureMap(const ChannelSet& channels, 
                                         const FeatureSet& features, 
                                         IdFeatureMap& idFeatureMap);

            /** Set the region of interest.*/
            void setRoi(const Window& window);
            
            /** The workhorse: call this to fill a Datum feature vector with the feature ids specified extracted from the roi specified. */
            void getDatum(const IdFeatureMap& idLookup, 
                          const FeatureIdSet& idList, 
                          boosting::Datum& datum);
            
        private:
            // ******************************************
            // ***************** MEMBERS ****************
            // ******************************************
            std::map<unsigned int, double>  _valueCache;       /**< A buffer to store feature values when available. */
            cv::Rect_<unsigned int>         _roiRect;          /**< The region of interest to be considered. */
            
            // some vars for easy caching and to avoid multiple const/dest calls.
            cv::Mat _roi1, _roi2;
        };
        
    }
}


#endif //__LEGET_FEATURE_GENERATOR_H__



