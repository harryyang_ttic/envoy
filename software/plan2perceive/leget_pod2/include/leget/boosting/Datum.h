// --------------------------------------------------------------
/** @file Datum.h
 ** 
 ** Provides a container for individual data to be classified.
 **
 ** @author Ingmar Posner
 ** @date 4/11/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#ifndef __LEGET_BOOSTING_DATUM_H__
#define __LEGET_BOOSTING_DATUM_H__

#include <vector>
#include <iostream>
#include <cfloat>
#include <climits>

namespace leget 
{
    namespace boosting
    {
        
        // --------------------------------------------------------------
        /** Datum provides a container for training and testing data. Information
         ** stored includes the actual datum and, if available, the true label.
         ** @todo Make this a base class for both a sparse and a dense datum class (use Boost sparse vector).
         ** @todo Template label type.
         **/
        // --------------------------------------------------------------
        class Datum
        {
        public:
            
            typedef std::vector<double> FeatureVectorType;
            
            // ************************************************
            // ***************** CON-/DESTRUCT ****************
            // ************************************************
            
            /** @brief Default constructor. */
            Datum() : _datum(FeatureVectorType())
            {}
            
            
            // --------------------------------------------------------------
            /** Datum::Datum() Constructor used when data are available as FeatureVectorType. 
             **
             ** @param[in]     datum the datum to be classified.
             ** @param[in]     nTruth the ground truth label associated with datum. [Datum::nUNDEFINED]
             ** @brief Constructor for data.
             */
            // --------------------------------------------------------------
            Datum(const FeatureVectorType& rDatum)
            {
                set(rDatum);
            }
            
            // --------------------------------------------------------------
            /** Constructor used when data are available as C-style array. 
             **
             ** @param[in]     numDims the number of feature dimensions in the vector.
             ** @param[in]     pFeatVector the pointer to the feature data.
             ** @param[in]     truth the ground truth label associated with datum. [Datum::nUNDEFINED]
             ** @brief Constructor for data.
             */
            // --------------------------------------------------------------
            Datum(const unsigned int& rNumDims, const double* pDataVector)
            {
                set(rNumDims, pDataVector);
            }
            
            
            /** @brief Set the datum using FeatureVectorType as input. */
            void set(const FeatureVectorType& rDatum);
            
            /** @brief Set the datum using a C-style array as input. */
            void set(const unsigned int& rNumDims, const double* pFeatVector);
            
            /** @brief Print information. */
            void print() const;
            
            
            /** @brief clear the Datum object. */
            inline void clear()
            {
                _datum.clear();                
            }        
            
            
            /** @brief Feature access. */
            inline const double operator[](const unsigned int& rId) const { return _datum[rId]; }
            
            /** @brief Feature access. */
            inline double& operator[](const unsigned int& rId) 
            { 
                double& rVal = _datum[rId];
                return rVal; 
            }
            
            /** @brief Set the data vector to a specific size while keeping the data!. */
            inline void setSize(const unsigned int& rSize) { _datum.resize(rSize, 0.0); }; 
            
            /** @brief Get the size of the data vector. */
            inline unsigned int getSize() const { return _datum.size(); }
            
            
            // ******************************************
            // ***************** MEMBERS ****************
            // ******************************************
            
            static const int    nUNDEFINED;       /**< An indicator that the current value is invalid. */
            
        private:
            FeatureVectorType   _datum;           /**< The datum.*/
            
        };
    }
}

#endif //__LEGET_BOOSTING_DATUM_H__
