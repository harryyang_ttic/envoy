// --------------------------------------------------------------
/** @file BoostedClf.h
 **
 ** A boosted classifier.
 **
 ** @author Ingmar Posner
 ** @date 25/11/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#ifndef __LEGET_BOOSTEDCLF_H__
#define __LEGET_BOOSTEDCLF_H__

#include <list>
#include <boost/shared_ptr.hpp>
#include "leget/boosting/RegressionStump_Base.h"

namespace leget
{
    namespace boosting
    {
        
        // --------------------------------------------------------------
        /** A general boosted classifier consisting of a sequence
         ** weak classifiers.
         **/
        // --------------------------------------------------------------
        class BoostedClf 
        {
        public:
            
            /** A data type defining a container for the base classifiers. */
            typedef std::list< boost::shared_ptr < RegressionStump_Base > > BaseClassifiers;
            
            /** A data type ready to use as iterator. */
            typedef BaseClassifiers::iterator Iterator;
            
            /** A data type ready to use as const iterator. */
            typedef BaseClassifiers::const_iterator ConstIterator;
            
            
            // ************************************************
            // ***************** CON-/DESTRUCT ****************
            // ************************************************
            BoostedClf()
            {}
            
            
            // ****************************************
            // ***************** UTILS ****************
            // ****************************************
            
            /** Reset the classifier. */
            void reset(const unsigned int& numData);
            
            /** @brief Testing */
            double test(const Datum& datum) const;    
            
            /** @brief Loading */
            void loadModel(const std::string& dirName);
            
            /** @brief: Displays some information. */
            void printInfo();
            
            /** @brief: Accessor providing feature id used by this classifier. */
            void getFeatureIds(FeatureIdSet &idSet) const;
            
            
        protected:
            
            // ******************************************
            // ***************** MEMBERS ****************
            // ******************************************
            
            /** This guy holds the list of weak classifiers.*/
            BaseClassifiers _weakClfs;
            
            int          _scope;            /**< The labels the classifier caters for. */
            int          _backgroundLabel;  /**< The class label assigned to the background class. */
            
            
            /** Number of boosting rounds to go through. */
            unsigned int _numRounds;
        };
        
    }
}

#endif // __LEGET_BOOSTEDCLF_H__
