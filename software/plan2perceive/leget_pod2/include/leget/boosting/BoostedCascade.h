// --------------------------------------------------------------
/** @file BoostedCascade.h
 **
 ** Defines the interface for a Viola-Jones style attentional cascade.
 ** Currently only GentleBoost is supported.
 **
 ** @author Ingmar Posner
 ** @date 18/2/2011
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------


#ifndef __LEGET_BOOSTED_CASCADE_H__
#define __LEGET_BOOSTED_CASCADE_H__

#include <list>
#include <boost/shared_ptr.hpp>
#include "leget/boosting/BoostedClf.h"
#include "leget/features/FeatureGenerator.h"

namespace leget
{
    namespace boosting
    {
        
        class BoostedCascade;
        
        // --------------------------------------------------------------
        /** CascadeLevel - a container for data relating to cascade levels.
         **/
        // --------------------------------------------------------------
        class CascadeLevel
        {
        public:
            // ************************************************
            // ***************** CON-/DESTRUCT ****************
            // ************************************************
            
            /** Create a cascade level and load the classifier from file. */
            CascadeLevel(const std::string clfFileName)
            {
                _pClf = boost::shared_ptr<BoostedClf>(new BoostedClf);
                _pClf->loadModel(clfFileName);    
                
                /** @todo Think about whether you should read the featurelist here... */
                /** @todo Implement for logit boost... */
            }
            
            /** Set feature ID. */
            inline void setFeatureIds(const std::set<unsigned int> &idSet) {_featureIds = idSet;}
            
            /** Set threshold. */
            inline void setThreshold(const double &dfThresh) {_thresh = dfThresh;}
            
            
        private:
            
            // ******************************************
            // ***************** MEMBERS ****************
            // ******************************************
            boost::shared_ptr<BoostedClf> _pClf;  /**<The classifier for this level. */
            double                 _thresh;       /**<The threshold for this level. */
            std::set<unsigned int> _featureIds;   /**<The set of feature ids required by this level. */
            
            friend class BoostedCascade;
        };
        
        
        
        
        // --------------------------------------------------------------
        /** BoostedCascade declaration of an attentional cascade as proposed by Viola and Jones.
         **/
        // --------------------------------------------------------------
        class BoostedCascade
        {
        public:
            
            // *********************************************
            // ***************** DATA TYPES ****************
            // *********************************************
            
            /** A type to store the actual data. */
            typedef std::list< boost::shared_ptr<CascadeLevel> > LevelContainer;
            
            
            // ************************************************
            // ***************** CON-/DESTRUCT ****************
            // ************************************************
            
            /** @brief Make one from file. */
            BoostedCascade(const std::string& sDirName)
            {
                // load the classifier cascade from file
                loadModel(sDirName);
            }
        
            
            // ****************************************
            // ***************** UTILS ****************
            // ****************************************
            
            /** @brief Testing at a particular region of interest using feature generator. */
            bool testAt(const features::FeatureGenerator::Window& window, 
                        const features::FeatureGenerator::IdFeatureMap& idFeatureMap,
                        double& score) const;
            
            /** @brief Loading */
            void loadModel(const std::string& fName);
            
            /** @brief Get feature list - not needed for this classifier.*/
            void getFeatureIds(FeatureIdSet &idSet) const {}
            
            /** @brief: Display classifier information. */
            void printInfo();
            
            
        private:
              
            // ******************************************
            // ***************** MEMBERS ****************
            // ******************************************
            
            LevelContainer _levels;     /**< The cascade levels. */
            
        };
        
    }
}

#endif // __LEGET_BOOSTED_CASCADE_H__

