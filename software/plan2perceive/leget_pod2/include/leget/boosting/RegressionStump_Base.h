// --------------------------------------------------------------
/** @file RegressionStump_Base.h
 **
 ** @author Ingmar Posner
 ** @date 12/11/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#ifndef __LEGET_BOOSTING_REGRESSIONSTUMP_BASE_H__
#define __LEGET_BOOSTING_REGRESSIONSTUMP_BASE_H__

#include <iostream>
#include "leget/boosting/Datum.h"
#include "leget/misc/TextSpottingUtils.h"

namespace leget
{
    namespace boosting
    {
        // --------------------------------------------------------------
        /** 
         ** RegressionStump provides an implementation of a simple regression stump as used 
         ** by Antonio Torralba's ICCV 2005 tutorial.
         **
         ** The regression has the form:
         **
         ** z_hat = a * (x>th) + b;
         **
         ** where (a,b,th) are so that it minimizes the weighted error:
         ** error = sum(w * |z - (a*(x>th) + b)|^2) 
         ** 
         ** x,z and w are vectors of the same length
         ** x, and z are real values.
         ** w is a weight of positive values. There is no asumption that it sums to one.
         **
         */
        // --------------------------------------------------------------
        class RegressionStump_Base 
        {
            
        public:
            
            // ************************************************
            // ***************** CON-/DESTRUCT ****************
            // ************************************************
            RegressionStump_Base() : _a(0.0), _b(0.0), _threshold(0.0), 
            _featureID(0)
            {}
            
            /** @brief Loading */
            virtual void loadModel(const std::string& fileName);
            
            /** @brief Testing */
            double test(const Datum& testDatum) const;
            
            /** @brief: Displays classifier information. */
            void printInfo();
            
            /** @brief: Accessor providing feature id used by this classifier. */
            unsigned int getFeatureId() const { return _featureID; } 
            
            /** @brief: Accessor providing feature id used by this classifier. */
            void getFeatureIds(FeatureIdSet& idSet) const { idSet.insert(_featureID); } 
            
            
        private:
            
            // ******************************************
            // ***************** MEMBERS ****************
            // ******************************************
            double _a;                  /**< Regression parameter. */
            double _b;                  /**< Regression parameter. */
            double _threshold;          /**< Regression threshold. */
            double _error;              /**< Classification error. */
            unsigned int _featureID;	/**< Feature to operate on. */
            
            int _scope;            /**< The labels the classifier caters for. */
            int _backgroundLabel;  /**< The class label assigned to the background class. */
            
            // housekeeping
            bool _bValidParams;    /**< Indicator as to whether the stored parameters are valid (i.e. force training or loading). */
            
            
            static const int _positiveWorkingLabel = 1;  /** The positive label assigned by the classifier internally. */
            static const int _negativeWorkingLabel = -1; /** The negative label assigned by the classifier internally. */            
        };
        
    }
}
#endif // __LEGET_BOOSTING_REGRESSIONSTUMP_BASE_H__
