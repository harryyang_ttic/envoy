// --------------------------------------------------------------
/** @file Word.h
 **
 ** @author Ingmar Posner
 ** @date 14/7/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#ifndef __LEGET_WORD_H__
#define __LEGET_WORD_H__

namespace leget 
{
    // --------------------------------------------------------------
    /** Word provides a container for word detections obtained after layout analysis and OCR.
     **
     **/
    // --------------------------------------------------------------
    class Word
    {
    public:
        // ************************************************
        // ***************** CON-/DESTRUCT ****************
        // ************************************************
        Word(const std::string& sRawLabel, 
             const double& dfOCRConfidence, 
             const std::string& sCorrected,
             const double& dfPosterior, 
             const cv::Rect &box) 
        : _ocrWord(sRawLabel), _ocrConfidence(dfOCRConfidence), _correctedLabel(sCorrected), _wordPosterior(dfPosterior), _boundingBox(box)
        {}
        
        inline void setRawLabel(const std::string &sStr, const double &dfOCRConfidence) 
        {
            _ocrWord = sStr; 
            _ocrConfidence = dfOCRConfidence;
        }
        
        inline void setCorrectedLabel(const std::string &sStr, const double &dfPosterior) 
        {
            _correctedLabel = sStr;
            _wordPosterior = dfPosterior;
        }
        
        inline const std::string& getRawLabel() const
        {
            const std::string & sStrRef = _ocrWord;
            return sStrRef;
        }
        
        inline const double& getOCRConfidence() const
        {
            const double& rOCRConfidence = _ocrConfidence;
            return rOCRConfidence;
        }
        
        inline const std::string& getCorrectedLabel() const
        {
            const std::string& rStr = _correctedLabel;
            return rStr;
        }
        
        inline const double& getPosterior() const
        {
            const double& rPosterior = _wordPosterior;
            return rPosterior;
        }
        
        inline void getBoundingRect(cv::Rect &rect) const {rect = _boundingBox;}
        
        
    private:
        // ******************************************
        // ***************** MEMBERS ****************
        // ******************************************
        std::string _ocrWord;        /**<- the raw OCR output. */
        double _ocrConfidence;       /**<- the OCR engine confidence associated with this detection. */
        std::string _correctedLabel;  /**<- the label assigned by the spelling correction. */
        double _wordPosterior;           /**<- the posterior probaility associated with the spell corrected word. */
        cv::Rect _boundingBox;         /**<- the bounding box of the detected word. */
    };

}

#endif // __LEGET_WORD_H__
