// --------------------------------------------------------------
/** @file SpellChecker.h
 **
 ** @author Ingmar Posner
 ** @date 25/6/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#ifndef __LEGET_SPELL_CHECEKR_H__
#define __LEGET_SPELL_CHECEKR_H__


#include <cstring>
#include <map>


namespace leget
{
    // --------------------------------------------------------------
    /** SpellChecker provides a probabilistic spell checker.
     **
     **/
    // --------------------------------------------------------------
    
    class SpellChecker
    {
    public:
        
        
        // ************************************************
        // ***************** CON-/DESTRUCT ****************
        // ************************************************
        SpellChecker()
        {}
        
        
        // ***************************************************
        // ***************** PUBLIC UTILITIES ****************
        // ***************************************************
        
        /** @bried Initialization. */
        inline void initialise(const std::string &sDictFileName, const double &dfAlpha)
        {
            loadDictionary(sDictFileName);
            _alpha = dfAlpha;
        }
        
        /** @brief Get a suggestion for a given word. */
        void getWordSuggestion(const std::string& sIn, 
                               const double& ocrConfidence,
                               std::string& sSuggestion, 
                               double& dfWordPosterior) const;
        
        
    private:
        
        // **********************************************************
        // ***************** SOME INTERNAL UTILITIES ****************
        // **********************************************************
        /** @brief Load the dictionary. */
        void loadDictionary(const std::string &sFileName);
        
        /** @brief Calculate edit distance */
        unsigned int editDistance(const std::string &sWord1, const std::string &sWord2) const;
        
        struct lessThanStr
        {
            bool operator()(const std::string &s1, const std::string &s2) const 
            {
                return strcmp(s1.c_str(), s2.c_str()) < 0;
            }
        };
        
        
        // internal typedefs
        typedef std::map<std::string, double, lessThanStr> TDictionary;
        
        
        // ******************************************
        // ***************** MEMBERS ****************
        // ******************************************
        TDictionary _dictionary;   /**< The dictionary (including word occurance likelihoods) to be used. */
        double _alpha;             /**< The parameter for the sensor model. */
    };
    
}

#endif // __LEGET_SPELL_CHECEKR_H__
