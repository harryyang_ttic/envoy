// --------------------------------------------------------------
/** @file TextSpottingUtils.h
 **
 ** @author Ingmar Posner
 ** @date 14/7/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#ifndef __LEGET_UTILS_H__
#define __LEGET_UTILS_H__

#include <set>
#include <deque>
#include <string>
#include <cv.h>
#include "StrUtils.h"
#include "SpellChecker.h"


namespace leget
{
    
    typedef cv::Rect_<int> TCharBox; // I'd make this unsigned but some OpenCV methods will break without telling anyone (e.g. rect intersection!)
    
    typedef std::deque<TCharBox> TBagOfChars;
    
    /** The set of feature ids required by a classifier. */
    typedef std::set< unsigned int > FeatureIdSet;
    

    
    
    // --------------------------------------------------------------
    /** TextRoi provides a container for text block regions identified by the boosted classifier.
     **
     **/
    // --------------------------------------------------------------
    struct TextRoi
    {
        // construct
        TextRoi(cv::Rect &roi) : _roi(roi)
        {}
        
        // member
        cv::Rect _roi;
    };
    
        
    // --------------------------------------------------------------
    /** 
     **
     **/
    // --------------------------------------------------------------
    struct OcrWord
    {
        std::string _word;
        float       _confidence;
        cv::Rect    _boundingBox; 
    };
}

#endif // __LEGET_UTILS_H__

