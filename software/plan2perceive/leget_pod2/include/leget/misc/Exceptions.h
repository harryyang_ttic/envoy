// --------------------------------------------------------------
/** @file Exception.h
 **
 ** Exceptions available to text spotting library.
 **
 ** @author Ingmar Posner
 ** @date 11/7/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#ifndef __LEGET_EXCEPTIONS_H__
#define __LEGET_EXCEPTIONS_H__

#include <iostream>
#include <string>

namespace leget
{
    namespace error
    {
        
        // --------------------------------------------------------------
        /** Exception provides a base class for Leget Exceptions.
         **/
        // --------------------------------------------------------------
        
        class Exception
        {
            
        public:
            
            // ************************************************
            // ***************** CON-/DESTRUCT ****************
            // ************************************************
            
            // --------------------------------------------------------------
            /** 
             ** Constructor. 
             ** @param[in]     sStr  string containing exception information.
             ** @brief Constructor.
             */
            // --------------------------------------------------------------
            Exception(const std::string str) : _info(str) {}
            
            
            // --------------------------------------------------------------
            /** 
             ** DebugPrint() provides output to std::cerr.
             ** @brief Debug output.
             */
            // --------------------------------------------------------------
            virtual void debugPrint() const
            {
                std::cerr << _info << std::endl;
            }
            
        protected:
            
            std::string _info;
            
        };
        
        
        // --------------------------------------------------------------
        /** The IOError exception is thrown when a file writing or parsing fails.
         **/
        // --------------------------------------------------------------
        
        class IOError : public Exception
        {
        public:
            
            // ************************************************
            // ***************** CON-/DESTRUCT ****************
            // ************************************************
            
            IOError(std::string str) : Exception(str)
            {} 
        };
    }
}

#endif // __LEGET_EXCEPTIONS_H__
