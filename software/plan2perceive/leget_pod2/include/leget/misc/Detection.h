// --------------------------------------------------------------
/** @file Detection.h
 **
 ** @author Ingmar Posner
 ** @date 14/7/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#ifndef __LEGET_DETECTION_H__
#define __LEGET_DETECTION_H__

namespace leget 
{
    // --------------------------------------------------------------
    /** Detection provides a container for the raw detections of the boosted classifier.
     **
     **/
    // --------------------------------------------------------------
    class Detection
    {
    public:
        // ************************************************
        // ***************** CON-/DESTRUCT ****************
        // ************************************************
        Detection(const cv::Rect_<unsigned int> rect,
        		  const unsigned int uScale,
        		  const double score) : _rect(rect), _scale(uScale), _score(score)
        {}
        
        // ******************************************
        // **************** UTILITIES ***************
        // ******************************************
        /**@brief Detections need to be serialized for file I/O. */
        inline std::string serializeToString() const
        {
            // let's make a string containing [scale x y width height]
            std::stringstream ssDetection;
            ssDetection << _scale << " " << _rect.x << " " << _rect.y << " " << _rect.width << " " << _rect.height << " " << _score << std::endl;
            
            return ssDetection.str();
        }
        
        
        
        // ******************************************
        // ***************** MEMBERS ****************
        // ******************************************
        cv::Rect_<unsigned int> _rect;
        unsigned int 			_scale;
        double 		 			_score;
    };

}

#endif // __LEGET_DETECTION_H__
