Cascade trained using mltk.

Training parameters:

    maxFprPerStage = 0.45
    minTprPerStage = 0.91
    cascadeTargetFpr = 1e-5
    numPosTrain = 700
    numNegTrain = 7500
    numNegValidation = 7500

Training performance of entire cascade:

    On VALIDATION data: 
          
          TPR= 0.997319 FPR=0
    
    On final stage TRAINING data:
          
          TPR= 0.855714 FPR=0
    
    On negative pool: 
         
          Num of false positives: 427 (out of 32500)
