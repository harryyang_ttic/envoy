Leget 

The TextSpotting Library

Author: Ingmar Posner
Date: 20/4/2011
(c) Copyright Oxford University 2011



BUILD INSTRUCTIONS:

The first thing you should do after unpacking the archive is run doxygen on the "Leget_Doxyfile" in the Leget directory. 
This will produce all the documentation we have - including the build instructions. 
Point your browser to ./doc/html/index.html to find out how to proceed from here.
