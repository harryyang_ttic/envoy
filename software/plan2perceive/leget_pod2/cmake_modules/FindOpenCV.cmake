# - Try to find OpenCV library installation
# See http://sourceforge.net/projects/opencvlibrary/
#
# The follwoing variables are optionally searched for defaults
#  OpenCV_ROOT_DIR:            Base directory of OpenCv tree to use.
#  OpenCV_FIND_REQUIRED_COMPONENTS : FIND_PACKAGE(OpenCV COMPONENTS ..)
#    compatible interface. typically  CV CXCORE CVAUX HIGHGUI CVCAM .. etc.
#
# The following are set after configuration is done:
#  OpenCV_FOUND
#  OpenCV_INCLUDE_DIRS
#  OpenCV_LIBRARIES
#  OpenCV_LINK_DIRECTORIES
#
# deprecated:
#  OPENCV_* uppercase replaced by case sensitive OpenCV_*
#  OPENCV_EXE_LINKER_FLAGS
#  OPENCV_INCLUDE_DIR : replaced by plural *_DIRS
#
# 2004/05 Jan Woetzel, Friso, Daniel Grest
# 2006/01 complete rewrite by Jan Woetzel
# 2006/09 2nd rewrite introducing ROOT_DIR and PATH_SUFFIXES
#   to handle multiple installed versions gracefully by Jan Woetzel
#
# tested with:
# -OpenCV 0.97 (beta5a):  MSVS 7.1, gcc 3.3, gcc 4.1
# -OpenCV 0.99 (1.0rc1):  MSVS 7.1
#
# www.mip.informatik.uni-kiel.de/~jw
# --------------------------------

# Feb 2010: arh rewrote script to work better with OpenCV 2.0.0
# Still not sure if it works properly with non-installed out-of-source builds.
# But it should work properly if you've done a "make install" on OpenCV
#
# Might be worth incorporating some bits from here:
# http://code.google.com/p/embedded-control-library/source/browse/trunk/modules/build_tools/cmake/modules/FindOpenCV.cmake


MACRO(DBG_MSG _MSG)
  ##MESSAGE(STATUS "${CMAKE_CURRENT_LIST_FILE}(${CMAKE_CURRENT_LIST_LINE}):\n${_MSG}")
  #MESSAGE(STATUS "FindOpenCV: ${_MSG}")
ENDMACRO(DBG_MSG)

# This is a list of all possible components we might want to look for
SET(OpenCV_ALL_COMPONENTS CV CXCORE CVAUX HIGHGUI CVHAARTRAINING CXTS ML TRS)
IF (WIN32)
  LIST(APPEND OpenCV_ALL_COMPONENTS CVCAM)
ENDIF (WIN32)

# OpenCV_FIND_COMPONENTS contains requested components
# Here we decide what components we're actually going to search for, which
# will be stored in ${OpenCV_SEARCH_COMPONENTS}
IF (OpenCV_FIND_COMPONENTS)

  # Only search for the components requested
  SET(OpenCV_SEARCH_COMPONENTS ${OpenCV_FIND_COMPONENTS})

ELSE (OpenCV_FIND_COMPONENTS)

  # No components were explicitly requested.  We'll search for ALL of them,
  # but if the package is REQUIRED then we'll only REQUIRE a sensible subset.
  IF (OpenCV_FIND_REQUIRED)

    # REQUIRE a sensible subset, to cater for the people who just want to write
    # FIND_PACKAGE(OpenCV REQUIRED) and be done with it
    SET(OpenCV_FIND_COMPONENTS CV CXCORE CVAUX HIGHGUI)
    SET(OpenCV_SEARCH_COMPONENTS ${OpenCV_ALL_COMPONENTS})

  ELSE (OpenCV_FIND_REQUIRED)
    # Since nothing's REQUIRED, let's look for as many as we can
    SET(OpenCV_SEARCH_COMPONENTS ${OpenCV_ALL_COMPONENTS})

  ENDIF (OpenCV_FIND_REQUIRED)

ENDIF (OpenCV_FIND_COMPONENTS)

DBG_MSG("OpenCV_FIND_COMPONENTS = ${OpenCV_FIND_COMPONENTS}")
DBG_MSG("OpenCV_SEARCH_COMPONENTS = ${OpenCV_SEARCH_COMPONENTS}")

# typical root dirs of installations, exactly one of them is used
SET (OpenCV_POSSIBLE_ROOT_DIRS
  "${OpenCV_ROOT_DIR}"
  "$ENV{OpenCV_ROOT_DIR}"
  "$ENV{OPENCV_DIR}"  # only for backward compatibility deprecated by ROOT_DIR
  "$ENV{OPENCV_HOME}" # only for backward compatibility
  "[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Intel(R) Open Source Computer Vision Library_is1;Inno Setup: App Path]"
  "$ENV{ProgramFiles}/OpenCV"
  /usr/local
  /usr
  /Users/hip/Code/ThirdParty/OpenCV/OpenCV-2.1.0-Build
  /home/posnerhi/Code/Thirdparty/OpenCV/OpenCV-2.1.0-release/
  )


# MIP Uni Kiel /opt/net network installation
# get correct prefix for current gcc compiler version for gcc 3.x  4.x
IF    (${CMAKE_COMPILER_IS_GNUCXX})
  EXEC_PROGRAM(${CMAKE_CXX_COMPILER} ARGS --version OUTPUT_VARIABLE CXX_COMPILER_VERSION)
  IF   (CXX_COMPILER_VERSION MATCHES ".*3\\.[0-9].*")
    SET(IS_GNUCXX3 TRUE)
    LIST(APPEND OpenCV_POSSIBLE_ROOT_DIRS /opt/net/gcc33/OpenCV )
  ENDIF(CXX_COMPILER_VERSION MATCHES ".*3\\.[0-9].*")
  IF   (CXX_COMPILER_VERSION MATCHES ".*4\\.[0-9].*")
    SET(IS_GNUCXX4 TRUE)
    LIST(APPEND OpenCV_POSSIBLE_ROOT_DIRS /opt/net/gcc41/OpenCV )
  ENDIF(CXX_COMPILER_VERSION MATCHES ".*4\\.[0-9].*")
ENDIF (${CMAKE_COMPILER_IS_GNUCXX})

#DBG_MSG("DBG (OpenCV_POSSIBLE_ROOT_DIRS=${OpenCV_POSSIBLE_ROOT_DIRS}")

#
# select exactly ONE OpenCV base directory/tree
# to avoid mixing different version headers and libs
#
FIND_PATH(OpenCV_ROOT_DIR
  NAMES
  cv/include/cv.h     # windows
  include/opencv/cv.h # linux /opt/net
  include/cv/cv.h
  include/cv.h
  PATHS ${OpenCV_POSSIBLE_ROOT_DIRS})
DBG_MSG("OpenCV_ROOT_DIR=${OpenCV_ROOT_DIR}")


# header include dir suffixes appended to OpenCV_ROOT_DIR
SET(OpenCV_INCDIR_SUFFIXES
  include
  include/cv
  include/opencv
  cv/include
  cxcore/include
  cvaux/include
  otherlibs/cvcam/include
  otherlibs/highgui
  otherlibs/highgui/include
  otherlibs/_graphics/include
  )

# library linkdir suffixes appended to OpenCV_ROOT_DIR
SET(OpenCV_LIBDIR_SUFFIXES
  lib
  OpenCV/lib
  otherlibs/_graphics/lib
  )
#DBG_MSG("OpenCV_LIBDIR_SUFFIXES=${OpenCV_LIBDIR_SUFFIXES}")


#

# Search for all components in ${OpenCV_SEARCH_COMPONENTS}
# Those that were specifically requested by the user will HAVE to
# be found in order for the script to report success.  These mandatory
# components are stored in ${OpenCV_FIND_COMPONENTS}
SET(OpenCV_MISSING_COMPONENTS "")
FOREACH(NAME ${OpenCV_SEARCH_COMPONENTS})

  ############
  # HEADERS
  ############
  STRING(TOLOWER "${NAME}.h" HEADER_NAME)

  FIND_PATH(OpenCV_${NAME}_INCLUDE_DIR
    NAMES ${HEADER_NAME}
    PATHS ${OpenCV_ROOT_DIR}
    PATH_SUFFIXES ${OpenCV_INCDIR_SUFFIXES} )

  ############
  # LIBRARIES
  ############
  STRING(TOLOWER ${NAME} LIB_NAMES)

  # Deal with some special cases
  IF (LIB_NAMES EQUAL cv)
    LIST(APPEND LIB_NAMES opencv)
  ENDIF (LIB_NAMES EQUAL cv)

  FIND_LIBRARY(OpenCV_${NAME}_LIBRARY
    NAMES ${LIB_NAMES}
    PATHS ${OpenCV_ROOT_DIR}
    PATH_SUFFIXES  ${OpenCV_LIBDIR_SUFFIXES} )

  ############

  # Aggregate the two results for later testing
  IF (OpenCV_${NAME}_INCLUDE_DIR AND OpenCV_${NAME}_LIBRARY)
    SET(OpenCV_${NAME}_FOUND ON)
  ENDIF (OpenCV_${NAME}_INCLUDE_DIR AND OpenCV_${NAME}_LIBRARY)

  IF (OpenCV_${NAME}_FOUND)

    # We found the component, so add the paths to the output lists
    LIST(APPEND OpenCV_INCLUDE_DIRS ${OpenCV_${NAME}_INCLUDE_DIR} )
    LIST(APPEND OpenCV_LIBRARIES    ${OpenCV_${NAME}_LIBRARY} )

    DBG_MSG("FOUND OpenCV component: ${NAME}")
    DBG_MSG("OpenCV_${NAME}_INCLUDE_DIR=${OpenCV_${NAME}_INCLUDE_DIR} ")
    DBG_MSG("OpenCV_${NAME}_LIBRARY=${OpenCV_${NAME}_LIBRARY} ")

  ELSE (OpenCV_${NAME}_FOUND)

    DBG_MSG("NOT FOUND OpenCV component: ${NAME} ")
    DBG_MSG("OpenCV_${NAME}_INCLUDE_DIR=${OpenCV_${NAME}_INCLUDE_DIR} ")
    DBG_MSG("OpenCV_${NAME}_LIBRARY=${OpenCV_${NAME}_LIBRARY} ")

    # Component wasn't found.  Check to see if it was requested
    LIST(FIND OpenCV_FIND_COMPONENTS ${NAME} result)
    IF(NOT (result LESS 0))
      # Yes, it was requested.  We'll have to inform the user about it later.
      LIST(APPEND OpenCV_MISSING_COMPONENTS ${NAME})
    ENDIF(NOT (result LESS 0))

  ENDIF (OpenCV_${NAME}_FOUND)

  MARK_AS_ADVANCED(OpenCV_${NAME}_INCLUDE_DIR OpenCV_${NAME}_LIBRARY)

ENDFOREACH(NAME)

MARK_AS_ADVANCED(
  OpenCV_ROOT_DIR
  OpenCV_INCLUDE_DIRS
  OpenCV_LIBRARIES
)


# See if we can claim to have found the library
SET(OpenCV_FOUND ON)
IF (OpenCV_MISSING_COMPONENTS OR NOT OpenCV_ROOT_DIR OR NOT OpenCV_INCLUDE_DIRS)
  SET(OpenCV_FOUND OFF)
ENDIF (OpenCV_MISSING_COMPONENTS OR NOT OpenCV_ROOT_DIR OR NOT OpenCV_INCLUDE_DIRS)

DBG_MSG("OpenCV_INCLUDE_DIRS=${OpenCV_INCLUDE_DIRS}")
DBG_MSG("OpenCV_LIBRARIES=${OpenCV_LIBRARIES}")

# get the link directory for rpath to be used with LINK_DIRECTORIES:
IF    (OpenCV_CV_LIBRARY)
  GET_FILENAME_COMPONENT(OpenCV_LINK_DIRECTORIES ${OpenCV_CV_LIBRARY} PATH)
ENDIF (OpenCV_CV_LIBRARY)


# be backward compatible:
SET(OPENCV_LIBRARIES   ${OpenCV_LIBRARIES} )
SET(OPENCV_INCLUDE_DIR ${OpenCV_INCLUDE_DIRS} )
SET(OPENCV_FOUND       ${OpenCV_FOUND})


# display help message
IF(NOT OpenCV_FOUND)
  IF(OpenCV_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR
      "OpenCV could not find these REQUIRED component(s): ${OpenCV_MISSING_COMPONENTS}.\n"
      "Please specify its location with the OpenCV_ROOT_DIR env. variable.")
  ELSE(OpenCV_FIND_REQUIRED)
    IF(NOT OpenCV_FIND_QUIETLY)
      MESSAGE(STATUS "ERROR: OpenCV was not found.  Missing component(s): ${OpenCV_MISSING_COMPONENTS}")
    ENDIF(NOT OpenCV_FIND_QUIETLY)
  ENDIF(OpenCV_FIND_REQUIRED)
ENDIF(NOT OpenCV_FOUND)
