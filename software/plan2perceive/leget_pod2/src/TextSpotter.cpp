// --------------------------------------------------------------
/** @file TextSpotter.cpp
 ** Implementation of the class TextSpotter.
 **
 ** @brief Implementation of TextSpotter.
 **
 ** @author Ingmar Posner 
 ** @date 25/6/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#include <fstream>
#include "leget/TextSpotter.h"
#include "leget/misc/StrUtils.h"
#include "leget/misc/Exceptions.h"
#include "leget/misc/TextSpottingUtils.h"
#include "allheaders.h" //leptonica boxa and pixa includes


namespace leget
{
    
    // define static spell-checker
    SpellChecker TextSpotter::_spellChecker = SpellChecker();
    
    
    // ===================================================================
    void TextSpotter::spot(const cv::Mat &img, 
                           const unsigned int &winWidth, 
                           const unsigned int &winHeight,
                           const unsigned int &dx, 
                           const unsigned int &dy, 
                           const unsigned int numScales, 
                           const double scaleFactor)
    {
        // convert to grayscale
        cv::Mat imGray;
        cv::cvtColor(img, imGray, CV_RGB2GRAY);
        
        // Detect text roi candidates using boosted cascade
        detect(imGray, winWidth, winHeight, dx, dy, numScales, scaleFactor);
        
        // Count votes and find promising tight
        identifyAndParse(imGray);
        
        // Check spelling
        checkSpelling();
        
    }
    
    
    // ===================================================================
    void TextSpotter::detect(const cv::Mat &imGray, 
                             const unsigned int &winWidth, 
                             const unsigned int &winHeight,
                             const unsigned int &dx, 
                             const unsigned int &dy, 
                             const unsigned int numScales, 
                             const double scaleFactor)
    { 
        // clean slate
        _detections.clear();
        
        // compute the feature channels
        features::FeatureGenerator::ChannelSet channels;
        computeFeatureChannels(imGray, channels);
        
        // load features
        features::FeatureSet features;
        features.load();
        
        // prepare a featureId -> <channel, geometry> map
        features::FeatureGenerator::IdFeatureMap featureLookup;
        features::FeatureGenerator::makeIdFeatureMap(channels, features, featureLookup);
        
        // slide over image
        unsigned int maxRows, maxCols;
        unsigned int scaledWinWidth = winWidth;
        unsigned int scaledWinHeight = winHeight;
        unsigned int scaledDx = dx;
        unsigned int scaledDy = dy;
        unsigned int imWidth = imGray.cols;
        unsigned int imHeight = imGray.rows;
        for (unsigned int uScale = 0; uScale < numScales; ++uScale)
        {
            // perform scaling
            scaledWinWidth = static_cast<unsigned int>(static_cast<double>(scaledWinWidth) * scaleFactor);
            scaledWinHeight = static_cast<unsigned int>(static_cast<double>(scaledWinHeight) * scaleFactor);
            scaledDx = static_cast<unsigned int>(static_cast<double>(scaledDx) * scaleFactor);
            scaledDy = static_cast<unsigned int>(static_cast<double>(scaledDy) * scaleFactor);
            
            maxRows = imGray.rows - scaledWinHeight;
            maxCols = imGray.cols - scaledWinWidth;
            
            // make a window for the feature generator to operate on
            features::FeatureGenerator::Window slidingWindow;
            slidingWindow._w = scaledWinWidth;
            slidingWindow._h = scaledWinHeight;
            
            // check window is not too large for the image
            if ( (scaledWinWidth > imWidth) || (scaledWinHeight > imHeight) )
                break;
            
            // apply the classifier
            double score;
            for (slidingWindow._y = 0; slidingWindow._y < maxRows; slidingWindow._y += scaledDy)
            {
                for (slidingWindow._x = 0; slidingWindow._x < maxCols; slidingWindow._x += scaledDx)
                {
                    if (_cascade.testAt(slidingWindow, featureLookup, score))
                        _detections.push_back(Detection(cv::Rect_<unsigned int>(slidingWindow._x, 
                                                                                slidingWindow._y,
                                                                                slidingWindow._w, 
                                                                                slidingWindow._h),
                                                        uScale, score));
                }
            }
        }
    }

    
    
    // ===================================================================
    void TextSpotter::identifyAndParse(const cv::Mat& img)
    {
        // analyse detections
        countVotes(img);
        
        // **** THRESHOLD AWAY OUTLIERS ****
        
        // threshold at some useful low value to get rid of outliers
        cv::Mat binarizedVotes;
        cv::threshold(_votes, binarizedVotes, _absoluteVoteThreshold, 1.0, cv::THRESH_BINARY);
        
        
        // find contours
        Contours contours;
        cv::Mat binarizedVotes_8UC1;
        // ... convert to make this work (and to be able to count all the votes previously) 
        binarizedVotes.convertTo(binarizedVotes_8UC1, CV_8UC1);
        cv::findContours(binarizedVotes_8UC1, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
        
        
        // ****  PERFORM RELATIVE THRESHOLDING ****
        
        // loop over all proposed relative thresholds
        TextBlocks detectedTextRois;
        OcrWords rawOcrWords;
        double maxMeanOcrConfidence = 0.0;
        double meanOcrConfidence = 0.0;
        ThresholdSet::const_iterator citThresh;
        for(citThresh = _relativeVoteThresholds.begin(); citThresh != _relativeVoteThresholds.end(); ++citThresh)
	  {
	    // apply threshold
	    identifyTextRegions(contours, *citThresh, detectedTextRois);
            
            if (detectedTextRois.size() == 0)
	      continue;
            
            // parse the detected text regions
            parseWords(img, detectedTextRois, rawOcrWords);
	    
            // filter out unusable detections (i.e. those not containing any alphanumeric clues)
            OcrWords filteredOcrWords;
            for(unsigned int i = 0; i < rawOcrWords.size(); ++i)
	      {
		if( _filter_words ) {
		  std::string filteredWord;
		  filterForAlphaNumeric(rawOcrWords[i]._word, filteredWord);
		  strip(filteredWord);
		  
		  if (filteredWord.size() > 0)
		    filteredOcrWords.push_back(rawOcrWords[i]);
		} else {
		  filteredOcrWords.push_back( rawOcrWords[i] );
		  //filteredOrcWords.push_back( rawOcrWords[i] );
		}
	      }
            
            unsigned int numWords = filteredOcrWords.size();
            if (numWords == 0)
	      continue;
            
            // compute mean ocr confidence
            double confSum = 0.0;
            for(unsigned int i = 0; i < numWords; ++i)
                confSum += filteredOcrWords[i]._confidence;
            meanOcrConfidence = confSum / numWords;
            
            // store the winner
            if (meanOcrConfidence > maxMeanOcrConfidence)
            {
                maxMeanOcrConfidence = meanOcrConfidence;
                _detectedTextRois = detectedTextRois;
                _rawOcrWords = filteredOcrWords;
            }
        }
    }
    
    
    // ===================================================================
    void TextSpotter::checkSpelling()
    {
        // clean slate
        _correctedWords.clear();
        
        std::string sCorrected;
        double dfPost;
        std::string rawOcrWord;
        OcrWords::const_iterator citOcr;
        OcrWords::const_iterator citOcrEnd = _rawOcrWords.end();
        for (citOcr = _rawOcrWords.begin(); citOcr != citOcrEnd; ++citOcr)
        {
            rawOcrWord = citOcr->_word;
            
            // remove leading and trailling spaces
            strip(rawOcrWord);
            
            // is there anything left?
            if(rawOcrWord.size() == 0)
                continue;
            
            // convert to lower case - since dictionary is lower case!
            std::transform(rawOcrWord.begin(), rawOcrWord.end(), 
                           rawOcrWord.begin(), (int(*)(int)) tolower);
            
            // translate word
            _spellChecker.getWordSuggestion(rawOcrWord, (citOcr->_confidence/100.0), sCorrected, dfPost);
            
            // store
            _correctedWords.push_back(Word(citOcr->_word, citOcr->_confidence, sCorrected, dfPost, citOcr->_boundingBox));
        }
    }
    
    
    // ===================================================================
    void TextSpotter::parseWords(const cv::Mat& img, 
                                 const TextBlocks& detectedTextRois,
                                 OcrWords& rawOcrWords)
    {
        // clean slate
        rawOcrWords.clear();
        
        // Use Tesseract to find word boundaries and parsings in text block rois.
        cv::Mat roiMat, continuousRoi;
        TextBlocks::const_iterator citTextRegions;
        TextBlocks::const_iterator citTextRegionsEnd = detectedTextRois.end();
        for (citTextRegions = detectedTextRois.begin(); citTextRegions != citTextRegionsEnd; ++citTextRegions)
        {   
            roiMat = img(citTextRegions->_roi);
            
            // make sure the roi is continuous in memory - have to copy the data, I'm afraid.
            continuousRoi = roiMat.clone();
            
            // reset Tesseract - frees up memory and forget adaptive data.
            _ocrEngine.ClearAdaptiveClassifier();
            
            // Provide an image for Tesseract to recognize.  Does not copy the image buffer, or take
            // ownership. SetImage clears all recognition results, and sets the rectangle to the
            // full image, so it may be followed immediately by a GetUTF8Text, and it
            // will automatically perform recognition.
            int nBytesPerPixel = continuousRoi.elemSize();
            int nBytesPerLine = continuousRoi.cols * nBytesPerPixel;
            _ocrEngine.SetImage(continuousRoi.data, continuousRoi.cols, continuousRoi.rows, nBytesPerPixel, nBytesPerLine);
            
            // Get the recognized text as an stl array of strings coded as UTF8.
            std::vector<std::string> words;
            if(!_ocrEngine.GetSTLText(words))
                continue;
            
            // Get confidences. The number of confidences should correspond to the number of space-
            // delimited words in GetUTF8Text.
            int *conf;
            conf = _ocrEngine.AllWordConfidences();
            
            // get bounding boxes 
            Boxa* pBoxa = 0;
            Pixa** ppPixa = 0;
            // Get the words as a leptonica-style
            // Boxa, Pixa pair, in reading order.
            // Can be called before or after Recognize.
            pBoxa = _ocrEngine.GetWords(ppPixa);
            
            // store raw output (converted to lower case for comparison), 
            // confidences and bounding boxes
            cv::Rect rect;
            OcrWord ocrWord;
            for (unsigned int i = 0; i < words.size(); ++i)
            {
                // get the bounding box and add the correct offset
                cv::Rect rect(pBoxa->box[i]->x, pBoxa->box[i]->y, pBoxa->box[i]->w, pBoxa->box[i]->h);
                rect.x += citTextRegions->_roi.x;
                rect.y += citTextRegions->_roi.y;
                
                // fill in the OcrWord container
                ocrWord._word = words[i];
                ocrWord._confidence = conf[i];
                ocrWord._boundingBox = rect;
                
                // store
                rawOcrWords.push_back(ocrWord);
            }
            
            // clean up after Tesseract
            delete [] conf;
            delete [] pBoxa;
            delete [] ppPixa;
        }
    }
    
    
    // ===================================================================
    void TextSpotter::identifyTextRegions(const Contours& contours,
                                          const double& relativeThreshold, 
                                          TextBlocks& detectedTextRois) const
    {
        
        // ****  PERFORM RELATIVE THRESHOLDING ON REGIONS ****
        
        // clean slate
        detectedTextRois.clear();
        
        // go through contours and make text block regions
        cv::Mat roiMat;
        Contours::const_iterator citContours;
        Contours::const_iterator citContoursEnd = contours.end();
        for (citContours = contours.begin(); citContours != citContoursEnd; ++citContours)
        {   
            cv::Rect roi(cv::boundingRect(cv::Mat(*citContours)));
            
            roiMat = _votes(roi);
            
            
            // find maximum within roi and determine threshold relative to that
            double dfRoiMax = 0.0;
            int cols = roiMat.cols, rows = roiMat.rows;
            if(roiMat.isContinuous())
            {
                cols *= rows;
                rows = 1;
            }
            for (int i = 0; i < rows; i++)
            {
                const float* Mi = roiMat.ptr<float>(i);
                for(int j = 0; j < cols; j++)
                {
                    dfRoiMax = std::max(static_cast<double>(Mi[j]), dfRoiMax);
                }
            }
            
            // threshold
            double dfRoiThreshold = dfRoiMax * relativeThreshold;
            cv::Mat roiMatBinarized, roiMatBinarized_8UC1;
            cv::threshold(roiMat, roiMatBinarized, dfRoiThreshold, 1.0, cv::THRESH_BINARY);
            
            // find contours within roi
            Contours roiContours;
            roiMatBinarized.convertTo(roiMatBinarized_8UC1, CV_8UC1);
            cv::findContours(roiMatBinarized_8UC1, roiContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
            
            
            // From contours, create rectangles with the correct offset
            Contours::const_iterator citRoiContours;
            Contours::const_iterator citRoiContoursEnd = roiContours.end();
            for (citRoiContours = roiContours.begin(); citRoiContours != citRoiContoursEnd; ++citRoiContours)
            {
                cv::Rect textRoi(cv::boundingRect(cv::Mat(*citRoiContours)));
                
                // filter our rois that are too small
                if (textRoi.area() > _minAreaThreshold)
                { 
                    // adjust for roi offset
                    textRoi.x += roi.x;
                    textRoi.y += roi.y;            
                    
                    // store
                    detectedTextRois.push_back(textRoi);  
                }
            }
        }
    }
    
    
    // ===================================================================
    void TextSpotter::countVotes(const cv::Mat& img)
    {
        // OpenCV doesn't seem to like unsigned int matrices when adding matrices.
        // And thresholding can only be done with uchar or 32bit float. 
        // And FindContours only supports 8UC1. Hence...
        _votes.create(img.rows, img.cols, CV_32FC1);
        
        // set all elements to zero
        _votes = 0.0;
        
        // loop through detections
        cv::Mat roi;
        std::deque<Detection>::const_iterator citDetections;
        std::deque<Detection>::const_iterator citRectEnd = _detections.end();
        for (citDetections = _detections.begin(); citDetections != citRectEnd; ++citDetections)
        {
            roi = _votes(citDetections->_rect);
            roi += citDetections->_score;
        }
    }
        
    
    // ===================================================================
    void TextSpotter::computeFeatureChannels(const cv::Mat &imGray, 
                                             features::FeatureGenerator::ChannelSet& channels) const
    {
        // *** X-Gradient channel: ****
        cv::Mat xGradIm;
        cv::Mat absXGradIm;
        cv::Mat absXGradImSqrd;
        cv::Sobel(imGray, xGradIm, CV_64F, 1, 0, 3);
        absXGradIm = cv::abs(xGradIm);
        
        // *** Y-Gradient channel: ****
        cv::Mat yGradIm;
        cv::Mat absYGradIm;
        cv::Mat absYGradImSqrd;
        cv::Sobel(imGray, yGradIm, CV_64F, 0, 1, 3);
        absYGradIm = cv::abs(yGradIm);
        
        // *** Abs Gradient Magnitude channel: ****
        cv::Mat gradMagIm;
        cv::Mat sumGradSqr;
        cv::multiply(absXGradIm, absXGradIm, absXGradImSqrd);
        cv::multiply(absYGradIm, absYGradIm, absYGradImSqrd);
        sumGradSqr = absXGradImSqrd + absYGradImSqrd;
        cv::sqrt(sumGradSqr, gradMagIm);
        
        // make a channel set from the channel images calculated above
        boost::shared_ptr<features::FeatureChannel> pAbsXGradChannel(new features::FeatureChannel(absXGradIm));
        boost::shared_ptr<features::FeatureChannel> pAbsYGradChannel(new features::FeatureChannel(absYGradIm));
        boost::shared_ptr<features::FeatureChannel> pGradMagChannel(new features::FeatureChannel(gradMagIm));
        channels.push_back(pAbsXGradChannel);
        channels.push_back(pAbsYGradChannel);
        channels.push_back(pGradMagChannel);
    }



    // DEBUG ONLY
    //===================================================================
    void TextSpotter::dumpMat(const std::string sFName, const leget::TextSpotter::VotesArray &mat)
    {
    	std::cout << "_votes: " << mat.rows << " x " << mat.cols << std::endl;
    	std::cout << "Writing votes to: " << sFName.c_str() << std::endl;
    	std::ofstream osFile;
        osFile.open(sFName.c_str());
        for (int nX = 0; nX < mat.rows; ++nX)
        {
            for (int nY = 0; nY < mat.cols; ++nY)
            {
                //std::cout << "nX: " << nX << "  nY: " << nY << std::endl;
            	osFile << mat.at<float>(nX, nY) << ", ";
            }
            osFile << std::endl;
        }
        osFile.close();

    }

}



