// --------------------------------------------------------------
/** @file RegressionStump.cpp
 ** Implementation of the class RegressionStump_Base
 **
 ** @brief Implementation of RegressionStump_Base.
 **
 ** @author Ingmar Posner 
 ** @date 12/11/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#include <iostream>
#include <algorithm>
#include <limits>
#include <cmath>
#include <boost/lexical_cast.hpp>
#include <tiny-xml/tinyxml.h>
#include "leget/boosting/RegressionStump_Base.h"
#include "leget/misc/Exceptions.h"


namespace leget
{
    namespace boosting
    {
        
        // --------------------------------------------------------------
        /** Apply the decision stump to perform classification - once it has been trained.
         ** Note that in this version the prediction scores container is _not_ reset before use! 
         **
         ** The regression has the form: pred = a * (x>th) + b;
         ** @param[in]     testData     the data to train on.
         ** @param[in]     configInfo   the configuration information for training.
         ** @param[in]     bForce       (optional) force testing irrespective of whether parameters are valid.
         **                             Useful for classifier training. [False]
         ** @return boolean to indicate success or otherwise.
         */
        // --------------------------------------------------------------
        double RegressionStump_Base::test(const Datum& datum) const
        {
            double currentScore = 0.0;
            
            // evaluate threshold
            if ( datum[_featureID] > _threshold )
            {
                currentScore = _a + _b;
            }
            else
            {
                currentScore = _b;
            }
            
            // return prediction
            return currentScore;
        }
        
        
        // --------------------------------------------------------------
        /** Load the model from an xml file.
         ** 
         ** @param[in]     sFileName the name of the file to load from.
         */
        // --------------------------------------------------------------
        void RegressionStump_Base::loadModel(const std::string& fileName)
        {
            TiXmlDocument doc(fileName.c_str());
            bool bSuccess = doc.LoadFile();
            if (!bSuccess)
                throw leget::error::IOError("RegressionStump::LoadModel - Could not load file: " + fileName);
            TiXmlHandle docHandle (& doc);
            
            // check classifier type is correct
            TiXmlElement *pTypeElement = docHandle.FirstChild("Classifier_Type").ToElement();
            if(!pTypeElement)
                throw leget::error::IOError("RegressionStump::LoadModel - Could not find classifier type in file: " + fileName);
            const char *pBuff = pTypeElement->GetText();
            unsigned int uClfType;
            try
            {
                uClfType = boost::lexical_cast<unsigned int>(std::string(pBuff));
            }
            catch (boost::bad_lexical_cast &)
            {
                throw leget::error::IOError("RegressionStump::LoadModel - could not parse classifier type.");
            }
            
            //if (uClfType != BASE_REGRESSION_STUMP)
            //    throw leget::error::IOError("RegressionStump::LoadModel - wrong classifier type!");
            
            // OK, now parse the parameters.
            // ... 'a'
            TiXmlElement *pAElement = docHandle.FirstChild("Parameters").FirstChild("a").ToElement();
            if(!pAElement)
                throw leget::error::IOError("RegressionStump::LoadModel - Could not find parameter 'a' in file: " + fileName);
            pBuff = pAElement->GetText();
            try
            {
                _a = boost::lexical_cast<double>(std::string(pBuff));
            }
            catch (boost::bad_lexical_cast &)
            {
                throw leget::error::IOError("RegressionStump::LoadModel - could not parse parameter 'a' from file: " + fileName);
            }
            
            // ... 'b'
            TiXmlElement *pBElement = docHandle.FirstChild("Parameters").FirstChild("b").ToElement();
            if(!pBElement)
                throw leget::error::IOError("RegressionStump::LoadModel - Could not find parameter 'b' in file: " + fileName);
            pBuff = pBElement->GetText();
            try
            {
                _b = boost::lexical_cast<double>(std::string(pBuff));
            }
            catch (boost::bad_lexical_cast &)
            {
                throw leget::error::IOError("RegressionStump::LoadModel - could not parse parameter 'b' from file: " + fileName);
            }
            
            // ... 'threshold'
            TiXmlElement *pThreshElement = docHandle.FirstChild("Parameters").FirstChild("threshold").ToElement();
            if(!pThreshElement)
                throw leget::error::IOError("RegressionStump::LoadModel - Could not find parameter 'threshold' in file: " + fileName);
            pBuff = pThreshElement->GetText();
            try
            {
                _threshold = boost::lexical_cast<double>(std::string(pBuff));
            }
            catch (boost::bad_lexical_cast &)
            {
                throw leget::error::IOError("RegressionStump::LoadModel - could not parse parameter 'threshold' from file: " + fileName);
            }
            
            // ... 'feature_id'
            TiXmlElement *pIdElement = docHandle.FirstChild("Parameters").FirstChild("feature_id").ToElement();
            if(!pIdElement)
                throw leget::error::IOError("RegressionStump::LoadModel - Could not find parameter 'featureID' in file: " + fileName);
            pBuff = pIdElement->GetText();
            try
            {
                _featureID = boost::lexical_cast<unsigned int>(std::string(pBuff));
            }
            catch (boost::bad_lexical_cast &)
            {
                throw leget::error::IOError("RegressionStump::LoadModel - could not parse parameter 'featureID' from file: " + fileName);
            }
            
            // ... 'class_label'
            TiXmlElement *pScopeElement = docHandle.FirstChild("Parameters").FirstChild("class_label").ToElement();
            if(!pScopeElement)
                throw leget::error::IOError("RegressionStump::LoadModel - Could not find parameter 'class_label' in file: " + fileName);
            pBuff = pScopeElement->GetText();
            try
            {
                _scope = boost::lexical_cast<int>(std::string(pBuff));
            }
            catch (boost::bad_lexical_cast &)
            {
                throw leget::error::IOError("RegressionStump::LoadModel - could not parse parameter 'class_label' from file: " + fileName);
            }
            
            // ... 'class_label_other'
            TiXmlElement *pOtherClassElement = docHandle.FirstChild("Parameters").FirstChild("class_label_other").ToElement();
            if(!pOtherClassElement)
                throw leget::error::IOError("RegressionStump::LoadModel - Could not find parameter 'class_label_other' in file: " + fileName);
            pBuff = pOtherClassElement->GetText();
            try
            {
                _backgroundLabel = boost::lexical_cast<int>(std::string(pBuff));
            }
            catch (boost::bad_lexical_cast &)
            {
                throw leget::error::IOError("RegressionStump::LoadModel - could not parse parameter 'class_label_other' from file: " + fileName);
            }
            
            
            // finally, let the regression stump know that is now contains valid parameters
            _bValidParams = true;
            
        }
        
        
        
        // --------------------------------------------------------------
        /** Displays regression stump parameters. */
        // --------------------------------------------------------------
        void RegressionStump_Base::printInfo()
        {
            std::cout << "********** CLASSIFIER INFO **********" << std::endl;
            std::cout << "Classifier type: Regression Stump" << std::endl;
            if (!_bValidParams)
            {
                std::cout << "Parameters Valid: NO" << std::endl;
            }
            else
            {
                std::cout << "Parameters Valid: YES" << std::endl;
                std::cout << "Parameters:" << std::endl;
                std::cout << "label scope: [ " << _scope << "]" << std::endl; 
                std::cout << "background class label: " << _backgroundLabel << std::endl;
                std::cout << "\tthreshold: " << _threshold << std::endl;
                std::cout << "\ta: " << _a << std::endl;
                std::cout << "\tb: " << _b << std::endl;
                std::cout << "\tfeature ID: " << _featureID << std::endl;
            }
            
            std::cout << "*************************************" << std::endl;
        }
        
               
    }
}
