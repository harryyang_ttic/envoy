// --------------------------------------------------------------
/** @file BoostedClf.cpp
 ** Implementation of the class BoostedClf.
 **
 ** @brief Implementation of BoostedClf.
 **
 ** @author Ingmar Posner 
 ** @date 25/5/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------


#include <set>
#include <math.h>
#include <typeinfo>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem/operations.hpp>
#include <tiny-xml/tinyxml.h>
#include "leget/misc/StrUtils.h"
#include "leget/boosting/BoostedClf.h"
#include "leget/boosting/RegressionStump_Base.h"
#include "leget/misc/Exceptions.h"


namespace leget
{
    namespace boosting
    {
        
        // --------------------------------------------------------------
        /**
         ** BoostedClf::Test() applies the boosted classifier to perform 
         ** classification once it has been trained.
         ** @param[in]     testData     the data to train on.
         ** @param[in]     configInfo   the configuration information for training.
         ** @param[in]     bForce       (optional) force testing irrespective of whether parameters are valid.
         **                             Useful for classifier training. [False]
         ** @return boolean to indicate success or otherwise.
         */
        // --------------------------------------------------------------
        double BoostedClf::test(const Datum& datum) const
        {
            double strongScore = 0.0;
            
            ConstIterator citWeakClfs;
            // go through all the weak classifiers
            for ( citWeakClfs = _weakClfs.begin(); citWeakClfs != _weakClfs.end(); ++citWeakClfs )
            {
                // evaluate classifier
                double weakScore = (*citWeakClfs)->test(datum);
                
                strongScore += weakScore;
            }
            
            return strongScore;
        }
        
        
        // --------------------------------------------------------------
        /**
         ** BoostedClf::LoadModel() loads the model from a directory of xml files.
         ** 
         ** @param[in]     sDirName the name of the directory to load from.
         **
         */
        // --------------------------------------------------------------
        void BoostedClf::loadModel(const std::string& dirName)
        {
            // search for "index.xml"
            std::string sFileName = dirName + "/index.xml";
            if (!boost::filesystem::exists(sFileName))
                throw leget::error::IOError("Could not find index.xml for boosted classifier " + dirName);
            
            // parse "index.xml"
            TiXmlDocument doc(sFileName.c_str());
            bool bSuccess = doc.LoadFile();
            if (!bSuccess)
                throw leget::error::IOError("BoostedClf::LoadModel - Could not load file: " + sFileName);
            TiXmlHandle docHandle (& doc);
            
            // check classifier type is correct
            TiXmlElement *pTypeElement = docHandle.FirstChild("Classifier_Type").ToElement();
            if(!pTypeElement)
                throw leget::error::IOError("BoostedClf::LoadModel - Could not find classifier type in file: " + sFileName);
            const char *pBuff = pTypeElement->GetText();
            unsigned int uClfType;
            try
            {
                uClfType = boost::lexical_cast<unsigned int>(std::string(pBuff));
            }
            catch (boost::bad_lexical_cast &)
            {
                throw leget::error::IOError("BoostedClf::LoadModel - could not parse classifier type.");
            }
            
            //if (uClfType != BOOSTED_CLASSIFIER)
            //    throw leget::error::IOError("BoostedClf::LoadModel - stored model is not for this classifier.");
            
            
            // check boosting type is correct
            TiXmlElement *pBoostingTypeElement = docHandle.FirstChild("Boosting_Type").ToElement();
            if(!pBoostingTypeElement)
                throw leget::error::IOError("BoostedClf::LoadModel - Could not find boosting type in file: " + sFileName);
            pBuff = pBoostingTypeElement->GetText();
            int uBoostingType;
            try
            {
                uBoostingType = boost::lexical_cast<unsigned int>(std::string(pBuff));
            }
            catch (boost::bad_lexical_cast &)
            {
                throw leget::error::IOError("BoostedClf::LoadModel - could not parse boosting type.");
            }
            
            //if (uBoostingType != _boostingType)
            //    throw leget::error::IOError("BoostedClf::LoadModel - wrong boosting type for this classifier!");
            
            
            
            // OK, now parse the parameters.
            
            // ... get type of weak clfs
            TiXmlElement *pWeakClfTypeElement = docHandle.FirstChild("Parameters").FirstChild("Weak_Classifier_Type").ToElement();
            if(!pWeakClfTypeElement)
                throw leget::error::IOError("BoostedClf::LoadModel - Could not find parameter 'Weak_Classifier_Type' in file: " + sFileName);
            pBuff = pWeakClfTypeElement->GetText();
            unsigned int uType;
            try
            {
                uType = boost::lexical_cast<unsigned int>(std::string(pBuff));
            }
            catch (boost::bad_lexical_cast &)
            {
                throw leget::error::IOError("BoostedClf::LoadModel - could not parse parameter 'Weak_Classifier_Type' from file: " + sFileName);
            }
            //switch (uType)
            //{
            //    case BASE_REGRESSION_STUMP: 
            //        _weakClfType = BASE_REGRESSION_STUMP;
            //        break;
            //    default:
            //        throw leget::error::IOError("BoostedClf::LoadModel - Could not parse parameter 'Weak_Classifier_Type' in file: " + sFileName);
            //}
            
            // ... how many weak clfs are there?
            TiXmlElement *pNumWeakClfElement = docHandle.FirstChild("Parameters").FirstChild("Number_Of_Clfs").ToElement();
            if(!pNumWeakClfElement)
                throw leget::error::IOError("BoostedClf::LoadModel - Could not find parameter 'Number_Of_Clfs' in file: " + sFileName);
            pBuff = pNumWeakClfElement->GetText();
            try
            {
                _numRounds = boost::lexical_cast<unsigned int>(std::string(pBuff));
            }
            catch (boost::bad_lexical_cast &)
            {
                throw leget::error::IOError("BoostedClf::LoadModel - could not parse parameter 'Number_Of_Clfs' from file: " + sFileName);
            }
            
            // ... get the label scope
            TiXmlElement *pScopeElement = docHandle.FirstChild("Parameters").FirstChild("Label_scope").ToElement();
            if(!pScopeElement)
                throw leget::error::IOError("BoostedClf::LoadModel - Could not find parameter 'Label_scope' in file: " + sFileName);
            pBuff = pScopeElement->GetText();
            try
            {
                std::string scopeString(pBuff);
                // strip out spaces
                strip(scopeString);
                std::string label;
                split(scopeString, label, scopeString);
                while(!label.empty())
                {
                    _scope = boost::lexical_cast<int>(label);
                    split(scopeString, label, scopeString);
                }
            }
            catch (boost::bad_lexical_cast &)
            {
                throw leget::error::IOError("BoostedClf::LoadModel - could not parse parameter 'Label_scope' from file: " + sFileName);
            }
            
            // ... get the 'other' label
            TiXmlElement *pOtherClassElement = docHandle.FirstChild("Parameters").FirstChild("Class_label_other").ToElement();
            if(!pOtherClassElement)
                throw leget::error::IOError("BoostedClf::LoadModel - Could not find parameter 'Class_label_other' in file: " + sFileName);
            pBuff = pOtherClassElement->GetText();
            try
            {
                _backgroundLabel = boost::lexical_cast<int>(std::string(pBuff));
            }
            catch (boost::bad_lexical_cast &)
            {
                throw leget::error::IOError("BoostedClf::LoadModel - could not parse parameter 'Class_label_other' from file: " + sFileName);
            }
            
            // OK, now load the models
            std::string sClfNum;
            for (unsigned int uI = 0; uI < _numRounds; ++uI)
            {
                // get file name under which model is stored
                sClfNum = "Clf_" + boost::lexical_cast<std::string>(uI);
                TiXmlElement *pClfFNameElement = docHandle.FirstChild("Weak_Classifiers").FirstChild(sClfNum.c_str()).ToElement();
                if(!pClfFNameElement)
                    throw leget::error::IOError("BoostedClf::LoadModel - Could not find parameter find filename for classifier " + sClfNum + " in file: " + sFileName);
                pBuff = pClfFNameElement->GetText();
                std::string sFName(pBuff);
                std::string sFullName(dirName + "/" + sFName);
                try
                {
                    boost::shared_ptr< RegressionStump_Base > pClf(new RegressionStump_Base());
                    pClf->loadModel(sFullName);
                    _weakClfs.push_back(pClf);
                }
                catch (boost::bad_lexical_cast &)
                {
                    throw leget::error::IOError("BoostedClf::LoadModel - could not parse parameter 'Number_Of_Clfs' from file: " + sFileName);
                }
            }
            
        }
        
        
        // --------------------------------------------------------------
        /**
         ** Display classifier parameters.
         */
        // --------------------------------------------------------------
        void BoostedClf::printInfo()
        {
            std::cout << "********** CLASSIFIER INFO **********" << std::endl;
            std::cout << "Classifier type: Boosted Classifier" << std::endl;
            std::cout << "Weak classifier type: ";
            std::cout << "REGRESSION_STUMP" << std::endl;
            std::cout << "Number of weak classifiers: " << _weakClfs.size() << std::endl;
            std::cout << "class label: [ " << _scope << " ]" << std::endl;
            std::cout << "background label: " << _backgroundLabel << std::endl;
            std::cout << std::endl << "Classifier Details: " << std::endl;
            BaseClassifiers::const_iterator citClfs;
            for (citClfs = _weakClfs.begin(); citClfs != _weakClfs.end(); ++citClfs)
            {
                std::cout << "{ " << std::endl;
                (*citClfs)->printInfo();
                std::cout << "} " << std::endl << std::endl;
            }
            std::cout << std::endl << std::endl;
            std::cout << "*************************************" << std::endl;
            std::cout << "*************************************" << std::endl;
        }
        
        
        // --------------------------------------------------------------
        /**
         ** Provide the set of all feature ids used by this classifier.
         */
        // --------------------------------------------------------------
        void BoostedClf::getFeatureIds(std::set<unsigned int> &idSet) const
        {
            BaseClassifiers::const_iterator citClfs;
            for (citClfs = _weakClfs.begin(); citClfs != _weakClfs.end(); ++citClfs)
            {
                (*citClfs)->getFeatureIds(idSet);
            }
        }
        
    }
}



