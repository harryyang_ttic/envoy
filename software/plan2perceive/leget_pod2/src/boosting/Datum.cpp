// --------------------------------------------------------------
/** @file Datum.cpp
 ** Implementation of the class Datum.
 **
 ** @brief Implementation of Datum.
 **
 ** @author Ingmar Posner 
 ** @date 4/11/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#include "leget/boosting/Datum.h"

namespace leget
{
    namespace boosting
    {
        
        // definition of const static member
        const int Datum::nUNDEFINED = INT_MAX;         /**< An indicator that the current value is invalid. */
        
        
        // --------------------------------------------------------------
        /** 
         ** Datum::set() used to set contents of datum. 
         ** If unavailable, ground truth is set to be Datum::nUNDEFINED.
         ** @param[in]     datum the datum to be classified.
         ** @param[in]     rTruth the ground truth label associated with datum. [Datum::nUNDEFINED]
         ** @brief Set datum contents.
         */
        // --------------------------------------------------------------
        void Datum::set(const FeatureVectorType& rDatum)
        {
            _datum        = rDatum;
        }
        
        
        // --------------------------------------------------------------
        /** 
         ** Datum::set() used to set contents of datum when you only have pointers to the feature data. 
         ** If unavailable, ground truth is set to be Datum::nUNDEFINED.
         ** @param[in]     rNumDims the number of feature dimensions in the vector.
         ** @param[in]     pFeatVector the pointer to the feature data.
         ** @param[in]     rTruth the ground truth label associated with datum. [Datum::nUNDEFINED]
         ** @brief Set datum contents from data pointer.
         */
        // --------------------------------------------------------------
        void Datum::set(const unsigned int& rNumDims, const double* pFeatVector)
        {
            // now get the feature data
            _datum.clear();
            _datum.reserve(rNumDims);
            for (unsigned int id = 0; id < rNumDims; ++id)
            {
                _datum.push_back(pFeatVector[id]);
            }
            
        }
        
        
        // --------------------------------------------------------------
        /** Datum::print() prints all the data contained in the current object.
         ** @brief print Datum object.
         */
        // --------------------------------------------------------------
        void Datum::print() const
        {
            std::cout << "[ " ;
            for (FeatureVectorType::const_iterator cit = _datum.begin(); cit != _datum.end(); ++cit)
                std::cout << *cit << " ";
            std::cout << "]" << std::endl;
        }
        
        
    }
}
