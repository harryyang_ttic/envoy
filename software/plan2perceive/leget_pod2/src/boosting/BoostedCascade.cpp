// --------------------------------------------------------------
/** @file AttentionalCascade.cpp
 ** Implementation of the class BoostedCascade.
 **
 ** @brief Implementation of BoostedCascade.
 **
 ** @author Ingmar Posner 
 ** @date 10/6/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#include <math.h>
#include <typeinfo>
#include <boost/filesystem/operations.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <tiny-xml/tinyxml.h>
#include "leget/boosting/BoostedClf.h"
#include "leget/boosting/BoostedCascade.h"
#include "leget/misc/Exceptions.h"


namespace leget
{
    namespace boosting
    {
        
        // --------------------------------------------------------------
        /**
         ** Applies the attentional cascade to a given ROI.
         ** Features are generated using the feature generator contained in the class.
         **
         ** @param[in]     
         ** @return        a bool indicating class (true) or non-class (false).
         */
        // --------------------------------------------------------------
        //bool BoostedCascade::testAt(const unsigned int &uX, const unsigned int &uY, 
        //                            const unsigned int &uW, const unsigned int &uH)
        bool BoostedCascade::testAt(const features::FeatureGenerator::Window& window, 
                                    const features::FeatureGenerator::IdFeatureMap& idFeatureMap,
                                    double& cumScore) const
        {
            // let's make ourselves an 'on-demand' feature generator
            features::FeatureGenerator generator(window);
            
            // a datum to fill in
            Datum datum;
            
            // this guy is going to keep a running score of the datum as it progresses through the cascade
            cumScore = 0.0;

            // go through cascade levels
            std::list< boost::shared_ptr<CascadeLevel> >::const_iterator citLevels;
            for (citLevels = _levels.begin(); citLevels != _levels.end(); ++citLevels) 
            {
                // get the relevant features for this cascade level
                FeatureIdSet& levelIdSet = (*citLevels)->_featureIds;
                generator.getDatum(idFeatureMap, levelIdSet, datum);
                
                // apply level classifier
                double score = (*citLevels)->_pClf->test(datum);
                
                
                //DEBUG
                //std::cout << "Level: " << level++ << "   Thresh: " << (*citLevels)->_thresh << "   Score: " << score << "   Datum: ";
                //datum.print();
                
                
                
                // do we need to go on?
                if (score <= (*citLevels)->_thresh)
                {
                    // didn't make it
                    return false;
                }
                else
                {
                	// update cumulative score
                	cumScore += score;
                }
            }    
            
            // if we get here the roi has made it through the cascade! Return positive result.
            return true;
        }
         
        
        // --------------------------------------------------------------
        /**
         ** Load the model from a directory of xml files.
         ** 
         ** @param[in]     sDirName the name of the directory to load from.
         **
         */
        // --------------------------------------------------------------
        void BoostedCascade::loadModel(const std::string &dirName)
        {
            // search for "index.xml"
            std::string sFileName = dirName + "/index.xml";
            if (!boost::filesystem::exists(sFileName))
                throw leget::error::IOError("[ERROR] BoostedCascade::loadModel() - Could not find index.xml for cascade classifier in " + dirName);
            
            // parse "index.xml"
            TiXmlDocument doc(sFileName.c_str());
            bool bSuccess = doc.LoadFile();
            if (!bSuccess)
                throw leget::error::IOError("[ERROR] BoostedCascade::loadModel() - Could not load file: " + sFileName);
            TiXmlHandle docHandle (& doc);
            
            // check classifier type is correct
            TiXmlElement *pTypeElement = docHandle.FirstChild("Classifier_Type").ToElement();
            if(!pTypeElement)
                throw leget::error::IOError("[ERROR] BoostedCascade::loadModel() - Could not find classifier type in file: " + sFileName);
            const char *pBuff = pTypeElement->GetText();
            unsigned int uClfType;
            try
            {
                uClfType = boost::lexical_cast<unsigned int>(std::string(pBuff));
            }
            catch (boost::bad_lexical_cast &)
            {
                throw leget::error::IOError("[ERROR] BoostedCascade::loadModel() - could not parse classifier type.");
            }
            
            //if (uClfType != BOOSTED_CASCADE_CLASSIFIER)
            //    throw leget::error::IOError("[ERROR] BoostedCascade::loadModel() - wrong classifier type!");
            
            // OK, now parse the parameters.
            
            // ... get type of weak clfs
            TiXmlElement *pWeakClfTypeElement = docHandle.FirstChild("Parameters").FirstChild("Weak_Classifier_Type").ToElement();
            if(!pWeakClfTypeElement)
                throw leget::error::IOError("[ERROR] BoostedCascade::loadModel() - Could not find parameter 'Weak_Classifier_Type' in file: " + sFileName);
            pBuff = pWeakClfTypeElement->GetText();
            unsigned int uType;
            try
            {
                uType = boost::lexical_cast<unsigned int>(std::string(pBuff));
            }
            catch (boost::bad_lexical_cast &)
            {
                throw leget::error::IOError("[ERROR] BoostedCascade::loadModel() - could not parse parameter 'Weak_Classifier_Type' from file: " + sFileName);
            }
            
            
            // ... how many levels are there?
            unsigned int numLevels = 0;
            TiXmlElement *pNumLevelsElement = docHandle.FirstChild("Parameters").FirstChild("Number_Of_Levels").ToElement();
            if(!pNumLevelsElement)
                throw leget::error::IOError("[ERROR] BoostedCascade::loadModel() - Could not find parameter 'Number_Of_Levels' in file: " + sFileName);
            pBuff = pNumLevelsElement->GetText();
            try
            {
                numLevels = boost::lexical_cast<unsigned int>(std::string(pBuff));
            }
            catch (boost::bad_lexical_cast &)
            {
                throw leget::error::IOError("[ERROR] BoostedCascade::loadModel() - could not parse parameter 'Number_Of_Levels' from file: " + sFileName);
            }
            
            
            // OK, now load the levels
            std::string sLevelNum;
            for (unsigned int uI = 0; uI < numLevels; ++uI)
            {
                // get file name under which model is stored
                sLevelNum = "Level_" + boost::lexical_cast<std::string>(uI);
                TiXmlElement *pLevelFNameElement = docHandle.FirstChild("Levels").FirstChild(sLevelNum.c_str()).FirstChild("Classifier_Location").ToElement();
                if(!pLevelFNameElement)
                    throw leget::error::IOError("[ERROR] BoostedCascade::loadModel() - Could not find filename for level " + sLevelNum + " in file: " + sFileName);
                pBuff = pLevelFNameElement->GetText();
                std::string sSubDirName(pBuff);
                std::string sFullName(dirName + "/" + sSubDirName);
                
                // load a level (i.e. a booster) from file
                boost::shared_ptr<CascadeLevel> pLevel(new CascadeLevel(sFullName));
                               
                // ... get threshold
                TiXmlElement *pThreshElement = docHandle.FirstChild("Levels").FirstChild(sLevelNum.c_str()).FirstChild("Threshold").ToElement();
                if(!pThreshElement)
                    throw leget::error::IOError("[ERROR] BoostedCascade::loadModel() - Could not find parameter 'Threshold' for " + sLevelNum + " in " + sFileName);
                pBuff = pThreshElement->GetText();
                try
                {
                    pLevel->setThreshold(boost::lexical_cast<double>(std::string(pBuff)));
                }
                catch (boost::bad_lexical_cast &)
                {
                    throw leget::error::IOError("[ERROR] BoostedCascade::loadModel() - could not parse parameter parameter 'Threshold' for " + sLevelNum + " in " + sFileName);
                }
                
                // ... populate feature id list
                pLevel->_pClf->getFeatureIds(pLevel->_featureIds);
                
                // add the level to the cascade
                _levels.push_back(pLevel);
                
            }
        }
        
        
        // --------------------------------------------------------------
        /**
         ** BoostedCascade::printInfo() displays cascade parameters.
         */
        // --------------------------------------------------------------
        void BoostedCascade::printInfo()
        {
            
            std::cout << "===============================" << std::endl;
            std::cout << "BOOSTED CASCADE" << std::endl;
            std::cout << "===============================" << std::endl;
            std::cout << "Weak classifier type: ";
            std::cout << "Number of levels: " << _levels.size() << std::endl;
            std::cout << std::endl << "Level Details: " << std::endl;
            std::list< boost::shared_ptr<CascadeLevel> >::const_iterator citLevels;
            unsigned int uLevelIdx = 0;
            for (citLevels = _levels.begin(); citLevels != _levels.end(); ++citLevels, ++uLevelIdx)
            {
                std::cout << "*** Level " << uLevelIdx << " ***" << std::endl;
                std::cout << "Features used: [";
                FeatureIdSet::const_iterator citIds;
                for (citIds = (*citLevels)->_featureIds.begin(); citIds != (*citLevels)->_featureIds.end(); ++ citIds)
                {
                    std::cout << *citIds << " ";
                }
                std::cout << "]" << std::endl;
                std::cout << "Threshold: " << (*citLevels)->_thresh << std::endl;
                (*citLevels)->_pClf->printInfo();
            }
            std::cout << std::endl << std::endl;
            std::cout << "===============================" << std::endl;
            std::cout << "===============================" << std::endl;
        }
        
        
                
    }
}











