/**
 * @mainpage  Leget \n\n <SMALL> The TextSpotting Library \SMALL
 *
 * @author Ingmar Posner
 * @date 20/4/2011
 *
 * <b> (c) Copyright Oxford University 2011 </b>
 *
 * <p>The leget library implements a fairly standard approach to text detection and parsing in natural scene images.
 * It is intended to provide a building block for roboticists to exploit text in man-made environments for robotics applications. \p
 *
 *
 * <p>The idea is, of course, that you can simply link against leget to provide off-the-shelf text-spotting. Build instructions can be found @link BuildSec below. @endlink
 * The api consists only of a single header: TextSpotter.h. In addition, Leget comes with a command-line utility for text-spotting (spot_text) which
 * provides an example of how Leget is used as well as a quick way of evaluating its capabilities on images of your choosing. Note
 * that spot_text does not allow you to set all of the parameters provided in the API. Looking at the documentation in the source is imperative! \p
 *
 * \n
 * @section HowSec How It Works...
 * <p> A short overview: \p
 * <OL>
 * <LI> A boosted cascade classifier detects windows of putative text at multiple scales.\LI
 * <LI> These detections are accumulated in a voting array. An absolute threshold on these
 *      votes is applied to discard spurious detections. \LI
 * <LI> Then contour analysis is performed on the voting array in order to delimit putative text regions according to
 *      another threshold specified relative to the local vote maximum.\LI
 * <LI> The areas resulting from this last thresholding operation are passed to the OCR engine for parsing.\LI
 * <LI> The resulting text strings and detection confidences are post-processed using a probabilistic spell-checking scheme.\LI
 * \OL
 * <p>Apart from obvious parameters such as window size and number of scales, Leget is most sensitive to the relative threshold used to determine
 * the putative text regions! Depending on the value these regions will be smaller (high threshold) or larger (low threshold). This can be examined
 * using the output produced by the sample command-line tool spot_text.\p
 *
 * \n
 * @section BuildSec Build Instructions...
 *
 * @subsection ingredents Ingredients
 * <p>
 * Leget has the following dependencies:
 * <UL>
 * <LI> The Tesseract OCR engine. Leget depends on a modified version of Tesseract 3 which ships with Leget (in the "thirdparty" directory). \LI
 * <LI> Leptonica. This really is a dependency introduced by Tesseract. Leget has been tested with Leptonica 1.62 (on Ubuntu) and Leptonica 1.66 (on OS-X) \LI
 * <LI> Boost. Leget has been tested with Boost 1.40.0 (on Ubuntu 10.04) and Boost 1.44.0 (on OS-X). \LI
 * <LI> OpenCV. Leget needs OpenCV 2.1. (Neither 2.0 nor 2.2 will work.) \LI
 * <LI> LibTiff (required by Tesseract/Leptonica). \LI
 * <LI> LibGif (required by Tesseract/Leptonica). \LI
 * <LI> CMake 2.8. Leget uses the CMake build system and will be available for linking as an exported package. \LI
 * \UL
 * \p
 *
 * \n
 * @subsection linux LINUX (tested on UBUNTU 10.04)
 * <p> Installation on Linux is relatively straightforward since most of the dependencies can be installed using apt packages. \p
 * <OL>
 * <LI> <b> Leptonica: </b> Install the libleptonica-dev apt package. \LI
 * <LI> <b> Boost: </b> Install the libboost-all-dev apt package. \LI
 * <LI> <b> OpenCV: </b> An apt package for OpenCV 2.1 together with install instructions can be found at http://opencv.willowgarage.com/wiki/Ubuntu_Packages . \LI
 * <LI> <b> LibGif, LibTiff: </b> Use apt packages. \LI
 * <LI> <b> Tesseract: </b> Now build the modified version of Tesseract provided in the thirdparty directory. It uses autoconf:  For a 32bit build:
 *          @code ./configure --prefix=<path to install dir> CFLAGS="-O3 -m32" CXXFLAGS="-O3 -m32" @endcode
 *          @code make @endcode
 *          @code make install @endcode
 * 		    <p> There is more information on the installation of Tesseract 3.0 at http://code.google.com/p/tesseract-ocr/wiki/ReadMe . \p
 * 			<p> <b> NOTE: Make sure the TESSDATA prefix is set correctly! See the website above for more details. </b> \p
 * 		    <p> Once building and installing is complete it would be a good time to test tesseract by calling the tesseract binary with a sample image.
 *          There are some sample images in <leget_source>/examples/sampleImages! The cmd-line output will let you know whether it has found Leptonica.
 *          If not, Leget is not going to work!  \p \LI
 * <LI> <b> Then build Leget (see below). </b>
 * \OL
 *
 * \n
 * @subsection osx MAC OS-X (tested on 10.6.6 SNOW LEOPARD)
 * <p> Installation on Mac is almost straightforward. MacPorts can be used for some of the load. \p
 * <OL>
 * <LI> <b> Leptonica: </b> Install it from source using autoconf. I suggest you install it globally since Tesseract will otherwise not pick it up - the usual autoconf tricks to point Tesseract at a
 *          custom installation were futile. \LI
 * <LI> <b> Boost: </b> Install using MacPorts \LI
 * <LI> <b> OpenCV: </b> Install from source using cmake. \LI
 * <LI> <b> LibGif, LibTiff: </b> Use MacPorts. \LI
 * <LI> <b> Tesseract: </b> Now build the modified version of Tesseract provided in the thirdparty directory. It uses autoconf:  For a 32bit build:
 *          @code ./configure --prefix=<path to build dir> CFLAGS="-O3 -m32" CXXFLAGS="-O3 -m32" LDFLAGS=-L/usr/local/lib LIBS=-lgif @endcode
 *          You needed the LDFLAGS and LIBS to point tesseract to Leptonica and make it link against it.
 *          @code make @endcode
 *          @code make install @endcode
 * 		    <p> There is more information on the installation of Tesseract 3.0 here: http://code.google.com/p/tesseract-ocr/wiki/ReadMe . \p
 * 			<p> <b> NOTE: Make sure the TESSDATA prefix is set correctly! See the website above for more details. </b> \p
 * 		    <p> Once building and installing is complete it would be a good time to test tesseract by calling the tesseract binary with a sample image.
 *          There are some sample images in <leget_source>/examples/sampleImages! The cmd-line output will let you know whether it has found Leptonica.
 *          If not, Leget is not going to work!  \p \LI
 * <LI> <b> Then build Leget (see below). </b>
 * \OL
 *
 * \n
 * @subsection leget Building Leget
 * <OL>
 * <LI> Leget uses CMake. So, in the directory you want to build the library in invoke @code ccmake <leget-source>/. @endcode \LI
 * <LI> Configure CMake - <b> check that it found correct versions of libgif, libtiff, leptonica include path and the leptonica library (liblept.a) </b> \LI
 * <LI> Make sure to specify Release for CMAKE_BUILD_TYPE. \LI
 * <LI> Make sure that TESSERACT_BASE_PATH is set correctly. \LI
 * <LI> Check that Boost and OpenCV have been picked up correctly (you may need to toggle into advanced mode for that). \LI
 * <LI> Configure and GOOOOOO! \LI
 * \OL
 * <p> Then, at the command prompt: @code make @endcode and @code make install @endcode and you are good to go. \p
 * <p> Go to the binary directory and invoke spot_text. That will show you the command-line options. There are some sample images in
 * <leget_source>/examples/sampleImages. Run spot_text on one of them to get an idea of what it can do.\p
 * <p> <b> NOTE: The Leget library allows for the specification of multiple relative thresholds which the library works through - selecting
 * the best according to a given metric. Currently, the metric is max. mean ocr confidence. However, qualitative assessment thus far
 * suggests that you are still better off using a single, predetermined threshold. Values found to perform well in a rage of situations
 * are those set as Leget defaults. </b>\p
 *
 * \n
 * @section linking Linking against Leget ...
 * <p>When using cmake, the easiest way to link against Leget is using the find_package facility in your CMakeLists.txt file:
 * @code find_package( Leget ) @endcode
 * @code include_directories( ${LEGET_INCLUDE_DIRS} ) @endcode
 * @code link_directories( ${LEGET_LINK_DIRS} ) @endcode
 * and when finally adding your executable or library target simply link against the Leget libraries:
 * @code add_executable( ... ${LEGET_LIBRARIES} ... ) @endcode
 * As well as providing these variables, cmake will also add defines for DEFAULT_CASCADE and DEFAULT_DICTIONARY for use in your code. \p
 *
 * <p>For linking the more conventional way lib and include folders can be found in the install directory.
 * Note that you will need to link against all the dependencies as well (i.e. Tesseract, OpenCV, etc).\p

 */
