// --------------------------------------------------------------
/** @file FeatureGenerator.cpp
 ** Implementation of the class FeatureGenerator.
 **
 ** @brief Implementation of FeatureGenerator.
 **
 ** @author Ingmar Posner 
 ** @date 15/6/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#include <map>
#include <set>
#include <string>
#include <cv.h>
#include "leget/features/FeatureGenerator.h"
#include "leget/boosting/Datum.h"
#include "leget/misc/Exceptions.h"


namespace leget
{
    namespace features
    {
        /** Make feature id -> <channel, feature> pair. */
        void FeatureGenerator::makeIdFeatureMap(const ChannelSet& channels, 
                                                const FeatureSet& features, 
                                                IdFeatureMap& idFeatureMap)
        {
            // fill in look-up
            // ...... Here we define a 'Feature-major' ordering, meaning consecutive 
            // ...... IDs refer to the different haar features all evaluated on the same channel until the numeber
            // ...... of haar features is exceeded - then the next channel is used.
            
            
            // clean slate
            idFeatureMap.clear();
            
            // create the map
            unsigned int featureId = 0;
            ChannelSet::const_iterator citChannels;
            FeatureSet::ConstIterator citFeatures;
            for (citChannels = channels.begin(); citChannels != channels.end(); ++citChannels)
            {
                for (citFeatures = features.begin(); citFeatures != features.end(); ++citFeatures, ++featureId)
                {
                    idFeatureMap[featureId] = std::make_pair(*citChannels, *citFeatures);
                }
            }
        }
        
        
        
        // --------------------------------------------------------------
        /**
         ** 
         ** @param[in] 
         */
        // --------------------------------------------------------------
        void FeatureGenerator::setRoi(const Window& window)
        {
            // sanity check
            assert( (window._w > 0) && (window._h > 0) );
            
            // clean slate on feature cache - with a new roi it is no longer valid
            _valueCache.clear();
            
            
            _roiRect.x = window._x;
            _roiRect.y = window._y;
            // adjust for integral image having an extra row and column
            _roiRect.width = window._w + 1;
            _roiRect.height = window._h + 1;
        }
        
        
        
        // --------------------------------------------------------------
        /**
         ** ...
         ** @param[in] 
         */
        // --------------------------------------------------------------
        void FeatureGenerator::getDatum(const IdFeatureMap& idLookup, 
                                        const FeatureIdSet& idList, 
                                        boosting::Datum& datum) 
        {
            // make sure the roi is set
            if ( (_roiRect.width == 0) || (_roiRect.height == 0) )
                throw error::Exception("FeatureGenerator::getDatum() - Generator must be initialised!");
            
            // Reserve the right size for Datum feature vect! We'll get that from the back of the sorted container.
            // @todo A sparse datum would be better here!
            unsigned int uSize = *(idList.rbegin());
            // ... remember, the features are also indexed by 0! So add 1...
            datum.setSize(uSize+1); 
            
            // go through the list of wanted features
            std::set<unsigned int>::const_iterator citWanted;
            std::map<unsigned int, double>::const_iterator citTheOne;
            std::map<unsigned int, double>::const_iterator citMapEnd = _valueCache.end();
            ChannelFeaturePair chanFeatPair;
            std::pair< cv::Mat_<double>, cv::Mat_<double> > channelPair;
            double dfValue;
            for (citWanted = idList.begin(); citWanted != idList.end(); ++citWanted)
            {
                // check if the feature already exists in our map
                citTheOne = _valueCache.find(*citWanted);
                if (citTheOne != citMapEnd)
                {
                    // if yes, stick it in the correct place in the feature vector
                    datum[*citWanted] = citTheOne->second;
                }
                else
                {
                    // if not, go and get the value
                    // ... get the <channel, feature> pair
                    chanFeatPair = idLookup.find(*citWanted)->second; 
                    
                    // DEBUG
                    //std::cout << "map size: " << idLookup.size() << "   wanted id: " <<  *citWanted << std::endl;
                    
                    // ... set region of interest (in this case for both integral and integral sqr images - 
                    // ... ... this is Haar specific.)
                    channelPair = chanFeatPair.first->getChannelPair();
                    _roi1 = channelPair.first(_roiRect);
                    _roi2 = channelPair.second(_roiRect);
                    
                    // ... get it.
                    dfValue = chanFeatPair.second->compute(std::make_pair(_roi1, _roi2));
                    
                    // stick it into the feature vector
                    datum[*citWanted] = dfValue;
                    
                    // and also cache it
                    _valueCache[*citWanted] = dfValue;
                }
            }
        }
                
    }
}

