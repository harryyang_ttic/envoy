// --------------------------------------------------------------
/** @file FeatureSet.cpp
 ** Implementation of the class FeatureSet.
 **
 ** @brief Implementation of FeatureSet.
 **
 ** @author Ingmar Posner 
 ** @date 10/02/2011
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#include "leget/features/FeatureSet.h"
#include "leget/features/HaarFeature.h"

namespace leget
{
    namespace features
    {
        // --------------------------------------------------------------
        /**
         ** Eventually this will load features from file. Currently they are hardcoded here.
         */
        // --------------------------------------------------------------
        void FeatureSet::load()
        {
            // add mean features
            tempLoadHelper<MeanBlock>(_features);
            
            // add variance features
            tempLoadHelper<VarianceBlock>(_features); 
        }
        
        
        
        
        // --------------------------------------------------------------
        /**
         ** FeatureGenerator::TempLoadHelper() contains the hard-coded features.
         ** @note TEMPORARY ONLY!
         */
        // --------------------------------------------------------------
        template<typename FeatureType>
        void FeatureSet::tempLoadHelper(FeatureContainer& features) 
        {
                        
            // ==========
            // GEOMETRY 1
            // ==========
            
            // ... blocks
            boost::shared_ptr<FeatureType> pBlock1(new FeatureType(0, 0, 1, 0.1667));
            boost::shared_ptr<FeatureType> pBlock2(new FeatureType(0, 0.1667, 1, 0.8333));
            boost::shared_ptr<FeatureType> pBlock3(new FeatureType(0, 0.8333, 1, 1));
            // ... geometry
            boost::shared_ptr< HaarGeometry<FeatureType> > pGeometry1(new HaarGeometry<FeatureType>(pBlock1));
            pGeometry1->AddBlock(pBlock2);
            pGeometry1->AddBlock(pBlock3);
            
            // ... coeffs & feature 1
            cv::Mat_<double> coeffs1(3, 1);
            coeffs1.ptr<double>()[0] = -1.0;
            coeffs1.ptr<double>()[1] = 1.0;
            coeffs1.ptr<double>()[2] = 0.0;
            //HaarMeanFeature feature1(pGeometry, coeffs1);
            boost::shared_ptr< HaarFeature<FeatureType> > pFeature1(new HaarFeature<FeatureType>(pGeometry1, coeffs1));
            _features.push_back(pFeature1);
            
            // ... coeffs & feature 2    
            cv::Mat_<double> coeffs2(3, 1);
            coeffs2.ptr<double>()[0] = 0.0;
            coeffs2.ptr<double>()[1] = 1.0;
            coeffs2.ptr<double>()[2] = -1.0;
            //HaarMeanFeature feature2(pGeometry, coeffs2);
            //HaarFeature<FeatureType> feature2(pGeometry, coeffs2);
            boost::shared_ptr< HaarFeature<FeatureType> > pFeature2(new HaarFeature<FeatureType>(pGeometry1, coeffs2));
            features.push_back(pFeature2);
            
            // ... coeffs & feature 3
            cv::Mat_<double> coeffs3(3, 1);
            coeffs3.ptr<double>()[0] = -1.0;
            coeffs3.ptr<double>()[1] = 1.0;
            coeffs3.ptr<double>()[2] = -1.0;
            //HaarMeanFeature feature3(pGeometry, coeffs3);
            //HaarFeature<FeatureType> feature3(pGeometry1, coeffs3);
            boost::shared_ptr< HaarFeature<FeatureType> > pFeature3(new HaarFeature<FeatureType>(pGeometry1, coeffs3));
            features.push_back(pFeature3);
            
            
            // ==========
            // GEOMETRY 2
            // ==========
            
            // ... blocks
            boost::shared_ptr<FeatureType> pBlock4(new FeatureType(0, 0, 1, 0.1111));
            boost::shared_ptr<FeatureType> pBlock5(new FeatureType(0, 0.1111, 1, 0.4444));
            boost::shared_ptr<FeatureType> pBlock6(new FeatureType(0, 0.4444, 1, 0.5556));
            boost::shared_ptr<FeatureType> pBlock7(new FeatureType(0, 0.5556, 1, 0.8889));
            boost::shared_ptr<FeatureType> pBlock8(new FeatureType(0, 0.8889, 1, 1));
            
            // ... geometry
            boost::shared_ptr< HaarGeometry<FeatureType> > pGeometry2(new HaarGeometry<FeatureType>(pBlock4));
            pGeometry2->AddBlock(pBlock5);
            pGeometry2->AddBlock(pBlock6);
            pGeometry2->AddBlock(pBlock7);
            pGeometry2->AddBlock(pBlock8);
            
            // ... coeffs & feature 4
            cv::Mat_<double> coeffs4(5, 1);
            coeffs4.ptr<double>()[0] = -1.0;
            coeffs4.ptr<double>()[1] = 1.0;
            coeffs4.ptr<double>()[2] = -1.0;
            coeffs4.ptr<double>()[3] = 0.0;
            coeffs4.ptr<double>()[4] = 0.0;
            boost::shared_ptr< HaarFeature<FeatureType> > pFeature4(new HaarFeature<FeatureType>(pGeometry2, coeffs4));
            features.push_back(pFeature4);
            
            // ... coeffs & feature 5    
            cv::Mat_<double> coeffs5(5, 1);
            coeffs5.ptr<double>()[0] = 0.0;
            coeffs5.ptr<double>()[1] = 0.0;
            coeffs5.ptr<double>()[2] = -1.0;
            coeffs5.ptr<double>()[3] = 1.0;
            coeffs5.ptr<double>()[4] = -1.0;
            //HaarMeanFeature feature2(pGeometry, coeffs2);
            //HaarFeature<FeatureType> feature2(pGeometry, coeffs2);
            boost::shared_ptr< HaarFeature<FeatureType> > pFeature5(new HaarFeature<FeatureType>(pGeometry2, coeffs5));
            features.push_back(pFeature5);
            
            // ... coeffs & feature 6
            cv::Mat_<double> coeffs6(5, 1);
            coeffs6.ptr<double>()[0] = -1.0;
            coeffs6.ptr<double>()[1] = 1.0;
            coeffs6.ptr<double>()[2] = -1.0;
            coeffs6.ptr<double>()[3] = 1.0;
            coeffs6.ptr<double>()[4] = -1.0;
            //HaarMeanFeature feature3(pGeometry, coeffs3);
            //HaarFeature<FeatureType> feature3(pGeometry1, coeffs3);
            boost::shared_ptr< HaarFeature<FeatureType> > pFeature6(new HaarFeature<FeatureType>(pGeometry2, coeffs6));
            features.push_back(pFeature6);
            
            
            // ==========
            // GEOMETRY 3
            // ==========
            
            // ... blocks
            boost::shared_ptr<FeatureType> pBlock9(new FeatureType(0.0, 0.0, 0.3333, 1.0));
            boost::shared_ptr<FeatureType> pBlock10(new FeatureType(0.3333, 0.0, 0.6667, 1.0));
            boost::shared_ptr<FeatureType> pBlock11(new FeatureType(0.6667, 0.0, 1.0, 1.0));
            // ... geometry
            boost::shared_ptr< HaarGeometry<FeatureType> > pGeometry3(new HaarGeometry<FeatureType>(pBlock9));
            pGeometry3->AddBlock(pBlock10);
            pGeometry3->AddBlock(pBlock11);
            
            // ... coeffs & feature 7
            cv::Mat_<double> coeffs7(3, 1);
            coeffs7.ptr<double>()[0] = -1.0;
            coeffs7.ptr<double>()[1] = 1.0;
            coeffs7.ptr<double>()[2] = 0.0;
            boost::shared_ptr< HaarFeature<FeatureType> > pFeature7(new HaarFeature<FeatureType>(pGeometry3, coeffs7));
            features.push_back(pFeature7);
            
            // ... coeffs & feature 8    
            cv::Mat_<double> coeffs8(3, 1);
            coeffs8.ptr<double>()[0] = 0.0;
            coeffs8.ptr<double>()[1] = 1.0;
            coeffs8.ptr<double>()[2] = -1.0;
            boost::shared_ptr< HaarFeature<FeatureType> > pFeature8(new HaarFeature<FeatureType>(pGeometry3, coeffs8));
            features.push_back(pFeature8);
            
            // ... coeffs & feature 9
            cv::Mat_<double> coeffs9(3, 1);
            coeffs9.ptr<double>()[0] = -1.0;
            coeffs9.ptr<double>()[1] = 1.0;
            coeffs9.ptr<double>()[2] = -1.0;
            boost::shared_ptr< HaarFeature<FeatureType> > pFeature9(new HaarFeature<FeatureType>(pGeometry3, coeffs9));
            features.push_back(pFeature9);
            
            
            // ==========
            // GEOMETRY 4
            // ==========
            
            // ... blocks
            boost::shared_ptr<FeatureType> pBlock12(new FeatureType(0.0, 0.0, 1.0, 0.5));
            boost::shared_ptr<FeatureType> pBlock13(new FeatureType(0.0, 0.5, 1.0, 1.0));
            // ... geometry
            boost::shared_ptr< HaarGeometry<FeatureType> > pGeometry4(new HaarGeometry<FeatureType>(pBlock12));
            pGeometry4->AddBlock(pBlock13);
            
            // ... coeffs & feature 10
            cv::Mat_<double> coeffs10(2, 1);
            coeffs10.ptr<double>()[0] = -1.0;
            coeffs10.ptr<double>()[1] = 1.0;
            boost::shared_ptr< HaarFeature<FeatureType> > pFeature10(new HaarFeature<FeatureType>(pGeometry4, coeffs10));
            features.push_back(pFeature10);
            
            
            // ==========
            // GEOMETRY 5
            // ==========
            
            // ... blocks
            boost::shared_ptr<FeatureType> pBlock14(new FeatureType(0.0, 0.0, 0.5, 1.0));
            boost::shared_ptr<FeatureType> pBlock15(new FeatureType(0.5, 0.0, 1.0, 1.0));
            // ... geometry
            boost::shared_ptr< HaarGeometry<FeatureType> > pGeometry5(new HaarGeometry<FeatureType>(pBlock14));
            pGeometry5->AddBlock(pBlock15);
            
            // ... coeffs & feature 11
            cv::Mat_<double> coeffs11(2, 1);
            coeffs11.ptr<double>()[0] = -1.0;
            coeffs11.ptr<double>()[1] = 1.0;
            boost::shared_ptr< HaarFeature<FeatureType> > pFeature11(new HaarFeature<FeatureType>(pGeometry5, coeffs11));
            features.push_back(pFeature11);
            
        }
        
    }
}
