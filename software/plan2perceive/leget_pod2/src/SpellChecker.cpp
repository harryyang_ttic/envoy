// --------------------------------------------------------------
/** @file SpellChecker.cpp
 ** Implementation of the class SpellChecker.
 **
 ** @brief Implementation of SpellChecker.
 **
 ** @author Ingmar Posner 
 ** @date 11/7/2010
 **
 ** (c) Copyright Oxford University 2011
 */
// --------------------------------------------------------------

#include <iostream>
#include <fstream>
#include <cmath>
#include <boost/lexical_cast.hpp>
#include <vector>
#include <algorithm>
#include "leget/misc/SpellChecker.h"
#include "leget/misc/Exceptions.h"


namespace leget
{
    
    // --------------------------------------------------------------
    /**
     ** SpellChecker::GetWordSuggestion() 
     **
     ** is case-sensitive (but input and dict entries should be converted to lower case beforehand!)
     **
     **
     */
    // --------------------------------------------------------------
    void SpellChecker::getWordSuggestion(const std::string& sIn, 
                                         const double& ocrConfidence,
                                         std::string& sSuggestion, 
                                         double &dfWordPosterior) const 
    {
        
        //unsigned int uD = CalcEditDistance("gambol", "gumbo");
        
        
        // go through dictionary
        unsigned int uDist;
        double dfP_d_given_w, dfP_w, dfP_d_and_w;
        double dfMaxJoint = 0.0;
        double dfSumLikelihood = 0.0;
        double dfAlphaInv = 1.0 / _alpha;
        TDictionary::const_iterator citWords;
        TDictionary::const_iterator citWordsEnd = _dictionary.end();
        unsigned int uIdx = 0;
        for (citWords = _dictionary.begin(); citWords != citWordsEnd; ++citWords, ++uIdx)
        {    
            // calc p(d | w) as exp(-editDist)
            //... calculate the Levenshtein edit distance
            uDist = editDistance(sIn, citWords->first);
            dfP_d_given_w = std::exp(-1.0 * _alpha * static_cast<double>(uDist)) * dfAlphaInv;
            
            // p(w) is simply the frequency count in the dictionary
            dfP_w = citWords->second;
            
            dfP_d_and_w = dfP_d_given_w * dfP_w;
            
            // record if this is the most promising so far
            if (dfP_d_and_w > dfMaxJoint)
            {
                dfMaxJoint = dfP_d_and_w;
                sSuggestion = citWords->first;
            }
            
            // collect terms for normalization
            dfSumLikelihood += dfP_d_and_w;
        }
        
        // now get the posterior p(w|d)
        if (dfSumLikelihood > 0.0)
            dfWordPosterior = (dfMaxJoint / dfSumLikelihood) * ocrConfidence;
        else 
            dfWordPosterior = 0.0;
    }
    
    
    
    // --------------------------------------------------------------
    /**
     ** SpellChecker::CheckSpelling() 
     **
     ** expect file format: lines (N = number of terms) with items separated by space:
     ** - # of occurrences
     ** - term
     ** anything after that is ignored.
     */
    // --------------------------------------------------------------
    void SpellChecker::loadDictionary(const std::string &sFileName)
    {
        // open dictionary file
        std::ifstream dictFile(sFileName.c_str());
        if (!dictFile.is_open())
            throw error::IOError("Could not open dictionary file: " + sFileName);
        
        // fill _dictionary
        std::string sLine;
        std::string sTerm;
        std::string sFreq;
        std::string sRemainder;
        double dfFreq;
        unsigned int uIdxFirst, uIdxSecond;
        while (!dictFile.eof())
        {
            std::getline(dictFile, sLine);
            
            if (sLine.empty())
                continue;
            
            // find item delineations
            uIdxFirst = sLine.find_first_of(" ");
            uIdxSecond = sLine.find_first_of(" ", uIdxFirst + 1);
            
            
            // get term
            sTerm.clear();
            if (uIdxSecond != std::string::npos)
            {
                sTerm = sLine.substr(uIdxFirst + 1, uIdxSecond - (uIdxFirst+1));
            }
            else
            {
                sTerm = sLine.substr(uIdxFirst+1);
            }
            
            
            // get frequency
            sFreq = sLine.substr(0, uIdxFirst);
            try
            {
                dfFreq = boost::lexical_cast<double>(sFreq);
            }
            catch (boost::bad_lexical_cast &)
            {
                throw error::IOError("Could not read term frequency for term " + sTerm + " from file: " + sFileName);
            }
            
            // convert to lower case!
            std::transform(sTerm.begin(), sTerm.end(), sTerm.begin(), (int(*)(int)) tolower);
            
            // add to dictionary
            std::pair<TDictionary::const_iterator, bool> result;
            result = _dictionary.insert(std::make_pair(sTerm, dfFreq));
            if(!result.second)
            {
                // cater for the dictionary counting the same term in different positions
                _dictionary[sTerm] += dfFreq;
            }
        }
        
        dictFile.close();
        
        // now go through the dictionary and count total number of word occurances
        double dfNumTerms = 0.0;
        TDictionary::const_iterator citWords;
        TDictionary::const_iterator citWordsEnd = _dictionary.end();
        for (citWords = _dictionary.begin(); citWords != citWordsEnd; ++citWords)
        {
            dfNumTerms += citWords->second;
        }
        
        // and normalize to get occurence likelihoods
        TDictionary::iterator itWords;
        TDictionary::iterator itWordsEnd = _dictionary.end();
        for (itWords = _dictionary.begin(); itWords != itWordsEnd; ++itWords)
        {
            itWords->second = itWords->second / dfNumTerms; 
        }
    }
    
    
    // --------------------------------------------------------------
    /**
     ** SpellChecker::CalcEditDistance() 
     **
     **
     */
    // --------------------------------------------------------------
    unsigned int SpellChecker::editDistance(const std::string &sWord1, const std::string &sWord2) const
    {
        
        /**@todo This function is a time-sink! Think about an alternative!*/
        
        // We are going to do this the old-fashioned way, by computing a matrix.
        // We'll take our matrix to be stored in col-major ordering : M[i, j] = M[i+n*j]
        // Also, allow for an extra row and column: the first row and column is for the empty string    
        unsigned int uW1Length = sWord1.size();
        unsigned int uW2Length = sWord2.size();
        unsigned int uXDims = uW2Length + 1;
        unsigned int uYDims = uW1Length + 1;    
        std::vector<unsigned int> distMat( uXDims * uYDims );
        
        for(unsigned int uI = 0; uI < uYDims; ++uI)
            distMat[uI] = uI;        // distMat[uI,0]
              
        for(unsigned int uJ = 0; uJ < uXDims; ++uJ) 
            distMat[uYDims * uJ] = uJ;      // distMat[0,uJ]
               
        unsigned int uCost;
        unsigned int uDist;
        for(unsigned int uI = 1; uI < uYDims; ++uI)
        {   
            for(unsigned int uJ = 1; uJ < uXDims; ++uJ) 
            {
                
                uCost = ((sWord1[uI-1] == sWord2[uJ-1]) ? 0 : 1);
                
                uDist = std::min(distMat[uI - 1 + uYDims * uJ] + 1, // deletion
                                 std::min(distMat[ uI + uYDims * (uJ - 1)] + 1, // insertion
                                          distMat[(uI - 1) + uYDims * (uJ - 1)] + uCost));// substitution
                                
                distMat[uI+uYDims*uJ] = uDist;
                
            }
        }
                
        // return the final edit distance.    
        return distMat[uW1Length + uYDims * uW2Length];
    }
    
}


