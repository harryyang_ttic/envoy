
#include "stereo_engine_t.hpp"


using namespace ptp;
using namespace ptp::stereo;
using namespace ptp::common;
using namespace ptp::interfaces;

//=====================================================================

stereo_engine_t::stereo_engine_t()
{}

//=====================================================================

void stereo_engine_t::load( const std::string& filename ) 
{
  // Create a new cv store and load the matrices we want from it
  CvFileStorage* fs = cvOpenFileStorage( filename.c_str(), 0, CV_STORAGE_READ );
  CvMat* temp;

  temp = (CvMat*)cvReadByName( fs, NULL, "mx1" );
  _mx1 = opencv::copy( temp );
  cvReleaseMat( &temp );

  temp = (CvMat*)cvReadByName( fs, NULL, "my1" );
  _my1 = opencv::copy( temp );
  cvReleaseMat( &temp );

  temp = (CvMat*)cvReadByName( fs, NULL, "mx2" );
  _mx2 = opencv::copy( temp );
  cvReleaseMat( &temp );

  temp = (CvMat*)cvReadByName( fs, NULL, "my2" );
  _my2 = opencv::copy( temp );
  cvReleaseMat( &temp );

  temp = (CvMat*)cvReadByName( fs, NULL, "_q" );
  _disparity_to_depth_Q = opencv::copy( temp );
  cvReleaseMat( &temp );

  cvReleaseFileStorage( &fs );
  
  // store the calibration filename
  _calibration_filename = filename;

  // default block matching parameters
  _num_disparity = 0;
  _min_disparity = 0;
  _sad_window_size = 21;
}

//=====================================================================

void stereo_engine_t::set_block_matching_param( const int& num_disparity,
						const int& min_disparity,
						const int& sad_window_size )
{
  _num_disparity = num_disparity;
  _min_disparity = min_disparity;
  _sad_window_size = sad_window_size;
}

//=====================================================================

bool stereo_engine_t::calibration_loaded() const
{
  return ( !_mx1.empty() && !_my1.empty() && 
	   !_mx2.empty() && !_my2.empty() && 
	   !_disparity_to_depth_Q.empty() );
}

//=====================================================================

void stereo_engine_t::expected_image_size( int& height, int& width ) const
{
  if( calibration_loaded() == false )
    BOOST_THROW_EXCEPTION( exceptions::calibration_not_loaded_error() );
  
  height = _mx1.rows;
  width = _mx1.cols;
}

//=====================================================================

bool stereo_engine_t::is_of_expected_size( const cv::Mat& a ) const
{
  if( a.empty() )
    return false;
  int r, c;
  expected_image_size( r, c );
  return ( a.rows == r && a.cols == c );
}

//=====================================================================

cv::Mat stereo_engine_t::rectify_left( const cv::Mat& left_image ) const
{

  // Make sure we have a calibration and correct image size
  if( calibration_loaded() == false )
    BOOST_THROW_EXCEPTION( exceptions::calibration_not_loaded_error() );
  if( is_of_expected_size( left_image ) == false ) 
    BOOST_THROW_EXCEPTION( exceptions::image_size_error() );

  // Call cvRemap with the left mapping ( marices 1 )
  int h, w;
  expected_image_size( h, w );
  cv::Mat left_rec = cv::Mat( h, w, CV_8U );
  //cv::remap( left_image, left_rec , _mx1, _my1, cv::INTER_LINEAR );
  // HACK to NOT RECTIFY
  left_rec = left_image;
  
  // return new rectified image
  return left_rec;
}

//=====================================================================

cv::Mat stereo_engine_t::rectify_right( const cv::Mat& right_image ) const
{

  // Make sure we have a calibration and correct image size
  if( calibration_loaded() == false )
    BOOST_THROW_EXCEPTION( exceptions::calibration_not_loaded_error() );
  if( is_of_expected_size( right_image ) == false ) 
    BOOST_THROW_EXCEPTION( exceptions::image_size_error() );

  // Call cvRemap with the left mapping ( marices 1 )
  int h, w;
  expected_image_size( h, w );
  cv::Mat right_rec = cv::Mat( h, w, CV_8U );
  //cv::remap( right_image, right_rec , _mx2, _my2, cv::INTER_LINEAR );
  // HACK to NOT RECTIFY
  right_rec = right_image;
  
  // return new rectified image
  return right_rec;
}

//=====================================================================

cv::Mat stereo_engine_t::disparity_map_from_rectified( const cv::Mat& left_image,
							 const cv::Mat& right_image ) const
{
  // Make usre we have a calibration, and correct image size
  if( calibration_loaded() == false )
    BOOST_THROW_EXCEPTION( exceptions::calibration_not_loaded_error() );
  if( is_of_expected_size( left_image ) == false )
    BOOST_THROW_EXCEPTION( exceptions::image_size_error() );
  if( is_of_expected_size( right_image ) == false )
    BOOST_THROW_EXCEPTION( exceptions::image_size_error() );

  // Create a disparity matrix. This is a floating point matrix
  int h, w;
  expected_image_size( h, w );
  //cv::Mat disparity = cv::Mat( h, w, CV_16SC1 );
  cv::Mat disparity, disparity_16;
  
  // ok, Use a block matching algorithm to find the
  // corospondances between left and right and then compute
  // a dense set of disparity pixels
  //cv::StereoBM BM = cv::StereoBM( cv::StereoBM::BASIC_PRESET, _num_disparity, _sad_window_size );
  //BM.state->minDisparity = _min_disparity;
  //BM( left_image, right_image, disparity, CV_16SC1 );

  cv::StereoSGBM sgbm;
  //sgbm.preFilterCap = 63;
  sgbm.numberOfDisparities = _num_disparity;
  sgbm.minDisparity = _min_disparity;
  sgbm.SADWindowSize = _sad_window_size;
  int cn = 1;
  //sgbm.P1 = 8*cn*sgbm.SADWindowSize*sgbm.SADWindowSize;
  //sgbm.P2 = 32*cn*sgbm.SADWindowSize*sgbm.SADWindowSize;
  //sgbm.uniquenessRatio = 10;
  sgbm.uniquenessRatio = 5;
  //sgbm.speckleWindowSize = 100;
  sgbm.speckleWindowSize = 0;
  //sgbm.speckleRange = 32;
  //sgbm.disp12MaxDiff = 1;
  sgbm.disp12MaxDiff = -1;
  sgbm( left_image, right_image, disparity_16 );

  
  // Return the disparity
  return disparity_16;
}

//=====================================================================

cv::Mat stereo_engine_t::depth_map( const cv::Mat& disparity ) const
{
  // Make sure we are calibrated, and the size is as expected
  if( calibration_loaded() == false )
    BOOST_THROW_EXCEPTION( exceptions::calibration_not_loaded_error() );
  if( is_of_expected_size( disparity ) == false )
    BOOST_THROW_EXCEPTION( exceptions::image_size_error() );

  // Create the resulting depth map
  int h, w;
  expected_image_size( h, w );
  cv::Mat depth = cv::Mat( h, w, CV_32FC3 );
  
  // Now, reproject to get the 3d points
  cv::reprojectImageTo3D( disparity, depth, _disparity_to_depth_Q );
  
  // return the depth
  return depth;
}

//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
