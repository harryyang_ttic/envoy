
## The ptp-stereo library
add_library( ptp-stereo SHARED
  camera_t.hpp
  camera_t.cpp
  stereo_engine_t.hpp
  stereo_engine_t.cpp
  stereo_image_t.hpp
  stereo_image_t.cpp
  stereo_util.hpp
  stereo_util.cpp
  )

pods_use_pkg_config_packages( ptp-stereo ptp-common ptp-common-coordinates ptp-common-interfaces )

pods_install_headers(
  camera_t.hpp
  stereo_engine_t.hpp
  stereo_image_t.hpp
  stereo_util.hpp
  DESTINATION ptp-stereo
  )

pods_install_libraries( ptp-stereo )
pods_install_pkg_config_file( ptp-stereo LIBS -lptp-stereo REQUIRES ptp-common ptp-common-coordinates ptp-common-interfaces )
