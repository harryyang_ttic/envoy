
#include "stereo_util.hpp"
#include <ptp-common-interfaces/opencv_interface.hpp>

namespace ptp {
  namespace stereo {


    using namespace ptp::common;
    using namespace ptp::coordinates;
    using namespace ptp::interfaces;
  

    //==================================================================

    void load_and_break_stereo_image_gray( const std::string& filename ,
					   cv::Mat& left, cv::Mat& right,
					   const bool rotate180 )
    {
      // Load the image
      IplImage *stereo_image = cvLoadImage( filename.c_str() );

      // Convert to grayscale
      cv::Mat stereo_image_mat_color = cv::Mat( stereo_image );
      cv::Mat stereo_image_mat_gray;
      cv::cvtColor(stereo_image_mat_color, stereo_image_mat_gray, CV_RGB2GRAY); 

      // Compute the height of a single image from stereo image
      int h = stereo_image_mat_gray.rows / 2;

      // rotate if wanted
      if( rotate180 ) {
	cv::Mat temp;
	cv::warpAffine( stereo_image_mat_gray, temp, 
			cv::getRotationMatrix2D( cv::Point2f( stereo_image_mat_gray.cols / 2.0f, stereo_image_mat_gray.rows / 2.0f ), 180, 1.0 ), stereo_image_mat_gray.size() );
	stereo_image_mat_gray = temp;
      }
      
      // crop to left/right
      left = stereo_image_mat_gray( cv::Range(0, h), cv::Range::all() );
      right = stereo_image_mat_gray( cv::Range(h, h*2 ), cv::Range::all() );
      
      // release the image
      cvReleaseImage( &stereo_image );
    }

    //==================================================================

  }
}
