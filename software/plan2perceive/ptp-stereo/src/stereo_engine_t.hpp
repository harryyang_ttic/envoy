

#if !defined ( __PTP_STEREO_stereo_engine_HPP__ )
#define __PTP_STEREO_stereo_engine_HPP__

#include <ptp-common/common.hpp>
#include <ptp-common-interfaces/opencv_interface.hpp>
#include <string>

namespace ptp {
  namespace stereo {

    using namespace ptp::common;


    //================================================================

    namespace exceptions {
    
      // Description:
      // Thrown when we try to use a function that requires a 
      // calibratin to have been loaded, but non was/is
      struct calibration_not_loaded_error : public virtual exception_base {};

      // Description:
      // The image size of incorrect / not what expected
      struct image_size_error : public virtual exception_base {};
      
    }

    //================================================================

    // Description:
    // A stereo engine provides the low-level methods needed to
    // use stereo images.
    // In particular, it takes care of computing dense disparity
    // as well as using camera calibration data to compute 3D points.
    class stereo_engine_t
    {
    public:

      // Description:
      // Creates a new, unloaded stereo engine
      stereo_engine_t();

      // Description:
      // Loads a stereo calibration from the given file
      void load( const std::string& filename );

      // Description:
      // Returns the expected image size for a loaded
      // stereo calibraiton ( and for this engine )
      void expected_image_size( int& height, int& width ) const;

      // Description:
      // Rectify the given left and right images
      cv::Mat rectify_left( const cv::Mat& left_image ) const;
      cv::Mat rectify_right( const cv::Mat& right_image ) const;
      
      
      // Description:
      // Generates a dense disparity map between a left and right image.
      // The left and right image should be RECTIFIED
      // The returned disparity if a floating point matrix.
      cv::Mat disparity_map_from_rectified(const cv::Mat& left_image, 
					     const cv::Mat& right_image) const;
      
      
      // Description:
      // Generate a dense depth map from a disparity map.
      // The returned matrix will have m( y, x ) = z, where z is 
      // the 3D coordinate z, and the triple (x,y,z) is the 3D point.
      // These points are in the frame of the CAMERAS
      cv::Mat depth_map( const cv::Mat& disparity ) const;


      // Description:
      // Gets the disparity-to-depth mapping matrix from the 
      // calibration loaded in this engine
      cv::Mat disparity_to_depth_matrix() const
      { return _disparity_to_depth_Q; }

      // Description:
      // Set the Block Matching parameters
      void set_block_matching_param( const int& num_disparity,
				     const int& min_disparity,
				     const int& sad_window_size );

    protected:

      // Description:
      // The stereo calibration filename
      std::string _calibration_filename;

      // Description:
      // These are the undistort matrices for left and right camera
      // images, loaded from the stereo calibration file
      cv::Mat _mx1, _my1;
      cv::Mat _mx2, _my2;

      // Description:
      // The Q matrix mapping disparity to depth, read from
      // the stereo calibration file
      cv::Mat _disparity_to_depth_Q;

      // Description:
      // The parameters for the block matching stereo algorithm
      int _num_disparity;
      int _min_disparity;
      int _sad_window_size;

      // Description:
      // Returns true iff a calibration has been loaded
      bool calibration_loaded() const;
      
      // Description:
      // Returns true iff the given matrix is of the expected size
      bool is_of_expected_size( const cv::Mat& a ) const;

      // Description:
      // Create camera objects from calibration data
      void create_cameras();
      
    };

    // Description:
    // Smart pointer for stereo engines
    typedef boost::shared_ptr<stereo_engine_t> stereo_engine_tp;

    //================================================================

  }
}

#endif

