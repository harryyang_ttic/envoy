
#include "stereo_image_t.hpp"

using namespace ptp;
using namespace ptp::stereo;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::interfaces;

//=====================================================================

stereo_image_t::stereo_image_t( const cv::Mat& left,
				const cv::Mat& right,
				const stereo_engine_tp& engine,
				const manifold_tp& manifold_with_camera_down_z )
{
  init( engine, manifold_with_camera_down_z, left, right );
}

//=====================================================================

matrix_tp stereo_image_t::left_image() const
{
  if( !_left_image_matrix )
    create_and_store_matrix_images();
  return _left_image_matrix;
}

//=====================================================================

matrix_tp stereo_image_t::right_image() const
{
  if( !_right_image_matrix )
    create_and_store_matrix_images();
  return _right_image_matrix;
}

//=====================================================================

cv::Mat stereo_image_t::cv_left_image() const
{
  return _cv_left_image;
}

//=====================================================================

cv::Mat stereo_image_t::cv_right_image() const
{
  return _cv_right_image;
}

//=====================================================================

coordinate_t stereo_image_t::coordinate_for_pixel( const int& x, const int& y ) const
{
  // // We just have to look up the pixel in our depth image
  // cv::Vec3f point = _cv_depth.at<cv::Vec3f>( y, x );
  // std::cout << "[" << x << ", " << y << "]: " << point.val[0] << " , " << point.val[1] << " , " << point.val[2] << std::endl;
  // // take care of having too large float values be infinite
  // if( point.val[2] >= 10000 )
  //   point.val[2] = std::numeric_limits<float>::infinity();
  // coordinate_t coord = coordinate( point.val[0], point.val[1], point.val[2] ).on( left_camera()->manifold() );

  // // Actually, we're going to direcly use the disparity-to-depth matrix
  // // from the engine
  // float disparity = _cv_disparity.at<float>( y, x );
  // // if the disparity is a signed bit matrix, then divide by 16
  // //disparity /= 16;
  // math_vector_t ic( 4 );
  // ic(0) = x;
  // ic(1) = y;
  // ic(2) = disparity;
  // ic(3) = 1;
  // matrix_tp disparity_to_depth_q = opencv::to_matrix( _engine->disparity_to_depth_matrix() );
  // math_vector_t c = ublas::prod( *disparity_to_depth_q, ic );
  // c /= c(3);

  // the magical depth calculateion from the one calibration tha worked
  double focal = 7.7606738820049850e+02;
  double magic_num = -8.4621079322982737e+0;
  float _Qvals[4][4] = {{1., 0., 0., -268.20271301269531 * 2},{0., 1., 0., -194.47385215759277 * 2},{0., 0., 0., focal},{0., 0., magic_num, -20.1916 / 4}};
  float disparity = _cv_disparity.at<int16_t>( y, x );
  float xc = x * _Qvals[0][0] + _Qvals[0][3];
  float yc = y * _Qvals[1][1] + _Qvals[1][3];
  float zc = _Qvals[2][3];
  //float w = ((float)(-disparity)/16.0) * _Qvals[3][2] + _Qvals[3][3];
  float w = ((float)(disparity)) * _Qvals[3][2] + _Qvals[3][3];
  coordinate_t coord = coordinate( xc/w , yc/w, zc/w ).on( left_camera()->manifold() );
  if( coord[2] > 0 ) {
    coord[0] *= -1;
    coord[1] *= -1;
  } else {
    coord[0] *= -1;
    coord[1] *= -1;
    coord[2] *= -1;
  }
  
  return coord;
}

//=====================================================================

mapped_point_cloud_tp 
stereo_image_t::mapped_point_cloud_for_rectangle( const int& start_x,
						  const int& start_y,
						  const int& end_x,
						  const int& end_y ) const
{
  mapped_point_cloud_tp p( new mapped_point_cloud_t() );
  for( int x = start_x; x < end_x; ++x ) {
    for( int y = start_y; y < end_y; ++y ) {
      p->add( coordinate( x, y ).on( _left_camera->camera_plane_manifold() ),
	      coordinate_for_pixel( x, y ) );
    }
  }
  return p;
}

//=====================================================================

void stereo_image_t::create_and_store_matrix_images() const
{
  _left_image_matrix = opencv::to_matrix( _cv_left_image );
  _right_image_matrix = opencv::to_matrix( _cv_right_image );
}

//=====================================================================

void stereo_image_t::init( const stereo_engine_tp& engine,
			   const manifold_tp& manifold_with_camera_down_z,
			   const cv::Mat& left, const cv::Mat& right )
{
  _engine = engine;
  _cv_left_image = left;
  _cv_right_image = right;
  
  // Ok, grab the disparity and depth images from engine
  _left_rec = _engine->rectify_left( _cv_left_image );
  _right_rec = _engine->rectify_right( _cv_right_image );
  _cv_disparity = _engine->disparity_map_from_rectified( _left_rec, _right_rec );
  _cv_depth = _engine->depth_map( _cv_disparity );
  
  
  // Create a camera object based on hte given manifold
  // and the intrinsic parameters
  // TODO: WRITE THIS OUT
  coordinate_t origin = coordinate( 0,0,0 ).on( manifold_with_camera_down_z );
  plane_tp plane( new plane_t( line_simplex( coordinate( 0,0,1 ),
					     coordinate( 0,0,2 ),
					     manifold_with_camera_down_z ),
			       line_simplex( coordinate( 0,0,1 ),
					     coordinate( 1,0,1 ),
					     manifold_with_camera_down_z )));
  matrix_t params = matrix_t::Identity(3,3);
  _left_camera = camera_tp( new camera_t( plane, origin, manifold_with_camera_down_z, params ) );
}

//=====================================================================

void stereo_image_t::write_debug_info( const std::string& dir ) const
{
  // Ok, write out the left, right, disparity, and rectified images
  opencv::save_image( dir + "/left_image.jpg", _cv_left_image );
  opencv::save_image( dir + "/right_image.jpg", _cv_right_image );
  opencv::save_image( dir + "/left_rec_image.jpg", _left_rec );
  opencv::save_image( dir + "/right_rec_image.jpg", _right_rec );
  opencv::save_image( dir + "/disparity_image.jpg", _cv_disparity );
  
  // 
}

//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================
//=====================================================================

