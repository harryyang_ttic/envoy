
#include "camera_t.hpp"
#include <ptp-common-coordinates/coordinate_math.hpp>

using namespace ptp;
using namespace ptp::stereo;
using namespace ptp::common;
using namespace ptp::coordinates;

//==================================================================

camera_t::camera_t( const plane_tp& plane_manifold_on_camera_manifold,
		    const coordinate_t& origin_on_camera_manifold,
		    const manifold_tp& camera_manifold,
		    const matrix_t& intrinsic_params )
  : _camera_plane_manifold( plane_manifold_on_camera_manifold ),
    _origin_on_camera_manifold( origin_on_camera_manifold ),
    _manifold( camera_manifold ),
    _intrinsic_parameters( intrinsic_params )
{
}

//==================================================================

basis_t camera_t::project_out_ray( const coordinate_t& point_in_plane ) const
{
  // Make sure the point is on the camera  plane
  if( point_in_plane.manifold() != camera_plane_manifold() )
    BOOST_THROW_EXCEPTION( coordinates::exceptions::coordinate_not_in_manifold_error() );

  // Calculate the basis from the camera origin ( pinhole )
  basis_t from_pinhole = basis_t( _origin_on_camera_manifold,
				  point_in_plane.on( manifold() ) );
  
  // Now just center the basis on the pixel point
  basis_t ray = from_pinhole.center_on( point_in_plane.on( manifold() ) );
  
  return normalize_length( ray, 1.0 );
}

//==================================================================

coordinate_t camera_t::project_into_plane( const coordinate_t& point_on_manifold ) const
{
  // Check that the point is on the camera manifold
  if( point_on_manifold.manifold() != manifold() )
    BOOST_THROW_EXCEPTION( coordinates::exceptions::coordinate_not_in_manifold_error() );

  // Algorithm from wikipedia ( the algebraic way :-P )
  // http://en.wikipedia.org/wiki/Line-plane_intersection
  
  // Get the ray direction
  coordinate_t line_normal = point_on_manifold - _origin_on_camera_manifold;
  basis_t line_ray = basis_t( _origin_on_camera_manifold,
			      _origin_on_camera_manifold + line_normal );

  // Get the plane's normal as a basis
  basis_t plane_normal = basis_from_simplex( camera_plane_manifold()->normal_simplex() );
  coordinate_t plane_origin = camera_plane_manifold()->origin_in_reference();

  // center the normal on the line origin
  plane_normal = plane_normal.center_on( _origin_on_camera_manifold );
  
  // Check if the line if parallel to the plane
  float denom = dot( line_ray, plane_normal );
  if( denom == 0 ) {
    BOOST_THROW_EXCEPTION( coordinates::exceptions::coordinate_not_in_manifold_error() );
  }
  
  // Ok, compute the distance along the line where it intersects the plane
  basis_t orig_diff = basis_t( _origin_on_camera_manifold,
			       plane_origin - _origin_on_camera_manifold );
  float numerator = dot( orig_diff, plane_normal );
  float d = numerator / denom;
  
  // Ok, return the point starting at the line origin and going
  // d distance down the line
  return _origin_on_camera_manifold + line_normal * d;
}

//==================================================================

bool camera_t::operator== ( const camera_t& a ) const
{
  return _camera_plane_manifold == a._camera_plane_manifold &&
    _origin_on_camera_manifold == a._origin_on_camera_manifold &&
    _manifold == a._manifold;
  
  // TODO: test for  the equality of intrinsic params.
  //       _intrinsic_parameters == a._intrinsic_parameters;
}

//==================================================================

namespace ptp {
  namespace stereo {

    std::ostream& operator<< ( std::ostream& os, const camera_t& a )
    {
      os << "Cam::plane=" << (*a._camera_plane_manifold) << "/o=" << a._origin_on_camera_manifold << "/man=" << (*a._manifold) << "/param=" << a._intrinsic_parameters;
      return os;
    }

  }
}

//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
//==================================================================
