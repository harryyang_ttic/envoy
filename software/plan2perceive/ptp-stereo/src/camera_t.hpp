
#if !defined( __PTP_STEREO_camera_t_HPP__ )
#define __PTP_STEREO_camera_t_HPP__


#include <ptp-common-coordinates/manifold.hpp>
#include <ptp-common-coordinates/plane_t.hpp>
#include <ptp-common/matrix.hpp>

namespace ptp {
  namespace stereo {

    using namespace ptp::common;
    using namespace ptp::coordinates;

    //===============================================================

    // Description:
    // A camera_t represents a particular pin-hole-projection model
    // for a camera. It includes a manifold in which the camera
    // lives, which has origin at the pinhole and the z axis
    // pointing towards the camera's viewing direction.
    class camera_t
    {
    public:

      // Description:
      // Creates a new camera with the given camera plane manifold,
      // A given camera manifold,
      // and whoose intrinsic parameters are given.
      // The camera is looking down the z axis of the plane, with the
      // origin at the given point ( in the camera manifold )
      // Hence the view direction is from the origin to the 
      // (0,0).on(plane).on(camera manifold) point of the camera plane
      // on the camera manifold.
      camera_t( const plane_tp& plane_manifold_on_camera_manifold,
		const coordinate_t& origin_on_camera_manifold,
		const manifold_tp& camera_manifold, 
		const matrix_t& intrinsic_params );
      

      
      // Description:
      // The manifold for the camera.
      // This is by definition the reference of the camera plane manifold.
      manifold_tp manifold() const
      { return _manifold; }

      // Description:
      // The manifold for the camera plane
      plane_tp camera_plane_manifold() const
      { return _camera_plane_manifold; }
      
      // Description:
      // The origina and direction of the camera
      // This is in the camera's manifold
      basis_t look_at_basis() const
      { 
	return basis_t( _origin_on_camera_manifold,
			coordinate( 0,0 ).on( camera_plane_manifold() ).on( this->manifold() ) );
      }
      
      // Description:
      // The camera intrinsic parameter matrix
      matrix_t intrinsic_parameters() const
      { return _intrinsic_parameters; }

      // Description:
      // Go from the camera plane to a ray in the camera's manifold
      // The returns basis is on unit length ( since distance is 
      // arbritary for a single pinhole camera ).
      basis_t project_out_ray( const coordinate_t& point_in_plane ) const;
      
      // Description:
      // Take a point in the camera's manifold and project it onto
      // the camera plane
      coordinate_t project_into_plane( const coordinate_t& point_in_manifold ) const;
      

      // Description:
      // Test equality of cameras
      bool operator!= ( const camera_t& a ) const
      { return !( *this == a ); }
      bool operator== ( const camera_t& a ) const;

      // Description:
      // IO operators
      friend std::ostream& operator<< ( std::ostream& os, const camera_t& a );
     

    protected:
      
      
      // Description:
      // The camera plane's manifold
      plane_tp _camera_plane_manifold;

      // Description:
      // The origin of the camera ( the pinhole ) in the camera manifold
      coordinate_t _origin_on_camera_manifold;

      // Description:
      // The camera manifold
      manifold_tp _manifold;
      
      // Description:
      // The intrinsic parameters
      matrix_t _intrinsic_parameters;
      
      
    };

    // Description:
    // Smart pointer for camera
    typedef boost::shared_ptr<const camera_t> camera_tp;
    
    //===============================================================

  }
}

#endif

