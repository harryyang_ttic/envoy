#include "stereovision.h"

StereoVision::StereoVision(int imageWidth,int imageHeight)
{
    this->imageSize = cvSize(imageWidth,imageHeight);
    mx1 = my1 = mx2 = my2 = 0;
    calibrationStarted = false;
    calibrationDone = false;
    imagesRectified[0] = imagesRectified[1] = imageDepth = imageDepthNormalized = 0;
    imageDepth = 0;
    sampleCount = 0;
}

StereoVision::~StereoVision()
{
    cvReleaseMat(&imagesRectified[0]);
    cvReleaseMat(&imagesRectified[1]);
    cvReleaseMat(&imageDepth);
    cvReleaseMat(&imageDepthNormalized);
}

void StereoVision::calibrationStart(int cornersX,int cornersY){
    this->cornersX = cornersX;
    this->cornersY = cornersY;
    this->cornersN = cornersX*cornersY;
    ponintsTemp[0].resize(cornersN);
    ponintsTemp[1].resize(cornersN);
    sampleCount = 0;
    calibrationStarted = true;
}


int StereoVision::calibrationAddSample(IplImage* imageLeft,IplImage* imageRight){

    if(!calibrationStarted) return RESULT_FAIL;

    IplImage* image[2] = {imageLeft,imageRight};

    int succeses = 0;

    for(int lr=0;lr<2;lr++){
        CvSize imageSize =  cvGetSize(image[lr]);

        if(imageSize.width != this->imageSize.width || imageSize.height != this->imageSize.height)
            return RESULT_FAIL;

        int cornersDetected = 0;

        //FIND CHESSBOARDS AND CORNERS THEREIN:
        int result = cvFindChessboardCorners(
            image[lr], cvSize(cornersX, cornersY),
            &ponintsTemp[lr][0], &cornersDetected,
            CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_NORMALIZE_IMAGE
        );

        if(result && cornersDetected == cornersN){


            //Calibration will suffer without subpixel interpolation
            //cvFindCornerSubPix(
            //    image[lr], &ponintsTemp[lr][0], cornersDetected,
            //    cvSize(11, 11), cvSize(-1,-1),
            //    cvTermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS,30, 0.01)
            //);
            succeses++;
        }

    }
    if(2==succeses){
        for(int lr=0;lr<2;lr++){
            points[lr].resize((sampleCount+1)*cornersN);
            copy( ponintsTemp[lr].begin(), ponintsTemp[lr].end(),  points[lr].begin() + sampleCount*cornersN);
        }
        sampleCount++;
        return RESULT_OK;
    }else{
        return RESULT_FAIL;
    }
}

int StereoVision::calibrationEnd(){
    calibrationStarted = false;

    // release any old matrices
    // cvReleaseMat( &mx1 );
    // cvReleaseMat( &my1 );
    // cvReleaseMat( &mx2 );
    // cvReleaseMat( &my2 );
    // cvReleaseMat( &_M1 );
    // cvReleaseMat( &_M2 );
    // cvReleaseMat( &_D1 );
    // cvReleaseMat( &_D2 );
    // cvReleaseMat( &_R  );
    // cvReleaseMat( &_T  );
    // cvReleaseMat( &_E  );
    // cvReleaseMat( &_F  );
    // cvReleaseMat( &_Q  );
    // cvReleaseMat( &_R1 );
    // cvReleaseMat( &_R2 );
    // cvReleaseMat( &_P1 );
    // cvReleaseMat( &_P2 );

    // Create new matrices to fill
    mx1 = cvCreateMat( imageSize.height,imageSize.width, CV_64F );
    my1 = cvCreateMat( imageSize.height,imageSize.width, CV_64F );
    mx2 = cvCreateMat( imageSize.height,imageSize.width, CV_64F );
    my2 = cvCreateMat( imageSize.height,imageSize.width, CV_64F );
    _M1 = cvCreateMat( 3, 3, CV_64F );
    _M2 = cvCreateMat( 3, 3, CV_64F );
    _D1 = cvCreateMat( 1, 5, CV_64F );
    _D2 = cvCreateMat( 1, 5, CV_64F );
    _R  = cvCreateMat( 3, 3, CV_64F );
    _T  = cvCreateMat( 3, 1, CV_64F );
    _E  = cvCreateMat( 3, 3, CV_64F );
    _F  = cvCreateMat( 3, 3, CV_64F ); 
    _Q  = cvCreateMat( 4, 4, CV_64F );
    _R1 = cvCreateMat( 3, 3, CV_64F );
    _R2 = cvCreateMat( 3, 3, CV_64F );
    _P1 = cvCreateMat( 3, 4, CV_64F );
    _P2 = cvCreateMat( 3, 4, CV_64F );



    // HARVEST CHESSBOARD 3D OBJECT POINT LIST:
    objectPoints.resize(sampleCount*cornersN);

    for(int k=0;k<sampleCount;k++)
        for(int i = 0; i < cornersY; i++ )
            for(int j = 0; j < cornersX; j++ )
                objectPoints[k*cornersY*cornersX + i*cornersX + j] = cvPoint3D32f(i, j, 0);


    npoints.resize(sampleCount,cornersN);

    int N = sampleCount * cornersN;


    CvMat _objectPoints = cvMat(1, N, CV_32FC3, &objectPoints[0] );
    CvMat _imagePoints1 = cvMat(1, N, CV_32FC2, &points[0][0] );
    CvMat _imagePoints2 = cvMat(1, N, CV_32FC2, &points[1][0] );
    CvMat _npoints = cvMat(1, npoints.size(), CV_32S, &npoints[0] );
    cvSetIdentity(_M1);
    cvSetIdentity(_M2);
    cvZero(_D1);
    cvZero(_D2);

    //CALIBRATE THE STEREO CAMERAS
    cvStereoCalibrate( &_objectPoints, &_imagePoints1,
        &_imagePoints2, &_npoints,
        _M1, _D1, _M2, _D2,
        imageSize, _R, _T, _E, _F,
        cvTermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 100, 1e-5),
        CV_CALIB_FIX_ASPECT_RATIO + CV_CALIB_ZERO_TANGENT_DIST + CV_CALIB_SAME_FOCAL_LENGTH
    );

    // RECTIFY STEREO
    cvStereoRectify( _M1, _M2, _D1, _D2, imageSize, _R, _T,
		     _R1, _R2, _P1, _P2, _Q );
    

    //Precompute map for cvRemap()
    cvInitUndistortRectifyMap(_M1,_D1,_R1,_P1,mx1,my1);
    cvInitUndistortRectifyMap(_M2,_D2,_R2,_P2,mx2,my2);

    calibrationDone = true;

    return RESULT_OK;
}

int StereoVision::stereoProcess(CvArr* imageSrcLeft,CvArr* imageSrcRight){
    if(!calibrationDone) return RESULT_FAIL;

    if(!imagesRectified[0]) imagesRectified[0] = cvCreateMat( imageSize.height,imageSize.width, CV_8U );
    if(!imagesRectified[1]) imagesRectified[1] = cvCreateMat( imageSize.height,imageSize.width, CV_8U );
    if(!imageDepth) imageDepth = cvCreateMat( imageSize.height,imageSize.width, CV_16S );
    if(!imageDepthNormalized) imageDepthNormalized = cvCreateMat( imageSize.height,imageSize.width, CV_8U );

    //rectify images
    cvRemap( imageSrcLeft, imagesRectified[0] , mx1, my1 );
    cvRemap( imageSrcRight, imagesRectified[1] , mx2, my2 );


    CvStereoBMState *BMState = cvCreateStereoBMState();
    BMState->preFilterSize=41;
    BMState->preFilterCap=31;
    BMState->SADWindowSize=41;
    BMState->minDisparity=-64;
    BMState->numberOfDisparities=128;
    BMState->textureThreshold=10;
    BMState->uniquenessRatio=15;

    cvFindStereoCorrespondenceBM( imagesRectified[0], imagesRectified[1], imageDepth, BMState);
    cvNormalize( imageDepth, imageDepthNormalized, 0, 256, CV_MINMAX );

    cvReleaseStereoBMState(&BMState);

    return RESULT_OK;
}

int StereoVision::calibrationSave(const char* filename){
  if(!calibrationDone) return RESULT_FAIL;
  
  CvFileStorage* fs = cvOpenFileStorage( filename, 0, CV_STORAGE_WRITE );
  cvWrite( fs, "mx1", mx1, cvAttrList(0,0) );
  cvWrite( fs, "my1", my1, cvAttrList(0,0) );
  cvWrite( fs, "mx2", mx2, cvAttrList(0,0) );
  cvWrite( fs, "my2", my2, cvAttrList(0,0) );
  cvWrite( fs, "_m1", _M1, cvAttrList(0,0) );
  cvWrite( fs, "_m2", _M2, cvAttrList(0,0) );
  cvWrite( fs, "_d1", _D1, cvAttrList(0,0) );
  cvWrite( fs, "_d2", _D2, cvAttrList(0,0) );
  cvWrite( fs, "_r", _R, cvAttrList(0,0) );
  cvWrite( fs, "_t", _T, cvAttrList(0,0) );
  cvWrite( fs, "_e", _E, cvAttrList(0,0) );
  cvWrite( fs, "_f", _F, cvAttrList(0,0) );
  cvWrite( fs, "_q", _Q, cvAttrList(0,0) );
  cvWrite( fs, "_r1", _R1, cvAttrList(0,0) );
  cvWrite( fs, "_r2", _R2, cvAttrList(0,0) );
  cvWrite( fs, "_p1", _P1, cvAttrList(0,0) );
  cvWrite( fs, "_p2", _P2, cvAttrList(0,0) );

  cvReleaseFileStorage( &fs );

  return RESULT_OK;
}


int StereoVision::calibrationLoad(const char* filename){
    // cvReleaseMat(&mx1);
    // cvReleaseMat(&my1);
    // cvReleaseMat(&mx2);
    // cvReleaseMat(&my2);
    // cvReleaseMat( &_M1 );
    // cvReleaseMat( &_M2 );
    // cvReleaseMat( &_D1 );
    // cvReleaseMat( &_D2 );
    // cvReleaseMat( &_R );
    // cvReleaseMat( &_T );
    // cvReleaseMat( &_E );
    // cvReleaseMat( &_F );
    // cvReleaseMat( &_Q );
    // cvReleaseMat( &_R1 );
    // cvReleaseMat( &_R2 );
    // cvReleaseMat( &_P1 );
    // cvReleaseMat( &_P2 );

    CvFileStorage* fs = cvOpenFileStorage( filename, 0, CV_STORAGE_READ );
    mx1 = (CvMat*)cvReadByName( fs, NULL, "mx1" );
    my1 = (CvMat*)cvReadByName( fs, NULL, "my1" );
    mx2 = (CvMat*)cvReadByName( fs, NULL, "mx2" );
    my2 = (CvMat*)cvReadByName( fs, NULL, "my2" );
    _M1 = (CvMat*)cvReadByName( fs, NULL, "_m1" );
    _M2 = (CvMat*)cvReadByName( fs, NULL, "_m2" );
    _D1 = (CvMat*)cvReadByName( fs, NULL, "_d1" );
    _D2 = (CvMat*)cvReadByName( fs, NULL, "_d2" );
    _R  = (CvMat*)cvReadByName( fs, NULL, "_r" );
    _T  = (CvMat*)cvReadByName( fs, NULL, "_t" );
    _E  = (CvMat*)cvReadByName( fs, NULL, "_e" );
    _F  = (CvMat*)cvReadByName( fs, NULL, "_f" );
    _Q  = (CvMat*)cvReadByName( fs, NULL, "_q" );
    _R1 = (CvMat*)cvReadByName( fs, NULL, "_r1" );
    _R2 = (CvMat*)cvReadByName( fs, NULL, "_r2" );
    _P1 = (CvMat*)cvReadByName( fs, NULL, "_p1" );
    _P2 = (CvMat*)cvReadByName( fs, NULL, "_p2" );

    cvReleaseFileStorage( &fs );


    calibrationDone = true;
    return RESULT_OK;
}

