
#if !defined ( __PTP_STEREO_stereo_image_t_HPP__ )
#define __PTP_STEREO_stereo_image_t_HPP__

#include "stereo_engine_t.hpp"
#include <ptp-common/matrix.hpp>
#include <ptp-common-coordinates/point_cloud_t.hpp>
#include "camera_t.hpp"


namespace ptp {
  namespace stereo {

    using namespace ptp::common;
    using namespace ptp::coordinates;

    //===============================================================

    // Description:
    // This is an object interface to a stereo image pair.
    // Each image has a stereo_engine associated with it that is 
    // use to do the rectification and depth computation.
    // Rather than construct stereo images yourself, there are
    // factory functions for constructing them out of common
    // objects ( such as LCM messages, files, etc.. )
    class stereo_image_t
    {
    public:

      // Description:
      // Create a stereo image from
      // A left and right image, an engine, and a camera
      // manifold which has the camera down the z axis
      stereo_image_t( const cv::Mat& left, const cv::Mat& right,
		      const stereo_engine_tp& engine,
		      const manifold_tp& manifold_with_camera_down_z );
      
      // Description:
      // Returns the images as a matrix
      matrix_tp left_image() const;
      matrix_tp right_image() const;

      // Description:
      // Returns the images as cv::Mat
      cv::Mat cv_left_image() const;
      cv::Mat cv_right_image() const;
      
      // Description:
      // Returns the cameras
      camera_tp left_camera() const
      { return _left_camera; }
      //camera_tp right_camera() const;
      
      // Description:
      // Returns the stereo engine for this image
      stereo_engine_tp stereo_engine() const
      { return _engine; }
      
      // Description:
      // Returns the width and height
      int width() const
      { return _cv_left_image.cols; }
      int height() const
      { return _cv_left_image.rows; }
      
      // Description:
      // Returns the coordinate for a particular pixel.
      // This is the 3D coordinate as projected from the image plane
      // to the world in front of the camera
      coordinate_t coordinate_for_pixel( const int& i, const int& j ) const;
      
      // Description:
      // Returns a mapped point cloud for the given rectangle of pixels
      mapped_point_cloud_tp 
      mapped_point_cloud_for_rectangle( const int& start_i, 
					const int& start_j,
					const int& end_i, 
					const int& end_j ) const;
      

      // Description:
      // Returns the disparity image
      cv::Mat disparity() const
      { return _cv_disparity; }

      // Description:
      // Writes out a bunch of debug information to the given directory
      // This includes images and stuff
      void write_debug_info( const std::string& dir_path ) const;

    protected:
      
      // Description:
      // The engine
      stereo_engine_tp _engine;
      
      // Description:
      // The left and righ images as OpenCV matrices
      cv::Mat _cv_left_image;
      cv::Mat _cv_right_image;
      
      // Description:
      // The Disparity and depth images as OpenCV
      cv::Mat _cv_disparity;
      cv::Mat _cv_depth;
      
      // Description:
      // The left camera
      camera_tp _left_camera;
      
      // Description:
      // The images and matrix_t.
      // These are lazily computed in needed and are copies of the Cv
      mutable matrix_tp _left_image_matrix;
      mutable matrix_tp _right_image_matrix;

      // Description:
      // The rectified left and right images.
      // Here for debug purposes
      cv::Mat _left_rec;
      cv::Mat _right_rec;

      // Description:
      // Create an store matrix representations of images
      void create_and_store_matrix_images() const;
      
      // Description:
      // Init this image with a given engine and
      // pair of cv images as well as the manifold which
      // points the camera down the z axis at teh origin.
      // This will now OWN the cvMat* given
      void init( const stereo_engine_tp& engine,
		 const manifold_tp& manifold_with_camera_down_z,
		 const cv::Mat& left, const cv::Mat& right );
      
    };
    
    //===============================================================
    //===============================================================
    //===============================================================
    //===============================================================
    
  }
}

#endif

