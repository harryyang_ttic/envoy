

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>

// Include the object candidate message
#include <carmen3d/carmen3d_lcmtypes.h>
#include <carmen3d/lcm_utils.h>
#include <lcmtypes/bot_core_image_t.h>

#include <bot_core/ppm.h>

// Include all common ptp code
#include <ptp_common/ptp_common.h>

// For image IO
#include <cv.h>
#include <highgui.h>

#define MAX_TIMESTAMP_RANGE 100

// We'll be using std and ptp a lot, so have using clauses
using namespace std;


//==========================================================================

// Descripption:
// The input and output directories
string disparity_directory = "disparity_images/";

//==========================================================================

// Descripption:
// The set of wanted images to extract
vector<size_t> wanted_timestamp_ids;

// Description:
// The set of extracted disparity images
vector<size_t> extracted_disparity_timestamp_ids;

//==========================================================================

// Description:
// Given a master log file stream
// Returns the set of timestamp ids in the stream
vector<size_t> parse_timestamp_ids_from_parsable_log( istream& is ) 
{

  vector<size_t> results;

  // Ok, read the stream line by line
  string line;
  while( getline(is,line) ) {
    
    // Ok, we want the 8th token
    string t0, t1, t2, t3, t4, t5, t6;
    size_t t7;
    istringstream iss( line );
    iss >> t0 >> t1 >> t2 >> t3 >> t4 >> t5 >> t6 >> t7;
    
    results.push_back( t7 );
  }

  return results;
}

//==========================================================================

// Description:
// Returns true iff the given timestamp is within range of
// any timestamp in the vector
bool timestamp_id_within_range( const size_t& x, 
				const size_t& range, 
				const vector<size_t>& data )
{
  for( size_t i = 0; i < data.size(); ++i ) {
    if( abs( (long)data[i] - (long)x ) < range ) {
      return true;
    }
  }
  return false;
}


//==========================================================================

// Description:
// Converts from a 16-bit grayscale image to an 8bit 
uint8_t* convert_16bit_image_to_8bit_data
( const bot_core_image_t* msg )
{
  uint8_t *data = new uint8_t[ msg->width * msg->height ];
  for( size_t h = 0; h < msg->height; ++h ) {
    for( size_t w = 0; w < msg->width; ++w ) {
      
      // This equaly subdivides the sapce into a byte
      //data[ h * msg->width + w ] = (uint8_t)(((uint16_t)msg->data[ h * msg->row_stride + w * 2 ]) / 256);
      
      // Just take the second bits
      //data[ h * msg->width + w ] = msg->data[ h * msg->row_stride + w * 2 + 1 ];

      // Just take the first bits of data
      data[ h * msg->width + w ] = msg->data[ h * msg->row_stride + w * 2];

      // saturate if we need to
      if( msg->data[ h * msg->row_stride + w * 2 + 1 ] > 0 ) {
	data[ h * msg->width + w ] = 255;
      }
    }
  }
  
  return data;
}

//==========================================================================


// Description:
// This disparity image message contains an image we would like to
// save
void save_disparity_image( const bot_core_image_t * msg )
{
  // Ok create the filename for this image
  ostringstream oss;
  oss << disparity_directory << "disparity_image_" << msg->utime << ".tif";

  // debug
  //cout << "Image Width: " << msg->width << ", Height: " << msg->height << " RowStride: " << msg->row_stride << " Size: " << msg->size << " Format: " << msg->pixelformat << endl;
  
  // write out a pgm image with the date from the message
  //FILE* fp = fopen( oss.str().c_str(), "w" );
  ////bot_ppm_write( fp, msg->data, msg->width, msg->height, msg->row_stride);
  //uint8_t* data8 = convert_16bit_image_to_8bit_data( msg );
  //bot_pgm_write( fp, data8, msg->width, msg->height, msg->width);
  //delete[] data8;
  //fclose( fp );

  // convert data to cvMat and write it
  cv::Mat mat = cv::Mat( msg->height, msg->width, CV_16SC1, msg->data );
  mat /= 16;
  mat = cv::abs( mat );
  double minv, maxv;
  cv::minMaxLoc( mat, &minv, &maxv );
  cout << "Min: " << minv << " Max: " << maxv << endl;
  cv::imwrite( oss.str(), mat);
  // add to extracted set
  extracted_disparity_timestamp_ids.push_back( msg->utime );

  cout << "Saved disparity " << msg->utime << " [" << oss.str() << "]" << endl;
}

//==========================================================================

// Description:
// Handles a disparity image
// We simply check if it is one of our wanted timestamps
// ( or within a region ) and if so call
// save_disparity_image on it
static void disparity_image_handler
( const lcm_recv_buf_t *rbuf __attribute__((unused)), 
  const char * channel __attribute__((unused)),
  const bot_core_image_t * msg, 
  void * user) 
{  
  size_t timestamp = msg->utime;
  
  if( timestamp_id_within_range( timestamp, MAX_TIMESTAMP_RANGE, wanted_timestamp_ids ) ) {
    save_disparity_image( msg );
  }

}


//==========================================================================

// Description:
// Setup the lcm handlers
void setup_handlers( lcm_t* lcm )
{
  bot_core_image_t_subscribe( lcm, "DISPARITY_IMAGE", disparity_image_handler, NULL );
}

//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================
//==========================================================================

// The main function, which just sets up the handlers and 
// runs them
int main( int argc, char** argv )
{

  if( argc < 4 ) {
    cout << "USAGE: " << argv[0] << " lcmlog-file parsable-log output-directory" << endl;
    exit( -1 );
  }

  // get LCM singetone
  lcm_t* lcm;
  std::ostringstream oss;
  oss << "file://" << argv[1]; // << "?speed=1.0"; 
  cout << "Reading LOG file: " << oss.str() << endl << flush;
  lcm = lcm_create( oss.str().c_str() );
  
  // read in hte disparity directory and lcmlog file fom command line
  string parsable_log_filename = argv[2];
  disparity_directory = argv[3];
  disparity_directory += '/'; // terminating path separator

  // ok, load the wanted timetamps from parsable log
  ifstream fin( parsable_log_filename.c_str() );
  wanted_timestamp_ids = parse_timestamp_ids_from_parsable_log( fin );
  cout << "Looking for " << wanted_timestamp_ids.size() << " disparity images..." << endl;
    
  // setup handlers
  setup_handlers( lcm );

  // dispatch
  while( !lcm_handle( lcm ) ) {
    ; 
  }

  return 0;

}


//==========================================================================
