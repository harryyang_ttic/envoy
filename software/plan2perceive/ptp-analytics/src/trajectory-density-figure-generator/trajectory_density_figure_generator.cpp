
#include <ptp-common/matrix.hpp>
#include <lcm/lcm.h>
#include <lcmtypes/bot_core_pose_t.h>
#include <boost/filesystem.hpp>
#include <bot_core/small_linalg.h>
#include <iostream>
#include <fstream>

using namespace boost::filesystem;
using namespace ptp::common;


// Description:
// The global range for our figure in meters
double _min_x;
double _max_x;
double _min_y;
double _max_y;
double _num_x;
double _num_y;

// Description:
// Our figure, as a matrix, with rows = y
matrix_t _data;



// Description:
// A bin striucture, whic is just a discreete set of x,y / row,col
struct bin_t
{
  int x;
  int y;
};


// Description:
// Take xyz array and convert into bin for data
bin_t compute_bin( const double loc[3] )
{
  bin_t b;
  double step_x = ( _max_x - _min_x ) / _num_x;
  double step_y = ( _max_y - _min_y ) / _num_y;
  b.x = (loc[0] - _min_x) / step_x;
  b.y = (loc[1] - _min_y) / step_y;
  return b;
}


// Description:
// Updates the data grid for given bin by given amount
void update_data( const bin_t& bin, const float& val )
{
  // only update things inside the data grid
  if( bin.x < 0 || bin.y < 0 ||
      bin.x >= _data.cols() ||
      bin.y >= _data.rows() )
    return;

  _data( bin.y, bin.x ) += val;
}


// Description:
// Handle a pose event.
// This will increment the data for teh grid by one
bot_core_pose_t* _last_pose = NULL;
void handle_pose_t ( const lcm_recv_buf_t *rbuf __attribute__((unused)), 
		     const char * channel __attribute__((unused)),
		     const bot_core_pose_t * msg, 
		     void * user )
{

  // check if we have moved
  if( _last_pose == false ||
      bot_vector_dist_3d( _last_pose->pos, msg->pos ) > 0.1 ) {
    
    if( _data.size() > 0 ) {
      
      // get the bin for position
      bin_t b = compute_bin( msg->pos );
      
    // updateh the data
      update_data( b, 0.1 );
    }

    // update last pose used
    if( _last_pose )
      bot_core_pose_t_destroy( _last_pose );
    _last_pose = bot_core_pose_t_copy( msg );
  }

}
  



int main( int argc, char** argv )
{

  // we expect a log directory
  if( argc < 2 ) {
    std::cout << "usage: " << argv[0] << " simulation-dir" << std::endl;
    exit(-1);
  }

  // Ok, initialize the system
  _min_x = -10;
  _max_x = 2;
  _min_y = -14;
  _max_y = 0;
  _num_x = 120;
  _num_y = 140;
  _data = matrix_t( _num_y, _num_x );
  
  // initialzie data to zero
  for( size_t i = 0; i < _data.rows(); ++i ) {
    for( size_t j = 0; j < _data.cols(); ++j ) {
      _data(i,j) = 0;
    }
  }

  // read files from directory and process each
  path base_dir( argv[1] );
  for( directory_iterator diter( base_dir ); 
       diter != directory_iterator(); 
       ++diter ) {

    std::string filename = diter->path().c_str();
    
    // Ok, reset our position state
    _last_pose = NULL;

    // Create LCM and process this log
    std::string lcm_url = "file://" + filename + "?speed=0.0";
    lcm_t *lcm = lcm_create( lcm_url.c_str() );

    // if we could not create it, skip it 
    if( lcm == NULL ) {
      std::cout << "  SKIPPING '" << filename << "'" << std::endl;
      continue;
    }
    
    // setup lcm hooks
    bot_core_pose_t_subscribe( lcm, "POSE", handle_pose_t, NULL );

    // run lcm log until done
    std::cout << "running '" << diter->path().filename() << "'";
    while( lcm_handle( lcm ) == 0 ) {
      ; // run lcm
    }
    std::cout << " ... done" << std::endl;

    lcm_destroy( lcm );
    
  }

  // create output file
  path out_path( base_dir / "density_traj.ssv" );
  std::ofstream fout( out_path.c_str() );
  fout << _data;
  std::cout << "Wrote '" << out_path << "'" << std::endl;
}
