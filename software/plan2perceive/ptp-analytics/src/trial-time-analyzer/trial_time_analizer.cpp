
#include <iostream>
#include <sstream>
#include <fstream>

// Include the object candidate message
#include <carmen3d/carmen3d_lcmtypes.h>
//#include <carmen3d_object_candidate_list_t.h>
//#include <carmen3d_detector_output_t.h>
#include <carmen3d/lcm_utils.h>

// Include all common ptp code
#include <ptp_common/ptp_common.h>


// We'll be using std and ptp a lot, so have using clauses
using namespace std;
//using namespace ptp;

//==========================================================================


// This structure represents the commulative state we are
// accruing as we get more object candidate messages
struct accum_data_t
{
  bool trial_has_started;
  bool trial_has_ended;
  bool object_accepted;
  double path_length;
  double trial_start_time;
  double trial_end_time;
  size_t detection_of_object_count;
  size_t detection_frames_without_object_count;
  ofstream out;

  accum_data_t()
    : trial_has_started( false ),
      trial_has_ended( false ),
      object_accepted( false ),
      path_length( 0.0 ),
      trial_start_time( -1 ),
      trial_end_time( -1 ),
      detection_of_object_count( 0 ),
      detection_frames_without_object_count( 0 )
  { }

};


//==========================================================================

// This function integrates a detector_output_t message into
// the accumulation data
void integrate( const carmen3d_detector_output_t* msg, accum_data_t* data )
{
  if( data->trial_has_started &&
      !data->trial_has_ended ) {

    // If the message has any detection, count them as being for the object
    data->detection_of_object_count += msg->num_detections;
    
    // No detections incremenets the frames-without-object count
    if( msg->num_detections < 1 ) {
      data->detection_frames_without_object_count++;
    }

  }

  // we start the trial when we get the first object detection
  if( data->trial_has_started == false &&
      msg->num_detections > 0 ) {
    data->trial_has_started = true;
    data->trial_start_time = msg->utime;
    cout << "Started Trial" << endl << flush;
  }
}


//==========================================================================


// This function integrates an object_candidata_list_t message int
// the accumulation data
void integrate( const carmen3d_object_candidate_list_t* msg, accum_data_t* data )
{
  if( data->trial_has_started &&
      !data->trial_has_ended )
    {
      
      // the trial ends as soon as the probability of the object
      // get to 5% ( or 95% )
      for( int i = 0; i < msg->num_objects; ++i ) {
	if( msg->obj_prob[i] <= 0.05 ) {
	  
	  data->trial_has_ended = true;
	  data->object_accepted = false;
	  data->trial_end_time = msg->utime;
	  cout << "Ended Trial (NEGATIVE)" << endl << flush;
	}
	
	if( msg->obj_prob[i] >= 0.95 ) {
	  
	  data->trial_has_ended = true;
	  data->object_accepted = true;
	  data->trial_end_time = msg->utime;
	  cout << "Ended Trial (POISITVE)" << endl << flush;
	}
      }
      
    }
}


//==========================================================================


// Integrate the position of the robot into the accumulated data
void integrate( const carmen3d_pose_t* msg, accum_data_t* data )
{

  // this is a local static variable
  static carmen3d_pose_t* last_pose_message = NULL;


  if( data->trial_has_ended &&
      !data->trial_has_ended ) {
   
    if( last_pose_message != NULL ) {
      double dx = msg->pos[0] - last_pose_message->pos[0];
      double dy = msg->pos[1] - last_pose_message->pos[1];
      double dist = sqrt( dx * dx + dy * dy );
      if( true || dist > 0.001 ) {
	data->path_length += dist;
	if( last_pose_message ) {
	  carmen3d_pose_t_destroy( last_pose_message );
	}
	last_pose_message = carmen3d_pose_t_copy( msg );
      }
    } else {
      last_pose_message = carmen3d_pose_t_copy( msg );
    }
  
  }
}


//==========================================================================


// These are the LCM handlers, which simply forward to the integrate methods
static void pose_handler
( const lcm_recv_buf_t *rbuf __attribute__((unused)), 
  const char * channel __attribute__((unused)),
  const carmen3d_pose_t * msg, 
  void * user) 
{  
  integrate( msg, (accum_data_t*)user );
}

static void object_candidate_list_handler
( const lcm_recv_buf_t *rbuf __attribute__((unused)), 
  const char * channel __attribute__((unused)),
  const carmen3d_object_candidate_list_t * msg, 
  void * user )
{
  integrate( msg, (accum_data_t*)user );
}

static void detector_output_handler
( const lcm_recv_buf_t *rbuf __attribute__((unused)), 
  const char * channel __attribute__((unused)),
  const carmen3d_detector_output_t * msg, 
  void * user )
{
  integrate( msg, (accum_data_t*)user );
  cout << "." << flush;
}


//==========================================================================


// print results of accumulated data
void print_results( accum_data_t* data )
{
  cout << "Analysis: " << endl;
  cout << "  Object Accepted: " << data->object_accepted << endl;
  cout << "  Path Length    : " << data->path_length << endl;
  cout << "  Time           : " << ( data->trial_end_time - data->trial_start_time ) / 1000000.0 << "s" << endl;
  cout << "  Detections     : " << data->detection_of_object_count << endl;
  cout << "  Empty Frames   : " << data->detection_frames_without_object_count << endl;
  
  data->out << "Analysis: " << endl;
  data->out << "  Object Accepted: " << data->object_accepted << endl;
  data->out << "  Path Length    : " << data->path_length << endl;
  data->out << "  Time           : " << ( data->trial_end_time - data->trial_start_time ) / 1000000.0 << "s" << endl;
  data->out << "  Detections     : " << data->detection_of_object_count << endl;
  data->out << "  Empty Frames   : " << data->detection_frames_without_object_count << endl;
}


//==========================================================================


// Setup handlers
void setup_lcm_handlers( lcm_t* lcm , accum_data_t* data)
{
  carmen3d_object_candidate_list_t_subscribe( lcm, "OBJECT_CANDIDATES", object_candidate_list_handler, data );
  carmen3d_pose_t_subscribe( lcm, "POSE", pose_handler, data );
  carmen3d_detector_output_t_subscribe( lcm, "DETECTOR_OUTPUT_CHANNEL", detector_output_handler, data );

  cout << "Setup LCM handlers ..." << endl << flush;
}


//==========================================================================


// The main function, which just sets up the handlers and 
// prints out the final accumulated data
int main( int argc, char** argv )
{

  // get LCM singetone
  lcm_t* lcm;
  if( argc > 1 ) {
    std::ostringstream oss;
    oss << "file://" << argv[1] << "?speed=1.0"; 
    cout << "Reading LOG file: " << oss.str() << endl << flush;
    lcm = lcm_create( oss.str().c_str() );
  } else {
    lcm = lcm_create( NULL );
  }
  


  accum_data_t *data = new accum_data_t();


  if( argc > 2 ) {
    data->out.open( argv[2] );
    data->out << "Filename: " << argv[1] << endl << flush;
  } else {
    ostringstream oss;
    oss << argv[1] << ".analysis";
    data->out.open( oss.str().c_str() );
    data->out << "Filename: " << argv[1] << endl << flush;
  }    

  
  // setup handlers
  setup_lcm_handlers( lcm, data );

  // dispatch
  while( data->trial_has_ended == false ) {
    lcm_handle( lcm );
  }

  // print out results
  print_results( data );

  delete data;

  return 0;

}
