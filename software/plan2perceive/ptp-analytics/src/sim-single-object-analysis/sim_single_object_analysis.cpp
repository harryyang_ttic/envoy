
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <set>

// Include the object candidate message
#include <carmen3d/carmen3d_lcmtypes.h>
#include <carmen3d/lcm_utils.h>

// Include all common ptp code
#include <ptp_common/ptp_common.h>


// We'll be using std and ptp a lot, so have using clauses
using namespace std;


//==========================================================================


// This structure describes a ground truth object
struct ground_truth_object_t 
{
  double x;
  double y;

  ground_truth_object_t( const double& a, const double& b )
    : x(a), y(b)
  {}
};

// This structure represents the commulative state we are
// accruing as we get more object candidate messages
struct accum_data_t
{
  bool trial_has_started;
  bool trial_has_ended;
  double path_length;
  double trial_start_time;
  double trial_end_time;
  double wanted_trial_end_time;
  size_t detection_of_object_count;
  size_t detection_frames_without_object_count;
  carmen3d_object_candidate_list_t* last_object_candidate_list_message;
  vector<ground_truth_object_t> ground_truth_objects;
  ofstream out;
  ofstream global_results_out;

  size_t false_positive_count;
  size_t true_positive_count;
  size_t false_negative_count;

  accum_data_t()
    : trial_has_started( false ),
      trial_has_ended( false ),
      path_length( 0.0 ),
      trial_start_time( -1 ),
      trial_end_time( -1 ),
      wanted_trial_end_time( -1 ),
      detection_of_object_count( 0 ),
      detection_frames_without_object_count( 0 ),
      last_object_candidate_list_message( NULL ),
      false_positive_count(0),
      true_positive_count(0),
      false_negative_count(0)
  { }

};


//==========================================================================

// This function integrates a detector_output_t message into
// the accumulation data
void integrate( const carmen3d_detector_output_t* msg, accum_data_t* data )
{
  if( !data->trial_has_ended ) {

    // If the message has any detection, count them as being for the object
    data->detection_of_object_count += msg->num_detections;
    
    // No detections incremenets the frames-without-object count
    if( msg->num_detections < 1 ) {
      data->detection_frames_without_object_count++;
    }

  }

}


//==========================================================================


// This function integrates an object_candidata_list_t message int
// the accumulation data
void integrate( const carmen3d_object_candidate_list_t* msg, accum_data_t* data )
{
  if( data->trial_has_started &&
      !data->trial_has_ended )
    {

      // save this message as last
      if( data->last_object_candidate_list_message != NULL ) {
	carmen3d_object_candidate_list_t_destroy( data->last_object_candidate_list_message );
      }
      data->last_object_candidate_list_message = carmen3d_object_candidate_list_t_copy( msg );
      
      // the trial ends if we have a wanted time and it is up
      if( data->wanted_trial_end_time > 0 &&
	  msg->utime > data->wanted_trial_end_time ) {
	
	data->trial_has_ended = true;
	data->trial_end_time = msg->utime;
      }
      
    }
}


//==========================================================================


// Integrate the position of the robot into the accumulated data
void integrate( const carmen3d_pose_t* msg, accum_data_t* data )
{

  // this is a local static variable
  static carmen3d_pose_t* last_pose_message = NULL;


  if( data->trial_has_started &&
      !data->trial_has_ended ) {
   
    if( last_pose_message != NULL ) {
      double dx = msg->pos[0] - last_pose_message->pos[0];
      double dy = msg->pos[1] - last_pose_message->pos[1];
      double dist = sqrt( dx * dx + dy * dy );
      if( true || dist > 0.001 ) {
	data->path_length += dist;
	if( last_pose_message ) {
	  carmen3d_pose_t_destroy( last_pose_message );
	}
	last_pose_message = carmen3d_pose_t_copy( msg );
      }
    } else {
      last_pose_message = carmen3d_pose_t_copy( msg );
    }

    // the trial ends if we have a wanted time and it is up
    if( data->wanted_trial_end_time > 0 &&
	msg->utime > data->wanted_trial_end_time ) {
      
      data->trial_has_ended = true;
      data->trial_end_time = msg->utime;
    }

    // always set the end trial time
    data->trial_end_time = msg->utime;
  }

  // if we have not started, start
  if( data->trial_has_started == false ) {
    data->trial_has_started = true;
    data->trial_start_time = msg->utime;
    if( data->wanted_trial_end_time > 0 ) {
      data->wanted_trial_end_time += msg->utime;
    }
  }
}


//==========================================================================


// These are the LCM handlers, which simply forward to the integrate methods
static void pose_handler
( const lcm_recv_buf_t *rbuf __attribute__((unused)), 
  const char * channel __attribute__((unused)),
  const carmen3d_pose_t * msg, 
  void * user) 
{  
  integrate( msg, (accum_data_t*)user );
}

static void object_candidate_list_handler
( const lcm_recv_buf_t *rbuf __attribute__((unused)), 
  const char * channel __attribute__((unused)),
  const carmen3d_object_candidate_list_t * msg, 
  void * user )
{
  integrate( msg, (accum_data_t*)user );
}

static void detector_output_handler
( const lcm_recv_buf_t *rbuf __attribute__((unused)), 
  const char * channel __attribute__((unused)),
  const carmen3d_detector_output_t * msg, 
  void * user )
{
  integrate( msg, (accum_data_t*)user );
  cout << "." << flush;
}



//==========================================================================

// Loads the ground truth from a sim_objects file
void load_ground_truth_from_sim_objects_stream
( istream& is, accum_data_t* data )
{
  double x, y, z, theta;
  string obj_class;

  //cout << "Loading groiund truth ...." << endl;
  
  string line;
  while( getline( is, line ) ) {
    if( line.find( "#" ) == string::npos ) {
      istringstream iss( line );
      iss >> x >> y >> z >> theta >> obj_class;
      //cout << "  .. loaded line '" << line << "' = " << x << "," << y << "," << z << "," << theta << ",'" << obj_class << "'" << endl;
      if( obj_class == "door" ) {
	//cout << "added ground truth: " << line << endl;
	data->ground_truth_objects.push_back( ground_truth_object_t( x, y ) );
      }
    }
  }
}

//==========================================================================

// Returns if trow objects are the same based on location
bool are_objects_the_same( const ground_truth_object_t& a,
			   const ground_truth_object_t& b ) 
{
  return sqrt( (a.x - b.x) * (a.x - b.x) +
	       (a.y - b.y) * (a.y - b.y) ) 
    < 2.0; 
}

//==========================================================================

// Analyze the last candidates message and ground truth to compute
// the false positive, false negatigve, and true positive counts
void analyze( accum_data_t* data )
{
  
  // ok, get temporaty copy of candidate desicions
  vector<ground_truth_object_t> accepted_objects;
  if( data->last_object_candidate_list_message != NULL ) {
    for( size_t i = 0; i < data->last_object_candidate_list_message->num_objects; ++i ) {
      if( data->last_object_candidate_list_message->obj_prob[i] > 0.5 ) {
	accepted_objects.push_back
	  ( ground_truth_object_t
	    ( data->last_object_candidate_list_message->obj_x[i],
	      data->last_object_candidate_list_message->obj_y[i] ) );
      }							  
    }
  }

  // Ok, compute true positive count
  set<size_t> indices_to_remove;
  for( size_t i = 0; i < accepted_objects.size(); ++i ) {
    bool found_accepted = false;
    for( size_t j = 0; j < data->ground_truth_objects.size(); ++j ) {
      if( are_objects_the_same( accepted_objects[i], data->ground_truth_objects[j] ) ) {
	data->true_positive_count++;
	indices_to_remove.insert( j );
	found_accepted = true;
	break;
      }
    }

    // ok, if accepted was not found, then it is a false positiuve
    if( found_accepted == false ) {
      data->false_positive_count++;
    }
  }

  // Ok, now all we have left are to check the false negatives
  data->false_negative_count = data->ground_truth_objects.size() - indices_to_remove.size();
  
}

//==========================================================================


// print results of accumulated data
void print_results( accum_data_t* data )
{
  cout << "Truth: " << endl;
  for( size_t i = 0; i < data->ground_truth_objects.size(); ++i ) {
    cout << "  (" << i << ") : " << data->ground_truth_objects[i].x << " , " << data->ground_truth_objects[i].y << endl; 
  }
  cout << "Found: " << endl;
  if( data->last_object_candidate_list_message != NULL ) {
    for( size_t i = 0; i < data->last_object_candidate_list_message->num_objects; ++i ) {
      cout << "  [" << i << "] : " << ( data->last_object_candidate_list_message->obj_prob[i] > 0.5 ? "DOOR " : "OTHER" ) << " " << data->last_object_candidate_list_message->obj_x[i] << " , " << data->last_object_candidate_list_message->obj_y[i] << endl;
    }
  }

  cout << "Analysis: " << endl;
  cout << "TP: " << data->true_positive_count << endl;
  cout << "FP: " << data->false_positive_count << endl;
  cout << "FN: " << data->false_negative_count << endl;
  cout << "  Path Length    : " << data->path_length << endl;
  cout << "  Time           : " << ( data->trial_end_time - data->trial_start_time ) / 1000000.0 << "s" << endl;
  cout << "  Detections     : " << data->detection_of_object_count << endl;
  cout << "  Empty Frames   : " << data->detection_frames_without_object_count << endl;
  
  data->out << "Analysis: " << endl;
  data->out << "TP: " << data->true_positive_count << endl;
  data->out << "FP: " << data->false_positive_count << endl;
  data->out << "FN: " << data->false_negative_count << endl;
  data->out << "  Path Length    : " << data->path_length << endl;
  data->out << "  Time           : " << ( data->trial_end_time - data->trial_start_time ) / 1000000.0 << "s" << endl;
  data->out << "  Detections     : " << data->detection_of_object_count << endl;
  data->out << "  Empty Frames   : " << data->detection_frames_without_object_count << endl;

  if( data->global_results_out.is_open() ) {
    data->global_results_out 
      << data->true_positive_count << " " 
      << data->false_positive_count << " " 
      << data->false_negative_count << " " 
      << data->path_length << " " 
      << setprecision( 20 ) << ( data->trial_end_time - data->trial_start_time ) << " "
      << data->detection_of_object_count << " " 
      << data->detection_frames_without_object_count 
      << endl << flush;
  }
}


//==========================================================================


// Setup handlers
void setup_lcm_handlers( lcm_t* lcm , accum_data_t* data)
{
  carmen3d_object_candidate_list_t_subscribe( lcm, "OBJECT_CANDIDATES", object_candidate_list_handler, data );
  carmen3d_pose_t_subscribe( lcm, "POSE", pose_handler, data );
  carmen3d_detector_output_t_subscribe( lcm, "DETECTOR_OUTPUT_CHANNEL", detector_output_handler, data );

  cout << "Setup LCM handlers ..." << endl << flush;
}


//==========================================================================


// The main function, which just sets up the handlers and 
// prints out the final accumulated data
int main( int argc, char** argv )
{

  if( argc < 2 ) {
    cout << "usage: " << argv[0] << " lcm-log-filename [global-result-analysis-filename]" << endl;
    return -1;
  }

  // get LCM singetone
  lcm_t* lcm;
  if( argc > 1 ) {
    std::ostringstream oss;
    //oss << "file://" << argv[1] << "?speed=1.0"; 
    oss << "file://" << argv[1] << "?speed=3.0"; 
    cout << "Reading LOG file: " << oss.str() << endl << flush;
    lcm = lcm_create( oss.str().c_str() );
  } else {
    lcm = lcm_create( NULL );
  }
  


  accum_data_t *data = new accum_data_t();

  ostringstream oss;
  oss << argv[1] << ".analysis";
  data->out.open( oss.str().c_str() );
  data->out << "Filename: " << argv[1] << endl << flush;

  // open the global_resulsts file ( if any )
  if( argc > 2 ) {
    data->global_results_out.open( argv[2], ios_base::app );
    data->out << "Global Reulsts: " << argv[2] << endl << flush;
  }

  // load the grouth truth
  ifstream fin( "/home/velezj/projects/object_detector_pose_planning/wheelchair/plan2perceive/build/data/maps/sim-small-2/sim_objects.txt" );
  load_ground_truth_from_sim_objects_stream( fin, data );
  
  // setup handlers
  setup_lcm_handlers( lcm, data );

  // dispatch
  while( data->trial_has_ended == false && !lcm_handle( lcm ) ) {
  }

  // analyse
  analyze( data );

  // print out results
  print_results( data );

  delete data;

  return 0;

}
