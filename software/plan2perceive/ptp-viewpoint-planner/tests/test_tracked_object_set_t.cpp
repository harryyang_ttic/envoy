

#include <ptp-viewpoint-planner/tracked_object_set_t.hpp>
#include <ptp-common-coordinates/identity_manifold_t.hpp>
#include <ptp-common-coordinates/translation_manifold_t.hpp>
#include <ptp-common-test/test.hpp>
#include <bot_core/tictoc.h>
#include <iostream>
#include <fstream>


using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::detectors;
using namespace ptp::sensormodels;
using namespace ptp::planner;

//==================================================================

object_detection_t create_dummy_detection( const int& type, 
					   const float& det,
					   const double loc[3],
					   const angle_t& orient,
					   const manifold_tp& man )
{
  // create a dummy lcm detection
  ptplcm_object_detection_t *p = new ptplcm_object_detection_t();
  p->detector_value = det;
  p->position_in_detector_frame[0] = loc[0];
  p->position_in_detector_frame[1] = loc[1];
  p->position_in_detector_frame[2] = loc[2];
  p->orientation_from_detector_plane = orient.from(angle_t::X_AXIS).radians();
  p->object_class = type;

  std::cout << "test : creating dummy det, man=" << *man << std::endl;
  
  object_detection_t d( *p, man );
  return d;
}

//==================================================================

int test_compile()
{
  start_of_test;

  // Create the robot manifold
  manifold_tp man( new identity_manifold_t(manifold_t::BASE_MANIFOLD) );

  // Create a new set
  tracked_object_set_t tset( 1, deg( 45 ).from( angle_t::X_AXIS ), true );

  // Feed initial two detections for object
  double loc0[] = { 10, 0, 0 };
  object_detection_t d0 = create_dummy_detection( 0, 0, loc0, deg(0).from(angle_t::X_AXIS), man );
  tset.update( d0, man );
  double loc1[] = { 3, 0, 0 };
  object_detection_t d1 = create_dummy_detection( 0, 1, loc1, deg(0).from(angle_t::X_AXIS), man );
  tset.update( d1, man );
  
  
  // Feed it some dummy detections
  for( size_t i = 0; i < 10; ++i ) {
    double dist = 2;
    double loc[] = { dist, 0, 0 };
    manifold_tp tman = translate( coordinate( 10-dist, 0, 0 ).on( man ), man );
    object_detection_t d = create_dummy_detection( 0, 8, loc, deg(0).from( angle_t::X_AXIS ), tman );
    bot_tictoc( "tset-update");
    tset.update( d, man );
    bot_tictoc( "tset-update");
  }
  for( size_t i = 0; i < 10; ++i ) {
    double dist = 7;
    double loc[] = { dist, 0, 0 };
    manifold_tp tman = translate( coordinate( 3-dist, 0, 0 ).on( man ), man );
    object_detection_t d = create_dummy_detection( 0, 0.03, loc, deg(0).from( angle_t::X_AXIS ), tman );
    bot_tictoc( "tset-update");
    tset.update( d, man );
    bot_tictoc( "tset-update");
  }

  // print out teh object set
  tset.print( std::cout, man );
  std::cout << std::endl;
  
  // print out the tictoc
  bot_tictoc_print_stats( BOT_TICTOC_ALPHABETICAL );

  end_of_test;
}

//==================================================================


object_detection_t
create_object_detection( const detection_and_location_t& dx, 
			 const tracked_object_tp& obj, 
			 const manifold_tp& view_man,
			 const ptplcm_detector_info_t& info )
{
  coordinate_t oc = coordinate( 0,0,0 ).on( obj->object_manifold() ).on( view_man );
  ptplcm_object_detection_t msg;
  msg.object_class = obj->object_type();
  msg.detector_value = dx.detection_value;
  msg.position_in_detector_frame[0] = oc[0];
  msg.position_in_detector_frame[1] = oc[1];
  msg.position_in_detector_frame[2] = oc[2];
  msg.distance_from_detector = dx.location.dist;
  msg.orientation_from_detector_plane = dx.location.orientation.radians();
  msg.detector_info = info;

  object_detection_t det( msg, view_man );
  
  return det;
}



int test_sampling()
{
  start_of_test;

  // Create the robot manifold
  manifold_tp man( new identity_manifold_t(manifold_t::BASE_MANIFOLD) );

  // Create a new set
  tracked_object_set_t tset( 1, deg( 45 ).from( angle_t::X_AXIS ), true );

  // creat detector info
  ptplcm_detector_info_t info;
  info.position_in_robot_body_frame[0] = 0;
  info.position_in_robot_body_frame[1] = 0;
  info.position_in_robot_body_frame[2] = 0;
  info.look_at_axis_in_robot_body_frame[0] = 1;
  info.look_at_axis_in_robot_body_frame[1] = 0;
  info.look_at_axis_in_robot_body_frame[2] = 0;
  info.angle_about_look_at_axis = 0;
  info.fov_left_radians = deg( 60).from(angle_t::X_AXIS).radians();
  info.fov_right_radians = deg(-60).from(angle_t::X_AXIS).radians();
  info.fov_top_radians = deg(-60).from(angle_t::X_AXIS).radians();
  info.fov_bottom_radians = deg(-60).from(angle_t::X_AXIS).radians();
  info.fov_min_distance = 0;
  info.fov_max_distance = 9;


  // Feed initial three detections for object
  double loc0[] = { 10, 0, 0 };
  object_detection_t d0 = create_dummy_detection( 1, 0, loc0, deg(0).from(angle_t::X_AXIS), man );
  tset.update( d0, man );
  double loc1[] = { 3, 0, 0 };
  object_detection_t d1 = create_dummy_detection( 1, 0.6, loc1, deg(0).from(angle_t::X_AXIS), man );
  tset.update( d1, man );
  tset.update_perception( info, man );
  //double loc2[] = { 5, 5, 0 };
  //object_detection_t d2 = create_dummy_detection( 1, 0.6, loc2, deg(90).from(angle_t::X_AXIS), man );
  //tset.update( d2, man );


  // dump the normalized perception fields and additive fields
  //tset.dump_norm_perception_fields( "nrom_perception_field_" );
  //tset.dump_additive_perception_field( "additive_perception_field.ssv", man );

  // print out the tracked object set
  tset.print( std::cout, man );
  std::cout << std::endl;

  // now sample some viewpoints
  size_t num_samples = 10;
  std::vector< viewpoint_t > views;
  matrix_t view_mat( num_samples, 2 );
  views.reserve( num_samples );
  
  std::cout << "  sampliong..." << std::endl;
  bot_tictoc( "tset-sample-100");
  for( size_t k = 0; k < num_samples; ++k ) {
    
    size_t traj_size = 1;
    bot_tictoc( "single-traj-sample" );
    for( size_t i = 0; i < traj_size; ++i ) {
      
      viewpoint_t view = tset.sample_viewpoint( man );
      
      // updateh te perception field
      detection_and_location_t dx = view.create_mean_detection_from_viewpoint( tset.object(0), tset.sensor_model( 0 ), man );
      object_detection_t det = create_object_detection( dx, tset.object(0), view.viewpoint_manifold(), info );
      tset.update_tracked_object( tset.object(0), det, man );
      tset.update_perception( info, man );
    }    
    bot_tictoc( "single-traj-sample" );

  }
  bot_tictoc( "tset-sample-100");

  // for( size_t i = 0; i < num_samples; ++i ) {
  //   viewpoint_t vp = tset.sample_viewpoint(man);
  //   views.push_back( vp );
  //   coordinate_t c = coordinate( 0,0,0 ).on(vp.viewpoint_manifold()).on(man);
  //   view_mat( i, 0 ) = c[0];
  //   view_mat( i, 1 ) = c[1];
  // }

  std::ofstream fout( "sampled_viewpoints.ssv" );
  fout << view_mat;
  fout.close();

  // print out the tictoc
  bot_tictoc_print_stats( BOT_TICTOC_ALPHABETICAL );
  

  end_of_test;
}

//==================================================================

int main( int argc, char** argv )
{

  //run_test( test_compile() );
  run_test( test_sampling() );

  return 0;
}
