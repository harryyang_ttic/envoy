
#include <lcm/lcm.h>
#include <lcmtypes/erlcm_navigator_goal_msg_t.h>
#include <lcmtypes/ptplcm_object_initial_beliefs_t.h>
#include <bot_core/timestamp.h>


int main( int argc, char** argv ) 
{

  ptplcm_object_initial_beliefs_t ping;
  ping.utime = bot_timestamp_now();
  ping.size = 0;
  

  erlcm_navigator_goal_msg_t msg;
  msg.utime = bot_timestamp_now();
  msg.goal.x = 20;
  msg.goal.y = -7;
  msg.velocity = 0.07;
  msg.sender = ERLCM_NAVIGATOR_GOAL_MSG_T_SENDER_USAR;
  msg.nonce = msg.utime;
  
  lcm_t *lcm = lcm_create(NULL);
  ptplcm_object_initial_beliefs_t_publish( lcm, "PTP_INITIAL_BELIEFS", &ping );
  erlcm_navigator_goal_msg_t_publish( lcm, "PTP_PLANNER_GOAL", &msg );
  
  lcm_destroy( lcm );

  return 0;
}
