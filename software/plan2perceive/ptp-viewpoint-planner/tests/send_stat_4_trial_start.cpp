
#include <lcm/lcm.h>
#include <lcmtypes/erlcm_navigator_goal_msg_t.h>
#include <lcmtypes/ptplcm_object_initial_beliefs_t.h>
#include <bot_core/timestamp.h>
#include <math.h>


int main( int argc, char** argv ) 
{

  ptplcm_object_initial_beliefs_t ping;
  ping.utime = bot_timestamp_now();
  ping.size = 1;
  ping.object_types = new int32_t[1];
  ping.object_poses = new ptplcm_point_t[1];
  ping.object_belief_means = new double[1];
  ping.object_belief_variances = new double[1];
  ping.object_types[0] = 1;
  ping.object_poses[0].x = 21.875;
  ping.object_poses[0].y = -7.325;
  ping.object_poses[0].z = -1.373;
  ping.object_poses[0].yaw = -135 * M_PI / 180.0;
  ping.object_belief_means[0] = 0.5;
  ping.object_belief_variances[0] = 0.25;
  

  erlcm_navigator_goal_msg_t msg;
  msg.utime = bot_timestamp_now();
  msg.goal.x = 25;
  msg.goal.y = -10;
  msg.velocity = 0.07;
  msg.sender = ERLCM_NAVIGATOR_GOAL_MSG_T_SENDER_USAR;
  msg.nonce = msg.utime;
  
  lcm_t *lcm = lcm_create(NULL);
  ptplcm_object_initial_beliefs_t_publish( lcm, "PTP_INITIAL_BELIEFS", &ping );
  erlcm_navigator_goal_msg_t_publish( lcm, "PTP_PLANNER_GOAL", &msg );
  
  lcm_destroy( lcm );

  return 0;
}
