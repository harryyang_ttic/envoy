

#include <ptp-sensor-models/sensor_models_t.hpp>
#include <ptp-sensor-models/learned_sensor_models.hpp>
#include <ptp-viewpoint-planner/perception_field_t.hpp>
#include <ptp-common-coordinates/identity_manifold_t.hpp>
#include <ptp-common-test/test.hpp>
#include <iostream>
#include <fstream>


using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::detectors;
using namespace ptp::sensormodels;
using namespace ptp::planner;

//========================================================================

int test_create_perception_field()
{
  start_of_test;

  // create a sensor model for tet objects
  sensor_models_t models;
  dist_and_orientation_t dao( 0.1, rad(0).from(angle_t::X_AXIS));
  detection_and_location_t dx( 0.4, dao );

  // add new object
  int obj = models.add_new_object( 1, dx );
  std::cout << "added object" << std::endl;

  // our prior belief
  mean_variance_t bel;
  bel.mean = 0.5;
  bel.var = sqrt(0.25);

  // Ok, create a sensor model and query the perception field at certain
  // points
  float min_dist = 0;
  float max_dist = 6;
  float min_angle = 0;
  float max_angle = M_PI/2;
  size_t num_dist = 60;
  size_t num_angle = 20;
  
  std::cout << "querying field..." << std::endl;
  matrix_t field( num_dist, num_angle );
  for( size_t i = 0; i < num_dist; ++i ) {
    for( size_t j = 0; j < num_angle; ++j ) {
      dao.dist = min_dist + ( max_dist - min_dist ) * ( i / (double)(num_dist-1) );
      float r = min_angle + ( max_angle - min_angle ) * ( j / (double)(num_angle - 1));
      dao.orientation = rad(r).from(angle_t::X_AXIS);
      field( i, j ) = perception_field_t::field_value_at( dao, bel, models.object(obj) );
    }
  }
  
  // write out matrix to stream
  std::ofstream fout( "perception_field.ssv" );
  fout << field;
  fout.close();
  std::cout << "wrote field to file" << std::endl;

  end_of_test;
}

//========================================================================

int test_create_perception_field_xy()
{
  start_of_test;

  // create a sensor model for tet objects
  sensor_models_t models;

  // try different initi object detections
  std::vector<float> init_detection_values;
  std::vector<float> init_detection_angle;
  std::vector<float> init_detection_dist;
  init_detection_values.push_back( 0.4 );
  init_detection_dist.push_back( 0.1 );
  init_detection_angle.push_back( 0 );
  init_detection_values.push_back( 0.4 );
  init_detection_dist.push_back( 1 );
  init_detection_angle.push_back( 0 );
  init_detection_values.push_back( 0.4 );
  init_detection_dist.push_back( 2 );
  init_detection_angle.push_back( 0 );
  init_detection_values.push_back( 0.4 );
  init_detection_dist.push_back( 3 );
  init_detection_angle.push_back( 0 );
  init_detection_values.push_back( 0.4 );
  init_detection_dist.push_back( 4 );
  init_detection_angle.push_back( 0 );

  for( size_t di = 0; di < init_detection_values.size(); ++di ) {
    dist_and_orientation_t dao( init_detection_dist[di],
				rad(init_detection_angle[di]).from(angle_t::X_AXIS));
    detection_and_location_t dx( init_detection_values[di], dao );

    // add new object
    int obj = models.add_new_object( 1, dx );
    std::cout << "added object (" << di << ")" << std::endl;

    // our prior belief
    mean_variance_t bel;
    bel.mean = 0.5;
    bel.var = sqrt(0.25);
    
    // Ok, create a sensor model and query the perception field at certain
    // points
    float min_x = 0;
    float max_x = 6;
    float min_y = 0;
    float max_y = 6;
    size_t num_x = 60;
    size_t num_y = 60;
    float step_x = (max_x - min_x) / num_x;
    float step_y = (max_y - min_y) / num_y;
    std::cout << "querying field..." << std::endl;
    matrix_t field( num_y, num_x );
    for( size_t i = 0; i < num_y; ++i ) {
      for( size_t j = 0; j < num_x; ++j ) {
	float x = j*step_x;
	float y = i*step_y;
	dao.dist = sqrt( x*x + y*y );
	float r = atan2( y, x );
	dao.orientation = rad(r).from(angle_t::X_AXIS);
	field( i, j ) = perception_field_t::field_value_at( dao, bel, models.object(obj) );
      }
    }
    
    // write out matrix to stream
    std::ostringstream oss;
    oss << "perception_field_" 
	<< init_detection_values[di] << "_" 
	<< init_detection_dist[di] << "_" 
	<< init_detection_angle[di] << ".ssv";
    std::ofstream fout( oss.str().c_str() );
    fout << field;
    fout.close();
    std::cout << "wrote field " << oss.str() << " to file" << std::endl;
  }
    
  end_of_test;
}

//========================================================================

int test_create_perception_field_updated_xy(int object_type )
{
  start_of_test;

  // create a sensor model for tet objects
  sensor_models_t models;


  // try different initi object detections
  std::vector<float> detection_values;
  std::vector<float> detection_angle;
  std::vector<float> detection_dist;
  detection_values.push_back( 0.4 );
  detection_dist.push_back( 20 );
  detection_angle.push_back( 0 );
  detection_values.push_back( 0.4 );
  detection_dist.push_back( 2 );
  detection_angle.push_back( 0 );
  //detection_values.push_back( 0.4 );
  //detection_dist.push_back( 3 );
  //detection_angle.push_back( 0 );
  detection_values.push_back( 0.4 );
  detection_dist.push_back( 4 );
  detection_angle.push_back( 0 );
  //detection_values.push_back( 0.4 );
  //detection_dist.push_back( 5 );
  //detection_angle.push_back( 0 );
  detection_values.push_back( 0.8 );
  detection_dist.push_back( 6 );
  detection_angle.push_back( 0 );


  // add new object
  dist_and_orientation_t dao( detection_dist[0],
			      rad(detection_angle[0]).from(angle_t::X_AXIS));
  detection_and_location_t dx( detection_values[0], dao );
  int obj = models.add_new_object( object_type, dx );
  std::cout << "added object (" << obj << ")" << std::endl;
  


  for( size_t di = 1; di < detection_values.size(); ++di ) {
    
    // update the sensor model
    dist_and_orientation_t dao( detection_dist[di],
			      rad(detection_angle[di]).from(angle_t::X_AXIS));
    detection_and_location_t dx( detection_values[di], dao );
    models.object(obj)->update( dx );
    std::cout << "updated model" << std::endl;
    
    // our prior belief
    mean_variance_t bel;
    bel.mean = 0.5;
    bel.var = sqrt(0.25);
    
    // Ok, create a sensor model and query the perception field at certain
    // points
    float min_x = 0;
    float max_x = ( object_type == 1 ? 6 : 10 );
    float min_y = 0;
    float max_y =  ( object_type == 1 ? 6 : 10 );
    size_t num_x = 10 * max_x;
    size_t num_y = 10 * max_y;
    float step_x = (max_x - min_x) / num_x;
    float step_y = (max_y - min_y) / num_y;
    std::cout << "querying field..." << std::endl;
    matrix_t field( num_y, num_x );
    for( size_t i = 0; i < num_y; ++i ) {
      for( size_t j = 0; j < num_x; ++j ) {
	float x = j*step_x;
	float y = i*step_y;
	dao.dist = sqrt( x*x + y*y );
	float r = atan2( y, x );
	dao.orientation = rad(r).from(angle_t::X_AXIS);
	field( i, j ) = perception_field_t::field_value_at( dao, bel, models.object(obj) );
	if( object_type == 1 && fabs( r ) > M_PI / 4 )
	  field( i,j ) = 0;
      }
    }
    
    // write out matrix to stream
    std::ostringstream oss;
    oss << "perception_field_" << object_type << "_updated_" 
	<< detection_values[di] << "_" 
	<< detection_dist[di] << "_" 
	<< detection_angle[di] << ".ssv";
    std::ofstream fout( oss.str().c_str() );
    fout << field;
    fout.close();
    std::cout << "wrote field " << oss.str() << " to file" << std::endl;

    oss.str("");
    oss << "model_dump_" << object_type << "_updated_" 
	<< detection_values[di] << "_" 
	<< detection_dist[di] << "_" 
	<< detection_angle[di];
    models.object(obj)->dump_gps( oss.str() );

  }
    
  end_of_test;
}

//========================================================================

int test_create_init_perception_field_xy(int object_type)
{
  start_of_test;

  // create a sensor model for initial object
  // add new object
  sensor_models_t models;
  dist_and_orientation_t dao( 10,
			      rad(0).from(angle_t::X_AXIS));
  detection_and_location_t dx( 0.1, dao );
  int obj = models.add_new_object( object_type, dx );
  std::cout << "added object (" << obj << ")" << std::endl;


  // dump the gps
  models.object(obj)->dump_gps( "model_dump_" );
      
  // our prior belief
  mean_variance_t bel;
  bel.mean = 0.5;
  bel.var = sqrt(0.25);

  
  // Ok, create a sensor model and query the perception field at certain
  // points
  float min_x = 0;
  float max_x =  ( object_type == 1 ? 6 : 10 );
  float min_y = 0;
  float max_y =  ( object_type == 1 ? 6 : 10 );
  size_t num_x = 10 * max_x;
  size_t num_y = 10 * max_y;
  float step_x = (max_x - min_x) / num_x;
  float step_y = (max_y - min_y) / num_y;
  std::cout << "querying field..." << std::endl;
  matrix_t field( num_y, num_x );
  for( size_t i = 0; i < num_y; ++i ) {
    for( size_t j = 0; j < num_x; ++j ) {
      float x = j*step_x;
      float y = i*step_y;

      // take care of querying field only in positive quadrant
      // since it is symetric
      if( y < 0 )
	y = -y;
      if( x < 0 )
	x = -x;

      dao.dist = sqrt( x*x + y*y );
      float r = atan2( y, x );
      dao.orientation = rad(r).from(angle_t::X_AXIS);
      field( i, j ) = perception_field_t::field_value_at( dao, bel, models.object(obj) );
      if( object_type == 1 && fabs( r ) > M_PI / 4 )
	field( i,j ) = 0;
      if( dao.dist > max_x )
	field( i,j ) = 0;
    }
  }
  
  // write out matrix to stream
  std::ostringstream oss;
  oss << "perception_field_" << object_type << "_init.ssv";
  std::ofstream fout( oss.str().c_str() );
  fout << field;
  fout.close();
  std::cout << "wrote field " << oss.str() << " to file" << std::endl;


  end_of_test;
}

//========================================================================


int main( int argc, char** argv )
{

  //run_test( test_create_perception_field_xy() );
  run_test( test_create_init_perception_field_xy(0) );
  run_test( test_create_perception_field_updated_xy(0) );
  run_test( test_create_perception_field_updated_xy(1) );
  run_test( test_create_init_perception_field_xy(1) );

  return 0;
}
