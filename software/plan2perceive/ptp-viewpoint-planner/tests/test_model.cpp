
#include <ptp-viewpoint-planner/tracked_object_set_t.hpp>
#include <ptp-common-coordinates/identity_manifold_t.hpp>
#include <ptp-common-coordinates/translation_manifold_t.hpp>
#include <ptp-common-test/test.hpp>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
#include <GL/gl.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <iostream>
#include <fstream>
#include <vector>


using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::detectors;
using namespace ptp::sensormodels;
using namespace ptp::planner;

//==================================================================

object_detection_t create_dummy_detection( const int& type, 
					   const float& det,
					   const double loc[3],
					   const angle_t& orient,
					   const manifold_tp& robot_man,
					   const manifold_tp& world_man)
{

  coordinate_t c = coordinate( 0,0,0 ).on( robot_man ).on( world_man );
  dist_and_orientation_t dao = calculate_dist_and_orientation( robot_man, world_man );
  double q[4];
  double axis[3] = { 0,0,1 };
  bot_angle_axis_to_quat( dao.orientation.radians(), axis, q );
  double rpy[3];
  bot_quat_to_roll_pitch_yaw( q, rpy );

  std::cout << "   got det-loc= " << loc[0] << "," << loc[1] << "," << loc[2] << " det-angle= " << orient.radians() << std::endl;
  std::cout << "   robot= " << c << ", robot-angle= " << dao.orientation.radians() << " (" << rpy[2] << ")" << std::endl;

  // create a dummy lcm detection
  ptplcm_object_detection_t *p = new ptplcm_object_detection_t();
  p->detector_value = det;
  p->position_in_detector_frame[0] = loc[0];
  p->position_in_detector_frame[1] = loc[1];
  p->position_in_detector_frame[2] = loc[2];
  p->position_of_robot_when_taken[0] = c[0];
  p->position_of_robot_when_taken[1] = c[1];
  p->position_of_robot_when_taken[2] = c[2];
  p->orientation_of_robot_when_taken[0] = q[0];
  p->orientation_of_robot_when_taken[1] = q[1];
  p->orientation_of_robot_when_taken[2] = q[2];
  p->orientation_of_robot_when_taken[3] = q[3];
  p->orientation_from_detector_plane = orient.from(angle_t::X_AXIS).radians();
  p->object_class = type;

  
  object_detection_t d( *p, world_man );
  return d;
}


// Description:
// Get a set of angle, orientation, detection values to test with
std::vector<object_detection_t>
load_same_object_detections_from_master_log( const std::string& filename,
					     const manifold_tp& obj_man,
					     const manifold_tp& world_man )
{
  // The known same object detection from the text master log
  int min_det_index = 590;
  int max_det_index = 781;
  max_det_index = min_det_index + 5;
  
  // Load the file and read it line by line, creating a detection
  // out of those lines we want
  std::vector<object_detection_t> dets;
  std::ifstream fin( filename.c_str() );
  while( fin ) {
    int id;
    double det_value, x, y, w, h, dist, angle;
    int dummy, label;
    fin >> id >> det_value >> x >> y >> w >> h >> dist >> angle >> dummy >> label;

    if( id < min_det_index ||
	id > max_det_index )
      continue;
    
    // normalize the angle/dist
    dist = fabs( dist );
    while( angle > M_PI )
      angle -= M_PI;
    while( angle < M_PI )
      angle += M_PI;
    angle = fabs( angle );
    

    std::cout << "read in: det=" << det_value << " dist=" << dist << " angle=" << angle << std::endl;

    // Create the robot's location manifold
    manifold_tp temp_man 
      = create_object_manifold( coordinate( -dist * cos(-angle),
					    -dist * sin(-angle),
					    0 ).on( obj_man ),
				rad(-angle).from(angle_t::X_AXIS),
				obj_man );
    dist_and_orientation_t dao
      = calculate_dist_and_orientation( temp_man, world_man );
    std::cout << "temp_man= " << ( coordinate( 0,0,0 ).on( temp_man ).on( world_man ) ) << "  dao= " << dao.dist << ", " << dao.orientation.radians() << std::endl;
    manifold_tp robot_man 
      = create_object_manifold( coordinate( 0,0,0 ).on( temp_man ).on(world_man),
				dao.orientation,
				world_man );
    
    // create detection
    double loc[3] = { dist * cos(angle), dist * sin(angle), 0 };
    object_detection_t det = create_dummy_detection( 1, det_value, loc, 
						     rad(angle).from(angle_t::X_AXIS),
						     robot_man, world_man );
    
    dets.push_back( det );
  }

  return dets;
}


void draw_tracked_object_set( bot_lcmgl_t* _lcmgl,
			      const tracked_object_set_t& _tracked_objects,
			      const manifold_tp& _world_manifold ) 
{
  for( size_t i = 0; i < _tracked_objects.size(); ++i ) {
    
    // choose color based on mean belief
    mean_variance_t bel = _tracked_objects.object(i)->belief();
    float norm_mean = bel.mean;
    if( norm_mean < 0 )
      norm_mean = 0;
    if( norm_mean > 1 )
      norm_mean = 1;
    if( norm_mean < 0.5 ) {
      bot_lcmgl_color3f( _lcmgl, 0.75 - norm_mean, 0, 0 );
    } else if( norm_mean < 0.75 ) {
      bot_lcmgl_color3f( _lcmgl, 0, 0, 1 - 2 * (0.75 - norm_mean) );
    } else {
      bot_lcmgl_color3f( _lcmgl, 0, 1 - ( 1 - norm_mean ), 0 );
    }

    // grab the object center
    coordinate_t c = coordinate( 0,0,0 ).on( _tracked_objects.object(i)->object_manifold() ).on( _world_manifold );

    // draw a circle where we are tracking objects
    float radius = 0.5;
    double p[3] = { c[0], c[1], 0 };
    bot_lcmgl_disk( _lcmgl, p, 0, radius );

    // draw an orientation line
    bot_lcmgl_color3f( _lcmgl, 1, 1, 1 );
    dist_and_orientation_t dao = calculate_dist_and_orientation( _tracked_objects.object(i)->object_manifold(), _world_manifold );
    bot_lcmgl_begin( _lcmgl, GL_LINES );
    bot_lcmgl_vertex2f( _lcmgl, c[0], c[1] );
    bot_lcmgl_vertex2f( _lcmgl, 
			c[0] + radius * cos( dao.orientation.radians() ),
			c[1] + radius * sin( dao.orientation.radians() ) );
    bot_lcmgl_end( _lcmgl );

    
  } 
}


// Description:
// Feeds known detections from master log for a single
// object to the model and prints out the deliefs
void test_model()
{

  // Input log and output files
  std::string mlog = "/home/velezj/projects/object_detector_pose_planning/envoy/software/plan2perceive/training-data/master-logs/floor_3_and_1_lcmlog-2011-09-28.00-subset.master-log";
  std::string out_filename = "test_model.ssv";

  lcm_t* _lcm = lcm_create( NULL );
  bot_lcmgl_t* _lcmgl;
  _lcmgl = bot_lcmgl_init(_lcm, "ptp-test-model");
  bot_lcmgl_switch_buffer( _lcmgl );

  
  // open the output file
  std::ofstream fout( out_filename.c_str() );

  // Create the world manifold
  manifold_tp world_man( new identity_manifold_t(manifold_t::BASE_MANIFOLD) );

  // Creat the object location 
  manifold_tp obj_man = translate( coordinate( 0,0,0 ).on( world_man ), world_man );

  // Create a new set
  tracked_object_set_t tset( 1, deg( 45 ).from( angle_t::X_AXIS ), true );

  // Ok, now get the set of detection to feed it
  std::vector<object_detection_t> dets 
    = load_same_object_detections_from_master_log( mlog, obj_man, world_man );
  
  // Feed detection one at a time and output to log file
  for( size_t i = 0; i < dets.size(); ++i ) {
    object_detection_t det = dets[i];
    std::cout << "det " << i << "  |  ";
    tset.update( det, world_man );

    draw_tracked_object_set( _lcmgl, tset, world_man );
    bot_lcmgl_switch_buffer( _lcmgl );

    
    // make sure we have onloy a single obkect
    if( tset.size() != 1 ) {
      std::cout << std::endl << "    **** MORE THAN ONE OBJECT" << std::endl;
    }
    
    // dump the belief and detection
    fout << det.get_detection_value() << " "
	 << det.get_dist_and_orientation( det.get_manifold_when_taken()).dist << " "
	 << det.get_dist_and_orientation( det.get_manifold_when_taken()).orientation.radians() << " "
	 << tset.object(0)->belief().mean << " "
	 << tset.object(0)->belief().var << std::endl;
    std::cout << "det:" << det.get_detection_value() << " "
	      << "dao:" << det.get_dist_and_orientation( det.get_manifold_when_taken()).dist << " "
	      << det.get_dist_and_orientation( det.get_manifold_when_taken()).orientation.radians() << " "
	      << "bel:" << tset.object(0)->belief().mean << " "
	      << tset.object(0)->belief().var << std::endl;
    
  }

  lcm_destroy( _lcm );
  bot_lcmgl_destroy( _lcmgl );
}


int main( int argc, char** argv )
{
  test_model();
  return 0;
}
