
#include <ptp-viewpoint-planner/random_planner_t.hpp>
#include <ptp-common-test/test.hpp>


using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::planner;

//=====================================================================


//=====================================================================

int main( int argc, char** argv )
{

  // create a new LCM
  lcm_t *lcm = lcm_create(NULL);

  // Create a new viewpoint planner
  random_planner_t planner( lcm, 0.7 );
  
  // start the planner
  planner.start();

  // loop until done
  //while( planner.done() == false ) {
  while( planner.done() == false ) {
    lcm_handle( lcm );
  }

  // force system to exit
  lcm_destroy( lcm );
  exit( 0 );

  return 0;
}
