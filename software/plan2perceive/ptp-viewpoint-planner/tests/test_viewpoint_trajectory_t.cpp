
#include <ptp-viewpoint-planner/viewpoint_trajectory_t.hpp>
#include <ptp-common-coordinates/identity_manifold_t.hpp>
#include <ptp-common-test/test.hpp>
#include <iostream>


using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::detectors;
using namespace ptp::sensormodels;
using namespace ptp::planner;



//======================================================================

object_detection_t create_dummy_detection( const float& det,
					   const double loc[3],
					   const angle_t& orient,
					   const manifold_tp& man )
{
  // create a dummy lcm detection
  ptplcm_object_detection_t *p = new ptplcm_object_detection_t();
  p->detector_value = det;
  p->position_in_detector_frame[0] = loc[0];
  p->position_in_detector_frame[1] = loc[1];
  p->position_in_detector_frame[2] = loc[2];
  p->orientation_from_detector_plane = orient.from(angle_t::X_AXIS).radians();
  p->object_class = 0;
  
  object_detection_t d( *p, man );
  return d;
}


//======================================================================

int test_compile()
{
  start_of_test;

  // create robot manifold
  manifold_tp robot( new identity_manifold_t(  manifold_t::BASE_MANIFOLD ) );
  
  // create a tracked object set
  tracked_object_set_tp tset( new tracked_object_set_t( 1, deg( 45 ).from( angle_t::X_AXIS ) ) );

  // create a viewpoint trajectory
  viewpoint_trajectory_t traj;
  
  // add some detections to tset
  for( size_t i = 0; i < 10; ++i ) {
    double loc[] = { 3, 0, 0 };
    object_detection_t d = create_dummy_detection( 0.03, loc, deg(0).from( angle_t::X_AXIS ), robot );
    tset->update( d, robot );
  }

  // set the object set and manifold
  traj.set_tracked_object_set( tset );
  traj.set_robot_manifold( robot );

  // add some viewpoints
  traj.add_viewpoint( viewpoint_t( coordinate( 2,0,0 ).on(robot),
				   rad(0).from(angle_t::X_AXIS),
				   robot ) );
  traj.add_viewpoint( viewpoint_t( coordinate( 1,0,0 ).on(robot),
				   rad(0).from(angle_t::X_AXIS),
				   robot ) );
  traj.add_viewpoint( viewpoint_t( coordinate( 2.5,0,0 ).on(robot),
				   rad(0).from(angle_t::X_AXIS),
				   robot ) );

  // start cost calculation
  std::cout << "TEST CALCULATING COSTS STARTING" << std::endl;

  // calculate all costs
  std::cout << "est total cost: " << traj.estimated_total_cost() << std::endl;
  std::cout << "actual total cost: " << traj.actual_total_cost() << std::endl;

  end_of_test;
}

//======================================================================

int test_decision_ordering()
{
  start_of_test;

  // create robot manifold
  manifold_tp robot( new identity_manifold_t(  manifold_t::BASE_MANIFOLD ) );
  
  // create a tracked object set
  tracked_object_set_tp tset( new tracked_object_set_t( 1, deg( 45 ).from( angle_t::X_AXIS ) ) );

  // create two viewpoint trajectory
  viewpoint_trajectory_t traj0, traj1;
  
  // add some detections to tset
  for( size_t i = 0; i < 10; ++i ) {
    double loc[] = { 3, 0, 0 };
    object_detection_t d = create_dummy_detection( 5, loc, deg(0).from( angle_t::X_AXIS ), robot );
    tset->update( d, robot );
  }

  // set the object set and manifold
  traj0.set_tracked_object_set( tset );
  traj0.set_robot_manifold( robot );
  traj1.set_tracked_object_set( tset );
  traj1.set_robot_manifold( robot );


  // add some viewpoints
  traj0.add_viewpoint( viewpoint_t( coordinate( 2,0,0 ).on(robot),
				    rad(0).from(angle_t::X_AXIS),
				    robot ) );
  traj0.add_viewpoint( viewpoint_t( coordinate( 1,0,0 ).on(robot),
				    rad(0).from(angle_t::X_AXIS),
				    robot ) );
  traj0.add_viewpoint( viewpoint_t( coordinate( 2.5,0,0 ).on(robot),
				    rad(0).from(angle_t::X_AXIS),
				    robot ) );

  traj1.add_viewpoint( viewpoint_t( coordinate( -2,0,0 ).on(robot),
				    rad(0).from(angle_t::X_AXIS),
				    robot ) );
  traj1.add_viewpoint( viewpoint_t( coordinate( -1,0,0 ).on(robot),
				    rad(0).from(angle_t::X_AXIS),
				    robot ) );
  traj1.add_viewpoint( viewpoint_t( coordinate( -2.5,0,0 ).on(robot),
				    rad(0).from(angle_t::X_AXIS),
				    robot ) );


  // start cost calculation
  std::cout << "TEST CALCULATING COSTS STARTING" << std::endl;

  // calculate all costs
  std::cout << "T0 est total cost: " << traj0.estimated_total_cost() << std::endl;
  std::cout << "T0 actual total cost: " << traj0.actual_total_cost() << std::endl;
  std::cout << "T1 est total cost: " << traj1.estimated_total_cost() << std::endl;
  std::cout << "T1 actual total cost: " << traj1.actual_total_cost() << std::endl;



  end_of_test;
}

//======================================================================

int main( int argc, char** argv )
{
  run_test( test_compile() );
  run_test( test_decision_ordering() );

  return 0;
}

//======================================================================
