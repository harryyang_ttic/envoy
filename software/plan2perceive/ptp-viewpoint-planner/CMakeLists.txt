cmake_minimum_required(VERSION 2.6.0)

# pull in the pods macros. See cmake/pods.cmake for documentation
set(POD_NAME ptp-viewpoint-planner)
include(cmake/pods.cmake)

# automatically build LCM types.  This also defines a number of CMake
# variables, see cmake/lcmtypes.cmake for details
include(cmake/lcmtypes.cmake)
lcmtypes_build()

include_directories(${LCMTYPES_INCLUDE_DIRS})

set( CMAKE_C_FLAGS_DEBUG "-O0 -g3 -ggdb3" )
set( CMAKE_CXX_FLAGS_DEBUG "-O0 -g3 -ggdb3" )

add_subdirectory( src )
add_subdirectory( tests )