
#include "viewpoint_planner_t.hpp"
#include <bot_core/timestamp.h>
#include <ptp-common-coordinates/identity_manifold_t.hpp>
#include <er_lcmtypes/lcm_channel_names.h>
#include <bot_core/rand_util.h>
#include <bot_core/rotations.h>
#include <GL/gl.h>
#include <lcmtypes/erlcm_mission_control_msg_t.h>
#include <lcmtypes/ptplcm_object_candidate_list_t.h>
#include <lcmtypes/erlcm_speech_cmd_t.h>
#include <lcmtypes/erlcm_localize_reinitialize_cmd_t.h>
#include <sstream>



using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::detectors;
using namespace ptp::sensormodels;
using namespace ptp::planner;

//=====================================================================


void publish_mission_control(lcm_t *lc,int type){
    erlcm_mission_control_msg_t msg;
    msg.utime = bot_timestamp_now();
    msg.type = type;
    erlcm_mission_control_msg_t_publish(lc, MISSION_CONTROL_CHANNEL, &msg);
}


//=====================================================================

viewpoint_planner_t::viewpoint_planner_t( lcm_t* lcm )
  : _lcm( lcm ), _started( false ), _robot_frame_history( 10000 )
{
  init( lcm );
}

//=====================================================================

void viewpoint_planner_t::init( lcm_t* lcm )
{
  _done = false;
  _detection_set_counter = 0;
  _started = false;
  _may_replan = true;
  _lcm = lcm;
  _last_send_goal_timestamp = 0;
  _last_reached_goal_timestamp = 0;
  _going_towards_viewpoint = false;
  _world_manifold = manifold_tp(new identity_manifold_t(manifold_t::BASE_MANIFOLD));

  // creat teh empty tracked object set
  _tracked_objects = tracked_object_set_tp( new tracked_object_set_t(0.5, deg(90).from(angle_t::X_AXIS) ) );

  // initialize and hook into LCM
  initialize_lcm_hooks( lcm );
  
  // create the lcmgl
  _lcmgl = bot_lcmgl_init(_lcm, "ptp-viewpoint-planner");
  _sample_lcmgl = bot_lcmgl_init( _lcm, "ptp-viewpoint-planner-samples" );
  _field_lcmgl = bot_lcmgl_init( _lcm, "ptp-viewpoint-planner-field" );
  bot_lcmgl_switch_buffer(_lcmgl);
  bot_lcmgl_switch_buffer( _sample_lcmgl );
  bot_lcmgl_switch_buffer( _field_lcmgl );


  // initialize the detector information
  _stored_detector_info.position_in_robot_body_frame[0] = 0;
  _stored_detector_info.position_in_robot_body_frame[1] = 0;
  _stored_detector_info.position_in_robot_body_frame[2] = 0;
  _stored_detector_info.look_at_axis_in_robot_body_frame[0] = 1;
  _stored_detector_info.look_at_axis_in_robot_body_frame[1] = 0;
  _stored_detector_info.look_at_axis_in_robot_body_frame[2] = 0;
  _stored_detector_info.angle_about_look_at_axis = 0;
  _stored_detector_info.fov_left_radians = deg( 60).from(angle_t::X_AXIS).radians();
  _stored_detector_info.fov_right_radians = deg(-60).from(angle_t::X_AXIS).radians();
  _stored_detector_info.fov_top_radians = deg(-60).from(angle_t::X_AXIS).radians();
  _stored_detector_info.fov_bottom_radians = deg(-60).from(angle_t::X_AXIS).radians();
  _stored_detector_info.fov_min_distance = 0;
  _stored_detector_info.fov_max_distance = 9;


  // call derrived init
  subclass_init();
}

//=====================================================================

void viewpoint_planner_t::initialize_lcm_hooks( lcm_t * lcm )
{
  bot_core_pose_t_subscription_t* pose = bot_core_pose_t_subscribe( lcm, POSE_CHANNEL, &viewpoint_planner_t::handle_pose_t_wrapper, this );
  bot_core_pose_t_subscription_set_queue_capacity( pose, 1 );
  erlcm_navigator_goal_msg_t_subscribe( lcm, "PTP_PLANNER_GOAL", &viewpoint_planner_t::handle_ptp_goal_change_wrapper, this );
  ptplcm_object_detection_set_t_subscription_t *det = ptplcm_object_detection_set_t_subscribe( lcm, "DETECTOR_OUTPUT_CHANNEL", &viewpoint_planner_t::handle_detection_set_wrapper, this );
  ptplcm_object_detection_set_t_subscription_set_queue_capacity( det, 1 );
  erlcm_speech_cmd_t_subscription_t *reached = erlcm_speech_cmd_t_subscribe( lcm, "WAYPOINT_STATUS", &viewpoint_planner_t::handle_navigator_goal_reached_wrapper, this );
  erlcm_speech_cmd_t_subscription_set_queue_capacity( reached, 300 );
  ptplcm_object_initial_beliefs_t_subscribe( lcm, "PTP_INITIAL_BELIEFS", &viewpoint_planner_t::handle_initial_beliefs_wrapper, this );
}

//=====================================================================

void viewpoint_planner_t::start() 
{
  _started = true;
  std::cout << "Starting palnner" << std::endl;
}

//=====================================================================

void viewpoint_planner_t::stop()
{
  _started = false;
}

//=====================================================================

void viewpoint_planner_t::process_detection_set( const ptplcm_object_detection_set_t set )
{

  // check if we should skip this becuase we just sent a goal
  double skip_detections_after_sending_goal_time = 5 * 1000000;
  bool not_too_close_to_last_goal_sent = false;
  if( bot_timestamp_now() - _last_send_goal_timestamp > skip_detections_after_sending_goal_time ) {
    not_too_close_to_last_goal_sent = true;
  }

  // Check that the detection is after we arrived at goal
  bool wanted_detection = false;
  if( set.source_time > _last_reached_goal_timestamp ) {
    wanted_detection = true;
  }

  //std::cout << "detections: " << _started << " " << not_too_close_to_last_goal_sent << " " << _going_towards_viewpoint << " " << wanted_detection << "  {gs:" << _last_send_goal_timestamp << " rg:" << _last_reached_goal_timestamp << " ds:" << set.utime << " " << std::endl;

  // process detection set if we are started and it has been enough time 
  // since the last sent goal
  if( _started 
      && not_too_close_to_last_goal_sent 
      && _going_towards_viewpoint == false 
      && wanted_detection 
      && _current_goal ) {

    // get the robot manifold when the detector fired
    //boost::optional<object_frame_t> past_robot = _robot_frame_history.at( set.source_time );
    bool past_robot = true;

    if( past_robot ) {
      
      // increment the counter
      ++_detection_set_counter;
      
      // keep track of the updated objects with detection set
      std::vector<tracked_object_tp> updated_objects;
      
      // First process all the actual detections
      for( size_t i = 0; i < set.size; ++i ) {
	tracked_object_tp obj = process_detection( &(set.detections[i]));
	updated_objects.push_back( obj );
      }
      
      // Now process detections we did not get but where expecting
      //process_absent_detections( set, updated_objects, *past_robot );

      // update the perception field
      if( set.size > 0 ) {
	_tracked_objects->update_perception( _stored_detector_info, _world_manifold );
      }
	
      // draw the perception field
      _tracked_objects->draw_additive_perception_field( _field_lcmgl );
      bot_lcmgl_switch_buffer( _field_lcmgl );

      // dump new additive field if it changed
      if( set.size > 0 ) {
	std::ostringstream oss;
	oss << "additive_perception_field_" << _detection_set_counter << ".ssv";
	//_tracked_objects->dump_additive_perception_field_obj( oss.str() );
      }

      //std::cout << "processed " << set.size << " detections" << std::endl;
    }

    //  now replan if able
    if( may_replan() ) {
      stop_robot_motion();
      replan();
      start_robot_motion();
    }  

    // HACK:
    //_may_replan = true;

    // start the robot
    //start_robot_motion();
    
  }

  // draw the state
  draw_tracked_object_set();
  draw_goal();
  draw_planned_trajectory();
  draw_nonempty_detection_history();
  bot_lcmgl_switch_buffer(_lcmgl);

  // publish the state
  publish_object_set();

}

//=====================================================================

tracked_object_tp viewpoint_planner_t::process_detection( const ptplcm_object_detection_t* msg )
{

  //std::cout << "processing detection" << std::endl;
  
  // update the detector info
  _stored_detector_info = msg->detector_info;

  tracked_object_tp updated_object;

  if( _started ) {
    
    
    // create an object detection object
    object_detection_t det( *msg, _world_manifold );

    // draw while processing
    draw_processing_detection( det );
    
    // First update our belief
    updated_object = _tracked_objects->update( det, det.get_manifold_when_taken() );

    _nonempty_detections.push_back( det );
  }
  
 
  return updated_object;
  
}

//=====================================================================

void viewpoint_planner_t::process_absent_detections( const ptplcm_object_detection_set_t* set,
						     const std::vector<tracked_object_tp>& updated_objs )
{
  
  // // Get the list of objects that should have been seen but where not
  // std::vector<tracked_object_tp> objs;
  // for( size_t i = 0; i < _tracked_objects->size(); ++i ) {
  //   bool inside = _tracked_objects->object(i)->in_fov( &_stored_detector_info, past_robot.manifold );
  //   if( inside ) {
  //     bool already_found = false;
  //     for( size_t j = 0; j < updated_objs.size(); ++j ) {
  // 	if( _tracked_objects->object(i) == updated_objs[j] ) {
  // 	  already_found = true;
  // 	  break;
  // 	}
  //     }
  //     if( already_found == false ) {
  // 	objs.push_back( _tracked_objects->object(i) );
  //     }
  //   }
  // }

  // // Now create fake detection for each of these
  // for( size_t i = 0; i < objs.size(); ++i ) {
  //   tracked_object_tp obj = objs[i];
  //   // TODO : write this
  // }
}

//=====================================================================

bool viewpoint_planner_t::may_replan() const
{
  return 
    _started && 
    _may_replan && 
    _current_goal;
}

//=====================================================================

void viewpoint_planner_t::replan()
{
  
  if( may_replan() == false ) 
    return;

  std::cout << "replaning" << std::endl;

  stop_robot_motion();

  // clear previous sampling paths
  bot_lcmgl_switch_buffer( _sample_lcmgl );

  // Ok, here we sample some trajectories, evaluate their cost
  // and then choose the minimum cost traj
  
  // sample a set of trajectories
  int64_t max_sampling_useconds = 0.4 * 1000000;
  std::vector<viewpoint_trajectory_tp> trajs;
  trajs.reserve( 1000 );

  // only sample if we have objects
  if( _tracked_objects->size() > 0 ) {
    int64_t start_time = bot_timestamp_now();
    while( bot_timestamp_now() - start_time < max_sampling_useconds ) {
      trajs.push_back( sample_trajectory() );
      draw_sampled_trajectory( trajs[ trajs.size() - 1] );
      bot_lcmgl_switch_buffer( _sample_lcmgl );
    }
    
    
  }
    
  // print out number of trajs sampled
  std::cout << "   sampled " << trajs.size() << " trajectories" << std::endl;

  // add the nominal trajecotry
  trajs.push_back( create_nominal_trajecory() );
  draw_sampled_trajectory( trajs[ trajs.size() - 1] );
  std::cout << "  cost nominal = " << trajs[ trajs.size() - 1 ]->estimated_total_cost() << std::endl;

  // Now pick the trajectory with the smallest const
  double min_cost = std::numeric_limits<double>::infinity();
  int min_idx = -1;
  int idx = 0;
  for( std::vector<viewpoint_trajectory_tp>::const_iterator citer = trajs.begin();
       citer != trajs.end();
       ++citer, ++idx ) {
    double c = (*citer)->estimated_total_cost();
    if( c < min_cost ) {
      min_cost = c;
      min_idx = idx;
      std::cout << "    min cost [" << idx << "] = " << c << std::endl;
    }
  }

  // set the new trakecotyr and send as goal
  if( min_idx >= 0 ) {
    start_executing_trajectory( trajs[ min_idx ] );
  }

  // Ok, here we say that we wich to replan on the next detection
  // because we have chosen the nominal trajectory
  if( min_idx == trajs.size() - 1 ) {
    _may_replan = true;
  }

  // clear the sampling paths
  bot_lcmgl_switch_buffer( _sample_lcmgl );

  
}

//=====================================================================

viewpoint_trajectory_tp viewpoint_planner_t::sample_trajectory() const
{
  // Ok, pick a length for trajectory
  int traj_size = 3 + bot_irand( 7 );
  traj_size = 3 + bot_irand( 3 );
  //traj_size = 3;

  // creat the traj and set it up
  viewpoint_trajectory_tp traj( new viewpoint_trajectory_t() );
  traj->set_tracked_object_set( _tracked_objects );
  traj->set_robot_manifold( _current_robot_frame.manifold );
  
  // Ok, now for each viewpoint ask the tracked object set
  // for a sample using the additive perception fields
  tracked_object_set_tp tset = _tracked_objects;
  for( size_t i = 0; i < traj_size; ++i ) {
    
    viewpoint_t view = tset->sample_viewpoint( _world_manifold );
    
    traj->add_viewpoint( view );

    // THIS IS CAUSING THE GPs to become NaN
    // // Now, create copy of tracked object set and update with 
    // // mean detection from viewpoint
    // tset = tset->clone();
    // std::vector<tracked_object_tp> objs = tset->objects_in_fov( _stored_detector_info, view.viewpoint_manifold() );
    // for( size_t i = 0; i < objs.size(); ++i ) {
    //   detection_and_location_t dx = view.create_mean_detection_from_viewpoint( objs[i], tset->sensor_model( objs[i]->object_id() ), _world_manifold );
    //   object_detection_t det = create_object_detection( dx, objs[i], view.viewpoint_manifold() );
    //   tset->update( det, view.viewpoint_manifold() );
    // }


  }

  // add the last goal viewpoint
  traj->add_viewpoint( goal_viewpoint() );

  return traj;
  
}

//=====================================================================

viewpoint_trajectory_tp viewpoint_planner_t::create_nominal_trajecory() const
{
  // create tyraj and set it up
  viewpoint_trajectory_tp traj( new viewpoint_trajectory_t() );
  traj->set_tracked_object_set( _tracked_objects );
  traj->set_robot_manifold( _current_robot_frame.manifold );
  
  // the nominal trajectory has a single viewpoint: the goal
  traj->add_viewpoint( goal_viewpoint() );
  
  return traj;
}

//=====================================================================

viewpoint_t viewpoint_planner_t::goal_viewpoint() const
{
  // ensure we have a goal
  if( !_current_goal ) {
    BOOST_THROW_EXCEPTION( exception_base() );
  }

  // returns the last viewpoint which is the goal
  viewpoint_t view( coordinate(0,0,0).on( (*_current_goal).manifold).on( _world_manifold ),
		    rad(0).from(angle_t::X_AXIS),
		    _world_manifold );
  return view;
}

//=====================================================================

void viewpoint_planner_t::start_executing_trajectory( const viewpoint_trajectory_tp& traj ) 
{

  // reset going towards viewpoint ( will set below
  _going_towards_viewpoint = false;

  // Start the robot motion
  //start_robot_motion();
  
  // set this as the current trajectory
  _latest_planned_viewpoints = traj;

  // Get the corodinate of teh first viewpoint
  coordinate_t coord = coordinate( 0,0,0 ).on( traj->viewpoint(0).viewpoint_manifold() ).on( _world_manifold );

  // // Keep moving down the viewpoint list if they are too clsoe
  // dist_and_orientation_t dao = calculate_dist_and_orientation( _current_robot_frame.manifold, traj->viewpoint(0).viewpoint_manifold() );
  int idx = 0;
  // while( dao.dist < 1 && traj->size() > idx+1) {
  //   ++idx;
  //   dao = calculate_dist_and_orientation( _current_robot_frame.manifold, traj->viewpoint(idx).viewpoint_manifold() );
  //   coord = coordinate( 0,0,0 ).on( traj->viewpoint(idx).viewpoint_manifold() ).on( _world_manifold );
  //   std::cout << "moving goal along trajectory" << std::endl;
  // }

  // debug
  std::cout << "Sending goal: " << coord << "   ";
  
  _last_send_viewpoint_index = idx;


  dist_and_orientation_t ddd = calculate_dist_and_orientation( traj->viewpoint(idx).viewpoint_manifold(), _world_manifold );
  double viewpoint_angle = ddd.orientation.radians();

  _last_send_goal_timestamp = bot_timestamp_now();

 // Ok, now if the next goal is too close to our ciurrnet location, 
  // say that we are at it
  dist_and_orientation_t dao = calculate_dist_and_orientation( _current_robot_frame.manifold, traj->viewpoint(idx).viewpoint_manifold() );
  if( dao.dist < 1 ) {
   

    _may_replan = true; // since this is a faked goal
    _going_towards_viewpoint = false;
 
    // fake a goal reached message
    erlcm_speech_cmd_t msg;
    msg.utime = bot_timestamp_now();
    msg.cmd_property = new char[100];
    strcpy( msg.cmd_property, "REACHED" );
    handle_navigator_goal_reached( &msg );
    delete[] msg.cmd_property;

    std::cout << "FAKING goal reached  ";

  }
  else {
  
    // send the first viewpoint as the goal for the navigator
    erlcm_navigator_goal_msg_t msg;
    msg.utime = bot_timestamp_now();
    msg.goal.x = coord[0];
    msg.goal.y = coord[1];
    msg.goal.yaw = viewpoint_angle;
    msg.velocity = 0.07;
    msg.nonce = msg.utime;
    msg.sender = ERLCM_NAVIGATOR_GOAL_MSG_T_SENDER_USAR;
    erlcm_navigator_goal_msg_t_publish( _lcm, NAV_GOAL_CHANNEL, &msg );
    
    // we may *not* replan until we reach the goal (unless we are at end )
    if( idx != traj->size()-1 ) {
      _may_replan = false;
      _going_towards_viewpoint = true;
    } else {
      _may_replan = true;
      _going_towards_viewpoint = false;
    }
    
  }

  // draw the chosen trajecotry
  draw_planned_trajectory();

  std::cout << "chose planned trajectory" << std::endl;

}

//=====================================================================

void viewpoint_planner_t::handle_pose_t_wrapper
(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
 const char * channel __attribute__((unused)), 
 const bot_core_pose_t * msg, 
 void * user)
{
  viewpoint_planner_t* self = static_cast<viewpoint_planner_t*>( user );
  self->handle_pose_t( msg );
}

//=====================================================================

void viewpoint_planner_t::handle_pose_t( const bot_core_pose_t* msg )
{
  // Ok, get the orietnataion as a roll/pitch/ywa
  double rpy[3];
  bot_quat_to_roll_pitch_yaw( msg->orientation, rpy );
  
  // Now, create a manifold for the robot on the world ad the
  // given location, with x axis pointing foward (along yaw )
  manifold_tp robot_man = create_object_manifold( coordinate( msg->pos[0],
							      msg->pos[1],
							      msg->pos[2] ).on( _world_manifold ),
						  rad(rpy[2]).from(angle_t::X_AXIS),
						  _world_manifold );
  
  // Create an object frame and add it to the history
  // as well as setting the current robot frame
  object_frame_t frame( msg->utime, robot_man );
  _current_robot_frame = frame;
  _robot_frame_history.add( frame );

  //std::cout << ".";
  //std::cout.flush();
}

//=====================================================================

void viewpoint_planner_t::handle_ptp_goal_change_wrapper
(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
 const char * channel __attribute__((unused)), 
 const erlcm_navigator_goal_msg_t * msg, 
 void * user)
{
  viewpoint_planner_t* self = static_cast<viewpoint_planner_t*>( user );
  self->handle_ptp_goal_change( msg );
}

//=====================================================================

void viewpoint_planner_t::handle_ptp_goal_change( const erlcm_navigator_goal_msg_t* msg )
{
  // Create a manifold for the goal
  manifold_tp goal_man = create_object_manifold( coordinate( msg->goal.x,
							     msg->goal.y,
							     0 ).on( _world_manifold  ),
						 rad(0).from(angle_t::X_AXIS),
						 _world_manifold );
  // create a new frame
  object_frame_t frame( msg->utime, goal_man );
  
  _current_goal = frame;
  _may_replan = true;

  draw_goal();

  std::cout << "got goal" << std::endl;
}

//=====================================================================

void viewpoint_planner_t::handle_navigator_goal_reached_wrapper
(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
 const char * channel __attribute__((unused)), 
 const erlcm_speech_cmd_t * msg, 
 void * user)
{
  viewpoint_planner_t* self = static_cast<viewpoint_planner_t*>( user );
  self->handle_navigator_goal_reached( msg );
}


//=====================================================================

void viewpoint_planner_t::handle_navigator_goal_reached( const erlcm_speech_cmd_t* msg )
{
  // Only care about REACHED events
  if( strcmp( msg->cmd_property, "REACHED" ) != 0 ) 
    return;


  // Ok, check if it is the current goal, and if so then we are done
  if( _current_goal ) {

    // SIMULATION HACK
    // TLEPORT to the desired goal since the 
    // RRTSTAR/Navigator do not get us htere
    if( _last_send_goal_timestamp > 0 ) {
      coordinate_t c = coordinate( 0,0,0 ).on( _latest_planned_viewpoints->viewpoint(_last_send_viewpoint_index).viewpoint_manifold() ).on( _world_manifold );
      dist_and_orientation_t dao = calculate_dist_and_orientation( _latest_planned_viewpoints->viewpoint(_last_send_viewpoint_index).viewpoint_manifold(),
								   _world_manifold);
      erlcm_localize_reinitialize_cmd_t msg;
      msg.utime = bot_timestamp_now();
      msg.mean[0] = c[0];
      msg.mean[1] = c[1];
      msg.mean[2] = dao.orientation.radians();
      msg.variance[0] = 0.0001;
      msg.variance[1] = 0.0001;
      msg.variance[2] = 0.0001;
      erlcm_localize_reinitialize_cmd_t_publish( _lcm, LOCALIZE_REINITIALIZE_CHANNEL, &msg );
    }


    dist_and_orientation_t dao = calculate_dist_and_orientation( (*_current_goal).manifold, _current_robot_frame.manifold );
    if( dao.dist < 1 ) {
      _current_goal = boost::optional<object_frame_t>();
      _may_replan = true;
      
      std::cout << std::endl << "**** DONE****" << std::endl;
      _done = true;
      _going_towards_viewpoint = false;
    }
  }

  // Check if it is one of our current viewpoints
  // HACK: assume that we are the only one's sending goals!
  _may_replan = true;
  
  // we are no longer going to a viewpoint
  _going_towards_viewpoint = false;
  _last_reached_goal_timestamp = msg->utime;

  std::cout << "reached viewpoint goal" << std::endl;

  // we do not immediatly replan to allow for an observation at the goal.
}

//=====================================================================

void viewpoint_planner_t::handle_detection_set_wrapper
(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
 const char * channel __attribute__((unused)), 
 const ptplcm_object_detection_set_t * msg, 
 void * user)

{
  viewpoint_planner_t* self = static_cast<viewpoint_planner_t*>( user );
  self->handle_detection_set( msg );
}

//=====================================================================

void viewpoint_planner_t::handle_detection_set( const ptplcm_object_detection_set_t* msg )
{
  ptplcm_object_detection_set_t *copy = ptplcm_object_detection_set_t_copy( msg );
  process_detection_set( *copy );
  ptplcm_object_detection_set_t_destroy( copy );
}

//=====================================================================

void viewpoint_planner_t::handle_initial_beliefs_wrapper
(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
 const char * channel __attribute__((unused)), 
 const ptplcm_object_initial_beliefs_t * msg, 
 void * user)

{
  viewpoint_planner_t* self = static_cast<viewpoint_planner_t*>( user );
  self->handle_initial_beliefs( msg );
}

//=====================================================================

void viewpoint_planner_t::handle_initial_beliefs( const ptplcm_object_initial_beliefs_t* msg )
{
  _tracked_objects->add_initial_beliefs( msg, _world_manifold );
  _tracked_objects->update_perception( _stored_detector_info, _world_manifold );
}

//=====================================================================


// Description:
// Draws the given sampled trajecoty.
// This will NOT have cost information drawn on it
void viewpoint_planner_t::draw_sampled_trajectory( const viewpoint_trajectory_tp& traj ) const
{
  bot_lcmgl_color3f( _sample_lcmgl, 1, 1, 0 );
  bot_lcmgl_line_width( _sample_lcmgl, 1.0 );

  // draw the trajectory lines
  bot_lcmgl_begin( _sample_lcmgl, GL_LINE_LOOP );
  for( size_t i = 0; i < traj->size(); ++i ) {
    viewpoint_t vp = traj->viewpoint( i );
    coordinate_t c = coordinate( 0,0,0 ).on( vp.viewpoint_manifold() ).on( _world_manifold );
    bot_lcmgl_vertex2f( _sample_lcmgl, c[0], c[1] );
  }
  bot_lcmgl_end( _sample_lcmgl );

  // Now draw the viewpoint locations as dots
  float vp_radius = 0.15;
  for( size_t i = 0; i < traj->size(); ++i ) {
    viewpoint_t vp = traj->viewpoint(i);
    coordinate_t c = coordinate( 0,0,0 ).on( vp.viewpoint_manifold() ).on( _world_manifold );
    dist_and_orientation_t dao = calculate_dist_and_orientation( vp.viewpoint_manifold(), _world_manifold );
    double p[3] = { c[0], c[1], 0 };
    bot_lcmgl_circle( _sample_lcmgl, p, vp_radius );
    bot_lcmgl_begin( _sample_lcmgl, GL_LINES );
    bot_lcmgl_vertex2f( _sample_lcmgl, c[0], c[1] );
    bot_lcmgl_vertex2f( _sample_lcmgl, 
			c[0] + vp_radius * cos( dao.orientation.radians() ),
			c[1] + vp_radius * sin( dao.orientation.radians() ) );
    bot_lcmgl_end( _sample_lcmgl );
  }

  bot_lcmgl_line_width( _sample_lcmgl, 1.0 );
}

//=====================================================================
      
// Description:
// Draw the current goal
void viewpoint_planner_t::draw_goal() const
{
  if( !_current_goal )
    return;
  bot_lcmgl_color3f( _lcmgl, 1, 1, 0 );
  coordinate_t c = coordinate( 0,0,0 ).on( (*_current_goal).manifold ).on( _world_manifold );
  float goal_radius = 0.5;
  double p[3] = { c[0], c[1], 0 };
  bot_lcmgl_circle( _lcmgl, p, goal_radius );
  double p2[2] = { goal_radius, goal_radius };
  bot_lcmgl_rect( _lcmgl, p, p2, 1 );
}

//=====================================================================

// Description:
// Draw the planned and chosen trajecotry
void viewpoint_planner_t::draw_planned_trajectory() const
{
  if( !_latest_planned_viewpoints  )
    return;

  bot_lcmgl_color3f( _lcmgl, 1, 1, 0 );
  bot_lcmgl_line_width( _lcmgl, 2.0 );

  // draw the trajectory lines
  bot_lcmgl_begin( _lcmgl, GL_LINE_LOOP );
  for( size_t i = 0; i < _latest_planned_viewpoints->size(); ++i ) {
    viewpoint_t vp = _latest_planned_viewpoints->viewpoint( i );
    coordinate_t c = coordinate( 0,0,0 ).on( vp.viewpoint_manifold() ).on( _world_manifold );
    bot_lcmgl_vertex2f( _lcmgl, c[0], c[1] );
  }
  bot_lcmgl_end( _lcmgl );

  // Now draw the viewpoint locations as dots
  float vp_radius = 0.25;
  for( size_t i = 0; i < _latest_planned_viewpoints->size(); ++i ) {
    viewpoint_t vp = _latest_planned_viewpoints->viewpoint(i);
    coordinate_t c = coordinate( 0,0,0 ).on( vp.viewpoint_manifold() ).on( _world_manifold );
    dist_and_orientation_t dao = calculate_dist_and_orientation( vp.viewpoint_manifold(), _world_manifold );
    double p[3] = { c[0], c[1], 0 };
    bot_lcmgl_circle( _lcmgl, p, vp_radius );
    bot_lcmgl_begin( _lcmgl, GL_LINES );
    bot_lcmgl_vertex2f( _lcmgl, c[0], c[1] );
    bot_lcmgl_vertex2f( _lcmgl, 
			c[0] + vp_radius * cos( dao.orientation.radians() ),
			c[1] + vp_radius * sin( dao.orientation.radians() ) );
    bot_lcmgl_end( _lcmgl );
  }

  // reset line width
  bot_lcmgl_line_width( _lcmgl, 1.0 );
}

//=====================================================================
      
// Description:
// Draw a detection that is being processed
void viewpoint_planner_t::draw_processing_detection( const object_detection_t& det ) const
{
  bot_lcmgl_color3f( _lcmgl, 1, 0, 1 );
  bot_lcmgl_begin( _lcmgl, GL_LINES );
  coordinate_t c0 = coordinate( 0,0,0 ).on( det.get_manifold_when_taken() ).on( _world_manifold );
  coordinate_t c1 = coordinate( 0,0,0 ).on( det.get_object_manifold() ).on( _world_manifold );
  bot_lcmgl_vertex2f( _lcmgl, c0[0], c0[1] );
  bot_lcmgl_vertex2f( _lcmgl, c1[0], c1[1] );
  bot_lcmgl_end( _lcmgl );
}

//=====================================================================

// Description:
// Draw the state of the current tracked oibject set
void viewpoint_planner_t::draw_tracked_object_set() const
{
  for( size_t i = 0; i < _tracked_objects->size(); ++i ) {
    
    // choose color based on mean belief
    mean_variance_t bel = _tracked_objects->object(i)->belief();
    float norm_mean = bel.mean;
    if( norm_mean < 0 )
      norm_mean = 0;
    if( norm_mean > 1 )
      norm_mean = 1;
    if( norm_mean < 0.5 ) {
      bot_lcmgl_color3f( _lcmgl, 0.75 - norm_mean, 0, 0 );
    } else if( norm_mean < 0.75 ) {
      bot_lcmgl_color3f( _lcmgl, 0, 0, 1 - 2 * (0.75 - norm_mean) );
    } else {
      bot_lcmgl_color3f( _lcmgl, 0, 1 - ( 1 - norm_mean ), 0 );
    }

    // grab the object center
    coordinate_t c = coordinate( 0,0,0 ).on( _tracked_objects->object(i)->object_manifold() ).on( _world_manifold );

    // draw a circle where we are tracking objects
    float radius = 0.5;
    double p[3] = { c[0], c[1], 0 };
    bot_lcmgl_disk( _lcmgl, p, 0, radius );

    // Ok, now draw an inner ring with whetehr we are above 0.5 or not
    float ring_radius = 0.3;
    if( bel.mean > 0.5 )
      bot_lcmgl_color3f( _lcmgl, 0, 1, 0 );
    else 
      bot_lcmgl_color3f( _lcmgl, 1, 0, 0 );
    bot_lcmgl_disk( _lcmgl, p, radius, radius + ring_radius );
    

    // draw an orientation line
    bot_lcmgl_color3f( _lcmgl, 1, 1, 1 );
    dist_and_orientation_t dao = calculate_dist_and_orientation( _tracked_objects->object(i)->object_manifold(), _world_manifold );
    bot_lcmgl_begin( _lcmgl, GL_LINES );
    bot_lcmgl_vertex2f( _lcmgl, c[0], c[1] );
    bot_lcmgl_vertex2f( _lcmgl, 
			c[0] + radius * cos( dao.orientation.radians() ),
			c[1] + radius * sin( dao.orientation.radians() ) );
    bot_lcmgl_end( _lcmgl );

    // draw a square around it if it is ignored
    // bool ignored = false;
    // for( size_t k = 0; k < _tracked_objects->_ignored_objects.size(); ++k ) {
    //   if( _tracked_objects->_ignored_objects[k] == i ) {
    // 	ignored = true;
    // 	break;
    //   }
    // }
    // if( ignored ) {
    //   double sz[2] = { 2.2 * radius, 2.2 * radius };
    //   bot_lcmgl_rect( _lcmgl, p, sz, 0 );
    // }

    // Draw the number of times it has been updated (aka. seen )
    std::ostringstream oss;
    p[0] += 0.7;
    p[1] += 0.7;
    oss << _tracked_objects->object(i)->_belief_update_count;
    bot_lcmgl_color3f( _lcmgl, 255.0/255.0, 179.0/255.0, 102.0/255.0 );
    bot_lcmgl_text( _lcmgl, p, oss.str().c_str() );

    // Draw the belief
    p[1] -= 0.7;
    p[1] -= 1.5;
    oss.str("");
    oss << "N(" << _tracked_objects->object(i)->belief().mean << ","
	<< _tracked_objects->object(i)->belief().var << ")";
    bot_lcmgl_color3f( _lcmgl, 255.0/255.0, 179.0/255.0, 102.0/255.0 );
    bot_lcmgl_text( _lcmgl, p, oss.str().c_str() );

  } 
}

//=====================================================================

void viewpoint_planner_t::draw_nonempty_detection_history() const
{
  for( size_t i = 0; i < _nonempty_detections.size(); ++i ) {
    bot_lcmgl_color3f( _lcmgl, 1,0,1 );
    coordinate_t c = coordinate( 0,0,0 ).on( _nonempty_detections[i].get_manifold_when_taken() ).on( _world_manifold );
    double p[3] = { c[0], c[1], 0 };
    double radius = 0.15;
    bot_lcmgl_disk( _lcmgl, p, 0, radius );
    
    bot_lcmgl_color3f( _lcmgl, 1, 1, 1 );
    dist_and_orientation_t dao = calculate_dist_and_orientation( _nonempty_detections[i].get_manifold_when_taken(), _world_manifold );
    bot_lcmgl_begin( _lcmgl, GL_LINES );
    bot_lcmgl_vertex2f( _lcmgl, c[0], c[1] );
    bot_lcmgl_vertex2f( _lcmgl, 
			c[0] + radius * cos( dao.orientation.radians() ),
			c[1] + radius * sin( dao.orientation.radians() ) );
    bot_lcmgl_end( _lcmgl );

  }
}

//=====================================================================

void viewpoint_planner_t::stop_robot_motion() const
{
  publish_mission_control(_lcm, ERLCM_MISSION_CONTROL_MSG_T_NAVIGATOR_PAUSE);
  erlcm_speech_cmd_t msg;
  msg.cmd_type = "FOLLOWER";
  msg.cmd_property = "STOP";
  erlcm_speech_cmd_t_publish(_lcm, "WAYPOINT_NAVIGATOR", &msg);
}

//=====================================================================

void viewpoint_planner_t::start_robot_motion() const
{
  publish_mission_control(_lcm, ERLCM_MISSION_CONTROL_MSG_T_NAVIGATOR_GO);
  erlcm_speech_cmd_t msg;
  msg.cmd_type = "FOLLOWER";
  msg.cmd_property = "GO";
  erlcm_speech_cmd_t_publish(_lcm, "WAYPOINT_NAVIGATOR", &msg);  
}

//=====================================================================

void viewpoint_planner_t::publish_object_set() const
{
  ptplcm_object_candidate_list_t msg;
  msg.utime = bot_timestamp_now();
  msg.num_objects = _tracked_objects->size();
  msg.obj_x = (double*) malloc(msg.num_objects * sizeof(double));
  msg.obj_y = (double*) malloc(msg.num_objects * sizeof(double));
  msg.obj_theta = (double*) malloc(msg.num_objects * sizeof(double));
  msg.obj_prob = (double*) malloc(msg.num_objects * sizeof(double));
  msg.obj_var = (double*) malloc(msg.num_objects * sizeof(double));
  msg.obj_class = (int16_t*) malloc(msg.num_objects * sizeof(int16_t));
  msg.declare_positive = (int8_t*) malloc(msg.num_objects);
  msg.true_positive_cost = (double*) malloc(msg.num_objects * sizeof(double));
  msg.false_positive_cost = (double*) malloc(msg.num_objects * sizeof(double));

  for(int i=0; i<msg.num_objects; i++) {
    //door_candidate_t* cand = g_ptr_array_index(door_candidates, i);
    tracked_object_tp obj = _tracked_objects->object(i);
    coordinate_t c = coordinate( 0,0,0 ).on( obj->object_manifold() ).on( _world_manifold );
    msg.obj_x[i] = c[0];
    msg.obj_y[i] = c[1];
    msg.obj_theta[i] = 0;
    msg.obj_prob[i] = obj->belief().mean;
    msg.obj_var[i] = obj->belief().var;
    msg.obj_class[i] = obj->object_type();
    msg.declare_positive[i] = ( obj->belief().mean > 0.75 );
    msg.true_positive_cost[i] = OBJECT_TRUE_POSITIVE_COST[obj->object_type()] * OBJECT_COST_MULTIPLIER[obj->object_type()];
    msg.false_positive_cost[i] = OBJECT_FALSE_POSITIVE_COST[obj->object_type()] * OBJECT_COST_MULTIPLIER[obj->object_type()];

  }
  ptplcm_object_candidate_list_t_publish(_lcm, "OBJECT_CANDIDATES", &msg);

  free(msg.obj_x);
  free(msg.obj_y);
  free(msg.obj_theta);
  free(msg.obj_prob);
  free(msg.obj_var);
  free(msg.obj_class);
  free(msg.true_positive_cost);
  free(msg.false_positive_cost);
  free(msg.declare_positive);

}

//=====================================================================

object_detection_t viewpoint_planner_t::create_object_detection( const detection_and_location_t& dx, const tracked_object_tp& obj, const manifold_tp& view_man ) const
{
  coordinate_t oc = coordinate( 0,0,0 ).on( obj->object_manifold() ).on( view_man );
  ptplcm_object_detection_t msg;
  msg.object_class = obj->object_type();
  msg.detector_value = dx.detection_value;
  msg.position_in_detector_frame[0] = oc[0];
  msg.position_in_detector_frame[1] = oc[1];
  msg.position_in_detector_frame[2] = oc[2];
  msg.distance_from_detector = dx.location.dist;
  msg.orientation_from_detector_plane = dx.location.orientation.radians();
  msg.detector_info = _stored_detector_info;

  object_detection_t det( msg, view_man );
  
  return det;
}

//=====================================================================
