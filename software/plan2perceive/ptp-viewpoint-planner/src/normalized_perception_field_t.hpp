
#if !defined( __PTP_PLANNER_normalized_perception_field_t_HPP__ )
#define __PTP_PLANNER_normalized_perception_field_t_HPP__


#include <ptp-sensor-models/sensor_model_t.hpp>
#include <ptp-common/matrix.hpp>
#include <ptp-object-detectors/common.hpp>

namespace ptp {
  namespace planner {

    using namespace ptp::sensormodels;
    using namespace ptp::detectors;
    using namespace ptp::common;

    //=================================================================

    // Description:
    // A perception field which has been normalizsed to go from
    // (0,1) range.  This is an approximation of the true perception
    // field by taking discrete measurement.
    class normalized_perception_field_t
    {
    public:

      // Description:
      // Creates an empty normalized perception field with given
      // discretization paremters
      normalized_perception_field_t( const float& min_dist,
				     const float& max_dist,
				     const size_t& num_dist,
				     const float& min_angle,
				     const float& max_angle,
				     const size_t& num_angle );

      // Description:
      // Update the normalized perception field using 
      // the given sensor model
      void update( const sensor_model_tp& model,
		   const mean_variance_t& belief );

      // Description:
      // Returns the value at the given location
      double norm_field_value_at( const dist_and_orientation_t& x,
				  const mean_variance_t& belief ) const;

      // Description:
      // Returns true iff the given dist_angle is within this
      // field
      bool is_within_field( const dist_and_orientation_t& x ) const
      { int d0,d1;
	return compute_sample_row_col( x, d0, d1 );
      }

      // Description:
      // returns a random dist/angle within this field
      dist_and_orientation_t sample_dao_within_field() const;

      // Description:
      // Dumps the nromalized perception field onto the given filename
      void dump( const std::string& filename ) const;


    protected:


      // Description:
      // Given a diistance and angle, returns the row/column of
      // our sample matrix for it.
      // Returns true iff the given dist/angle has a valid
      // field value and is inside the range of our field.
      bool compute_sample_row_col( const dist_and_orientation_t& x,
				   int& row, int& col ) const;

      
      // Description:
      // The field extents
      float _min_dist;
      float _max_dist;
      float _min_angle;
      float _max_angle;
      size_t _num_dist;
      size_t _num_angle;
      
      // Description:
      // The field samples, normalized
      // Where rows are distance, and columns are angle
      matrix_t _sampled_field;
    };

    // Description:
    // Shared pointer typedef
    typedef boost::shared_ptr<normalized_perception_field_t> normalized_perception_field_tp;

    //=================================================================

  }
}

#endif

