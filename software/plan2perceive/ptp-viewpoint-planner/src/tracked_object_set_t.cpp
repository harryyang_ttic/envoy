
#include "tracked_object_set_t.hpp"
#include "efkf.hpp"
#include <bot_core/rand_util.h>
#include <bot_core/tictoc.h>
#include <fstream>

using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::sensormodels;
using namespace ptp::detectors;
using namespace ptp::planner;


//==================================================================

tracked_object_set_t::tracked_object_set_t( const float& dist_thresh,
					    const angle_t& orientation_thresh,
					    const bool& track_belief )
  : _same_object_distance_threshold( dist_thresh ),
    _same_object_orientation_threshold( orientation_thresh ),
    _track_belief( track_belief ),
    _perception_num_x( 30 ), _perception_num_y( 30 )
{}

//==================================================================

void tracked_object_set_t::add_initial_beliefs( const ptplcm_object_initial_beliefs_t* msg, const manifold_tp& world_man  )
{

  // Ok, create a new tracked object for each initial belief object
  for( size_t i = 0; i < msg->size; ++i ) {
    
    // creat the object manifold
    manifold_tp obj_man = 
      create_object_manifold( coordinate( msg->object_poses[i].x,
					  msg->object_poses[i].y,
					  msg->object_poses[i].z ).on( world_man ),
			      rad(msg->object_poses[i].yaw).from(angle_t::X_AXIS),
			      world_man );
    
    // create a new sensor model for it		 
    dist_and_orientation_t dao( 20, rad(0).from(angle_t::X_AXIS));
    detection_and_location_t dx( 0, dao );
    int id = _sensor_models.add_new_object( msg->object_types[i],
					    dx );
    sensor_model_tp sensor = _sensor_models.object(id);

    // create a tracked object 
    mean_variance_t bel;
    bel.mean = msg->object_belief_means[i];
    bel.var = msg->object_belief_variances[i];
    tracked_object_tp obj( new tracked_object_t( bel,
						 sensor,
						 obj_man,
						 id,
						 msg->object_types[i] ) );
    obj->track_beliefs( _track_belief );
    _objects.push_back( obj );
  }

}

//==================================================================

tracked_object_tp tracked_object_set_t::update( const object_detection_t& det,
						const manifold_tp& robot )
{
  tracked_object_tp obj = find_or_create_object( det, robot );
  update_tracked_object( obj, det, robot );
  
  return obj;
}

//==================================================================

void tracked_object_set_t::update_perception( const ptplcm_detector_info_t& info,
					      const manifold_tp& ref ) 
{
  // find the boundary of the objects
  float min_x = std::numeric_limits<float>::infinity();
  float max_x = -std::numeric_limits<float>::infinity();
  float min_y = std::numeric_limits<float>::infinity();
  float max_y = -std::numeric_limits<float>::infinity();
  size_t num_x = _perception_num_x;
  size_t num_y = _perception_num_y;
  for( size_t i = 0; i < _objects.size(); ++i ) {
    coordinate_t c = coordinate( 0,0,0 ).on( _objects[i]->object_manifold() ).on( ref );
    if( c[0] < min_x )
      min_x = c[0];
    if( c[0] > max_x )
      max_x = c[0];
    if( c[1] < min_y )
      min_y = c[1];
    if( c[1] > max_y )
      max_y = c[1];
  }

  // expand bounds
  float expand_val = info.fov_max_distance;
  max_x += expand_val;
  min_x -= expand_val;
  max_y += expand_val;
  min_y -= expand_val;

  
  // update the field
  _field.update( _objects, 
		 min_x, max_x,
		 min_y, max_y,
		 num_x, num_y,
		 info,
		 ref );

  std::cout << "Updating field (" << min_x << "," << max_x << ") (" << min_y << "," << max_y << ") [" << num_x << "," << num_y << "]" << std::endl;
  
}

//==================================================================

tracked_object_tp tracked_object_set_t::find_or_create_object( const object_detection_t& det,
							       const manifold_tp& robot)
{

  //std::cout << "tset::find-or-create : robot=" << robot << " det.obj-man=" << *det.get_object_manifold() << " det.taken-man=" << *det.get_manifold_when_taken() << std::endl;

  // Get the detection.s distance and orientation in the robot's manifold
  //dist_and_orientation_t dao = det.get_dist_and_orientation( robot );
  
  // Get the location of object in given manifol
  coordinate_t oc = coordinate( 0,0,0 ).on( det.get_object_manifold() ).on( robot );
    
  
  // iterate over all known objects and see if any match
  // ( keep track of the closest in distance )
  float min_dist = std::numeric_limits<float>::infinity();
  tracked_object_tp found_object;
  for( size_t i = 0; i < _objects.size(); ++i ) {

    // find the object's distance and orientation in robot base
    //dist_and_orientation_t dao_i = _objects[i]->get_dist_and_orientation( robot );

    // find the coordinate of this object in the manifold
    coordinate_t c = coordinate( 0,0,0 ).on( _objects[i]->object_manifold() ).on( robot );

    // Find distance between locations
    float dist = basis_t( oc, c ).arc_length();
    
    if( dist < min_dist ) {
      min_dist = dist;
      if( min_dist < _same_object_distance_threshold /*&&
	  abs( dao.orientation.radians() - dao_i.orientation.radians() ) < _same_object_distance_threshold*/ ) {
	found_object = _objects[i];
      }
    }
  }
  
  // If we found object, return it else create one
  if( found_object ) {
    return found_object;
  } else {
    
    int id = _sensor_models.add_new_object( det.get_object_type(),
					     det.get_detection_and_location( robot ) );
    sensor_model_tp sensor = _sensor_models.object(id);
    mean_variance_t prior;
    prior.mean = 0.5;
    prior.var = 0.25;
    tracked_object_tp obj( new tracked_object_t( prior,
						 sensor,
						 det.get_object_manifold(),
						 id,
						 det.get_object_type() ) );
    obj->track_beliefs( _track_belief );
    _objects.push_back( obj );
    return obj;
  }
}

//==================================================================

void tracked_object_set_t::update_tracked_object( tracked_object_tp obj,
						  const object_detection_t& det,
						  const manifold_tp& robot )
{
  
  // // Ok, first get the likelihood of detection
  // probability_t pdet = _sensor_models->object( obj->object_id() )->p( det );

  // // compute teh variance of the detection according to sensor model
  // mean_variance_t mv = _sensor_models->object( obj->object_id() )->query_no_update( det.get_dist_and_orientation() );
  
  // mean_variance_t prior_mv = obj->belief();
  // double var2 = mv.var * mv.var;
  // double prior_var2 = prior_mv.var * prior_mv.var;
  // double mu = ( prior_mv.mean * var2 + prior_var2 * det.detection_value() ) / ( var2 + prior_var2 );
  // double var = 1.0 / ( 1.0/prior_var2 + 1.0/var2 );

  sensor_model_tp sensor = _sensor_models.object( obj->object_id() );
  mean_variance_t obs_lik;
  if( _use_independent_model_only ) {
    mean_variance_t corr_model;
    float p_indept;
    sensor->query_separate( det.get_dist_and_orientation( robot ),
			    obj->belief().mean,
			    obs_lik, corr_model, p_indept );
  } else {
    obs_lik = sensor->query_no_update( det.get_dist_and_orientation( robot ), obj->belief().mean );
  }
  detection_and_location_t det_and_loc = det.get_detection_and_location( robot );
  double mu = EFKF::update_mean_after_observation( obj->belief(),
						   obs_lik,
						   det_and_loc,
						   sensor,
						   _use_independent_model_only);
  double var = EFKF::update_var_after_observation( obj->belief(),
						   obs_lik,
						   det_and_loc.location,
						   sensor,
						   _use_independent_model_only);
			
  // update the sensor model
  sensor->update( det_and_loc );

  // update the belief
  mean_variance_t post_mv;
  post_mv.mean = mu;
  post_mv.var = var;
  if( post_mv.mean < 0 )
    post_mv.mean = 0.0001;
  if( post_mv.mean > 1 )
    post_mv.mean = 0.9999;
  obj->set_belief( post_mv, sensor );
}

//==================================================================

viewpoint_t tracked_object_set_t::greedy_best_viewpoint(const manifold_tp& ref, const int& obj ) const
{
  return _field.best_viewpoint( ref, obj );
}

//==================================================================

std::vector<viewpoint_t> tracked_object_set_t::nonzero_viewpoint_set( const manifold_tp& ref,
								      std::vector<int>& object_index ) const
{
  return _field.nonzero_viewpoint_set( ref, object_index  );
}

//==================================================================

viewpoint_t tracked_object_set_t::sample_viewpoint( const manifold_tp& ref ) const
{
  return _field.sample_viewpoint( ref );
}

// viewpoint_t tracked_object_set_t::sample_viewpoint( const manifold_tp& ref ) const
// {

//   bot_tictoc( "trakced-object-set::sample_viewpoint" );
  
//   // We are rejection sampling so we may need to loop
//   int max_iterations = 100;
//   for( int iteration = 0; iteration < max_iterations; ++iteration )  {
    
//     // Ok, first pick a random object
//     int obj = bot_irand( _objects.size() );
    
//     // Now get a dist-orientation using it's percetion fiueld
//     dist_and_orientation_t dao = _objects[obj]->norm_perception_field()->sample_dao_within_field();

//     // Ok, create the viewpoint manifold for this initial view
//     double o_rad = dao.orientation.radians();
//     coordinate_t sample_on_ref = coordinate( cos( o_rad ) * dao.dist,
// 					     sin( o_rad ) * dao.dist,
// 					     0 ).on( _objects[obj]->object_manifold() ).on( ref );
//     coordinate_t obj_on_ref = coordinate(0,0,0).on( _objects[obj]->object_manifold()).on( ref );
//     double view_angle = atan2( obj_on_ref[1] - sample_on_ref[1],
// 			       obj_on_ref[0] - sample_on_ref[0] );
//     manifold_tp vman = create_object_manifold( sample_on_ref, 
// 					       rad(view_angle).from(angle_t::X_AXIS) , 
// 					       ref );
    
//     // Ok, now compute the additive sum of all perception field for viewpoint
//     double additive_field = 0;
//     for( size_t i = 0; i < _objects.size(); ++i ) {

//       // compute dist_orint of viewpoint for the object
//       dist_and_orientation_t dao = calculate_dist_and_orientation_from_object( vman, _objects[i]->object_manifold() );
      
//       // Add teh perception field value for object
//       double local_field_val = _objects[i]->norm_perception_field()->norm_field_value_at( dao, _objects[i]->belief() );
//       additive_field += local_field_val;
//     }
    
//     // Now, rejection sample using the additive field
//     // Or return this view if we are at max iterations
//     double p = additive_field / _objects.size();
//     double ur = bot_randf();
//     if( ur < p || iteration == max_iterations - 1 ) {
      
//       // we win
//       bot_tictoc( "trakced-object-set::sample_viewpoint" );
//       return viewpoint_t( sample_on_ref,
// 			  rad(view_angle).from(angle_t::X_AXIS),
// 			  ref );
//     }
//   }

//   // Should never get here
//   bot_tictoc( "trakced-object-set::sample_viewpoint" );
//   BOOST_THROW_EXCEPTION( exception_base() );
//   return viewpoint_t( coordinate( 0,0,0 ).on( ref ),
// 		      rad(0).from(angle_t::X_AXIS),
// 		      ref );

    
// }


//==================================================================

void tracked_object_set_t::dump_norm_perception_fields(const std::string& prefix ) const
{
  for( size_t i = 0; i < _objects.size(); ++i ) {
    std::ostringstream oss;
    oss << prefix << i << ".ssv";
    _objects[i]->norm_perception_field()->dump( oss.str() );
  }
}


//==================================================================

void tracked_object_set_t::dump_additive_perception_field( const std::string& filename, const manifold_tp& ref  ) const
{
  // select a range of x,y values from the current objectset
  float min_x =  std::numeric_limits<float>::infinity();
  float max_x = -std::numeric_limits<float>::infinity();
  float min_y =  std::numeric_limits<float>::infinity();
  float max_y = -std::numeric_limits<float>::infinity();

  // find the boundary for field
  for( size_t i = 0; i < _objects.size(); ++i ) {
    coordinate_t c = coordinate( 0,0,0 ).on( _objects[i]->object_manifold() ).on( ref );
    if( c[0] < min_x ) {
      min_x = c[0];
    }
    if( c[0] > max_x ) {
      max_x = c[0];
    }
    if( c[1] < min_y ) {
      min_y = c[1];
    }
    if( c[1] > max_y ) {
      max_y = c[1];
    }
  }

  // expand the bounds by 3
  float expand_factor = 3;
  min_x -= expand_factor;
  min_y -= expand_factor;
  max_x += expand_factor;
  max_y += expand_factor;

  // make the bounds a square
  min_x = min_y = std::min( min_x, min_y );
  max_x = max_y = std::max( max_x, max_y );

  // print out the bounds
  std::cout << "dump-additive: bounds=" 
	    << min_x << "," << max_x << "  "
	    << min_y << "," << max_y << std::endl;

  // Now compute the additive field at each location
  size_t num_x = 200;
  size_t num_y = 200;
  float step_y = (max_y - min_y) / num_y;
  float step_x = (max_x - min_x) / num_x;
  matrix_t field( num_y, num_x );
  for( int i = 0; i < num_y; ++i ) {
    for( int j = 0; j < num_x; ++j ) {
      float x = min_x + j*step_x;
      float y = min_y + i*step_y;

      // creat teh view manifold which looks from center towards location
      float angle = atan2( y, x ) + M_PI;
      manifold_tp vman = create_object_manifold( coordinate( x, y, 0 ).on(ref),
						 rad(angle).from(angle_t::X_AXIS),
						 ref );
      
      // Now sum the field over all object for view
      double additive_field = 0;
      for( size_t obj = 0; obj < _objects.size(); ++obj ) {
	
	// compute dist_orint of viewpoint for the object
	dist_and_orientation_t dao = calculate_dist_and_orientation_from_object( vman, _objects[obj]->object_manifold() );
	
	// Add teh perception field value for object
	double local_field_val = _objects[obj]->norm_perception_field()->norm_field_value_at( dao, _objects[obj]->belief() );
	additive_field += local_field_val;
	
	//std::cout << "local field( xya=" << x << "," << y << "," << rad(angle).from(angle_t::X_AXIS).degrees() << "  obj=" << obj << ", dao=" << dao.dist << "," << dao.orientation.degrees() << ") = " << local_field_val << std::endl; 
	//std::cout << "   addfield=" << additive_field << std::endl;
      }
      additive_field /= _objects.size();

      // Ok, store this additive field
      field( i,j ) = additive_field;

      //std::cout << "field(" << i << "," << j << ") = {" << x << "," << y << "} = " << additive_field << std::endl;
    }
  }

  // Ok, now dump the field matrix
  std::ofstream fout( filename.c_str() );
  fout << field;
  fout.close();
}

//==================================================================

tracked_object_set_tp tracked_object_set_t::clone() const
{
  tracked_object_set_tp copy( new tracked_object_set_t(_same_object_distance_threshold, _same_object_orientation_threshold ) );
  copy->set_perception_resolution( _perception_num_x, _perception_num_y );
  copy->_field = additive_perception_field_t( _field );
  copy->_use_independent_model_only = _use_independent_model_only;

  // clone the senso models
  copy->_sensor_models = _sensor_models.clone();
  
  // clone the objects
  for( size_t i = 0; i < _objects.size(); ++i ) {
    tracked_object_tp oc( new tracked_object_t( *_objects[i], copy->_sensor_models.object(i) ) );
    copy->_objects.push_back( oc );
  }

  return copy;
}

//==================================================================

std::vector<tracked_object_tp> tracked_object_set_t::objects_in_fov( const ptplcm_detector_info_t& info, const manifold_tp& robot ) const
{
  std::vector< tracked_object_tp > ret;
  for( size_t i = 0; i < _objects.size(); ++i ) {
    if( _objects[i]->in_fov( &info, robot ) )
      ret.push_back( _objects[i] );
  }
  return ret;
}

//==================================================================


void tracked_object_set_t::print( std::ostream& os, const manifold_tp& robot ) const
{ 
  os << "Set{ " << std::endl;
  for( size_t i = 0; i < _objects.size(); ++i ) {
    os << "  ";
    _objects[i]->print( os, robot);
    os << std::endl;
  }
  os << " EndSet}";
}
  
//==================================================================

namespace ptp {
  namespace planner {

    std::ostream& operator<< ( std::ostream& os, const tracked_object_set_t& a )
    {
      os << "Set{ " << std::endl;
      for( size_t i = 0; i < a._objects.size(); ++i ) {
	os << "  " << (*a._objects[i]) << std::endl;
      }
      os << " EndSet}";
      return os;
    }

  }
}

//==================================================================
