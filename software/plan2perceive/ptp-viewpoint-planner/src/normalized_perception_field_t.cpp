
#include "normalized_perception_field_t.hpp"
#include "perception_field_t.hpp"
#include <bot_core/tictoc.h>
#include <cstdlib>
#include <fstream>

using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::detectors;
using namespace ptp::sensormodels;
using namespace ptp::planner;

//===================================================================

normalized_perception_field_t::
normalized_perception_field_t( const float& min_dist,
			       const float& max_dist,
			       const size_t& num_dist,
			       const float& min_angle,
			       const float& max_angle,
			       const size_t& num_angle )
  : _min_dist( min_dist ),
    _max_dist( max_dist ),
    _num_dist( num_dist ),
    _min_angle( min_angle ),
    _max_angle( max_angle ),
    _num_angle( num_angle )
{
}
			       
//===================================================================

double normalized_perception_field_t::norm_field_value_at( const dist_and_orientation_t& x,
							   const mean_variance_t& belief ) const
{
  int r, c;
  bool found = compute_sample_row_col( x, r, c );
  if( found ) {
    return _sampled_field( r,c );
  } else {
    return 0;
  }
}

//===================================================================

bool normalized_perception_field_t::compute_sample_row_col( const dist_and_orientation_t& x,
							    int& row,
							    int& col ) const
{
  if( _sampled_field.rows() == 0 || _sampled_field.cols() == 0 )
    return false;
  if( x.dist < _min_dist )
    return false;
  if( x.orientation.radians() < _min_angle )
    return false;
  float d_step = (_max_dist - _min_dist ) / _num_dist;
  float d = x.dist - _min_dist;
  int dbin = (int)floor( d / d_step );
  float a_step = (_max_angle - _min_angle ) / _num_angle;
  float a = x.orientation.radians() - _min_angle;
  int abin = (int)floor( a / a_step );
  if( dbin < _sampled_field.rows() &&
      abin < _sampled_field.cols() ) {
    row = dbin;
    col = abin;
    return true;
  } else {
    return false;
  }
}

//===================================================================

void normalized_perception_field_t::update( const sensor_model_tp& model,
					    const mean_variance_t& belief )
{
  bot_tictoc( "norm-perception-field::update" );

  // resize our field
  _sampled_field.resize( _num_dist, _num_angle );

  // Ok, we will just wuery the field at the wanted points
  double sum = 0;
  for( size_t i = 0; i < _num_dist; ++i ) {
    for( size_t j = 0; j < _num_angle; ++j ) {
      float d = _min_dist + ((double)i / (_num_dist-1)) * (_max_dist - _min_dist);
      float a = _min_angle + ((double)j / (_num_angle-1)) * (_max_angle - _min_angle);
      dist_and_orientation_t dao( d, rad(a).from(angle_t::X_AXIS) );
      double val = perception_field_t::field_value_at( dao, belief, model );
      sum += val;
      _sampled_field( i,j ) = val;
    }
  }

  // normlaize hte entries so that the sum of all of them equals 1
  _sampled_field /= sum;

  bot_tictoc( "norm-perception-field::update" );
}

//===================================================================

dist_and_orientation_t normalized_perception_field_t::sample_dao_within_field() const
{
  // pick two random numbers between (0,1) for dist and angle
  double rd = rand() / (double)RAND_MAX;
  double ra = rand() / (double)RAND_MAX;
  float d = _min_dist + rd * (_max_dist - _min_dist );
  float a = _min_angle + ra * (_max_angle - _min_angle );
  return dist_and_orientation_t( d,
				 rad( a ).from( angle_t::X_AXIS ) );
}

//===================================================================

void normalized_perception_field_t::dump( const std::string& filename ) const
{
  std::ofstream fout( filename.c_str() );
  fout << _sampled_field;
  fout.close();
}

//===================================================================
