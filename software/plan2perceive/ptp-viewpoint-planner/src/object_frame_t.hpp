
#if !defined( __PTP_PLANNER_object_frame_t_HPP__ )
#define __PTP_PLANNER_object_frame_t_HPP__

#include <ptp-common-coordinates/manifold.hpp>


namespace ptp {
  namespace planner {

    using namespace ptp::common;
    using namespace ptp::coordinates;
    
    //===================================================================

    // Description:
    // An object frame is a parigin between  a mnifold ( which encodes a 
    // location and orientation using it's x axis ) and a timestamp
    struct object_frame_t
    {
      int64_t timestamp;
      manifold_tp manifold;
      object_frame_t( const int64_t& t, const manifold_tp& m )
	: timestamp(t), manifold(m)
      {}
      object_frame_t()
	: timestamp(0) 
      {}
    };

    //===================================================================

    // Description:
    // A history of frames for an object.
    // This allows to query the location of an object in the past
    class object_frame_history_t
    {
    public:

      // Description:
      // Creates a new empty history with given capacity
      object_frame_history_t( const size_t& capacity );
      
      // Description:
      // Add to the history
      void add( const object_frame_t& f );
      
      // Description:
      // Return the object frame at a given time
      // (or as close to the time as teh history knows
      boost::optional<object_frame_t> at( const int64_t& taget ) const;

    protected:
      
      // Description:
      // the array of known frames
      std::vector<object_frame_t> _frames;
      
      // Description:
      // The current write index ( the end of the list )
      int _end_index;

      // DescriptionL
      // The capaacity
      size_t _capacity;
    };

    //===================================================================

  }
}

#endif

