
#include "dummy_planner_t.hpp"


using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::detectors;
using namespace ptp::sensormodels;
using namespace ptp::planner;

//=====================================================================

dummy_planner_t::dummy_planner_t( lcm_t* lcm )
  : viewpoint_planner_t(lcm)
{}

//=====================================================================

void dummy_planner_t::replan()
{
  
}

//=====================================================================
