
#if !defined( __PTP_PLANNER_efkf_HPP__ )
#define __PTP_PLANNER_efkf_HPP__


#include <ptp-common/matrix.hpp>
#include <ptp-object-detectors/common.hpp>
#include <ptp-sensor-models/sensor_model_t.hpp>


namespace ptp {
  namespace planner {

    using namespace ptp::common;
    using namespace ptp::detectors;
    using namespace sensormodels;

    //==============================================================

    // Description:
    // Static methods for expoentnail family kalmna filter.
    // This code assumes that the state and observations models 
    // are both gaussian.
    // This also assumes a stationary transition model for state
    // with zero process noise.
    //
    // So:
    //    S_{t} = S_{t-1}    p( z | \theta ) = N( mu, var )
    class EFKF 
    {
    public:

      // Description:
      // Returns the mean after a seen observation
      static double 
      update_mean_after_observation( const mean_variance_t& prior,
				     const mean_variance_t& obs_lik,
				     const detection_and_location_t& dx,
				     const sensor_model_tp& sensor,
				     const bool& use_independent_model_only = false);
      
      // Description:
      // Returns the variance acter a seen observation
      static double
      update_var_after_observation( const mean_variance_t& prior,
				    const mean_variance_t& obs_lik,
				    const dist_and_orientation_t& dx,
				    const sensor_model_tp& sensor,
				    const bool& use_independent_model_only = false);

      // Description:
      // Computes the single step variance update for a 
      // multi-step trajectory
      // This just given the Y bdd Y transform in the equations
      static double
      compute_single_step_variance_update( const mean_variance_t& prior,
					   const mean_variance_t& obs_lik,
					   const dist_and_orientation_t& x,
					   const sensor_model_tp& sensor,
					   const bool& use_independent_model_only = false);

    protected:

      // Description:
      // Compute teh kalman gain
      static matrix_t kalman_gain( const mean_variance_t& prior,
				   const mean_variance_t& obs_lik,
				   const dist_and_orientation_t& x,
				   const sensor_model_tp& sensor,
				   const bool& use_independent_model_only = false);

      // Description:
      // Maps the mean,var into exponential family parametere space theta
      static math_vector_t map_to_theta( const mean_variance_t& mv );
      
      // Description:
      // Returns the variance / mean of the expoentnai lfamily
      // described with given mean/var ( in the theta parameter space)
      static matrix_t var_as_theta( const mean_variance_t& obs_lik );
      static math_vector_t mean_as_theta( const mean_variance_t& obs_lik );

    };

    //==============================================================


  }
}


#endif

