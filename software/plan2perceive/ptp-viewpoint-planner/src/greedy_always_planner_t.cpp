
#include "greedy_always_planner_t.hpp"


using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::detectors;
using namespace ptp::sensormodels;
using namespace ptp::planner;

//=====================================================================

greedy_always_planner_t::greedy_always_planner_t( lcm_t* lcm, const float& t )
  : viewpoint_planner_t( lcm ), _belief_threshold( t )
{}

//=====================================================================


void greedy_always_planner_t::replan()
{
  
  // resize our checked vector to the number of objects being tracked
  _done_objects.resize( _tracked_objects->size() );
  
  // Pick first done object
  int idx = -1;
  for( size_t i = 0; i < _done_objects.size(); ++i ) {
    if( _done_objects[i] != 1 ) {

      // update flag with current bleief
      if( (_tracked_objects->object(i)->belief().mean > _belief_threshold) ||
	  (_tracked_objects->object(i)->belief().mean < 1 - _belief_threshold) ) {
	//_done_objects[i] = 1;
	//_tracked_objects->ignore_object_updates( i );
	//std::cout << "IGNORING OBJECT " << i << std::endl;
	continue;
      }

      idx = i;
      break;
    }
  }
  
  // If all objects are done, continue along nominal traj
  if( idx == -1 ) {

    start_executing_trajectory( create_nominal_trajecory() );
    _may_replan = true;
    _going_towards_viewpoint = false;

  } else {
  
    // Ok, choose a greedy_always viewpoint for object
    // inside it's perception field
    viewpoint_t vp = _tracked_objects->greedy_best_viewpoint( _world_manifold, idx  );

    //std::cout << "Greedy_Always viewpoint: " << coordinate( 0,0,0 ).on( vp.viewpoint_manifold() ).on( _world_manifold ) << std::endl;
    std::cout << "going for object " << idx << std::endl;
    
    // craete trajectory and start it
    viewpoint_trajectory_tp traj( new viewpoint_trajectory_t() );
    traj->set_tracked_object_set( _tracked_objects );
    traj->set_robot_manifold( _current_robot_frame.manifold );
    traj->add_viewpoint( vp );
    traj->add_viewpoint( goal_viewpoint() );
    start_executing_trajectory( traj );
  }
  
}

//=====================================================================

//=====================================================================
