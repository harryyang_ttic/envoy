
#include "object_frame_t.hpp"

using namespace ptp;
using namespace ptp::coordinates;
using namespace ptp::planner;

//=======================================================================

object_frame_history_t::object_frame_history_t( const size_t& capacity )
  : _capacity( capacity ), _end_index(0), _frames( capacity )
{
}

//=======================================================================

void object_frame_history_t::add( const object_frame_t& f ) 
{
  _frames[_end_index] = f;
  ++_end_index;
  if( _end_index >= _capacity )
    _end_index = 0;
}

//=======================================================================

boost::optional<object_frame_t> object_frame_history_t::at( const int64_t& target ) const
{
  // linear serch for closes frame
  double min_diff = std::numeric_limits<double>::infinity();
  int min_idx = -1;
  for( size_t i = 0; i < _frames.size(); ++i ) {

    // skip uninitialized targets 
    if( _frames[i].timestamp == 0 )
      continue;

    double td = abs(_frames[i].timestamp - target);
    if( td < min_diff ) {
      min_diff = td;
      min_idx = i;
    }
  }

  // return the found object frame, else nothing
  if( min_idx >= 0 ) 
    return _frames[min_idx];
  return boost::optional<object_frame_t>();
}

//=======================================================================
//=======================================================================
//=======================================================================
