
#include "perception_field_t.hpp"
#include "efkf.hpp"
#include <gsl/gsl_randist.h>

using namespace ptp;
using namespace ptp::detectors;
using namespace ptp::sensormodels;
using namespace ptp::planner;


//===================================================================

double entropy( const mean_variance_t& mv )
{
  return 0.5 * log( 2 * M_PI * mv.var * mv.var * exp(0) );
}

//===================================================================

double perception_field_t::field_value_at( const dist_and_orientation_t& x,
					   const mean_variance_t& belief,
					   const sensor_model_tp& model )
{

  // Cap values of orientation
  //if( x.orientation.degrees() > 45 ||
  //    x.orientation.degrees() < -45 ) {
  //  return 0;
  //}

  bool no_efkf = true;
  
  if( no_efkf ) {

    // We want to take expectation over 100 possible detection values
    size_t num_det = 100;
    float min_det = 0;
    float max_det = 1.0;
    float det_step = ( max_det - min_det ) / num_det;
    

    // calculate expected entropy reduction
    double delta_ent = 0;
    for( size_t i = 0; i < num_det; ++i ) {
      float det = min_det + i * det_step;

      // calculate the probability after that observations
      mean_variance_t det_lik = model->query_no_update( x, belief.mean);
      

      float pdet = gsl_ran_gaussian_pdf( det - det_lik.mean, sqrt(det_lik.var) );

      // calculate new belief after observation
      detection_and_location_t dx( det, x );
      model->update( dx );
      //std::cout << "calling update" << std::endl;
      mean_variance_t new_belief;
      new_belief.mean = EFKF::update_mean_after_observation( belief,
							     det_lik,
							     dx,
							     model );
      new_belief.var = EFKF::update_var_after_observation( belief,
							   det_lik,
							   x,
							   model );
      
      // calculate change in entroyp
      double dent = entropy( belief ) - entropy( new_belief );

      // update expectation
      delta_ent += fabs( pdet * dent );
    }

    return delta_ent;

  } else {
    // Just compute the expected belief after the observation
    mean_variance_t obs_lik = model->query_no_update( x, belief.mean );
    double var_diff = EFKF::compute_single_step_variance_update( belief, 
								 obs_lik,
								 x,
								 model );


    return var_diff*var_diff;
    
  }
  
}

//===================================================================
//===================================================================
