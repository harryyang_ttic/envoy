
#include "additive_perception_field_t.hpp"
#include <GL/gl.h>
#include <boost/functional/hash.hpp>
#include <bot_core/rand_util.h>
#include <fstream>
#include <sstream>


using namespace ptp;
using namespace ptp::distributions;
using namespace ptp::coordinates;
using namespace ptp::planner;


//======================================================================

namespace ptp {
  namespace planner {

    size_t hash_value( const bin_xyt& a ) {
      size_t seed = 0;
      boost::hash_combine( seed, a.x );
      boost::hash_combine( seed, a.y );
      boost::hash_combine( seed, a.obj );
      return seed;
    }

    bool operator==( const bin_xyt& a, const bin_xyt& b )
    {
      return a.x == b.x &&
	a.y == b.y &&
	a.obj == b.obj;
    }

    bool operator<( const bin_xyt& a, const bin_xyt& b ) {
      if( a.obj < b.obj )
	return true;
      if( a.x < b.x )
	return true;
      if( a.y < b.y )
	return true;
      return false;
    }

  }
}

//======================================================================

additive_perception_field_t::additive_perception_field_t()
{
}

//======================================================================

void additive_perception_field_t::update( const std::vector<tracked_object_tp>& objects,
					  const float& min_x, const float& max_x,
					  const float& min_y, const float& max_y,
					  const size_t& num_x, const size_t& num_y,
					  const ptplcm_detector_info_t& info,
					  const manifold_tp& ref)
{
  // store state
  _objects = objects;
  _min_x = min_x;
  _max_x = max_x;
  _min_y = min_y;
  _max_y = max_y;
  _num_x = num_x;
  _num_y = num_y;

  // clear the previous field
  _field_map.clear();
  _distribution = discrete_distribution_t<bin_xyt>();
  _min_value = std::numeric_limits<float>::infinity();
  _max_value = -std::numeric_limits<float>::infinity();
  
  // Ok, now build up the field
  size_t num_angles = objects.size();
  float step_x = ( _max_x - _min_x ) / _num_x;
  float step_y = ( _max_y - _min_y ) / _num_y;
  for( size_t obj = 0; obj < _objects.size(); ++obj ) {
    for( size_t i = 0; i < num_y; ++i ) {
      for( size_t j = 0; j < num_x; ++j ) {
      
	// compute x,y for bin
	float x = _min_x + j * step_x;
	float y = _min_y + i * step_y;

	// Get the object location
	coordinate_t obj_coord = coordinate( 0,0,0 ).on( _objects[obj]->object_manifold() ).on( ref );
	
	// Now get the angle for the object
	float view_angle = atan2( obj_coord[1] - y,
				  obj_coord[0] - x );

	// Create the view manifold
	manifold_tp view_man = create_object_manifold( coordinate( x,y,0 ).on(ref),
						       rad( view_angle ).from(angle_t::X_AXIS ),
						       ref );
	
	// Ok, add up the normalized field for all objects which are seen
	double additive_field = 0;
	size_t num_objects_in_view = 0;
	for( size_t k = 0; k < _objects.size(); ++k ) {

	  // if we cannot see the look-at object, break
	  //if( _objects[obj]->in_fov( &info, view_man ) == false ) {
	  //  break;
	  //}

	  if( _objects[k]->in_fov( &info, view_man ) == false )
	    continue;

	  // compute dist_orint of viewpoint for the object
	  dist_and_orientation_t dao = calculate_dist_and_orientation_from_object( view_man, _objects[k]->object_manifold() );

	  if( _objects[k]->norm_perception_field()->is_within_field( dao ) == false ) {
	    continue;
	  }
	  
	  // Add teh perception field value for object
	  double local_field_val = _objects[k]->norm_perception_field()->norm_field_value_at( dao, _objects[k]->belief() );
	  additive_field += local_field_val;
	  ++num_objects_in_view;

	  //std::cout << "afield: " << j << " " << i << " " << obj << " " << k << " {" << x << "," << y << "} obj:{" << obj_coord[0] << "," << obj_coord[1] << "} dao:{" << dao.dist << "," << dao.orientation.degrees() << "} = " << local_field_val << " add=" << additive_field << " (" << num_objects_in_view << ")" << std::endl;
	}
	if( num_objects_in_view < 1 )
	  num_objects_in_view = 1;
	additive_field /= num_objects_in_view;

	// Store this field value
	bin_xyt bin;
	bin.x = j;
	bin.y = i;
	bin.obj = obj;
	_field_map[ bin ] = additive_field;
	_distribution.set( bin, additive_field );

	
	// trakc min/max
	if( additive_field < _min_value )
	  _min_value = additive_field;
	if( additive_field > _max_value )
	  _max_value = additive_field;
      }
    }
  }
}

//======================================================================

viewpoint_t additive_perception_field_t::convert_bin_to_viewpoint( const bin_xyt& bin, const manifold_tp& ref ) const
{
  float step_x = ( _max_x - _min_x ) / _num_x;
  float step_y = ( _max_y - _min_y ) / _num_y;
  float x = _min_x + bin.x * step_x + bot_randf() * step_x;
  float y = _min_y + bin.y * step_y + bot_randf() * step_y;
  coordinate_t obj_c = coordinate( 0,0,0 ).on( _objects[bin.obj]->object_manifold() ).on( ref );
  float view_angle = atan2( obj_c[1] - y,
			    obj_c[0] - x );
  
  viewpoint_t view( coordinate( x,y,0 ).on( ref ),
		    rad( view_angle ).from( angle_t::X_AXIS ),
		    ref );
  return view;
}

//======================================================================

viewpoint_t additive_perception_field_t::sample_viewpoint( const manifold_tp& ref ) const
{
  // Get a bin from the distribution
  bin_xyt bin = _distribution.sample();
  
  // Convert from bin to viewpoint
  return convert_bin_to_viewpoint( bin, ref );
}

//======================================================================

viewpoint_t additive_perception_field_t::best_viewpoint(const manifold_tp& ref, const int& obj) const
{
  // find the best bin
  double max_val = - std::numeric_limits<double>::infinity();
  bin_xyt bin;
  for( boost::unordered_map<bin_xyt,double>::const_iterator citer = _field_map.begin();
       citer != _field_map.end();
       ++citer ) {
    if( citer->first.obj == obj && citer->second > max_val ) {
      max_val = citer->second;
      bin = citer->first;
    }
  }

  //std::cout << "  found max bin {" << bin.x << ", " << bin.y << ", " << bin.obj << "} val= " << max_val << std::endl;

  return convert_bin_to_viewpoint( bin, ref );
}

//======================================================================

std::vector<viewpoint_t>
additive_perception_field_t::nonzero_viewpoint_set( const manifold_tp& ref,
						    std::vector<int>& object_index) const
{
  std::vector<viewpoint_t> views;
  
  // For all bins, add viewpoint if nonzero weight
  for( boost::unordered_map<bin_xyt,double>::const_iterator citer = _field_map.begin();
       citer != _field_map.end();
       ++citer ) {
    
    if( citer->second > 0 ) {
      viewpoint_t vo = convert_bin_to_viewpoint( citer->first, ref );
      views.push_back( vo );
      object_index.push_back( citer->first.obj );
    }
  }

  return views;
}

//======================================================================


double additive_perception_field_t::field_at( const float& x,
					      const float& y,
					      const float& theta,
					      const manifold_tp& ref ) const
{
  // turn x,y into bin
  float step_x = ( _max_x - _min_x ) / _num_x;
  float step_y = ( _max_y - _min_y ) / _num_y;
  bin_xyt bin;
  bin.x = (x - _min_x) / step_x;
  bin.y = (y - _min_y) / step_y;
  
  // Compiute closes theta
  float min_angle = std::numeric_limits<float>::infinity();
  int obj = -1;
  for( size_t i = 0; i < _objects.size(); ++i ) {
    coordinate_t oc = coordinate( 0,0,0 ).on( _objects[i]->object_manifold() ).on( ref );
    float a = atan2( oc[1] - y,
		     oc[0] - x );
    float da = abs( a - theta );
    if( da < min_angle ) {
      min_angle = da;
      obj = i;
    }
  }
  
  // Query the field map
  // Return 0 if not found 
  if( obj >= 0 ) {
    bin.obj = obj;
    boost::unordered_map<bin_xyt,double>::const_iterator citer = 
      _field_map.find( bin );
    if( citer == _field_map.end() )
      return 0.0;
    return citer->second;
  } else {
    return 0.0;
  }
}

//======================================================================

void additive_perception_field_t::draw( bot_lcmgl_t* lcmgl ) const
{
  
  // This is a large drawing

  float step_x = ( _max_x - _min_x ) / _num_x;
  float step_y = ( _max_y - _min_y ) / _num_y;

  // the transparecy of field
  float trans = 0.5;

  // do some blending on
  bot_lcmgl_enable( lcmgl, GL_BLEND );

  // start drawing quads
  bot_lcmgl_begin( lcmgl, GL_QUADS );
  
  // Ok, draw each bin with the height = obj index
  //for( size_t obj = 0; obj < _objects.size(); ++obj ) {
  { size_t obj = 0;
    for( size_t i = 0; i < _num_y; ++i ) {
      for( size_t j = 0; j < _num_x; ++j ) {
	
	// compute x,y from bin
	float x = _min_x + j * step_x;
	float y = _min_y + i * step_y;

	// query the field at bin
	bin_xyt bin;
	bin.x = j;
	bin.y = i;
	bin.obj = obj;
	boost::unordered_map<bin_xyt,double>::const_iterator citer 
	  = _field_map.find( bin );
	if( citer == _field_map.end() )
	  continue;
	double val = citer->second;

	if( val < 1e-10 )
	  continue;
	
	// set color
	float color = 0.1 + 0.9 * (val - _min_value) / ( _max_value - _min_value );
	if( color > 1 )
	  color = 1;
	bot_lcmgl_color4f( lcmgl, 0, color, 0, trans );
	
	// draw quad
	float height_mult = 3;
	bot_lcmgl_vertex3f( lcmgl, x, y, (obj+1) * height_mult );
	bot_lcmgl_vertex3f( lcmgl, x + step_x, y, (obj+1) * height_mult );
	bot_lcmgl_vertex3f( lcmgl, x + step_x, y + step_y, (obj+1) * height_mult );
	bot_lcmgl_vertex3f( lcmgl, x, y + step_y, (obj+1) * height_mult );
      }
    }
  }

  bot_lcmgl_end( lcmgl );

  bot_lcmgl_disable( lcmgl, GL_BLEND );

  // add text for min and max
  std::ostringstream oss;
  oss << "min: " << _min_value << "  max: " << _max_value;
  double pos[3];
  pos[0] = _min_x + 1/5 * ( _max_x - _min_x );
  pos[1] = _min_y;
  pos[3] = 0;
  bot_lcmgl_color3f( lcmgl, 1,1,1 );
  bot_lcmgl_text( lcmgl, pos, oss.str().c_str() );
}

//======================================================================

void additive_perception_field_t::dump( const std::string& filename ) const
{
  if( _objects.size() <= 0 )
    return;

  std::ofstream fout( filename.c_str() );
  for( size_t i = 0; i < _num_y; ++i ) {
    for( size_t j = 0; j < _num_x; ++j ) {
      bin_xyt bin;
      bin.x = j;
      bin.y = i;
      bin.obj = 0;
      boost::unordered_map<bin_xyt,double>::const_iterator citer 
	= _field_map.find( bin );
      double val = 0.0;
      if( citer != _field_map.end() )
	val = citer->second;
      fout << val;
      if( j != _num_x - 1 ) {
	fout << " ";
      }
    }
    fout << std::endl;
  }
  fout.close();
}

//======================================================================
//======================================================================
//======================================================================
//======================================================================
//======================================================================
//======================================================================
//======================================================================
//======================================================================
//======================================================================
//======================================================================
