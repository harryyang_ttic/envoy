
#if !defined( __PTP_PLANNER_random_planer_t_HPP__ )
#define __PTP_PLANNER_random_planer_t_HPP__

#include "viewpoint_planner_t.hpp"

namespace ptp {
  namespace planner {

    //==========================================================

    // Description:
    // A planner which choses random viewpoint until a given
    // belief is met
    class random_planner_t
      : public viewpoint_planner_t
    {
    public:

      // Description:
      // Created a new randome planner with given threshold
      random_planner_t( lcm_t* lcm, const float& thresh );

    protected:

      // Description:
      // Replans
      void replan();
      
    protected:

      // Description:
      // The belief thresholf
      float _belief_threshold;
      
      // Description:
      // Flags for object which have already been checked
      std::vector<int> _done_objects;
    };

    //==========================================================

  }
}

#endif

