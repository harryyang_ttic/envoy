
#include "viewpoint_trajectory_t.hpp"
#include "efkf.hpp"

using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::planner;

// some constanst costs
double OBJECT_TRUE_POSITIVE_COST[2] = {-4, -4};
double OBJECT_FALSE_POSITIVE_COST[2] = { 4, 4 };
double OBJECT_COST_MULTIPLIER[2] = { 4, 1.4 };



//====================================================================

void viewpoint_trajectory_t::set_tracked_object_set( const tracked_object_set_tp& obs )
{
  // clear decision cache
  reset_decision_cached_costs();
  _objects = obs;
  
  // resize all of the belif caches
  for( size_t i = 0; i < _viewpoint_belief_stubs.size(); ++i ) {
    _viewpoint_belief_stubs[i].resize( _objects->size() );
  }
  for( size_t i = 0; i < _viewpoint_belief_update_stubs.size(); ++i ) {
    _viewpoint_belief_update_stubs[i].resize( _objects->size() );
  }
    
}

//====================================================================

void viewpoint_trajectory_t::set_robot_manifold( const manifold_tp& robot )
{
  // clear the motion caches
  reset_motion_cached_costs();
  _robot_manifold = robot;
}

//====================================================================

void viewpoint_trajectory_t::add_viewpoint( const viewpoint_t& view )
{
  // clear decision and motion costs
  reset_decision_cached_costs();
  reset_motion_cached_costs();

  // add the viewpoint 
  _viewpoints.push_back( view );

  // expand the cost stub vectors
  _actual_viewpoint_motion_stubs.push_back( boost::optional<double>() );
  _estimate_viewpoint_motion_stubs.push_back( boost::optional<double>() );
  _viewpoint_belief_stubs.push_back( belief_set_t() );
  _viewpoint_belief_update_stubs.push_back( belief_set_updates_t() );
  if( _objects ) {
    _viewpoint_belief_stubs[ _viewpoint_belief_stubs.size()-1 ].resize( _objects->size() );
    _viewpoint_belief_update_stubs[ _viewpoint_belief_update_stubs.size() - 1 ].resize( _objects->size() );
  }
}

//====================================================================

double viewpoint_trajectory_t::estimated_motion_cost() const
{
  // Check cache and return if there
  if( _estimate_motion_cost )
    return *_estimate_motion_cost;
  
  // Ok, sub up the motion costs stubs
  double cost = 0;
  for( size_t i = 0; i < _viewpoints.size(); ++i ) {
    cost += compute_estimated_motion_cost_viewpoint( i );
  }
  
  // cache and return
  _estimate_motion_cost = cost;
  return cost;
}

//====================================================================

double viewpoint_trajectory_t::estimated_decision_cost() const
{
  return actual_decision_cost();
}

//====================================================================

double viewpoint_trajectory_t::estimated_total_cost() const
{
  return estimated_decision_cost() + estimated_motion_cost();
}

//====================================================================

double viewpoint_trajectory_t::actual_motion_cost() const
{
  // check cache 
  if( _actual_motion_cost )
    return *_actual_motion_cost;
  
  // Ok, sum up all of the stubs
  double cost = 0;
  for( size_t i = 0; i < _viewpoints.size(); ++i ) {
    cost += compute_actual_motion_cost_viewpoint( i );
  }
  
  // Cache and return
  _actual_motion_cost = cost;
  return cost;
}

//====================================================================

double viewpoint_trajectory_t::actual_decision_cost() const
{
  // check cache
  if( _actual_decision_cost )
    return *_actual_decision_cost;

  // compute the decision cost for each object individually
  // and add then cache
  double cost = 0;
  for( size_t i = 0; i < _objects->size(); ++i ) {
    cost += compute_decision_cost_single_object( i );
  }
  _actual_decision_cost = cost;
  return cost;
}

//====================================================================

double viewpoint_trajectory_t::actual_total_cost() const
{
  return actual_decision_cost() + actual_motion_cost();
}

//====================================================================

void viewpoint_trajectory_t::reset_motion_cached_costs()
{
  _actual_motion_cost = boost::optional<double>();
  _estimate_motion_cost = boost::optional<double>();

  // the first motion stub is special, detelte it's cache if there
  if( _actual_viewpoint_motion_stubs.size() > 0 ) {
    _actual_viewpoint_motion_stubs[0] = boost::optional<double>();
  }
  if( _estimate_viewpoint_motion_stubs.size() > 0 ) {
    _estimate_viewpoint_motion_stubs[0] = boost::optional<double>();
  }

  _actual_total_cost = boost::optional<double>();
}

//====================================================================

void viewpoint_trajectory_t::reset_decision_cached_costs()
{
  _actual_decision_cost = boost::optional<double>();
  
  // reset the mean estimates for decisions ( not the variance )
  for( size_t i = 0; i < _viewpoint_belief_update_stubs.size(); ++i ) {
    _viewpoint_belief_update_stubs[i].reset_means();
  }
  // reset the belie sets
  for( size_t i = 0; i < _viewpoint_belief_stubs.size(); ++i ) {
    _viewpoint_belief_stubs[i].reset();
  }
}

//====================================================================

double viewpoint_trajectory_t::compute_decision_cost_single_object( const int& index ) const
{
  // get the current belief of that object
  mean_variance_t bel = _objects->object(index)->belief();
  
  // Now, get the mean and variance updates for this object
  double mean_update = compute_mean_update_single_object( index );
  double var_update = compute_var_update_single_object( index );
  
  // compute the new belief at emnd
  mean_variance_t new_bel;
  new_bel.mean = mean_update;
  new_bel.var = 1.0 / ( 1.0/bel.var + var_update );
  
  // compute decision cost with belief
  double cost = decision_cost( new_bel, _objects->object(index)->object_type() );
  if( cost > 0 )
    cost = 0;
  return cost;
}

//====================================================================

double viewpoint_trajectory_t::compute_mean_update_single_object( const int& index ) const
{
  // this is just he mean update for the last viewpoint
  return compute_mean_update_single_object_single_view( index, _viewpoint_belief_stubs.size() -1 );
}

//====================================================================

double viewpoint_trajectory_t::compute_var_update_single_object( const int& index ) const
{
  // this is just the sumation of var update for all viewpoints
  double sum = 0;
  for( size_t i = 0; i < _viewpoints.size(); ++i ) {
    sum += compute_var_update_single_object_single_view( index, i );
  }

  // return
  return sum;
}

//====================================================================

double viewpoint_trajectory_t::compute_mean_update_single_object_single_view( const int& obj_index, const int& viewpoint_index ) const
{
  // Check if we already have this
  if( _viewpoint_belief_update_stubs[ viewpoint_index ].mean_updates[ obj_index ] )
    return *_viewpoint_belief_update_stubs[ viewpoint_index ].mean_updates[ obj_index ];


  double mean;

  //std::cout << "mean update: obj=" << obj_index << ", vp=" << viewpoint_index << std::endl;

  // grab our previus belief
  mean_variance_t bel;
  bel = compute_belief_at_viewpoint( obj_index, viewpoint_index - 1 );

  //std::cout << "  mu[" << obj_index << "," << viewpoint_index << "] : bel=" << bel << std::endl;
  
  mean_variance_t obs_lik = _viewpoints[viewpoint_index].observation_model_at_viewpoint( _objects->object(obj_index), _objects->sensor_model( obj_index ), bel, _robot_manifold, _use_independent_model_only );

  //std::cout << "  mu[" << obj_index << "," << viewpoint_index << "] : vp-loc=" << coordinate( 0,0,0 ).on( _viewpoints[ viewpoint_index ].viewpoint_manifold() ).on( _robot_manifold ) << std::endl;
  //std::cout << "  mu[" << obj_index << "," << viewpoint_index << "] : obs_lik=" << obs_lik << std::endl;
  
  // Create the mean observation according to observation model
  detection_and_location_t mean_det = _viewpoints[viewpoint_index].create_detection_from_viewpoint( _objects->object(obj_index), obs_lik.mean, _robot_manifold );

  //std::cout << "  mu[" << obj_index << "," << viewpoint_index << "] mean-det=" << mean_det.detection_value << std::endl;
  
  // Now perform a single step efkf mean update
  mean = EFKF::update_mean_after_observation( bel, obs_lik, mean_det, _objects->sensor_model( obj_index ), _use_independent_model_only );

  //std::cout << "  mu[" << obj_index << "," << viewpoint_index << "] new mean=" << mean << std::endl;
  
  // cache this result and return
  _viewpoint_belief_update_stubs[ viewpoint_index ].mean_updates[ obj_index ] = mean;
  return mean;
}

//====================================================================

mean_variance_t viewpoint_trajectory_t::compute_belief_at_viewpoint( const int& obj_index, const int& viewpoint_index ) const
{
  // -1 viewpoint is base case
  if( viewpoint_index <= -1 ) {
    return _objects->object(obj_index)->belief();
  }
  
  // Check cache for belief
  if( _viewpoint_belief_stubs[ viewpoint_index ].beliefs[ obj_index ] ) {
    return *_viewpoint_belief_stubs[ viewpoint_index ].beliefs[ obj_index ];
  }
  
  // calculate belief from previous belief
  mean_variance_t prev_bel = compute_belief_at_viewpoint( obj_index, viewpoint_index -1 );
  
  // get the mean and variance updates
  double mean_up = compute_mean_update_single_object_single_view( obj_index, viewpoint_index );
  double var_up = compute_var_update_single_object_single_view( obj_index, viewpoint_index );
  
  // update the belief
  mean_variance_t bel;
  bel.mean = mean_up;
  bel.var = 1.0/( 1.0/prev_bel.var + var_up );

  //std::cout << "belief[" << obj_index << "," << viewpoint_index << "] = " << bel << std::endl;
  
  // cache and return
  _viewpoint_belief_stubs[ viewpoint_index ].beliefs[ obj_index ] = bel;
  return bel;
}

//====================================================================

double viewpoint_trajectory_t::compute_var_update_single_object_single_view( const int& obj_index, const int& viewpoint_index ) const
{
  // Check if we have it cached
  if( _viewpoint_belief_update_stubs[ viewpoint_index ].var_updates[ obj_index ] ) {
    return *_viewpoint_belief_update_stubs[ viewpoint_index ].var_updates[ obj_index ];
  }

  // grab the original belief
  mean_variance_t orig_bel = _objects->object( obj_index )->belief();

  //std::cout << "var_update : obj=" << obj_index << ", vp=" << viewpoint_index << std::endl;
  //std::cout << "  vu[" << obj_index << "," << viewpoint_index << "] : bel=" << orig_bel << std::endl;

  // ok, compute the single step variance update
  dist_and_orientation_t dao = _viewpoints[viewpoint_index].get_dist_and_orientation( _objects->object(obj_index), _robot_manifold );
  mean_variance_t obs_lik;
  if( _use_independent_model_only ) {
    mean_variance_t corr_model;
    float p_indept;
    _objects->sensor_model( obj_index )->query_separate( dao, orig_bel.mean, obs_lik, corr_model, p_indept );
  } else {
    obs_lik = _objects->sensor_model( obj_index )->query_no_update( dao, orig_bel.mean );
  }
  double var_up = EFKF::compute_single_step_variance_update( obs_lik, orig_bel, dao, _objects->sensor_model( obj_index ), _use_independent_model_only );

  //std::cout << "  vu[" << obj_index << "," << viewpoint_index << "] : vp-loc=" << coordinate( 0,0,0 ).on(_viewpoints[viewpoint_index].viewpoint_manifold()).on(_robot_manifold) << std::endl;
  //std::cout << "  vu[" << obj_index << "," << viewpoint_index << "] : dist=" << dao.dist << ", orient=" << dao.orientation.radians() << std::endl;
  //std::cout << "  vu[" << obj_index << "," << viewpoint_index << "] : obs-lik=" << obs_lik << std::endl;
  //std::cout << "  vu[" << obj_index << "," << viewpoint_index << "] : var_up=" << var_up << std::endl;

  // cache and return
  _viewpoint_belief_update_stubs[ viewpoint_index ].var_updates[ obj_index ] = var_up;
  return var_up;
}

//====================================================================

double viewpoint_trajectory_t::decision_cost( const mean_variance_t& bel,
					      const int& obj_type ) const
{
  // Returns the cost of deciding to accept
  
  
  // compute cose using hte mean of belief over object existance
  double CTP = OBJECT_TRUE_POSITIVE_COST[obj_type];
  double CFP = OBJECT_FALSE_POSITIVE_COST[obj_type];
  double p = bel.mean;
  if( p < 0 )
    p = 0;
  if( p > 1 )
    p = 1;
  double cost = OBJECT_COST_MULTIPLIER[obj_type] * (p*CTP + (1-p)*CFP);

  return cost;
}

//====================================================================

double viewpoint_trajectory_t::compute_estimated_motion_cost_viewpoint( const int& viewpoint_index ) const
{
  // check cache
  if( _estimate_viewpoint_motion_stubs[ viewpoint_index ] ) {
    return * _estimate_viewpoint_motion_stubs[ viewpoint_index ];
  }
  
  // Ok, get the previous viewpoint location and just use the straight line
  // distance as the cost
  manifold_tp prev_manifold;
  if( viewpoint_index > 1 ) {
    prev_manifold = _viewpoints[ viewpoint_index - 1 ].viewpoint_manifold();
  } else {
    prev_manifold = _robot_manifold;
  }

  // Grab both location in the robot coordinate frame
  coordinate_t prev_loc = coordinate( 0,0,0 ).on( prev_manifold ).on( _robot_manifold );
  coordinate_t loc = coordinate( 0,0,0 ).on( _viewpoints[viewpoint_index].viewpoint_manifold() ).on( _robot_manifold );
  
  // Get the distance between the two
  double dist = basis_t( prev_loc, loc ).arc_length();
  
  // cache and return
  _estimate_viewpoint_motion_stubs[ viewpoint_index ] = dist;
  return dist;
}

//====================================================================

double viewpoint_trajectory_t::compute_actual_motion_cost_viewpoint( const int& viewpoint_index ) const
{
  // check cache
  if( _actual_viewpoint_motion_stubs[ viewpoint_index ] ) {
    return * _actual_viewpoint_motion_stubs[ viewpoint_index ];
  }
  
  // Ok, get the previous viewpoint location and just use the straight line
  // distance as the cost
  manifold_tp prev_manifold;
  if( viewpoint_index > 1 ) {
    prev_manifold = _viewpoints[ viewpoint_index - 1 ].viewpoint_manifold();
  } else {
    prev_manifold = _robot_manifold;
  }

  // Grab both location in the robot coordinate frame
  coordinate_t prev_loc = coordinate( 0,0,0 ).on( prev_manifold ).on( _robot_manifold );
  coordinate_t loc = coordinate( 0,0,0 ).on( _viewpoints[viewpoint_index].viewpoint_manifold() ).on( _robot_manifold );
  
  // Get the distance between the two
  double dist = std::numeric_limits<double>::signaling_NaN();
  //double dist = path_planner.path( prev_loc, loc ).length();
  
  // cache and return
  _actual_viewpoint_motion_stubs[ viewpoint_index ] = dist;
  return dist;
}

//====================================================================
//====================================================================
//====================================================================
//====================================================================
//====================================================================
//====================================================================
