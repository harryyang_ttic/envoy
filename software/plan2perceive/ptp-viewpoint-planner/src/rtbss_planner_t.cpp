
#include "rtbss_planner_t.hpp"


using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::detectors;
using namespace ptp::sensormodels;
using namespace ptp::planner;

//=====================================================================

rtbss_planner_t::rtbss_planner_t( lcm_t* lcm )
  : viewpoint_planner_t( lcm )
{
  subclass_init();
}

//=====================================================================

void rtbss_planner_t::subclass_init()
{
  _tracked_objects->set_perception_resolution( 5, 5 );
  _tracked_objects->use_inependent_model_only( true );
  //std::cout << "Setting perception resolution: 5,5 " << std::endl;
}

//=====================================================================

void rtbss_planner_t::replan()
{
  
  // Ok, build up the rtbbs depth 2 tree for viewpoints trajectories
  std::vector<viewpoint_trajectory_tp> trajs;
  
  // get the set of all non-zero weighed viewpoints
  //std::vector<viewpoint_t> views = _tracked_objects->nonzero_viewpoint_set(_world_manifold);

  // Build up a sampling of views from around the objects
  for( size_t i = 0; i < _tracked_objects->size(); ++i ) {

    // the object's location
    coordinate_t oc = coordinate( 0,0,0 ).on( _tracked_objects->object(i)->object_manifold() ).on( _world_manifold );
    
    // the view around thi object
    std::vector<viewpoint_t> views;
    float radius = 8;
    size_t num_points = 4;
    float step = 2 * radius / num_points;
    for( size_t yi = 0; yi < num_points; ++yi ) {
      for( size_t xi = 0; xi < num_points; ++xi ) {
	float x = ( oc[0] - radius + step/2 ) + xi * step;
	float y = ( oc[1] - radius + step/2 ) + yi * step;
	viewpoint_t vp( coordinate( x, y, 0 ).on( _world_manifold ),
			rad( atan2( oc[1] - y,
				    oc[0] - x ) ).from( angle_t::X_AXIS ),
			_world_manifold );
	views.push_back( vp );
      }
    }

    // Build up trajectories (*all*) for a given depth
    size_t depth = 4;
    std::vector<size_t> indices;
    for( size_t i = 0; i < depth-1; ++i ) {
      indices.push_back(0);
    }
    bool done = false;
    while( done == false ) {
      viewpoint_trajectory_tp t( new viewpoint_trajectory_t() );
      t->set_tracked_object_set( _tracked_objects );
      t->set_robot_manifold( _current_robot_frame.manifold );
      t->use_inependent_model_only( true );
      for( size_t i = 0; i < indices.size(); ++i ) {
	t->add_viewpoint( views[ indices[i] ] );
      }
      t->add_viewpoint( goal_viewpoint() );
      trajs.push_back( t );

      // increment the indices, break if done
      for( size_t i = indices.size()-1; i>= 0; --i ) {
	indices[i] += 1;
	if( indices[i] >= views.size() ) {
	  if( i == 0 ) {
	    // done!
	    done = true;
	    break;
	  } else {
	    indices[i] = 0;
	  }
	} else {
	  break;
	}
      }
    }
    
    // // Build up trajectories for this object
    // for( size_t i = 0; i < views.size(); ++i ) {
    //   for( size_t j = 0; j < views.size(); ++j ) {
    // 	viewpoint_trajectory_tp t( new viewpoint_trajectory_t() );
    // 	t->set_tracked_object_set( _tracked_objects );
    // 	t->set_robot_manifold( _current_robot_frame.manifold );
    // 	t->use_inependent_model_only( true );
    // 	t->add_viewpoint( views[i] );
    // 	t->add_viewpoint( views[j] );
    // 	t->add_viewpoint( goal_viewpoint() );
    // 	trajs.push_back( t );
    //   }
    // }

  }
    
  // add the nominal traj
  trajs.push_back( create_nominal_trajecory() );

  std::cout << "evaluating " << trajs.size() << " trajectories" << std::endl;

  // Now choose the lowest cost trajectory
  double min_cost = std::numeric_limits<double>::infinity();
  viewpoint_trajectory_tp min_traj;
  int idx = -1;
  for( size_t i = 0; i < trajs.size(); ++i ) {

    // draw something fancy
    draw_sampled_trajectory( trajs[ i ] );
    if( i % 100 == 0 ) {
      bot_lcmgl_switch_buffer( _sample_lcmgl );
    }

    double c = trajs[i]->estimated_total_cost();
    if( c < min_cost ) {
      min_cost = c;
      min_traj = trajs[i];
      idx = i;
    }
  }

  std::cout << "Nomimal cost= " << trajs[trajs.size()-1]->estimated_total_cost() << std::endl;
  std::cout << "Min cost= " << min_traj->estimated_total_cost() << std::endl;

  // go for min cost trajectory
  start_executing_trajectory( min_traj );

  // Cause replanning on detections if we had the nominal traj
  if( idx == trajs.size() - 1 ) {
    _may_replan = true;
  }

}

//=====================================================================

//=====================================================================
