
#include "tracked_object_t.hpp"

using namespace ptp::planner;
using namespace ptp::coordinates;
using namespace ptp::common;
using namespace ptp::detectors;
using namespace ptp::sensormodels;


//============================================================

void tracked_object_t::update_perception_field( const sensor_model_tp& sensor )
{
  // Create the field if need be
  if( _field == false ) {
    if( _object_type == 0 ) {
      _field = normalized_perception_field_tp( new normalized_perception_field_t( 0, 9, 12, -deg(90).from(angle_t::X_AXIS).radians(), deg(90).from(angle_t::X_AXIS).radians(), 36 ) );
    } else {
      _field = normalized_perception_field_tp( new normalized_perception_field_t( 0, 8, 12, -deg(45).from(angle_t::X_AXIS).radians(), deg(45).from(angle_t::X_AXIS).radians(), 18 ) );
    }
  }

  _field->update( sensor, _belief );
    
}

//============================================================

bool tracked_object_t::in_fov( const ptplcm_detector_info_t* info,
			       const manifold_tp& robot ) const
{
  // make a manifold for teh detector
  coordinate_t c = coordinate( info->position_in_robot_body_frame[0],
			       info->position_in_robot_body_frame[1],
			       info->position_in_robot_body_frame[2] ).on( robot );
  float angle = atan2( info->look_at_axis_in_robot_body_frame[1],
		       info->look_at_axis_in_robot_body_frame[0] );
  manifold_tp view_man = create_object_manifold( c,
						 rad(angle).from(angle_t::X_AXIS),
						 robot );
  
  // compute the distance
  float dist = basis_t( c, coordinate( 0,0,0 ).on( object_manifold() ).on( robot ) ).arc_length();
  
  // check if distance and angle within FOV
  if( dist >= info->fov_min_distance &&
      dist <= info->fov_max_distance &&
      angle <= info->fov_left_radians && 
      angle >= info->fov_right_radians ) {

    // Now, also check that we are within the sensing part of the object
    // perception field
    dist_and_orientation_t dao = calculate_dist_and_orientation_from_object( view_man, object_manifold() );
    if( _field->is_within_field( dao ) ) {
      return true;
    } else {
      return false;
    }
  }
  return false;
}

//============================================================

void tracked_object_t::print( std::ostream& os, const manifold_tp& robot ) const
{
  os << "id=" << object_id() << ",type=" << object_type() << ",bel=N(" << belief().mean << "," << belief().var << "),loc=" << location().on(robot);
  if( _belief_history ) {
    os << std::endl;
    os << "  belief history: " << std::endl;
    for( size_t i = 0; i < (*_belief_history).size(); ++i ) {
      os << i << " " << (*_belief_history)[i].mean << " " << (*_belief_history)[i].var << std::endl;
    }
  }
}

//============================================================

namespace ptp {
  namespace planner {

    std::ostream& operator<< (std::ostream& os, const tracked_object_t& a )
    {
      os << "id=" << a.object_id() << ",type=" << a.object_type() << ",bel=N(" << a.belief().mean << "," << a.belief().var << "),loc=" << a.location();
      if( a._belief_history ) {
	os << std::endl;
	os << "  belief history: " << std::endl;
	for( size_t i = 0; i < (*a._belief_history).size(); ++i ) {
	  os << i << " " << (*a._belief_history)[i].mean << " " << (*a._belief_history)[i].var << std::endl;
	}
      }
      return os;
    }

  }
}

//============================================================

