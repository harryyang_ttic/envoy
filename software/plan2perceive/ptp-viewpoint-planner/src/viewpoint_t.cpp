
#include "viewpoint_t.hpp"
#include <ptp-common-coordinates/rotation_manifold_t.hpp>
#include <ptp-common-coordinates/translation_manifold_t.hpp>


using namespace ptp;
using namespace ptp::planner;
using namespace ptp::coordinates;
using namespace ptp::sensormodels;
using namespace ptp::detectors;

//=================================================================

viewpoint_t::viewpoint_t( const coordinate_t& view_position_on_ref,
			  const angle_t& view_angle_from_ref_x_axis,
			  const manifold_tp& ref )
  : _reference_manifold( ref )
{
  // create the viewpoint's manifold from the position and 
  // angle of viewpoint based on ref
  manifold_tp tref = translate( view_position_on_ref, ref );
  manifold_tp rtref = rotate( line_simplex( coordinate(0,0,0),
					    coordinate(0,0,1),
					    tref ),
			      view_angle_from_ref_x_axis,
			      tref );
  _viewpoint_manifold = rtref;
}

//=================================================================

dist_and_orientation_t viewpoint_t::get_dist_and_orientation( const tracked_object_tp& obj, const manifold_tp& robot ) const
{
  // THIS MAY FAIL!!
  return calculate_dist_and_orientation( obj->object_manifold(),
					 viewpoint_manifold() );
}

//=================================================================

mean_variance_t viewpoint_t::observation_model_at_viewpoint( const tracked_object_tp& obj, const sensor_model_tp& model, const mean_variance_t& bel, const manifold_tp& robot, const bool& use_inependent_model_only ) const
{
  // First get the dist/orientation for ibject
  dist_and_orientation_t dao = get_dist_and_orientation( obj, robot );

  //std::cout << "view : viewpoint loc= " << coordinate( 0,0,0 ).on( viewpoint_manifold() ).on( robot ) << " axis=" << coordinate( 1,0,0 ).on( viewpoint_manifold() ).on( robot ) << std::endl;
  //std::cout << "view : object loc= " << coordinate( 0,0,0 ).on( obj->object_manifold() ).on( robot ) << " axis= " << coordinate( 1,0,0 ).on( obj->object_manifold() ).on( robot ) << std::endl;
  //std::cout << "view : dist/orient= " << dao.dist << ", " << dao.orientation.radians() << std::endl;
  
  // Now, query the model at the location
  mean_variance_t mv;
  if( use_inependent_model_only ) {
    mean_variance_t corr_model;
    float p_indept;
    model->query_separate( dao, bel.mean, mv, corr_model, p_indept );
  } else {
    mv = model->query_no_update( dao, bel.mean );
  }

  return mv;
}

//=================================================================

detection_and_location_t viewpoint_t::create_detection_from_viewpoint( const tracked_object_tp& obj, const float& det_value, const manifold_tp& robot ) const
{
  // create dist/orient
  dist_and_orientation_t dao = get_dist_and_orientation( obj, robot );
  
  // Now create a detection object
  detection_and_location_t dx( det_value, dao );

  return dx;
}

//=================================================================

detection_and_location_t viewpoint_t::create_mean_detection_from_viewpoint( const tracked_object_tp& obj, const sensor_model_tp& sensor, const manifold_tp& robot ) const
{
  // create dist/orient
  dist_and_orientation_t dao = get_dist_and_orientation( obj, robot );

  // get the mean sensor model prediction
  mean_variance_t mv = sensor->query_no_update( dao, obj->belief().mean );
  
  // Now create a detection object
  detection_and_location_t dx( mv.mean, dao );
  
  return dx;

}

//=================================================================
//=================================================================
