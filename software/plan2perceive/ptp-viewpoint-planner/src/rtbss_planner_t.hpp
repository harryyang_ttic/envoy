
#if !defined( __PTP_PLANNER_rtbss_planer_t_HPP__ )
#define __PTP_PLANNER_rtbss_planer_t_HPP__

#include "viewpoint_planner_t.hpp"

namespace ptp {
  namespace planner {

    //==========================================================

    // Description:
    // A planner which uses RTBSS fowards search algorithm
    class rtbss_planner_t
      : public viewpoint_planner_t
    {
    public:

      // Description:
      // Created a new rtbss planner
      rtbss_planner_t( lcm_t* lcm);

    protected:

      // Description:
      // Replans
      void replan();

      // Description:
      // Add our own initialization
      void subclass_init();
      
    protected:
    };

    //==========================================================

  }
}

#endif

