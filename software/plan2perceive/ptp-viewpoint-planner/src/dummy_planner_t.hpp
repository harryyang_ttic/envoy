
#if !defined( __PTP_PLANNER_dummy_planer_t_HPP__ )
#define __PTP_PLANNER_dummy_planer_t_HPP__

#include "viewpoint_planner_t.hpp"

namespace ptp {
  namespace planner {

    //==========================================================

    // Description:
    // A planner which uses DUMMY fowards search algorithm
    class dummy_planner_t
      : public viewpoint_planner_t
    {
    public:

      // Description:
      // Created a new dummy planner
      dummy_planner_t( lcm_t* lcm);

    protected:

      // Description:
      // Replans
      void replan();
      
    protected:
    };

    //==========================================================

  }
}

#endif

