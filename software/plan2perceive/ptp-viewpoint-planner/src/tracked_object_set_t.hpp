
#if !defined( __PTP_PLANNER_tracked_object_set_t_HPP__ )
#define __PTP_PLANNER_tracked_object_set_t_HPP__

#include "tracked_object_t.hpp"
#include <ptp-object-detectors/object_detection_t.hpp>
#include <ptp-sensor-models/sensor_models_t.hpp>
#include "viewpoint_t.hpp"
#include "additive_perception_field_t.hpp"
#include <lcmtypes/ptplcm_object_initial_beliefs_t.h>
#include <vector>

namespace ptp {
  namespace planner {

    using namespace ptp::coordinates;
    using namespace ptp::sensormodels;

    //==================================================================
    
    class tracked_object_set_t;

    // Description:
    // Smart pointer typedef
    typedef boost::shared_ptr<tracked_object_set_t> tracked_object_set_tp;


    // Descritption:
    // A set of potential objects that are being tracked.
    // Each object has a belief associated with it as well as a 
    // location and type.
    class tracked_object_set_t
    {
    public:

      // Description:
      // Creates a new object set with given thresholds which is empty
      tracked_object_set_t( const float& dist_thresh,
			    const angle_t& orientation_thresh,
			    const bool& track_belief = false);

      // Description:
      // Set whertehr to use only the independent model or the full model
      // For RTBSS
      void use_inependent_model_only( const bool& f )
      { _use_independent_model_only = f; }

      // Description:
      // Adds the initial beliefs to this object set
      // It will begin tracking the objects in the given messag
      // if not already tracking them
      void add_initial_beliefs( const ptplcm_object_initial_beliefs_t* msg,
				const manifold_tp& world_man );

      // Description:
      // Updates the belief of the tracked objects with a given detection.
      // If the object is a new object, we will add it to the track set, 
      // otherwise the belief of it will be updated.
      // Returns the tracked_object_t structure for the detection
      tracked_object_tp update( const object_detection_t& det,
				const manifold_tp& robot );

      // Description:
      // Update the perception field
      void update_perception( const ptplcm_detector_info_t& info,
			      const manifold_tp& ref );

      // Description:
      // Set hte perception numx,numy resolution
      void set_perception_resolution( const size_t& num_x,
				      const size_t& num_y )
      { _perception_num_x = num_x;
	_perception_num_y = num_y; }

      // Description:
      // Returns a copy of the object set.
      // THis is a deep copy.
      tracked_object_set_tp clone() const;

      // Description:
      // Returns the object in the fielf ov view
      std::vector<tracked_object_tp> objects_in_fov( const ptplcm_detector_info_t& info, const manifold_tp& robot ) const;

      // Description:
      // IO operator
      friend std::ostream& operator<< (std::ostream& os, const tracked_object_set_t& a );

      // Description:
      // Print to stream, using locations in given manifold
      void print( std::ostream& os, const manifold_tp& robot ) const;

      // Description:
      // Returns the sensor model for a particular object
      // in this set
      sensor_model_tp sensor_model( const int& object_id ) const
      { return _sensor_models.object( object_id ); }

      // Description:
      // Returns the sie of this object set;
      size_t size() const
      { return _objects.size(); }

      // Description:
      // Returns the given object inside this set
      tracked_object_tp object( const int& i ) const
      { return _objects[i];  }

      // Description:
      // Samples a viewpoint for objects using the normalized perception
      // field to bias sampling
      viewpoint_t sample_viewpoint( const manifold_tp& ref ) const;

      // Description:
      // Returns the greedy best viewpoint using the perception field
      viewpoint_t greedy_best_viewpoint( const manifold_tp& ref, const int& obj ) const;

      // Description:
      // Returns all viewpoints which have nonzero weight using perception field
      std::vector<viewpoint_t> nonzero_viewpoint_set( const manifold_tp& ref,
						      std::vector<int>& object_index ) const;



      // Description:
      // dumps the normalized perception fields for all object
      // in files with given prefix
      void dump_norm_perception_fields( const std::string& prefix ) const;

      // Description:
      // Sumpds the additive perception field for all objects
      void dump_additive_perception_field( const std::string& filename,
					   const manifold_tp& ref ) const;

      // Descripition:
      // Draw the additive perception field
      void draw_additive_perception_field( bot_lcmgl_t* lcmgl ) const
      { _field.draw( lcmgl ); }

      // Description:
      // Dumps the additive perception field
      void dump_additive_perception_field_obj(const std::string& filename) const
      { _field.dump( filename ); }
      


      // Description:
      // Updates a particular object with a detection
      void update_tracked_object( tracked_object_tp obj,
				  const object_detection_t& det,
				  const manifold_tp& robot );

    protected:

      // Description:
      // Retrusn the tracked object for the detection
      tracked_object_tp find_or_create_object( const object_detection_t& det,
					       const manifold_tp& robot);

      
      // Description:
      // Flag to use only the independent model
      bool _use_independent_model_only;
      
      // Description:
      // The set of tracked objects
      std::vector<tracked_object_tp> _objects;



      // Description:
      // The sensor models for the object in this set
      sensor_models_t _sensor_models;

      // Description:
      // The distance between which objects are considered to be the same
      float _same_object_distance_threshold;
      
      // Description:
      // The orientation between which two objects are considered
      // the same
      angle_t _same_object_orientation_threshold;

      // Description:
      // Are we tracking belifs
      bool _track_belief;

      // Description:
      // the addivite perception field
      additive_perception_field_t _field;

      // Description:
      // The number of x,y block in perception field
      size_t _perception_num_x;
      size_t _perception_num_y;
    };

    
    //==================================================================

    // Description:
    // IO operator
    std::ostream& operator<< ( std::ostream& os, const tracked_object_set_t& a );

    //==================================================================


  }
}

#endif

