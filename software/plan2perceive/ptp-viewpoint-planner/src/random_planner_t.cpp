
#include "random_planner_t.hpp"
#include <bot_core/rand_util.h>


using namespace ptp;
using namespace ptp::common;
using namespace ptp::coordinates;
using namespace ptp::detectors;
using namespace ptp::sensormodels;
using namespace ptp::planner;

//=====================================================================

random_planner_t::random_planner_t( lcm_t* lcm, const float& t )
  : viewpoint_planner_t( lcm ), _belief_threshold( t )
{}


//=====================================================================

void random_planner_t::replan()
{
  
  // resize our checked vector to the number of objects being tracked
  _done_objects.resize( _tracked_objects->size() );
  
  // Pick first done object
  int idx = -1;
  for( size_t i = 0; i < _done_objects.size(); ++i ) {
    if( _done_objects[i] != 1 ) {

      // update flag with current bleief
      if( (_tracked_objects->object(i)->belief().mean > _belief_threshold) ||
	  (_tracked_objects->object(i)->belief().mean < 1 - _belief_threshold) ) {
	_done_objects[i] = 1;
	//_tracked_objects->ignore_object_updates( i );
	continue;
      }

      idx = i;
      break;
    }
  }
  
  // If all objects are done, continue along nominal traj
  if( idx == -1 ) {

    start_executing_trajectory( create_nominal_trajecory() );
    _may_replan = true;
    _going_towards_viewpoint = false;

  } else {
  
    // // Ok, choose a random viewpoint for object
    // // inside it's perception field
    // //dist_and_orientation_t dao = _tracked_objects->object(idx)->norm_perception_field()->sample_dao_within_field();
    
    // // Actually, sample a random distance/orientation for object
    // double x = 6 * bot_randf();
    // double y = -6  +  12 * bot_randf();
    
    // // Get the coordinates in the world reference
    // coordinate_t c = coordinate( x,
    // 				 y,
    // 				 0 ).on( _tracked_objects->object(idx)->object_manifold() ).on( _world_manifold );
    
    // // create a viewpoint
    // coordinate_t rc = coordinate( 0,0,0 ).on( _current_robot_frame.manifold ).on( _world_manifold );
    // viewpoint_t vp( c, 
    // 		    rad( atan2( c[1] - rc[1], 
    // 				c[0] - rc[0] ) ).from(angle_t::X_AXIS), 
    // 		    _world_manifold );

    // Ok, get teh set of nonzero viewpoint
    std::vector<int> object_index;
    std::vector<viewpoint_t> all_views = _tracked_objects->nonzero_viewpoint_set( _world_manifold, object_index );

    // filter the views by those which look at thee wanted object
    std::vector<viewpoint_t> views;
    for( size_t i = 0; i < all_views.size(); ++i ) {
      if( object_index[i] == idx ) {
	views.push_back( all_views[i] );
      }
    }

    // If no views, go on to nominal path
    if( views.size() < 1 ) {
      
      start_executing_trajectory( create_nominal_trajecory() );
      _may_replan = true;
      _going_towards_viewpoint = false;
      
    } else {
    
      // Pick on at random
      int idx = bot_irand( views.size() );
      viewpoint_t vp = views[ idx ];
      
      // craete trajectory and start it
      viewpoint_trajectory_tp traj( new viewpoint_trajectory_t() );
      traj->set_tracked_object_set( _tracked_objects );
      traj->set_robot_manifold( _current_robot_frame.manifold );
      traj->add_viewpoint( vp );
      traj->add_viewpoint( goal_viewpoint() );
      start_executing_trajectory( traj );
    }
  }
  
}

//=====================================================================

//=====================================================================
