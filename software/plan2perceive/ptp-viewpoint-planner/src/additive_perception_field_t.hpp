
#if !defined( __PTP_PLANNER_additive_perception_field_t_HPP__ )
#define __PTP_PLANNER_additive_perception_field_t_HPP__


#include "tracked_object_t.hpp"
#include "viewpoint_t.hpp"
#include <ptp-common-distributions/discrete_distribution_t.hpp>
#include <boost/unordered_map.hpp>
#include <bot_lcmgl_client/lcmgl.h>


namespace ptp {
  namespace planner {

    using namespace ptp::common;
    using namespace ptp::coordinates;
    using namespace ptp::detectors;
    using namespace ptp::distributions;

    //==================================================================

    struct bin_xyt {
      size_t x;
      size_t y;
      size_t obj;
    };
    

    
    // Description:
    // An additive perception field is a normalized perception field
    // which is a sumation of individual perception fields
    class additive_perception_field_t
    {
    public:

      // Description:
      // Creates a perception field, empty
      additive_perception_field_t();

      additive_perception_field_t( const additive_perception_field_t& a )
	: _objects( a._objects ),
	  _min_x( a._min_x ),
	  _max_x( a._max_x ),
	  _min_y( a._min_y ),
	  _max_y( a._max_y ),
	  _num_x( a._num_x ),
	  _num_y( a._num_y ),
	  _min_value( a._min_value ),
	  _max_value( a._max_value ),
	  _field_map( a._field_map ),
	  _distribution( a._distribution )
      {}

      // Description:
      // Updates using the given objects and 
      // given size
      void update( const std::vector<tracked_object_tp>& objects,
		   const float& min_x, const float& max_x,
		   const float& min_y, const float& max_y,
		   const size_t& num_x, const size_t& num_y,
		   const ptplcm_detector_info_t& info,
		   const manifold_tp& ref );
      
      // Description:
      // Samples a location from the perception field
      viewpoint_t sample_viewpoint(const manifold_tp& ref ) const;

      // Description:
      // Returns the single bests viewpoint according to field
      viewpoint_t best_viewpoint(const manifold_tp& ref, const int& obj) const;

      // Description:
      // Returns the set of viewpoints which have non-zero weight in 
      // the field.
      std::vector<viewpoint_t> nonzero_viewpoint_set( const manifold_tp& ref,
						      std::vector<int>& object_index) const;

      // Description:
      // Returns the field value at given point
      double field_at( const float& x, const float& y, const float& theta, const manifold_tp& ref  ) const;

      // Description:
      // Draw the field
      void draw( bot_lcmgl_t* lcmgl ) const;

      // Description:
      // Dump the field to file
      void dump( const std::string& filename ) const;

    protected:

      // Description:
      // Given a bin, returns a viewpoint for it
      viewpoint_t convert_bin_to_viewpoint( const bin_xyt& bin,
					    const manifold_tp& ref ) const;


      // Description:
      // The object set
      std::vector<tracked_object_tp> _objects;

      // Description:
      // The current size of field
      float _min_x;
      float _max_x;
      float _min_y;
      float _max_y;
      size_t _num_x;
      size_t _num_y;

      // Description:
      // The min and max field values
      float _min_value;
      float _max_value;
      
      // Description:
      // The built perception field
      boost::unordered_map< bin_xyt, double > _field_map;
      
      // Description:
      // The distribution for field
      discrete_distribution_t<bin_xyt> _distribution;
      
		   
    };

    //==================================================================

    // Description:
    // Hash function for bins
    size_t hash_value( const bin_xyt& a );
    
    bool operator==( const bin_xyt& a, const bin_xyt& b );
    bool operator<( const bin_xyt& a, const bin_xyt& b );

    //==================================================================


  }
}

#endif

