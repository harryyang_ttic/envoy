
#if !defined( __PTP_PLANNER_tracked_object_t_HPP__ )
#define __PTP_PLANNER_tracked_object_t_HPP__

#include <ptp-object-detectors/object_detection_t.hpp>
#include <ptp-sensor-models/sensor_model_t.hpp>
#include <lcmtypes/ptplcm_detector_info_t.h>
#include "normalized_perception_field_t.hpp"
#include <boost/optional.hpp>

namespace ptp {
  namespace planner {

    using namespace ptp::common;
    using namespace ptp::detectors;
    using namespace ptp::sensormodels;

    //===============================================================

    // Description:
    // A tacked object consists of a belief and a location
    class tracked_object_t
    {
    public:

      // Description:
      // Creates a new tracked object with given manifold
      // and initial belief
      tracked_object_t( const mean_variance_t& b,
			const sensor_model_tp& sensor,
			const manifold_tp& man,
			const int& object_id,
			const int& object_type )
	: _belief( b ),
	  _object_manifold( man ),
	  _object_id( object_id ),
	  _object_type( object_type ),
	  _belief_update_count(0)
      {update_perception_field(sensor);}


      // Description:
      // Copy constructor
      tracked_object_t( const tracked_object_t& t,
			const sensor_model_tp& sensor )
	: _belief( t.belief() ),
	  _object_manifold( t.object_manifold() ),
	  _object_id( t._object_id ),
	  _object_type( t._object_type ),
	  _belief_update_count( t._belief_update_count )
      {update_perception_field(sensor);}
      
      // Description:
      // Retursn the object id
      int object_id() const
      {	return _object_id; }

      // Description:
      // Returns the object type
      int object_type() const
      { return _object_type; }

      // Description:
      // Returns the current belied
      mean_variance_t belief() const
      { return _belief; }
      
      // Description:
      // Retursn the object's location
      coordinate_t location() const
      { return coordinate( 0,0,0 ).on( _object_manifold ); }

      // Description:
      // Returns the object's manifold
      manifold_tp object_manifold() const
      { return _object_manifold; }

      // Description:
      // Retursn the object's distance and oprientation
      // from a particular manifold
      dist_and_orientation_t 
      get_dist_and_orientation( const manifold_tp& robot_manifold ) const
      {
	return calculate_dist_and_orientation(_object_manifold, robot_manifold);
      }


      // Description:
      // set the belief
      void set_belief( const mean_variance_t& b,
		       const sensor_model_tp& sensor )
      { 
	_belief = b; 
	if( _belief_history ) {
	  (*_belief_history).push_back( b );
	}
	update_perception_field(sensor);
	++_belief_update_count;
      }


      // Description:
      // Turn on or off belief tracking
      void track_beliefs( const bool& t )
      {
	if( t && !_belief_history ) {
	  _belief_history = std::vector<mean_variance_t>();
	} else if( t == false ) {
	  _belief_history = boost::optional<std::vector<mean_variance_t> >();
	}
      }

      // Description
      // Returns the tracked beliefs (if any)
      boost::optional<std::vector<mean_variance_t> > get_belief_history() const
      { return _belief_history; }

      // Description:
      // Returns the perception field
      normalized_perception_field_tp norm_perception_field() const
      { return _field; }

      // Description:
      // Returns true iff the given detector can see this object
      bool in_fov( const ptplcm_detector_info_t* info,
		   const manifold_tp& robot ) const;

      // Description:
      // IO operator
      friend std::ostream& operator<< (std::ostream& os, const tracked_object_t& a );

      // Description:
      // Pritns this object to a stream, with locations in given manifold
      void print( std::ostream& os, const manifold_tp& robot ) const;


      // Description:
      // The number of tiem the belief has been set ( most likely the 
      // number of times this object has been 'ssen')
      int _belief_update_count;


    protected:

      // Description:
      // updates the normlaized perception field
      void update_perception_field( const sensor_model_tp& sensor );


      // Description:
      // The object id
      int _object_id;

      // Description:
      // the object tyep
      int _object_type;
      
      // Description:
      // The current belief
      mean_variance_t _belief;
      
      // Description:
      // The object's manifold
      // Here, foward is x axis
      manifold_tp _object_manifold;

      // Description:
      // If we want to tack the beliefs over time, this stores them
      boost::optional<std::vector<mean_variance_t> > _belief_history;

      // Description:
      // The normalized perception fiel for this object
      normalized_perception_field_tp _field;
    };

    // Description:
    // Smart poitner typedef
    typedef boost::shared_ptr<tracked_object_t> tracked_object_tp;
    
    //===============================================================

  }
}

#endif

