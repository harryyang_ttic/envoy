
#if !defined( __PTP_PLANNER_viewpoint_trajectory_t_HPP__ )
#define __PTP_PLANNER_viewpoint_trajectory_t_HPP__

#include "viewpoint_t.hpp"
#include "tracked_object_set_t.hpp"
#include <boost/optional.hpp>
#include <vector>


// some constanst costs
extern double OBJECT_TRUE_POSITIVE_COST[2];
extern double OBJECT_FALSE_POSITIVE_COST[2];
extern double OBJECT_COST_MULTIPLIER[2];

namespace ptp {
  namespace planner {

    using namespace ptp::coordinates;
    using namespace ptp::detectors;
    using namespace ptp::sensormodels;


    //=================================================================

    // Description:
    // A belief set with decision updates
    // This has the belief over a set of objects at a point in
    // time and is used to propaget belierfs forwards in time to
    // acertain decision costs
    struct belief_set_t
    {
      std::vector<boost::optional<mean_variance_t> > beliefs;
      
      void resize( const size_t& n )
      {
	beliefs.resize(n);
      }

      void reset()
      {
	for( size_t i = 0; i < beliefs.size(); ++i ) {
	  beliefs[i] = boost::optional<mean_variance_t>();
	}
      }
    };
    struct belief_set_updates_t
    {
      std::vector<boost::optional<double> > mean_updates;
      std::vector<boost::optional<double> > var_updates;

      // Description:
      // resize the vectors
      void resize( const size_t& n )
      {
	mean_updates.resize( n );
	var_updates.resize( n );
      }

      // Description:
      // Resets the means / vars
      void reset_means()
      {
	for( size_t i = 0; i < mean_updates.size(); ++i ) {
	  mean_updates[i] = boost::optional<double>();
	}
      }
      void reset_vars()
      {
	for( size_t i = 0; i < var_updates.size(); ++i ) {
	  var_updates[i] = boost::optional<double>();
	}
      }
    };

    //=================================================================

    // Description:
    // A Trajectory of viewpoints.
    // When used with a tracked object set, we can evaluate the
    // 'cost' of going through the trajectory ( so these can be future
    // potential viewpoints )
    //
    // The class needs a tracked object set to be set first, and then the
    // costs are lazily calculated only if the accessors are accessed.
    class viewpoint_trajectory_t
    {
    public:

      viewpoint_trajectory_t()
	: _use_independent_model_only( false )
      {}

      // Description:
      // Sets the tracked object list.
      // This will reset decision cached costs
      void set_tracked_object_set( const tracked_object_set_tp& objs );

      // Description:
      // Sets the current robot manifold
      // This will reset motion costs (not decision costs)
      void set_robot_manifold( const manifold_tp& robot );

      // Description:
      // Adds a viewpoint to the end of the trajectory
      // (this will reset all the cached costs)
      void add_viewpoint( const viewpoint_t& view );


      // Description:
      // Returns the size of the trajectory
      size_t size() const
      { return _viewpoints.size(); }
      
      // Description:
      // Returns the given viewpoint
      viewpoint_t viewpoint( const size_t& i ) const
      { return _viewpoints[i]; }
      
      
      // Description:
      // Returns the estimated motion cost
      // (Will lazily compute and cache)
      double estimated_motion_cost() const;

      // Description:
      // Returns the estimated decision cost
      // (Will lazily compute and cahce )
      // Note: right now this is the same as teh actual decision cost
      double estimated_decision_cost() const;
      
      // Description:
      // Returns teh estimated total cost
      // (Will lazily compute and cahce )
      double estimated_total_cost() const;

      // Description:
      // Returns the actual motion cost
      // THIS IS SLOW
      // (Will lazily compute and cahce )
      double actual_motion_cost() const;

      // Description:
      // Returns the actual decision cost
      // (Will lazily compute and cahce )
      double actual_decision_cost() const;
      
      // Description:
      // Returns the actual total cost
      // THIS IS SLOW (because of actual motion cost)
      double actual_total_cost() const;


      // Description:
      // Set the flag to compute costs using only the independent model
      // ( for RTBSS )
      void use_inependent_model_only( const bool& f )
      { _use_independent_model_only = f; }


    protected:

      // Description:
      // Resets the motion cached costs
      void reset_motion_cached_costs();

      // Description:
      // Resets the decision cached cost
      void reset_decision_cached_costs();


      // Description:
      // Computes the decision cost for a single object
      double compute_decision_cost_single_object( const int& index ) const;


      // Description:
      // Returns the decision cost given a particular object type
      // and a belief
      double decision_cost( const mean_variance_t& bel, const int& obj_type ) const;
      
      // Description:
      // Returns the mena update for a single object
      // along whole trajectory
      double compute_mean_update_single_object( const int& index ) const;

      // Description:
      // Returns the var update for a single object
      double compute_var_update_single_object( const int& index ) const;
      
      // Description:
      // Computes the mean update for a single object and singleviewpoint
      double compute_mean_update_single_object_single_view( const int& obj_index, const int& viewpoint_index ) const;
      
      // Description:
      // Computes var update single object single viewpoint
      double compute_var_update_single_object_single_view( const int& obj_index, const int& viewpoint_index ) const;
      
      // Description:
      // Computes the belief for a single obejct after seeing
      // up to the given viewpoint (includes the viewpoint )
      mean_variance_t compute_belief_at_viewpoint( const int& obj_index, const int& viewpoint_index ) const;

      // Description:
      // Computes the estimated motion cost for a viewpoint
      // This is the estiamted motion cost between thre previous viewpoint
      // and the given vviewpoint
      double compute_estimated_motion_cost_viewpoint( const int& viewpoint_idex ) const;

      // Description:
      // Computes the actual motion cost between previous viewpoint 
      // and given
      double compute_actual_motion_cost_viewpoint( const int& viewpoint_index ) const;

      // Description:
      // The viewpoints
      std::vector<viewpoint_t> _viewpoints;

      // Description:
      // The tracked object set
      tracked_object_set_tp _objects;

      // Description:
      // Flag to use the independent model only ( for RTBSS )
      bool _use_independent_model_only;

      // Description:
      // The currently set robot manifold
      manifold_tp _robot_manifold;
      
      // Description:
      // The actual total cost the trajectory
      mutable boost::optional<double> _actual_total_cost;
      
      // Description:
      // The decision and motion costs ( actual ) for the 
      // trajectory
      mutable boost::optional<double> _actual_decision_cost;
      mutable boost::optional<double> _actual_motion_cost;
      
      // Description:
      // The estimate motion cost for trajectory
      mutable boost::optional<double> _estimate_motion_cost;

      // Description:
      // The motion costs between each viewpoint.
      mutable std::vector<boost::optional<double> > _actual_viewpoint_motion_stubs;
      mutable std::vector<boost::optional<double> > _estimate_viewpoint_motion_stubs;

      // Description:
      // The decision cost means and variance update to belief
      mutable std::vector<belief_set_updates_t> _viewpoint_belief_update_stubs;
      mutable std::vector<belief_set_t> _viewpoint_belief_stubs;

    };

    // Description:
    // Trajectory smart pointer
    typedef boost::shared_ptr< viewpoint_trajectory_t > viewpoint_trajectory_tp;

    //=================================================================

  }
}

#endif

