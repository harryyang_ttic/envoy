
#if !defined( __PTP_PLANNER_viewpoint_planner_t_HPP__  )
#define __PTP_PLANNER_viewpoint_planner_t_HPP__


#include <lcm/lcm.h>
#include <lcmtypes/bot_core_pose_t.h>
#include <lcmtypes/erlcm_navigator_goal_msg_t.h>
#include <lcmtypes/ptplcm_object_detection_set_t.h>
#include <ptp-object-detectors/object_detection_t.hpp>
#include "tracked_object_set_t.hpp"
#include "viewpoint_trajectory_t.hpp"
#include "object_frame_t.hpp"
#include <bot_lcmgl_client/lcmgl.h>
#include <lcmtypes/erlcm_speech_cmd_t.h>



namespace ptp {
  namespace planner {

    using namespace ptp;
    using namespace ptp::sensormodels;
    using namespace ptp::detectors;


    //=================================================================

    // Description:
    // The Plan2Perceive viewpoint planner.
    // This will listen to object detections and update the plan
    // accordingly. 
    class viewpoint_planner_t
    {
    public:

      // Description:
      // Creates a viewpoint planner wit hthe given LCM objects
      viewpoint_planner_t( lcm_t* lcm );


      // Description:
      // Start the planner
      void start();
      
      // Description:
      // Stop the planner
      void stop();

      // Description:
      // Have we reached the goal
      bool done() const
      { return _done; }

    protected:
      
      // Description:
      // Processes a new detection
      // Returns the update object ( if any, may be null )
      tracked_object_tp process_detection( const ptplcm_object_detection_t* det );

      // Description:
      // Process a detection set
      void process_detection_set( const ptplcm_object_detection_set_t set );

      // Description:
      // Process any expected bu abset detections
      void process_absent_detections( const ptplcm_object_detection_set_t* set,
				      const std::vector<tracked_object_tp>& updated_objs );

      // Description:
      // Changes the current goal
      void change_goal();
      
      // Description:
      // Replans for a path to the currently set goal
      // and sends out the new plan
      virtual void replan();

      // Description:
      // Rturns true iff we are able to replan 
      // This means we are started, have a goal, and have reached
      // at least one viewpoint from last plan
      bool may_replan() const;


      // Description:
      // Begin to Execute the given trajectory
      // This will send LCM message to our navigator with
      // the first viewpoint in trajectory
      void start_executing_trajectory( const viewpoint_trajectory_tp& traj );


      // Description:
      // Returns the viewpoint for the current goal set
      viewpoint_t goal_viewpoint() const;

      // Description:
      // Returns a trajectory for  the nominal case 
      // ( which is a traj directly from current location to goal )
      viewpoint_trajectory_tp create_nominal_trajecory() const;

      // Description:
      // Samples a trajectory from the current tracked object set
      // The trajectory will end in the goal viewpoint.
      // The trajectory will be biased according to the object
      // perception fields.
      viewpoint_trajectory_tp sample_trajectory() const;

      // Description:
      // Stops/Starts the motion of the robot
      void stop_robot_motion() const;
      void start_robot_motion() const;


      // Description:
      // Creates an object detection proxy from a
      // detction and distance as well as an object and the
      // view manifold 
      object_detection_t create_object_detection( const detection_and_location_t& dx, const tracked_object_tp& obj, const manifold_tp& view_man ) const;


      // Description:
      // Initialize teh object
      void init( lcm_t* lcm );

      // Description:
      // The subclass init. This is called after our init and 
      // should be overridden in any subclass wants any changes
      virtual void subclass_init() { };
      
    protected:  // CLM handlers

      // Description:
      // Setup all LCM hoks needed
      void initialize_lcm_hooks( lcm_t* lcm );
      
      // Description:
      // Keeps a history of robot poses as object_Frame_t.
      // This will create an object_Frame_t froim the pose mssage
      void handle_pose_t( const bot_core_pose_t* msg );
      static 
      void handle_pose_t_wrapper(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
				 const char * channel __attribute__((unused)), 
				 const bot_core_pose_t * msg, 
				 void * user);

      // Description:
      // Handles a goal for the PTP planner ( not the navigator )
      void handle_ptp_goal_change( const erlcm_navigator_goal_msg_t* msg );
      static void handle_ptp_goal_change_wrapper( const lcm_recv_buf_t *rbuf __attribute__((unused)), 
						  const char * channel __attribute__((unused)), 
						  const erlcm_navigator_goal_msg_t * msg, 
						  void * user);


      // Description:
      // Handle a goal reached from the navigator/rrtstar
      void handle_navigator_goal_reached( const erlcm_speech_cmd_t* msg );
      static void handle_navigator_goal_reached_wrapper( const lcm_recv_buf_t *rbuf __attribute__((unused)), 
							 const char * channel __attribute__((unused)), 
							 const erlcm_speech_cmd_t * msg, 
							 void * user);
      
      // Description:
      // Handle an object detection set
      void handle_detection_set( const ptplcm_object_detection_set_t* msg );
      static void handle_detection_set_wrapper( const lcm_recv_buf_t *rbuf __attribute__((unused)), 
						const char * channel __attribute__((unused)), 
						const ptplcm_object_detection_set_t * msg, 
						void * user);


      // Description:
      // Handle the initial belief set
      void handle_initial_beliefs( const ptplcm_object_initial_beliefs_t* msg );
      static void handle_initial_beliefs_wrapper(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
						 const char * channel __attribute__((unused)), 
						 const ptplcm_object_initial_beliefs_t * msg, 
						 void * user);



    protected: // lcmGL
      
      // Description:
      // Draws the given sampled trajecoty.
      // This will NOT have cost information drawn on it
      void draw_sampled_trajectory( const viewpoint_trajectory_tp& traj ) const;
      
      // Description:
      // Draw the current goal
      void draw_goal() const;

      // Description:
      // Draw the planned and chosen trajecotry
      void draw_planned_trajectory() const;
      
      // Description:
      // Draw a detection that is being processed
      void draw_processing_detection( const object_detection_t& det ) const;

      // Description:
      // Draw the state of the current tracked oibject set
      void draw_tracked_object_set() const;

      // Description:
      // Draws the nonempty detections used so far
      void draw_nonempty_detection_history() const;

      // Description:
      // Publish the current belief
      void publish_object_set() const;

    protected: // state

      // Description:
      // flag saying whetehr we are 'done' and have reached our goal
      bool _done;

      // Description:
      // the lcm object
      lcm_t* _lcm;

      // Descripiton:
      // THe lcmGL client
      bot_lcmgl_t* _lcmgl;
      bot_lcmgl_t* _sample_lcmgl;
      bot_lcmgl_t* _field_lcmgl;
      
      // Description:
      // The crrent set of known objects
      tracked_object_set_tp _tracked_objects;

      // Description:
      // The current robot frame
      object_frame_t _current_robot_frame;

      // Description:
      // A history of robot frames
      object_frame_history_t _robot_frame_history;

      // Description:
      // The current goal for the ptp planner
      boost::optional<object_frame_t> _current_goal;

      // Description:
      // The time of the last send goal command
      int64_t _last_send_goal_timestamp;
      
      // Description:
      // The timestamp of teh last arrived goal
      int64_t _last_reached_goal_timestamp;

      // Description:
      // Whetehr this planner is astarted or not
      bool _started;

      // Description:
      // Flag determining whetehr we are going to a viewpoint
      // goal or not
      bool _going_towards_viewpoint;

      // Description:
      // The latest planned set of future viewpoints
      viewpoint_trajectory_tp _latest_planned_viewpoints;
      int _last_send_viewpoint_index;

      // Description:
      // The world manifold
      manifold_tp _world_manifold;
      
      // Description:
      // flag used to denote that a replan is able to be done
      bool _may_replan;

      ptplcm_detector_info_t _stored_detector_info;

      // Description:
      // The counter of all detection sets
      size_t _detection_set_counter;

      // Description:
      // All the detections used so far that had objects
      std::vector<object_detection_t> _nonempty_detections;
    };

    //=================================================================

  }
}

#endif

