
#if !defined( __PTP_PLANNER_perception_field_t_HPP__ )
#define __PTP_PLANNER_perception_field_t_HPP__


#include <ptp-sensor-models/sensor_model_t.hpp>

namespace ptp {
  namespace planner {

    using namespace detectors;
    using namespace sensormodels;

    //==================================================================
    
    // Description:
    // Computes the perception field
    // Or the expected mutual information of taking an
    // observation at a particular point.
    class perception_field_t
    {
    public:

      // Description:
      // Returns the perception field wieght for taking a 
      // particula observation at given point, having the
      // given object belief and sensor model
      static 
      double field_value_at( const dist_and_orientation_t& x,
			     const mean_variance_t& belief,
			     const sensor_model_tp& model );

    };

    //==================================================================

  }
}

#endif

