
#if !defined( __PTP_PLANNER_viewpoint_t_HPP__ )
#define __PTP_PLANNER_viewpoint_t_HPP__

#include <ptp-common-coordinates/angle.hpp>
#include <ptp-common-coordinates/manifold.hpp>
#include <ptp-object-detectors/common.hpp>
#include <ptp-sensor-models/sensor_model_t.hpp>
#include "tracked_object_t.hpp"

namespace ptp {
  namespace planner {

    using namespace ptp::coordinates;
    using namespace ptp::detectors;
    using namespace ptp::sensormodels;

    //====================================================================

    // Description:
    // A viewpoint contains a location ( and a manifold with teh x axis
    // towards teh viewpoint ).
    class viewpoint_t
    {
    public:
      
      // Description:
      // Creates a new viewpoint with a particular location
      // and angle from the given reference manifold
      viewpoint_t( const coordinate_t& view_position_on_ref,
		   const angle_t& view_angle_from_ref_x_axis,
		   const manifold_tp& refrence );


      // Description:
      // Returns teh viewpoint's manifold
      manifold_tp viewpoint_manifold() const
      { return _viewpoint_manifold; }
      
      // Description:
      // Returns the reference manifold
      manifold_tp reference_manifold() const
      { return _reference_manifold; }

      // Description:
      // Returns the observfation model for an object viewed by this
      // vewipoint
      mean_variance_t observation_model_at_viewpoint( const tracked_object_tp& obj, const sensor_model_tp& model, const mean_variance_t& belief, const manifold_tp& robot, const bool& use_inependent_model_only = false ) const;
      
      // Description:
      // Returns a distance and orientation of given object when viewed from
      // this viewpoint
      dist_and_orientation_t get_dist_and_orientation( const tracked_object_tp& obj, const manifold_tp& robot ) const;
      
      // Description:
      // Returns a new detection as if from this viewpoint with the
      // given detection value
      detection_and_location_t create_detection_from_viewpoint( const tracked_object_tp& obj, const float& det_value, const manifold_tp& robot ) const;


      // Description:
      // Returns the mean detection as if from this viewpoint 
      // for the given object
      detection_and_location_t create_mean_detection_from_viewpoint( const tracked_object_tp& obj, const sensor_model_tp& sensor, const manifold_tp& robot ) const;
      

    protected:

      // Description:
      // The manifold for this viewpoint, which has the x axis pointing
      // in the direction of the view
      manifold_tp _viewpoint_manifold;
      
      // Description:
      // The base reference manifold
      manifold_tp _reference_manifold;
      
    };

    //====================================================================


  }
}

#endif

