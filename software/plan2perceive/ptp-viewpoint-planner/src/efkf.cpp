


#include "efkf.hpp"

using namespace ptp::planner;
using namespace ptp::detectors;
using namespace ptp::sensormodels;

// The process noise for our filter
#define PROCESS_NOISE_R 0.0001


//======================================================================

double EFKF::update_mean_after_observation( const mean_variance_t& prior,
					    const mean_variance_t& obs_lik,
					    const detection_and_location_t& dx,
					    const sensor_model_tp& sensor,
					    const bool& use_independent_model_only )
{
  //std::cout << " up_mean : prior=N(" << prior.mean << "," << prior.var << ")  obs=N(" << obs_lik.mean << "," << obs_lik.var << ")" << std::endl;

  // create teh z vector for the exponential familu
  math_vector_t z_vector(2);
  z_vector(0) = dx.detection_value;
  z_vector(1) = z_vector(0) * z_vector(0);

  //std::cout << "z vec: " << z_vector << std::endl;

  // map from previous mean to theta space
  // HERE WE ASSUME THAT our obs_lik was given for the current
  // mean belief!!!
  math_vector_t mean_in_theta = map_to_theta( obs_lik );
  
  //std::cout << "mean_in_theta: " << mean_in_theta << std::endl;

  // Get teh variance and mean of obsercation as theta space
  matrix_t obs_var_in_theta = var_as_theta( obs_lik );
  math_vector_t obs_mean_in_theta = mean_as_theta( obs_lik );

  //std::cout << "obs_var:  " << obs_var_in_theta << std::endl;
  //std::cout << "obs_mean: " << obs_mean_in_theta << std::endl;
  
  // calculate projection of obsercvation onto theta space
  math_vector_t z_projection_to_theta = mean_in_theta - obs_var_in_theta.inverse() * ( obs_mean_in_theta - z_vector );

  //std::cout << "z project: " << z_projection_to_theta << std::endl;
  //std::cout << "obs_var_inv: " << obs_var_in_theta.inverse() << std::endl;

  // get th calman gain
  matrix_t K = kalman_gain( prior, obs_lik, dx.location, sensor, use_independent_model_only );

  //std::cout << "kalman: " << K << std::endl;
  
  // Ok, update the mean
  matrix_t mean_up = K * ( z_projection_to_theta - mean_in_theta );
  assert( mean_up.size() == 1 );
  double new_mean = prior.mean + mean_up( 0,0 );

  //std::cout << "mean update: " << mean_up << std::endl;
  //std::cout << "new mean: " << new_mean << std::endl;
  
  // return it
  return new_mean;
}

//======================================================================

math_vector_t EFKF::map_to_theta( const mean_variance_t& mv )
{
  math_vector_t t(2);
  t(0) = mv.mean / ( mv.var * mv.var );
  t(1) = -1.0 / (2 * mv.var * mv.var );
  return t;
}

//======================================================================

math_vector_t EFKF::mean_as_theta( const mean_variance_t& point )
{
  math_vector_t point_as_theta = map_to_theta( point );
  math_vector_t d(2);
  d(0) = - point_as_theta(0) / ( 2 * point_as_theta(1) );
  d(1) = ( point_as_theta(0)*point_as_theta(0) ) / ( 4 * point_as_theta(1)*point_as_theta(1) ) + 1.0 / ( 2 * point_as_theta(1) );
  return d;
}

//======================================================================

matrix_t EFKF::var_as_theta( const mean_variance_t& point )
{
  math_vector_t point_as_theta = map_to_theta( point );
  matrix_t dd( 2, 2 );
  dd( 0,0 ) = -1.0 / ( 2 * point_as_theta(1) );
  dd( 0,1 ) = point_as_theta(0) / ( 2 * point_as_theta(1)*point_as_theta(1) );
  dd( 1,0 ) = point_as_theta(0) / ( 2 * point_as_theta(1)*point_as_theta(1) );
  dd( 1,1 ) = -point_as_theta(0)*point_as_theta(0) / (2 * point_as_theta(1)*point_as_theta(1)*point_as_theta(1) ) - 1.0 / (2 * point_as_theta(1)*point_as_theta(1) );
  return dd;
}

//======================================================================

matrix_t EFKF::kalman_gain( const mean_variance_t& prior, 
			    const mean_variance_t& obs_lik,
			    const dist_and_orientation_t& x,
			    const sensor_model_tp& sensor,
			    const bool& use_independent_model_only )
{
  //std::cout << " kalman_gain : prior=N(" << prior.mean << "," << prior.var << ")  obs=N(" << obs_lik.mean << "," << obs_lik.var << ")" << std::endl;

  float process_noise_R = PROCESS_NOISE_R;
  double var = prior.var + process_noise_R;
  math_vector_t d_theta_state = sensor->gradient_of_theta_about( prior.mean, x, use_independent_model_only );
  matrix_t obs_var_in_theta = var_as_theta( obs_lik );
  
  matrix_t info = d_theta_state * ( var * d_theta_state.transpose() ) + obs_var_in_theta.inverse();
  matrix_t k = var * ( d_theta_state.transpose() * info.inverse() );
  return k;
}

//======================================================================

double EFKF::update_var_after_observation( const mean_variance_t& prior,
					   const mean_variance_t& obs_lik,
					   const dist_and_orientation_t& x,
					   const sensor_model_tp& sensor,
					   const bool& use_independent_model_only )
{
  //std::cout << " up_var : prior=N(" << prior.mean << "," << prior.var << ")  obs=N(" << obs_lik.mean << "," << obs_lik.var << ")" << std::endl;

  float process_noise_R = PROCESS_NOISE_R;
  math_vector_t d_theta_state = sensor->gradient_of_theta_about( prior.mean, x, use_independent_model_only );
 
  //std::cout << " up_var d_theta: " << d_theta_state << std::endl;
  
  double info_update = d_theta_state.dot( var_as_theta( obs_lik ) * d_theta_state );
  
  //std::cout << " up_var info: " << info_update << std::endl;
  
  double new_var = 1.0 / ( 1.0 / (prior.var + process_noise_R) + info_update );
  new_var = fabs( new_var );
  
  //std::cout << " up_var new_var: " << new_var << std::endl;
  
  return new_var;
}

//======================================================================

double EFKF::compute_single_step_variance_update( const mean_variance_t& prior,
						  const mean_variance_t& obs_lik,
						  const dist_and_orientation_t& x,
						  const sensor_model_tp& sensor,
						  const bool& use_independent_model_only)
{
  math_vector_t d_theta_state = sensor->gradient_of_theta_about( prior.mean, x, use_independent_model_only );
  double info_update = d_theta_state.dot( var_as_theta( obs_lik ) * d_theta_state );
  return info_update;
}

//======================================================================
