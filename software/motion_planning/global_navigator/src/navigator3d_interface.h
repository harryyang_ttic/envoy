/*********************************************************
 *
 * This source code is part of the Carnegie Mellon Robot
 * Navigation Toolkit (CARMEN)
 *
 * CARMEN Copyright (c) 2002 Michael Montemerlo, Nicholas
 * Roy, Sebastian Thrun, Dirk Haehnel, Cyrill Stachniss,
 * and Jared Glover
 *
 * CARMEN is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation; 
 * either version 2 of the License, or (at your option)
 * any later version.
 *
 * CARMEN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General 
 * Public License along with CARMEN; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, 
 * Suite 330, Boston, MA  02111-1307 USA
 *
 ********************************************************/


/** @addtogroup navigator libnavigator_interface **/
// @{

/** \file navigator_interface.h
 * \brief Definition of the interface of the module navigator.
 *
 * This file specifies the interface to subscribe the messages of
 * that module and to receive its data via ipc.
 **/

#ifndef NAVIGATOR3D_INTERFACE_H
#define NAVIGATOR3D_INTERFACE_H

#include "navigator3d_messages.h"

#ifdef __cplusplus
extern "C" {
#endif

  typedef struct {
    int num_lasers_to_use;
    int use_fast_laser;
    double map_update_radius;
    int map_update_obstacles;
    int map_update_freespace;
    double replan_frequency;
    int smooth_path;
    double waypoint_tolerance;
    double goal_size;
    double goal_theta_tolerance;
    int dont_integrate_odometry;
    int plan_to_nearest_free_point;
  } carmen_navigator_config_t;


  // deprecated - publish LCM navigator_goal_msg instead
  int carmen3d_navigator_set_goal(double x, double y, double theta, int use_theta);

  // deprecated - use LCM mission_control_msg with type MISSION_NAVIGATOR_CLEAR_GOAL
  int carmen3d_navigator_send_clear_goal_message();

#ifdef __cplusplus
}
#endif

#endif
// @}
