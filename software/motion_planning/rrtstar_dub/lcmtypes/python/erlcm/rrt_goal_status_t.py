"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

class rrt_goal_status_t(object):
    __slots__ = ["utime", "id", "status"]

    IDLE = 0x00
    ACTIVE = 0x01
    REACHED = 0x02
    FAILED = 0x03

    def __init__(self):
        self.utime = 0
        self.id = 0
        self.status = 0

    def encode(self):
        buf = BytesIO()
        buf.write(rrt_goal_status_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">qib", self.utime, self.id, self.status))

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != rrt_goal_status_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return rrt_goal_status_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = rrt_goal_status_t()
        self.utime, self.id, self.status = struct.unpack(">qib", buf.read(13))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if rrt_goal_status_t in parents: return 0
        tmphash = (0xf85e580e8d4e936) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if rrt_goal_status_t._packed_fingerprint is None:
            rrt_goal_status_t._packed_fingerprint = struct.pack(">Q", rrt_goal_status_t._get_hash_recursive([]))
        return rrt_goal_status_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

