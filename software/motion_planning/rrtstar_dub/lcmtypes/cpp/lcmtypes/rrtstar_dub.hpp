#ifndef __lcmtypes_rrtstar_dub_hpp__
#define __lcmtypes_rrtstar_dub_hpp__

#include "erlcm/rrt_tree_t.hpp"
#include "erlcm/rrt_command_t.hpp"
#include "erlcm/rrt_state_t.hpp"
#include "erlcm/rrt_environment_region_2d_t.hpp"
#include "erlcm/rrt_node_t.hpp"
#include "erlcm/ref_point_list_t.hpp"
#include "erlcm/rrt_goal_status_t.hpp"
#include "erlcm/ref_point_t.hpp"
#include "erlcm/rrt_status_t.hpp"
#include "erlcm/rrt_environment_t.hpp"
#include "erlcm/rrt_traj_t.hpp"

#endif
