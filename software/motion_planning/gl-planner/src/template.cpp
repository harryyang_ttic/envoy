#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>

#include <interfaces/map3d_interface.h>
#include <dp_planner/dp_planner.hpp>
#include <bot_lcmgl_client/lcmgl.h>
#include <er_lcmtypes/lcm_channel_names.h>

#ifdef __APPLE
#include <GL/gl.h>
#else
#include <GL/gl.h>
#endif

#define POSE_LIST_SIZE 10

using namespace dp_planner;

typedef struct
{
    BotParam   *b_server;
    lcm_t      *lcm;
    GMainLoop *mainloop;
    pthread_t  work_thread;

    //GList *pose_list;  
    erlcm_map_t global_map;
    //uint8_t *complete_global_map; 
    occ_map::FloatPixelMap *fvm; 
    occ_map::FloatPixelMap *utility;
    erlcm_navigator_goal_msg_t* curr_goal_msg;
    int clicked_map_goal_exists;
    int reached_clicked_goal;

    bot_core_pose_t *last_pose;
    int publish_all;
    int verbose; 
} state_t;

static void on_pose(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
			     const bot_core_pose_t * msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 
    if(s->last_pose !=NULL){
	bot_core_pose_t_destroy(s->last_pose);
    }
    s->last_pose = bot_core_pose_t_copy(msg);
    if(s->verbose){
	fprintf(stderr,"Pose : (%f,%f)\n",msg->pos[0], msg->pos[1]);//, rpy[2], vel[0] , vel[1]);
    }
}

//pthread
static void *track_work_thread(void *user)
{
    state_t *s = (state_t*) user;

    printf("obstacles: track_work_thread()\n");

    while(1){
	fprintf(stderr, " T - called ()\n");
	
	usleep(50000);
    }
}

void read_parameters_from_conf(state_t *s)
{
    BotParam *b_param = s->b_server; 
    double max_t_vel =  bot_param_get_double_or_fail(b_param, "robot.max_t_vel");
    double max_r_vel = bot_param_get_double_or_fail(b_param, "robot.max_r_vel");
  
    char **planar_lidar_names = bot_param_get_all_planar_lidar_names(b_param);
  
    if(planar_lidar_names) {
	for (int pind = 0; planar_lidar_names[pind] != NULL; pind++) {
	    fprintf(stderr, "Channel : %s\n", planar_lidar_names[pind]);
	}
    }
  
    g_strfreev(planar_lidar_names);
}

//doesnt do anything right now - timeout function
gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;
  
    //do the periodic stuff 
    if(s->verbose){
	fprintf(stderr, "Callback - Timeout\n");
    }
    //return true to keep running
    return TRUE;
}


int plan_to_goal(state_t *self){
    if(!self->last_pose || !self->curr_goal_msg)
        return -1;
    
    bot_lcmgl_t *lcmgl = bot_lcmgl_init(self->lcm, "new_person_tracks");
    
    //double start[3] = { 21, -7, 0 };
    //double goal[3] = { 24.8, -19, 0 };
    double start[3] = {self->last_pose->pos[0], self->last_pose->pos[1], self->last_pose->pos[2] };
    double goal[3] = { self->curr_goal_msg->goal.x, self->curr_goal_msg->goal.y, 0};

    std::vector<Waypoint2d> waypoints;
    self->utility = createUtilityMap(self->fvm, start);
    bool have_path = dpPathToGoal(self->fvm, start, goal, waypoints);

    fprintf(stderr, "Have Path : %d\n", have_path);

    
    lcmglColor3f(1.0, 0.0, 0.0);
    
   
    for (int i=0;i<waypoints.size();i++){
        Waypoint2d& wp = waypoints[i];
        fprintf(stderr,"[%d] : %f,%f\n", i, wp.x, wp.y);
        //if(have_path){
        if(i==0){
            lcmglBegin(GL_LINES);
            lcmglVertex3d( start[0], start[1], start[2] );
            lcmglVertex3d( wp.x, wp.y, 0 );
            lcmglEnd();
        }
        
        else if(i== waypoints.size()-1){
            lcmglBegin(GL_LINES);
            lcmglVertex3d( wp.x, wp.y, 0 );
            lcmglVertex3d( goal[0], goal[1], goal[2] );
            lcmglEnd();
        }
        else{
            Waypoint2d& wp_back = waypoints[i-1];
            lcmglBegin(GL_LINES);
            lcmglVertex3d( wp_back.x, wp_back.y, 0 );
            lcmglVertex3d( wp.x, wp.y, 0 );
            lcmglEnd();
        }
    }
    
    if(have_path){
        double s_pos[3] = {start[0], start[1], start[2] };
        lcmglCircle(s_pos, 0.2);

        for (int i=0;i<waypoints.size();i++){
            Waypoint2d& wp = waypoints[i];
            double l_pos[3] = {wp.x,wp.y,0};
            lcmglCircle(l_pos, 0.2);
        }

        double e_pos[3] = {goal[0], goal[1], goal[2] };
        lcmglCircle(e_pos, 0.2);
    }
        
    bot_lcmgl_switch_buffer (lcmgl);
}

static void navigator_goal_handler(const lcm_recv_buf_t *rbuf, const char *channel,
                                   const erlcm_navigator_goal_msg_t *msg, void *user)
{
    state_t *s = (state_t *) user;

    fprintf(stderr,"$$$$$$$$$$ Received Navigator Goal\n");
    if(s->curr_goal_msg != NULL){
        erlcm_navigator_goal_msg_t_destroy(s->curr_goal_msg);
    }

    s->curr_goal_msg = erlcm_navigator_goal_msg_t_copy(msg);
    s->reached_clicked_goal = 0;
    s->clicked_map_goal_exists = 1;

    plan_to_goal(s);
    
    /*carmen_point_t sim_rect_center;

    if(s->sim_rects_list){
        carmen_point_t g_sim_rect_center;
        g_sim_rect_center.x = s->sim_rects_list->xy[0];
        g_sim_rect_center.y = s->sim_rects_list->xy[1];
        g_sim_rect_center.theta = 0;
        
        sim_rect_center = carmen3d_map_global_to_map_coordinates(g_sim_rect_center, s->nav3d_map);
        }*/
}

void subscribe_to_channels(state_t *s)
{
    bot_core_pose_t_subscribe(s->lcm, "POSE", on_pose ,s);
    erlcm_navigator_goal_msg_t_subscribe(s->lcm, "NAV_GOAL", navigator_goal_handler, s);
}  


static void usage(char * funcName)
{
    printf("Usage: %s [options]\n"
	   "options are:\n"
	   "--help,        -h    display this message \n"
	   "--publish_all, -a    publish robot laser messages for all channels \n"
	   "--verbose,     -v    be verbose", funcName);
    exit(1);

}

//add map handler that takes in the wheelchair gridmap 
static void on_map_server_gridmap(const lcm_recv_buf_t *rbuf, const char *channel, 
                                  const erlcm_gridmap_t *msg, void *user)
{
    
    state_t *self = (state_t *)user; 
    //
    //pthread_mutex_lock (&self->global_map_gridmap_mutex);
    
    fprintf (stdout, "on map_server_gridmap before: global_map.map = %p\n", (void*) self->global_map.map);
    fprintf (stdout, "on map_server_gridmap before: global_map.complete_map = %p\n", (void*) self->global_map.complete_map);
    
    if (self->global_map.complete_map != NULL) {
        fprintf (stdout, "freeing global_map.complete_map\n");
        free(self->global_map.complete_map);
        self->global_map.complete_map = NULL;
    }
    if (self->global_map.map != NULL){
        free(self->global_map.map);
        self->global_map.map = NULL;
    }   

    fprintf (stderr,"Done freeing\n");
    
    carmen3d_map_uncompress_lcm_map(&(self->global_map), msg);
    
    fprintf (stdout, "on map_server_gridmap after: global_map.map = %p\n", (void*) self->global_map.map);
    fprintf (stdout, "on map_server_gridmap after: global_map.complete_map = %p\n", (void*) self->global_map.complete_map);


    /*double xyz0[3] = { -20, -20, 0 };
    double xyz1[3] = { 20, 20, self->global_map.config.resolution };
    double mpp[3] = { self->global_map.config.resolution, 
                      self->global_map.config.resolution, 
                      self->global_map.config.resolution };*/
    
    double xy0[2] = { -20, -20};
    double xy1[3] = { 20, 20};
    double mpp =  self->global_map.config.resolution;
    
    carmen3d_map3d_map_index_to_global_coordinates(&xy0[0], &xy0[1], self->global_map.midpt,
                                                   self->global_map.map_zero, 
                                                   self->global_map.config.resolution, 
                                                   0,  
                                                   0); 

    carmen3d_map3d_map_index_to_global_coordinates(&xy1[0], &xy1[1], self->global_map.midpt,
                                                   self->global_map.map_zero, 
                                                   self->global_map.config.resolution, 
                                                   self->global_map.config.x_size,  
                                                   self->global_map.config.y_size); 

    fprintf(stderr, "Map extent : %f,%f => %f,%f\n", 
            xy0[0], xy0[1], 
            xy1[0], xy1[1]);//, xyz1[2]);
    
    //occ_map::FloatVoxelMap fvm(xyz0, xyz1, mpp, 0);
    occ_map::FloatPixelMap *fvm = new occ_map::FloatPixelMap(xy0, xy1, mpp, 0);

    double radius = 0.5; 
    
    //this is filling stuff up 
    double ixy[3] = {.0,.0};//,.0};
    //for (ixyz[2] = ; ixyz[2] < 10; ixyz[2] += .2) {
    for (ixy[1] = xy0[1]; ixy[1] < xy1[1]; 
         ixy[1] += self->global_map.config.resolution) {
        for (ixy[0] = xy0[0]; ixy[0] < xy1[0]; ixy[0] += self->global_map.config.resolution) {
        
            carmen_point_t xyz_pt = {ixy[0], ixy[1], 0};
            int ig, jg;
            carmen3d_map3d_global_to_map_index_coordinates (xyz_pt, self->global_map.midpt, 
                                                            self->global_map.map_zero,
                                                            self->global_map.config.resolution, &ig, &jg);
            
            if ((ig < 0) || (ig > (self->global_map.config.x_size-1)) 
                || (jg < 0) || (jg > self->global_map.config.y_size-1))
                continue;

            //can dilate the map after

            if(self->global_map.map[ig][jg] > 0.7){
                for(double i = -radius; i <= +radius; i+= self->global_map.config.resolution){
                    for(double j = -radius; j <= +radius; j+= self->global_map.config.resolution){
                        double ixy_a[3] = {ixy[0] + i , ixy[1] + j};
                        if(hypot(i,j) > radius){
                            continue;
                        }
                        //distance 
                        fvm->writeValue(ixy_a, 0.99);
                        double dist_ratio = 1 - (hypot(i,j)/ radius); 
                    }
                }
            }
            
            if(self->global_map.map[ig][jg] == 0.5){
                fvm->writeValue(ixy, 0.99);
            }
        }
    }

    //there is a dilation function - but it doesnt seem to work properly
    self->fvm = fvm;

    //if(1){
    const occ_map_pixel_map_t *o_msg = self->fvm->get_pixel_map_t(bot_timestamp_now());
    fprintf(stderr,"\nPublishing Map\n");
    occ_map_pixel_map_t_publish(self->lcm, "PIXEL_MAP",  o_msg);

    if(o_msg)
        delete o_msg;
    //}
    
        
    fprintf(stderr,"Acquired gridmap\n");
}

void get_global_map(state_t *self)
{
    /* subscripe to map, and wait for it to come in... */
    erlcm_map_request_msg_t msg;
    msg.utime =  carmen_get_time()*1e6;
    msg.requesting_prog = "DP_PLANNER";

    
    erlcm_gridmap_t_subscribe(self->lcm, "MAP_SERVER", on_map_server_gridmap, self);    
    int sent_map_req = 0;
    fprintf(stderr,"Sent Request\n");
    while (self->global_map.map == NULL) {
        if(!sent_map_req){
            sent_map_req = 1;
            erlcm_map_request_msg_t_publish(self->lcm,"MAP_REQUEST_CHANNEL",&msg);
        }
        //sleep();
        lcm_handle(self->lcm);
    }
    fprintf(stderr,"Got Global Map\n");  
    
    //we should convert this because of row major/column major issue 
}

int 
main(int argc, char **argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));
    state->last_pose = NULL;
    state->curr_goal_msg = NULL;

    const char *optstring = "hav";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "publish_all", no_argument, 0, 'a' }, 
				  { "verbose", no_argument, 0, 'v' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    usage(argv[0]);
	    break;
	case 'a':
	    {
		fprintf(stderr,"Publishing all laser channels\n");
		break;
	    }
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
		state->verbose = 1;
		break;
	    }
	default:
	    {
		usage(argv[0]);
		break;
	    }
	}
    }

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
    state->b_server = bot_param_new_from_server(state->lcm, 1);

    memset(&state->global_map, 0, sizeof(erlcm_map_t));
    state->global_map.complete_map = NULL;
    state->global_map.map = NULL;

    get_global_map(state);

    //pthread_create(&state->work_thread, NULL, track_work_thread, state);

    subscribe_to_channels(state);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    read_parameters_from_conf(state);

    /* heart beat*/
    //g_timeout_add_seconds (1, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


