/*********************************************************
 *
 * This source code is part of the Carnegie Mellon Robot
 * Navigation Toolkit (CARMEN)
 *
 * CARMEN Copyright (c) 2002 Michael Montemerlo, Nicholas
 * Roy, Sebastian Thrun, Dirk Haehnel, Cyrill Stachniss,
 * and Jared Glover
 *
 * CARMEN is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation; 
 * either version 2 of the License, or (at your option)
 * any later version.
 *
 * CARMEN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General 
 * Public License along with CARMEN; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, 
 * Suite 330, Boston, MA  02111-1307 USA
 *
 ********************************************************/

#include <er_carmen/global.h>
#include <er_carmen/map_interface.h>

#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
#include <er_common/lcm_utils.h>
#include <lcmtypes/er_lcmtypes.h>

#include <assert.h>
#include "navigator3d.h"
#include <interfaces/map3d_interface.h>
#include "navigator3d_interface.h"
#ifndef NO_ZLIB
#include <zlib.h>
#endif


int carmen3d_navigator_set_goal(double x, double y, double theta, int use_theta)
{
  lcm_t *lcm = bot_lcm_get_global(NULL); //globals_get_lcm();

  erlcm_navigator_goal_msg_t msg;
  msg.goal.x = x;
  msg.goal.y = y;
  msg.goal.yaw = theta;

  // tbd - ignoring z,r,p for now
  msg.goal.z = 0;
  msg.goal.roll = 0;
  msg.goal.pitch = 0;

  msg.use_theta = use_theta;

  msg.utime = bot_timestamp_now();

  erlcm_navigator_goal_msg_t_publish(lcm, NAV_GOAL_CHANNEL, &msg);
  bot_glib_mainloop_detach_lcm(lcm);

  return 0;
}

int carmen3d_navigator_send_clear_goal_message()
{
  lcm_t *lcm = bot_lcm_get_global(NULL);//globals_get_lcm();

  erlcm_mission_control_msg_t mc_msg;
  mc_msg.type = ERLCM_MISSION_CONTROL_MSG_T_NAVIGATOR_CLEAR_GOAL;
  mc_msg.utime = bot_timestamp_now();
  erlcm_mission_control_msg_t_publish(lcm, MISSION_CONTROL_CHANNEL, &mc_msg);

  bot_glib_mainloop_detach_lcm(lcm);
  return 0;
}
