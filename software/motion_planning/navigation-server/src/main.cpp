#include <iostream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <getopt.h>
#include <libxml/tree.h>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <bot_core/bot_core.h>
#include <bot_param/param_client.h>
#include <bot_frames/bot_frames.h>
#include <lcmtypes/er_lcmtypes.h>
#include <er_common/path_util.h>
#include <bot_lcmgl_client/lcmgl.h>

using namespace std;

class Location {
public: 
    Location(string name, double _xyz[3], double _rpy[3]){
        name = name;
        memcpy(xyz, _xyz, 3 * sizeof(double));
        memcpy(rpy, _rpy, 3 * sizeof(double));
    }

    Location(xmlNodePtr root){
        memset(xyz, 0, 3 * sizeof(double));
        memset(rpy, 0, 3 * sizeof(double));

        bool failed = false;

        if(root->type == XML_ELEMENT_NODE ){
            xmlChar * tmp = xmlGetProp( root, ( const xmlChar* )( "name" ) );
            if( tmp != NULL ){
                name = string(( char* )( tmp ));
                xmlFree( tmp );
            }
            else{
                fprintf(stderr, "Error\n");
                failed = true;
            }
            
            tmp = xmlGetProp( root, ( const xmlChar* )( "pos" ) );
            if( tmp != NULL ){
                vector< string > data_strings;
                string pos(( char* )( tmp ));
                boost::split( data_strings, pos, boost::is_any_of( "," ) );
                //xyz[0] = atof(( char* )( tmp ));
                if(data_strings.size()== 3){
                    for(int i=0; i < data_strings.size(); i++){
                        xyz[i] = atof(data_strings[i].c_str());
                    }
                }
                else{
                    fprintf(stderr, "Error\n");
                    failed = true;
                }
                xmlFree( tmp );
            }
            else{
                fprintf(stderr, "Error\n");
                failed = true;
            }
            
            tmp = xmlGetProp( root, ( const xmlChar* )( "rpy" ) );
            if( tmp != NULL ){
                vector< string > data_strings;
                string orientation(( char* )( tmp ));
                boost::split( data_strings, orientation, boost::is_any_of( "," ) );
                //xyz[0] = atof(( char* )( tmp ));
                if(data_strings.size()== 3){
                    for(int i=0; i < data_strings.size(); i++){
                        rpy[i] = bot_to_radians(atof(data_strings[i].c_str()));
                    }
                }
                else{
                    fprintf(stderr, "Error\n");
                    failed = true;
                }                
                xmlFree( tmp );
            }
            else{
                fprintf(stderr, "Error\n");
                failed = true;
            }            
        }
        else{
            failed = true;
        }
    }

    void draw_location(bot_lcmgl_t *lcmgl, const BotTrans &global_to_local){
        BotTrans goal_to_global; 
        goal_to_global.trans_vec[0] = xyz[0];
        goal_to_global.trans_vec[1] = xyz[1];
        goal_to_global.trans_vec[2] = xyz[2];

        BotTrans goal1_to_global; 
        goal1_to_global.trans_vec[0] = xyz[0] + cos(rpy[2]);
        goal1_to_global.trans_vec[1] = xyz[1] + sin(rpy[2]);
        goal1_to_global.trans_vec[2] = xyz[2];

        BotTrans goal_to_local; 
        bot_trans_apply_trans_to( &global_to_local  , &goal_to_global, &goal_to_local);

        BotTrans goal1_to_local; 
        bot_trans_apply_trans_to( &global_to_local  , &goal1_to_global, &goal1_to_local);
        
        lcmglColor3f(1.0, 0.0,0.0);
        lcmglCircle(goal_to_local.trans_vec, 0.5);

        lcmglBegin(LCMGL_LINES);
        lcmglVertex3dv(goal_to_local.trans_vec);
        lcmglVertex3dv(goal1_to_local.trans_vec);
        lcmglEnd();

        bot_lcmgl_text(lcmgl, goal_to_local.trans_vec, name.c_str());
    }

    erlcm_navigator_goal_msg_t * get_lcm_message(BotTrans &global_to_local){
        erlcm_navigator_goal_msg_t *msg = (erlcm_navigator_goal_msg_t *) calloc(1, sizeof(erlcm_navigator_goal_msg_t));

        BotTrans goal_to_global; 
        goal_to_global.trans_vec[0] = xyz[0];
        goal_to_global.trans_vec[1] = xyz[1];
        goal_to_global.trans_vec[2] = xyz[2];

        bot_roll_pitch_yaw_to_quat(rpy, goal_to_global.rot_quat);

        BotTrans goal_to_local; 
        bot_trans_apply_trans_to( &global_to_local  , &goal_to_global, &goal_to_local);
        double rpy_l[3]; 
        bot_quat_to_roll_pitch_yaw(goal_to_local.rot_quat, rpy_l);

        msg->utime = bot_timestamp_now();
        msg->use_theta = 1;
        msg->goal.x = goal_to_local.trans_vec[0];
        msg->goal.y = goal_to_local.trans_vec[1];
        msg->goal.z = goal_to_local.trans_vec[2];
        msg->goal.roll = rpy_l[0];
        msg->goal.pitch = rpy_l[1];
        msg->goal.yaw = rpy_l[2];

        return msg;
    }

    void print(){
        fprintf(stdout, "%s : %f,%f - %f\n", name.c_str(), xyz[0], xyz[1], bot_to_degrees(rpy[2]));
    }

    string name;
    double xyz[3];
    double rpy[3];
};

class Navigator {

public:
    Navigator(lcm_t *_lcm, BotParam *_param = NULL, bool _verbose=false):verbose(_verbose), param(_param){
        lcm = _lcm;
        if(!param){
            param = bot_param_new_from_server(lcm, 1);
        }
        frames = bot_frames_get_global (lcm, param);
        lcmgl = bot_lcmgl_init (lcm, "NAV_GOAL_LOCATIONS");
    }

    int from_xml( xmlNodePtr root ){
        xmlNodePtr l1 = NULL;
        for( l1 = root->children; l1; l1 = l1->next ){
            if( l1->type == XML_ELEMENT_NODE ){
                if( xmlStrcmp( l1->name, ( const xmlChar* )( "location" ) ) == 0 ){
                    Location *loc = new Location(l1);
                    loc->print();
                    //nav.from_xml(l1);
                    locations.push_back(loc);
                }                    
            }
        }
        fprintf(stdout, "No of locations : %d\n", (int) locations.size());
        return 0;
    }

    vector<Location *> find_matched_locations(string name){
        vector<Location *> matches; 
        for(int i=0; i < locations.size(); i++){
            Location *loc = locations[i];
            if(!loc->name.compare(name)){
                matches.push_back(loc);
            }
        }
        return matches; 
    }
    
    void publish_goal_location(Location *loc){
        if(loc){
            BotTrans global_to_local;
            bot_frames_get_trans(frames, "global", "local", &global_to_local);
            erlcm_navigator_goal_msg_t *msg = loc->get_lcm_message(global_to_local);
            erlcm_navigator_goal_msg_t_publish(lcm, "NAV_GOAL_LOCAL", msg); //or send the global pose in the NAV_GOAL message
            erlcm_navigator_goal_msg_t_destroy(msg);
        }
    }

    void draw_locations(){
        BotTrans global_to_local;
        bot_frames_get_trans(frames, "global", "local", &global_to_local);

        lcmglPointSize(6);
        for(int i=0; i < locations.size(); i++){
            Location *loc = locations[i];
            loc->draw_location(lcmgl, global_to_local);
        }        
        bot_lcmgl_switch_buffer (lcmgl);

    }

    lcm_t * lcm;

protected:
    
    bool verbose;
    bot_core_pose_t slam_pose;
    BotParam *param;
    BotFrames *frames;
    vector<Location *> locations;
    bot_lcmgl_t *lcmgl;
};

void seed_from_xml(const char *filename, Navigator &nav){
    //read from the given xml file and seed the world 
    xmlDoc * doc = NULL;
    xmlNodePtr root = NULL;
    doc = xmlReadFile(filename, NULL, 0 );
    if( doc != NULL ){
        root = xmlDocGetRootElement( doc );
        if( root->type == XML_ELEMENT_NODE ){
            nav.from_xml(root);
            
        }
        xmlFreeDoc( doc );
    }
    return;
}

void shutdown_simulator(int x)
{
    if (x == SIGINT) {
        fprintf(stdout, "Exiting navigation-server\n");
        exit(1);
    }
}

static void feasibility_handler(const lcm_recv_buf_t *rbuf, const char *channel,
                                const erlcm_place_node_t *msg, void *user)
{
    Navigator *self = (Navigator *) user;
    fprintf(stderr, "Feasibility Check received\n");
    vector<Location *> results = self->find_matched_locations(msg->name); 
    fprintf(stdout, "Goal location name : %s\n", msg->name);
   
    erlcm_speech_cmd_t s_msg;
    s_msg.utime = bot_timestamp_now();
    if(results.size() > 0){
        fprintf(stdout, "Goal found\n");
        s_msg.cmd_type = "PATH_FEASIBLE_SAME_FLOOR";
        s_msg.cmd_property = msg->name;
    }
    else{
        fprintf(stdout, "Goal not found\n");
        s_msg.cmd_type = "PATH_FEASIBILITY_STATUS";
        s_msg.cmd_property = "UNKNOWN";
    }    
    erlcm_speech_cmd_t_publish(self->lcm, "FEASIBILITY_STATUS", &s_msg);
}

static void command_handler(const lcm_recv_buf_t *rbuf, const char *channel,
                                const erlcm_place_node_t *msg, void *user)
{
    Navigator *self = (Navigator *) user;
    fprintf(stderr, "Command received\n");
    vector<Location *> results = self->find_matched_locations(msg->name); 
    fprintf(stderr, "Goal location name : %s\n", msg->name);
    if(results.size() > 0){
        self->publish_goal_location(results[0]);
    }
    else{
        fprintf(stdout, "Goal not found\n");
    }  
}

//doesnt do anything right now - timeout function
gboolean heartbeat_cb (gpointer data)
{
    Navigator *s = (Navigator *)data;
  
    s->draw_locations();
    //return true to keep running
    return TRUE;
}

int main(int argc, char *argv[])
{
    setlinebuf(stdout);

    const char *optstring = "ax:p";
    char c;
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
                                  { "auto", no_argument, 0,'a' },
                                  { "xml", required_argument, 0,'x' },
                                  { 0, 0, 0, 0 } };

    char *file_name = NULL;
    bool auto_path = false;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
        case 'a':
            auto_path = true;
            break;        
        case 'x':
            file_name = strdup(optarg);
            break;
        default:
            break;
        }
    }

    GMainLoop *mainloop = g_main_loop_new( NULL, FALSE );
    lcm_t *lcm = bot_lcm_get_global(NULL);

    BotParam *b_param = bot_param_new_from_server(lcm, 1);
    
    Navigator nav(lcm);

    const char *base_prefix = getDataPath();   
    
    char path[1024];
    
    if(file_name){
        fprintf(stderr, "Loading from XML\n");
        if(auto_path){
            sprintf(path, "%s/map_locations/%s", base_prefix, file_name);  
            seed_from_xml(path, nav);
        }
        else{
            seed_from_xml(file_name, nav);
        }
    }

    erlcm_place_node_t_subscribe(lcm, "GOAL_NODE_NAME", feasibility_handler, &nav);
    erlcm_place_node_t_subscribe(lcm, "GOAL_NODE_CONFIRM", command_handler, &nav);

    bot_glib_mainloop_attach_lcm (lcm);
   
    /* Setup exit handler */
    signal(SIGINT, shutdown_simulator);

    /* heart beat*/
    g_timeout_add_seconds (1, heartbeat_cb, &nav);

    //adding proper exiting
    bot_signal_pipe_glib_quit_on_kill (mainloop);

    ///////////////////////////////////////////////
    g_main_loop_run(mainloop);

}
