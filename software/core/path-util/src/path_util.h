#ifndef __path_util__
#define __path_util__


#ifdef __cplusplus
extern "C" {
#endif


const char * getBasePath();
const char * getDataPath();
const char * getMapPath();
const char * getModelsPath();
const char * getConfigPath();


#ifdef __cplusplus
}
#endif


#endif
