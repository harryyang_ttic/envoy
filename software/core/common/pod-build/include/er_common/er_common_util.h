#ifndef __ER_COMMON_UTIL_H__
#define __ER_COMMON_UTIL_H__

#include <bot_core/bot_core.h>

#ifdef __cplusplus
extern "C" {
#endif


/**
 * er_common_get_unique_id
 *
 * Returns a random uint64_t
 */
    uint64_t er_common_get_unique_id ();

    uint32_t er_get_unique_id_32t ();

#ifdef __cplusplus
}
#endif

#endif
