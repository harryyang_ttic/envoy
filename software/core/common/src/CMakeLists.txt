add_definitions(
    #-ggdb3 
    -std=gnu99
    -msse2
)

# DO WE WANT THIS?
#pods_install_headers(lcm_utils.h DESTINATION lcm_utils)

file(GLOB c_files *.c)
file(GLOB h_files *.h)

file(GLOB cpp_files *.cpp)
file(GLOB hpp_files *.hpp)

include_directories(${GLIB2_INCLUDE_DIRS})

# Now for the remaining envoy common library
add_library(er-common SHARED ${c_files})

# Set the library API version. Increment every time the public API changes
set_target_properties(er-common PROPERTIES SOVERSION 1)

set(REQUIRED_LIBS bot2-core lcmtypes_er-lcmtypes geom-utils)

pods_use_pkg_config_packages(er-common ${REQUIRED_LIBS})

set(REQUIRED_LIBS_OPENCV bot2-core lcmtypes_er-lcmtypes geom-utils opencv)

target_link_libraries(er-common -ler-carmen)

pods_install_headers(${h_files} DESTINATION er_common)

pods_install_headers(${hpp_files} DESTINATION er_common)

pods_install_libraries(er-common)

pods_install_pkg_config_file(er-common
    LIBS -ler-common
    REQUIRES ${REQUIRED_LIBS}
    VERSION 0.0.1)

pods_install_libraries(er-common)