#include "full_laser.h"

typedef struct _state_t state_t;

struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    BotParam   *b_server;
    full_laser_state_t *full_laser; 
    BotFrames *frames;
    //bot_lcmgl_t *lcmgl;
};

static void full_laser_update_handler(int64_t utime, bot_core_planar_lidar_t *laser, void *user)
{
    state_t *state = (state_t *) user;
    bot_core_planar_lidar_t_publish(state->lcm, "SKIRT_FULL", laser);
}


int 
main(int argc, char **argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    state->lcm =  bot_lcm_get_global(NULL);

    state->full_laser = full_laser_init(state->lcm, 1441, &full_laser_update_handler, state);

    state->b_server = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->b_server);
    //state->lcmgl = bot_lcmgl_init(state->lcm,"full_laser_test");

    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }
    
    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}
