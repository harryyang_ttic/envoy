/*********************************************************
 *
 * This source code is part of the Carnegie Mellon Robot
 * Navigation Toolkit (CARMEN)
 *
 * CARMEN Copyright (c) 2002 Michael Montemerlo, Nicholas
 * Roy, Sebastian Thrun, Dirk Haehnel, Cyrill Stachniss,
 * and Jared Glover
 *
 * CARMEN is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation; 
 * either version 2 of the License, or (at your option)
 * any later version.
 *
 * CARMEN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General 
 * Public License along with CARMEN; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, 
 * Suite 330, Boston, MA  02111-1307 USA
 *
 ********************************************************/

#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <bot_frames/bot_frames.h>
#include <map>
#include <vector>
#include <gsl/gsl_sf_bessel.h>

#include <gsl/gsl_blas.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_randist.h>

#include <bot_lcmgl_client/lcmgl.h>
#include <GL/gl.h>

using namespace std;

#define DATA_CIRC_SIZE 80
#define POSE_LIST_SIZE 10
#define NORMAL_CONT 2.506628274 //sqrt(2 * pi)
typedef struct
{
    double distance;    //gaussian 
    double angle;       //von-mises
    double orientation; //von-mises
    double height;      //gaussian
    int activity;       //table??? - on/off?? 
    double l_prob;
} feature_t;

typedef struct
{
    double mu;
    double var;
} gaussian_params_t;

typedef struct
{
    double mu;
    double k;
} von_mises_params_t;

typedef struct
{
    double on; //needs to be <=1    
} table_params_t;

typedef struct
{
    int object_type;
    int activity_type; 
    gaussian_params_t distance_dist;
    von_mises_params_t angle_dist; 
    von_mises_params_t orientation_dist;
    gaussian_params_t height_dist;
    table_params_t activity_dist;
        
    //log prob for each term
    double l_dist_prob;
    double l_angle_prob;
    double l_orientation_prob;
    double l_height_prob;
    
    //example list??
    vector<feature_t> *examples;    
} object_distribution_t;


//typedef map<int, object_distribution_t*> obj_map_t;

typedef struct
{
    BotParam   *b_server;
    BotFrames *frames;
    lcm_t      *lcm;
    GMainLoop *mainloop;
    pthread_t  work_thread;
    bot_lcmgl_t *lcmgl;
    BotPtrCircular   *pose_circ;
    BotPtrCircular   *object_circ;

    GList *pose_list;     
    erlcm_object_list_t *object_list;
    bot_core_pose_t *last_pose;
    int publish_all;
    int verbose; 
    gsl_rng *rng;

    GHashTable *object_params;
    //obj_map_t *object_param_map;
    
} state_t;


feature_t sample_pose(state_t *s, object_distribution_t *o_dist);

//free function to clean up the circular buffer data 
void
circ_free_pose_data(void *user, void *p) {
    bot_core_pose_t *np = (bot_core_pose_t *) p; 
    bot_core_pose_t_destroy(np);
}

void
circ_free_object_data(void *user, void *p) {
    erlcm_object_list_t *np = (erlcm_object_list_t *) p; 
    erlcm_object_list_t_destroy(np);
}

void print_trans(const BotTrans * tran)
{
    double rpy[3];
    bot_quat_to_roll_pitch_yaw(tran->rot_quat, rpy);
    fprintf(stderr,"t=(%f %f %f) rpy=(%f,%f,%f)\n", tran->trans_vec[0], tran->trans_vec[1],
            tran->trans_vec[2], bot_to_degrees(rpy[0]), bot_to_degrees(rpy[1]), bot_to_degrees(rpy[2]));
}

// convenience function to get the bot's position in the local frame
static int
_frames_vehicle_pos_local (BotFrames *frames, double pos[3])
{
    double pos_body[3] = {1.0, 0, 0};

    double sensor_to_local[12];

    if(!bot_frames_get_trans_mat_3x4 (frames, "body",
                                      "local", 
                                      sensor_to_local)){
        fprintf(stderr,"\tError Getting trans\n");
    }
    else{
        fprintf(stderr,"\tGot trans\n");
    }
    BotTrans trans; 
    bot_frames_get_trans(frames, "body", "local", &trans);

    //body to test
    BotTrans v_trans; 
    //a point 1 m up from the 
    double v_pos[3] = {1,0,0};
    double v_quat[4] = {1,0,0,0};
    bot_trans_set_from_quat_trans(&v_trans, v_quat, v_pos);
    BotTrans dest; 
    //order of multiplication is important 

    // Frame transform (from frame A to B) , Pose in frame A, Pose in frame B) 
    bot_trans_apply_trans_to(&trans, &v_trans, &dest); 
    /*print_trans(&trans);
      print_trans(&v_trans);
      print_trans(&dest);*/

    //this does the actual frame transform
    return bot_frames_transform_vec (frames, "body", "local", pos_body, pos);
    
    /*
      if (!bot_frames_get_trans_mat_3x4_with_utime (self->frames, "VELODYNE",
      "local", lrc->utime,
      sensor_to_local)) {
      fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
      return;
      }

    */
}


void calculate_von_mises(int size, double *angles, double *mu, double *kappa){
    double sum_r = 0;
    double sum_c = 0;
    
    for(int i=0; i < size; i++){
        sum_r += cos(angles[i]);
        sum_c += sin(angles[i]);
    }
    
    double r = hypot(sum_r, sum_c)/ size;
    
    if(r < 0.53)
        *kappa = 2 * r + pow(r,3) + 5 * pow(r,5)/6;
    else if(r >=0.53 && r < 0.85)
        *kappa = -.4 + 1.39 * r + 0.43 / (1-r);
    else
        *kappa = 1/(pow(r,3) - 4 * pow(r,2) + 3 * r);

    *mu = atan2(sum_c, sum_r);
    fprintf(stderr, "Mu : %f Kappa : %f\n", *mu, *kappa);
}


void print_trans_frame(BotTrans trans, char *from, char *to){
    fprintf(stderr, "From %s - to %s : %f,%f,%f => %f,%f,%f,%f\n", from, to,
            trans.trans_vec[0], trans.trans_vec[1], trans.trans_vec[2],
            trans.rot_quat[0], trans.rot_quat[1], trans.rot_quat[2], trans.rot_quat[3]);
}

static void on_pose(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                    const bot_core_pose_t * msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 

    bot_ptr_circular_add (s->pose_circ,  bot_core_pose_t_copy(msg));

    /*if(s->last_pose !=NULL){
        bot_core_pose_t_destroy(s->last_pose);
    }
    s->last_pose = bot_core_pose_t_copy(msg);*/

    /*BotTrans sensor_to_body;
      bot_frames_get_trans_with_utime (s->frames, "SKIRT_FRONT", "body", msg->utime, 
      &sensor_to_body);

      BotTrans body_to_sensor;
      bot_frames_get_trans_with_utime (s->frames, "body", "SKIRT_FRONT", msg->utime, 
      &body_to_sensor);

      BotTrans body_to_local;
      bot_frames_get_trans_with_utime (s->frames, "body", "local", msg->utime, 
      &body_to_local);

      BotTrans local_to_sensor;
      bot_frames_get_trans_with_utime (s->frames, "local", "SKIRT_FRONT", msg->utime, 
      &local_to_sensor);

      BotTrans sensor_to_local;
      bot_frames_get_trans_with_utime (s->frames, "SKIRT_FRONT", "local", msg->utime, 
      &sensor_to_local);
    

    
      //the following transforms takes us from sensor to local (using sensor to body) 
      //and body_to_local
      //and then takes us back to body to local 
      //using body to sensor and sensor to local 

      BotTrans s_t_l;
      //gives sensor to local
      bot_trans_apply_trans_to(&body_to_local, &sensor_to_body, &s_t_l);

      //gives body to local
      BotTrans b_t_l;
      bot_trans_apply_trans_to( &s_t_l, &body_to_sensor,&b_t_l);

      print_trans_frame(b_t_l, "body", "local");
    
      print_trans_frame(body_to_local, "body", "local");

      //bot_trans_apply_trans_to(&sensor_to_body, &body_to_local, &s_t_l);

      print_trans_frame(sensor_to_local, "sensor", "local");
      print_trans_frame(s_t_l, "sensor", "local");
      print_trans_frame(local_to_sensor, "SKIRT_FRONT", "local");
      bot_trans_invert(&s_t_l);
      print_trans_frame(s_t_l, "SKIRT_FRONT", "local");*/
    /*fprintf(stderr, "From frame SKIRT - to Local : %f,%f,%f => %f,%f,%f,%f\n", 
      sensor_to_local.trans_vec[0], sensor_to_local.trans_vec[1], sensor_to_local.trans_vec[2],
      sensor_to_local.rot_quat[0], sensor_to_local.rot_quat[1], sensor_to_local.rot_quat[2], sensor_to_local.rot_quat[3]);

      fprintf(stderr, "From trans SKIRT - to Local : %f,%f,%f => %f,%f,%f,%f\n", 
      s_t_l.trans_vec[0], s_t_l.trans_vec[1], s_t_l.trans_vec[2],
      s_t_l.rot_quat[0], s_t_l.rot_quat[1], s_t_l.rot_quat[2], s_t_l.rot_quat[3]);*/
            
}

//pthread
static void *track_work_thread(void *user)
{
    state_t *s = (state_t*) user;

    printf("obstacles: track_work_thread()\n");

    while(1){
        double local_pos[3];

        _frames_vehicle_pos_local(s->frames, local_pos);
        if(s->verbose){
            fprintf(stderr, "Pose : %f,%f,%f\n", local_pos[0], local_pos[1], local_pos[2]); 
            
            fprintf(stderr, " T - called ()\n");
        }
        usleep(50000);
    }
}

//
/*
  double rvm (double mean, double k) 
{
    double result = 0.0;

    double a = 1.0 + sqrt(1 + 4.0 * (k * k));
    double b = (a - sqrt(2.0 * a))/(2.0 * k);
    double r = (1.0 + b * b)/(2.0 * b);

    while (1)
    {
	double U1 = gsl_ran_flat(rng, 0.0, 1.0);
	double z = cos(M_PI * U1);
	double f = (1.0 + r * z)/(r + z);
	double c = k * (r - f);
	double U2 = gsl_ran_flat(rng, 0.0, 1.0);
	
	if (c * (2.0 - c) - U2 > 0.0) 
	{
	    double U3 = gsl_ran_flat(rng, 0.0, 1.0);
	    double sign = 0.0;
	    if (U3 - 0.5 < 0.0)
		sign = -1.0;
	    if (U3 - 0.5 > 0.0)
		sign = 1.0;
	    result = sign * acos(f) + mean;
	    while (result >= 2.0 * M_PI)
		result -= 2.0 * M_PI;
	    break;
	}
	else 
	{
	    if(log(c/U2) + 1.0 - c >= 0.0) 
	    {
		double U3 = gsl_ran_flat(rng, 0.0, 1.0);
		double sign = 0.0;
		if (U3 - 0.5 < 0.0)
		    sign = -1.0;
		if (U3 - 0.5 > 0.0)
		    sign = 1.0;
		result = sign * acos(f) + mean;
		while (result >= 2.0 * M_PI)
		    result -= 2.0 * M_PI;
		break;
	    }
	}
    }
    return result;
}


 */

double sample_von_mises(state_t *s, von_mises_params_t param){
    double alpha = 0;
    if(param.k < 1e-6) //too small - assuming uniform 
        return 2 * M_PI * gsl_ran_flat(s->rng, 0.0, 1.0); //gsl_rng_uniform(s->rng); //rand no should be [0-1] however this function is [0,1)
    
    double a = 1 + pow((1+4 * pow(param.k,2)), 0.5);
    double b = (a - pow(2 *a, 0.5))/(2 * param.k);
    double r = (1 + b * b)/ (2*b);

    double u[3];

    //fprintf(stderr, "Kappa : %f Mu : %f\n", param.k, param.mu);
    double f = 0;
    while(1){
        u[0] = gsl_ran_flat(s->rng, 0.0, 1.0); //gsl_rng_uniform(s->rng);
        u[1] = gsl_ran_flat(s->rng, 0.0, 1.0);//gsl_rng_uniform(s->rng);
        u[2] = gsl_ran_flat(s->rng, 0.0, 1.0);//gsl_rng_uniform(s->rng);

        double z = cos(M_PI * u[0]);
        f = (1 + r * z)/(r + z);
        double c = param.k * (r - f);
        
        //fprintf(stderr, "\tRand %.3f, %.3f, %.3f\n", u[0], u[1], u[2]);
        if((u[1] < c * (2-c)) || !(log(c) - log(u[1])) + 1 - c < 0){
            //fprintf(stderr, "Breaking\n");
            break;
        }
    }

    int sign = 0;
    if((u[2] - 0.5) > 0){
        sign = +1;
    }
    else if((u[2] - 0.5) < 0){
        sign = -1;
    }   

    alpha = param.mu +  sign * acos(f);
    
    return alpha;
}

double sample_gaussian(state_t *s, gaussian_params_t param){
    double sigma = pow(param.var,0.5);
    return param.mu + gsl_ran_gaussian (s->rng, sigma);
}

feature_t sample_pose(state_t *s, object_distribution_t *o_dist){
    feature_t s_ft;
    s_ft.distance = sample_gaussian(s, o_dist->distance_dist);
    s_ft.angle = sample_von_mises(s, o_dist->angle_dist);
    s_ft.orientation = sample_von_mises(s, o_dist->orientation_dist);
    s_ft.height = sample_gaussian(s, o_dist->height_dist);

    fprintf(stderr, " Dist : %f Angle : %f Orientation : %f Height : %f\n", 
            s_ft.distance , bot_to_degrees(s_ft.angle), bot_to_degrees(s_ft.orientation), s_ft.height);
    return s_ft;
    //need to generate the robot's pose w.r.t to the object
}

void read_parameters_from_conf(state_t *s)
{
    BotParam *b_param = s->b_server; 
    double max_t_vel =  bot_param_get_double_or_fail(b_param, "robot.max_t_vel");
    double max_r_vel = bot_param_get_double_or_fail(b_param, "robot.max_r_vel");
  
    char **planar_lidar_names = bot_param_get_all_planar_lidar_names(b_param);
  
    if(planar_lidar_names) {
        for (int pind = 0; planar_lidar_names[pind] != NULL; pind++) {
            fprintf(stderr, "Channel : %s\n", planar_lidar_names[pind]);
        }
    }
  
    g_strfreev(planar_lidar_names);
}

double get_log_prob_normal(double val, gaussian_params_t param){
    if(param.var == 0 && param.mu == 0){
        fprintf(stderr, "Invalid normal param\n");
        return 1.0;
    }
    double sigma = pow(param.var,0.5);
    return log(2.50662827 * sigma) - 0.5 * pow((val - param.mu)/sigma,2);
}

double get_log_prob_von_mises(double val, von_mises_params_t param){
    double k = param.k;
    if(k > 100){ //this is pretty large
        k = 100;
    }
    return k * cos(val - param.mu) - log(2*M_PI * gsl_sf_bessel_I0(k));
}

feature_t get_feature_for_object(bot_core_pose_t *pose, erlcm_object_t *obj){
    feature_t ft;
    
    //bring person to object pose 
    double dx = pose->pos[0] - obj->pos[0];
    double dy = pose->pos[1] - obj->pos[1];

    double rpy_obj[3];
    bot_quat_to_roll_pitch_yaw(obj->orientation, rpy_obj);

    double c = cos(rpy_obj[2]);
    double s = sin(rpy_obj[2]);
    
    double x_o = dx * c + dy * s;
    double y_o = - dx *s + dy * c; 

    
    //this is fit to a log normal distribution 
    ft.distance = log(hypot(x_o, y_o));//pose->pos[0] - obj->pos[0],  pose->pos[1] - obj->pos[1]);
    ft.angle = atan2(y_o, x_o); //pose->pos[1] -  obj->pos[1], pose->pos[0] - obj->pos[0]);
    
    fprintf(stderr, "Pos in Object Frame : %f, %f - %f , %f\n", x_o, y_o, ft.distance, bot_to_degrees(ft.angle));

    double rpy_pose[3];
    bot_quat_to_roll_pitch_yaw(pose->orientation, rpy_pose);
    
    ft.orientation = bot_mod2pi(rpy_pose[2] - rpy_obj[2]);
    ft.height = pose->pos[2] - obj->pos[2];
    ft.activity = 0;
    return ft;
}

//get the pose from the sampled feature
void get_pose_from_features(state_t *s, feature_t s_ft, erlcm_object_t *obj, double pos[3], double rpy[3]){
    //double pos[3];
    //double rpy[3];
    rpy[0] = 0;
    rpy[1] = 0;

    double rpy_obj[3];
    bot_quat_to_roll_pitch_yaw(obj->orientation, rpy_obj);
    
    rpy[2] = bot_mod2pi(s_ft.orientation + rpy_obj[2]);
    pos[2] = s_ft.height + obj->pos[2];

    double dist =  exp(s_ft.distance);
    //robot position in the object's frame 
    double dx = dist * cos(s_ft.angle);
    double dy = dist * sin(s_ft.angle);

    pos[0] = obj->pos[0] + dx * cos(rpy_obj[2]) - dy * sin(rpy_obj[2]);
    pos[1] = obj->pos[1] + dx * sin(rpy_obj[2]) + dy * cos(rpy_obj[2]);
}

//calculate the likelihood of the data under the trained model
void update_prob(object_distribution_t *o_params){
    double l_dist_sum = 0;
    double l_angle_sum = 0;
    double l_orientation_sum = 0;
    double l_height_sum = 0;
    
    for(int i=0; i < o_params->examples->size(); i++){
        feature_t ft_c = o_params->examples->at(i);
        double l_dist = get_log_prob_normal(ft_c.distance, o_params->distance_dist); 
        double l_angle = get_log_prob_von_mises(ft_c.angle, o_params->angle_dist);
        double l_orientation = get_log_prob_von_mises(ft_c.orientation, o_params->orientation_dist);
        double l_height = get_log_prob_normal(ft_c.height, o_params->height_dist); 

        double l_sum = l_dist + l_angle + l_orientation + l_height;
        fprintf(stderr, "Prob : Dist : %f Angle : %f Orient : %f Height : %f => %f\n", 
                l_dist , l_angle , l_orientation , l_height, l_sum);

        l_dist_sum += l_dist;
        l_angle_sum += l_angle;
        l_orientation_sum += l_orientation;
        l_height_sum += l_height;
    }
    o_params->l_dist_prob = l_dist_sum;
    o_params->l_angle_prob = l_angle_sum;
    o_params->l_orientation_prob = l_orientation_sum;
    o_params->l_height_prob = l_height_sum;
    
    double l_overall_sum = l_dist_sum + l_angle_sum + l_orientation_sum + l_height_sum;
    fprintf(stderr, "\n\n Overall Prob : Dist : %f Angle : %f Orient : %f Height : %f => %f\n", 
                l_dist_sum , l_angle_sum , l_orientation_sum , l_height_sum, l_overall_sum);
}

object_distribution_t * update_distribution(state_t *s, int object_id, int activity_id, feature_t ft){ //add the feature to a list - dump it to an xml file as well??
    //find and add or update the distribution for object type for activity 
    
    //check the hash table - if found - update the distribution 
    //otherwise create a new one - and update 
    object_distribution_t *o_params = (object_distribution_t *) g_hash_table_lookup(s->object_params, &object_id);
    
    if(o_params == NULL){    
        fprintf(stderr, "\n--------------------\nCreating new param set - For Object : %d\n", object_id);
        object_distribution_t *new_obj = (object_distribution_t *) calloc(1, sizeof(object_distribution_t));
        new_obj->object_type = object_id;
        new_obj->examples = new vector<feature_t>;
        /*feature_t *ft_copy = (feature_t *)calloc(1, sizeof(feature_t));
          memcpy(ft_copy, &ft, sizeof(feature_t));*/
        new_obj->examples->push_back(ft);
        fprintf(stderr, "No of examples : %d\n", new_obj->examples->size());
        g_hash_table_insert(s->object_params, &new_obj->object_type, new_obj);
        o_params = new_obj;
    }
    else{
        fprintf(stderr, "\n------------------\nUpdating Params for Object type: %d\n", object_id);
        o_params->examples->push_back(ft);

        double d_sum = 0;
        double d_sq_sum = 0;
        
        double h_sum = 0;
        double h_sq_sum = 0;

        int size = o_params->examples->size();
        
        double *angles = (double *) calloc(o_params->examples->size(), sizeof(double));
        double *orient = (double *) calloc(o_params->examples->size(), sizeof(double));

        for(int i=0; i < o_params->examples->size(); i++){
                     
            feature_t ft_c = o_params->examples->at(i);
            
            fprintf(stderr, "[%d] Dist : %f Angle : %f Orientation : %f Height : %f\n", 
                    i, ft_c.distance, bot_to_degrees(ft_c.angle), 
                    bot_to_degrees(ft_c.orientation), ft_c.height); 

            d_sum += ft_c.distance;
            d_sq_sum += pow(ft_c.distance, 2);

            h_sum += ft_c.height;
            h_sq_sum += pow(ft_c.height, 2);

            angles[i] = ft_c.angle;
            orient[i] = ft_c.orientation;
        }

        o_params->distance_dist.mu = d_sum / size;
        o_params->distance_dist.var = d_sq_sum / (size) - pow(o_params->distance_dist.mu,2);

        fprintf(stderr, "Distance : - Mean : %f Sigma : %f\n", o_params->distance_dist.mu, pow(o_params->distance_dist.var, 0.5));

        o_params->height_dist.mu = h_sum / size;
        o_params->height_dist.var = 0.05;//h_sq_sum /(size) - pow(o_params->height_dist.mu,2);

        fprintf(stderr, "Height : - Mean : %f Sigma : %f\n", o_params->height_dist.mu, pow(o_params->height_dist.var, 0.5));
        double mu_angle, kappa_angle;
        fprintf(stderr, "Angle : -");
        calculate_von_mises(o_params->examples->size(), angles, &mu_angle, &kappa_angle);

        o_params->angle_dist.mu = mu_angle;
        o_params->angle_dist.k = kappa_angle;
        
        fprintf(stderr, "Orientation: - ");
        double mu_orientation, kappa_orientation;
        calculate_von_mises(o_params->examples->size(), orient, &mu_orientation, &kappa_orientation);

        o_params->orientation_dist.mu = mu_orientation;
        o_params->orientation_dist.k = kappa_orientation;

        fprintf(stderr, "No of examples : %d\n", o_params->examples->size());

        update_prob(o_params);        
    }

    return o_params;
    
    //s->object_param_map.insert(make_pair<0, new_obj>);
    /*vector< pair<int,int> >::iterator pit;
    pair <int, int> test_id_idx(nd->id, nnIdx[(l)]);
    pit = find ( matched_nodeid_pointidx.begin(),  matched_nodeid_pointidx.end(), test_id_idx);*/
}

double get_prob_of_feature(feature_t ft_c, object_distribution_t *o_params){
      double l_dist = get_log_prob_normal(ft_c.distance, o_params->distance_dist); 
      double l_angle = get_log_prob_von_mises(ft_c.angle, o_params->angle_dist);
      double l_orientation = get_log_prob_von_mises(ft_c.orientation, o_params->orientation_dist);
      double l_height = get_log_prob_normal(ft_c.height, o_params->height_dist);       

      double l_sum = l_dist + l_angle + l_orientation + l_height;
      return l_sum;
}

void calculate_features(state_t *s, const erlcm_affordance_tag_t *tag){
    /*
      for (int i=0; i<(bot_ptr_circular_size(app->front_laser_circ)); i++) {
                bot_core_planar_lidar_t *laser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(app->front_laser_circ, i);
                double time_diff = fabs(laser->utime - msg->utime)  / 1.0e6;

                if(time_diff< min_utime){
                    min_utime = time_diff;
                    close_ind = i;
                }
            }
            
      
     */
    fprintf(stderr, "Calculating features\n");
    
    int matched_ind = -1;
    double time_gap = 10000;
    
    //should check from the tail 
    for (int i=0; i<(bot_ptr_circular_size(s->pose_circ)); i++) {
        bot_core_pose_t *pose = (bot_core_pose_t *) bot_ptr_circular_index(s->pose_circ, i);
        if(fabs(tag->utime - pose->utime)/1.0e6 < time_gap){
            matched_ind = i;
            time_gap = fabs(tag->utime - pose->utime)/1.0e6;
        }
    }
    
    if(matched_ind < 0){
        fprintf(stderr, "Matching Pose not found\n");
        return;
    }

    fprintf(stderr, "Pose match : %d\n", matched_ind);

    bot_core_pose_t *m_pose = bot_core_pose_t_copy((bot_core_pose_t *) bot_ptr_circular_index(s->pose_circ, matched_ind));
    
    matched_ind = -1;
    time_gap = 10000;
    
    //should check from the tail 
    for (int i=0; i<(bot_ptr_circular_size(s->object_circ)); i++) {
        erlcm_object_list_t *olist = (erlcm_object_list_t *) bot_ptr_circular_index(s->object_circ, i);
        if(fabs(tag->utime - olist->utime)/1.0e6 < time_gap){
            matched_ind = i;
            time_gap = fabs(tag->utime - olist->utime)/1.0e6;
        }
    }

    if(matched_ind < 0){
        bot_core_pose_t_destroy(m_pose);
        fprintf(stderr, "Matching Object not found\n");
        return;
    }

    fprintf(stderr, "Object match : %d\n", matched_ind);
    erlcm_object_list_t *m_olist = (erlcm_object_list_t *) bot_ptr_circular_index(s->object_circ, matched_ind);
    
    bot_lcmgl_t *lcmgl = s->lcmgl;
    
    //process features
    for(int i=0; i < m_olist->num_objects; i++){
        erlcm_object_t *obj = &m_olist->objects[i];
        feature_t ft = get_feature_for_object(m_pose, obj);    
        object_distribution_t *o_params = update_distribution(s, obj->object_type, tag->activity, ft);

        //the sampling of poses should be done for the objects in the list 
        //sample some poses 
        fprintf(stderr, "Sampling Poses\n");       
        
        vector<feature_t> samples; 

        double max_prob = -1000000000;
        double min_prob = 1000000000;

        for(int i=0; i < 100; i++){
            feature_t s_ft = sample_pose(s,o_params);

            //get the prob for the features 
            s_ft.l_prob = get_prob_of_feature(s_ft, o_params);

            fprintf(stderr, "Prob : %f\n", s_ft.l_prob);
            if(s_ft.l_prob < min_prob){
                min_prob = s_ft.l_prob;
            }

            if(s_ft.l_prob > max_prob){
                max_prob = s_ft.l_prob;
            }

            samples.push_back(s_ft);
        }        

        for(int i=0; i < 100; i++){
            feature_t s_ft = samples.at(i);
            double v = (exp(s_ft.l_prob) - exp(min_prob)) / (exp(max_prob) - exp(min_prob));
            
            fprintf(stderr, "Val : %f Max : %f Min : %f V : %f\n", s_ft.l_prob, max_prob, min_prob, v);

            double pos[3], rpy[3];
            get_pose_from_features(s, s_ft, obj, pos, rpy);

            float *color = bot_color_util_jet(v);

            bot_lcmgl_color3f(s->lcmgl, color[0], color[1], color[2]);

            bot_lcmgl_point_size(s->lcmgl, 4);
            bot_lcmgl_line_width(s->lcmgl, 4);
            bot_lcmgl_begin(s->lcmgl, GL_LINES);
            
            bot_lcmgl_vertex3f(s->lcmgl, pos[0], pos[1], v);//pos[2]);
            double pos_p[3] = {pos[0] + 0.5 * cos(rpy[2]), pos[1] + 0.5 * sin(rpy[2]), v};//pos[2]};
            bot_lcmgl_vertex3f(s->lcmgl, pos_p[0], pos_p[1], pos_p[2]);
            bot_lcmgl_end(s->lcmgl);                        
        }        
    }

    bot_lcmgl_switch_buffer(s->lcmgl);
}


static void
on_affordance_tag(const lcm_recv_buf_t *rbuf, const char *channel,
               const erlcm_affordance_tag_t *msg, void *user)
{
    state_t *self = (state_t*)user;
    //call function that takes in the objects and the robot pose - for now both will be assumed to be in the local frame 
    //calculate the feature values - and update the distribution 
    
    calculate_features(self, msg);
}

static void
on_object_list(const lcm_recv_buf_t *rbuf, const char *channel,
               const erlcm_object_list_t *msg, void *user)
{
    state_t *self = (state_t*)user;

    /* copy lcm message data to local buffer and let draw update the display */
    //g_mutex_lock(self->mutex);
    bot_ptr_circular_add (self->object_circ, erlcm_object_list_t_copy(msg));
    
    /*if (self->object_list)
        erlcm_object_list_t_destroy(self->object_list);
        self->object_list = erlcm_object_list_t_copy(msg);*/
    //fprintf(stderr, "+");
    //g_mutex_unlock(self->mutex); 
    /* not sure whether viewer_request_redraw 
    * calls this renderer's draw function before 
    * returning. therefore, I am releaseing the 
    * mutex here to avoid possibility of a 
    * deadlock */
}

//doesnt do anything right now - timeout function
gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;
  
    //do the periodic stuff 
    if(s->verbose){
        fprintf(stderr, "Callback - Timeout\n");
    }
    //return true to keep running
    return TRUE;
}

void subscribe_to_channels(state_t *s)
{
    bot_core_pose_t_subscribe(s->lcm, "POSE", on_pose ,s);
    erlcm_object_list_t_subscribe(s->lcm, "OBJECT_LIST", on_object_list, s);
    erlcm_affordance_tag_t_subscribe(s->lcm, "AFFORDANCE_TAG", on_affordance_tag, s);
}  


static void usage(char * funcName)
{
    printf("Usage: %s [options]\n"
           "options are:\n"
           "--help,        -h    display this message \n"
           "--publish_all, -a    publish robot laser messages for all channels \n"
           "--verbose,     -v    be verbose", funcName);
    exit(1);

}

int 
main(int argc, char **argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));
    state->last_pose = NULL;
    state->pose_list = NULL;
    state->object_list = NULL;
    state->pose_circ = bot_ptr_circular_new (DATA_CIRC_SIZE,
                                             circ_free_pose_data, state);

    state->object_circ = bot_ptr_circular_new (DATA_CIRC_SIZE,
                                             circ_free_object_data, state);

    const char *optstring = "hav";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
                                  { "publish_all", no_argument, 0, 'a' }, 
                                  { "verbose", no_argument, 0, 'v' }, 
                                  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
            case 'h':
                usage(argv[0]);
                break;
            case 'a':
                {
                    fprintf(stderr,"Publishing all laser channels\n");
                    break;
                }
            case 'v':
                {
                    fprintf(stderr,"Verbose\n");
                    state->verbose = 1;
                    break;
                }
            default:
                {
                    usage(argv[0]);
                    break;
                }
        }
    }

    // Create and seed the random number generator
    const gsl_rng_type * T = gsl_rng_default;
    state->rng = gsl_rng_alloc (T);
    
    unsigned int seed;
    struct timeval tv;
    gettimeofday(&tv,0);
    seed = tv.tv_sec + tv.tv_usec;
    gsl_rng_set (state->rng, seed);
     
    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
    state->b_server = bot_param_new_from_server(state->lcm, 1);
    state->lcmgl = bot_lcmgl_init(state->lcm,"mode-learner");
    state->frames = bot_frames_get_global (state->lcm, state->b_server);

    //for each affordance - for each object?? 
    state->object_params = g_hash_table_new(g_int_hash,g_int_equal);
        
        //new obj_map_t();// map<int, object_distribution_t* >(); //map<int, map<int, int> > ();

    //pair<int, int> ind(1,1);// = make_pair(1,1);
    //state->object_param_map->insert(make_pair<ind , 2>);
    //pthread_create(&state->work_thread, NULL, track_work_thread, state);

    subscribe_to_channels(state);

    double test[] = {1.2000    ,1.4000    ,1.0000    ,1.8000    ,1.3000    ,1.5000};
    
    double mu, kappa;
    calculate_von_mises(6, test, &mu, &kappa);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    read_parameters_from_conf(state);

    /* heart beat*/
    g_timeout_add_seconds (1, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


