
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <glib.h>
#define _GNU_SOURCE
#include <getopt.h>

#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/om_object_list_t.h>
#include <lcmtypes/om_xml_cmd_t.h>
#include <er_common/er_common_util.h>

#if 1
#define ERR(...) do { fprintf(stderr, "[%s:%d] ", __FILE__, __LINE__); \
                      fprintf(stderr, __VA_ARGS__); fflush(stderr); } while(0)
#else
#define ERR(...) 
#endif

#if 1
#define DBG(...) do { fprintf(stdout, __VA_ARGS__); fflush(stdout); } while(0)
#else
#define DBG(...) 
#endif

#define SIM_OBJECTS_ERROR_DOMAIN (g_quark_from_string("SimObjects"))

#define OBJECTS_PUBLISH_HZ 20


 
typedef struct _sim_object_t {
    lcm_t     *lcm;
    GMainLoop *main_loop;
    guint     timer_id;
    guint     object_msg_timeout_id;
    guint     parse_xml_timeout_id;

    char *object_xml_filename;

    GMutex *mutex;

    int verbose;

    om_object_list_t object_list;

    GHashTable *objects;

    int use_dynamic;
    int use_lat_lon_theta; 
} sim_object_t;

GError *
process_xml_file (sim_object_t *self);

static gboolean
_g_int64_t_equal (gconstpointer v1,gconstpointer v2) {
    return (*(int64_t*)v1)==(*(int64_t*)v2);
}

static guint
_g_int64_t_hash (gconstpointer v) {
    // use bottom 4 bytes (should be most unique)
    return 0x00ffffffff&(*(int64_t *)v);
}

static void
objects_publish_object_list(sim_object_t *self)
{
    g_mutex_lock(self->mutex);

    GList *objects = g_hash_table_get_values (self->objects);
    
    int nobjects = g_list_length(objects);
    if (nobjects!=self->object_list.num_objects) {
        if (self->object_list.objects)
            free(self->object_list.objects);
        self->object_list.num_objects=nobjects;
        self->object_list.objects=calloc(nobjects,sizeof(om_object_t));
    }
    int64_t now = bot_timestamp_now();
    self->object_list.utime = now;
    int idx=0;
    for (GList *iter = objects; iter; iter=iter->next) {
        memcpy(&self->object_list.objects[idx++],iter->data,sizeof(om_object_t));
    }
    om_object_list_t_publish(self->lcm, "OBJECT_LIST", &self->object_list);
    g_list_free(objects);
    g_mutex_unlock(self->mutex);
}

static gboolean
send_object_list(gpointer data)
{
    sim_object_t *self = (sim_object_t*)data;
    g_assert(self);

    /*int64_t now = bot_timestamp_now();
    self->object_list.utime = now;

    if (self->use_dynamic)
        om_object_list_t_publish(self->lcm, "OBJECTS_UPDATE_SIM", &self->object_list);
    else
    om_object_list_t_publish(self->lcm, "OBJECT_LIST", &self->object_list);*/
    return TRUE;
}


static gboolean
on_timer(gpointer data)
{
    sim_object_t *self = (sim_object_t *)data;
    objects_publish_object_list(self);
    
    return TRUE;
}


static void
sim_object_destroy(sim_object_t *self)
{
    if (!self)
        return;

    //if (self->object_array)
    //    g_array_free(self->object_array, TRUE);

    if (self->objects)
        g_hash_table_destroy(self->objects);

    if (self->lcm)
        lcm_destroy(self->lcm);
    if (self->main_loop)
        g_main_loop_unref(self->main_loop);

    /*
    if (self->atrans)
        globals_release_atrans (self->atrans);
    */

    if (self->object_xml_filename)
        free (self->object_xml_filename);

    free(self);
}

static sim_object_t*
sim_object_create()
{
    sim_object_t *self = (sim_object_t*)calloc(1, sizeof(sim_object_t));
    if (!self) {
        ERR("Error: sim_object_create() failed to allocate self\n");
        goto fail;
    }

    self->object_xml_filename = NULL;

    g_thread_init(NULL);
    self->mutex = g_mutex_new();

    /*    self->atrans = globals_get_atrans();
    if (!self->atrans) {
        ERR("Error: sim_object_create() failed to get global atrans\n");
        goto fail;
        }*/


    /* main loop */
    self->main_loop = g_main_loop_new(NULL, FALSE);
    if (!self->main_loop) {
        ERR("Error: sim_object_create() failed to create the main loop\n");
        goto fail;
    }

    /* LCM */
    self->lcm = bot_lcm_get_global (NULL);//globals_get_lcm();

    bot_glib_mainloop_attach_lcm (self->lcm);
    if (!self->lcm) {
        ERR("Error: sim_object_create() failed to create the transmit-only lcm "
            "object\n");
        goto fail;
    }

    /* object list periodic timeout */
    self->object_msg_timeout_id = g_timeout_add(500, send_object_list, self);
    if (!self->object_msg_timeout_id) {
        ERR("Error: sim_object_create() failed to create the object message "
            "timeout\n");
        goto fail;
    }

    /* object list periodic timeout */
    self->timer_id = g_timeout_add(1000.0/OBJECTS_PUBLISH_HZ, on_timer, self);    
    
    self->objects = g_hash_table_new(_g_int64_t_hash,_g_int64_t_equal);

    if (!self->objects) {
        ERR("Error: dynamic_objects_create() failed to create the object array\n");
    }

    /*self->object_array = g_array_sized_new(FALSE ,
                                           TRUE 
                                           sizeof(om_object_t),
                                               100 );
    if (!self->object_array) {
        ERR("Error: sim_object_create() failed to create the object array\n");
        goto fail;
    }*/
    
    return self;
 fail:
    sim_object_destroy(self);
    return NULL;
}

static void
add_object(sim_object_t *self, int16_t type, //om_object_enum_t type,
           double x, double y, double z,
           double roll, double pitch, double yaw,
           double bb_min_x, double bb_min_y, double bb_min_z,
           double bb_max_x, double bb_max_y, double bb_max_z,
           int64_t object_id, char *label)
{
    g_assert(self && self->objects);//self->object_array);


    g_mutex_lock(self->mutex);
    om_object_t object;
    memset(&object, 0, sizeof(object));

    object.object_type = type;
    object.utime = bot_timestamp_now();

    object.label = strdup(label);

    if (object_id == -1){
        fprintf(stderr,"Need to add random value\n");
        object.id = er_common_get_unique_id(); 
    }
    else
        object.id = object_id;

    object.pos[0] = x;
    object.pos[1] = y;
    object.pos[2] = z;

    double rpy[3];
    rpy[0] = bot_to_radians(roll);
    rpy[1] = bot_to_radians(pitch);
    rpy[2] = bot_to_radians(yaw);
    bot_roll_pitch_yaw_to_quat(rpy, object.orientation);

    object.bbox_min[0] = bb_min_x;
    object.bbox_min[1] = bb_min_y;
    object.bbox_min[2] = bb_min_z;

    object.bbox_max[0] = bb_max_x;
    object.bbox_max[1] = bb_max_y;
    object.bbox_max[2] = bb_max_z;

    //g_array_append_val(self->object_array, object);
    om_object_t *my_object = NULL;
    my_object = om_object_t_copy(&object);
    g_hash_table_insert(self->objects, &my_object->id, my_object);

    g_mutex_unlock(self->mutex);
}

static void
on_xml_command(const lcm_recv_buf_t *rbuf, const char *channel,
               const om_xml_cmd_t *msg, void *user)
{
    sim_object_t *self = (sim_object_t*)user;

    if(msg->path != ""){       
    
        if(msg->cmd_type == OM_XML_CMD_T_WRITE_FILE ){
            g_mutex_lock(self->mutex);
        
            GList *objects = g_hash_table_get_values (self->objects);
    
            int nobjects = g_list_length(objects);
            if (nobjects!=self->object_list.num_objects) {
                if (self->object_list.objects)
                    free(self->object_list.objects);
                self->object_list.num_objects=nobjects;
                self->object_list.objects=calloc(nobjects,sizeof(om_object_t));
            }
            int64_t now = bot_timestamp_now();
            self->object_list.utime = now;
            int idx=0;
            for (GList *iter = objects; iter; iter=iter->next) {
                memcpy(&self->object_list.objects[idx++],iter->data,sizeof(om_object_t));
            }

            //now write to file based on the name 
            GString *result = 
                g_string_new ("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");

            //convert this to bot trans
            /*ATrans *atrans;
              atrans =  globals_get_atrans();
              arlcm_gps_to_local_t gps_to_local;*/
            double lat_lon_el[3];
            double theta;
            double rpy[3];
    
            g_string_append (result, "<objects>\n");

            int count = 0;
    
            // loop through the object list
    
            if (self->object_list.num_objects){
                for (int i=0; i<self->object_list.num_objects; i++) {
                    om_object_t *o = &(self->object_list.objects[i]);
                    if (o->id == -1)
                        continue;

                    g_string_append (result,"    <item type=\"object\">\n");
                    g_string_append_printf (result, 
                                            "        <id>%"PRId64"</id>\n", o->id);

                    g_string_append_printf (result, 
                                            "        <label>%s</label>\n", o->label);
                    g_string_append_printf (result, 
                                            "        <type id=\"object_type\">%d</type>\n", o->object_type);
                    g_string_append_printf (result, 
                                            "        <position id=\"local-frame\">%lf %lf %lf</position>\n", 
                                            o->pos[0], o->pos[1], o->pos[2]);

                    bot_quat_to_roll_pitch_yaw (o->orientation, rpy);
                    theta = 0;// rpy[2] - gps_to_local.lat_lon_el_theta[3];
 
                    //atrans_local_to_gps (atrans, o->pos, lat_lon_el, NULL);
                    lat_lon_el[2] = 0.0;

                    g_string_append_printf (result, 
                                            "        <position id=\"lat_lon_theta\">%lf %lf %lf</position>\n", 
                                            lat_lon_el[0], lat_lon_el[1], theta);

                    g_string_append_printf (result, 
                                            "        <orientation id=\"local-frame\">%lf %lf %lf %lf</orientation>\n", 
                                            o->orientation[0], o->orientation[1], o->orientation[2], o->orientation[3]);
        
                    g_string_append (result, "    </item>\n");
                    count += 1;
                }
            }
            fprintf(stdout,"Writing %d generic objects\n", count);
            //globals_release_atrans (atrans);        
            g_string_append (result, "</objects>\n");
            char *objects_state =  g_string_free (result, FALSE);
    
            char *filename = strdup(msg->path);
    
            GError *gerr = NULL;
            g_file_set_contents (filename, objects_state, -1, &gerr);
            free (objects_state);
            free(filename);
            g_mutex_unlock(self->mutex);
        }

        else if(msg->cmd_type == OM_XML_CMD_T_LOAD_FILE){
            //fprintf(stderr,"Currently Not loading file - add feature\n");

            //dump the old hash table and load from given file 
            g_mutex_lock(self->mutex);
            if (self->objects)
                g_hash_table_remove_all(self->objects);
            g_mutex_unlock(self->mutex);
            
            self->object_xml_filename = strdup(msg->path);

            if ((self->object_xml_filename != NULL) && (self->use_lat_lon_theta != 1)) {
                GError *gerr = process_xml_file (self);

                if (gerr) {
                    fprintf (stderr, "Error loading objects from XML file '%s': %s\n", 
                             self->object_xml_filename, gerr->message);
                    g_error_free (gerr);
                    sim_object_destroy (self);
                    exit(-1);
                }
                else{
                    fprintf(stderr,"Loaded\n");
                }
            }
            else{
                fprintf(stderr,"Incorrect file path\n");
            }
        }
        
    }
}

static void
on_objects_update(const lcm_recv_buf_t *rbuf, const char *channel,
               const om_object_list_t *msg, void *user)
{
    sim_object_t *self = (sim_object_t*)user;
    g_mutex_lock(self->mutex);
    
    for (int i = 0; i < msg->num_objects; i++) {
        om_object_t *object = &msg->objects[i]; 
        om_object_t *my_object = g_hash_table_lookup(self->objects, &object->id);

        if (self->verbose)
            fprintf (stdout,"Got request to update object with id = %"PRId64" : \n", object->id);

        if (!my_object) {
            // add object to hash table.
            my_object = om_object_t_copy(object);
            g_hash_table_insert(self->objects, &my_object->id, my_object);                        
            if (self->verbose)
                fprintf (stdout,"... Doesn't exist, adding object with id = %"PRId64" \n", my_object->id);
        }
        else {
            // update object if the update time is newer than the last access
            if (my_object->utime < object->utime) {
                om_object_t_destroy(my_object);
                my_object = om_object_t_copy(object);
                //free(my_object->lable
                //memcpy(my_object,object,sizeof(om_object_t));
                //does this copy the string also??

                if (self->verbose)
                    fprintf (stdout, "... Exists, updating object id = %"PRId64" \n", my_object->id);
            }
            else if (self->verbose)
                fprintf (stdout, "... Exists but utime is old. Ignoring update for object id = %"PRId64" \n", my_object->id);
        }
    }
    g_mutex_unlock(self->mutex);
}

static void
add_object_no_bb(sim_object_t *self, int16_t type, //om_object_enum_t type,
                 double x, double y, double z,
                 double roll, double pitch, double yaw,
                 int64_t object_id, char *label)
{
    fprintf(stderr,"adding Object");
    add_object(self, type, x, y, z, roll, pitch, yaw, 0, 0, 0, 0, 0, 0, object_id,label);
}

static void
add_object_bb_rwx(sim_object_t *self, int16_t type,//arlcm_object_enum_t type,
                  double x, double y, double z,
                  double roll, double pitch, double yaw,
                  int64_t object_id, char *label)
{
    GString *conf_path = g_string_new("");
    g_string_printf(conf_path, "object_rwx.%d", type);
    //RWXConfModel *model = rwx_conf_get_model(self->rwx_conf, conf_path->str);
    g_string_free(conf_path, TRUE);

    //need to add with bbox 

    /*if (model) {
        double bb_min[3], bb_max[3];
        rwx_conf_model_get_bbox(model, bb_min, bb_max);
        add_object(self, type, x, y, z, roll, pitch, yaw,
                   bb_min[0], bb_min[1], bb_min[2],
                   bb_max[0], bb_max[1], bb_max[2],
                   object_id);
    }
    else {*/
        /* no model for type, add without bbox */
    add_object(self, type, x, y, z, roll, pitch, yaw,
                   2.0, 2.0, 1.0,
                   3.0, 3.0, 2.0,
               object_id, label);
    //add_object_no_bb(self, type, x, y, z, roll, pitch, yaw, object_id);
    //}
}

static void
make_objects(sim_object_t *self)
{
    fprintf (stdout, "shouldn't be here\n");
    g_assert(self && self->objects);


    //add_object_bb_rwx(self, ARLCM_OBJECT_ENUM_T_LR3,
    //                  -13, 37, 0, 0, 0, 30, -1);
    add_object_bb_rwx(self, OM_OBJECT_T_TABLE,
                      6, 10, 0, 0, 0, -60, -1, "sachi's table");

    add_object_bb_rwx(self, OM_OBJECT_T_CHAIR,
                      6, 10.5, 0, 0, 0, -60, -1, "sachi's chair");
    /*add_object_bb_rwx(self, ARLCM_OBJECT_ENUM_T_DRUM_55GAL,
                      -5, 9, 0, 0, 0, 0, -1);
    add_object_bb_rwx(self, ARLCM_OBJECT_ENUM_T_DRUM_55GAL,
                      -4, 10, 0, 0, 0, 0, -1);
    add_object_bb_rwx(self, ARLCM_OBJECT_ENUM_T_DRUM_55GAL,
                      -4.5, 8.5, 0, 0, 0, 0, -1);

    add_object_bb_rwx(self, ARLCM_OBJECT_ENUM_T_OPERATOR,
                      -7, 36, 0, 0, 0, 290, -1);
    add_object_bb_rwx(self, ARLCM_OBJECT_ENUM_T_OPERATOR,
                      -9, 14, 0, 0, 0, 0, -1);

    add_object_bb_rwx(self, ARLCM_OBJECT_ENUM_T_FLATBED_TRAILER,
                      -14, 34.7, 1.0, 0, 0, 30, -1);

    //add_object_bb_rwx(self, ARLCM_OBJECT_ENUM_T_FLATBED_TRUCK,
    //                  -18, 35, 0.605, 0, 0, -90, -1);
    */      
    
    
    /* GList *objects = g_hash_table_get_values (self->objects);
    
    int nobjects = g_list_length(objects);
    
    fprintf(stderr,"No of Objects : %d\n", nobjects);

    self->object_list.num_objects = no_objects;
    self->object_list.objects = (om_object_t*)self->object_array->data;*/
}




static void
add_object_from_xml (sim_object_t *self, om_object_t *object)
{
    double rpy[3];
    bot_quat_to_roll_pitch_yaw (object->orientation, rpy);

    add_object_bb_rwx (self, object->object_type,
                       object->pos[0], object->pos[1], object->pos[2],
                       rpy[0]*180/M_PI, rpy[1]*180/M_PI, rpy[2]*180/M_PI,
                       object->id, object->label);                                      

    return;
}


typedef struct {
    sim_object_t *sim_object;
    int in_objects;
    int in_item;
    int in_id;
    int in_relative_to_id;
    int in_position;
    int in_orientation;
    int in_type;
    int in_label;
    int in_approach;
    int in_child_field;
    om_object_t *object;
    GQuark error_domain;
} ObjectsParseContext;

static void
_start_element (GMarkupParseContext *ctx, const char *element_name,
                const char **attribute_names, const char **attribute_values,
                void *user_data, GError **error)
{
    ObjectsParseContext *opc = user_data;

    if (!strcmp (element_name, "objects")) {
        if (! opc->in_objects) {
            opc->in_objects = 1;
            opc->in_item = 0;
            opc->in_id = 0;
            opc->in_position = 0;
            opc->in_orientation = 0;
            opc->in_type = 0;
            opc->in_label = 0;
            opc->in_approach = 0;
            opc->in_child_field = 0;
        } else {
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0, 
                    "Unexpected <objects> element");
        }
        return;
    }

    if (! opc->in_objects) {
        *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0, "Missing <objects> element");
        return;
    }
    g_assert (opc->in_objects);
    if (!strcmp (element_name, "item")) {
        if ( opc->object) {
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0, 
                    "Unexpected <item> element");
            return;
        }

        const char *item_type = NULL;

        for (int i=0; attribute_names[i]; i++) {
            if (!strcmp (attribute_names[i], "type")) {
                item_type = attribute_values[i];
            } else {
                *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0, 
                        "Unrecognized attribute \"%s\"", attribute_names[i]);
                return;
            }
        }
        if (!item_type) {
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0, 
                    "<item> element missing required type attribute");
            return;
        }

        if (!strcmp (item_type, "object"))
            opc->object = (om_object_t *) calloc (1, sizeof (om_object_t));
        else {
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0, 
                    "Unknown item type [%s]", item_type);
            return;
        }


        //opc->unit = cam_unit_manager_create_unit_by_id (opc->chain->manager, 
        //        unit_id);
        if (!opc->object) {
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0, 
                    "Unable to instantiate [%s]", item_type);
            return;
        }
        //cam_unit_set_preferred_format (opc->unit, pfmt, width, height, 
        //        fmt_name);

        //cam_unit_chain_insert_unit_tail (opc->chain, opc->unit);

        //free(fmt_name);
        opc->in_item = 1;
        return;
    }
    if (!strcmp (element_name, "id")) {
        if (opc->in_child_field || (!opc->object))  {
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0, 
                                  "Unexpected <%s> element", element_name);
            return;
        }
        opc->in_id = 1;
        opc->in_child_field = 1;
    }
    else if (!strcmp (element_name, "relative-to-id")) {
        if (opc->in_child_field || (!opc->object))  {
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0, 
                                  "Unexpected <%s> element", element_name);
            return;
        }
        opc->in_relative_to_id = 1;
        opc->in_child_field = 1;
    }

    else if (!strcmp (element_name, "type")) {
        if (opc->in_child_field || (!opc->object))  {
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0, 
                                  "Unexpected <%s> element", element_name);
            return;
        }
        opc->in_type = 1;
        opc->in_child_field = 1;
    }
    else if (!strcmp (element_name, "position")) {
        if (opc->in_child_field || (!opc->object )) {
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0, 
                                  "Unexpected <%s> element", element_name);
            return;
        }

        const char *ref_frame = NULL;
        for (int i=0; attribute_names[i]; i++) {
            if (!strcmp (attribute_names[i], "id")) {
                ref_frame = attribute_values[i];
                
                if (strcmp (ref_frame, "local-frame") == 0) {
                    
                } else {
                    // ignore for now,and hope there is a local.
                }
                
            } else {
                *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0, 
                                      "Unrecognized attribute \"%s\"", attribute_names[i]);
                return;
            }
        }
        if ((strcmp(ref_frame, "lat_lon_theta") == 0) && (opc->sim_object->use_lat_lon_theta == 1)) {
            opc->in_position = 1;
            opc->in_child_field = 1;
        }
        else if ((strcmp(ref_frame, "local-frame") == 0) && (opc->sim_object->use_lat_lon_theta == 0)) {
            opc->in_position = 1;
            opc->in_child_field = 1;
        }
    }
    else if (!strcmp (element_name, "orientation")) {
        if (opc->in_child_field || (!opc->object))  {
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0, 
                                  "Unexpected <%s> element", element_name);
            return;
        }
        opc->in_orientation = 1;
        opc->in_child_field = 1;
    }
    else if (!strcmp (element_name, "label")) {
        if (opc->in_child_field || (!opc->object))  {
            fprintf(stderr,"Hit : %d ", opc->in_child_field);
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0, 
                                  "Unexpected <%s> element", element_name);
            return;
        }
        opc->in_child_field = 1;
        opc->in_label = 1;
    }
    else if (!strcmp (element_name, "approach")) {
        if (opc->in_child_field || (opc->object))  {
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0, 
                                  "Unexpected <%s> element", element_name);
            return;
        }
        opc->in_approach = 1;
        opc->in_child_field = 1;
    }
}

static void
_end_element (GMarkupParseContext *ctx, const char *element_name,
              void *user_data, GError **error)
{
    ObjectsParseContext *opc = user_data;
    if (!strcmp (element_name, "id")) {
        opc->in_id = 0;
        opc->in_child_field = 0;
    }
    else if (!strcmp (element_name, "relative-to-id")) {
        opc->in_relative_to_id = 0;
        opc->in_child_field = 0;
    }
    else if (!strcmp (element_name, "position")) {
        opc->in_position = 0;
        opc->in_child_field = 0;
    }
    else if (!strcmp (element_name, "orientation")) {
        opc->in_orientation = 0;
        opc->in_child_field = 0;
    }
    else if (!strcmp (element_name, "type")) {
        opc->in_type = 0;
        opc->in_child_field = 0;
    }
    else if (!strcmp (element_name, "label")) {
        opc->in_label = 0;
        opc->in_child_field = 0;
    }
    else if (!strcmp (element_name, "approach")) {
        opc->in_approach = 0;
        opc->in_child_field = 0;
    }
    else if (!strcmp (element_name, "objects"))
        opc->in_objects = 0;
    else if (!strcmp (element_name, "item")) {
        if (opc->object) {
            // now that we've reached the closing tag, add the object
            add_object_from_xml (opc->sim_object, opc->object);
            om_object_t_destroy (opc->object);
            opc->object = NULL;
        }
        opc->in_item = 0;
    }
}

static void
_text (GMarkupParseContext *ctx, const char *text, gsize text_len, 
        void *user_data, GError **error)
{
    ObjectsParseContext *opc = user_data;
    char buf[text_len + 1];
    memcpy (buf, text, text_len);
    buf[text_len] = 0;
    int args_assigned = 0;
    if (opc->in_id) {
        int64_t item_id;
        args_assigned = sscanf (buf, "%"PRId64"", &item_id);
        if (args_assigned != 1) 
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                  "invalid value [%s] for object id",
                                  buf);
        else {
            if (opc->object)
                opc->object->id = item_id;
            else
                *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                      "unexpected value [%s]",
                                      buf);
        }
    }
    else if (opc->in_relative_to_id) {
        int64_t relative_to_id;
        args_assigned = sscanf (buf, "%"PRId64"", &relative_to_id);
        if (args_assigned != 1) {
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                  "invalid value [%s] for relative to id",
                                  buf);
        } else {
            if (opc->object)
                *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                      "Can't pass relative-to-id for objects");
            else
                *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                      "unexpected value [%s]",
                                      buf);
        }
    }

    else if (opc->in_label) {
        char label[text_len + 1];
        args_assigned = sscanf (buf, "%s", label);

        if (args_assigned != 1)
            //there is no label
            if(opc->object) 
                opc->object->label = "";
            else
                *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                      "invalid value [%s] for object label",
                                      buf);
        else {
            if(opc->object)
                opc->object->label = strdup(label);
            else
                *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                      "No Object [%s]",
                                      buf);
        }
    }

    else if (0) {
        double pos_x, pos_y, pos_z;
        args_assigned = sscanf (buf, "%lf %lf %lf", &pos_x, &pos_y, &pos_z);
        if (args_assigned != 3) 
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                  "invalid value [%s] for object id",
                                  buf);
        else {
            if (opc->object)  {
                opc->object->pos[0] = pos_x;
                opc->object->pos[1] = pos_y;
                opc->object->pos[2] = pos_z;
            }
            else
                *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                      "unexpected value [%s]",
                                      buf);
        }
    }
    else if (opc->in_position) {
        if (opc->sim_object->use_lat_lon_theta == 0) {
            double pos_x, pos_y, pos_z;
            args_assigned = sscanf (buf, "%lf %lf %lf", &pos_x, &pos_y, &pos_z);
            if (args_assigned != 3) 
                *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                      "invalid value [%s] for object id",
                                      buf);
            else {
                if (opc->object)  {
                    opc->object->pos[0] = pos_x;
                    opc->object->pos[1] = pos_y;
                    opc->object->pos[2] = pos_z;
                }
                else
                    *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                          "unexpected value [%s]",
                                          buf);
            }
        }
        else {

            double lat, lon, theta;
            args_assigned = sscanf (buf, "%lf %lf %lf", &lat, &lon, &theta);
            fprintf (stdout, "reading %s\n", buf);
            if (args_assigned != 3) 
                *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                      "invalid value [%s] for object id",
                                      buf);
            else {
                double gps[] = {lat, lon, 0};
                //ATrans *atrans = globals_get_atrans();
                //Sachi - figure out 
                
                /* (!(opc->sim_object->atrans)) 
                    *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                          "invalid value [%s] for object id",
                                          buf);
                
                double xyz[3];
                if (!atrans_gps_to_local (opc->sim_object->atrans, gps, xyz, NULL))
                    *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                          "atrans invalid value [%s] for object id",
                                          buf);*/

                double xyz[3] = {.0,.0,.0};
                
                fprintf (stdout, "(lat, lon) = (%.6f, %.6f) to (x,y) = (%.10f, %.10f)\n",
                         gps[0], gps[1], xyz[0], xyz[1]);

                double rpy[] = {0, 0, theta};
                double quat[4];
                bot_roll_pitch_yaw_to_quat (rpy, quat);
                if (opc->object)  {
                    opc->object->pos[0] = xyz[0];
                    opc->object->pos[1] = xyz[1];
                    opc->object->pos[2] = 0;

                    opc->object->orientation[0] = quat[0];
                    opc->object->orientation[1] = quat[1];
                    opc->object->orientation[2] = quat[2];
                    opc->object->orientation[3] = quat[3];
                }
                else
                    *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                          "unexpected value [%s]",
                                          buf);
            }
        }
    }
    else if (opc->in_orientation) {
        double orientation_0, orientation_1, orientation_2, orientation_3;
        args_assigned = sscanf (buf, "%lf %lf %lf %lf", &orientation_0, &orientation_1, &orientation_2, &orientation_3);
        if (args_assigned != 4) 
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                  "invalid value [%s] for object id",
                                  buf);
        else if (opc->sim_object->use_lat_lon_theta == 0) {
            if (opc->object)  {
                opc->object->orientation[0] = orientation_0;
                opc->object->orientation[1] = orientation_1;
                opc->object->orientation[2] = orientation_2;
                opc->object->orientation[3] = orientation_3;
            }
            else
                *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                      "unexpected value [%s]",
                                      buf);
        }
    }
    else if (opc->in_type) {
        int item_type;
        args_assigned = sscanf (buf, "%d", &item_type);
        if (args_assigned != 1) 
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                  "invalid value [%s] for object id",
                                  buf);
        else {
            if(opc->object)
                opc->object->object_type = item_type;
            else
                *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                      "No object [%s]",
                                      buf);
        }
    }
    else if (opc->in_approach) {
        double lat, lon, theta;
        args_assigned = sscanf (buf, "%lf %lf %lf", &lat, &lon, &theta);
        if (args_assigned != 3) 
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                  "invalid value [%s] for object id",
                                  buf);
        else {
            *error = g_error_new (SIM_OBJECTS_ERROR_DOMAIN, 0,
                                  "unexpected value [%s]",
                                  buf);
        }
    }
}

static void
_parse_error (GMarkupParseContext *ctx, GError *error, void *user_data)
{
    fprintf (stderr, "parse error?\n");
    fprintf (stderr, "%s\n", error->message);
    fprintf (stderr, "===\n");
}



static GMarkupParser _parser = {
    .start_element = _start_element,
    .end_element = _end_element,
    .text = _text,
    .passthrough = NULL,
    .error = _parse_error
};

void
sim_objects_load_from_str (sim_object_t *self, const char *xml_str,
                           GError **error)
{
    ObjectsParseContext opc = {
        .sim_object = self,
        .in_objects = 0,
        .in_item = 0,
        .in_id = 0,
        .in_position = 0,
        .in_orientation = 0,
        .in_type = 0,
        .in_label = 0,
        .in_approach = 0,
        .in_child_field = 0,
        .object = NULL,
        .error_domain = SIM_OBJECTS_ERROR_DOMAIN,
    };

    GMarkupParseContext *otx = g_markup_parse_context_new (&_parser,
            0, &opc, NULL);

    GError *parse_err = NULL;
    g_markup_parse_context_parse (otx, xml_str, strlen (xml_str), &parse_err);

//    int result = parse_err ? -1 : 0;
    if (parse_err) {
        if (error) {
            *error = parse_err;
        } else {
            g_error_free (parse_err);
        }
    }

    g_markup_parse_context_free (otx);  

    //self->object_list.num_objects = self->object_array->len;
    //self->object_list.objects = (om_object_t*)self->object_array->data;

}

GError *
process_xml_file (sim_object_t *self)
{
    char *text = NULL;
    GError *gerr = NULL;

    if (g_file_get_contents (self->object_xml_filename, &text, NULL, &gerr)) {
        fprintf (stdout, "Loading objects from XML file '%s'\n", self->object_xml_filename);
        sim_objects_load_from_str (self, text, &gerr);
    }

    return gerr;
}

static gboolean
parse_xml(gpointer data)
{
    sim_object_t *self = (sim_object_t*) data;

    //sachi - figure out 
    /*if (atrans_have_gps_to_local (self->atrans) == 0)
        return TRUE;
    */

    GError *gerr = process_xml_file (self);

    if (gerr) {
        fprintf (stderr, "Error loading objects from XML file '%s': %s\n", 
                 self->object_xml_filename, gerr->message);
        g_error_free (gerr);
        sim_object_destroy (self);
        return -1;
    }
    else {
        fprintf (stdout, "Successfully loaded objects from XML file '%s'\n", 
                 self->object_xml_filename);
        return FALSE;
    }
}


static void usage()
{
    fprintf (stderr, "usage: ar-sim-objects [options]\n"
             "\n"
             "  -h, --help             shows this help text and exits\n"
             "  -d, --dynamic          use dynamic object server\n"
             "  -f, --file <fname>     load objects from the specified xml file\n"
             "  -g, --gps              use lat_lon_theta from xml file\n"
             );
}

int
main(int argc, char *argv[])
{
    setlinebuf (stdout);

    sim_object_t *self = (sim_object_t*)sim_object_create();
    if (!self)
        return -1;

    self->use_lat_lon_theta = 0;
    //char *object_xml_filename = NULL;
    char *optstring = "hdgf:v";
    char c;
    struct option long_opts[] = {
        {"help", no_argument, 0, 'h'},
        {"dynamic", no_argument, 0, 'd'},
        {"gps", no_argument, 0, 'd'},
        {"file", required_argument, 0, 'f'},
        {"verbose", no_argument, 0, 'v'},
        {0, 0, 0, 0}};
    
    while ((c = getopt_long (argc, argv, optstring, long_opts, 0)) >= 0)
    {
        switch (c) 
        {
        case 'f':
            self->object_xml_filename = strdup (optarg);
            break;
        case 'v':
            self->verbose = 1;
            break;
        case 'd':
            self->use_dynamic=1;
            break;
        case 'g':
            self->use_lat_lon_theta = 1;
            break;
        default:
            usage();
            return 1;
        }
    }

    // lat_lon_theta usage makes sense only with xml file
    if (self->use_lat_lon_theta && !(self->object_xml_filename)) {
        usage();
        sim_object_destroy (self);
        return -1;
    }


    if (self->object_xml_filename == NULL) {
        // load default objects
        make_objects(self);
    }
    else if ((self->object_xml_filename != NULL) && (self->use_lat_lon_theta != 1)) {
        GError *gerr = process_xml_file (self);

        if (gerr) {
            fprintf (stderr, "Error loading objects from XML file '%s': %s\n", 
                     self->object_xml_filename, gerr->message);
            g_error_free (gerr);
            sim_object_destroy (self);
            return -1;
        }
    }
    else if ((self->object_xml_filename != NULL) && (self->use_lat_lon_theta == 1)) 
        self->parse_xml_timeout_id = g_timeout_add(500, parse_xml, self);


    /* subscribe to update channels */
    om_object_list_t_subscribe(self->lcm, "OBJECTS_UPDATE_RENDERER", on_objects_update, self);

    om_xml_cmd_t_subscribe(self->lcm, "XML_COMMAND", on_xml_command, self);
    
    g_main_loop_run(self->main_loop);
    
    sim_object_destroy(self);

    return 0;
}
