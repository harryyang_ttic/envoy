/*
 * Recieves arm commands from arm_interface and sends back new servo  positions
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <lcm/lcm.h>

#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif


#include <lcm/lcm.h>
#include <lcmtypes/dynamixel_status_list_t.h>
#include <lcmtypes/dynamixel_cmd_list_t.h>

#include <bot_param/param_client.h>

#define MAX_POS_ERROR 1
#define SERVO_VELOCITY 50 //ticks per second

typedef struct _servo_limits_t {
  int min;
  int max;
} servo_limits_t;

typedef struct _state_t state_t;

struct _state_t {
  lcm_t *lcm;
  GMainLoop *mainloop;
  dynamixel_status_list_t *cur_positions;
  dynamixel_cmd_list_t *cmd_last;
  int at_goal;
  int num_servos;
  servo_limits_t *servo_limits;
};

static gboolean
on_timer (gpointer data){
  state_t *self = (state_t *) data;

  //Check to see if joints are in correct position
  if (!self->cmd_last || self->at_goal) {
    // publish the dynamixel_status message
    self->cur_positions->utime = bot_timestamp_now();
    dynamixel_status_list_t_publish(self->lcm, "DYNAMIXEL_STATUS_SIMULATION", self->cur_positions);
    return TRUE;
  }
  
  /* Otherwise, move towards the goal */

  int64_t new_time = bot_timestamp_now();
  //int64_t distance = ((new_time - self->cur_positions->utime)/1000000)*SERVO_VELOCITY;
  int64_t distance = 2;
  int32_t new_pos;
  // dynamixel_status_t servo_statuses[self->num_servos];
  dynamixel_status_t * servo_statuses = calloc(self->num_servos, sizeof(dynamixel_status_t));
  
  int at_joint_goal[self->num_servos];
  
  // Check each servo each servo if it is within MAX_POS_ERROR of its goal and which direction it needs to move
  for(int j = 0; j < self->num_servos; j++){
    if(abs(self->cmd_last->commands[j].goal_position - self->cur_positions->servos[j].values[1]) > MAX_POS_ERROR){
      at_joint_goal[j] = 0;
      if(self->cmd_last->commands[j].goal_position - self->cur_positions->servos[j].values[1] > 0){ // .values[1] = current position; .values[0] = goal position
	new_pos = self->cur_positions->servos[j].values[1] + distance;
      }
      else {
	new_pos = self->cur_positions->servos[j].values[1] - distance;
      }
    }
    else{
      new_pos = self->cur_positions->servos[j].values[1];
      at_joint_goal[j] = 1;
    }

    // Set jth servo's dynamixel_status_t
    // int32_t servo_values[2] = {self->cmd_last->commands[j].goal_position, new_pos};
    //int16_t servo_addresses[2] = {30, 36};
    /* dynamixel_status_t status = { */
    /*   .utime = bot_timestamp_now(), */
    /*   .servo_id = j, */
    /*   .num_values = 2, */
    /*   .addresses = servo_addresses, */
    /*   .values = servo_values, */
    /* }; */
    
    servo_statuses[j].utime = bot_timestamp_now();
    servo_statuses[j].servo_id = j;
    servo_statuses[j].num_values = 2;
    servo_statuses[j].addresses = calloc(2, sizeof(int16_t));
    servo_statuses[j].addresses[0] = 30;
    servo_statuses[j].addresses[1] = 36;
    servo_statuses[j].values = calloc(2, sizeof(int32_t));
    servo_statuses[j].values[0] = self->cmd_last->commands[j].goal_position;
    servo_statuses[j].values[1] = new_pos;

      //  servo_statuses[j] = status;
  }

  // Compile list to publish
  dynamixel_status_list_t status_list = {
    .utime = bot_timestamp_now(),
    .nservos = self->num_servos,
    .servos = servo_statuses,
  };

  // Publish dynamixel_status message
  dynamixel_status_list_t_publish(self->lcm, "DYNAMIXEL_STATUS_SIMULATION", &status_list);

  // Update cur_positions to newly published status
  if(self->cur_positions)
    dynamixel_status_list_t_destroy(self->cur_positions);
  
  self->cur_positions = dynamixel_status_list_t_copy(&status_list);

  // Check to see if you're at the goal (within MAX_POS_ERROR). If you are, set self->at_goal = 1
  self->at_goal = 1;
  for(int k = 0; k < self->num_servos; k++){
    if(at_joint_goal[k] == 0){
      self->at_goal = 0;
    }
  }
  free(servo_statuses);
  return TRUE;
}

static void
on_command(const lcm_recv_buf_t *rbuf, const char * channel, const dynamixel_cmd_list_t * msg, void * user){

  state_t *self = (state_t*) user;

  if (self->cmd_last)
    dynamixel_cmd_list_t_destroy (self->cmd_last);

  self->cmd_last = dynamixel_cmd_list_t_copy (msg);

  //Check that goal positions are within servo ranges//
  for(int i = 0; i < self->num_servos; i++){
    if(self->cmd_last->commands[i].goal_position < self->servo_limits[i].min){
      self->cmd_last->commands[i].goal_position == self->servo_limits[i].min;
      fprintf(stderr,"Servo %d goal ouside of range. Goal now set to min position in range\n",i);
    }
    if(self->cmd_last->commands[i].goal_position > self->servo_limits[i].max){
      self->cmd_last->commands[i].goal_position == self->servo_limits[i].max;
      fprintf(stderr,"Servo %d goal ouside of range. Goal now set to servo max position in range\n",i);
    }
  }

  // Set to 1 in the timer once you are within MAX_POS_ERROR of the goal specified in cmd_last
  self->at_goal = 0;
}

int main(int argc, char ** argv){
  
  state_t *self = (state_t *) calloc(1, sizeof(state_t));
  self->at_goal = 1;

  self->lcm = bot_lcm_get_global (NULL);
  //Get BotFrames instance
  BotParam * param = bot_param_new_from_server(self->lcm, 0);
  if (!param) {
    fprintf (stderr, "Error getting BotParam. Are you running the server?\n");
  }
  
  bot_param_get_int(param, "arm_config.num_servos", &self->num_servos);

  self->servo_limits = (servo_limits_t *) calloc(self->num_servos, sizeof(servo_limits_t));
  
  bot_param_get_int(param, "arm_config.joints.shoulder_twist.servos.servo_1.min_range", &self->servo_limits[0].min);
  bot_param_get_int(param, "arm_config.joints.shoulder_twist.servos.servo_1.max_range", &self->servo_limits[0].max);
  bot_param_get_int(param, "arm_config.joints.shoulder.servos.servo_1.min_range", &self->servo_limits[1].min);
  bot_param_get_int(param, "arm_config.joints.shoulder.servos.servo_1.max_range", &self->servo_limits[1].max);
  bot_param_get_int(param, "arm_config.joints.shoulder.servos.servo_2.min_range", &self->servo_limits[2].min);
  bot_param_get_int(param, "arm_config.joints.shoulder.servos.servo_2.max_range", &self->servo_limits[2].max);
  bot_param_get_int(param, "arm_config.joints.elbow.servos.servo_1.min_range", &self->servo_limits[3].min);
  bot_param_get_int(param, "arm_config.joints.elbow.servos.servo_1.max_range", &self->servo_limits[3].max);
  bot_param_get_int(param, "arm_config.joints.lateral_wrist.servos.servo_1.min_range", &self->servo_limits[4].min);
  bot_param_get_int(param, "arm_config.joints.lateral_wrist.servos.servo_1.max_range", &self->servo_limits[4].max);
  bot_param_get_int(param, "arm_config.joints.gripper.servos.servos_1.min_range", &self->servo_limits[5].min);
  bot_param_get_int(param, "arm_config.joints.gripper.servos.servos_1.max_range", &self->servo_limits[5].max);
  bot_param_get_int(param, "arm_config.joints.twist_wrist.servos.servo_1.min_range", &self->servo_limits[6].min);
  bot_param_get_int(param, "arm_config.joints.twist_wrist.servos.servo_1.max_range", &self->servo_limits[6].max);

  // Set initial values for cur_positions
  self->cur_positions = (dynamixel_status_list_t *) calloc(1, sizeof(dynamixel_status_list_t));
  self->cur_positions->nservos = self->num_servos;
  self->cur_positions->utime = bot_timestamp_now();
  /* dynamixel_status_t servo_statuses[self->num_servos]; */
  /* for(int i = 0; i<self->num_servos; i++){ */
  /*   int32_t servo_values[2] = {0, 0}; */
  /*   int16_t servo_addresses[2] = {0, 0}; */
  /*   dynamixel_status_t status = { */
  /*     .utime = bot_timestamp_now(), */
  /*     .servo_id = i, */
  /*     .num_values = 2, */
  /*     .addresses = servo_addresses, */
  /*     .values = servo_values, */
  /*   }; */
  /*   servo_statuses[i] = status; */
  /* } */

  dynamixel_status_t * servo_statuses = calloc(self->num_servos, sizeof(dynamixel_status_t));
  for(int i = 0; i < self->num_servos; i++){
    servo_statuses[i].utime = bot_timestamp_now();
    servo_statuses[i].servo_id = i;
    servo_statuses[i].num_values = 2;
    servo_statuses[i].addresses = calloc(2, sizeof(int16_t));
    servo_statuses[i].addresses[0] = 30;
    servo_statuses[i].addresses[1] = 36;
    servo_statuses[i].values = calloc(2, sizeof(int32_t));
    servo_statuses[i].values[0] = (self->servo_limits[i].max - self->servo_limits[i].min)/2;
    servo_statuses[i].values[1] = (self->servo_limits[i].max - self->servo_limits[i].min)/2;
  }

  self->cur_positions->servos = servo_statuses;
  
  bot_glib_mainloop_attach_lcm(self->lcm);

  g_timeout_add(50, on_timer, self);

  dynamixel_cmd_list_t_subscribe(self->lcm, "DYNAMIXEL_COMMAND", &on_command, self);

  self->mainloop = g_main_loop_new(NULL, FALSE);
  g_main_loop_run(self->mainloop);
  
  free(servo_statuses);

  return 0;
}
