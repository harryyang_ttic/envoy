#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/visualization/cloud_viewer.h>
#include <interfaces/pcl_to_lcm.hpp>

int
main (int argc, char** argv)
{
    // load point cloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    //pcl::io::loadPCDFile ("table_scene_mug_stereo_textured.pcd", *cloud);
    //pcl::io::loadPCDFile ("office_scene.pcd", *cloud);

     std::string filename = argv[1];
     std::cout << "Reading " << filename << std::endl;

     if (pcl::io::loadPCDFile<pcl::PointXYZ> (filename, *cloud) == -1) // load the file
         {
             PCL_ERROR ("Couldn't read file");
             return -1;
         }
    // estimate normals
    pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);

    pcl::IntegralImageNormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
    ne.setNormalEstimationMethod (ne.AVERAGE_3D_GRADIENT);
    ne.setMaxDepthChangeFactor(0.02f);
    ne.setNormalSmoothingSize(10.0f);
    ne.setInputCloud(cloud);
    ne.compute(*normals);

    lcm_t *lcm = bot_lcm_get_global(NULL);
    publish_pcl_points_to_lcm(cloud, lcm);

    publish_pcl_normal_points_to_lcm(cloud, normals, lcm);

}
