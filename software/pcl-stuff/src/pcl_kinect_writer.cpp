#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>

#include <velodyne/velodyne.h>
#include <path_util/path_util.h>

#include <lcmtypes/senlcm_velodyne_t.h>
#include <lcmtypes/er_lcmtypes.h>

#include <gsl/gsl_blas.h>
#include <bot_frames/bot_frames.h>

#include <pcl/visualization/cloud_viewer.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d.h>
#include <lcmtypes/kinect_frame_msg_t.h>
#include <kinect/kinect-utils.h>
#include <pcl/common/common_headers.h>
//#include "jpeg-utils-ijg.h"
#include <zlib.h>
#include "pcl/features/normal_3d.h"
#include <pcl/features/integral_image_normal.h>

#include <interfaces/pcl_to_lcm.hpp>

typedef struct _pose_data_t pose_data_t;
struct _pose_data_t {
    double pose[6];
    double motion[6];
    int64_t utime;
};

typedef struct _vel_params_t vel_params_t;
struct _vel_params_t {
    int64_t last_collector_utime;

    int have_data;
    
    velodyne_calib_t *calib;
    velodyne_laser_return_collector_t *collector;
    
    int64_t 	      last_velodyne_data_utime;
    int64_t           last_pose_utime;
};

typedef struct _kinect_params_t k_params_t;
struct _kinect_params_t {
    kinect_frame_msg_t *kinect_msg;
    
    int width;
    int height;

    // raw disparity
    uint16_t* disparity;
    int need_to_recompute_frame_data;

    uint8_t* uncompress_buffer;
    int uncompress_buffer_size;

    uint8_t* rgb_data;
    KinectCalibration* kcal;
};

typedef struct
{
    lcm_t *lcm;
    BotParam *param;
    BotFrames *frames;
    GMainLoop *mainloop;

    pthread_t  work_thread;
    
    bot_core_pose_t *bot_pose_last;

    vel_params_t *v_params;
    k_params_t *k_params;

    int velodyne; //if 0 - we use the kinect 

    int do_viewer;
    int do_planes;

    int decimation_factor;
    double depth_change;
    double smoothing_size; 
    
    GMutex *mutex;    
} state_t;

static int
frames_vehicle_pos_local (BotFrames *frames, double pos[3])
{
    double pos_body[3] = {0, 0, 0};
    return bot_frames_transform_vec (frames, "body", "local", pos_body, pos);
}

static void do_pcl_stuff(state_t *self){    

    velodyne_laser_return_collection_t *lrc =
        velodyne_collector_pull_collection (self->v_params->collector);
    
    double sensor_to_local[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (self->frames, "VELODYNE",
                                                  "local", lrc->utime,
                                                  sensor_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;
    }

    static const pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>);

    static const pcl::PointCloud<pcl::PointXYZ>::Ptr color_cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>);

    pcl::PointCloud<pcl::PointXYZ>& cloud = *point_cloud_ptr;

    int decimation_factor = self->decimation_factor; 

    // Fill in the cloud data
    cloud.height   = 32;
    cloud.width    = lrc->num_lr / 32.0 / decimation_factor;//2364;    

    cloud.is_dense = true;
    cloud.points.resize (cloud.width * cloud.height);

    //decimate??? - seems like a lot of points

    fprintf(stderr, "No of returns : %d\n" , lrc->num_lr);

    int count = 0; 

    int p = 0;
    
    for(unsigned int s = 0; s < lrc->num_lr; s++) {        
        
        velodyne_laser_return_t *lr = &(lrc->laser_returns[s]);
        
        double local_xyz[3];
        bot_vector_affine_transform_3x4_3d (sensor_to_local, lr->xyz, local_xyz);

        cloud.points[p].x = local_xyz[0]; 
        cloud.points[p].y = local_xyz[1]; 
        cloud.points[p].z = local_xyz[2];
        //cloud.points[p].rgb = 4.2108e+06; //4.2108e+06 * rand () / (RAND_MAX + 1.0f); 

        count++;
        if(count == cloud.height){//skip points 
            s += (decimation_factor -1) * cloud.height;
            count = 0;
        }

        p++;       
    }

    if(0){ //sample data 
        for (float z(-1.0); z <= 1.0; z += 0.05){
            for (float angle(0.0); angle <= 360.0; angle += 5.0){
                pcl::PointXYZ basic_point;
                basic_point.x = 0.5 * cosf (pcl::deg2rad(angle));
                basic_point.y = sinf (pcl::deg2rad(angle));
                basic_point.z = z;
                point_cloud_ptr->points.push_back(basic_point);
            }
        }

        point_cloud_ptr->width = (int) point_cloud_ptr->points.size ();
        point_cloud_ptr->height = 1;
    }

    /*if(1){
        // Create the normal estimation class, and pass the input dataset to it
        pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
        ne.setInputCloud (cloud);

        // Create an empty kdtree representation, and pass it to the normal estimation object.
        // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
        pcl::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::KdTree<pcl::PointXYZ> ());
        ne.setSearchMethod (tree);

        // Output datasets
        pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

        // Use all neighbors in a sphere of radius 3cm
        ne.setRadiusSearch (0.03);

        // Compute the features
        ne.compute (*cloud_normals);
        }*/

    if(self->do_planes){
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob (new pcl::PointCloud<pcl::PointXYZ>); 
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>), cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
        //pcl::VoxelGrid<sensor_msgs::PointCloud2> sor;
        pcl::VoxelGrid<pcl::PointXYZ> sor;

        sor.setInputCloud (cloud.makeShared ());
        sor.setLeafSize (0.1f, 0.1f, 0.1f); //was 0.01
        sor.filter (*cloud_filtered_blob);

        pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
        pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
        // Create the segmentation object
        pcl::SACSegmentation<pcl::PointXYZ> seg;
        // Optional
        seg.setOptimizeCoefficients (true);
        // Mandatory
        seg.setModelType (pcl::SACMODEL_PLANE);
        seg.setMethodType (pcl::SAC_RANSAC);
        seg.setMaxIterations (1000);
        seg.setDistanceThreshold (0.2); //was 0.01
          
        // Create the filtering object
        pcl::ExtractIndices<pcl::PointXYZ> extract;

        //pcl::PCDWriter writer;
        //writer.write<pcl::PointXYZ> ("table_scene_lms400_downsampled.pcd", *cloud_filtered_blob, false);
        
        int i = 0, nr_points = (int) cloud_filtered_blob->points.size ();

        erlcm_segment_list_t msg;
        msg.utime = bot_timestamp_now(); 
        msg.segments = NULL;

        msg.no_segments = 0;

        // While 30% of the original cloud is still there
        while (cloud_filtered_blob->points.size () > 0.3 * nr_points){
            // Segment the largest planar component from the remaining cloud
            seg.setInputCloud (cloud_filtered_blob);
            seg.segment (*inliers, *coefficients);
            if (inliers->indices.size () == 0){
                std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
                break;
            }

            msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
              
            erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];//(erlcm_seg_point_list_t *) calloc(1, sizeof(erlcm_seg_point_list_t));
            seg_msg->segment_id = msg.no_segments; 
            seg_msg->no_points = inliers->indices.size();

            // Extract the inliers
            extract.setInputCloud(cloud_filtered_blob);
            extract.setIndices (inliers);
            extract.setNegative (false);
            extract.filter (*cloud_p);
            std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." << std::endl;

            seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));

            for (size_t k = 0; k < cloud_p->points.size (); ++k){
                seg_msg->points[k].xyz[0] = cloud_p->points[k].x; 
                seg_msg->points[k].xyz[1] = cloud_p->points[k].y; 
                seg_msg->points[k].xyz[2] = cloud_p->points[k].z; 
            }

            msg.no_segments++; 

            //std::stringstream ss;
            //ss << "table_scene_lms400_plane_" << i << ".pcd";
            //writer.write<pcl::PointXYZ> (ss.str (), *cloud_p, false);
              
            // Create the filtering object
            extract.setNegative (true);
            extract.filter (*cloud_filtered_blob);
              
            i++;
        }

        //publish
        erlcm_segment_list_t_publish(self->lcm, "PCL_SEGMENT_LIST", &msg);
        
        for(int k = 0; k < msg.no_segments; k++){
            free(msg.segments[k].points);
        }
        free(msg.segments);        
    }

    //free the laser returns 
    free(lrc->laser_returns);
    free(lrc);
    
    //pcl::io::savePCDFileBinary ("velodyne.pcd", cloud);

    //free the pcl pointcloud 
    cloud.points.resize(0);
}


static inline void
_matrix_vector_multiply_3x4_4d (const double m[12], const double v[4],
        double result[3])
{
    result[0] = m[0]*v[0] + m[1]*v[1] + m[2] *v[2] + m[3] *v[3];
    result[1] = m[4]*v[0] + m[5]*v[1] + m[6] *v[2] + m[7] *v[3];
    result[2] = m[8]*v[0] + m[9]*v[1] + m[10]*v[2] + m[11]*v[3];
}

static inline void
_matrix_transpose_4x4d (const double m[16], double result[16])
{
    result[0] = m[0];
    result[1] = m[4];
    result[2] = m[8];
    result[3] = m[12];
    result[4] = m[1];
    result[5] = m[5];
    result[6] = m[9];
    result[7] = m[13];
    result[8] = m[2];
    result[9] = m[6];
    result[10] = m[10];
    result[11] = m[14];
    result[12] = m[3];
    result[13] = m[7];
    result[14] = m[11];
    result[15] = m[15];
}



static void do_kinect_pcl_stuff(state_t *self){    

    static const pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>);
        
    static const pcl::PointCloud<pcl::PointXYZRGB>::Ptr color_cloud_ptr (new pcl::PointCloud<pcl::PointXYZRGB>);
        
    pcl::PointCloud<pcl::PointXYZ>& cloud = *point_cloud_ptr;
    pcl::PointCloud<pcl::PointXYZRGB>& color_cloud = *color_cloud_ptr;
        
    int decimation_factor = 1; 

    // Fill in the cloud data
    cloud.height   = self->k_params->height;
    cloud.width    = self->k_params->width;

    cloud.is_dense = true;
    cloud.points.resize (cloud.width * cloud.height);

    color_cloud.height   = self->k_params->height;
    color_cloud.width    = self->k_params->width;

    color_cloud.is_dense = true;
    color_cloud.points.resize (color_cloud.width * color_cloud.height);
    
    //decimate??? - seems like a lot of points
    
    //    float so = self->kcal->shift_offset; //unused
    double depth_to_rgb_uvd[12];
    double depth_to_depth_xyz[16];

    kinect_calib_get_depth_uvd_to_rgb_uvw_3x4(self->k_params->kcal, depth_to_rgb_uvd);
    kinect_calib_get_depth_uvd_to_depth_xyz_4x4(self->k_params->kcal, depth_to_depth_xyz);

    double depth_to_depth_xyz_trans[16];
    _matrix_transpose_4x4d(depth_to_depth_xyz, depth_to_depth_xyz_trans);

    //this is not happneing glMultMatrixd(depth_to_depth_xyz_trans);

    int p = 0;

    for(int u=0; u<self->k_params->width; u++) {
        for(int v=0; v<self->k_params->height; v++) {

            uint16_t disparity = self->k_params->disparity[v*self->k_params->width+u];

            double uvd_depth[4] = { u, v, disparity, 1 };
            double uvd_rgb[3];
            _matrix_vector_multiply_3x4_4d(depth_to_rgb_uvd, uvd_depth, uvd_rgb);
            
            double uv_rect[2] = {
                uvd_rgb[0] / uvd_rgb[2],
                uvd_rgb[1] / uvd_rgb[2]
            };
            double uv_dist[2];

            // compute distorted pixel coordinates
            kinect_calib_distort_rgb_uv(self->k_params->kcal, uv_rect, uv_dist);
            int u_rgb = uv_dist[0] + 0.5;
            int v_rgb = uv_dist[1] + 0.5;

            uint8_t r, g, b;
            if(u_rgb >= self->k_params->width || u_rgb < 0 || v_rgb >= self->k_params->height || v_rgb < 0) {
                r = g = b = 0;
            } else {
                r = self->k_params->rgb_data[v_rgb*self->k_params->width*3 + u_rgb*3 + 0];
                g = self->k_params->rgb_data[v_rgb*self->k_params->width*3 + u_rgb*3 + 1];
                b = self->k_params->rgb_data[v_rgb*self->k_params->width*3 + u_rgb*3 + 2];
            }

            cloud.points[p].x = u;
            cloud.points[p].y = v;
            cloud.points[p].z = disparity;

            color_cloud.points[p].x = u;
            color_cloud.points[p].y = v;
            color_cloud.points[p].z = disparity;
            
            uint32_t rgb = (static_cast<uint32_t>(r) << 16 |
                                      static_cast<uint32_t>(g) << 8 | static_cast<uint32_t>(b));
            color_cloud.points[p].rgb = *reinterpret_cast<float*>(&rgb);

            

            p++;       
        }
    }

    if(self->do_planes){
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob (new pcl::PointCloud<pcl::PointXYZ>); 
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>), cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
        //pcl::VoxelGrid<sensor_msgs::PointCloud2> sor;
        pcl::VoxelGrid<pcl::PointXYZ> sor;

        sor.setInputCloud (cloud.makeShared ());
        sor.setLeafSize (0.1f, 0.1f, 0.1f); //was 0.01
        sor.filter (*cloud_filtered_blob);

        pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
        pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
        // Create the segmentation object
        pcl::SACSegmentation<pcl::PointXYZ> seg;
        // Optional
        seg.setOptimizeCoefficients (true);
        // Mandatory
        seg.setModelType (pcl::SACMODEL_PLANE);
        seg.setMethodType (pcl::SAC_RANSAC);
        seg.setMaxIterations (1000);
        seg.setDistanceThreshold (0.2); //was 0.01
          
        // Create the filtering object
        pcl::ExtractIndices<pcl::PointXYZ> extract;

        //pcl::PCDWriter writer;
        //writer.write<pcl::PointXYZ> ("table_scene_lms400_downsampled.pcd", *cloud_filtered_blob, false);
        
        int i = 0, nr_points = (int) cloud_filtered_blob->points.size ();

        erlcm_segment_list_t msg;
        msg.utime = bot_timestamp_now(); 
        msg.segments = NULL;

        msg.no_segments = 0;

        // While 30% of the original cloud is still there
        while (cloud_filtered_blob->points.size () > 0.3 * nr_points){
            // Segment the largest planar component from the remaining cloud
            seg.setInputCloud (cloud_filtered_blob);
            seg.segment (*inliers, *coefficients);
            if (inliers->indices.size () == 0){
                std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
                break;
            }

            msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
              
            erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];
            seg_msg->segment_id = msg.no_segments; 
            seg_msg->no_points = inliers->indices.size();

            // Extract the inliers
            extract.setInputCloud(cloud_filtered_blob);
            extract.setIndices (inliers);
            extract.setNegative (false);
            extract.filter (*cloud_p);
            std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." << std::endl;

            seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));

            for (size_t k = 0; k < cloud_p->points.size (); ++k){
                seg_msg->points[k].xyz[0] = cloud_p->points[k].x; 
                seg_msg->points[k].xyz[1] = cloud_p->points[k].y; 
                seg_msg->points[k].xyz[2] = cloud_p->points[k].z; 
            }

            msg.no_segments++; 
             
            // Create the filtering object
            extract.setNegative (true);
            extract.filter (*cloud_filtered_blob);
              
            i++;
        }

        //publish
        erlcm_segment_list_t_publish(self->lcm, "PCL_SEGMENT_LIST", &msg);
        
        for(int k = 0; k < msg.no_segments; k++){
            free(msg.segments[k].points);
        }
        free(msg.segments);        
    }
        
    //pcl::io::savePCDFileBinary ("velodyne.pcd", cloud);

    //free the pcl pointcloud 
    cloud.points.resize(0);
    color_cloud.points.resize(0);
}

double RawDepthToMeters(uint16_t depthValue){
    
    if (depthValue < 2047) {
        return (1.0 /((double)(depthValue) * -0.0030711016 + 3.3309495161 ));
    }
    return 0.0f;
}


//this only works for the old driver - not the new one 
static void do_kinect_pcl_stuff_new(state_t *self){    

    static const pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>);
        
    static const pcl::PointCloud<pcl::PointXYZRGB>::Ptr color_cloud_ptr (new pcl::PointCloud<pcl::PointXYZRGB>);
        
    pcl::PointCloud<pcl::PointXYZ>& cloud = *point_cloud_ptr;
    pcl::PointCloud<pcl::PointXYZRGB>& color_cloud = *color_cloud_ptr;
        
    int decimation_factor = self->decimation_factor; 

    // Fill in the cloud data
    cloud.height   = self->k_params->height/(double) decimation_factor;
    cloud.width    = self->k_params->width/(double) decimation_factor;

    cloud.is_dense = true;
    cloud.points.resize (cloud.width * cloud.height);

    color_cloud.height   = self->k_params->height/(double) decimation_factor;
    color_cloud.width    = self->k_params->width/(double) decimation_factor;

    color_cloud.is_dense = true;
    color_cloud.points.resize (color_cloud.width * color_cloud.height);
    
    int p = 0;

    double fx_d = 1.0 / 5.9421434211923247e+02;
    double fy_d = 1.0 / 5.9104053696870778e+02;
    double cx_d = 3.3930780975300314e+02;
    double cy_d = 2.4273913761751615e+02;

    double depth_to_rgb_uvd[12];
    double depth_to_depth_xyz[16];

    kinect_calib_get_depth_uvd_to_rgb_uvw_3x4(self->k_params->kcal, depth_to_rgb_uvd);
    kinect_calib_get_depth_uvd_to_depth_xyz_4x4(self->k_params->kcal, depth_to_depth_xyz);

    double depth_to_depth_xyz_trans[16];
    _matrix_transpose_4x4d(depth_to_depth_xyz, depth_to_depth_xyz_trans);

    
    for(int u=0; u<self->k_params->width; u+=decimation_factor) {
        for(int v=0; v<self->k_params->height; v+=decimation_factor) {

            uint16_t disparity = self->k_params->disparity[v*self->k_params->width+u];

            double uvd_depth[4] = { u, v, disparity, 1 };
            double uvd_rgb[3];
            _matrix_vector_multiply_3x4_4d(depth_to_rgb_uvd, uvd_depth, uvd_rgb);

            double uv_rect[2] = {
                uvd_rgb[0] / uvd_rgb[2],
                uvd_rgb[1] / uvd_rgb[2]
                };
            double uv_dist[2];

            kinect_calib_distort_rgb_uv(self->k_params->kcal, uv_rect, uv_dist);
            int u_rgb = uv_dist[0] + 0.5;
            int v_rgb = uv_dist[1] + 0.5;
            
            uint8_t r=255, g=255, b=255;
            if(u_rgb >= self->k_params->width || u_rgb < 0 || v_rgb >= self->k_params->height || v_rgb < 0) {
                r = g = b = 0;
            } else {
                r = self->k_params->rgb_data[v_rgb*self->k_params->width*3 + u_rgb*3 + 0];
                g = self->k_params->rgb_data[v_rgb*self->k_params->width*3 + u_rgb*3 + 1];
                b = self->k_params->rgb_data[v_rgb*self->k_params->width*3 + u_rgb*3 + 2];
            }

            double depth = RawDepthToMeters(disparity);//RawDepthToMeters(((int16_t*)self->k_params->uncompress_buffer)[v*self->k_params->width+u]);

            cloud.points[p].x = ((u - cx_d) * /*disparity*/ depth * fx_d);//u;
            cloud.points[p].y = ((v-cy_d) * /*disparity*/ depth * fy_d);//v;
            cloud.points[p].z = depth;//(depth > 3)?3:depth; // disparity;

            color_cloud.points[p].x = ((u - cx_d) * /*disparity*/depth * fx_d);//u;
            color_cloud.points[p].y = ((v-cy_d) * /*disparity*/depth * fy_d);//v;
            color_cloud.points[p].z = (depth > 3)?3:depth;//disparity;
            
            uint32_t rgb = (static_cast<uint32_t>(r) << 16 |
                            static_cast<uint32_t>(g) << 8 | static_cast<uint32_t>(b));
            color_cloud.points[p].rgb = *reinterpret_cast<float*>(&rgb);

            

            p++;       

            //            glColor3f(r / 255.0, g / 255.0, b / 255.0);

        }
    }

    /*erlcm_xyz_point_list_t msg_xyz;
    
    msg_xyz.utime = bot_timestamp_now();
    msg.no_points = self->k_params->height * self->k_params->width;
    msg.points = (erlcm_xyz_point_t *)calloc(msg.no_points, sizeof(erlcm_xyz_point_t));

    fprintf(stderr,"Points : %d\n", point_cloud_ptr->points.size ());

    for(int i=0; i < point_cloud_ptr->points.size (); i++){
        msg.points[i].xyz[0] = point_cloud_ptr->points[i].x;
        msg.points[i].xyz[1] = point_cloud_ptr->points[i].y;
        msg.points[i].xyz[2] = point_cloud_ptr->points[i].z;
        
        msg.points[i].normals[0] = cloud_normals1->points[i].normal[0];//1.0;
        msg.points[i].normals[1] = cloud_normals1->points[i].normal[1];
        msg.points[i].normals[2] = cloud_normals1->points[i].normal[2];
        
    }
    erlcm_xyz_point_list_t_publish(self->lcm, "PCL_XYZ_LIST", &msg);
    free(msg.points);*/
    
    publish_pcl_points_to_lcm(point_cloud_ptr, self->lcm);

    std::cout << "points: " << point_cloud_ptr->points.size () << std::endl;

    // Create the normal estimation class, and pass the input dataset to it
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normal_estimation;
    normal_estimation.setInputCloud (point_cloud_ptr);

    // Create an empty kdtree representation, and pass it to the normal estimation object.
    // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
    normal_estimation.setSearchMethod (tree);

    // Output datasets
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

    // Use all neighbors in a sphere of radius 3cm
    normal_estimation.setRadiusSearch (0.03);

    // Compute the features
    normal_estimation.compute (*cloud_normals);

    publish_pcl_normal_points_to_lcm(point_cloud_ptr, cloud_normals, self->lcm);

    if(0){ //calculate normals
        // Estimate normals
        pcl::IntegralImageNormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
        
        //pcl::PointCloud<pcl::Normal> normals;
        fprintf(stderr,"Normal Estimation\n");

        static const pcl::PointCloud<pcl::Normal>::Ptr cloud_normals1 (new pcl::PointCloud<pcl::Normal>);
        
        ne.setNormalEstimationMethod (ne.AVERAGE_DEPTH_CHANGE);
        //        ne.setMaxDepthChangeFactor(0.02f);
        ne.setMaxDepthChangeFactor(self->depth_change);
        ne.setNormalSmoothingSize(self->smoothing_size);
        ne.setInputCloud(point_cloud_ptr);
        ne.compute(*cloud_normals1);    

        erlcm_normal_point_list_t msg;
    
        msg.utime = bot_timestamp_now();
        msg.no_points = self->k_params->height * self->k_params->width;
        msg.points = (erlcm_normal_point_t *)calloc(msg.no_points, sizeof(erlcm_normal_point_t));

        fprintf(stderr,"Normals : %d Points : %d\n", cloud_normals1->points.size (), point_cloud_ptr->points.size ());

        for(int i=0; i < point_cloud_ptr->points.size (); i++){
            msg.points[i].xyz[0] = point_cloud_ptr->points[i].x;
            msg.points[i].xyz[1] = point_cloud_ptr->points[i].y;
            msg.points[i].xyz[2] = point_cloud_ptr->points[i].z;

            msg.points[i].normals[0] = cloud_normals1->points[i].normal[0];//1.0;
            msg.points[i].normals[1] = cloud_normals1->points[i].normal[1];
            msg.points[i].normals[2] = cloud_normals1->points[i].normal[2];
                
        }
        erlcm_normal_point_list_t_publish(self->lcm, "PCL_NORMAL_LIST", &msg);
        free(msg.points);
    }

    /*if(0){ //this takes a long time 
        pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
        ne.setInputCloud (point_cloud_ptr);
        const pcl::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::KdTreeFLANN<pcl::PointXYZ> ());
        //pcl::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::KdTree<pcl::PointXYZRGB> ());
        ne.setSearchMethod (tree);
        pcl::PointCloud<pcl::Normal>::Ptr cloud_normals1 (new pcl::PointCloud<pcl::Normal>);
        ne.setRadiusSearch (0.1);
        ne.compute (*cloud_normals1); 

        erlcm_normal_point_list_t msg;
    
        msg.utime = bot_timestamp_now();
        msg.no_points = self->k_params->height * self->k_params->width;
        msg.points = (erlcm_normal_point_t *)calloc(msg.no_points, sizeof(erlcm_normal_point_t));

        for(int i=0; i < point_cloud_ptr->points.size (); i++){
            msg.points[i].xyz[0] = point_cloud_ptr->points[i].x;
            msg.points[i].xyz[1] = point_cloud_ptr->points[i].y;
            msg.points[i].xyz[2] = point_cloud_ptr->points[i].z;

            msg.points[i].normals[0] = cloud_normals1->points[i].normal[0];//1.0;
            msg.points[i].normals[1] = cloud_normals1->points[i].normal[1];
            msg.points[i].normals[2] = cloud_normals1->points[i].normal[2];
                
        }
        erlcm_normal_point_list_t_publish(self->lcm, "PCL_NORMAL_LIST", &msg);
        free(msg.points);
        }*/
    

    //issue seems to be in the viewer for some reason??


    if(self->do_planes){
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob (new pcl::PointCloud<pcl::PointXYZ>); 
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>), cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
        //pcl::VoxelGrid<sensor_msgs::PointCloud2> sor;
        pcl::VoxelGrid<pcl::PointXYZ> sor;

        sor.setInputCloud (cloud.makeShared ());
        sor.setLeafSize (0.1f, 0.1f, 0.1f); //was 0.01
        sor.filter (*cloud_filtered_blob);

        pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
        pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
        // Create the segmentation object
        pcl::SACSegmentation<pcl::PointXYZ> seg;
        // Optional
        seg.setOptimizeCoefficients (true);
        // Mandatory
        seg.setModelType (pcl::SACMODEL_PLANE);
        seg.setMethodType (pcl::SAC_RANSAC);
        seg.setMaxIterations (1000);
        seg.setDistanceThreshold (0.2); //was 0.01
          
        // Create the filtering object
        pcl::ExtractIndices<pcl::PointXYZ> extract;

        //pcl::PCDWriter writer;
        //writer.write<pcl::PointXYZ> ("table_scene_lms400_downsampled.pcd", *cloud_filtered_blob, false);
        
        int i = 0, nr_points = (int) cloud_filtered_blob->points.size ();

        erlcm_segment_list_t msg;
        msg.utime = bot_timestamp_now(); 
        msg.segments = NULL;

        msg.no_segments = 0;

        // While 30% of the original cloud is still there
        while (cloud_filtered_blob->points.size () > 0.3 * nr_points){
            // Segment the largest planar component from the remaining cloud
            seg.setInputCloud (cloud_filtered_blob);
            seg.segment (*inliers, *coefficients);
            if (inliers->indices.size () == 0){
                std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
                break;
            }

            msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
              
            erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];//(erlcm_seg_point_list_t *) calloc(1, sizeof(erlcm_seg_point_list_t));
            seg_msg->segment_id = msg.no_segments; 
            seg_msg->no_points = inliers->indices.size();

            // Extract the inliers
            extract.setInputCloud(cloud_filtered_blob);
            extract.setIndices (inliers);
            extract.setNegative (false);
            extract.filter (*cloud_p);
            std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." << std::endl;

            seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));

            for (size_t k = 0; k < cloud_p->points.size (); ++k){
                seg_msg->points[k].xyz[0] = cloud_p->points[k].x; 
                seg_msg->points[k].xyz[1] = cloud_p->points[k].y; 
                seg_msg->points[k].xyz[2] = cloud_p->points[k].z; 
            }

            msg.no_segments++; 

            // Create the filtering object
            extract.setNegative (true);
            extract.filter (*cloud_filtered_blob);
              
            i++;
        }

        //publish
        erlcm_segment_list_t_publish(self->lcm, "PCL_SEGMENT_LIST", &msg);
        
        for(int k = 0; k < msg.no_segments; k++){
            free(msg.segments[k].points);
        }
        free(msg.segments);        
    }
        
    //pcl::io::savePCDFileBinary ("velodyne.pcd", cloud);

    //free the pcl pointcloud 
    cloud.points.resize(0);
    color_cloud.points.resize(0);
}


static void
on_velodyne (const lcm_recv_buf_t *rbuf, const char *channel,
             const senlcm_velodyne_t *msg, void *user)
{
    state_t *self = (state_t *)user;
    g_assert(self);

    int do_push_motion = 0; // only push motion data if we are starting a new collection or there is a new pose
    //double hist_spc = 0;//bot_gtk_param_widget_get_double (self->pw, PARAM_HISTORY_FREQUENCY);
    
    static int64_t last_redraw_utime = 0;
    int64_t now = bot_timestamp_now();

    // Is this a scan packet?
    if (msg->packet_type == SENLCM_VELODYNE_T_TYPE_DATA_PACKET) {
        
        velodyne_laser_return_collection_t *lrc =
            velodyne_decode_data_packet(self->v_params->calib, msg->data, msg->datalen, msg->utime);
        
        int ret = velodyne_collector_push_laser_returns (self->v_params->collector, lrc);
        
        velodyne_free_laser_return_collection (lrc);
        
        if (VELODYNE_COLLECTION_READY == ret) {

            fprintf(stderr,".");
            
            //starting a new collection
            do_push_motion = 1;
        }
    }
        
    // Update the Velodyne's state information (pos, rpy, linear/angular velocity)
    if (do_push_motion) {

        if (!self->bot_pose_last)
            return;

        // push new motion onto collector
        velodyne_state_t state;

        state.utime = msg->utime;

        BotTrans velodyne_to_local;
        bot_frames_get_trans_with_utime (self->frames, "VELODYNE", "local", msg->utime, &velodyne_to_local);
        
        memcpy (state.xyz, velodyne_to_local.trans_vec, 3*sizeof(double));
        bot_quat_to_roll_pitch_yaw (velodyne_to_local.rot_quat, state.rph);

        // Compute translational velocity
        //
        // v_velodyne = v_bot + r x w
        BotTrans velodyne_to_body;
        bot_frames_get_trans (self->frames, "VELODYNE", "body", &velodyne_to_body);
        
        double v_velodyne[3];
        double r_body_to_velodyne_local[3];
        bot_quat_rotate_to (self->bot_pose_last->orientation, velodyne_to_body.trans_vec, r_body_to_velodyne_local);

        // r x w
        double vel_rot[3];
        bot_vector_cross_3d (r_body_to_velodyne_local, self->bot_pose_last->rotation_rate, vel_rot);

        bot_vector_add_3d (state.xyz_dot, vel_rot, self->bot_pose_last->vel);

        
        // Compute angular rotation rate
        memcpy (state.rph_dot, self->bot_pose_last->rotation_rate, 3*sizeof(double));
    
        do_pcl_stuff(self);

        //do the pcl stuff here (e.g. write to file for now)

        // ******************* //

        do_push_motion = 0;
    }
}

static void
recompute_frame_data(state_t *self)
{
    if(!self->k_params->kinect_msg) {
        return;
    }

    int npixels = self->k_params->width * self->k_params->height;

    const uint8_t* depth_data = self->k_params->kinect_msg->depth.depth_data;

    if(self->k_params->kinect_msg->depth.compression != KINECT_DEPTH_MSG_T_COMPRESSION_NONE) {
        if(self->k_params->kinect_msg->depth.uncompressed_size > self->k_params->uncompress_buffer_size) {
            self->k_params->uncompress_buffer_size = self->k_params->kinect_msg->depth.uncompressed_size;
            self->k_params->uncompress_buffer = (uint8_t*) realloc(self->k_params->uncompress_buffer, self->k_params->uncompress_buffer_size);
        }
        unsigned long dlen = self->k_params->kinect_msg->depth.uncompressed_size;
        int status = uncompress(self->k_params->uncompress_buffer, &dlen, 
                                self->k_params->kinect_msg->depth.depth_data, self->k_params->kinect_msg->depth.depth_data_nbytes);
        if(status != Z_OK) {
            return;
        }
        depth_data = self->k_params->uncompress_buffer;
    }

    switch(self->k_params->kinect_msg->depth.depth_data_format) {
        case KINECT_DEPTH_MSG_T_DEPTH_11BIT:
            if(G_BYTE_ORDER == G_LITTLE_ENDIAN) {
                int16_t* rdd = (int16_t*) depth_data;
                int i;
                for(i=0; i<npixels; i++) {
                    int d = rdd[i];
                    self->k_params->disparity[i] = d;
                }
            } else {
                fprintf(stderr, "Big endian systems not supported\n");
            }
            break;
        case KINECT_DEPTH_MSG_T_DEPTH_10BIT:
            fprintf(stderr, "10-bit depth data not supported\n");
            break;
        default:
            break;
    }

    do_kinect_pcl_stuff_new(self);

    fprintf(stderr,"Done\n");
}


static void 
on_kinect_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                 const kinect_frame_msg_t *msg, void *user_data )
{
    state_t *self =  (state_t *) user_data;
    g_assert(self);
    fprintf(stderr,"Called - kinect()\n");
    g_mutex_lock (self->mutex);

    if(self->k_params->kinect_msg)
        kinect_frame_msg_t_destroy(self->k_params->kinect_msg);

    self->k_params->kinect_msg = kinect_frame_msg_t_copy(msg);

    if(msg->image.image_data_format == KINECT_IMAGE_MSG_T_VIDEO_RGB) {
        memcpy(self->k_params->rgb_data, msg->image.image_data, 
                self->k_params->width * self->k_params->height * 3);
    } else if(msg->image.image_data_format == KINECT_IMAGE_MSG_T_VIDEO_RGB_JPEG) {
        //jpegijg_decompress_8u_rgb(msg->image.image_data, msg->image.image_data_nbytes,
          //self->rgb_data, self->width, self->height, self->width * 3);
        fprintf(stderr,"Not doing decompression for now\n");
    }
    
    recompute_frame_data(self);

    g_mutex_unlock (self->mutex);
}


static void 
on_kinect_frame_gl (const lcm_recv_buf_t *rbuf, const char *channel,
        const kinect_frame_msg_t *msg, void *user_data )
{
    state_t *self =  (state_t *) user_data;
    g_assert(self);
    fprintf(stderr,"Called - kinect()\n");
        g_mutex_lock (self->mutex);

    if(self->k_params->kinect_msg)
        kinect_frame_msg_t_destroy(self->k_params->kinect_msg);

    self->k_params->kinect_msg = kinect_frame_msg_t_copy(msg);

    // TODO check width, height

    if(msg->image.image_data_format == KINECT_IMAGE_MSG_T_VIDEO_RGB) {
        memcpy(self->k_params->rgb_data, msg->image.image_data, 
                self->k_params->width * self->k_params->height * 3);
    } else if(msg->image.image_data_format == KINECT_IMAGE_MSG_T_VIDEO_RGB_JPEG) {
        //jpegijg_decompress_8u_rgb(msg->image.image_data, msg->image.image_data_nbytes,
          //self->rgb_data, self->width, self->height, self->width * 3);
        fprintf(stderr,"Not doing decompression for now\n");
    }
    
    recompute_frame_data(self);

    g_mutex_unlock (self->mutex);

}

static void
on_bot_pose (const lcm_recv_buf_t *buf, const char *channel,
             const bot_core_pose_t *msg, void *user) {
    
    state_t *self =  (state_t *) user;

    g_mutex_lock (self->mutex);

    if (self->bot_pose_last)
        bot_core_pose_t_destroy (self->bot_pose_last);
    self->bot_pose_last = bot_core_pose_t_copy (msg);
    
    g_mutex_unlock (self->mutex);

}

//pthread
static void *track_work_thread(void *user)
{
    state_t *s = (state_t*) user;

    printf("viewer: track_work_thread()\n");

    //seems the viewer is not ready in time when the first data comes in  

    //    SimpleOpenNIViewer v; //v;
    //s->viewer = &v;
    //s->viewer->run(); 

}

void
usage (const char* progName)
{
  std::cout << "\n\nUsage: "<<progName<<" [options]\n\n"
            << "Options:\n"
            << "-------------------------------------------\n"
            << "-h           this help\n"
            << "-s           Create Visualizer\n"
            << "-v           Use velodyne (otherwise defaults to kinect\n"
            << "-p           Run basic RANSAC to do plane extraction\n"
            << "\n\n";
}

int main(int argc, char** argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *self = (state_t*) calloc(1, sizeof(state_t));

    self->decimation_factor = 1;
    self->depth_change = 0.1;
    self->smoothing_size = 10.0;

    const char *optstring = "hpvVd:D:s:";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "velodyne", no_argument, 0, 'v' }, 
				  { "planes", no_argument, 0, 'p' }, 
                                  { "viewer", no_argument, 0, 'V' }, 
                                  { "decimation", required_argument, 0, 'D' }, 
                                  { "depth-change", required_argument, 0, 'd' }, 
                                  { "smooth-size", required_argument, 0, 's' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    usage(argv[0]);
	    break;
	case 'v':
	    {
		fprintf(stderr,"Doing Velodyne\n");
                self->velodyne = 1;
		break;
	    }
	case 'p':
	    {
		fprintf(stderr,"Planes\n");
                self->do_planes = 1;
		break;
	    }
        case 'V':
	    {
		fprintf(stderr,"Viz\n");
                self->do_viewer = 1;
       	break;
	    }
        case 'D':
	    {
		fprintf(stderr,"Decimation\n");
                self->decimation_factor = atoi(optarg);
       	break;
	    }
        case 'd':
	    {
                self->depth_change = strtod(optarg, 0);
                fprintf(stderr,"depth change : %f\n", self->depth_change);
                break;
	    }
        case 's':
	    {
                self->smoothing_size = strtod(optarg, 0);
                fprintf(stderr,"Smoothing Size : %f\n", self->smoothing_size);
                break;
	    }

	default:
	    {
		usage(argv[0]);
		break;
	    }
	}
    }

    if(self->velodyne){
        self->v_params = (vel_params_t *) calloc(1, sizeof(vel_params_t));
    }
    else{
        self->k_params = (k_params_t *) calloc(1,sizeof(k_params_t));
    }


    self->lcm = bot_lcm_get_global (NULL);
    if (!self->lcm) {
        fprintf (stderr,"Error: setup_renderer_laser() failed to get global lcm object\n");
        return -1;
    }

    self->param = bot_param_new_from_server(self->lcm, 1);
    if (!self->param) {
        fprintf (stderr,"Error: setup_renderer_laser() failed to get BotParam instance\n");
        return -1;
    }

    self->mutex = g_mutex_new ();
    self->frames = bot_frames_get_global (self->lcm, self->param);

    if(self->do_viewer){
        pthread_create(&self->work_thread, NULL, track_work_thread, self);
    }

    if(self->velodyne){
        
        char key[256] = {'\0'};
        
        snprintf (key, sizeof(key), "%s.channel", "calibration.velodyne");
        char *lcm_channel = bot_param_get_str_or_fail (self->param, key);
        
        fprintf(stderr,"Channel : %s\n", lcm_channel);
        
        char *velodyne_model = bot_param_get_str_or_fail (self->param, "calibration.velodyne.model");
        char *calib_file = bot_param_get_str_or_fail (self->param, "calibration.velodyne.intrinsic_calib_file");
        
        char calib_file_path[2048];
        
        sprintf(calib_file_path, "%s/%s", getConfigPath(), calib_file);
        
        if (0 == strcmp (velodyne_model, VELODYNE_HDL_32E_MODEL_STR)) 
            self->v_params->calib = velodyne_calib_create (VELODYNE_SENSOR_TYPE_HDL_32E, calib_file_path);
        else if (0 == strcmp (velodyne_model, VELODYNE_HDL_64E_S1_MODEL_STR))
            self->v_params->calib = velodyne_calib_create (VELODYNE_SENSOR_TYPE_HDL_64E_S1, calib_file_path);
        else if (0 == strcmp (velodyne_model, VELODYNE_HDL_64E_S2_MODEL_STR))
            self->v_params->calib = velodyne_calib_create (VELODYNE_SENSOR_TYPE_HDL_64E_S2, calib_file_path);    
        else 
            fprintf (stderr, "ERROR: Unknown Velodyne model \'%s\'", velodyne_model);
        
        free (velodyne_model);
        free (calib_file);
        
        self->v_params->collector = velodyne_laser_return_collector_create (1, 0, 0);

        senlcm_velodyne_t_subscribe (self->lcm, lcm_channel, on_velodyne, self);
    }
    else{

        self->k_params->width = 640;
        self->k_params->height = 480;
        
        self->k_params->kinect_msg = NULL;
        
        self->k_params->disparity = (uint16_t*) malloc(self->k_params->width * self->k_params->height * sizeof(uint16_t));
        self->k_params->rgb_data = (uint8_t*) malloc(self->k_params->width * self->k_params->height * 3);
        
        self->k_params->kcal = kinect_calib_new();
        self->k_params->kcal->width = 640;
        self->k_params->kcal->height = 480;
        
        self->k_params->kcal->intrinsics_depth.fx = 576.09757860;
        self->k_params->kcal->intrinsics_depth.cx = 321.06398107;
        self->k_params->kcal->intrinsics_depth.cy = 242.97676897;
        
        self->k_params->kcal->intrinsics_rgb.fx = 528.49404721;
        self->k_params->kcal->intrinsics_rgb.cx = 319.50000000;
        self->k_params->kcal->intrinsics_rgb.cy = 239.50000000;
        self->k_params->kcal->intrinsics_rgb.k1 = 0;
        self->k_params->kcal->intrinsics_rgb.k2 = 0;
        
        self->k_params->kcal->shift_offset = 1093.4753;
        self->k_params->kcal->projector_depth_baseline = 0.07214;;

        double R[9] = { 0.999999, -0.000796, 0.001256, 0.000739, 0.998970, 0.045368, -0.001291, -0.045367, 0.998970 };
        double T[3] = { -0.015756, -0.000923, 0.002316 };

        memcpy(self->k_params->kcal->depth_to_rgb_rot, R, 9*sizeof(double));
        memcpy(self->k_params->kcal->depth_to_rgb_translation, T, 3*sizeof(double));

        self->k_params->uncompress_buffer = NULL;
        self->k_params->uncompress_buffer_size = 0;

        kinect_frame_msg_t_subscribe(self->lcm, "KINECT_FRAME", on_kinect_frame, self);
    }    

    // Subscribe to the POSE message
    bot_core_pose_t_subscribe (self->lcm, "POSE", on_bot_pose, self);

    self->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!self->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }
    
    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (self->lcm);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (self->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(self->mainloop);
  
    bot_glib_mainloop_detach_lcm(self->lcm);
    
    return (0);
}
