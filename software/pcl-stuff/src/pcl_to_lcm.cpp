
#include "pcl_to_lcm.hpp"
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>


void publish_pcl_points_to_lcm(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, lcm_t *_lcm){
    erlcm_xyz_point_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.no_points = cloud_p->points.size();
    
    msg.points = (erlcm_xyz_point_t *)calloc(msg.no_points, sizeof(erlcm_xyz_point_t));

    for (size_t k = 0; k < cloud_p->points.size (); ++k){
        msg.points[k].xyz[0] = cloud_p->points[k].x; 
        msg.points[k].xyz[1] = cloud_p->points[k].y; 
        msg.points[k].xyz[2] = cloud_p->points[k].z; 
    }

    //publish
    erlcm_xyz_point_list_t_publish(_lcm, "PCL_XYZ_LIST", &msg);
    free(msg.points);    
}

void publish_pcl_points_to_lcm(pcl::PointCloud<pcl::PointXYZ> cloud_p, lcm_t *_lcm){
    erlcm_xyz_point_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.no_points = cloud_p.points.size();
    
    msg.points = (erlcm_xyz_point_t *)calloc(msg.no_points, sizeof(erlcm_xyz_point_t));

    for (size_t k = 0; k < cloud_p.points.size (); ++k){
        msg.points[k].xyz[0] = cloud_p.points[k].x; 
        msg.points[k].xyz[1] = cloud_p.points[k].y; 
        msg.points[k].xyz[2] = cloud_p.points[k].z; 
    }

    //publish
    erlcm_xyz_point_list_t_publish(_lcm, "PCL_XYZ_LIST", &msg);
    free(msg.points);    
}


void publish_pcl_normal_points_to_lcm(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, 
                                      pcl::PointCloud<pcl::Normal>::Ptr normals, lcm_t *_lcm){
    erlcm_normal_point_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.no_points = cloud_p->points.size();
    
    msg.points = (erlcm_normal_point_t *)calloc(msg.no_points, sizeof(erlcm_normal_point_t));

    for (size_t k = 0; k < cloud_p->points.size (); ++k){
        msg.points[k].xyz[0] = cloud_p->points[k].x; 
        msg.points[k].xyz[1] = cloud_p->points[k].y; 
        msg.points[k].xyz[2] = cloud_p->points[k].z; 
        
        msg.points[k].normals[0] = normals->points[k].normal[0];
        msg.points[k].normals[1] = normals->points[k].normal[1];
        msg.points[k].normals[2] = normals->points[k].normal[2];
    }

    //publish
    erlcm_normal_point_list_t_publish(_lcm, "PCL_NORMAL_LIST", &msg);
    free(msg.points);    
}
