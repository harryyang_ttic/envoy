#include "template_stack.hpp"

template<class T> Stack<T>::Stack(int s){
    if(s >max_size) {throw Bad_size();}
    v = new T[s];
    top = 0;
}

template<class T> Stack<T>::~Stack(){
    delete[] v;
}

template<class T> void  Stack<T>::push(T c){
    if(top == max_size){
        v[top] = c;
        top++;
    }
}

template<class T> T Stack<T>::pop(){
    if(top == 0){throw Underflow();}
    top = top -1;
    return v[top];
}
