#include "complex.hpp"

complex complex::operator+(complex a1, complex a2){
    return complex(a1.re + a2.re, a1.im + a2.im);
}

complex complex::operator-(complex a1, complex a2){
    return complex(a1.re - a2.re, a1.im - a2.im);
}

complex complex::operator-(complex a1){
    return complex(this.re - a1.re, this.im - a1.im);
}
