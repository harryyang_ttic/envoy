#include "virtual_stack.hpp"
#include <list>
class List_stack : public Stack{
    std::list<char> lc;
public:
    List_stack(int s){
    }

    ~List_stack(){
        
    }
    
    void push(char c){
        lc.push_front(c);
    }
        
    char pop(){
        char x = lc.front();
        lc.pop_front();
        return x;
    }
};
