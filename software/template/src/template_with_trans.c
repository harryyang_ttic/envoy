/*********************************************************
 *
 * This source code is part of the Carnegie Mellon Robot
 * Navigation Toolkit (CARMEN)
 *
 * CARMEN Copyright (c) 2002 Michael Montemerlo, Nicholas
 * Roy, Sebastian Thrun, Dirk Haehnel, Cyrill Stachniss,
 * and Jared Glover
 *
 * CARMEN is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation; 
 * either version 2 of the License, or (at your option)
 * any later version.
 *
 * CARMEN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General 
 * Public License along with CARMEN; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, 
 * Suite 330, Boston, MA  02111-1307 USA
 *
 ********************************************************/

#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <bot_frames/bot_frames.h>

#define POSE_LIST_SIZE 10

typedef struct
{
    BotParam   *b_server;
    BotFrames *frames;
    lcm_t      *lcm;
    GMainLoop *mainloop;
    pthread_t  work_thread;

    GList *pose_list;     

    bot_core_pose_t *last_pose;
    int publish_all;
    int verbose; 
} state_t;

void print_trans(const BotTrans * tran)
{
    double rpy[3];
    bot_quat_to_roll_pitch_yaw(tran->rot_quat, rpy);
    fprintf(stderr,"t=(%f %f %f) rpy=(%f,%f,%f)\n", tran->trans_vec[0], tran->trans_vec[1],
           tran->trans_vec[2], bot_to_degrees(rpy[0]), bot_to_degrees(rpy[1]), bot_to_degrees(rpy[2]));
}

// convenience function to get the bot's position in the local frame
static int
_frames_vehicle_pos_local (BotFrames *frames, double pos[3])
{
    double pos_body[3] = {1.0, 0, 0};

    double sensor_to_local[12];

    if(!bot_frames_get_trans_mat_3x4 (frames, "body",
                                  "local", 
                                      sensor_to_local)){
        fprintf(stderr,"\tError Getting trans\n");
    }
    else{
        fprintf(stderr,"\tGot trans\n");
    }
    BotTrans trans; 
    bot_frames_get_trans(frames, "body", "local", &trans);

    //body to test
    BotTrans v_trans; 
    //a point 1 m up from the 
    double v_pos[3] = {1,0,0};
    double v_quat[4] = {1,0,0,0};
    bot_trans_set_from_quat_trans(&v_trans, v_quat, v_pos);
    BotTrans dest; 
    //order of multiplication is important 

    // Frame transform (from frame A to B) , Pose in frame A, Pose in frame B) 
    bot_trans_apply_trans_to(&trans, &v_trans, &dest); 
    /*print_trans(&trans);
    print_trans(&v_trans);
    print_trans(&dest);*/

    //this does the actual frame transform
    return bot_frames_transform_vec (frames, "body", "local", pos_body, pos);
    
    /*
      if (!bot_frames_get_trans_mat_3x4_with_utime (self->frames, "VELODYNE",
                                                      "local", lrc->utime,
                                                      sensor_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            return;
        }

     */
}


static void on_pose(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
			     const bot_core_pose_t * msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 
    if(s->last_pose !=NULL){
	bot_core_pose_t_destroy(s->last_pose);
    }
    s->last_pose = bot_core_pose_t_copy(msg);
    if(s->verbose){
	fprintf(stderr,"Pose : (%f,%f)\n",msg->pos[0], msg->pos[1]);//, rpy[2], vel[0] , vel[1]);
    }

    bot_core_pose_t *pose = bot_core_pose_t_copy(msg);
    
    if(s->verbose){
        fprintf(stderr, "Size of List (at start): %d\n", g_list_length(s->pose_list));
    }
    
    if(g_list_length(s->pose_list) < POSE_LIST_SIZE){
        if(s->verbose)
            fprintf(stderr,"Adding\n");
	s->pose_list = g_list_prepend (s->pose_list, (pose));
    }
    else{
        if(s->verbose)
            fprintf(stderr, "Removing and Inserting\n");
	GList* last = g_list_last (s->pose_list);
	bot_core_pose_t *last_pose = (bot_core_pose_t *) last->data;
        if(s->verbose)
            fprintf(stderr,"Pose : %.3f (%f,%f)\n",last_pose->utime / 1000000.0 , last_pose->pos[0], last_pose->pos[1]);
	
	//	s->pose_list = g_list_remove_link (s->pose_list, last);	
	bot_core_pose_t_destroy((bot_core_pose_t *) last->data);
	s->pose_list = g_list_delete_link (s->pose_list, last);	

	if(s->verbose)
            fprintf(stderr, "Size of List (After remove): %d\n", g_list_length(s->pose_list));
	s->pose_list = g_list_prepend (s->pose_list , (gpointer) (pose));
    }
    if(s->verbose)
        fprintf(stderr, "Size of List (at end): %d\n", g_list_length(s->pose_list));
}

//pthread
static void *track_work_thread(void *user)
{
    state_t *s = (state_t*) user;

    printf("obstacles: track_work_thread()\n");

    while(1){
        double local_pos[3];

        _frames_vehicle_pos_local(s->frames, local_pos);
        
        fprintf(stderr, "Pose : %f,%f,%f\n", local_pos[0], local_pos[1], local_pos[2]); 
        
	fprintf(stderr, " T - called ()\n");
	
	usleep(50000);
    }
}

void read_parameters_from_conf(state_t *s)
{
    BotParam *b_param = s->b_server; 
    double max_t_vel =  bot_param_get_double_or_fail(b_param, "robot.max_t_vel");
    double max_r_vel = bot_param_get_double_or_fail(b_param, "robot.max_r_vel");
  
    char **planar_lidar_names = bot_param_get_all_planar_lidar_names(b_param);
  
    if(planar_lidar_names) {
	for (int pind = 0; planar_lidar_names[pind] != NULL; pind++) {
	    fprintf(stderr, "Channel : %s\n", planar_lidar_names[pind]);
	}
    }
  
    g_strfreev(planar_lidar_names);
}

//doesnt do anything right now - timeout function
gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;
  
    //do the periodic stuff 
    if(s->verbose){
	fprintf(stderr, "Callback - Timeout\n");
    }
    //return true to keep running
    return TRUE;
}

void subscribe_to_channels(state_t *s)
{
    bot_core_pose_t_subscribe(s->lcm, "POSE", on_pose ,s);
}  


static void usage(char * funcName)
{
    printf("Usage: %s [options]\n"
	   "options are:\n"
	   "--help,        -h    display this message \n"
	   "--publish_all, -a    publish robot laser messages for all channels \n"
	   "--verbose,     -v    be verbose", funcName);
    exit(1);

}

int 
main(int argc, char **argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));
    state->last_pose = NULL;
    state->pose_list = NULL;

    const char *optstring = "hav";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "publish_all", no_argument, 0, 'a' }, 
				  { "verbose", no_argument, 0, 'v' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    usage(argv[0]);
	    break;
	case 'a':
	    {
		fprintf(stderr,"Publishing all laser channels\n");
		break;
	    }
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
		state->verbose = 1;
		break;
	    }
	default:
	    {
		usage(argv[0]);
		break;
	    }
	}
    }

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
    state->b_server = bot_param_new_from_server(state->lcm, 1);

    state->frames = bot_frames_get_global (state->lcm, state->b_server);

    pthread_create(&state->work_thread, NULL, track_work_thread, state);

    subscribe_to_channels(state);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    read_parameters_from_conf(state);

    /* heart beat*/
    g_timeout_add_seconds (1, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


