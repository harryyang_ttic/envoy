#include "stack.h"

namespace Stack{
    const int max_size = 200;
    char v[max_size];
    int top = 0;
}

void Stack::push(char c){
    if(top == max_size) throw Overflow();
    else{
        top++;
        v[top-1] = c;
    }    
}

char Stack::pop(){
    if(top == 0){
        throw Underflow();
    }
    else{
        top--;
        return v[top];
    }
}
