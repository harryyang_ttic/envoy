import sys

import lcm
from erlcm.annotation_t import annotation_t
#from erlcm.raw_odometry_msg_t import raw_odometry_msg_t

fname = sys.argv[1]
channel_name = sys.argv[2]
utime_offset = sys.argv[3]
new_logfile = sys.argv[4]

# 1) Open .lab text file
f = open(fname, 'r')
# 2) Instantiate LCM
'''
for line in f:
	print line
'''
lc = lcm.LCM()

new_log = lcm.EventLog(new_logfile, "w")

start_time = 0
end_time = 0
annotation = ' '


# 3) for each line in file 
# a) read start time, end time, and annotation
# each line in the format [start_time   end_time   annotation]
for line in f:
	info = line.split()
	start_time = int(float(info[0]) + int(utime_offset))
	end_time = int(float(info[1]) + int(utime_offset))
	annotation = " ".join(info[2:])
#	print start_time
	
	
# b) create annotation_t message

	msg = annotation_t()
	msg.utime = end_time
	msg.start_utime = start_time	
	msg.end_utime = end_time
	msg.annotation = annotation
	msg.reference_channel = channel_name

	print msg.utime, msg.start_utime, msg.end_utime, msg.annotation, msg.reference_channel

	lc.publish ("ANNOTATION", msg.encode())
	new_log.write_event(msg.utime, "ANNOTATION", msg.encode())


'''
map_request_msg_t()
        msg.utime = time() * 1e6
        msg.requesting_prog = "FLOOR_PREDICTOR"
        self.lc.publish("FLOOR_STATUS_REQUEST", msg.encode())
'''

new_log.close()

