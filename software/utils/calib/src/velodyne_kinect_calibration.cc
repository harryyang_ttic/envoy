// First find normals in kinect frame
// on_kinect_image, use mouse over to pick 3 points, one for a each plane of the box
// Once, the 3 points have been picked, use it to find the normals that correspond
// to the selected plane (color them in the renderer, rgb)
// Find the spherical diameter of the box, and only search for those normals in the 
// velodyne point cloud s.t. all the 3 normals of the box fall within this spherical
// diameter

// Velodyne data
// First extract the normals and their corresponding inliers
// For each of the normals and their normal origin, find the 3 nearest neighbors;
// n clusters that could be possible matches to the selected box

// prune clusters based on the fact that the 3 normals within the cluster needs to be
// orthogonal to each other

// For each of the pruned clusters (should be on the order of 2 or 3 at most), 
// do least squares fit on the whole point cloud (for each point in the kinect 
// point cloud find the nearest velodyne point, and find the corresponding 
// error)

#include <gsl/gsl_blas.h>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <velodyne/velodyne.h>
#include <velodyne/velodyne_extractor.h>
#include <lcmtypes/er_lcmtypes.h>

#include <lcmtypes/erlcm_roi_t.h>
#include "kinect_opencv_utils.h"

#include <interfaces/pcl_lcm_lib.hpp>

#include <pcl/visualization/cloud_viewer.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d.h>
#include <lcmtypes/kinect_frame_msg_t.h>
#include <kinect/kinect-utils.h>
#include <pcl/common/common_headers.h>
#include <zlib.h>
#include "pcl/features/normal_3d.h"
#include <pcl/features/integral_image_normal.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/sample_consensus/sac_model_plane.h>

#include <pthread.h>

typedef struct _state_t state_t;

struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    velodyne_extractor_state_t *velodyne; 
    // pthread_t opencv_thread;
};
state_t * state = NULL;

struct MouseEvent
{
    MouseEvent() { event = -1; buttonState = 0; }
    cv::Point pt;
    int event;
    int buttonState;
};
MouseEvent mouse;

kinect_data kinect;

bool capture = false;
std::vector<cv::Point> plane_pts;

void kinect_depth_to_pcl(pcl::PointCloud<pcl::PointXYZ>::Ptr& pcl, int step=2) {
    pcl->width = kinect.width / step;
    pcl->height = kinect.height / step;
    pcl->points.resize(pcl->width*pcl->height);

    // Convert to XYZ
    int i, j, idx;
    for (i = 0; i < kinect.height; i+=step) {
        for(j = 0; j < kinect.width; j+=step, idx++) {
            // idx = i*kinect.width + j;
            cv::Vec3f xyz = kinect.getXYZ(i,j);
            pcl->points[idx].x = xyz[0], pcl->points[idx].y = xyz[1], pcl->points[idx].z = xyz[2];
        }
    }
    return;
} 


void velodyne_to_pcl(pcl::PointCloud<pcl::PointXYZ>::Ptr& pcl){
    xyz_point_list_t *ret = velodyne_extract_points(state->velodyne);
    if(ret != NULL && ret->no_points){

        pcl->height   = 32;
        pcl->width    = ret->no_points / 32.0;

        pcl->is_dense = true;
        pcl->points.resize (pcl->width * pcl->height);

        fprintf(stderr, "Utime : %f Size of Points : %d \n", ret->utime / 1.0e6, ret->no_points);
        
        for(int i = 0; i < pcl->height; i++){
            for(int s = 0; s < pcl->width; s++) {
                int ind_vel = s * 32 + i; 
                int ind_pcl = pcl->width * i + s; 
        
                pcl->points[ind_pcl].x = ret->points[ind_vel].xyz[0];
                pcl->points[ind_pcl].y = ret->points[ind_vel].xyz[1];
                pcl->points[ind_pcl].z = ret->points[ind_vel].xyz[2];
            }
        } 
        destroy_xyz_list(ret);
    } 
    return;
} 


void publish_pcl_points_to_lcm(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, const char* channel, int rgb){
    erlcm_xyzrgb_point_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.no_points = cloud_p->points.size();
    
    msg.points = (erlcm_xyzrgb_point_t *)calloc(msg.no_points, sizeof(erlcm_xyzrgb_point_t));

    for (size_t k = 0; k < cloud_p->points.size (); ++k){
        msg.points[k].xyz[0] = cloud_p->points[k].x; 
        msg.points[k].xyz[1] = cloud_p->points[k].y; 
        msg.points[k].xyz[2] = cloud_p->points[k].z; 
        msg.points[k].rgb = rgb; 
    }

    //publish
    erlcm_xyzrgb_point_list_t_publish(state->lcm, channel, &msg);
    free(msg.points);    
}

void compute_normals(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, lcm_t *lcm, double search_radius, double vp[3]){
    std::cout << "points: " << cloud->points.size () << std::endl;
    
    pcl::VoxelGrid<pcl::PointXYZ> sor;
    
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob (new pcl::PointCloud<pcl::PointXYZ>);
    sor.setInputCloud (cloud->makeShared ());
    sor.setLeafSize (0.03f, 0.03f, 0.03f); //was 0.01
    sor.filter (*cloud_filtered_blob);

    publish_pcl_points_to_lcm(cloud_filtered_blob, "KINECT_POINT_LIST", 0x880000);
    // publish_pcl_points_to_lcm(cloud_filtered_blob, lcm);
    
    // Create the normal estimation class, and pass the input dataset to it
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normal_estimation;
    //normal_estimation.setInputCloud (cloud);
    //using the filtered points messes things up 
    normal_estimation.setInputCloud (cloud_filtered_blob);

    // Create an empty kdtree representation, and pass it to the normal estimation object.
    // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
    normal_estimation.setSearchMethod (tree);

    // Output datasets
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

    // Use all neighbors in a sphere of radius 3cm
    //normal_estimation.setRadiusSearch (0.03);
    normal_estimation.setRadiusSearch (search_radius);

    //might need to set viewpoints 

    normal_estimation.setViewPoint (vp[0], vp[1], vp[2]);

    // Compute the features
    normal_estimation.compute (*cloud_normals);

    publish_pcl_normal_points_to_lcm(cloud_filtered_blob, cloud_normals, lcm);

    // cloud_normals->points.size () should have the same size as the input cloud->points.size ()
    std::cout << "cloud_normals->points.size (): " << cloud_normals->points.size () << std::endl;
}

void compute_normals(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, lcm_t *lcm, double search_radius){
    double vp[3] = {.0,.0,.0}; 
    compute_normals(cloud, lcm, search_radius, vp);
}

void segment_planes(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, lcm_t *lcm){
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob (new pcl::PointCloud<pcl::PointXYZ>); 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>); 
    const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
    //pcl::VoxelGrid<sensor_msgs::PointCloud2> sor;

    // pcl::VoxelGrid<pcl::PointXYZ> sor;
    // sor.setInputCloud (cloud->makeShared ());
    // sor.setLeafSize (0.05f, 0.05f, 0.05f); //was 0.01
    // sor.filter (*cloud_filtered_blob);
    *cloud_filtered_blob = *cloud;

    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (1000);

    //play with this to get different size segments 
    seg.setDistanceThreshold (0.01); //was 0.01
    
    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    
    pcl::PCDWriter writer;
    writer.write<pcl::PointXYZ> ("out_downsampled.pcd", *cloud_filtered_blob, false);
        
    int i = 0, nr_points = (int) cloud_filtered_blob->points.size ();

    erlcm_segment_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.segments = NULL;
    
    msg.no_segments = 0;
    
    // While 30% of the original cloud is still there
    while (cloud_filtered_blob->points.size () > 0.05 * nr_points){
        // Segment the largest planar component from the remaining cloud
        seg.setInputCloud (cloud_filtered_blob);
        seg.segment (*inliers, *coefficients);
        if (inliers->indices.size () == 0){
            std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
            break;
        }
        
        std::cerr << "Model coefficients: " << coefficients->values[0] << " " 
                                      << coefficients->values[1] << " "
                                      << coefficients->values[2] << " " 
                                      << coefficients->values[3] << std::endl;
        
        msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
        
        erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];//(erlcm_seg_point_list_t *) calloc(1, sizeof(erlcm_seg_point_list_t));
        seg_msg->segment_id = msg.no_segments; 
        seg_msg->no_points = inliers->indices.size();

        seg_msg->coefficients[0] = coefficients->values[0]; 
        seg_msg->coefficients[1] = coefficients->values[1]; 
        seg_msg->coefficients[2] = coefficients->values[2]; 
        seg_msg->coefficients[3] = coefficients->values[3];

        // pcl::PointXYZ sensor_loc; 

        // sensor_loc.x = 0;
        // sensor_loc.y = 0;
        // sensor_loc.z = 0; 
        
        // double dist = pcl::pointToPlaneDistance(sensor_loc, coefficients->values[0], 
        //                                         coefficients->values[1], 
        //                                         coefficients->values[2], 
        //                                         coefficients->values[3]);

        // fprintf(stderr,"Distance to plane : %f\n", dist);
        
        // if(fabs(dist - 1.2) < 0.4){
        //     fprintf(stderr, "Ground plane found\n"); 
        // }

        // Extract the inliers
        extract.setInputCloud(cloud_filtered_blob);
        extract.setIndices (inliers);
        extract.setNegative (false);
        extract.filter (*cloud_p);

        std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." << std::endl;
        
        seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));

        double mean[3] = {0,0,0};
        
        for (size_t k = 0; k < cloud_p->points.size (); ++k){
            seg_msg->points[k].xyz[0] = cloud_p->points[k].x; 
            seg_msg->points[k].xyz[1] = cloud_p->points[k].y; 
            seg_msg->points[k].xyz[2] = cloud_p->points[k].z; 
            mean[0] += cloud_p->points[k].x; 
            mean[1] += cloud_p->points[k].y; 
            mean[2] += cloud_p->points[k].z; 
        }
        
        seg_msg->centroid[0] = mean[0] / cloud_p->points.size (); 
        seg_msg->centroid[1] = mean[1] / cloud_p->points.size (); 
        seg_msg->centroid[2] = mean[2] / cloud_p->points.size (); 

        fprintf(stderr, "Mean : %f, %f, %f\n", seg_msg->centroid[0], 
                seg_msg->centroid[1], seg_msg->centroid[2]);
        
        msg.no_segments++; 
        
        // Create the filtering object
        extract.setNegative (true);
        extract.filter (*cloud_filtered_blob);
        
        i++;
    }
    
    //publish
    erlcm_segment_list_t_publish(lcm, "PCL_SEGMENT_LIST", &msg);
    
    for(int k = 0; k < msg.no_segments; k++){
        free(msg.segments[k].points);
    }
    free(msg.segments);        
}


void calibrate() { 
    pcl::PointCloud<pcl::PointXYZ>::Ptr kinect_pcl(new pcl::PointCloud<pcl::PointXYZ>);
    kinect_depth_to_pcl(kinect_pcl, 2);
    //publish_pcl_points_to_lcm(kinect_pcl, "KINECT_POINT_LIST", 0x880000);
    // compute_normals(kinect_pcl, state->lcm, .03);
    //segment_planes(kinect_pcl, state->lcm);

    pcl::PointCloud<pcl::PointXYZ>::Ptr velodyne_pcl(new pcl::PointCloud<pcl::PointXYZ>);
    velodyne_to_pcl(velodyne_pcl);
    publish_pcl_points_to_lcm(velodyne_pcl, "VELODYNE_POINT_LIST", 0x000088);

    // uses plane_pts to find closest set of normals
    pcl::PointCloud<pcl::Normal>::Ptr kinect_normals (new pcl::PointCloud<pcl::Normal>);
    // compute_kinect_normals_from_mouseover(kinect_normals, kinect_pcl);

    pcl::PointCloud<pcl::Normal>::Ptr velodyne_normals (new pcl::PointCloud<pcl::Normal>);
    // compute_velodyne_normals(velodyne_normals, velodyne_pcl);

    pcl::PointCloud<pcl::Normal>::Ptr pruned_velodyne_normals (new pcl::PointCloud<pcl::Normal>);
    // prune_velodyne_normals(pruned_velodyne_normals, velodyne_normals);


}

//doesnt do anything right now - timeout function
gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;

    if (!kinect.isLive())
        return TRUE;

    //return true to keep running
    return TRUE;
}

static void on_kinect_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                                   const kinect_frame_msg_t *msg, 
                                   void *user_data ) {
    
    if (msg->depth.depth_data_format != KINECT_DEPTH_MSG_T_DEPTH_MM) { 
        std::cerr << "Warning: kinect data format is not KINECT_DEPTH_MSG_T_DEPTH_MM" << std::endl 
                  << "Program may not function properly" << std::endl;
    }
    assert (msg->depth.depth_data_format == KINECT_DEPTH_MSG_T_DEPTH_MM);

    kinect.update(msg);

    cv::Mat3b img = kinect.getRGB();

    // Display the 3 points on the image
    for (int j=0; j<plane_pts.size(); j++) 
        line(img,plane_pts[j],plane_pts[(j+1)%plane_pts.size()],cv::Scalar(255, 255, 255), 2);


    if (/*plane_pts.size() == 3 && */capture) { 
        calibrate();
        capture = false;
    } 


    imshow("Kinect", img);
    return;
}

static void onMouse(int event, int x, int y, int flags, void* userdata) {
    MouseEvent* data = (MouseEvent*)userdata;

    switch (event) {
    case CV_EVENT_LBUTTONDOWN:
        break;
    case CV_EVENT_LBUTTONUP:
        if (plane_pts.size() < 3)
            plane_pts.push_back(cv::Point(x, y));
        else { 
            // computed = false;
            plane_pts.clear();
            plane_pts.push_back(cv::Point(x, y));
        }   
        break;
    }
    return;
}


//doesnt do anything right now - timeout function
gboolean waitkey_cb (gpointer data)
{
    state_t *s = (state_t *)data;

    unsigned char c = cv::waitKey(10) & 0xff;        
    if (c == 'c') { 
        printf("Capturing\n");
        capture = true;
    }

    //return true to keep running
    return TRUE;
}


void  INThandler(int sig)
{
    //lcm_destroy(lcm);
    printf("Exiting\n");
    exit(0);
}

int main(int argc, char** argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state = (state_t*) calloc(1, sizeof(state_t));

    state->lcm =  bot_lcm_get_global(NULL);
    state->velodyne = velodyne_extractor_init(state->lcm);
    state->mainloop = g_main_loop_new( NULL, FALSE );  
    kinect.lcm = state->lcm;
  
    cv::namedWindow( "Kinect" );
    cv::setMouseCallback("Kinect", onMouse, &mouse);

    if (!state->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    // Install signal handler to free data
    signal(SIGINT, INThandler);

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);
    kinect_frame_msg_t_subscribe(state->lcm, "KINECT_FRAME", on_kinect_image_frame, NULL);

    g_timeout_add (100, heartbeat_cb, state);
    g_timeout_add (10, waitkey_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);

    return 0;
}
