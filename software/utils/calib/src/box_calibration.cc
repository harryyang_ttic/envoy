#include "kinect_opencv_utils.h"

// PCL
#include <pcl/io/pcd_io.h>
#include <pcl/PointIndices.h>
#include <pcl/registration/icp.h>

// LCM includes
#include <lcm/lcm.h>
#include <lcmtypes/kinect_frame_msg_t.h>
#include <kinect/kinect-utils.h>
#include <bot_core/bot_core.h>
#include <bot_param/param_client.h>
#include <bot_frames/bot_frames.h>

#include <velodyne/velodyne.h>
#include <er_common/path_util.h>
#include <lcmtypes/senlcm_velodyne_t.h> 
#include <lcmtypes/senlcm_velodyne_list_t.h>

#define PARAM_HISTORY_FREQUENCY 0.1
#define VELODYNE_DATA_CIRC_SIZE 10
#define PARAM_HISTORY_LENGTH 1

kinect_data kinect;
struct velodyne_data { 
    BotParam *param;
    BotFrames *frames;
    
    bot_core_pose_t *bot_pose_last;

    int64_t last_collector_utime;

    int have_data;
    
    velodyne_calib_t *calib;
    velodyne_laser_return_collector_t *collector;
    
    BotPtrCircular   *velodyne_data_circ;
    int64_t 	      last_velodyne_data_utime;
    int64_t           last_pose_utime;
};

velodyne_data velodyne;

typedef std::pair<cv::Mat1b, cv::Mat_<uint16_t> > kinect_rgbd_pair;
using namespace cv;


static int
process_velodyne (const senlcm_velodyne_t *v, velodyne_data *self)
{
    g_assert(self);

    int do_push_motion = 0; // only push motion data if we are starting a new collection or there is a new pose
    double hist_spc = PARAM_HISTORY_FREQUENCY;

    // Is this a scan packet?
    if (v->packet_type == SENLCM_VELODYNE_T_TYPE_DATA_PACKET) {
        
        velodyne_laser_return_collection_t *lrc =
            velodyne_decode_data_packet(self->calib, v->data, v->datalen, v->utime);
        
        int ret = velodyne_collector_push_laser_returns (self->collector, lrc);
        
        velodyne_free_laser_return_collection (lrc);
        
        if (VELODYNE_COLLECTION_READY == ret) {

            velodyne_laser_return_collection_t *lrc =
                velodyne_collector_pull_collection (self->collector);

            // if enough time has elapsed since the last scan push it onto the circular buffer
	    if (abs (lrc->utime - self->last_velodyne_data_utime) > (int64_t)(1E6/hist_spc)) {
                bot_ptr_circular_add (self->velodyne_data_circ, lrc);
                self->last_velodyne_data_utime = lrc->utime;
            } else {
                // memory leak city if this isnt here as soon as you increase the history spacing
                velodyne_free_laser_return_collection (lrc);
            }
                        
            //starting a new collection
            do_push_motion = 1;
        }
        else if(VELODYNE_COLLECTION_READY_LOW == ret) {
            fprintf(stderr,"Low packet - ignoring");

            velodyne_laser_return_collection_t *lrc =
                velodyne_collector_pull_collection (self->collector);
            
            velodyne_free_laser_return_collection (lrc);
        }
    }

    // Update the Velodyne's state information (pos, rpy, linear/angular velocity)
    if (do_push_motion) {

        if (!self->bot_pose_last)
            return 0;

        // push new motion onto collector
        velodyne_state_t state;

        state.utime = v->utime;

        // find sensor pose in local/world frame
        /* 
         * double x_lr[6] = {self->pose->x, self->pose->y, self->pose->z,
         *                   self->pose->r, self->pose->p, self->pose->h};
         * double x_ls[6] = {0};
         * ssc_head2tail (x_ls, NULL, x_lr, self->x_vs);
         */

        BotTrans velodyne_to_local;
        bot_frames_get_trans_with_utime (self->frames, "VELODYNE", "local", v->utime, &velodyne_to_local);
        
        memcpy (state.xyz, velodyne_to_local.trans_vec, 3*sizeof(double));
        bot_quat_to_roll_pitch_yaw (velodyne_to_local.rot_quat, state.rph);

        // Compute translational velocity
        //
        // v_velodyne = v_bot + r x w
        BotTrans velodyne_to_body;
        bot_frames_get_trans (self->frames, "VELODYNE", "body", &velodyne_to_body);
        
        double v_velodyne[3];
        double r_body_to_velodyne_local[3];
        bot_quat_rotate_to (self->bot_pose_last->orientation, velodyne_to_body.trans_vec, r_body_to_velodyne_local);

        // r x w
        double vel_rot[3];
        bot_vector_cross_3d (r_body_to_velodyne_local, self->bot_pose_last->rotation_rate, vel_rot);

        bot_vector_add_3d (state.xyz_dot, vel_rot, self->bot_pose_last->vel);
        
        // Compute angular rotation rate
        memcpy (state.rph_dot, self->bot_pose_last->rotation_rate, 3*sizeof(double));
        do_push_motion = 0;
    }    

}

static void on_kinect_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                                   const kinect_frame_msg_t *msg, 
                                   void *user_data ) {
    
    if (msg->depth.depth_data_format != KINECT_DEPTH_MSG_T_DEPTH_MM) { 
        std::cerr << "Warning: kinect data format is not KINECT_DEPTH_MSG_T_DEPTH_MM" << std::endl 
                  << "Program may not function properly" << std::endl;
    }
    assert (msg->depth.depth_data_format == KINECT_DEPTH_MSG_T_DEPTH_MM);

    // Update the kinect_data struct with depth and rgb information
    kinect.update(msg);

    kinect.showRGB();
}

static void
on_velodyne_list (const lcm_recv_buf_t *rbuf, const char *channel,
		  const senlcm_velodyne_list_t *msg, void *user)
{
    velodyne_data *self = (velodyne_data *)user;

    static int64_t last_redraw_utime = 0;
    int64_t now = bot_timestamp_now();

    for (int i=0; i < msg->num_packets; i++) 
	process_velodyne (&(msg->packets[i]), self);
    
    return;
}



static void usage(const char* progname)
{
  fprintf (stderr, "Usage: %s [options]\n"
                   "\n"
                   "Options:\n"
                   "  -l URL    Specify LCM URL\n"
                   "  -h        This help message\n", 
                   g_path_get_basename(progname));
  exit(1);
}

void  INThandler(int sig)
{
    printf("Exiting\n");
    exit(0);
}

std::vector<kinect_rgbd_pair> getFiles(std::string dir) { 

    std::vector<kinect_rgbd_pair> kinect_pairs;
    int cap_count = 0;

    while(1) { 
        char fn[64];
        sprintf(fn, "kinect_frame_image_%i.png", cap_count);
        cv::Mat1b gray = cv::imread(fn, 0);

        if (gray.data == NULL)
            break;

        char fnd[64];
        sprintf(fnd, "kinect_frame_depth_%i.png", cap_count);
        cv::Mat3b depth_hilo = cv::imread(fn, 1);

        if (depth_hilo.data == NULL)
            break;

        cv::Mat_<uint16_t> depth(depth_hilo.size());
        for (int i=0; i<depth_hilo.rows; i++)
            for (int j=0; j<depth_hilo.cols; j++) { 
                uint16_t lo = (uint16_t)depth_hilo(i,j)[0];
                uint16_t hi = (uint16_t)depth_hilo(i,j)[1];
                uint16_t v = (hi << 8) & 0xff00;
                v = v | lo;
                depth(i,j) = v;
            }
        
        std::cerr << "Reading: " << fn << std::endl;

        kinect_pairs.push_back(std::make_pair(gray, depth));
        cap_count++;
    }

    return kinect_pairs;
}


void addCalibrationSnapshot() { 

}

void calibrate() { 

}

    // kinect_point_list_t point_cloud;
    // point_cloud.points = (kinect_point_t*) malloc(sizeof(kinect_point_t) * pcl->points.size());
    // memset(point_cloud.points, 0, sizeof(kinect_point_t) * pcl->points.size());
    // point_cloud.num_points = pcl->points.size();

    // kinect_point_t* points = point_cloud.points;
    // int idx=0, f_idx=0;
    // for (int j=0; j<pcl->points.size(); j++) {
    //         points[j].x = pcl->points[j].x;
    //         points[j].y = pcl->points[j].y;
    //         points[j].z = pcl->points[j].z;
    //         points[j].r = pcl->points[j].r;
    //         points[j].g = pcl->points[j].g;
    //         points[j].b = pcl->points[j].b;
    // }

    // kinect_point_list_t_publish(kinect.lcm, "KINECT_POINT_CLOUD", &point_cloud);
    // free(point_cloud.points);

void
circ_free_velodyne_data(void *user, void *p) {
       
    velodyne_laser_return_collection_t *lrc = (velodyne_laser_return_collection_t *)p;
    velodyne_free_laser_return_collection (lrc);
}


int main(int argc, char** argv)
{
    int c;
    char *lcm_url = NULL;

    velodyne_data *self = (velodyne_data*) calloc (1, sizeof (*self));

    // command line options - to publish on a specific url  
    while ((c = getopt (argc, argv, "vhir:jq:zl:")) >= 0) {
        switch (c) {
        case 'l':
            lcm_url = strdup(optarg);
            printf("Using LCM URL \"%s\"\n", lcm_url);
            break;
        case 'h':
        case '?':
            usage(argv[0]);
        }
    }

    // LCM-related
    kinect.lcm = lcm_create(lcm_url);
    if(!kinect.lcm) {
        printf("Unable to initialize LCM\n");
        return 1;
    } 

    const char *velodyne_model = "HDL_32E"; // bot_param_get_str_or_fail (self->param, "calibration.velodyne.model");
    const char *calib_file = "velodyne/HDL32E_segway_db.xml"; // bot_param_get_str_or_fail (self->param, "calibration.velodyne.intrinsic_calib_file");

    char calib_file_path[2048];

    sprintf(calib_file_path, "%s/%s", getConfigPath(), calib_file);

    if (0 == strcmp (velodyne_model, VELODYNE_HDL_32E_MODEL_STR)) 
        self->calib = velodyne_calib_create (VELODYNE_SENSOR_TYPE_HDL_32E, calib_file_path);
    else if (0 == strcmp (velodyne_model, VELODYNE_HDL_64E_S1_MODEL_STR))
        self->calib = velodyne_calib_create (VELODYNE_SENSOR_TYPE_HDL_64E_S1, calib_file_path);
    else if (0 == strcmp (velodyne_model, VELODYNE_HDL_64E_S2_MODEL_STR))
        self->calib = velodyne_calib_create (VELODYNE_SENSOR_TYPE_HDL_64E_S2, calib_file_path);    
    else 
        fprintf (stderr, "ERROR: Unknown Velodyne model \'%s\'", velodyne_model);
    
    self->collector = velodyne_laser_return_collector_create (1, 0, 2* M_PI); // full scan

    self->velodyne_data_circ = bot_ptr_circular_new (VELODYNE_DATA_CIRC_SIZE,
                                                     circ_free_velodyne_data, self);

    // Subscribe to the kinect image
    kinect_frame_msg_t_subscribe(kinect.lcm, "KINECT_FRAME", on_kinect_image_frame, NULL);
    senlcm_velodyne_list_t_subscribe (kinect.lcm, "VELODYNE_LIST", on_velodyne_list, self);
    //bot_core_image_t_subscribe(kinect.lcm, "FRNT_IMAGE_BUMBLEBEE", on_bumblebee_image_frame, NULL);

    // Install signal handler to free data.
    signal(SIGINT, INThandler);

    while(1) { 
        lcm_handle(kinect.lcm);
        unsigned char c = cv::waitKey(10) & 0xff;
        if (c == 'q') { 
            break;
        } else if (c == ' ') { 
            addCalibrationSnapshot();
        } else if (c == 'c') { 
            calibrate();
        }
    }

    std::vector<kinect_rgbd_pair> calib_pairs = getFiles(std::string(argv[1]));

    return 0;
}
