#ifndef __calib_viewer_h__
#define ___calib_viewer_h__

#include <lcm/lcm.h>

#include <bot_vis/bot_vis.h>
#include <bot_frames/bot_frames.h>

#ifdef __cplusplus
extern "C" {
#endif

void add_calib_renderer_to_viewer(BotViewer* viewer, int priority,lcm_t* lcm, BotFrames * frames, const char * frame);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif
