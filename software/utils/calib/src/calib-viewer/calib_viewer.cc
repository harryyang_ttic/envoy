#include "calib_viewer.h"
#include <lcm/lcm.h>

#include <bot_core/bot_core.h>
#include <bot_vis/bot_vis.h>
#include <bot_frames/bot_frames.h>

#include "lcmtypes/er_lcmtypes.h" 

#include <kinect/kinect-utils.h>
#include <deque>
#include <iostream>
#include <limits>
#include <algorithm>

#define PARAM_NAME_SHOW_VELODYNE "Show Velodyne Point Cloud"
#define PARAM_NAME_TRANSLUCENT_CLOUD "Translucent Point Cloud"
#define PARAM_NAME_TF_TRAIL "Show TF Trail"
#define PARAM_NAME_SHOW_KINECT "Show Kinect Point Cloud"
#define PI 3.141592

std::deque<bot_core_rigid_transform_t*> tf_msgs;
typedef struct _CalibViewer {
    BotRenderer renderer;
    BotGtkParamWidget *pw;
    BotViewer   *viewer;
    lcm_t     *lcm;
    BotFrames *frames;
    char * frame;

    erlcm_xyzrgb_point_list_t* kinect_point_list_msg;
    erlcm_xyzrgb_point_list_t* velodyne_point_list_msg;
    erlcm_normal_point_list_t *normals_msg;
    erlcm_segment_list_t *segments_msg; 
    // bot_core_rigid_transform_t* tf_msg;


    int width, height;

    bool show_velodyne;
    bool show_kinect;
    bool show_tf_trail;

    // GLUquadricObj *quadratic;               // Storage For Our Quadratic Objects ( NEW )
    
} CalibViewer;

static void 
on_point_list (const lcm_recv_buf_t *rbuf, const char *channel,
        const erlcm_xyzrgb_point_list_t *point_list_msg, void *user_data )
{
    CalibViewer *self = (CalibViewer*) user_data;

    
    if (strcmp(channel, "KINECT_POINT_LIST") == 0) { 
        if(self->kinect_point_list_msg)
            erlcm_xyzrgb_point_list_t_destroy(self->kinect_point_list_msg);
        self->kinect_point_list_msg = erlcm_xyzrgb_point_list_t_copy(point_list_msg);
    } else if (strcmp(channel, "VELODYNE_POINT_LIST") == 0) { 
        if(self->velodyne_point_list_msg)
            erlcm_xyzrgb_point_list_t_destroy(self->velodyne_point_list_msg);
        self->velodyne_point_list_msg = erlcm_xyzrgb_point_list_t_copy(point_list_msg);
    } else { 
        printf("Unknown channel\n");
    }
    bot_viewer_request_redraw(self->viewer);

}

static void
on_normal_points (const lcm_recv_buf_t *rbuf, const char *channel,
             const erlcm_normal_point_list_t *msg, void *user)
{
    CalibViewer *self = (CalibViewer*) user;
    if(self->normals_msg){
        erlcm_normal_point_list_t_destroy(self->normals_msg);
    }
    self->normals_msg = erlcm_normal_point_list_t_copy(msg);
    bot_viewer_request_redraw(self->viewer);
}

static void
on_segment_points (const lcm_recv_buf_t *rbuf, const char *channel,
             const erlcm_segment_list_t *msg, void *user)
{
    CalibViewer *self = (CalibViewer*) user;
    if(self->segments_msg){
        erlcm_segment_list_t_destroy(self->segments_msg);
    }
    self->segments_msg = erlcm_segment_list_t_copy(msg);
    bot_viewer_request_redraw(self->viewer);    
}

// static void 
// on_rigid_transform (const lcm_recv_buf_t *rbuf, const char *channel,
// 		   const bot_core_rigid_transform_t *tf_msg, void *user_data )
// {
//     CalibViewer *self = (CalibViewer*) user_data;

//     if(self->tf_msg)
//         bot_core_rigid_transform_t_destroy(self->tf_msg);
//     self->tf_msg = bot_core_rigid_transform_t_copy(tf_msg);

//     tf_msgs.push_front(bot_core_rigid_transform_t_copy(tf_msg));

//     bot_viewer_request_redraw(self->viewer);
// }

static void on_param_widget_changed (BotGtkParamWidget *pw, const char *name, void *user)
{
    CalibViewer *self = (CalibViewer*) user;

    if (!&self->renderer)
    	return;
    
    //show cloud updates
    if (strcmp(name, PARAM_NAME_SHOW_VELODYNE) == 0)
        self->show_velodyne = bot_gtk_param_widget_get_bool(self->pw, PARAM_NAME_SHOW_VELODYNE);
    else if (strcmp(name, PARAM_NAME_TF_TRAIL) == 0)
        self->show_tf_trail = bot_gtk_param_widget_get_bool(self->pw, PARAM_NAME_TF_TRAIL);
    else if (strcmp(name, PARAM_NAME_SHOW_KINECT) == 0)
        self->show_kinect = bot_gtk_param_widget_get_bool(self->pw, PARAM_NAME_SHOW_KINECT);

    bot_viewer_request_redraw(self->viewer);
}

// static inline void
// _matrix_vector_multiply_3x4_4d (const double m[12], const double v[4],
//         double result[3])
// {
//     result[0] = m[0]*v[0] + m[1]*v[1] + m[2] *v[2] + m[3] *v[3];
//     result[1] = m[4]*v[0] + m[5]*v[1] + m[6] *v[2] + m[7] *v[3];
//     result[2] = m[8]*v[0] + m[9]*v[1] + m[10]*v[2] + m[11]*v[3];
// }

// static inline void
// _matrix_transpose_4x4d (const double m[16], double result[16])
// {
//     result[0] = m[0];
//     result[1] = m[4];
//     result[2] = m[8];
//     result[3] = m[12];
//     result[4] = m[1];
//     result[5] = m[5];
//     result[6] = m[9];
//     result[7] = m[13];
//     result[8] = m[2];
//     result[9] = m[6];
//     result[10] = m[10];
//     result[11] = m[14];
//     result[12] = m[3];
//     result[13] = m[7];
//     result[14] = m[11];
//     result[15] = m[15];
// }

// static void 
// draw_tf(double* axis_to_local_m, float size, float lineThickness, float opacity) {
//   // double axis_to_local_m[16];
//   // bot_trans_get_mat_4x4(axis_to_local, axis_to_local_m);

//   // opengl expects column-major matrices
//   double axis_to_local_m_opengl[16];
//   bot_matrix_transpose_4x4d(axis_to_local_m, axis_to_local_m_opengl);

//   glPushMatrix();
//   // rotate and translate the vehicle
//   glMultMatrixd(axis_to_local_m_opengl);

//   glEnable(GL_BLEND);
//   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//   glEnable(GL_DEPTH_TEST);

//   glLineWidth(lineThickness);
//   //x-axis
//   glBegin(GL_LINES);
//   glColor4f(1, 0, 0, opacity);
//   glVertex3f(size, 0, 0);
//   glVertex3f(0, 0, 0);
//   glEnd();

//   //y-axis
//   glBegin(GL_LINES);
//   glColor4f(0, 1, 0, opacity);
//   glVertex3f(0, size, 0);
//   glVertex3f(0, 0, 0);
//   glEnd();

//   //z-axis
//   glBegin(GL_LINES);
//   glColor4f(0, 0, 1, opacity);
//   glVertex3f(0, 0, size);
//   glVertex3f(0, 0, 0);
//   glEnd();

//   glDisable(GL_BLEND);
//   glDisable(GL_DEPTH_TEST);

//   glPopMatrix();

// }

static void _draw(BotViewer *viewer, BotRenderer *renderer)
{

    CalibViewer *self = (CalibViewer*) renderer->user;

    // Draw Kinect Point Cloud
    if (self->kinect_point_list_msg) { 
        // Draw point cloud
        glEnable(GL_DEPTH_TEST);
        glBegin(GL_POINTS);
        for (int j=0; j<self->kinect_point_list_msg->no_points; j++) { 

            glColor3f( ((self->kinect_point_list_msg->points[j].rgb >> 16) & 0x0000ff) * 1.f / 255, 
                       ((self->kinect_point_list_msg->points[j].rgb >> 8) & 0x0000ff) * 1.f / 255, 
                       ((self->kinect_point_list_msg->points[j].rgb) & 0x0000ff) * 1.f / 255 );

            glVertex3f(self->kinect_point_list_msg->points[j].xyz[0], 
                       self->kinect_point_list_msg->points[j].xyz[1], 
                       self->kinect_point_list_msg->points[j].xyz[2]);
        }
        glEnd();
    }

    // Draw Kinect Point Cloud
    if (self->velodyne_point_list_msg) { 
        // Draw point cloud
        glEnable(GL_DEPTH_TEST);
        glBegin(GL_POINTS);
        for (int j=0; j<self->velodyne_point_list_msg->no_points; j++) { 
            
            glColor3f( ((self->velodyne_point_list_msg->points[j].rgb >> 16) & 0x0000ff) * 1.f / 255, 
                       ((self->velodyne_point_list_msg->points[j].rgb >> 8) & 0x0000ff) * 1.f / 255, 
                       ((self->velodyne_point_list_msg->points[j].rgb) & 0x0000ff) * 1.f / 255 );

            glVertex3f(self->velodyne_point_list_msg->points[j].xyz[0], 
                       self->velodyne_point_list_msg->points[j].xyz[1], 
                       self->velodyne_point_list_msg->points[j].xyz[2]);
        }
        glEnd();
    }


    if(self->normals_msg){      
        glPushAttrib (GL_DEPTH_BUFFER_BIT | GL_POINT_BIT | GL_CURRENT_BIT);
        glEnable (GL_DEPTH_TEST);
        glEnable(GL_BLEND);

        double alpha = 1.0;
        double scale = 0.05;
        
        glPointSize(1.0f);

        glColor4d(1.0, 0.0, 0, alpha);

        for(int i=0; i< self->normals_msg->no_points; i++){
            glBegin(GL_LINE_STRIP);
            glVertex3d (self->normals_msg->points[i].xyz[0], 
                        self->normals_msg->points[i].xyz[1],
                        self->normals_msg->points[i].xyz[2]);

            glVertex3d (self->normals_msg->points[i].xyz[0] + self->normals_msg->points[i].normals[0] * scale,
                        self->normals_msg->points[i].xyz[1] + self->normals_msg->points[i].normals[1] * scale,
                        self->normals_msg->points[i].xyz[2] + self->normals_msg->points[i].normals[2]*scale);
            glEnd();            
        }

        glPointSize(2.0f);
        
        glBegin(GL_POINTS);
        glColor4d(0.0, .0, 1.0, alpha);
        
        for(int i=0; i< self->normals_msg->no_points; i++){
            glVertex3d (self->normals_msg->points[i].xyz[0], 
                        self->normals_msg->points[i].xyz[1],
                        self->normals_msg->points[i].xyz[2]);
        }
        glEnd();         
        glPopAttrib();
    }

    if(self->segments_msg){
        glPushAttrib (GL_DEPTH_BUFFER_BIT | GL_POINT_BIT | GL_CURRENT_BIT);
        glEnable (GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glPointSize(1.f);
        glBegin(GL_POINTS);
        double alpha = 1.0;
    
        for(int i=0; i< self->segments_msg->no_segments; i++){
            erlcm_seg_point_list_t *seg_msg = &self->segments_msg->segments[i];
            //for now pick one color (to-do pick from palet)
            float *color = bot_color_util_jet(i/(double) self->segments_msg->no_segments);
            glColor4d(color[0], color[1], color[2], alpha);

            //free(color);

            // fprintf(stderr, "Segment : %d => Points : %d\n", i, (int)seg_msg->no_points);

            for(int j=0; j< seg_msg->no_points; j++){
                glVertex3d (seg_msg->points[j].xyz[0], seg_msg->points[j].xyz[1], 
                            seg_msg->points[j].xyz[2]);
            }
        }    
    
        glEnd();
        glPopAttrib();
    }

  return;
}

static void _free(BotRenderer *renderer)
{
    CalibViewer *self = (CalibViewer*) renderer;

    if(self->kinect_point_list_msg)
        erlcm_xyzrgb_point_list_t_destroy(self->kinect_point_list_msg);
    if(self->velodyne_point_list_msg)
        erlcm_xyzrgb_point_list_t_destroy(self->velodyne_point_list_msg);
    // for (int j=0; j<tf_msgs.size(); j++)
    //     bot_core_rigid_transform_t_destroy(tf_msgs[j]);

    free(self);
}

void 
add_calib_renderer_to_viewer(BotViewer* viewer, int priority, lcm_t* lcm, BotFrames * frames, const char * frame)
{
    CalibViewer *self = (CalibViewer*) calloc(1, sizeof(CalibViewer));

    self->frames = frames;
    if (self->frames!=NULL)
      self->frame = strdup(frame);

    self->kinect_point_list_msg = NULL;
    self->velodyne_point_list_msg = NULL;
    self->normals_msg = NULL;
    self->segments_msg = NULL;
    // self->tf_msg = NULL;

    BotRenderer *renderer = &self->renderer;

    self->lcm = lcm;
    self->viewer = viewer;
    self->pw = BOT_GTK_PARAM_WIDGET(bot_gtk_param_widget_new());
    self->show_velodyne = false;
    self->show_kinect = true;
    self->show_tf_trail = false;

    renderer->draw = _draw;
    renderer->destroy = _free;
    renderer->name = "Calib Viewer";
    renderer->widget = GTK_WIDGET(self->pw);
    renderer->enabled = 1;
    renderer->user = self;

    bot_gtk_param_widget_add_booleans(self->pw, 
                                      BOT_GTK_PARAM_WIDGET_CHECKBOX, 
                                      PARAM_NAME_SHOW_VELODYNE, 0, NULL);
    bot_gtk_param_widget_add_booleans(self->pw, 
                                      BOT_GTK_PARAM_WIDGET_CHECKBOX, 
                                      PARAM_NAME_TRANSLUCENT_CLOUD, 0, NULL);
    bot_gtk_param_widget_add_booleans(self->pw, 
                                      BOT_GTK_PARAM_WIDGET_CHECKBOX, 
                                      PARAM_NAME_TF_TRAIL, 0, NULL);
    bot_gtk_param_widget_add_booleans(self->pw, 
                                      BOT_GTK_PARAM_WIDGET_CHECKBOX, 
                                      PARAM_NAME_SHOW_KINECT, 0, NULL);

    g_signal_connect (G_OBJECT (self->pw), "changed",
                      G_CALLBACK (on_param_widget_changed), self);

    erlcm_xyzrgb_point_list_t_subscribe(self->lcm, "KINECT_POINT_LIST", on_point_list, self);
    erlcm_xyzrgb_point_list_t_subscribe(self->lcm, "VELODYNE_POINT_LIST", on_point_list, self);
    erlcm_normal_point_list_t_subscribe(self->lcm, "PCL_NORMAL_LIST", on_normal_points, self);
    erlcm_segment_list_t_subscribe(self->lcm, "PCL_SEGMENT_LIST", on_segment_points, self);

    bot_viewer_add_renderer(viewer, renderer, priority);
}
