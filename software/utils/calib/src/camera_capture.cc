#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <lcmtypes/er_lcmtypes.h>

#include <lcmtypes/erlcm_roi_t.h>
#include "kinect_opencv_utils.h"

#include <lcmtypes/kinect_frame_msg_t.h>
#include <kinect/kinect-utils.h>
#include <pcl/common/common_headers.h>
#include <zlib.h>

kinect_data kinect;

struct ts_image { 
    double utime;
    cv::Mat1b img;
};

std::deque<ts_image> df_imgs;
std::deque<ts_image> kt_imgs;

int img_count = 0;

void sync_capture() { 
    // for (int j=0; j<df_imgs.size(); j++) 
    //     printf("df: %i %f\n", j, df_imgs[j].utime);
    // // std::cerr << "df t: " << df_imgs[j].utime * 1e-6 << std::endl;

    // for (int j=0; j<kt_imgs.size(); j++) 
    //     printf("kt: %i %f\n", j, kt_imgs[j].utime);
    // // std::cerr << "kt t: " << kt_imgs[j].utime * 1e-6 << std::endl;
    
    if (df_imgs.size() > 10) { 
        int idx = df_imgs.size()/2;
        double utime = df_imgs[idx].utime;

        printf("%f\n", utime);
        // std::cerr << "search: " << utime * 1e-6 << std::endl;

        double min=1e6; 
        int min_idx = -1;
        for (int j=0; j<kt_imgs.size(); j++) {
            double dt = abs(kt_imgs[j].utime - utime);
            if (dt < min) { 
                min = dt;
                min_idx = j;
            }
        }

        std::cerr << "min_idx: " << min_idx << std::endl;
        printf("%f %f diff: %f\n", kt_imgs[min_idx].utime, utime, min * 1e-6);;

        std::stringstream ss; 
        ss << img_count++ << ".png";
        imwrite("kinect_img_"+ss.str(), kt_imgs[min_idx].img);
        imwrite("dragonfly_img_"+ss.str(), df_imgs[idx].img);
        std::cerr << "writing both images" << std::endl;
        std::cerr << "============" << std::endl;
    }

}

static void on_kinect_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                                   const kinect_frame_msg_t *msg, 
                                   void *user_data ) {
    
    if (msg->depth.depth_data_format != KINECT_DEPTH_MSG_T_DEPTH_MM) { 
        std::cerr << "Warning: kinect data format is not KINECT_DEPTH_MSG_T_DEPTH_MM" << std::endl 
                  << "Program may not function properly" << std::endl;
    }
    assert (msg->depth.depth_data_format == KINECT_DEPTH_MSG_T_DEPTH_MM);

    kinect.update(msg);

    cv::Mat3b img = kinect.getRGB();

    ts_image im;
    im.utime = msg->timestamp;
    cvtColor(img, im.img, CV_RGB2GRAY);

    if (kt_imgs.size() > 20) 
        kt_imgs.pop_back();
    kt_imgs.push_front(im);

    imshow("Kinect", img);

    return;
}


int envoy_decompress_bot_core_image_t(const bot_core_image_t * jpeg_image, bot_core_image_t *uncomp_image)
{
  //copy metadata
  uncomp_image->utime = jpeg_image->utime;
  if (uncomp_image->metadata != NULL)
    bot_core_image_metadata_t_destroy(uncomp_image->metadata);
  uncomp_image->nmetadata = jpeg_image->nmetadata;
  if (jpeg_image->nmetadata > 0) {
    uncomp_image->metadata = bot_core_image_metadata_t_copy(jpeg_image->metadata);
  }
  
  uncomp_image->width = jpeg_image->width;
  uncomp_image->height = jpeg_image->height;
  
  uncomp_image->row_stride = jpeg_image->width * 3;//jpeg_image->row_stride;

  int depth=0;
  if (uncomp_image->width>0)
      depth= uncomp_image->row_stride / (double)uncomp_image->width;

  int ret;

  if(uncomp_image->data == NULL){
      int uncomp_size = (uncomp_image->width) * (uncomp_image->height) * depth;
      uncomp_image->data = (unsigned char *) realloc(uncomp_image->data, uncomp_size * sizeof(uint8_t));
  }

  ret = jpeg_decompress_8u_rgb(jpeg_image->data, 
                               jpeg_image->size, uncomp_image->data, 
                               uncomp_image->width, 
                               uncomp_image->height, 
                               uncomp_image->row_stride);
  
  uncomp_image->row_stride = uncomp_image->width * depth;
  uncomp_image->size = uncomp_image->width *uncomp_image->height* depth;
  if (depth == 1) {
    uncomp_image->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY;
  }
  else if (depth == 3) {
    uncomp_image->pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB; //TODO: lets just assume its rgb... dunno what else to do :-/
  }
  return ret;
}

static void on_bumblebee_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                                   const bot_core_image_t *msg, 
                                   void *user_data ) {
    if (msg->width != WIDTH || msg->height != HEIGHT)
        return;
    cv::Mat3b bb_img = cv::Mat3b::zeros(msg->height, msg->width);
    
    static bot_core_image_t * localFrame=(bot_core_image_t *) calloc(1,sizeof(bot_core_image_t));
    if (msg->pixelformat==BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG){
        //create space for decompressed image
        envoy_decompress_bot_core_image_t(msg,localFrame);
        memcpy(bb_img.data, localFrame->data, sizeof(uint8_t)*msg->width*msg->height*3);            
    } else{
        //uncompressed, just copy pointer
        memcpy(bb_img.data, msg->data, sizeof(uint8_t)*msg->width*msg->height*3);            
    }
    cvtColor(bb_img, bb_img, CV_BGR2RGB);

    imshow("bumblebee", bb_img);

    return;
}

static void on_dragonfly_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                                      const bot_core_image_t *msg, 
                                      void *user_data ) {
    cv::Mat1b bb_img = cv::Mat1b::zeros(msg->height, msg->width);
   
    static bot_core_image_t * localFrame=(bot_core_image_t *) calloc(1,sizeof(bot_core_image_t));
    if (msg->pixelformat==BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG){
        //create space for decompressed image
        envoy_decompress_bot_core_image_t(msg,localFrame);
        memcpy(bb_img.data, localFrame->data, sizeof(uint8_t)*msg->width*msg->height*3);            
    } else{
        //uncompressed, just copy pointer
        memcpy(bb_img.data, msg->data, sizeof(uint8_t)*msg->width*msg->height);
    }
    // cvtColor(bb_img, bb_img, CV_BGR2RGB);

    ts_image im;
    im.utime = msg->utime;
    im.img = bb_img.clone();
    // cvtColor(bb_img, im.img, CV_RGB2GRAY);

    if (df_imgs.size() > 20) 
        df_imgs.pop_back();
    df_imgs.push_front(im);

    imshow("dragonfly", bb_img);
    return;
}

void  INThandler(int sig)
{
    //lcm_destroy(lcm);
    printf("Exiting\n");
    exit(0);
}

static void usage(const char* progname)
{
  fprintf (stderr, "Usage: %s [options]\n"
                   "\n"
                   "Options:\n"
                   "  -l URL    Specify LCM URL\n"
                   "  -h        This help message\n", 
                   g_path_get_basename(progname));
  exit(1);
}

int main(int argc, char** argv)
{

    int c;
    char *lcm_url = NULL;

    // command line options - to publish on a specific url  
    while ((c = getopt (argc, argv, "vhir:jq:zl:")) >= 0) {
        switch (c) {
        case 'l':
            lcm_url = strdup(optarg);
            printf("Using LCM URL \"%s\"\n", lcm_url);
            break;
        case 'h':
        case '?':
            usage(argv[0]);
        }
    }

    // LCM-related
    kinect.lcm = lcm_create(lcm_url);
    if(!kinect.lcm) {
        printf("Unable to initialize LCM\n");
        return 1;
    } 
  
    cv::namedWindow( "Kinect" );

    // Install signal handler to free data
    signal(SIGINT, INThandler);

    //add lcm to mainloop 
    kinect_frame_msg_t_subscribe(kinect.lcm, "KINECT_FRAME", on_kinect_image_frame, NULL);
    bot_core_image_t_subscribe(kinect.lcm, "FRNT_IMAGE_BUMBLEBEE", on_bumblebee_image_frame, NULL);
    bot_core_image_t_subscribe(kinect.lcm, "DRAGONFLY_IMAGE", on_dragonfly_image_frame, NULL);

    while(1) { 
        lcm_handle(kinect.lcm);
        unsigned char c = cv::waitKey(10) & 0xff;
        if (c == 'q') { 
            break;
        } else if (c == 'c') { 
            printf("Capturing\n");
            sync_capture();
            // capture = true;
        }
    }

    fprintf(stderr, "Starting Main Loop\n");
    return 0;
}
