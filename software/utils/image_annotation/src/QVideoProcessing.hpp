#ifndef _QVideoProcessing_h
#define _QVideoProcessing_h

#include <QFileInfo>
class QString;

class QVideoProcessing
{
public:
    QVideoProcessing(void);
public:
    ~QVideoProcessing(void);
    static QFileInfoList findimagefiles(const QString& path);
    static QFileInfoList findImageFilesSingleFormat(const QString& path);
    static bool FileListExist(const QString& pathName,const QStringList& fileList);
    static void separatePath_FileNames(const QString& fullname,QString& pathname,QString& filename);
};

#endif