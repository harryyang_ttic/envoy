#include <iostream>

#include <QtGui>
#include "MainWindow.hpp"
#include "QVideoProcessing.hpp"

#include <lcm/lcm.h>

MainWindow::MainWindow(const QString& lcmLog, const QString& channel, const bool beVerbose)
{
    scrollArea = new QScrollArea;
    imageViewer=new ImageViewer(scrollArea);

    // creating the scoll area for ImageViewer
    scrollArea->setBackgroundRole(QPalette::Dark);
    scrollArea->setWidget(imageViewer);
    setCentralWidget(scrollArea);
    connect(imageViewer,SIGNAL(adjustScrollBar(double)),this,SLOT(adaptScrollBar(double)));

    pVideoLabeling=new VideoLabeling(this);

    // the widgets for changing the frame index
    spinBoxFrameIndex=new QSpinBox;
    sliderFrameIndex=new QSlider(Qt::Horizontal);
    spinBoxFrameIndex->setRange(1,10);
    sliderFrameIndex->setRange(1,10);
    sliderFrameIndex->setFixedWidth(550);
    connect(spinBoxFrameIndex,SIGNAL(valueChanged(int)),sliderFrameIndex,SLOT(setValue(int)));
    connect(sliderFrameIndex,SIGNAL(valueChanged(int)),spinBoxFrameIndex,SLOT(setValue(int)));
    
    depthControl=new DepthControl(this); 
     
    sliderFrameIndex->setValue(1);
    
    createActions();
    createMenus();
    createToolBar();
    
    binding();
    
    setWindowTitle(tr("Motion Ground-Truth Annotation"));
    setWindowIcon(QIcon(":/icons/camera-video.png"));

    resize(1000,760);
    IsProjFileSpecified=false;
    verbose = beVerbose;
    lcmChannel = QString (channel);

    if (!lcmLog.isEmpty())
        openLCMLogFile(lcmLog);
}

MainWindow::~MainWindow(void)
{
    delete pVideoLabeling;
}

void MainWindow::adaptScrollBar(double factor)
{
    QScrollBar *scrollBar;
    scrollBar=scrollArea->horizontalScrollBar();
    scrollBar->setValue(int(factor * scrollBar->value() + ((factor - 1) * scrollBar->pageStep()/2)));

    scrollBar=scrollArea->verticalScrollBar();
    scrollBar->setValue(int(factor * scrollBar->value() + ((factor - 1) * scrollBar->pageStep()/2)));
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    imageViewer->setParentSize(scrollArea->frameSize());
}

void MainWindow::binding()
{
    connect(pVideoLabeling,SIGNAL(NumFramesChanged(int)),this,SLOT(setFrameIndexRange(int)));
    connect(pVideoLabeling,SIGNAL(ImageChanged(QImage&)),imageViewer,SLOT(loadImage(QImage&)));
    connect(pVideoLabeling,SIGNAL(FrameControlChanged(bool,bool,bool,bool)),this,SLOT(setFrameControl(bool,bool,bool,bool)));
    connect(pVideoLabeling,SIGNAL(FrameIndexChanged(int)),sliderFrameIndex,SLOT(setValue(int)));
    connect(pVideoLabeling,SIGNAL(Update()),imageViewer,SLOT(redraw()));
    connect(pVideoLabeling,SIGNAL(changeLayerDepth(const QList<LayerDepth>&,int,int)),this,SLOT(setLayerDepth(const QList<LayerDepth>&,int,int)));
    connect(depthControl,SIGNAL(LayerDepthInterpolated(const LayerDepth&)),pVideoLabeling,SLOT(setLayerDepth(const LayerDepth&)));

    connect(imageViewer,SIGNAL(viewerMouseEvent(ImageViewer& , ImageViewer::MouseAction& ,QPoint& )),pVideoLabeling,SLOT(ProcessMouseEvent(ImageViewer&, ImageViewer::MouseAction&,QPoint&)));
    connect(imageViewer,SIGNAL(paintingImageViewer(ImageViewer&,QPainter&)),pVideoLabeling,SLOT(Paint(ImageViewer&,QPainter&)));

     connect(sliderFrameIndex,SIGNAL(valueChanged(int)),pVideoLabeling,SLOT(gotoFrame(int)));
     connect(nextFrameAct,SIGNAL(triggered()),pVideoLabeling,SLOT(gotoNextFrame()));
     connect(preFrameAct,SIGNAL(triggered()),pVideoLabeling,SLOT(gotoPreFrame()));
     connect(firstFrameAct,SIGNAL(triggered()),pVideoLabeling,SLOT(gotoFirstFrame()));
     connect(lastFrameAct,SIGNAL(triggered()),pVideoLabeling,SLOT(gotoLastFrame()));
}

void MainWindow::createActions()
{
    //-----------------------------------------------------------------------------------------------------------------
    // the actions of file IO, closing or opening the program
     openFolderAct = new QAction(tr("&Open LCM log..."), this);
     openFolderAct->setShortcut(tr("Ctrl+O"));
     openFolderAct->setIcon(QIcon(":/icons/Open16.png"));
     //connect(openFolderAct, SIGNAL(triggered()), this, SLOT(openFolder()));
     connect(openFolderAct, SIGNAL(triggered()), this, SLOT(openLCMLogFile()));

     loadProjectAct=new QAction(tr("&Load project..."),this);
     loadProjectAct->setShortcut(tr("Ctrl+P"));
     loadProjectAct->setIcon(QIcon(":/icons/folder-open.png"));
     connect(loadProjectAct,SIGNAL(triggered()),this,SLOT(load()));

     saveAct = new QAction(tr("&Save project..."),this);
     saveAct->setShortcut(tr("Ctrl+S"));
     saveAct->setIcon(QIcon(":/icons/Save16.png"));
     saveAct->setEnabled(false);
     connect(saveAct,SIGNAL(triggered()),this,SLOT(save()));

     saveAsAct=new QAction(tr("Save &as project..."),this);
     saveAsAct->setShortcut(tr("Ctrl+A"));
     saveAsAct->setEnabled(false);
     connect(saveAsAct,SIGNAL(triggered()),this,SLOT(saveAs()));

     exportAct=new QAction(tr("Export labeling..."),this);
     exportAct->setShortcut(tr("Ctrl+E"));
     exportAct->setEnabled(false);
     connect(exportAct,SIGNAL(triggered()),this,SLOT(exportLabeling()));

     exitAct = new QAction(tr("E&xit"), this);
     exitAct->setShortcut(tr("Ctrl+Q"));
     connect(exitAct, SIGNAL(triggered()), this, SLOT(cleanupClose()));

     //-----------------------------------------------------------------------------------------------------------------
     // the actions of zoom in, zoom out and set to normal size
     zoomInAct = new QAction(tr("Zoom &In"), this);
     zoomInAct->setShortcut(tr("="));
     zoomInAct->setEnabled(true);
     zoomInAct->setIcon(QIcon(":/icons/zoom_in.png"));
     connect(zoomInAct, SIGNAL(triggered()), imageViewer, SLOT(zoomIn()));
     connect(imageViewer,SIGNAL(setZoomIn(bool)),zoomInAct,SLOT(setEnabled(bool)));

     zoomOutAct = new QAction(tr("Zoom &Out"), this);
     zoomOutAct->setShortcut(tr("-"));
     zoomOutAct->setEnabled(true);
     zoomOutAct->setIcon(QIcon(":/icons/zoom_out.png"));
     connect(zoomOutAct, SIGNAL(triggered()), imageViewer, SLOT(zoomOut()));
     connect(imageViewer,SIGNAL(setZoomOut(bool)),zoomOutAct,SLOT(setEnabled(bool)));

     normalSizeAct = new QAction(tr("&Normal Size"), this);
     normalSizeAct->setShortcut(tr("S"));
     normalSizeAct->setEnabled(true);
     normalSizeAct->setIcon(QIcon(":/icons/zoom_previous.png"));
     connect(normalSizeAct, SIGNAL(triggered()), imageViewer, SLOT(normalSize()));

     //-----------------------------------------------------------------------------------------------------------------
     // the actions of viewing the video by frames

     nextFrameAct=new QAction(tr("Next frame"),this);
     nextFrameAct->setShortcut(tr("Right"));
     nextFrameAct->setEnabled(false);
     nextFrameAct->setIcon(QIcon(":/icons/go-next.png"));
     

     preFrameAct=new QAction(tr("Previous frame"),this);
     QList<QKeySequence> mKeyList;
     mKeyList.append(QKeySequence(tr("Left")));
     mKeyList.append(QKeySequence(tr("Space")));
     preFrameAct->setShortcuts(mKeyList);
     preFrameAct->setEnabled(false);
     preFrameAct->setIcon(QIcon(":/icons/go-previous.png"));
     

     firstFrameAct=new QAction(tr("First frame"),this);
     firstFrameAct->setShortcut(tr("Home"));
     firstFrameAct->setEnabled(false);
     firstFrameAct->setIcon(QIcon(":/icons/go-first.png"));
     
     lastFrameAct=new QAction(tr("Last frame"),this);
     lastFrameAct->setShortcut(tr("End"));
     lastFrameAct->setEnabled(false);
     lastFrameAct->setIcon(QIcon(":/icons/go-last.png"));
    

}

void MainWindow::createMenus()
{
     fileMenu = new QMenu(tr("&File"), this);
     fileMenu->addAction(openFolderAct);
     fileMenu->addAction(loadProjectAct);
     fileMenu->addAction(saveAct);
     fileMenu->addAction(saveAsAct);
     fileMenu->addAction(exportAct);
     fileMenu->addAction(exitAct);

     viewMenu=new  QMenu(tr("&View"), this);
     viewMenu->addAction(zoomInAct);
     viewMenu->addAction(zoomOutAct);
     viewMenu->addAction(normalSizeAct);

     menuBar()->addMenu(fileMenu);
     menuBar()->addMenu(viewMenu);
}

void MainWindow::createToolBar()
{
    fileToolBar=addToolBar(tr("&File (open, load project, save project)"));
    fileToolBar->addAction(openFolderAct);
    fileToolBar->addAction(loadProjectAct);
    fileToolBar->addAction(saveAct);

    // create the tool bar for zoom-in and zoom-out
    zoomToolBar=addToolBar(tr("&Zoom (zoom-in, zoom-out, reset)"));
    zoomToolBar->addAction(zoomInAct);
    zoomToolBar->addAction(zoomOutAct);
    zoomToolBar->addAction(normalSizeAct);

    //-----------------------------------------------------------------------------------------------------------------
     // create the toolbar for viewing video frames
     videoToolBar=new QToolBar(tr("&Change frames"));
     videoToolBar->addAction(firstFrameAct);
     videoToolBar->addAction(preFrameAct);
     videoToolBar->addAction(nextFrameAct);
     videoToolBar->addAction(lastFrameAct);
     videoToolBar->addSeparator();


     QWidget* windowFrameIndex=new QWidget;
    //windowFrameIndex->setMaximumWidth(300);
    QLabel* labelFrameIndex=new QLabel(tr("Frame index:"));
    labelFrameIndex->setBuddy(spinBoxFrameIndex);
    QHBoxLayout *layoutFrameIndex = new QHBoxLayout;
    layoutFrameIndex->addWidget(labelFrameIndex);
    layoutFrameIndex->addWidget(spinBoxFrameIndex);
     layoutFrameIndex->addWidget(sliderFrameIndex);
    windowFrameIndex->setLayout(layoutFrameIndex);
    videoToolBar->addWidget(windowFrameIndex);

     addToolBar(Qt::BottomToolBarArea,videoToolBar);

}

void MainWindow::setFrameIndexRange(int nFrames)
{
    spinBoxFrameIndex->setRange(1,nFrames);
    sliderFrameIndex->setRange(1,nFrames);
}

void MainWindow::setFrameControl(bool firstFrame,bool preFrame,bool nextFrame,bool lastFrame)
{
    firstFrameAct->setEnabled(firstFrame);
    preFrameAct->setEnabled(preFrame);
    nextFrameAct->setEnabled(nextFrame);
    lastFrameAct->setEnabled(lastFrame);
}

void MainWindow::setLayerDepth(const QList<LayerDepth>& layerDepthList,int activeLayerIndex,int frameIndex)
{
    depthControl->loadDepthList(layerDepthList,activeLayerIndex,frameIndex);
    depthControl->exec();
}

void MainWindow::openLCMLogFile(const QString& lcmLog )
{
    lcmLogFile=lcmLog;
    if(!lcmLogFile.isEmpty())
    {
        lcm_eventlog_t *src_log = lcm_eventlog_create ( lcmLogFile.toLatin1(), "r");
        if (!src_log) {
            QMessageBox::warning(this,"Warning","Error opening LCM log");
            return;
        }

        int num_messages = 0;
        for (lcm_eventlog_event_t *event = lcm_eventlog_read_next_event (src_log);
             event != NULL;
             event = lcm_eventlog_read_next_event (src_log)) {
            num_messages += 1;
            lcm_eventlog_free_event (event);
        }

        lcm_eventlog_destroy (src_log);

        QVideoProcessing::separatePath_FileNames(lcmLogFile,projPath,projFileName);

        projFileName+=".xml";

        pVideoLabeling->InitializeLCM(lcmLogFile, lcmChannel, verbose);
        IsProjFileSpecified=false;
        saveAct->setEnabled(true);
        saveAsAct->setEnabled(true);
        exportAct->setEnabled(true);
    }
}


void MainWindow::openLCMLogFile()
{
    lcmLogFile=QFileDialog::getOpenFileName(this,"Specify LCM log file");//,"/home/mwalter/data/","");
    if(!lcmLogFile.isEmpty())
    {
        lcm_eventlog_t *src_log = lcm_eventlog_create ( lcmLogFile.toLatin1(), "r");
        if (!src_log) {
            QMessageBox::warning(this,"Warning","Error opening LCM log");
            return;
        }

        int num_messages = 0;
        for (lcm_eventlog_event_t *event = lcm_eventlog_read_next_event (src_log);
             event != NULL;
             event = lcm_eventlog_read_next_event (src_log)) {
            num_messages += 1;
            lcm_eventlog_free_event (event);
        }

        lcm_eventlog_destroy (src_log);

        QVideoProcessing::separatePath_FileNames(lcmLogFile,projPath,projFileName);

        projFileName+=".xml";

        pVideoLabeling->InitializeLCM(lcmLogFile, lcmChannel, verbose);
        IsProjFileSpecified=false;
        saveAct->setEnabled(true);
        saveAsAct->setEnabled(true);
        exportAct->setEnabled(true);
    }
}

void MainWindow::openFolder()
{
    framePath=QFileDialog::getExistingDirectory(this,"Specify path for video frames",framePath);
    if(!framePath.isEmpty())
    {
        QFileInfoList fileList=QVideoProcessing::findImageFilesSingleFormat(framePath);
        if(fileList.size()<2)
        {
            QMessageBox::warning(this,"Warning","No or not enough images found in the folder!");
            return;
        }

        if(framePath.right(1)=="/")
            framePath.chop(1);
        QVideoProcessing::separatePath_FileNames(framePath,projPath,projFileName);
        framePath+="/";
        projFileName+=".xml";

        pVideoLabeling->Initialize(framePath,fileList);
        IsProjFileSpecified=false;
        saveAct->setEnabled(true);
        saveAsAct->setEnabled(true);
        exportAct->setEnabled(true);
    }
}

void MainWindow::exportLabeling()
{
    outputPathName=QFileDialog::getExistingDirectory(this,"Specify path for output labeling",outputPathName);
    if(!outputPathName.isEmpty())
    {
        pVideoLabeling->OutputAllLabeling(outputPathName);
    }
}

void MainWindow::load()
{
    QString filename=QFileDialog::getOpenFileName(this,"Load video labelme format",projPath,tr("XML file (*.xml)"));
    if(!filename.isEmpty())
    {
        if(pVideoLabeling->readVideoLabelMeXML(filename, verbose)==false)
        {
            QMessageBox::warning(this,"Warning","Fail to load the video labelme xml file!");
            return;
        }
        QVideoProcessing::separatePath_FileNames(filename,projPath,projFileName);
        IsProjFileSpecified=true;
        saveAct->setEnabled(true);
        saveAsAct->setEnabled(true);
        exportAct->setEnabled(true);
    }
}

void MainWindow::save()
{
    if(IsProjFileSpecified==true)
    {
        if(pVideoLabeling->writeVideoLabelMeXML(projPath+"/"+projFileName)==false)
            QMessageBox::warning(this,"Warning!","Failed to write the VideoLabelMe XML file!");
    }
    else
        saveAs();
}

void MainWindow::saveAs()
{
    QString filename=QFileDialog::getSaveFileName(this,"Save the labeling to LabelMe format",projPath+"/"+projFileName,tr("XML file (*.xml)"));
    if(!filename.isEmpty())
    {
        if(pVideoLabeling->writeVideoLabelMeXML(filename)==false)
            QMessageBox::warning(this,"Warning!","Failed to write the VideoLabelMe XML file!");
        else
        {
            QVideoProcessing::separatePath_FileNames(filename,projPath,projFileName);
            IsProjFileSpecified=true;
        }
    }
}

void MainWindow::cleanupClose()
{
    pVideoLabeling->freeLCM();

    close();
}
        
        
