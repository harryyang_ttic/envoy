#include <QtGui>
#include <iostream>
#include "LayerObject.hpp"
#include "MyPreference.hpp"

// static variables
int LayerObject::selectedPointIndex=0;
ContourLabel LayerObject::m_OrgContourLabel=ContourLabel();
QPointF LayerObject::m_OrgCursorPoint=QPointF();


LayerObject::LayerObject(void): QObject()
{
    m_NumFrames=0;
    m_StartFrame=0;
    m_EndFrame=0;
    m_CreatedFrame=0;
}

LayerObject::LayerObject(const LayerObject& layerObject):QObject()
{
    copyData(layerObject);
}

LayerObject::~LayerObject(void)
{
}

void LayerObject::copyData(const LayerObject &layerObject)
{
    m_ContourLabelList=layerObject.m_ContourLabelList;
    m_DepthList=layerObject.m_DepthList;
    m_LayerName=layerObject.m_LayerName;

    m_IsGlobalLabelList=layerObject.m_IsGlobalLabelList;
    m_IsLocalLabelList=layerObject.m_IsLocalLabelList;
    m_IsTrackedList=layerObject.m_IsTrackedList;
    m_IsDepthLabeledList=layerObject.m_IsDepthLabeledList;

    m_NumFrames=layerObject.m_NumFrames;
    m_StartFrame=layerObject.m_StartFrame;
    m_EndFrame=layerObject.m_EndFrame;
    m_CreatedFrame=layerObject.m_CreatedFrame;
}

LayerObject& LayerObject::operator =(const LayerObject &layerObject)
{
    copyData(layerObject);
    return *this;
}

void LayerObject::create(int numFrames, int frameIndex, const QPointF& point, int layer_index)
{
    m_NumFrames=numFrames;
    m_StartFrame=frameIndex;
    m_EndFrame=frameIndex;
    m_CreatedFrame=frameIndex;

    QVector<ContourLabel> contourLabelVector(m_NumFrames);
    m_ContourLabelList=QList<ContourLabel>::fromVector(contourLabelVector);
    QVector<bool> labelStatusVector(m_NumFrames,false);
    m_IsGlobalLabelList=QList<bool>::fromVector(labelStatusVector);
    m_IsLocalLabelList=m_IsGlobalLabelList;
    m_IsTrackedList=m_IsGlobalLabelList;
    m_IsDepthLabeledList=m_IsGlobalLabelList;

    m_IsLocalLabelList[frameIndex]=true;
    m_IsGlobalLabelList[frameIndex]=true;
    m_IsDepthLabeledList[frameIndex]=true;
    m_IsTrackedList[frameIndex]=true;

    // add point
    m_ContourLabelList[frameIndex].addPoint(point,true);
    m_ContourLabelList[frameIndex].addPoint(point,true);

    // initialize depth
    int depth=layer_index*30;
    if(depth>255)
        depth=255;
    QVector<double> depthVector(m_NumFrames,depth);
    m_DepthList=QList<double>::fromVector(depthVector);

    // initialize name
    m_LayerName=MyPreference::DefaultLayerName;
    
}

// create the list based on the number of frames
void LayerObject::create(int numFrames)
{
    m_NumFrames=numFrames;
    QVector<ContourLabel> contourLabelVector(m_NumFrames);
    m_ContourLabelList=QList<ContourLabel>::fromVector(contourLabelVector);
    QVector<bool> labelStatusVector(m_NumFrames,false);
    m_IsGlobalLabelList=QList<bool>::fromVector(labelStatusVector);
    m_IsLocalLabelList=m_IsGlobalLabelList;
    m_IsTrackedList=m_IsGlobalLabelList;
    m_IsDepthLabeledList=m_IsGlobalLabelList;

    QVector<double> depthVector(m_NumFrames,0);
    m_DepthList=QList<double>::fromVector(depthVector);
}

//-----------------------------------------------------------------------------------------------------------------------
//     function to stretch the range of the object
//-----------------------------------------------------------------------------------------------------------------------
void LayerObject::StretchToFrame(int frameIndex)
{
    if(InRange(frameIndex))
        return;
    frameIndex=qMin(qMax(frameIndex,0),m_NumFrames-1);
    if(frameIndex>m_EndFrame)
    {
        for(int i=m_EndFrame+1;i<=frameIndex;i++)
        {
            m_ContourLabelList[i]=m_ContourLabelList[m_EndFrame];
            m_ContourLabelList[i].clearLabels();
            m_DepthList[i]=m_DepthList[m_EndFrame];
        }
        m_EndFrame=frameIndex;
    }
    if(frameIndex<m_StartFrame)
    {
        for(int i=frameIndex;i<=m_StartFrame-1;i++)
        {
            m_ContourLabelList[i]=m_ContourLabelList[m_StartFrame];
            m_ContourLabelList[i].clearLabels();
            m_DepthList[i]=m_DepthList[m_StartFrame];
        }
        m_StartFrame=frameIndex;
    }
}

// Return the bounding box for a particular contour
const QRectF& LayerObject::boundingBox (int frameIndex)
{
    //if(InRange(frameIndex)==false)
    //    return NULL;
    return m_ContourLabelList[frameIndex].boundingBox();
}

//-----------------------------------------------------------------------------------------------------------------------
// display functions
//-----------------------------------------------------------------------------------------------------------------------
void LayerObject::DrawContour(int frameIndex, QPainter &painter, QPointF &ScreenOffset, double ScaleFactor, bool IsActive, bool IsCreating, bool IsCursorOver)
{
    if(InRange(frameIndex)==false)
        return;
    m_ContourLabelList[frameIndex].DrawContour(painter,ScreenOffset,ScaleFactor,m_DepthList[frameIndex],IsActive,IsCreating,IsCursorOver,m_IsGlobalLabelList[frameIndex]);
}

void LayerObject::ShowText(int frameIndex, QPainter &painter, QPointF &ScreenOffset, double ScaleFactor)
{
    if(InRange(frameIndex)==false)
        return;
    m_ContourLabelList[frameIndex].ShowText(painter,ScreenOffset,ScaleFactor,m_LayerName);
}

double LayerObject::distanceToContour(int frameIndex, const QPointF& point)
{
    if(InRange(frameIndex)==false)
        return MyPreference::DefaultContourDistance;
    return m_ContourLabelList[frameIndex].distanceToContour(point);
}

void LayerObject::addPoint(int frameIndex, const QPointF &point, bool IsLabeled)
{
    if(InRange(frameIndex)==false)
        return;
    m_ContourLabelList[frameIndex].addPoint(point,IsLabeled);
}

void LayerObject::updateLastPoint(int frameIndex, const QPointF &point, bool IsLabeled)
{
    if(InRange(frameIndex)==false)
        return;
    m_ContourLabelList[frameIndex].updateLastPoint(point,IsLabeled);
}

void LayerObject::removeLastPoint(int frameIndex)
{
    if(InRange(frameIndex)==false)
        return;
    m_ContourLabelList[frameIndex].removeLastPoint();
}

double LayerObject::minPointDistance(int frameIndex, const QPointF &point)
{
    if(InRange(frameIndex)==false)
        return MyPreference::DefaultContourDistance;
    return m_ContourLabelList[frameIndex].minPointDistance(point,selectedPointIndex);
}

double LayerObject::minLineSegDistance(int frameIndex,const QPointF& point)
{
    if(InRange(frameIndex)==false)
        return MyPreference::DefaultContourDistance;
    return m_ContourLabelList[frameIndex].minLineSegDistance(point,selectedPointIndex);
}

bool LayerObject::IsCentroidSelected(int frameIndex, const QPointF &point, double ScaleFactor)
{
    if(InRange(frameIndex)==false)
        return false;
    if(m_ContourLabelList[frameIndex].IsCentroidSelected(point,ScaleFactor))
    {
        m_OrgContourLabel=m_ContourLabelList[frameIndex];
        m_OrgCursorPoint=point;
        return true;
    }
    else
        return false;
}

int LayerObject::selectBoundingBox(int frameIndex,const QPointF &point, double ScaleFactor)
{
    if(InRange(frameIndex)==false)
        return false;
    int result = m_ContourLabelList[frameIndex].selectBoundingBox(point,ScaleFactor,MyPreference::Threshold_BoundingBox);
    if( result >=0)
    {
        m_OrgContourLabel=m_ContourLabelList[frameIndex];
        m_OrgCursorPoint=point;
    }
    return result;
}

//------------------------------------------------------------------------------------------------------------
// function that need to propagate the operation of the current frame to the rest of the frames
//------------------------------------------------------------------------------------------------------------

// function to find the first labeled frame
int LayerObject::findFirstLocalLabeledFrame(int frameIndex,int pointIndex,bool& IsForwardLabeled)
{
    if(frameIndex==m_StartFrame)
    {
        IsForwardLabeled=m_ContourLabelList[m_StartFrame].labeled(pointIndex);
        return m_StartFrame;
    }
    int i;
    IsForwardLabeled=false;
    bool IsInside=m_ContourLabelList[frameIndex].IsInsideImage(pointIndex);
    for(i=frameIndex-1;i>=m_StartFrame;i--)
    {
        // if the intial point is outside image boundary and the i-th one is inside, then don't change the inside one
        if(IsInside==false && m_ContourLabelList[i].IsInsideImage(pointIndex)==true)
        {
            IsForwardLabeled=true;
            return i;
        }
        if(m_ContourLabelList[i].labeled(pointIndex)==true)
        {
            IsForwardLabeled=true;
            return i;
        }
    }
    return i;
}

// function to find the last labeled frame
int LayerObject::findLastLocalLabeledFrame(int frameIndex, int pointIndex,bool& IsBackwardLabeled)
{
    if(frameIndex==m_EndFrame)
    {
        IsBackwardLabeled=m_ContourLabelList[m_EndFrame].labeled(pointIndex);
        return m_EndFrame;
    }
    int i;
    IsBackwardLabeled=false;
    bool IsInside=m_ContourLabelList[frameIndex].IsInsideImage(pointIndex);
    for(i=frameIndex+1;i<=m_EndFrame;i++)
    {
        // if the intial point is outside image boundary and the i-th one is inside, then don't change the inside one
        if(IsInside==false && m_ContourLabelList[i].IsInsideImage(pointIndex)==true)
        {
            IsBackwardLabeled=true;
            return i;
        }
        if(m_ContourLabelList[i].labeled(pointIndex)==true)
        {
            IsBackwardLabeled=true;
            return i;
        }
    }
    return i;
}

int LayerObject::findFirstGlobalLabeledFrame(int frameIndex,bool& IsForwardLabeled)
{
    if(frameIndex==m_StartFrame)
    {
        IsForwardLabeled=m_IsGlobalLabelList[m_StartFrame];
        return m_StartFrame;
    }
    int i;
    IsForwardLabeled=false;
    for(i=frameIndex-1;i>=m_StartFrame;i--)
        if(m_IsGlobalLabelList[i]==true || m_IsLocalLabelList[i]==true)
        {
            IsForwardLabeled=true;
            return i;
        }
    return i;
}

int LayerObject::findLastGlobalLabeledFrame(int frameIndex,bool& IsBackwardLabeled)
{
    if(frameIndex==m_EndFrame)
    {
        IsBackwardLabeled=m_IsGlobalLabelList[m_EndFrame];
        return m_EndFrame;
    }
    int i;
    IsBackwardLabeled=false;
    for(i=frameIndex+1;i<=m_EndFrame;i++)
        if(m_IsGlobalLabelList[i]==true || m_IsLocalLabelList[i]==true)
        {
            IsBackwardLabeled=true;
            return i;
        }
    return i;
}

//-------------------------------------------------------------------------------------------------------
// Function to insert a point. Note that the contour has already been selected before this
// function is called. So there is only the need to compute which lin segment has been
// selected. Then the coordinate of the new point needs to be propagated to other frames
//-------------------------------------------------------------------------------------------------------
void LayerObject::insertPoint(int frameIndex, const QPointF &point)
{
    if(InRange(frameIndex)==false)
        return;
    m_ContourLabelList[frameIndex].minLineSegDistance(point,selectedPointIndex);
    m_ContourLabelList[frameIndex].insertPoint(point,++selectedPointIndex,true);
    m_IsLocalLabelList[frameIndex]=true;
    m_IsGlobalLabelList[frameIndex]=true;
    
    // propagate to the rest of the frames
    double projection,extension;
    m_ContourLabelList[frameIndex].computeTwoPointsCoeff(selectedPointIndex,projection,extension);
    for(int i=startFrame();i<=endFrame();i++)
    {
        if(i==frameIndex)
            continue;
        m_ContourLabelList[i].insertPointTwoPointsCoeff(selectedPointIndex,projection,extension);
    }
}

void LayerObject::updatePoint(int frameIndex, const QPointF &point)
{
    if(InRange(frameIndex)==false)
        return;
    m_ContourLabelList[frameIndex].updatePoint(point,selectedPointIndex,true);
    m_IsLocalLabelList[frameIndex]=true;
    m_IsGlobalLabelList[frameIndex]=true;
}

void LayerObject::propagateUpdatePoint(int frameIndex)
{
    if(InRange(frameIndex)==false)
        return;

    int firstFrame,lastFrame;
    bool IsForwardLabeled,IsBackwardLabeled;
    firstFrame=findFirstLocalLabeledFrame(frameIndex,selectedPointIndex,IsBackwardLabeled);
    lastFrame=findLastLocalLabeledFrame(frameIndex,selectedPointIndex,IsForwardLabeled);
    propagateUpdatePoint(frameIndex,lastFrame,IsForwardLabeled,firstFrame,IsBackwardLabeled);
}


//void LayerObject::propagateUpdatePoint(int frameIndex,int lastFrame,int firstFrame)
//{
//    if(InRange(frameIndex)==false)
//        return;
//    // propagate to the rest of the frames
//    double projection0,extension0;
//    m_ContourLabelList[frameIndex].computeTwoPointsCoeff(selectedPointIndex,projection0,extension0);
//
//    double projection1,extension1;
//    double ratio;
//    // backward
//    m_ContourLabelList[qMax(firstFrame,m_StartFrame)].computeTwoPointsCoeff(selectedPointIndex,projection1,extension1);
//    for(int i=firstFrame+1;i<frameIndex;i++)
//    {
//        ratio=(double)(i-firstFrame)/(frameIndex-firstFrame);
//        m_ContourLabelList[i].updatePointTwoPointsCoeff(selectedPointIndex,projection0,extension0,projection1,extension1,ratio);
//        m_ContourLabelList[i].computeCentroid();
//    }
//    // forward
//    m_ContourLabelList[qMin(lastFrame,m_EndFrame)].computeTwoPointsCoeff(selectedPointIndex,projection1,extension1);
//    for(int i=frameIndex+1;i<lastFrame;i++)
//    {
//        ratio=1-(double)(i-frameIndex)/(lastFrame-frameIndex);
//        m_ContourLabelList[i].updatePointTwoPointsCoeff(selectedPointIndex,projection0,extension0,projection1,extension1,ratio);
//        m_ContourLabelList[i].computeCentroid();
//    }
//}

void LayerObject::propagateUpdatePoint(int frameIndex,int lastFrame,bool IsForwardLabeled,int firstFrame,bool IsBackwardLabeled)
{
    if(InRange(frameIndex)==false)
        return;
    // propagate to the rest of the frames
    QPointF curPoint,endPoint;

    // backward
    for(int i=firstFrame+1;i<frameIndex;i++)
    {
        m_ContourLabelList[frameIndex].AffineFit(selectedPointIndex,m_ContourLabelList[i],curPoint);
        if(IsBackwardLabeled)
        {
            double ratio=(double)(i-firstFrame)/(frameIndex-firstFrame);
            m_ContourLabelList[firstFrame].AffineFit(selectedPointIndex,m_ContourLabelList[i],endPoint);
            curPoint=curPoint*ratio+endPoint*(1-ratio);
        }
        m_ContourLabelList[i].updatePoint(curPoint,selectedPointIndex,false);
        m_ContourLabelList[i].computeCentroid();
    }
    // forward
    for(int i=frameIndex+1;i<lastFrame;i++)
    {
        m_ContourLabelList[frameIndex].AffineFit(selectedPointIndex,m_ContourLabelList[i],curPoint);
        if(IsForwardLabeled)
        {
            double ratio=1-(double)(i-frameIndex)/(lastFrame-frameIndex);
            m_ContourLabelList[lastFrame].AffineFit(selectedPointIndex,m_ContourLabelList[i],endPoint);
            curPoint=curPoint*ratio+endPoint*(1-ratio);
        }
        m_ContourLabelList[i].updatePoint(curPoint,selectedPointIndex,false);
        m_ContourLabelList[i].computeCentroid();
    }
}



void LayerObject::propagateUpdatePointForward(int frameIndex, int lastFrame)
{
    if(InRange(frameIndex)==false)
        return;
    double projection0,extension0;
    m_ContourLabelList[frameIndex].computeTwoPointsCoeff(selectedPointIndex,projection0,extension0);

    double projection1,extension1;
    double ratio;
    
    m_ContourLabelList[qMin(lastFrame,m_EndFrame)].computeTwoPointsCoeff(selectedPointIndex,projection1,extension1);
    for(int i=frameIndex+1;i<lastFrame;i++)
    {
        ratio=1-(double)(i-frameIndex)/(lastFrame-frameIndex);
        m_ContourLabelList[i].updatePointTwoPointsCoeff(selectedPointIndex,projection0,extension0,projection1,extension1,ratio);
        m_ContourLabelList[i].computeCentroid();
    }
}

void LayerObject::propagateUpdatePointBackward(int frameIndex, int firstFrame)
{
    if(InRange(frameIndex)==false)
        return;
    double projection0,extension0;
    m_ContourLabelList[frameIndex].computeTwoPointsCoeff(selectedPointIndex,projection0,extension0);

    double projection1,extension1;
    double ratio;
    
    m_ContourLabelList[qMax(firstFrame,m_StartFrame)].computeTwoPointsCoeff(selectedPointIndex,projection1,extension1);
    for(int i=firstFrame+1;i<frameIndex;i++)
    {
        ratio=(double)(i-firstFrame)/(frameIndex-firstFrame);
        m_ContourLabelList[i].updatePointTwoPointsCoeff(selectedPointIndex,projection0,extension0,projection1,extension1,ratio);
        m_ContourLabelList[i].computeCentroid();
    }
}


//-------------------------------------------------------------------------------------------------------
// function to move the contour globally
//-------------------------------------------------------------------------------------------------------
void LayerObject::moveContour(int frameIndex, const QPointF &point)
{
    if(InRange(frameIndex)==false)
        return;
    m_ContourLabelList[frameIndex].shiftContour(m_OrgContourLabel, point-m_OrgCursorPoint);
    m_IsGlobalLabelList[frameIndex]=true;
}

//-------------------------------------------------------------------------------------------------------
// function to propagate the contour moving to the rest of the frames
//-------------------------------------------------------------------------------------------------------
void LayerObject::propagateMoveContour(int frameIndex)
{
    if(InRange(frameIndex)==false)
        return;
    int firstFrame,lastFrame;
    bool IsForwardLabel, IsBackwardLabel;
    firstFrame=findFirstGlobalLabeledFrame(frameIndex,IsForwardLabel);
    lastFrame=findLastGlobalLabeledFrame(frameIndex,IsBackwardLabel);

    QPointF shift=m_ContourLabelList[frameIndex].centroid()-m_OrgContourLabel.centroid();
    double ratio;
    for(int i=firstFrame+1;i<frameIndex;i++)
    {
        if(IsForwardLabel)
        {
            ratio=(double)(i-firstFrame)/(frameIndex-firstFrame);
            m_ContourLabelList[i].shiftContour(shift*ratio);
            if(m_ContourLabelList[i].isPolygon())
                m_ContourLabelList[i].computeCentroid();
        }
        else
            m_ContourLabelList[i]=m_ContourLabelList[frameIndex];
    }
    for(int i=frameIndex+1;i<lastFrame;i++)
    {
        if(IsBackwardLabel)
        {
            ratio=1-(double)(i-frameIndex)/(lastFrame-frameIndex);
            m_ContourLabelList[i].shiftContour(shift*ratio);
            if(m_ContourLabelList[i].isPolygon())
                m_ContourLabelList[i].computeCentroid();
        }
        else
            m_ContourLabelList[i]=m_ContourLabelList[frameIndex];
    }
}

//-------------------------------------------------------------------------------------------------------
// function to scale the contour globally
//-------------------------------------------------------------------------------------------------------
void LayerObject::scaleContour(int frameIndex,const QPointF& point,int selectedBoundingBoxCorner)
{
    if(InRange(frameIndex)==false)
        return;
    m_ContourLabelList[frameIndex].scaleContour(m_OrgContourLabel,point-m_OrgCursorPoint,selectedBoundingBoxCorner);
    m_IsGlobalLabelList[frameIndex]=true;
}

void LayerObject::propagateScaleContour(int frameIndex)
{
    if(InRange(frameIndex)==false)
        return;
    int firstFrame,lastFrame;
    bool IsForwardLabel, IsBackwardLabel;
    firstFrame=findFirstGlobalLabeledFrame(frameIndex,IsForwardLabel);
    lastFrame=findLastGlobalLabeledFrame(frameIndex,IsBackwardLabel);

    double rate,ratio;
    rate=m_ContourLabelList[frameIndex].scale()/m_OrgContourLabel.scale();

    for(int i=firstFrame+1;i<frameIndex;i++)
    {
        if(IsForwardLabel)
        {
            ratio=(double)(i-firstFrame)/(frameIndex-firstFrame);
            if(m_ContourLabelList[i].isPolygon())
            {
                m_ContourLabelList[i].scaleContour((rate-1)*ratio+1);
                m_ContourLabelList[i].computeCentroid();
            }
            else
                m_ContourLabelList[i].scaleBoundingBox(ratio,m_ContourLabelList[frameIndex],m_ContourLabelList[firstFrame]);
        }
        else
            m_ContourLabelList[i]=m_ContourLabelList[frameIndex];
    }
    for(int i=frameIndex+1;i<lastFrame;i++)
    {
        if(IsBackwardLabel)
        {
            ratio=1-(double)(i-frameIndex)/(lastFrame-frameIndex);
            if(m_ContourLabelList[i].isPolygon())
            {
                m_ContourLabelList[i].scaleContour((rate-1)*ratio+1);
                m_ContourLabelList[i].computeCentroid();
            }
            else
                m_ContourLabelList[i].scaleBoundingBox(ratio,m_ContourLabelList[frameIndex],m_ContourLabelList[lastFrame]);
        }
        else
            m_ContourLabelList[i]=m_ContourLabelList[frameIndex];
    }
}

void LayerObject::removePoint()
{
    for(int i=m_StartFrame;i<=m_EndFrame;i++)
        m_ContourLabelList[i].removePoint(selectedPointIndex);
}

void LayerObject::removeLabel(int frameIndex)
{
    m_ContourLabelList[frameIndex].removeLabel(selectedPointIndex);
}

void LayerObject::FormLayerDepth(int frameIndex,LayerDepth &layerDepth)
{
    QList<int> indexList;
    for(int i=m_StartFrame;i<=m_EndFrame;i++)
        if(m_IsDepthLabeledList[i]==true || i==frameIndex)
            indexList.append(i);
    m_IsDepthLabeledList[frameIndex]=true;
    layerDepth.Initialize(m_DepthList.size(),m_StartFrame,m_EndFrame,m_DepthList,indexList);
}

void LayerObject::SetLayerDepth(const LayerDepth& layerDepth)
{
    //layerDepth.printDepth();
    for(int i=m_StartFrame;i<=m_EndFrame;i++)
        m_DepthList[i]=layerDepth.depth(i);
    //printLayerDepth();
}

void LayerObject::printLayerDepth()
{
    std::cout<<std::endl;
    for(int i=m_StartFrame;i<=m_EndFrame;i++)
        std::cout<<m_DepthList[i]<<std::endl;
}

//-------------------------------------------------------------------------------------------------------------------
// file IO, to write xml file of the object
//-------------------------------------------------------------------------------------------------------------------
void LayerObject::writeXML(XmlWriter &xw,int id)
{
    xw.writeOpenTag("object");
    xw.writeTaggedString("name",m_LayerName);
    xw.writeTaggedString("deleted","0");
    xw.writeTaggedString("verified","0");
    xw.writeDate();
    xw.writeTaggedString("id",QString::number(id));
    xw.writeTaggedString("username","anonymous");
    xw.writeTaggedString("numFrames",QString::number(m_NumFrames));
    xw.writeTaggedString("startFrame",QString::number(m_StartFrame));
    xw.writeTaggedString("endFrame",QString::number(m_EndFrame));
    xw.writeTaggedString("createdFrame",QString::number(m_CreatedFrame));
    xw.writeOpenTag("labels");
    for(int i=m_StartFrame;i<=m_EndFrame;i++) {
        xw.writeOpenTag("frame");
        xw.writeTaggedString("index",QString::number(i));
        xw.writeTaggedString("depth",QString::number(m_DepthList[i]));
        xw.writeTaggedString("globalLabel",QString::number(m_IsGlobalLabelList[i]));
        xw.writeTaggedString("localLabel",QString::number(m_IsLocalLabelList[i]));
        xw.writeTaggedString("tracked", QString::number(m_IsTrackedList[i]));
        xw.writeTaggedString("depthLabel",QString::number(m_IsDepthLabeledList[i]));
        xw.writeTaggedString("isPolygon",QString::number(m_ContourLabelList[i].isPolygon()));

                
        if(m_ContourLabelList[i].isPolygon()) // output contour
        {
            xw.writeOpenTag("polygon");
            for(int j=0;j<m_ContourLabelList[i].size();j++) {
                xw.writeOpenTag("pt");
                xw.writeTaggedString("x",QString::number(m_ContourLabelList[i].x(j)));
                xw.writeTaggedString("y",QString::number(m_ContourLabelList[i].y(j)));
                xw.writeTaggedString("labeled",QString::number(m_ContourLabelList[i].labeled(j)));
                xw.writeCloseTag("pt");
            }
            xw.writeCloseTag("polygon");
        }
        else // output bounding box
        {
            xw.writeOpenTag("boundingBox");
            QRectF boundingBox=m_ContourLabelList[i].boundingBox();
            xw.writeOpenTag("topLeft");
            xw.writeTaggedString("x",QString::number(boundingBox.topLeft().x()));
            xw.writeTaggedString("y",QString::number(boundingBox.topLeft().y()));
            xw.writeCloseTag("topLeft");
            xw.writeOpenTag("bottomRight");
            xw.writeTaggedString("x",QString::number(boundingBox.bottomRight().x()));
            xw.writeTaggedString("y",QString::number(boundingBox.bottomRight().y()));
            xw.writeCloseTag("bottomRight");
            xw.writeCloseTag("boundingBox");
        }
                    
        xw.writeCloseTag("frame");
    }
    xw.writeCloseTag("labels");

    xw.writeCloseTag("object");
}

//-------------------------------------------------------------------------------------------------------------------
// function to cut the labeling
//-------------------------------------------------------------------------------------------------------------------
void LayerObject::cutLabeling(int frameIndex)
{
    if(frameIndex>m_CreatedFrame)
    {
        if(frameIndex>m_EndFrame)
            return;
        m_EndFrame=frameIndex;
        for(int i=m_EndFrame+1;i<m_NumFrames;i++)
        {
            m_IsGlobalLabelList[i]=false;
            m_IsLocalLabelList[i]=false;
            m_IsTrackedList[i]=false;
            m_IsDepthLabeledList[i]=false;
        }
        return;
    }
    if(frameIndex<m_CreatedFrame)
    {
        if(frameIndex<m_StartFrame)
            return;
        m_StartFrame=frameIndex;
        for(int i=0;i<m_StartFrame;i++)
        {
            m_IsGlobalLabelList[i]=false;
            m_IsLocalLabelList[i]=false;
            m_IsTrackedList[i]=false;
            m_IsDepthLabeledList[i]=false;
        }
    }
}

void LayerObject::cutForward(int frameIndex)
{
    if(frameIndex<m_StartFrame)
        return;
    m_StartFrame=frameIndex;
    m_IsGlobalLabelList[frameIndex]=true;
    for(int i=0;i<m_StartFrame;i++)
    {
        m_IsGlobalLabelList[i]=false;
        m_IsLocalLabelList[i]=false;
        m_IsTrackedList[i]=false;
        m_IsDepthLabeledList[i]=false;
    }
}

void LayerObject::cutBackward(int frameIndex)
{
    if(frameIndex>m_EndFrame)
        return;
    m_EndFrame=frameIndex;
    m_IsGlobalLabelList[frameIndex]=true;
    for(int i=m_EndFrame+1;i<m_NumFrames;i++)
    {
        m_IsGlobalLabelList[i]=false;
        m_IsLocalLabelList[i]=false;
        m_IsTrackedList[i]=false;
        m_IsDepthLabeledList[i]=false;
    }
}

//-------------------------------------------------------------------------------------------------------------------
// function to remove all the tracking
//-------------------------------------------------------------------------------------------------------------------
void LayerObject::removeAllTracking()
{
    for(int i=m_StartFrame;i<=m_EndFrame;i++)
    {
        if(i==m_CreatedFrame)
            continue;
        m_IsGlobalLabelList[i]=false;
        m_IsLocalLabelList[i]=false;
        m_IsTrackedList[i]=false;
        m_IsDepthLabeledList[i]=false;
    }
    m_StartFrame=m_CreatedFrame;
    m_EndFrame=m_CreatedFrame;
}

//-------------------------------------------------------------------------------------------------------------------
// function to enforce the current frame to be a key frame
//-------------------------------------------------------------------------------------------------------------------
void LayerObject::enforceKeyFrame(int frameIndex)
{
    if(frameIndex>=m_StartFrame && frameIndex<=m_EndFrame)
        m_ContourLabelList[frameIndex].setUniformLabels(true);
}

void LayerObject::removeUserLabels(int frameIndex)
{
    if(frameIndex>=m_StartFrame && frameIndex<=m_EndFrame)
        m_ContourLabelList[frameIndex].setUniformLabels(false);
}

void LayerObject::extendDepth()
{
    for(int i=0;i<m_StartFrame;i++)
        m_DepthList[i]=m_DepthList[m_StartFrame];
    for(int i=m_EndFrame+1;i<m_NumFrames;i++)
        m_DepthList[i]=m_DepthList[m_EndFrame];
}


//-------------------------------------------------------------------------------------------------------------------
// functions to handle tracking
//-------------------------------------------------------------------------------------------------------------------
bool LayerObject::IsForwardTrackable(int frameIndex)
{
    if(frameIndex==m_EndFrame && m_EndFrame<m_NumFrames-1)
        return true;
    else
        return false;

    //if(frameIndex==m_NumFrames-1)
    //    return false;
    //if(m_IsTrackedList[frameIndex]==true && m_IsTrackedList[frameIndex+1]==false)
    //    return true;
    //else
    //    return false;
}

bool LayerObject::IsBackwardTrackable(int frameIndex)
{
    if(frameIndex==m_StartFrame && m_StartFrame>0)
        return true;
    else
        return false;
    //if(frameIndex==0)
    //    return false;
    //if(m_IsTrackedList[frameIndex]==true && m_IsTrackedList[frameIndex-1]==false)
    //    return true;
    //else
    //    return false;
}

void LayerObject::TrackToFrame(int frameIndex, const QList<QPointF> &contour)
{
    if(m_StartFrame>frameIndex)
        m_StartFrame=frameIndex;
    if(m_EndFrame<frameIndex)
        m_EndFrame=frameIndex;
    m_ContourLabelList[frameIndex].loadPoints(contour);
    m_IsTrackedList[frameIndex]=true;
}
