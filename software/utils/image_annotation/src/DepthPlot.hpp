#ifndef _DepthPlot_h
#define _DepthPlot_h

#include <QWidget>
#include <QSize>
#include <QList>
#include <QPainter>
#include "LayerDepth.hpp"

class LayerDepth;

class DepthPlot: public QWidget
{
    Q_OBJECT
public:
    DepthPlot(QWidget* parent,const QSize& widgetSize);
    ~DepthPlot(void);
    void loadDepthList(const QList<LayerDepth>& depthList,int active_depthindex,int frame_index);
    void computeStepSize();
protected:
    virtual void paintEvent(QPaintEvent *event);
    void plotYLabel(QPainter& painter,int y);
    void plotXLabel(QPainter& painter,int x);
signals:
    void changeLabeledFrames(const QVector<int>& frameIndexList,int frame_index);
    void changeDepth(int depth);
    void LayerDepthInterpolated(const LayerDepth& layerDepth);
public slots:
    void interpolateDepth(int depth);
    void setFrameIndex(int findex);
private:
    QSize WidgetSize;
    QSize PlotSize;
    QPoint Offset;
    double x_StepSize;
    double y_StepSize;
    double MaxDepth,MinDepth;

    QList<LayerDepth>  m_DepthList;
    int activeDepthIndex;
    int frameIndex;
    int orgFrameIndex;
};

#endif
