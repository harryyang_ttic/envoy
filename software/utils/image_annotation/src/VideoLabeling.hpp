#ifndef _VideoLabeling_h
#define _VideoLabeling_h

#include <QObject>
#include <QString>
#include <QList>
#include <QPainter>
#include "VideoSequence.hpp"
#include "LayerObject.hpp"
#include "ContourLabel.hpp"
#include "ImageViewer.hpp"
#include "MainWindow.hpp"
#include "LayerDepth.hpp"
#include "XmlWriter.hpp"

class VideoSequence;
class LayerObject;
class ContourLabel;
class MainWindow;
class LayerDepth;
class XmlWriter;

class VideoLabeling: public VideoSequence
{
    Q_OBJECT

    // the declaimation of state
public:
    enum LabelState{Idle,CursorOver,CreateContour,Selected,MovePoint,ScaleContour,MoveContour};

    // member variables
private:
    lcm_t *m_lcm;
    QList<LayerObject> m_LayerObjectList;
    int selectedLayerIndex;

    // variables for labeling
    LabelState labelState;  // variable of labeling state
    int selectedBoundingBoxCorner;

    MainWindow* pMainWindow;

    // variable to handle UI control
    bool IsMouseAllowed;
    // member functions
public:
    VideoLabeling(MainWindow* parent);
    VideoLabeling(const VideoLabeling& videoLabeling);
    void copyData(const VideoLabeling& videoLabeling);
    VideoLabeling& operator=(const VideoLabeling& videoLabeling);
    ~VideoLabeling(void);
    
    void Initialize(const QString& path,const QFileInfoList& filelist);
    void InitializeLCM(const QString& lcmlog, const QString& channel, const bool verboseOutput);
    void freeLCM();
    void OutputLabelFrame(int frameIndex,const QString& filename,int quality=95);
    void OutputAllLabeling(const QString& pathname);
    // function to help handle mouse events (operating the list of LayerObjects)
    bool selectLayerObject(const QPointF& point,double ScaleFactor=1);
    void createLayerObject(const QPointF& point);
    void selectPoint(const QPointF& point,double ScaleFactor=1);
    int selectPopupMenu(bool IsPointSelected);
    void ProcessPopupMenuSelection(int choice);
    
    bool writeVideoLabelMeXML(const QString& filename);
    bool readVideoLabelMeXML(const QString& filename, const bool verboseOutput);
    // function for replicating
    void ReplicateContour(bool direction,int destFrame);
    // function for tracking
    void TrackContour(bool direction,int nFrames);
    void OccludingReasoning(QList<bool>& IsVisibleList,const QList<QPointF>& contour);
    bool OccludingReasoning(const QPointF& point);
    void propagateUpdatePoint();
    void TrackPoint(int pointIndex,bool direction,int nFrames,int dstFrame);
    int findLastLocalLabeledFrame(int frameIndex,int pointIndex,bool &IsLastFrameLabeled);
    int findFirstLocalLabeledFrame(int frameIndex,int pointIndex,bool &IsFirstFrameLabeled);
signals:
    void changeLayerDepth(const QList<LayerDepth>& layerDepthList,int activeLayerIndex,int frame_index);
public slots:
    void ProcessMouseEvent(ImageViewer& imageViewer,ImageViewer::MouseAction& mouseAction,QPoint& Pos);
    void Paint(ImageViewer& imageViewer,QPainter& painter);
    void gotoFrame(int findex);
    void setLayerDepth(const LayerDepth& layerDepth);
    bool goForwardToFrameLCM (int frameIndex);
};

#endif
