#ifndef _LayerDepth_h
#define _LayerDepth_h

#include <QObject>
#include <QVector>
#include <QList>
#include <QPainter>

class LayerDepth: public QObject
{
    Q_OBJECT
public:
    LayerDepth(void);
    LayerDepth(const LayerDepth& m_LayerDepth);
    LayerDepth& operator = (const LayerDepth& m_LayerDepth);
    void copyData(const LayerDepth& m_LayerDepth);
    ~LayerDepth(void);
    void clear();

    void printDepth() const;

    void Initialize(int nFrames,int startFrame,int endFrame,const QList<int>& indexList,const QList<int>& indexDepthList);
    void Initialize(int nFrames,int startFrame,int endFrame,const QList<double>& depthList,const QList<int>& indexList);
    void insertIndex(int frameIndex);
    void setDepth(int frameIndex,int depth);

    void InterpolateLinear();
    int InterpolateLinear(int frameIndex);
    void InterpolateCG(int nIterations=20);
    void A(double*& input,double*& output);
    void AT(double*& input,double*& output);
    void B(double*& input, double*& output);
    void H1(double*& input,double*& output);
    void H1T(double*& input,double*& output);
    void H2(double*& input,double*& output);
    void H2T(double*& input,double*& output);
    void LinearSystem(double*& input,double*& output);
    void compute_b(double*& output);
    
    void DrawCurve(QPainter& painter,const QPoint& Offset,double plotHeight,double minDpeth,double x_stepsize,double y_stepsize,int frameIndex,bool IsActive=false);

    // function to access the members
    double max();
    double min();
    inline double depth(int frameIndex) const {return m_Depth[frameIndex];};
    inline int numFrames() const {return m_Depth.size();};
    inline int numControlPoints() const {return m_Index.size();};
    inline int numToInterpolate() const {return dim;};
    inline const QVector<int>& frameIndexList() const{return m_Index;};
    inline int getFrameIndex(int findex) const {return m_Index[findex];};
    static void printVector(double* vect,int d);
    
    void testH1();
    void testH1T();
    void testH2();
    void testH2T();
private:
    QVector<double> m_Depth;
    QVector<bool> m_IsLabeled;
    QVector<bool> m_SqIsLabeled;
    QVector<int> m_Index;
    int m_StartFrame,m_EndFrame; // the frame index
    int m_Dim;    // the number of the effect frames
    int dim;            // the number of the frames that need to be interpolated
    double weight;
};

#endif