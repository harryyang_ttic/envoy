#include <QtGui>
#include <QString>
#include "DepthControl.hpp"

DepthControl::DepthControl(QWidget* parent): QDialog(parent)
{
    setWindowTitle(tr("Change depth"));

    depthPlot=new DepthPlot(this,QSize(480,300));

    spinDepth=new QSpinBox;
    sliderDepth=new QSlider(Qt::Horizontal);
    spinDepth->setRange(0,255);
    sliderDepth->setRange(0,255);
    sliderDepth->setFixedWidth(200);
    connect(spinDepth,SIGNAL(valueChanged(int)),sliderDepth,SLOT(setValue(int)));
    connect(sliderDepth,SIGNAL(valueChanged(int)),spinDepth,SLOT(setValue(int)));
    connect(sliderDepth,SIGNAL(valueChanged(int)),depthPlot,SLOT(interpolateDepth(int)));
    connect(depthPlot,SIGNAL(changeDepth(int)),sliderDepth,SLOT(setValue(int)));
    sliderDepth->setValue(0);

     comboBox=new QComboBox;
     comboBox->setMinimumWidth(30);
     //comboBox->setFixedWidth(100);
     comboBox->setEditable(false);
     connect(comboBox,SIGNAL(activated(int)),depthPlot,SLOT(setFrameIndex(int)));
     connect(depthPlot,SIGNAL(changeLabeledFrames(const QVector<int>&,int)),this,SLOT(setLabeledFrame(const QVector<int>&,int)));
     connect(depthPlot,SIGNAL(LayerDepthInterpolated(const LayerDepth&)),this,SIGNAL(LayerDepthInterpolated(const LayerDepth&)));

     label1=new QLabel;
     label1->setText("Select frame:");
     label2=new QLabel;
     label2->setText(" Set depth:");
    // handle the layout
     hLayout = new QHBoxLayout;
     hLayout->setSpacing(0);
     hLayout->addWidget(label1);
     hLayout->addWidget(comboBox);
     hLayout->addWidget(label2);
     hLayout->addWidget(spinDepth);
     hLayout->addWidget(sliderDepth);

     vLayout=new QVBoxLayout;
     vLayout->addLayout(hLayout);
     vLayout->addWidget(depthPlot);

     setLayout(vLayout);
}

DepthControl::~DepthControl(void)
{
}

void DepthControl::setLabeledFrame(const QVector<int>& frameIndexList,int frameIndex)
{
    comboBox->clear();
    int cindex;
    for(int i=0;i<frameIndexList.size();i++)
    {
        comboBox->addItem(QString::number(frameIndexList[i]+1));
        if(frameIndexList[i]==frameIndex)
            cindex=i;
    }
    comboBox->setCurrentIndex(cindex);
}
