#ifndef _MainWindow_h
#define _MainWindow_h

#include <QMainWindow>
#include <QScrollArea>
#include <QDialog>
#include <QSlider>
#include <QSpinBox>
#include <QHBoxLayout>

#include "ImageViewer.hpp"
#include "VideoLabeling.hpp"
#include "DepthControl.hpp"
#include "LayerDepth.hpp"

class ImageViewer;
class VideoLabeling;
class DepthControl;
class LayerDepth;

class MainWindow: public QMainWindow
{
    Q_OBJECT
public:
    //MainWindow(void);
    MainWindow(const QString& lcmLog, const QString& channel, const bool beVerbose);
    ~MainWindow(void);
protected:
    void resizeEvent(QResizeEvent* event);

private slots:
    void openFolder();
    void openLCMLogFile(const QString& lcmLog);
    void openLCMLogFile();
    void load();
    void save();
    void saveAs();
    void exportLabeling();
    void cleanupClose();

    void adaptScrollBar(double factor);
    void setFrameIndexRange(int nFrames);
    void setFrameControl(bool firstFrame,bool preFrame,bool nextFrame,bool lastFrame);
    void setLayerDepth(const QList<LayerDepth>& layerDepthList,int activeLayerIndex,int frameIndex);
    
private:
    void createActions();
    void createMenus();
    void createToolBar();
    void binding();
    
    
    ImageViewer* imageViewer;
    QScrollArea* scrollArea;
    VideoLabeling* pVideoLabeling;

    // Menus
    QMenu* fileMenu;
    QMenu* viewMenu;

    // Toolbars
    QToolBar* fileToolBar;
    QToolBar* zoomToolBar;
    QToolBar* videoToolBar;

    // Actions for files
    QAction* openFolderAct;
    QAction* loadProjectAct;
    QAction* saveAct;
    QAction* saveAsAct;
    QAction* exportAct;
    QAction* exitAct;
    
    // Actions for zooming the images
    QAction* zoomInAct;
    QAction* zoomOutAct;
    QAction* normalSizeAct;

    // the actions for displaying the video
    QAction* nextFrameAct;
    QAction* preFrameAct;
    QAction* firstFrameAct;
    QAction* lastFrameAct;

    QSpinBox* spinBoxFrameIndex;
    QSlider* sliderFrameIndex;
    
    QString framePath;
    QString lcmLogFile;
    QString lcmChannel;
    QString projPath;
    QString projFileName;
    QString outputPathName;
    bool IsProjFileSpecified;
    bool verbose;

    DepthControl* depthControl;
};

#endif
