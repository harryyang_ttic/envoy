#ifndef _PyrdPatchSet_h
#define _PyrdPatchSet_h

#include <QList>
#include <QPointF>
#include "ImPyramid.hpp"
#include "Image.hpp"

//------------------------------------------------------------------
// class of PatchSet, the basic element for PyradPatchSet
//------------------------------------------------------------------
class PatchSet
{
private:
    int nPoints;
    ImageType* pPatch;
    DImage* pWeight;
public:
    PatchSet();
    ~PatchSet();
    void clear();
    void copyData(const PatchSet& other);
    void updateData(const PatchSet& other,double ratio);
    void SamplePatch(const ImageType& image,const QList<QPointF>& contour,int winsize);
    void ComputeWeight(const ImageType& image,const QList<QPointF>& contour,int winsize,double sigma);
    void ComputeWeight(const ImageType& image,int pointIndex,const QList<QPointF>& contour,int winsize,double sigma);
    void dispPatches(const QString& filename,int nCols=4);
    void dispFeatures(const QString& filename,int nCols=4);
    void dispWeights(const QString& filename,int nCols=4);
    inline const ImageType& Patch(int index) const {return (const ImageType&)pPatch[index];};
    inline const DImage& Weight(int index) const {return (const DImage&)pWeight[index];};
};

//------------------------------------------------------------------
// class of pyramid patch set
//------------------------------------------------------------------
class PyrdPatchSet
{
private:
    int nLevels,nPoints;
    PatchSet* pPatchSet;
public:
    PyrdPatchSet(void);
    ~PyrdPatchSet(void);
    void clear();
    void copyData(const PyrdPatchSet& other);
    void updateData(const PyrdPatchSet& other,double ratio=0.5);
    void CreatePyrdPatchSet(const ImPyramid& imPyramid,const QList<QPointF>& contour);
    void CreatePyrdPatchSet(const ImPyramid& imPyramid,const QPointF& point,int pointIndex,const QList<QPointF>& contour);
    void dispPyrdPatches(const QString& BaseName,int nCols=4);
    void dispPyrdFeatures(const QString& BaseName,int nCols=4);
    void dispPyrdWeights(const QString& BaseName,int nCols=4);

    // function to access the member functions
    inline int EnforceLevel(int level) const {return __max(__min(level,nLevels-1),0);};
    inline int nlevels() const {return nLevels;};
    inline const PatchSet& getPatchSet(int level) const {return (const PatchSet&) pPatchSet[level];};
    inline PatchSet& getPatchSet(int level) {return pPatchSet[level];};
    inline PatchSet& operator[](int level){return pPatchSet[level];};

};

#endif
