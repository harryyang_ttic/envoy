#ifndef _LinearSystem_h
#define _LinearSystem_h

class LinearSystem
{
public:
    LinearSystem(void);
    ~LinearSystem(void);
    static void ConjugateGradient(int dim,double* A,double* b,double* x);
    static void Ax(int dim,double* A,double* x,double* y);
    static double norm2(int dim,double* x);
    static double innerproduct(int dim,double* x,double* y);
    static void printMatrix(int dim,double* A);
    static void printVector(int dim,double* x);
};

#endif