#include <QtGui>
#include "ImageViewer.hpp"
#include "math.h"

ImageViewer::ImageViewer(QWidget* parent): QWidget(parent)
{
    // set the default values
    minScale=0.3;
    maxScale=6;
    ImHeight=240;
    ImWidth=320;
    HMargin=VMargin=40;
    
    QImage temp(ImWidth,ImHeight,QImage::Format_RGB32);

    QPainter painter(&temp);
    QBrush brush(QColor(200,200,200));
    //painter.setBrush(brush);
    painter.fillRect(0,0,ImWidth,ImHeight,brush);

    m_Image=temp;
    dispImage=m_Image;
    scaleFactor=1;
    setBackgroundRole(QPalette::Dark);

    if (parent!=0)
        parentViewerSize=parent->frameSize()-QSize(10,10);
    else
        parentViewerSize=QSize(ViewerWidth,ViewerHeight);

    computeViewerSize();    
    setMouseTracking(true);
}

ImageViewer::~ImageViewer(void)
{
}

void ImageViewer::setParentSize(QSize size)
{
    parentViewerSize=size-QSize(10,10);
    computeViewerSize();
}

void ImageViewer::computeViewerSize()
{
    ViewerHeight=ImHeight+2+VMargin*scaleFactor*2;
    ViewerWidth=ImWidth+2+HMargin*scaleFactor*2;

    xoffset=yoffset=0;
    if (parentViewerSize.height()>ViewerHeight)
        yoffset=(parentViewerSize.height()-ViewerHeight)/2;
    if (parentViewerSize.width()>ViewerWidth)
        xoffset=(parentViewerSize.width()-ViewerWidth)/2;
    HScreenOffset=HMargin*scaleFactor+xoffset+1;
    VScreenOffset=VMargin*scaleFactor+yoffset+1;
    setFixedSize(qMax(parentViewerSize.width(),ViewerWidth),qMax(parentViewerSize.height(),ViewerHeight));
    //if(pVideoRotoscoping!=NULL)
    //    pVideoRotoscoping->setDispInfo(QPointF(HScreenOffset,VScreenOffset),scaleFactor);
}

void ImageViewer::paintEvent(QPaintEvent *event)
{
    QPainter mPainter(this);
    mPainter.setPen(QColor(0,0,0));

    mPainter.drawRect(HScreenOffset-1,VScreenOffset-1,ImWidth+1,ImHeight+1);
    mPainter.drawImage(QPoint(HScreenOffset,VScreenOffset),dispImage);

    mPainter.setRenderHint(QPainter::Antialiasing);
    emit paintingImageViewer(*this,mPainter);
    //mPainter.end();
}

void ImageViewer::loadImage(QImage &inputImage)
{
    m_Image=inputImage;
    resizeImage(-1);
}

void ImageViewer::resizeImage(double factor)
{
    if (factor==0)
        scaleFactor=1;
    else if (factor!=-1)
        scaleFactor*=factor;

    ImWidth=m_Image.width()*scaleFactor;
    ImHeight=m_Image.height()*scaleFactor;
    if (fabs(scaleFactor-1)<0.001)
    {
        scaleFactor=1;
        dispImage=m_Image;
    }
    else
    {
        if (scaleFactor<1)
            dispImage=m_Image.scaled(ImWidth,ImHeight, Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
        else
            dispImage=m_Image.scaled(ImWidth,ImHeight, Qt::IgnoreAspectRatio);
    }
    computeViewerSize();
    update();

    if (scaleFactor<=minScale)
        emit setZoomOut(false);
    else
        emit setZoomOut(true);
    if (scaleFactor>=maxScale)
        emit setZoomIn(false);
    else
        emit setZoomIn(true);
}

void ImageViewer::zoomOut()
{
    resizeImage(0.9);
    emit adjustScrollBar(0.9);
}

void ImageViewer::zoomIn()
{
    resizeImage(1.2);
    emit adjustScrollBar(1.2);
}

void ImageViewer::normalSize()
{
    double temp=scaleFactor;
    scaleFactor=1;
    resizeImage(0);
    emit adjustScrollBar((double)1/temp);
}

void ImageViewer::mousePressEvent ( QMouseEvent * event )
{
    QPoint pos=event->pos();
    MouseAction mouseAction;
    if(event->button()==Qt::LeftButton)
    {
        mouseAction=LeftClick;
        emit viewerMouseEvent(*this,mouseAction,pos);
    }
    else if(event->button()==Qt::RightButton)
    {
        mouseAction=RightClick;
        emit viewerMouseEvent(*this,mouseAction,pos);
    }
}

void ImageViewer::mouseMoveEvent(QMouseEvent *event)
{
    QPoint pos=event->pos();
    MouseAction mouseAction=Move;
    emit viewerMouseEvent(*this,mouseAction,pos);
}

void ImageViewer::mouseReleaseEvent(QMouseEvent *event)
{
    QPoint pos=event->pos();
    MouseAction mouseAction=Release;
    emit viewerMouseEvent(*this,mouseAction,pos);
}

void ImageViewer::mouseDoubleClickEvent(QMouseEvent *event)
{
    QPoint pos=event->pos();
    MouseAction mouseAction=DoubleClick;
    emit viewerMouseEvent(*this,mouseAction,pos);
}
