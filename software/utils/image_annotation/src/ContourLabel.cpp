#include <QtGui>
#include <iostream>
#include <QVector>
#include "ContourLabel.hpp"
#include "MyPreference.hpp"
#include "LinearSystem.hpp"

int ContourLabel::imWidth=0;
int ContourLabel::imHeight=0;

ContourLabel::ContourLabel(void): QObject()
{
    IsPolygon=true;
}

ContourLabel::ContourLabel(const ContourLabel& contourLabel): QObject()
{
    m_PointList=contourLabel.m_PointList;
    m_LabelList=contourLabel.m_LabelList;
    m_BoundingRect=contourLabel.m_BoundingRect;
    m_Centroid=contourLabel.m_Centroid;

    IsPolygon=contourLabel.IsPolygon;
    m_InnerRect=contourLabel.m_InnerRect;
}

ContourLabel& ContourLabel::operator = (const ContourLabel& contourLabel)
{
    m_PointList=contourLabel.m_PointList;
    m_LabelList=contourLabel.m_LabelList;
    m_BoundingRect=contourLabel.m_BoundingRect;
    m_Centroid=contourLabel.m_Centroid;

    IsPolygon=contourLabel.IsPolygon;
    m_InnerRect=contourLabel.m_InnerRect;
    return *this;
}

ContourLabel::~ContourLabel(void)
{
}
//------------------------------------------------------------------------------------------------------------------------------
// function to plot the contour
// Input variables:
//   ScreenOffset and ScaleFactor are used to transfer the contour to the coordinates of ImageViewer
//   IsActive: indicate whether the contour is active. If a contour is active, then
//                    (1) The color of the contour is fully saturated and the width is bigger
//                    (2) The contour is close
//                    (3) Bounding box and the centroid is plotted
//   IsCreating:: indicate whether the contour is in the process of creating. If so, then
//                    (1) The color is saturated and the width is wider
//                    (2) The contour is not close
//                    (3) Bounding box and the centroid is not plotted
//   IsCursorOver: indicate whether the cursor is over the boundary of the contour. If so, then
//                    (1) the color of the contour is saturated, and the width of the contour is bigger
//                    (2) the contour is close
//                    (3) Instead of plotting bounding box and centroid, fill the polygon with a transparent map
//   Otherwise (not the above cases)
//                    (1) The color is less saturated, and the width is thin
//                    (2) the contour is close
//                    (3) no other displays
//   Only one of IsActive, IsCreating and IsCursorOver can be true
//------------------------------------------------------------------------------------------------------------------------------
void ContourLabel::DrawContour(QPainter& painter,QPointF ScreenOffset,double ScaleFactor,int depth,bool IsActive,bool IsCreating, bool IsCursorOver,bool IsGlobal)
{
    // if no points in the contour label, the return
    if(m_PointList.size()==0 && IsPolygon)
        return;
    
    // for sanity check
    if((int)IsActive+IsCreating+IsCursorOver>1)
        std::cout<<"Error: IsActive="<<IsActive<<" IsCreating="<<IsCreating<<" IsCursorOver="<<IsCursorOver<<" only one of them can be true"<<std::endl;

    if( (IsActive || IsCreating || IsCursorOver) && IsPolygon)
        computeBoundingBox(ScaleFactor);

    //--------------------------------------------------------------------------------------
    // set the color and width of the contour
    // the pen for plotting the contour
    //--------------------------------------------------------------------------------------
    QPen pen_fg,pen_bg;
    QColor color_pts,color_line,color_bg,color_region;

    if(IsActive||IsCreating ||IsCursorOver)
    {
        color_pts.setHsv(qMin(qMax(depth,0),255),255,255);
        color_line.setHsv(qMin(qMax(depth,0),255),250,230);
        color_bg.setHsv(qMin(qMax(depth,0),255),40,60);
        pen_bg.setWidthF(4);
        pen_fg.setWidthF(2.2);
    }
    else
    {
        color_pts.setHsv(qMin(qMax(depth,0),255),100,255);
        color_line.setHsv(qMin(qMax(depth,0),255),180,230);
        color_bg.setHsv(qMin(qMax(depth,0),255),40,40);
        pen_bg.setWidthF(3);
        pen_fg.setWidthF(1.2);
    }
    color_region.setHsv(qMin(qMax(depth,0),255),255,255,128);
    pen_bg.setColor(color_bg);
    pen_fg.setColor(color_line);

    //--------------------------------------------------------------------------------------
    // creating the data points in the coordinate of ImageViewer
    //--------------------------------------------------------------------------------------
    QPolygonF ptsVect(m_PointList.size());
    for(int i=0;i<m_PointList.size();i++)
        ptsVect[i]=m_PointList[i]*ScaleFactor+ScreenOffset;

    //--------------------------------------------------------------------------------------
    // plot the contour region is IsCursorOver==true
    //--------------------------------------------------------------------------------------
    if(IsCursorOver)
    {
        QBrush brush(Qt::SolidPattern);
        brush.setColor(color_region);
        painter.setBrush(brush);
        if(IsPolygon)
            painter.drawPolygon(ptsVect);
    }

    //--------------------------------------------------------------------------------------
    // plot the global control
    //--------------------------------------------------------------------------------------
    if((IsActive || IsCreating && IsPolygon) || !IsPolygon)
    {
        QPen pen;
        // draw the bounding box
        QRectF dispRect(m_BoundingRect.topLeft()*ScaleFactor+ScreenOffset,m_BoundingRect.size()*ScaleFactor);

        pen.setStyle(Qt::SolidLine);
        pen.setWidthF(3);
        if(IsPolygon)
            pen.setColor(QColor(60,60,60));
        else
        {
            if(IsActive||IsCreating ||IsCursorOver)
            {
                pen.setColor(Qt::black);
                if(IsGlobal)
                    pen.setColor(QColor(160,160,0));
                pen.setWidthF(4);
            }
            else
            {
                pen.setColor(color_bg);
                if(IsGlobal)
                    pen.setColor(QColor(100,100,0));
            }
        }
        painter.setPen(pen);
        painter.drawRect(dispRect);

        pen.setStyle(Qt::SolidLine);
        pen.setWidthF(1);
        if(IsPolygon)
        {
            if(IsGlobal)
                pen.setColor(QColor(255,255,50));
            else
                pen.setColor(QColor(200,200,200));
        }
        else
        {
            pen.setColor(color_line);
            if(IsActive||IsCreating ||IsCursorOver)
            {
                pen.setWidthF(2);
                //if(IsGlobal)
                //    pen.setColor(QColor(255,255,50));
            }
        }
        painter.setPen(pen);
        painter.drawRect(dispRect);

        // draw the corners of the bounding box
        QPointF point;
        double bar_size=4*ScaleFactor;
        pen.setWidthF(1);
        if(IsPolygon)
            pen.setColor(Qt::green);
        else
        {
            if(IsActive||IsCreating ||IsCursorOver)
                pen.setColor(Qt::white);
            else
                pen.setColor(QColor(200,200,200));
        }
        painter.setPen(pen);

        point=dispRect.topLeft();
        painter.drawLine(point,QPointF(point.x(),point.y()+bar_size));
        painter.drawLine(point,QPointF(point.x()+bar_size,point.y()));

        point=dispRect.topRight();
        painter.drawLine(point,QPointF(point.x(),point.y()+bar_size));
        painter.drawLine(point,QPointF(point.x()-bar_size,point.y()));

        point=dispRect.bottomRight();
        painter.drawLine(point,QPointF(point.x(),point.y()-bar_size));
        painter.drawLine(point,QPointF(point.x()-bar_size,point.y()));

        point=dispRect.bottomLeft();
        painter.drawLine(point,QPointF(point.x(),point.y()-bar_size));
        painter.drawLine(point,QPointF(point.x()+bar_size,point.y()));

        // draw the background of the points
        if(!IsPolygon)
        {
            QPen tempPen;
            if(IsActive||IsCreating ||IsCursorOver)
            {
                tempPen.setColor(Qt::black);
                tempPen.setWidthF(6);
            }
            else
            {
                tempPen.setColor(color_bg);
                tempPen.setWidthF(5);
            }
            painter.setPen(tempPen);
            painter.drawPoint(dispRect.topLeft());
            painter.drawPoint(dispRect.topRight());
            painter.drawPoint(dispRect.bottomRight());
            painter.drawPoint(dispRect.bottomLeft());
        }
        pen.setWidthF(3);
        if(!IsPolygon & (IsActive||IsCreating ||IsCursorOver))
            pen.setWidthF(4);
        
        painter.setPen(pen);
        painter.drawPoint(dispRect.topLeft());
        painter.drawPoint(dispRect.topRight());
        painter.drawPoint(dispRect.bottomRight());
        painter.drawPoint(dispRect.bottomLeft());

        // draw the center cross
        pen.setStyle(Qt::SolidLine);
        double cross_size=5;//*ScaleFactor;
        QPointF centroid(m_Centroid*ScaleFactor+ScreenOffset);

        pen.setWidthF(3);
        pen.setColor(QColor(40,40,40));
        painter.setPen(pen);
        painter.drawLine(QPointF(centroid.x()-cross_size,centroid.y()),QPointF(centroid.x()+cross_size,centroid.y()));
        painter.drawLine(QPointF(centroid.x(),centroid.y()-cross_size),QPointF(centroid.x(),centroid.y()+cross_size));
        pen.setWidthF(1);
        pen.setColor(QColor(240,240,240));
        painter.setPen(pen);
        painter.drawLine(QPointF(centroid.x()-cross_size,centroid.y()),QPointF(centroid.x()+cross_size,centroid.y()));
        painter.drawLine(QPointF(centroid.x(),centroid.y()-cross_size),QPointF(centroid.x(),centroid.y()+cross_size));
    }

    if(IsCursorOver)
        painter.setBrush(Qt::NoBrush);

    if(IsPolygon==false)
        return;

    //--------------------------------------------------------------------------------------
    //  plot the contour
    //--------------------------------------------------------------------------------------
    for(int i=0;i<m_PointList.size()-1;i++)
    {
        painter.setPen(pen_bg);
        painter.drawLine(ptsVect[i],ptsVect[i+1]);  //draw the background of the line
        painter.setPen(pen_fg);
        painter.drawLine(ptsVect[i],ptsVect[i+1]); // draw the foreground of the line
    }
    if(IsCreating==false)// draw the close contour if the contour is not in the creation
    {
        painter.setPen(pen_bg);
        painter.drawLine(ptsVect[ptsVect.size()-1],ptsVect[0]);
        painter.setPen(pen_fg);
        painter.drawLine(ptsVect[ptsVect.size()-1],ptsVect[0]);
    }

    //--------------------------------------------------------------------------------------
    //    plot the points
    //--------------------------------------------------------------------------------------
    if(IsActive==true || IsCreating==true || IsCursorOver==true)
    {
        pen_fg.setWidthF(3.5);
        pen_bg.setWidthF(6);
    }
    else
    {
        pen_fg.setWidthF(2.5);
        pen_bg.setWidthF(4.5);
    }
    pen_fg.setColor(color_pts);
    pen_bg.setColor(color_bg);

    painter.setPen(pen_bg);
    for(int i=0;i<ptsVect.size();i++)
        painter.drawPoint(ptsVect[i]);
    painter.setPen(pen_fg);
    for(int i=0;i<ptsVect.size();i++)
        painter.drawPoint(ptsVect[i]);

    // plot the points that are human labelled
    if(IsActive==true || IsCreating==true || IsCursorOver==true)
        pen_fg.setColor(Qt::white);
    else
        pen_fg.setColor(Qt::lightGray);
    painter.setPen(pen_fg);
    for(int i=0;i<ptsVect.size();i++)
        if(m_LabelList[i]==true)
            painter.drawPoint(ptsVect[i]);

}

void ContourLabel::ShowText(QPainter& painter,QPointF ScreenOffset,double ScaleFactor,const QString& objname)
{
    QPen pen_fg,pen_bg;
    QRectF rect(m_BoundingRect.topLeft()*ScaleFactor+ScreenOffset,m_BoundingRect.size()*ScaleFactor);
    QFont font;
    font.setPixelSize(20*((ScaleFactor-1)*0.5+1));

    pen_fg.setColor(QColor(40,40,40));
    painter.setFont(font);
    painter.setPen(pen_fg);
    double foo=1*((ScaleFactor-1)*0.2+1);
    painter.drawText(rect.adjusted(foo-50,foo,foo+50,foo),Qt::AlignCenter,objname);

    pen_fg.setColor(Qt::white);
    painter.setFont(font);
    painter.setPen(pen_fg);
    painter.drawText(rect.adjusted(-foo-50,-foo,-foo+50,-foo),Qt::AlignCenter,objname);
}


//------------------------------------------------------------------------------------------------------------
// function to compute the distance to the contour (defined as the minimum distance to each line 
// segment and point)
//------------------------------------------------------------------------------------------------------------
double ContourLabel::distanceToContour(const QPointF &point)
{
    double minDistance=10000;
    int index=-1;
    QPointF P1,P2,P,h;
    double r,p,s;
    //----------------------------------------------------------
    // compute the distance to the line segments
    //----------------------------------------------------------
    for(int i=0;i<m_PointList.size();i++)
    {
        P1=m_PointList[i];
        if(i<m_PointList.size()-1)
            P2=m_PointList[i+1];
        else
            P2=m_PointList[0];
        h=P2-P1;
        P=point-P1;
        r=pabs(h);
        if(abs(r)<0.0001)
            continue;
        h/=r;
        p=h.x()*P.x()+h.y()*P.y(); // p is the projection on the line segment
        if(p<0 || p>r)                            // this is to judge that the point has to be inside the line segment after projection
            continue;
        s=pabs(P-h*p);                        // s is the orthogonal component
        minDistance=qMin(s,minDistance);
    }
    //----------------------------------------------------------
    // compute the distance to the points
    //----------------------------------------------------------
    for(int i=0;i<m_PointList.size();i++)
    {
        r=pabs(point-m_PointList[i]);
        minDistance=qMin(r,minDistance);
    }
    return minDistance;
}

//------------------------------------------------------------------------------------------------------------
// function to add a point to the contour
//------------------------------------------------------------------------------------------------------------
void ContourLabel::addPoint(const QPointF &point, const bool isLabeled)
{
    m_PointList.append(point);
    m_LabelList.append(isLabeled);
}

//------------------------------------------------------------------------------------------------------------
// function to compute the bounding box and mean of the contour
//------------------------------------------------------------------------------------------------------------
void ContourLabel::computeBoundingBox(double scaleFactor)
{
    QPolygonF ptsVect=QVector<QPointF>::fromList(m_PointList);
    m_BoundingRect=ptsVect.boundingRect();
    double offset=6/scaleFactor;
    m_BoundingRect.adjust(-offset,-offset,offset,offset);
    m_Centroid=m_BoundingRect.center();
    // compute the inner rect
    m_InnerRect.setTopLeft((m_BoundingRect.topLeft()-m_Centroid)*0.5+m_Centroid);
    m_InnerRect.setBottomRight((m_BoundingRect.bottomRight()-m_Centroid)*0.5+m_Centroid);
}

void ContourLabel::computeCentroid()
{
    QPolygonF ptsVect=QVector<QPointF>::fromList(m_PointList);
    m_BoundingRect=ptsVect.boundingRect();
    m_Centroid=m_BoundingRect.center();
}

//------------------------------------------------------------------------------------------------------------
// function to print out the points
//------------------------------------------------------------------------------------------------------------
void ContourLabel::printContour()
{
    for(int i=0;i<m_PointList.size();i++)
        std::cout<<"("<<m_PointList[i].x()<<","<<m_PointList[i].y()<<")"<<std::endl;
    std::cout<<std::endl;
}

//------------------------------------------------------------------------------------------------------------
// function to compute the minimum distance to the points
//------------------------------------------------------------------------------------------------------------
double ContourLabel::minPointDistance(const QPointF &point, int &pointIndex)
{
    double minDistance=10000,r;            
    for(int i=0;i<m_PointList.size();i++)
    {
        r=pabs(point-m_PointList[i]);
        if(r<minDistance)
        {
            pointIndex=i;
            minDistance=r;
        }
    }
    return minDistance;
}

//------------------------------------------------------------------------------------------------------------
// function to compute the minimum distance to the line segments
//------------------------------------------------------------------------------------------------------------
double ContourLabel::minLineSegDistance(const QPointF &point, int &pointIndex,double tolerance)
{
    double minDistance=10000;
    int index=-1;
    QPointF P1,P2,P,h;
    double r,p,s;
    //----------------------------------------------------------
    // compute the distance to the line segments
    //----------------------------------------------------------
    for(int i=0;i<m_PointList.size();i++)
    {
        P1=m_PointList[i];
        if(i<m_PointList.size()-1)
            P2=m_PointList[i+1];
        else
            P2=m_PointList[0];
        h=P2-P1;
        P=point-P1;
        r=pabs(h);
        if(abs(r)<0.0001)
            continue;
        h/=r;
        p=h.x()*P.x()+h.y()*P.y();                // p is the projection on the line segment
        if(p<tolerance || p>r-tolerance)            // tolerance is given to see whether the point lies inside the line segment
            continue;
        s=pabs(P-h*p);                                        // s is the orthogonal component
        if(s<minDistance)
        {
            minDistance=s;
            pointIndex=i;
        }
    }
    return minDistance;
}

//------------------------------------------------------------------------------------------------------------
// function to insert a point to a certain position
//------------------------------------------------------------------------------------------------------------
void ContourLabel::insertPoint(const QPointF& point,int index,bool IsLabeled)
{
    m_PointList.insert(index,point);
    m_LabelList.insert(index,IsLabeled);
}

//------------------------------------------------------------------------------------------------------------
// function to check whether a query point is the centroid
//------------------------------------------------------------------------------------------------------------
bool ContourLabel::IsCentroidSelected(const QPointF &point, double ScaleFactor)
{
    if(pabs(point-m_Centroid)<MyPreference::Threshold_CentroidDistance/ScaleFactor)
        return true;
    else
        return false;
}

//------------------------------------------------------------------------------------------------------------
// function to shift the contour
//------------------------------------------------------------------------------------------------------------
void ContourLabel::shiftContour(const QPointF& shift)
{
    if(IsPolygon)
        for(int i=0;i<m_PointList.size();i++)
            m_PointList[i]+=shift;
    else
    {
        m_Centroid+=shift;
        m_BoundingRect.moveCenter(m_Centroid);
        m_InnerRect.moveCenter(m_InnerRect.center()+shift);
    }
}

void ContourLabel::shiftContour(ContourLabel &org, const QPointF &shift)
{
    if(IsPolygon)
        for(int i=0;i<m_PointList.size();i++)
            m_PointList[i]=org.getPoint(i)+shift;
    else
    {
        m_Centroid=org.m_BoundingRect.center()+shift;
        m_BoundingRect.moveCenter(m_Centroid);
        m_InnerRect.moveCenter(org.m_InnerRect.center()+shift);
    }
}

//------------------------------------------------------------------------------------------------------------
// function to move the contour
//------------------------------------------------------------------------------------------------------------
int ContourLabel::selectBoundingBox(const QPointF &point, double ScaleFactor , double threshold)
{
    QList<QPointF> pointList;
    pointList.append(m_BoundingRect.topLeft());
    pointList.append(m_BoundingRect.topRight());
    pointList.append(m_BoundingRect.bottomRight());
    pointList.append(m_BoundingRect.bottomLeft());
    // first check whether the corners are selected
    for(int i=0;i<4;i++)
        if(pabs(pointList[i]-point)<threshold/ScaleFactor)
            return i;
    // then check whether the boundary is selected
    for(int i=0;i<4;i++)
        if(distPointToLineSeg(pointList[i],pointList[(i+1)%4],point)<threshold/ScaleFactor)
            return 4;
    return -1;
}

double ContourLabel::distPointToLineSeg(const QPointF& P1,const QPointF& P2,const QPointF& point,double tolerance)
{
    QPointF P,h;
    double r,p,s;
    h=P2-P1;
    P=point-P1;
    r=pabs(h);
    h/=r;
    p=h.x()*P.x()+h.y()*P.y();
    if(p<tolerance || p>r-tolerance)
        return 1000;
    s=pabs(P-h*p);
    return s;
}

//------------------------------------------------------------------------------------------------------------
// function to scale the contour
//------------------------------------------------------------------------------------------------------------
void ContourLabel::scaleContour(ContourLabel &org, const QPointF &shift, int selectedBoundingBoxCorner)
{
    if(IsPolygon)
    {
        QList<QPointF> pointList;
        pointList.append(org.m_BoundingRect.topLeft());
        pointList.append(org.m_BoundingRect.topRight());
        pointList.append(org.m_BoundingRect.bottomRight());
        pointList.append(org.m_BoundingRect.bottomLeft());
        QPointF orgVect=pointList[selectedBoundingBoxCorner]-org.m_Centroid;
        double orgScale=pabs(orgVect),dScale;
        orgVect/=orgScale; // normalize;
        dScale=shift.x()*orgVect.x()+shift.y()*orgVect.y();
        if(orgScale+dScale<0)
            dScale=2-orgScale;
        double ratio=(orgScale+dScale)/orgScale;
        for(int i=0;i<m_PointList.size();i++)
            m_PointList[i]=(org.m_PointList[i]-org.m_Centroid)*ratio+org.m_Centroid;
    }
    else
    {
        QPointF Shift(shift);
        QRectF orgRect(org.m_BoundingRect);

        if(selectedBoundingBoxCorner%2==1 && orgRect.left()+shift.x()>orgRect.right()-3)
            Shift.rx()=orgRect.right()-3-orgRect.left();

        if(selectedBoundingBoxCorner%2==0 && orgRect.right()+shift.x()<orgRect.left()+3)
            Shift.rx()=orgRect.left()+3-orgRect.right();

        if(selectedBoundingBoxCorner<2 && orgRect.top()+shift.y()>orgRect.bottom()-3)
            Shift.ry()=orgRect.right()-3-orgRect.top();

        if(selectedBoundingBoxCorner>=2 && orgRect.bottom()+shift.y()<orgRect.top()+3)
            Shift.ry()=orgRect.top()+3-orgRect.bottom();

        switch(selectedBoundingBoxCorner){
            case 0:
                m_BoundingRect.setTopLeft(orgRect.topLeft()+Shift);
                break;
            case 1:
                m_BoundingRect.setTopRight(orgRect.topRight()+Shift);
                break;
            case 2:
                m_BoundingRect.setBottomRight(orgRect.bottomRight()+Shift);
                break;
            case 3:
                m_BoundingRect.setBottomLeft(orgRect.bottomLeft()+Shift);
                break;
        }
        m_Centroid=m_BoundingRect.center();
    }
}

void ContourLabel::scaleContour(double ratio)
{
    for(int i=0;i<m_PointList.size();i++)
        m_PointList[i]=(m_PointList[i]-m_Centroid)*ratio+m_Centroid;
}

void ContourLabel::scaleBoundingBox(double ratio,const ContourLabel& contour1,const ContourLabel& contour2)
{
    m_BoundingRect.setTopLeft(contour1.m_BoundingRect.topLeft()*ratio+contour2.m_BoundingRect.topLeft()*(1-ratio));
    m_BoundingRect.setBottomRight(contour1.m_BoundingRect.bottomRight()*ratio+contour2.m_BoundingRect.bottomRight()*(1-ratio));
    m_Centroid=m_BoundingRect.center();
}

//------------------------------------------------------------------------------------------------------------
// function to compute the scale of the contour
//------------------------------------------------------------------------------------------------------------
double ContourLabel::scale() const
{
    double scale=0;
    QPointF foo;
    for(int i=0;i<m_PointList.size();i++)
    {
        foo=m_PointList[i]-m_Centroid;
        scale+=foo.x()*foo.x()+foo.y()*foo.y();
    }
    scale=sqrt(scale/m_PointList.size());
    return scale;
}

//------------------------------------------------------------------------------------------------------------
// function to remove the point according to the index
//------------------------------------------------------------------------------------------------------------
void ContourLabel::removePoint(int pointIndex)
{
    m_PointList.removeAt(pointIndex);
    m_LabelList.removeAt(pointIndex);
}

//------------------------------------------------------------------------------------------------------------
// function to compute the coeffecient pair (projection, extension) for two points interpolation 
// projection and extensions are the ratios
//------------------------------------------------------------------------------------------------------------
void ContourLabel::computeTwoPointsCoeff(int pointIndex, double &projection, double &extension)
{
    QPointF A,B,AB,Ref,X1,X2;
    double r;
    if(pointIndex>0)
        A=m_PointList[pointIndex-1];
    else
        A=m_PointList.last();
    B=m_PointList[(pointIndex+1)%m_PointList.size()];

    // get the basis X1 and X2
    AB=B-A;
    r=pabs(AB);
    X1=AB/r;
    X2=QPointF(-X1.y(),X1.x());

    // compute coefficients
    Ref=m_PointList[pointIndex]-A;
    projection=Ref.x()*X1.x()+Ref.y()*X1.y();
    
    Ref=Ref-X1*projection;
    extension=Ref.x()*X2.x()+Ref.y()*X2.y();
    projection/=r;
    extension/=r;
}

void ContourLabel::insertPointTwoPointsCoeff(int pointIndex, double projection, double extension)
{
    QPointF A,B,AB,Ref,X1,X2;
    double r;
    if(pointIndex>0)
        A=m_PointList[pointIndex-1];
    else
        A=m_PointList.last();
    // note that poinIndex is possible to go beyond the size of the list
    B=m_PointList[pointIndex%m_PointList.size()];

    // get the basis X1 and X2
    AB=B-A;
    r=pabs(AB);
    X1=AB/r;
    X2=QPointF(-X1.y(),X1.x());
    
    // reconstruct the point
    Ref=A+X1*projection*r+X2*extension*r;
    insertPoint(Ref,pointIndex,false);
}

void ContourLabel::updatePointTwoPointsCoeff(int pointIndex,double projection0,double extension0,double projection1,double extension1,double ratio)
{
    QPointF A,B,AB,X1,X2;
    double r;
    if(pointIndex>0)
        A=m_PointList[pointIndex-1];
    else
        A=m_PointList.last();
    B=m_PointList[(pointIndex+1)%m_PointList.size()];

    // get the basis X1 and X2
    AB=B-A;
    r=pabs(AB);
    X1=AB/r;
    X2=QPointF(-X1.y(),X1.x());

    // update the point
    QPointF Point0,Point1;
    Point0=A+X1*projection0*r+X2*extension0*r;
    Point1=A+X1*projection1*r+X2*extension1*r;
    //updatePoint(Point0,pointIndex,false);
    updatePoint(Point0*ratio+Point1*(1-ratio),pointIndex,false);

    //updatePoint(Ref*ratio+m_PointList[pointIndex]*(1-ratio),pointIndex,false);
}

void ContourLabel::loadPoints(const QList<QPointF> &contour)
{
    m_PointList=contour;
    m_LabelList.clear();
    for(int i=0;i<m_PointList.size();i++)
        m_LabelList.append(false);
    computeCentroid();
}

bool ContourLabel::IsKeyFrame()
{
    if(m_LabelList.size()==0)
        return false;
    for(int i=0;i<m_LabelList.size();i++)
        if(m_LabelList[i]==false)
            return false;
    return true;
}

bool ContourLabel::IsInsideImage(int pointIndex)
{
    if(m_PointList[pointIndex].x()<0 || m_PointList[pointIndex].x()>imWidth-1 || m_PointList[pointIndex].y()<0 || m_PointList[pointIndex].y()>imHeight-1)
        return false;
    else
        return true;
}

//------------------------------------------------------------------------------------------------
// function to fit affine motion from this contour to the other and interpolate the point
//------------------------------------------------------------------------------------------------
void ContourLabel::AffineFit(int pointIndex, ContourLabel &other,QPointF& result)
{
    // compute the mean distance of the other points to this point
    QVector<double> distance(m_PointList.size(),0),weight(m_PointList.size(),0);
    double meanDistance=0;
    for(int i=0;i<m_PointList.size();i++)
    {
        distance[i]=pabs(m_PointList[i]-m_PointList[pointIndex]);
        if(i!=pointIndex)
            meanDistance+=distance[i];
    }
    meanDistance/=m_PointList.size()-1;
    for(int i=0;i<m_PointList.size();i++)
        weight[i]=exp(-distance[i]/meanDistance);

    double A[9],b[3],x[3];

    // x coordinate
    memset(A,0,sizeof(double)*9);
    memset(b,0,sizeof(double)*3);

    for(int i=0;i<m_PointList.size();i++)
    {
        if(i==pointIndex)
            continue;
        A[0]+=pow(m_PointList[i].x(),2)*weight[i];
        A[1]+=m_PointList[i].x()*m_PointList[i].y()*weight[i];
        A[2]+=m_PointList[i].x()*weight[i];
        A[4]+=pow(m_PointList[i].y(),2)*weight[i];
        A[5]+=m_PointList[i].y()*weight[i];
        A[8]+=weight[i];

        b[0]+=m_PointList[i].x()*other.getPoint(i).x()*weight[i];
        b[1]+=m_PointList[i].y()*other.getPoint(i).x()*weight[i];
        b[2]+=other.getPoint(i).x()*weight[i];
    }
    A[3]=A[1];
    A[6]=A[2];
    A[7]=A[5];
    LinearSystem::ConjugateGradient(3,A,b,x);
    result.setX(m_PointList[pointIndex].x()*x[0]+m_PointList[pointIndex].y()*x[1]+x[2]);

    // y coordinate, there is no need to reform A

    memset(b,0,sizeof(double)*3);

    for(int i=0;i<m_PointList.size();i++)
    {
        if(i==pointIndex)
            continue;
        b[0]+=m_PointList[i].x()*other.getPoint(i).y()*weight[i];
        b[1]+=m_PointList[i].y()*other.getPoint(i).y()*weight[i];
        b[2]+=other.getPoint(i).y()*weight[i];
    }
    LinearSystem::ConjugateGradient(3,A,b,x);
    result.setY(m_PointList[pointIndex].x()*x[0]+m_PointList[pointIndex].y()*x[1]+x[2]);
}
