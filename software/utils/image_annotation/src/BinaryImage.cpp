
#include <stdio.h>
#include <memory.h>
#include <QImage>
#include <QVector>
#include <QColor>
#include "BinaryImage.hpp"

BinaryImage::BinaryImage(void)
{
    imWidth=imHeight=nPixels=VirtualWidth=0;
    pData=NULL;
}

BinaryImage::~BinaryImage(void)
{
    if(pData!=NULL)
        delete []pData;
}

void BinaryImage::clear()
{
    imWidth=imHeight=nPixels=VirtualWidth=0;
    if(pData!=NULL)
        delete []pData;
    pData=NULL;
}

void BinaryImage::copyData(const BinaryImage &other)
{
    clear();
    imWidth=other.imWidth;
    imHeight=other.imHeight;
    nPixels=other.nPixels;
    VirtualWidth=other.VirtualWidth;
    pData=new unsigned char[VirtualWidth*imHeight];
    memcpy(pData,other.pData,VirtualWidth*imHeight);
}

BinaryImage& BinaryImage::operator =(const BinaryImage& other)
{
    copyData(other);
    return *this;
}

BinaryImage::BinaryImage(const BinaryImage &other)
{
    pData=NULL;
    copyData(other);
}

BinaryImage::BinaryImage(int width, int height)
{
    imWidth=width;
    imHeight=height;
    nPixels=imWidth*imHeight;
    computeVirtualWidth();
    pData=new unsigned char[VirtualWidth*imHeight];
    memset(pData,255,VirtualWidth*imHeight);
}

int BinaryImage::computeVirtualWidth()
{
    // VirtualWidth must be the multiple of 4
    if(imWidth%32==0)
        VirtualWidth=imWidth/8;
    else
        VirtualWidth=(imWidth/32+1)*4;
    return VirtualWidth;
}

void BinaryImage::setAllOnes()
{
    if(pData==NULL)
        return;
    memset(pData,255,VirtualWidth*imHeight);
}

void BinaryImage::setAllZeros()
{
    if(pData==NULL)
        return;
    memset(pData,0,VirtualWidth*imHeight);
}

bool BinaryImage::imwrite(const QString& filename)
{
    QImage m_Image(pData,imWidth,imHeight,QImage::Format_Mono);
    QVector<QRgb> colors(2);
    colors[0]=QColor(Qt::black).rgb();
    colors[1]=QColor(Qt::white).rgb();
    m_Image.setColorTable(colors);
    return m_Image.save(filename,0,-1);
}

bool BinaryImage::getPixel(unsigned x, unsigned y)
{
    unsigned byte_index,byte_offset;
    register unsigned char one;
    byte_index=x/8;
    byte_offset=x%8;
    one=128>>byte_offset;
    if(pData[y*VirtualWidth+byte_index] & one == one)
        return true;
    else
        return false;
}

void BinaryImage::setPixel(unsigned x,unsigned y,bool value)
{
    unsigned byte_index,byte_offset;
    register unsigned char one;
    byte_index=x/8;
    byte_offset=x%8;
    one=128>>byte_offset;
    if(value)
        pData[y*VirtualWidth+byte_index] |= one;
    else
        pData[y*VirtualWidth+byte_index] &= ~one;
}

//-------------------------------------------------------------------------------------------
// function to generate a mask where the pixel value (1 or 0) indicates whether
// the pixel lies inside the specified polygon
//-------------------------------------------------------------------------------------------
void BinaryImage::inpolygon(const QList<QPointF>& polygon)
{
    double *pX,*pY;
    int nPoints=polygon.size();

    pX=new double[nPoints];
    pY=new double[nPoints];
    for(int i=0;i<nPoints;i++)
    {
        pX[i]=polygon[i].x();
        pY[i]=polygon[i].y();
    }
    inpolygon(nPoints,pX,pY);

    delete pX;
    delete pY;
}

void BinaryImage::inpolygon(int nPoints,const double* pX,const double* pY)
{
    setAllZeros();
    // determine the bounding box
    int Top,Left,Bottom,Right;
    Top=Bottom=pY[0];
    Left=Right=pX[0];
    for(int i=1;i<nPoints;i++)
    {
        Top=__min(Top,pY[i]);
        Bottom=__max(Bottom,pY[i]);
        Left=__min(Left,pX[i]);
        Right=__max(Right,pX[i]);
    }
    Top=__max(--Top,0);
    Left=__max(--Left,0);
    Bottom=__min(++Bottom,imHeight-1);
    Right=__min(++Right,imWidth-1);

    for(int i=Top;i<=Bottom;i++)
        for(int j=Left;j<=Right;j++)
            if(pointInPolygon((double)j,(double)i,nPoints,pX,pY))
                setPixel(j,i);
}

//-------------------------------------------------------------------------------------------
// function to dilate the mask
//-------------------------------------------------------------------------------------------
void BinaryImage::imdilate8()
{
    // step 1. horizontal dilation
    BinaryImage tempIm(*this);

    unsigned byte_index,byte_offset,offset;
    unsigned char one;
    unsigned char bytevalue;

    // dilate horizontally
    for(int i=0;i<imHeight;i++)
        for(int j=0;j<imWidth;j+=8)
            {
                byte_index=j/8;
                offset=i*VirtualWidth+byte_index;
                bytevalue=pData[offset];
                // if all zero, continue
                if(bytevalue==0)
                    continue;
                // if all one, then only dilate the begining and end
                if(bytevalue==255)
                {
                    if(byte_index>0)
                        tempIm.pData[offset-1] = tempIm.pData[offset-1] |1;
                    if(j+8<imWidth)
                        tempIm.pData[offset+1]= tempIm.pData[offset+1] |128;
                    continue;
                }
                one=128;
                for(int k=0;k<8;k++)
                {
                    if((bytevalue & one) ==one)
                    {
                        // dilate left
                        if(k==0)
                        {
                            if(byte_index>0)
                                tempIm.pData[offset-1] = tempIm.pData[offset-1] | 1;
                        }
                        else
                            tempIm.pData[offset] = tempIm.pData[offset] | (one<<1);
                        // dilate right
                        if(k==7)
                        {
                            if(j+8<imWidth)
                                tempIm.pData[offset+1] =tempIm.pData[offset+1] | 128;
                        }
                        else
                            tempIm.pData[offset] = tempIm.pData[offset] | (one>>1);
                    }
                    one = one>>1;
                }
            }
    // dilate vertically
    for(int i=0;i<imHeight;i++)
        for(int j=0;j<imWidth;j+=8)
            {
                byte_index=j/8;
                offset=i*VirtualWidth+byte_index;
                bytevalue=tempIm.pData[offset];
                // if all zero, continue
                if(bytevalue==0)
                    continue;
                pData[offset]=bytevalue;
                if(i>0)
                    pData[offset-VirtualWidth]=pData[offset-VirtualWidth]|bytevalue;
                if(i<imHeight-1)
                    pData[offset+VirtualWidth]=pData[offset+VirtualWidth]|bytevalue;
            }
}
