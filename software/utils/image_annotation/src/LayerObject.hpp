#ifndef _LayerObject_h
#define _LayerObject_h

#include <QObject>
#include "ContourLabel.hpp"
#include "LayerDepth.hpp"
#include "XmlWriter.hpp"

class ContourLabel;
class LayerDepth;
class XmlWriter;

class LayerObject: public QObject
{
    Q_OBJECT

        // the member variables
private:
    QList<ContourLabel> m_ContourLabelList;            // the contour labels, from the first frame to the last 
    QList<double> m_DepthList;                                                // the depth, as a variable of frame index
    QString m_LayerName;

    QList<bool> m_IsGlobalLabelList;
    QList<bool> m_IsLocalLabelList;
    QList<bool> m_IsTrackedList;
    QList<bool> m_IsDepthLabeledList;
    int m_NumFrames;   // the total number of frames, accessed by numFrames();
    int m_StartFrame,m_EndFrame; // the start and end frame index, range [0, numFrames()-1]. accessed by startFrame() and endFrame();
    int m_CreatedFrame; // the index of the frame where the object is created;

    static int selectedPointIndex;
    static ContourLabel m_OrgContourLabel;
    static QPointF m_OrgCursorPoint;
public:
    // construction, desctrution, copy, and clear
    LayerObject(void);
    LayerObject(const LayerObject& layerObject);
    ~LayerObject(void);
    LayerObject& operator=(const LayerObject& layerObject);
    void copyData(const LayerObject& layerObject);

    // functions to directly access the members
    static int SelectedPointIndex() {return selectedPointIndex;};
    inline int numFrames() const{return m_NumFrames;};
    inline int startFrame() const{return m_StartFrame;};
    inline int endFrame() const{return m_EndFrame;};
    inline int createdFrame() const{return m_CreatedFrame;};
    inline QString& layerName()  {return m_LayerName;};
    inline int depth(int frameIndex) const {return m_DepthList[frameIndex];};
    inline ContourLabel& contourLabel(int frameIndex)  {return m_ContourLabelList[frameIndex];};
    inline bool isPolygon(int frameIndex) const {return m_ContourLabelList[frameIndex].isPolygon();};

    // function to control the LayerObject
    inline void setLayerName(const QString& name){m_LayerName=name;};
    void create(int numFrames,int frameIndex,const QPointF& point,int layer_index);
    void create(int numFrames);
    
    inline void setStartFrame(int startFrame){m_StartFrame=startFrame;};
    inline void setEndFrame(int endFrame){m_EndFrame=endFrame;};
    inline void setCreatedFrame(int createdFrame){m_CreatedFrame=createdFrame;};
    inline void setDepth(int frameIndex,double depth){m_DepthList[frameIndex]=depth;};
    inline void setGlobalLabel(int frameIndex,bool label){m_IsGlobalLabelList[frameIndex]=label;};
    inline void setLocalLabel(int frameIndex,bool label){m_IsLocalLabelList[frameIndex]=label;};
    inline void setTracked(int frameIndex,bool label){m_IsTrackedList[frameIndex]=label;};
    inline void setDepthLabel(int frameIndex,bool label){m_IsDepthLabeledList[frameIndex]=label;};
    inline void setContourLabel(int frameIndex,const ContourLabel& contourLabel){m_ContourLabelList[frameIndex]=contourLabel;};

    inline bool InRange(int frameIndex){if(frameIndex>=m_StartFrame && frameIndex<=m_EndFrame) return true;else return false;};
    void StretchToFrame(int frameIndex);

    const QRectF& boundingBox (int frameIndex);

    // functions severing as a filter to access each ContourLabel at a specified frame
    void DrawContour(int frameIndex,QPainter& painter,QPointF& ScreenOffset,double ScaleFactor,bool IsActive=false,bool IsCreating=false,bool IsCursorOver=false);
    void ShowText(int frameIndex,QPainter& painter,QPointF& ScreenOffset,double ScaleFactor);
    double distanceToContour(int frameIndex,const QPointF& point);
    void addPoint(int frameIndex,const QPointF& point,bool IsLabeled=true);
    void updateLastPoint(int frameIndex,const QPointF& point,bool IsLabeled=true);
    void removeLastPoint(int frameIndex);
    double minPointDistance(int frameIndex,const QPointF& point);
    double minLineSegDistance(int frameIndex,const QPointF& point);
    bool IsCentroidSelected(int frameIndex,const QPointF& point,double ScaleFactor);
    int selectBoundingBox(int frameIndex,const QPointF& point,double ScaleFactor);
    
    // functions that propagatee the operation of a particular frame to the rest of the frames
    int findFirstLocalLabeledFrame(int frameIndex,int pointIndex,bool& IsForwardLabeld);
    int findLastLocalLabeledFrame(int frameIndex,int pointIndex,bool& IsBackwardLabeld);
    int findFirstGlobalLabeledFrame(int frameIndex,bool& IsForwardLabeld);
    int findLastGlobalLabeledFrame(int frameIndex,bool& IsBackwardLabeld);
    void insertPoint(int frameIndex,const QPointF& point);
    void updatePoint(int frameIndex,const QPointF& point);
    void propagateUpdatePoint(int frameIndex);
    void propagateUpdatePoint(int frameIndex,int lastFrame,bool IsForwardLabeled,int firstFrame,bool IsBackwardLabeled);
    void propagateUpdatePointForward(int frameIndex,int lastFrame);
    void propagateUpdatePointBackward(int frameIndex,int firstFrame);

    void moveContour(int frameIndex,const QPointF& point);
    void propagateMoveContour(int frameIndex);

    void scaleContour(int frameIndex,const QPointF& point,int selectedBoundingBoxCorner);
    void propagateScaleContour(int frameIndex);

    void removePoint();
    void removeLabel(int frameIndex);

    // form LayerDepth;
    void FormLayerDepth(int frameIndex,LayerDepth& layerDepth);
    void SetLayerDepth(const LayerDepth& layerDepth);
    void printLayerDepth();

    // file IO
    void writeXML(XmlWriter& xw,int id=0);

    // function to cut the labeling
    void cutLabeling(int frameIndex);
    void cutForward(int frameIndex);
    void cutBackward(int frameIndex);
    void removeAllTracking();
    void enforceKeyFrame(int frameIndex);
    void removeUserLabels(int frameIndex);
    void extendDepth();
    // track to frame
    bool IsForwardTrackable(int frameIndex);
    bool IsBackwardTrackable(int frameIndex);
    void TrackToFrame(int frameIndex,const QList<QPointF>& contour);
};

#endif
