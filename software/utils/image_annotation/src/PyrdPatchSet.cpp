#include "PyrdPatchSet.hpp"
#include "BinaryImage.hpp"
#include "ImageProcessing.hpp"

//**************************************************************
// member functions of PatchSet
//**************************************************************
PatchSet::PatchSet()
{
    nPoints=0;
    pPatch=NULL;
    pWeight=NULL;
}

PatchSet::~PatchSet()
{
    if(pPatch!=NULL)
        delete []pPatch;
    if(pWeight!=NULL)
        delete []pWeight;
}

void PatchSet::clear()
{
    if(pPatch!=NULL)
        delete []pPatch;
    if(pWeight!=NULL)
        delete []pWeight;
    pPatch=NULL;
    pWeight=NULL;
}

void PatchSet::copyData(const PatchSet &other)
{
    if(nPoints!=other.nPoints)
    {
        clear();
        nPoints=other.nPoints;
        pPatch=new ImageType[nPoints];
        pWeight=new DImage[nPoints];
    }
    for(int i=0;i<nPoints;i++)
    {
        pPatch[i].copyData(other.pPatch[i]);
        pWeight[i].copyData(other.pWeight[i]);
    }
}

void PatchSet::updateData(const PatchSet& other,double ratio)
{
    for(int k=0;k<nPoints;k++)
    {
        double *pData1,*pData2;
        pData1=pPatch[k].data();
        pData2=other.pPatch[k].data();
        for(int i=0;i<pPatch[k].nelements();i++)
            pData1[i]=pData1[i]*(1-ratio)+pData2[i]*ratio;
    }
}

//---------------------------------------------------------------------------------------------------------------
// function to sample the patch at each point of the contour, given the image and window size
//---------------------------------------------------------------------------------------------------------------
void PatchSet::SamplePatch(const ImageType &image, const QList<QPointF>& contour, int winsize)
{
    int winlength=winsize*2+1;
    nPoints=contour.size();

    // allocate the buffer for pPatch
    if(pPatch==NULL)
    {
        pPatch=new ImageType[nPoints];
        for(int i=0;i<nPoints;i++)
            pPatch[i].allocate(winlength,winlength,image.nchannels());
    }
    else
        if(pPatch[0].width()!=winlength || pPatch[0].height()!=winlength || pPatch[0].nchannels()!=image.nchannels())
        {
            delete []pPatch;
            pPatch=new ImageType[nPoints];
            for(int i=0;i<nPoints;i++)
                pPatch[i].allocate(winlength,winlength,image.nchannels());
        }
    // loop over the points to get the patch
    for(int i=0;i<nPoints;i++)
        image.getPatch(pPatch[i],contour[i].x(),contour[i].y(),winsize);
}

//---------------------------------------------------------------------------------------------------------------
// function to compute the weight at each point of the contour, given the image and window size
// a weight array consists of three components
//    (1) the pixels should lie inside the polygon specified by contour
//    (2) the pixels should lie inside the image boundary
//    (3) there is a Gaussian function that attenuate the mask
//---------------------------------------------------------------------------------------------------------------
void PatchSet::ComputeWeight(const ImageType &image, const QList<QPointF> &contour, int winsize,double sigma)
{
    int winlength=winsize*2+1;
    int winarea=winlength*winlength;
    nPoints=contour.size();

    // allocate the buffer for pWeight
    if(pWeight==NULL)
    {
        pWeight=new DImage[nPoints];
        for(int i=0;i<nPoints;i++)
            pWeight[i].allocate(winlength,winlength);
    }
    else
        if(pWeight[0].width()!=winlength || pWeight[0].height()!=winlength || pWeight[0].nchannels()!=1)
        {
            delete []pWeight;
            pWeight=new DImage[nPoints];
            for(int i=0;i<nPoints;i++)
                pWeight[i].allocate(winlength,winlength);
        }
    double *pX0,*pX,*pY0,*pY;
    
    pX0=new double[nPoints];
    pX=new double[nPoints];
    pY0=new double[nPoints];
    pY=new double[nPoints];

    for(int j=0;j<nPoints;j++)
    {
        pX0[j]=contour[j].x();
        pY0[j]=contour[j].y();
    }
    double *pGaussian2D;
    pGaussian2D=new double[winlength*winlength];
    if(sigma==0)
        sigma=(double)winsize*2/3;
    ImageProcessing::generate2DGaussian(pGaussian2D,winsize,sigma);
    BinaryImage mImage(winlength,winlength);
    DImage tempImage;
    double x,y,sum,*pData;
    int offset;

    // loop over the points to get the mask
    for(int k=0;k<nPoints;k++)
    {
        // transfer the contour so that  i-th point has the coordinate (winsize,winsize)
        for(int j=0;j<nPoints;j++)
        {
            pX[j]=pX0[j]-pX0[k]+winsize;
            pY[j]=pY0[j]-pY0[k]+winsize;
        }
        // generate the mask
        mImage.inpolygon(nPoints,pX,pY);
        // dilate the binary image
        mImage.imdilate8();
        // export to a temp image
        mImage.export2Image(tempImage);
        // smoothing
        tempImage.smoothing(pWeight[k],3);

        // consider the boundary effect of the image and add Gaussian weight
        // and also normalize the weight
        pData=pWeight[k].data();
        sum=0;
        for(int i=-winsize;i<=winsize;i++)
            for(int j=-winsize;j<=winsize;j++)
            {
                x=j+pX0[k];
                y=i+pY0[k];
                offset=(i+winsize)*winlength+j+winsize;
                if(x<0 || x>image.width()-1 || y<0 || y>image.height()-1)
                {
                    pData[offset]=0;
                    continue;
                }
                pData[offset]*=pGaussian2D[offset];
                sum+=pData[offset];
            }
        if(sum==0)
            sum=1;
        // normalize the weight so that the sum is one
        for(int i=0;i<winarea;i++)
            pData[i]/=sum;
    }
    delete [] pX0;
    delete [] pX;
    delete [] pY0;
    delete [] pY;
    delete [] pGaussian2D;
}


void PatchSet::ComputeWeight(const ImageType &image, int pointIndex,const QList<QPointF> &contour, int winsize,double sigma)
{
    if(nPoints!=1)
    {
        cout<<"Error: there has to be only one point in the patchset!"<<endl;
        return;
    }
    int winlength=winsize*2+1;
    int winarea=winlength*winlength;
    
    // allocate the buffer for pWeight
    if(pWeight==NULL)
    {
        pWeight=new DImage[nPoints];
        for(int i=0;i<nPoints;i++)
            pWeight[i].allocate(winlength,winlength);
    }
    else
        if(pWeight[0].width()!=winlength || pWeight[0].height()!=winlength || pWeight[0].nchannels()!=1)
        {
            delete []pWeight;
            pWeight=new DImage[nPoints];
            for(int i=0;i<nPoints;i++)
                pWeight[i].allocate(winlength,winlength);
        }
    double *pX,*pY;
    pX=new double[contour.size()];
    pY=new double[contour.size()];

    double *pGaussian2D;
    pGaussian2D=new double[winlength*winlength];
    if(sigma==0)
        sigma=(double)winsize*2/3;
    ImageProcessing::generate2DGaussian(pGaussian2D,winsize,sigma);
    BinaryImage mImage(winlength,winlength);
    DImage tempImage;
    double x,y,sum,*pData;
    int offset;

    QPointF Point(contour[pointIndex]);

    // transfer the contour so that  i-th point has the coordinate (winsize,winsize)
    for(int j=0;j<contour.size();j++)
    {
        pX[j]=contour[j].x()-Point.x()+winsize;
        pY[j]=contour[j].y()-Point.y()+winsize;
    }
    // generate the mask
    mImage.inpolygon(contour.size(),pX,pY);
    // dilate the binary image
    mImage.imdilate8();
    // export to a temp image
    mImage.export2Image(tempImage);
    // smoothing
    tempImage.smoothing(pWeight[0],3);

    // consider the boundary effect of the image and add Gaussian weight
    // and also normalize the weight
    pData=pWeight[0].data();
    sum=0;
    for(int i=-winsize;i<=winsize;i++)
        for(int j=-winsize;j<=winsize;j++)
        {
            x=j+Point.x();
            y=i+Point.y();
            offset=(i+winsize)*winlength+j+winsize;
            if(x<0 || x>image.width()-1 || y<0 || y>image.height()-1)
            {
                pData[offset]=0;
                continue;
            }
            pData[offset]*=pGaussian2D[offset];
            sum+=pData[offset];
        }
    if(sum==0)
        sum=1;
    // normalize the weight so that the sum is one
    for(int i=0;i<winarea;i++)
        pData[i]/=sum;

    delete pX;
    delete pY;
    delete pGaussian2D;
}

//---------------------------------------------------------------------------------------------------------------
// function to display the patches in a "big" image
//---------------------------------------------------------------------------------------------------------------
void PatchSet::dispPatches(const QString &filename, int nCols)
{
    int nRows;
    nRows=nPoints/nCols;
    if(nPoints%nCols!=0)
        nRows++;
    int dispWidth,dispHeight,winlength;
    winlength=pPatch[0].width();
    dispWidth=(winlength+1)*nCols+1;
    dispHeight=(winlength+1)*nRows+1;

    ImageType dispImage(dispWidth,dispHeight,3);
    dispImage.reset();
    
    int index=0;
    for(int i=0;i<nRows;i++)
        for(int j=0;j<nCols;j++)
        {
            pPatch[index].moveto(dispImage,j*(winlength+1)+1,i*(winlength+1)+1);                    
            if(++index==nPoints)
                break;
        }

    dispImage.imwrite(filename);
}

void PatchSet::dispFeatures(const QString &filename, int nCols)
{
    if(pPatch[0].nchannels()<=3)
        return;
    int nRows;
    nRows=nPoints/nCols;
    if(nPoints%nCols!=0)
        nRows++;
    int dispWidth,dispHeight,winlength;
    winlength=pPatch[0].width();
    dispWidth=(winlength+1)*nCols+1;
    dispHeight=(winlength+1)*nRows+1;

    ImageType dispImage(dispWidth,dispHeight,1);
    dispImage.reset();
    
    ImageType rgbImage,featureImage;
    int index=0;
    for(int i=0;i<nRows;i++)
        for(int j=0;j<nCols;j++)
        {
            pPatch[index].separate(3,rgbImage,featureImage);
            featureImage.moveto(dispImage,j*(winlength+1)+1,i*(winlength+1)+1);                    
            if(++index==nPoints)
                break;
        }
    dispImage.setDerivative();
    dispImage.imwrite(filename);
}


void PatchSet::dispWeights(const QString &filename, int nCols)
{
    int nRows;
    nRows=nPoints/nCols;
    if(nPoints%nCols!=0)
        nRows++;
    int dispWidth,dispHeight,winlength;
    winlength=pPatch[0].width();
    dispWidth=(winlength+1)*nCols+1;
    dispHeight=(winlength+1)*nRows+1;

    DImage tempImage,dispImage(dispWidth,dispHeight,1);
    dispImage.reset();
    
    int index=0;
    for(int i=0;i<nRows;i++)
        for(int j=0;j<nCols;j++)
        {
            pWeight[index].normalize(tempImage);
            tempImage.moveto(dispImage,j*(winlength+1)+1,i*(winlength+1)+1);                    
            if(++index==nPoints)
                break;
        }
    dispImage.imwrite(filename);
}

//**************************************************************
// member functions of PyrdPatchSet
//**************************************************************
PyrdPatchSet::PyrdPatchSet(void)
{
    nLevels=0;
    pPatchSet=NULL;
}

PyrdPatchSet::~PyrdPatchSet(void)
{
    if(pPatchSet!=NULL)
        delete []pPatchSet;
}

void PyrdPatchSet::clear()
{
    if(pPatchSet!=NULL)
        delete []pPatchSet;
    pPatchSet=NULL;
}

void PyrdPatchSet::copyData(const PyrdPatchSet &other)
{
    if(nLevels!=other.nLevels)
    {
        clear();
        nLevels=other.nLevels;
        pPatchSet=new PatchSet[nLevels];
    }
    for(int i=0;i<nLevels;i++)
        pPatchSet[i].copyData(other.pPatchSet[i]);
}

void PyrdPatchSet::updateData(const PyrdPatchSet &other, double ratio)
{
    for(int i=0;i<nLevels;i++)
        pPatchSet[i].updateData(other.pPatchSet[i],ratio);
}

//---------------------------------------------------------------------------------------------------------------
// function to create the patch set pyramid from a image pyramid and contour
//---------------------------------------------------------------------------------------------------------------
void PyrdPatchSet::CreatePyrdPatchSet(const ImPyramid &imPyramid, const QList<QPointF>& contour)
{
    nLevels=imPyramid.nlevels();
    nPoints=contour.size();
    clear();
    pPatchSet=new PatchSet[nLevels];
    QList<QPointF> scaledContour(contour);
    double alpha=1;
    double sigma;
    for(int i=0;i<nLevels;i++)
    {
        int winsize=(double)(nLevels-i)*2+1;
        if(i==0)
            sigma=(double)winsize*4/5;
        if(i>0)
            for(int j=0;j<nPoints;j++)
                scaledContour[j]=contour[j]*alpha;
        pPatchSet[i].SamplePatch(imPyramid.Image(i),scaledContour,winsize);
        pPatchSet[i].ComputeWeight(imPyramid.Image(i),scaledContour,winsize,sigma+0.5);
        alpha/=2;
        sigma/=2;
    }
}

void PyrdPatchSet::CreatePyrdPatchSet(const ImPyramid &imPyramid, const QPointF &point, int pointIndex, const QList<QPointF> &contour)
{
    nLevels=imPyramid.nlevels();
    nPoints=1;
    clear();
    pPatchSet=new PatchSet[nLevels];
    QList<QPointF> scaledContour(contour);
    QList<QPointF> pointContour;
    double sigma;
    pointContour.append(point);
    QPointF Point(point);
    for(int i=0;i<nLevels;i++)
    {
        int winsize=(double)(nLevels-i)*3+1;
        if(i==0)
            sigma=(double)winsize*4/5;
        if(i>0)
        {
            Point/=2;
            for(int j=0;j<contour.size();j++)
                scaledContour[j]/=2;
            scaledContour[pointIndex]=Point;
        }
        pointContour[0]=Point;
        pPatchSet[i].SamplePatch(imPyramid.Image(i),pointContour,winsize);
        pPatchSet[i].ComputeWeight(imPyramid.Image(i),pointIndex,scaledContour,winsize,sigma+0.5);
        sigma/=2;
    }
}

void PyrdPatchSet::dispPyrdPatches(const QString &BaseName, int nCols)
{
    for(int i=0;i<nLevels;i++)
        pPatchSet[i].dispPatches(BaseName+QString::number(i)+".jpg",nCols);
}

void PyrdPatchSet::dispPyrdFeatures(const QString &BaseName, int nCols)
{
    for(int i=0;i<nLevels;i++)
        pPatchSet[i].dispFeatures(BaseName+QString::number(i)+".jpg",nCols);
}

void PyrdPatchSet::dispPyrdWeights(const QString &BaseName, int nCols)
{
    for(int i=0;i<nLevels;i++)
        pPatchSet[i].dispWeights(BaseName+QString::number(i)+".jpg",nCols);
}
