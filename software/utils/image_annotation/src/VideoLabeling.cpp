#include <QtGui>
#include <QString>
#include <QtXml/QDomDocument>
#include <QtXml/QDomNode>
#include <QtXml/QDomElement>
#include "VideoLabeling.hpp"
#include "MyPreference.hpp"
#include "QVideoProcessing.hpp"
#include "ContourTracking.hpp"
#include "Image.hpp"
#include "ImPyramid.hpp"
#include "PyrdPatchSet.hpp"

#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
#include <lcmtypes/erlcm_reacquisition_segmentation_list_t.h>

VideoLabeling::VideoLabeling(MainWindow* parent):VideoSequence()
{
    selectedLayerIndex=0;
    labelState=Idle;
    pMainWindow=parent;
    IsMouseAllowed=true;
    m_lcm = bot_lcm_get_global (NULL);
}

VideoLabeling::VideoLabeling(const VideoLabeling &videoLabeling):VideoSequence(videoLabeling)
{
    copyData(videoLabeling);
}


VideoLabeling::~VideoLabeling(void)
{
}

void VideoLabeling::copyData(const VideoLabeling &videoLabeling)
{
    VideoSequence::copyData(videoLabeling);
    m_LayerObjectList=videoLabeling.m_LayerObjectList;
    selectedLayerIndex=videoLabeling.selectedLayerIndex;
    labelState=videoLabeling.labelState;
    selectedBoundingBoxCorner=videoLabeling.selectedBoundingBoxCorner;
}

VideoLabeling& VideoLabeling::operator =(const VideoLabeling &videoLabeling)
{
    copyData(videoLabeling);
    return *this;
}

//------------------------------------------------------------------------------------------------------
// function to process mouse event based on the state variable
//------------------------------------------------------------------------------------------------------
void VideoLabeling::ProcessMouseEvent(ImageViewer& imageViewer,ImageViewer::MouseAction& mouseAction,QPoint& pos)
{
    if(IsVideoLoaded==false)
        return;
    if(IsMouseAllowed==false)
        return;
    //---------------------------------------------------------------------
    // compute the logical coordinates on the image
    //---------------------------------------------------------------------
    QPointF point;
    QSize ScreenOffset=imageViewer.getPaintingOffset();
    double ScaleFactor=imageViewer.getScaleFactor();
    point.setX((double)(pos.x()-ScreenOffset.width())/ScaleFactor);
    point.setY((double)(pos.y()-ScreenOffset.height())/ScaleFactor);

    int preSelectedLayerIndex;
    bool IsPointSelected;
    int popupMenuSelection;
    QString objname;
    int64_t roi_utime = frameUtimeList.at(curFrameIndex) + bot_randf() * 1E6;
    //---------------------------------------------------------------------
    // the branches of this function is based on the mouse action
    // and the labeling state (mouse_action x state)-->state
    //---------------------------------------------------------------------
    switch(mouseAction){
        case ImageViewer::LeftClick: // left click
            switch(labelState){
                //---------------------------------------------------------------------------
                // after left click, the Idel and CursorOver state will be activated to 
                // either CreateContour or Edit_InsertPoint, based on whether a
                // contour is selected
                //---------------------------------------------------------------------------                    
                case Idle:
                case CursorOver:
                    if(selectLayerObject(point,ScaleFactor)==true) // one conto
                        labelState=Selected;
                    else
                    {
                        // an empty spot is clicked
                        labelState=CreateContour;
                        createLayerObject(point);
                    }
                    emit Update();
                    break;
                //---------------------------------------------------------------------------
                // if the current state is CreateContour, then clicking adds a new
                // point to the existing contour and the state doesn't change. 
                // update the plot.
                //---------------------------------------------------------------------------
                case CreateContour:
                    m_LayerObjectList[selectedLayerIndex].addPoint(curFrameIndex,point,true);
                    emit Update();
                    break;
                //---------------------------------------------------------------------------
                // if the current state is Edit_InsertPoint, first check whether the click
                // is outside the contour (so the contour is disselected). Otherwise, 
                // check whether a point is selected or a line segment is selected
                //---------------------------------------------------------------------------
                case Selected:
                    preSelectedLayerIndex=selectedLayerIndex;
                    if(m_LayerObjectList[selectedLayerIndex].isPolygon(curFrameIndex) && selectLayerObject(point,ScaleFactor))
                    {
                        // other contour is selected
                        if(preSelectedLayerIndex!=selectedLayerIndex)
                            labelState=Idle;
                        else    // else check whether a line segment or point is selected
                        {
                            selectPoint(point,ScaleFactor);
                            labelState=MovePoint;
                        }
                    }
                    else // if global changes are selected
                    {
                        if(m_LayerObjectList[selectedLayerIndex].IsCentroidSelected(curFrameIndex,point,ScaleFactor)==true) // start move the contour
                            labelState=MoveContour;
                        else
                        {
                            selectedBoundingBoxCorner=m_LayerObjectList[selectedLayerIndex].selectBoundingBox(curFrameIndex,point,ScaleFactor);
                            if(selectedBoundingBoxCorner==4) // the boundary of the bonding box is selected, moving
                                labelState=MoveContour;
                            else if(selectedBoundingBoxCorner==-1) // an empty region is selected
                                labelState=Idle;
                            else // the corners are selected, scaling
                                labelState=ScaleContour;
                        }
                    }
                    emit Update();
                    break;
                default:
                    break;
            }
            break; // end of LeftClick
        //---------------------------------------------------------------------------
        // right lick, to generate popup menus
        //---------------------------------------------------------------------------
        case ImageViewer::RightClick:
            switch(labelState){
                case CreateContour:
                    labelState=Selected;
                    if(m_LayerObjectList[selectedLayerIndex].contourLabel(curFrameIndex).size()==2)
                        m_LayerObjectList[selectedLayerIndex].contourLabel(curFrameIndex).setIsPolygon(false);
                    emit Update();
                   objname=QInputDialog::getText(pMainWindow,"Annotation","Please name the object:",QLineEdit::Normal,m_LayerObjectList[selectedLayerIndex].layerName(),0,Qt::Dialog);
                    if(!objname.isEmpty())
                        m_LayerObjectList[selectedLayerIndex].setLayerName(objname);

                    // Publish the segmentation message
                    erlcm_reacquisition_segmentation_list_t seg_list;
                    seg_list.utime = roi_utime;
                    seg_list.num_segs = 1;

                    seg_list.segmentations =  (erlcm_reacquisition_segmentation_t *) calloc (1, seg_list.num_segs * sizeof (erlcm_reacquisition_segmentation_t));
                    for (int i=0; i < seg_list.num_segs; i++) {
                        seg_list.segmentations[i].utime = roi_utime;
                        seg_list.segmentations[i].roi_utime = roi_utime;
                        seg_list.segmentations[i].image_utime = frameUtimeList.at(curFrameIndex);
                        seg_list.segmentations[i].object_id = (0xefffffffffffffff&(bot_timestamp_now()<<8)) + 256*rand()/RAND_MAX;
                        seg_list.segmentations[i].camera = "dragonfly";
                        seg_list.segmentations[i].roi.npoints = m_LayerObjectList[selectedLayerIndex].contourLabel(curFrameIndex).size();
                        seg_list.segmentations[i].roi.points = (erlcm_point2d_t *) calloc (1, seg_list.segmentations[i].roi.npoints * sizeof (erlcm_point2d_t));
                        
                        for (int j=0; j < m_LayerObjectList[selectedLayerIndex].contourLabel(curFrameIndex).size(); j++) {
                            seg_list.segmentations[i].roi.points[j].x = m_LayerObjectList[selectedLayerIndex].contourLabel(curFrameIndex).x(j);
                            seg_list.segmentations[i].roi.points[j].y = m_LayerObjectList[selectedLayerIndex].contourLabel(curFrameIndex).y(j);
                        }
                    }

                    // lcm_t *lcm = bot_lcm_get_global (NULL);
                    cout << "Publishing message with utime " << roi_utime << " and image utime = " << frameUtimeList.at(curFrameIndex) << endl;
                    erlcm_reacquisition_segmentation_list_t_publish (m_lcm, "TABLET_SEGMENTATION", &seg_list);
                    
                    free (seg_list.segmentations);

                    break;
                case CursorOver:
                    ProcessPopupMenuSelection(selectPopupMenu(false));
                    break;
                case Selected:
                    if(m_LayerObjectList[selectedLayerIndex].minPointDistance(curFrameIndex,point)<MyPreference::Threshold_ContourDistance/ScaleFactor)
                        IsPointSelected=true;
                    else if(m_LayerObjectList[selectedLayerIndex].minLineSegDistance(curFrameIndex,point)<MyPreference::Threshold_ContourDistance/ScaleFactor ||
                                m_LayerObjectList[selectedLayerIndex].selectBoundingBox(curFrameIndex,point,ScaleFactor)>0)
                                IsPointSelected=false;
                    else
                    {
                        labelState=Idle;
                        emit Update();
                        return;
                    }
                    ProcessPopupMenuSelection(selectPopupMenu(IsPointSelected));
                    break;
                default:
                    break;
            }
            break; // end of right click
        //---------------------------------------------------------------------------
        // mouse move, transitions
        //---------------------------------------------------------------------------
        case ImageViewer::Move:
            switch(labelState){
                case Idle:
                    if(selectLayerObject(point,ScaleFactor)==true)
                    {
                        labelState=CursorOver;
                        emit Update();
                    }
                    break;
                case CursorOver:
                    preSelectedLayerIndex=selectedLayerIndex;
                    if(selectLayerObject(point,ScaleFactor)==true)
                    {
                        if(selectedLayerIndex!=preSelectedLayerIndex)
                            emit Update();
                    }
                    else
                    {
                        labelState=Idle;
                        emit Update();
                    }
                    break;
                case CreateContour:
                    m_LayerObjectList[selectedLayerIndex].updateLastPoint(curFrameIndex,point);
                    emit Update();
                    break;
                case MovePoint:
                    m_LayerObjectList[selectedLayerIndex].updatePoint(curFrameIndex,point);
                    emit Update();
                    break;
                case MoveContour:
                    m_LayerObjectList[selectedLayerIndex].moveContour(curFrameIndex,point);
                    emit Update();
                    break;
                case ScaleContour:
                    m_LayerObjectList[selectedLayerIndex].scaleContour(curFrameIndex,point,selectedBoundingBoxCorner);
                    emit Update();
                    break;
            }
            break; // end of Move
        //---------------------------------------------------------------------------
        // double click
        // it is effective only when the state is CreateContour. double click 
        // means finishing creating the contour and the state transit to Selected
        //---------------------------------------------------------------------------
        case ImageViewer::DoubleClick: // double click
            if(labelState==CreateContour)
            {
                m_LayerObjectList[selectedLayerIndex].removeLastPoint(curFrameIndex);
                if(m_LayerObjectList[selectedLayerIndex].contourLabel(curFrameIndex).size()==2)
                        m_LayerObjectList[selectedLayerIndex].contourLabel(curFrameIndex).setIsPolygon(false);
                labelState=Selected;
                emit Update();
                 objname=QInputDialog::getText(pMainWindow,"Annotation","Please name the object:",QLineEdit::Normal,m_LayerObjectList[selectedLayerIndex].layerName(),0,Qt::Dialog);
                if(!objname.isEmpty())
                    m_LayerObjectList[selectedLayerIndex].setLayerName(objname);
            }
            break; // end of double lick
        //---------------------------------------------------------------------------
        // release
        //---------------------------------------------------------------------------
        case ImageViewer::Release: // mouse released
            switch(labelState){
                case MovePoint:
                    m_LayerObjectList[selectedLayerIndex].updatePoint(curFrameIndex,point);
                    m_LayerObjectList[selectedLayerIndex].propagateUpdatePoint(curFrameIndex);
                    //propagateUpdatePoint();
                    labelState=Selected;
                    emit Update();
                    break;
                case MoveContour:
                    m_LayerObjectList[selectedLayerIndex].moveContour(curFrameIndex,point);
                    m_LayerObjectList[selectedLayerIndex].propagateMoveContour(curFrameIndex);
                    labelState=Selected;
                    emit Update();
                    break;
                case ScaleContour:
                    m_LayerObjectList[selectedLayerIndex].scaleContour(curFrameIndex,point,selectedBoundingBoxCorner);
                    m_LayerObjectList[selectedLayerIndex].propagateScaleContour(curFrameIndex);
                    labelState=Selected;
                    emit Update();
                    break;
            }
            break; // end of release
    }
}


//-------------------------------------------------------------------------------------------
// function to jump FORWARD to a particular frame in log
//-------------------------------------------------------------------------------------------
bool VideoLabeling::goForwardToFrameLCM (int frameIndex)
{
    int at_event = 0;
    // Check the current event
    if (lcmChannel.compare (lcm_event->channel) == 0) {
        bot_core_image_t img;
        int decode_status = bot_core_image_t_decode (lcm_event->data, 0, lcm_event->datalen, &img);
        if (decode_status < 0) {
            qDebug() << "Error decoding bot_core_image_t message!" << endl;
            exit (1);
        }
        if (frameUtimeList.at(frameIndex) == img.utime) {
            at_event = 1;
            return true;
        }
    }

    lcm_eventlog_free_event (lcm_event);
    while (at_event == 0) {
        lcm_event = lcm_eventlog_read_next_event (lcm_eventlog);
        
        if (lcm_event == NULL) {
            qDebug() << "lcm_event is NULL: Reached end of log." << endl;
            return false;
        }
        
        if (lcmChannel.compare (lcm_event->channel) == 0) {
            bot_core_image_t img;
            int decode_status = bot_core_image_t_decode (lcm_event->data, 0, lcm_event->datalen, &img);
            if (decode_status < 0) {
                qDebug() << "Error decoding bot_core_image_t message!" << endl;
                exit (1);
            }            
            
            if (frameUtimeList.at(frameIndex) == img.utime) {
                at_event = 1;
                bot_core_image_t_decode_cleanup (&img);
                return true;
            }
            bot_core_image_t_decode_cleanup (&img);
        }
        lcm_eventlog_free_event (lcm_event);
    }
    return true;
}    

//------------------------------------------------------------------------------------------------------
// function to display the contour labels
//------------------------------------------------------------------------------------------------------
void VideoLabeling::Paint(ImageViewer &imageViewer, QPainter &mPainter)
{
    if(m_LayerObjectList.size()==0)
        return;

    QSize temp;
    QPointF ScreenOffset;
    double ScaleFactor;
    temp=imageViewer.getPaintingOffset();
    ScreenOffset.setX(temp.width());
    ScreenOffset.setY(temp.height());
    ScaleFactor=imageViewer.getScaleFactor();    

    // draw the contour according to the depth
    QMap<unsigned int, int> map;
    for (int i=0;i<m_LayerObjectList.size();i++)
        map.insert(m_LayerObjectList[i].depth(curFrameIndex)*255+i,i);
    QList<unsigned int> list;
    list=map.keys();

    for(int i=0;i<m_LayerObjectList.size();i++)
    {
        int index=map.value(list[m_LayerObjectList.size()-1-i]);
        if(index!=selectedLayerIndex)
            m_LayerObjectList[index].DrawContour(curFrameIndex,mPainter,ScreenOffset,ScaleFactor);
        else
        {
            bool IsActive=false,IsCreating=false,IsCursorOver=false;
            switch(labelState){
                case Idle:
                    break;
                case CursorOver:
                    IsCursorOver=true;
                    break;
                case CreateContour:
                    IsCreating=true;
                    break;
                case Selected:
                case MovePoint:
                case ScaleContour:    
                case MoveContour:
                    IsActive=true;
                    break;
            }
            m_LayerObjectList[index].DrawContour(curFrameIndex,mPainter,ScreenOffset,ScaleFactor,IsActive,IsCreating,IsCursorOver);
        }
    }
    if(labelState==CursorOver)
        m_LayerObjectList[selectedLayerIndex].ShowText(curFrameIndex,mPainter,ScreenOffset,ScaleFactor);
}

//------------------------------------------------------------------------------------------------------
// Function to output the labeling as images. For each labeled image, it outputs the full image overlayed 
// with labels along with cropped versions of each labeled region.
//------------------------------------------------------------------------------------------------------
void VideoLabeling::OutputLabelFrame(int frameIndex, const QString& pathName, int quality)
{
    // seek to the correct place in the eventlog (assuming forward seek)
    VideoLabeling::goForwardToFrameLCM (frameIndex);

    // Load the frame from the LCM image data
    bot_core_image_t img;
    int decode_status = bot_core_image_t_decode (lcm_event->data, 0, lcm_event->datalen, &img);
    if (decode_status < 0) {
        qDebug() << "Error decoding bot_core_image_t message, got " << decode_status << " !" << endl;
        exit (1);
    }


    QImage outputImage;
    if(outputImage.loadFromData(img.data, img.size) == false) {
        qDebug() << "Error loading image data for output" << endl;
        bot_core_image_t_decode_cleanup (&img);            
        return;
    }
    QImage outputImageCopy = outputImage.copy ();
    bot_core_image_t_decode_cleanup (&img);
    //if(outputImage.load(framePath+fileList[frameIndex])==false)
    //    return;
    QPainter painter(&outputImage);
    painter.setRenderHint(QPainter::Antialiasing);
    bool hasLabel = false;
    if(m_LayerObjectList.size()>0)
    {
        // draw the contour according to the depth
        QMap<unsigned int, int> map;
        for (int i=0;i<m_LayerObjectList.size();i++)
            map.insert(m_LayerObjectList[i].depth(frameIndex)*255+i,i);
        QList<unsigned int> list;
        list=map.keys();
        for(int i=0;i<m_LayerObjectList.size();i++)
        {
            int index=map.value(list[m_LayerObjectList.size()-1-i]);
            QPointF p1(0,0), p2(0,0);
            m_LayerObjectList[index].DrawContour(frameIndex,painter,p1,1,false,false,true);
            m_LayerObjectList[index].ShowText(frameIndex,painter,p2,1);

            QRectF boundingBox = m_LayerObjectList[index].boundingBox(frameIndex);

            int u0 = boundingBox.topLeft().x(), v0 = boundingBox.topLeft().y();
            int width = boundingBox.bottomRight().x() - u0;
            int height = boundingBox.bottomRight().y() - v0;
            
            hasLabel = hasLabel | m_LayerObjectList[index].InRange(frameIndex);

            if (width==0 || height==0)
                continue;

            QImage croppedImage = outputImageCopy.copy (u0, v0, width, height);

            QString fileName = pathName+m_LayerObjectList[index].layerName()+"_"+QString::number(frameUtimeList.at(frameIndex))+".jpg";

            qDebug() << "frameIndex = " << frameIndex << ", index = " << index << "saving fileName = " << fileName << endl;
            if(croppedImage.save(fileName,0,quality) == false)
                qDebug() << "VideoLabeling:OutputLabelFrame Error saving image" << fileName << endl;
            
        }
    }

    if (hasLabel) {
        QString fileName = pathName+"Label_"+QString::number(frameUtimeList.at(frameIndex))+".jpg";
        if(outputImage.save(fileName,0,quality) == false)
            qDebug() << "VideoLabeling:OutputLabelFrame Error saving image" << fileName << endl;
    }
}

//------------------------------------------------------------------------------------------------------
// function to output the whole labeling
//------------------------------------------------------------------------------------------------------
void VideoLabeling::OutputAllLabeling(const QString &pathname)
{
    // Temporarily close the LCM log and reopen at the beginning
    lcm_eventlog_destroy (lcm_eventlog);
    lcm_eventlog = lcm_eventlog_create (lcmLogFile.toLatin1(), "r");
    if (lcm_eventlog == NULL) {
        qDebug() << "VideoLabeling:OutputAllLabeling: Error opening LCM log" << endl;
        exit(1);
    }
    lcm_eventlog_free_event (lcm_event);
    lcm_event = lcm_eventlog_read_next_event (lcm_eventlog);

    QString PathName(pathname);
    if(PathName.right(1)!="/")
        PathName+="/";
    IsMouseAllowed=false;
    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
    for(int i=0;i<numFrames;i++) {
        //OutputLabelFrame(i,PathName+"Label "+fileList[i],95);
        //QString filename;
        //filename.setNum(frameUtimeList.at(i));
        //OutputLabelFrame(i,PathName+"Label_"+QString::number(frameUtimeList.at(i))+".jpg",95);
        OutputLabelFrame(i, PathName, 95);
    }
    IsMouseAllowed=true;
    QApplication::restoreOverrideCursor();

    lcm_eventlog_free_event (lcm_event);

    // Now go back to the curFrameIndex event in the log
    // First, go to the beginning
    lcm_eventlog_destroy (lcm_eventlog);
    lcm_eventlog = lcm_eventlog_create (lcmLogFile.toLatin1(), "r");
    if (lcm_eventlog == NULL) {
        qDebug() << "VideoLabeling:OutputAllLabeling: Error opening LCM log" << endl;
        exit(1);
    }
    lcm_event = lcm_eventlog_read_next_event (lcm_eventlog);

    if (VideoLabeling::goForwardToFrameLCM (curFrameIndex) == false) {
        qDebug() << "VideoLabeling:OutputAllLabeling: Error returning to current frame in LCM log" << endl;
        exit(1);
    }
}

//------------------------------------------------------------------------------------------------------
// overrided function
//------------------------------------------------------------------------------------------------------
void VideoLabeling::gotoFrame(int findex)
{
    // for now we don't strecth the frames; it can only be tracked
    //for(int i=0;i<m_LayerObjectList.size();i++)
    //    if(i==selectedLayerIndex && labelState!=Idle && labelState!=CursorOver)
    //        m_LayerObjectList[i].StretchToFrame(findex-1);

    // when the contour is modified, change frame is prohibited
    if(labelState==CreateContour || labelState==MovePoint || labelState==ScaleContour || labelState==MoveContour)
        return;

    if(labelState==Selected)
        if(m_LayerObjectList[selectedLayerIndex].isPolygon(curFrameIndex)==false)
            m_LayerObjectList[selectedLayerIndex].StretchToFrame(findex-1);

    if(m_LayerObjectList.size()>0)
        if(m_LayerObjectList[selectedLayerIndex].startFrame()>findex-1 || m_LayerObjectList[selectedLayerIndex].endFrame()<findex-1)
            if(labelState!=Idle)
                labelState=Idle;

    // it's important that this function is called later since it will update the drawing
    VideoSequence::gotoFrame(findex);

    ContourLabel::setImWidth(m_Image.width());
    ContourLabel::setImHeight(m_Image.height());
}

//-----------------------------------------------------------------------------------------------------
// function to select layer object according to the distance to the contour
//-----------------------------------------------------------------------------------------------------
bool VideoLabeling::selectLayerObject(const QPointF &point, double ScaleFactor)
{
    double MinDistance=MyPreference::DefaultContourDistance;
    int layerIndex=0;
    // first check whether the query point selects a point
    for(int i=0;i<m_LayerObjectList.size();i++)
    {
        //double distance=m_LayerObjectList[i].distanceToContour(curFrameIndex,point);
        double distance;
        if(m_LayerObjectList[i].isPolygon(curFrameIndex))
            distance=m_LayerObjectList[i].minPointDistance(curFrameIndex,point);
        else
            continue;
        if(distance<MinDistance)
        {
            MinDistance=distance;
            layerIndex=i;
        }
    }
    if(MinDistance<MyPreference::Threshold_PointDistance/sqrt(ScaleFactor))
    {
        selectedLayerIndex=layerIndex;
        return true;
    }
    // then check whether the query point selects a contour
    MinDistance=MyPreference::DefaultContourDistance;
    layerIndex=0;
    for(int i=0;i<m_LayerObjectList.size();i++)
    {
        double distance;
        if(m_LayerObjectList[i].isPolygon(curFrameIndex))
            distance=m_LayerObjectList[i].minLineSegDistance(curFrameIndex,point);
        else
            continue;
        if(distance<MinDistance)
        {
            MinDistance=distance;
            layerIndex=i;
        }
    }
    if(MinDistance<MyPreference::Threshold_LineDistance/sqrt(ScaleFactor))
    {
        selectedLayerIndex=layerIndex;
        return true;
    }
    // finally, check whether the bounding box is selected
    for(int i=0;i<m_LayerObjectList.size();i++)
    {
        if(m_LayerObjectList[i].isPolygon(curFrameIndex))
            continue;
        if(m_LayerObjectList[i].IsCentroidSelected(curFrameIndex,point,ScaleFactor))
        {
            selectedLayerIndex=i;
            return true;
        }
        if(m_LayerObjectList[i].selectBoundingBox(curFrameIndex,point,ScaleFactor)>0)
        {
            selectedLayerIndex=i;
            return true;
        }
    }
    return false;
}

//-----------------------------------------------------------------------------------------------------
// function to create a layer object
//-----------------------------------------------------------------------------------------------------
void VideoLabeling::createLayerObject(const QPointF &point)
{
    LayerObject newLayerObject;
    newLayerObject.create(numFrames,curFrameIndex,point,m_LayerObjectList.size());
    m_LayerObjectList.append(newLayerObject);
    selectedLayerIndex=m_LayerObjectList.size()-1;
}

void VideoLabeling::selectPoint(const QPointF &point, double ScaleFactor)
{
    double minDistance;
    // first search the points
    minDistance=m_LayerObjectList[selectedLayerIndex].minPointDistance(curFrameIndex,point);
    if(minDistance<MyPreference::Threshold_PointDistance/ScaleFactor)
        return;
    m_LayerObjectList[selectedLayerIndex].insertPoint(curFrameIndex,point);
    return;
}

void VideoLabeling::InitializeLCM(const QString&lcmlog, const QString&channel, const bool verboseOutput)
{
    m_LayerObjectList.clear();
    selectedLayerIndex=0;
    labelState=Idle;
    lcm_eventlog = NULL;
    lcm_event = NULL;
    VideoSequence::InitializeLCM(lcmlog, channel, verboseOutput);
}

void VideoLabeling::Initialize(const QString&path, const QFileInfoList& filelist)
{
    m_LayerObjectList.clear();
    selectedLayerIndex=0;
    labelState=Idle;
    lcm_eventlog = NULL;
    lcm_event = NULL;
    VideoSequence::Initialize(path,filelist);
}

void VideoLabeling::freeLCM()
{
    if (lcm_event != NULL)
        lcm_eventlog_free_event (lcm_event);
   
    if (lcm_eventlog != NULL)
        lcm_eventlog_destroy (lcm_eventlog);
}

//----------------------------------------------------------------------------------------
// function to display a popup menu and return which one is chosen based on
// whether a point is selected
// return value: 0--delete point if selected (when IsPointSelected==false, no 0
//                         -2--delete label
//                          1--delete cfontour is selected
//                          2--rename is selected
//                          3--change depth
//
//                          11--forward track one frame
//                          12--forward track five frames
//                          13--forward track ten frames
//
//                          21--backward track one frame
//                          22--backward track five frames
//                          23--backward track five frames
//
//                         100--track all the frames
//
//                         51--forward replicate one frame
//                         52--forward replicate five frames
//                         53--forward replicate ten frames
//                         57--forward replicate all frames
//
//                         61--backward replicate one frame
//                         62--backward replicate five frames
//                         63--backward replicate ten frames
//                         67--backward replicate all frames
//
//                         70--replicate all the frames
//
//                          40--cut the labeling beyond the point
//                          41--remove all the trackings
//
//                          30--enforce the frame to be a key frame
//                          31--remove the user labels
//
//                          81--cut all the forward frames
//                          82--cut all the backward frames
//----------------------------------------------------------------------------------------
int VideoLabeling::selectPopupMenu(bool IsPointSelected)
{
    QMenu popupMenu;
    QAction *deletePoint,*deleteLabel,*deleteContour,*rename,*changeDepth,*selected;
    QAction *forwardTrackOneFrame,*forwardTrackFiveFrames,*forwardTrackTenFrames,*forwardTrackAll;
    QAction *backwardTrackOneFrame,*backwardTrackFiveFrames,*backwardTrackTenFrames,*backwardTrackAll,*trackAll;
    QAction* forwardReplicateOneFrame,*forwardReplicateFiveFrames,*forwardReplicateTenFrames,*forwardReplicateAll;
    QAction* backwardReplicateOneFrame=NULL,*backwardReplicateFiveFrames=NULL,*backwardReplicateTenFrames=NULL,*backwardReplicateAll=NULL,*replicateAll=NULL;
    QAction *enforceKeyFrame=NULL,*removeUserLabels=NULL;
    QAction *cutLabeling=NULL,*removeAllTracking=NULL;
    // these are the actions for the boundingbox objects
    QAction *cutForward=NULL,*cutBackward=NULL;

    if(IsPointSelected)
    {
        deletePoint=popupMenu.addAction(tr("Delete &Point"));
        deleteLabel=popupMenu.addAction(tr("Delete &Label"));
    }
    deleteContour=popupMenu.addAction(tr("Delete &Contour"));
    rename=popupMenu.addAction(tr("&Rename..."));
    changeDepth=popupMenu.addAction(tr("&Change depth"));
    
    if(curFrameIndex!=m_LayerObjectList[selectedLayerIndex].createdFrame())
    {
        popupMenu.addSeparator();
        enforceKeyFrame=popupMenu.addAction(tr("Enforce this frame to key frame"));
        removeUserLabels=popupMenu.addAction(tr("Remove all the user labels"));
    }
    popupMenu.addSeparator();

    if(m_LayerObjectList[selectedLayerIndex].isPolygon(curFrameIndex))
    {
        // the tracking subMenus
        bool IsForwardTracking=false;
        QMenu* forwardTrackingMenu;
        if(m_LayerObjectList[selectedLayerIndex].IsForwardTrackable(curFrameIndex))
        {
            forwardTrackingMenu=popupMenu.addMenu("Forward tracking");
            forwardTrackOneFrame=forwardTrackingMenu->addAction(tr("1 frame"));
            forwardTrackFiveFrames=forwardTrackingMenu->addAction(tr("5 frames"));
            forwardTrackTenFrames=forwardTrackingMenu->addAction(tr("10 frames"));
            forwardTrackAll=forwardTrackingMenu->addAction(tr("All frames"));
            IsForwardTracking=true;
        }
        bool IsBackwardTracking=false;
        QMenu* backwardTrackingMenu;
        if(m_LayerObjectList[selectedLayerIndex].IsBackwardTrackable(curFrameIndex))
        {
            backwardTrackingMenu=popupMenu.addMenu("Backward tracking");
            backwardTrackOneFrame=backwardTrackingMenu->addAction(tr("1 frame"));
            backwardTrackFiveFrames=backwardTrackingMenu->addAction(tr("5 frames"));
            backwardTrackTenFrames=backwardTrackingMenu->addAction(tr("10 frames"));
            backwardTrackAll=backwardTrackingMenu->addAction(tr("All frames"));
            IsBackwardTracking=true;
        }
        bool IsTrackAll=false;
        if(m_LayerObjectList[selectedLayerIndex].endFrame()<numFrames-1 || m_LayerObjectList[selectedLayerIndex].startFrame()>0)
        {
            trackAll=popupMenu.addAction(tr("Track all the frames"));
            IsTrackAll=true;
        }
        if(IsForwardTracking || IsBackwardTracking || IsTrackAll)
            popupMenu.addSeparator();

        // the replicating subMenus
        bool IsForwardReplicate=false;
        QMenu* forwardReplicateMenu;
        if(m_LayerObjectList[selectedLayerIndex].IsForwardTrackable(curFrameIndex))
        {
            forwardReplicateMenu=popupMenu.addMenu("Forward replicate");
            forwardReplicateOneFrame=forwardReplicateMenu->addAction(tr("1 frame"));
            forwardReplicateFiveFrames=forwardReplicateMenu->addAction(tr("5 frames"));
            forwardReplicateTenFrames=forwardReplicateMenu->addAction(tr("10 frames"));
            forwardReplicateAll=forwardReplicateMenu->addAction(tr("All frames"));
            IsForwardReplicate=true;
        }
        bool IsBackwardReplicate=false;
        QMenu* backwardReplicateMenu;
        if(m_LayerObjectList[selectedLayerIndex].IsBackwardTrackable(curFrameIndex))
        {
            backwardReplicateMenu=popupMenu.addMenu("Backward replicate");
            backwardReplicateOneFrame=backwardReplicateMenu->addAction(tr("1 frame"));
            backwardReplicateFiveFrames=backwardReplicateMenu->addAction(tr("5 frames"));
            backwardReplicateTenFrames=backwardReplicateMenu->addAction(tr("10 frames"));
            backwardReplicateAll=backwardReplicateMenu->addAction(tr("All frames"));
            IsBackwardReplicate=true;
        }
        bool IsReplicateAll=false;
        if(m_LayerObjectList[selectedLayerIndex].endFrame()<numFrames-1 || m_LayerObjectList[selectedLayerIndex].startFrame()>0)
        {
            replicateAll=popupMenu.addAction(tr("Replicate to the rest"));
            IsReplicateAll=true;
        }
        if(IsForwardReplicate || IsBackwardReplicate || IsReplicateAll)
            popupMenu.addSeparator();

        // actions to cut the labeling
        bool IsCutLabeling=false;
        if(m_LayerObjectList[selectedLayerIndex].createdFrame()!=curFrameIndex)
        {
            cutLabeling=popupMenu.addAction(tr("Cut labeling beyond this frame"));
            IsCutLabeling=true;
        }
        bool IsRemoveAllTracking=false;;
        if(m_LayerObjectList[selectedLayerIndex].endFrame()-m_LayerObjectList[selectedLayerIndex].startFrame()>0)
        {
            removeAllTracking=popupMenu.addAction(tr("Remove all trackings"));
            IsRemoveAllTracking=true;
        }
    }
    else
    {
        cutForward=popupMenu.addAction(tr("Remove all the annotations before this frame"));
        cutBackward=popupMenu.addAction(tr("Remove all the annotations after this frame"));
    }

    selected=popupMenu.exec(QCursor::pos());

    if(selected==NULL)
        return -1;

    if(IsPointSelected)
    {
        if(selected==deletePoint)
            return 0;
        if(selected==deleteLabel)
            return -2;
    }

    if(selected==deleteContour)
        return 1;
    if(selected==rename)
        return 2;
    if(selected==changeDepth)
        return 3;
    
    if(m_LayerObjectList[selectedLayerIndex].isPolygon(curFrameIndex))
    {
        if(selected==forwardTrackOneFrame)
            return 11;
        if(selected==forwardTrackFiveFrames)
            return 12;
        if(selected==forwardTrackTenFrames)
            return 13;
        if(selected==forwardTrackAll)
            return 17;

        if(selected==backwardTrackOneFrame)
            return 21;
        if(selected==backwardTrackFiveFrames)
            return 22;
        if(selected==backwardTrackTenFrames)
            return 23;
        if(selected==backwardTrackAll)
            return 27;

        if(selected==forwardReplicateOneFrame)
            return 51;
        if(selected==forwardReplicateFiveFrames)
            return 52;
        if(selected==forwardReplicateTenFrames)
            return 53;
        if(selected==forwardReplicateAll)
            return 57;

        if(selected==backwardReplicateOneFrame)
            return 61;
        if(selected==backwardReplicateFiveFrames)
            return 62;
        if(selected==backwardReplicateTenFrames)
            return 63;
        if(selected==backwardReplicateAll)
            return 67;

        if(selected==replicateAll)
            return 70;

        if(selected==trackAll)
            return 100;

        if(selected==cutLabeling)
            return 40;
        if(selected==removeAllTracking)
            return 41;

        if(selected==enforceKeyFrame)
            return 30;
        if(selected==removeUserLabels)
            return 31;
    }
    else
    {
        if(selected==cutForward)
            return 81;
        if(selected==cutBackward)
            return 82;
    }
    return -1;
}

void VideoLabeling::ProcessPopupMenuSelection(int choice)
{
    QString objname;
    QList<LayerDepth> layerDepthList;
    LayerDepth layerDepth;
    int tempCurFrameIndex;
    //cout<<choice<<endl;
    switch(choice){
        case 0: // delete point
            m_LayerObjectList[selectedLayerIndex].removePoint();
            labelState=Selected;
            emit Update();
            break;
        case -2:// delete label
            m_LayerObjectList[selectedLayerIndex].removeLabel(curFrameIndex);
            labelState=Selected;
            emit Update();
            break;
        case 1: // delete the contour
            if(QMessageBox::question(pMainWindow,"Warning!","Are you sure deleting this contour?",QMessageBox::Yes,QMessageBox::No)==QMessageBox::No)
                return;
            m_LayerObjectList.removeAt(selectedLayerIndex);
            selectedLayerIndex=0;
            labelState=Idle;
            emit Update();
            break;
        case 2: // rename the contour
            objname=QInputDialog::getText(pMainWindow,"Annotation","Rename the object:",QLineEdit::Normal,m_LayerObjectList[selectedLayerIndex].layerName(),0,Qt::Dialog);
            if(!objname.isEmpty())
                m_LayerObjectList[selectedLayerIndex].setLayerName(objname);
            break;
        case 3: // change the depth
            for(int i=0;i<m_LayerObjectList.size();i++)
            {
                m_LayerObjectList[i].FormLayerDepth(curFrameIndex,layerDepth);
                layerDepthList.append(layerDepth);
            }
            emit changeLayerDepth(layerDepthList,selectedLayerIndex,curFrameIndex);
            m_LayerObjectList[selectedLayerIndex].extendDepth();
            break;
        case 11:// forward track one frame
            TrackContour(true,1);
            break;
        case 12:// forward track five frames
            TrackContour(true,5);
            break;
        case 13:// forward track five frames
            TrackContour(true,10);
            break;
        case 17:// forward track all
            curFrameIndex=m_LayerObjectList[selectedLayerIndex].endFrame();
            gotoFrame(curFrameIndex+1);
            QApplication::processEvents();
            TrackContour(true,numFrames-1-curFrameIndex);
            break;
        case 21://backward track one frame
            TrackContour(false,1);
            break;
        case 22://backward track five frames
            TrackContour(false,5);
            break;
        case 23://backward track five frames
            TrackContour(false,10);
            break;
        case 27: //backward track all
            curFrameIndex=m_LayerObjectList[selectedLayerIndex].startFrame();
            gotoFrame(curFrameIndex+1);
            QApplication::processEvents();
            TrackContour(false,curFrameIndex);
            break;
        case 100:// track all the frames;
            tempCurFrameIndex=curFrameIndex;
            if(m_LayerObjectList[selectedLayerIndex].endFrame()<numFrames-1)
            {
                curFrameIndex=m_LayerObjectList[selectedLayerIndex].endFrame();
                gotoFrame(curFrameIndex+1);
                QApplication::processEvents();
                TrackContour(true,numFrames-1-curFrameIndex);
            }
            if(m_LayerObjectList[selectedLayerIndex].startFrame()>0)
            {
                curFrameIndex=m_LayerObjectList[selectedLayerIndex].startFrame();
                gotoFrame(curFrameIndex+1);
                QApplication::processEvents();
                TrackContour(false,curFrameIndex);
            }
            break;
        case 51: // forward replicate one frame
            ReplicateContour(true,curFrameIndex+1);
            break;
        case 52: // forward replicate five frames
            ReplicateContour(true,curFrameIndex+5);
            break;
        case 53: // forward replicate ten frames
            ReplicateContour(true,curFrameIndex+10);
            break;
        case 57: // forward replicate all the frames
            ReplicateContour(true,numFrames-1);
            break;
        case 61: // backward replicate one frame
            ReplicateContour(false,curFrameIndex-1);
            break;
        case 62: // backward replicate five frames
            ReplicateContour(false,curFrameIndex-5);
            break;
        case 63: // backward replicate ten frames
            ReplicateContour(false,curFrameIndex-10);
            break;
        case 67: // backward replicate all the frames
            ReplicateContour(false,0);
            break;
        case 70: // replicate all the frames
            m_LayerObjectList[selectedLayerIndex].StretchToFrame(0);
            m_LayerObjectList[selectedLayerIndex].StretchToFrame(numFrames-1);
            break;
        case 40:// cut labeling
            m_LayerObjectList[selectedLayerIndex].cutLabeling(curFrameIndex);
            break;
        case 41:// remove all the trackings
            m_LayerObjectList[selectedLayerIndex].removeAllTracking();
            if(m_LayerObjectList[selectedLayerIndex].createdFrame()!=curFrameIndex)
            {
                labelState=Idle;
                emit Update();
            }
            break;
        case 30://enforce the current frame to be a key frame
            m_LayerObjectList[selectedLayerIndex].enforceKeyFrame(curFrameIndex);
            emit Update();
            break;
        case 31: // remove the user labels
            m_LayerObjectList[selectedLayerIndex].removeUserLabels(curFrameIndex);
            emit Update();
        case 81:
            m_LayerObjectList[selectedLayerIndex].cutForward(curFrameIndex);
            emit Update();
            break;
        case 82:
            m_LayerObjectList[selectedLayerIndex].cutBackward(curFrameIndex);
            emit Update();
            break;
    }
}

void VideoLabeling::ReplicateContour(bool direction,int destFrame)
{
    m_LayerObjectList[selectedLayerIndex].StretchToFrame(destFrame);
    if(direction)
        gotoFrame(m_LayerObjectList[selectedLayerIndex].endFrame()+1);
    else
        gotoFrame(m_LayerObjectList[selectedLayerIndex].startFrame()+1);
}

void VideoLabeling::setLayerDepth(const LayerDepth &layerDepth)
{
    m_LayerObjectList[selectedLayerIndex].SetLayerDepth(layerDepth);
    emit Update();
}

//-------------------------------------------------------------------------------------------------------------------------
// file IO
//-------------------------------------------------------------------------------------------------------------------------
bool VideoLabeling::writeVideoLabelMeXML(const QString &filename)
{
    QFile file(filename);
    if(file.open(QIODevice::WriteOnly)==false)
        return false;

    // compute the relative path
    QString pathName,subFolder;
    QVideoProcessing::separatePath_FileNames(framePath,pathName,subFolder);

    XmlWriter xw(&file);
    xw.setAutoNewLine(true);

    xw.writeOpenTag("annotation");
        xw.writeTaggedString("version","1.00");
        xw.writeTaggedString("videoType","frames");
        
        if (fileList.isEmpty() == false)
            xw.writeTaggedString("folder",framePath);
        if (lcmLogFile.isEmpty() == false)
            xw.writeTaggedString("lcmLogFile",lcmLogFile);
        xw.writeTaggedString("NumFrames",QString::number(numFrames));
        xw.writeTaggedString("currentFrame",QString::number(curFrameIndex));
        if (lcmChannel.isEmpty() == false)
            xw.writeTaggedString("lcmChannelName", lcmChannel);
        if (fileList.isEmpty() == false) {
            xw.writeOpenTag("fileList");
            for(int i=0;i<numFrames;i++)
                xw.writeTaggedString("fileName",fileList[i]);
            xw.writeCloseTag("fileList");
        }
        else if(frameUtimeList.isEmpty() == false) {
            xw.writeOpenTag("lcmImageUtimes");
            for (int i=0; i<numFrames;i++)
                xw.writeTaggedString("utime",QString::number(frameUtimeList.at(i)));
            xw.writeCloseTag("lcmImageUtimes");
        }
        xw.writeOpenTag("source");
        if (lcmLogFile.isEmpty() == false)
            xw.writeTaggedString("sourceImage","LCM log file");
        xw.writeTaggedString("sourceAnnotation","Adaptation of Qt-based C++ video labeling tool (by Ce Liu, Aug 2007)");
        xw.writeCloseTag("source");
        for(int i=0;i<m_LayerObjectList.size();i++)
            m_LayerObjectList[i].writeXML(xw,i);
    xw.writeCloseTag("annotation");
    return true;
}

bool VideoLabeling::readVideoLabelMeXML(const QString& filename, const bool verboseOutput)
{
    // read XML into buffer
     QDomDocument doc("mydocument");
     QFile file(filename);
     if (!file.open(QIODevice::ReadOnly))
         return false;
     if (!doc.setContent(&file)) {
         file.close();
         return false;
     }
     file.close();

     verbose = verboseOutput;

     // clear the member variables
     fileList.clear();
     frameUtimeList.clear();
     lcmLogFile.clear();
     lcmChannel.clear();
     m_LayerObjectList.clear();
     selectedLayerIndex=0;
     curFrameIndex=0;
     labelState=Idle;

     // 
     QDomElement root=doc.documentElement();
     QDomNode n=root.firstChild();
     numFrames=0;
     while(!n.isNull())
     {
         QDomElement e=n.toElement();
         // parse the element at the top level
         if(e.tagName()=="folder")
             framePath=e.text();
         if(e.tagName()=="NumFrames")
             numFrames=e.text().toInt();
         if(e.tagName()=="currentFrame")
             curFrameIndex=e.text().toInt();
         if(e.tagName()=="lcmLogFile")
             lcmLogFile = e.text();
         if(e.tagName()=="lcmChannelName")
             lcmChannel = e.text();

         // load all the image file names
         if(e.tagName()=="fileList")
         {
             QDomNode filelist_n=e.firstChild();
             while(!filelist_n.isNull())
             {
                 QDomElement filelist_e=filelist_n.toElement();
                 if(filelist_e.tagName()=="fileName")
                     fileList.append(filelist_e.text());
                 filelist_n=filelist_n.nextSibling();
             }
             if(numFrames==0)
                 numFrames=fileList.size();
             // check whether  the files are found
             if(QVideoProcessing::FileListExist(framePath,fileList)==false)
             {
                 QString curFolderPath,temp,subFolderName;
                 QVideoProcessing::separatePath_FileNames(filename,curFolderPath,temp);
                 if(framePath.right(1)=="/")
                     framePath.chop(1);
                 QVideoProcessing::separatePath_FileNames(framePath,temp,subFolderName);
                 if(curFolderPath.right(1)!="/")
                     curFolderPath+="/";
                 if(QVideoProcessing::FileListExist(curFolderPath+subFolderName,fileList)==false)
                     return false;
                 framePath=curFolderPath+subFolderName+"/";
             }
            if(framePath.right(1)!="/")
                 framePath+="/";
         }

         // LCM log-based annotation
         if(e.tagName()=="lcmImageUtimes")
         {
             QDomNode frameUtimeList_n = e.firstChild();
             while (!frameUtimeList_n.isNull())
             {
                 QDomElement frameUtimeList_e = frameUtimeList_n.toElement();
                 if (frameUtimeList_e.tagName()=="utime") {
                     QString utime_string = frameUtimeList_e.text();
                     frameUtimeList.append(utime_string.toLongLong());
                 }
                 frameUtimeList_n = frameUtimeList_n.nextSibling();
             }
             if (numFrames==0)
                 numFrames = frameUtimeList.size();
         }

         if(e.tagName()=="source") // so far does not parse source
         {
         }

         if(e.tagName()=="object") // parse the layer object
         {
             QDomNode obj_n=e.firstChild();
             LayerObject layerObject;
             while(!obj_n.isNull())
             {
                 QDomElement obj_e=obj_n.toElement();
                 if(obj_e.tagName()=="name")
                     layerObject.setLayerName(obj_e.text());
                 if(obj_e.tagName()=="deleted")
                 {
                 }
                 if(obj_e.tagName()=="verified")
                 {
                 }
                 if(obj_e.tagName()=="date")
                 {
                 }
                 if(obj_e.tagName()=="id")
                 {
                 }
                 if(obj_e.tagName()=="numFrames")
                     layerObject.create(obj_e.text().toInt());
                 if(obj_e.tagName()=="startFrame")
                     layerObject.setStartFrame(obj_e.text().toInt());
                 if(obj_e.tagName()=="endFrame")
                     layerObject.setEndFrame(obj_e.text().toInt());
                 if(obj_e.tagName()=="createdFrame")
                     layerObject.setCreatedFrame(obj_e.text().toInt());
                 if(obj_e.tagName()=="labels")
                 {
                     // load the labels
                     QDomNode label_n=obj_e.firstChild();
                     while(!label_n.isNull())
                     {
                         int frameIndex;
                         QDomElement label_e=label_n.toElement();
                         if(label_e.tagName()=="frame")
                         {
                             QDomNode frame_n=label_e.firstChild();
                             while(!frame_n.isNull())
                             {
                                 QDomElement frame_e=frame_n.toElement();

                                 if(frame_e.tagName()=="index")
                                     frameIndex=frame_e.text().toInt();
                                 if(frame_e.tagName()=="depth")
                                    layerObject.setDepth(frameIndex,frame_e.text().toDouble());
                                 if(frame_e.tagName()=="globalLabel")
                                     layerObject.setGlobalLabel(frameIndex,frame_e.text().toInt());
                                 if(frame_e.tagName()=="localLabel")
                                     layerObject.setLocalLabel(frameIndex,frame_e.text().toInt());
                                 if(frame_e.tagName()=="tracked")
                                     layerObject.setTracked(frameIndex,frame_e.text().toInt());
                                 if(frame_e.tagName()=="depthLabel")
                                     layerObject.setDepthLabel(frameIndex,frame_e.text().toInt());
                                 if(frame_e.tagName()=="polygon") // read the polygon
                                 {
                                     ContourLabel contourLabel;
                                     QDomNode contour_n=frame_e.firstChild();
                                     while(!contour_n.isNull())
                                     {
                                         QPointF point(0,0);
                                         bool isLabeled=true;
                                         QDomElement contour_e=contour_n.toElement();
                                         // add one point
                                         if(contour_e.tagName()=="pt")
                                         {
                                             QDomNode pt_n=contour_e.firstChild();
                                             while(!pt_n.isNull())
                                             {
                                                 QDomElement pt_e=pt_n.toElement();
                                                 if(pt_e.tagName()=="x")
                                                     point.setX(pt_e.text().toDouble());
                                                 if(pt_e.tagName()=="y")
                                                     point.setY(pt_e.text().toDouble());
                                                 if(pt_e.tagName()=="labeled")
                                                     isLabeled=pt_e.text().toInt();
                                                 pt_n=pt_n.nextSibling();
                                             }
                                             contourLabel.addPoint(point,isLabeled);
                                         }
                                        contour_n=contour_n.nextSibling();
                                     }
                                    layerObject.setContourLabel(frameIndex,contourLabel);
                                 } // end of reading the polygon
                                 if(frame_e.tagName()=="boundingBox")
                                 {
                                     ContourLabel contourLabel;
                                     contourLabel.setIsPolygon(false);
                                     QRectF boundingBox;
                                     QDomNode contour_n=frame_e.firstChild();
                                     while(!contour_n.isNull())
                                     {
                                         QPointF point(0,0);
                                         bool isLabeled=true;
                                         QDomElement contour_e=contour_n.toElement();
                                         // add one point
                                         if(contour_e.tagName()=="topLeft")
                                         {
                                             QDomNode pt_n=contour_e.firstChild();
                                             while(!pt_n.isNull())
                                             {
                                                 QDomElement pt_e=pt_n.toElement();
                                                 if(pt_e.tagName()=="x")
                                                     point.setX(pt_e.text().toDouble());
                                                 if(pt_e.tagName()=="y")
                                                     point.setY(pt_e.text().toDouble());
                                                 pt_n=pt_n.nextSibling();
                                             }
                                             boundingBox.setTopLeft(point);
                                         }
                                         if(contour_e.tagName()=="bottomRight")
                                         {
                                             QDomNode pt_n=contour_e.firstChild();
                                             while(!pt_n.isNull())
                                             {
                                                 QDomElement pt_e=pt_n.toElement();
                                                 if(pt_e.tagName()=="x")
                                                     point.setX(pt_e.text().toDouble());
                                                 if(pt_e.tagName()=="y")
                                                     point.setY(pt_e.text().toDouble());
                                                 pt_n=pt_n.nextSibling();
                                             }
                                             boundingBox.setBottomRight(point);
                                         }
                                        contour_n=contour_n.nextSibling();
                                     }
                                    contourLabel.setBoundingBox(boundingBox);
                                    layerObject.setContourLabel(frameIndex,contourLabel);
                                 }
                                 frame_n=frame_n.nextSibling();
                             }
                         }// end of reading the frames
                         label_n=label_n.nextSibling();
                     }// end of reading the labels
                 }
                 obj_n=obj_n.nextSibling();
             } // end of parsing all the element in one layer

             layerObject.extendDepth();
             m_LayerObjectList.append(layerObject);
         } // end of looping over layers

         // end parsing the element at the top level
         n=n.nextSibling();
     }
     emit NumFramesChanged(numFrames);
     
     // load the LCM log if specified
     if (lcmLogFile.isEmpty() == false) {
         if (verbose == true)
             qDebug() << "Reading LCM file" << lcmLogFile << endl;
         lcm_eventlog = lcm_eventlog_create (lcmLogFile.toLatin1(), "r");
         if (lcm_eventlog == NULL) {
             qDebug() << "Error loading LCM logfile" << lcmLogFile << "while loading XML project file" << filename << endl;
             exit(1);
         }

         // Now, go to first image in log
         int decode_status;
         for (lcm_event = lcm_eventlog_read_next_event (lcm_eventlog);
              lcm_event != NULL;
              lcm_event = lcm_eventlog_read_next_event (lcm_eventlog)) {
        
             if (lcmChannel.compare (lcm_event->channel) == 0) {
                 bot_core_image_t img;
                 decode_status = bot_core_image_t_decode (lcm_event->data, 0, lcm_event->datalen, &img);
                 if (decode_status >= 0) {
                     if (frameUtimeList.at(curFrameIndex) == img.utime) {
                         bot_core_image_t_decode_cleanup (&img);
                         break;
                     }
                 }
                 
                 bot_core_image_t_decode_cleanup (&img);
             }

             lcm_eventlog_free_event (lcm_event);
         }
     }

     gotoFrame(curFrameIndex+1);
     IsVideoLoaded=true;
     return true;
}

//-----------------------------------------------------------------------------------------------------------------------------------------
// function to track contours
//     Input arguments
//        direction-- the tracking direction, true: forward, false: backward
//        nFrames-- the number of frames to track
//-----------------------------------------------------------------------------------------------------------------------------------------
void VideoLabeling::TrackContour(bool direction,int nFrames)
{
    if((curFrameIndex==0 && direction==false) || (curFrameIndex==numFrames-1 && direction==true))
        return;
    if(curFrameIndex<m_LayerObjectList[selectedLayerIndex].endFrame() && curFrameIndex>m_LayerObjectList[selectedLayerIndex].startFrame())
        return;
    IsMouseAllowed=false;
    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

    // get the contour of the current object at the current frame
    QList<QPointF> contour=m_LayerObjectList[selectedLayerIndex].contourLabel(curFrameIndex).PointList();
    // display the contour
    //for(int i=0;i<contour.size();i++)
    //    cout<<contour[i].x()<<" "<<contour[i].y()<<endl;

    BiImage frame1(m_Image),frame2;
    ImPyramid CurFramePyrd;
    QPointF offset1;
    PyrdPatchSet pyrdPatchSet1,pyrdPatchSet2;

    // build image pyramid and sample the patches
    offset1=ContourTracking::CreateCroppedPyramid(CurFramePyrd,frame1,contour,MyPreference::nPyrdLevels);
    ContourTracking::SubtractOffset(contour,offset1);
    pyrdPatchSet1.CreatePyrdPatchSet(CurFramePyrd,contour);
    ContourTracking::AddOffset(contour,offset1);

    // code for debugging
    //pyrdPatchSet1.dispPyrdWeights("Weight");

    // prepare for tracking
    int nPoints=contour.size();
    QList<QPointF> trackedContour(QList<QPointF>::fromVector(QVector<QPointF>(nPoints)));
    QList<bool> IsVisibleList(QList<bool>::fromVector(QVector<bool>(nPoints,true)));
    QList<QPointF> prediction;
    
    prediction=contour;
    OccludingReasoning(IsVisibleList,prediction);
    //pyrdPatchSet1.dispPyrdWeights("Weight",6);
    //pyrdPatchSet1.dispPyrdPatches("Patch",6);
    //pyrdPatchSet1.dispPyrdFeatures("Feature",6);

    QList<QPointF> vHistory; // the list of velocity
    // start tracking
    for(int k=1;k<=nFrames;k++)
    {
        // check whether frame beyond the scope of the video needs to be loaded
        if((curFrameIndex==0 && direction==false) || (curFrameIndex==numFrames-1 && direction==true))
            break;


        // goto the next frame
        if(direction)
            curFrameIndex++;
        else
            curFrameIndex--;
        

        // hack if we're moving back to an earlier frame
        if (!direction) {
            lcm_eventlog_destroy (lcm_eventlog);
            lcm_eventlog = lcm_eventlog_create (lcmLogFile.toLatin1(), "r");
            if (lcm_eventlog == NULL) {
                qDebug() << "Error opening LCM log during bacwards seek" << endl;
                exit(1);
            }

            lcm_eventlog_free_event (lcm_event);
            lcm_event = lcm_eventlog_read_next_event (lcm_eventlog);
        }


        // seek to the correct place in the eventlog
        if (goForwardToFrameLCM(curFrameIndex) == false) {
            qDebug() << "VideoLabeling::TrackContour: Error retrieving image frame from LCM log file" << endl;
            return;
        }

        // Load the frame from the LCM image data
        bot_core_image_t img;
        int decode_status = bot_core_image_t_decode (lcm_event->data, 0, lcm_event->datalen, &img);
        if (decode_status < 0) {
            qDebug() << "Error decoding bot_core_image_t message!" << endl;
            exit (1);
        }
        
        if (m_Image.loadFromData(img.data, img.size) == false) {
            qDebug() << "Error loading image from LCM message!" << endl;
            bot_core_image_t_decode_cleanup (&img);
            return;
        }
        bot_core_image_t_decode_cleanup (&img);

        // load the next image
        //if(m_Image.load(framePath+fileList[curFrameIndex])==false)
        //    return;
        frame2.imread(m_Image);

        // compute the visibility of the prediction
        OccludingReasoning(IsVisibleList,prediction);
        bool IsWholeOccluded=true;
        for(int i=0;i<IsVisibleList.size();i++)
            if(IsVisibleList[i]==true)
            {
                IsWholeOccluded=false;
                break;
            }
        if(IsWholeOccluded==true)
        {
            m_LayerObjectList[selectedLayerIndex].TrackToFrame(curFrameIndex,prediction);
            emit ImageChanged(m_Image);
            updateFrameControl();
            emit Update();
            QApplication::processEvents();
            break;
        }
        //for(int i=0;i<IsVisibleList.size();i++)
        //    cout<<(int)prediction[i].x()<<" "<<(int)prediction[i].y()<<" "<<IsVisibleList[i]<<endl;
        //cout<<endl;
        
        // the tracking procedure
        if(k==1)
            ContourTracking::TrackOneFrameCropped(trackedContour,pyrdPatchSet2,contour,prediction,IsVisibleList,pyrdPatchSet1,frame2,0.002,6,16); // extra margin for the first time calling
        else
            ContourTracking::TrackOneFrameCropped(trackedContour,pyrdPatchSet2,contour,prediction,IsVisibleList,pyrdPatchSet1,frame2,0.002,6,10); 
        
        // prepare for the next tracking and update the contour
        // meanwhile, compute the velocity and check whether the tracking is totally wrong
        QPointF velocity(0,0);
        for(int i=0;i<nPoints;i++)
        {
            prediction[i]=trackedContour[i]*2-contour[i];
            QPointF diff=trackedContour[i]-contour[i];
            velocity+=diff;
        }
        velocity/=nPoints;

        contour=trackedContour;
        //pyrdPatchSet1.copyData(pyrdPatchSet2);
        pyrdPatchSet1.updateData(pyrdPatchSet2,0.7);

        // update the layer object information
        m_LayerObjectList[selectedLayerIndex].TrackToFrame(curFrameIndex,contour);

        // update the controller and draw the tracked contour
        emit ImageChanged(m_Image);
        updateFrameControl();
        emit Update();
        QApplication::processEvents();

        // check wether the tracking is valid
        if(vHistory.size()==0)
        {
            double temp=sqrt(velocity.x()*velocity.x()+velocity.y()*velocity.y());
            if(temp>MyPreference::Threshold_MaxTrackingVelocity)  // it's unlikely the object moves beyond a certain velocity
                break;
        }
        else
        {
            QPointF acceleration;
            acceleration=velocity-vHistory.last();
            double temp=sqrt(acceleration.x()*acceleration.x()+acceleration.y()*acceleration.y());
            if(temp>MyPreference::Threshold_MaxTrackingAcceleration)
                break;
        }
        vHistory.append(velocity);
        m_LayerObjectList[selectedLayerIndex].setGlobalLabel(curFrameIndex,true);
    }
    IsMouseAllowed=true;
    QApplication::restoreOverrideCursor();
}
//-----------------------------------------------------------------------------------------------------------------------------------------
// function to derive whether the contour points are visible
//-----------------------------------------------------------------------------------------------------------------------------------------
void VideoLabeling::OccludingReasoning(QList<bool> &IsVisibleList,const QList<QPointF>& contour)
{
    QVector<QList<QPointF> > OccludingContours;
    for(int i=0;i<m_LayerObjectList.size();i++)
    {
        // if this contour, continue;
        if(i==selectedLayerIndex)
            continue;
        // if the current frame is not in the life of the contour object
        if(m_LayerObjectList[i].endFrame()<curFrameIndex || m_LayerObjectList[i].startFrame()>curFrameIndex)
            continue;
        if(m_LayerObjectList[i].depth(curFrameIndex)<m_LayerObjectList[selectedLayerIndex].depth(curFrameIndex))
            OccludingContours.append(m_LayerObjectList[i].contourLabel(curFrameIndex).PointList());
    }
    ContourTracking::OcclusionReasoning(IsVisibleList,contour,OccludingContours,m_Image.width(),m_Image.height(),7);
}

//-----------------------------------------------------------------------------------------------------------------------------------------
// function to derive whether a point is visible
//-----------------------------------------------------------------------------------------------------------------------------------------
bool VideoLabeling::OccludingReasoning(const QPointF& point)
{
    if(point.x()<0 || point.x()>m_Image.width()-1 || point.y()<0 || point.y()>m_Image.height()-1)
        return false;
    QVector<QList<QPointF> > OccludingContours;
    for(int i=0;i<m_LayerObjectList.size();i++)
    {
        // if this contour, continue;
        if(i==selectedLayerIndex)
            continue;
        // if the current frame is not in the life of the contour object
        if(m_LayerObjectList[i].endFrame()<curFrameIndex || m_LayerObjectList[i].startFrame()>curFrameIndex)
            continue;
        if(m_LayerObjectList[i].depth(curFrameIndex)<m_LayerObjectList[selectedLayerIndex].depth(curFrameIndex))
            OccludingContours.append(m_LayerObjectList[i].contourLabel(curFrameIndex).PointList());
    }
    if(OccludingContours.size()==0)
        return true;
    QList<bool> IsVisibleList;
    IsVisibleList.append(true);
    QList<QPointF> contour;
    contour.append(point);
    ContourTracking::OcclusionReasoning(IsVisibleList,contour,OccludingContours,m_Image.width(),m_Image.height(),7);
    return IsVisibleList[0];
}

//-----------------------------------------------------------------------------------------------------------------------------------------
// function to propagate the point that has just been updates
//
// CURRENTLY NOT OPERATIONAL IN LCM PORT AS VIDEOLABELING:TRACKPOINT HAS NOT BEEN PORTED
//-----------------------------------------------------------------------------------------------------------------------------------------
void VideoLabeling::propagateUpdatePoint()
{
    qDebug() << "VideoLabeling:propagateUpdatePoint() SHOULD NOT BE HERE" << endl;
    return;

    int lastLabelFrame,firstLabelFrame;
    bool IsLastFrameLabeled,IsFirstFrameLabeled;
    int pointIndex=m_LayerObjectList[selectedLayerIndex].SelectedPointIndex();
    lastLabelFrame=findLastLocalLabeledFrame(curFrameIndex,pointIndex,IsLastFrameLabeled);
    firstLabelFrame=findFirstLocalLabeledFrame(curFrameIndex,pointIndex,IsFirstFrameLabeled);
    // first only do goemetric propagation
    if(OccludingReasoning(m_LayerObjectList[selectedLayerIndex].contourLabel(curFrameIndex).getPoint(pointIndex))==false)
    {
        m_LayerObjectList[selectedLayerIndex].propagateUpdatePoint(curFrameIndex,lastLabelFrame,IsLastFrameLabeled,firstLabelFrame,IsFirstFrameLabeled);
        return;
    }
    // forward tracking
    if(lastLabelFrame-curFrameIndex-1>0)
        TrackPoint(pointIndex,true,lastLabelFrame-curFrameIndex-1,lastLabelFrame);
    // backward tracking
    if(curFrameIndex-firstLabelFrame-1>0)
        TrackPoint(pointIndex,false,curFrameIndex-firstLabelFrame-1,firstLabelFrame);
}

//-----------------------------------------------------------------------------------------------------------------------------------------
// function to track a particular point from curFrameIndex, forward (direction=true) or backward (direction=false), for
// nFrames.
//
// CURRENTLY NOT OPERATIONAL IN LCM PORT.
//-----------------------------------------------------------------------------------------------------------------------------------------
void VideoLabeling::TrackPoint(int pointIndex,bool direction, int nFrames,int destFrame)
{
    qDebug() << "VideoLabeling:TrackPoint SHOULD NOT BE HERE!" << endl;
    if((curFrameIndex==0 && direction==false) || (curFrameIndex==numFrames-1 && direction==true))
        return;
    IsMouseAllowed=false;
    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

    // get the point of the current object at the current frame
    QPointF point=m_LayerObjectList[selectedLayerIndex].contourLabel(curFrameIndex).getPoint(pointIndex);


    BiImage frame1(m_Image),frame2;
    ImPyramid CurFramePyrd;
    QPointF offset1;
    PyrdPatchSet pyrdPatchSet1,pyrdPatchSet2;

    // build image pyramid and sample the patches
    QList<QPointF> contour;
    contour.append(point);
    offset1=ContourTracking::CreateCroppedPyramid(CurFramePyrd,frame1,contour,4,20);
    ContourTracking::SubtractOffset(contour,offset1);
    QList<QPointF> tempContour(m_LayerObjectList[selectedLayerIndex].contourLabel(curFrameIndex).PointList());
    ContourTracking::SubtractOffset(tempContour,offset1);
    pyrdPatchSet1.CreatePyrdPatchSet(CurFramePyrd,point-offset1,pointIndex,tempContour);
    ContourTracking::AddOffset(contour,offset1);

    //pyrdPatchSet1.dispPyrdWeights("weight1_",1);
    //pyrdPatchSet1.dispPyrdPatches("patch1_",1);

    // prepare for tracking
    QPointF    prediction,tracked;
    int frameIndex=curFrameIndex;

    // start tracking
    double alpha;

    // for debug
    QPointF refPoint=m_LayerObjectList[selectedLayerIndex].contourLabel(10).getPoint(pointIndex);

    for(int k=1;k<=nFrames;k++)
    //for(int k=1;k<=1;k++)
    {
        // check whether frame beyond the scope of the video needs to be loaded
        if((curFrameIndex==0 && direction==false) || (curFrameIndex==numFrames-1 && direction==true))
            break;

        // goto the next frame
        if(direction)
            frameIndex++;
        else
            frameIndex--;
        // get the prediction
        prediction=m_LayerObjectList[selectedLayerIndex].contourLabel(frameIndex).getPoint(pointIndex);

        // TODO: Update to work with LCM log rather than image file
        // load the next image
        if(frame2.imread(framePath+fileList[frameIndex])==false)
            return;

        // compute the visibility of the prediction
        if(OccludingReasoning(prediction)==false)
        {
            if(direction==true)
                m_LayerObjectList[selectedLayerIndex].propagateUpdatePointForward(frameIndex-1,destFrame);
            else
                m_LayerObjectList[selectedLayerIndex].propagateUpdatePointBackward(frameIndex+1,destFrame);
            break;
        }

        tracked=ContourTracking::TrackPointOneFrameCropped(pyrdPatchSet2,point,prediction,pointIndex,m_LayerObjectList[selectedLayerIndex].contourLabel(frameIndex).PointList(),pyrdPatchSet1,frame2,0.00001,5,25);
        //pyrdPatchSet2.dispPyrdWeights("weight2_",1);
        //pyrdPatchSet2.dispPyrdPatches("patch2_",1);

        // prepare for the next tracking and update the point
        if(nFrames>1)
            alpha=(double)(k-1)/(nFrames-1);
        else
            alpha=0;
        QPointF updated=tracked*(1-alpha)+prediction*alpha;
        m_LayerObjectList[selectedLayerIndex].contourLabel(frameIndex).updatePoint(updated,pointIndex,false);
        point=tracked;
        pyrdPatchSet1.updateData(pyrdPatchSet2,0.6);

    }
    IsMouseAllowed=true;
    QApplication::restoreOverrideCursor();
}

//-----------------------------------------------------------------------------------------------------------------------------------------
// function to find the last labeled frame
// If the current point is visible, and it becomes invisible and visible again before reaching the last labeled frame, the last
// invisible frame is the last labeled frame
// If the current point is invisible, and it becomes visible before reaching the last labeled frame, the last invisible frame is 
// the last labeled frame
//-----------------------------------------------------------------------------------------------------------------------------------------
int VideoLabeling::findLastLocalLabeledFrame(int frameIndex,int pointIndex,bool &IsLastFrameLabeled)
{
    if(frameIndex==m_LayerObjectList[selectedLayerIndex].endFrame())
    {
        IsLastFrameLabeled=m_LayerObjectList[selectedLayerIndex].contourLabel(frameIndex).labeled(pointIndex);
        return frameIndex;
    }
    int i;
    IsLastFrameLabeled=false;
    bool IsVisible0=OccludingReasoning(m_LayerObjectList[selectedLayerIndex].contourLabel(frameIndex).getPoint(pointIndex));
    bool IsCurVisible;
    int flag=0;
    for(i=frameIndex+1;i<=m_LayerObjectList[selectedLayerIndex].endFrame();i++)
    {
        IsCurVisible=OccludingReasoning(m_LayerObjectList[selectedLayerIndex].contourLabel(i).getPoint(pointIndex));
        if(IsVisible0==false && IsCurVisible==true)
        {
            IsLastFrameLabeled=true;
            return i;
        }
        if(IsVisible0==true && IsCurVisible==true && flag==1)
        {
            IsLastFrameLabeled=true;
            return i;
        }
        if(IsVisible0==true && IsCurVisible==false && flag==0)
            flag=1;
        if(m_LayerObjectList[selectedLayerIndex].contourLabel(i).labeled(pointIndex))
        {
            IsLastFrameLabeled=true;
            return i;
        }
    }
    return i;
}

//-----------------------------------------------------------------------------------------------------------------------------------------
// function to find the first labeled frame for a particular point pointIndex
//-----------------------------------------------------------------------------------------------------------------------------------------
int VideoLabeling::findFirstLocalLabeledFrame(int frameIndex,int pointIndex,bool &IsFirstFrameLabeled)
{
    if(frameIndex==m_LayerObjectList[selectedLayerIndex].startFrame())
    {
        IsFirstFrameLabeled=m_LayerObjectList[selectedLayerIndex].contourLabel(frameIndex).labeled(pointIndex);
        return frameIndex;
    }
    int i;
    IsFirstFrameLabeled=false;
    bool IsVisible0=OccludingReasoning(m_LayerObjectList[selectedLayerIndex].contourLabel(frameIndex).getPoint(pointIndex));
    bool IsCurVisible;
    int flag=0;
    for(i=frameIndex-1;i>=m_LayerObjectList[selectedLayerIndex].startFrame();i--)
    {
        IsCurVisible=OccludingReasoning(m_LayerObjectList[selectedLayerIndex].contourLabel(i).getPoint(pointIndex));
        if(IsVisible0==false && IsCurVisible==true)
        {
            IsFirstFrameLabeled=true;
            return i;
        }
        if(IsVisible0==true && IsCurVisible==true && flag==1)
        {
            IsFirstFrameLabeled=true;
            return i;
        }
        if(IsVisible0==true && IsCurVisible==false && flag==0)
            flag=1;
        if(m_LayerObjectList[selectedLayerIndex].contourLabel(i).labeled(pointIndex))
        {
            IsFirstFrameLabeled=true;
            return i;
        }
    }
    return i;
}
