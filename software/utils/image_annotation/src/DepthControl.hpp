#ifndef _DepthControl_h
#define _DepthControl_h

#include <QDialog>
#include <QSpinBox>
#include <QSlider>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QList>
#include <QComboBox>
#include <QLabel>

#include "DepthPlot.hpp"
#include "LayerDepth.hpp"

class DepthControl: public QDialog
{
    Q_OBJECT
public:
    DepthControl(QWidget* parent);
    ~DepthControl(void);
    void loadDepthList(const QList<LayerDepth>& depthList,int activeDepthIndex,int frameIndex)
    {
        depthPlot->loadDepthList(depthList,activeDepthIndex,frameIndex);
        //int index=depthList[activeDepthIndex].depth(frameIndex);
        //sliderDepth->setValue(qMin(qMax(index,0),255));
    }
signals:
     void LayerDepthInterpolated(const LayerDepth& layerDepth);
public slots:
    void setLabeledFrame(const QVector<int>& frameIndexList,int frameIndex);
private:
    QSpinBox* spinDepth; // gui for changing the depth
    QSlider* sliderDepth;
    DepthPlot *depthPlot;
    QHBoxLayout* hLayout;
    QVBoxLayout* vLayout;
    QLabel *label1,*label2;
    QComboBox* comboBox;
};

#endif
