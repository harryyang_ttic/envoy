#include "VideoSequence.hpp"

VideoSequence::VideoSequence(void): QObject()
{
    IsVideoLoaded=false;
    curFrameIndex=0;
    numFrames=0;
}

VideoSequence::~VideoSequence(void)
{
}

//-------------------------------------------------------------------------------------------
// the function to copy the data from another instance
//-------------------------------------------------------------------------------------------
void VideoSequence::copyData(const VideoSequence& videoSequence)
{
    framePath=videoSequence.framePath;
    fileList=videoSequence.fileList;
    m_Image=videoSequence.m_Image;
    curFrameIndex=videoSequence.curFrameIndex;
    numFrames=videoSequence.numFrames;
    lcmLogFile=videoSequence.lcmLogFile;
    lcmChannel=videoSequence.lcmChannel;
    frameUtimeList=videoSequence.frameUtimeList;
}

VideoSequence::VideoSequence(const VideoSequence& videoSequence)
{
    copyData(videoSequence);
}

void VideoSequence::clear()
{
    framePath.clear();
    fileList.clear();
    numFrames=0;
    curFrameIndex=0;
    lcmLogFile.clear();
    lcmChannel.clear();    
    frameUtimeList.clear();
    if (lcm_event != NULL)
        lcm_eventlog_free_event (lcm_event);
    if (lcm_eventlog != NULL)
        lcm_eventlog_destroy (lcm_eventlog);
}

void VideoSequence::InitializeLCM(const QString& lcmLog, const QString& channel, const bool verboseOutput)
{
    lcmLogFile = QString(lcmLog);
    lcmChannel = QString(channel);
    verbose = verboseOutput;

    lcm_eventlog_t *lcm_log = lcm_eventlog_create (lcmLogFile.toLatin1(), "r");

    numFrames = 0;
    frameUtimeList.clear();
    int decode_status;
    if (verbose == true) 
        qDebug() << "Counting frames in LCM log file " << lcmLogFile.toLatin1() << "...." << endl;
    for (lcm_eventlog_event_t *event = lcm_eventlog_read_next_event (lcm_log);
         event != NULL;
         event = lcm_eventlog_read_next_event (lcm_log)) {
        if (lcmChannel.compare (event->channel) == 0) {
            bot_core_image_t img;
            decode_status = bot_core_image_t_decode (event->data, 0, event->datalen, &img);
            if (decode_status >= 0) {
                frameUtimeList.append(img.utime);
                numFrames += 1;
            }
            bot_core_image_t_decode_cleanup (&img);
        }

        lcm_eventlog_free_event (event);
    }
    lcm_eventlog_destroy (lcm_log);

    if (verbose == true)
        qDebug() << ".... Found" << numFrames << "total frames with channel name" << lcmChannel << "in LCM log" << endl;

    curFrameIndex=0;
    lcm_eventlog = lcm_eventlog_create (lcmLogFile.toLatin1(), "r");
    if (lcm_eventlog == NULL) {
        qDebug() << "lcm_eventlog for" << lcmLogFile.toLatin1() << "is NULL!" << endl;
        exit(1);
    }

    // Now, go to first image in log
    for (lcm_event = lcm_eventlog_read_next_event (lcm_eventlog);
         lcm_event != NULL;
         lcm_event = lcm_eventlog_read_next_event (lcm_eventlog)) {
        
        if (lcmChannel.compare (lcm_event->channel) == 0) {
            bot_core_image_t img;
            decode_status = bot_core_image_t_decode (lcm_event->data, 0, lcm_event->datalen, &img);
            if (decode_status >= 0) {
                if (frameUtimeList.at(curFrameIndex) == img.utime)
                    break;
            }

            bot_core_image_t_decode_cleanup (&img);
        }

        lcm_eventlog_free_event (lcm_event);
    }

    gotoFrame(curFrameIndex+1);
    IsVideoLoaded=true;
    emit NumFramesChanged(numFrames);
}

void VideoSequence::Initialize(const QString& path,const QFileInfoList& filelist)
{
    framePath=path;
    fileList.clear();
    for(int i=0;i<filelist.size();i++)
        fileList.append(filelist[i].fileName());

    numFrames=fileList.size();
    curFrameIndex=0;
    gotoFrame(curFrameIndex+1);
    IsVideoLoaded=true;
    emit NumFramesChanged(numFrames);
}

//----------------------------------------------------------------------------------------------------------------------
// function to load the current bot_core_image_t frame
//----------------------------------------------------------------------------------------------------------------------
bool VideoSequence::loadFrameLCM()
{
    if(curFrameIndex<0 || curFrameIndex>=numFrames)
        return false;

    // Load the frame from the LCM image data
    bot_core_image_t img;

    int decode_status = bot_core_image_t_decode (lcm_event->data, 0, lcm_event->datalen, &img);
    if (decode_status < 0) {
        qDebug() << "Error decoding bot_core_image_t message!" << endl;
        exit (1);
    }
    
    if (m_Image.loadFromData(img.data, img.size) == false) {
        qDebug() << "Error loading image from LCM message!" << endl;
        bot_core_image_t_decode_cleanup (&img);
        return false;
    }

    bot_core_image_t_decode_cleanup (&img);
    emit ImageChanged(m_Image);
    return true;
}

//----------------------------------------------------------------------------------------------------------------------
// function to load the current frame according to curFrameIndex
//----------------------------------------------------------------------------------------------------------------------
bool VideoSequence::loadFrame()
{
    if(curFrameIndex<0 || curFrameIndex>=numFrames)
        return false;
    if(m_Image.load(framePath+fileList[curFrameIndex])==false)
        return false;

    emit ImageChanged(m_Image);
    return true;
}

//----------------------------------------------------------------------------------------------------------------------
// function to set the control of frame indexing
//----------------------------------------------------------------------------------------------------------------------
void VideoSequence::updateFrameControl()
{
    if(curFrameIndex==0)
        emit FrameControlChanged(false,false,true,true);
    else if(curFrameIndex==numFrames-1)
        emit FrameControlChanged(true,true,false,false);
    else
        emit FrameControlChanged(true,true,true,true);
    emit FrameIndexChanged(curFrameIndex+1);
}

//----------------------------------------------------------------------------------------------------------------------
// Goto frame findex. Note that findex ranges [1,numFrames] instead of C++ way [0,numFrames-1] so
// that other widgets can directly control change of frames. So increase the frame number by 1 before
// calling this function
//----------------------------------------------------------------------------------------------------------------------
void VideoSequence::gotoFrame(int findex)
{
    findex--;
    if(findex<0 || findex>=numFrames)
        return;

    // hack if we're moving back to an earlier frame
    if (findex < curFrameIndex) {
        lcm_eventlog_destroy (lcm_eventlog);
        lcm_eventlog = lcm_eventlog_create (lcmLogFile.toLatin1(), "r");
        if (lcm_eventlog == NULL) {
            qDebug() << "Error opening LCM log during bacwards seek" << endl;
            exit(1);
        }

        lcm_eventlog_free_event (lcm_event);
        lcm_event = lcm_eventlog_read_next_event (lcm_eventlog);
    }

    curFrameIndex=findex;

    // seek to the correct place in the eventlog
    int at_event = 0;
    if (lcmChannel.compare (lcm_event->channel) == 0) {
        bot_core_image_t img;
        int decode_status = bot_core_image_t_decode (lcm_event->data, 0, lcm_event->datalen, &img);
        if (decode_status < 0) {
            qDebug() << "Error decoding bot_core_image_t message!" << endl;
            exit (1);
        }
        if (frameUtimeList.at(curFrameIndex) == img.utime)
            at_event = 1;

        bot_core_image_t_decode_cleanup (&img);
    }
    
    while (at_event == 0) {
        lcm_eventlog_free_event (lcm_event);
        lcm_event = lcm_eventlog_read_next_event (lcm_eventlog);

        if (lcm_event == NULL) {
            qDebug() << "lcm_event is NULL!" << endl;
            exit(1);
        }

        if (lcmChannel.compare (lcm_event->channel) == 0) {
            bot_core_image_t img;
            int decode_status = bot_core_image_t_decode (lcm_event->data, 0, lcm_event->datalen, &img);
            if (decode_status < 0) {
                qDebug() << "Error decoding bot_core_image_t message!" << endl;
                lcm_eventlog_free_event (lcm_event);
                bot_core_image_t_decode_cleanup (&img);
                exit (1);
            }            

            if (frameUtimeList.at(curFrameIndex) == img.utime) {
                at_event = 1;
                bot_core_image_t_decode_cleanup (&img);
                break;
            }
            bot_core_image_t_decode_cleanup (&img);
        }
    }

    
    //loadFrame();
    loadFrameLCM();
    updateFrameControl();
    emit Update();
}

void VideoSequence::gotoFirstFrame()
{
    if(curFrameIndex==0)
        return;
    gotoFrame(1);
}

void VideoSequence::gotoPreFrame()
{
    if(curFrameIndex==0)
        return;
    gotoFrame(curFrameIndex);
}

void VideoSequence::gotoNextFrame()
{
    if(curFrameIndex==numFrames-1)
        return;
    gotoFrame(curFrameIndex+2);
}

void VideoSequence::gotoLastFrame()
{
    if(curFrameIndex==numFrames-1)
        return;
    gotoFrame(numFrames);
}

