#ifndef _XMLWRITER_H
#define _XMLWRITER_H

#include <QIODevice>
#include <QString>
#include <QTextCodec>
#include <QTextStream>
#include <QMap>

class AttrMap : public QMap<QString, QString>
{
public:
    AttrMap() { }
    AttrMap( const QString& name, const QString& value ) {
        insert( name, value );
    }
};

class XmlWriter
{
public:
    XmlWriter( QIODevice *device,
               QTextCodec *codec = 0 );
     XmlWriter();

    void writeRaw( const QString& xml );
    void writeString( const QString& string );
    void writeOpenTag( const QString& name,
                       const AttrMap& attrs = AttrMap() );
    void writeCloseTag( const QString& name );
    void writeAtomTag( const QString& name,
                       const AttrMap& attrs = AttrMap() );
    void writeTaggedString( const QString& name,
                            const QString& string,
                            const AttrMap& attrs = AttrMap() );
    void newLine();
    void setIndentSize( int size ) { indentSize = size; }
    void setAutoNewLine( bool on ) { autoNewLine = on; }
    void writePt(QPointF& point);
    void writePtList(QList<QPointF>& pointList,QString username="anonymous");
    void writeDate();

private:
    QString protect( const QString& string );
    QString opening( const QString& tag,  const AttrMap& attrs = AttrMap());
    void writePendingIndent();

    QTextStream out;
    QString indentStr;
    int indentSize;
    bool autoNewLine;
    bool atBeginningOfLine;
};


void writeProperty( XmlWriter& xw, const QString& name,
                    const QString& type,
                    const QString& value );

#endif