#include <QtGui>
#include "DepthPlot.hpp"

DepthPlot::DepthPlot(QWidget* parent,const QSize& widgetSize): QWidget(parent)
{
    setFixedSize(widgetSize);
    WidgetSize=widgetSize;
    Offset=QPoint(30,20);
    PlotSize.setWidth(WidgetSize.width()-Offset.x()*1.5);
    PlotSize.setHeight(WidgetSize.height()-Offset.y()*2);
    activeDepthIndex=0;
}

DepthPlot::~DepthPlot(void)
{
}

void DepthPlot::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    

    // draw background
    QBrush brush(QColor(128,128,128));
    painter.fillRect(Offset.x(),Offset.y(),PlotSize.width()-1,PlotSize.height()-1,brush);

    // plot the grid
    QPen pen;
    pen.setColor(Qt::black);
    painter.setPen(pen);
    painter.drawRect(Offset.x()-1,Offset.y()-1,PlotSize.width(),PlotSize.height());

    if(m_DepthList.size()==0)
        return;

    QFont font;
    font.setPixelSize(11);
    painter.setFont(font);

    // plot the x-label
    int foo=m_DepthList[0].numFrames()-1;
    plotXLabel(painter,0);
    plotXLabel(painter,foo*0.25);
    plotXLabel(painter,foo*0.75);
    plotXLabel(painter,foo*0.5);
    plotXLabel(painter,foo);
    bool IsClose=false;
    for(int i=0;i<5;i++)
        if(abs(frameIndex-foo*i/4)<4)
        {
            IsClose=true;
            break;
        }
    if(IsClose==false)
        plotXLabel(painter,frameIndex);

    // plot the y-label
    plotYLabel(painter,0);
    plotYLabel(painter,63);
    plotYLabel(painter,127);
    plotYLabel(painter,191);
    plotYLabel(painter,255);

    pen.setColor(Qt::lightGray);
    painter.setPen(pen);
    painter.drawLine(Offset.x()+frameIndex*x_StepSize,Offset.y()+PlotSize.height()-1,Offset.x()+frameIndex*x_StepSize,Offset.y());

    // plot the curve
    painter.setRenderHint(QPainter::Antialiasing);
    for(int i=0;i<m_DepthList.size();i++)
        if(i!=activeDepthIndex)
            m_DepthList[i].DrawCurve(painter,Offset,PlotSize.height(),MinDepth,x_StepSize,y_StepSize,frameIndex);
    if(m_DepthList.size()>0)
        m_DepthList[activeDepthIndex].DrawCurve(painter,Offset,PlotSize.height(),MinDepth,x_StepSize,y_StepSize,frameIndex,true);
}

void DepthPlot::plotYLabel(QPainter& painter,int y)
{
    painter.drawLine(Offset.x(),Offset.y()+PlotSize.height()-1-(y-MinDepth)*y_StepSize,Offset.x()+7,Offset.y()+PlotSize.height()-1-(y-MinDepth)*y_StepSize);

    int width=40,height=11;
    QRect rect(Offset.x()-width-3,Offset.y()+PlotSize.height()-2-(y-MinDepth)*y_StepSize-height/2,width,height);
    painter.drawText(rect,Qt::AlignRight,QString::number(y));
}

void DepthPlot::plotXLabel(QPainter& painter,int x)
{
    int width=40,height=11;
    QRect rect(x*x_StepSize+Offset.x()-width/2,Offset.y()+PlotSize.height()+2,width,height);
    painter.drawText(rect,Qt::AlignCenter,QString::number(x+1));
}

void DepthPlot::loadDepthList(const QList<LayerDepth> &depthList,int active_depthindex,int frame_index)
{
    m_DepthList=depthList;
    activeDepthIndex=active_depthindex;
    frameIndex=frame_index;
    orgFrameIndex=frameIndex;
    m_DepthList[activeDepthIndex].insertIndex(frameIndex);

    // it's important that allthe LayerDepth objects have the same number of frames
    computeStepSize();
    int index=m_DepthList[activeDepthIndex].depth(frameIndex);
    emit changeDepth(qMin(qMax(index,0),255));
    emit changeLabeledFrames(m_DepthList[activeDepthIndex].frameIndexList(),frameIndex);
    update();
}

void DepthPlot::computeStepSize()
{
    MaxDepth=m_DepthList[0].max();
    MinDepth=m_DepthList[0].min();

    for(int i=1;i<m_DepthList.size();i++)
    {
        MaxDepth=qMax(MaxDepth,m_DepthList[i].max());
        MinDepth=qMin(MinDepth,m_DepthList[i].min());
    }
    if(MinDepth>0)
        MinDepth=0;
    if(MaxDepth<255)
        MaxDepth=255;

    double range=MaxDepth-MinDepth;
    MinDepth-=range*0.07;
    MaxDepth+=range*0.07;

    y_StepSize=(double)PlotSize.height()/(MaxDepth-MinDepth);
    x_StepSize=(double)PlotSize.width()/(m_DepthList[0].numFrames()-1);
}

void DepthPlot::interpolateDepth(int depth)
{
    if(m_DepthList.size()==0)
        return;
    m_DepthList[activeDepthIndex].setDepth(frameIndex,depth);
    int count=m_DepthList[activeDepthIndex].InterpolateLinear(frameIndex);
    if(m_DepthList[activeDepthIndex].numControlPoints()==1 || m_DepthList[activeDepthIndex].numToInterpolate()<5)
    {
        computeStepSize();
        update();
        emit LayerDepthInterpolated(m_DepthList[activeDepthIndex]);
        return;
    }
    m_DepthList[activeDepthIndex].InterpolateCG(m_DepthList[activeDepthIndex].numFrames()+count);
    computeStepSize();
    update();
    // send signal to higher level to update drawing the contour label
    emit LayerDepthInterpolated(m_DepthList[activeDepthIndex]);
}

void DepthPlot::setFrameIndex(int findex)
{
    frameIndex=m_DepthList[activeDepthIndex].getFrameIndex(findex);
    int index=m_DepthList[activeDepthIndex].depth(frameIndex);
    emit changeDepth(qMin(qMax(index,0),255));
    update();
}
