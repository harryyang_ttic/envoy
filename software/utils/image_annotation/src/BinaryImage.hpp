#ifndef _BinaryImage_h
#define _BinaryImage_h

#include <QString>
#include <QList>
#include <QPointF>
#include "Image.hpp"

class BinaryImage
{
private:
    int imWidth,imHeight,nPixels;
    int VirtualWidth;
    unsigned char* pData;
public:
    BinaryImage(void);
    BinaryImage(int width,int height);
    ~BinaryImage(void);
    BinaryImage(const BinaryImage& other);
    void copyData(const BinaryImage& other);
    BinaryImage& operator=(const BinaryImage& other);
    void clear();
    int computeVirtualWidth();
    bool imwrite(const QString& filename);
    void setAllOnes();
    void setAllZeros();

    // interface of access the pixel value
    bool getPixel(unsigned x,unsigned y);
    void setPixel(unsigned x,unsigned y,bool value=true);
    
    // function to dilate the mask
    void imdilate8();

    // generate a mask of pixels that lie inside a polygon
    void inpolygon(const QList<QPointF>& polygon);
    void inpolygon(int nPoints,const double* pX,const double* pY);

    // the following function is copied from http://alienryderflex.com/polygon/
    static bool pointInPolygon(double x,double y,int nPoints,const double* pX,const double *pY)
    {
        int i,j=nPoints-1;
        bool oddNodes=false;
        for(i=0;i<nPoints;i++)
        {
            if (pY[i]<y && pY[j]>=y || pY[j]<y && pY[i]>=y)
                if (pX[i]+(y-pY[i])/(pY[j]-pY[i])*(pX[j]-pX[i])<x)
                    oddNodes=!oddNodes;
            j=i;
        }
        return oddNodes;
    }

    // function to convert BinaryImage to Image
    template <class T>
    void export2Image(Image<T>& image);
};

template <class T>
void BinaryImage::export2Image(Image<T>& image)
{
    if(image.width()!=imWidth || image.height()!=imHeight || image.nchannels()!=1)
        image.allocate(imWidth,imHeight);
    else
        image.reset();
    T ImMax;
    if(image.IsFloat())
        ImMax=1;
    else
        ImMax=255;

    unsigned byte_index,byte_offset,offset;
    unsigned char one;
    unsigned char bytevalue;

    T* pImageData=image.data();

    for(int i=0;i<imHeight;i++)
        for(int j=0;j<imWidth;j+=8)
        {
                byte_index=j/8;
                offset=i*VirtualWidth+byte_index;
                bytevalue=pData[offset];
                // if all zero, continue
                if(bytevalue==0)
                    continue;
                // if all one
                if(bytevalue==255)
                {
                    for(int k=0;k<8;k++)
                    {
                        if(k+j>=imWidth)
                            break;
                        pImageData[i*imWidth+j+k]=ImMax;
                    }
                    continue;
                }
                one=128;
                for(int k=0;k<8;k++)
                {
                    if(k+j>=imWidth)
                        break;
                    if((bytevalue & one) ==one)
                        pImageData[i*imWidth+j+k]=ImMax;
                    one = one >>1;
                }
        }
}

#endif
