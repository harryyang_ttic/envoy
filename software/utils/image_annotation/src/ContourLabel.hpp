#ifndef _ContourLabel_h
#define _ContourLabel_h

#include <math.h>
#include <QObject>
#include <QPointF>
#include <QList>
#include <QRectF>
#include <QPainter>

class ContourLabel: QObject
{
    Q_OBJECT
    // definition of the member variables
private:
    QList<QPointF> m_PointList; // the list of the points
    QList<bool> m_LabelList; // the flag of whether the point is manually labelled or compute generated
    QRectF m_BoundingRect; // the bounding box of the contour
    QPointF m_Centroid; // the centroid of the contour

    // member variable to indicate whether the contour is indeed a boundingbox
    bool IsPolygon;
    QRectF m_InnerRect;

    static int imWidth,imHeight; // the dimension of the image
public:
    static void setImWidth(int width){imWidth=width;};
    static void setImHeight(int height){imHeight=height;};
public:
    ContourLabel(void);
    ContourLabel(const ContourLabel& contourLabel);
    ~ContourLabel(void);
    ContourLabel& operator =(const ContourLabel& contourLabel);
public: //public functions
    void DrawContour(QPainter& painter,QPointF ScreenOffset,double ScaleFactor,int depth,bool IsActive=false,bool IsCreating=false,bool IsCursorOver=false,bool IsGlobal=false);
    void ShowText(QPainter& painter,QPointF ScreenOffset,double ScaleFactor,const QString& objname=QString());

    double distanceToContour(const QPointF& point);
    void addPoint(const QPointF& point,const bool isLabeled=true);
    inline void updateLastPoint(const QPointF& point,bool isLabeled=true){m_PointList.last()=point;m_LabelList.last()=isLabeled;};
    inline void updatePoint(const QPointF& point,int index,bool IsLabeled=true){m_PointList[index]=point;m_LabelList[index]=IsLabeled;};
    void computeBoundingBox(double scaleFactor=1);
    void computeCentroid();
    void printContour();
    inline int numPoints(){return m_PointList.size();};
    inline void removeLastPoint(){m_PointList.removeLast();m_LabelList.removeLast();};
    void removePoint(int pointIndex);
    void removeLabel(int pointIndex) {m_LabelList[pointIndex]=false;};
    double minPointDistance(const QPointF& point,int& pointIndex);
    double minLineSegDistance(const QPointF& point,int& pointIndex,double tolerance=0);
    void insertPoint(const QPointF& point,int index,bool IsLabeled=true);
    bool IsCentroidSelected(const QPointF& point,double ScaleFactor=1);
    void shiftContour(const QPointF& shift);
    void shiftContour(ContourLabel& org,const QPointF& shift);
    QPointF& getPoint(int index){return m_PointList[index];};
    int selectBoundingBox(const QPointF& point,double ScaleFactor=1,double threshold=6);
    static double distPointToLineSeg(const QPointF& P1,const QPointF& P2,const QPointF& point,double tolerance=0);
    void scaleContour(ContourLabel& org,const QPointF &shift,int selectedBoundingBoxCorner);
    void scaleContour(double ratio);
    void scaleBoundingBox(double ratio,const ContourLabel& contour1,const ContourLabel& contour2);
    double scale() const;

    // function to access member variables
    inline int size() const{return m_PointList.size();};
    inline double x(int i=0) const {return m_PointList[i].x();};
    inline double y(int i=0) const {return m_PointList[i].y();};
    inline bool labeled(int i=0) const {return m_LabelList[i];};
    inline QPointF& centroid() {return m_Centroid;};
    inline const QList<QPointF>& PointList() const {return (const QList<QPointF>&)m_PointList;};
    inline void setIsPolygon(bool isPolygon=true){IsPolygon=isPolygon;};
    inline bool isPolygon()const {return IsPolygon;};
    inline void clearLabels(){for(int i=0;i<m_LabelList.size();i++) m_LabelList[i]=false;};
    void setUniformLabels(bool value=true) {for(int i=0;i<m_LabelList.size();i++) m_LabelList[i]=value;};
    const QRectF& boundingBox() const {return m_BoundingRect;};
    void setBoundingBox(const QRectF& boundingBox) {m_BoundingRect=boundingBox;m_Centroid=m_BoundingRect.center();};
    bool IsKeyFrame();

    // function for interpolation
    void computeTwoPointsCoeff(int pointIndex,double& projection,double& extension);
    void insertPointTwoPointsCoeff(int pointIndex,double projection,double extension);
    void updatePointTwoPointsCoeff(int pointIndex,double projection0,double extension0,double projection1,double extension1,double ratio);

    // function for loading the contour points and set the label to be false
    void loadPoints(const QList<QPointF>& contour);

    // function to check whether a point is inside image boundary
    bool IsInsideImage(int pointIndex);

    // function to fit an affine motion to interpolate the specified point
    void AffineFit(int pointIndex,ContourLabel& other,QPointF& result);
private:
    static inline double pabs(const QPointF& point){return sqrt(point.x()*point.x()+point.y()*point.y());};
};

#endif
