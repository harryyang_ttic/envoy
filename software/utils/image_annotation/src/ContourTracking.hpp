#ifndef _CONTOURTRACKING_H
#define _CONTOURTRACKING_H

#include <QList>
#include <QVector>
#include "ImPyramid.hpp"
#include "Image.hpp"
#include "PyrdPatchSet.hpp"

// class to handle contour tracking

class ContourTracking
{
public:
    ContourTracking();
    ~ContourTracking();
    static double PointNorm(const QPointF& P);
    static QPointF CreateCroppedPyramid(ImPyramid& pyrd,const BiImage& frame,const QList<QPointF>& contour,int nLevels=5,int ExtraMargin=0);
    static void SubtractOffset(QList<QPointF>& contour,const QPointF& offset);
    static void AddOffset(QList<QPointF>& contour,const QPointF& offset);
    static void DisplayContour(const QString& filename, const BiImage& frame,const QList<QPointF>& contour);
    static void TrackOneFrameCropped(QList<QPointF>& trackedContour,PyrdPatchSet& pyrdPatchSet,const QList<QPointF>& curContour,const QList<QPointF>& prediction,
                                                                            const QList<bool>& IsVisibleList,const PyrdPatchSet& pyradPatchSet,const BiImage& nextFrame,double alpha=0.01,int nIterations=4,int ExtraMargin=0);

    static void TrackOneFrame(QList<QPointF>& trackedContour,const QList<QPointF>& curContour,const QList<QPointF>& prediction,const QList<bool>& IsVisibleList,
                                                         const PyrdPatchSet& pyrPatchSet, const ImPyramid& pyrdNextFrame,double alpha=0.01,int nIterations=5);
    
    static void MatchContour(const QList<QPointF>& fContour,QList<QPointF>& flow,const PatchSet& patchSet,
                                                       const ImageType& im,const ImageType& imdx,const ImageType& imdy,
                                                       const QVector<double>& h,const QList<bool>& IsVisible,int nIterations);

    template <class T>
    static double WeightedSum(const Image<T>& Image1,const Image<T>& Image2,const DImage& weightImage);
    static void Laplacian(int nPoints,double *W,const double* pU,const double* pH);

    // function to reason the occlusion
    static void OcclusionReasoning(QList<bool>& IsVisibleList,const QList<QPointF>& contour,const QVector<QList<QPointF> >& FrontalContours,int imWidth,int imHeight,int winsize=7);

    // functions to track a single point
    static QPointF TrackPointOneFrameCropped(PyrdPatchSet& pyrdPatchSet2,const QPointF &origin, const QPointF &reference, int pointIndex,const QList<QPointF>& refcontour,
                                                                                            const PyrdPatchSet &pyrdPatchSet1, const BiImage &nextFrame, double alpha, int nIterations, int ExtraMargin);
    static QPointF TrackPointOneFrame(const QPointF& origin,const QPointF& reference,const PyrdPatchSet& pyrPatchSet,
                                                                         const ImPyramid& pyrdNextFrame,double alpha,int nIterations=5);
    static void MatchPoint(QPointF& flow,const QPointF& origin,const QPointF& flow0,const PatchSet& patchSet,const ImageType& im,const ImageType& imdx,const ImageType& imdy,
                                                      double alpha,int nIterations);
    static QPointF SolveLinearSystem2D(double* A,double* b);
};

//----------------------------------------------------------------------------------------------------------------
// function to compute the weighted sum, equivalent to MATLAB code
//         foo=sum(Image1.*Image2).*weightImage;
//        return sum(foo(:));
//----------------------------------------------------------------------------------------------------------------
template <class T>
double ContourTracking::WeightedSum(const Image<T> &Image1, const Image<T> &Image2, const DImage &weightImage)
{
    int imWidth=Image1.width(),imHeight=Image1.height(),nChannels=Image1.nchannels();
    int offset,noffset;
    double result=0,temp=0;
    const T*& pData1=Image1.data();
    const T*& pData2=Image2.data();
    const double*& pWeight=weightImage.data();

    for(int i=0;i<imHeight;i++)
        for(int j=0;j<imWidth;j++)
        {
            offset=i*imWidth+j;
            noffset=offset*nChannels;
            temp=0;
            for(int k=0;k<nChannels;k++)
                temp+=pData1[noffset+k]*pData2[noffset+k];
            result+=(double)temp/nChannels*pWeight[offset];
        }
    return result;
}


#endif // _CONTOURTRACKING_H
