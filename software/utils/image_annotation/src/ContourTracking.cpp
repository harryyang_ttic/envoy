#include <QImage>
#include <QPainter>
#include <math.h>
#include <iostream>
#include "ContourTracking.hpp"
#include "BinaryImage.hpp"
#include "ImageProcessing.hpp"
#include "MyPreference.hpp"

using namespace std;

ContourTracking::ContourTracking()
{
}

ContourTracking::~ContourTracking()
{
}

double ContourTracking::PointNorm(const QPointF &P)
{
    return sqrt(P.x()*P.x()+P.y()*P.y());
}

//----------------------------------------------------------------------------------------------------------------
// function to create a pyramid from an input image and return the offset of the pyramid
QPointF ContourTracking::CreateCroppedPyramid(ImPyramid &pyrd, const BiImage &frame, const QList<QPointF> &contour, int nLevels,int ExtraMargin)
{
    // compute the bounding box of the contour
    unsigned one=1;
    int MarginSize=(one<<(nLevels-1))+4;
    MarginSize=__min(MarginSize,40);
    MarginSize+=ExtraMargin;
    int x,y,Left,Top,Right,Bottom;
    
    Left=Right=contour[0].x();
    Top=Bottom=contour[0].y();

    for(int i=1;i<contour.size();i++)
    {
        x=contour[i].x();
        y=contour[i].y();
        Left=__min(Left,x);
        Right=__max(Right,x);
        Top=__min(Top,y);
        Bottom=__max(Bottom,y);
    }
    
    Left-=MarginSize;
    Right+=MarginSize;
    Top-=MarginSize;
    Bottom+=MarginSize;

    Left=__max(Left,0);
    Right=__min(Right,frame.width()-1);
    Top=__max(Top,0);
    Bottom=__min(Bottom,frame.height()-1);

    // if nothing cropped out
    if(Left==0 && Right==frame.width()-1 && Top==0 && Bottom==frame.height()-1)
        pyrd.CreatePyramid(frame,nLevels);
    else
    {
        BiImage croppedImage;
        frame.crop(croppedImage,Left,Top,Right-Left+1,Bottom-Top+1);
        pyrd.CreatePyramid(croppedImage,nLevels);
    }
    return QPointF(Left,Top);
}

void ContourTracking::SubtractOffset(QList<QPointF>& contour,const QPointF& offset)
{
    for(int i=0;i<contour.size();i++)
        contour[i]-=offset;
}

void ContourTracking::AddOffset(QList<QPointF>& contour,const QPointF& offset)
{
    for(int i=0;i<contour.size();i++)
        contour[i]+=offset;
}

void ContourTracking::DisplayContour(const QString& filename,const BiImage& frame,const QList<QPointF>& contour)
{
    unsigned char* pTempBuffer;
    int nPixels=frame.npixels();
    pTempBuffer=new unsigned char[nPixels*4];
    const unsigned char* pData=frame.data();
    for(int i=0;i<nPixels;i++)
        for(int j=0;j<3;j++)
            pTempBuffer[i*4+j]=pData[i*3+j];
    QImage *pQImage=new QImage(pTempBuffer,frame.width(),frame.height(),QImage::Format_RGB32);
    QPainter painter(pQImage);
    QPen pen;
    pen.setColor(Qt::red);
    pen.setWidthF(2);
    painter.setPen(pen);
    painter.setRenderHint(QPainter::Antialiasing);
    
    for(int i=0;i<contour.size();i++)
        painter.drawLine(contour[i],contour[(i+1)%contour.size()]);

    pQImage->save(filename,0,100);

    delete pQImage;
    delete pTempBuffer;
    painter.end();
}

//----------------------------------------------------------------------------------------------------------------
// function to perform tracking and generating pyramid patch set
// this function also use cropped image to generate image pyramid, more efficient!
// ExtraMargin is set to be some number when the first time this function is called for tracking
//----------------------------------------------------------------------------------------------------------------
void ContourTracking::TrackOneFrameCropped(QList<QPointF> &trackedContour, PyrdPatchSet &pyrdPatchSet2, const QList<QPointF> &curContour, const QList<QPointF> &prediction, 
                                                                                                const QList<bool> &IsVisibleList, const PyrdPatchSet &pyrdPatchSet1, const BiImage &nextFrame, double alpha, int nIterations,int ExtraMargin)
{
    QPointF offset;
    ImPyramid pyrdNextFrame;
    offset=CreateCroppedPyramid(pyrdNextFrame,nextFrame,prediction,pyrdPatchSet1.nlevels(),ExtraMargin);

    QList<QPointF> CurContour(curContour);
    QList<QPointF> Prediction(prediction);
    
    // transform to the new coordinate
    SubtractOffset(CurContour,offset);
    SubtractOffset(Prediction,offset);

    TrackOneFrame(trackedContour,CurContour,Prediction,IsVisibleList,pyrdPatchSet1,pyrdNextFrame,alpha,nIterations);
    pyrdPatchSet2.CreatePyrdPatchSet(pyrdNextFrame,trackedContour);

    // transform back to the original image coordinate
    AddOffset(trackedContour,offset);
}

//----------------------------------------------------------------------------------------------------------------
// function to perform one frame tracking of the contour
// In general, this function performs coarse to fine 
// Output:
//        trackedContour-- the list of the contour points at frame t+1
// Input:
//        curContour--         the list of the contour points at frame t
//        prediction--             the prediction of the contour at frame t+1, used for initialization
//        pyrPatchSet--         the pyramid patch set, extracted from frame t
//        pyrNextFrame--  the image pyramid at frame t+1
//        alpha--                     the regularization parameters
//        nIterations--             the number of iterations at each pyramid level
//----------------------------------------------------------------------------------------------------------------
void ContourTracking::TrackOneFrame(QList<QPointF>& trackedContour,const QList<QPointF>& curContour,const QList<QPointF>& prediction,const QList<bool>& IsVisibleList,
                                                                              const PyrdPatchSet& pyrdPatchSet, const ImPyramid& pyrdNextFrame,double alpha,int nIterations)
{
    int nLevels=pyrdPatchSet.nlevels();
    if(pyrdNextFrame.nlevels()!=nLevels)
    {
        cout<<"The image pyramid and contour patch pyramid don't match in number of levels!"<<endl;
        return;
    }
    int nPoints=curContour.size();
    if(prediction.size()!=nPoints)
    {
        cout<<"The contours have different dimensions!"<<endl;
        return;
    }

    QList<QPointF> flow(prediction);
    QList<QPointF> fContour(flow);

    QVector<double> pointDistance(nPoints,0);
    QVector<double> h(nPoints,0);
    double temp,meanDistance;
    
    // compute from coarse to fine
    for(int k=nLevels-1;k>=0;k--)
    {
        if(k==0)
            temp=1;
        else
            temp=pow(0.5,k);

        // initialize or propagate the flow
        if(k==nLevels-1)
            for(int i=0;i<nPoints;i++)
                flow[i]=(prediction[i]-curContour[i])*temp;
        else
            for(int i=0;i<nPoints;i++)
                flow[i]*=2;

        // compute fContour
        for(int i=0;i<nPoints;i++)
            fContour[i]=curContour[i]*temp;

        // compute h, the weight on the difference between neighboring nodes
        // in general, the closer the nodes, the bigger the weight; the further the nodes, the smaller the weight
        // this weight also contains the regularizer alpha
        
        // compute distance and the mean
        meanDistance=0;
        for(int i=0;i<nPoints;i++)
        {
            pointDistance[i]=PointNorm(fContour[i]-fContour[(i+1)%nPoints]);
            meanDistance+=pointDistance[i];
        }
        meanDistance/=nPoints;
            
        // compute h
        for(int i=0;i<nPoints;i++)
            h[i]=alpha/temp*meanDistance/(meanDistance+pointDistance[i]);

        // display fContour
        //cout<<"fContour"<<endl;
        //for(int i=0;i<nPoints;i++)
        //    cout<<fContour[i].x()<<" "<<fContour[i].y()<<endl;

        // call matching function at a particular level
        MatchContour(fContour,flow,pyrdPatchSet.getPatchSet(k),pyrdNextFrame.Image(k),pyrdNextFrame.dX(k),pyrdNextFrame.dY(k),h,IsVisibleList,nIterations);
    }
    // form the contour
    for(int i=0;i<nPoints;i++)
        trackedContour[i]=curContour[i]+flow[i];
}

//----------------------------------------------------------------------------------------------------------------
// function that performs contour matching at a particular pyramid level
//----------------------------------------------------------------------------------------------------------------
void ContourTracking::MatchContour(const QList<QPointF>& fContour,QList<QPointF>& flow,const PatchSet& patchSet,
                                                                          const ImageType& im,const ImageType& imdx,const ImageType& imdy,
                                                                          const QVector<double>& h,const QList<bool>& IsVisibleList,int nIterations)
{
    int nPoints,winlength,winsize,imHeight,imWidth,Dim;
    nPoints=fContour.size();
    Dim=nPoints*2;
    winlength=patchSet.Patch(0).width();
    winsize=(winlength-1)/2;
    imWidth=im.width();
    imHeight=im.height();

    QList<QPointF> nextContour(fContour);
    ImageType patch(winlength,winlength,im.nchannels()),patchDt(winlength,winlength,im.nchannels());
    ImageType patchDx(winlength,winlength,im.nchannels()),patchDy(winlength,winlength,im.nchannels());
    double *Psi_xx,*Psi_xy,*Psi_yy,*Psi_xt,*Psi_yt;
    double *pH;

    Psi_xx=new double[nPoints];
    Psi_xy=new double[nPoints];
    Psi_yy=new double[nPoints];
    Psi_xt=new double[nPoints];
    Psi_yt=new double[nPoints];
    pH=new double[nPoints];

    double *dW,*W,*r,*rou,*p,*q,*foo,temp,beta;
    
    dW=new double[nPoints*2];
    W=new double[nPoints*2];
    r=new double[nPoints*2];
    p=new double[nPoints*2];
    q=new double[nPoints*2];
    foo=new double[nPoints*2];
    

    int CGnIterations=nPoints*3;
    rou=new double[CGnIterations];

    for(int i=0;i<nPoints;i++)
    {
        pH[i]=h[i];
        W[i*2]=flow[i].x();
        W[i*2+1]=flow[i].y();
    }

    // iteratively update the flow
    for(int count=0;count<nIterations;count++)
    {
        // set the data term to zero
        memset(Psi_xx,0,sizeof(double)*nPoints);
        memset(Psi_xy,0,sizeof(double)*nPoints);
        memset(Psi_yy,0,sizeof(double)*nPoints);
        memset(Psi_xt,0,sizeof(double)*nPoints);
        memset(Psi_yt,0,sizeof(double)*nPoints);
    
        // compute the data term parameters
        for(int i=0;i<nPoints;i++)
        {
            nextContour[i]=fContour[i]+QPointF(W[i*2],W[i*2+1]);
            if(nextContour[i].x()<0 || nextContour[i].x()>imWidth-1 || nextContour[i].y()<0 || nextContour[i].y()>imHeight-1)
                continue;
            if(IsVisibleList[i]==false)
                continue;
            // the weight has been normalized during the generation process; no need of normalization here

            // sample patches from frame t+1
            im.getPatch(patch,nextContour[i].x(),nextContour[i].y(),winsize);
            imdx.getPatch(patchDx,nextContour[i].x(),nextContour[i].y(),winsize);
            imdy.getPatch(patchDy,nextContour[i].x(),nextContour[i].y(),winsize);

            // compute temporal derivative. the following line is equivalent to
            // patchDt=patch-patchSet.Image(i)
            patchDt.Subtract(patch,patchSet.Patch(i));

            // compute Psi
            Psi_xx[i]=WeightedSum(patchDx,patchDx,patchSet.Weight(i));
            Psi_xy[i]=WeightedSum(patchDx,patchDy,patchSet.Weight(i));
            Psi_yy[i]=WeightedSum(patchDy,patchDy,patchSet.Weight(i));
            Psi_xt[i]=WeightedSum(patchDx,patchDt,patchSet.Weight(i));
            Psi_yt[i]=WeightedSum(patchDy,patchDt,patchSet.Weight(i));
        }

        // prepare for solving the linear system
        memset(dW,0,sizeof(double)*Dim);
        // compute the residual
        Laplacian(nPoints,foo,W,pH);
        for(int i=0;i<nPoints;i++)
        {
            r[i*2]=-Psi_xt[i]-foo[i*2];
            r[i*2+1]=-Psi_yt[i]-foo[i*2+1];
        }

        //-----------------------------------------------------------------------------------------
        // conjugate gradient method to solve dW: A*dW=r
        for(int k=0;k<CGnIterations;k++)
        {
            temp=0;
            for(int i=0;i<Dim;i++)
                temp+=r[i]*r[i];
            rou[k]=temp;
            if(temp<1E-20)
                break;
            
            if(k==0)
                memcpy(p,r,sizeof(double)*Dim);
            else
            {
                double tempRatio=rou[k]/rou[k-1];
                for(int i=0;i<Dim;i++)
                    p[i]=r[i]+tempRatio*p[i];
            }
            // go through the linear system q=Ap
            Laplacian(nPoints,foo,p,pH);
            for(int i=0;i<nPoints;i++)
            {
                q[i*2]=Psi_xx[i]*p[i*2]+Psi_xy[i]*p[i*2+1]+foo[i*2];
                q[i*2+1]=Psi_xy[i]*p[i*2]+Psi_yy[i]*p[i*2+1]+foo[i*2+1];
            }
            // compute the inner product of p and q
            temp=0;
            for(int i=0;i<Dim;i++)
                temp+=p[i]*q[i];
            beta=rou[k]/temp;
            // update dW and r
            for(int i=0;i<Dim;i++)
            {
                dW[i]=dW[i]+beta*p[i];
                r[i]=r[i]-beta*q[i];
            }
        }
        // end of conjugate gradient
        //-----------------------------------------------------------------------------------------
        // output the norm of dW
        temp=0;
        for(int i=0;i<Dim;i++)
            temp+=dW[i]*dW[i];
        //cout<<"dW:"<<temp<<endl;

        for(int i=0;i<Dim;i++)
            W[i]+=dW[i];
        if(temp<1E-6)
            break;
    }
    //cout<<endl;
    // assign the output
    for(int i=0;i<nPoints;i++)
    {
        flow[i].setX(W[i*2]);
        flow[i].setY(W[i*2+1]);
    }
    // release the allocated buffers
    delete Psi_xx;
    delete Psi_xy;
    delete Psi_yy;
    delete Psi_xt;
    delete Psi_yt;
    delete pH;

    delete W;
    delete dW;
    delete r;
    delete p;
    delete q;
    delete rou;
    delete foo;
}


//----------------------------------------------------------------------------------------------------------------
// function to perform generalized Laplacian operator on pU given weight pH
// the result is stored in pW
//----------------------------------------------------------------------------------------------------------------
void ContourTracking::Laplacian(int nPoints, double *pW, const double *pU, const double *pH)
{
    double *pTemp;
    pTemp=new double[nPoints*2];
    for(int i=0;i<nPoints;i++)
    {
        if(i<nPoints-1)
        {
            pTemp[i*2]=(pU[i*2]-pU[(i+1)*2])*pH[i];
            pTemp[i*2+1]=(pU[i*2+1]-pU[(i+1)*2+1])*pH[i];
        }
        else
        {
            pTemp[i*2]=(pU[i*2]-pU[0])*pH[i];
            pTemp[i*2+1]=(pU[i*2+1]-pU[1])*pH[i];
        }
    }
    for(int i=0;i<nPoints;i++)
    {
        if(i>0)
        {
            pW[i*2]=pTemp[i*2]-pTemp[(i-1)*2];
            pW[i*2+1]=pTemp[i*2+1]-pTemp[(i-1)*2+1];
        }
        else
        {
            pW[i*2]=pTemp[i*2]-pTemp[(nPoints-1)*2];
            pW[i*2+1]=pTemp[i*2+1]-pTemp[(nPoints-1)*2+1];
        }
    }
    delete pTemp;
}

//---------------------------------------------------------------------------------------------------------------
// function to compute whether a landmark on a contour is occluded or visible
//---------------------------------------------------------------------------------------------------------------
void ContourTracking::OcclusionReasoning(QList<bool> &IsVisibleList, const QList<QPointF> &contour, const QVector<QList<QPointF> >& FrontalContours,int imWidth, int imHeight,int winsize)
{
    int winlength=winsize*2+1;
    int nPoints=contour.size();

    // if IsVisibleList has not been built, build it with the default value
    if(IsVisibleList.size()!=contour.size())
    {
        IsVisibleList.clear();
        for(int k=0;k<nPoints;k++)
            IsVisibleList.append(true);
    }
    double *pX0,*pX,*pY0,*pY;
    pX0=new double[nPoints];
    pX=new double[nPoints];
    pY0=new double[nPoints];
    pY=new double[nPoints];

    for(int j=0;j<nPoints;j++)
    {
        pX0[j]=contour[j].x();
        pY0[j]=contour[j].y();
    }
    double *pGaussian2D;
    pGaussian2D=new double[winlength*winlength];
    ImageProcessing::generate2DGaussian(pGaussian2D,winsize,(double)winsize*4/5);
    BinaryImage mImage(winlength,winlength);
    DImage tempImage,Weight;
    double *pData;
    int offset;
    double x,y,sum0,sum1;

    // loop over the points
    for(int k=0;k<nPoints;k++)
    {
        // step 1. if the point is alreayd invisible, continue
        if(IsVisibleList[k]==false)
            continue;

        // step 2. if the point lies outside the image boundary, continue
        if(pX0[k]<0 || pX0[k]>=imWidth || pY0[k]<0 || pY0[k]>=imHeight)
        {
            IsVisibleList[k]=false;
            continue;
        }
        // if there is no occluding contour, continue
        if(FrontalContours.size()==0)
            continue;

        // step 3. compute the local mask of the patch
        // transfer the contour so that  i-th point has the coordinate (winsize,winsize)
        for(int j=0;j<nPoints;j++)
        {
            pX[j]=pX0[j]-pX0[k]+winsize;
            pY[j]=pY0[j]-pY0[k]+winsize;
        }
        // generate the mask
        mImage.inpolygon(nPoints,pX,pY);
        // dilate the binary image
        mImage.imdilate8();
        // export to a temp image
        mImage.export2Image(tempImage);
        // smoothing
        tempImage.smoothing(Weight,3);

        // consider the boundary effect of the image and add Gaussian weight
        // and also normalize the weight
        pData=Weight.data();
        sum0=0;
        for(int i=-winsize;i<=winsize;i++)
            for(int j=-winsize;j<=winsize;j++)
            {
                x=j+pX0[k];
                y=i+pY0[k];
                offset=(i+winsize)*winlength+j+winsize;
                if(x<0 || x>imWidth-1 || y<0 || y>imHeight-1)
                {
                    pData[offset]=0;
                    continue;
                }
                pData[offset]*=pGaussian2D[offset];
                sum0+=pData[offset];
            }

        
        // step 4.  loop over the occluding contours
        sum1=0;
        for(int n=0;n<FrontalContours.size();n++)
        {
            const QList<QPointF>& occludingContour=FrontalContours[n];
            double *pX1,*pY1;
            pX1=new double[occludingContour.size()];
            pY1=new double[occludingContour.size()];
            for(int j=0;j<occludingContour.size();j++)
            {
                pX1[j]=occludingContour[j].x()-pX0[k]+winsize;
                pY1[j]=occludingContour[j].y()-pY0[k]+winsize;
            }
            for(int i=0;i<winlength;i++)
            {
                for(int j=0;j<winlength;j++)
                {
                    offset=i*winlength+j;
                    if(pData[offset]==0)
                        continue;
                    // check whether the point is occluded
                    if(BinaryImage::pointInPolygon((double)j,(double)i,occludingContour.size(),pX1,pY1))
                    {
                        sum1+=pData[offset];
                        pData[offset]=0;
                    }
                    if(sum1/sum0>=MyPreference::Threshold_OccludingAreaRatio)
                    {
                        IsVisibleList[k]=false;
                        break;
                    }
                }
                if(IsVisibleList[k]==false)
                    break;
            }
            delete pX1;
            delete pY1;
        }
    }

    delete pX0;
    delete pX;
    delete pY0;
    delete pY;
    delete pGaussian2D;
}

//---------------------------------------------------------------------------------------------------------------
// function to track one point with building a cropped pyramid
//---------------------------------------------------------------------------------------------------------------
QPointF ContourTracking::TrackPointOneFrameCropped(PyrdPatchSet& pyrdPatchSet2,const QPointF &origin, const QPointF &reference, int pointIndex,const QList<QPointF>& refcontour,
                                                                                                                const PyrdPatchSet &pyrdPatchSet1, const BiImage &nextFrame, double alpha, int nIterations, int ExtraMargin)
{
    QPointF offset;
    ImPyramid pyrdNextFrame;
    QList<QPointF> prediction;
    prediction.append(reference);
    offset=CreateCroppedPyramid(pyrdNextFrame,nextFrame,prediction,pyrdPatchSet1.nlevels(),ExtraMargin);

    QPointF Origin,Reference,Tracked;
    QList<QPointF> RefContour(refcontour);

    // transform to the new coordinate
    Origin=origin-offset;
    Reference=reference-offset;
    SubtractOffset(RefContour,offset);

    Tracked=TrackPointOneFrame(Origin,Reference,pyrdPatchSet1,pyrdNextFrame,alpha,nIterations);
    pyrdPatchSet2.CreatePyrdPatchSet(pyrdNextFrame,Tracked,pointIndex,RefContour);

    // transform back to the original image coordinate
    return Tracked+offset;
}

//---------------------------------------------------------------------------------------------------------------
// function to track one point for one frame
//    origin:                     the coordinate in frame t
//    reference:             the coordinate in frame t+1
//    pyrPatchSet:         the pyramid patch set of frame t
//    pyrdNextFrame: the pyrmid of next frame
//     alpha:                     the coefficient of the regularization
//    nIterations:             the number of iterations
//---------------------------------------------------------------------------------------------------------------
QPointF ContourTracking::TrackPointOneFrame(const QPointF &origin, const QPointF &reference, const PyrdPatchSet &pyrdPatchSet, 
                                                                                       const ImPyramid &pyrdNextFrame, double alpha, int nIterations)
{
    int nLevels=pyrdPatchSet.nlevels();
    if(pyrdNextFrame.nlevels()!=nLevels)
    {
        cout<<"The image pyramid and contour patch pyramid don't match in number of levels!"<<endl;
        return QPointF(0,0);
    }
    QPointF flow,flow0,fOrigin,fReference;
    double temp;
    // compute from coarse to fine
    for(int k=nLevels-1;k>=0;k--)
    {
        if(k==0)
            temp=1;
        else
            temp=pow(0.5,k);

        fOrigin=origin*temp;
        fReference=reference*temp;
        flow0=fReference-fOrigin;

        // initialize or propagate the flow
        if(k==nLevels-1)
            flow=fReference-fOrigin;
        else
            flow*=2;

        // call matching function at a particular level
        MatchPoint(flow,fOrigin,flow0,pyrdPatchSet.getPatchSet(k),pyrdNextFrame.Image(k),pyrdNextFrame.dX(k),pyrdNextFrame.dY(k),alpha,nIterations);
    }
    QPointF result=origin+flow;
    return result;
}

//---------------------------------------------------------------------------------------------------------------
// function match points
// This is a special case of MatchContour when the contour is reduced to one point
// Input arguments:
//        flow-- the current estimate of the flow
//        flow0-- the reference point of the flow, ||flow-flow0||^2 should be small
//        patchSet-- the template of the patch to match
//        im, imdx, imdy-- the information of the next frame
//        alpha-- the coefficent of the regularziation
//        nIterations-- the number of iterations
//
//---------------------------------------------------------------------------------------------------------------
void ContourTracking::MatchPoint(QPointF &flow, const QPointF& origin,const QPointF &flow0, const PatchSet &patchSet, const ImageType &im, 
                                                                    const ImageType &imdx, const ImageType &imdy, double alpha, int nIterations)
{
    int winlength,winsize,imHeight,imWidth;
    QPointF nextPoint,dflow;
    winlength=patchSet.Patch(0).width();
    winsize=(winlength-1)/2;
    imWidth=im.width();
    imHeight=im.height();

    ImageType patch(winlength,winlength,im.nchannels()),patchDt(winlength,winlength,im.nchannels());
    ImageType patchDx(winlength,winlength,im.nchannels()),patchDy(winlength,winlength,im.nchannels());
    double Psi_xx,Psi_xy,Psi_yy,Psi_xt,Psi_yt;
    double A[4],b[2];
    // iteratively update the flow
    for(int count=0;count<nIterations;count++)
    {
        // set the data term to zero
        Psi_xx=Psi_xy=Psi_yy=Psi_xt=Psi_yt=0;
        
        // compute the data term parameters
        nextPoint=origin+flow;

        if(nextPoint.x()<0 || nextPoint.x()>imWidth-1 || nextPoint.y()<0 || nextPoint.y()>imHeight-1)
            continue;
    
        // sample patches from frame t+1
        im.getPatch(patch,nextPoint.x(),nextPoint.y(),winsize);
        imdx.getPatch(patchDx,nextPoint.x(),nextPoint.y(),winsize);
        imdy.getPatch(patchDy,nextPoint.x(),nextPoint.y(),winsize);

        // compute temporal derivative. the following line is equivalent to
        // patchDt=patch-patchSet.Patch(0)
        patchDt.Subtract(patch,patchSet.Patch(0));

        // compute Psi
        Psi_xx=WeightedSum(patchDx,patchDx,patchSet.Weight(0));
        Psi_xy=WeightedSum(patchDx,patchDy,patchSet.Weight(0));
        Psi_yy=WeightedSum(patchDy,patchDy,patchSet.Weight(0));
        Psi_xt=WeightedSum(patchDx,patchDt,patchSet.Weight(0));
        Psi_yt=WeightedSum(patchDy,patchDt,patchSet.Weight(0));

        // solve the linear system
        A[0]=Psi_xx+alpha;
        A[1]=Psi_xy;
        A[2]=Psi_xy;
        A[3]=Psi_yy+alpha;
        b[0]=-(Psi_xt+alpha*(flow.x()-flow0.x()));
        b[1]=-(Psi_yt+alpha*(flow.y()-flow0.y()));

        dflow=SolveLinearSystem2D(A,b);
        //cout<<dflow.x()<<" "<<dflow.y()<<endl;
        flow+=dflow;
    }
}

QPointF ContourTracking::SolveLinearSystem2D(double *A, double *b)
{
    double det=A[3]*A[0]-A[1]*A[2];
    QPointF result;
    result.setX((A[3]*b[0]-A[1]*b[1])/det);
    result.setY((-A[2]*b[0]+A[0]*b[1])/det);
    return result;
}
