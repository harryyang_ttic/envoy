#include <memory.h>
#include <iostream>

#include "LinearSystem.hpp"

using namespace std;

LinearSystem::LinearSystem(void)
{
}

LinearSystem::~LinearSystem(void)
{
}

void LinearSystem::Ax(int dim,double *A,double *x,double* y)
{
    int offset;
    for(int i=0;i<dim;i++)
    {
        y[i]=0;
        offset=i*dim;
        for(int j=0;j<dim;j++)
            y[i]+=A[offset+j]*x[j];
    }
}

 void LinearSystem::ConjugateGradient(int dim, double *A, double *b, double *x)
{
    int nIterations=dim+2; // more iterations for numerical issues
    double *rou,*r,*p,*q;
    rou=new double[nIterations];
    r=new double[dim];
    p=new double[dim];
    q=new double[dim];
    //memset(r,0,sizeof(double)*dim);
    memcpy(r,b,sizeof(double)*dim);
    memset(x,0,sizeof(double)*dim);

    for(int k=0;k<nIterations;k++)
    {
        rou[k]=norm2(dim,r);
        if(rou[k]<1E-20)
            break;
        if(k==0)
            memcpy(p,r,sizeof(double)*dim);
        else
        {
            double ratio=rou[k]/rou[k-1];
            for(int i=0;i<dim;i++)
                p[i]=r[i]+ratio*p[i];
        }
        Ax(dim,A,p,q);
        double alpha=rou[k]/innerproduct(dim,p,q);
        for(int i=0;i<dim;i++)
        {
            x[i]+=alpha*p[i];
            r[i]-=alpha*q[i];
        }
        //cout<<rou[k]<<endl;
    }

    delete [] r;
    delete [] p;
    delete [] q;
    delete [] rou;
}

 double LinearSystem::norm2(int dim,double* x)
 {
     double sum=0;
     for(int i=0;i<dim;i++)
         sum+=x[i]*x[i];
     return sum;
 }

 double LinearSystem::innerproduct(int dim, double *x, double *y)
 {
     double sum=0;
     for(int i=0;i<dim;i++)
         sum+=x[i]*y[i];
     return sum;
 }

 void LinearSystem::printMatrix(int dim,double *A)
 {
     for(int i=0;i<dim;i++)
     {
         for(int j=0;j<dim;j++)
             cout<<A[i*dim+j]<<" ";
         cout<<endl;
     }
 }

 void LinearSystem::printVector(int dim,double* x)
 {
     for(int i=0;i<dim;i++)
         cout<<x[i]<<endl;
 }
