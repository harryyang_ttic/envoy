#ifndef _project_h
#define _project_h

#include <stdio.h>

//#define _MEX

#ifdef _MEX
#include "mex.h"
#endif

//#define __min(a,b)  (a>b)?b:a
//#define __max(a,b) (a>b)?a:b

#ifndef __min
template <class T1,class T2>
T1 __min(T1 a, T2 b)
{
  return (a>b)?b:a;
}
#endif

#ifndef __max
template <class T1,class T2>
T1 __max(T1 a, T2 b)
{
  return (a<b)?b:a;
}
#endif

template <class T>
void _Release1DBuffer(T*& pBuffer)
{
    if(pBuffer!=NULL)
    {
        delete []pBuffer;
        pBuffer=NULL;
    }
}

template <class T>
void _Release2DBuffer(T**& pBuffer,int length)
{
    if(pBuffer!=NULL)
    {
        for(int i=0;i<length;i++)
            if(pBuffer[i]!=NULL)
                delete []pBuffer[i];
        delete []pBuffer;
        pBuffer=NULL;
    }
}

// define the most typically used macros here

//#ifndef _Release1DBuffer(pBuffer)
//    #define _Release1DBuffer(pBuffer) if(pBuffer!=NULL) {delete []pBuffer; pBuffer=NULL;}
//#endif

//#ifndef _Release2DBuffer(pBuffer,length,i)
//    #define _Release2DBuffer(pBuffer,length,i) if(pBuffer!=NULL){for(i=0;i<length;i++) if(pBuffer[i]!=NULL) delete pBuffer[i]; delete []pBuffer;pBuffer=NULL;}
//#endif

//#ifndef _IsUnknown(str)
//#define _IsUnknown(str) strcmp(str,"Unknown") ==0 || strcmp(str,"UNKNOWN")==0 || strcmp(str,"unknown")==0
//#endif
//
//#ifndef _EnforcePathName(pPathName)
//    #ifndef _LINUX //windows
//        #define _EnforcePathName(pPathName) if(pPathName[strlen(pPathName)-1]!='\\') strcat(pPathName,"\\");
//    #else //linux
//        #define _EnforcePathName(pPathName) if(pPathName[strlen(pPathName)-1]!='/') strcat(pPathName,"/");
//    #endif 
//#endif


#endif
