#include <QtGui/QApplication>
#include <QList>
#include <QString>
#include <QDir>
#include <QDebug>
#include <iostream>
#include <getopt.h>
#include <string>
#include "MainWindow.hpp"
#include "DepthControl.hpp"
#include "LayerDepth.hpp"
#include "Image.hpp"
#include "LinearSystem.hpp"

void DepthMap(int width=10);

static void
Usage(const char *app)
{
    qDebug() << "Usage: " << app << " [options] <FILE>" << endl
             << endl
             << " -c, --channel=IMG_CHANNEL  desired image channel name (default: FRNT_IMAGE)" << endl
             << " -h, --help                 print this help and exit." << endl;
} 
    

int main(int argc, char *argv[])
{
    QString channel = "FRNT_IMAGE";
    bool verbose = false;
    char *optstring = (char *) "hvc:";
    char c;
    struct option long_opts[] = {
        { "help", no_argument, 0, 'h' },
        { "verbose", no_argument, 0, 'v' },
        { "channel", required_argument, NULL, 'c' },
        { 0, 0, 0, 0 }
    };

    while ((c = getopt_long (argc, (char **)argv, optstring, long_opts, 0)) >= 0)
    {
        switch (c)
        {
            case 'c':
                channel = QString(optarg);
                break;
            case 'v':
                verbose = true;
                break;
            case 'h':
            default:
                Usage(argv[0]);
                exit(1);
                break;
        }            
    }

    QString path=QDir::currentPath();
    if (path.right(1)!="/")
        path+="/";
    path += "imageformats";

    QApplication a(argc, argv);
    QApplication::addLibraryPath(path);

    //DepthMap();

    QString lcmLogFile = "";
    if (optind == argc -1)
        lcmLogFile = QString(argv[optind]);
    else if (optind < argc-1) {
        Usage (argv[0]);
        exit(1);
    }

    MainWindow w(lcmLogFile, channel, verbose);
    w.show();
    a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
    return a.exec();
}

void DepthMap(int width)
{
    BiImage image(width,256,3);
    unsigned char* pData=image.data();
    QColor color;
    int RGB[3];

    for(int i=0;i<256;i++)
    {
        color.setHsv(i,255,255);
        RGB[0]=color.red();
        RGB[1]=color.green();
        RGB[2]=color.blue();
        for(int j=0;j<width;j++)
            for(int k=0;k<3;k++)
                pData[(i*width+j)*3+k]=RGB[k];
    }
    image.imwrite("DepthMap.bmp");
}

//int main(int argc, char *argv[])
//{
//    int nFrames=100;
//    QList<LayerDepth> DepthList;
//
//    LayerDepth layerDepth;
//    QList<int> index;
//    QList<int> depth;
//    index.append(0);
//    depth.append(30);
//    index.append(3);
//    depth.append(20);
//    index.append(27);
//    depth.append(100);
//    index.append(65);
//    depth.append(60);
//    layerDepth.Initialize(nFrames,0,nFrames-1,index,depth);
//    layerDepth.InterpolateLinear();
//    layerDepth.InterpolateCG(nFrames*3);
//    DepthList.append(layerDepth);
//
//    index.clear();
//    depth.clear();
//    index.append(40);
//    depth.append(120);
//    index.append(78);
//    depth.append(60);
//    layerDepth.Initialize(nFrames,10,nFrames-10,index,depth);
//    layerDepth.InterpolateLinear();
//    layerDepth.InterpolateCG(nFrames*3);
//    DepthList.append(layerDepth);
//
//    QApplication a(argc, argv);
//    DepthControl depthControl(NULL);
//    depthControl.loadDepthList(DepthList,0,40);
//    depthControl.show();
//    a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
//    return a.exec();
//}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    DepthControl depthControl(NULL);
//    depthControl.show();
//    a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
//    return a.exec();
//}

