#include <QDir>

#include "Stochastic.hpp"
#include "QVideoProcessing.hpp"

QVideoProcessing::QVideoProcessing(void)
{
}

QVideoProcessing::~QVideoProcessing(void)
{
}

QFileInfoList QVideoProcessing::findimagefiles(const QString& path)
{
    QDir dir;
    if (path.isEmpty()==false)
        dir.setPath(path);
     dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
     QStringList fileformat;
     fileformat<<"*.bmp"<<"*.jpg"<<"*.png"<<"*.gif";
     dir.setNameFilters(fileformat);
     dir.setSorting(QDir::Name);
     

     QFileInfoList list = dir.entryInfoList();
     return list;
}

QFileInfoList QVideoProcessing::findImageFilesSingleFormat(const QString& path)
{
    QDir dir;
    if (path.isEmpty()==false)
        dir.setPath(path);
     dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
     QStringList fileformat;
     fileformat<<"*.bmp"<<"*.jpg"<<"*.jpeg"<<"*.png"<<"*.gif";
     QFileInfoList list;
     int numFiles[5]={0,0,0,0,0};
     for (int i=0;i<5;i++)
     {
        QStringList format(fileformat[i]);
        dir.setNameFilters(format);
        dir.setSorting(QDir::Name);
        list= dir.entryInfoList();
        numFiles[i]=list.size();
     }

     // find the format with the maximum number of files
     int index=CStochastic::FindMax(5,numFiles);
    
     QStringList format(fileformat[index]);
    dir.setNameFilters(format);
    dir.setSorting(QDir::Name);
    list= dir.entryInfoList();
    return list; 
}

bool QVideoProcessing::FileListExist(const QString &pathName, const QStringList &fileList)
{
    QString folderName=pathName;
    if(folderName.right(1)!="/")
        folderName+="/";
    for(int i=0;i<fileList.size();i++)
    {
        QFileInfo fileInfo(folderName+fileList[i]);
        if(fileInfo.exists()==false)
            return false;
    }
    return true;
}

void QVideoProcessing::separatePath_FileNames(const QString& fullname,QString& pathname,QString& filename)
{
        QString FullName(fullname);
        if(FullName.right(1)=="/")
            FullName.chop(1);
        // separate file name and path name
        QStringList strlist=fullname.split("/");
        filename=strlist.last();
        pathname.clear();
        for (int j=0;j<strlist.size()-1;j++)
        {
            pathname+=strlist[j];
            if(j<strlist.size()-2)
                pathname+="/";
        }
}
