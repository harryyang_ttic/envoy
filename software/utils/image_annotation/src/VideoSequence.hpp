#ifndef _VideoSequence_h
#define _VideoSequence_h

#include <QObject>
#include <QString>
#include <QStringList>
#include <QImage>
#include <QFileInfo>
#include <QDebug>

#include <unistd.h>

#include <lcm/lcm.h>
#include <lcmtypes/bot_core.h>

class VideoSequence: public QObject
{
    Q_OBJECT
protected:
    QString framePath;
    QString lcmLogFile;
    QString lcmChannel;
    QStringList fileList;
    QList<int64_t> frameUtimeList;
    QImage m_Image;
    int curFrameIndex;
    int numFrames;
    bool IsVideoLoaded;
    bool verbose;
    
    lcm_eventlog_t *lcm_eventlog;
    lcm_eventlog_event_t *lcm_event;
public:
    VideoSequence(void);
    VideoSequence(const VideoSequence& videoSequence);
    virtual void copyData(const VideoSequence& videoSequence);
    ~VideoSequence(void);

    bool loadFrameLCM();
    bool loadFrame();
    void updateFrameControl();
    virtual void clear();
    virtual void Initialize(const QString& path,const QFileInfoList& filelist);
    virtual void InitializeLCM(const QString& lcmlog, const QString& channel, const bool verboseOutput);
    
signals:
    void ImageChanged(QImage& image);
    void FrameControlChanged(bool firstFrame,bool preFrame,bool nextFrame,bool lastFrame);
    void FrameIndexChanged(int frameIndex);
    void Update();
    void NumFramesChanged(int nFrames);

public slots:
    virtual void gotoFrame(int index);
    virtual void gotoFirstFrame();
    virtual void gotoPreFrame();
    virtual void gotoNextFrame();
    virtual void gotoLastFrame();
};

#endif
