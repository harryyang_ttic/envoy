#ifndef _ImPyramid_h
#define _ImPyramid_h

#include <QString>
#include <iostream>
#include "Image.hpp"

using namespace std;

typedef DImage ImageType;

class ImPyramid
{
private:
    int nLevels;
    ImageType* pImages;
    ImageType* pXDerivatives;
    ImageType* pYDerivatives;
public:
    ImPyramid(void);
    ImPyramid(const ImPyramid& other);
    ~ImPyramid(void);
    void clear();
    void copyData(const ImPyramid& other);

    void CreatePyramid(const BiImage& m_Image,int nlevels=5);
    void showPyramid(const QString& basename=QString("Pyramid"));

    // functions to access the members
    inline int enforceLevelRange(int level) const {
        if(level<0)
        {
            level=0;
            cout<<"Error: the level is small than 0 when accessing ImPyramid"<<endl;
        }
        if(level>=nLevels)
        {
            level=nLevels-1;
            cout<<"Error: the level is bigger than the total number of levels "<<nLevels<<" of ImPyramid"<<endl;
        }
        return level;
    };
    inline int nlevels()const {return nLevels;};
    inline ImageType& Image(unsigned level) {return pImages[enforceLevelRange(level)];};
    inline ImageType& dX(unsigned level) {return pXDerivatives[enforceLevelRange(level)];};
    inline ImageType& dY(unsigned level) {return pYDerivatives[enforceLevelRange(level)];};

    inline const ImageType& Image(unsigned level) const  {return (const ImageType&)pImages[enforceLevelRange(level)];};
    inline const ImageType& dX(unsigned level) const {return (const ImageType&)pXDerivatives[enforceLevelRange(level)];};
    inline const ImageType& dY(unsigned level) const {return (const ImageType&)pYDerivatives[enforceLevelRange(level)];};

};

#endif
