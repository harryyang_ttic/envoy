#include <QtGui>
#include <iostream>
#include "LayerDepth.hpp"
#include "memory.h"

LayerDepth::LayerDepth(void): QObject()
{
    weight=0.01;
}

LayerDepth::~LayerDepth(void)
{
}

LayerDepth::LayerDepth(const LayerDepth &m_LayerDepth): QObject()
{
    copyData(m_LayerDepth);
}

LayerDepth& LayerDepth::operator =(const LayerDepth &m_LayerDepth)
{
    copyData(m_LayerDepth);
    return *this;
}

void LayerDepth::copyData(const LayerDepth &m_LayerDepth)
{
    m_Depth=m_LayerDepth.m_Depth;
    m_IsLabeled=m_LayerDepth.m_IsLabeled;
    m_SqIsLabeled=m_LayerDepth.m_SqIsLabeled;
    m_Index=m_LayerDepth.m_Index;

    m_StartFrame=m_LayerDepth.m_StartFrame;
    m_EndFrame=m_LayerDepth.m_EndFrame;

    m_Dim=m_LayerDepth.m_Dim;
    dim=m_LayerDepth.dim;
    weight=m_LayerDepth.weight;
}

void LayerDepth::clear()
{
    m_Depth.clear();
    m_IsLabeled.clear();
    m_SqIsLabeled.clear();
    m_Index.clear();

    m_StartFrame=m_EndFrame=m_Dim=dim=0;
}

void LayerDepth::printDepth() const
{
    for(int i=m_StartFrame;i<=m_EndFrame;i++)
        std::cout<<m_Depth[i]<<std::endl;
}

void LayerDepth::Initialize(int nFrames, int startFrame, int endFrame, const QList<int>& indexList, const QList<int>& indexDepthList)
{
    clear();

    m_StartFrame=startFrame;
    m_EndFrame=endFrame;
    m_Dim=m_EndFrame-m_StartFrame+1;
    m_Depth=QVector<double>(nFrames,0);
    m_IsLabeled=QVector<bool>(nFrames,false);
    m_SqIsLabeled=QVector<bool>(m_Dim,false);

    for(int i=0;i<indexList.size();i++)
    {
        int index=indexList[i];
        m_Depth[index]=indexDepthList[i];
        m_IsLabeled[index]=true;
        m_SqIsLabeled[index-m_StartFrame]=true;
    }
    dim=m_Dim-indexList.size();
    m_Index=QVector<int>::fromList(indexList);
}

void LayerDepth::Initialize(int nFrames, int startFrame, int endFrame, const QList<double>& depthList, const QList<int> & indexList)
{
    clear();

    m_StartFrame=startFrame;
    m_EndFrame=endFrame;
    m_Dim=m_EndFrame-m_StartFrame+1;
    m_Depth=QVector<double>(nFrames,0);
    m_IsLabeled=QVector<bool>(nFrames,false);
    m_SqIsLabeled=QVector<bool>(m_Dim,false);

    m_Depth=QVector<double>::fromList(depthList);
    for(int i=0;i<indexList.size();i++)
    {
        int index=indexList[i];
        m_IsLabeled[index]=true;
        m_SqIsLabeled[index-m_StartFrame]=true;
    }
    dim=m_Dim-indexList.size();
    m_Index=QVector<int>::fromList(indexList);
}

//------------------------------------------------------------------------------------------------
// function to check whether frameIndex has been labeled. if not, add it
//------------------------------------------------------------------------------------------------
void LayerDepth::insertIndex(int frameIndex)
{
    if(m_IsLabeled[frameIndex]==true)
        return;
    m_IsLabeled[frameIndex]=true;
    m_SqIsLabeled[frameIndex-m_StartFrame]=true;
    dim--;

    for(int i=0;i<m_Index.size()-1;i++)
        if(m_Index[i]<frameIndex && m_Index[i+1]>=frameIndex)
        {
            m_Index.insert(i+1,frameIndex);
            return;
        }
        
    m_Index.append(frameIndex);
}

void LayerDepth::setDepth(int frameIndex,int depth)
{
    insertIndex(frameIndex);
    m_Depth[frameIndex]=depth;
}

double LayerDepth::max()
{
    double max_depth=m_Depth[m_StartFrame];
    for(int i=m_StartFrame+1;i<=m_EndFrame;i++)
        max_depth=qMax(max_depth,m_Depth[i]);
    return max_depth;
}

double LayerDepth::min()
{
    double min_depth=m_Depth[m_StartFrame];
    for(int i=m_StartFrame+1;i<=m_EndFrame;i++)
        min_depth=qMin(min_depth,m_Depth[i]);
    return min_depth;
}

//-------------------------------------------------------------------------------------------------------------------------------------
// function to draw the curve in a specified widget
//-------------------------------------------------------------------------------------------------------------------------------------
void LayerDepth::DrawCurve(QPainter &painter, const QPoint &Offset, double plotHeight, double minDepth,double x_stepsize, double y_stepsize, int frameIndex,bool IsActive)
{
    QPen pen;
    QColor curveColor,bgColor;

    bgColor.setRgb(50,50,50);
    pen.setColor(curveColor);
    pen.setWidthF(2);
    pen.setStyle(Qt::SolidLine);
    painter.setPen(pen);

    // plot curve
    QPointF prePoint,curPoint;
    prePoint.setX(m_StartFrame*x_stepsize+Offset.x());
    prePoint.setY(plotHeight-(m_Depth[m_StartFrame]-minDepth)*y_stepsize+Offset.y());

    for(int i=m_StartFrame+1;i<=m_EndFrame;i++)
    {
        curPoint.setX(i*x_stepsize+Offset.x());
        curPoint.setY(plotHeight-(m_Depth[i]-minDepth)*y_stepsize+Offset.y());

        if(IsActive)
            curveColor.setHsv(qMax<double>(qMin<double>(m_Depth[i],255),0),255,255);
        else
            curveColor.setHsv(qMax<double>(qMin<double>(m_Depth[i],255),0),120,255);
        pen.setColor(curveColor);
        if(IsActive)
            pen.setWidthF(3);
        else
            pen.setWidthF(2);
        painter.setPen(pen);
        painter.drawLine(prePoint,curPoint);

        prePoint=curPoint;
    }

    // plot the control point
    if(IsActive)
        for(int i=0;i<m_Index.size();i++)
        {
            int index=m_Index[i];
            curPoint.setX(index*x_stepsize+Offset.x());
            curPoint.setY(plotHeight-(m_Depth[index]-minDepth)*y_stepsize+Offset.y());
            
            pen.setColor(bgColor);
            pen.setWidthF(6);
            painter.setPen(pen);
            painter.drawPoint(curPoint);

            if(index!=frameIndex)
                pen.setColor(Qt::lightGray);
            else
                pen.setColor(Qt::white);
            pen.setWidthF(4.5);
            painter.setPen(pen);
            painter.drawPoint(curPoint);
        }
}

void LayerDepth::InterpolateLinear()
{
    // extrapolate the beginning and ending part
    if(m_Index[0]>m_StartFrame)
        for(int i=m_StartFrame;i<m_Index[0];i++)
            m_Depth[i]=m_Depth[m_Index[0]];
    if(m_Index.last()<m_EndFrame)
        for(int i=m_Index.last()+1;i<=m_EndFrame;i++)
            m_Depth[i]=m_Depth[m_Index.last()];
    for(int i=0;i<m_Index.size()-1;i++)
    {
        int start,end;
        start=m_Index[i];
        end=m_Index[i+1];
        if(end==start+1)
            continue;
        double x1,x2,ratio;
        x1=m_Depth[start];
        x2=m_Depth[end];
        for(int k=start+1;k<end;k++)
        {
            ratio=(double)(k-start)/(end-start);
            m_Depth[k]=x1*(1-ratio)+x2*ratio;
        }
    }
}

int LayerDepth::InterpolateLinear(int frameIndex)
{
    int count=0;
    if(frameIndex==m_Index[0])
        for(int i=m_StartFrame;i<m_Index[0];i++)
        {
            m_Depth[i]=m_Depth[m_Index[0]];
            count++;
        }
    if(frameIndex==m_Index.last())
        for(int i=m_Index.last()+1;i<=m_EndFrame;i++)
        {
            m_Depth[i]=m_Depth[m_Index.last()];
            count++;
        }
    int j;
    for(j=0;j<m_Index.size();j++)
        if(frameIndex==m_Index[j])
            break;
    if(j==m_Index.size())
        return count;

    int start,end;
    double x1,x2,ratio;
    
    if(j>0)
    {
        start=m_Index[j-1];
        end=m_Index[j];
        x1=m_Depth[start];
        x2=m_Depth[end];
        for(int k=start+1;k<end;k++)
        {
            ratio=(double)(k-start)/(end-start);
            m_Depth[k]=x1*(1-ratio)+x2*ratio;
            count++;
        }
    }
    if(j<m_Index.size()-1)
    {
        start=m_Index[j];
        end=m_Index[j+1];
        x1=m_Depth[start];
        x2=m_Depth[end];
        for(int k=start+1;k<end;k++)
        {
            ratio=(double)(k-start)/(end-start);
            m_Depth[k]=x1*(1-ratio)+x2*ratio;
            count++;
        }
    }
    return count;
}

//------------------------------------------------------------------------------
// function to use conjugate gradient to interpolate the depth curve
//------------------------------------------------------------------------------
void LayerDepth::InterpolateCG(int nIterations)
{
    int i,j,k;
    double *x,*b,*r,*temp1,*p,*q,foo,alpha;
    x=new double[m_Dim*5];
    r=x+m_Dim;
    b=x+m_Dim*2;
    p=x+m_Dim*3;
    q=x+m_Dim*4;

    // initialize x
    for(i=0,j=0;i<m_Dim;i++)
        if(m_SqIsLabeled[i]==false)
            x[j++]=m_Depth[i+m_StartFrame];

    //printVector(x,dim);

    LinearSystem(x,p);
    //printVector(p,dim);

    compute_b(b);

    for(i=0;i<dim;i++)
        r[i]=b[i]-p[i];

    //printVector(r,dim);

    // CG iteration
    double* rou;
    rou=new double[nIterations];
    for(k=0;k<nIterations;k++)
    {
        // compute rou
        foo=0;
        for(i=0;i<dim;i++)
            foo+=r[i]*r[i];
        rou[k]=foo;

        if(rou[k]<0.000001)
            break;

        if(k==0)
            memcpy(p,r,sizeof(double)*dim);
        else
        {
            double ratio=rou[k]/rou[k-1];
            for(i=0;i<dim;i++)
                p[i]=r[i]+ratio*p[i];
        }

        LinearSystem(p,q);

        foo=0;
        for(i=0;i<dim;i++)
            foo+=p[i]*q[i];
        alpha=rou[k]/foo;

        for(i=0;i<dim;i++)
        {
            x[i]=x[i]+alpha*p[i];
            r[i]=r[i]-alpha*q[i];
        }
    }

    for(i=0,j=0;i<m_Dim;i++)
        if(m_SqIsLabeled[i]==false)
            m_Depth[i+m_StartFrame]=x[j++];

    delete x;
    delete rou;
}

//----------------------------------------------------------------------------------------------------------
//  the linear system, applying A^T*(weight*H_1^T*H_1+H_2^T*H_2)*A to the input vector
//----------------------------------------------------------------------------------------------------------
void LayerDepth::LinearSystem(double*& input,double*& output)
{
    double *x,*temp1,*temp2,*temp3;
    x=new double[m_Dim*4];
    memset(x,0,sizeof(double)*m_Dim*4);
    temp1=x+m_Dim;
    temp2=x+m_Dim*2;
    temp3=x+m_Dim*3;

    A(input,x);

    H2(x,temp1);
    H2T(temp1,temp3);

    H1(x,temp1);
    H1T(temp1,temp2);

    for(int i=0;i<m_Dim;i++)
        temp1[i]=temp2[i]*weight+temp3[i];

    AT(temp1,output);

    //AT(temp3,output);

    delete x;
}

void LayerDepth::compute_b(double*& output)
{
    double *y,*temp1,*temp2,*temp3;
    y=new double[m_Dim*4];
    temp1=y+m_Dim;
    temp2=y+m_Dim*2;
    temp3=y+m_Dim*3;
    memset(y,0,sizeof(double)*m_Dim*3);

    int i;
    for(i=0;i<m_Dim;i++)
        if(m_SqIsLabeled[i]==true)
            y[i]=m_Depth[i+m_StartFrame];

    H2(y,temp1);
    H2T(temp1,temp3);

    H1(y,temp1);
    H1T(temp1,temp2);

    for(i=0;i<m_Dim;i++)
        temp1[i]=temp2[i]*weight+temp3[i];

    AT(temp1,output);

    //AT(temp3,output);

    for(i=0;i<dim;i++)
        output[i]=-output[i];

    delete y;
}

void LayerDepth::printVector(double *vect, int d)
{
    for(int i=0;i<d;i++)
        std::cout<<vect[i]<<" ";
    std::cout<<std::endl;
}

void LayerDepth::AT(double *&input, double *&output)
{
    memset(output,0,sizeof(double)*m_Dim);
    int i,j=0;
    for(i=0;i<m_Dim;i++)
        if(m_SqIsLabeled[i]==false)
            output[j++]=input[i];
}

void LayerDepth::A(double*&input,double*& output)
{
    memset(output,0,sizeof(double)*m_Dim);
    int i,j=0;
    for(i=0;i<m_Dim;i++)
        if(m_SqIsLabeled[i]==false)
            output[i]=input[j++];
}

void LayerDepth::B(double*& input,double*& output)
{
    memset(output,0,sizeof(double)*m_Dim);
    int i,j=0;
    for(i=0;i<m_Dim;i++)
        if(m_SqIsLabeled[i]==true)
            output[j++]=input[i];
}

void LayerDepth::H1(double *&input, double *&output)
{
    //memset(output,0,sizeof(double)*m_Dim);
    for(int i=0;i<m_Dim-1;i++)
        output[i]=input[i+1]-input[i];
}

void LayerDepth::H1T(double *&input, double *&output)
{
    //memset(output,0,sizeof(double)*m_Dim);
    output[0]=-input[0];
    for(int i=1;i<m_Dim-1;i++)
        output[i]=input[i-1]-input[i];
    output[m_Dim-1]=input[m_Dim-2];
}

void LayerDepth::H2(double*& input,double*& output)
{
    for(int i=1;i<m_Dim-1;i++)
        output[i-1]=-input[i-1]+2*input[i]-input[i+1];
}

void LayerDepth::H2T(double *&input, double *&output)
{
    output[0]=-input[0];
    output[1]=-input[1]+2*input[0];
    for(int i=2;i<m_Dim-2;i++)
        output[i]=-input[i]+2*input[i-1]-input[i-2];
    output[m_Dim-2]=input[m_Dim-3]*2-input[m_Dim-4];
    output[m_Dim-1]=-input[m_Dim-3];
}


//--------------------------------------------------------------------------------------
// functions to test whether the linear functions are correct
//--------------------------------------------------------------------------------------

void LayerDepth::testH1()
{
    double *x,*y,*Matrix;
    x=new double[m_Dim];
    y=new double[m_Dim];
    Matrix=new double[m_Dim*m_Dim];
    for(int i=0;i<m_Dim;i++)
    {
        memset(x,0,sizeof(double)*m_Dim);
        x[i]=1;
        H1(x,y);
        for(int j=0;j<m_Dim-1;j++)
            Matrix[j*m_Dim+i]=y[j];
    }
    for(int j=0;j<m_Dim-1;j++)
    {
        for(int i=0;i<m_Dim;i++)
            std::cout<<Matrix[j*m_Dim+i]<<" ";
        std::cout<<std::endl;
    }
    delete x;
    delete y;
    delete Matrix;
}

void LayerDepth::testH1T()
{
    double *x,*y,*Matrix;
    x=new double[m_Dim];
    y=new double[m_Dim];
    Matrix=new double[m_Dim*m_Dim];
    for(int i=0;i<m_Dim-1;i++)
    {
        memset(x,0,sizeof(double)*m_Dim);
        x[i]=1;
        H1T(x,y);
        for(int j=0;j<m_Dim;j++)
            Matrix[j*m_Dim+i]=y[j];
    }
    for(int j=0;j<m_Dim;j++)
    {
        for(int i=0;i<m_Dim-1;i++)
            std::cout<<Matrix[j*m_Dim+i]<<" ";
        std::cout<<std::endl;
    }
    delete x;
    delete y;
    delete Matrix;
}

void LayerDepth::testH2()
{
    double *x,*y,*Matrix;
    x=new double[m_Dim];
    y=new double[m_Dim];
    Matrix=new double[m_Dim*m_Dim];
    for(int i=0;i<m_Dim;i++)
    {
        memset(x,0,sizeof(double)*m_Dim);
        x[i]=1;
        H2(x,y);
        for(int j=0;j<m_Dim-2;j++)
            Matrix[j*m_Dim+i]=y[j];
    }
    for(int j=0;j<m_Dim-2;j++)
    {
        for(int i=0;i<m_Dim;i++)
            std::cout<<Matrix[j*m_Dim+i]<<" ";
        std::cout<<std::endl;
    }
    delete x;
    delete y;
    delete Matrix;
}

void LayerDepth::testH2T()
{
    double *x,*y,*Matrix;
    x=new double[m_Dim];
    y=new double[m_Dim];
    Matrix=new double[m_Dim*m_Dim];
    for(int i=0;i<m_Dim-2;i++)
    {
        memset(x,0,sizeof(double)*m_Dim);
        x[i]=1;
        H2T(x,y);
        for(int j=0;j<m_Dim;j++)
            Matrix[j*m_Dim+i]=y[j];
    }
    for(int j=0;j<m_Dim;j++)
    {
        for(int i=0;i<m_Dim-2;i++)
            std::cout<<Matrix[j*m_Dim+i]<<" ";
        std::cout<<std::endl;
    }
    delete x;
    delete y;
    delete Matrix;
}
