#ifndef _IMAGEVIEWER_H
#define _IMAGEVIEWER_H

#include <QWidget>
#include <QImage>
#include <QPainter>

class QImage;

//-----------------------------------------------------------------------------
// Image viewer is set to be a generic image viewing widget
//----------------------------------------------------------------------------- 
class ImageViewer :  public QWidget
{
    Q_OBJECT
public:
    // MouseAction may be defined at elsewhere
    enum MouseAction{LeftClick,RightClick,Move,DoubleClick,Release};
public:
    ImageViewer(QWidget* parent=0);
    ~ImageViewer(void);
    void setParentSize(QSize size);
    inline void setMinScale(double min_scale){minScale=min_scale;};
    inline void setMaxScale(double max_scale){maxScale=max_scale;};
    inline double getScaleFactor(){return scaleFactor;};
    inline QSize getPaintingOffset(){return QSize(HScreenOffset, VScreenOffset);};
protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void mousePressEvent ( QMouseEvent * event );
    virtual void mouseMoveEvent ( QMouseEvent * event );
    virtual void mouseDoubleClickEvent ( QMouseEvent * event );
    virtual void mouseReleaseEvent ( QMouseEvent * event );

signals:
    void adjustScrollBar(double factor);
    void setZoomIn(bool flag);
    void setZoomOut(bool flag);
    void viewerMouseEvent(ImageViewer& imageViewer, ImageViewer::MouseAction& mouseAction,QPoint& pos);
    void paintingImageViewer(ImageViewer& imageViewer,QPainter& painter);

public slots:
    void loadImage(QImage& inputImage);
    void zoomIn();
    void zoomOut();
    void normalSize();
    void redraw(){update();};

private:
    void resizeImage(double factor=0);
    void computeViewerSize();
    int ImHeight,ImWidth;// the dimension of the image
    int HMargin,VMargin;// the horizontal and vertical margin of the viewing on the canvas
    int xoffset,yoffset;// the offset to the top left corner of the frame
    int HScreenOffset,VScreenOffset; // the offset of the top-left corner of the image
    int ViewerHeight,ViewerWidth; // the dimension of the image viewer, ViewerHeight=ImHeight+HMargin*2+2;
    QImage m_Image,dispImage;
    double scaleFactor;
    double minScale,maxScale;
    QSize parentViewerSize;// the size of the parent widget
};

#endif