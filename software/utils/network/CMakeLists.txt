cmake_minimum_required(VERSION 2.6.0)

# pull in the pods macros. See cmake/pods.cmake for documentation
set(POD_NAME network)
include(cmake/pods.cmake)
#include(cmake/lcmtypes.cmake)

#lcmtypes_build()

find_package(PkgConfig REQUIRED)

pkg_check_modules(LCM REQUIRED lcm)

include_directories(${LCMTYPES_INCLUDE_DIRS})

#tell cmake to build these subdirectories
add_subdirectory(src)
