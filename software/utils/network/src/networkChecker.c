#include <stdio.h>
#include <errno.h>
#include <sys/time.h>
#include <signal.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/file.h>

#include <netinet/in_systm.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>

#include <lcm/lcm.h>
#include <lcmtypes/erlcm_network_check_msg_t.h>

#define MAXWAIT     10  /* max time to wait for response, sec. */
#define MAXPACKET   4096    /* max packet size */
#define VERBOSE     1   /* verbose flag */
#define QUIET       2   /* quiet flag */
#define FLOOD       4   /* floodping flag */
#ifndef MAXHOSTNAMELEN
#define MAXHOSTNAMELEN  64
#endif

u_char  packet[MAXPACKET];
int i, pingflags, options;
extern  int errno;

int s;          /* Socket file descriptor */
struct hostent *hp; /* Pointer to host info */
struct timezone tz; /* leftover */

struct sockaddr whereto;/* Who to ping */
int datalen;        /* How much data */

char *hostname;
char hnamebuf[MAXHOSTNAMELEN];

int npackets;
int preload = 0;        /* number of packets to "preload" */
int ntransmitted = 0;       /* sequence # for outbound packets = #sent */
int ident;

int nreceived = 0;      /* # of packets we got back */
int tmin = 999999999;
int tmax = 0;
int tsum = 0;           /* sum of all times, for doing average */
int finish(), catcher();
char *inet_ntoa();

int nlost = 0;
lcm_t *lcm;

main(argc, argv)
char *argv[];
{
    struct sockaddr_in from;
    char **av = argv;
    struct sockaddr_in *to = (struct sockaddr_in *) &whereto;
    int on = 1;
    struct protoent *proto;

    if(argc != 2)  {
        printf("Needs an IP address\n");
        exit(1);
    }

    av++;

    bzero((char *)&whereto, sizeof(struct sockaddr) );
    to->sin_family = AF_INET;
    to->sin_addr.s_addr = inet_addr(av[0]);
    if(to->sin_addr.s_addr != (unsigned)-1) {
        strcpy(hnamebuf, av[0]);
        hostname = hnamebuf;
    } else {
        hp = gethostbyname(av[0]);
        if (hp) {
            to->sin_family = hp->h_addrtype;
            bcopy(hp->h_addr, (caddr_t)&to->sin_addr, hp->h_length);
            hostname = hp->h_name;
        } else {
            printf("%s: unknown host %sn", argv[0], av[0]);
            exit(1);
        }
    }

    lcm = lcm_create(NULL);

    datalen = 64-8;

    ident = getpid() & 0xFFFF;

    if ((proto = getprotobyname("icmp")) == NULL) {
        fprintf(stderr, "icmp: unknown protocoln");
        exit(10);
    }

    if ((s = socket(AF_INET, SOCK_RAW, proto->p_proto)) < 0) {
        perror("ping: socket");
        exit(5);
    }

    signal( SIGINT, finish );
    signal(SIGALRM, catcher);

    /* fire off them quickies */
    for(i=0; i < preload; i++)
        pinger();

    if(!(pingflags & FLOOD))
        catcher();  /* start things going */

    for (;;) {
        int len = sizeof (packet);
        int fromlen = sizeof (from);
        int cc;
        struct timeval timeout;
        int fdmask = 1 << s;

        timeout.tv_sec = 0;
        timeout.tv_usec = 10000;

        if(pingflags & FLOOD) {
            pinger();
            if( select(32, &fdmask, 0, 0, &timeout) == 0)
                continue;
        }
        cc=recvfrom(s, packet, len, 0, &from, &fromlen);
        if ( cc != 84) {
            continue;
        }
        //pr_pack( packet, cc, &from );
        nreceived++;
        if (npackets && nreceived >= npackets)
            finish();
    }
    /*NOTREACHED*/
}

/*
 *          C A T C H E R
 * 
 * This routine causes another PING to be transmitted, and then
 * schedules another SIGALRM for 1 second from now.
 * 
 * Bug -
 *  Our sense of time will slowly skew (ie, packets will not be launched
 *  exactly at 1-second intervals).  This does not affect the quality
 *  of the delay and loss statistics.
 */
catcher()
{
    int waittime;

    pinger();
    if (npackets == 0 || ntransmitted < npackets)
        alarm(1);
    else {
        if (nreceived) {
            waittime = 2 * tmax / 1000;
            if (waittime == 0)
                waittime = 1;
        } else
            waittime = MAXWAIT;
        signal(SIGALRM, finish);
        alarm(waittime);
    }

    erlcm_network_check_msg_t network;
    network.networkStatus = ERLCM_NETWORK_CHECK_MSG_T_NETWORK_DOWN;
    if ((ntransmitted - nreceived) > nlost)   {
        nlost = ntransmitted-nreceived;
    }
    else {
        network.networkStatus = ERLCM_NETWORK_CHECK_MSG_T_NETWORK_UP;
    }
    erlcm_network_check_msg_t_publish(lcm, "NETWORK_STATUS", &network);
}

/*
 *          P I N G E R
 * 
 * Compose and transmit an ICMP ECHO REQUEST packet.  The IP packet
 * will be added on by the kernel.  The ID field is our UNIX process ID,
 * and the sequence number is an ascending integer.  The first 8 bytes
 * of the data portion are used to hold a UNIX "timeval" struct in VAX
 * byte-order, to compute the round-trip time.
 */
pinger()
{
    static u_char outpack[MAXPACKET];
    register struct icmp *icp = (struct icmp *) outpack;
    int i, cc;
    register struct timeval *tp = (struct timeval *) &outpack[8];
    register u_char *datap = &outpack[8+sizeof(struct timeval)];

    icp->icmp_type = ICMP_ECHO;
    icp->icmp_code = 0;
    icp->icmp_cksum = 0;
    icp->icmp_seq = ntransmitted++;
    icp->icmp_id = ident;       /* ID */

    cc = datalen+8;         /* skips ICMP portion */

    for( i=8; i<datalen; i++)   /* skip 8 for time */
        *datap++ = i;

    /* Compute ICMP checksum here */
    icp->icmp_cksum = in_cksum( icp, cc );

    /* cc = sendto(s, msg, len, flags, to, tolen) */
    i = sendto( s, outpack, cc, 0, &whereto, sizeof(struct sockaddr) );
}

/*
 *          I N _ C K S U M
 *
 * Checksum routine for Internet Protocol family headers (C Version)
 *
 */
in_cksum(addr, len)
u_short *addr;
int len;
{
    register int nleft = len;
    register u_short *w = addr;
    register u_short answer;
    register int sum = 0;

    /*
     *  Our algorithm is simple, using a 32 bit accumulator (sum),
     *  we add sequential 16 bit words to it, and at the end, fold
     *  back all the carry bits from the top 16 bits into the lower
     *  16 bits.
     */
    while( nleft > 1 )  {
        sum += *w++;
        nleft -= 2;
    }

    /* mop up an odd byte, if necessary */
    if( nleft == 1 ) {
        u_short u = 0;

        *(u_char *)(&u) = *(u_char *)w ;
        sum += u;
    }

    /*
     * add back carry outs from top 16 bits to low 16 bits
     */
    sum = (sum >> 16) + (sum & 0xffff); /* add hi 16 to low 16 */
    sum += (sum >> 16);         /* add carry */
    answer = ~sum;              /* truncate to 16 bits */
    return (answer);
}

/*
 *          F I N I S H
 *
 * Print out statistics, and give up.
 * Heavily buffered STDIO is used here, so that all the statistics
 * will be written with 1 sys-write call.  This is nice when more
 * than one copy of the program is running on a terminal;  it prevents
 * the statistics output from becomming intermingled.
 */
finish()
{
    exit(0);
}

