#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <regex.h>

#include <glib.h>

#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <stdio.h>
#include <iostream>
#include <lcmtypes/erlcm_interrupt_msg_t.h>

typedef struct _state_t state_t;
bool event_processed = false;
char *channame = NULL;

static void
usage (int argc, char ** argv)
{
    fprintf (stderr, "Usage %s -c <IMG_CHAN> [OPTIONS] <source_logfile>\n"
             "\n"
             "Generate jpeg images corresponding to LCM messages with <IMG_CHAN> channel name \n"
             "located within specified LCM logfile with generated files named according to image utime.\n"
             "\n"
             " -h                      prints this help and exits\n"
             " -n, --numframe          name images according to frame number starting at 0\n" 
             " -c, --channel=IMG_CHAN  channel name for desired image\n"
             " -s, --start=START       start time. Images logged less than START seconds\n"
             "                         after the first logged message will be ignored.\n"
             " -e, --end=END           end time. Images logged after END seconds\n"
             "                         from the beginning of the log will be ignored.\n"
             " -v, --verbose           verbose output\n"
             , argv[0]);
}

static void on_interrupt(const lcm_recv_buf_t* rbuf, const char* channel, 
                         const erlcm_interrupt_msg_t* msg, void* user_data) { 
    if (strcmp(channame, msg->source_channel) == 0) 
        event_processed = false;
}

int main (int argc, char **argv)
{
    char *lcm_fname = NULL;

    char *optstring = (char*)"hnvc:s:e:";
    char c;
    struct option long_opts[] = {
        { "help", no_argument, NULL, 'h' },
        { "verbose", no_argument, NULL, 'v' },
        { "numframe", no_argument, NULL, 'n' },
        { "channel", required_argument, NULL, 'c' },
        { "start", required_argument, NULL, 's' },
        { "end", required_argument, NULL, 'e' },
        { 0, 0, 0, 0 }
    };

    while ((c = getopt_long (argc, argv, optstring, long_opts, 0)) >= 0)
    {
        switch (c) {
            case 'c':
                channame = strdup (optarg);
                break;
            case 'h':
            default:
                usage (argc, argv);
            return 1;
        }
    }

    // if (start_utime < 0 || (have_end_utime && (end_utime < start_utime)))
    //     usage (argc, argv);

    if (!channame)
        usage (argc, argv);
    
    lcm_fname = argv[argc -1];

    // now, generate the images
    lcm_eventlog_t *src_log = lcm_eventlog_create (lcm_fname, "r");
    if (!src_log) {
        fprintf (stderr, "Unable to open source logfile %s\n", lcm_fname);
        return 1;
    }

    lcm_t* lcm_in = lcm_create(NULL);
    lcm_t* lcm_out = lcm_create(NULL);
    if (!lcm_in || !lcm_out) { 
        printf("Unable to initialize LCM\n");
        return 1;
    }
    erlcm_interrupt_msg_t_subscribe(lcm_in, "EVENT_INTERRUPT", &on_interrupt, NULL); 

    printf("Starting ...\n");
    for (lcm_eventlog_event_t *event = lcm_eventlog_read_next_event (src_log);
         event != NULL;
         event = lcm_eventlog_read_next_event (src_log)) {
        
        if (strcmp(channame, event->channel) == 0) { 
            lcm_publish(lcm_out, event->channel, event->data, event->datalen);
            event_processed = true;
        }
        lcm_eventlog_free_event (event);

        while (event_processed) 
         lcm_handle(lcm_in);
        
    }
    
    lcm_eventlog_destroy (src_log);

    // if (verbose)
    //     fprintf (stdout, "Generated %d images\n", frameno);

    lcm_destroy(lcm_in);
    lcm_destroy(lcm_out);
    
    return 0;
}
