// file: send_interrupt.cc

#include <stdio.h>
#include <lcm/lcm.h>

#include <lcmtypes/erlcm_interrupt_msg_t.h>

static void
send_message(lcm_t * lcm)
{
    erlcm_interrupt_msg_t interrupt;
    interrupt.source_channel = "KINECT_FRAME";
    
    erlcm_interrupt_msg_t_publish(lcm, "EVENT_INTERRUPT", &interrupt);
}

int
main(int argc, char ** argv)
{
    lcm_t * lcm;

    lcm = lcm_create(NULL);
    if(!lcm)
        return 1;

    send_message(lcm);

    lcm_destroy(lcm);
    return 0;
}
