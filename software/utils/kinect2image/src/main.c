#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/kinect_frame_msg_t.h>

//#include <bot_param/param_client.h>
//#include <lcmtypes/bot2_param.h>
//#include <bot_param/param_util.h>

#define POSE_LIST_SIZE 10

typedef struct
{
    lcm_t      *lcm;
    GMainLoop *mainloop;
    pthread_t  work_thread;
    char *channel;
    char *ochannel;

    int verbose; 
} state_t;

static void on_kinect(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
			     const kinect_frame_msg_t * msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 
        
    bot_core_image_t img_msg;
    img_msg.utime = msg->image.timestamp;
    img_msg.width = msg->image.width;
    img_msg.height = msg->image.height;
    
    img_msg.row_stride = msg->image.width;
    
    switch(msg->image.image_data_format){
    case KINECT_IMAGE_MSG_T_VIDEO_RGB:
        img_msg.pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB;
        break;
    case KINECT_IMAGE_MSG_T_VIDEO_BAYER:
        img_msg.pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_BAYER_BGGR;
        break;
    case KINECT_IMAGE_MSG_T_VIDEO_IR_8BIT:
        //img_msg.pixelformat = BOT_CORE_IMAGE_T_
        fprintf(stderr, "Not handled \n");
        return;
        break;
    case KINECT_IMAGE_MSG_T_VIDEO_IR_10BIT:
        //img_msg.pixelformat = BOT_CORE_IMAGE_T_
        fprintf(stderr, "Not handled \n");
        return;
        break;
    case KINECT_IMAGE_MSG_T_VIDEO_IR_10BIT_PACKED:
        //img_msg.pixelformat = BOT_CORE_IMAGE_T_
        fprintf(stderr, "Not handled \n");
        return;
        break;
    case KINECT_IMAGE_MSG_T_VIDEO_YUV_RGB:
        //img_msg.pixelformat = BOT_CORE_IMAGE_T_
        fprintf(stderr, "Not handled \n");
        return;
        break;
    case KINECT_IMAGE_MSG_T_VIDEO_YUV_RAW:
        //img_msg.pixelformat = BOT_CORE_IMAGE_T_
        fprintf(stderr, "Not handled \n");
        return;
        break;
    case KINECT_IMAGE_MSG_T_VIDEO_RGB_JPEG:
        img_msg.pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG;
        break;
    case KINECT_IMAGE_MSG_T_VIDEO_NONE:
        img_msg.pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_INVALID;
        break;
    }
            
    img_msg.size = msg->image.image_data_nbytes;
    img_msg.data = msg->image.image_data;//NULL;//(int8_t *) calloc(img_msg.size, sizeof(int8_t));
    //memcpy(img_msg.data, msg->image.image_data, img_msg.size *sizeof(int8_t));
    img_msg.nmetadata = 0;
    img_msg.metadata = NULL;
    

    bot_core_image_t_publish(s->lcm, s->ochannel, &img_msg);
}

void subscribe_to_channels(state_t *s)
{
    kinect_frame_msg_t_subscribe(s->lcm, s->channel, on_kinect, s);
    
}  


static void usage(char * funcName)
{
    printf("Usage: %s [options]\n"
	   "options are:\n"
	   "--help,        -h    display this message \n"
           "--chanel       -c    kinect channel\n"
	   "--verbose,     -v    be verbose", funcName);
    exit(1);

}

int 
main(int argc, char **argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));
    state->channel = NULL;
    state->ochannel = NULL;

    const char *optstring = "hc:vo:";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "channel", required_argument, 0, 'c' }, 
                                  { "output_channel", required_argument, 0, 'o' }, 
				  { "verbose", no_argument, 0, 'v' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    usage(argv[0]);
	    break;
	case 'c':
	    {
		//fprintf(stderr,"Add setting channel (Not implemented)\n"); 
                state->channel = strdup(optarg);
		break;
	    }
        case 'o':
	    {
                state->ochannel = strdup(optarg);
		break;
	    }
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
		state->verbose = 1;
		break;
	    }
	default:
	    {
		usage(argv[0]);
		break;
	    }
	}
    }

    if(state->ochannel == NULL){
        state->ochannel = strdup("IMAGE");
    }

    if(state->channel == NULL){
        state->channel = strdup("KINECT_FRAME");
    }
    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);

    subscribe_to_channels(state);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


