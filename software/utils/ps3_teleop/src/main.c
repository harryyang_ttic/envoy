#include <unistd.h>
#include <getopt.h>


#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
#include <lcmtypes/er_lcmtypes.h>

/****************************************

Mapping from the robot full state to the buttons on the controller 

=== Pressed === 
0 - Select
1 - Left J/S
2 - Right J/S
3 - Start 
4 - D-pad up 
5 - D-pad right 
6 - D-pad down 
7 - D-pad left
8 - L-2
9 - R-2
10 - L-1
11 - R-1
12 - Triangle
13 - Circle 
14 - Cross
15 - Square
16 - P/S button 

=== Analog (0-255) ===
17 - Left J/S horizontal 
18 - Left J/S vertical 
19 - Right J/S horizontal 
20 - Right J/S vertical 
21 - D-pad Up
22 - D-pad right
23 - D-pad down
24 - D-pad left
25 - L-2
26 - R-2
27 - L-1
28 - R-1
29 - Triangle
30 - Circle
31 - Cross
32 - Square
33 - Gyro-left/right (Roll)
34 - Gyro-up/down (Pitch)
35 - Gyro-yaw
******************************************/

//#define USE_TWO_JS
#define DEADMAN_SWITCH 10
#define SPEED_INCREASE 11
#define SPEED_DECREASE  9
#define ROT_VEL 19
#ifdef USE_TWO_JS
#define TRANS_VEL 18
#else
#define TRANS_VEL 20
#endif

#define CHECK_FOR_PERSON 13
#define START_FOLLOWING 12
#define STOP_FOLLOWING 14

#define TOGGLE_LISTENING 7

#define JS_IGNORE_THRESHOLD 20

#define MAX_NEG_TV -0.3

// Send STOP command if we have not heard from ps3 deadman switch
#define DEADMAN_SWITCH_MAX_TIMEOUT_SEC 0.5

enum VELOCITY_LEVEL{
    SPEED_20 = 0,
    SPEED_40 = 1, 
    SPEED_60 = 2, 
    SPEED_80 = 3,
    SPEED_100 = 4
};

typedef enum VELOCITY_LEVEL velocity_level_t; 

typedef struct
{
    lcm_t      *lcm;
    GMainLoop *mainloop;

    GMutex *mutex;

    guint timer_id;
    int deadman_switch_depressed;
    int64_t deadman_switch_depressed_last_utime;

    erlcm_robot_status_t *robot_status_last;

    velocity_level_t vel_level;
    int64_t time_of_last_speed_change;

    int64_t time_of_last_person_check; 
    int64_t time_of_last_start_follow; 
    int64_t time_of_last_stop_follow; 
    int sent_deadman;
    int use_deadman;
    int verbose; 
} state_t;


int change_speed(state_t *self, int up){

    if(up) {
        if(self->vel_level == SPEED_100){
            //at maximum - no change
            return 1;
        }
        else{
            //ask the wheelchair to also change speed 
            erlcm_shift_velocity_msg_t shift_msg; 
            shift_msg.utime = bot_timestamp_now(); 
            shift_msg.shift = ERLCM_SHIFT_VELOCITY_MSG_T_SHIFT_UP;
            erlcm_shift_velocity_msg_t_publish(self->lcm, "SHIFT_VELOCITY_CMD", &shift_msg);
            
            if (self->verbose)
                fprintf(stderr,"Shifting speed up\n");

            switch(self->vel_level){
                case(SPEED_20):
                    self->vel_level = SPEED_40;
                    break;
                case(SPEED_40):
                    self->vel_level = SPEED_60;
                    break;
                case(SPEED_60):
                    self->vel_level = SPEED_80;
                    break;
                case(SPEED_80):
                    self->vel_level = SPEED_100;
                    break;   
                default:
                break;
            }
            return 0;

            
        }
    }
    else{
        if(self->vel_level == SPEED_20){
            //at maximum - no change
            return 1;
        }
        else{
            erlcm_shift_velocity_msg_t shift_msg; 
            shift_msg.utime = bot_timestamp_now(); 
            shift_msg.shift = ERLCM_SHIFT_VELOCITY_MSG_T_SHIFT_DOWN;
            erlcm_shift_velocity_msg_t_publish(self->lcm, "SHIFT_VELOCITY_CMD", &shift_msg);
            
            if (self->verbose)
                fprintf(stderr,"Shifting speed up\n");

            switch(self->vel_level){
                case(SPEED_40):
                    self->vel_level = SPEED_20;
                    break;
                case(SPEED_60):
                    self->vel_level = SPEED_40;
                    break;
                case(SPEED_80):
                    self->vel_level = SPEED_60;
                    break;
                case(SPEED_100):
                    self->vel_level = SPEED_80;
                    break;   
            default:
                break;
            }
            return 0;
        }
    }
}

void publish_start_following(state_t *self){
    fprintf(stdout, "Sending command to START following person\n");

    erlcm_speech_cmd_t p_msg; 
    p_msg.utime = bot_timestamp_now();
    p_msg.cmd_type = "FOLLOWER";
    p_msg.cmd_property = "START_FOLLOWING";
    erlcm_speech_cmd_t_publish(self->lcm, "PERSON_TRACKER", &p_msg);
}

void publish_stop_following(state_t *self){
    fprintf(stdout, "Seding command to STOP following person\n");

    erlcm_speech_cmd_t p_msg; 
    p_msg.utime = bot_timestamp_now();
    p_msg.cmd_type = "FOLLOWER";
    p_msg.cmd_property = "IDLE";
    erlcm_speech_cmd_t_publish(self->lcm, "PERSON_TRACKER", &p_msg);    
}


static gboolean
on_timer (gpointer data) {
    
    state_t *self = (state_t *) data;

    g_mutex_lock (self->mutex);

    // Publish a STOP command if (the deadman's switch isn't depressed OR
    // it has been too long since hearing from the joystick) AND (the robot is
    // in RUN mode OR we have yet to receive a robot status message).

    int send_stop = 0;
    int in_stopped_state = 0;

    double dt = ((double) (bot_timestamp_now() - self->deadman_switch_depressed_last_utime)) * 1E-6;
    if (self->use_deadman && (dt > DEADMAN_SWITCH_MAX_TIMEOUT_SEC))
        send_stop = 1;

    if (self->use_deadman && self->deadman_switch_depressed == 0)
        send_stop = 1;

    if (self->robot_status_last)
        if (self->robot_status_last->state == ERLCM_ROBOT_STATUS_T_STATE_STOP)
            in_stopped_state = 1;

    if (send_stop && !in_stopped_state) {
        
        erlcm_robot_state_command_t state_cmd;
        state_cmd.utime = bot_timestamp_now();
        state_cmd.state = ERLCM_ROBOT_STATUS_T_STATE_STOP;
        state_cmd.faults = ERLCM_ROBOT_STATUS_T_FAULT_MANUAL;
        state_cmd.fault_mask =  ERLCM_ROBOT_STATUS_T_FAULT_MASK_NO_CHANGE;
        state_cmd.sender = "ps3-teleop";
        state_cmd.comment  = "deadman switch released";
        
        erlcm_robot_state_command_t_publish (self->lcm, "ROBOT_STATE_COMMAND", &state_cmd);
        
        fprintf (stdout, "Deadman's switch released: Requesting transition to STOP\n");
    }
    
    g_mutex_unlock (self->mutex);
    return TRUE;

}


static void
robot_status_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                      const erlcm_robot_status_t *msg, void *user) {
    
    state_t *self = (state_t *) user;

    g_mutex_lock (self->mutex);

    if (self->robot_status_last)
        erlcm_robot_status_t_destroy (self->robot_status_last);

    self->robot_status_last = erlcm_robot_status_t_copy (msg);

     g_mutex_unlock (self->mutex);
}


static void lcm_base_status_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                                    const erlcm_orc_full_stat_msg_t * msg, void * user  __attribute__((unused))) {
    
    state_t *self = (state_t *) user; 
    if(self->verbose)
        fprintf(stderr, "Vel Value : %d\n", (int)  msg->velocity_level);
    
    self->vel_level = (int) msg->velocity_level;
}



static void lcm_full_joystick_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), 
                                 const erlcm_joystick_full_state_t * msg, void * user  __attribute__((unused)))
{  
    state_t *self = (state_t *) user; 

    if(msg->count < 36){
        fprintf(stderr, "Not receiving the expected no of readings\n");
        return;
    }

    int deadman_switch = msg->values[DEADMAN_SWITCH];
    int speed_increase = msg->values[SPEED_INCREASE];
    int speed_decrease = msg->values[SPEED_DECREASE];
    int rot_vel =  msg->values[ROT_VEL];
    int trans_vel = msg->values[TRANS_VEL];
    int check_for_person = msg->values[CHECK_FOR_PERSON];
    int start_following = msg->values[START_FOLLOWING];
    int stop_following = msg->values[STOP_FOLLOWING];
    int toggle_listening = msg->values[TOGGLE_LISTENING];

    
    if(check_for_person){      
        if((msg->utime - self->time_of_last_person_check)/1.0e6 > 0.5){

            fprintf (stdout, "Sending command to START person tracking\n");

            erlcm_person_tracking_cmd_t p_msg;
            p_msg.utime = bot_timestamp_now();
            p_msg.command = ERLCM_PERSON_TRACKING_CMD_T_CMD_PERSON_IN_FRONT;
            p_msg.sender = ERLCM_PERSON_TRACKING_CMD_T_SENDER_JS;
            erlcm_person_tracking_cmd_t_publish(self->lcm, "PERSON_TRACKING_CMD", &p_msg);
            self->time_of_last_person_check = msg->utime;
        }
    }

    if(start_following) {
        if((msg->utime - self->time_of_last_start_follow)/1.0e6 > 0.5){
            publish_start_following(self);
            self->time_of_last_start_follow = msg->utime;
        }
    }

    if(stop_following){
        if((msg->utime - self->time_of_last_stop_follow)/1.0e6 > 0.5){
            publish_stop_following(self);
            self->time_of_last_stop_follow = msg->utime;
        }
    }

    if (toggle_listening) {
        erlcm_comment_t t_msg;
        t_msg.utime = bot_timestamp_now();
        t_msg.comment = "Toggle listening state";
        erlcm_comment_t_publish (self->lcm, "TOGGLE_LISTENING_STATE", &t_msg);

        fprintf (stdout, "Toggling listening state\n");
    }
    /*
      struct joystick_full_state_t
      {
      int64_t utime;
      int16_t count;
      int16_t values[count];
      }
     */


    // Require that deadman switch be depressed to be in RUN state
    if (self->use_deadman){
        
        g_mutex_lock (self->mutex);
        self->deadman_switch_depressed_last_utime = bot_timestamp_now();
        self->deadman_switch_depressed = deadman_switch;
        
        if (self->robot_status_last) {

            if (deadman_switch) {
            
                // Publish the command to transition to RUN if we are in the STOP or MANUAL states
                if (self->robot_status_last->state == ERLCM_ROBOT_STATUS_T_STATE_STOP ||
                    self->robot_status_last->state == ERLCM_ROBOT_STATUS_T_STATE_MANUAL) {
                    
                    erlcm_robot_state_command_t state_cmd;
                    state_cmd.utime = bot_timestamp_now();
                    state_cmd.state = ERLCM_ROBOT_STATUS_T_STATE_RUN;
                    state_cmd.faults = ERLCM_ROBOT_STATUS_T_FAULT_NONE;
                    state_cmd.fault_mask = ERLCM_ROBOT_STATUS_T_FAULTS_OK_IN_RUN;
                    state_cmd.sender = "ps3-teleop";
                    state_cmd.comment  = "deadman switch depressed";

                    erlcm_robot_state_command_t_publish (self->lcm, "ROBOT_STATE_COMMAND", &state_cmd);
                    
                    fprintf (stdout, "Deadman's switch depressed: Requesting transition to RUN\n");
                }
            } else {
                
                if (self->robot_status_last->state == ERLCM_ROBOT_STATUS_T_STATE_RUN) {
                    erlcm_robot_state_command_t state_cmd;
                    state_cmd.utime = bot_timestamp_now();
                    state_cmd.state = ERLCM_ROBOT_STATUS_T_STATE_STOP;
                    state_cmd.faults = ERLCM_ROBOT_STATUS_T_FAULT_MANUAL;
                    state_cmd.fault_mask =  ERLCM_ROBOT_STATUS_T_FAULT_MASK_NO_CHANGE;
                    state_cmd.sender = "ps3-teleop";
                    state_cmd.comment  = "deadman switch released";

                    erlcm_robot_state_command_t_publish (self->lcm, "ROBOT_STATE_COMMAND", &state_cmd);
                    
                    fprintf (stdout, "Deadman's switch released: Requesting transition to STOP\n");
                }
            }
        }
        g_mutex_unlock (self->mutex);
    }
    // Otherwise the deadman can be used to switch between RUN and STOP
    else if (self->robot_status_last && deadman_switch) {
        // Publish the command to transition to RUN if we are in the STOP or MANUAL states
        if (self->robot_status_last->state == ERLCM_ROBOT_STATUS_T_STATE_STOP ||
            self->robot_status_last->state == ERLCM_ROBOT_STATUS_T_STATE_MANUAL) {
            
            erlcm_robot_state_command_t state_cmd;
            state_cmd.utime = bot_timestamp_now();
            state_cmd.state = ERLCM_ROBOT_STATUS_T_STATE_RUN;
            state_cmd.faults = ERLCM_ROBOT_STATUS_T_FAULT_NONE;
            state_cmd.fault_mask = ERLCM_ROBOT_STATUS_T_FAULTS_OK_IN_RUN;
            state_cmd.sender = "ps3-teleop";
            state_cmd.comment  = "ps3: switch to RUN";
            
            erlcm_robot_state_command_t_publish (self->lcm, "ROBOT_STATE_COMMAND", &state_cmd);
        }
        else if (self->robot_status_last->state == ERLCM_ROBOT_STATUS_T_STATE_RUN) {
            erlcm_robot_state_command_t state_cmd;
            state_cmd.utime = bot_timestamp_now();
            state_cmd.state = ERLCM_ROBOT_STATUS_T_STATE_STOP;
            state_cmd.faults = ERLCM_ROBOT_STATUS_T_FAULT_MANUAL;
            state_cmd.fault_mask =  ERLCM_ROBOT_STATUS_T_FAULT_MASK_NO_CHANGE;
            state_cmd.sender = "ps3-teleop";
            state_cmd.comment  = "ps3: switch to STOP";
            
            erlcm_robot_state_command_t_publish (self->lcm, "ROBOT_STATE_COMMAND", &state_cmd);
            
        }
    }
    

    int publish_velocity = 0;
    if (self->robot_status_last && ((self->robot_status_last->state == ERLCM_ROBOT_STATUS_T_STATE_STOP) || (self->robot_status_last->state == ERLCM_ROBOT_STATUS_T_STATE_MANUAL)))
      publish_velocity = 1;

    // We use the L1 button as the dead man's switch for the person follower
    /* if (self->use_deadman) { */
    /*     if (deadman_switch) { */
    /*         if(!self->sent_deadman){ */
    /*             publish_start_following(self); */
    /*             self->sent_deadman = 1;  */
    /*         } */
    /*         if (self->verbose) { */
    /*             if ( (abs(rot_vel - 128) > JS_IGNORE_THRESHOLD) || (abs(trans_vel - 127) > JS_IGNORE_THRESHOLD) ) */
    /*                 fprintf (stderr, "L1 button depressed (dead man's switch) - Ignoring commanded velocities\n"); */
    /*         } */
    /*         publish_velocity = 0; */
    /*     } */
    /* } */
    /* else{ */
    /*     if(self->sent_deadman){ */
    /*         publish_stop_following(self); */
    /*         self->sent_deadman = 0; */
    /*     } */
    /* } */

    if(self->verbose && publish_velocity){
        fprintf(stderr,"Received LCM velocity command - from J/S \n");
        fprintf(stderr, "%d, %d\n", rot_vel, trans_vel);
    }
    
    //right up/down is used to change the speed setting 
    if(speed_increase || speed_decrease){
        //we only allow speed changes at a rate of one per second (least for now)      
        if((msg->utime - self->time_of_last_speed_change)/1.0e6 > 0.5){
	  fprintf(stderr, "Change speed heard : %d - %d\n", speed_increase, speed_decrease);
            if(speed_increase){
                if(!change_speed (self, 1))
                    self->time_of_last_speed_change = msg->utime;
            }
            else{
                if(!change_speed (self, 0))
                    self->time_of_last_speed_change = msg->utime;
            }
        }
    }
    
    if (publish_velocity) {
        double RV_MAX = 2.0;
        double TV_MAX = 1.2;
        double RV_range[5] = {RV_MAX/5, RV_MAX*2/5, RV_MAX*3/5, RV_MAX*4/5, RV_MAX};
        double TV_range[5] = {TV_MAX/5, TV_MAX*2/5, TV_MAX*3/5, TV_MAX*4/5, TV_MAX};
        
        //considering the right j/s only (least for now) 
        //this is not the same though - this caps the maximum velocity (not prevent the chair from jumping up and down in the velocity ranges) 
        
        double rv_val = fmax(0, fabs(rot_vel - 128) - JS_IGNORE_THRESHOLD) / 127.0 * RV_range[self->vel_level];

        double tv_val = fmax(0, fabs(trans_vel - 127) - JS_IGNORE_THRESHOLD) / 127.0 * TV_range[self->vel_level];
        
        int rv_sign = 1; 
        if((rot_vel - 128) > 0){
            rv_sign = -1; 
        }
        
        int tv_sign = 1; 
        if((trans_vel - 127) > 0){
            tv_sign = -1; 
        }
        
        double rv =  rv_sign * rv_val;
        
        double tv = tv_sign * tv_val;
	if(tv < 0){
	  tv = fmax(tv, MAX_NEG_TV);
	}

        /*
        
        double rv =  -(msg->js_right_value[0] - 128) / 127.0 *RV_range[self->vel_level];// 2.0; //M_PI; //scaled to 0-1 * Max
        double tv = fmax(-(msg->js_right_value[1] - 127) / 127.0, 0) * TV_range[self->vel_level];//1.2;  //scaled to 0-1 * Max 
        
        //ignore drift 
        if(fabs(rv) < 0.1 || fabs(msg->js_right_value[0] - 128) < 10)
            rv = 0;
        if(fabs(tv) < 0.1 || fabs(msg->js_right_value[1] - 128) < 10)
            tv = 0;
        */
        if(self->verbose)
            fprintf(stderr, "Tv : %f Rv : %f\n" , tv, rv);
        
        erlcm_velocity_msg_t v;
        
        v.tv = tv;
        v.rv = rv;
        v.utime = bot_timestamp_now();
        erlcm_velocity_msg_t_publish(self->lcm,"ROBOT_VELOCITY_CMD_JS",&v);
    }

    //this is for the head position 
    //considering the left j/s for the head commands 
    //can move max of 5 degrees??
    //disabled for now 
#ifndef USE_TWO_JS

#endif
    
    //publish_vector_status(s, 0, 0);
}




static void usage (int argc, char ** argv)
{
    fprintf(stderr, "Usage: %s [options]\n"
            "PS3 Controller Wheelchair Teleop\n"
            "\n"
            "    -d, --disable-deadman  operate without requiring deadman switch (L1) to be depressed\n"
            "                              in this mode, the switch toggles the RUN/STOP state\n"
            "    -h, --help             print this help and exit"
            "\n\n", argv[0]);
}


int 
main(int argc, char **argv)
{
    setlinebuf (stdout);
    state_t *self = (state_t*) calloc(1, sizeof(state_t));

    self->use_deadman = 1;
    self->vel_level = SPEED_80;

    const char *optstring = "hd";
    struct option long_opts[] = { 
        { "help", no_argument, 0, 'h'},
        { "disable-deadman", no_argument, 0, 'd'},
        { 0, 0, 0, 0 } };
    
    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
        case 'd':
            self->use_deadman = 0;
            break;
        case 'h':
        case '?':
            usage (argc, argv);
            return 1;
        }
    }
    

    if (!self->use_deadman)
        fprintf (stdout, "Warning: Deadman switch (L1) will be ignored\n");

    self->lcm =  bot_lcm_get_global(NULL);
    if (!self->lcm) {
        fprintf (stderr, "Error getting LCM\n");
        return -1;
    }

    erlcm_orc_full_stat_msg_t_subscribe (self->lcm, "BASE_FULL_STAT", lcm_base_status_handler, self);
    erlcm_joystick_full_state_t_subscribe (self->lcm, "PS3_JS_FULL_CMD", lcm_full_joystick_handler, self);
    erlcm_robot_status_t_subscribe (self->lcm, "ROBOT_STATUS", robot_status_handler, self);

    // Timer to publis robot state command messages based on deadman value
    if (self->use_deadman)
        self->timer_id = g_timeout_add (500, on_timer, self);

    self->mutex = g_mutex_new();

    self->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!self->mainloop) {
        fprintf(stderr, "Couldn't create main loop\n");
        return -1;
    }

    bot_glib_mainloop_attach_lcm (self->lcm);

    bot_signal_pipe_glib_quit_on_kill (self->mainloop);
    
    g_main_loop_run(self->mainloop);
  
    bot_glib_mainloop_detach_lcm(self->lcm);
}


