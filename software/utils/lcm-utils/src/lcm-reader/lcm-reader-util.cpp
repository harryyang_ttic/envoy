#include "lcm-reader-util.hpp"

LCMLogReader::LCMLogReader () {
    last_utime = 0;
    frame_num = 0; 
    log = NULL;
    event = NULL;
}

LCMLogReader::~LCMLogReader() { 
    if (log) delete log;

    // if (state) 
    //     delete state;
}

inline int64_t LCMLogReader::find_closest_utime(int64_t utime) { 
    printf("find_closest_utime: %lli\n",utime);
    std::map<int64_t, int64_t>::iterator it = utime_map.lower_bound(utime);
    if (it == utime_map.begin() || it->first == utime) { 
        printf("closest upper: %lli\n", it->first);
        return it->second;
    }
    std::map<int64_t, int64_t>::iterator it2 = it;
    it2--;
    if (it2 == utime_map.end() || ((utime - it2->first)  < (it->first - utime))) { 
        printf("closest lower: %lli\n", it2->first);
        return it2->second;
    }
    printf("closest upper: %lli\n",it->first);
    return it->second;
}

void LCMLogReader::init_index() { 
    std::cerr << "Init indexing" << std::endl;
    // read all events and create map
    // lcm_eventlog_event_t* ev = lcm_eventlog_read_next_event (log);

    int count = 0; 
    for (event = log->readNextEvent(); event != NULL; 
         event = log->readNextEvent()) { 
        if (options.ch.length() && options.ch == event->channel) { 

            // Decode msg
            kinect::frame_msg_t msg; 
            if(msg.decode(event->data, 0, event->datalen) != event->datalen)
                continue;
            utime_map[int64_t(msg.timestamp)] = int64_t(event->timestamp);

            
            count++;
            // debug
            if (count %100 == 0) { 
                std::cerr << "Indexed " << count << "frames:  " 
                          << msg.timestamp << "->" << event->timestamp << std::endl;
            }
        }
    } 
    // Seek back to first timestamp
    int ret = log->seekToTimestamp(int64_t(utime_map.begin()->second));
    std::cerr << "Done indexing" << std::endl;
}

// // for python wrapper
// void LCMLogReader::init (char* _fn) { 
//     std::string fn(_fn);
//     LCMLogReaderOptions options;

//     //----------------------------------
//     // Log player options
//     //----------------------------------
//     options.lcm =  bot_lcm_get_global(NULL);
//     options.fn = fn;
//     options.ch = "KINECT_FRAME";
//     options.fps = 1000.f;
//     options.handler = _on_kinect_image_frame;
//     options.user_data = NULL;

//     //----------------------------------
//     // State
//     //----------------------------------
//     state = new logplayer_state_t();
//     bool flip_coords = false;
//     state->lcm = options.lcm;
//     // state->pc_lcm = new pointcloud_lcm(state->lcm, false);
//     // state->pc_lcm->set_decimate(1.f);
//     // // state->pc_lcm->set_decimate(1.f);  // no SCALE (ing)
//     // state->pc_vis = new pointcloud_vis(state->lcm);

//     init(options, false);

//     return;
// }

// for c++ calls
void LCMLogReader::init (const LCMLogReaderOptions& _options, bool sequential_read) { 
    // log player options
    options = _options;

    // open log
    log = new lcm::LogFile(options.fn, "r");
    if (!log) {
        fprintf (stderr, "Unable to open source logfile %s\n", options.fn.c_str());
        return;
    }

    std::cerr << "===========  LCM LOG PLAYER ============" << std::endl;
    std::cerr << "=> LOG FILENAME : " << options.fn << std::endl;
    std::cerr << "=> CHANNEL : " << options.ch << std::endl;
    std::cerr << "=> FPS : " << options.fps << std::endl;
    std::cerr << "=> LCM PTR : " << (options.lcm != 0) << std::endl;
    std::cerr << "=> USER_DATA PTR : " << (options.user_data != 0) << std::endl;
    std::cerr << "=> START FRAME : " << options.start_frame << std::endl;
    std::cerr << "=> END FRAME : " << options.end_frame << std::endl;
    // std::cerr << "=> Note: Hit 'space' to proceed to next frame" << std::endl;
    // std::cerr << "=> Note: Hit 'p' to proceed to previous frame" << std::endl;
    // std::cerr << "=> Note: Hit 'n' to proceed to previous frame" << std::endl;
    std::cerr << "===============================================" << std::endl;

    if (!options.user_data) { std::cerr << "WARNING USER DATA NOT SET!!!" << std::endl; } 

    // Set usleep interval
    usleep_interval = 1.f / options.fps * 1e6;

    if (sequential_read)
        getNextFrame(); // start
    else { 
        init_index();
    }
}

bool LCMLogReader::getNextFrame() { 
    assert(good());
    // assert(options.handler);

    // get next event from log
    event = log->readNextEvent();
    if (event != NULL) { 
        if (options.ch.length() && options.ch == event->channel) { 
            // Handle start/end frames
            std::cerr << options.start_frame << " " << options.end_frame << " " << frame_num << std::endl;
            if ((options.end_frame < 0) || 
                (options.start_frame <= frame_num && options.end_frame >= frame_num)) { 
                // Decode msg
                kinect::frame_msg_t msg; 
                if(msg.decode(event->data, 0, event->datalen) != event->datalen)
                    return false;

                // HACK recv_buf_t NULL??
                std::cerr << "Seeking: " << msg.timestamp << std::endl;
                options.handler(NULL, event->channel, &msg);
                // on_kinect_image_frame(NULL, event->channel, &msg, options.user_data);
            } else { 
                std::cerr << "lcm-reader-util: Processed " << frame_num << " frames: exiting! " << std::endl;
                return true;
            }
            frame_num++;            
        }
    } else { 
        return false;
    }

        
    // Get next frame
    if (options.fps > 0.f) { 
        usleep(usleep_interval);
        getNextFrame();
    } else  { 
        while(1) { 
            unsigned char c = cv::waitKey(10) & 0xff;
            if (c == 'q') break;  
            else if ( c == ' ' ) getNextFrame();
        }
    }
    return true;
}

bool LCMLogReader::getFrame(int64_t sensor_utime, uint8_t** img_data, uint32_t* sz, float** cloud_data) { 
    assert(good());
    // assert(options.handler);

    // // printf("getFrame: %lli\n", sensor_utime);
    // int64_t event_utime = find_closest_utime(sensor_utime);
    // // printf("seek to : %lli\n", event_utime);
    // // get next event from log
    // int ret = lcm_eventlog_seek_to_timestamp(log, event_utime);
    // if (ret == 0) { 

    //     // run until th
    //     for (event = lcm_eventlog_read_next_event(log); event != NULL; 
    //          event = lcm_eventlog_read_next_event(log)) { 
    //         if (options.ch.length() && strcmp(options.ch.c_str(), event->channel) == 0) { 
    //             // Decode msg
    //             kinect_frame_msg_t msg; 
    //             memset (&msg, 0, sizeof (kinect_frame_msg_t));
    //             kinect_frame_msg_t_decode(event->data, 0, event->datalen, &msg);


    //             // Check msg type
    //             if (msg.depth.depth_data_format != KINECT_DEPTH_MSG_T_DEPTH_MM) { 
    //                 std::cerr << "Warning: kinect data format is not KINECT_DEPTH_MSG_T_DEPTH_MM" << std::endl 
    //                           << "Program may not function properly" << std::endl;
    //             }
    //             assert (msg.depth.depth_data_format == KINECT_DEPTH_MSG_T_DEPTH_MM);

    //             //----------------------------------
    //             // Unpack Data
    //             //----------------------------------
    //             double t1 = bot_timestamp_now();
    //             if (img.empty())
    //                 img.create(msg.image.height, msg.image.width, CV_8UC3);
    //             // if (depth_img.empty())
    //             //     depth_img.create(msg.image.height, msg.image.width, CV_32FC1);
    //             if (cloud.empty())
    //                 cloud.create(msg.image.height, msg.image.width, CV_32FC3);

    //             // pcl::PointCloud<pcl::PointXYZRGB>::Ptr p = 
    //             //     pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>());
    //             // state->pc_lcm->unpack_kinect_frame(&msg, img.data, p);

    //             cv::Mat_<Vec3d> cloud;
    //             opencv_utils::unpack_kinect_frame(msg, img, cloud);
    //             printf("===> UNPACK TIME: %4.2f ms\n", (bot_timestamp_now() - t1) * 1e-3);                     

    //             //----------------------------------
    //             // Unpack Point cloud
    //             //----------------------------------
    //             float* cptr = (float*)cloud.data;
    //             for (int y=0, j=0; y<cloud.rows; y++) {
    //                 for (int x=0; x<cloud.cols; x++, j++) {
    //                     Vec3d& vec = cloud.at<Vec3d>(y,x);
    //                     cptr[j*3] = vec[0], cptr[j*3+1] = vec[1], cptr[j*3+2] = vec[2]; 
    //                 }
    //             }

    //             // printf("match frame: %lli %lli\n",msg.timestamp, sensor_utime);
    //             // printf("msg: %i %i\n", msg.image.height, msg.image.width); 
    //             assert(msg.timestamp == sensor_utime);
                           
    //             *img_data = (uchar*)img.data;
    //             *cloud_data = (float*)cloud.data;
    //             // printf("data: %p %i\n", data, img.data); 
    //             sz[0] = img.rows, sz[1] = img.cols, sz[2] = img.channels();
            
    //             kinect_frame_msg_t_decode_cleanup (&msg);
    //             lcm_eventlog_free_event(event);
    //             break;
    //         }
    //     }
    //     return true;
    // }
    return false;
}
