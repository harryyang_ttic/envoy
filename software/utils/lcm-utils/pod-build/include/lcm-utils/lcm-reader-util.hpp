#ifndef LCM_READER_UTIL_HPP_
#define LCM_READER_UTIL_HPP_

// lcm
#include <lcm/lcm-cpp.hpp>
#include <lcmtypes/bot2_param.h>

// libbot includes
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <ConciseArgs>

// kinect frame
#include <lcmtypes/kinect.hpp>
#include <kinect/kinect-utils.h>

// opencv includes
#include <opencv2/opencv.hpp>

typedef void (*kinect_frame_msg_t_handler_cpp) (const lcm::ReceiveBuffer* rbuf, 
                                const std::string& chan,
                                const kinect::frame_msg_t *msg);


struct logplayer_state_t {
    lcm::LCM lcm;            
};

struct LCMLogReaderOptions { 
    std::string fn;    // Filename
    std::string ch;    // Specific channel subscribe
    float fps;
    int start_frame, end_frame;

    lcm::LCM* lcm;
    kinect_frame_msg_t_handler_cpp handler;
    
    void* user_data;    // user data

    LCMLogReaderOptions () { 
        fn = "";
        ch = "";

        // default starts and ends as expected
        start_frame = 0, end_frame = -1; 

        lcm = NULL;
        user_data = NULL;
    }    
}; 

class LCMLogReader { 

public: 
    LCMLogReaderOptions options;

    logplayer_state_t* state; // state

    lcm::LogFile* log; 
    const lcm::LogEvent* event;
    int frame_num;

    int64_t last_utime;
    double usleep_interval;

    std::map<int64_t, int64_t> utime_map; // sensor to log utime map

    cv::Mat img, depth_img, cloud; 

    LCMLogReader();
    ~LCMLogReader();
    int64_t find_closest_utime(int64_t utime);
    void init_index();
    void init (char* _fn);
    void init (const LCMLogReaderOptions& _options, bool sequential_read = true);
    bool good() { return (log->good() && options.ch != ""); }
    bool getNextFrame();
    bool getFrame(int64_t sensor_utime, uint8_t** img_data, uint32_t* sz, float** cloud_data);
};

#endif // LCM_READER_UTIL_HPP


// // pcl includes
// #include <pcl/io/pcd_io.h>
// #include <pcl/PointIndices.h>
// #include <pcl/point_types.h>
// #include <pcl/filters/extract_indices.h>
// #include <pcl/filters/random_sample.h>
// #include <pcl/kdtree/kdtree.h>
// #include <pcl/kdtree/kdtree_flann.h>
// #include <pcl/segmentation/extract_clusters.h>
// #include <pcl/features/normal_3d.h>
// #include <pcl/features/integral_image_normal.h>
// #include <pcl/surface/mls.h>

// pcl includes for kinect
// #include <pointcloud_tools/pointcloud_lcm.hpp>
// #include <pointcloud_tools/pointcloud_vis.hpp>

// // // default handler receiving images
// static void _on_kinect_image_frame (const lcm_recv_buf_t *rbuf, const char *channel,
//                                        const kinect_frame_msg_t *msg, 
//                                        void *user_data ) {
//     double tic; 

//     // Check msg type
//     // state_t *state = (state_t*) user_data;
//     if (msg->depth.depth_data_format != KINECT_DEPTH_MSG_T_DEPTH_MM) { 
//         std::cerr << "Warning: kinect data format is not KINECT_DEPTH_MSG_T_DEPTH_MM" << std::endl 
//                   << "Program may not function properly" << std::endl;
//     }
//     assert (msg->depth.depth_data_format == KINECT_DEPTH_MSG_T_DEPTH_MM);

//     //----------------------------------
//     // Unpack Point cloud
//     //----------------------------------
//     double t1 = bot_timestamp_now();
//     cv::Mat3b img(msg->image.height, msg->image.width);
//     cv::Mat_<float> depth_img = cv::Mat_<float>(img.size());

//     // opencv_utils::unpack_kinect_frame(msg, img);

//     // state->pc_lcm->unpack_kinect_frame(msg, img.data, depth_img.data, cloud);
//     // cv::resize(img, rgb_img, cv::Size(int(img.cols / SCALE), int(img.rows / SCALE)));
//     // printf("%s===> UNPACK TIME: %4.2f ms%s\n", esc_yellow, (bot_timestamp_now() - t1) * 1e-3, esc_def); 
//     // std::cerr << "cloud: " << cloud->points.size() << std::endl;
//     // std::cerr << "rgb: " << rgb_img.size() << std::endl;

//     //--------------------------------------------
//     // Convert Color
//     //--------------------------------------------
//     // cvtColor(rgb_img, rgb_img, CV_RGB2BGR);

//     return;
// }

