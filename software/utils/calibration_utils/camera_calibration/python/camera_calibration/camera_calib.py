#! /usr/bin/env python

# Author: Sudeep Pillai (spillai@mit.edu)
# Compute the camera intrinsics/extrinsics, projector intrinsics/extrinsics w.r.t. the camera

# Compute camera intrinsics only
# projector_camera_calib.py <camera_images>

# Compute camera intrinsics, projector intrinsics, and camera to projector extrinsics
# 3rd argument <camera+proj_image> is used for the single shot extrinsic 
# projector_camera_calib.py <camera_image(s)> <camera+proj_image(s)> <camera+proj_image> <projected calibration pattern>

import sys
import numpy as np
import cv
import cv2
import os
from glob import glob
from matplotlib import pyplot
from mpl_toolkits.mplot3d import Axes3D
from numpy import sin, cos
from math import sqrt
global im, calib_mode;

# some default constants
proj_yres = 800;
proj_xres = 1280;
camera_size = 100;
pattern_size = ( 8, 6 )
square_size = 58.7375; # in mm
formats = ['.png', '.jpg', '.jpeg', '.bmp', '.pgm']
calib_mode = None;

printed_chessboard_corners = None;
printed_chessboard_fn = None;

#global figure, ax; 
figure = pyplot.figure()
ax = Axes3D(figure)
ax.set_title('Projector Camera Calibration')

figure2 = pyplot.figure()
ax2 = Axes3D(figure2)
ax2.set_title('Camera Calibration - Camera centered view')

figure3 = pyplot.figure()
ax3 = Axes3D(figure3)
ax3.set_title('Camera Calibration - World centered view')

figure4 = pyplot.figure()
ax2d = figure4.add_subplot(111);
ax2d.set_title('Reprojection Error')

corner_finder_cache = {}

def colored(string, color='reset'): 
    colors = {'reset':'\033[0m', 'blue':'\033[36m', 'red':'\033[31m', 'yellow':'\033[33m', 'white':'\033[37m', 'green':'\033[32m'}
    return '%s%s%s' %(colors[color], string, colors['reset'])

class calib_data: 
    def __init__(self, img_pts, obj_pts, rvec, tvec): 
        self.img_pts = img_pts;
        self.obj_pts = obj_pts;
        self.rvec = rvec;
        self.tvec = tvec;
        self.obj_pts_global = None;

        self.R, jac = cv2.Rodrigues(self.rvec);

class proj_calib_data: 
    def __init__(self, cam_img_pts, proj_img_pts, projected_chess_pts, cam_obj_pts, proj_obj_pts, 
                 cam_rvec, cam_tvec): 
        self.cam_img_pts = cam_img_pts;
        self.proj_img_pts = proj_img_pts;
        self.projected_chess_pts = projected_chess_pts;
        self.cam_obj_pts = cam_obj_pts;        
        self.proj_obj_pts = proj_obj_pts;
        self.cam_rvec = cam_rvec;
        self.cam_tvec = cam_tvec;

        self.cam_obj_pts_np = np.array(self.cam_obj_pts)[0];
        self.proj_obj_pts_np = np.array(self.proj_obj_pts)[0];
        self.cam_img_pts_np = np.array(self.cam_img_pts)[0];
        self.proj_img_pts_np = np.array(self.proj_img_pts)[0];

        self.cam_rvec_np = np.array(self.cam_rvec);
        self.cam_tvec_np = np.array(self.cam_tvec);
        self.cam_R_np, jac = cv2.Rodrigues(self.cam_rvec_np);

        self.cam_obj_pts_global = None;
        self.proj_obj_pts_global = None;

def basename(fn): 
    # Draw origin for board
    path, fn_ = os.path.split(fn)
    name, ext = os.path.splitext(fn_)
    return name

def isimage(fn): 
    for f in formats: 
        if f in fn and '.pks' not in fn: 
            return True
    return False

def find_min_max(corners, w, h): 
    xmin = w; xmax = 0; ymin = h; ymax = 0;
    for c in corners: 
        x, y = c[0];
        if x < xmin: xmin = x;
        if y < ymin: ymin = y;
        if x > xmax: xmax = x;
        if y > ymax: ymax = y;

    return xmin, xmax, ymin, ymax

def find_mean(corners, w, h): 
    xmin, xmax, ymin, ymax = find_min_max(corners, w, h);
    return int((xmin + xmax)*.5), int((ymin + ymax)*.5)

def convert_corners(corners): 
    corners_cv = cv.CreateMat(1, pattern_size[0] * pattern_size[1], cv.CV_32FC2)
    for i in range(pattern_size[0] * pattern_size[1]):
        corners_cv[0, i] = (corners[i][0][0], corners[i][0][1])
    return corners_cv

def convert_points(points): 
    r, c = points.shape[:2]
    points_cv = cv.CreateMat(1, r, cv.CV_32FC3)
    for i in range(r):
        x,y,z = points[i]
        points_cv[0, i] = (x,y,z)
    return points_cv

def generate_object_points(): 
    object_points = cv.CreateMat(1, pattern_size[0] * pattern_size[1], cv.CV_32FC3)
    for y in range(pattern_size[1]):
        for x in range(pattern_size[0]):
            object_points[0, y*pattern_size[0] + x] = (x * square_size, y * square_size, 0.0)
    return object_points

def generate_projector_image_points(): 
    points = cv.CreateMat(1, pattern_size[0] * pattern_size[1], cv.CV_32FC3)
    for y in range(pattern_size[1]):
        for x in range(pattern_size[0]):
            object_points[0, y*pattern_size[0] + x] = (x * square_size, y * square_size, 0.0)
    return object_points

def write_image(im3, fn): 
    # Write to debug folder
    path, fn_ = os.path.split(fn)
    name, ext = os.path.splitext(fn_)
    debug_dir = os.path.join(os.path.join(path, 'debug'))
    if not os.path.exists(debug_dir): 
        os.mkdir(debug_dir);
    debug_fn = '%s/%s_chess.png' % (debug_dir, name)
    #print debug_fn, name
    cv2.imwrite(debug_fn, im3)

def flip_corners(corners): 
    np_corners = np.array(corners)[0];
    flipped_np_corners = np.flipud(np_corners);
    r, c = flipped_np_corners.shape[:2]
    corners_cv = cv.CreateMat(1, r, cv.CV_32FC2)
    for i in range(r):
        corners_cv[0, i] = (flipped_np_corners[i,0], flipped_np_corners[i,1])
    return corners_cv;

def align_corners(corners): 
    r,c = corners.shape[:2]
    x1,y1 = corners[0][0];
    x2,y2 = corners[r-1][0];
    if x1 > x2: 
        return np.array(np.flipud(corners))
    else:
        return corners

def write_to_file(fn, corners): 
    # Write to debug folder
    path, fn_ = os.path.split(fn)
    name, ext = os.path.splitext(fn_)
    debug_dir = os.path.join(os.path.join(path, 'debug'))
    if not os.path.exists(debug_dir): 
        os.mkdir(debug_dir);
    debug_fn = '%s/%s.pks' % (debug_dir, name)

    pksfile = open(debug_fn,"w")
    for i in range(pattern_size[0] * pattern_size[1]):
        pksfile.write('%f,%f\n' % (corners[i][0][0], corners[i][0][1]))
    pksfile.close()

def read_from_file(fn, im3): 
    global corner_finder_cache

    # Write to debug folder
    path, fn_ = os.path.split(fn)
    name, ext = os.path.splitext(fn_)
    debug_dir = os.path.join(os.path.join(path, 'debug'))
    if not os.path.exists(debug_dir): 
        os.mkdir(debug_dir);
    debug_fn = '%s/%s.pks' % (debug_dir, name)

    corners_cv = cv.CreateMat(1, pattern_size[0] * pattern_size[1], cv.CV_32FC2)

    if not os.path.exists(debug_fn): 
        return;

    pksfile = open(debug_fn,"r")
    
    i=0
    while pksfile: 
        vals = pksfile.readline().split(',');
        if len(vals) != 2: 
            break;
        #print vals
        x,y = float(vals[0]),float(vals[1])
        corners_cv[0, i] = (x,y)
        i+=1;

    corner_finder_cache[fn] = corners_cv, None, im3

def find_corners(im, fn, both=True):
    global corner_finder_cache

    im3 = cv2.cvtColor(im, cv2.COLOR_GRAY2BGR)

    read_from_file(fn, im3);

    if fn in corner_finder_cache: 
        return corner_finder_cache[fn]

    h, w = im.shape[:2];
    mask = np.zeros((h+2, w+2), np.uint8)
    
    # Chessboard detection
    found_all1, corners1 = cv2.findChessboardCorners( im, pattern_size )
    if found_all1:
        term = ( cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.1 )
        cv2.cornerSubPix(im, corners1, (5, 5), (-1, -1), term)
        corners1 = align_corners(corners1);
        cv2.drawChessboardCorners( im3, pattern_size, corners1, found_all1 )

    found_all2 = None;
    if both == True: 
        # Find min, max of chessboard 1
        xmin, xmax, ymin, ymax = find_min_max(corners1, w, h);
        chess_w = xmax - xmin;
        chess_h = ymax - ymin;

        # Remove detected chessboard to detect the second one
        im_sub = im.copy();
        im_sub[int(ymin-.2*chess_h):int(ymax+.2*chess_h), int(xmin-.2*chess_w):int(xmax+.2*chess_w)] = 0;
    
        # Chessboard detection
        found_all2, corners2 = cv2.findChessboardCorners( im_sub, pattern_size )
        if found_all2:
            term = ( cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.1 )
            cv2.cornerSubPix(im_sub, corners2, (5, 5), (-1, -1), term)
            corners2 = align_corners(corners2);
            cv2.drawChessboardCorners( im3, pattern_size, corners2, found_all2 )

    if not found_all1 and not found_all2: 
        print colored('No chessboards found', 'red')
        return None, None, None
    elif found_all1 and not found_all2: 
        write_image(im3, fn)
        corner_finder_cache[fn] = convert_corners(corners1), None, im3
        write_to_file(fn, corners1)
        return corner_finder_cache[fn]
        
    # Assume left checkerboard is printed, and right one is projected
    chx1, chy1 = find_mean(corners1, w, h);
    chx2, chy2 = find_mean(corners2, w, h);

    printed_corners = None; 
    projected_corners = None;
    if chx1 < chx2: 
        printed_corners = corners1;
        projected_corners = corners2;
    else: 
        printed_corners = corners2;
        projected_corners = corners1;

    write_image(im3, fn)
    corner_finder_cache[fn] = convert_corners(printed_corners), convert_corners(projected_corners), im3
    write_to_file(fn, corners1)
    return corner_finder_cache[fn]

def plot_camera_T(ax, R, T, label, color='blue'):
    Rt = cv2.transpose(R);

    back_end = np.array([
            [0, 0, -150],
            [0,  0, -150],
            [0,  0, -150],
            [ 0, 0, -150],
            [0, 0, -150],
            ]) 

    front_end = np.array([
            [-70, -70,  0],
            [-70,  70,  0],
            [ 70,  70,  0],
            [ 70, -70,  0],
            [-70, -70,  0],
            ]) 

    xaxis = np.array([[0, 0, 0],[150, 0, 0]]) 
    yaxis = np.array([[0, 0, 0],[0, 150, 0]]) 
    zaxis = np.array([[0, 0, 0],[0, 0, 150]]) 

    r,c = back_end.shape[:2]
    for i in range(r): 
        back_end[i,:] = np.dot(Rt[0:3, 0:3], back_end[i,:])
        back_end[i,:] += np.dot(-Rt[0:3, 0:3], T[0:3]);

    r,c = front_end.shape[:2]
    for i in range(r): 
        front_end[i,:] = np.dot(Rt[0:3, 0:3], front_end[i,:])
        front_end[i,:] += np.dot(-Rt[0:3, 0:3], T[0:3]);

    r,c = xaxis.shape[:2]
    for i in range(r): 
        xaxis[i,:] = np.dot(Rt[0:3, 0:3], xaxis[i,:])
        xaxis[i,:] += np.dot(-Rt[0:3, 0:3], T[0:3]);
        yaxis[i,:] = np.dot(Rt[0:3, 0:3], yaxis[i,:])
        yaxis[i,:] += np.dot(-Rt[0:3, 0:3], T[0:3]);
        zaxis[i,:] = np.dot(Rt[0:3, 0:3], zaxis[i,:])
        zaxis[i,:] += np.dot(-Rt[0:3, 0:3], T[0:3]);

    ax.plot(back_end[:,0], back_end[:,2], zs=-back_end[:,1], color=color)
    ax.plot(front_end[:,0], front_end[:,2], zs=-front_end[:,1], color=color)

    for back, front in zip(back_end[:-1], front_end[:-1]):
        xs = np.array([back[0], front[0]])
        ys = np.array([back[2], front[2]])
        zs = np.array([back[1], front[1]])
        ax.plot(xs, ys, zs=-zs, color=color)

    ax.plot(xaxis[:,0], xaxis[:,2], zs=-xaxis[:,1], color='red')
    ax.plot(yaxis[:,0], yaxis[:,2], zs=-yaxis[:,1], color='green')
    ax.plot(zaxis[:,0], zaxis[:,2], zs=-zaxis[:,1], color='blue')

    # Text
    text_loc = np.array([0, -100, -100])
    text_loc = np.dot(Rt[0:3, 0:3], text_loc)
    text_loc += np.dot(-Rt[0:3, 0:3], T[0:3]);
    ax.text(text_loc[0], text_loc[2], -text_loc[1], label)

def plot_camera_mat(ax, rvec, tvec, label, color='blue'):
    rvec_np = np.array(rvec);
    R, jac = cv2.Rodrigues(rvec_np)
    T = np.array(tvec);
    T = T.ravel();
    plot_camera_T(ax, R, T, label, color);

def plot_camera(ax, label, color='blue'):
	# Coordinates are specified with the front center of the
	# lens at (0, 0, 0), then rotated and adjusted based on
	# the camera's location.
    back_end = np.array([
            [0, 0, -150],
            [0,  0, -150],
            [0,  0, -150],
            [ 0, 0, -150],
            [0, 0, -150],
            ]) 
    
    front_end = np.array([
            [-70, -70,  0],
            [-70,  70,  0],
            [ 70,  70,  0],
            [ 70, -70,  0],
            [-70, -70,  0],
            ]) 
    ax.plot(back_end[:,0], back_end[:,2], zs=-back_end[:,1], color=color)
    ax.plot(front_end[:,0], front_end[:,2], zs=-front_end[:,1], color=color)
    for back, front in zip(back_end[:-1], front_end[:-1]):
        xs = np.array([back[0], front[0]])
        ys = np.array([back[2], front[2]])
        zs = np.array([back[1], front[1]])
        ax.plot(xs, ys, zs=-zs, color=color)

    xaxis = np.array([[0, 0, 0],[150, 0, 0]]) 
    yaxis = np.array([[0, 0, 0],[0, 150, 0]]) 
    zaxis = np.array([[0, 0, 0],[0, 0, 150]]) 
    
    ax.plot(xaxis[:,0], xaxis[:,2], zs=-xaxis[:,1], color='red')
    ax.plot(yaxis[:,0], yaxis[:,2], zs=-yaxis[:,1], color='green')
    ax.plot(zaxis[:,0], zaxis[:,2], zs=-zaxis[:,1], color='blue')

    # Text
    text_loc = np.array([0, 0, 0])
    ax.text(text_loc[0], text_loc[2], text_loc[1], label)
    
def plot_points_3d(ax, points, color):
    ax.plot(points[:,2], points[:,1], zs=points[:,0], color=color)

def plot_checkerboard(ax, fn, v, color='blue'): 
    for i in range(pattern_size[0]): 
        ax.plot(v[i*pattern_size[0]:(i+1)*pattern_size[0],0], 
                v[i*pattern_size[0]:(i+1)*pattern_size[0],2], 
                zs=-v[i*pattern_size[0]:(i+1)*pattern_size[0],1], color=color);

    for j in range(pattern_size[0]): 
        ax.plot(v[j:j+pattern_size[0]*pattern_size[1]+1:pattern_size[0],0], 
                v[j:j+pattern_size[0]*pattern_size[1]+1:pattern_size[0],2], 
                zs=-v[j:j+pattern_size[0]*pattern_size[1]+1:pattern_size[0],1], color=color);

    if len(fn): 
        # Draw origin for board
        path, fn_ = os.path.split(fn)
        name, ext = os.path.splitext(fn_)
        ax.text(v[0,0], v[0,2], v[0,1], 'o');
        #ax.text(v[0,0], v[0,2]+40, v[0,1], 'o_%s' % name);
    #else: 
        #ax.text(v[0,0], v[0,2]+40, v[0,1], 'o');
    ax.scatter(v[:1,0], v[:1,2], zs=-v[:1,1], marker='^', color='green');

def plot_scene_3d(calibration_data, c2p_rvec, c2p_tvec):
    global ax; 

    # Plot the camera in original and transformed positions
    plot_camera(ax, 'C')
    plot_camera_mat(ax, c2p_rvec, c2p_tvec, 'P', color='red')        

    # Plot checkerboard pattern in global coordinates
    for fn, v in calibration_data.iteritems(): 
        obj_pts_3d = []
        for pt in v.cam_obj_pts_np: 
            pt_3d = np.dot(v.cam_R_np, pt);
            pt_3d += v.cam_tvec_np[:,0];
            obj_pts_3d.append(pt_3d)
        obj_pts_3d = np.vstack(obj_pts_3d);
        plot_checkerboard(ax, fn, obj_pts_3d, color='blue');
        v.cam_obj_pts_global = obj_pts_3d;

        obj_pts_3d = []
        for pt in v.proj_obj_pts_np: 
            pt_3d = np.dot(v.cam_R_np, pt);
            pt_3d += v.cam_tvec_np[:,0];
            obj_pts_3d.append(pt_3d)
        obj_pts_3d = np.vstack(obj_pts_3d);
        plot_checkerboard(ax, fn, obj_pts_3d, color='red');
        v.proj_obj_pts_global = obj_pts_3d;

def plot_cameras_3d(camera_calibration_data, color='blue'):
    global ax2, ax3; 

    # Plot the camera at origin
    plot_camera(ax2, 'C')

    # Plot checkerboard pattern in global coordinates
    for fn, v in camera_calibration_data.iteritems(): 
        obj_pts_3d = []
        for pt in v.obj_pts: 
            pt_3d = np.dot(v.R, pt);
            pt_3d += v.tvec[:,0];
            obj_pts_3d.append(pt_3d)
        obj_pts_3d = np.vstack(obj_pts_3d);
        plot_checkerboard(ax2, fn, obj_pts_3d, color);
        v.obj_pts_global = obj_pts_3d;

    # Plot the camera in global coordinates
    # Plot checkerboard pattern in global coordinates
    once = False;
    for fn, v in camera_calibration_data.iteritems(): 
        if not once: 
            plot_checkerboard(ax3, '', v.obj_pts, color);
            once = True;
        plot_camera_mat(ax3, v.rvec, v.tvec, basename(fn), color)



def set_axes_3d(ax, max_dim):
    ax.set_xlim3d(-0.5 * max_dim, 0.5 * max_dim)
    ax.set_zlim3d(-0.5 * max_dim, 0.5 * max_dim)
    ax.set_ylim3d(-0.5 * max_dim, 0.5 * max_dim)
    #ax.set_ylim3d(-camera_size * 2, max_dim)
    ax.set_xlabel('x')
    ax.set_ylabel('z')
    ax.set_zlabel('y')

def homogenize(points):
    for i in range(points.shape[1]):
        points[:,i] /= points[:,-1]
               
def asarray(mat):
    return [[c for c in r] for r in mat]

def normalize_corners(corners, K, D): 
    K_ = K.ravel();
    fx,fy,cx,cy = K_[0], K_[4], K_[2], K_[5]; 
    #print 'Params: ', fx, fy, cx, cy
    r,c = corners.rows, corners.cols
    for i in range(0,c): 
        pre = corners[0,i];
        u,v = corners[0,i];
        corners[0, i] = ( ( u-cx ) / fx, (v-cy) / fy)
        #print pre, '--->', corners[0,i]
    return corners;

def revert_corners(corners, K, D): 
    K_ = K.ravel();
    fx,fy,cx,cy = K_[0], K_[4], K_[2], K_[5]; 
    r,c = corners.rows, corners.cols
    for i in range(0,c): 
        pre = corners[0,i];
        x,y = corners[0,i];
        corners[0, i] = ( x * fx + cx, y * fy + cy )
    return corners;

def calibrate_camera(camera_images): 

    # Build pattern points
    pattern_points = np.zeros( (np.prod(pattern_size), 3), np.float32 )
    pattern_points[:,:2] = np.indices(pattern_size).T.reshape(-1, 2)
    pattern_points *= square_size

    obj_points = []
    img_points = []
    processed_images = []

    h, w = 0, 0
    # Detect checkerboard corners all the camera images
    for fn in camera_images:
        print colored('Processing Camera Image %s' % fn, 'white')
        img = cv2.imread(fn, 0)
        h, w = img.shape[:2]

        # Chessboard finding
        printed_corners, projected_corners, im3 = find_corners(img, fn, both=False);
        if printed_corners == None and projected_corners == None: 
            print colored('Failed to find corners', 'red')
            continue;
        else: 
            if printed_corners is not None: 
                corners = np.array(printed_corners)[0]
            else: 
                corners = np.array(projected_corners)[0]
            print colored('Found corners successfully', 'blue')

        processed_images.append(fn)
        img_points.append(corners.reshape(-1, 2))
        obj_points.append(pattern_points)

    rms, camera_matrix, dist_coefs, rvecs, tvecs = cv2.calibrateCamera(obj_points, img_points, (w, h))
    print "\nRMS:", rms
    print "Camera matrix (K):\n", camera_matrix
    print "Distortion coefficients (D): \n", dist_coefs.ravel(), '\n'

    # Consolidate calibration data
    assert(len(processed_images) == len(obj_points))
    assert(len(processed_images) == len(rvecs))    

    camera_calibration_data = {}
    for i in range(0, len(processed_images)): 
        camera_calibration_data[processed_images[i]] = calib_data(img_points[i], obj_points[i], rvecs[i], tvecs[i])

    return camera_calibration_data, camera_matrix, dist_coefs

def calibrate_projector(camera_projector_images, camera_matrix, dist_coefs):
    global printed_chessboard_corners, printed_chessboard_fn;

    obj_points = []
    img_points = []
    processed_camera_projector_images = []

    img = cv2.imread(printed_chessboard_fn, 0);
    printed_chessboard_corners, val1, val2 = find_corners(img, printed_chessboard_fn, both=False);

    h, w = 0, 0
    proj_calibration_data = {}
    for fn in camera_projector_images:
        print colored( 'Processing Projector-Camera Image %s' % fn, 'white')
        img = cv2.imread(fn, 0)
        h, w = img.shape[:2]

        printed_corners, projected_corners, im3 = find_corners(img, fn);
        if printed_corners == None or projected_corners == None: 
            print colored( 'Failed to find corners', 'red')
            continue
        else: 
            print colored( 'Found corners successfully', 'blue')

        # Find the object points w.r.t projector
        homography = cv.CreateMat(3, 3, cv.CV_32FC1)
        cam_obj_points_3d = generate_object_points()

        # Undistort detected corners
        cv.UndistortPoints(printed_corners, printed_corners, 
                           cv.fromarray(camera_matrix), cv.fromarray(dist_coefs));
        cv.UndistortPoints(projected_corners, projected_corners, 
                           cv.fromarray(camera_matrix), cv.fromarray(dist_coefs));

        # Find homography between 2d printed corners (detected) and their 3d points (known from scale)
        cv.FindHomography(printed_corners, cam_obj_points_3d, homography)
        #print 'Homography', np.array(homography)

        # Go from 2D to 3D based on homography computed
        proj_obj_points = cv.CreateMat(1, pattern_size[0] * pattern_size[1], cv.CV_32FC2)
        cv.PerspectiveTransform(projected_corners, proj_obj_points, homography)
        #print 'Projected corners', np.array(projected_corners)
        #print 'Projected object corners', np.array(proj_obj_points)

        # Build obj_points mat 
        proj_obj_points_3d = cv.CreateMat(1, pattern_size[0] * pattern_size[1], cv.CV_32FC3)
        x = cv.CreateMat(1, pattern_size[0] * pattern_size[1], cv.CV_32FC1)
        y = cv.CreateMat(1, pattern_size[0] * pattern_size[1], cv.CV_32FC1)
        z = cv.CreateMat(1, pattern_size[0] * pattern_size[1], cv.CV_32FC1)
        cv.Split(proj_obj_points, x, y, None, None)
        cv.SetZero(z)
        cv.Merge(x, y, z, None, proj_obj_points_3d)
        #print 'Object points 3D', np.array(obj_points_3d)

        processed_camera_projector_images.append(fn);
        img_points.append(np.array(printed_chessboard_corners)[0])
        obj_points.append(np.array(proj_obj_points_3d)[0])

    rms, proj_matrix, proj_dist_coefs, rvecs, tvecs = cv2.calibrateCamera(obj_points, img_points, (1280, 800))
    print "\nRMS:", rms
    print "Proj matrix (K):\n", proj_matrix
    print "Distortion coefficients (D): \n", proj_dist_coefs.ravel(), '\n'

    # Consolidate calibration data
    assert(len(processed_camera_projector_images) == len(obj_points))
    assert(len(processed_camera_projector_images) == len(rvecs))    

    proj_calibration_data = {}
    for i in range(0, len(processed_camera_projector_images)): 
        proj_calibration_data[processed_camera_projector_images[i]] = calib_data(img_points[i], obj_points[i], rvecs[i], tvecs[i])

    return proj_calibration_data, proj_matrix, proj_dist_coefs

# def calibrate_projector2(camera_projector_images, 
#                          camera_matrix, dist_coefs, 
#                          proj_matrix, proj_dist_coefs):
#     global printed_chessboard_corners, printed_chessboard_fn;

#     processed_camera_projector_images = []

#     projector_to_camera_translation_vector = cv.CreateMat(3, 1, cv.CV_32FC1)
#     projector_to_camera_rotation_vector = cv.CreateMat(3, 1, cv.CV_32FC1)
#     cv.SetZero(projector_to_camera_translation_vector)
#     cv.SetZero(projector_to_camera_rotation_vector)

#     img = cv2.imread(printed_chessboard_fn, 0);
#     printed_chessboard_corners, val1, val2 = find_corners(img, printed_chessboard_fn, both=False);

#     h, w = 0, 0
#     proj_calibration_data = {}
#     for fn in camera_projector_images:
#         print colored( 'Processing Projector-Camera Image %s' % fn, 'white')
#         img = cv2.imread(fn, 0)
#         h, w = img.shape[:2]

#         printed_corners, projected_corners, im3 = find_corners(img, fn);
#         if printed_corners == None or projected_corners == None: 
#             print colored( 'Failed to find corners', 'red')
#             continue
#         else: 
#             print colored( 'Found corners successfully', 'blue')
        
#         # Undistort detected corners
#         # cv.UndistortPoints(printed_corners, printed_corners, 
#         #                    cv.fromarray(camera_matrix), cv.fromarray(dist_coefs));
#         # cv.UndistortPoints(projected_corners, projected_corners, 
#         #                    cv.fromarray(camera_matrix), cv.fromarray(dist_coefs));

#         # Find the object points w.r.t projector
#         homography = cv.CreateMat(3, 3, cv.CV_32FC1)
#         cam_obj_points_3d = generate_object_points()
#         cv.FindHomography(printed_corners, cam_obj_points_3d, homography)   
#         print '\nHomography: \n', np.array(homography)
        
#         # For testing purposes only
#         H = cv.CreateMat(3, 3, cv.CV_32FC1)        
#         cv.FindHomography(projected_corners, printed_chessboard_corners, H)

#         # Go from 2D to 3D based on homography computed
#         proj_obj_points = cv.CreateMat(1, pattern_size[0] * pattern_size[1], cv.CV_32FC2)
#         cv.PerspectiveTransform(projected_corners, proj_obj_points, homography)
#         #print 'Projected corners', np.array(projected_corners)
#         #print 'Projected object corners', np.array(proj_obj_points)

#         # Build obj_points mat 
#         proj_obj_points_3d = cv.CreateMat(1, pattern_size[0] * pattern_size[1], cv.CV_32FC3)
#         x = cv.CreateMat(1, pattern_size[0] * pattern_size[1], cv.CV_32FC1)
#         y = cv.CreateMat(1, pattern_size[0] * pattern_size[1], cv.CV_32FC1)
#         z = cv.CreateMat(1, pattern_size[0] * pattern_size[1], cv.CV_32FC1)
#         cv.Split(proj_obj_points, x, y, None, None)
#         cv.SetZero(z)
#         cv.Merge(x, y, z, None, proj_obj_points_3d)
#         #print 'Object points 3D', np.array(obj_points_3d)

#         # Camera w.r.t projected chessboard
#         camera_tvec = cv.CreateMat(3, 1,cv.CV_32FC1)
#         camera_rvec = cv.CreateMat(3, 1,cv.CV_32FC1)    
#         cv.FindExtrinsicCameraParams2(proj_obj_points_3d, projected_corners, 
#                                       cv.fromarray(camera_matrix), 
#                                       cv.fromarray(dist_coefs), 
#                                       camera_rvec, camera_tvec)


#         # Projector w.r.t camera directly
#         # Bring camera to origin and transform the board object points
#         #print 'Obj points', np.array(proj_obj_points_3d)
#         camera_rmat = cv.CreateMat(3, 3, cv.CV_32FC1)
#         cv.Rodrigues2(camera_rvec, camera_rmat)      

#         proj_obj_points_3d_wrt_cam = cv.CreateMat(1, proj_obj_points_3d.width, cv.CV_32FC3)
#         cv.Transform(proj_obj_points_3d, proj_obj_points_3d_wrt_cam, camera_rmat);
#         for col in range(proj_obj_points_3d_wrt_cam.width): 
#             x,y,z = proj_obj_points_3d_wrt_cam[0, col];
#             x += camera_tvec[0,0]; 
#             y += camera_tvec[1,0];
#             z += camera_tvec[2,0];
#             proj_obj_points_3d_wrt_cam[0, col] = (x,y,z)            
            
#         #print 'Obj points rotated: ', np.array(proj_obj_points_3d_wrt_cam)
#         cam2proj_tvec = cv.CreateMat(3, 1,cv.CV_32FC1)
#         cam2proj_rvec = cv.CreateMat(3, 1,cv.CV_32FC1)
#         cv.FindExtrinsicCameraParams2(proj_obj_points_3d_wrt_cam, printed_chessboard_corners, 
#                                       cv.fromarray(proj_matrix), 
#                                       cv.fromarray(proj_dist_coefs),
#                                       cam2proj_rvec, cam2proj_tvec)        


#         # print colored( '\n==> Camera to Projector directly <==', 'green')
#         # print colored( '==> Rvec <== \n %s' % np.array(cam2proj_rvec), 'green')
#         # print colored( '==> Tvec <== \n %s' % np.array(cam2proj_tvec), 'green')

#         # print "Camera To Board Vector:"
#         # for row in range(camera_tvec.height):
#         #     for col in range(camera_tvec.width):
#         #         print camera_tvec[row, col],
#         #     print
#         # print

#         # Projector w.r.t projected chessboard
#         print 'Using projector matrix: \n %s \n %s' % ( proj_matrix, proj_dist_coefs )
#         projector_tvec = cv.CreateMat(3, 1,cv.CV_32FC1)
#         projector_rvec = cv.CreateMat(3, 1,cv.CV_32FC1)
#         #print 'Projector points', printed_chessboard_corners
#         cv.FindExtrinsicCameraParams2(proj_obj_points_3d, printed_chessboard_corners, 
#                                       cv.fromarray(proj_matrix), 
#                                       cv.fromarray(proj_dist_coefs),
#                                       projector_rvec, projector_tvec)        

#         proj_calibration_data[fn] = proj_calib_data(printed_corners, projected_corners, printed_chessboard_corners, 
#                                                     cam_obj_points_3d, 
#                                                     proj_obj_points_3d,
#                                                     camera_rvec, camera_tvec, projector_rvec, projector_tvec);

#         # print "Projector To Board Vector:"
#         # for row in range(projector_tvec.height):
#         #     for col in range(projector_tvec.width):
#         #         print projector_tvec[row, col],
#         #     print
#         # print
        
#         camera_rmat = cv.CreateMat(3, 3, cv.CV_32FC1)
#         cv.Rodrigues2(camera_rvec, camera_rmat)
        
#         projector_rmat = cv.CreateMat(3, 3, cv.CV_32FC1)
#         cv.Rodrigues2(projector_rvec, projector_rmat)
        
#         scene_projector_to_camera_rotation_matrix = cv.CreateMat(3, 3, cv.CV_32FC1)
#         cv.GEMM(camera_rmat, projector_rmat, 1, None, 0, 
#                 scene_projector_to_camera_rotation_matrix, cv.CV_GEMM_B_T)

#         scene_projector_to_camera_rotation_vector = cv.CreateMat(3, 1, cv.CV_32FC1)
#         # for i in range(3):
#         #     for j in range(3):
#         #         print scene_projector_to_camera_rotation_matrix[i, j],
#         #     print
#         # print

#         cv.Rodrigues2(scene_projector_to_camera_rotation_matrix, scene_projector_to_camera_rotation_vector)
#         # print "Scene Rotation Vector:"
#         # for row in range(scene_projector_to_camera_rotation_vector.height):
#         #     for col in range(scene_projector_to_camera_rotation_vector.width):
#         #         print scene_projector_to_camera_rotation_vector[row, col],
#         #     print
#         # print

#         scene_projector_to_camera_translation_vector = cv.CreateMat(3, 1, cv.CV_32FC1)
#         cv.GEMM(projector_rmat, projector_tvec, -1, None, 0, 
#                 scene_projector_to_camera_translation_vector, cv.CV_GEMM_A_T)
#         cv.GEMM(camera_rmat, scene_projector_to_camera_translation_vector, 1, 
#                 camera_tvec, 1, scene_projector_to_camera_translation_vector, 0)

#         # print "Scene Translation Vector:"
#         # for row in range(scene_projector_to_camera_translation_vector.height):
#         #     for col in range(scene_projector_to_camera_translation_vector.width):
#         #         print scene_projector_to_camera_translation_vector[row, col],
#         #     print
#         # print            

#         cv.Add(scene_projector_to_camera_translation_vector, 
#                projector_to_camera_translation_vector, 
#                projector_to_camera_translation_vector)
#         cv.Add(scene_projector_to_camera_rotation_vector, 
#                projector_to_camera_rotation_vector, 
#                projector_to_camera_rotation_vector)

#         processed_camera_projector_images.append(fn);        
#         break;

#     print 'Num of scenes: ', len(processed_camera_projector_images), ' out of ', len(camera_projector_images)
#     cv.ConvertScale(projector_to_camera_translation_vector, 
#                     projector_to_camera_translation_vector, 
#                     scale=1.0/len(processed_camera_projector_images))
#     cv.ConvertScale(projector_to_camera_rotation_vector, 
#                     projector_to_camera_rotation_vector, 
#                     scale=1.0/len(processed_camera_projector_images))

#     return proj_calibration_data, projector_to_camera_rotation_vector, projector_to_camera_translation_vector


def calibrate_camera_to_camera(camera_camera_images, 
                         cam1_matrix, cam1_dist_coefs, 
                         cam2_matrix, cam2_dist_coefs):
    global printed_chessboard_corners, printed_chessboard_fn;

    cam2_corners_list = []
    cam2_obj_points_3d_wrt_cam_list = []
    processed_camera_projector_images = []

    h, w = 0, 0
    cams_calibration_data = {}
    for fn1,fn2 in camera_camera_images:
        print colored( 'Processing Camera-Camera Image %s & %s' % (fn1, fn2), 'white')
        img1 = cv2.imread(fn1, 0)
        h, w = img1.shape[:2]

        img2 = cv2.imread(fn2, 0)
        h, w = img2.shape[:2]

        cam1_corners, projected_corners, im3 = find_corners(img1, fn1, both=False);
        if cam1_corners == None: 
            print colored( 'Failed to find corners', 'red')
            continue
        else: 
            print colored( 'Found corners successfully', 'blue')

        cam2_corners, projected_corners, im3 = find_corners(img2, fn2, both=False);
        if cam2_corners == None: 
            print colored( 'Failed to find corners', 'red')
            continue
        else: 
            print colored( 'Found corners successfully', 'blue')

        #print 'Pre distort: ', np.array(cam1_corners), cam1_matrix, cam1_dist_coefs

        # cam1_corners = normalize_corners(cam1_corners, cam1_matrix, cam1_dist_coefs);
        # cam2_corners = normalize_corners(cam2_corners, cam2_matrix, cam2_dist_coefs);

        # # Undistort detected corners
        # cv.UndistortPoints(cam1_corners, cam1_corners, 
        #                    cv.fromarray(cam1_matrix), cv.fromarray(cam1_dist_coefs));


        # cv.UndistortPoints(cam2_corners, cam2_corners, 
        #                    cv.fromarray(cam2_matrix), cv.fromarray(cam2_dist_coefs));

        # cam1_corners = revert_corners(cam1_corners, cam1_matrix, cam1_dist_coefs);
        # cam2_corners = revert_corners(cam2_corners, cam2_matrix, cam2_dist_coefs);
        # print 'Post distort: ', np.array(cam1_corners)
        # print 'Post distort: ', np.array(cam2_corners)


        # Find the object points w.r.t projector
        cam1_obj_points_3d = generate_object_points()

        # Camera 1 w.r.t board
        cam1_tvec = cv.CreateMat(3, 1,cv.CV_32FC1)
        cam1_rvec = cv.CreateMat(3, 1,cv.CV_32FC1)    
        D = np.zeros( (5, 1), np.float32 );
        cv.FindExtrinsicCameraParams2(cam1_obj_points_3d, cam1_corners, 
                                      cv.fromarray(cam1_matrix), 
                                      cv.fromarray(cam1_dist_coefs), 
                                      cam1_rvec, cam1_tvec)

        print colored( 'cam1_rvec \n %s' % np.array(cam1_rvec), 'white')
        print colored( 'cam1_tvec \n %s' % np.array(cam1_tvec), 'white')

        # # Camera 1 w.r.t board
        # cam2_tvec = cv.CreateMat(3, 1,cv.CV_32FC1)
        # cam2_rvec = cv.CreateMat(3, 1,cv.CV_32FC1)    
        # D = np.zeros( (5, 1), np.float32 );
        # cv.FindExtrinsicCameraParams2(convert_points(cam2_obj_points_3d_wrt_cam), cv.fromarray(cam2_corners_stacked), 
        #                               cv.fromarray(cam2_matrix), 
        #                               cv.fromarray(cam2_dist_coefs), 
        #                               cam2_rvec, cam2_tvec)
        # print colored( 'cam2_rvec \n %s' % np.array(cam2_rvec), 'white')
        # print colored( 'cam2_tvec \n %s' % np.array(cam2_tvec), 'white')

        # # Projector w.r.t projected chessboard
        # print 'Using projector matrix: \n %s \n %s' % ( proj_matrix, proj_dist_coefs )
        # projector_tvec = cv.CreateMat(3, 1,cv.CV_32FC1)
        # projector_rvec = cv.CreateMat(3, 1,cv.CV_32FC1)
        # #print 'Projector points', printed_chessboard_corners
        # cv.FindExtrinsicCameraParams2(proj_obj_points_3d, printed_chessboard_corners, 
        #                               cv.fromarray(proj_matrix), 
        #                               cv.fromarray(proj_dist_coefs),
        #                               projector_rvec, projector_tvec)        

        # proj_calibration_data[fn] = proj_calib_data(printed_corners, projected_corners, printed_chessboard_corners, 
        #                                             cam_obj_points_3d, 
        #                                             proj_obj_points_3d,
        #                                             camera_rvec, camera_tvec);

        # Camera 1 w.r.t camera 2 directly
        # Bring camera 1 to origin and transform the board object points
        cam1_obj_points_3d_np = np.array(cam1_obj_points_3d)[0];
        cam1_rvec_np = np.array(cam1_rvec);
        cam1_R_np, jac = cv2.Rodrigues(cam1_rvec_np);
        cam1_tvec_np = np.array(cam1_tvec);
        #print 'Obj points', proj_obj_points_3d_np;

        for pt in cam1_obj_points_3d_np: 
            pt_3d = np.dot(cam1_R_np, pt);
            pt_3d += cam1_tvec_np[:,0];
            cam2_obj_points_3d_wrt_cam_list.append(pt_3d)

        for pt in np.array(cam2_corners)[0]: 
            cam2_corners_list.append(pt)


    if (len(cam2_corners_list) == len(cam2_obj_points_3d_wrt_cam_list)): 
        cam2_obj_points_3d_wrt_cam = np.vstack(cam2_obj_points_3d_wrt_cam_list);
        cam2_corners_stacked = np.vstack(cam2_corners_list);
    #print 'Obj points rotated: ', proj_obj_points_3d_wrt_cam
    #print 'Len Obj points: ', proj_obj_points_3d_wrt_cam.shape[:2]

    #print 'Image points: ', printed_chessboard_corners_stacked
    #print 'len Image points: ', printed_chessboard_corners_stacked.shape[:2]
    #print 
    #print 'extrinsic: ', cv.fromarray(proj_obj_points_3d_wrt_cam), cv.fromarray(printed_chessboard_corners_stacked)

    cam2cam_tvec = cv.CreateMat(3, 1,cv.CV_32FC1)
    cam2cam_rvec = cv.CreateMat(3, 1,cv.CV_32FC1)
    cv.FindExtrinsicCameraParams2(convert_points(cam2_obj_points_3d_wrt_cam), cv.fromarray(cam2_corners_stacked), 
                                  cv.fromarray(cam2_matrix), 
                                  cv.fromarray(cam2_dist_coefs),
                                  cam2cam_rvec, cam2cam_tvec)        

    print 'result: ', np.array(cam2cam_rvec), np.array(cam2cam_tvec)
    print colored( '\n==> Camera to Projector directly <==', 'green')
    print colored( '==> Rvec <== \n %s' % np.array(cam2cam_rvec), 'green')
    print colored( '==> Tvec <== \n %s' % np.array(cam2cam_tvec), 'green')
    return cam2_calibration_data, cam2cam_rvec, cam2cam_tvec

def invert(p2c_rvec, p2c_tvec): 
    c2p_R = cv.CreateMat(3, 3, cv.CV_32FC1);
    p2c_tvec_np = np.array(p2c_tvec);
    p2c_rvec_np = np.array(p2c_rvec);
    p2c_R, jac = cv2.Rodrigues(p2c_rvec_np)

    c2p_R = cv2.transpose(p2c_R);
    c2p_tvec = np.dot(-c2p_R, p2c_tvec_np);
    c2p_rvec, jac = cv2.Rodrigues(c2p_R)
    return c2p_rvec, c2p_tvec

def l2_dist(pt1, pt2): 
    return sqrt((pt1[0]-pt2[0])**2 + (pt1[1]-pt2[1])**2)

def reproj_error(pts1, pts2): 
    global ax2d

    assert(len(pts1) == len(pts2));
    sumerr = 0;
    errors = []
    xs, ys = [], []
    for i in range(len(pts1)): 
        err = l2_dist(pts1[i], pts2[i]);
        xs.append(pts2[i][0]-pts1[i][0])
        ys.append(pts2[i][1]-pts1[i][1])
        errors.append(err)
        sumerr += err;
    sorted_errors = sorted(errors);

    ax2d.plot(xs, ys, '+')

    print 'Min Error: ', sorted_errors[0]
    print 'Median Error: ', sorted_errors[len(sorted_errors)/2]
    print 'Max Error: ', sorted_errors[len(sorted_errors)-1]
    print 'Mean Error: ', sumerr / len(sorted_errors);    

def check_calibration(camera_calibration_data, proj_calibration_data, 
                      c2p_rvec, c2p_tvec, 
                      camera_matrix, dist_coefs, 
                      proj_matrix, proj_dist_coefs):
    global printed_chessboard_corners, printed_chessboard_fn;

    pts1, pts2 = [], []
    for fn, v in proj_calibration_data.iteritems(): 
        obj_pts = np.array(v.proj_obj_pts);
        cam_img_pts = np.array(v.cam_img_pts);
        proj_img_pts = np.array(v.proj_img_pts);

        obj_pts_3d = []
        for pt in obj_pts[0]: 
            pt_3d = np.dot(v.cam_R_np, pt);
            pt_3d += v.cam_tvec_np[:,0];
            obj_pts_3d.append(pt_3d)
        obj_pts_3d = np.vstack(obj_pts_3d);

        img_pts = cv.CreateMat(1, v.proj_obj_pts.width * v.proj_obj_pts.height, cv.CV_32FC2);
        cv.ProjectPoints2(cv.fromarray(obj_pts_3d), 
                          c2p_rvec, 
                          c2p_tvec, 
                          cv.fromarray(proj_matrix), cv.fromarray(proj_dist_coefs), img_pts);

        # print np.array(v.projected_chess_pts)
        # for pt in np.array(img_pts): 
        #    print 'Calc. Proj pt: ', pt
        #    break;

        #print 'Proj. pt: ', np.array(printed_chessboard_corners)
        pts1.append(np.array(v.projected_chess_pts)[0])
        pts2.append(np.array(img_pts)[0])
        #reproj_error(np.array(v.projected_chess_pts)[0], np.array(img_pts)[0])
        # print colored( '\n==> Camera to board <==', 'green')
        # print colored( '==> Rvec <== \n %s' % v.cam_rvec_np, 'green')
        # print colored( '==> Tvec <== \n %s' % v.cam_tvec_np, 'green')

    pts1 = np.vstack(pts1)
    pts2 = np.vstack(pts2);

    print '==> Reprojection Error'
    reproj_error(pts1, pts2);

    print colored( '\n==> Camera to projector <==', 'green')
    print colored( '==> Rvec <== \n %s' % np.array(c2p_rvec), 'green')
    print colored( '==> Tvec (in mm) <== \n %s' % np.array(c2p_tvec), 'green')
    

if __name__ == "__main__":
    if len(sys.argv) < 2: 
        print './projector_camera_calib.py <camera_images> OR'        
        print './projector_camera_calib.py <camera1_image(s)> <camera2_image(s)>'
        exit(0)
    # if len(sys.argv) == 2: 
    #     print 'Camera calibration'
    #     calib_mode = 'camera'
    if len(sys.argv) >= 3: 
        print 'Camera-Camera calibration'
        calib_mode = 'camera-camera'
        
    # Camera1 Images
    listing = os.listdir(sys.argv[1]);
    camera1_images, camera2_images, camera_camera_images = [], [], []
    for c in listing: 
        fn = os.path.join(sys.argv[1], c);
        if not os.path.isfile(fn): continue
        if isimage(fn): 
            camera1_images.append(fn);

    # Camera + Projector Images
    if calib_mode == 'camera-camera': 
        listing = os.listdir(sys.argv[2])    
        for c in listing: 
            fn = os.path.join(sys.argv[2], c);
            if not os.path.isfile(fn): 
                continue
            if isimage(fn): 
                camera2_images.append(fn);

    both_images = []
    listing = os.listdir(sys.argv[3]);
    for c in listing: 
        fn = os.path.join(sys.argv[3], c);
        if not os.path.isfile(fn): 
            continue
        if isimage(fn): 
            both_images.append(fn);
    
    for fn1 in both_images: 
        sp = fn1.split('_');
        common = '_%s' % sp[len(sp)-1];
        for fn2 in both_images: 
            if fn2 == fn1: 
                continue;
            if common in fn2: 
                camera_camera_images.append((fn1, fn2));
        # for fn1 in camera1_images: 
        #     sp = fn1.split('_');
        #     common = '_%s' % sp[len(sp)-1];
        #     for fn2 in camera2_images: 
        #         if common in fn2: 
        #             camera_camera_images.append((fn1, fn2));

    for fn1,fn2 in camera_camera_images: 
        print fn1, fn2

    pattern_points = np.zeros( (np.prod(pattern_size), 3), np.float32 )
    pattern_points[:,:2] = np.indices(pattern_size).T.reshape(-1, 2)
    pattern_points *= square_size

    print colored( '\n*******************', 'yellow')
    print colored( 'Calibrating Camera 1 ... ', 'yellow')
    print colored( '*******************\n', 'yellow')

    # cam1_matrix = np.array([
    #         [521.93428242,    0.0,     320.03682487],
    #         [   0.0,     523.48743953, 245.7085731 ],
    #         [   0.0,        0.0,        1.0    ],
    #         ])
    # cam1_dist_coefs = np.array([[0.2467938,  -0.78813194,  0.0014376,   0.00160078,  0.83780247]]);
    cam1_calibration_data, cam1_matrix, cam1_dist_coefs = calibrate_camera(camera1_images);

    # Plot cameras
    plot_cameras_3d(cam1_calibration_data, color='blue')

    if len(sys.argv) >= 3: 


        print colored( '\n*******************', 'yellow')
        print colored( 'Calibrating Camera 2 ... ', 'yellow')
        print colored( '*******************\n', 'yellow')


        # cam2_matrix = np.array([
        #         [521.93428242,    0.0,     320.03682487],
        #         [   0.0,     523.48743953, 245.7085731 ],
        #         [   0.0,        0.0,        1.0    ],
        #         ])
        # cam2_dist_coefs = np.array([[0.2467938,  -0.78813194,  0.0014376,   0.00160078,  0.83780247]]);

        # # 
        cam2_calibration_data, cam2_matrix, cam2_dist_coefs = calibrate_camera(camera2_images);
        plot_cameras_3d(cam2_calibration_data, color='red')
    
        print colored( '\n*******************', 'yellow')
        print colored( 'Joint Camera & Camera calibration ... ', 'yellow')
        print colored( '*******************\n', 'yellow')
        cams_calibration_results, c2c_rvec, c2c_tvec = calibrate_camera_to_camera(camera_camera_images, 
                                                                        cam1_matrix, cam1_dist_coefs, 
                                                                        cam2_matrix, cam2_dist_coefs)

        # print colored( '\n*******************', 'yellow')
        # print colored( 'Calibration check ... ', 'yellow')
        # print colored( '*******************\n', 'yellow')
        # check_calibration(cam2_calibration_data, 
        #                   cams_calibration_results, 
        #                   c2c_rvec, c2c_tvec, 
        #                   cam1_matrix, cam1_dist_coefs, 
        #                   cam2_matrix, cam2_dist_coefs);

        # # 3D Plot
        # plot_scene_3d(cams_calibration_results, c2c_rvec, c2c_tvec)

    set_axes_3d(ax, 4000)
    set_axes_3d(ax2, 4000)
    set_axes_3d(ax3, 4000)
    ax.set_title('Projector Camera Calibration')
    ax2.set_title('Camera Calibration - Camera centered view')
    ax3.set_title('Camera Calibration - World centered view')
    pyplot.show()

