FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/cpp"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/lcmtypes_timesync_jar"
  "lcmtypes_timesync.jar"
  "../lcmtypes/java/erlcm/timesync_t.class"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/lcmtypes_timesync_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
