# list of subdirectories to build, one one each line.  Empty lines
# and lines starting with '#' are ignored

#externals
lcmtypes
core
drivers
robot
arm_collection
world_model
localization
motion_planning
slam
simulation
perception
controllers
renderers
viewer
python_scripts
scripts
utils
