add_definitions(
    -std=gnu99)

#build the viewer

add_executable(er-viewer 
    #er_viewer.cpp
    udp_util.c
    main.cpp
    )

add_executable(er-full-viewer 
    #er_viewer.cpp
    udp_util.c
    main_full.cpp
    )

include_directories(
    ${GTK2_INCLUDE_DIRS}
    ${GLUT_INCLUDE_DIR})

target_link_libraries(er-viewer
    ${GTK2_LDFLAGS}
    ${GLUT_LIBRARIES})

pods_use_pkg_config_packages(er-viewer
    #arm-interface-renderer
    bot2-vis 
    bot2-lcmgl-renderer 
    bot2-frames-renderers 
    bot2-frames
    opencv
    er-renderers
    laser-util-renderer
    image-util-renderer
    lcmtypes_er-lcmtypes
    map3d_interface
    er-path-util
    #velodyne-renderer
    image-util-renderer
    occ-map-renderers
    #nodder-renderer
    #kinect-renderer
    #collections_renderer  
    #rrtstar-arm-renderer
    octomap-renderer
    #er-semantic-perception-renderers
    #renderer_visualization
    #collections_renderer
    #articulation-renderer
    #kinect-skeleton-renderer
    #object-model-renderer
    )

pods_install_executables(er-viewer)


target_link_libraries(er-full-viewer
    ${GTK2_LDFLAGS}
    ${GLUT_LIBRARIES})

pods_use_pkg_config_packages(er-full-viewer
    #arm-interface-renderer
    bot2-vis 
    bot2-lcmgl-renderer 
    bot2-frames-renderers 
    bot2-frames
    opencv
    er-renderers
    laser-util-renderer
    image-util-renderer
    lcmtypes_er-lcmtypes
    map3d_interface
    er-path-util
    #velodyne-renderer
    image-util-renderer
    occ-map-renderers
    #nodder-renderer
    #kinect-renderer
    #collections_renderer  
    #rrtstar-arm-renderer
    octomap-renderer
    )

pods_install_executables(er-full-viewer)
