#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <lcm/lcm.h>
#include <lcmtypes/er_lcmtypes.h>
#include <bot_core/bot_core.h>
#include <pcl/filters/filter.h>
#include <pcl/common/centroid.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/sample_consensus/sac_model_plane.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

typedef pcl::PointXYZ PointT;

/*void segment_cylinders(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, lcm_t *lcm){
    //pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob (new pcl::PointCloud<pcl::PointXYZ>); 
    //pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>); 
    const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
    //pcl::VoxelGrid<sensor_msgs::PointCloud2> sor;

    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_CYLINDER);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (1000);

    //play with this to get different size segments 
    seg.setDistanceThreshold (0.02); //was 0.01
    
    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    
    //    pcl::PCDWriter writer;
    //writer.write<pcl::PointXYZ> ("out_downsampled.pcd", *cloud_filtered_blob, false);
        
    int i = 0, nr_points = (int) cloud->points.size ();

    erlcm_segment_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.segments = NULL;
    
    msg.no_segments = 0;
    
    // While 30% of the original cloud is still there
    while (cloud->points.size () > 0.3 * nr_points){
        // Segment the largest planar component from the remaining cloud
        seg.setInputCloud (cloud);
        seg.segment (*inliers, *coefficients);
        if (inliers->indices.size () == 0){
            std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
            break;
        }
        
        std::cerr << "Model coefficients: " << coefficients->values[0] << " " 
                                      << coefficients->values[1] << " "
                                      << coefficients->values[2] << " " 
                                      << coefficients->values[3] << std::endl;
        
        msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
        
        erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];//(erlcm_seg_point_list_t *) calloc(1, sizeof(erlcm_seg_point_list_t));
        seg_msg->segment_id = msg.no_segments; 
        seg_msg->no_points = inliers->indices.size();

        seg_msg->coefficients[0] = coefficients->values[0]; 
        seg_msg->coefficients[1] = coefficients->values[1]; 
        seg_msg->coefficients[2] = coefficients->values[2]; 
        seg_msg->coefficients[3] = coefficients->values[3];

        pcl::PointXYZ sensor_loc; 

        sensor_loc.x = 0;
        sensor_loc.y = 0;
        sensor_loc.z = 0; 
        
        double dist = pcl::pointToPlaneDistance(sensor_loc, coefficients->values[0], 
                                                coefficients->values[1], 
                                                coefficients->values[2], 
                                                coefficients->values[3]);

        fprintf(stderr,"Distance to plane : %f\n", dist);
        
        if(fabs(dist - 1.2) < 0.4){
            fprintf(stderr, "Ground plane found\n"); 
        }

        // Extract the inliers
        extract.setInputCloud(cloud);
        extract.setIndices (inliers);
        extract.setNegative (false);
        extract.filter (*cloud_p);

        std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." << std::endl;
        
        seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));

        double mean[3] = {0,0,0};
        
        for (size_t k = 0; k < cloud_p->points.size (); ++k){
            seg_msg->points[k].xyz[0] = cloud_p->points[k].x; 
            seg_msg->points[k].xyz[1] = cloud_p->points[k].y; 
            seg_msg->points[k].xyz[2] = cloud_p->points[k].z; 
            mean[0] += cloud_p->points[k].x; 
            mean[1] += cloud_p->points[k].y; 
            mean[2] += cloud_p->points[k].z; 
        }
        
        seg_msg->centroid[0] = mean[0] / cloud_p->points.size (); 
        seg_msg->centroid[1] = mean[1] / cloud_p->points.size (); 
        seg_msg->centroid[2] = mean[2] / cloud_p->points.size (); 

        fprintf(stderr, "\tMean : %f, %f, %f\n", seg_msg->centroid[0], 
                seg_msg->centroid[1], seg_msg->centroid[2]);
        
        msg.no_segments++; 
        
        // Create the filtering object
        extract.setNegative (true);
        extract.filter (*cloud);
        
        i++;
    }
    
    //publish
    erlcm_segment_list_t_publish(lcm, "PCL_SEGMENT_LIST", &msg);
    
    for(int k = 0; k < msg.no_segments; k++){
        free(msg.segments[k].points);
    }
    free(msg.segments);        
}*/

void segment_cylinders_old(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud){
    //pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob (new pcl::PointCloud<pcl::PointXYZ>); 
    //pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>); 
    const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
    //pcl::VoxelGrid<sensor_msgs::PointCloud2> sor;

    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_CYLINDER);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (1000);

    //play with this to get different size segments 
    seg.setDistanceThreshold (0.02); //was 0.01
    
    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    
    //    pcl::PCDWriter writer;
    //writer.write<pcl::PointXYZ> ("out_downsampled.pcd", *cloud_filtered_blob, false);
        
    int i = 0, nr_points = (int) cloud->points.size ();

    erlcm_segment_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.segments = NULL;
    
    msg.no_segments = 0;
    
    // While 30% of the original cloud is still there
    while (cloud->points.size () > 0.3 * nr_points){
        // Segment the largest planar component from the remaining cloud
        seg.setInputCloud (cloud);
        seg.segment (*inliers, *coefficients);
        if (inliers->indices.size () == 0){
            std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
            break;
        }
        
        std::cerr << "Model coefficients: " << coefficients->values[0] << " " 
                                      << coefficients->values[1] << " "
                                      << coefficients->values[2] << " " 
                                      << coefficients->values[3] << std::endl;
        
        msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
        
        erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];//(erlcm_seg_point_list_t *) calloc(1, sizeof(erlcm_seg_point_list_t));
        seg_msg->segment_id = msg.no_segments; 
        seg_msg->no_points = inliers->indices.size();

        seg_msg->coefficients[0] = coefficients->values[0]; 
        seg_msg->coefficients[1] = coefficients->values[1]; 
        seg_msg->coefficients[2] = coefficients->values[2]; 
        seg_msg->coefficients[3] = coefficients->values[3];

        pcl::PointXYZ sensor_loc; 

        sensor_loc.x = 0;
        sensor_loc.y = 0;
        sensor_loc.z = 0; 
        
        double dist = pcl::pointToPlaneDistance(sensor_loc, coefficients->values[0], 
                                                coefficients->values[1], 
                                                coefficients->values[2], 
                                                coefficients->values[3]);

        fprintf(stderr,"Distance to plane : %f\n", dist);
        
        if(fabs(dist - 1.2) < 0.4){
            fprintf(stderr, "Ground plane found\n"); 
        }

        // Extract the inliers
        extract.setInputCloud(cloud);
        extract.setIndices (inliers);
        extract.setNegative (false);
        extract.filter (*cloud_p);

        std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." << std::endl;
        
        seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));

        double mean[3] = {0,0,0};
        
        for (size_t k = 0; k < cloud_p->points.size (); ++k){
            seg_msg->points[k].xyz[0] = cloud_p->points[k].x; 
            seg_msg->points[k].xyz[1] = cloud_p->points[k].y; 
            seg_msg->points[k].xyz[2] = cloud_p->points[k].z; 
            mean[0] += cloud_p->points[k].x; 
            mean[1] += cloud_p->points[k].y; 
            mean[2] += cloud_p->points[k].z; 
        }
        
        seg_msg->centroid[0] = mean[0] / cloud_p->points.size (); 
        seg_msg->centroid[1] = mean[1] / cloud_p->points.size (); 
        seg_msg->centroid[2] = mean[2] / cloud_p->points.size (); 

        fprintf(stderr, "\tMean : %f, %f, %f\n", seg_msg->centroid[0], 
                seg_msg->centroid[1], seg_msg->centroid[2]);
        
        msg.no_segments++; 
        
        // Create the filtering object
        extract.setNegative (true);
        extract.filter (*cloud);
        
        i++;
    }
    
    //publish
    //erlcm_segment_list_t_publish(lcm, "PCL_SEGMENT_LIST", &msg);
    
    for(int k = 0; k < msg.no_segments; k++){
        free(msg.segments[k].points);
    }
    free(msg.segments);        
}

int segment_cylinders(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, double *param){
    const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    pcl::NormalEstimation<PointT, pcl::Normal> ne;
    pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());

    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
    pcl::PointCloud<PointT>::Ptr cloud_filtered2 (new pcl::PointCloud<PointT>);

    pcl::PointIndices::Ptr inliers_cylinder (new pcl::PointIndices);

    // Create the segmentation object
    //pcl::SACSegmentation<pcl::PointXYZ> seg;
    pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg; 
    pcl::ExtractIndices<pcl::PointXYZ> extract;

    // Estimate point normals
    ne.setSearchMethod (tree);
    ne.setInputCloud (cloud);
    ne.setKSearch (50);
    ne.compute (*cloud_normals);

    // Create the segmentation object for cylinder segmentation and set all the parameters
    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_CYLINDER);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setNormalDistanceWeight (0.1);
    seg.setMaxIterations (10000);
    seg.setDistanceThreshold (0.1); //was 0.05
    

    //Sachi - figure out the actual radius
    //this sets the radius limit -  
    seg.setRadiusLimits (0, 0.15);
    seg.setInputCloud (cloud);
    seg.setInputNormals (cloud_normals);


    //first three co-efficients are the line point
    //second three are the line direction - this should match with the ground plane normal - more or less 
    //last is the radius 
    
    // Obtain the cylinder inliers and coefficients
    seg.segment (*inliers_cylinder, *coefficients);

    

    // Extract the inliers
    extract.setInputCloud(cloud);
    extract.setIndices (inliers_cylinder);
    extract.setNegative (false);
    extract.filter (*cloud_p);
    std::cout << "Inlier Count : " << cloud_p->points.size()<< " Total : " <<
        cloud->points.size() << std::endl;

    if(cloud_p->points.size() / ((double) cloud->points.size() > 0.7)){
        std::cerr << "Cylinder coefficients: " << *coefficients << std::endl;
        
        fprintf(stderr," Radius : %f , Line Point : %f,%f,%f Line Direction : %f,%f,%f\n", coefficients->values[6], coefficients->values[0], coefficients->values[1], coefficients->values[2], 
                coefficients->values[3], coefficients->values[4], coefficients->values[5]);

        param[0] = coefficients->values[0]; 
        param[1] = coefficients->values[1]; 
        param[2] = coefficients->values[2]; 
        param[3] = coefficients->values[3]; 
        param[4] = coefficients->values[4]; 
        param[5] = coefficients->values[5]; 
        param[6] = coefficients->values[6]; 
        //free shit 

        return 0;


    }
    else{
        fprintf(stderr, "Error - Not enough Points for inliers\n");
        return 1; 
    }
}

static void 
on_point_list (const lcm_recv_buf_t *rbuf, const char *channel,
                 const erlcm_segment_list_t *msg, void *user_data )
{
    fprintf(stderr,"Rec\n");

    //for each segment - try to fit a cylinder 

    lcm_t * lcm  = (lcm_t *) user_data; 

    erlcm_cylinder_list_t cyl_msg; 

    cyl_msg.count = 0;

    cyl_msg.list = NULL;
    fprintf(stderr,"utime %ld\n", msg->utime);
    
    for(int i=0; i< msg->no_segments; i++){
        erlcm_seg_point_list_t *seg_msg = &msg->segments[i];

        static const pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>);
        pcl::PointCloud<pcl::PointXYZ>& cloud = *point_cloud_ptr;

        cloud.height   = 1;
        cloud.width    = seg_msg->no_points;
        
        cloud.is_dense = true;
        cloud.points.resize (cloud.width * cloud.height);
        
        for(int j=0; j< seg_msg->no_points; j++){
            cloud.points[j].x = seg_msg->points[j].xyz[0]; 
            cloud.points[j].y = seg_msg->points[j].xyz[1]; 
            cloud.points[j].z = seg_msg->points[j].xyz[2]; 
        }
        
        double param[6]; 
 
        if(!segment_cylinders(point_cloud_ptr, param)){
            //we have a valid cylinder
            cyl_msg.list = (erlcm_cylinder_model_t *) realloc(cyl_msg.list, sizeof(erlcm_cylinder_model_t) * (cyl_msg.count +1)); 
            cyl_msg.list[cyl_msg.count].line_point[0] = param[0]; 
            cyl_msg.list[cyl_msg.count].line_point[1] = param[1]; 
            cyl_msg.list[cyl_msg.count].line_point[2] = param[2]; 

            cyl_msg.list[cyl_msg.count].line_direction[0] = param[3]; 
            cyl_msg.list[cyl_msg.count].line_direction[1] = param[4]; 
            cyl_msg.list[cyl_msg.count].line_direction[2] = param[5]; 
            
            cyl_msg.list[cyl_msg.count].radius = param[6]; 
            cyl_msg.count++;
        }
        
        
        cloud.points.resize (0);
    }
    
    if(cyl_msg.count){
        cyl_msg.utime = msg->utime;
        erlcm_cylinder_list_t_publish(lcm, "CYLINDER_LIST", &cyl_msg);
        
        free(cyl_msg.list);    
    }
}

int
main (int argc, char** argv)
{
    //std::string filename = argv[1];
    //std::cout << "Reading " << filename << std::endl;

    lcm_t *lcm = bot_lcm_get_global (NULL);
    
    /*pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    
    if (pcl::io::loadPCDFile<pcl::PointXYZ> (filename, *cloud) == -1) // load the file
        {
            PCL_ERROR ("Couldn't read file");
            return -1;
        }
    */
    //segment_planes(cloud, lcm);

    erlcm_segment_list_t_subscribe(lcm, "PCL_SEGMENT_LIST", on_point_list, lcm);

    GMainLoop *mainloop = g_main_loop_new(NULL, FALSE);

    if (!mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }

    bot_glib_mainloop_attach_lcm(lcm);

     //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (mainloop);

    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(mainloop);

    bot_glib_mainloop_detach_lcm(lcm);

    return (0);
}
