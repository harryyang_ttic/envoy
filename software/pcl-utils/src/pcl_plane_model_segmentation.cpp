#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <lcm/lcm.h>
#include <lcmtypes/er_lcmtypes.h>
#include <bot_core/bot_core.h>
#include <pcl/filters/filter.h>
#include <pcl/common/centroid.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/sample_consensus/sac_model_plane.h>

//#include <interfaces/pcl_lcm_lib.hpp>

void segment_planes(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, lcm_t *lcm){
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob (new pcl::PointCloud<pcl::PointXYZ>); 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>); 
    const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
    //pcl::VoxelGrid<sensor_msgs::PointCloud2> sor;
    pcl::VoxelGrid<pcl::PointXYZ> sor;
    
    sor.setInputCloud (cloud->makeShared ());
    sor.setLeafSize (0.1f, 0.1f, 0.1f); //was 0.01
    sor.filter (*cloud_filtered_blob);

    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (1000);

    //play with this to get different size segments 
    seg.setDistanceThreshold (0.05); //was 0.01
    
    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    
    pcl::PCDWriter writer;
    writer.write<pcl::PointXYZ> ("out_downsampled.pcd", *cloud_filtered_blob, false);
        
    int i = 0, nr_points = (int) cloud_filtered_blob->points.size ();

    erlcm_segment_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.segments = NULL;
    
    msg.no_segments = 0;
    
    // While 30% of the original cloud is still there
    while (cloud_filtered_blob->points.size () > 0.3 * nr_points){
        // Segment the largest planar component from the remaining cloud
        seg.setInputCloud (cloud_filtered_blob);
        seg.segment (*inliers, *coefficients);
        if (inliers->indices.size () == 0){
            std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
            break;
        }
        
        std::cerr << "Model coefficients: " << coefficients->values[0] << " " 
                                      << coefficients->values[1] << " "
                                      << coefficients->values[2] << " " 
                                      << coefficients->values[3] << std::endl;
        
        msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
        
        erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];//(erlcm_seg_point_list_t *) calloc(1, sizeof(erlcm_seg_point_list_t));
        seg_msg->segment_id = msg.no_segments; 
        seg_msg->no_points = inliers->indices.size();

        seg_msg->coefficients[0] = coefficients->values[0]; 
        seg_msg->coefficients[1] = coefficients->values[1]; 
        seg_msg->coefficients[2] = coefficients->values[2]; 
        seg_msg->coefficients[3] = coefficients->values[3];

        pcl::PointXYZ sensor_loc; 

        sensor_loc.x = 0;
        sensor_loc.y = 0;
        sensor_loc.z = 0; 
        
        double dist = pcl::pointToPlaneDistance(sensor_loc, coefficients->values[0], 
                                                coefficients->values[1], 
                                                coefficients->values[2], 
                                                coefficients->values[3]);

        fprintf(stderr,"Distance to plane : %f\n", dist);
        
        if(fabs(dist - 1.2) < 0.4){
            fprintf(stderr, "Ground plane found\n"); 
        }

        // Extract the inliers
        extract.setInputCloud(cloud_filtered_blob);
        extract.setIndices (inliers);
        extract.setNegative (false);
        extract.filter (*cloud_p);

        std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." << std::endl;
        
        seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));

        double mean[3] = {0,0,0};
        
        for (size_t k = 0; k < cloud_p->points.size (); ++k){
            seg_msg->points[k].xyz[0] = cloud_p->points[k].x; 
            seg_msg->points[k].xyz[1] = cloud_p->points[k].y; 
            seg_msg->points[k].xyz[2] = cloud_p->points[k].z; 
            mean[0] += cloud_p->points[k].x; 
            mean[1] += cloud_p->points[k].y; 
            mean[2] += cloud_p->points[k].z; 
        }
        
        seg_msg->centroid[0] = mean[0] / cloud_p->points.size (); 
        seg_msg->centroid[1] = mean[1] / cloud_p->points.size (); 
        seg_msg->centroid[2] = mean[2] / cloud_p->points.size (); 

        fprintf(stderr, "\tMean : %f, %f, %f\n", seg_msg->centroid[0], 
                seg_msg->centroid[1], seg_msg->centroid[2]);
        
        msg.no_segments++; 
        
        // Create the filtering object
        extract.setNegative (true);
        extract.filter (*cloud_filtered_blob);
        
        i++;
    }
    
    //publish
    erlcm_segment_list_t_publish(lcm, "PCL_SEGMENT_LIST", &msg);
    
    for(int k = 0; k < msg.no_segments; k++){
        free(msg.segments[k].points);
    }
    free(msg.segments);        
}

int
main (int argc, char** argv)
{
    std::string filename = argv[1];
    std::cout << "Reading " << filename << std::endl;

    lcm_t *lcm = bot_lcm_get_global (NULL);
    
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    
    if (pcl::io::loadPCDFile<pcl::PointXYZ> (filename, *cloud) == -1) // load the file
        {
            PCL_ERROR ("Couldn't read file");
            return -1;
        }

    segment_planes(cloud, lcm);

    return (0);
}
