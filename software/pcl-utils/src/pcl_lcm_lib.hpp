#ifndef PCL_LCM_LIB_H
#define PCL_LCM_LIB_H

#include <iostream>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <interfaces/pcl_to_lcm.hpp>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <lcm/lcm.h>
#include <lcmtypes/er_lcmtypes.h>
#include <bot_core/bot_core.h>
#include <pcl/filters/filter.h>
#include <pcl/common/centroid.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
#include <lcmtypes/er_lcmtypes.h>
#include <geom_utils/convexhull.h>
#include "pcl_to_lcm.hpp"

typedef pcl::PointXYZ PointT;

/*typedef struct _xyz_t{
    double x;
    double y;
    double z;
} xyz_t;

typedef struct _xyz_list_t{
    int count;
    xyz_t *points;
    } xyz_list_t;*/

typedef struct _object_t{
    double center[3];
    double radius;
    int is_cylinder;
} object_t; 

typedef struct _cylinder_object_list_t{
    int count;
    object_t *objects;
} object_list_t;

struct pcl_plane_t {
    //pcl::PointCloud<pcl::PointXYZ>::Ptr cloud;
    xyz_list_t cloud;
    double coeffs[4];
    double mean[3];
};

struct pcl_object_t {
    //pcl::PointCloud<pcl::PointXYZ>::Ptr cloud;
    xyz_list_t cloud;
    double mean[3];
    double max_extent[3];
    double min_extent[3];
    object_t object;
};

typedef struct _table_object_t{
    //inlier object information 
    pcl_plane_t table_plane;
    int no_objects;
    pcl_object_t *objects;
} table_object_t;

void add_object_to_list(object_list_t *list, object_t *object);

void add_object_to_lcm_cylinder_list(erlcm_cylinder_list_t *msg, table_object_t *table);

void free_table_object(table_object_t *table);

void get_points_from_pcl_pointcloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, xyz_list_t *pts);

void add_table_to_lcm_msg(erlcm_segment_list_t *msg, table_object_t *table);
    
void add_cloud_to_table_object(table_object_t *table, 
                               pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, 
                               double max_extents[3], double min_extents[3],
                               double mean[3], double center[3], 
                               double radius, int is_cylinder);



void add_to_cylinder_object(erlcm_cylinder_list_t *cyl_objects, double xyz[3], double dir[3], double radius);

//int find_table_and_objects(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, erlcm_segment_list_t *msg, table_object_t *table);//erlcm_cylinder_list_t *cyl_objects);

int find_table_and_objects(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, table_object_t *table);

void add_point_cloud_to_lcm_msg(erlcm_segment_list_t *msg, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, double coeffs[4], double mean[3]);

void segment_planes(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, lcm_t *lcm);

void find_tabletop_objects_after_segmentation(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, 
                                              double forward_heading, double bot_local_pos[3],
                                              lcm_t *lcm, object_list_t *detections);

void compute_normals(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, lcm_t *lcm); 

void publish_pcl_points_to_lcm(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, lcm_t *_lcm);

void publish_pcl_points_to_lcm(pcl::PointCloud<pcl::PointXYZ> cloud_p, lcm_t *_lcm);

void publish_pcl_normal_points_to_lcm(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, 
                                      pcl::PointCloud<pcl::Normal>::Ptr normals, 
                                      lcm_t *_lcm);

//void find_table_planes(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, int filter, lcm_t *lcm);

//void find_tabletop_objects(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, int filter, lcm_t *lcm);
int find_tabletop_objects(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, int filter, int cluster_table, 
                          int fit_cylinders_to_objects, int find_best_fit, int draw_table, int draw_objects, 
                          double forward_heading, double bot_local_pos[3],
                          object_list_t *detected_objects, lcm_t *lcm);

int fit_cylinder(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, double param[8]);

#endif
