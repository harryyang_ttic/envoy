
#include "pcl_to_lcm.hpp"
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>


void publish_pcl_points_to_lcm(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, lcm_t *_lcm){
    erlcm_xyz_point_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.no_points = cloud_p->points.size();
    
    msg.points = (erlcm_xyz_point_t *)calloc(msg.no_points, sizeof(erlcm_xyz_point_t));

    for (size_t k = 0; k < cloud_p->points.size (); ++k){
        msg.points[k].xyz[0] = cloud_p->points[k].x; 
        msg.points[k].xyz[1] = cloud_p->points[k].y; 
        msg.points[k].xyz[2] = cloud_p->points[k].z; 
    }

    //publish
    erlcm_xyz_point_list_t_publish(_lcm, "PCL_XYZ_LIST", &msg);
    
    free(msg.points);   
}

void publish_xyz_points_to_lcm(xyz_list_t* cloud_p, lcm_t *_lcm){
    erlcm_xyz_point_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.no_points = cloud_p->count;
    
    msg.points = (erlcm_xyz_point_t *) cloud_p->points;

    //publish
    erlcm_xyz_point_list_t_publish(_lcm, "PCL_XYZ_LIST", &msg);
}

void publish_xyz_points_to_lcm(xyz_list_t cloud, lcm_t *_lcm){
    erlcm_xyz_point_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.no_points = cloud.count;
    
    msg.points = (erlcm_xyz_point_t *) cloud.points;

    //publish
    erlcm_xyz_point_list_t_publish(_lcm, "PCL_XYZ_LIST", &msg);
}

void publish_bot_frame(int64_t utime, lcm_t *lcm) { 
    vs_obj_collection_t objs_msg; 
    objs_msg.id = 100; 
    objs_msg.name = "LOCAL_POSE"; 
    objs_msg.type = VS_OBJ_COLLECTION_T_POSE3D; 
    objs_msg.reset = true; 
    vs_obj_t poses[1]; 
    poses[0].id = 1; 

    poses[0].x = 0;
    poses[0].y = 0;
    poses[0].z = 0;
    poses[0].roll = 0;
    poses[0].pitch = 0;
    poses[0].yaw = 0;

    objs_msg.nobjs = 1; 
    objs_msg.objs = &poses[0];
    vs_obj_collection_t_publish(lcm, "OBJ_COLLECTION", &objs_msg);
    return;
}

void publish_pcl_points_to_lcm(pcl::PointCloud<pcl::PointXYZ> cloud, lcm_t *_lcm){
    erlcm_xyz_point_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.no_points = cloud.points.size();
    
    msg.points = (erlcm_xyz_point_t *)calloc(msg.no_points, sizeof(erlcm_xyz_point_t));

    for (size_t k = 0; k < cloud.points.size (); ++k){
        msg.points[k].xyz[0] = cloud.points[k].x; 
        msg.points[k].xyz[1] = cloud.points[k].y; 
        msg.points[k].xyz[2] = cloud.points[k].z; 
    }

    //publish
    erlcm_xyz_point_list_t_publish(_lcm, "PCL_XYZ_LIST", &msg);
    
    free(msg.points);   

    //vs_point3d_list_collection_t point_lists;
    
}

double * select_color(char* str) { 
    double rgb[3];
    if (strcmp(str, "red") == 0)
        rgb[0] = .5f, rgb[1] = 0.f, rgb[2] = 0.f;
    else if (strcmp(str, "blue") == 0)
        rgb[0] = 0.f, rgb[1] = 0.f, rgb[2] = .5f;
    else if (strcmp(str, "green") == 0)
        rgb[0] = 0.f, rgb[1] = .5f, rgb[2] = 0.f;
    else if (strcmp(str, "yellow") == 0)
        rgb[0] = 0.5f, rgb[1] = .5f, rgb[2] = 0.f;
    else if (strcmp(str, "magenta") == 0)
        rgb[0] = 0.5f, rgb[1] = 0.f, rgb[2] = 0.5f;
    else if (strcmp(str, "cyan") == 0)
        rgb[0] = 0.f, rgb[1] = .5f, rgb[2] = 0.5f;
    else if (strcmp(str, "purple") == 0)
        rgb[0] = 0.25f, rgb[1] = 0.f, rgb[2] = 0.5f;
    // else if (strcmp(str, "green") == 0)
    //     rgb[0] = 0.f, rgb[1] = .5f, rgb[2] = 0.f;
    // else if (strcmp(str, "green") == 0)
    //     rgb[0] = 0.f, rgb[1] = .5f, rgb[2] = 0.f;
    else 
        rgb[0] = 0.3f, rgb[1] = 0.3f, rgb[2] = 0.3f;
    return rgb;
}

void populate_viz_cloud(vs_point3d_list_t &viz_list, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& track_cloud, 
                        char* str) { 
    //cv::Vec3f color = select_color(str); 
    viz_list.nnormals = 0; 
    viz_list.normals = NULL; 
    viz_list.npointids = 0; 
    viz_list.pointids = NULL; 

    // This is set from 3d_klt_tracker (hack!!)
    uint64_t time_id = (uint64_t)bot_timestamp_now(); 
    viz_list.id = time_id; 
    viz_list.collection = 100; 
    viz_list.element_id = 1;

    viz_list.npoints = track_cloud->points.size();
    viz_list.points = new vs_point3d_t[track_cloud->points.size()];

    viz_list.ncolors = track_cloud->points.size(); 
    viz_list.colors = new vs_color_t[track_cloud->points.size()];

    for (int k=0; k<track_cloud->points.size(); k++) { 
        viz_list.points[k].x = track_cloud->points[k].x;
        viz_list.points[k].y = track_cloud->points[k].y;
        viz_list.points[k].z = track_cloud->points[k].z;
        viz_list.colors[k].r = 0.0;//color[0];
        viz_list.colors[k].g = 1;//color[1];
        viz_list.colors[k].b = 0;//color[2];
    }
}

void populate_viz_cloud(vs_point3d_list_t &viz_list, pcl::PointCloud<pcl::PointXYZ>::Ptr& track_cloud, 
                        char* str) { 
    //cv::Vec3f color = select_color(str); 
    viz_list.nnormals = 0; 
    viz_list.normals = NULL; 
    viz_list.npointids = 0; 
    viz_list.pointids = NULL; 

    // This is set from 3d_klt_tracker (hack!!)
    uint64_t time_id = (uint64_t)bot_timestamp_now(); 
    viz_list.id = time_id; 
    viz_list.collection = 100; 
    viz_list.element_id = 1;

    viz_list.npoints = track_cloud->points.size();
    viz_list.points = new vs_point3d_t[track_cloud->points.size()];

    viz_list.ncolors = track_cloud->points.size(); 
    viz_list.colors = new vs_color_t[track_cloud->points.size()];

    for (int k=0; k<track_cloud->points.size(); k++) { 
        viz_list.points[k].x = track_cloud->points[k].x;
        viz_list.points[k].y = track_cloud->points[k].y;
        viz_list.points[k].z = track_cloud->points[k].z;
        viz_list.colors[k].r = 0.0;//color[0];
        viz_list.colors[k].g = 1;//color[1];
        viz_list.colors[k].b = 0;//color[2];
    }
}

void populate_viz_cloud(vs_point3d_list_t &viz_list, pcl::PointCloud<pcl::PointXYZ>& track_cloud, 
                        char* str) { 
    //cv::Vec3f color = select_color(str); 
    viz_list.nnormals = 0; 
    viz_list.normals = NULL; 
    viz_list.npointids = 0; 
    viz_list.pointids = NULL; 

    uint64_t time_id = (uint64_t)bot_timestamp_now(); 
    viz_list.id = time_id; 
    viz_list.collection = 100; 
    viz_list.element_id = 1;

    viz_list.npoints = track_cloud.points.size();
    viz_list.points = new vs_point3d_t[track_cloud.points.size()];

    viz_list.ncolors = track_cloud.points.size(); 
    viz_list.colors = new vs_color_t[track_cloud.points.size()];

    for (int k=0; k<track_cloud.points.size(); k++) { 
        viz_list.points[k].x = track_cloud.points[k].x;
        viz_list.points[k].y = track_cloud.points[k].y;
        viz_list.points[k].z = track_cloud.points[k].z;
        viz_list.colors[k].r = 0.0;//color[0];
        viz_list.colors[k].g = 1;//color[1];
        viz_list.colors[k].b = 0;//color[2];
    }
}

void populate_viz_cloud(vs_point3d_list_t &viz_list, erlcm_seg_point_list_t *segment, 
                        float color[3]) { 
    //cv::Vec3f color = select_color(str); 
    viz_list.nnormals = 0; 
    viz_list.normals = NULL; 
    viz_list.npointids = 0; 
    viz_list.pointids = NULL; 

    uint64_t time_id = (uint64_t)bot_timestamp_now(); 
    viz_list.id = time_id; 
    viz_list.collection = 100; 
    viz_list.element_id = 1;

    viz_list.npoints = segment->no_points;//track_cloud.points.size();
    viz_list.points = new vs_point3d_t[segment->no_points];

    viz_list.ncolors = segment->no_points;
    viz_list.colors = new vs_color_t[segment->no_points];

    for (int k=0; k< segment->no_points; k++) { 
        viz_list.points[k].x = segment->points[k].xyz[0];//track_cloud.points[k].x;
        viz_list.points[k].y = segment->points[k].xyz[1];//track_cloud.points[k].y;
        viz_list.points[k].z = segment->points[k].xyz[2];//track_cloud.points[k].z;
        viz_list.colors[k].r = color[0];
        viz_list.colors[k].g = color[1];
        viz_list.colors[k].b = color[2];
    }
}

void publish_pcl_points_to_collection(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, lcm_t *_lcm, char *name){//, char *name){
    publish_bot_frame(bot_timestamp_now(), _lcm);
    
    vs_point3d_list_collection_t cloud_msg;
    cloud_msg.id = 1000; 
    cloud_msg.name = name;//"TRACK_CLOUD"; 
    cloud_msg.type = VS_POINT3D_LIST_COLLECTION_T_POINT; 
    cloud_msg.reset = true; 

    vs_point3d_list_t point_list;
    populate_viz_cloud(point_list, cloud_p, "points");
    cloud_msg.nlists = 1;
    cloud_msg.point_lists = &point_list;
    vs_point3d_list_collection_t_publish(_lcm, "POINTS_COLLECTION", &cloud_msg);
    
    delete [] cloud_msg.point_lists->points;
    delete [] cloud_msg.point_lists->colors;    
}

void publish_segment_to_collections(erlcm_segment_list_t *msg, lcm_t *lcm, char *name){
    publish_bot_frame(bot_timestamp_now(), lcm);

    vs_point3d_list_collection_t cloud_msg;
    cloud_msg.id = 1001; 
    cloud_msg.name = name;//"TRACK_CLOUD"; 
    cloud_msg.type = VS_POINT3D_LIST_COLLECTION_T_POINT; 
    cloud_msg.reset = true; 

    vs_point3d_list_t point_list;

    std::vector<vs_point3d_list_t> viz_track_cloud(msg->no_segments);

    for(int i=0; i < msg->no_segments; i++){
        float *color = bot_color_util_jet(i/(double) msg->no_segments);//{1, 0, 0};
        populate_viz_cloud(viz_track_cloud[i], &msg->segments[i], color);
    }
    cloud_msg.nlists = msg->no_segments;
    cloud_msg.point_lists = &viz_track_cloud[0];

    vs_point3d_list_collection_t_publish(lcm, "POINTS_COLLECTION", &cloud_msg);

    for(int i=0; i < msg->no_segments; i++){
        delete [] viz_track_cloud[i].points;
        delete [] viz_track_cloud[i].colors;
    }
}

void publish_pcl_points_to_collection(pcl::PointCloud<pcl::PointXYZ> cloud, lcm_t *_lcm, char *name){
    publish_bot_frame(bot_timestamp_now(), _lcm);
    
    vs_point3d_list_collection_t cloud_msg;
    cloud_msg.id = 1000; 
    cloud_msg.name = name;//"TRACK_CLOUD"; 
    cloud_msg.type = VS_POINT3D_LIST_COLLECTION_T_POINT; 
    cloud_msg.reset = true; 

    vs_point3d_list_t point_list;
    populate_viz_cloud(point_list, cloud, "points");
    cloud_msg.nlists = 1;
    cloud_msg.point_lists = &point_list;
    vs_point3d_list_collection_t_publish(_lcm, "POINTS_COLLECTION", &cloud_msg);
    
    delete [] cloud_msg.point_lists->points;
    delete [] cloud_msg.point_lists->colors;    
}

//don't seem to work - get sudeep to have a look 
/*void publish_pcl_points_to_collection(pcl::PointCloud<pcl::PointXYZ> cloud_p, lcm_t *_lcm){//, char *name){
    
    Viewer viewer(_lcm);
    PointCloudCollection pc_collection(3, std::string("Point cloud"), 1, VS_POINT3D_LIST_COLLECTION_T_POINT);
    PointCloudPtr pc = boost::make_shared<PointCloud>(100);
    pc->addPoint(10.,  1.,  1., 1., 0., 0.);
    pc->addPoint( 1., 10.,  1., 0., 1., 0.);
    pc->addPoint( 1.,  1., 10., 0., 0., 1.);
    pc_collection.add(boost::make_shared<PointCloudPtr>(pc));
    viewer.sendCollection(pc_collection, true);
}*/

pcl::PointCloud<pcl::PointXYZ>::Ptr getPCLPointsFromLCM(xyz_point_list_t *l_points, int width, 
                                                        int height, lcm_t *_lcm){
    static const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>& cloud = *cloud_ptr;
    
    //for a filtered point cloud - we can forget the height/width n * 1 
    cloud.height   = height;
    cloud.width    = width;
        
    if(width > 1) 
        cloud.is_dense = true;
    else
        cloud.is_dense = false;
    cloud.points.resize (cloud.width * cloud.height);
    
    fprintf(stderr, "Utime : %f Size of Points : %d \n", l_points->utime / 1.0e6, l_points->no_points);

    for(int ind_pcl=0; ind_pcl < l_points->no_points; ind_pcl++){
        cloud.points[ind_pcl].x = l_points->points[ind_pcl].xyz[0];
        cloud.points[ind_pcl].y = l_points->points[ind_pcl].xyz[1];
        cloud.points[ind_pcl].z = l_points->points[ind_pcl].xyz[2];
    }

    if(1 && _lcm)
        publish_pcl_points_to_lcm(cloud, _lcm);

    return cloud_ptr;
    
}

void publish_pcl_normal_points_to_lcm(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, 
                                      pcl::PointCloud<pcl::Normal>::Ptr normals, lcm_t *_lcm){
    erlcm_normal_point_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.no_points = cloud_p->points.size();
    
    msg.points = (erlcm_normal_point_t *)calloc(msg.no_points, sizeof(erlcm_normal_point_t));

    for (size_t k = 0; k < cloud_p->points.size (); ++k){
        msg.points[k].xyz[0] = cloud_p->points[k].x; 
        msg.points[k].xyz[1] = cloud_p->points[k].y; 
        msg.points[k].xyz[2] = cloud_p->points[k].z; 
        
        msg.points[k].normals[0] = normals->points[k].normal[0];
        msg.points[k].normals[1] = normals->points[k].normal[1];
        msg.points[k].normals[2] = normals->points[k].normal[2];
    }

    //publish
    erlcm_normal_point_list_t_publish(_lcm, "PCL_NORMAL_LIST", &msg);
    free(msg.points);    
}
