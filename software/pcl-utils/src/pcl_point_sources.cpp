#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>

#include <velodyne/velodyne.h>
#include <path_util/path_util.h>

#include <lcmtypes/senlcm_velodyne_list_t.h>
#include <lcmtypes/er_lcmtypes.h>

#include <gsl/gsl_blas.h>
#include <bot_frames/bot_frames.h>

#include <pcl/visualization/cloud_viewer.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d.h>
#include <lcmtypes/kinect_frame_msg_t.h>
#include <kinect/kinect-utils.h>
#include <pcl/common/common_headers.h>
#include <zlib.h>
#include "pcl/features/normal_3d.h"
#include <pcl/features/integral_image_normal.h>

#include <interfaces/pcl_to_lcm.hpp>

typedef struct _pose_data_t pose_data_t;
struct _pose_data_t {
    double pose[6];
    double motion[6];
    int64_t utime;
};

typedef struct _vel_params_t vel_params_t;
struct _vel_params_t {
    int64_t last_collector_utime;

    int have_data;
    
    velodyne_calib_t *calib;
    velodyne_laser_return_collector_t *collector;
    
    int64_t 	      last_velodyne_data_utime;
    int64_t           last_pose_utime;
};

typedef struct _kinect_params_t k_params_t;
struct _kinect_params_t {
    kinect_frame_msg_t *kinect_msg;
    
    int width;
    int height;

    // raw disparity
    uint16_t* disparity;
    int need_to_recompute_frame_data;

    uint8_t* uncompress_buffer;
    int uncompress_buffer_size;

    uint8_t* rgb_data;
    KinectCalibration* kcal;
};

typedef struct
{
    lcm_t *lcm;
    BotParam *param;
    BotFrames *frames;
    GMainLoop *mainloop;

    pthread_t  work_thread;
    
    bot_core_pose_t *bot_pose_last;

    vel_params_t *v_params;
    k_params_t *k_params;

    int velodyne; //if 0 - we use the kinect 

    int do_viewer;
    int do_planes;
    int write;

    int decimation_factor;
    double draw;
    double smoothing_size; 
    
    GMutex *mutex;    
} state_t;

static int
process_velodyne (const senlcm_velodyne_t *msg, state_t *self);

static void write_kinect_to_pcd(state_t *self){    

    static const pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>);
    static const pcl::PointCloud<pcl::PointXYZRGB>::Ptr color_cloud_ptr (new pcl::PointCloud<pcl::PointXYZRGB>);
        
    pcl::PointCloud<pcl::PointXYZ>& cloud = *point_cloud_ptr;
    pcl::PointCloud<pcl::PointXYZRGB>& color_cloud = *color_cloud_ptr;
        
    // Fill in the cloud data
    cloud.height   = self->k_params->height;
    cloud.width    = self->k_params->width;

    cloud.is_dense = true;
    cloud.points.resize (cloud.width * cloud.height);

    color_cloud.height   = self->k_params->height;
    color_cloud.width    = self->k_params->width;

    color_cloud.is_dense = true;
    color_cloud.points.resize (color_cloud.width * color_cloud.height);

    int p = 0;

    if(self->k_params->kinect_msg->depth.depth_data_format == KINECT_DEPTH_MSG_T_DEPTH_MM){
        double fx_inv = 1/ self->k_params->kcal->intrinsics_depth.fx;
        double cx = self->k_params->kcal->intrinsics_depth.cx;
        double cy = self->k_params->kcal->intrinsics_depth.cy;
        
        for(int u=0; u< self->k_params->width; u++) {
            for(int v=0; v< self->k_params->height; v++) {
                uint16_t disparity = self->k_params->disparity[v*self->k_params->width+u];
                float depth = disparity / 1000.0;

                uint8_t r, g, b;
                r = self->k_params->rgb_data[v*self->k_params->width*3 + u*3 + 0];
                g = self->k_params->rgb_data[v*self->k_params->width*3 + u*3 + 1];
                b = self->k_params->rgb_data[v*self->k_params->width*3 + u*3 + 2];

                //glColor3f(r / 255.0, g / 255.0, b / 255.0);
                float x = ( u * fx_inv - cx* fx_inv) * depth;
                float y = ( v * fx_inv - cy* fx_inv) * depth;

                //fprintf(stderr,"%d,%d, %d,%d,%d\n", u, v, r, g, b);

                cloud.points[p].x = x;
                cloud.points[p].y = y;
                cloud.points[p].z = depth;

                color_cloud.points[p].x = x;
                color_cloud.points[p].y = y;
                color_cloud.points[p].z = depth;

                uint32_t rgb = ((uint32_t)r << 16 | (uint32_t)g << 8 | (uint32_t)b);

                //uint32_t rgb = (static_cast<uint32_t>(r) << 16 |
                //              static_cast<uint32_t>(g) << 8 | static_cast<uint32_t>(b));
                
                //the color doesnt seem to work properly -even when given red or green or something 
                color_cloud.points[p].rgb = *reinterpret_cast<float*>(&rgb);
                color_cloud.points[p].r = r;
                color_cloud.points[p].b = b;
                color_cloud.points[p].g = g;
                //*reinterpret_cast<float*>(&rgb);
                p++;       
            }
        }
    }
    else if(self->k_params->kinect_msg->depth.depth_data_format == KINECT_DEPTH_MSG_T_DEPTH_11BIT ){
        //fprintf(stderr,"Not handled\n");
        /*if(msg->depth.compression != KINECT_DEPTH_MSG_T_COMPRESSION_NONE) {

            m_uncompressBuffer.resize(msg->depth.uncompressed_size);

            unsigned long dlen = msg->depth.uncompressed_size;
            int status = uncompress(&m_uncompressBuffer[0], &dlen, msg->depth.depth_data, msg->depth.depth_data_nbytes);
            if(status != Z_OK) {
                return;
            }
            depth_data = (uint16_t*)&m_uncompressBuffer[0];

            }*/

        if ( self->k_params->kinect_msg->depth.depth_data_format != KINECT_DEPTH_MSG_T_DEPTH_11BIT ) {
            std::cout << "ERROR: unable to handle anything but 11 bit depth data" << std::endl;
            return;
        }

        int p = 0;

        double depth_to_depth_xyz[16];
        kinect_calib_get_depth_uvd_to_depth_xyz_4x4(self->k_params->kcal, depth_to_depth_xyz);
        uint16_t* depth_data = (uint16_t*) self->k_params->kinect_msg->depth.depth_data;

        for ( int u = 0; u < self->k_params->kinect_msg->depth.width; u++ ) {
            for ( int v = 0; v < self->k_params->kinect_msg->depth.height; v++ ) {

                if (depth_data[v*self->k_params->kinect_msg->depth.width + u] == 2047) continue;

                double pix[] = { u, v, depth_data[v*self->k_params->kinect_msg->depth.width + u], 1.0 };
                double xyz[4];
                bot_matrix_vector_multiply_4x4_4d (depth_to_depth_xyz, pix, xyz);

                cloud.points[p].x = xyz[0]/xyz[3];
                cloud.points[p].y = xyz[1]/xyz[3];
                cloud.points[p].z = xyz[2]/xyz[3];

                color_cloud.points[p].x = xyz[0]/xyz[3];
                color_cloud.points[p].y = xyz[1]/xyz[3];
                color_cloud.points[p].z = xyz[2]/xyz[3];

                uint8_t r, g, b;
                r = self->k_params->rgb_data[v*self->k_params->width*3 + u*3 + 0];
                g = self->k_params->rgb_data[v*self->k_params->width*3 + u*3 + 1];
                b = self->k_params->rgb_data[v*self->k_params->width*3 + u*3 + 2];

                uint32_t rgb = ((uint32_t)r << 16 | (uint32_t)g << 8 | (uint32_t)b);
                
                //the color doesnt seem to work properly -even when given red or green or something 
                color_cloud.points[p].rgb = *reinterpret_cast<float*>(&rgb);
                color_cloud.points[p].r = r;
                color_cloud.points[p].b = b;
                color_cloud.points[p].g = g;
                //*reinterpret_cast<float*>(&rgb);
                p++;       
            }
        }
    }

    if(self->draw)
        publish_pcl_points_to_lcm(cloud, self->lcm);

    if(self->write){
        char name[2048]; 
        sprintf(name, "kinect_%d.pcd", (int)self->k_params->kinect_msg->timestamp); 
        
        char name_color[2048]; 
        sprintf(name_color, "kinect_color_%d.pcd", (int)self->k_params->kinect_msg->timestamp); 
        
        //pcl::io::savePCDFileASCII ("out_pcd.pcd", cloud);
        pcl::io::savePCDFileASCII (name, cloud);
        std::cerr << "Saved " << cloud.points.size () << " data points to " << name << std::endl;
        
        pcl::io::savePCDFileASCII (name_color, color_cloud);
        std::cerr << "Saved " << cloud.points.size () << " data points to " << name_color << std::endl;
    }
}

static void write_velodyne_to_pcl(state_t *self){    

    velodyne_laser_return_collection_t *lrc =
        velodyne_collector_pull_collection (self->v_params->collector);
    
    double sensor_to_local[12];
    
    if (!bot_frames_get_trans_mat_3x4_with_utime (self->frames, "VELODYNE",
                                                  "VELODYNE", lrc->utime,
                                                  sensor_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;
    }

    static const pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>);

    pcl::PointCloud<pcl::PointXYZ>& cloud = *point_cloud_ptr;

    cloud.height   = 32;
    cloud.width    = lrc->num_lr / 32.0;

    cloud.is_dense = true;
    cloud.points.resize (cloud.width * cloud.height);

    fprintf(stderr, "No of returns : %d\n" , lrc->num_lr);

    //hmm the points are in colum major format - need to set up the point cloud properly
    for(int i = 0; i < cloud.height; i++){
        for(int s = 0; s < cloud.width; s++) {        
            int ind_vel = s * 32 + i; 
            int ind_pcl = cloud.width * i + s; 
            velodyne_laser_return_t *lr = &(lrc->laser_returns[ind_vel]);
        
            double local_xyz[3];
            bot_vector_affine_transform_3x4_3d (sensor_to_local, lr->xyz, local_xyz);
            
            cloud.points[ind_pcl].x = local_xyz[0]; 
            cloud.points[ind_pcl].y = local_xyz[1]; 
            cloud.points[ind_pcl].z = local_xyz[2];
        }
    }

    if(self->draw)
        publish_pcl_points_to_lcm(cloud, self->lcm);
    
    if(self->write){
        char name[2048]; 
        sprintf(name, "velodyne_%d.pcd", (int)lrc->utime); 
        
        //pcl::io::savePCDFileASCII ("out_pcd.pcd", cloud);
        pcl::io::savePCDFileASCII (name, cloud);
        std::cerr << "Saved " << cloud.points.size () << " data points to test_pcd.pcd." << std::endl;
    }
}

static inline void
_matrix_vector_multiply_3x4_4d (const double m[12], const double v[4],
        double result[3])
{
    result[0] = m[0]*v[0] + m[1]*v[1] + m[2] *v[2] + m[3] *v[3];
    result[1] = m[4]*v[0] + m[5]*v[1] + m[6] *v[2] + m[7] *v[3];
    result[2] = m[8]*v[0] + m[9]*v[1] + m[10]*v[2] + m[11]*v[3];
}

static inline void
_matrix_transpose_4x4d (const double m[16], double result[16])
{
    result[0] = m[0];
    result[1] = m[4];
    result[2] = m[8];
    result[3] = m[12];
    result[4] = m[1];
    result[5] = m[5];
    result[6] = m[9];
    result[7] = m[13];
    result[8] = m[2];
    result[9] = m[6];
    result[10] = m[10];
    result[11] = m[14];
    result[12] = m[3];
    result[13] = m[7];
    result[14] = m[11];
    result[15] = m[15];
}

double RawDepthToMeters(uint16_t depthValue){
    
    if (depthValue < 2047) {
        return (1.0 /((double)(depthValue) * -0.0030711016 + 3.3309495161 ));
    }
    return 0.0f;
}

static void
on_velodyne_list (const lcm_recv_buf_t *rbuf, const char *channel,
		  const senlcm_velodyne_list_t *msg, void *user)
{
    state_t *self = (state_t *)user;

    for (int i=0; i < msg->num_packets; i++) 
	process_velodyne (&(msg->packets[i]), self);
        
    return;
}


static int
process_velodyne (const senlcm_velodyne_t *msg, state_t *self)
{
    int do_push_motion = 0; 
    static int64_t last_redraw_utime = 0;
    int64_t now = bot_timestamp_now();

    // Is this a scan packet?
    if (msg->packet_type == SENLCM_VELODYNE_T_TYPE_DATA_PACKET) {
        
        velodyne_laser_return_collection_t *lrc =
            velodyne_decode_data_packet(self->v_params->calib, msg->data, msg->datalen, msg->utime);
        
        int ret = velodyne_collector_push_laser_returns (self->v_params->collector, lrc);
        
        velodyne_free_laser_return_collection (lrc);
        
        if (VELODYNE_COLLECTION_READY == ret) {
            fprintf(stderr,".");
            //starting a new collection
            do_push_motion = 1;
        }
    }
        
    // Update the Velodyne's state information (pos, rpy, linear/angular velocity)
    if (do_push_motion) {
        //might need to add a naming convention
        write_velodyne_to_pcl(self);
        do_push_motion = 0;
    }
    return 0;
}

static void
recompute_frame_data(state_t *self)
{
    if(!self->k_params->kinect_msg) {
        return;
    }

    int npixels = self->k_params->width * self->k_params->height;

    const uint8_t* depth_data = self->k_params->kinect_msg->depth.depth_data;

    if(self->k_params->kinect_msg->depth.compression != KINECT_DEPTH_MSG_T_COMPRESSION_NONE) {
        if(self->k_params->kinect_msg->depth.uncompressed_size > self->k_params->uncompress_buffer_size) {
            self->k_params->uncompress_buffer_size = self->k_params->kinect_msg->depth.uncompressed_size;
            self->k_params->uncompress_buffer = (uint8_t*) realloc(self->k_params->uncompress_buffer, self->k_params->uncompress_buffer_size);
        }
        unsigned long dlen = self->k_params->kinect_msg->depth.uncompressed_size;
        int status = uncompress(self->k_params->uncompress_buffer, &dlen, 
                                self->k_params->kinect_msg->depth.depth_data, self->k_params->kinect_msg->depth.depth_data_nbytes);
        if(status != Z_OK) {
            return;
        }
        depth_data = self->k_params->uncompress_buffer;
    }

    switch(self->k_params->kinect_msg->depth.depth_data_format) {
        case KINECT_DEPTH_MSG_T_DEPTH_11BIT:
            if(G_BYTE_ORDER == G_LITTLE_ENDIAN) {
                int16_t* rdd = (int16_t*) depth_data;
                int i;
                for(i=0; i<npixels; i++) {
                    int d = rdd[i];
                    self->k_params->disparity[i] = d;
                }
            } else {
                fprintf(stderr, "Big endian systems not supported\n");
            }
            break;
        case KINECT_DEPTH_MSG_T_DEPTH_MM:
            if(G_BYTE_ORDER == G_LITTLE_ENDIAN) {
                int16_t* rdd = (int16_t*) depth_data;
                int i;
                for(i=0; i<npixels; i++) {
                    int d = rdd[i];
                    self->k_params->disparity[i] = d;
                }
            } else {
                fprintf(stderr, "Big endian systems not supported\n");
            }
            break;
        case KINECT_DEPTH_MSG_T_DEPTH_10BIT:
            fprintf(stderr, "10-bit depth data not supported\n");
            break;
        default:
            break;
    }
    
    write_kinect_to_pcd(self);

    fprintf(stderr,"Done\n");
}


static void 
on_kinect_frame (const lcm_recv_buf_t *rbuf, const char *channel,
                 const kinect_frame_msg_t *msg, void *user_data )
{
    state_t *self =  (state_t *) user_data;
    g_assert(self);
    fprintf(stderr,"Called - kinect()\n");
    g_mutex_lock (self->mutex);

    if(self->k_params->kinect_msg)
        kinect_frame_msg_t_destroy(self->k_params->kinect_msg);

    self->k_params->kinect_msg = kinect_frame_msg_t_copy(msg);

    if(msg->image.image_data_format == KINECT_IMAGE_MSG_T_VIDEO_RGB) {
        memcpy(self->k_params->rgb_data, msg->image.image_data, 
                self->k_params->width * self->k_params->height * 3);
    } else if(msg->image.image_data_format == KINECT_IMAGE_MSG_T_VIDEO_RGB_JPEG) {
        //jpegijg_decompress_8u_rgb(msg->image.image_data, msg->image.image_data_nbytes,
        //self->rgb_data, self->width, self->height, self->width * 3);
        fprintf(stderr,"Not doing decompression for now\n");
    }
    
    recompute_frame_data(self);

    g_mutex_unlock (self->mutex);
}

//pthread
static void *track_work_thread(void *user)
{
    state_t *s = (state_t*) user;

    printf("viewer: track_work_thread()\n");

    //seems the viewer is not ready in time when the first data comes in  

    //    SimpleOpenNIViewer v; //v;
    //s->viewer = &v;
    //s->viewer->run(); 

}

void
usage (const char* progName)
{
  std::cout << "\n\nUsage: "<<progName<<" [options]\n\n"
            << "Options:\n"
            << "-------------------------------------------\n"
            << "-h           this help\n"
            << "-s           Create Visualizer\n"
            << "-v           Use velodyne (otherwise defaults to kinect\n"
            << "-p           Run basic RANSAC to do plane extraction\n"
            << "\n\n";
}

int main(int argc, char** argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *self = (state_t*) calloc(1, sizeof(state_t));

    self->decimation_factor = 1;
    self->smoothing_size = 10.0;

    const char *optstring = "hpvVdD:s:w";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "velodyne", no_argument, 0, 'v' }, 
                                  { "write", no_argument, 0, 'w' },
				  { "planes", no_argument, 0, 'p' }, 
                                  { "viewer", no_argument, 0, 'V' }, 
                                  { "decimation", required_argument, 0, 'D' }, 
                                  { "draw", required_argument, 0, 'd' }, 
                                  { "smooth-size", required_argument, 0, 's' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    usage(argv[0]);
	    break;
	case 'v':
	    {
		fprintf(stderr,"Doing Velodyne\n");
                self->velodyne = 1;
		break;
	    }
	case 'p':
	    {
		fprintf(stderr,"Planes\n");
                self->do_planes = 1;
		break;
	    }

        case 'w':
            {
		fprintf(stderr,"Planes\n");
                self->write = 1;
		break;
	    }
        case 'V':
	    {
		fprintf(stderr,"Viz\n");
                self->do_viewer = 1;
       	break;
	    }
        case 'D':
	    {
		fprintf(stderr,"Decimation\n");
                self->decimation_factor = atoi(optarg);
       	break;
	    }
        case 'd':
	    {
                self->draw = 1;
                break;
	    }
        case 's':
	    {
                self->smoothing_size = strtod(optarg, 0);
                fprintf(stderr,"Smoothing Size : %f\n", self->smoothing_size);
                break;
	    }

	default:
	    {
		usage(argv[0]);
		break;
	    }
	}
    }

    if(self->velodyne){
        self->v_params = (vel_params_t *) calloc(1, sizeof(vel_params_t));
    }
    else{
        self->k_params = (k_params_t *) calloc(1,sizeof(k_params_t));
    }


    self->lcm = bot_lcm_get_global (NULL);
    if (!self->lcm) {
        fprintf (stderr,"Error: setup_renderer_laser() failed to get global lcm object\n");
        return -1;
    }

    self->param = bot_param_new_from_server(self->lcm, 1);
    if (!self->param) {
        fprintf (stderr,"Error: setup_renderer_laser() failed to get BotParam instance\n");
        return -1;
    }

    self->mutex = g_mutex_new ();
    self->frames = bot_frames_get_global (self->lcm, self->param);

    if(self->do_viewer){
        pthread_create(&self->work_thread, NULL, track_work_thread, self);
    }

    if(self->velodyne){
        char key[256] = {'\0'};
        snprintf (key, sizeof(key), "%s.channel", "calibration.velodyne");
        char *lcm_channel = bot_param_get_str_or_fail (self->param, key);
        char lcm_channel_list[256];
        snprintf (lcm_channel_list, sizeof(lcm_channel_list), "%s_LIST", lcm_channel);
        
        fprintf(stderr,"Channel : %s\n", lcm_channel);
        
        char *velodyne_model = bot_param_get_str_or_fail (self->param, "calibration.velodyne.model");
        char *calib_file = bot_param_get_str_or_fail (self->param, "calibration.velodyne.intrinsic_calib_file");
        
        char calib_file_path[2048];
        
        sprintf(calib_file_path, "%s/%s", getConfigPath(), calib_file);
        
        if (0 == strcmp (velodyne_model, VELODYNE_HDL_32E_MODEL_STR)) 
            self->v_params->calib = velodyne_calib_create (VELODYNE_SENSOR_TYPE_HDL_32E, calib_file_path);
        else if (0 == strcmp (velodyne_model, VELODYNE_HDL_64E_S1_MODEL_STR))
            self->v_params->calib = velodyne_calib_create (VELODYNE_SENSOR_TYPE_HDL_64E_S1, calib_file_path);
        else if (0 == strcmp (velodyne_model, VELODYNE_HDL_64E_S2_MODEL_STR))
            self->v_params->calib = velodyne_calib_create (VELODYNE_SENSOR_TYPE_HDL_64E_S2, calib_file_path);    
        else 
            fprintf (stderr, "ERROR: Unknown Velodyne model \'%s\'", velodyne_model);
        
        free (velodyne_model);
        free (calib_file);
        
        self->v_params->collector = velodyne_laser_return_collector_create (1, 0, 0);

        senlcm_velodyne_list_t_subscribe (self->lcm, lcm_channel_list, on_velodyne_list, self);
    }
    else{

        self->k_params->width = 640;
        self->k_params->height = 480;
        
        self->k_params->kinect_msg = NULL;
        
        self->k_params->disparity = (uint16_t*) malloc(self->k_params->width * self->k_params->height * sizeof(uint16_t));
        self->k_params->rgb_data = (uint8_t*) malloc(self->k_params->width * self->k_params->height * 3);
        
        self->k_params->kcal = kinect_calib_new();
        self->k_params->kcal->width = 640;
        self->k_params->kcal->height = 480;
        
        self->k_params->kcal->intrinsics_depth.fx = 576.09757860;
        self->k_params->kcal->intrinsics_depth.cx = 321.06398107;
        self->k_params->kcal->intrinsics_depth.cy = 242.97676897;
        
        self->k_params->kcal->intrinsics_rgb.fx = 528.49404721;
        self->k_params->kcal->intrinsics_rgb.cx = 319.50000000;
        self->k_params->kcal->intrinsics_rgb.cy = 239.50000000;
        self->k_params->kcal->intrinsics_rgb.k1 = 0;
        self->k_params->kcal->intrinsics_rgb.k2 = 0;
        
        self->k_params->kcal->shift_offset = 1093.4753;
        self->k_params->kcal->projector_depth_baseline = 0.07214;;

        double R[9] = { 0.999999, -0.000796, 0.001256, 0.000739, 0.998970, 0.045368, -0.001291, -0.045367, 0.998970 };
        double T[3] = { -0.015756, -0.000923, 0.002316 };

        memcpy(self->k_params->kcal->depth_to_rgb_rot, R, 9*sizeof(double));
        memcpy(self->k_params->kcal->depth_to_rgb_translation, T, 3*sizeof(double));

        self->k_params->uncompress_buffer = NULL;
        self->k_params->uncompress_buffer_size = 0;

        kinect_frame_msg_t_subscribe(self->lcm, "KINECT_FRAME", on_kinect_frame, self);
    }    

    self->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!self->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }
    
    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (self->lcm);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (self->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(self->mainloop);
  
    bot_glib_mainloop_detach_lcm(self->lcm);
    
    return (0);
}
