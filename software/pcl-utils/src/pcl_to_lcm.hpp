#ifndef PCL_TO_LCM_H
#define PCL_TO_LCM_H

#include <pcl/point_types.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
#include <point_types/point_types.h>

#include <lcmtypes/er_lcmtypes.h>
#include <visualization/viewer.hpp>
#include <visualization/pointcloud.hpp>

//#ifdef __cplusplus
//extern "C" {
//#endif

typedef struct _xyz_t{
    double xyz[3];
} xyz_t;

typedef struct _xyz_list_t{
    int count;
    xyz_t *points;
} xyz_list_t;

void publish_pcl_points_to_lcm(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, lcm_t *_lcm);

void publish_pcl_points_to_lcm(pcl::PointCloud<pcl::PointXYZ> cloud_p, lcm_t *_lcm);

void publish_xyz_points_to_lcm(xyz_list_t* cloud_p, lcm_t *_lcm);

void publish_xyz_points_to_lcm(xyz_list_t cloud, lcm_t *_lcm);

void publish_pcl_normal_points_to_lcm(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, 
                                      pcl::PointCloud<pcl::Normal>::Ptr normals, 
                                      lcm_t *_lcm);
pcl::PointCloud<pcl::PointXYZ>::Ptr getPCLPointsFromLCM(xyz_point_list_t *l_points, int width, 
                                                        int height, lcm_t *_lcm);

void publish_segment_to_collections(erlcm_segment_list_t *msg, lcm_t *lcm, char *name);

void publish_pcl_points_to_collection(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, lcm_t *_lcm, char *name);

void publish_pcl_points_to_collection(pcl::PointCloud<pcl::PointXYZ> cloud_p, lcm_t *_lcm, char *name);
//#ifdef __cplusplus
//}
//#endif

#endif
