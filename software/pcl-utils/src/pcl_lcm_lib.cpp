#include "pcl_lcm_lib.hpp"
//added for segmentation stuff
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/project_inliers.h>

void get_points_from_pcl_pointcloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, xyz_list_t *pts){
    pts->count =  cloud->points.size();
    pts->points = (xyz_t *) calloc(pts->count, sizeof(xyz_t));
    for(int i=0; i < cloud->points.size(); i++){
        pts->points[i].xyz[0] = cloud->points[i].x;
        pts->points[i].xyz[1] = cloud->points[i].y;
        pts->points[i].xyz[2] = cloud->points[i].z;
    }    
}

void add_object_to_lcm_cylinder_list(erlcm_cylinder_list_t *cyl_objects, table_object_t *table){

    cyl_objects->list = (erlcm_cylinder_model_t *) realloc(cyl_objects->list, sizeof(erlcm_cylinder_model_t) * (cyl_objects->count + table->no_objects)); 

    //now add the objects 
    for(int i=0; i < table->no_objects; i++){
        pcl_object_t *object = &table->objects[i];
        
        cyl_objects->list[cyl_objects->count].line_point[0] = object->object.center[0];
        cyl_objects->list[cyl_objects->count].line_point[1] = object->object.center[1];
        cyl_objects->list[cyl_objects->count].line_point[2] = object->object.center[2];

        cyl_objects->list[cyl_objects->count].line_direction[0] = table->table_plane.coeffs[0];//dir[0];
        cyl_objects->list[cyl_objects->count].line_direction[1] = table->table_plane.coeffs[1];//dir[1];
        cyl_objects->list[cyl_objects->count].line_direction[2] = table->table_plane.coeffs[2];//dir[2];
            
        cyl_objects->list[cyl_objects->count].radius = object->object.radius;
        cyl_objects->count++;  
    } 
}

void free_table_object(table_object_t *table){
    for(int i=0; i < table->no_objects; i++){
        free(table->objects[i].cloud.points);
    }
    free(table->objects);
    free(table->table_plane.cloud.points);
    free(table);
}

void add_object_to_list(object_list_t *list, object_t *object){
    list->objects = (object_t *) realloc(list->objects, sizeof(object_t) * (list->count + 1));
    memcpy(&list->objects[list->count], object, sizeof(object_t));
    list->count++;
}

void segment_planes(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, lcm_t *lcm){
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob (new pcl::PointCloud<pcl::PointXYZ>); 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>); 
    const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
    //pcl::VoxelGrid<sensor_msgs::PointCloud2> sor;
    pcl::VoxelGrid<pcl::PointXYZ> sor;
    
    sor.setInputCloud (cloud->makeShared ());
    sor.setLeafSize (0.01f, 0.01f, 0.01f); //was 0.01
    sor.filter (*cloud_filtered_blob);

    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (1000);

    //play with this to get different size segments 
    seg.setDistanceThreshold (0.01); //was 0.01
    
    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    
    if(0){
        pcl::PCDWriter writer;
        writer.write<pcl::PointXYZ> ("out_downsampled.pcd", *cloud_filtered_blob, false);
    }

    int i = 0, nr_points = (int) cloud_filtered_blob->points.size ();

    erlcm_segment_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.segments = NULL;
    
    msg.no_segments = 0;
    
    // While 30% of the original cloud is still there
    while (cloud_filtered_blob->points.size () > 0.3 * nr_points){
        // Segment the largest planar component from the remaining cloud
        seg.setInputCloud (cloud_filtered_blob);
        seg.segment (*inliers, *coefficients);
        if (inliers->indices.size () == 0){
            std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
            break;
        }
        
        std::cerr << "Model coefficients: " << coefficients->values[0] << " " 
                                      << coefficients->values[1] << " "
                                      << coefficients->values[2] << " " 
                                      << coefficients->values[3] << std::endl;
        
        msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
        
        erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];//(erlcm_seg_point_list_t *) calloc(1, sizeof(erlcm_seg_point_list_t));
        seg_msg->segment_id = msg.no_segments; 
        seg_msg->no_points = inliers->indices.size();

        seg_msg->coefficients[0] = coefficients->values[0]; 
        seg_msg->coefficients[1] = coefficients->values[1]; 
        seg_msg->coefficients[2] = coefficients->values[2]; 
        seg_msg->coefficients[3] = coefficients->values[3]; 
        
        // Extract the inliers
        extract.setInputCloud(cloud_filtered_blob);
        extract.setIndices (inliers);
        extract.setNegative (false);
        extract.filter (*cloud_p);

        std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." << std::endl;
        
        seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));

        double mean[3] = {0,0,0};
        
        for (size_t k = 0; k < cloud_p->points.size (); ++k){
            seg_msg->points[k].xyz[0] = cloud_p->points[k].x; 
            seg_msg->points[k].xyz[1] = cloud_p->points[k].y; 
            seg_msg->points[k].xyz[2] = cloud_p->points[k].z; 
            mean[0] += cloud_p->points[k].x; 
            mean[1] += cloud_p->points[k].y; 
            mean[2] += cloud_p->points[k].z; 
        }
        
        seg_msg->centroid[0] = mean[0] / cloud_p->points.size (); 
        seg_msg->centroid[1] = mean[1] / cloud_p->points.size (); 
        seg_msg->centroid[2] = mean[2] / cloud_p->points.size (); 

        fprintf(stderr, "\tMean : %f, %f, %f\n", seg_msg->centroid[0], 
                seg_msg->centroid[1], seg_msg->centroid[2]);
        
        msg.no_segments++; 
        
            // Create the filtering object
        extract.setNegative (true);
        extract.filter (*cloud_filtered_blob);
        
        i++;
    }
    
    //publish
    //erlcm_segment_list_t_publish(lcm, "PCL_SEGMENT_LIST", &msg);
    publish_segment_to_collections(&msg, lcm, "SEGMENT_POINTS");
    //erlcm_segment_list_t_publish(lcm, "PCL_SEGMENT_LIST", &msg);
    
    for(int k = 0; k < msg.no_segments; k++){
        free(msg.segments[k].points);
    }
    free(msg.segments);        
}

int fit_cylinder(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, double param[7]){
    fprintf(stderr, "Fitting Cylinder\n");
    const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    pcl::NormalEstimation<PointT, pcl::Normal> ne;
    pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());

    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
    pcl::PointCloud<PointT>::Ptr cloud_filtered2 (new pcl::PointCloud<PointT>);

    pcl::PointIndices::Ptr inliers_cylinder (new pcl::PointIndices);

    // Create the segmentation object
    //pcl::SACSegmentation<pcl::PointXYZ> seg;
    pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg; 
    pcl::ExtractIndices<pcl::PointXYZ> extract;

    // Estimate point normals
    ne.setSearchMethod (tree);
    ne.setInputCloud (cloud);
    ne.setKSearch (50);
    ne.compute (*cloud_normals);

    // Create the segmentation object for cylinder segmentation and set all the parameters
    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_CYLINDER);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setNormalDistanceWeight (0.1);
    seg.setMaxIterations (10000);
    seg.setDistanceThreshold (0.05);//was 0.05
    

    //Sachi - figure out the actual radius
    //this sets the radius limit -  
    seg.setRadiusLimits (0, 0.2);
    seg.setInputCloud (cloud);
    seg.setInputNormals (cloud_normals);


    //first three co-efficients are the line point
    //second three are the line direction - this should match with the ground plane normal - more or less 
    //last is the radius 
    
    // Obtain the cylinder inliers and coefficients
    seg.segment (*inliers_cylinder, *coefficients);

    

    // Extract the inliers
    extract.setInputCloud(cloud);
    extract.setIndices (inliers_cylinder);
    extract.setNegative (false);
    extract.filter (*cloud_p);
    std::cout << "Inlier Count : " << cloud_p->points.size()<< " Total : " <<
        cloud->points.size() << std::endl;

    std::cout<< "Inlier Percentage : " << (cloud_p->points.size() / (double) cloud->points.size()) << std::endl;

    param[7]  = cloud_p->points.size() / ((double) cloud->points.size());
    
    if(param[7] > 0.1){
        std::cerr << "Cylinder coefficients: " << *coefficients << std::endl;
        
        fprintf(stderr," Radius : %f , Line Point : %f,%f,%f Line Direction : %f,%f,%f\n", coefficients->values[6], coefficients->values[0], coefficients->values[1], coefficients->values[2], 
                coefficients->values[3], coefficients->values[4], coefficients->values[5]);

        param[0] = coefficients->values[0]; 
        param[1] = coefficients->values[1]; 
        param[2] = coefficients->values[2]; 
        param[3] = coefficients->values[3]; 
        param[4] = coefficients->values[4]; 
        param[5] = coefficients->values[5]; 
        param[6] = coefficients->values[6]; 

        //clear memory
        fprintf(stderr, "Done fitting Cylinder\n");
        return 1;


    }
    else{
        fprintf(stderr, "Error - Not enough Points for inliers in cylinder\n");
        return 0; 
    }
}

int fit_circle(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, double params[4]){//, double param[7]){

    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_CIRCLE2D);
    seg.setMethodType (pcl::SAC_RANSAC);

    //play with this to get different size segments 
    seg.setDistanceThreshold (0.02); 
    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    seg.setInputCloud (cloud);
    seg.segment (*inliers, *coefficients);

    if (inliers->indices.size () == 0){
        std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
    }
    else{
        fprintf(stderr, "Circle fit %d / %d\n", inliers->indices.size (), cloud->points.size());
        std::cout << coefficients->values[0] << ", " << coefficients->values[1] << " - R : " << coefficients->values[2] << std::endl;
        params[0] = coefficients->values[0];
        params[1] = coefficients->values[1];
        params[2] = coefficients->values[2];
        params[3] = inliers->indices.size() / ((double) cloud->points.size());
        //seems to be decent 
        return 1;
    }

    return 0;

}

void find_tabletop_objects_after_segmentation(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, 
                                              double forward_heading, double bot_local_pos[3],
                                              lcm_t *lcm, object_list_t *detections){
    fprintf(stderr, "Looking for tabletop points\n");
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
    tree->setInputCloud (cloud_p->makeShared());
    std::vector<pcl::PointIndices> cluster_indices;
    pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
    ec.setClusterTolerance (0.1); // 2cm
    ec.setMinClusterSize (100);
    ec.setMaxClusterSize (25000);
    ec.setSearchMethod (tree);
    ec.setInputCloud (cloud_p->makeShared());
    ec.extract (cluster_indices);

    int j = 0;

    //lists to publish out 
    erlcm_segment_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.segments = NULL;
    msg.no_segments = 0;

    erlcm_cylinder_list_t cyl_objects;    
    cyl_objects.utime = msg.utime;
    cyl_objects.count = 0;    
    cyl_objects.list = NULL;
            
    double mean[3] ={0};
    double coeffs[4] = {0};

    pcl::PointCloud<pcl::PointXYZ>::Ptr largest_cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);
    
    //one option is to try the object detection on all tables (that suceed) 
    //the other is to get the points that don't belong to the plane out and to do that later 
    //right now we will go with the first option 

    std::vector<table_object_t *> table_object_list;

    for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it){
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);
        for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); pit++){
            cloud_cluster->points.push_back (cloud_p->points[*pit]); //
        }

        if(cloud_cluster->points.size() > 100){
            table_object_t *table = (table_object_t *) calloc(1, sizeof(table_object_t));
            table->objects = NULL;
            int is_table = find_table_and_objects(cloud_cluster, table);
            if(is_table){
                fprintf(stderr, "\n\n Table found - No objects : %d\n", table->no_objects);
                table_object_list.push_back(table);                
            }
            else{
                free(table);
            }
        }
    }

    if(table_object_list.size() == 1){        
        fprintf(stderr, "Only found one table - with %d objects in it\n", table_object_list[0]->no_objects);
        add_table_to_lcm_msg(&msg, table_object_list[0]);
        fprintf(stderr, "Setting goal to this object\n");

        //add this object to the object list 
        //object_list_t
        for(int i=0; i < table_object_list[0]->no_objects; i++){
            add_object_to_list(detections, &table_object_list[0]->objects[i].object);
            add_object_to_lcm_cylinder_list(&cyl_objects, table_object_list[0]);
        }
    }
    else{
        //we need to search through the tables and find the best one 
        double heading_from_forward = 100000;
        int best_table_ind = -1;

        //if(find_best_fit){
        fprintf(stderr, "Looking for the best fit table - table infront of the robot\n");
        
        for(size_t i=0; i < table_object_list.size(); i++){
            table_object_t *table = table_object_list[i];
            if(table->no_objects == 0){
                continue;
            }
                
            double heading_local = atan2(table->table_plane.mean[1] - bot_local_pos[1], table->table_plane.mean[0] - bot_local_pos[0]) - forward_heading;
            if(fabs(heading_local) < heading_from_forward){
                heading_from_forward = fabs(heading_local);
                best_table_ind = i;
            }
        }
        if(best_table_ind> 0){
            fprintf(stderr, "Found the best table with objects : %d\n", 
                    table_object_list[best_table_ind]->no_objects);
            add_table_to_lcm_msg(&msg, table_object_list[best_table_ind]);
            add_object_to_lcm_cylinder_list(&cyl_objects, table_object_list[best_table_ind]);

            for(int i=0; i < table_object_list[best_table_ind]->no_objects; i++){
                add_object_to_list(detections, &table_object_list[best_table_ind]->objects[i].object);
            }
        }
        else{
            fprintf(stderr, "Did not find a table with a bottle object\n");
        }
    }

    //erlcm_segment_list_t_publish(lcm, "PCL_SEGMENT_LIST", &msg);
    publish_segment_to_collections(&msg, lcm, "SEGMENT_POINTS");

    for(int k = 0; k < msg.no_segments; k++){
        free(msg.segments[k].points);
    }
    free(msg.segments);

    if(cyl_objects.count){
        cyl_objects.utime = bot_timestamp_now();
        erlcm_cylinder_list_t_publish(lcm, "CYLINDER_LIST", &cyl_objects);
        free(cyl_objects.list);    
    }

    //free the stuff 
    for(size_t i=0; i < table_object_list.size(); i++){
        table_object_t *table = table_object_list[i];
        free_table_object(table);
    }
}

void add_cloud_to_table_object(table_object_t *table, 
                               pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p,
                               double max_extent[3], 
                               double min_extent[3], double _mean[3], 
                               double center[3], double radius, 
                               int is_cylinder){
    
    int index = table->no_objects;
    table->no_objects++;
    table->objects = (pcl_object_t *) realloc(table->objects, table->no_objects * sizeof(pcl_object_t));
    
    pcl_object_t *object = &table->objects[index];
    get_points_from_pcl_pointcloud(cloud_p, &object->cloud);
    memcpy(object->mean, _mean, 3 * sizeof(double));
    memcpy(object->max_extent, max_extent, 3 * sizeof(double));
    memcpy(object->min_extent, min_extent, 3 * sizeof(double));
    memcpy(object->object.center, center, 3 * sizeof(double));
    object->object.radius = radius;
    object->object.is_cylinder = is_cylinder;    
}

void add_point_cloud_to_lcm_msg(erlcm_segment_list_t *msg, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, double coeffs[4], double mean[3]){
    msg->segments = (erlcm_seg_point_list_t *)realloc(msg->segments, sizeof(erlcm_seg_point_list_t) * (msg->no_segments + 1));
                    
    erlcm_seg_point_list_t *seg_msg = &msg->segments[msg->no_segments];
    seg_msg->segment_id = msg->no_segments; 
    seg_msg->no_points = cloud_p->points.size ();
                    
    seg_msg->coefficients[0] = coeffs[0];
    seg_msg->coefficients[1] = coeffs[1];
    seg_msg->coefficients[2] = coeffs[2];
    seg_msg->coefficients[3] = coeffs[3];
                    
    seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));
                    
    for (size_t k = 0; k < cloud_p->points.size (); ++k){
        seg_msg->points[k].xyz[0] = cloud_p->points[k].x; 
        seg_msg->points[k].xyz[1] = cloud_p->points[k].y; 
        seg_msg->points[k].xyz[2] = cloud_p->points[k].z; 
    }
                    
    seg_msg->centroid[0] = mean[0];
    seg_msg->centroid[1] = mean[1];
    seg_msg->centroid[2] = mean[2];
    msg->no_segments++;
}

void add_table_to_lcm_msg(erlcm_segment_list_t *msg, table_object_t *table){
    int no_elements = 1 + table->no_objects;

    msg->segments = (erlcm_seg_point_list_t *)realloc(msg->segments, sizeof(erlcm_seg_point_list_t) * (msg->no_segments + no_elements));
                    
    //add the table 
    erlcm_seg_point_list_t *seg_msg = &msg->segments[msg->no_segments];
    seg_msg->segment_id = msg->no_segments; 
    seg_msg->no_points = table->table_plane.cloud.count;
                    
    seg_msg->coefficients[0] = table->table_plane.coeffs[0];
    seg_msg->coefficients[1] = table->table_plane.coeffs[1];
    seg_msg->coefficients[2] = table->table_plane.coeffs[2];
    seg_msg->coefficients[3] = table->table_plane.coeffs[3];

    seg_msg->centroid[0] = table->table_plane.mean[0];
    seg_msg->centroid[1] = table->table_plane.mean[1];
    seg_msg->centroid[2] = table->table_plane.mean[2];
                    
    seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));
                    
    memcpy(seg_msg->points, table->table_plane.cloud.points, seg_msg->no_points* sizeof(erlcm_xyz_point_t));
    msg->no_segments++;

    //now add the objects 
    for(int i=0; i < table->no_objects; i++){
        erlcm_seg_point_list_t *seg_msg = &msg->segments[msg->no_segments];
        seg_msg->segment_id = msg->no_segments; 
        seg_msg->no_points = table->objects[i].cloud.count;
                    
        seg_msg->coefficients[0] = table->table_plane.coeffs[0];
        seg_msg->coefficients[1] = table->table_plane.coeffs[1];
        seg_msg->coefficients[2] = table->table_plane.coeffs[2];
        seg_msg->coefficients[3] = table->table_plane.coeffs[3];

        seg_msg->centroid[0] = table->objects[i].mean[0];
        seg_msg->centroid[1] = table->objects[i].mean[1];
        seg_msg->centroid[2] = table->objects[i].mean[2];
                    
        seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));
                    
        memcpy(seg_msg->points, table->objects[i].cloud.points, seg_msg->no_points* sizeof(erlcm_xyz_point_t));
        msg->no_segments++;        
    }
}


int filter_points(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob){
    //pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>); 
    pcl::VoxelGrid<pcl::PointXYZ> sor;
    //pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob (new pcl::PointCloud<pcl::PointXYZ>); 

    sor.setInputCloud (cloud->makeShared ());
    sor.setLeafSize (0.01f, 0.01f, 0.01f); 
    sor.filter (*cloud_filtered_blob);
}

//int find_table_and_objects(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, erlcm_segment_list_t *msg,  table_object_t *table){ 
int find_table_and_objects(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, table_object_t *table){ 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>); 
    const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob (new pcl::PointCloud<pcl::PointXYZ>); 

    cloud_filtered_blob = cloud;
    
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);

    //play with this to get different size segments 
    seg.setDistanceThreshold (0.02); //was 0.01
    
    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    
    int nr_points = (int) cloud_filtered_blob->points.size ();

    // While 30% of the original cloud is still there
    //cloud_filtered_blob->points.size () > 0.3 * nr_points
    seg.setInputCloud (cloud_filtered_blob);
    seg.segment (*inliers, *coefficients);
    if (inliers->indices.size () == 0){
        std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
    }
    
    int table_top = 0;
    if(fabs(coefficients->values[0]) < 0.1 && fabs(coefficients->values[1]) < 0.1 && 
       fabs(coefficients->values[2]) > 0.9 && (-coefficients->values[3]/ coefficients->values[2] > 0.2 && coefficients->values[3] /coefficients->values[2] < 1.0)){// &&
        table_top = 1;
        fprintf(stderr, "Plane parallel to ground plane - detected => Possible table\n");
        
    }

    if(table_top){
        // Extract the inliers
        extract.setInputCloud(cloud_filtered_blob);
        extract.setIndices (inliers);
        extract.setNegative (false);
        extract.filter (*cloud_p);

        if(cloud_p->points.size() > (0.4 * nr_points)){
            table_top = 1;
            fprintf(stderr, "Confirmed\n");
        }
        else{
            fprintf(stderr, "Not enough points - rejecting\n");
            table_top = 0;
            return 0;
        }

        double mean[3];
        for(size_t k = 0; k < cloud_p->points.size (); ++k){
            mean[0] += cloud_p->points[k].x;
            mean[1] += cloud_p->points[k].y;
            mean[2] += cloud_p->points[k].z;
        }

        mean[0] /= cloud_p->points.size();
        mean[1] /= cloud_p->points.size();
        mean[2] /= cloud_p->points.size();

        //this is a table - lets add to the message 
        //table->table_plane.cloud = cloud_p;
        get_points_from_pcl_pointcloud(cloud_p, &table->table_plane.cloud);
        table->table_plane.coeffs[0] = coefficients->values[0];
        table->table_plane.coeffs[1] = coefficients->values[1];
        table->table_plane.coeffs[2] = coefficients->values[2];
        table->table_plane.coeffs[3] = coefficients->values[3];
        table->table_plane.mean[0] = mean[0]; 
        table->table_plane.mean[1] = mean[1]; 
        table->table_plane.mean[2] = mean[2]; 
        
        //we don't need to do clustering again on the points in the plane 
        double coeffs[4] = {coefficients->values[0], coefficients->values[1], coefficients->values[2], coefficients->values[3]};
        double table_height = - coeffs[3] / coeffs[2];

        int no_points = cloud_p->points.size ();
        pointlist2d_t *ptr = pointlist2d_new (no_points);
        
        //we can even skip this now - since the stuff that is on the table should be connected to the table - thus this would come as 1 anyway - or should 
        for(size_t k = 0; k < cloud_p->points.size (); ++k){
            ptr->points[k].x = cloud_p->points[k].x;
            ptr->points[k].y = cloud_p->points[k].y;
        }

        pointlist2d_t *table_bb = convexhull_graham_scan_2d(ptr);

        pcl::PointCloud<pcl::PointXYZ>::Ptr object_cloud (new pcl::PointCloud<pcl::PointXYZ>);
        
        //lets cluster the stuff on the table 
        extract.setNegative (true);
        extract.filter (*object_cloud);

        if(object_cloud->points.size()== 0){
            fprintf(stderr, "No points on top of the table - returning failed\n");
            return 0;
        }

        fprintf(stderr, "Doing segmentation on remaining points\n");
        //cluster this guy 
        pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);

        fprintf(stderr, "Cloud Size : %d\n", object_cloud->points.size());
        tree->setInputCloud (object_cloud);

        std::vector<pcl::PointIndices> cluster_indices;
        pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
        ec.setClusterTolerance (0.1); // 2cm
        ec.setMinClusterSize (50);
        ec.setMaxClusterSize (25000);
        ec.setSearchMethod (tree);
        ec.setInputCloud (object_cloud);
        ec.extract (cluster_indices);

        int j = 0;
        for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it){

            std::cout<< "Remaining cloud size " <<  object_cloud->points.size() << std::endl;
            pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);

            pcl_object_t obj;

            obj.mean[0] = 0;
            obj.mean[1] = 0;
            obj.mean[2] = 0;

            obj.max_extent[0] = -100000;
            obj.max_extent[1] = -100000;
            obj.max_extent[2] = -100000;

            obj.min_extent[0] = 100000;
            obj.min_extent[1] = 100000;
            obj.min_extent[2] = 100000;
        
            fprintf(stderr, "Cloud cluster size : %d\n", cloud_cluster->points.size()); 
            for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); pit++){
                //sometimes get segfaults here 
                cloud_cluster->points.push_back (object_cloud->points[*pit]);           
            }
            fprintf(stderr, "Cloud cluster new size : %d\n", cloud_cluster->points.size()); 
            
            //lets check if the object is on top or bottom 

            //lets fit a cylinder to this 
            point2d_t pt;
            int is_inside = 0;
            for (size_t k = 0; k < cloud_cluster->points.size (); ++k){
                obj.mean[0] += cloud_cluster->points[k].x;
                obj.mean[1] += cloud_cluster->points[k].y;
                obj.mean[2] += cloud_cluster->points[k].z;

                pt.x = cloud_cluster->points[k].x;
                pt.y = cloud_cluster->points[k].y;

                if(geom_point_inside_or_on_edge_of_convex_polygon_2d (&pt, table_bb)==1)
                    is_inside = 1;
            
                if(obj.max_extent[0] < cloud_cluster->points[k].x)
                    obj.max_extent[0] = cloud_cluster->points[k].x;

                if(obj.max_extent[1] < cloud_cluster->points[k].y)
                    obj.max_extent[1] = cloud_cluster->points[k].y;

                if(obj.max_extent[2] < cloud_cluster->points[k].z)
                    obj.max_extent[2] = cloud_cluster->points[k].z;

                if(obj.min_extent[0] > cloud_cluster->points[k].x)
                    obj.min_extent[0] = cloud_cluster->points[k].x;

                if(obj.min_extent[1] > cloud_cluster->points[k].y)
                    obj.min_extent[1] = cloud_cluster->points[k].y;

                if(obj.min_extent[2] > cloud_cluster->points[k].z)
                    obj.min_extent[2] = cloud_cluster->points[k].z;
            }

            obj.mean[0] /= cloud_cluster->points.size();
            obj.mean[1] /= cloud_cluster->points.size();
            obj.mean[2] /= cloud_cluster->points.size();
            
            int above = 1;

            if((table_height - obj.mean[2]) > 0){
                above = 0;
            }

            //obj.cloud = cloud_cluster;
            get_points_from_pcl_pointcloud(cloud_cluster, &obj.cloud);
            cloud_cluster->width = cloud_cluster->points.size ();
            cloud_cluster->height = 1;
            cloud_cluster->is_dense = false;

            double params[8] = {0};
            int is_cylinder = fit_cylinder(cloud_cluster, params); 

            //how about we also project these points to the table and try fitting circles 
            
            // Create the filtering object
            pcl::ProjectInliers<pcl::PointXYZ> proj;
            pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_projected (new pcl::PointCloud<pcl::PointXYZ>);

            proj.setModelType (pcl::SACMODEL_PLANE);
            proj.setInputCloud (cloud_cluster);
            proj.setModelCoefficients (coefficients);
            proj.filter (*cloud_projected);
            
            int wrong_position = 0;

            if((obj.min_extent[2] - table_height) > 0.5){
                fprintf(stderr, "A floating object - we should ignore\n");
                wrong_position = 1;
            }
                        
            if(wrong_position == 0){
                double circ_params[4] = {0};
                fit_circle(cloud_projected, circ_params);
                
                if(circ_params[3] > 0.5){
                    fprintf(stderr, "Circle Fit Sucess (%f,%f) - Radius : %f - Inliers %f\n", circ_params[0], 
                            circ_params[1], circ_params[2] , circ_params[3]);
                }
                else{
                    fprintf(stderr, "Circle fit failed\n");
                }
                
                //the points are properly projected to the table 
                double detection_threshold = 0.4;
                
                //we still need to see if it fits to a cylinder - because there are other objects that fit circles 
                //center looks good and the radius looks accurate - which is all we care about 
                
                if(is_inside && above && params[7] > detection_threshold){                

                    double t, x_t, y_t, z_t;

                    t = - (coeffs[0] * params[0] + coeffs[1] * params[1] + coeffs[2] * params[2] + coeffs[3]) / (coeffs[0] * params[3] + coeffs[1] * params[4] + coeffs[2] * params[5]);

                    x_t = circ_params[0];//params[0] + t * params[3];
                    y_t = circ_params[1];//params[1] + t * params[4];
                    z_t = table_height;//params[2] + t * params[5];


                    double xyz[3] = {x_t, y_t, z_t};
                    //point 1m higher - for visualization 
                
                    double t1 = - (coeffs[0] * params[0] + coeffs[1] * params[1] + coeffs[2] * params[2] + coeffs[3] - 1.0) / (coeffs[0] * params[3] + coeffs[1] * params[4] + coeffs[2] * params[5]);
                
                    double dir[3] = {0};

                    dir[0] = coeffs[0];//(t1-t) * params[3];
                    dir[1] = coeffs[1];//(t1-t) * params[4];
                    dir[2] = coeffs[2];//(t1-t) * params[5];

                    double center[3] = {circ_params[0], circ_params[1], (obj.max_extent[2] + obj.min_extent[2])/2 };

                    add_cloud_to_table_object(table, cloud_cluster, 
                                              obj.max_extent, 
                                              obj.min_extent, obj.mean, 
                                              center,circ_params[3], 1);
                }
            }
        }        
        return 1;
    }

    return 0;
}

void add_to_cylinder_object(erlcm_cylinder_list_t *cyl_objects, double xyz[3], double dir[3], double radius){
    cyl_objects->list = (erlcm_cylinder_model_t *) realloc(cyl_objects->list, sizeof(erlcm_cylinder_model_t) * (cyl_objects->count +1)); 
    cyl_objects->list[cyl_objects->count].line_point[0] = xyz[0];
    cyl_objects->list[cyl_objects->count].line_point[1] = xyz[1];
    cyl_objects->list[cyl_objects->count].line_point[2] = xyz[2];

    cyl_objects->list[cyl_objects->count].line_direction[0] = dir[0];
    cyl_objects->list[cyl_objects->count].line_direction[1] = dir[1];
    cyl_objects->list[cyl_objects->count].line_direction[2] = dir[2];
            
    cyl_objects->list[cyl_objects->count].radius = radius;
    cyl_objects->count++;    
}


/*int find_tabletop_objects(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, int filter, int cluster_table, 
                          int fit_cylinders_to_objects, int find_best_fit, int draw_table, int draw_objects,
                          double forward_heading, double bot_local_pos[3],
                          object_list_t *detected_objects, lcm_t *lcm){
    
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>); 
    const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::VoxelGrid<pcl::PointXYZ> sor;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob (new pcl::PointCloud<pcl::PointXYZ>); 

    if(filter){        
        sor.setInputCloud (cloud->makeShared ());
        sor.setLeafSize (0.005f, 0.005f, 0.005f); 
        sor.filter (*cloud_filtered_blob);
    }
    else{
        cloud_filtered_blob = cloud;
    }

    int debug = 0;

    int v = 0;
    
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);

    //play with this to get different size segments 
    seg.setDistanceThreshold (0.02); //was 0.01
    
    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    
    int nr_points = (int) cloud_filtered_blob->points.size ();

    erlcm_segment_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.segments = NULL;

    msg.no_segments = 0;

    std::vector<pcl_plane_t> table_tops;
    
    // While 30% of the original cloud is still there
    while (cloud_filtered_blob->points.size () > 0.3 * nr_points){
        // Segment the largest planar component from the remaining cloud
        //std::cout << "Cloud size : " << cloud_filtered_blob->points.size () << std::endl; 
        seg.setInputCloud (cloud_filtered_blob);
        seg.segment (*inliers, *coefficients);
        if (inliers->indices.size () == 0){
            if(v)
                std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
            break;
        }
        
        int table_top = 0;
        if( fabs(coefficients->values[0]) < 0.1 && fabs(coefficients->values[1]) < 0.1 && 
            fabs(coefficients->values[2]) > 0.9 && (-coefficients->values[3]/ coefficients->values[2] > 0.2 && coefficients->values[3] /coefficients->values[2] < 1.0)){
            table_top = 1;
            if(v)
                fprintf(stderr, "Plane parallel to ground plane - detected => Possible table\n");
        }
        
        // Extract the inliers
        extract.setInputCloud(cloud_filtered_blob);
        extract.setIndices (inliers);
        extract.setNegative (false);
        extract.filter (*cloud_p);

        if(table_top && cluster_table){
            //remove outliers that seem to get attached to the plane 
            pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
            tree->setInputCloud (cloud_p);

            if(debug){
                msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
                    
                erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];
                seg_msg->segment_id = msg.no_segments; 
                seg_msg->no_points = cloud_p->points.size ();
                    
                seg_msg->coefficients[0] = coefficients->values[0];
                seg_msg->coefficients[1] = coefficients->values[1];
                seg_msg->coefficients[2] = coefficients->values[2];
                seg_msg->coefficients[3] = coefficients->values[3];
                    
                seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));
                    
                for (size_t k = 0; k < cloud_p->points.size (); ++k){
                    seg_msg->points[k].xyz[0] = cloud_p->points[k].x; 
                    seg_msg->points[k].xyz[1] = cloud_p->points[k].y; 
                    seg_msg->points[k].xyz[2] = cloud_p->points[k].z; 
                }
                    
                seg_msg->centroid[0] = 0;
                seg_msg->centroid[1] = 0;
                seg_msg->centroid[2] = 0;
                msg.no_segments++;
            }
            

            std::vector<pcl::PointIndices> cluster_indices;
            pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
            ec.setClusterTolerance (0.1); // 2cm
            ec.setMinClusterSize (50);
            ec.setMaxClusterSize (25000);
            ec.setSearchMethod (tree);
            ec.setInputCloud (cloud_p);
            ec.extract (cluster_indices);

            int j = 0;
            
            //we just want the biggest chunk for the table

            pcl::PointCloud<pcl::PointXYZ>::Ptr largest_cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);

            for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it){
                pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);
                for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); pit++){
                    cloud_cluster->points.push_back (cloud_p->points[*pit]); //
                }

                if(cloud_cluster->points.size() > largest_cloud_cluster->points.size()){
                    largest_cloud_cluster->resize(0);
                    largest_cloud_cluster = cloud_cluster;
                }
                else{
                    cloud_cluster->resize(0);
                }
            }

            if(largest_cloud_cluster->points.size()>100){ 
                double mean[3] = {0,0,0};
                
                for (size_t k = 0; k < largest_cloud_cluster->points.size (); ++k){
                    mean[0] += largest_cloud_cluster->points[k].x; 
                    mean[1] += largest_cloud_cluster->points[k].y; 
                    mean[2] += largest_cloud_cluster->points[k].z; 
                }

                mean[0] /= largest_cloud_cluster->points.size (); 
                mean[1] /= largest_cloud_cluster->points.size (); 
                mean[2] /= largest_cloud_cluster->points.size (); 

                if(draw_table && find_best_fit==0){
                    fprintf(stderr, "Not best fit - Drawing table here\n");
                    msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
                    
                    erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];
                    seg_msg->segment_id = msg.no_segments; 
                    seg_msg->no_points = largest_cloud_cluster->points.size ();
                    
                    seg_msg->coefficients[0] = coefficients->values[0];
                    seg_msg->coefficients[1] = coefficients->values[1];
                    seg_msg->coefficients[2] = coefficients->values[2];
                    seg_msg->coefficients[3] = coefficients->values[3];
                    
                    seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));
                    
                    for (size_t k = 0; k < largest_cloud_cluster->points.size (); ++k){
                        seg_msg->points[k].xyz[0] = largest_cloud_cluster->points[k].x; 
                        seg_msg->points[k].xyz[1] = largest_cloud_cluster->points[k].y; 
                        seg_msg->points[k].xyz[2] = largest_cloud_cluster->points[k].z; 
                    }
                    
                    seg_msg->centroid[0] = mean[0];
                    seg_msg->centroid[1] = mean[1];
                    seg_msg->centroid[2] = mean[2];
                    msg.no_segments++;
                }

                pcl_plane_t table_det;
                //table_det.cloud =  largest_cloud_cluster;
                get_points_from_pcl_pointcloud(largest_cloud_cluster, &table_det.cloud);
                table_det.coeffs[0] = coefficients->values[0];
                table_det.coeffs[1] = coefficients->values[1];
                table_det.coeffs[2] = coefficients->values[2];
                table_det.coeffs[3] = coefficients->values[3];
                table_det.mean[0] = mean[0];
                table_det.mean[1] = mean[1];
                table_det.mean[2] = mean[2];

                table_tops.push_back(table_det);
                
                std::cout << "Largest Table Top cluster : " << largest_cloud_cluster->points.size () << " data points." << std::endl;

            }
        }
        else if(table_top){
            if(cloud_p->points.size()>200){ 
                double mean[3] = {0,0,0};
        
                for (size_t k = 0; k < cloud_p->points.size (); ++k){
                    mean[0] += cloud_p->points[k].x; 
                    mean[1] += cloud_p->points[k].y; 
                    mean[2] += cloud_p->points[k].z; 
                }
        
                mean[0] /= cloud_p->points.size (); 
                mean[1] /= cloud_p->points.size (); 
                mean[2] /= cloud_p->points.size (); 
                
                if(draw_table && find_best_fit==0){
                    fprintf(stderr, "Not best fit - Drawing table here\n");
                    msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
                    
                    erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];
                    seg_msg->segment_id = msg.no_segments; 
                    seg_msg->no_points = cloud_p->points.size ();
                    
                    seg_msg->coefficients[0] = coefficients->values[0];
                    seg_msg->coefficients[1] = coefficients->values[1];
                    seg_msg->coefficients[2] = coefficients->values[2];
                    seg_msg->coefficients[3] = coefficients->values[3];
                    
                    seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));
        
                    for (size_t k = 0; k < cloud_p->points.size (); ++k){
                        seg_msg->points[k].xyz[0] = cloud_p->points[k].x; 
                        seg_msg->points[k].xyz[1] = cloud_p->points[k].y; 
                        seg_msg->points[k].xyz[2] = cloud_p->points[k].z; 
                    }
                    
                    seg_msg->centroid[0] = mean[0];
                    seg_msg->centroid[1] = mean[1];
                    seg_msg->centroid[2] = mean[2];
                    msg.no_segments++;
                }

                pcl_plane_t table_det;
                //table_det.cloud =  cloud_p; 
                get_points_from_pcl_pointcloud(cloud_p, &table_det.cloud);
                table_det.coeffs[0] = coefficients->values[0];
                table_det.coeffs[1] = coefficients->values[1];
                table_det.coeffs[2] = coefficients->values[2];
                table_det.coeffs[3] = coefficients->values[3];
                table_det.mean[0] = mean[0];
                table_det.mean[1] = mean[1];
                table_det.mean[2] = mean[2];

                table_tops.push_back(table_det);
        
                std::cout << "Largest Table Top cluster : " << cloud_p->points.size () << " data points." << std::endl;

            }
            
        }
        
        // Create the filtering object
        extract.setNegative (true);
        extract.filter (*cloud_filtered_blob);
    }
    
    fprintf(stderr, "No of tables found : %d\n", (int)table_tops.size());

    if(table_tops.size() == 0){
        fprintf(stderr, "No tabletops found - returning\n");
        //make sure to free stuff 
        return -1;
    }

    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
    tree->setInputCloud (cloud_filtered_blob);

    //publish_pcl_points_to_lcm(*cloud_filtered_blob, lcm);

    std::vector<pcl::PointIndices> cluster_indices;
    pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
    ec.setClusterTolerance (0.1); // 2cm
    ec.setMinClusterSize (50);
    ec.setMaxClusterSize (25000);
    ec.setSearchMethod (tree);
    ec.setInputCloud (cloud_filtered_blob);
    ec.extract (cluster_indices);
    
    fprintf(stderr, "Doing segmentation on remaining points\n");

    fprintf(stderr, "No of clusters : %d\n",  (int) cluster_indices.size());

    std::vector<pcl_object_t> objects;//pcl::PointCloud<pcl::PointXYZ>::Ptr> objects;

    int j = 0;
    for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it){
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);

        pcl_object_t obj;

        obj.mean[0] = 0;
        obj.mean[1] = 0;
        obj.mean[2] = 0;

        obj.max_extent[0] = -100000;
        obj.max_extent[1] = -100000;
        obj.max_extent[2] = -100000;

        obj.min_extent[0] = 100000;
        obj.min_extent[1] = 100000;
        obj.min_extent[2] = 100000;
        

        for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); pit++){
            cloud_cluster->points.push_back (cloud_filtered_blob->points[*pit]); //           
        }

        for (size_t k = 0; k < cloud_cluster->points.size (); ++k){
            obj.mean[0] += cloud_cluster->points[k].x;
            obj.mean[1] += cloud_cluster->points[k].y;
            obj.mean[2] += cloud_cluster->points[k].z;
            
            if(obj.max_extent[0] < cloud_cluster->points[k].x)
                obj.max_extent[0] = cloud_cluster->points[k].x;

            if(obj.max_extent[1] < cloud_cluster->points[k].y)
                obj.max_extent[1] = cloud_cluster->points[k].y;

            if(obj.max_extent[2] < cloud_cluster->points[k].z)
                obj.max_extent[2] = cloud_cluster->points[k].z;

            if(obj.min_extent[0] > cloud_cluster->points[k].x)
                obj.min_extent[0] = cloud_cluster->points[k].x;

            if(obj.min_extent[1] > cloud_cluster->points[k].y)
                obj.min_extent[1] = cloud_cluster->points[k].y;

            if(obj.min_extent[2] > cloud_cluster->points[k].z)
                obj.min_extent[2] = cloud_cluster->points[k].z;
        }

        obj.mean[0] /= cloud_cluster->points.size();
        obj.mean[1] /= cloud_cluster->points.size();
        obj.mean[2] /= cloud_cluster->points.size();

        //obj.cloud = cloud_cluster;
        get_points_from_pcl_pointcloud(cloud_cluster, &obj.cloud);
        objects.push_back(obj);
        
        cloud_cluster->width = cloud_cluster->points.size ();
        cloud_cluster->height = 1;
        cloud_cluster->is_dense = true;
        //std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size () << " data points." << std::endl;        
    }

    erlcm_cylinder_list_t cyl_objects; 
    
    cyl_objects.count = 0;    
    cyl_objects.list = NULL;

    double heading_from_forward = 100000;
    int best_table_ind = -1;

    if(find_best_fit){
        fprintf(stderr, "Looking for the best fit table - table infront of the robot\n");
        
        for(size_t i=0; i < table_tops.size(); i++){
            double heading_local = atan2(table_tops[i].mean[1] - bot_local_pos[1], table_tops[i].mean[0] - bot_local_pos[0]) - forward_heading;
            if(fabs(heading_local) < heading_from_forward){
                heading_from_forward = fabs(heading_local);
                best_table_ind = i;
            }
        }
    }

    if(find_best_fit){

        if(best_table_ind == -1){
            fprintf(stderr, "Did not find a table in front - returning\n");
            return -1;
        }

        fprintf(stderr, "Found best table at : %d\n", best_table_ind);
        
        int i = best_table_ind;
        pcl::PointCloud<pcl::PointXYZ>::Ptr table_points = table_tops[i].cloud;

        if(draw_table){
            msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
                    
            erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];
            seg_msg->segment_id = msg.no_segments; 
            seg_msg->no_points = table_points->points.size ();//largest_cloud_cluster->points.size ();
                    
            seg_msg->coefficients[0] = table_tops[i].coeffs[0];//coefficients->values[0];
            seg_msg->coefficients[1] = table_tops[i].coeffs[1];//coefficients->values[1];
            seg_msg->coefficients[2] = table_tops[i].coeffs[2];//coefficients->values[2];
            seg_msg->coefficients[3] = table_tops[i].coeffs[3];////coefficients->values[3];
                    
            seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));
                    
            for (size_t k = 0; k < table_points->points.size (); ++k){
                seg_msg->points[k].xyz[0] = table_points->points[k].x; 
                seg_msg->points[k].xyz[1] = table_points->points[k].y; 
                seg_msg->points[k].xyz[2] = table_points->points[k].z; 
            }
                    
            seg_msg->centroid[0] = table_tops[i].mean[0];
            seg_msg->centroid[1] = table_tops[i].mean[1];
            seg_msg->centroid[2] = table_tops[i].mean[2];
            msg.no_segments++;
        }

        double table_height = -(table_tops[i].coeffs[3]/table_tops[i].coeffs[2]);

        int no_points = table_points->points.size ();
        pointlist2d_t *ptr = pointlist2d_new (no_points);
        
        for(size_t k = 0; k < table_points->points.size (); ++k){
            ptr->points[k].x = table_points->points[k].x;
            ptr->points[k].y = table_points->points[k].y;
        }

        double coeffs[4] = {table_tops[i].coeffs[0], table_tops[i].coeffs[1], table_tops[i].coeffs[2], table_tops[i].coeffs[3]};
              
        pointlist2d_t *table_bb = convexhull_graham_scan_2d(ptr);
        
        for(size_t j=0; j < objects.size(); j++){
            if((table_height - objects[j].mean[2]) > 0)
              continue;

            point2d_t pt;
            int inside = 0;
            for(size_t k = 0; k < objects[j].cloud->points.size (); ++k){
                pt.x = objects[j].cloud->points[k].x;
                pt.y = objects[j].cloud->points[k].y;

                int is_inside = geom_point_inside_or_on_edge_of_convex_polygon_2d (&pt, table_bb);
                if(is_inside ==1){
                    inside = 1;
                    fprintf(stderr, "Object found on top of table\n");
                    break;
                }
            }
            
            if(inside){
                if(draw_objects){
                    msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
                    erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];
                    
                    seg_msg->segment_id = msg.no_segments; 
                    seg_msg->no_points = objects[j].cloud->points.size ();
                    
                    seg_msg->coefficients[0] = 0;
                    seg_msg->coefficients[1] = 0;
                    seg_msg->coefficients[2] = 1;
                    seg_msg->coefficients[3] = 1;
                    
                    seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));
        
                    for (size_t k = 0; k < objects[j].cloud->points.size (); ++k){
                        seg_msg->points[k].xyz[0] = objects[j].cloud->points[k].x; 
                        seg_msg->points[k].xyz[1] = objects[j].cloud->points[k].y; 
                        seg_msg->points[k].xyz[2] = objects[j].cloud->points[k].z; 
                    }
                    
                    seg_msg->centroid[0] =  objects[j].mean[0]; 
                    seg_msg->centroid[1] =  objects[j].mean[1]; 
                    seg_msg->centroid[2] =  objects[j].mean[2]; 
                    msg.no_segments++;
                }

                double params[8] = {0};
                int is_cylinder = fit_cylinder(objects[j].cloud, params);
                fprintf(stderr, "Object is cylinder : %d\n", is_cylinder);

      
                detected_objects->objects = (object_t *) realloc(detected_objects->objects, sizeof(object_t) * (detected_objects->count +1));

                double t, x_t, y_t, z_t;

                if(is_cylinder){                
                    //use the line params to find the point that intersects with the tabletop
                    //also figure out the height - using the vertical extent of the cloud 
                    t = - (coeffs[0] * params[0] + coeffs[1] * params[1] + coeffs[2] * params[2] + coeffs[3]) / (coeffs[0] * params[3] + coeffs[1] * params[4] + coeffs[2] * params[5]);

                    x_t = params[0] + t * params[3];
                    y_t = params[1] + t * params[4];
                    z_t = params[2] + t * params[5];

                    detected_objects->objects[detected_objects->count].center[0] = x_t;//objects[j].mean[0];
                    detected_objects->objects[detected_objects->count].center[1] = y_t;//objects[j].mean[1];
                    detected_objects->objects[detected_objects->count].center[2] = z_t;//objects[j].mean[2];
                    
                    detected_objects->objects[detected_objects->count].radius = params[6];
                    detected_objects->objects[detected_objects->count].is_cylinder = 1;
                }
                else{
                    detected_objects->objects[detected_objects->count].center[0] = objects[j].mean[0];
                    detected_objects->objects[detected_objects->count].center[1] = objects[j].mean[1];
                    detected_objects->objects[detected_objects->count].center[2] = objects[j].mean[2];
                    
                    detected_objects->objects[detected_objects->count].radius = params[6];
                    detected_objects->objects[detected_objects->count].is_cylinder = 0;
                }
                
                detected_objects->count++; 
                
                if(is_cylinder){
                    cyl_objects.list = (erlcm_cylinder_model_t *) realloc(cyl_objects.list, sizeof(erlcm_cylinder_model_t) * (cyl_objects.count +1)); 
                    cyl_objects.list[cyl_objects.count].line_point[0] = x_t;//params[0]; 
                    cyl_objects.list[cyl_objects.count].line_point[1] = y_t;//params[1]; 
                    cyl_objects.list[cyl_objects.count].line_point[2] = z_t;//params[2]; 

                    cyl_objects.list[cyl_objects.count].line_direction[0] = params[3]; 
                    cyl_objects.list[cyl_objects.count].line_direction[1] = params[4]; 
                    cyl_objects.list[cyl_objects.count].line_direction[2] = params[5]; 
            
                    cyl_objects.list[cyl_objects.count].radius = params[6]; 
                    cyl_objects.count++;
                }
            }
        }            
        pointlist2d_free(table_bb);
        pointlist2d_free(ptr);
    }
    else{
        for(size_t i=0; i < table_tops.size(); i++){
            pcl::PointCloud<pcl::PointXYZ>::Ptr table_points = table_tops[i].cloud;

            double table_height = -table_tops[i].coeffs[3];

            int no_points = table_points->points.size ();
            pointlist2d_t *ptr = pointlist2d_new (no_points);
        
            for(size_t k = 0; k < table_points->points.size (); ++k){
                ptr->points[k].x = table_points->points[k].x;
                ptr->points[k].y = table_points->points[k].y;
            }

            double coeffs[4] = {table_tops[i].coeffs[0], table_tops[i].coeffs[1], table_tops[i].coeffs[2], table_tops[i].coeffs[3]};
              
            pointlist2d_t *table_bb = convexhull_graham_scan_2d(ptr);
        
            for(size_t j=0; j < objects.size(); j++){
                if((table_height - objects[j].mean[2]) > 0)
                    continue;

                point2d_t pt;
                int inside = 0;
                for(size_t k = 0; k < objects[j].cloud->points.size (); ++k){
                    pt.x = objects[j].cloud->points[k].x;
                    pt.y = objects[j].cloud->points[k].y;

                    int is_inside = geom_point_inside_or_on_edge_of_convex_polygon_2d (&pt, table_bb);
                    if(is_inside ==1){
                        inside = 1;
                        fprintf(stderr, "Object found on top of table\n");
                        break;
                    }
                }
            
                if(inside){
                    if(draw_objects){
                        msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
                        erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];
                    
                        seg_msg->segment_id = msg.no_segments; 
                        seg_msg->no_points = objects[j].cloud->points.size ();
                    
                        seg_msg->coefficients[0] = 0;
                        seg_msg->coefficients[1] = 0;
                        seg_msg->coefficients[2] = 1;
                        seg_msg->coefficients[3] = 1;
                    
                        seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));
        
                        for (size_t k = 0; k < objects[j].cloud->points.size (); ++k){
                            seg_msg->points[k].xyz[0] = objects[j].cloud->points[k].x; 
                            seg_msg->points[k].xyz[1] = objects[j].cloud->points[k].y; 
                            seg_msg->points[k].xyz[2] = objects[j].cloud->points[k].z; 
                        }
                    
                        seg_msg->centroid[0] =  objects[j].mean[0]; 
                        seg_msg->centroid[1] =  objects[j].mean[1]; 
                        seg_msg->centroid[2] =  objects[j].mean[2]; 
                        msg.no_segments++;
                    }

                    double params[8] = {0};
                    int is_cylinder = fit_cylinder(objects[j].cloud, params);
                    fprintf(stderr, "Object is cylinder : %d\n", is_cylinder);

      
                    detected_objects->objects = (object_t *) realloc(detected_objects->objects, sizeof(object_t) * (detected_objects->count +1));

                    double t, x_t, y_t, z_t;

                    if(is_cylinder){                
                        //use the line params to find the point that intersects with the tabletop
                        //also figure out the height - using the vertical extent of the cloud 
                        t = - (coeffs[0] * params[0] + coeffs[1] * params[1] + coeffs[2] * params[2] + coeffs[3]) / (coeffs[0] * params[3] + coeffs[1] * params[4] + coeffs[2] * params[5]);

                        x_t = params[0] + t * params[3];
                        y_t = params[1] + t * params[4];
                        z_t = params[2] + t * params[5];

                        detected_objects->objects[detected_objects->count].center[0] = x_t;//objects[j].mean[0];
                        detected_objects->objects[detected_objects->count].center[1] = y_t;//objects[j].mean[1];
                        detected_objects->objects[detected_objects->count].center[2] = z_t;//objects[j].mean[2];
                    
                        detected_objects->objects[detected_objects->count].radius = params[6];
                        detected_objects->objects[detected_objects->count].is_cylinder = 1;
                    }
                    else{
                        detected_objects->objects[detected_objects->count].center[0] = objects[j].mean[0];
                        detected_objects->objects[detected_objects->count].center[1] = objects[j].mean[1];
                        detected_objects->objects[detected_objects->count].center[2] = objects[j].mean[2];
                    
                        detected_objects->objects[detected_objects->count].radius = params[6];
                        detected_objects->objects[detected_objects->count].is_cylinder = 0;
                    }
                
                    detected_objects->count++; 
                
                    if(is_cylinder){
                        cyl_objects.list = (erlcm_cylinder_model_t *) realloc(cyl_objects.list, sizeof(erlcm_cylinder_model_t) * (cyl_objects.count +1)); 
                        cyl_objects.list[cyl_objects.count].line_point[0] = x_t;//params[0]; 
                        cyl_objects.list[cyl_objects.count].line_point[1] = y_t;//params[1]; 
                        cyl_objects.list[cyl_objects.count].line_point[2] = z_t;//params[2]; 

                        cyl_objects.list[cyl_objects.count].line_direction[0] = params[3]; 
                        cyl_objects.list[cyl_objects.count].line_direction[1] = params[4]; 
                        cyl_objects.list[cyl_objects.count].line_direction[2] = params[5]; 
            
                        cyl_objects.list[cyl_objects.count].radius = params[6]; 
                        cyl_objects.count++;
                    }
                    
                }
            }            
            pointlist2d_free(table_bb);
            pointlist2d_free(ptr);
        }   
    } 

    if(cyl_objects.count){
        cyl_objects.utime = bot_timestamp_now();
        erlcm_cylinder_list_t_publish(lcm, "CYLINDER_LIST", &cyl_objects);
        
        free(cyl_objects.list);    
    }

    erlcm_segment_list_t_publish(lcm, "PCL_SEGMENT_LIST", &msg);
    
    for(int k = 0; k < msg.no_segments; k++){
        free(msg.segments[k].points);
    }
    free(msg.segments);     
    
    return 0;
    }*/

/*int find_table_planes(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, int filter, lcm_t *lcm){
    
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>); 
    const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
    //pcl::VoxelGrid<sensor_msgs::PointCloud2> sor;
    pcl::VoxelGrid<pcl::PointXYZ> sor;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob (new pcl::PointCloud<pcl::PointXYZ>); 

    if(filter){        
        sor.setInputCloud (cloud->makeShared ());
        sor.setLeafSize (0.01f, 0.01f, 0.01f); //was 0.01
        sor.filter (*cloud_filtered_blob);
    }
    else{
        cloud_filtered_blob = cloud;
    }
    
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    //seg.setMaxIterations (1000);

    //play with this to get different size segments 
    seg.setDistanceThreshold (0.01); //was 0.01
    
    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    
    int nr_points = (int) cloud_filtered_blob->points.size ();

    //while (cloud_filtered_blob->points.size () > 0.3 * nr_points){
    // Segment the largest planar component from the remaining cloud
    seg.setInputCloud (cloud_filtered_blob);
    seg.segment (*inliers, *coefficients);
    if (inliers->indices.size () == 0){
        std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
        break;
    }
    
    std::cerr << "Model coefficients: " << coefficients->values[0] << " " 
              << coefficients->values[1] << " "
              << coefficients->values[2] << " " 
              << coefficients->values[3] << std::endl;
    
    int table_top = 0;
    if( fabs(coefficients->values[0]) < 0.1 && fabs(coefficients->values[1]) < 0.1 && 
        fabs(coefficients->values[2]) > 0.9 && (-coefficients->values[3]/ coefficients->values[2] > 0.2 && coefficients->values[3] /coefficients->values[2] < 1.5)){
        table_top = 1;
        fprintf(stderr, "Plane parallel to ground plane - detected => Possible table\n");
    }
    
    // Extract the inliers
    extract.setInputCloud(cloud_filtered_blob);
    extract.setIndices (inliers);
    extract.setNegative (false);
    extract.filter (*cloud_p);
    
    if(table_top){
        return 1;
    }
    return 0;
}*/

void compute_normals(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, lcm_t *lcm){
    std::cout << "points: " << cloud->points.size () << std::endl;

    // Create the normal estimation class, and pass the input dataset to it
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normal_estimation;
    normal_estimation.setInputCloud (cloud);

    // Create an empty kdtree representation, and pass it to the normal estimation object.
    // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
    normal_estimation.setSearchMethod (tree);

    // Output datasets
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

    // Use all neighbors in a sphere of radius 3cm
    //normal_estimation.setRadiusSearch (0.03);
    normal_estimation.setRadiusSearch (0.3);

    //might need to set viewpoints 

    //cloud.setViewPoint (0, 0, 0);

    // Compute the features
    normal_estimation.compute (*cloud_normals);

    publish_pcl_normal_points_to_lcm(cloud, cloud_normals, lcm);

    // cloud_normals->points.size () should have the same size as the input cloud->points.size ()
    std::cout << "cloud_normals->points.size (): " << cloud_normals->points.size () << std::endl;
}
