#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>

#include <gsl/gsl_blas.h>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <velodyne/velodyne.h>
#include <velodyne/velodyne_extractor.h>

#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <unistd.h>
#include <getopt.h>
#include <pthread.h>

#include <lcmtypes/er_lcmtypes.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>

#include <velodyne/velodyne.h>
#include <path_util/path_util.h>

#include <lcmtypes/senlcm_velodyne_list_t.h>
#include <lcmtypes/er_lcmtypes.h>

#include <gsl/gsl_blas.h>
#include <bot_frames/bot_frames.h>

#include <pcl/visualization/cloud_viewer.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d.h>
#include <lcmtypes/kinect_frame_msg_t.h>
#include <kinect/kinect-utils.h>
#include <pcl/common/common_headers.h>
#include <zlib.h>
#include "pcl/features/normal_3d.h"
#include <pcl/features/integral_image_normal.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <interfaces/pcl_to_lcm.hpp>

typedef struct _state_t state_t;

struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    velodyne_extractor_state_t *velodyne; 
    GMutex *mutex; 
    BotParam *param;
    BotFrames *frames;
    
    int full;
    int publish; 
    int write; 
    int normals;
    int segment;
    int sensor_frame; 
};

void compute_normals(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, lcm_t *lcm, double search_radius, double vp[3]){
    std::cout << "points: " << cloud->points.size () << std::endl;
    
    pcl::VoxelGrid<pcl::PointXYZ> sor;
    
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob (new pcl::PointCloud<pcl::PointXYZ>);
    sor.setInputCloud (cloud->makeShared ());
    sor.setLeafSize (0.1f, 0.1f, 0.1f); //was 0.01
    sor.filter (*cloud_filtered_blob);

    publish_pcl_points_to_lcm(cloud_filtered_blob, lcm);
    
    // Create the normal estimation class, and pass the input dataset to it
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normal_estimation;
    //normal_estimation.setInputCloud (cloud);
    //using the filtered points messes things up 
    normal_estimation.setInputCloud (cloud);

    // Create an empty kdtree representation, and pass it to the normal estimation object.
    // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
    normal_estimation.setSearchMethod (tree);

    // Output datasets
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

    // Use all neighbors in a sphere of radius 3cm
    //normal_estimation.setRadiusSearch (0.03);
    normal_estimation.setRadiusSearch (search_radius);

    //might need to set viewpoints 

    normal_estimation.setViewPoint (vp[0], vp[1], vp[2]);

    // Compute the features
    normal_estimation.compute (*cloud_normals);

    publish_pcl_normal_points_to_lcm(cloud, cloud_normals, lcm);

    // cloud_normals->points.size () should have the same size as the input cloud->points.size ()
    std::cout << "cloud_normals->points.size (): " << cloud_normals->points.size () << std::endl;
    
    cloud_filtered_blob->resize(0); 
}

void compute_normals(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, lcm_t *lcm, double search_radius){
    double vp[3] = {.0,.0,.0}; 
    compute_normals(cloud, lcm, search_radius, vp);
}

void segment_planes(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, state_t *s, int64_t utime, double vp[3]){
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob (new pcl::PointCloud<pcl::PointXYZ>); 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>); 
    const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
    //pcl::VoxelGrid<sensor_msgs::PointCloud2> sor;
    /*pcl::VoxelGrid<pcl::PointXYZ> sor;
    
    sor.setInputCloud (cloud->makeShared ());
    sor.setLeafSize (0.1f, 0.1f, 0.1f); //was 0.01
    sor.filter (*cloud_filtered_blob);
    */

    cloud_filtered_blob = cloud; 
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (1000);

    //play with this to get different size segments 
    seg.setDistanceThreshold (0.05); //was 0.01
    
    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    
    //pcl::PCDWriter writer;
    //writer.write<pcl::PointXYZ> ("out_downsampled.pcd", *cloud_filtered_blob, false);
        
    int i = 0, nr_points = (int) cloud_filtered_blob->points.size ();

    erlcm_segment_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.segments = NULL;
    
    msg.no_segments = 0;

    //figure out the ground plane 
    erlcm_plane_model_t grnd_msg;
    
    grnd_msg.utime = utime;

    double height_of_sensor = 1.23; 

    double dist_from_sensor = 10000; 

    int segment_ind = -1;
    
    // While 30% of the original cloud is still there
    while (cloud_filtered_blob->points.size () > 0.3 * nr_points){
        // Segment the largest planar component from the remaining cloud
        seg.setInputCloud (cloud_filtered_blob);
        seg.segment (*inliers, *coefficients);
        if (inliers->indices.size () == 0){
            std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
            break;
        }
        
        std::cerr << "Model coefficients: " << coefficients->values[0] << " " 
                                      << coefficients->values[1] << " "
                                      << coefficients->values[2] << " " 
                                      << coefficients->values[3] << std::endl;
        
        msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
        
        erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];//(erlcm_seg_point_list_t *) calloc(1, sizeof(erlcm_seg_point_list_t));
        seg_msg->segment_id = msg.no_segments; 
        seg_msg->no_points = inliers->indices.size();

        seg_msg->coefficients[0] = coefficients->values[0]; 
        seg_msg->coefficients[1] = coefficients->values[1]; 
        seg_msg->coefficients[2] = coefficients->values[2]; 
        seg_msg->coefficients[3] = coefficients->values[3];

        pcl::PointXYZ sensor_loc; 

        sensor_loc.x = vp[0];
        sensor_loc.y = vp[1];
        sensor_loc.z = vp[2]; 

        //these points are in the local frame - they need to be converted to the sensor frame 
        
        double dist = pcl::pointToPlaneDistance(sensor_loc, coefficients->values[0], 
                                                coefficients->values[1], 
                                                coefficients->values[2], 
                                                coefficients->values[3]);

        fprintf(stderr,"Distance to plane : %f\n", dist);
        
        //also check if its mostly normal 
        
        if(fabs(dist - height_of_sensor) < dist_from_sensor){
            segment_ind = msg.no_segments;
            dist_from_sensor = fabs(dist - height_of_sensor);
            grnd_msg.params[0] = coefficients->values[0];
            grnd_msg.params[1] = coefficients->values[1];
            grnd_msg.params[2] = coefficients->values[2];
            grnd_msg.params[3] = coefficients->values[3];
        }

        // Extract the inliers
        extract.setInputCloud(cloud_filtered_blob);
        extract.setIndices (inliers);
        extract.setNegative (false);
        extract.filter (*cloud_p);

        std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." << std::endl;
        
        seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));

        double mean[3] = {0,0,0};
        
        for (size_t k = 0; k < cloud_p->points.size (); ++k){
            seg_msg->points[k].xyz[0] = cloud_p->points[k].x; 
            seg_msg->points[k].xyz[1] = cloud_p->points[k].y; 
            seg_msg->points[k].xyz[2] = cloud_p->points[k].z; 
            mean[0] += cloud_p->points[k].x; 
            mean[1] += cloud_p->points[k].y; 
            mean[2] += cloud_p->points[k].z; 
        }
        
        seg_msg->centroid[0] = mean[0] / cloud_p->points.size (); 
        seg_msg->centroid[1] = mean[1] / cloud_p->points.size (); 
        seg_msg->centroid[2] = mean[2] / cloud_p->points.size (); 

        fprintf(stderr, "\tMean : %f, %f, %f\n", seg_msg->centroid[0], 
                seg_msg->centroid[1], seg_msg->centroid[2]);
        
        msg.no_segments++; 
        
        cloud_p->resize(0);
        
        // Create the filtering object
        extract.setNegative (true);
        extract.filter (*cloud_filtered_blob);
        
        i++;
    }


    fprintf(stderr,"------------------------------------------------------------\n");
    fprintf(stderr,"Dist from Ground Plane Estimate: %f\n", dist_from_sensor);
    fprintf(stderr,"------------------------------------------------------------\n");
    

    if(dist_from_sensor < 0.1){
        fprintf(stderr, "%f,%f,%f,%f\n", grnd_msg.params[0], 
                grnd_msg.params[1], 
                grnd_msg.params[2], 
                grnd_msg.params[3]);

        //transform the normal

        //then find the point on plane on the new coord - and then figure the last co-efficient 
        double old_normal[3] = {grnd_msg.params[0], grnd_msg.params[1], grnd_msg.params[2]};
        double new_normal[3] = {0,0,0};
        bot_frames_rotate_vec(s->frames, "body", "VELODYNE", old_normal, new_normal);

        //calculate point on the plane 
        double xyz_old[3] = {1,1, (grnd_msg.params[3] - grnd_msg.params[0]- grnd_msg.params[1]) / grnd_msg.params[2] };

        double body_to_sensor[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                      "VELODYNE", utime,
                                                      body_to_sensor)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        }

        double xyz_sensor[3];
        bot_vector_affine_transform_3x4_3d (body_to_sensor, xyz_old, xyz_sensor); 

        fprintf(stderr,"Transformed Points ; %f,%f,%f => %f,%f,%f\n", 
                xyz_old[0], xyz_old[1], xyz_old[2], 
                xyz_sensor[0], xyz_sensor[1], xyz_sensor[2]);

        double new_d = new_normal[0] * xyz_sensor[0] + 
            new_normal[1] * xyz_sensor[1] + 
            new_normal[2] * xyz_sensor[2];

        grnd_msg.params[0] = new_normal[0]; 
        grnd_msg.params[1] = new_normal[1]; 
        grnd_msg.params[2] = new_normal[2];
        grnd_msg.params[3] = new_d;

        fprintf(stderr, "Ground Normal in Sensor frame : %f,%f,%f,%f\n", grnd_msg.params[0], 
                grnd_msg.params[1], 
                grnd_msg.params[2], 
                grnd_msg.params[3]);
                
        erlcm_plane_model_t_publish(s->lcm,"GROUND_PLANE", &grnd_msg); 
    }

    msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
        
    erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];//(erlcm_seg_point_list_t *) calloc(1, sizeof(erlcm_seg_point_list_t));
    seg_msg->segment_id = -1;//msg.no_segments; 
    seg_msg->no_points = cloud_filtered_blob->points.size(); 
    msg.no_segments++; 

    fprintf(stderr,"Remainder : %d\n", cloud_filtered_blob->points.size());
    
    seg_msg->coefficients[0] = -1;
    seg_msg->coefficients[1] = -1;
    seg_msg->coefficients[2] = -1;
    seg_msg->coefficients[3] = -1;

    //add the rest to a different segment 
    seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));

    double mean[3] = {0,0,0};
    
    for (size_t k = 0; k < cloud_filtered_blob->points.size (); ++k){
        seg_msg->points[k].xyz[0] = cloud_filtered_blob->points[k].x; 
        seg_msg->points[k].xyz[1] = cloud_filtered_blob->points[k].y; 
        seg_msg->points[k].xyz[2] = cloud_filtered_blob->points[k].z; 
        mean[0] += cloud_filtered_blob->points[k].x; 
        mean[1] += cloud_filtered_blob->points[k].y; 
        mean[2] += cloud_filtered_blob->points[k].z; 
    }
    
    seg_msg->centroid[0] = mean[0] / cloud_filtered_blob->points.size (); 
    seg_msg->centroid[1] = mean[1] / cloud_filtered_blob->points.size (); 
    seg_msg->centroid[2] = mean[2] / cloud_filtered_blob->points.size (); 
    
    //
    
    //publish
    // erlcm_segment_list_t_publish(s->lcm, "PCL_SEGMENT_LIST", &msg);
    
    for(int k = 0; k < msg.no_segments; k++){
        free(msg.segments[k].points);
    }
    free(msg.segments);  
    cloud_filtered_blob->resize(0);
}

void segment_planes_all(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, lcm_t *lcm, int64_t utime, double vp[3]){
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_blob (new pcl::PointCloud<pcl::PointXYZ>); 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>); 
    const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
    //pcl::VoxelGrid<sensor_msgs::PointCloud2> sor;
    /*pcl::VoxelGrid<pcl::PointXYZ> sor;
    
    sor.setInputCloud (cloud->makeShared ());
    sor.setLeafSize (0.1f, 0.1f, 0.1f); //was 0.01
    sor.filter (*cloud_filtered_blob);
    */

    cloud_filtered_blob = cloud; 
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (1000);

    //play with this to get different size segments 
    seg.setDistanceThreshold (0.05); //was 0.01
    
    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    
    pcl::PCDWriter writer;
    writer.write<pcl::PointXYZ> ("out_downsampled.pcd", *cloud_filtered_blob, false);
        
    int i = 0, nr_points = (int) cloud_filtered_blob->points.size ();

    erlcm_segment_list_t msg;
    msg.utime = bot_timestamp_now(); 
    msg.segments = NULL;
    
    msg.no_segments = 0;

    // While 30% of the original cloud is still there
    while (cloud_filtered_blob->points.size () > 0.3 * nr_points){
        // Segment the largest planar component from the remaining cloud
        seg.setInputCloud (cloud_filtered_blob);
        seg.segment (*inliers, *coefficients);
        if (inliers->indices.size () == 0){
            std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
            break;
        }
        
        std::cerr << "Model coefficients: " << coefficients->values[0] << " " 
                                      << coefficients->values[1] << " "
                                      << coefficients->values[2] << " " 
                                      << coefficients->values[3] << std::endl;
        
        msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
        
        erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];//(erlcm_seg_point_list_t *) calloc(1, sizeof(erlcm_seg_point_list_t));
        seg_msg->segment_id = msg.no_segments; 
        seg_msg->no_points = inliers->indices.size();

        seg_msg->coefficients[0] = coefficients->values[0]; 
        seg_msg->coefficients[1] = coefficients->values[1]; 
        seg_msg->coefficients[2] = coefficients->values[2]; 
        seg_msg->coefficients[3] = coefficients->values[3];

        pcl::PointXYZ sensor_loc; 

        sensor_loc.x = vp[0];
        sensor_loc.y = vp[1];
        sensor_loc.z = vp[2]; 

        //these points are in the local frame - they need to be converted to the sensor frame 
        
        double dist = pcl::pointToPlaneDistance(sensor_loc, coefficients->values[0], 
                                                coefficients->values[1], 
                                                coefficients->values[2], 
                                                coefficients->values[3]);

        fprintf(stderr,"Distance to plane : %f\n", dist);
        
        // Extract the inliers
        extract.setInputCloud(cloud_filtered_blob);
        extract.setIndices (inliers);
        extract.setNegative (false);
        extract.filter (*cloud_p);

        std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." << std::endl;
        
        seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));

        double mean[3] = {0,0,0};
        
        for (size_t k = 0; k < cloud_p->points.size (); ++k){
            seg_msg->points[k].xyz[0] = cloud_p->points[k].x; 
            seg_msg->points[k].xyz[1] = cloud_p->points[k].y; 
            seg_msg->points[k].xyz[2] = cloud_p->points[k].z; 
            mean[0] += cloud_p->points[k].x; 
            mean[1] += cloud_p->points[k].y; 
            mean[2] += cloud_p->points[k].z; 
        }
        
        seg_msg->centroid[0] = mean[0] / cloud_p->points.size (); 
        seg_msg->centroid[1] = mean[1] / cloud_p->points.size (); 
        seg_msg->centroid[2] = mean[2] / cloud_p->points.size (); 

        fprintf(stderr, "\tMean : %f, %f, %f\n", seg_msg->centroid[0], 
                seg_msg->centroid[1], seg_msg->centroid[2]);
        
        msg.no_segments++; 
        
        cloud_p->resize(0);
        
        // Create the filtering object
        extract.setNegative (true);
        extract.filter (*cloud_filtered_blob);
        
        i++;
    }

    msg.segments = (erlcm_seg_point_list_t *)realloc(msg.segments, sizeof(erlcm_seg_point_list_t) * (msg.no_segments + 1));
        
    erlcm_seg_point_list_t *seg_msg = &msg.segments[msg.no_segments];//(erlcm_seg_point_list_t *) calloc(1, sizeof(erlcm_seg_point_list_t));
    seg_msg->segment_id = -1;//msg.no_segments; 
    seg_msg->no_points = cloud_filtered_blob->points.size(); 
    msg.no_segments++; 

    fprintf(stderr,"Remainder : %d\n", cloud_filtered_blob->points.size());
    
    seg_msg->coefficients[0] = -1;
    seg_msg->coefficients[1] = -1;
    seg_msg->coefficients[2] = -1;
    seg_msg->coefficients[3] = -1;

    //add the rest to a different segment 
    seg_msg->points = (erlcm_xyz_point_t *)calloc(seg_msg->no_points, sizeof(erlcm_xyz_point_t));

    double mean[3] = {0,0,0};
    
    for (size_t k = 0; k < cloud_filtered_blob->points.size (); ++k){
        seg_msg->points[k].xyz[0] = cloud_filtered_blob->points[k].x; 
        seg_msg->points[k].xyz[1] = cloud_filtered_blob->points[k].y; 
        seg_msg->points[k].xyz[2] = cloud_filtered_blob->points[k].z; 
        mean[0] += cloud_filtered_blob->points[k].x; 
        mean[1] += cloud_filtered_blob->points[k].y; 
        mean[2] += cloud_filtered_blob->points[k].z; 
    }
    
    seg_msg->centroid[0] = mean[0] / cloud_filtered_blob->points.size (); 
    seg_msg->centroid[1] = mean[1] / cloud_filtered_blob->points.size (); 
    seg_msg->centroid[2] = mean[2] / cloud_filtered_blob->points.size (); 
    
    //
    
    //publish
     erlcm_segment_list_t_publish(lcm, "PCL_SEGMENT_LIST", &msg);
    
    for(int k = 0; k < msg.no_segments; k++){
        free(msg.segments[k].points);
    }
    free(msg.segments);  
    cloud_filtered_blob->resize(0);
}

void integral_normals(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, lcm_t *lcm, double vp[3]){
    // estimate normals
    pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);

    pcl::IntegralImageNormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
    ne.setNormalEstimationMethod (ne.AVERAGE_3D_GRADIENT);
    ne.setMaxDepthChangeFactor(0.2f);
    ne.setNormalSmoothingSize(10.0f);
    ne.setInputCloud(cloud);
    //ne.setViewPoint (vp[0], vp[1], vp[2]);
    ne.compute(*normals);

    publish_pcl_normal_points_to_lcm(cloud, normals, lcm);
}


void process_velodyne_ground_plane_old(state_t *s){
    g_mutex_lock(s->mutex); 
    static const pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>& cloud = *point_cloud_ptr;
    //xyz_point_list_t *ret = velodyne_extract_points(s->velodyne);//, &p_list); 
    xyz_point_list_t *ret = velodyne_extract_points_frame(s->velodyne, "VELODYNE");//, &p_list); 

    static int64_t last_utime = 0;

    if(ret != NULL){
        //fprintf(stderr,"Have data\n");
        if(last_utime == ret->utime){
            destroy_xyz_list(ret);
            g_mutex_unlock(s->mutex);
            return;
        }

        last_utime = ret->utime;

        int valid_count = 0;
        for(int i=0; i < ret->no_points; i++){
            if(ret->points[i].xyz[2] < 0.5){
                valid_count++;
            }
        }

        fprintf(stderr,"Valid Count : %d\n", valid_count);

        cloud.height   = 1;
        cloud.width    = valid_count;

        cloud.is_dense = true;
        cloud.points.resize (cloud.width * cloud.height);

        valid_count = 0;

        //need to add the viewpoint - in the normal estimator 
        double sensor_to_body[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "VELODYNE",
                                                      "body", ret->utime,
                                                      sensor_to_body)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        }
        
        
        for(int i=0; i < ret->no_points; i++){
            double p_pos[3] = {ret->points[i].xyz[0],ret->points[i].xyz[1],ret->points[i].xyz[2]};
            double body_xyz[3];
            bot_vector_affine_transform_3x4_3d (sensor_to_body, p_pos, body_xyz);   
            //fprintf(stderr,"body_xyz : %f,%f,%f\n", body_xyz[0], body_xyz[1], body_xyz[2]);
            if(body_xyz[2] < 0.5){
                cloud.points[valid_count].x = ret->points[i].xyz[0];
                cloud.points[valid_count].y = ret->points[i].xyz[1];
                cloud.points[valid_count].z = ret->points[i].xyz[2];
                valid_count++;
            }
        }


        double sensor_pos[3] = {0,0,0};
        double local_xyz[3] = {0,0,0};
        //bot_vector_affine_transform_3x4_3d (sensor_to_body, sensor_pos, local_xyz);   

        if(s->publish)
            publish_pcl_points_to_lcm(cloud, s->lcm);

        if(s->write){
            char name[2048]; 
            sprintf(name, "velodyne_%d.pcd", (int) last_utime); 
        
            pcl::io::savePCDFileASCII (name, cloud);
            std::cerr << "Saved " << cloud.points.size () << " data points to test_pcd.pcd." << std::endl;
        }

        if(s->normals)
            //integral_normals(point_cloud_ptr, s->lcm, local_xyz);
            compute_normals(point_cloud_ptr, s->lcm, 0.3, local_xyz);
        
        if(s->segment)
            segment_planes(point_cloud_ptr, s, ret->utime, local_xyz);
        
        //******** Remember to destroy the cloud at the end
        cloud.points.resize (0);

        destroy_xyz_list(ret);
    }
    else{
        //fprintf(stderr,"No data\n");
    }
    
    g_mutex_unlock(s->mutex);
}

void process_velodyne_ground_plane(state_t *s){
    g_mutex_lock(s->mutex); 
    static const pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>& cloud = *point_cloud_ptr;
    //xyz_point_list_t *ret = velodyne_extract_points(s->velodyne);//, &p_list); 
    xyz_point_list_t *ret = velodyne_extract_points_frame(s->velodyne, "body");//, &p_list); 

    static int64_t last_utime = 0;

    if(ret != NULL){
        //fprintf(stderr,"Have data\n");
        if(last_utime == ret->utime){
            destroy_xyz_list(ret);
            g_mutex_unlock(s->mutex);
            return;
        }

        last_utime = ret->utime;

        int valid_count = 0;
        for(int i=0; i < ret->no_points; i++){
            if(ret->points[i].xyz[2] < 0.5){
                valid_count++;
            }
        }

        fprintf(stderr,"Valid Count : %d\n", valid_count);

        cloud.height   = 1;
        cloud.width    = valid_count;

        cloud.is_dense = true;
        cloud.points.resize (cloud.width * cloud.height);

        valid_count = 0;

        //need to add the viewpoint - in the normal estimator 
        double sensor_to_body[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "VELODYNE",
                                                      "body", ret->utime,
                                                      sensor_to_body)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        }
        
        
        for(int i=0; i < ret->no_points; i++){
            if(ret->points[i].xyz[2] < 0.5){
                cloud.points[valid_count].x = ret->points[i].xyz[0];
                cloud.points[valid_count].y = ret->points[i].xyz[1];
                cloud.points[valid_count].z = ret->points[i].xyz[2];
                valid_count++;
            }
        }

        double sensor_pos[3] = {0,0,0};
        double local_xyz[3];
        bot_vector_affine_transform_3x4_3d (sensor_to_body, sensor_pos, local_xyz);   

        if(s->publish)
            publish_pcl_points_to_lcm(cloud, s->lcm);

        if(s->write){
            char name[2048]; 
            sprintf(name, "velodyne_%d.pcd", (int) last_utime); 
        
            pcl::io::savePCDFileASCII (name, cloud);
            std::cerr << "Saved " << cloud.points.size () << " data points to test_pcd.pcd." << std::endl;
        }

        if(s->normals)
            //integral_normals(point_cloud_ptr, s->lcm, local_xyz);
            compute_normals(point_cloud_ptr, s->lcm, 0.3, local_xyz);
        
        if(s->segment)
            segment_planes(point_cloud_ptr, s, ret->utime, local_xyz);
        
        //******** Remember to destroy the cloud at the end
        cloud.points.resize (0);

        destroy_xyz_list(ret);
    }
    else{
        //fprintf(stderr,"No data\n");
    }
    
    g_mutex_unlock(s->mutex);
}

void process_velodyne_full(state_t *s){
    g_mutex_lock(s->mutex); 
    static const pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>& cloud = *point_cloud_ptr;
    xyz_point_list_t *ret = velodyne_extract_points(s->velodyne);//, &p_list); 

    static int64_t last_utime = 0;

    if(ret != NULL){

        if(last_utime == ret->utime){
            destroy_xyz_list(ret);
            g_mutex_unlock(s->mutex);
            return;
        }

        last_utime = ret->utime;

        cloud.height   = 32;
        cloud.width    = ret->no_points / 32.0;
        
        cloud.is_dense = true;
        cloud.points.resize (cloud.width * cloud.height);
    
        fprintf(stderr, "Utime : %f Size of Points : %d \n", ret->utime / 1.0e6, ret->no_points);

        for(int i = 0; i < cloud.height; i++){
            for(int s = 0; s < cloud.width; s++) {
                int ind_vel = s * 32 + i; 
                int ind_pcl = cloud.width * i + s; 
        
                cloud.points[ind_pcl].x = ret->points[ind_vel].xyz[0];
                cloud.points[ind_pcl].y = ret->points[ind_vel].xyz[1];
                cloud.points[ind_pcl].z = ret->points[ind_vel].xyz[2];
            }
        } 

        //here the points are in the local frame 

        //need to add the viewpoint - in the normal estimator 
        double sensor_to_local[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "VELODYNE",
                                                      "local", ret->utime,
                                                      sensor_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        }
        
        double sensor_pos[3] = {0,0,0};
        double local_xyz[3];
        bot_vector_affine_transform_3x4_3d (sensor_to_local, sensor_pos, local_xyz);   

        if(s->publish)
            publish_pcl_points_to_lcm(cloud, s->lcm);

        if(s->write){
            char name[2048]; 
            sprintf(name, "velodyne_%d.pcd", (int) last_utime); 
        
            pcl::io::savePCDFileASCII (name, cloud);
            std::cerr << "Saved " << cloud.points.size () << " data points to test_pcd.pcd." << std::endl;
        }

        if(s->normals)
            //integral_normals(point_cloud_ptr, s->lcm, local_xyz);
            compute_normals(point_cloud_ptr, s->lcm, 0.3, local_xyz);
        
        if(s->segment)
            segment_planes_all(point_cloud_ptr, s->lcm, ret->utime, local_xyz);
        
        //******** Remember to destroy the cloud at the end
        cloud.points.resize (0);

        destroy_xyz_list(ret);
    } 
    
    g_mutex_unlock(s->mutex);
}

//doesnt do anything right now - timeout function
gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;
    
    if(!s->full)
        process_velodyne_ground_plane(s);
    else
        process_velodyne_full(s);

    //return true to keep running
    return TRUE;
}

static void point_update_handler(int64_t utime, void *user)
{
    state_t *s = (state_t *)user;
    fprintf(stderr," ++++++++ Callback Called\n");
    xyz_point_list_t *points = velodyne_extract_points_frame(s->velodyne, "body");
    
    destroy_xyz_list(points);
}

static void usage(char * funcName)
{
    printf("Usage: %s [options]\n"
	   "options are:\n"
	   "--help,        -h    display this message \n"
	   "--publish_all, -a    publish robot laser messages for all channels \n"
	   "--verbose,     -v    be verbose", funcName);
    exit(1);

}

int 
main(int argc, char **argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    state->lcm =  bot_lcm_get_global(NULL);
    state->velodyne = velodyne_extractor_init(state->lcm, &point_update_handler, state);
    state->mainloop = g_main_loop_new( NULL, FALSE );  
    state->mutex = g_mutex_new();
    state->param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->param);

    const char *optstring = "pwnshfS";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "publish", no_argument, 0, 'p' }, 
				  { "write", no_argument, 0, 'w' }, 
                                  { "full", no_argument, 0, 'f' }, 
                                  { "sensor", no_argument, 0, 'S'},
                                  { "normals", no_argument, 0, 'n' }, 
                                  { "segment", no_argument, 0, 's' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    usage(argv[0]);
	    break;
	case 's':
	    {
		fprintf(stderr,"Segmenting\n");
                state->segment = 1;
		break;
	    }
        case 'p':
	    {
		fprintf(stderr,"Publishing\n");
                state->publish = 1;
		break;
	    }
        case 'f':
	    {
		fprintf(stderr,"Using full set of points\n");
                state->full = 1;
		break;
	    }
        case 'S':
	    {
		fprintf(stderr,"Publishing Planes in sensor frame\n");
                state->sensor_frame = 1;
		break;
	    }
	case 'w':
	    {
		fprintf(stderr,"Writing\n");
		state->write = 1;
		break;
	    }
        case 'n':
	    {
		fprintf(stderr,"Normals\n");
		state->normals = 1;
		break;
	    }
	default:
	    {
		usage(argv[0]);
		break;
	    }
	}
    }

    int publish; 
    int write; 
    int normals;
    int segment; 
  
    if (!state->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    /* heart beat*/
    g_timeout_add (100, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}
