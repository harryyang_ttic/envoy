#ifndef __ARM_COLLISION_CHECK_HPP
#define __ARM_COLLISION_CHECK_HPP

#include <glib.h>
#include "arm_collision_check_wrapper.h"
#include <bot_lcmgl_client/lcmgl.h>
#include <octomap/octomap.h>
#include <octomap/OcTree.h>




#ifdef __cplusplus
extern "C" {
#endif

using namespace std;
using namespace octomap;


typedef struct _cc_joint_t {
    double deg_to_tick;
    char *joint_name;
    BotTrans base_frame_to_parent;
    double seg_dim[3];
} cc_joint_t;


typedef struct _cc_joint_list_t {
    int no_joints;
    cc_joint_t *joints;
} cc_joint_list_t;


namespace arm_collision_check
{
    class ArmCollisionCheck {
    public:
        ArmCollisionCheck (gboolean _render, gboolean _publish_octree);
        
        virtual
        ~ArmCollisionCheck ();

        void
        checkConfiguration (double *angles, ArmCollisionCheckResult *result);

        void
        printStats ();

        message_buffer_t *nodder_plist_buffer;
        int got_message;
        nodder_extractor_state_t *nodder;

    private:
        lcm_t *lcm;
        BotParam *param;
        BotFrames *frames;

        int no_joints;
        cc_joint_list_t jlist;
        char *base_frame;

        double min_yaw_rad;
        double max_yaw_rad;

        message_buffer_t *goals_buffer;

        OcTree *octree;
        pthread_mutex_t obs_octree_mutex;

        pthread_mutex_t signal_refresh_octree_mutex;
        pthread_cond_t signal_refresh_octree;
        int signal_refresh_octree_count;
        pthread_t refresh_octree_thread; // Thread that populates the OcTree
        pthread_t lcm_handle_thread; 
        
        int64_t last_octree_send_utime;

        // LCMGL rendering
        bot_lcmgl_t *lcmgl_obs_check;

        // Vehicle footprint
        double footprint_front_left[2];
        double footprint_front_right[2];
        double footprint_rear_left[2];
        double footprint_rear_right[2];
        
        


        gboolean publish_octree;
        int publish_voxelmap;

        int verbose;

        int32_t num_check_calls;
        int64_t total_check_utime;
        int64_t max_check_utime;
        int64_t min_check_utime;

        void
        requestOctreeRefresh ();

        void
        refreshOctreeRequestWait (int64_t timeout_us);

        void
        drawObsOctree (OcTree *octree);

        static void *
        handleLCMThread (ArmCollisionCheck *self);

        static void *
        refreshOctreeThread (ArmCollisionCheck *self);

    };
};



#ifdef __cplusplus
}
#endif

#endif
