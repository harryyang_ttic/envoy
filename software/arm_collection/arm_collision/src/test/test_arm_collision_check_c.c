#include <glib.h>

#include <lcm/lcm.h>

#include <bot_core/bot_core.h>
#include <arm_collision_check/arm_collision_check_wrapper.h>



int main() {

    arm_collision_check_t *arm_cc = arm_collision_check_new(TRUE, TRUE);

    double angles_rad[2];
    ArmCollisionCheckResult res;
    
    arm_collision_check_configuration (arm_cc, angles_rad, &res);

    GMainLoop *mainloop = g_main_loop_new (NULL, FALSE);
    
    bot_signal_pipe_glib_quit_on_kill (mainloop);

    g_main_loop_run (mainloop);

}
