add_definitions(
    -std=gnu99
    )

add_executable (test-arm-collision-check  test_arm_collision_check.cpp)
pods_use_pkg_config_packages (test-arm-collision-check arm-collision-check)
pods_install_executables (test-arm-collision-check)

add_executable (test-arm-collision-check-c  test_arm_collision_check_c.c)
pods_use_pkg_config_packages (test-arm-collision-check-c arm-collision-check)
pods_install_executables (test-arm-collision-check-c)