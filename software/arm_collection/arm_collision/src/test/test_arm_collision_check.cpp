#include <glib.h>

#include <lcm/lcm.h>

#include <bot_core/bot_core.h>
#include <arm_collision_check/arm_collision_check.hpp>


using namespace arm_collision_check;

int main() {
    ArmCollisionCheck *cc = new ArmCollisionCheck (true, true);

    GMainLoop *mainloop = g_main_loop_new (NULL, FALSE);
    
    bot_signal_pipe_glib_quit_on_kill (mainloop);

    g_main_loop_run (mainloop);

}
