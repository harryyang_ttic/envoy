#include <stdlib.h>
#include <sstream>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif 

#include <octomap_utils/octomap_util.hpp>
#include <lcmtypes/octomap_raw_t.h>

#include "arm_collision_check.hpp"


#define JOINT_PREFIX "arm_config.joints"
#define NODDER_START_PITCH_DEG -40
#define NODDER_END_PITCH_DEG 40
#define NODDER_MAX_XY_DIST_TO_OCTREE 1.0

#define OCTREE_RESOLUTION 0.02
#define OCCUPANCY_THRESHOLD 0.5

#define LOGODDS_HIT 0.85
#define LOGODDS_MISS 0.4

// Collision check methods
#define COLLISION_CHECK_TYPE_LINE 1    // Models the link as a line checks points of DELTA_X spacing
#define COLLISION_CHECK_TYPE_CORNERS 2 // Checks the 8 corners of the polygon for each link
#define COLLISION_CHECK_TYPE_EDGES 3   // Checks along the entirety of the 4 edges along the each link

#define DELTA_X 0.05

using namespace octomap_utils;
using namespace std;
using namespace octomap;
using namespace arm_collision_check;


extern "C" {

arm_collision_check_t *arm_collision_check_new(gboolean _render, gboolean _publish_octree) 
{
    ArmCollisionCheck *arm_cc = new ArmCollisionCheck(_render, _publish_octree);

    return (arm_collision_check_t *) arm_cc;
}

void arm_collision_check_destroy (arm_collision_check_t *arm_cc)
{
    ArmCollisionCheck *self = (ArmCollisionCheck *) arm_cc;
    delete self;
}

void 
arm_collision_check_configuration (const arm_collision_check_t *arm_cc, double *angles_rad,
                                   ArmCollisionCheckResult *result)
{
    ArmCollisionCheck *self = (ArmCollisionCheck *) arm_cc;

    self->checkConfiguration (angles_rad, result);
}

void
arm_collision_check_print_stats (const arm_collision_check_t *arm_cc)
{
    ArmCollisionCheck *self = (ArmCollisionCheck *) arm_cc;

    self->printStats ();
}
}

bool
pointInPolygon (double polyX[4], double polyY[4], double xy[2]) 
{
    int   i, j=3;
    bool  oddNodes=FALSE;
    
    for (i=0; i<4; i++) {
        if ((polyY[i]< xy[1] && polyY[j]>=xy[1]
             ||   polyY[j]< xy[1] && polyY[i] >= xy[1])
            &&  (polyX[i]<= xy[0] || polyX[j] <= xy[0])) {
            oddNodes^=(polyX[i]+(xy[1]-polyY[i])/(polyY[j]-polyY[i])*(polyX[j]-polyX[i])<xy[0]); }
        j=i; }
    
    return oddNodes; 
}



void
nodderCallback (int64_t utime, void* user)
{
    ArmCollisionCheck *self = (ArmCollisionCheck *) user;

    xyz_point_list_t *plist = nodder_extract_new_points_frame (self->nodder, "local");
    if (!plist) {
        fprintf (stderr, "No points\n");
        return;
    }

    message_buffer_give (self->nodder_plist_buffer, plist);
    self->got_message = 1;
}


void 
timestampToTimespec(int64_t v, struct timespec *ts) 
{
    ts->tv_sec  = bot_timestamp_seconds(v);
    ts->tv_nsec = bot_timestamp_useconds(v)*1000;
}

void
ArmCollisionCheck::printStats ()
{
    fprintf (stdout, "ArmCollisionCheck check times: avg = %.6f ms (%d total checks), min = %.6f ms, max = %.6f ms\n",
             ((double) total_check_utime)/(((double) num_check_calls) * 1E3), num_check_calls,
             ((double) min_check_utime)/1E3, ((double) max_check_utime)/1E3);
}


void
ArmCollisionCheck::requestOctreeRefresh ()
{
    pthread_mutex_lock (&signal_refresh_octree_mutex);
    signal_refresh_octree_count++;
    pthread_mutex_unlock (&signal_refresh_octree_mutex);
}


void
ArmCollisionCheck::refreshOctreeRequestWait (int64_t timeout_us)
{
    pthread_mutex_lock (&signal_refresh_octree_mutex);

    if (signal_refresh_octree_count == 0) {
        double utime_timeout = bot_timestamp_now() + timeout_us;
        struct timespec ts;
        timestampToTimespec(utime_timeout, &ts);
 
        pthread_cond_timedwait(&signal_refresh_octree, &signal_refresh_octree_mutex, &ts);
    }

    signal_refresh_octree_count = 0;

    pthread_mutex_unlock(&signal_refresh_octree_mutex);
}


void
ArmCollisionCheck::drawObsOctree (OcTree *tmp_octree)
{
    // Add the nodder points to the OcTree
    xyz_point_list_t *plist = (xyz_point_list_t *) message_buffer_get (nodder_plist_buffer);
    if (plist) {

        // Used to define arm workspace
        BotTrans body_to_local;
        double bot_infront_body[] = {1.0, 0.0, 0.0};
        double bot_infront_local[3];

        bot_frames_get_trans_with_utime (frames, "body", "local", plist->utime, &body_to_local);
        bot_trans_apply_vec (&body_to_local, bot_infront_body, bot_infront_local);

        // For ray tracing, we need the origin of the ray in the local frame
        // Note that this is only approximate since there is a single utime associated with the plist
        // This may result in errors in the ray tracing.
        BotTrans sensor_to_local;
        double sensor_origin_sensor[] = {0, 0, 0};
        double sensor_origin_local[3];

        bot_frames_get_trans_with_utime (frames, "NODDING_LASER", "local", plist->utime, &sensor_to_local);
        bot_trans_apply_vec (&sensor_to_local, sensor_origin_sensor, sensor_origin_local);
        point3d origin (sensor_origin_local[0], sensor_origin_local[1], sensor_origin_local[2]);


        // We will ignore points that fall within the robot's footprint
        double footprint_front_left_local[2];
        double footprint_front_right_local[2];
        double footprint_rear_left_local[2];
        double footprint_rear_right_local[2];
        bot_trans_apply_vec (&body_to_local, footprint_front_left, footprint_front_left_local);
        bot_trans_apply_vec (&body_to_local, footprint_front_right, footprint_front_right_local);
        bot_trans_apply_vec (&body_to_local, footprint_rear_left, footprint_rear_left_local);
        bot_trans_apply_vec (&body_to_local, footprint_rear_right, footprint_rear_right_local);
        double footprintX[] = {footprint_front_left_local[0], footprint_front_right_local[0], 
                               footprint_rear_right_local[0], footprint_rear_left_local[0]};
        double footprintY[] = {footprint_front_left_local[1], footprint_front_right_local[1], 
                               footprint_rear_right_local[1], footprint_rear_left_local[1]};

        
        /*double forward_heading = atan2(bot_local_infront[1] - bot_local[1], 
          bot_local_infront[0] - bot_local[0]);*/

        for (size_t k=0; k < plist->no_points; k++) {
            
            if (plist->points[k].xyz[2] < 0.2 || plist->points[k].xyz[2] > 1.5)
                continue;

            // Ignore points outside the arm's workspace defined by range in XY plane
            if(hypot(plist->points[k].xyz[0] - bot_infront_local[0], 
                     plist->points[k].xyz[1] - bot_infront_local[1]) >  NODDER_MAX_XY_DIST_TO_OCTREE)
                     continue;

            // Check to see whether we are in the bot's footprint
            double xy[] = {plist->points[k].xyz[0], plist->points[k].xyz[1]};
            if (pointInPolygon (footprintX, footprintY, xy)) {
                continue;
            }

            point3d endpoint(plist->points[k].xyz[0], plist->points[k].xyz[1], plist->points[k].xyz[2]); 

            //if (!tmp_octree->insertRay (origin, endpoint))
            //    fprintf (stderr, "Error inserting ray into OcTree\n");
            if(!tmp_octree->updateNode(endpoint, true))
                fprintf (stderr, "Error updating node in OcTree\n");
        }
    }
    // TODO: We should clear the positions that correspond tot he robot arm
}


void*
ArmCollisionCheck::refreshOctreeThread (ArmCollisionCheck *self)
{

    while (1) {

        // wait up to 500ms
        self->refreshOctreeRequestWait (500000);

        if (!self->got_message)
            continue;

        // Create a new OcTree
        OcTree *local_octree = new OcTree (OCTREE_RESOLUTION);
        local_octree->setProbHit (LOGODDS_HIT);
        local_octree->setProbMiss (LOGODDS_MISS);


        pthread_mutex_lock (&self->obs_octree_mutex);
        // Populate the OcTree
        self->drawObsOctree (local_octree);

        if (self->octree)
            delete self->octree;
        self->octree = local_octree;
        pthread_mutex_unlock (&self->obs_octree_mutex);
        
        double minX, minY, minZ, maxX, maxY, maxZ;
        self->octree->getMetricMin(minX, minY, minZ);
        self->octree->getMetricMax(maxX, maxY, maxZ);

        if (self->publish_octree) {
            octomap_raw_t octomap_msg;
            octomap_msg.utime = bot_timestamp_now();
            
            for (int i = 0; i < 4; ++i) {
                for (int j = 0; j < 4; ++j)
                    octomap_msg.transform[i][j] = 0;
                octomap_msg.transform[i][i] = 1;
            }
            
            
            std::stringstream datastream;
            self->octree->writeBinaryConst(datastream);
            std::string datastring = datastream.str();
            octomap_msg.data = (uint8_t *) datastring.c_str();
            octomap_msg.length = datastring.size();
            
            octomap_raw_t_publish(self->lcm, "OCTOMAP", &octomap_msg);
        }
        
        // For generating voxel maps
        // int occupied_depth = 16;
        // int free_depth = 16;
        
        // //this drawing function and writing should also be only optional 
        // if (self->got_message) {
        //     occ_map::FloatVoxelMap * voxmap = octomapToVoxelMap(self->octree, occupied_depth, free_depth);
        //     const occ_map_voxel_map_t *occ_map_msg = voxmap->get_voxel_map_t(bot_timestamp_now());
        //     occ_map_voxel_map_t_publish(self->lcm, "VOXEL_MAP", occ_map_msg);
        //     //octree->clear();
        //     delete voxmap;
        // }
       

    }

    return NULL;

}

void
ArmCollisionCheck::checkConfiguration (double *angles_rad, ArmCollisionCheckResult *result)
{
    // Sets the type of collision check performed. See DEFINE statements at top
    int collision_check_type = 2;

    if (!got_message) {
        result->cost = -1;
        result->max = -1;
        return;
    }

    int64_t start_utime = bot_timestamp_now();

    // Keep track of the BotTrans from the current frame to the local frame
    BotTrans frame_to_local;

    bot_frames_get_trans(frames, base_frame, "local", &frame_to_local);

    // We will check one arm at a time, modeling it as a rectangle
    double worst_cost = 0;
    int in_collision = 0;
    pthread_mutex_lock (&obs_octree_mutex);


    double minXYZ[3];
    double maxXYZ[3];
    octree->getMetricMin(minXYZ[0], minXYZ[1], minXYZ[2]);
    octree->getMetricMax(maxXYZ[0], maxXYZ[1], maxXYZ[2]);
    

    bot_lcmgl_t *lcmgl = lcmgl_obs_check;
    if (lcmgl) {
        lcmglPointSize (10.0);
        lcmglBegin (GL_POINTS);
    }

    for (int i=0; i<jlist.no_joints; i++) {
        
        double rpy[] = {0, 0, angles_rad[i]};
        BotTrans articulated_frame;
        bot_roll_pitch_yaw_to_quat (rpy, articulated_frame.rot_quat);
        articulated_frame.trans_vec[0] = 0;
        articulated_frame.trans_vec[1] = 0;
        articulated_frame.trans_vec[2] = 0;

        bot_trans_apply_trans (&articulated_frame, &(jlist.joints[i].base_frame_to_parent));

        bot_trans_apply_trans_to (&frame_to_local, &articulated_frame, &frame_to_local);


        if (collision_check_type == COLLISION_CHECK_TYPE_LINE) {

            double delta_x = DELTA_X;
            if (jlist.joints[i].seg_dim[0] < DELTA_X)
                delta_x = jlist.joints[i].seg_dim[0];

            int num_partitions = ceil(jlist.joints[i].seg_dim[0]/delta_x) + 1;
            double *x = (double *) calloc (num_partitions, sizeof(double));
            double *y = (double *) calloc (num_partitions, sizeof(double));
            double *z = (double *) calloc (num_partitions, sizeof(double));

            for (int idx=1; idx<(num_partitions-1); idx++) {
                x[idx] = x[idx-1] + delta_x;
            }

            x[num_partitions-1] = jlist.joints[i].seg_dim[0];
            for (int j=0; j<num_partitions; j++) {
            
                double xyz_seg[] = {x[j], y[j], z[j]};
                double xyz_seg_local[3];
                bot_trans_apply_vec (&frame_to_local, xyz_seg, xyz_seg_local);

                // Check to see whether we are out of bounds
                if ( (xyz_seg_local[0] < minXYZ[0]) || (xyz_seg_local[0] > maxXYZ[0]) ||
                    (xyz_seg_local[1] < minXYZ[1]) || (xyz_seg_local[1] > maxXYZ[1]) ||
                    (xyz_seg_local[2] < minXYZ[2]) || (xyz_seg_local[2] > maxXYZ[02])) {
                    continue;
                }
            
                point3d query_xyz (xyz_seg_local[0], xyz_seg_local[1], xyz_seg_local[2]);
            
                double this_cost = 0;
                int this_in_collision = 0;
                OcTreeNode *query_result = octree->search (query_xyz);
                if (query_result) {
                    this_cost = query_result->getOccupancy();
                    this_in_collision = octree->isNodeOccupied (query_result);
                }
            
                worst_cost = bot_max (worst_cost, this_cost);
                in_collision = bot_max (in_collision, this_in_collision);
            
                if (lcmgl) {
                    if (!this_in_collision)
                        lcmglColor3f (0,1,0);
                    else
                        lcmglColor3f (1,0,0);
                
                    lcmglVertex3d (xyz_seg_local[0], xyz_seg_local[1], xyz_seg_local[2]);
                }
            }
            free (x);
            free (y);
            free (z);
        }
        else {
            double x[] = {0.0, jlist.joints[i].seg_dim[0]};
            double y[] = {-jlist.joints[i].seg_dim[1], jlist.joints[i].seg_dim[1]};
            double z[] = {-jlist.joints[i].seg_dim[2], jlist.joints[i].seg_dim[2]};
            

            // As an approximation, check the 8 corners of the polygon representing the arm
            if (collision_check_type == COLLISION_CHECK_TYPE_CORNERS) {
                for (int j=0; j<2; j++) {
                    for (int k=0; k<2; k++) {
                        for (int l=0; l<2; l++) {
                            
                            double xyz_seg[] = {x[j], y[k], z[l]};
                            double xyz_seg_local[3];
                            bot_trans_apply_vec (&frame_to_local, xyz_seg, xyz_seg_local);
                            
                            // Check to see whether we are out of bounds
                            if ( (xyz_seg_local[0] < minXYZ[0]) || (xyz_seg_local[0] > maxXYZ[0]) ||
                                (xyz_seg_local[1] < minXYZ[1]) || (xyz_seg_local[1] > maxXYZ[1]) ||
                                (xyz_seg_local[2] < minXYZ[2]) || (xyz_seg_local[2] > maxXYZ[02])) {
                                continue;
                            }
                            
                            point3d query_xyz (xyz_seg_local[0], xyz_seg_local[1], xyz_seg_local[2]);
                            
                            double this_cost = 0;
                            int this_in_collision = 0;
                            OcTreeNode *query_result = octree->search (query_xyz);
                            if (query_result) {
                                this_cost = query_result->getOccupancy();
                                this_in_collision = octree->isNodeOccupied (query_result);
                            }
                            
                            worst_cost = bot_max (worst_cost, this_cost);
                            in_collision = bot_max (in_collision, this_in_collision);
                            
                            if (lcmgl) {
                                if (!this_in_collision)
                                    lcmglColor3f (0,1,0);
                                else
                                    lcmglColor3f (1,0,0);
                                
                                lcmglVertex3d (xyz_seg_local[0], xyz_seg_local[1], xyz_seg_local[2]);
                            }
                        }
                    }
                }
            }
            else {
                
                
                // Check the four segments that comprise the corners of the
                // polygon that extends along the length of the arm segment
                for (int k=0; k<2; k++) {
                    for (int l=0; l<2; l++) {
                        
                        double xyz_seg_origin[] = {x[0], y[k], z[l]};
                        double xyz_seg_end[] = {x[1], y[k], z[l]};
                        
                        double xyz_seg_origin_local[3];
                        double xyz_seg_end_local[3];
                        bot_trans_apply_vec (&frame_to_local, xyz_seg_origin, xyz_seg_origin_local);
                        bot_trans_apply_vec (&frame_to_local, xyz_seg_end, xyz_seg_end_local);
                        
                        point3d point3d_origin (xyz_seg_origin_local[0], xyz_seg_origin_local[1], 
                                                xyz_seg_origin_local[2]);
                        
                        point3d point3d_end (xyz_seg_end_local[0], xyz_seg_end_local[1], 
                                             xyz_seg_end_local[2]);
                        
                        KeyRay ray;
                        octree->computeRayKeys (point3d_origin, point3d_end, ray);
                        
                        for (std::vector<octomap::OcTreeKey>::iterator it = ray.begin(); it != ray.end(); ++it) {
                            
                            point3d xyz_coord;
                            octree->genCoords (*it, 16, xyz_coord);
                            OcTreeNode *query_result = octree->search (xyz_coord);
                            double this_cost = 0;
                            int this_in_collision = 0;
                            if (query_result) {
                                this_cost = query_result->getOccupancy();
                                this_in_collision = octree->isNodeOccupied (query_result);
                                //fprintf (stdout, "Cost = %.4f, isOccupied = %d\n", this_cost,
                                //         octree->isNodeOccupied (query_result));
                            }
                            
                            worst_cost = bot_max (worst_cost, this_cost);
                            in_collision = bot_max (in_collision, this_in_collision);
                            
                            if (lcmgl) {
                                if (!this_in_collision)
                                    lcmglColor3f (0,1,0);
                                else
                                    lcmglColor3f (1,0,0);
                                
                                lcmglVertex3d (xyz_coord.x(), xyz_coord.y(), xyz_coord.z());
                            }
                        }
                        
                    }
                }
            }
        }
    }
    pthread_mutex_unlock (&obs_octree_mutex);

    if (lcmgl) {
        lcmglEnd();
        bot_lcmgl_switch_buffer (lcmgl_obs_check);
    }
    
    result->cost = worst_cost;
    result->max = worst_cost;
    result->in_collision = in_collision;

    int dt_utime =  bot_timestamp_now () - start_utime;
    num_check_calls++;
    total_check_utime += dt_utime;
    if (dt_utime > max_check_utime)
        max_check_utime = dt_utime;
    else if (dt_utime < min_check_utime)
        min_check_utime = dt_utime;
    

    return;

}


ArmCollisionCheck::~ArmCollisionCheck()
{
    printStats();

    if (!jlist.joints)
        free (jlist.joints);

    if (refresh_octree_thread) {
        pthread_cancel (refresh_octree_thread);
        pthread_join (refresh_octree_thread, NULL);
    }

    pthread_mutex_destroy (&signal_refresh_octree_mutex);
    pthread_mutex_destroy (&obs_octree_mutex);
}


// Constructor
ArmCollisionCheck::ArmCollisionCheck (gboolean _render, gboolean _publish_octree)
{
    pthread_mutex_init (&obs_octree_mutex, NULL);
    pthread_mutex_init (&signal_refresh_octree_mutex, NULL);

    verbose = 1;
    got_message = 0;
    publish_octree = _publish_octree;

    if (!(lcm = bot_lcm_get_global (NULL))) {
        fprintf (stderr, "Unable to get LCM!\n");
    }

    if (!(param = bot_param_new_from_server (lcm, 0))) {
        fprintf (stderr, "Unable to get BotParam!\n");
    }

    if (!(frames = bot_frames_get_global (lcm, param))) {
        fprintf (stderr, "Unable to get BotFrames!\n");
    }

    if (_render)
        lcmgl_obs_check = bot_lcmgl_init (lcm, "ARM_COLLISION_CHECK");
    else
        lcmgl_obs_check = NULL;
    

    nodder = nodder_extractor_init_full (lcm, 1, NODDER_START_PITCH_DEG, NODDER_END_PITCH_DEG,
                                         NULL,  &nodderCallback, (void *) this);


    //get information from param server
    char key[1024]; 
    char** joint_names = bot_param_get_subkeys(param, JOINT_PREFIX);

    int num_joints = 0;
    if (joint_names) {
        for (int pind = 0; joint_names[pind] != NULL; pind++) 
            num_joints++;
    }

    jlist.no_joints = num_joints; 
    jlist.joints = (cc_joint_t *) calloc (jlist.no_joints, sizeof(cc_joint_t));

    //incrementally fill them 
    for (int i=0; i< num_joints; i++) {

        jlist.joints[i].joint_name = strdup (joint_names[i]);
        sprintf (key,"%s.%s.joint_frame", JOINT_PREFIX, joint_names[i]);
       
        char *joint_frame;
        if (0 != bot_param_get_str (param, key, &joint_frame)){
            fprintf (stderr, "%s - frame not defined\n", key);
        }

        // Get the name of the base frame through which we transform to the local frame
        // as well as the min/max yaw of the arm workspace
        if (i==0) {
            sprintf (key, "%s.%s.relative_to", JOINT_PREFIX, joint_names[i]);

            if (0 != bot_param_get_str (param, key, &base_frame)){
                fprintf (stderr, "%s - frame not defined\n", key);
            }

            //fprintf (stdout, "Base frame = %s\n", base_frame);

            // Now get the yaw of the arm workspace
            int min_tick, max_tick, offset, sign;
            double deg_to_ticks;
            sprintf (key, "%s.%s.deg_to_ticks", JOINT_PREFIX, joint_names[i]);
            bot_param_get_double (param, key, &deg_to_ticks);
            sprintf (key, "%s.%s.servos.servo_1.min_range", JOINT_PREFIX, joint_names[i]);
            bot_param_get_int (param, key, &min_tick);
            sprintf (key, "%s.%s.servos.servo_1.max_range", JOINT_PREFIX, joint_names[i]);
            bot_param_get_int (param, key, &max_tick);
            sprintf (key, "%s.%s.servos.servo_1.offset", JOINT_PREFIX, joint_names[i]);
            bot_param_get_int (param, key, &offset);
            sprintf (key, "%s.%s.servos.servo_1.sign", JOINT_PREFIX, joint_names[i]);
            bot_param_get_int (param, key, &sign);

            min_yaw_rad = bot_to_radians (sign * ((float) (min_tick-offset))/deg_to_ticks);
            max_yaw_rad = bot_to_radians (sign * ((float) (max_tick-offset))/deg_to_ticks);

        }

        //fprintf (stdout, "Joint frame = %s\n", joint_frame);

        //get fixed transformation values for each arm segment
        sprintf(key,"coordinate_frames.%s.initial_transform.translation", joint_frame);
        bot_param_get_double_array (param, key, (jlist.joints[i].base_frame_to_parent.trans_vec), 3);

        sprintf (key,"coordinate_frames.%s.initial_transform.rpy", joint_frame);

        double rpy[3];
        bot_param_get_double_array (param, key, rpy, 3);
        
        // Convert to radians
        rpy[0] = bot_to_radians (rpy[0]);
        rpy[1] = bot_to_radians (rpy[1]);
        rpy[2] = bot_to_radians (rpy[2]);

        bot_roll_pitch_yaw_to_quat (rpy, jlist.joints[i].base_frame_to_parent.rot_quat);

        sprintf (key, "%s.%s.seg_dim", JOINT_PREFIX, joint_names[i]);
        if (bot_param_get_double_array (param, key, jlist.joints[i].seg_dim, 3) != 3) {
            fprintf (stderr, "%s = error getting segment dimensions\n", key);
        }

    }

    bot_param_get_double_array (param, "calibration.vehicle_bounds.front_left", footprint_front_left, 2);
    bot_param_get_double_array (param, "calibration.vehicle_bounds.front_right", footprint_front_right, 2);
    bot_param_get_double_array (param, "calibration.vehicle_bounds.rear_left", footprint_rear_left, 2);
    bot_param_get_double_array (param, "calibration.vehicle_bounds.rear_right", footprint_rear_right, 2);

    nodder_plist_buffer = message_buffer_create ((message_buffer_free_func_t) destroy_xyz_list);

    octree = NULL;

    // Stats-related variables
    num_check_calls = 0;
    total_check_utime = 0;
    max_check_utime = -1;
    min_check_utime = 1E10;

    pthread_cond_init (&signal_refresh_octree, NULL);
    pthread_mutex_init (&signal_refresh_octree_mutex, NULL);


    bot_glib_mainloop_attach_lcm (lcm);

    pthread_create (&refresh_octree_thread, NULL, (void * (*)(void *))refreshOctreeThread, 
                    (void *) this);

    /*pthread_create (&lcm_handle_thread, NULL, (void * (*)(void *))handleLCMThread, 
      (void *) this);*/
    
}




