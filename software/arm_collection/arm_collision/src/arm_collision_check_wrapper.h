#ifndef __ARM_COLLISION_CHECK_H
#define __ARM_COLLISION_CHECK_H

#include <lcm/lcm.h>
#include <bot_param/param_client.h>
#include <bot_core/bot_core.h>
#include <nodder/nodder_extractor.h>

#include "message_buffer.h"


typedef void arm_collision_check_t;

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _arm_collision_check_result_t {
    double cost;
    double max;
    int in_collision;
} ArmCollisionCheckResult;



arm_collision_check_t *arm_collision_check_new(gboolean _render, gboolean _publish_octree);
void arm_collision_check_destroy (arm_collision_check_t *arm_cc);
void arm_collision_check_configuration (const arm_collision_check_t *arm_cc, double *angles_rad,
                                        ArmCollisionCheckResult *result);
void arm_collision_check_print_stats (const arm_collision_check_t *arm_cc);



#ifdef __cplusplus
}
#endif

#endif
