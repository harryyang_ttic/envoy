//main.c
//Calculates waypoints for circular trajectory
#include <stdio.h>
#include <inttypes.h>
#include <glib.h>
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>

#include <math.h>
#include <stdbool.h>

#include <lcm/lcm.h>

#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif

//#include <arm_ik/ik.h>
#include <arm_interface/arm_interface.h>

#include <lcmtypes/arm_rrts_state_list_t.h>
#include <lcmtypes/arm_cmd_joint_list_t.h>
#include <lcmtypes/arm_cmd_joint_status_list_t.h>
#include <lcmtypes/arm_cmd_trace_status_t.h>

#define PI 3.14159265
//Errors are in degrees
#define MAX_CONTINUE_ERROR 5
#define MAX_AT_GOAL_ERROR 3
#define MAX_NEAR_GOAL_ERROR 8
#define MAX_WAIT_TIME  2
#define MAX_GOAL_WAIT_TIME 5
#define MIN_JOINT_MOTION 2
#define DYNAMIXEL_CUR_POS_ADDRESS 36

#define JOINT_PREFIX "arm_config.joints"

typedef struct _joint_info_t {
    int joint_id;
    int offset;
    int reference_servo;
    double deg_to_ticks;
    int sign;
} joint_info_t;

typedef struct _joint_progress_t {
    double angle;
    //    int64_t utime;
} joint_progress_t;

typedef struct _state_t{
    //    IK *ik;
    lcm_t *lcm;
    GMainLoop *mainloop;

    joint_info_t *joints;
    int num_joints;
    int verbose; 
    arm_rrts_state_list_t *trace_last;
    arm_cmd_joint_status_list_t *joint_status;
    arm_cmd_joint_status_list_t *joint_status_last;
    arm_cmd_joint_list_t *cmd_last;
    joint_progress_t *joint_progress;
    int trace_failed;
    int64_t last_progress_utime;
    int goal_index;

    int at_goal;
} state_t;

static gboolean
on_timer(gpointer data){
    state_t *self = (state_t *) data;

    if(!self->trace_last)
        return TRUE;

    if(self->at_goal){
        return TRUE;
    }
    if(self->trace_failed)
        return TRUE;
    if(!self->joint_status)
        return TRUE;
    
    if(self->goal_index >= self->trace_last->num_states && self->goal_index >0 && self->cmd_last){
        //this is not when we should put at goal - we should do that when the arm is actually at the goal
        //check the current to the goal - and check the errors - if within threshold - then say at goal 

        int all_joints_at_goal = 1;
        
        if((self->joint_status->utime - self->last_progress_utime)/ 1.0e6 > MAX_GOAL_WAIT_TIME){
            fprintf(stderr, "We have not made progress to the goal in a while - declaring error - Time lapsed : %f\n", 
                    (self->joint_status->utime - self->last_progress_utime)/ 1.0e6);	
	    	    
            int pretty_close = 1;
            for(int i = 0; i < self->joint_status->njoints; i++){
                double goal_angle = self->cmd_last->commands[i].goal_angle;
                double error = fabs(goal_angle - self->joint_status->joints[i].angle);
		
                if(error > MAX_NEAR_GOAL_ERROR)
                    pretty_close = 0;
            }
	    
            self->trace_failed = 1;
        
            for(int i = 0; i < self->joint_status->njoints; i++){
                fprintf(stderr, "Joint %d error: %f\n", i, fabs(self->cmd_last->commands[i].goal_angle - self->joint_status->joints[i].angle));
            }
            if(pretty_close){
                self->at_goal = 1;
                self->trace_failed = 0;
                return TRUE;
            }
            
            return TRUE;
        }
	
        for(int j = 0; j < self->joint_status->njoints; j++){
            //we assume that the joint IDs are the same 
            double cur_angle = self->joint_status->joints[j].angle;
            double goal_angle = self->cmd_last->commands[j].goal_angle;
            double error = fabs(goal_angle - cur_angle);
            //this will have problems if the commanded goal was outside the joint thresholds
            if(self->verbose)
                fprintf(stderr, "Joint %d Error %f Current Angle %f Goal Angle %f\n", j, error, cur_angle, goal_angle);
            
            if(error > MAX_AT_GOAL_ERROR){
                all_joints_at_goal = 0;
                if(self->verbose)
                    fprintf(stderr, "Moving to goal...\n");
	       
                break;
            }
        }
        if(all_joints_at_goal){
            if(self->cmd_last){
                arm_cmd_joint_list_t_destroy(self->cmd_last);
            }
            self->cmd_last = NULL;
            fprintf(stderr, "At goal\n");
            self->at_goal = 1;
        }
        return TRUE;
    }

    if(self->goal_index>0){
        double error = 0;
        int cur_angle_index = -1;

        if((self->joint_status->utime - self->last_progress_utime)/ 1.0e6 > MAX_WAIT_TIME){
            fprintf(stderr, "We have not made progress in a while - moving to next waypoint\n");
		
        }
        else{
            for(int i = 0; i < self->trace_last->states[0].num_dimensions; i++){      
                int joint_index = -1;
                for(int j = 0; j < self->joint_status->njoints; j++){
                    if(j == self->joint_status->joints[j].joint_id)
                        joint_index = j;
                }
		
                double cur_angle = self->joint_status->joints[i].angle;
		
                double goal_angle = self->cmd_last->commands[i].goal_angle;
                error = fabs(goal_angle - cur_angle);
                //this will have problems if the commanded goal was outside the joint thresholds
                if(self->verbose)
                    fprintf(stderr, "Joint %d Error %f Current Angle %f Goal Angle %f\n", i, error, cur_angle, goal_angle);
		
                if(error > MAX_CONTINUE_ERROR){
                    if(self->verbose){
                        printf("Max error exceded; waiting...\n");
                        //fprintf(stderr, "Wait time = %lu\n", self->joint_status->joints[i].utime - self->joint_progress[i].utime);
                    }
                    return TRUE;
                }
            }
        }
    }

    // Find the maximum distance any single joint must travel
    /* double max_travel_id = -1; */
    /* double max_travel_distance = -1; */
    /* for(int i = 0; i < self->joint_status->njoints; i++){ */
    /*     double cur_angle = self->joint_status->joints[i].angle; */
    /*     double goal_angle = bot_to_degrees(self->trace_last-> */
    /*                                        states[self->goal_index].angles[i]); */
    /*     double travel = fabs(cur_angle - goal_angle); */
    /*     if(travel > max_travel_distance){ */
    /*         max_travel_distance = travel; */
    /*         max_travel_id = self->joint_status->joints[i].joint_id; */
    /*     } */
    /* } */
    
    arm_cmd_joint_list_t jlist;
    int size = self->trace_last->states[self->goal_index].num_dimensions;
    jlist.ncommands = size;
    jlist.commands = (arm_cmd_joint_t *) calloc(size, sizeof(arm_cmd_joint_t));

    for(int i=0; i < size; i++){
        jlist.commands[i].joint_index = i;
        jlist.commands[i].torque_enable = 1;
        //arm joint commands expected in degrees
        jlist.commands[i].goal_angle = bot_to_degrees(self->trace_last->states[self->goal_index].angles[i]); 
        
	/* // calculate speed proportional to distance joint travels */
        /* double cur_angle = self->joint_status->joints[i].angle; */
        /* double goal_angle = bot_to_degrees(self->trace_last-> */
        /*                                    states[self->goal_index].angles[i]); */
        /* double travel = cur_angle - goal_angle; */
        /* double speed = (abs(travel)/max_travel_distance)*30; */
        /* if(speed < 10) */
        /*     speed = 10; */
        /* jlist.commands[i].max_speed = (abs(travel)/max_travel_distance)*30; */
        jlist.commands[i].max_speed = 30;
        jlist.commands[i].max_torque = 1023;
    }
  
    fprintf(stderr, "Goal ID : %d - Current Waypoint Ind : %d\n", self->trace_last->id, self->goal_index);
    
    arm_cmd_joint_list_t_publish(self->lcm, "ARM_JOINT_COMMANDS", &jlist);
    if(self->verbose)
        fprintf(stderr, "published.\n");
  
    if(self->cmd_last)
        arm_cmd_joint_list_t_destroy(self->cmd_last);

    self->cmd_last = arm_cmd_joint_list_t_copy(&jlist);

    free(jlist.commands);
  
    self->goal_index++;

    return TRUE;
}

void on_status_update(const lcm_recv_buf_t *rbuf, const char * channel_name,
                      const arm_cmd_joint_status_list_t* msg, void * user){
    state_t *self = user;
    
    if(self->joint_status)
        arm_cmd_joint_status_list_t_destroy(self->joint_status);	
  
    self->joint_status = arm_cmd_joint_status_list_t_copy(msg);

    //update progress

    if(self->cmd_last){
        int progress = 0;
        for(int i = 0; i < msg->njoints; i++){
            double goal_angle = self->cmd_last->commands[i].goal_angle;
            double error = fabs(goal_angle - msg->joints[i].angle);
	    
            if(fabs(error - self->joint_progress[i].angle) > MIN_JOINT_MOTION){
                self->last_progress_utime = msg->utime;
                progress = 1;
                break;
            }
        }

        if(progress){
            for(int i = 0; i < msg->njoints; i++){
                double goal_angle = self->cmd_last->commands[i].goal_angle;
                double error = fabs(goal_angle - msg->joints[i].angle);
		
                self->joint_progress[i].angle = error;
            }
        }	
    }
}

static gboolean
publish_status(gpointer data){
    state_t *self = (state_t *) data;
    
    if(!self->trace_last)
        return TRUE;
    
    arm_cmd_trace_status_t* msg = (arm_cmd_trace_status_t *) calloc(1, sizeof(arm_cmd_trace_status_t));

    msg->utime = bot_timestamp_now();
    msg->waypoint_id = self->trace_last->states[self->goal_index].id;
    
    if(self->at_goal == 1){
        //fprintf(stderr, "Goal Reached\n");
        msg->status = ARM_CMD_TRACE_STATUS_T_REACHED_GOAL;
    }
    else if(self->trace_failed == 1){
        msg->status = ARM_CMD_TRACE_STATUS_T_FAILED;
    }
    else if(self->trace_last){
        //fprintf(stderr, "Planning to goal\n");
        msg->status = ARM_CMD_TRACE_STATUS_T_PLANNING;
    }
    else{
        msg->status = ARM_CMD_TRACE_STATUS_T_IDLE;
    }

    msg->goal_id = self->trace_last->id;

    msg->waypoint_id = self->goal_index;

    arm_cmd_trace_status_t_publish(self->lcm, "TRACE_STATUS", msg);

    arm_cmd_trace_status_t_destroy(msg);

    return TRUE;
}

void command_trace_handler(const lcm_recv_buf_t * rbuf, const char * channel_name, const arm_rrts_state_list_t*  msg, void * user){

    state_t *self = user;
  
    if(self->trace_last)
        arm_rrts_state_list_t_destroy(self->trace_last);

    self->trace_last = arm_rrts_state_list_t_copy(msg);
    if(self->joint_status)
        self->last_progress_utime = self->joint_status->utime;
    else
        self->last_progress_utime = bot_timestamp_now();

    fprintf(stderr, "\n------------------------  Recieved new trace command ------------------------\n");
    fprintf(stderr, " Trace ID: %d\n\n", msg->id);

    self->trace_failed = 0;
    self->at_goal = 0;
    self->goal_index = 0;
    if(self->cmd_last)
        arm_cmd_joint_list_t_destroy(self->cmd_last);
    self->cmd_last = NULL;

    fprintf(stderr, "Clearing last cmd\n");
}

int main(int argc, char ** argv){
    state_t *self = (state_t*) calloc(1, sizeof(state_t));
 
    self->at_goal = 0;
    self->trace_failed = 0;
    self->verbose = 0;

    self->lcm = bot_lcm_get_global (NULL);
    if(!self->lcm) {
        fprintf (stderr, "Error getting LCM\n");
        //goto failed;
    }

    bot_glib_mainloop_attach_lcm(self->lcm);
  
    //    self->ik = ik_get_global(self->lcm);

    //Get information from param server
    BotParam * param = bot_param_new_from_server(self->lcm, 0);

    char key[1024]; 

    sprintf(key,"%s.num_joints", JOINT_PREFIX);
    bot_param_get_int(param, key, &self->num_joints);

    self->joints = (joint_info_t *) calloc(self->num_joints, sizeof(joint_info_t));
    self->joint_progress = (joint_progress_t *) calloc(self->num_joints, sizeof(joint_progress_t));
    self->last_progress_utime = 0;
    for(int i = 0; i < self->num_joints; i++){
        //self->joint_progress[i].utime = bot_timestamp_now();
        self->joint_progress[i].angle = 0;
    }

    char** joint_names = bot_param_get_subkeys(param, JOINT_PREFIX);
  
    int id_list[self->num_joints];
    for(int i = 0; i < self->num_joints; i++){
        sprintf(key, "%s.%s.joint_index", JOINT_PREFIX, joint_names[i]);
        bot_param_get_int(param, key, &id_list[i]);
    }

    for(int i = 0; i < self->num_joints; i++){
        sprintf(key, "%s.%s.joint_index", JOINT_PREFIX, joint_names[id_list[i]]);
        bot_param_get_int(param, key, &self->joints[i].joint_id);

        sprintf(key, "%s.%s.deg_to_ticks", JOINT_PREFIX, joint_names[id_list[i]]);
        bot_param_get_double(param, key, &self->joints[i].deg_to_ticks);

        sprintf(key, "%s.%s.servos", JOINT_PREFIX, joint_names[id_list[i]]);
        char **servos = bot_param_get_subkeys(param, key);
    
        for (int j = 0; servos[j] != NULL; j++){
            sprintf(key, "%s.%s.servos.%s.use_for_frame", JOINT_PREFIX, joint_names[id_list[i]], servos[j]);
            int use_for_frame;
            bot_param_get_int(param, key, &use_for_frame);
            if(use_for_frame){
                char servo_sub[1024];
                sprintf(servo_sub, "%s.%s.servos.%s", JOINT_PREFIX, joint_names[id_list[i]], servos[j]);
                sprintf(key, "%s.servo_index", servo_sub);
                bot_param_get_int(param, key, &self->joints[i].reference_servo);
	
                sprintf(key, "%s.offset", servo_sub);
                bot_param_get_int(param, key, &self->joints[i].offset);

                sprintf(key, "%s.sign", servo_sub);
                bot_param_get_int(param, key, &self->joints[i].sign);
            }
        }
    }

    g_timeout_add(25, on_timer, self);
    
    g_timeout_add(25, publish_status, self);
 
    arm_rrts_state_list_t_subscribe(self->lcm, "ARM_RRTS_STATE_LIST", &command_trace_handler, self);
    arm_cmd_joint_status_list_t_subscribe(self->lcm, "ARM_JOINT_STATUS", &on_status_update, self);

    self->mainloop = g_main_loop_new(NULL, FALSE);
    g_main_loop_run(self->mainloop);

       
    free(self);

    /*failed:
      if (self)
      free(self);
      return -1;
    */
}
