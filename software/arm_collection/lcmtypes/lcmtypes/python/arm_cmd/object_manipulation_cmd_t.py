"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

class object_manipulation_cmd_t(object):
    __slots__ = ["utime", "type", "requester"]

    VIEWER = 0
    SPEECH = 1
    JOYSTICK = 2
    TYPE_DETECT_OBJECT = 0
    TYPE_RELEASE_OBJECT = 1
    TYPE_PUT_OBJECT_BACK = 2
    TYPE_SEND_ARM_HOME = 3

    def __init__(self):
        self.utime = 0
        self.type = 0
        self.requester = 0

    def encode(self):
        buf = BytesIO()
        buf.write(object_manipulation_cmd_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">qbb", self.utime, self.type, self.requester))

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != object_manipulation_cmd_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return object_manipulation_cmd_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = object_manipulation_cmd_t()
        self.utime, self.type, self.requester = struct.unpack(">qbb", buf.read(10))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if object_manipulation_cmd_t in parents: return 0
        tmphash = (0xd4226a74f1a37f5a) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if object_manipulation_cmd_t._packed_fingerprint is None:
            object_manipulation_cmd_t._packed_fingerprint = struct.pack(">Q", object_manipulation_cmd_t._get_hash_recursive([]))
        return object_manipulation_cmd_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

