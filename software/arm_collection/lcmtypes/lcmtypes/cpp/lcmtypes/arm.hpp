#ifndef __lcmtypes_arm_hpp__
#define __lcmtypes_arm_hpp__

#include "arm_cmd/joint_draw.hpp"
#include "arm_cmd/qs_t.hpp"
#include "arm_cmd/object_manipulation_cmd_t.hpp"
#include "arm_cmd/joint_list_t.hpp"
#include "arm_cmd/circle_trace_t.hpp"
#include "arm_cmd/ef_pose_t.hpp"
#include "arm_cmd/joint_status_t.hpp"
#include "arm_cmd/joint_status_list_t.hpp"
#include "arm_cmd/joint_t.hpp"
#include "arm_cmd/qs_list_t.hpp"
#include "arm_cmd/grasp_plan_t.hpp"
#include "arm_cmd/trace_status_t.hpp"
#include "arm_cmd/ik_t.hpp"

#endif
