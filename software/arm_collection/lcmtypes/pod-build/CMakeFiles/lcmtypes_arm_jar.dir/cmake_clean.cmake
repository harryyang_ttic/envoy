FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/cpp"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/lcmtypes_arm_jar"
  "lcmtypes_arm.jar"
  "../lcmtypes/java/arm_cmd/trace_status_t.class"
  "../lcmtypes/java/arm_cmd/ef_pose_t.class"
  "../lcmtypes/java/arm_cmd/joint_draw.class"
  "../lcmtypes/java/arm_cmd/circle_trace_t.class"
  "../lcmtypes/java/arm_cmd/qs_t.class"
  "../lcmtypes/java/arm_cmd/qs_list_t.class"
  "../lcmtypes/java/arm_cmd/grasp_plan_t.class"
  "../lcmtypes/java/arm_cmd/joint_t.class"
  "../lcmtypes/java/arm_cmd/joint_status_t.class"
  "../lcmtypes/java/arm_cmd/ik_t.class"
  "../lcmtypes/java/arm_cmd/joint_list_t.class"
  "../lcmtypes/java/arm_cmd/object_manipulation_cmd_t.class"
  "../lcmtypes/java/arm_cmd/joint_status_list_t.class"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/lcmtypes_arm_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
