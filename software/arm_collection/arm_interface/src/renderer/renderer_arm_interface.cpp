#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <gdk/gdkkeysyms.h>
#include <geom_utils/geometry.h>

#include <bot_core/bot_core.h>
#include <bot_vis/gtk_util.h>
#include <bot_vis/viewer.h>
#include <bot_vis/gl_util.h>
#include <bot_vis/scrollplot2d.h>
#include <bot_vis/bot_vis.h>
#include <bot_frames/bot_frames.h>
#include <lcmtypes/arm_rrts_cmd_t.h>
#include <lcmtypes/arm_rrts_joint_cmd_t.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot2_param.h>
#include <lcmtypes/arm_cmd_joint_list_t.h>
#include <lcmtypes/arm_cmd_ik_t.h>
#include <lcmtypes/dynamixel_cmd_address_vals_t.h>

#include <arm_forward_kinematics/forward_kinematics.h>
#include <arm_forward_kinematics/ForwardK.hpp>
#include <arm_collision_check/arm_collision_check.hpp>

#include "renderer_arm_interface.h"

//#include "robot-arm-sensor-renderer.h"

#define RENDERER_NAME "Arm Interface"

#define SEND_ARM_COMMAND "Send Command"
#define SEND_HOME_COMMAND "Go To Home"
#define SEND_REST_COMMAND "Go To Rest"
#define SEND_DISABLE_TORQUE_COMMAND "Disable servo torque"
#define SEND_RRT_COMMAND "Send RRT Command"
#define SEND_RRT_JOINT_COMMAND "Send RRT Joint Command"
#define ITERATIONS "RRT Star Iterations"
#define HISTORY_STEP "Tree History Step Size"
#define SEND_XYZ_COMMAND "Send XYZ Command"

#define JOINT_PREFIX "arm_config.joints"
#define DRAW_GOAL_POSITION "Draw Goal"

#define DRAW_JOINT_POSITION "Draw Joints"

#define ADDRESS_VAL "Address Name"
#define SET_ADDRESS_VAL "Address Value"
#define SEND_ADDRESS_VALS "Send Values"

#define X_POS "X Position"
#define Y_POS "Y Position"
#define Z_POS "Z Position"
#define ROLL "Roll"
#define PITCH "Pitch"
#define YAW "Yaw"

using namespace arm_collision_check;

typedef struct _RendererRobotArmCommands RendererRobotArmCommands;

struct _RendererRobotArmCommands {
    lcm_t *lcm;

    BotRenderer renderer;
    BotViewer *viewer;
    BotFrames *frames;
    BotEventHandler ehandler;
    int goal_count;
    BotGtkParamWidget    *pw;
    ForwardK *forwardK;
    int num_joints;
    char **joint_names;
    int num_servos;
    int *num_servos_per_joint;
    BotTrans goal_position;

    double pos[2];

    double xyz_goal[3];

    uint64_t      max_utime;

    ForwardKinematics *fk;
    ArmCollisionCheck *cc;
};

void apply_values(arm_cmd_joint_t *joint, double joint_angle, int joint_index, int torque, double speed){
    joint->joint_index = joint_index; 
    joint->torque_enable = torque; 
    joint->goal_angle = joint_angle; 
    joint->max_speed = speed;
    joint->max_torque = 1023;
}

BotTrans get_end_effector_base_to_local(RendererRobotArmCommands *self, BotTrans *end_effector_to_base){
    BotTrans base_to_local; 
    bot_frames_get_trans(self->frames, "base", "local", &base_to_local);
        
    BotTrans end_effector_to_local; 

    bot_trans_apply_trans_to(&base_to_local, end_effector_to_base, &end_effector_to_local);

    return end_effector_to_local;
}

void print_bot_trans(BotTrans &trans){
    fprintf(stderr, "Bot Trans : %f,%f,%f\n", trans.trans_vec[0], 
            trans.trans_vec[1], trans.trans_vec[2]);
    
    double rpy[3] = {0};
    bot_quat_to_roll_pitch_yaw(trans.rot_quat, rpy);
    fprintf(stderr, "\t RPY : %f,%f,%f\n", bot_to_degrees(rpy[0]), 
            bot_to_degrees(rpy[1]),     
            bot_to_degrees(rpy[2]));
}

static void 
on_param_widget_changed (BotGtkParamWidget *pw, const char *name, 
                         RendererRobotArmCommands *self)
{
    if(!strcmp(name, SEND_ARM_COMMAND)) {
        //prob should have enable joint tick bozes also 
        
        int enable[self->num_joints];
        double joint_values[self->num_joints];
        char enable_str[1024];
        for (int i=0; i<self->num_joints; i++) {
            sprintf (enable_str, "%s-enable", self->joint_names[i]);
            enable[i] = bot_gtk_param_widget_get_bool (self->pw, enable_str);
            joint_values[i] = bot_gtk_param_widget_get_double (pw, self->joint_names[i]);
        }
        
        double angles_rad[self->num_joints];
        for (int i=0; i<self->num_joints; i++) 
            angles_rad[i] = bot_to_radians (joint_values[i]);
            

        BotTrans end_effector;
        fk_forward_kinematics (self->fk, angles_rad, &end_effector);
        //fprintf (stdout, "end_effector: trans = [%.4f, %.4f, %.4f]\n",
        //   end_effector.trans_vec[0], end_effector.trans_vec[1], end_effector.trans_vec[2]);

        //ArmCollisionCheckResult cc_result;
        //self->cc->checkConfiguration (angles_rad, &cc_result);
        //arm_collision_check_configuration (self->cc, angles_rad, &cc_result);
        //fprintf (stdout, "\n\n\nCC Result: cost = %.2f, max = %.2f, in_collision = %d\n\n\n", 
        //         cc_result.cost, cc_result.max, cc_result.in_collision);
        


        bot_frames_transform_vec (self->frames, "base", "local", end_effector.trans_vec, self->xyz_goal);
        int size = 0;
        for(int i=0; i < self->num_joints; i++){
            if(enable[i]){
                size++;
            }
        }

        double speed = 50;

        //ignoring the twist for now 
        arm_cmd_joint_t *list = (arm_cmd_joint_t *) calloc(size, sizeof(arm_cmd_joint_t));
    
        arm_cmd_joint_list_t msg;
        //= {
        msg.ncommands = size;
        msg.commands = list;
            //};


        int c = 0;
        //the index will be wrong for now
        for(int i=0; i < self->num_joints; i++){
            if(enable[i]){
                apply_values(&list[c], joint_values[i], i, 1, speed);
                c++;
            }
        }
        
        arm_cmd_joint_list_t_publish(self->lcm, "ARM_JOINT_COMMANDS", &msg);

        free(list);
        bot_viewer_request_redraw (self->viewer);
    }
     if(!strcmp(name, SEND_RRT_JOINT_COMMAND)){
        //Do a forward kinematics - and then send it off as a rrt command 
        double joint_values[self->num_joints];
        double joint_tolls[self->num_joints];
        for (int i=0; i<self->num_joints; i++) {
            joint_values[i] = bot_to_radians(bot_gtk_param_widget_get_double (pw, self->joint_names[i]));
            joint_tolls[i] = bot_to_radians(10.0);
            fprintf(stderr, "Joint [%d] : %f\n", i, joint_values[i]); 
        }
        
        double angles_rad[self->num_joints];
        for (int i=0; i<self->num_joints; i++) 
            angles_rad[i] = bot_to_radians (joint_values[i]);
        
        arm_rrts_joint_cmd_t msg;
        msg.id = ++self->goal_count;
        msg.no_joints = self->num_joints;
        msg.joint_angles = joint_values;
        msg.joint_tollerances = joint_tolls;
        msg.iterations = bot_gtk_param_widget_get_int(pw, ITERATIONS);
        msg.skip_collision_checks = 0;
        arm_rrts_joint_cmd_t_publish(self->lcm, "ARM_RRTS_JOINT_COMMAND", &msg);
    }

    if(!strcmp(name, SEND_RRT_COMMAND)){
        //Do a forward kinematics - and then send it off as a rrt command 
        double joint_values[self->num_joints];
        for (int i=0; i<self->num_joints; i++) {
            joint_values[i] = bot_gtk_param_widget_get_double (pw, self->joint_names[i]);
            fprintf(stderr, "Joint [%d] : %f\n", i, joint_values[i]); 
        }
        
        double angles_rad[self->num_joints];
        for (int i=0; i<self->num_joints; i++) 
            angles_rad[i] = bot_to_radians (joint_values[i]);
        
        BotTrans ef_to_base;

        self->forwardK->getJointsToBotFrames(angles_rad, &ef_to_base);
        
        BotTrans ef_to_local = get_end_effector_base_to_local(self, &ef_to_base);

        self->goal_position = ef_to_local;

        print_bot_trans(ef_to_base);

        arm_rrts_cmd_t msg;
        msg.id = ++self->goal_count;
        msg.gripper_enable = 1;
        msg.gripper_pos = 0;//gripper_pos;
        msg.xyz[0] = ef_to_local.trans_vec[0];
        msg.xyz[1] = ef_to_local.trans_vec[1];
        msg.xyz[2] = ef_to_local.trans_vec[2];
        bot_quat_to_roll_pitch_yaw(ef_to_local.rot_quat, msg.ef_rpy);
        msg.xyz_tolerance[0] = 0.3;//xyz_tolerance[0];
        msg.xyz_tolerance[1] = 0.3;//xyz_tolerance[1];
        msg.xyz_tolerance[2] = 0.3;//xyz_tolerance[2];
        msg.ef_rpy_tolerance[0] = bot_to_radians(10);//rpy_tolerance[0];
        msg.ef_rpy_tolerance[1] = bot_to_radians(10);//rpy_tolerance[1];
        msg.ef_rpy_tolerance[2] = bot_to_radians(10);//rpy_tolerance[2];
        msg.iterations = bot_gtk_param_widget_get_int(pw, ITERATIONS);
        msg.skip_collision_checks = 0;

        arm_rrts_cmd_t_publish(self->lcm, "ARM_RRTS_COMMAND", &msg);
    }
    if(!strcmp(name, SEND_HOME_COMMAND)) {
        //prob should have enable joint tick bozes also 
        
        double joint_values[self->num_joints];
        for (int i=0; i<self->num_joints; i++) {
            joint_values[i] = 0;
        }
        
        joint_values[1] = 107;
        joint_values[2] = -91.9;
        
        double speed = 50;

        //ignoring the twist for now 
        arm_cmd_joint_t *list = (arm_cmd_joint_t *) calloc(self->num_joints, sizeof(arm_cmd_joint_t));
    
        arm_cmd_joint_list_t msg;
        msg.ncommands = self->num_joints;
        msg.commands = list;

        for(int i=0; i < self->num_joints; i++){
            apply_values(&list[i], joint_values[i], i, 1, speed);
        }
        
        arm_cmd_joint_list_t_publish(self->lcm, "ARM_JOINT_COMMANDS", &msg);

        free(list);
        bot_viewer_request_redraw (self->viewer);
    }   
    if(!strcmp(name, SEND_REST_COMMAND)) {
        
        double joint_values[self->num_joints];
        
        joint_values[0] = 0;
        joint_values[1] = 140.1;
        joint_values[2] = -145;
        joint_values[3] = -81.7;
        joint_values[4] = 0;	
        joint_values[5] = 0;
        
        double speed = 50;

        arm_cmd_joint_t *list = (arm_cmd_joint_t *) calloc(self->num_joints, sizeof(arm_cmd_joint_t));
    
        arm_cmd_joint_list_t msg;
        msg.ncommands = self->num_joints;
        msg.commands = list;

        for(int i=0; i < self->num_joints; i++){
            apply_values(&list[i], joint_values[i], i, 1, speed);
        }
        
        arm_cmd_joint_list_t_publish(self->lcm, "ARM_JOINT_COMMANDS", &msg);

        free(list);
        bot_viewer_request_redraw (self->viewer);
    }  
    if(!strcmp(name, SEND_DISABLE_TORQUE_COMMAND)) {
        
        double joint_values[self->num_joints];
        
	joint_values[0] = 0;
        joint_values[1] = 139.7;
        joint_values[2] = -140.1;
        joint_values[3] = -81.7;
        joint_values[4] = 0;	
        joint_values[5] = 0;
        
        double speed = 50;

        arm_cmd_joint_t *list = (arm_cmd_joint_t *) calloc(self->num_joints, sizeof(arm_cmd_joint_t));
    
        arm_cmd_joint_list_t msg;
        msg.ncommands = self->num_joints;
        msg.commands = list;

        for(int i=0; i < self->num_joints; i++){
            apply_values(&list[i], joint_values[i], i, 0, speed);
        }
        
        arm_cmd_joint_list_t_publish(self->lcm, "ARM_JOINT_COMMANDS", &msg);

        free(list);
        bot_viewer_request_redraw (self->viewer);
    }  
}

static void 
on_address_vals_changed (BotGtkParamWidget *pw, const char *name, 
                         RendererRobotArmCommands *self)
{
    if(!strcmp(name, SEND_ADDRESS_VALS)){
        
        int enable[self->num_servos];
        int idx = 0;
        char enable_str[1024];
        for (int i=0; i<self->num_joints; i++) {
            sprintf (enable_str, "%s-enable", self->joint_names[i]);
            for (int j=0; j<self->num_servos_per_joint[i]; j++) {
                enable[idx] = bot_gtk_param_widget_get_bool (self->pw, enable_str);
                idx++;
            }
        }

            

        /* 
         * int enable[7] = {bot_gtk_param_widget_get_bool(self->pw, BASE_ENABLE), 
         *                  bot_gtk_param_widget_get_bool(self->pw, SHOULDER_ENABLE),
         *                  bot_gtk_param_widget_get_bool(self->pw, SHOULDER_ENABLE),
         *                  bot_gtk_param_widget_get_bool(self->pw, ELBOW_ENABLE), 
         *                  bot_gtk_param_widget_get_bool(self->pw, WRIST_BEND_ENABLE),
         *                  bot_gtk_param_widget_get_bool(self->pw, WRIST_TWIST_ENABLE),
         *                  bot_gtk_param_widget_get_bool(self->pw, GRIPPER_ENABLE) 
         * };
         */
      
        int add = bot_gtk_param_widget_get_enum(self->pw, ADDRESS_VAL);
        int value = bot_gtk_param_widget_get_int(self->pw, SET_ADDRESS_VAL);
        if(!(add == 6 || add == 8 || add == 14 || add == 30 || add == 32 || add == 34)){
            if(value > 255)
                return;
        }

        int size = 0;
        //int id[7]; 
        int id[self->num_servos];
        for(int i=0; i < self->num_servos; i++){
            if(enable[i]){
                id[size] = i;
                size++;
            }
        }
      
        dynamixel_cmd_address_vals_t msg;// = {
        msg.nservos = size;
        msg.address = add;
        //};

        msg.servo_id = (int32_t *)calloc(msg.nservos, sizeof(int32_t));
        msg.value = (int32_t *)calloc(msg.nservos, sizeof(int32_t));

        for(int i=0; i<size; i++){
            msg.servo_id[i] = id[i];
            msg.value[i] = value;
        }

        dynamixel_cmd_address_vals_t_publish(self->lcm, "DYNAMIXEL_COMMAND_ADDRESSES", &msg);
    
        free(msg.servo_id);
        free(msg.value);
    }
}

static int 
mouse_press (BotViewer *viewer, BotEventHandler *ehandler, const double ray_start[3], 
             const double ray_dir[3], const GdkEventButton *event)
{
    RendererRobotArmCommands *self = (RendererRobotArmCommands*) ehandler->user;

    point2d_t click_pt_local;
  
    if (0 != geom_ray_z_plane_intersect_3d(POINT3D(ray_start), 
                                           POINT3D(ray_dir), 0, &click_pt_local)) {
        bot_viewer_request_redraw(self->viewer);
        return 0;
    }

    bot_viewer_request_redraw(self->viewer);
    return 1;
}

static void draw_axis(BotTrans * axis_to_local, float size, float lineThickness, float opacity)
{
    double axis_to_local_m[16];
    bot_trans_get_mat_4x4(axis_to_local, axis_to_local_m);

    // opengl expects column-major matrices
    double axis_to_local_m_opengl[16];
    bot_matrix_transpose_4x4d(axis_to_local_m, axis_to_local_m_opengl);

    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glEnable(GL_DEPTH_TEST);

    glPushMatrix();
    // rotate and translate the vehicle
    glMultMatrixd(axis_to_local_m_opengl);

    glLineWidth(lineThickness);
    //x-axis
    glBegin(GL_LINES);
    glColor3f(1.0, 0.0, 0.0);
    glVertex3f(size, 0, 0);
    glVertex3f(0, 0, 0);
    glEnd();

    //y-axis
    glBegin(GL_LINES);  
    glColor3f(0.0, 1.0, 0.0);
    glVertex3f(0, size, 0);
    glVertex3f(0, 0, 0);
    glEnd();

    //z-axis
    glBegin(GL_LINES);  
    glColor3f(0.0, 0.0, 1.0);
    glColor4f(0, 0, 1, opacity);
    glVertex3f(0, 0, size);
    glVertex3f(0, 0, 0);
    glEnd();

    glPopMatrix();
}

static void
robot_commands_draw (BotViewer *viewer, BotRenderer *renderer)
{
    RendererRobotArmCommands *self = (RendererRobotArmCommands*) renderer->user;

    glEnable (GL_DEPTH_TEST);
    glPointSize (10.0);
    glColor3f(1.0, 0.0, 0.0);
    glBegin (GL_POINTS);
    glVertex3f (self->xyz_goal[0],
                self->xyz_goal[1],
                self->xyz_goal[2]);
    glEnd ();
    
    bool draw_goal = bot_gtk_param_widget_get_bool(self->pw, DRAW_GOAL_POSITION);
    bool draw_joints = bot_gtk_param_widget_get_bool(self->pw, DRAW_JOINT_POSITION);
    if(draw_goal){
        draw_axis(&self->goal_position, 0.3, 4, .7);             
    }

    if(draw_joints){
        double joint_values[self->num_joints];
        for (int i=0; i<self->num_joints; i++) {
            joint_values[i] = bot_gtk_param_widget_get_double (self->pw, self->joint_names[i]);
        }
        
        double angles_rad[self->num_joints];
        for (int i=0; i<self->num_joints; i++) 
            angles_rad[i] = bot_to_radians (joint_values[i]);
            
        int status = 0, no_joints = 0;
        BotTrans *joint_pos = self->forwardK->getJointPos(angles_rad, &status, &no_joints);
        
        for(int i=0; i < no_joints; i++){
            BotTrans joint_to_local = get_end_effector_base_to_local(self, &joint_pos[i]);
            draw_axis(&joint_to_local, 0.1, 2, .7);  
        }
        glLineWidth(5);
        glBegin (GL_LINES);
        for(int i=0; i < no_joints-1; i++){
            BotTrans joint_to_local_c = get_end_effector_base_to_local(self, &joint_pos[i]);
            BotTrans joint_to_local_n = get_end_effector_base_to_local(self, &joint_pos[i+1]);
            glVertex3f (joint_to_local_c.trans_vec[0],
                        joint_to_local_c.trans_vec[1],
                        joint_to_local_c.trans_vec[2]);
            glVertex3f (joint_to_local_n.trans_vec[0],
                        joint_to_local_n.trans_vec[1],
                        joint_to_local_n.trans_vec[2]);
        }
        glEnd ();    
        delete joint_pos;
    }
    //should render the proposed arm positions
}

static void
robot_commands_free (BotRenderer *renderer) 
{
    RendererRobotArmCommands *self = (RendererRobotArmCommands*) renderer;
    
    if (self)
        free (self);
    
}

BotRenderer *renderer_robot_arm_interface_new (BotViewer *viewer, lcm_t *lcm, BotParam *param)
{
    RendererRobotArmCommands *self = 
        (RendererRobotArmCommands*) calloc (1, sizeof (RendererRobotArmCommands));
    self->viewer = viewer;
    self->renderer.draw = robot_commands_draw;
    self->renderer.destroy = robot_commands_free;
    self->renderer.name = RENDERER_NAME;
    self->renderer.user = self;
    self->renderer.enabled = 1;
    self->goal_count = 0;

    BotEventHandler *ehandler = &self->ehandler;
    ehandler->name = (char*) RENDERER_NAME;
    ehandler->enabled = 1;
    ehandler->pick_query = NULL;
    ehandler->key_press = NULL;
    ehandler->hover_query = NULL;
    ehandler->mouse_press = NULL; //mouse_press;
    ehandler->mouse_release = NULL;
    ehandler->mouse_motion = NULL;
    ehandler->user = self;

    bot_viewer_add_event_handler(viewer, &self->ehandler, 1);

    self->renderer.widget = gtk_alignment_new (0, 0.5, 1.0, 0);

    self->lcm = lcm;

    self->forwardK = new ForwardK();
    //BotParam *param = bot_param_get_global (self->lcm, 0);
    
    self->frames = bot_frames_get_global (self->lcm, param);

    self->fk = fk_new();
    //self->cc = new ArmCollisionCheck (TRUE, TRUE);

    self->goal_position.trans_vec[0] = 0;
    self->goal_position.trans_vec[1] = 0;
    self->goal_position.trans_vec[1] = 0;

    double rpy[3] = {0};
    bot_roll_pitch_yaw_to_quat(rpy, self->goal_position.rot_quat);    
    
    self->pw = BOT_GTK_PARAM_WIDGET (bot_gtk_param_widget_new ());
    gtk_container_add (GTK_CONTAINER (self->renderer.widget), 
                       GTK_WIDGET(self->pw));
    gtk_widget_show (GTK_WIDGET (self->pw));


      

    double min_deg, max_deg, min_ticks, max_ticks, offset, deg_to_ticks;
    int sign;
    char key[1024];
    char **joint_names = bot_param_get_subkeys (param, JOINT_PREFIX);

    for (int i=0; joint_names[i]; i++) 
        self->num_joints++;

    self->joint_names = (char **) calloc (self->num_joints, sizeof (char *));
    self->num_servos_per_joint = (int *) calloc (self->num_joints, sizeof (int));

    for (int i=0; joint_names[i]; i++) {

        self->joint_names[i] = strdup (joint_names[i]);
        
        sprintf (key, "%s.%s.servos.servo_1.min_range", JOINT_PREFIX, joint_names[i]);
        bot_param_get_double (param, key, &min_ticks);
        
        sprintf (key, "%s.%s.servos.servo_1.max_range", JOINT_PREFIX, joint_names[i]);
        bot_param_get_double (param, key, &max_ticks);
        
        sprintf (key, "%s.%s.servos.servo_1.offset", JOINT_PREFIX, joint_names[i]);
        bot_param_get_double (param, key, &offset);
        
        sprintf (key, "%s.%s.servos.deg_to_ticks", JOINT_PREFIX, joint_names[i]);
        bot_param_get_double (param, key, &deg_to_ticks);

        sprintf (key, "%s.%s.servos.servo_1.sign", JOINT_PREFIX, joint_names[i]);
        bot_param_get_int (param, key, &sign);

        sprintf (key, "%s.%s.servos", JOINT_PREFIX, joint_names[i]);
        char **servo_names = bot_param_get_subkeys (param, key);
        for (int j=0; servo_names[j]; j++) {
            self->num_servos_per_joint[i]++;
            self->num_servos++;
        }

        min_deg = sign*(min_ticks - offset)/deg_to_ticks;
        max_deg = sign*(max_ticks - offset)/deg_to_ticks;

        bot_gtk_param_widget_add_double(self->pw, joint_names[i], 
                                    BOT_GTK_PARAM_WIDGET_SLIDER, bot_min(min_deg, max_deg), bot_max(min_deg, max_deg), 0.1, 0);
    
        char enable_str[1024];
        sprintf (enable_str, "%s-enable", self->joint_names[i]);//joint_names[i]);
        char *str = strdup(enable_str);
        fprintf (stdout, "Adding %s\n", enable_str);
        bot_gtk_param_widget_add_booleans(self->pw, BOT_GTK_PARAM_WIDGET_CHECKBOX,  str, 1, NULL);
    }

    //{"CW Angle Limit", "CCW Angle Limit", "Highest Limit Temperature", "Lowest Limit Voltage", "Highest Limit Voltage", "Max torque", "Status return level", "Alarm LED", "Alarm Shutdown","Torque Enable","LED","CW Compliance Margin", "CCW Compliance Margin", "CW Compliance Slope", "CCW Compliance Slope", "Goal Position", "Moving Speed", "Torque Limit"};
    char **add_name = (char **) calloc(18, sizeof(char *));
    for (int i=0; i<18; i++){
        add_name[i] = (char *) calloc(30, sizeof(char));
    }

    bot_gtk_param_widget_add_enum(self->pw, ADDRESS_VAL, BOT_GTK_PARAM_WIDGET_MENU, 6, "CW Angle Limit", 6, "CCW Angle Limit", 8, "Highest Limit Temperature", 11, "Lowest Limit Voltage", 12, "Highest Limit Voltage", 13, "Max torque", 14, "Status return level", 16, "Alarm LED", 17, "Alarm Shutdown", 18, "Torque Enable", 24, "LED", 25, "CW Compliance Margin", 26, "CCW Compliance Margin", 27, "CW Compliance Slope", 28, "CCW Compliance Slope", 29, "Goal Position", 30, "Moving Speed", 32, "Torque Limit", 34, NULL);

    bot_gtk_param_widget_add_int(self->pw, SET_ADDRESS_VAL, BOT_GTK_PARAM_WIDGET_SLIDER, 0, 1023, 1, 0);
    
    g_signal_connect (G_OBJECT (self->pw), "changed", 
                      G_CALLBACK (on_param_widget_changed), self);
    
    //bot_gtk_param_widget_add_booleans(self->pw, 0, SEND_ARM_COMMAND , 0, NULL);

    bot_gtk_param_widget_add_booleans(self->pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, DRAW_GOAL_POSITION , 0, NULL);
    bot_gtk_param_widget_add_booleans(self->pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, DRAW_JOINT_POSITION , 1, NULL);

    bot_gtk_param_widget_add_buttons(self->pw, SEND_ARM_COMMAND , NULL);
    bot_gtk_param_widget_add_buttons(self->pw, SEND_HOME_COMMAND, NULL);
    bot_gtk_param_widget_add_buttons(self->pw, SEND_REST_COMMAND, NULL);
    bot_gtk_param_widget_add_buttons(self->pw, SEND_DISABLE_TORQUE_COMMAND, NULL);

    bot_gtk_param_widget_add_int(self->pw, ITERATIONS, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 1,100000 , 100, 5000);
    bot_gtk_param_widget_add_int(self->pw, ITERATIONS, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 0,200 , 1, 10);

    bot_gtk_param_widget_add_buttons(self->pw, SEND_RRT_JOINT_COMMAND, NULL);
    bot_gtk_param_widget_add_buttons(self->pw, SEND_RRT_COMMAND, NULL);

    g_signal_connect (G_OBJECT (self->pw), "changed", 
                      G_CALLBACK (on_address_vals_changed), self);

    bot_gtk_param_widget_add_buttons(self->pw, SEND_ADDRESS_VALS, NULL);

    
    return &self->renderer;
}

void setup_renderer_arm_interface (BotViewer *viewer, int priority, lcm_t *lcm, BotParam *param)
{
    BotRenderer *renderer = renderer_robot_arm_interface_new (viewer, lcm, param);
    bot_viewer_add_renderer (viewer, renderer, priority);
}

