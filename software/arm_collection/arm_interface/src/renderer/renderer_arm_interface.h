#ifndef RENDERER_ARM_INTERFACE_H_
#define RENDERER_ARM_INTERFACE_H_

#include <bot_vis/bot_vis.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>



#ifdef __cplusplus
extern "C" {
#endif
    
void setup_renderer_arm_interface (BotViewer* viewer, int priority, lcm_t *lcm, BotParam *param);
    
#ifdef __cplusplus
}
#endif

#endif
