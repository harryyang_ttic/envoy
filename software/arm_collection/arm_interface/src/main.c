// file: main.c
//


#include <stdio.h>
#include <inttypes.h>
#include <glib.h>
#include <unistd.h>

#include <getopt.h>

#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif

#include <dynamixel/dynamixel.h>
#include <lcmtypes/dynamixel_cmd_list_t.h>
#include <lcmtypes/dynamixel_cmd_address_vals_t.h>
#include <lcmtypes/dynamixel_status_list_t.h>
#include <lcmtypes/arm_cmd_joint_t.h>
#include <lcmtypes/arm_cmd_joint_list_t.h>
#include <lcmtypes/arm_cmd_joint_status_list_t.h>
#include <lcmtypes/arm_cmd_circle_trace_t.h>

#include <bot_param/param_client.h>

#include <arm_interface/arm_interface.h>

#define JOINT_PREFIX "arm_config.joints"
#define TICKS_TO_DEG 3.4133333333333336
#define MAX_TICK_DELTA 2000//20
#define CURRENT_POSITION_ADDRESS 36
//#define NUM_SERVOS 6

//// Joint ID's -- Base: 0, Shoulder: 1, Elbow: 2, Wrist: 3, Gripper: 4

#define ARM_COMMAND_CHANNEL "ARM_COMMAND"

typedef struct _state_t state_t;

typedef struct {
    int ticks;
    int64_t utime;
} servo_pos_t;


struct _state_t {
    lcm_t *lcm;

    int exit_flag;
    int adjust;
    int verbose;
    int simulation; 
    //we need to use a hash table 
    GHashTable *h_servos;

    GHashTable *h_joints;

    //joint list data structure 
    //this should be filled from param file and used to do everything
    joint_list_t jlist; 
    //might be useful to generate a servo to parent mapping 


    int num_joints;
    int num_servos;
    char** joint_names;

    GMainLoop *mainloop;
    guint timer_id_servo;

    //this is to prevent being affected by large jumps 
    servo_pos_t *last_valid_ticks;

    dynamixel_status_list_t* latest_servo_states;
    dynamixel_cmd_list_t *last_dynamixel_cmd;

    int center_pos;
    int cmd_list_id;

};

//Returns degree value using servo specific offsets and conversion values
double convert_degrees_from_ticks(int ticks, servo_t *s){
    //fprintf(stderr, "Sign : %d Deg to ticks : %f Offset : %d\n", s->sign, s->parent->deg_to_tick, s->offset);
    return s->sign * ((float) (ticks - s->offset))/TICKS_TO_DEG; ///(s->parent->deg_to_tick);
}


//Converts degrees into ticks and confirms that goal position is within the servo bounds
int compute_goal_pos(double degrees, servo_t *s){

    int goal_pos =  s->sign * degrees* s->parent->deg_to_tick + s->offset; 

    goal_pos = bot_clamp(goal_pos, s->min_tick, s->max_tick);
    
    //fprintf(stderr, "[%d] Degrees : %f - Ticks : %d - Min %d Max %d\n", s->servo_id, degrees, goal_pos, s->min_tick, s->max_tick);

    return goal_pos;
}


void set_compliance_slope(state_t *self)
{

    for(int j=0; j<2; j++){
        dynamixel_cmd_address_vals_t msg = {
            .nservos = 7,
            .address = 28+j
        };
    
        msg.servo_id = (int32_t *)calloc(msg.nservos, sizeof(int32_t));
        msg.value = (int32_t *)calloc(msg.nservos, sizeof(int32_t));


        for(int i=0; i<msg.nservos; i++){
            msg.servo_id[i] = i;
            msg.value[i] = 5;
        }
    
        if (self->verbose)
            printf("publishing");
        
        dynamixel_cmd_address_vals_t_publish(self->lcm, "ARM_DYNAMIXEL_COMMAND_ADDRESSES", &msg);

        free(msg.servo_id);
        free(msg.value);
    }
}

void set_address_value(int address, int value, state_t *self){
    dynamixel_cmd_address_vals_t msg = {
        .nservos = self->num_servos,
        .address = address,
    };
  
    msg.servo_id = (int32_t *)calloc(msg.nservos, sizeof(int32_t));
    msg.value = (int32_t *)calloc(msg.nservos, sizeof(int32_t));


    for(int i=0; i<msg.nservos; i++){
        msg.servo_id[i] = i;
        msg.value[i] = value;
    }

    for(int i = 0; i < 5; i++){
        dynamixel_cmd_address_vals_t_publish(self->lcm, "ARM_DYNAMIXEL_COMMAND_ADDRESSES", &msg);
        usleep(100000);
    }

    free(msg.servo_id);
    free(msg.value);
}

/*
 * command_handler
 *
 * Description: called when new Joint List LCM command data comes in. Converts Joint List into servo commands and publishes to DYNAMIXEL_COMMAND
 *
 */

//update this to convert to the proper servo's
void command_handler (const lcm_recv_buf_t * rbuf, const char * channel_name, const arm_cmd_joint_list_t*  msg, void * user){
    state_t * self = (state_t *) user;

    //convert arm_cmd_joint_list_t into dynamixel servo commands

    int offsets[7] = {0, 4, -4, -4, 5, 0, 0}; //6
   
    int nServoCommands = 0;
    for (int i = 0; i < msg->ncommands; i++){
        int id = msg->commands[i].joint_index;
        joint_t *joint = (joint_t*) g_hash_table_lookup(self->h_joints, &id);
        if(self->verbose){
            fprintf(stderr, " Joint Ind : %d - %p\n", msg->commands[i].joint_index, (void *) joint);
        }
        if(joint)
            nServoCommands += joint->no_servos;
    }

    if (self->verbose)
        fprintf(stderr, "No servos in the command : %d\n", nServoCommands);
    dynamixel_cmd_t joint_commands[nServoCommands];

    int cmd_counter = 0;//counter for index of joint_commands
    for(int i = 0; i < msg->ncommands; i++){
        int id = msg->commands[i].joint_index;
        joint_t *joint = (joint_t*) g_hash_table_lookup(self->h_joints, &id);
        if(!joint){
            fprintf(stderr, "No joint found : %d\n", id);
            continue;
        }
        if(self->verbose){
            fprintf(stderr, "Joint found in config : %d - Num servos : %d\n", id, joint->no_servos);
        }
            
        for(int j = 0; j < joint->no_servos; j++){
            dynamixel_cmd_t c1 = {
                .servo_id = joint->servos[j].servo_id, 
                .torque_enable = msg->commands[i].torque_enable,
                .goal_position = compute_goal_pos(msg->commands[i].goal_angle, &joint->servos[j]),
                .max_speed = msg->commands[i].max_speed,// * abs(self->deg_to_ticks_const[self->shoulder_servo1_index]),
                .max_torque = msg->commands[i].max_torque,
            };
            
            //Append joint_commands with two shoulder servo commands
            joint_commands[cmd_counter] = c1;
            cmd_counter++;
        }

        if(msg->commands[i].joint_index == 1 && msg->commands[i].goal_angle > 90){
            offsets[1] = 1;
            offsets[2] = 1;
        }
    }


    //dynamixel cmd_list
    dynamixel_cmd_list_t c_list = {
        .utime = bot_timestamp_now(),
        .ncommands = nServoCommands,
        .commands = joint_commands,
    };

    self->last_dynamixel_cmd = dynamixel_cmd_list_t_copy(&c_list);

    if (self->adjust | !self->simulation)            
    for(int i = 0; i < c_list.ncommands; i++){
        c_list.commands[i].goal_position += offsets[c_list.commands[i].servo_id];
        if (self->verbose)
            fprintf(stdout, "servo %d Offset: %d\n", i, offsets[i]);
    }
    

    if (dynamixel_cmd_list_t_publish(self->lcm, "ARM_DYNAMIXEL_COMMAND", &c_list) != 0) {
        fprintf(stderr, "arm command publish error \n");
    }
    //else{
    //  if (dynamixel_cmd_list_t_publish(self->lcm, "DYNAMIXEL_COMMAND_SIMULATION", &c_list) != 0) {
    //      fprintf(stderr, "arm command publish error \n");
    //  }
    //}
    
}


/* move_arm
 *
 * Description: Debugging function to generate LCM joint_lists. 
 * Needs to be moved to seperate file. 
 */
static void move_arm(const lcm_recv_buf_t *rbuf, void * user){
    //Individual Joint Movements
    state_t * self = (state_t *) user;
    arm_cmd_joint_t j_base1 = {
	    .joint_index = 0,
	    .torque_enable = 1,
	    .goal_angle = 50.0,
	    .max_speed = 30,
	    .max_torque = 250,
    };
    arm_cmd_joint_t j_elbow1 = {
	    .joint_index = 2,
	    .torque_enable = 1,
	    .goal_angle = 45.0,
	    .max_speed = 30,
	    .max_torque = 983,
    };
    arm_cmd_joint_t j_shldr1 = {
	    .joint_index = 1,
	    .torque_enable = 1,
	    .goal_angle = 0.0,
	    .max_speed = 30,
	    .max_torque = 983,
    };
    arm_cmd_joint_t j_wrist1 = {
	    .joint_index = 3,
	    .torque_enable = 1,
	    .goal_angle = 30.0,
	    .max_speed = 30,
	    .max_torque = 983,
    };
    arm_cmd_joint_t j_gripper1 = {
	    .joint_index = 4,
	    .torque_enable = 1,
	    .goal_angle = -10.0,
	    .max_speed = 30,
	    .max_torque = 983,
    };

    arm_cmd_joint_t j_base2 = {
	    .joint_index = 0,
	    .torque_enable = 1,
	    .goal_angle = -50.0,
	    .max_speed = 30,
	    .max_torque = 250,
    };
    arm_cmd_joint_t j_elbow2 = {
	    .joint_index = 2,
	    .torque_enable = 1,
	    .goal_angle = 120.0,
	    .max_speed = 30,
	    .max_torque = 983,
    };
    arm_cmd_joint_t j_shldr2 = {
	    .joint_index = 1,
	    .torque_enable = 1,
	    .goal_angle = -100.0,
	    .max_speed = 30,
	    .max_torque = 983,
    };
    arm_cmd_joint_t j_wrist2 = {
	    .joint_index = 3,
	    .torque_enable = 1,
	    .goal_angle = 0.0,
	    .max_speed = 30,
	    .max_torque = 983,
    };
    arm_cmd_joint_t j_gripper2 = {
	    .joint_index = 4,
	    .torque_enable = 1,
	    .goal_angle = 10.0,
	    .max_speed = 30,
	    .max_torque = 983,
    };
    arm_cmd_joint_t joint_commands1[] = {j_wrist1, j_base1,  j_gripper1};
    arm_cmd_joint_t joint_commands2[] = {j_base2, j_shldr2, j_elbow2, j_wrist2, j_gripper2};
    arm_cmd_joint_t joint_commands3[] = {j_base1};
    arm_cmd_joint_list_t j_list = {
        .ncommands = 5,
        .commands = joint_commands1,
    };
    //select joint_command based on cmd_list_id (default value is 0)
    if (self->cmd_list_id == 0){    
        arm_cmd_joint_list_t j_list = {
	        .ncommands = 3,
	        .commands = joint_commands1,
        };
        command_handler(rbuf, ARM_COMMAND_CHANNEL, &j_list, user); //send arm_cmd_joint_list to command_handler
    }
    else if (self->cmd_list_id == 1){    
        arm_cmd_joint_list_t j_list = {
	        .ncommands = 5,
	        .commands = joint_commands2,
        };
        command_handler(rbuf, ARM_COMMAND_CHANNEL, &j_list, user); //send arm_cmd_joint_list to command_handler
    }
    else if (self->cmd_list_id == 2){    
        arm_cmd_joint_list_t j_list = {
	        .ncommands = 1,
	        .commands = joint_commands3,
        };
        command_handler(rbuf, ARM_COMMAND_CHANNEL, &j_list, user); //send arm_cmd_joint_list to command_handler
    }
    else {
        fprintf(stderr, "cmd_list_id is invalid!\n");
    }
    
        
}


//80 Hz
static void on_servo(const lcm_recv_buf_t *rbuf, const char * channel, const dynamixel_status_list_t * msg, void * user){
    state_t *self = (state_t*) user;

    self->latest_servo_states = dynamixel_status_list_t_copy(msg); 

    int curr_address_ind = -1;
    for(int i = 0; i < msg->nservos; i++){
        for(int j=0; j < msg->servos[i].num_values; j++){
            if(msg->servos[i].addresses[j] == CURRENT_POSITION_ADDRESS){
                curr_address_ind = j;
                break;
            }
        }
        if(curr_address_ind>=0)
            break;
    }

    if(curr_address_ind == -1){
        fprintf(stderr, "Could not find the address in the index - returning\n");
        return;
    }

    if(self->last_valid_ticks==NULL){
        //first time - copy for everyone
        self->last_valid_ticks= (servo_pos_t *) calloc(self->num_servos, sizeof(servo_pos_t));
        for(int i=0; i < self->num_servos; i++){                
            self->last_valid_ticks[i].utime = 0;
            self->last_valid_ticks[i].ticks = -1;
        }
    }

    for(int i = 0; i < msg->nservos; i++){
        if(curr_address_ind >= msg->servos[i].num_values)
            continue;
        int cur_ticks = msg->servos[i].values[curr_address_ind];

        if(fabs(self->last_valid_ticks[msg->servos[i].servo_id].utime - msg->utime) > 1.0e6 *5){
            //self->last_valid_ticks[msg->servos[i].servo_id] == -1){
            self->last_valid_ticks[msg->servos[i].servo_id].utime = msg->utime;
            self->last_valid_ticks[msg->servos[i].servo_id].ticks = cur_ticks;
        }
        else{
            int delta = fabs(cur_ticks - self->last_valid_ticks[msg->servos[i].servo_id].ticks);
            if(delta <= MAX_TICK_DELTA){
                self->last_valid_ticks[msg->servos[i].servo_id].utime = msg->utime;
                self->last_valid_ticks[msg->servos[i].servo_id].ticks = cur_ticks;
            }
            else{
                fprintf(stderr, "Servo Jumped large amount - using last valid address\n");
            }
        }
    }

    if(self->last_dynamixel_cmd){
        
        //search the index of the address 
        //CURRENT_POSITION_ADDRESS
        

        /*for(int i = 0; i < msg->nservos; i++){
            if(curr_address_ind >= msg->servos[i].num_values)
                continue;
            int cur_ticks = msg->servos[i].values[curr_address_ind];

            if(first_time){
                self->last_valid_tick[msg->servos[i].servo_id] = cur_ticks;
            }
            else{

            }*/
            
        for(int j=0; j < self->last_dynamixel_cmd->ncommands; j++){
            dynamixel_cmd_t *cmd = &self->last_dynamixel_cmd->commands[j];
            int cur_ticks = self->last_valid_ticks[cmd->servo_id].ticks;
            int goal_ticks = cmd->goal_position;
            double err_angle = abs(cur_ticks-goal_ticks)/3.5;
            if (self->verbose)
                fprintf(stdout, "Servo %d | Goal: %d | Current: %d | Error (deg): %f\n", cmd->servo_id,
                        goal_ticks, cur_ticks, err_angle);
        }
        
        if (self->verbose)
            fprintf(stdout, "\n");
    }

    if(msg->nservos == 0){
        fprintf(stderr, "No servos in the list - returning\n");
        return;
    }
    
    if(self->verbose)
        printf("\n ---Arm Debug Print---\n");
    
    //update botframes for arm

    double degrees;

    arm_cmd_joint_status_list_t joint_msg;
    joint_msg.joints = (arm_cmd_joint_status_t *) 
        calloc(self->num_joints, sizeof(arm_cmd_joint_status_t));
    joint_msg.njoints = self->num_joints;
    joint_msg.utime =  bot_timestamp_now();

    bot_core_rigid_transform_t cur_trans;
    cur_trans.utime = msg->utime;

    //search and find the correct 
    int curr_pos_index = 1;

    for(int i = 0; i < msg -> nservos; i++){
	if(msg->servos[i].num_values == 0 || curr_pos_index > (msg->servos[i].num_values - 1)){
	    fprintf(stderr, "Didnt hear from servo at index : %d\n", i);
	    continue;
	}
        int id = msg->servos[i].servo_id;
      
        servo_t *s = (servo_t*) g_hash_table_lookup(self->h_servos, &id);
      
        if(s==NULL){
            if(self->verbose)
                fprintf(stderr,"No servo found - skipping\n");
            continue;
        }

        if(s->parent->update_channel==NULL){
            fprintf(stderr,"Null Update channel - something wrong with the config - please fix me\n");
        }

        //fprintf(stderr,"Joint Name : %s\n",  s->parent->joint_frame);
            
        //check flag and ignore 
        if(s->use_for_frame == 0)
            continue;

        //TODO: Update to search for correct address value instead of automatically using '1'
        degrees = convert_degrees_from_ticks(msg->servos[i].values[1], s);

        // Add joint angle to joint_update message
        joint_msg.joints[s->parent->joint_id].joint_id = s->parent->joint_id;
        joint_msg.joints[s->parent->joint_id].angle = degrees;
        joint_msg.joints[s->parent->joint_id].utime = joint_msg.utime;

        //get fixed translation
        cur_trans.trans[0] = s->parent->frame_translation[0];//self->base_to_body_trans[0];
        cur_trans.trans[1] = s->parent->frame_translation[1];//self->base_to_body_trans[1];
        cur_trans.trans[2] = s->parent->frame_translation[2];//self->base_to_body_trans[2];

        double q1[4], q2[4];

        bot_roll_pitch_yaw_to_quat(s->parent->frame_rpy, q1);
        
        //only yaw rotation, other rpy values fixed.
        /*for(int j=0; j < 3; j++){
          if(j != s->parent->rpy_update_ind)
          rpy[j] = bot_to_radians(s->parent->frame_rpy[j]);
          else
          rpy[j] = bot_to_radians(degrees);
          }*/

        double rpy[3] = {0};
	
        rpy[s->parent->rpy_update_ind] = bot_to_radians(degrees);
        //bot_roll_pitch_yaw_to_quat (rpy, cur_trans.quat);
        bot_roll_pitch_yaw_to_quat (rpy, q2);
	
        //bot_quat_mult(cur_trans.quat, q2, q1);
        bot_quat_mult(cur_trans.quat, q1, q2);
	
        //publish botframes update
        bot_core_rigid_transform_t_publish (self->lcm, s->parent->update_channel, &cur_trans);
        if(self->verbose)
            fprintf(stdout,"[%d] - Tick : %d Channel : %s Angle: %.2f \n",  i, msg->servos[i].values[1], s->parent->update_channel, degrees); //debug print
    }

    /*printf("njoints: %d", joint_msg.njoints);
      for(int i = 0; i < self->num_joints; i++){
      fprintf(stderr, "Joint id: %d Current angle = %f utime %lu\n", joint_msg.joints[i].joint_id, joint_msg.joints[i].angle, joint_msg.joints[i].utime);
      }*/

    // Publish list of current joint angles
    arm_cmd_joint_status_list_t_publish(self->lcm, "ARM_JOINT_STATUS", &joint_msg);

    free(joint_msg.joints);
    
    //execute arm movements
    //move_arm(rbuf, self);

    //debug print of goal and current positions of all servos
    //if(self->verbose)
    for(int k = 0; k < msg -> nservos; k++){
        //printf("Servo %d -- ", k);
      
        //printf("Goal Pos: %"PRId32" | Current Pos: %"PRId32"\n", msg->servos[k].values[0],msg->servos[k].values[1]);
    }

    return;
}

static gboolean on_servo_timeout (gpointer data) {
    state_t *self = (state_t*) data;
    //printf("TIMEOUT -- no servo status received\n");
    return TRUE;
}

static void usage() {
    fprintf (stdout, "usage: arm_command [options]\n"
             "\n"
             "  -h, --help              Shows this help text and exits\n"
             "  -c, --center_pos        Specify center offset for all joints\n"
             "  -s, --simulation        Simulation mode\n"
             "  -i, --cmd_list_id       Specify which joint command list to use\n"
             "  -v, --verbose           Verbose output\n\n"
             );
    exit(1);
}
int main(int argc, char ** argv) {

    state_t *self = (state_t*) calloc(1, sizeof(state_t));

    //Default settings
    self->center_pos = 0;
    self->adjust = 1;
    self->cmd_list_id = 0; //cmd_list_id specifies which arm_cmd_list to use in move_arm (for debugging!)
    
    self->lcm = bot_lcm_get_global (NULL);
    if(!self->lcm) {
        fprintf (stderr, "Error getting LCM\n");
        goto failed;
    }
    bot_glib_mainloop_attach_lcm(self->lcm);

    //Are usage options correctly implemented????
    char *optstring = "hc:d:si:vn";
    struct option long_opts[] = {
        {"help", no_argument, 0, 'h'},
        {"no_adjust", no_argument, 0, 'n'},
        {"verbose", no_argument, 0, 'v'},
        {0, 0}
    };	

    char c;
    int override_failsafe = 0;
    while ((c = getopt_long (argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
        case 's':
            self->simulation = 1;
            break;
        case 'v':
            self->verbose = 1;
            break;
        case 'n':
            self->adjust = 0;
            break;
        case 'h':
        default:
            usage();
        }
    }
    
    // Get BotFrames instance
    BotParam * param = bot_param_new_from_server(self->lcm, 0);
    if (!param) {
        fprintf (stderr, "Error getting BotParam. Are you running the server?\n");
        goto failed;
    }

    //get information from param server
    char key[1024]; 
    sprintf(key,"%s.num_servos", JOINT_PREFIX);

    bot_param_get_int(param, key, &self->num_servos);

    self->joint_names = bot_param_get_subkeys(param, JOINT_PREFIX);

    self->num_joints = 0;
    if (self->joint_names) {
        for (int pind = 0; self->joint_names[pind] != NULL; pind++) {
            fprintf(stderr, "Joint Name : %s\n", self->joint_names[pind]);
            self->num_joints++;
        }
    }

    self->jlist.no_joints = self->num_joints; 
    self->jlist.joints = (joint_t *) calloc(self->jlist.no_joints, sizeof(joint_t));

    self->h_servos = g_hash_table_new(g_int_hash, g_int_equal);
    self->h_joints = g_hash_table_new(g_int_hash, g_int_equal);

    
    //incrementally fill them 
    for(int i=0; i< self->num_joints; i++){
        self->jlist.joints[i].joint_name = strdup(self->joint_names[i]);
        
        sprintf(key,"%s.%s.joint_frame", JOINT_PREFIX, self->joint_names[i]);
       
        char *joint_frame;
        if (0 != bot_param_get_str(param, key, &self->jlist.joints[i].joint_frame)){
            fprintf(stderr, "%s - frame not defined\n", key);
            return -1;
        }
        sprintf(key,"%s.%s.relative_to", JOINT_PREFIX, self->joint_names[i]);
        char *relative_to;
        if (0 != bot_param_get_str(param, key, &self->jlist.joints[i].relative_to)){
            fprintf(stderr, "%s - relative to not defined\n", key);
            return -1;
        }

        sprintf(key,"%s.%s.variable_ind", JOINT_PREFIX, self->joint_names[i]);
        bot_param_get_int(param, key, &self->jlist.joints[i].rpy_update_ind);

        

        sprintf(key,"%s.%s.joint_index", JOINT_PREFIX, self->joint_names[i]);
        bot_param_get_int(param, key, &self->jlist.joints[i].joint_id);

        fprintf(stderr,"Joint Index : %d\n", self->jlist.joints[i].joint_id);
        
        fprintf(stderr, "Variable Ind : %d\n", self->jlist.joints[i].rpy_update_ind);
        
        fprintf(stderr, "Frame : %s\n", self->jlist.joints[i].joint_frame);

        sprintf(key,"coordinate_frames.%s.initial_transform.translation", self->jlist.joints[i].joint_frame);
        //get fixed translation values for each arm segment
        bot_param_get_double_array (param, key, (self->jlist.joints[i].frame_translation), 3);

        sprintf(key,"coordinate_frames.%s.initial_transform.rpy", self->jlist.joints[i].joint_frame);
        //get fixed translation values for each arm segment
        bot_param_get_double_array (param, key, (self->jlist.joints[i].frame_rpy), 3);

        fprintf(stderr, "Initial translation : %f,%f,%f\n", self->jlist.joints[i].frame_translation[0],            
                self->jlist.joints[i].frame_translation[1], self->jlist.joints[i].frame_translation[2]);

        fprintf(stderr, "Initial rpy : %f,%f,%f\n", self->jlist.joints[i].frame_rpy[0],            
                self->jlist.joints[i].frame_rpy[1],            
                self->jlist.joints[i].frame_rpy[2]);  

        self->jlist.joints[i].frame_rpy[0] = bot_to_radians(self->jlist.joints[i].frame_rpy[0]);
        self->jlist.joints[i].frame_rpy[1] = bot_to_radians(self->jlist.joints[i].frame_rpy[1]);
        self->jlist.joints[i].frame_rpy[2] = bot_to_radians(self->jlist.joints[i].frame_rpy[2]);

        sprintf(key,"coordinate_frames.%s.update_channel", self->jlist.joints[i].joint_frame);

        if (0 != bot_param_get_str(param, key, &self->jlist.joints[i].update_channel)){
            fprintf(stderr, "%s - frame not defined\n", key);
            //return -1;
            self->jlist.joints[i].update_channel = NULL;
        }
        
        fprintf(stderr, "Update Channel : %s\n", self->jlist.joints[i].update_channel);
        
        sprintf(key,"%s.%s.servos", JOINT_PREFIX, self->joint_names[i]);

        char **servos = bot_param_get_subkeys(param, key);

        int s_count = 0;

        if (servos) {
            for (int pind = 0; servos[pind] != NULL; pind++) {
                fprintf(stderr, "Servo Name : %s\n", servos[pind]);
                s_count++;
            }
        }

        sprintf(key,"%s.%s.deg_to_ticks", JOINT_PREFIX, self->joint_names[i]);

        bot_param_get_double(param, key, &self->jlist.joints[i].deg_to_tick);
        
        fprintf(stderr, "Deg to ticks : %f\n", self->jlist.joints[i].deg_to_tick);
        self->jlist.joints[i].no_servos = s_count;
        self->jlist.joints[i].servos = (servo_t *) calloc(s_count , sizeof (servo_t));
        
        fprintf(stderr, "No of servos : %d\n", s_count);

        char servo_sub[1024];

        for(int j=0; j < s_count; j++){
            sprintf(servo_sub,"%s.%s.servos.%s", JOINT_PREFIX, self->joint_names[i], servos[j]);

            sprintf(key,"%s.servo_index", servo_sub);
            bot_param_get_int(param, key, &(self->jlist.joints[i].servos[j].servo_id));

            sprintf(key,"%s.min_range", servo_sub);
            bot_param_get_int(param, key, &self->jlist.joints[i].servos[j].min_tick);
            
            sprintf(key,"%s.max_range", servo_sub);
            bot_param_get_int(param, key, &self->jlist.joints[i].servos[j].max_tick);
            

            sprintf(key,"%s.offset", servo_sub);
            bot_param_get_int(param, key, &self->jlist.joints[i].servos[j].offset);

            sprintf(key,"%s.sign", servo_sub);
            bot_param_get_int(param, key, &self->jlist.joints[i].servos[j].sign);

            sprintf(key,"%s.use_for_frame", servo_sub);
            bot_param_get_int(param, key, &self->jlist.joints[i].servos[j].use_for_frame);

            fprintf(stderr, "\t %s - %d [%d-%d] : %d\n", 
                    servos[j], self->jlist.joints[i].servos[j].servo_id, 
                    self->jlist.joints[i].servos[j].min_tick, 
                    self->jlist.joints[i].servos[j].max_tick,
                    self->jlist.joints[i].servos[j].offset);

            self->jlist.joints[i].servos[j].parent = &self->jlist.joints[i];

            g_hash_table_insert(self->h_servos, &self->jlist.joints[i].servos[j].servo_id, &self->jlist.joints[i].servos[j]);
        }
        if (servos) {
            for (int pind = 0; servos[pind] != NULL; pind++) {
                free(servos[pind]);
            }
            free(servos);
        }
        g_hash_table_insert(self->h_joints, &self->jlist.joints[i].joint_id, 
                            &self->jlist.joints[i]);
    }

    fprintf(stderr, "Num of servos : %d\n", self->num_servos);
    
    for(int i=0; i < self->num_servos; i++){
        servo_t *s = (servo_t*) g_hash_table_lookup(self->h_servos, &i);
        
        fprintf(stderr, "Servo id : %d Joint : %s\n", 
                s->servo_id, 
                s->parent->joint_name);
    }

    for(int i=0; i < self->num_joints; i++){
        joint_t *s = (joint_t*) g_hash_table_lookup(self->h_joints, &i);
        
        fprintf(stderr, "\tJoint : %s\n", 
                s->joint_name);
    }

    self->last_valid_ticks = NULL; //
    
    set_address_value(28, 16, self);
    set_address_value(29, 16, self);

    dynamixel_status_list_t_subscribe(self->lcm, "ARM_DYNAMIXEL_STATUS", &on_servo, self);
    //}
    //else{
    //dynamixel_status_list_t_subscribe(self->lcm, "DYNAMIXEL_STATUS_SIMULATION", &on_servo, self);
    //}

    arm_cmd_joint_list_t_subscribe(self->lcm, "ARM_JOINT_COMMANDS", &command_handler, self);
    //arm_cmd_joint_list_t_subscribe(self->lcm, "ARM_JOINT_IK_COMMANDS", &command_handler_render, self);

    printf ("subscribe attempt\n");
    
    //set_compliance_slope(self);
    
    self->mainloop = g_main_loop_new(NULL, FALSE);

    self->timer_id_servo=g_timeout_add(1000,on_servo_timeout,self);

    //execute arm movements
    //move_arm(NULL, self);

 
    g_main_loop_run(self->mainloop);
    return 0;


 failed:
    if (self)
        free(self);
    return -1;
}
