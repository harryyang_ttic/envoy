/*
 * Recieves arm commands from arm_interface and sends back new servo  positions
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <lcm/lcm.h>

#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif

#include <lcm/lcm.h>
#include <lcmtypes/dynamixel_status_list_t.h>
#include <lcmtypes/dynamixel_cmd_list_t.h>

#include <bot_param/param_client.h>

#define JOINT_PREFIX "arm_config.joints"
#define MAX_POS_ERROR 1//1
#define SERVO_VELOCITY 50 //ticks per second

typedef struct _servo_limits_t {
    int min;
    int max;
} servo_limits_t;

typedef struct _state_t state_t;

struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    dynamixel_status_list_t *cur_positions;
    dynamixel_cmd_list_t *cmd_last;
    int at_goal;
    int num_servos;
    servo_limits_t *servo_limits;
};

static gboolean
on_timer (gpointer data){
    state_t *self = (state_t *) data;

    //Check to see if joints are in correct position
    if (!self->cmd_last || self->at_goal) {
        // publish the dynamixel_status message
        self->cur_positions->utime = bot_timestamp_now();
        dynamixel_status_list_t_publish(self->lcm, "ARM_DYNAMIXEL_STATUS", self->cur_positions);
        return TRUE;
    }
  
    /* Otherwise, move towards the goal */

    int64_t new_time = bot_timestamp_now();
    //int64_t distance = ((new_time - self->cur_positions->utime)/1000000)*SERVO_VELOCITY;
    int64_t distance = 2;
    int32_t new_pos;
    // dynamixel_status_t servo_statuses[self->num_servos];
    dynamixel_status_t * servo_statuses = calloc(self->num_servos, sizeof(dynamixel_status_t));
  
    int at_joint_goal[self->num_servos];
  
    // Check each servo each servo if it is within MAX_POS_ERROR of its goal and which direction it needs to move
    for(int j = 0; j < self->num_servos; j++){
        dynamixel_cmd_t *cmd = NULL;
        for(int i=0; i < self->cmd_last->ncommands; i++){
            if(self->cmd_last->commands[i].servo_id == self->cur_positions->servos[j].servo_id){
                cmd = &self->cmd_last->commands[i];
                break;
            }
        }

        if(cmd){
            int servo_id  = self->cur_positions->servos[j].servo_id;
            /*fprintf(stderr, "Ind : %d CUR[%d] Pos %d => CMD [%d] - Goal %d\n", 
                    j, servo_id, self->cur_positions->servos[j].values[1], cmd->servo_id, cmd->goal_position);
            */

            if(abs(cmd->goal_position - self->cur_positions->servos[j].values[1]) > MAX_POS_ERROR){
                at_joint_goal[servo_id] = 0;
                if((cmd->goal_position - self->cur_positions->servos[j].values[1]) >= 0){ 
                    new_pos = self->cur_positions->servos[j].values[1] + fmin(distance, cmd->goal_position - self->cur_positions->servos[j].values[1]);
                }
                else {
                    new_pos = self->cur_positions->servos[j].values[1] + fmax(-distance, cmd->goal_position - self->cur_positions->servos[j].values[1]);
                }
            }
            else{
                new_pos = self->cur_positions->servos[j].values[1];
                at_joint_goal[servo_id] = 1;
            }
            
            /*fprintf(stderr, "Ind : %d - Curr Pos servo ID: %d  Goal ID : %d - Position : %d Cmd Goal : %d\n", j, self->cur_positions->servos[j].servo_id, cmd->servo_id, new_pos,  self->cmd_last->commands[j].goal_position);
             */

            servo_statuses[j].utime = bot_timestamp_now();
            servo_statuses[j].servo_id = self->cur_positions->servos[j].servo_id;
            servo_statuses[j].num_values = 2;
            servo_statuses[j].addresses = calloc(2, sizeof(int16_t));
            servo_statuses[j].addresses[0] = 30;
            servo_statuses[j].addresses[1] = 36;
            servo_statuses[j].values = calloc(2, sizeof(int32_t));
            servo_statuses[j].values[0] = cmd->goal_position;
            servo_statuses[j].values[1] = new_pos;
            //  servo_statuses[j] = status;
        }
        else{
            servo_statuses[j].utime = bot_timestamp_now();
            servo_statuses[j].servo_id = self->cur_positions->servos[j].servo_id;
            servo_statuses[j].num_values = 2;
            servo_statuses[j].addresses = calloc(2, sizeof(int16_t));
            servo_statuses[j].addresses[0] = 30;
            servo_statuses[j].addresses[1] = 36;
            servo_statuses[j].values = calloc(2, sizeof(int32_t));
            servo_statuses[j].values[0] = self->cur_positions->servos[j].values[1];
            servo_statuses[j].values[1] = self->cur_positions->servos[j].values[1];
        }
    }

    // Compile list to publish
    dynamixel_status_list_t status_list = {
        .utime = bot_timestamp_now(),
        .nservos = self->num_servos,
        .servos = servo_statuses,
    };

    // Publish dynamixel_status message
    dynamixel_status_list_t_publish(self->lcm, "ARM_DYNAMIXEL_STATUS", &status_list);

    // Update cur_positions to newly published status
    if(self->cur_positions)
        dynamixel_status_list_t_destroy(self->cur_positions);
  
    self->cur_positions = dynamixel_status_list_t_copy(&status_list);

    // Check to see if you're at the goal (within MAX_POS_ERROR). If you are, set self->at_goal = 1
    self->at_goal = 1;
    for(int k = 0; k < self->num_servos; k++){
        if(at_joint_goal[k] == 0){
            self->at_goal = 0;
        }
    }
    free(servo_statuses);
    return TRUE;
}

static void
on_command(const lcm_recv_buf_t *rbuf, const char * channel, 
           const dynamixel_cmd_list_t * msg, void * user){

    state_t *self = (state_t*) user;

    if (self->cmd_last)
        dynamixel_cmd_list_t_destroy (self->cmd_last);

    self->cmd_last = dynamixel_cmd_list_t_copy (msg);

    //Check that goal positions are within servo ranges//
    for(int i = 0; i < self->cmd_last->ncommands; i++){
        int servo_id = self->cmd_last->commands[i].servo_id;
        
        fprintf(stderr,"Servo %d Goal : %d - Max : %d Min : %d\n",servo_id, self->cmd_last->commands[servo_id].goal_position, 
                self->servo_limits[servo_id].max, self->servo_limits[servo_id].min);

        if(self->cmd_last->commands[servo_id].goal_position < self->servo_limits[servo_id].min){
            self->cmd_last->commands[servo_id].goal_position == self->servo_limits[servo_id].min;
            fprintf(stderr,"Servo %d goal ouside of range. Goal now set to min position in range\n",i);
        }
        if(self->cmd_last->commands[servo_id].goal_position > self->servo_limits[servo_id].max){
            self->cmd_last->commands[servo_id].goal_position == self->servo_limits[servo_id].max;
            fprintf(stderr,"Servo %d goal ouside of range. Goal now set to servo max position in range\n",i);
        }
    }

    for(int i = 0; i < self->num_servos; i++){
        fprintf(stderr, "Servo : %d Goal : %d\n", self->cmd_last->commands[i].servo_id, self->cmd_last->commands[i].goal_position);
    }

    // Set to 1 in the timer once you are within MAX_POS_ERROR of the goal specified in cmd_last
    self->at_goal = 0;
}

int main(int argc, char ** argv){
  
    state_t *self = (state_t *) calloc(1, sizeof(state_t));
    self->at_goal = 1;

    self->lcm = bot_lcm_get_global (NULL);
    //Get BotFrames instance
    BotParam * param = bot_param_new_from_server(self->lcm, 0);
    if (!param) {
        fprintf (stderr, "Error getting BotParam. Are you running the server?\n");
    }
  
    //get information from param server
    char key[1024]; 
    sprintf(key,"%s.num_servos", JOINT_PREFIX);

    bot_param_get_int(param, key, &self->num_servos);

    self->servo_limits = (servo_limits_t *) calloc(self->num_servos, sizeof(servo_limits_t));

    char **joint_names = bot_param_get_subkeys(param, JOINT_PREFIX);

    int num_joints = 0;
    if (joint_names) {
        for (int pind = 0; joint_names[pind] != NULL; pind++) {
            fprintf(stderr, "Joint Name : %s\n", joint_names[pind]);
            num_joints++;
        }
    }

    //incrementally fill them 
    for(int i=0; i< num_joints; i++){
        sprintf(key,"%s.%s.joint_frame", JOINT_PREFIX, joint_names[i]);
       
        sprintf(key,"%s.%s.servos", JOINT_PREFIX, joint_names[i]);

        char **servos = bot_param_get_subkeys(param, key);

        int s_count = 0;

        if (servos) {
            for (int pind = 0; servos[pind] != NULL; pind++) {
                fprintf(stderr, "Servo Name : %s\n", servos[pind]);
                s_count++;
            }
        }

        fprintf(stderr, "Joint : %s No of servos : %d\n", joint_names[i], s_count);

        char servo_sub[1024];

        for(int j=0; j < s_count; j++){
            sprintf(servo_sub,"%s.%s.servos.%s", JOINT_PREFIX, joint_names[i], servos[j]);

            int servo_id = 0;

            sprintf(key,"%s.servo_index", servo_sub);
            bot_param_get_int(param, key, &(servo_id));
            if(servo_id <0 | servo_id >= self->num_servos){
                fprintf(stderr, "Error Servo ID is greater than the num of servos\n");
            }
            
            sprintf(key,"%s.min_range", servo_sub);
            bot_param_get_int(param, key, &self->servo_limits[servo_id].min);

            sprintf(key,"%s.max_range", servo_sub);
            bot_param_get_int(param, key, &self->servo_limits[servo_id].max);
            
            fprintf(stderr, "\t %s - %d [%d-%d]\n", 
                    joint_names[i], servo_id, 
                    self->servo_limits[servo_id].min,
                    self->servo_limits[servo_id].max);
        }

        if (servos) {
            for (int pind = 0; servos[pind] != NULL; pind++) {
                free(servos[pind]);
            }
            free(servos);
        }
    }
    
    if(joint_names)
        for (int pind = 0; joint_names[pind] != NULL; pind++) {
            free(joint_names[pind]);
        }

    // Set initial values for cur_positions
    self->cur_positions = (dynamixel_status_list_t *) calloc(1, sizeof(dynamixel_status_list_t));
    self->cur_positions->nservos = self->num_servos;
    //this is never set to the values  
    self->cur_positions->utime = bot_timestamp_now();
    /* dynamixel_status_t servo_statuses[self->num_servos]; */
    /* for(int i = 0; i<self->num_servos; i++){ */
    /*   int32_t servo_values[2] = {0, 0}; */
    /*   int16_t servo_addresses[2] = {0, 0}; */
    /*   dynamixel_status_t status = { */
    /*     .utime = bot_timestamp_now(), */
    /*     .servo_id = i, */
    /*     .num_values = 2, */
    /*     .addresses = servo_addresses, */
    /*     .values = servo_values, */
    /*   }; */
    /*   servo_statuses[i] = status; */
    /* } */

    dynamixel_status_t * servo_statuses = (dynamixel_status_t *) calloc(self->num_servos, sizeof(dynamixel_status_t));
    for(int i = 0; i < self->num_servos; i++){
        servo_statuses[i].utime = bot_timestamp_now();
        servo_statuses[i].servo_id = i;
        servo_statuses[i].num_values = 2;
        servo_statuses[i].addresses = calloc(2, sizeof(int16_t));
        servo_statuses[i].addresses[0] = 30;
        servo_statuses[i].addresses[1] = 36;
        servo_statuses[i].values = calloc(2, sizeof(int32_t));
        servo_statuses[i].values[0] = (self->servo_limits[i].max - self->servo_limits[i].min)/2;
        servo_statuses[i].values[1] = (self->servo_limits[i].max - self->servo_limits[i].min)/2;
    }

    self->cur_positions->servos = servo_statuses;
  
    bot_glib_mainloop_attach_lcm(self->lcm);

    g_timeout_add(50, on_timer, self);

    dynamixel_cmd_list_t_subscribe(self->lcm, "ARM_DYNAMIXEL_COMMAND", &on_command, self);

    self->mainloop = g_main_loop_new(NULL, FALSE);
    g_main_loop_run(self->mainloop);
  
    free(servo_statuses);

    return 0;
}
