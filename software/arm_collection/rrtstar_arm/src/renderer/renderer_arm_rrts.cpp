#include "renderer_arm_rrts.h"

#include <arm_forward_kinematics/forward_kinematics.h>
#include <arm_forward_kinematics/ForwardK.hpp>

#include <lcmtypes/arm_rrts_state_list_t.h>
#include <lcmtypes/arm_cmd_object_manipulation_cmd_t.h>
#include <lcmtypes/arm_rrts_cmd_t.h>
#include <lcmtypes/arm_rrts_environment_t.h>
#include <lcmtypes/arm_rrts_graph_t.h>
#include <lcmtypes/arm_rrts_render_settings_t.h>
#include <lcmtypes/arm_cmd_joint_status_list_t.h>
#include <lcmtypes/arm_cmd_grasp_plan_t.h>
#include <arm_inverse_kinematics/InverseK.hpp>
//#include <vector>
//#include <cmath>

#define RENDERER_NAME "Arm RRT*"
#define SEND_COMMAND "Solve/send RRT*"

#define GRIPPER_POSITION "Gripper-Position"

#define X_POS "X Position"
#define Y_POS "Y Position"
#define Z_POS "Z Position"
#define ROLL "Roll"
#define PITCH "Pitch"
#define YAW "Yaw"
#define X_TOL "X Tolerance"
#define Y_TOL "Y Tolerance"
#define Z_TOL "Z Tolerance"
#define ROLL_TOL "Roll Tolerance"
#define PITCH_TOL "Pitch Tolerance"
#define YAW_TOL "Yaw Tolerance"
#define DRAW_PRE_PLAN "Draw Pre-Plan"
#define DRAW_PRE_GRASP "Draw Pre-Grasp"
#define DRAW_GOAL_PLAN "Draw Grasp"
#define DRAW_LIFT "Draw Lift"
#define DRAW_GOAL "Draw Goal"
#define DRAW_TREE "Draw Tree"
#define DRAW_SOLUTION "Draw Solution"
#define DRAW_JOINT_POSITION "Draw IK Solution"
#define DETECT_OBJECT "Detect Object"
#define RELEASE_OBJECT "Release Object"
#define PUT_OBJECT_BACK "Put Object Back"
#define DRAW_POSE "Draw Current Pose"
#define CLEAR_GRAPH "Clear Graph"
#define ITERATIONS "Iterations"
#define DRAW_EDGES "Draw Edges"
#define EDGE_OPACITY "Edge Opacity"
#define VIEW_JOINT_SPACE "View Joint Space"
#define VIEW_XYZ_SPACE "View XYZ Space"
#define AXIS_1 "x-axis"
#define AXIS_2 "y-axis"
#define AXIS_3 "z-axis"
#define SEND_RENDER_SETTINGS "Send Render Settings"
#define TREE_HISTORY_STEP "Tree History Step Size"
#define TREE_HISTORY_DRAW "View Tree History"

//using namespace std;

typedef struct _xyz_t {
    double x;
    double y;
    double z;
}xyz_t;

typedef struct _edge_t {
    xyz_t src;
    xyz_t dst;
}edge_t;

typedef struct _tree_t {
    arm_rrts_graph_t *tree;
    xyz_t *vertices;
    edge_t *edges;
}tree_t;

typedef struct _trajectory_t {
    arm_rrts_state_list_t *trajectory;
    xyz_t *vertices;
}trajectory_t;

typedef struct _RendererGraph{
    BotRenderer renderer;
    BotGtkParamWidget *pw;
    BotViewer *viewer;
    BotFrames *frames;
    lcm_t * lcm;
    ForwardKinematics *fk;
    ForwardK *forwardK;
    InverseK *ik;

    arm_cmd_grasp_plan_t *grasp_plan;
    arm_rrts_graph_t *graph_last;
    arm_rrts_environment_t *environment_last;
    arm_rrts_state_list_t *trajectory_last;
    arm_rrts_cmd_t *last_goal;     
    

    double goal_joints[6];
    BotTrans goal_to_local;
    xyz_t goal_pos;
    double goal_orientation[3];
    int have_goal;

    xyz_t *trajectory_last_positions;
    xyz_t goal_last_center;
    xyz_t goal_last_max;
    xyz_t goal_last_min;
    xyz_t operating_last_center;
    xyz_t operating_last_min;
    xyz_t operating_last_max;
    xyz_t *obstacles_last_center;
    xyz_t *obstacles_last_min;
    xyz_t *obstacles_last_max;
    xyz_t root_state_last;
    xyz_t *vertices_last;
    edge_t *edges_last;
    arm_cmd_joint_status_list_t *joints;

    double obstacle_opacity;
    double goal_opacity;
    double edge_opacity;
  
    bool draw_edges;
    bool joint_space;
    bool xyz_space;

    int tree_history_step;
    BotPtrCircular *tree_history;
    BotPtrCircular *trajectory_history;
    int draw_index;

    int x_axis;
    int y_axis;
    int z_axis;

} RendererGraph;

BotTrans get_end_effector_base_to_local(RendererGraph *self, BotTrans *end_effector_to_base){
    BotTrans base_to_local; 
    bot_frames_get_trans(self->frames, "base", "local", &base_to_local);
        
    BotTrans end_effector_to_local; 

    bot_trans_apply_trans_to(&base_to_local, end_effector_to_base, &end_effector_to_local);

    return end_effector_to_local;
}

BotTrans get_end_effector_body_to_local(RendererGraph *self, double xyz[3] , double rpy[3]){
    BotTrans end_effector_to_body; 
    end_effector_to_body.trans_vec[0] = xyz[0];
    end_effector_to_body.trans_vec[1] = xyz[1];
    end_effector_to_body.trans_vec[2] = xyz[2];
        
    bot_roll_pitch_yaw_to_quat(rpy, end_effector_to_body.rot_quat);
        
    BotTrans body_to_local; 
    bot_frames_get_trans(self->frames, "body", "local", &body_to_local);
        
    BotTrans end_effector_to_local; 

    bot_trans_apply_trans_to(&body_to_local, &end_effector_to_body, &end_effector_to_local);

    return end_effector_to_local;
}

int forward_kinematics(RendererGraph *self, double *angles_rad, xyz_t *out, double transform[12]){

    BotTrans end_effector;
    fk_forward_kinematics (self->fk, angles_rad, &end_effector);

    double xyz[3];
    bot_vector_affine_transform_3x4_3d (transform, end_effector.trans_vec, xyz);

    out->x = xyz[0];
    out->y = xyz[1];
    out->z = xyz[2];
    
    return 1;
}

static void tree_destroy(void *user, void *p){
    if(p){
	tree_t *tree = (tree_t *) p;
	arm_rrts_graph_t_destroy(tree->tree);
	free(tree->vertices);
	free(tree->edges);
    }
}

static void trajectory_destroy(void *user, void *p){
    if(p){
	trajectory_t *trajectory = (trajectory_t *) p;
	arm_rrts_state_list_t_destroy(trajectory->trajectory);
	free(trajectory->vertices);
    }
}

void on_status(const lcm_recv_buf_t *rbuf, const char * channel, const arm_cmd_joint_status_list_t * msg, void * user) {
  
    RendererGraph *self = (RendererGraph *) user;

    if(self->joints)
        arm_cmd_joint_status_list_t_destroy(self->joints);

    self->joints = arm_cmd_joint_status_list_t_copy(msg);
}

static void on_grasp_plan (const lcm_recv_buf_t *rbuf, const char *channel, const arm_cmd_grasp_plan_t *msg, void *user){
    
    RendererGraph *self = (RendererGraph *) user;
    if(self->grasp_plan)
        arm_cmd_grasp_plan_t_destroy(self->grasp_plan);
    self->grasp_plan = arm_cmd_grasp_plan_t_copy(msg);
    bot_viewer_request_redraw (self->viewer);
}

static void on_goal (const lcm_recv_buf_t *rbuf, const char *channel,
                     const arm_rrts_cmd_t *msg, void *user) {
    RendererGraph *self = (RendererGraph *) user;
    if(self->last_goal){
        arm_rrts_cmd_t_destroy(self->last_goal);
    }
    self->last_goal = arm_rrts_cmd_t_copy(msg);

    
    self->goal_to_local.trans_vec[0] = msg->xyz[0];
    self->goal_to_local.trans_vec[1] = msg->xyz[1];
    self->goal_to_local.trans_vec[2] = msg->xyz[2];

    bot_roll_pitch_yaw_to_quat(msg->ef_rpy, self->goal_to_local.rot_quat);

    BotTrans local_to_base; 
    bot_frames_get_trans(self->frames, "local", "base", &local_to_base);
    
    BotTrans goal_to_base; 
    bot_trans_apply_trans_to(&local_to_base, &self->goal_to_local, &goal_to_base);
    
    double pos[3] = {goal_to_base.trans_vec[0], goal_to_base.trans_vec[1], goal_to_base.trans_vec[2]};

    double rpy[3] = {0};
    bot_quat_to_roll_pitch_yaw(goal_to_base.rot_quat, rpy);

    int status = self->ik->solvePosRPY(pos, rpy, self->goal_joints);
    //goal received - should draw this 

    fprintf(stderr, "Status : %d\n", status);
    if(status != 0){
        fprintf(stderr, "Error - Inverse kinematics failed to find a solution - returning\n");
        return;        
    }

    fprintf(stderr, "Resulting Joint Angles\n");
    for(int i=0; i < 6; i++){
        fprintf(stderr, "\tJoint [%d] - %f\n", i, bot_to_radians(self->goal_joints[i]));
    }
}

static void graph_message_handler (const lcm_recv_buf_t *rbuf, const char *channel,
				   const arm_rrts_graph_t *msg, void *user) {
    
    RendererGraph *self = (RendererGraph *) user;

    
    if (self->graph_last) {
        arm_rrts_graph_t_destroy (self->graph_last);
    }

    self->graph_last = arm_rrts_graph_t_copy (msg);
    
    tree_t *tree = (tree_t *) calloc(1, sizeof(tree_t));
    tree->tree = arm_rrts_graph_t_copy(msg);
    
    bot_gtk_param_widget_modify_double(self->pw, TREE_HISTORY_DRAW, 0, self->tree_history->size, 1, 0);

    if(self->vertices_last)
        free(self->vertices_last);

    self->vertices_last = (xyz_t *) calloc(self->graph_last->num_vertices, sizeof(xyz_t));
    
    double arm_base_to_local[12];
    if (!bot_frames_get_trans_mat_3x4 (self->frames, "base",
                                       "local",                                  
                                       arm_base_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from local to arm frame!\n");
        return;
    }

    for(int i = 0; i < self->graph_last->num_vertices; i++)
	forward_kinematics(self, self->graph_last->vertices[i].angles, &self->vertices_last[i], arm_base_to_local);	
    free(self->edges_last);
    self->edges_last = (edge_t *) calloc(self->graph_last->num_edges, sizeof(edge_t));

    for(int i = 0; i < self->graph_last->num_edges; i++){
        forward_kinematics(self, self->graph_last->edges[i].src.angles, &self->edges_last[i].src, 
                           arm_base_to_local);
        forward_kinematics(self, self->graph_last->edges[i].dst.angles, &self->edges_last[i].dst, 
                           arm_base_to_local);
    }

    tree->vertices = (xyz_t *) calloc(tree->tree->num_vertices, sizeof(xyz_t));
    for(int i = 0; i < tree->tree->num_vertices; i++)
	forward_kinematics(self, tree->tree->vertices[i].angles, &tree->vertices[i], arm_base_to_local);
	
    tree->edges = (edge_t *) calloc(tree->tree->num_edges, sizeof(edge_t));
    for(int i = 0; i < tree->tree->num_edges; i++){
        forward_kinematics(self, tree->tree->edges[i].src.angles, &tree->edges[i].src, 
                           arm_base_to_local);
        forward_kinematics(self, tree->tree->edges[i].dst.angles, &tree->edges[i].dst, 
                           arm_base_to_local);

    }

    bot_ptr_circular_add(self->tree_history, tree);
 
    bot_viewer_request_redraw (self->viewer);
}

// static void environment_message_handler (const lcm_recv_buf_t *rbuf, const char *channel, const arm_rrts_state_list_t *msg, void *user) {

//     RendererGraph *self = (RendererGraph *) user;
    
//     //get the trajectory 

// }


static void environment_message_handler (const lcm_recv_buf_t *rbuf, const char *channel, const arm_rrts_environment_t *msg, void *user) {

    RendererGraph *self = (RendererGraph *) user;
    
    if (self->environment_last) 
        arm_rrts_environment_t_destroy (self->environment_last);
    self->environment_last = arm_rrts_environment_t_copy (msg);
    
    if(self->tree_history != NULL)
	bot_ptr_circular_destroy(self->tree_history);
    if(self->trajectory_history != NULL)
	bot_ptr_circular_destroy(self->trajectory_history);
    int buffer_size = 1;
    if(self->tree_history_step > 0)
	buffer_size = (int)ceil(self->environment_last->num_iterations/self->tree_history_step);
  
    self->tree_history = bot_ptr_circular_new(buffer_size, tree_destroy,NULL);
    self->trajectory_history = bot_ptr_circular_new(buffer_size, trajectory_destroy, NULL);

    bot_gtk_param_widget_modify_double(self->pw, TREE_HISTORY_DRAW, 0, 1, 1, 0);
    self->draw_index = 0;

    if(self->obstacles_last_min)
        free(self->obstacles_last_min);
    self->obstacles_last_min = 
        (xyz_t *) calloc(self->environment_last->num_obstacles, sizeof(xyz_t));
    
    if(self->obstacles_last_max)
        free(self->obstacles_last_max);
    self->obstacles_last_max = 
        (xyz_t *) calloc(self->environment_last->num_obstacles, sizeof(xyz_t));

    if(self->obstacles_last_center)
        free(self->obstacles_last_center);
    self->obstacles_last_center = 
        (xyz_t *) calloc(self->environment_last->num_obstacles, sizeof(xyz_t));

    double arm_base_to_local[12];
    if (!bot_frames_get_trans_mat_3x4 (self->frames, "base",
                                       "local",                                  
                                       arm_base_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from local to arm frame!\n");
        return;
    }

    forward_kinematics(self, self->environment_last->goal.center, &self->goal_last_center, arm_base_to_local);
    
    double *extrema = (double *) calloc(self->environment_last->goal.num_dimensions, sizeof(double));
    for(int i = 0; i < self->environment_last->goal.num_dimensions; i++){
        extrema[i] = self->environment_last->goal.center[i] -
            (self->environment_last->goal.size[i] / 2.0);
    }
    forward_kinematics(self, extrema, &self->goal_last_min, arm_base_to_local);

    for(int i = 0; i < self->environment_last->goal.num_dimensions; i++){
        extrema[i] = self->environment_last->goal.center[i] +
            (self->environment_last->goal.size[i] / 2.0);
    }
    forward_kinematics(self, extrema, &self->goal_last_max, arm_base_to_local);

    forward_kinematics(self, self->environment_last->operating.center, &self->operating_last_center, arm_base_to_local);

    for(int i = 0; i < self->environment_last->operating.num_dimensions; i++){
        extrema[i] = self->environment_last->operating.center[i] -
            (self->environment_last->operating.size[i] / 2.0);
    }
    forward_kinematics(self, extrema, &self->operating_last_min, arm_base_to_local);
    
    for(int i = 0; i < self->environment_last->operating.num_dimensions; i++){
        extrema[i] = self->environment_last->operating.center[i] +
            (self->environment_last->operating.size[i] / 2.0);
    }
    forward_kinematics(self, extrema, &self->operating_last_max, arm_base_to_local);

    forward_kinematics(self, self->environment_last->root_state, &self->root_state_last, arm_base_to_local);

    for(int i = 0; i < self->environment_last->num_obstacles; i++){

        forward_kinematics(self, self->environment_last->obstacles[i].center, 
                           &self->obstacles_last_center[i], arm_base_to_local);

        for( int j = 0; j < self->environment_last->obstacles[i].num_dimensions; j++){
            extrema[j] = self->environment_last->obstacles[i].center[j] -
                (self->environment_last->obstacles[i].size[j] / 2.0);
        }
        forward_kinematics(self, extrema, &self->obstacles_last_min[i], arm_base_to_local);

        for( int j = 0; j < self->environment_last->obstacles[i].num_dimensions; j++){
            extrema[j] = self->environment_last->obstacles[i].center[j] +
                (self->environment_last->obstacles[i].size[j] / 2.0);
        }
        forward_kinematics(self, extrema, &self->obstacles_last_max[i], arm_base_to_local);
    }
    
    free(extrema);

    bot_viewer_request_redraw (self->viewer);
}

static void trajectory_message_handler (const lcm_recv_buf_t *rbuf, const char *channel, const arm_rrts_state_list_t *msg, void *user) {

    RendererGraph *self = (RendererGraph *) user;
    
    trajectory_t *trajectory = (trajectory_t *) calloc(1, sizeof(trajectory_t));
    trajectory->trajectory = arm_rrts_state_list_t_copy(msg);
    if(trajectory->trajectory->num_states <= 0){
	trajectory->vertices = NULL;

	bot_ptr_circular_add(self->trajectory_history, trajectory);
        
	bot_viewer_request_redraw (self->viewer);
	
	return;
    }

    if (self->trajectory_last) 
        arm_rrts_state_list_t_destroy (self->trajectory_last);
    
    self->trajectory_last = arm_rrts_state_list_t_copy (msg);

    if(self->trajectory_last_positions)
        free(self->trajectory_last_positions);
    
    self->trajectory_last_positions = 
        (xyz_t *) calloc(self->trajectory_last->num_states, sizeof(xyz_t));

    double arm_base_to_local[12];
    if (!bot_frames_get_trans_mat_3x4 (self->frames, "base",
                                       "local",                                  
                                       arm_base_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from local to arm frame!\n");
        return;
    }

     
    for(int j = 0; j < self->trajectory_last->num_states; j++){
      
        forward_kinematics(self, self->trajectory_last->states[j].angles, 
                           &self->trajectory_last_positions[j], arm_base_to_local);
	fprintf(stderr, "point: %f\n", self->trajectory_last->states[j].angles[0]);
    }

    trajectory->vertices = (xyz_t *) calloc(trajectory->trajectory->num_states, sizeof(xyz_t));
    for(int i = 0; i < trajectory->trajectory->num_states; i++){
	forward_kinematics(self, trajectory->trajectory->states[i].angles, 
			   &trajectory->vertices[i], arm_base_to_local);
	//fprintf(stderr, "point: %f, %f, %f\n", trajectory->vertices[i].x,trajectory->vertices[i].y,trajectory->vertices[i].z);
    }
    bot_ptr_circular_add(self->trajectory_history, trajectory);
        
    bot_viewer_request_redraw (self->viewer);
}

static void on_widget_submit(BotGtkParamWidget *pw, const char *name, void *user){
    RendererGraph *self = (RendererGraph *) user;

    if(!strcmp(name, SEND_COMMAND)) {
        double xyz[3] = {bot_gtk_param_widget_get_double(pw, X_POS),
                         bot_gtk_param_widget_get_double(pw, Y_POS), 
                         bot_gtk_param_widget_get_double(pw, Z_POS)
        };
        double rpy[3] = {bot_gtk_param_widget_get_double(pw, ROLL), 
                         bot_gtk_param_widget_get_double(pw, PITCH), 
                         bot_gtk_param_widget_get_double(pw, YAW)
        };
	double xyz_tolerance[3] = {bot_gtk_param_widget_get_double(pw, X_TOL),
				   bot_gtk_param_widget_get_double(pw, Y_TOL),
				   bot_gtk_param_widget_get_double(pw, Z_TOL)
	};
	double rpy_tolerance[3] = {bot_gtk_param_widget_get_double(pw, ROLL_TOL), 
				   bot_gtk_param_widget_get_double(pw, PITCH_TOL), 
				   bot_gtk_param_widget_get_double(pw, YAW_TOL)
	};

        rpy[0] = bot_to_radians(rpy[0]);
        rpy[1] = bot_to_radians(rpy[1]);
        rpy[2] = bot_to_radians(rpy[2]);

        rpy_tolerance[0] = bot_to_radians(rpy_tolerance[0]);
        rpy_tolerance[1] = bot_to_radians(rpy_tolerance[1]);
        rpy_tolerance[2] = bot_to_radians(rpy_tolerance[2]);

        double gripper_pos = bot_gtk_param_widget_get_double(pw, GRIPPER_POSITION);

        int iterations = bot_gtk_param_widget_get_int(pw, ITERATIONS);
      
        BotTrans ef_to_local = get_end_effector_body_to_local(self, xyz, rpy);

        arm_rrts_cmd_t msg;
        msg.gripper_enable = 1;
        msg.gripper_pos = gripper_pos;
        msg.xyz[0] = ef_to_local.trans_vec[0];
        msg.xyz[1] = ef_to_local.trans_vec[1];
        msg.xyz[2] = ef_to_local.trans_vec[2];
        bot_quat_to_roll_pitch_yaw(ef_to_local.rot_quat, msg.ef_rpy);
        msg.xyz_tolerance[0] = xyz_tolerance[0];
        msg.xyz_tolerance[1] = xyz_tolerance[1];
        msg.xyz_tolerance[2] = xyz_tolerance[2];
        msg.ef_rpy_tolerance[0] = rpy_tolerance[0];
        msg.ef_rpy_tolerance[1] = rpy_tolerance[1];
        msg.ef_rpy_tolerance[2] = rpy_tolerance[2];
        msg.iterations = iterations;
        msg.skip_collision_checks = 0;

        
        /*BotTrans end_effector_to_body; 
        end_effector_to_body.trans_vec[0] = xyz[0];
        end_effector_to_body.trans_vec[1] = xyz[1];
        end_effector_to_body.trans_vec[2] = xyz[2];
        
        bot_roll_pitch_yaw_to_quat(rpy, end_effector_to_body.rot_quat);
        
        BotTrans body_to_local; 
        bot_frames_get_trans(self->frames, "body", "local", &body_to_local);
        
        BotTrans end_effector_to_local; 

        bot_trans_apply_trans_to(&body_to_local, &end_effector_to_body, &end_effector_to_local);*/

        self->goal_pos.x = ef_to_local.trans_vec[0];
        self->goal_pos.y = ef_to_local.trans_vec[1];
        self->goal_pos.z = ef_to_local.trans_vec[2];

        //this also needs to be transformed to local frame 
        bot_quat_to_roll_pitch_yaw(ef_to_local.rot_quat, self->goal_orientation);

        self->have_goal = 1;        

        arm_rrts_cmd_t_publish(self->lcm, "ARM_RRTS_COMMAND", &msg);

    }
    if(!strcmp(name, CLEAR_GRAPH)){
        //clear graph 
        

        if (self->graph_last) {
            arm_rrts_graph_t_destroy (self->graph_last);
        }
        self->graph_last = NULL;
        if(self->vertices_last){
            free(self->vertices_last);
            self->vertices_last = NULL;
        }
        if(self->edges_last){
            free(self->edges_last);
            self->edges_last = NULL;
        }
        if(self->trajectory_last){
            arm_rrts_state_list_t_destroy(self->trajectory_last);
            self->trajectory_last = NULL;
        }

        if (self->environment_last) {
            arm_rrts_environment_t_destroy(self->environment_last);
            self->environment_last = NULL;
        }
    }
    if(!strcmp(name, DETECT_OBJECT)){
        arm_cmd_object_manipulation_cmd_t msg;
        msg.utime = bot_timestamp_now();
        msg.type = ARM_CMD_OBJECT_MANIPULATION_CMD_T_TYPE_DETECT_OBJECT;
        msg.requester = ARM_CMD_OBJECT_MANIPULATION_CMD_T_VIEWER;        
        arm_cmd_object_manipulation_cmd_t_publish(self->lcm, "OBJECT_MANIPULATION_REQUEST", &msg);
    }
    if(!strcmp(name, RELEASE_OBJECT)){
        arm_cmd_object_manipulation_cmd_t msg;
        msg.utime = bot_timestamp_now();
        msg.type = ARM_CMD_OBJECT_MANIPULATION_CMD_T_TYPE_RELEASE_OBJECT;
        msg.requester = ARM_CMD_OBJECT_MANIPULATION_CMD_T_VIEWER;        
        arm_cmd_object_manipulation_cmd_t_publish(self->lcm, "OBJECT_MANIPULATION_REQUEST", &msg);
    }
    if(!strcmp(name, PUT_OBJECT_BACK)){
        arm_cmd_object_manipulation_cmd_t msg;
        msg.utime = bot_timestamp_now();
        msg.type = ARM_CMD_OBJECT_MANIPULATION_CMD_T_TYPE_PUT_OBJECT_BACK;
        msg.requester = ARM_CMD_OBJECT_MANIPULATION_CMD_T_VIEWER;        
        arm_cmd_object_manipulation_cmd_t_publish(self->lcm, "OBJECT_MANIPULATION_REQUEST", &msg);
    }

    if(!strcmp(name, SEND_RENDER_SETTINGS)){
	arm_rrts_render_settings_t msg;
	int step = bot_gtk_param_widget_get_double(pw, TREE_HISTORY_STEP);
	msg.tree_history_step = step;
	arm_rrts_render_settings_t_publish(self->lcm, "ARM_RRTS_RENDER_SETTINGS", &msg);
	self->tree_history_step = step;
    }
}

static void draw_axis(BotTrans * axis_to_local, float size, float lineThickness, float opacity)
{
    double axis_to_local_m[16];
    bot_trans_get_mat_4x4(axis_to_local, axis_to_local_m);

    // opengl expects column-major matrices
    double axis_to_local_m_opengl[16];
    bot_matrix_transpose_4x4d(axis_to_local_m, axis_to_local_m_opengl);

    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glEnable(GL_DEPTH_TEST);

    glPushMatrix();
    // rotate and translate the vehicle
    glMultMatrixd(axis_to_local_m_opengl);

    glLineWidth(lineThickness);
    //x-axis
    glBegin(GL_LINES);
    //glColor4f(1, 0, 0, opacity);
    float color_vertex[] = {1.0, 0, 0, 1.0};
    glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_vertex);   
    glVertex3f(size, 0, 0);
    glVertex3f(0, 0, 0);
    glEnd();

    //y-axis
    glBegin(GL_LINES);
    //glColor4f(0, 1, 0, opacity);
    color_vertex[0] = 0;
    color_vertex[1] = 1;
    glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_vertex);   
    glVertex3f(0, size, 0);
    glVertex3f(0, 0, 0);
    glEnd();

    //z-axis
    glBegin(GL_LINES);
    color_vertex[1] = 0;
    color_vertex[2] = 1;
    glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_vertex);   
    glColor4f(0, 0, 1, opacity);
    glVertex3f(0, 0, size);
    glVertex3f(0, 0, 0);
    glEnd();

    glPopMatrix();

    //glDisable(GL_BLEND);
    //glDisable(GL_DEPTH_TEST);
}

void print_bot_trans(BotTrans &trans){
    fprintf(stderr, "Bot Trans : %f,%f,%f\n", trans.trans_vec[0], 
            trans.trans_vec[1], trans.trans_vec[2]);
    
    double rpy[3] = {0};
    bot_quat_to_roll_pitch_yaw(trans.rot_quat, rpy);
    fprintf(stderr, "\t RPY : %f,%f,%f\n", bot_to_degrees(rpy[0]), 
            bot_to_degrees(rpy[1]),     
            bot_to_degrees(rpy[2]));
}

static void on_widget_change(BotGtkParamWidget *pw, const char *name, void *user){

    RendererGraph *self = (RendererGraph *) user;

    self->edge_opacity = bot_gtk_param_widget_get_double(pw, EDGE_OPACITY);
    self->draw_edges = bot_gtk_param_widget_get_bool(pw, DRAW_EDGES);

    bool joint_space_last = self->joint_space;
    bool xyz_space_last = self->xyz_space;
  
    self->joint_space = bot_gtk_param_widget_get_bool(pw, VIEW_JOINT_SPACE);
    self->xyz_space = bot_gtk_param_widget_get_bool(pw, VIEW_XYZ_SPACE);
  
    if (self->joint_space != joint_space_last)
        self->xyz_space = !self->xyz_space;

    else if (self->xyz_space != xyz_space_last)
        self->joint_space = !self->joint_space;

    self->x_axis = bot_gtk_param_widget_get_enum(pw, AXIS_1);
    self->y_axis = bot_gtk_param_widget_get_enum(pw, AXIS_2);
    self->z_axis = bot_gtk_param_widget_get_enum(pw, AXIS_3);
    

    bot_gtk_param_widget_set_bool(pw, VIEW_XYZ_SPACE, self->xyz_space);
    bot_gtk_param_widget_set_bool(pw, VIEW_JOINT_SPACE, self->joint_space);

    self->draw_index = bot_gtk_param_widget_get_double(pw, TREE_HISTORY_DRAW);

    bot_viewer_request_redraw (self->viewer);

}

void draw_arm_position(RendererGraph *self, double *goal_joints){
    int status = 0, no_joints = 0;
    BotTrans *joint_pos = self->forwardK->getJointPos(goal_joints, &status, &no_joints);
        
    for(int i=0; i < no_joints; i++){
        BotTrans joint_to_local = get_end_effector_base_to_local(self, &joint_pos[i]);
        draw_axis(&joint_to_local, 0.1, 2, .7);  
    }
    glLineWidth(5);
    glBegin (GL_LINES);
    for(int i=0; i < no_joints-1; i++){
        BotTrans joint_to_local_c = get_end_effector_base_to_local(self, &joint_pos[i]);
        BotTrans joint_to_local_n = get_end_effector_base_to_local(self, &joint_pos[i+1]);
        glVertex3f (joint_to_local_c.trans_vec[0],
                    joint_to_local_c.trans_vec[1],
                    joint_to_local_c.trans_vec[2]);
        glVertex3f (joint_to_local_n.trans_vec[0],
                    joint_to_local_n.trans_vec[1],
                    joint_to_local_n.trans_vec[2]);
    }
    glEnd (); 
    delete joint_pos;
}

static void renderer_graph_draw(BotViewer *viewer, BotRenderer *renderer)
{   
    
    RendererGraph *self = (RendererGraph*) renderer;

    //fprintf(stderr, "Starting to draw\n");

    glEnable(GL_DEPTH_TEST);
    
    glEnable (GL_BLEND);
    glEnable (GL_RESCALE_NORMAL);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glShadeModel (GL_SMOOTH);
    glEnable (GL_LIGHTING);

    int draw_goal = bot_gtk_param_widget_get_bool(self->pw, DRAW_GOAL);
    int draw_pose = bot_gtk_param_widget_get_bool(self->pw, DRAW_POSE);
    int draw_solution = bot_gtk_param_widget_get_bool(self->pw, DRAW_SOLUTION);
    int draw_tree = bot_gtk_param_widget_get_bool(self->pw, DRAW_TREE);
        
    bool draw_joints = bot_gtk_param_widget_get_bool(self->pw, DRAW_JOINT_POSITION);
    
    double body_to_local[12];
    if (!bot_frames_get_trans_mat_3x4 (self->frames, "body",
                                       "local",                                  
                                       body_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from local to arm frame!\n");
        return;
    }

    if(self->joints && draw_pose){
        double current_joints[6] = {0};
        for(int j = 0; j < 6; j++){
            int joint_id = self->joints->joints[j].joint_id;
            current_joints[joint_id] = bot_to_radians(self->joints->joints[j].angle);
        }

        BotTrans current_ef_to_base;

        self->forwardK->getJointsToBotFrames(current_joints, &current_ef_to_base);
        
        BotTrans current_ef_to_local = get_end_effector_base_to_local(self, &current_ef_to_base);
        draw_axis(&current_ef_to_local, 0.3, 4, .7);     
    }

    if(draw_goal){// && self->have_goal){
        //draw the goal position 
        double xyz[3] = {bot_gtk_param_widget_get_double(self->pw, X_POS),
                         bot_gtk_param_widget_get_double(self->pw, Y_POS), 
                         bot_gtk_param_widget_get_double(self->pw, Z_POS)
        };
        double rpy[3] = {bot_gtk_param_widget_get_double(self->pw, ROLL), 
                         bot_gtk_param_widget_get_double(self->pw, PITCH), 
                         bot_gtk_param_widget_get_double(self->pw, YAW)
        };

        rpy[0] = bot_to_radians(rpy[0]);
        rpy[1] = bot_to_radians(rpy[1]);
        rpy[2] = bot_to_radians(rpy[2]);


        BotTrans ef_to_local = get_end_effector_body_to_local(self, xyz, rpy);

        draw_axis(&ef_to_local, 0.2, 4, .7);     
    }
    if(draw_joints && self->last_goal){
        int status = 0, no_joints = 0;
        BotTrans *joint_pos = self->forwardK->getJointPos(self->goal_joints, &status, &no_joints);
        
        for(int i=0; i < no_joints; i++){
            BotTrans joint_to_local = get_end_effector_base_to_local(self, &joint_pos[i]);
            draw_axis(&joint_to_local, 0.1, 2, .7);  
        }
        glLineWidth(5);
        glBegin (GL_LINES);
        for(int i=0; i < no_joints-1; i++){
            BotTrans joint_to_local_c = get_end_effector_base_to_local(self, &joint_pos[i]);
            BotTrans joint_to_local_n = get_end_effector_base_to_local(self, &joint_pos[i+1]);
            glVertex3f (joint_to_local_c.trans_vec[0],
                        joint_to_local_c.trans_vec[1],
                        joint_to_local_c.trans_vec[2]);
            glVertex3f (joint_to_local_n.trans_vec[0],
                        joint_to_local_n.trans_vec[1],
                        joint_to_local_n.trans_vec[2]);
        }
        glEnd (); 
        delete joint_pos;
    }

    if(self->grasp_plan){
        int draw_pre_plan = bot_gtk_param_widget_get_bool(self->pw, DRAW_PRE_PLAN);
        int draw_pre_grasp = bot_gtk_param_widget_get_bool(self->pw, DRAW_PRE_GRASP);
        int draw_grasp = bot_gtk_param_widget_get_bool(self->pw, DRAW_GOAL_PLAN);
        int draw_lift = bot_gtk_param_widget_get_bool(self->pw, DRAW_LIFT);
        if(draw_pre_plan){
            draw_arm_position(self, self->grasp_plan->pre_plan.joints);
        }
        if(draw_pre_grasp){
            draw_arm_position(self, self->grasp_plan->pre_grasp.joints);
        }
        if(draw_grasp){
            draw_arm_position(self, self->grasp_plan->goal.joints);
        }
        if(draw_lift){
            draw_arm_position(self, self->grasp_plan->lift.joints);
        }
    }


    //draw the goal sent to rrt star 
    if(self->last_goal){
        //draw the goal 
        BotTrans goal_to_local; 
        goal_to_local.trans_vec[0] = self->last_goal->xyz[0];
        goal_to_local.trans_vec[1] = self->last_goal->xyz[1];
        goal_to_local.trans_vec[2] = self->last_goal->xyz[2];

        bot_roll_pitch_yaw_to_quat(self->last_goal->ef_rpy, goal_to_local.rot_quat);

        draw_axis(&goal_to_local, 0.4, 7, .9);        

        //do IK and then draw the solution also 
    }

    // Draw the graph
    if (draw_tree && self->graph_last) {

	tree_t *cur_tree = (tree_t *) bot_ptr_circular_index(self->tree_history, self->draw_index);

        // Draw the vertices
        glPointSize (3.0);
        float color_vertex[] = {0.1, 0.1, 0.8, 1.0};
        glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_vertex);        
        glBegin (GL_POINTS);

        double local_pos[3];
        for (int i = 0; i < cur_tree->tree->num_vertices; i++) {
            if(self->xyz_space){
                glVertex3f (cur_tree->vertices[i].x, cur_tree->vertices[i].y, cur_tree->vertices[i].z);
            }
            
            if(self->joint_space)
                glVertex3f (cur_tree->tree->vertices[i].angles[self->x_axis],
                            cur_tree->tree->vertices[i].angles[self->y_axis],
                            cur_tree->tree->vertices[i].angles[self->z_axis]+2);
        }

        glEnd();

        
        // Draw the edges
	if(self->draw_edges){
            for (int i = 0; i < cur_tree->tree->num_edges; i++) {
            
                glLineWidth (2.0); 
                float color_edge[] = {0.8, 0.3, 0.3, ((double)(self->edge_opacity))/100.0};
                glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_edge);
                glBegin (GL_LINE_STRIP);

                if(self->xyz_space){
                    glVertex3f (cur_tree->edges[i].src.x,
                                cur_tree->edges[i].src.y,
                                cur_tree->edges[i].src.z);
                    glVertex3f (cur_tree->edges[i].dst.x, 
                                cur_tree->edges[i].dst.y,
                                cur_tree->edges[i].dst.z);
                }
                if(self->joint_space){
                    glVertex3f (cur_tree->tree->edges[i].src.angles[self->x_axis],
                                cur_tree->tree->edges[i].src.angles[self->y_axis],
                                cur_tree->tree->edges[i].src.angles[self->z_axis]+2);
                    glVertex3f (cur_tree->tree->edges[i].dst.angles[self->x_axis],
                                cur_tree->tree->edges[i].dst.angles[self->y_axis],
                                cur_tree->tree->edges[i].dst.angles[self->z_axis]+2);
                }


                glEnd();
            }
        }
    }
    

    //Draw the trajectory
    if (draw_solution && self->trajectory_last){
        //Draw the vertices       

	trajectory_t *cur_traj = (trajectory_t *) 
	    bot_ptr_circular_index(self->trajectory_history, self->draw_index);

	if (cur_traj->trajectory->num_states > 0){

	    //fprintf(stderr, "first point x,y,z = %f,%f,%f\n", cur_traj->vertices[3].x,cur_traj->vertices[3].y,cur_traj->vertices[3].z);

	    glPointSize (8.0);
	    float color_vertex[] = {0.9, 0.1, 0.6, 1.0};
	    glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_vertex);
	    glBegin (GL_POINTS);

	    for (int i = 0; i < cur_traj->trajectory->num_states; i++){
		if(self->xyz_space)
		    glVertex3f (cur_traj->vertices[i].x,
				cur_traj->vertices[i].y,
				cur_traj->vertices[i].z);
		if(self->joint_space)
		    glVertex3f (cur_traj->trajectory->states[i].angles[self->x_axis],
				cur_traj->trajectory->states[i].angles[self->y_axis],
				cur_traj->trajectory->states[i].angles[self->z_axis]+2);

	    }
	    glEnd();
    
      
	    //Draw the edges
      
	    glLineWidth (8.0);
	    float color_edge[] = {0.1, 1.0, 0.1, 1.0};
	    glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_edge);
	    glBegin (GL_LINE_STRIP);

	    for (int i = 0; i < (cur_traj->trajectory->num_states - 1); i++){

		if(self->xyz_space){
		    glVertex3f (cur_traj->vertices[i].x,
				cur_traj->vertices[i].y,
				cur_traj->vertices[i].z);
		    glVertex3f (cur_traj->vertices[i+1].x,
				cur_traj->vertices[i+1].y,
				cur_traj->vertices[i+1].z);
		}
		if(self->joint_space){
		    glVertex3f (cur_traj->trajectory->states[i].angles[self->x_axis],
				cur_traj->trajectory->states[i].angles[self->y_axis],
				cur_traj->trajectory->states[i].angles[self->z_axis]+2);
		    glVertex3f (cur_traj->trajectory->states[i+1].angles[self->x_axis],
				cur_traj->trajectory->states[i+1].angles[self->y_axis],
				cur_traj->trajectory->states[i+1].angles[self->z_axis]+2);
		}
	    }
	    glEnd();
	}
    }
    
    // Draw the environment
    if (self->environment_last) {
      
        //Draw the starting point
        glPointSize (8.0);
        float color_vertex[] = {1, 0.7, 0.1, 1.0};
        glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_vertex);
        glBegin (GL_POINTS);

        for (int i = 0; i < 3; i++){
            if(self->xyz_space)
                glVertex3f (self->root_state_last.x,
                            self->root_state_last.y,
                            self->root_state_last.z);
            if(self->joint_space)
                glVertex3f (self->environment_last->root_state[self->x_axis],
                            self->environment_last->root_state[self->y_axis],
                            self->environment_last->root_state[self->z_axis]+2);
        }
        glEnd();

      
     
        // Draw the goal region 
        float color_goal[] = { 0.8, 0.1, 0.8, ((double)(self->goal_opacity))/100.0};
        glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_goal);

        glPushMatrix ();

        double size_x;
        double size_y;
        double size_z;

        if(self->xyz_space) {
            glTranslated (self->goal_last_center.x, self->goal_last_center.y, self->goal_last_center.z);
            glRotatef (0.0, 0.0, 0.0, 1.0);
      
            size_x = fabs(self->goal_last_max.x - self->goal_last_min.x);
            size_y = fabs(self->goal_last_max.y - self->goal_last_min.y);
            size_z = fabs(self->goal_last_max.z - self->goal_last_min.z);
        }

        if(self->joint_space){
            glTranslated (self->environment_last->goal.center[self->x_axis], self->environment_last->goal.center[self->y_axis], self->environment_last->goal.center[self->z_axis]+2);
            glRotatef (0.0, 0.0, 0.0, 1.0);

            size_x = self->environment_last->goal.size[self->x_axis];
            size_y = self->environment_last->goal.size[self->y_axis];
            size_z = self->environment_last->goal.size[self->z_axis];
        }

        glScalef (size_x, size_y, size_z);

        bot_gl_draw_cube ();

        glPopMatrix ();


      
        // Draw the obstacles        
        for (int i = 0; i < self->environment_last->num_obstacles; i++) {
      
            float color_obstacles[] = { 0.3, 0, 0, ((double)(self->obstacle_opacity))/100.0};
            glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_obstacles);
            
            glPushMatrix ();
            
            double size_x;
            double size_y;
            double size_z;

            if(self->xyz_space) {
                glTranslated (self->obstacles_last_center[i].x, self->obstacles_last_center[i].y, self->obstacles_last_center[i].z);
                glRotatef (0.0, 0.0, 0.0, 1.0);
	
                size_x = fabs(self->obstacles_last_max[i].x - self->obstacles_last_min[i].x);
                size_y = fabs(self->obstacles_last_max[i].y - self->obstacles_last_min[i].y);
                size_z = fabs(self->obstacles_last_max[i].z - self->obstacles_last_min[i].z);
            }

            if(self->joint_space){
                glTranslated (self->environment_last->obstacles[i].center[self->x_axis], self->environment_last->obstacles[i].center[self->y_axis], self->environment_last->obstacles[i].center[self->z_axis]+2);
                glRotatef (0.0, 0.0, 0.0, 1.0);

                size_x = self->environment_last->obstacles[i].size[self->x_axis];
                size_y = self->environment_last->obstacles[i].size[self->y_axis];
                size_z = self->environment_last->obstacles[i].size[self->z_axis];
            }

            glScalef (size_x, size_y, size_z);
            
            bot_gl_draw_cube ();
            
            glPopMatrix ();
        }
    
        // Draw the operating region

        if(self->joint_space){
            float color_operating[] = { 0.8, 0.1, 0.8, ((double)(self->goal_opacity))/100.0};
            glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_operating);

            glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);

            glPushMatrix ();

            glTranslated (self->environment_last->operating.center[self->x_axis], self->environment_last->operating.center[self->y_axis], self->environment_last->operating.center[self->z_axis]+2);
            glRotatef (0.0, 0.0, 0.0, 1.0);

            double size_x = self->environment_last->operating.size[self->x_axis];
            double size_y = self->environment_last->operating.size[self->y_axis];
            double size_z = self->environment_last->operating.size[self->z_axis];
      
            glScalef (size_x, size_y, size_z);

            bot_gl_draw_cube ();

            glPopMatrix ();
	}
          
    }
    
    return;
}


static void renderer_graph_free (BotRenderer *renderer)
{
    RendererGraph *self = (RendererGraph*) renderer;
    
    if (self->graph_last)
        arm_rrts_graph_t_destroy (self->graph_last);
    free(self);
}


void setup_renderer_arm_rrts (BotViewer* viewer, int render_priority, BotParam *param, BotFrames *frame, lcm_t* lcm)
{
    
    RendererGraph *self = (RendererGraph *) calloc(1, sizeof(RendererGraph));//new RendererGraph;
    BotRenderer *renderer = &self->renderer;
    self->lcm = lcm;
    self->viewer = viewer;
    self->frames = bot_frames_get_global (lcm, param);//frame;

    self->vertices_last = (xyz_t *) calloc(1, sizeof(xyz_t));
    self->edges_last = (edge_t *) calloc(1, sizeof(edge_t));
    self->trajectory_last_positions = (xyz_t *) calloc(1, sizeof(xyz_t));
    self->obstacles_last_min = (xyz_t *) calloc(1, sizeof(xyz_t));
    self->obstacles_last_max = (xyz_t *) calloc(1, sizeof(xyz_t));
    self->obstacles_last_center = (xyz_t *) calloc(1, sizeof(xyz_t));
    
    renderer->draw = renderer_graph_draw;
    renderer->destroy = renderer_graph_free;
    renderer->widget = bot_gtk_param_widget_new();
    renderer->name = (char *) RENDERER_NAME;
    renderer->user = self;
    renderer->enabled = 1;
    self->have_goal = 0;
    self->last_goal = NULL;
    self->grasp_plan = NULL;
    
    self->obstacle_opacity = 50;
    self->goal_opacity = 50;
    self->edge_opacity = 100;
    self->draw_edges = true;
    self->joint_space = false;
    self->xyz_space = true;
    
    self->graph_last = NULL;
    self->environment_last = NULL;
    self->trajectory_last = NULL;

    self->tree_history = NULL;
    self->trajectory_history = NULL;
    self->tree_history_step = 0;
    self->draw_index = 0;

    self->x_axis = 1;
    self->y_axis = 2;
    self->z_axis = 3;
    
    BotGtkParamWidget *pw = BOT_GTK_PARAM_WIDGET(renderer->widget);

    self->pw = pw;
    self->fk = fk_new();
    
    self->joints = NULL;
    self->forwardK = new ForwardK();
    self->ik = new InverseK();

    /*bot_gtk_param_widget_add_double(pw, X_POS, BOT_GTK_PARAM_WIDGET_SLIDER, -2, 2, 0.01, 0);
    bot_gtk_param_widget_add_double(pw, Y_POS, BOT_GTK_PARAM_WIDGET_SLIDER, -2, 2, 0.01, 0);
    bot_gtk_param_widget_add_double(pw, Z_POS, BOT_GTK_PARAM_WIDGET_SLIDER, -2, 2, 0.01, 0);
    
    bot_gtk_param_widget_add_double(pw, ROLL, BOT_GTK_PARAM_WIDGET_SLIDER, -180, 180, 0.1, 0);
    bot_gtk_param_widget_add_double(pw, PITCH, BOT_GTK_PARAM_WIDGET_SLIDER, -180, 180, 0.1, 0);    
    bot_gtk_param_widget_add_double(pw, YAW, BOT_GTK_PARAM_WIDGET_SLIDER, -180, 180, 0.1, 0);

    bot_gtk_param_widget_add_double(pw, X_TOL, BOT_GTK_PARAM_WIDGET_SLIDER, -2, 2, 0.01, 0.03);
    bot_gtk_param_widget_add_double(pw, Y_TOL, BOT_GTK_PARAM_WIDGET_SLIDER, -2, 2, 0.01, 0.03);
    bot_gtk_param_widget_add_double(pw, Z_TOL, BOT_GTK_PARAM_WIDGET_SLIDER, -2, 2, 0.01, 0.03);

    bot_gtk_param_widget_add_double(pw, ROLL_TOL, BOT_GTK_PARAM_WIDGET_SLIDER, 0, 360, .1, 5);
    bot_gtk_param_widget_add_double(pw, PITCH_TOL, BOT_GTK_PARAM_WIDGET_SLIDER, 0, 360, .1, 5);    
    bot_gtk_param_widget_add_double(pw, YAW_TOL, BOT_GTK_PARAM_WIDGET_SLIDER, 0, 360, .1, 5);

    bot_gtk_param_widget_add_double(pw, GRIPPER_POSITION, 
    BOT_GTK_PARAM_WIDGET_SLIDER, -150,150 , 0.1, 0);

    bot_gtk_param_widget_add_int(pw, ITERATIONS, 
    BOT_GTK_PARAM_WIDGET_SLIDER, 1,100000 , 100, 5000);*/

    g_signal_connect (G_OBJECT(pw), "changed", G_CALLBACK (on_widget_submit), self);

    
    //bot_gtk_param_widget_add_buttons(pw, SEND_COMMAND, NULL);
    bot_gtk_param_widget_add_buttons(pw, DETECT_OBJECT, NULL);
    bot_gtk_param_widget_add_buttons(pw, RELEASE_OBJECT, NULL);
    bot_gtk_param_widget_add_buttons(pw, PUT_OBJECT_BACK, NULL);

    bot_gtk_param_widget_add_buttons(pw, CLEAR_GRAPH, NULL);    

    bot_gtk_param_widget_add_booleans(pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, DRAW_PRE_PLAN, 0, NULL);
    bot_gtk_param_widget_add_booleans(pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, DRAW_PRE_GRASP, 0, NULL);
    bot_gtk_param_widget_add_booleans(pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, DRAW_GOAL_PLAN, 0, NULL);
    bot_gtk_param_widget_add_booleans(pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, DRAW_LIFT, 0, NULL);

    
    bot_gtk_param_widget_add_booleans(pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, DRAW_POSE, 1, NULL);
    bot_gtk_param_widget_add_booleans(pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, DRAW_GOAL, 0, NULL);
    bot_gtk_param_widget_add_booleans(pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, DRAW_SOLUTION, 1, NULL);
    bot_gtk_param_widget_add_booleans(pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, DRAW_TREE, 0, NULL);
    bot_gtk_param_widget_add_booleans(self->pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, DRAW_JOINT_POSITION , 0, NULL);
     
    bot_gtk_param_widget_add_booleans(pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, DRAW_EDGES, 1, NULL);
    bot_gtk_param_widget_add_double(pw, EDGE_OPACITY, 
                                    BOT_GTK_PARAM_WIDGET_SLIDER, 0, 100 , 1, 100);
  
    bot_gtk_param_widget_add_booleans(pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, VIEW_JOINT_SPACE, 0, NULL);
    bot_gtk_param_widget_add_booleans(pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, VIEW_XYZ_SPACE, 1, NULL);
    bot_gtk_param_widget_add_enum(pw, AXIS_1, BOT_GTK_PARAM_WIDGET_MENU, 1, "Shoulder twist", 0, "Shoulder", 1, 
				  "Elbow", 2, "Lateral wrist", 3, "Twist wrist", 4, NULL);
    bot_gtk_param_widget_add_enum(pw, AXIS_2, BOT_GTK_PARAM_WIDGET_MENU, 2, "Shoulder twist", 0, "Shoulder", 1, 
				  "Elbow", 2, "Lateral wrist", 3, "Twist wrist", 4, NULL);
    bot_gtk_param_widget_add_enum(pw, AXIS_3, BOT_GTK_PARAM_WIDGET_MENU, 3, "Shoulder twist", 0, "Shoulder", 1, 
				  "Elbow", 2, "Lateral wrist", 3, "Twist wrist", 4, NULL);

    bot_gtk_param_widget_add_double(pw, TREE_HISTORY_STEP, 
				    BOT_GTK_PARAM_WIDGET_SLIDER, 0, 1000 , 1, 100);
    
    bot_gtk_param_widget_add_buttons(pw, SEND_RENDER_SETTINGS, NULL);

    bot_gtk_param_widget_add_double(pw, TREE_HISTORY_DRAW, 
				    BOT_GTK_PARAM_WIDGET_SLIDER, 0, 1000 , 1, 0);

    g_signal_connect (G_OBJECT(pw), "changed", G_CALLBACK (on_widget_change), self);
    

    // subscribe to messages
    arm_rrts_graph_t_subscribe (lcm, "ARM_RRTS_GRAPH", graph_message_handler, self);
    arm_rrts_environment_t_subscribe (lcm, "ARM_RRTS_ENVIRONMENT", environment_message_handler, self);

    arm_rrts_state_list_t_subscribe (lcm, "ARM_RRTS_STATE_LIST", trajectory_message_handler, self);

    arm_cmd_joint_status_list_t_subscribe(self->lcm, "ARM_JOINT_STATUS", &on_status, self);
    arm_cmd_grasp_plan_t_subscribe(self->lcm, "OBJECT_GRASP_PLAN", on_grasp_plan, self);

    arm_rrts_cmd_t_subscribe(self->lcm, "ARM_RRTS_COMMAND", on_goal, self);

    bot_viewer_add_renderer(viewer, &self->renderer, render_priority);


}
 
