#ifndef RACECAR_ROAD_RENDERER_H_
#define RACECAR_ROAD_RENDERER_H_

//#include <iostream>

#include <bot_vis/bot_vis.h>
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <stdbool.h>
//#include <arm_ik/ik.h>

#include <gtk/gtk.h>
#include <lcm/lcm.h>

#include <GL/glut.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif
    
    void setup_renderer_arm_rrts (BotViewer* viewer, int render_priority, BotParam *param, BotFrames *frame, lcm_t* lcm);
    
#ifdef __cplusplus
}
#endif

#endif
