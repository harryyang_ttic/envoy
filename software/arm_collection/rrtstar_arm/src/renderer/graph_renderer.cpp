#include "graph_renderer.h"

#include <lcmtypes/rrtstarlcm_environment_t.h>
#include <lcmtypes/rrtstarlcm_trajectory_t.h>
#include <lcmtypes/rrtstarlcm_graph_t.h>
#include <lcmtypes/rrtstarlcm_vertex_t.h>
#include <lcmtypes/rrtstarlcm_edge_t.h>
#include <lcmtypes/rrtstarlcm_region_3d_t.h>

#include <lcmtypes/arm_rrts_state_list_t.h>
#include <lcmtypes/arm_rrts_cmd_t.h>
#include <lcmtypes/arm_rrts_environment_t.h>
#include <lcmtypes/arm_rrts_graph_t.h>

#include <vector>
#include <cmath>

#define RENDERER_NAME "Arm RRT*"
#define SEND_COMMAND "Solve/send RRT*"

#define GRIPPER_POSITION "Gripper-Position"

#define X_POS "X Position"
#define Y_POS "Y Position"
#define Z_POS "Z Position"
#define ROLL "Roll"
#define PITCH "Pitch"
#define YAW "Yaw"
#define DRAW_GOAL "Draw Goal"
#define ITERATIONS "Iterations"
#define DRAW_EDGES "Draw Edges"
#define EDGE_OPACITY "Edge Opacity"
#define VIEW_JOINT_SPACE "View Joint Space"
#define VIEW_XYZ_SPACE "View XYZ Space"

using namespace std;

typedef struct _xyz_t {
    double x;
    double y;
    double z;
}xyz_t;

typedef struct _edge_t {
    xyz_t src;
    xyz_t dst;
}edge_t;

class RendererGraph {
public:
    BotRenderer renderer;
    BotGtkParamWidget *pw;
    BotViewer *viewer;
    lcm_t * lcm;
    
    arm_rrts_graph_t *graph_last;
    arm_rrts_environment_t *environment_last;
    arm_rrts_state_list_t *trajectory_last;
    
    xyz_t goal_pos;
    double goal_orientation[3];
    int have_goal;

    xyz_t *trajectory_last_positions;
    xyz_t goal_last_center;
    xyz_t goal_last_max;
    xyz_t goal_last_min;
    xyz_t operating_last_center;
    xyz_t operating_last_min;
    xyz_t operating_last_max;
    xyz_t root_state_last;
    xyz_t *vertices_last;
    edge_t *edges_last;
    
    double obstacle_opacity;
    double goal_opacity;
    double edge_opacity;
  
    bool draw_edges;
    bool joint_space;
    bool xyz_space;
};

int forward_kinematics(double *in_angles, xyz_t *out){
    double joint_length[7] = {0,0.047625,0.38735,0.381,0.06985 + 0.038735 + 0.111125};
    double arm_magnitude_in_xy = 0;
    double cur_angle = 0;
    out->z = 0;
    for(int i = 1; i < 5; i++){
        arm_magnitude_in_xy = arm_magnitude_in_xy + joint_length[i]*cos(cur_angle);
        out->z += joint_length[i]*sin(cur_angle);
        if(i != 5)
            cur_angle = cur_angle + in_angles[i];
    }
    out->x = arm_magnitude_in_xy*cos(in_angles[0]);
    out->y = arm_magnitude_in_xy*sin(in_angles[0]);

    cout << "Forward kinematics: " << out->x << " " << out->y << " " << out->z << endl;

    return 1;
}
        
static void graph_message_handler (const lcm_recv_buf_t *rbuf, const char *channel,
				   const arm_rrts_graph_t *msg, void *user) {
    
    RendererGraph *self = (RendererGraph *) user;
    
    if (self->graph_last) 
        arm_rrts_graph_t_destroy (self->graph_last);
    
    self->graph_last = arm_rrts_graph_t_copy (msg);

    if(self->vertices_last)
        free(self->vertices_last);

    self->vertices_last = (xyz_t *) calloc(self->graph_last->num_vertices, sizeof(xyz_t));

    for(int i = 0; i < self->graph_last->num_vertices; i++)
        forward_kinematics(self->graph_last->vertices[i].angles, &self->vertices_last[i]);
    cout << "converted vertices" << endl;
    if(self->edges_last)
        free(self->edges_last);

    self->edges_last = (edge_t *) calloc(self->graph_last->num_edges, sizeof(edge_t));
 
    for(int i = 0; i < self->graph_last->num_edges; i++){
        forward_kinematics(self->graph_last->edges[i].src.angles, &self->edges_last[i].src);
        forward_kinematics(self->graph_last->edges[i].dst.angles, &self->edges_last[i].dst);
    }
    cout << "converted edges" << endl;

    bot_viewer_request_redraw (self->viewer);
}


static void environment_message_handler (const lcm_recv_buf_t *rbuf, const char *channel, const arm_rrts_environment_t *msg, void *user) {

    RendererGraph *self = (RendererGraph *) user;
    
    if (self->environment_last) 
        arm_rrts_environment_t_destroy (self->environment_last);
    
    self->environment_last = arm_rrts_environment_t_copy (msg);

    forward_kinematics(self->environment_last->goal.center, &self->goal_last_center);
    
    double *extrema = (double *) calloc(self->environment_last->goal.num_dimensions, sizeof(double));
    for(int i = 0; i < self->environment_last->goal.num_dimensions; i++){
        extrema[i] = self->environment_last->goal.center[i] -
            (self->environment_last->goal.size[i] / 2.0);
    }
    forward_kinematics(extrema, &self->goal_last_min);

    for(int i = 0; i < self->environment_last->goal.num_dimensions; i++){
        extrema[i] = self->environment_last->goal.center[i] +
            (self->environment_last->goal.size[i] / 2.0);
    }
    forward_kinematics(extrema, &self->goal_last_max);

    forward_kinematics(self->environment_last->operating.center, &self->operating_last_center);

    for(int i = 0; i < self->environment_last->operating.num_dimensions; i++){
        extrema[i] = self->environment_last->operating.center[i] -
            (self->environment_last->operating.size[i] / 2.0);
        cout << extrema[i] << endl;
    }
    forward_kinematics(extrema, &self->operating_last_min);
    cout << "maxes" << endl;
    for(int i = 0; i < self->environment_last->operating.num_dimensions; i++){
        extrema[i] = self->environment_last->operating.center[i] +
            (self->environment_last->operating.size[i] / 2.0);
        cout << extrema[i]<< endl;
    }
    forward_kinematics(extrema, &self->operating_last_max);

    forward_kinematics(self->environment_last->root_state, &self->root_state_last);
    cout << "Root State: " << self->root_state_last.x << " " << 
        self->root_state_last.y << " " << self->root_state_last.z << endl;
    
    free(extrema);

    bot_viewer_request_redraw (self->viewer);
}

static void trajectory_message_handler (const lcm_recv_buf_t *rbuf, const char *channel, const arm_rrts_state_list_t *msg, void *user) {

    RendererGraph *self = (RendererGraph *) user;
    
    if (self->trajectory_last) 
        arm_rrts_state_list_t_destroy (self->trajectory_last);
    
    self->trajectory_last = arm_rrts_state_list_t_copy (msg);

    if(self->trajectory_last_positions)
        free(self->trajectory_last_positions);
    
    self->trajectory_last_positions = 
        (xyz_t *) calloc(self->trajectory_last->num_states, sizeof(xyz_t));

    //calculate magnitude of arm length
    double joint_length[7] = {0,0.047625,0.38735,0.381,0.06985 + 0.038735 + 0.111125};
    for(int j = 0; j < self->trajectory_last->num_states; j++){
        double arm_magnitude_in_xy = 0;
        double cur_angle = 0;
        self->trajectory_last_positions[j].z = 0;
        for(int i = 1; i < 5; i++){
            arm_magnitude_in_xy = arm_magnitude_in_xy + joint_length[i]*cos(cur_angle);
            self->trajectory_last_positions[j].z += joint_length[i]*sin(cur_angle);
            if(i != 5)
                cur_angle = cur_angle + self->trajectory_last->states[j].angles[i];
        }
        self->trajectory_last_positions[j].x = 
            arm_magnitude_in_xy*cos(self->trajectory_last->states[j].angles[0]);
        self->trajectory_last_positions[j].y = 
            arm_magnitude_in_xy*sin(self->trajectory_last->states[j].angles[0]);

        cout << "State " << j << ": " << self->trajectory_last_positions[j].x << " " << self->trajectory_last_positions[j].y << " " << self->trajectory_last_positions[j].z << endl;
    }
    
    bot_viewer_request_redraw (self->viewer);
}

static void on_widget_submit(BotGtkParamWidget *pw, const char *name, void *user){

    if(!strcmp(name, SEND_COMMAND)) {

        RendererGraph *self = (RendererGraph *) user;
    
        fprintf(stderr, "Button pressed.\n");

        double xyz[3] = {bot_gtk_param_widget_get_double(pw, X_POS),
                         bot_gtk_param_widget_get_double(pw, Y_POS), 
                         bot_gtk_param_widget_get_double(pw, Z_POS)
        };
        double rpy[3] = {bot_gtk_param_widget_get_double(pw, ROLL), 
                         bot_gtk_param_widget_get_double(pw, PITCH), 
                         bot_gtk_param_widget_get_double(pw, YAW)
        };

        double gripper_pos = bot_gtk_param_widget_get_double(pw, GRIPPER_POSITION);

        int iterations = bot_gtk_param_widget_get_int(pw, ITERATIONS);
      
        arm_rrts_cmd_t msg;
        msg.gripper_enable = 1;
        msg.gripper_pos = gripper_pos;
        msg.xyz[0] = xyz[0];
        msg.xyz[1] = xyz[1];
        msg.xyz[2] = xyz[2];
        msg.ef_rpy[0] = rpy[0];
        msg.ef_rpy[1] = rpy[1];
        msg.ef_rpy[2] = rpy[2];
        msg.iterations = iterations;
        msg.skip_collision_checks = 0;

        self->goal_pos.x = xyz[0];
        self->goal_pos.y = xyz[1];
        self->goal_pos.z = xyz[2];

        self->goal_orientation[0] = rpy[0];
        self->goal_orientation[1] = rpy[1];
        self->goal_orientation[2] = rpy[2];

        self->have_goal = 1;
    
        arm_rrts_cmd_t_publish(self->lcm, "ARM_RRTS_COMMAND", &msg);

    }
}

static void on_widget_change(BotGtkParamWidget *pw, const char *name, void *user){

    RendererGraph *self = (RendererGraph *) user;

    self->edge_opacity = bot_gtk_param_widget_get_double(pw, EDGE_OPACITY);
    self->draw_edges = bot_gtk_param_widget_get_bool(pw, DRAW_EDGES);

    bool joint_space_last = self->joint_space;
    bool xyz_space_last = self->xyz_space;
  
    self->joint_space = bot_gtk_param_widget_get_bool(pw, VIEW_JOINT_SPACE);
    self->xyz_space = bot_gtk_param_widget_get_bool(pw, VIEW_XYZ_SPACE);
  
    if (self->joint_space != joint_space_last)
        self->xyz_space = !self->xyz_space;

    else if (self->xyz_space != xyz_space_last)
        self->joint_space = !self->joint_space;

    bot_gtk_param_widget_set_bool(pw, VIEW_XYZ_SPACE, self->xyz_space);
    bot_gtk_param_widget_set_bool(pw, VIEW_JOINT_SPACE, self->joint_space);

    bot_viewer_request_redraw (self->viewer);

}

static void renderer_graph_draw(BotViewer *viewer, BotRenderer *renderer)
{   
    
    RendererGraph *self = (RendererGraph*) renderer;


    int draw_goal = bot_gtk_param_widget_get_bool(self->pw, DRAW_GOAL);
    
    glEnable(GL_DEPTH_TEST);
    
    glEnable (GL_BLEND);
    glEnable (GL_RESCALE_NORMAL);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glShadeModel (GL_SMOOTH);
    glEnable (GL_LIGHTING);

    if(draw_goal && self->have_goal){
        //draw the goal position 
        glPointSize (10.0);
        float color_vertex[] = {1.0, 0.1, 0.8, 1.0};
        glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_vertex);     
        glBegin (GL_POINTS);
        glVertex3f (self->goal_pos.x, 
                    self->goal_pos.y, 
                    self->goal_pos.z); 
        glEnd();
    }

    
    // Draw the graph
    if (self->graph_last) {
     
        // Draw the vertices
        glPointSize (3.0);
        float color_vertex[] = {0.1, 0.1, 0.8, 1.0};
        glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_vertex);        
        glBegin (GL_POINTS);

        for (int i = 0; i < self->graph_last->num_vertices; i++) {
            
            if(self->xyz_space) 
                glVertex3f (self->vertices_last[i].x, 
                            self->vertices_last[i].y,
                            self->vertices_last[i].z);

            if(self->joint_space)
                glVertex3f (self->graph_last->vertices[i].angles[1],
                            self->graph_last->vertices[i].angles[2],
                            self->graph_last->vertices[i].angles[3]+2);
        }

        glEnd();

        
        // Draw the edges
	if(self->draw_edges){
            for (int i = 0; i < self->graph_last->num_edges; i++) {
            
                glLineWidth (2.0); 
                float color_edge[] = {0.8, 0.3, 0.3, ((double)(self->edge_opacity))/100.0};
                glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_edge);
                glBegin (GL_LINE_STRIP);
            
                if(self->xyz_space){
                    glVertex3f (self->edges_last[i].src.x,
                                self->edges_last[i].src.y,
                                self->edges_last[i].src.z);
                    glVertex3f (self->edges_last[i].dst.x, 
                                self->edges_last[i].dst.y,
                                self->edges_last[i].dst.z);
                }
                if(self->joint_space){
                    glVertex3f (self->graph_last->edges[i].src.angles[1],
                                self->graph_last->edges[i].src.angles[2],
                                self->graph_last->edges[i].src.angles[3]+2);
                    glVertex3f (self->graph_last->edges[i].dst.angles[1],
                                self->graph_last->edges[i].dst.angles[2],
                                self->graph_last->edges[i].dst.angles[3]+2);
                }


                glEnd();
            }
	}
    }


    //Draw the trajectory
    if (self->trajectory_last){
      
        //Draw the vertices
        glPointSize (8.0);
        float color_vertex[] = {0.9, 0.1, 0.6, 1.0};
        glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_vertex);
        glBegin (GL_POINTS);

        for (int i = 0; i < self->trajectory_last->num_states; i++){
            if(self->xyz_space)
                glVertex3f (self->trajectory_last_positions[i].x,
                            self->trajectory_last_positions[i].y,
                            self->trajectory_last_positions[i].z);
            if(self->joint_space)
                glVertex3f (self->trajectory_last->states[i].angles[1],
                            self->trajectory_last->states[i].angles[2],
                            self->trajectory_last->states[i].angles[3]+2);

        }
        glEnd();


        //Draw the edges
      
        glLineWidth (8.0);
        float color_edge[] = {0.1, 1.0, 0.1, 1.0};
        glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_edge);
        glBegin (GL_LINE_STRIP);

        for (int i = 0; i < (self->trajectory_last->num_states - 2); i++){// -2 because rrt output has
            // been adding a redundant state
            if(self->xyz_space){
                glVertex3f (self->trajectory_last_positions[i].x,
                            self->trajectory_last_positions[i].y,
                            self->trajectory_last_positions[i].z);
                glVertex3f (self->trajectory_last_positions[i+1].x,
                            self->trajectory_last_positions[i+1].y,
                            self->trajectory_last_positions[i+1].z);
            }
            if(self->joint_space){
                glVertex3f (self->trajectory_last->states[i].angles[1],
                            self->trajectory_last->states[i].angles[2],
                            self->trajectory_last->states[i].angles[3]+2);
                glVertex3f (self->trajectory_last->states[i+1].angles[1],
                            self->trajectory_last->states[i+1].angles[2],
                            self->trajectory_last->states[i+1].angles[3]+2);
            }
            glEnd();
	}
    }
    
    
    // Draw the environment
    if (self->environment_last) {

        //Draw the starting point
        glPointSize (8.0);
        float color_vertex[] = {1, 0.7, 0.1, 1.0};
        glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_vertex);
        glBegin (GL_POINTS);

        for (int i = 0; i < 3; i++){
            if(self->xyz_space)
                glVertex3f (self->root_state_last.x,
                            self->root_state_last.y,
                            self->root_state_last.z);
            if(self->joint_space)
                glVertex3f (self->environment_last->root_state[1],
                            self->environment_last->root_state[2],
                            self->environment_last->root_state[3]+2);
        }
        glEnd();



        // Draw the goal region 
        float color_goal[] = { 0.8, 0.1, 0.8, ((double)(self->goal_opacity))/100.0};
        glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_goal);

        glPushMatrix ();

        double size_x;
        double size_y;
        double size_z;

        if(self->xyz_space) {
            glTranslated (self->goal_last_center.x, self->goal_last_center.y, self->goal_last_center.z);
            glRotatef (0.0, 0.0, 0.0, 1.0);
      
            size_x = fabs(self->goal_last_max.x - self->goal_last_min.x);
            size_y = fabs(self->goal_last_max.y - self->goal_last_min.y);
            size_z = fabs(self->goal_last_max.z - self->goal_last_min.z);
        }

        if(self->joint_space){
            glTranslated (self->environment_last->goal.center[1], self->environment_last->goal.center[2], self->environment_last->goal.center[3]+2);
            glRotatef (0.0, 0.0, 0.0, 1.0);

            size_x = self->environment_last->goal.size[1];
            size_y = self->environment_last->goal.size[2];
            size_z = self->environment_last->goal.size[3];
        }

        glScalef (size_x, size_y, size_z);

        bot_gl_draw_cube ();

        glPopMatrix ();


        // Draw the operating region


        if(self->joint_space){
            float color_operating[] = { 0.8, 0.1, 0.8, ((double)(self->goal_opacity))/100.0};
            glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_operating);

            glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);

            glPushMatrix ();

            glTranslated (self->environment_last->operating.center[1], self->environment_last->operating.center[2], self->environment_last->operating.center[3]+2);
            glRotatef (0.0, 0.0, 0.0, 1.0);

            size_x = self->environment_last->operating.size[1];
            size_y = self->environment_last->operating.size[2];
            size_z = self->environment_last->operating.size[3];
      
            glScalef (size_x, size_y, size_z);

            bot_gl_draw_cube ();

            glPopMatrix ();
        }

      
        // Draw the obstacles        
        for (int i = 0; i < self->environment_last->num_obstacles; i++) {

            float color_obstacles[] = { 0.3, 0, 0, ((double)(self->obstacle_opacity))/100.0};
            glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_obstacles);
            
            
            glPushMatrix ();
            
            glTranslated (self->environment_last->obstacles[i].center[0], self->environment_last->obstacles[i].center[1], self->environment_last->obstacles[i].center[2]);
            glRotatef (0.0, 0.0, 0.0, 1.0);
            
            glScalef (self->environment_last->obstacles[i].size[0], self->environment_last->obstacles[i].size[1], self->environment_last->obstacles[i].size[2]);
            
            bot_gl_draw_cube ();
            
            glPopMatrix ();
        }        
    }
    
    
    return;
}


static void renderer_graph_free (BotRenderer *renderer)
{
    RendererGraph *self = (RendererGraph*) renderer;
    
    if (self->graph_last)
        arm_rrts_graph_t_destroy (self->graph_last);
    free(self);
}


void add_graph_renderer_to_viewer (BotViewer* viewer, int render_priority, lcm_t* lcm)
{
    
    RendererGraph *self = new RendererGraph;
    BotRenderer *renderer = &self->renderer;
    self->lcm = lcm;
    self->viewer = viewer;

    self->have_goal = 0;
    
    renderer->draw = renderer_graph_draw;
    renderer->destroy = renderer_graph_free;
    renderer->widget = bot_gtk_param_widget_new();
    renderer->name = (char *) RENDERER_NAME;
    renderer->user = self;
    renderer->enabled = 1;
    
    self->obstacle_opacity = 50;
    self->goal_opacity = 50;
    self->edge_opacity = 100;
    self->draw_edges = true;
    self->joint_space = false;
    self->xyz_space = true;
    
    self->graph_last = NULL;
    self->environment_last = NULL;
    self->trajectory_last = NULL;
    
    BotGtkParamWidget *pw = BOT_GTK_PARAM_WIDGET(renderer->widget);

    bot_gtk_param_widget_add_double(pw, X_POS, BOT_GTK_PARAM_WIDGET_SLIDER, -2, 2, 0.01, 0);
    bot_gtk_param_widget_add_double(pw, Y_POS, BOT_GTK_PARAM_WIDGET_SLIDER, -2, 2, 0.01, 0);
    bot_gtk_param_widget_add_double(pw, Z_POS, BOT_GTK_PARAM_WIDGET_SLIDER, -2, 2, 0.01, 0);
    
    bot_gtk_param_widget_add_double(pw, ROLL, BOT_GTK_PARAM_WIDGET_SLIDER, -3, 3, 0.01, 0);
    bot_gtk_param_widget_add_double(pw, PITCH, BOT_GTK_PARAM_WIDGET_SLIDER, -3, 3, 0.01, 0);    
    bot_gtk_param_widget_add_double(pw, YAW, BOT_GTK_PARAM_WIDGET_SLIDER, -3, 3, 0.01, 0);

    bot_gtk_param_widget_add_double(pw, GRIPPER_POSITION, 
                                    BOT_GTK_PARAM_WIDGET_SLIDER, -150,150 , 0.1, 0);

    bot_gtk_param_widget_add_int(pw, ITERATIONS, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 1,20000 , 100, 5000);

    g_signal_connect (G_OBJECT(pw), "changed", G_CALLBACK (on_widget_submit), self);

    bot_gtk_param_widget_add_buttons(pw, SEND_COMMAND, NULL);

    bot_gtk_param_widget_add_booleans(pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, DRAW_GOAL, 1, NULL);
        
    bot_gtk_param_widget_add_booleans(pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, DRAW_EDGES, 1, NULL);
    bot_gtk_param_widget_add_double(pw, EDGE_OPACITY, 
                                    BOT_GTK_PARAM_WIDGET_SLIDER, 0, 100 , 1, 100);
  
    bot_gtk_param_widget_add_booleans(pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, VIEW_JOINT_SPACE, 0, NULL);
    bot_gtk_param_widget_add_booleans(pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, VIEW_XYZ_SPACE, 1, NULL);
   

    g_signal_connect (G_OBJECT(pw), "changed", G_CALLBACK (on_widget_change), self);
    
    // subscribe to messages
    arm_rrts_graph_t_subscribe (lcm, "ARM_RRTS_GRAPH", graph_message_handler, self);
    arm_rrts_environment_t_subscribe (lcm, "ARM_RRTS_ENVIRONMENT", environment_message_handler, self);
    arm_rrts_state_list_t_subscribe (lcm, "ARM_RRTS_STATE_LIST", trajectory_message_handler, self);
    
    bot_viewer_add_renderer(viewer, &self->renderer, render_priority);
}
