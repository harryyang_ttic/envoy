#include <iostream>
#include <ctime>
#include <getopt.h>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <arm_forward_kinematics/forward_kinematics.h>

#include <lcm/lcm.h>

#include <lcmtypes/arm_rrts_state_list_t.h>
#include <lcmtypes/arm_rrts_state_t.h>
#include <lcmtypes/arm_rrts_cmd_t.h>
#include <lcmtypes/arm_rrts_joint_cmd_t.h>
#include <lcmtypes/arm_rrts_environment_t.h>
#include <lcmtypes/arm_rrts_region_t.h>
#include <lcmtypes/arm_rrts_graph_t.h>
#include <lcmtypes/arm_rrts_vertex_t.h>
#include <lcmtypes/arm_rrts_edge_t.h>
#include <lcmtypes/arm_rrts_render_settings_t.h>
#include <lcmtypes/dynamixel_status_list_t.h>
#include <lcmtypes/arm_cmd_joint_status_list_t.h>
#include <lcmtypes/arm_cmd_trace_status_t.h>
#include <GL/gl.h>

#include <bot_lcmgl_client/lcmgl.h>

//#include <arm_ik/ik.h>
#include <arm_inverse_kinematics/InverseK.hpp>
#include <arm_forward_kinematics/ForwardK.hpp>
#include "rrts.hpp"
#include "system_single_integrator.h"


using namespace RRTstar;
using namespace SingleIntegrator;

using namespace std;

#define JOINT_PREFIX "arm_config.joints"
#define DYNAMIXEL_CMD_PRESENT_POS 36
#define GOAL_ERROR .1
#define PI M_PI//3.14159265

typedef Planner<State,Trajectory,System> planner_t;
typedef Vertex<State,Trajectory,System> vertex_t;


typedef struct _xyz_t {
    double x;
    double y;
    double z;
}xyz_t;

typedef struct _joint_info_t {
  
    int joint_id;
    double max_angle;
    double min_angle;
    int offset;
    int reference_servo;
    double deg_to_ticks;

}joint_info_t;



typedef struct _state_t {
    lcm_t *lcm;
    int no_collision_checks;
    //IK * ik;
    InverseK *ik;
    ForwardKinematics *fk;
    ForwardK *forwardK;
    ArmCollisionCheck *arm_cc;
    BotFrames *frames; 
    BotParam * param;
    GMainLoop *mainloop;
    bot_lcmgl_t * lcmgl;
    


    dynamixel_status_list_t *last_servo_status;
    arm_cmd_joint_status_list_t *last_status;
    int num_joints;
    joint_info_t *joints;
    double *limit_angles;

    int tree_history_step;
    int last_tree_time;

}state_t; 


int publishTree (lcm_t *lcm, planner_t& planner, System &system);
int publishTraj (lcm_t *lcm, int64_t id, planner_t& planner, System &system);
int publishEnvironment (lcm_t *lcm, region& regionOperating, region& regionGoal, list<region*>& obstacles, State rootState, int num_dimensions, int iterations);


/*void on_update(const lcm_recv_buf_t *rbuf, const char * channel, const dynamixel_status_list_t * msg, void * user) {
  
    state_t *self = (state_t *) user;

    if(self->last_servo_status)
        dynamixel_status_list_t_destroy(self->last_servo_status);

    self->last_servo_status = dynamixel_status_list_t_copy(msg);

    }*/

void on_status(const lcm_recv_buf_t *rbuf, const char * channel, const arm_cmd_joint_status_list_t * msg, void * user) {
  
    state_t *self = (state_t *) user;

    if(self->last_status)
        arm_cmd_joint_status_list_t_destroy(self->last_status);

    self->last_status = arm_cmd_joint_status_list_t_copy(msg);

}

int forward_kinematics(state_t *self, double *angles_rad, double out[3]){

    BotTrans end_effector;
    fk_forward_kinematics (self->fk, angles_rad, &end_effector);

    bot_frames_transform_vec (self->frames, "base", "local", end_effector.trans_vec, out);
    return 1;
}
   
void run_rrt_star(state_t *self, int64_t id, int iterations, double *result_angles, 
                  double *angle_tollerances, int skip_collision_checks){
    planner_t *rrts = new planner_t();
    
    cout << "RRTstar is alive" << endl;
            
    System *system;
    //Create the dynamical system
    if(self->no_collision_checks || skip_collision_checks){
        system = new System(NULL);
    }
    else{
        system = new System(self->arm_cc);
    }
        

    // Set number dimensions of configuration space
    system->setNumDimensions (self->num_joints);
    
    // Define the operating region
    system->regionOperating.setNumDimensions(self->num_joints);
              
    // Define the goal region
    system->regionGoal.setNumDimensions(self->num_joints);

    double *goal_joints = new double[self->num_joints];
  
    int index;
    fprintf(stderr, "Goal Angles\n");
    for(int i = 0; i < self->num_joints; i++){
        index = i;//goal_angles->qs[0].joint_index[i];
        double size = self->joints[index].max_angle - self->joints[index].min_angle;
        double center = (self->joints[index].max_angle + self->joints[index].min_angle)/2;
        system->regionOperating.center[index] = center;
        system->regionOperating.size[index] = size;
        
        system->regionGoal.center[index] = bot_mod2pi(result_angles[i]);
        fprintf(stderr, "Joint ID : %d Angles : %f\n", i, bot_to_degrees(result_angles[i]));
        goal_joints[index] = result_angles[i];
        system->regionGoal.size[index] = angle_tollerances[i];
    }
    
    //double weights [] = {1.0, .35, .3, .15, .02, .01};
    double weights [] = {1, 1, 1, 1, 1, 1};
    //double weights [] = {100, 35, 30, 15, 2, 1};
    system->setCostWeights(weights);

    double useDimensions [] = {1,1,1,1,1,0};
    system->useDimensions = useDimensions;
    
    double defaults [] = {0,0,0,0,0,0};
    system->sampleDefaults = defaults;
    for(int i = 0; i < self->num_joints; i++){
        defaults[i] = system->regionGoal.center[i];
    }
    
    // Add the system to the planner
    rrts->setSystem (*system);    

    // Set up the root vertex
    vertex_t &root = rrts->getRootVertex();
    State &rootState = root.getState();
  
    
    //Get address index for current position in status message
    if(!self->last_status){
        fprintf(stderr, "No last servo status\nArm is not on or arm command is not running");
        return;
    }

    double *current_joints = new double[self->num_joints];

    fprintf(stderr, "\n\n--------------------------\n");
    for(int j = 0; j < self->num_joints; j++){
        int joint_id = self->last_status->joints[j].joint_id;
        double cur_angle = bot_to_radians(self->last_status->joints[j].angle); 
        current_joints[joint_id] = cur_angle;
        rootState[joint_id] = cur_angle;
        cout << "Starting Joint ID :  " << joint_id << " - Angle " << bot_to_degrees(rootState[joint_id]) << endl;
    }

    double arm_base_to_local[12];
    if (!bot_frames_get_trans_mat_3x4 (self->frames, "base",
                                       "local",                                  
                                       arm_base_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from local to arm frame!\n");
        return;
    }

    double current_pos[3];

    forward_kinematics(self, current_joints, current_pos);
    
    double goal_pos[3];
    forward_kinematics(self, goal_joints, goal_pos);
    
    bot_lcmgl_point_size(self->lcmgl, 10);
    bot_lcmgl_begin(self->lcmgl, GL_POINTS);
    bot_lcmgl_color3f(self->lcmgl, 0, 0, 1);
    bot_lcmgl_vertex3f(self->lcmgl, current_pos[0], current_pos[1],current_pos[2]);
    bot_lcmgl_color3f(self->lcmgl, 1, 0, 0);
    bot_lcmgl_vertex3f(self->lcmgl, goal_pos[0], goal_pos[1],goal_pos[2]);
    bot_lcmgl_end(self->lcmgl);
    bot_lcmgl_switch_buffer(self->lcmgl);


    publishEnvironment (self->lcm, system->regionOperating, system->regionGoal, 
			system->obstacles, rootState, system->getNumDimensions(), iterations); 
    
    // Initialize the planner
    rrts->initialize ();
    
    // This parameter should be larger than 1.5 for asymptotic 
    //   optimality. Larger values will weigh on optimization 
    //   rather than exploration in the RRT* algorithm. Lower 
    //   values, such as 0.1, should recover the RRT.
    //rrts.setGamma (1.0);
    rrts->setGamma (.8);//0.4); //2.0

    clock_t start = clock();
    // Run the algorithm for some number of iterations
    for (int i = 0; i < iterations; i++){
        if(i%500==0) {
            fprintf(stderr, "Iter : %d\n",i);
            arm_collision_check_print_stats (self->arm_cc);
        }
	if(self->tree_history_step > 0 && rrts->numVertices != self->last_tree_time){
	    self->last_tree_time = rrts->numVertices;
	    if(rrts->numVertices < 1)
		continue;
	    if(rrts->numVertices%self->tree_history_step == 0){
		publishTree (self->lcm, *rrts, *system);
		publishTraj (self->lcm, id, *rrts, *system);
	    }
	}
        rrts->iteration ();
    }

    clock_t finish = clock();
    cout << "Time : " << ((double)(finish-start))/CLOCKS_PER_SEC << endl;

    cout << rrts->numVertices << endl;
    
    publishTree (self->lcm, *rrts, *system);
    
    publishTraj (self->lcm, id, *rrts, *system);
    delete system;
    delete rrts;
    delete goal_joints;
    delete current_joints;
}

void on_joint_command(const lcm_recv_buf_t *rbuf, const char * channel, 
                      const arm_rrts_joint_cmd_t * msg, void * user) {
    state_t *self = (state_t *) user;

    if(msg->no_joints != self->num_joints){
        fprintf(stderr, "No of goal joints not equal to arm joints - error - returning\n");
        return;
    }

    run_rrt_star(self, msg->id, msg->iterations, msg->joint_angles, msg->joint_tollerances, msg->skip_collision_checks);    
}

void on_command(const lcm_recv_buf_t *rbuf, const char * channel, 
                const arm_rrts_cmd_t * msg, void * user) {
    state_t *self = (state_t *) user;
    
    fprintf(stderr, "Calling RRT Solver\n");

    double local_to_arm_base[12];
    if (!bot_frames_get_trans_mat_3x4 (self->frames, "local",
                                                  "base",                                  
                                                  local_to_arm_base)) {
        fprintf (stderr, "Error getting bot_frames transformation from local to arm frame!\n");
        return;
    }

    BotTrans ef_to_local;
    ef_to_local.trans_vec[0] = msg->xyz[0];
    ef_to_local.trans_vec[1] = msg->xyz[1];
    ef_to_local.trans_vec[2] = msg->xyz[2];

    bot_roll_pitch_yaw_to_quat(msg->ef_rpy, ef_to_local.rot_quat);

    BotTrans local_to_base; 
    bot_frames_get_trans(self->frames, "local", "base", &local_to_base);
    
    BotTrans ef_to_base; 
    bot_trans_apply_trans_to(&local_to_base, &ef_to_local, &ef_to_base);
    
    double pos[3] = {ef_to_base.trans_vec[0], ef_to_base.trans_vec[1], ef_to_base.trans_vec[2]};

    double rpy[3] = {0};
    double result_angles[6] = {0};

    bot_quat_to_roll_pitch_yaw(ef_to_base.rot_quat, rpy);
    double gripper = msg->gripper_pos;
    bool gripper_en = msg->gripper_enable;
    int iterations = msg->iterations;

    int status = self->ik->solvePosRPY(pos, rpy, result_angles);

    fprintf(stderr, "Status : %d\n", status);
    if(status != 0){
        fprintf(stderr, "Error - Inverse kinematics failed to find a solution - returning\n");
        return;        
    }

    fprintf(stderr, "IK Goal Joint Angles\n");
    for(int i=0; i < 6; i++){
        fprintf(stderr, "\tJoint [%d] - %f\n", i, bot_to_degrees(result_angles[i]));
    }

    double *tollerances = new double[self->num_joints];
    for(int i = 0; i < self->num_joints; i++){
        tollerances[i] = GOAL_ERROR;
    }

    run_rrt_star(self, msg->id, msg->iterations,result_angles, tollerances, msg->skip_collision_checks);    
    delete tollerances;
}

void on_render_settings(const lcm_recv_buf_t *rbuf, const char * channel, 
                const arm_rrts_render_settings_t * msg, void * user) {
    state_t *self = (state_t *) user;
    self->tree_history_step = msg->tree_history_step;
}

int publishEnvironment (lcm_t *lcm, region& regionOperating, region& regionGoal, list<region*>& obstacles, State rootState, int num_dimensions, int iterations) {
 
    // Publish the environment
    arm_rrts_environment_t *environment = (arm_rrts_environment_t*) calloc (1, sizeof(arm_rrts_environment_t));
    
    environment->num_iterations = iterations;
    environment->operating.center = (double *) calloc(num_dimensions, sizeof(double));
    environment->operating.size = (double *) calloc(num_dimensions, sizeof(double));
    environment->goal.center = (double *) calloc(num_dimensions, sizeof(double));
    environment->goal.size = (double *) calloc(num_dimensions, sizeof(double));
    environment->root_state = (double *) calloc(num_dimensions, sizeof(double));
       
    environment->num_dimensions = num_dimensions;
    environment->operating.num_dimensions = num_dimensions;
    environment->goal.num_dimensions = num_dimensions;

    for(int i = 0; i < num_dimensions; i++){
        environment->root_state[i] = rootState[i]; 
    }

    for(int i = 0; i < num_dimensions; i++){
        environment->operating.center[i] = regionOperating.center[i];
        environment->operating.size[i] = regionOperating.size[i];
        environment->goal.center[i] = regionGoal.center[i];
        environment->goal.size[i] = regionGoal.size[i];
    }
       
    environment->num_obstacles = obstacles.size();
    
    if (environment->num_obstacles > 0){ 
        environment->obstacles = (arm_rrts_region_t *) 
            calloc (environment->num_obstacles, sizeof(arm_rrts_region_t));
        environment->obstacles->center = (double *) calloc(num_dimensions, sizeof(double));
        environment->obstacles->size = (double *) calloc(num_dimensions, sizeof(double));
    }

    int idx_obstacles = 0;
    for (list<region*>::iterator iter = obstacles.begin(); iter != obstacles.end(); iter++){
        region* obstacleCurr = *iter;
        environment->obstacles[idx_obstacles].num_dimensions = num_dimensions;
	for(int i = 0; i < num_dimensions; i++){
            environment->obstacles[idx_obstacles].center[i] = obstacleCurr->center[i];
            environment->obstacles[idx_obstacles].size[i] = obstacleCurr->size[i];
        }
        idx_obstacles++;
    }
    
    arm_rrts_environment_t_publish (lcm, "ARM_RRTS_ENVIRONMENT", environment);

    free(environment->operating.center);
    free(environment->operating.size);
    free(environment->goal.center);
    free(environment->goal.size);
    free(environment->root_state);
    //    free(environment->obstacles->center);
    //free(environment->obstacles->size);
    free(environment);
    
    return 1;
}


int publishTraj (lcm_t *lcm, int64_t id, planner_t& planner, System &system) {
        
    cout << "Publishing trajectory -- start" << endl;

    int num_dimensions = system.getNumDimensions();
    
    vertex_t& vertexBest = planner.getBestVertex ();
   
    if (&vertexBest == NULL) {
        cout << "No best vertex" << endl;
        
        //TODO: remove this
        //Publish failure message
        arm_cmd_trace_status_t* msg = (arm_cmd_trace_status_t *) 
            calloc(1, sizeof(arm_cmd_trace_status_t));

        msg->utime = bot_timestamp_now();
        msg->waypoint_id = -1;
        msg->status = ARM_CMD_TRACE_STATUS_T_FAILED;
        msg->goal_id = -1;
        
        arm_cmd_trace_status_t_publish(lcm, "ARM_RRTS_FAILURE", msg);

        arm_cmd_trace_status_t_destroy(msg);
    
	
	arm_rrts_state_list_t* list = (arm_rrts_state_list_t *)
	    calloc(1, sizeof(arm_rrts_state_list_t));
	list->id = id;
	list->num_states = 0;
	list->states = NULL;
	
	arm_rrts_state_list_t_publish(lcm, "ARM_RRTS_STATE_LIST", list);
	
	arm_rrts_state_list_t_destroy(list);
	
        return 0;
    }

    cout << "Solution Found\n" <<endl;
    
    list<double*> stateList;
    
    planner.getBestTrajectory (stateList);
   
    arm_rrts_state_list_t *opttraj = (arm_rrts_state_list_t *) malloc (sizeof (arm_rrts_state_list_t));
    opttraj->id = id;
    opttraj->num_states = stateList.size() + 1;
    opttraj->states = (arm_rrts_state_t *) malloc (opttraj->num_states * sizeof (arm_rrts_state_t));
   
    int stateIndex = 0;
    for (list<double*>::iterator iter = stateList.begin(); iter != stateList.end(); iter++) { 
        double* stateRef = *iter;
        opttraj->states[stateIndex].id = stateIndex;
        opttraj->states[stateIndex].num_dimensions = num_dimensions;
      
        opttraj->states[stateIndex].angles = (double *) calloc(num_dimensions, sizeof(double));
        
        //fprintf(stderr, "Index [%d] \n", stateIndex);
        for(int i = 0; i < num_dimensions; i++){
            opttraj->states[stateIndex].angles[i] = stateRef[i];
	    // fprintf(stderr, "\tJoint [%d] - %f\n", i, bot_to_degrees(stateRef[i]));
	}
        delete [] stateRef;
      
        stateIndex++;
    }

    //add the goal as the last trajectory
    //fprintf(stderr, "Index [%d] \n", stateIndex);

    opttraj->states[stateIndex].id = stateIndex;
    opttraj->states[stateIndex].num_dimensions = num_dimensions;
    
    opttraj->states[stateIndex].angles = (double *) calloc(num_dimensions, sizeof(double));
    for(int i = 0; i < num_dimensions; i++){
        opttraj->states[stateIndex].angles[i] = system.regionGoal.center[i];
	//    fprintf(stderr, "\tJoint [%d] - %f\n", i, bot_to_degrees(opttraj->states[stateIndex].angles[i]));
    }
    
    //for (int iter = 0; iter < opttraj->num_states; iter++)
    //  cout << opttraj->states[iter].x << " " << opttraj->states[iter].y << " " << opttraj->states[iter].z << endl;
    
    
    arm_rrts_state_list_t_publish (lcm, "ARM_RRTS_STATE_LIST", opttraj);
    
    arm_rrts_state_list_t_destroy (opttraj);
    
    if(system.arm_cc){
        fprintf (stdout, "Collision check stats:\n");
        arm_collision_check_print_stats (system.arm_cc);
    }
    cout << "Publishing trajectory -- end" << endl;
    
    
    
    return 1;
}




int publishTree (lcm_t *lcm, planner_t& planner, System &system) {
    
    
    cout << "Publishing the tree -- start" << endl;
    
    int num_dimensions = system.getNumDimensions();
    
    arm_rrts_graph_t *graph = (arm_rrts_graph_t *) malloc (sizeof (arm_rrts_graph_t));
    graph->num_vertices = planner.numVertices;

    cout << planner.numVertices << endl; 
    
    
    if (graph->num_vertices > 0) {    
        
        graph->vertices = (arm_rrts_vertex_t *) malloc (graph->num_vertices * sizeof(arm_rrts_vertex_t));
        
        int vertexIndex = 0;
        for (list<vertex_t*>::iterator iter = planner.listVertices.begin(); iter != planner.listVertices.end(); iter++) {
            
            graph->vertices[vertexIndex].num_dimensions = num_dimensions;
            
            vertex_t &vertexCurr = **iter;
            State &stateCurr = vertexCurr.getState ();
            
            graph->vertices[vertexIndex].angles = (double *) calloc(num_dimensions, sizeof(double));
            for(int i = 0; i < num_dimensions; i++)
                graph->vertices[vertexIndex].angles[i] = stateCurr[i];
                        
            vertexIndex++;
            
        }
        
    }
    else {
        graph->vertices = NULL;
    }
    
    if (graph->num_vertices > 1) {
        
        graph->num_edges = graph->num_vertices - 1;
        graph->edges = (arm_rrts_edge_t *) malloc (graph->num_edges * sizeof(arm_rrts_edge_t));
        
        
        int edgeIndex = 0;
        for (list<vertex_t*>::iterator iter = planner.listVertices.begin(); iter != planner.listVertices.end(); iter++) {

	
            vertex_t &vertexCurr = **iter;
            
            vertex_t &vertexParent = vertexCurr.getParent();
            
            if ( &vertexParent == NULL ) 
                continue;
            
            State &stateCurr = vertexCurr.getState ();
            State &stateParent = vertexParent.getState();
            
            graph->edges[edgeIndex].src.num_dimensions = num_dimensions;

            graph->edges[edgeIndex].src.angles =
                (double *) calloc(num_dimensions, sizeof(double));
	
            for(int i = 0; i < num_dimensions; i++)
                graph->edges[edgeIndex].src.angles[i] = stateParent[i];

            graph->edges[edgeIndex].dst.num_dimensions = num_dimensions;
          
            graph->edges[edgeIndex].dst.angles = 
                (double *) calloc(num_dimensions, sizeof(double));
	    
            for(int i = 0; i < num_dimensions; i++)
                graph->edges[edgeIndex].dst.angles[i] = stateCurr[i];
            
            edgeIndex++;
        }
        
    }
    else {
        graph->num_edges = 0;
        graph->edges = NULL;
    }
    
    arm_rrts_graph_t_publish (lcm, "ARM_RRTS_GRAPH", graph);
    
    arm_rrts_graph_t_destroy (graph);
    
    cout << "Publishing the tree -- end" << endl;
    
    return 1;
}

int main(int argc, char ** argv) {
    state_t *self = (state_t *) calloc(1, sizeof(state_t));

    const char *optstring = "vhn";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
                                  { "no_collision_check", no_argument, 0, 'n' },
				  { "verbose", no_argument, 0, 'v' }}; 

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
            fprintf(stderr, "No help found - please add help\n");
	    break;
        case 'v':
            fprintf(stderr, "No help found - please add help\n");
	    break;
        case 'n':
            self->no_collision_checks = 1;
            fprintf(stderr, "No collision checks\n");
	    break;
        }
    }
    
    // Get lcm
    self->lcm = bot_lcm_get_global (NULL);
    bot_glib_mainloop_attach_lcm(self->lcm);
    self->ik = new InverseK();

    self->fk = fk_new();

    self->forwardK = new ForwardK() ;
    self->arm_cc = new ArmCollisionCheck (FALSE, TRUE);

    self->lcmgl = bot_lcmgl_init(self->lcm, "rrt_arm");

    self->tree_history_step = 0;

    //Get information from param server
    self->param = bot_param_new_from_server(self->lcm, 0);
    self->frames = bot_frames_get_global (self->lcm, self->param);

    char key[1024]; 

    sprintf(key,"%s.num_joints", JOINT_PREFIX);
    bot_param_get_int(self->param, key, &self->num_joints);

    self->joints = (joint_info_t *) calloc(self->num_joints, sizeof(joint_info_t));

    char** joint_names = bot_param_get_subkeys(self->param, JOINT_PREFIX);
  
    int id_list[self->num_joints];
    for(int i = 0; i < self->num_joints; i++){
        sprintf(key, "%s.%s.joint_index", JOINT_PREFIX, joint_names[i]);
        bot_param_get_int(self->param, key, &id_list[i]);
    }

    for(int i = 0; i < self->num_joints; i++){
        sprintf(key, "%s.%s.joint_index", JOINT_PREFIX, joint_names[id_list[i]]);
        bot_param_get_int(self->param, key, &self->joints[i].joint_id);

        sprintf(key, "%s.%s.deg_to_ticks", JOINT_PREFIX, joint_names[id_list[i]]);
        bot_param_get_double(self->param, key, &self->joints[i].deg_to_ticks);

        sprintf(key, "%s.%s.servos", JOINT_PREFIX, joint_names[id_list[i]]);
        char **servos = bot_param_get_subkeys(self->param, key);
    
        for (int j = 0; servos[j] != NULL; j++){
            sprintf(key, "%s.%s.servos.%s.use_for_frame", JOINT_PREFIX, joint_names[id_list[i]], servos[j]);
            int use_for_frame;
            bot_param_get_int(self->param, key, &use_for_frame);
            if(use_for_frame){
                char servo_sub[1024];
                sprintf(servo_sub, "%s.%s.servos.%s", JOINT_PREFIX, joint_names[id_list[i]], servos[j]);
                sprintf(key, "%s.servo_index", servo_sub);
                bot_param_get_int(self->param, key, &self->joints[i].reference_servo);
	
                sprintf(key, "%s.offset", servo_sub);
                bot_param_get_int(self->param, key, &self->joints[i].offset);
	
                int min;
                sprintf(key, "%s.min_range", servo_sub);
                bot_param_get_int(self->param, key, &min);
                self->joints[i].min_angle = (min - self->joints[i].offset)/self->joints[i].deg_to_ticks*PI/180;

                int max;
                sprintf(key, "%s.max_range", servo_sub);
                bot_param_get_int(self->param, key, &max);
                self->joints[i].max_angle = (max - self->joints[i].offset)/self->joints[i].deg_to_ticks*PI/180;

            }
        }
    }

    for(int k = 0; k < self->num_joints; k++){
        cout << "Joint id: " << self->joints[k].joint_id << " Ref Servo: " << self->joints[k].reference_servo << " Offset: " << self->joints[k].offset << " Max_angle: " << self->joints[k].max_angle << " Min_angle: " << self->joints[k].min_angle << " Deg_to_ticks: " << self->joints[k].deg_to_ticks << endl;
    }

    // Get inverse kinematics
    //self->ik = ik_get_global(self->lcm);
  
    //subscribe
    arm_rrts_cmd_t_subscribe(self->lcm, "ARM_RRTS_COMMAND", &on_command, self);
    arm_cmd_joint_status_list_t_subscribe(self->lcm, "ARM_JOINT_STATUS", &on_status, self);
    arm_rrts_joint_cmd_t_subscribe(self->lcm, "ARM_RRTS_JOINT_COMMAND", &on_joint_command, self);
    arm_rrts_render_settings_t_subscribe(self->lcm, "ARM_RRTS_RENDER_SETTINGS", &on_render_settings, self);

    self->mainloop = g_main_loop_new(NULL, FALSE);

    g_main_loop_run(self->mainloop);
    return 0;

    free(self);
}
