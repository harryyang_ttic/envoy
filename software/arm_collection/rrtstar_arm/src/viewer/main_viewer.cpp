#include <iostream>
#include <ctime>

#include <bot_core/bot_core.h>
#include <bot_vis/bot_vis.h>
#include <bot_param/param_client.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <rrtstar_arm_renderer/renderer_arm_rrts.h>

using namespace std;


typedef struct {
    BotViewer *viewer;
    lcm_t *lcm;
    BotParam *param;
    BotFrames *frame;
} viewer_app_t;



int main(int argc, char *argv[])
{
    
    gtk_init(&argc, &argv);
    glutInit(&argc, argv);
    g_thread_init(NULL);
    
    setlinebuf(stdout);
    
    viewer_app_t app;
    memset(&app, 0, sizeof(app));
    
    
    BotViewer *viewer = bot_viewer_new("Viewer");
    app.viewer = viewer;
    app.lcm = lcm_create(NULL);
    app.param = bot_param_get_global (app.lcm, 0);
    app.frame =  bot_frames_get_global (app.lcm, app.param);
    bot_glib_mainloop_attach_lcm(app.lcm);
    
    // setup renderers
    bot_viewer_add_stock_renderer(viewer, BOT_VIEWER_STOCK_RENDERER_GRID, 1); 
    setup_renderer_arm_rrts (viewer, 1, app.param, app.frame, app.lcm);
    
    // run the main loop
    gtk_main();

    // cleanup
    bot_viewer_unref(viewer);    
    
    cout << "RRTstar is alive" << endl;
    
    return 1;
}

