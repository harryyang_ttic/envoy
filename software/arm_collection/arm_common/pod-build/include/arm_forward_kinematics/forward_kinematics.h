#ifndef __FORWARD_KINEMATICS_H
#define __FORWARD_KINEMATICS_H

#include <lcm/lcm.h>
#include <bot_param/param_client.h>
#include <bot_core/bot_core.h>



#ifdef __cplusplus
extern "C" {
#endif

typedef struct _bot_joint_t {
    double deg_to_tick;
    char *relative_to;
    BotTrans base_frame_to_parent;
} bot_joint_t;


typedef struct _bot_joint_list_t {
    int no_joints;
    bot_joint_t *joints;
} bot_joint_list_t;


typedef struct _forward_kinematics_t ForwardKinematics;

struct _forward_kinematics_t 
{
    lcm_t *lcm;

    bot_joint_list_t jlist;
};


ForwardKinematics *fk_new();
void fk_destroy (ForwardKinematics *fk);
void fk_forward_kinematics (ForwardKinematics *fk, double *angles_rad, BotTrans *end_effector);





#ifdef __cplusplus
}
#endif

#endif
