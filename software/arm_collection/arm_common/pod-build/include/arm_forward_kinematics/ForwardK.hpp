#ifndef __FORWARD_K_H
#define __FORWARD_K_H

#include <lcm/lcm.h>
#include <bot_param/param_client.h>
#include <arm_forward_kinematics/forward_kinematics.h>
#include <bot_core/bot_core.h>
#include <kdl/chain.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainiksolverpos_lma.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>
#include <kdl/chainiksolverpos_nr.hpp>
#include <kdl/frames_io.hpp>
#include <stdio.h>
#include <iostream>
#include <arm_interface/arm_interface.h>
#include "chainfksolverpos_recursive_joint_limits.hpp"
//#ifdef __cplusplus
//extern "C" {
//#endif

class ForwardK{
public:
    lcm_t *lcm;
    
    KDL::Chain chain;
    KDL::ChainFkSolverPos_recursive *fksolver;
    KDL::ChainFkSolverPos_recursive_joint_limits *fksolver_limits;
    ForwardKinematics *fk;
    ForwardK();
    double getAngleFromTicks(int ticks, int offset, int sign);
    int getJointsToBotFrames(double *angles, BotTrans *trans);
    BotTrans* getJointPos(double *angles, int *status, int *no_joints);
    void getJointsToBotFramesUsingBotTrans(double *angles, BotTrans *trans);
    KDL::Frame getJointsToFrame(double *angles, int *status);
    KDL::Frame getJointsToFrameLimited(double *angles, int *status);
    int getJointsToBotFramesLimited(double *angles, BotTrans *trans);
};

#endif
