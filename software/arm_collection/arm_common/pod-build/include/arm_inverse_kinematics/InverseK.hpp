#ifndef __INVERSE_K_H
#define __INVERSE_K_H

#include <lcm/lcm.h>
#include <bot_param/param_client.h>
#include <bot_core/bot_core.h>
#include <kdl/chain.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainiksolverpos_lma.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>
#include <kdl/chainiksolverpos_nr.hpp>
#include <kdl/frames_io.hpp>
#include <stdio.h>
#include <iostream>
#include <arm_interface/arm_interface.h>
#include <arm_forward_kinematics/chainfksolverpos_recursive_joint_limits.hpp>

//#ifdef __cplusplus
//extern "C" {
//#endif

class InverseK{
public:
    lcm_t *lcm;
    
    KDL::Chain chain;
    KDL::ChainFkSolverPos_recursive *fksolver;
    KDL::ChainFkSolverPos_recursive_joint_limits *fksolver_limits;
    KDL::ChainIkSolverPos_NR *iksolver1;
    KDL::ChainIkSolverVel_pinv *iksolver1v;
    
    //second solver 
    KDL::ChainIkSolverPos_LMA *solver;

    InverseK();

    double getAngleFromTicks(int ticks, int offset, int sign);
    void testIK(double *angles);                
    int solvePos(double *xyz, double *angles_rad);
    int solvePos(KDL::Frame cartpos, double *angles_rad);

    int solvePosRPY(double xyz[3], double rpy[3], double *angles_rad);
    int solvePosQuat(double xyz[3], double quat[4], double *angles_rad);

    int solvePosLMA(double *xyz, double *angles_rad);
    int solvePosLMA(KDL::Frame cartpos, double *angles_rad);

    int solvePosLMARPY(double xyz[3], double rpy[3], double *angles_rad);
    int solvePosLMAQuat(double xyz[3], double quat[4], double *angles_rad);
};

#endif
