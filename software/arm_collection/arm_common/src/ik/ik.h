
//ik.h
#include <bot_core/bot_core.h>
#include <bot_param/param_client.h>
#include <bot_frames/bot_frames.h>
#include <stdio.h>
#include <string.h>

#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif

#include <math.h>
#include <stdbool.h>
#include <lcm/lcm.h>

#include <lcmtypes/arm_cmd_ik_t.h>
#include <lcmtypes/arm_cmd_joint_list_t.h>
#include <arm_interface/arm_interface.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _IK{
  joint_list_t jlist;
  BotFrames *frames;
  lcm_t *lcm;

  int num_joints;
  int num_servos;
  char** joint_names;

  GHashTable *h_servos;
  GHashTable *h_joints;
} IK;


typedef struct _qs_t {

  int               num_qs;
  int               *joint_index;
  double             *q_vals;
  double            speed;

} qs_t;

typedef struct _qs_list_t {
  
  int                 num_lists;
  qs_t                *qs;

} qs_list_t;

/*
places joint at frame_name in location xyz[3] in orientation rpy[3]; joint angles are stored at q_out.

Returns true if succesful, false if failed

all angles taken to be in radians. All lengths taken in meters.

maybe you want this to work for position and orientation of different joints? e.g. elbow here, with joint at this orientation?

work this out later. Maybe this should be included in cost calculation/constraints.

if at 0,0,z, then theta[base] will be set to [insert number here when you decide].
 */ 
bool ik_q_gen(IK *self,  double loc[3], double ef_rpy[3], double gripper_pos,  bool gripper_en, qs_list_t *q_out);

bool ik_is_in_self_collison(qs_t *qs, int num_joints, double *max_angles, double *min_angles);

IK *ik_get_global(lcm_t *lcm);

#ifdef __cplusplus
}
#endif
