//ik-frames.c
#include "../robot-frames.h"
#include "../list.h"
#include "ik-frames.h"
#include <bot_core/bot_core.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <stdbool.h>

typedef struct _ik_frame_t ik_frame_t;

struct _ik_frames_t {
	robot_frames_t *rf;
	list_head_t *frame_list;
};

struct _ik_frame_t{
	int independent_index;
	double length;
	double max;
	double min;

	int world_axis;
};

//function prototype
int ikf_parse_params(ik_frames_t *ikf, BotParam *param);
void ikf_calculate_lengths(ik_frames_t *ikf);
void ikf_calculate_world_axes(ik_frames_t *ikf);

ik_frames_t * ik_frames_new(BotParam * param){
	ik_frames_t *ikf = (ik_frames_t *)calloc (1, sizeof(ik_frames_t));
	ikf->rf = robot_frames_new(param);
	ikf->frame_list = list_new("ik frames");
	if (ikf_parse_params(ikf, param) != 0){
		printf ("Error: Parsing error!\n");
	}
	ikf_calculate_lengths(ikf);
	ikf_calculate_world_axes(ikf);
	return ikf;
}
void ik_frames_destroy(ik_frames_t *ikf){
	robot_frames_destroy(ikf->rf);
	if (ikf->frame_list != NULL){
		list_destroy(ikf->frame_list);
	}
	free(ikf);
}
int ik_frames_get_size(ik_frames_t *ikf){
	return robot_frames_get_size(ikf->rf);
}
const char * ik_frames_get_root_name(ik_frames_t *ikf){
	return robot_frames_get_root_name(ikf->rf);
}
char ** ik_frames_get_names(ik_frames_t * ikf){
	return robot_frames_get_names(ikf->rf);
}
int ik_frames_get_transform(	
									ik_frames_t * ikf,
									const char * from_frame,
									const char * to_frame,
									BotTrans *trans){

	return robot_frames_get_transform(ikf->rf, from_frame, to_frame, trans);
}
int ik_frames_get_initial_transform(
 									ik_frames_t * ikf,
									const char * from_frame,
									const char * to_frame,
									BotTrans *trans){

	return robot_frames_get_initial_transform(
										ikf->rf,
										from_frame,
										to_frame,
										trans);
}

int ik_frames_update_transform (
									ik_frames_t * ikf,
									const char * from_frame,
									const char * to_frame,
									BotTrans *trans){
	return robot_frames_update_transform(
										ikf->rf,
										from_frame,
										to_frame,
										trans);
}

const char * ik_frames_get_relative_to (
 									ik_frames_t * ikf,
									const char *frame_name){
	return robot_frames_get_relative_to ( ikf->rf, frame_name);
}

const char * ik_frames_get_next_name(
									ik_frames_t * ikf,
									const char *frame_name){
	return robot_frames_get_next_name( ikf->rf, frame_name);
}
int ik_frames_get_world_axis(
									ik_frames_t * ikf,
									const char *frame_name){
	ik_frame_t *frame = list_get_data_from_tag(ikf->frame_list, (char *)frame_name);
	return frame->world_axis;
}
double ik_frames_get_frame_length(
									ik_frames_t * ikf,
									const char *frame_name){
	ik_frame_t *frame = list_get_data_from_tag(ikf->frame_list, (char *)frame_name);
	return frame->length;
}

/**
 * ikf_parse_params
 *
 * parses the config files for ik specific components
 * Returns:
 *	0 = success
 * !0 = fail
 */

int ikf_parse_params(ik_frames_t *ikf, BotParam *param){
	//get the independent indexes out of the bot params
	//printf ("in %s\n", __func__);

	char key[200];
	int count 				= robot_frames_get_size(ikf->rf);
	char **names 			= robot_frames_get_names(ikf->rf);
	char *iv_name;

	ik_frame_t *frame = NULL;

	if (!bot_param_has_key(param, "arm_renderer")){
		printf ("Error: didn't find arm_renderer structure!\n");
		return -1;
	}
	for (int i = 0; i < count; i++){
		frame = (ik_frame_t *) calloc(1, sizeof(ik_frame_t));
		list_add(ikf->frame_list, names[i], frame);
		snprintf(&key[0], 200, "arm_renderer.%s.independent_variable", names[i]);
		if (bot_param_get_str(param, &key[0], &iv_name) == -1){
			//default to z axis to adhere to DH parameters
			printf ("Didn't find a value for %s\n", &key[0]);
			printf ("Defaulting to YAW assuming DH parameters\n");
			frame->independent_index = IK_YAW;
			continue;
		}
		if (strcmp (iv_name, "roll") == 0){
			frame->independent_index = IK_ROLL;
		}
		else if (strcmp (iv_name, "pitch") == 0){
			frame->independent_index = IK_PITCH;
		}
		else if (strcmp (iv_name, "yaw") == 0){
			frame->independent_index = IK_YAW;
		}
		else {
			printf ("Didn't find a recognizable RPY value for %s\n", &key[0]);
			printf ("Defaulting to YAW assuming DH parameters\n");
			frame->independent_index = IK_YAW;
		}
		snprintf(&key[0], 200, "arm_renderer.%s.min_range", names[i]);
		if (bot_param_get_double(param, &key[0], &frame->min) == -1){
			printf ("didn't find min value\n");
		}

		snprintf(&key[0], 200, "arm_renderer.%s.max_range", names[i]);
		if (bot_param_get_double(param, &key[0], &frame->max) == -1){
			printf ("didn't find max value\n");
		}

	}
	free (names);
	printf ("exiting %s\n", __func__);
	return 0;
}

//this needs to get done at the beginning
void ikf_calculate_lengths(ik_frames_t *ikf){
	//printf ("in %s\n", __func__);
	//go through each item in the list, and get the length
	int count 			= robot_frames_get_size(ikf->rf);

	for (int i = 0; i < count; i++){

		ik_frame_t *frame 		= (ik_frame_t *) list_get_data_from_index(ikf->frame_list, i);
		const char * name 		= list_get_tag_from_index(ikf->frame_list, i);		
		const char * next_name 	= robot_frames_get_next_name(ikf->rf, name);

		//default the length to 0
		frame->length = 0;

		if (next_name == NULL){
			continue;
		}
		BotTrans trans;

		if (robot_frames_get_initial_transform(
											ikf->rf,
											name,
											next_name,
											&trans) == 0){
			printf ("Couldn't get transform from %s -> %s\n", name, next_name);
		}
		
		//calculate out the length of the frame
		frame->length = sqrt(
							trans.trans_vec[0] * trans.trans_vec[0] +
							trans.trans_vec[1] * trans.trans_vec[1] +
							trans.trans_vec[2] * trans.trans_vec[2]);
		//printf ("length for %s : %4f\n", name, frame->length);

	}

}

//this calculates the contribution of a frames axis, independent of location
void ikf_calculate_world_axes(ik_frames_t *ikf){
	//it doesn't matter if I use the initial frame, or the working frame

	/*
	the DH only rotate around the Z axis, but that is useless for calculating location
	so the actual axis of rotation that a frame contributes is generated here
	*/
	//printf ("in %s\n", __func__);

	int count 					= robot_frames_get_size(ikf->rf);
	const char * root_name	 	= robot_frames_get_root_name(ikf->rf);

	for (int i = 0; i < count; i++){
		ik_frame_t *frame 		= (ik_frame_t *) 
							list_get_data_from_index(ikf->frame_list, i);
		const char * name 		= 
							list_get_tag_from_index(ikf->frame_list, i);
		const char * relative_name = 
							robot_frames_get_relative_to(ikf->rf, name);
		if (strcmp(name, root_name) == 0){
			continue;
		}
		//const char * next_name	= robot_frames_get_next_name(ikf->rf, name);
		
		frame->world_axis = IK_YAW;
		
		BotTrans trans;
		if (robot_frames_get_initial_transform(
											ikf->rf,
											root_name,
											relative_name,
											&trans) == 0){
			printf ("Couldn't get transform from %s -> %s\n", 
											root_name, 
											name);
		}
	
		double ind_vector[3] = {0.0, 0.0, 0.0};
		switch (frame->independent_index){
			case (IK_ROLL):
				ind_vector[0] = 1.0;
			break;
			case (IK_PITCH):
				ind_vector[1] = 1.0;
			break;
			case (IK_YAW):
			default: 
				ind_vector[2] = 1.0;
			break;
		}

		//determine what the angle of rotation of a local frame is
		//rotate the unit vector about all the rotations from the root frame until the present frame
		double world_vector[3] = {0.0, 0.0, 0.0};
		bot_trans_rotate_vec(&trans, ind_vector, world_vector);	
		//printf ("%s->%s:\n", root_name, relative_name);
		//find the effect of the previous frame rotations have on this one
		//printf ("rotation = %4f, %4f, %4f, %4f\n", 
					//trans.rot_quat[0],
					//trans.rot_quat[1],
					//trans.rot_quat[2],
					//trans.rot_quat[3]);
		//printf ("independent vector = %4f, %4f, %4f\n", 
		//			ind_vector[0], 
		//			ind_vector[1], 
		//			ind_vector[2]);

		//printf ("world vector = %4f, %4f, %4f\n",
			//		world_vector[0],
			//		world_vector[1],
			//		world_vector[2]);


		if (world_vector[0] <= -0.90 || world_vector[0] >= 0.90){
			//printf ("Roll\n");
			frame->world_axis = IK_ROLL;
		}
		else if (world_vector[1] <= -0.90 || world_vector[1] >= 0.90){
			//printf ("Pitch\n");
			frame->world_axis = IK_PITCH;
		}
		else {
			//printf ("Yaw\n");
			frame->world_axis = IK_YAW;
		}
				
			
	}
	
	
}

double ik_frames_get_frame_max(
								ik_frames_t *ikf,
								const char *frame_name){

	ik_frame_t *frame = list_get_data_from_tag(ikf->frame_list, (char *)frame_name);
	return frame->max;
}
double ik_frames_get_frame_min (
 									ik_frames_t *ikf,
									const char *frame_name){

	ik_frame_t *frame = list_get_data_from_tag(ikf->frame_list, (char *)frame_name);
	return frame->min;
}

