//ik-frames.h

/**
 *	ik-frames
 *
 *	an extension of robot-frames, adding in features that would be useful for ik
 *
 */

#ifndef __IK_FRAMES__
#define __IK_FRAMES__

#include <bot_vis/bot_vis.h>
#include <bot_frames/bot_frames.h>

typedef struct _ik_frames_t ik_frames_t;

//ROLL: 	Rotate counterclockwise around x axis
//PITCH: 	Rotate counterclockwise around y axis
//YAW: 		Rotate counterclockwise aroudn z axis
enum IK_RPY{
	IK_ROLL = 0,
	IK_PITCH = 1,
	IK_YAW = 2
};


/**
 * ik_frames_new:
 *
 * Constructor
 * Returns: a newly allocated ik_frames_t structure
 */
ik_frames_t * ik_frames_new(BotParam * param);

/**
 * ik_frames_destroy
 *
 * Releases memory used by a ik_frames_t structure
 */
void ik_frames_destroy(ik_frames_t *ikf);

/**
 * ik_frames_get_size
 *
 * get the size of the kinematic chain
 * Returns: size of the kinematica chain
 */
int ik_frames_get_size(ik_frames_t *ikf);

/**
 * ik_frames_get_root_name
 *
 * returns the name fo the root frame of the ik_frames
 * Returns: String
 */
const char * ik_frames_get_root_name(ik_frames_t *ikf);

/**
 * ik_frames_get_names
 *
 * returns a list of all the names of the coordinate frames
 * Returns: Pointer to a list of strings (YOU MUST RELEASE THIS!)
 */
char ** ik_frames_get_names(ik_frames_t * ikf);

/**
 * ik_frames_get_transform
 *
 * from the "from_frame", and "to_frame" populate the transform structure
 * Returns: 0 for fail, and 1 for success
 */
int ik_frames_get_transform(	
									ik_frames_t * ikf,
									const char * from_frame,
									const char * to_frame,
									BotTrans *trans);
/**
 * ik_frames_get_initial_transform
 *
 * get the initial transform that was called out in the bot frames
 * Returns: 0 for fail, and 1 for success
 */
int ik_frames_get_initial_transform(
 									ik_frames_t * ikf,
									const char * from_frame,
									const char * to_frame,
									BotTrans *trans);

/**
 * ik_frames_update_transform
 *
 * update the command_frame indicated by "from_frame", and "to_frame"
 * with the transform specified at "trans"
 * Returns: 0 for fail, and 1 for success
 */
int ik_frames_update_transform (
									ik_frames_t * ikf,
									const char * from_frame,
									const char * to_frame,
									BotTrans *trans);

/**
 * ik_frames_get_relative_to
 *
 * gets the relative_to frame
 * Returns: returns a pointer to the relative_to frame
 */
const char * ik_frames_get_relative_to (
 									ik_frames_t * ikf,
									const char *frame_name);

/**
 * ik_frames_get_next_name
 *
 * gets the name of the next frame in the kinematic chain
 * Returns: returns a name of the next from
 */

const char * ik_frames_get_next_name(
									ik_frames_t * ikf,
									const char *frame_name);

/**
 * ik_frames_get_world_axis
 *
 * gets the frame of the independent axis that this frame contributes in the world frame
 * due to the fact that frame (n) changes the angle of frame (n + 1) it is helpful to find
 * the contribution of a frame independent of current location
 * Returns a numerical representation of the independent index use IK_RPY
 *	for reference
 */
int ik_frames_get_world_axis(
									ik_frames_t * ikf,
									const char *frame_name);

/**
 * ik_frames_get_frame_length
 *
 * gets the length of the specified frame
 * Returns the length of the specified rigid body frame
 */
double ik_frames_get_frame_length (
									ik_frames_t * ikf,
									const char *frame_name);

/**
 * ik_frames_get_frame_max
 *
 * gets the maximum radian value the frame can rotate
 * Returns a double of the max
 */

double ik_frames_get_frame_max (
									ik_frames_t *ikf,
									const char *frame_name);

 /**
 * ik_frames_get_frame_min
 *
 * gets the minimum radian value the frame can rotate
 * Returns a double of the min
 */
double ik_frames_get_frame_min (
 									ik_frames_t *ikf,
									const char *frame_name);
#endif
