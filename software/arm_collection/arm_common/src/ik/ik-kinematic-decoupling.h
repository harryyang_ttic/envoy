//ik-kinematic-decoupling

/*
 * A simple form of inverse kinematic that separates out the positioning
 * of the arm, and the rotation of the wrist.
 *
 * assumptions are made about the kinematic chain, but these are kept
 * to a minimum.
 *
 * the goal is to produce a close form equation that takes as input
 * the current location of the arm, and the desired x, y, z, roll, pitch, yaw
 * and the system will output a list of joint values
 *
 * required files:
 * ik-frames
 * list
 */
#ifndef __IK_KINEMATIC_DECOUPLING__
#define __IK_KINEMATIC_DECOUPLING__

#include <stdbool.h>
#include <bot_frames/bot_frames.h>


typedef struct _kd_t kd_t;

/*
 * kd_new
 *
 * Description: generates a new kinematic decoupling structure (kd_t)
 *
 * Parameters:
 *	params: Configuration file handle
 *
 * Return:
 *	populated kd_t structure
 *	NULL on error
 */
kd_t * kd_new(BotParam * params);

/*
 * kd_destroy
 *
 * Description: frees a kinematic decoupling structure (kd_t)
 *
 * Parameters:
 *	kd: self
 *
 * Return:
 *	void
 */
void kd_destroy(kd_t * kd);

/*
 * kd_get_chain_size
 *
 * Description: gets the size of the kinematic chain
 *
 * Parameters:
 * 	kd: self
 *
 * Return:
 * 	number of joints in chain
 */
 int kd_get_chain_size(kd_t *kd);

/*
 * kd_generate_qs
 *
 * Description: populates a list of q's (joint angle values) based on 
 * the present position of the kinematic chain, and the selected otuput,
 * and orientation
 *
 * Parameters:
 *	kd: self
 *	loc[3]: x, y, z
 *	rot_quat[3]: quaternion rotation
 *	present_qs: a list of the current joint angle values
 *	out_qs: a list of the output joint angle values (a pointer with enough
 *		space for the output list must be generated OUTSIDE of this function
 *	
 *	Return:
 *	 STATUS:
 *		0 = SUCCESS
 *		-1 = Failed to acheive both position, and orientation
 *		-2 = Failed to acheive only orientation
 *
 */
int kd_generate_qs(
				kd_t *kd,
				const double loc[3],
				const double ef_rpy[3],
				bool rot_dk,
				const double *present_qs,
				double *out_qs);

/*
 *
 * kd_get_frame_names
 *
 * Description: returns a list of the names of the kinematic chain
 * Note: this list must be destroyed after use
 *
 * Parameers:
 *	kd: self
 *
 * Return:
 *	a list of names of the kinematic chain (this list must be destroyed by 
 *	calling function
 *	NULL = Failed
 */
char ** kd_get_frame_names(kd_t *kd);

/*
 * kd_get_frame_max
 *
 * Description: returns the max value of the frame with the specified name
 *
 * Return:
 *	a double representing the maximum location the frame can travel to
 */
double kd_get_frame_max(kd_t *kd, const char *frame_name);

/*
 * kd_get_frame_min
 *
 * Description: returns the name value of the frame with the specified name
 *
 * Return:
 *	a double representing the minimum location the frame can travel to
 */
double kd_get_frame_min(kd_t *kd, const char *frame_name);



#endif //__IK_KINEMATIC_DECOUPLING__
