//arm_interface.h

#ifndef __ARM_INTERFACE_H__
#define __ARM_INTERFACE_H__

#include <lcm/lcm.h>
#include <lcmtypes/arm_cmd_joint_list_t.h>


#define STATUS_CHANNEL "ARM_STATUS"
#define COMMAND_CHANNEL "ARM_COMMAND"
#define DEBUG 1
#define MS_DELAY 10

typedef struct _servo_t servo_t;
typedef struct _joint_t joint_t;

struct _servo_t{
    int max_tick; 
    int min_tick; 
    int sign; 
    int offset;
    int use_for_frame;
    joint_t *parent;
    int servo_id;
};

struct _joint_t{
  int joint_id; 
  int no_servos;
  servo_t *servos;
  double deg_to_tick;
  char *joint_name;
  char *joint_frame;
  char *relative_to;
  double frame_translation[3];
  double frame_rpy[3];
  char *update_channel;
  int rpy_update_ind;
};

typedef struct _joint_list_t{
    int no_joints;
    joint_t *joints;
} joint_list_t;

#endif
