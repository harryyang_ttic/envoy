#include "forward_kinematics.hpp"
//#include <cstdio>

#define JOINT_PREFIX "arm_config.joints"
using namespace KDL;


void fk_destroy (ForwardKinematics *fk)
{

    if (!fk) return;

    
    if (!fk->jlist.joints)
        free (fk->jlist.joints);

    free (fk);
}


// Constructor
ForwardKinematics *fk_new()
{
    ForwardKinematics *fk = (ForwardKinematics *) calloc (1, sizeof(ForwardKinematics));
    BotParam *param;

    if (!(fk->lcm = bot_lcm_get_global (NULL))) {
        fk_destroy (fk);

        fprintf (stderr, "Unable to get LCM!\n");
        return NULL;
    }

    
    if (!(param = bot_param_new_from_server(fk->lcm, 0))) {
        fk_destroy (fk);
        
        fprintf (stderr, "Unable to get BotParam!\n");
        return NULL;
    }

    fk->chain = new Chain();
    //base yaw
    fk->chain->addSegment(Segment(Joint(Joint::RotZ),Frame(Vector(0.047625,0.0,0.0))));
    //shoulder
    fk->chain->addSegment(Segment(Joint(Joint::RotY),Frame(Vector(0.38735,0.0,0.0))));
    //elbow
    fk->chain->addSegment(Segment(Joint(Joint::RotY),Frame(Vector(0.381,0.0,0.0))));
    //lateral wrist
    fk->chain->addSegment(Segment(Joint(Joint::RotY),Frame(Vector(0.06985,0.0,0.0))));
    //twist wrist 
    fk->chain->addSegment(Segment(Joint(Joint::RotX),Frame(Vector(0.038735,0.0,0.0))));

    fk->fksolver = new ChainFkSolverPos_recursive(*fk->chain);

    fprintf(stderr, "Created forward kinematic chain\n");
    
    //get information from param server
    /*char key[1024]; 

    char** joint_names = bot_param_get_subkeys(param, JOINT_PREFIX);

    int num_joints = 0;
    if (joint_names) {
        for (int pind = 0; joint_names[pind] != NULL; pind++) 
            num_joints++;
    }

    fk->jlist.no_joints = num_joints; 
    fk->jlist.joints = (bot_joint_t *) calloc (fk->jlist.no_joints, sizeof(bot_joint_t));

    //incrementally fill them 
    for (int i=0; i< num_joints; i++) {

        sprintf (key,"%s.%s.joint_frame", JOINT_PREFIX, joint_names[i]);
       
        char *joint_frame;
        if (0 != bot_param_get_str (param, key, &joint_frame)){
            fprintf (stderr, "%s - frame not defined\n", key);
            fk_destroy (fk);
            return NULL;
        }
        sprintf (key,"%s.%s.relative_to", JOINT_PREFIX, joint_names[i]);
        if (0 != bot_param_get_str (param, key, &(fk->jlist.joints[i].relative_to))) {
            fprintf (stderr, "%s - relative to not defined\n", key);
            fk_destroy (fk);
            return NULL;
        }

        // get fixed transformation wrt parent
        sprintf(key,"coordinate_frames.%s.initial_transform.translation", joint_frame);
        bot_param_get_double_array (param, key, (fk->jlist.joints[i].base_frame_to_parent.trans_vec), 3);

        sprintf (key,"coordinate_frames.%s.initial_transform.rpy", joint_frame);
        double rpy[3];
        bot_param_get_double_array (param, key, rpy, 3);
        
        // Convert to radians
        rpy[0] = bot_to_radians (rpy[0]);
        rpy[1] = bot_to_radians (rpy[1]);
        rpy[2] = bot_to_radians (rpy[2]);

        bot_roll_pitch_yaw_to_quat (rpy, fk->jlist.joints[i].base_frame_to_parent.rot_quat);
        }*/
        
    return fk;
    
}

//this gives the end effector relative to the body - not base 
void fk_forward_kinematics (ForwardKinematics *fk, double *angles_rad, BotTrans *end_effector)
{
    // Create joint array
    unsigned int nj = fk->chain->getNrOfJoints();
    KDL::JntArray jointpositions = JntArray(nj);
    fprintf(stderr, "Number of joints : %d\n", nj);
    for(unsigned int i=0;i<nj;i++){
        jointpositions(i)= angles_rad[i];//30 * M_PI / 180.0;
    }

    KDL::Frame cartpos;    
           
    fprintf(stderr, "Calling solver\n");
    // chain = new Chain();
    Chain chain;
    //base yaw
    chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Vector(0.047625,0.0,0.0))));
    //shoulder
    chain.addSegment(Segment(Joint(Joint::RotY),Frame(Vector(0.38735,0.0,0.0))));
    //elbow
    chain.addSegment(Segment(Joint(Joint::RotY),Frame(Vector(0.381,0.0,0.0))));
    //lateral wrist
    chain.addSegment(Segment(Joint(Joint::RotY),Frame(Vector(0.06985,0.0,0.0))));
    //twist wrist 
    chain.addSegment(Segment(Joint(Joint::RotX),Frame(Vector(0.038735,0.0,0.0))));


    ChainFkSolverPos_recursive fksolver = ChainFkSolverPos_recursive(chain);
    //fk->fksolver->JntToCart(jointpositions,cartpos);
    fksolver.JntToCart(jointpositions,cartpos);
    fprintf(stderr, "Done solver\n");
    std::cout << cartpos <<std::endl;

    // Initialize the end effector pose in its respective frame
    /*double rpy[] = {0.0, 0.0, 0.0};
    bot_roll_pitch_yaw_to_quat (rpy, end_effector->rot_quat);
    end_effector->trans_vec[0] = 0.0;
    end_effector->trans_vec[1] = 0.0;
    end_effector->trans_vec[2] = 0.0;

    for (int i=(fk->jlist.no_joints-1); i>=0; i--) {
        
        double rpy[] = {0, 0, angles_rad[i]};
        BotTrans base_frame_to_parent;
        BotTrans articulated_frame_to_base_frame;

        bot_roll_pitch_yaw_to_quat (rpy, articulated_frame_to_base_frame.rot_quat);
        articulated_frame_to_base_frame.trans_vec[0] = 0;
        articulated_frame_to_base_frame.trans_vec[1] = 0;
        articulated_frame_to_base_frame.trans_vec[2] = 0;

        bot_trans_apply_trans (&articulated_frame_to_base_frame, &(fk->jlist.joints[i].base_frame_to_parent));
        bot_trans_apply_trans (end_effector, &articulated_frame_to_base_frame);

        //fprintf (stdout, "Transforming to %s\n", fk->jlist.joints[i].relative_to);
        //fprintf(stderr, "Transform : %f,%f,%f\n", end_effector->trans_vec[0], end_effector->trans_vec[1], 
        //      end_effector->trans_vec[2]);
        }*/

    
 
    return;
}
