#include "ForwardK.hpp"


int main() {
    ForwardK fk;
    double angles_rad[] = {bot_to_radians(200), 0,0,0,0};//bot_to_radians(20), bot_to_radians(20), bot_to_radians(20), bot_to_radians(20), bot_to_radians(20)};////bot_to_radians(30), bot_to_radians(10), bot_to_radians(10)};//{20, 0, 10, 0, 30};
    /*double angles_rad[] = {bot_to_radians(13.183594), bot_to_radians(0), bot_to_radians(-44.531250), 
                           bot_to_radians(-37.792969), bot_to_radians(0.292969), 
                           bot_to_radians(-25.195312)};////bot_to_radians(30), bot_to_radians(10), bot_to_radians(10)};//{20, 0, 10, 0, 30};*/

    int status = 0;
    KDL::Frame cartpos = fk.getJointsToFrame(angles_rad, &status);
    
    BotTrans trans;
    fk.getJointsToBotFrames(angles_rad, &trans);
    fprintf (stdout, "KDL Method end_effector: trans = [%.6f, %.6f, %.6f]\n",
             trans.trans_vec[0], trans.trans_vec[1], trans.trans_vec[2]);

    double rpy[3] = {0};
    bot_quat_to_roll_pitch_yaw(trans.rot_quat, rpy);

    fprintf (stdout, "\tend_effector: RPY = [%.6f, %.6f, %.6f]\n",
             bot_to_degrees(rpy[0]), bot_to_degrees(rpy[1]),
             bot_to_degrees(rpy[2]));

    fk.getJointsToBotFramesUsingBotTrans(angles_rad, &trans);

    bot_quat_to_roll_pitch_yaw(trans.rot_quat, rpy);

    fprintf (stdout, "\nBotTrans end_effector: trans = [%.6f, %.6f, %.6f]\n",
             trans.trans_vec[0], trans.trans_vec[1], trans.trans_vec[2]);

    fprintf (stdout, "\tend_effector: RPY = [%.6f, %.6f, %.6f]\n",
             bot_to_degrees(rpy[0]), bot_to_degrees(rpy[1]),
             bot_to_degrees(rpy[2]));

    int no_joints = 0;
    BotTrans *joint_pos = fk.getJointPos(angles_rad, &status, &no_joints);
    
    fprintf(stderr, "No of joints returned : %d\n", no_joints);
    free(joint_pos);

    BotTrans trans_limited;
    fk.getJointsToBotFramesLimited(angles_rad, &trans);
    fprintf (stdout, "KDL Method end_effector (Limited): trans = [%.6f, %.6f, %.6f]\n",
             trans.trans_vec[0], trans.trans_vec[1], trans.trans_vec[2]);
    
    return 0;
}
