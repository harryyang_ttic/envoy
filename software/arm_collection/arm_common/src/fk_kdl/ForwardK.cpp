#include "ForwardK.hpp"
#define JOINT_PREFIX "arm_config.joints"

using namespace KDL;
#define TICKS_TO_DEG 3.4133333333333336


double ForwardK::getAngleFromTicks(int ticks, int offset, int sign){
    return bot_to_radians(sign * ((float) (ticks - offset))/TICKS_TO_DEG);
}

ForwardK::ForwardK(){
    BotParam *param;

    if (!(lcm = bot_lcm_get_global (NULL))) {
        fprintf (stderr, "Unable to get LCM!\n");
        return;
    }
    
    
    if (!(param = bot_param_new_from_server(lcm, 0))) {
        fprintf (stderr, "Unable to get BotParam!\n");
        return;
    }
    
    //get information from param server
    char key[1024]; 

    char** joint_names = bot_param_get_subkeys(param, JOINT_PREFIX);

    int num_joints = 0;
    if (joint_names) {
        for (int pind = 0; joint_names[pind] != NULL; pind++) 
            num_joints++;
    }

    BotTrans trans_prev_to_base; 
    trans_prev_to_base.trans_vec[0] = 0;
    trans_prev_to_base.trans_vec[1] = 0;
    trans_prev_to_base.trans_vec[2] = 0;

    double rpy_base[3] = {0};
    bot_roll_pitch_yaw_to_quat (rpy_base, trans_prev_to_base.rot_quat);

    double *max_limits = new double[num_joints];
    double *min_limits = new double[num_joints];

    //incrementally fill them
    for(int i=0; i< num_joints; i++){
        sprintf(key,"%s.%s.servos", JOINT_PREFIX, joint_names[i]);
        //fprintf(stderr, "Checking Joint : %s - %d\n", key, i); 
        char **servos = bot_param_get_subkeys(param, key);

        int s_count = 0;

        if (servos) {
            for (int pind = 0; servos[pind] != NULL; pind++) {
                s_count++;
            }
        }

        char servo_sub[1024];

        for(int j=0; j < s_count; j++){
            int use_for_frame;
            sprintf(servo_sub,"%s.%s.servos.%s", JOINT_PREFIX, joint_names[i], servos[j]);
            sprintf(key,"%s.use_for_frame", servo_sub);
            bot_param_get_int(param, key, &use_for_frame);
            //fprintf(stderr, "Checking for servo : %s - Use for Frame : %d\n", key, use_for_frame); 
            if(use_for_frame == 0)
                continue;
            
            int max_tick, min_tick, offset, sign; 
            sprintf(key,"%s.min_range", servo_sub);
            bot_param_get_int(param, key, &min_tick);
            
            sprintf(key,"%s.max_range", servo_sub);
            bot_param_get_int(param, key, &max_tick);
            
            sprintf(key,"%s.offset", servo_sub);
            bot_param_get_int(param, key, &offset);

            sprintf(key,"%s.sign", servo_sub);
            bot_param_get_int(param, key, &sign);

            //fprintf(stderr, "Checking for servo : %s - Max : %d, Min : %d, Offset : %d\n", 
            //        servo_sub, max_tick, min_tick, offset);

            double max_range = getAngleFromTicks(max_tick, offset, sign);
            double min_range = getAngleFromTicks(min_tick, offset, sign);

            if(max_range > min_range){
                max_limits[i] = max_range;
                min_limits[i] = min_range;
            }
            else{
                max_limits[i] = min_range;
                min_limits[i] = max_range;                
            }
        }
    }

    for(int i=0; i < num_joints; i++){
        fprintf(stderr, "Joint : %d - [%f - %f]\n", i, bot_to_degrees(max_limits[i]), 
                bot_to_degrees(min_limits[i]));
    }

//{bot_to_radians(90), bot_to_radians(90), bot_to_radians(90), bot_to_radians(90), bot_to_radians(90), bot_to_radians(90)};
    //    double min_limits[] = {bot_to_radians(-90), bot_to_radians(-90), bot_to_radians(-90), bot_to_radians(-90), bot_to_radians(-90), bot_to_radians(-90)};
    
    //incrementally fill them 
    //skip the first one - because it doesn't contain the required frame info 
    for (int i=1; i< num_joints; i++) {

        sprintf (key,"%s.%s.joint_frame", JOINT_PREFIX, joint_names[i]);
       
        char *joint_frame;
        if (0 != bot_param_get_str (param, key, &joint_frame)){
            fprintf (stderr, "%s - frame not defined\n", key);
        }
        sprintf (key,"%s.%s.relative_to", JOINT_PREFIX, joint_names[i]);
        char *relative_to;
        if (0 != bot_param_get_str (param, key, &relative_to)) {
            fprintf (stderr, "%s - relative to not defined\n", key);
        }

        fprintf(stderr, "%s is relative to : %s\n", joint_frame, relative_to);

        // get fixed transformation wrt parent
        sprintf(key,"coordinate_frames.%s.initial_transform.translation", joint_frame);
        
        double xyz[3];
        bot_param_get_double_array (param, key, xyz, 3);

        fprintf(stderr, "Joint Name : %s XYZ : %.3f,%.3f,%.3f\n", 
                joint_names[i], xyz[0], xyz[1], xyz[2]);

        sprintf (key,"coordinate_frames.%s.initial_transform.rpy", joint_frame);
        double rpy[3];
        bot_param_get_double_array (param, key, rpy, 3);
        
        fprintf(stderr, "\t RPY : %.3f,%.3f,%.3f\n", 
                rpy[0], rpy[1], rpy[2]);

        // Convert to radians
        rpy[0] = bot_to_radians (rpy[0]);
        rpy[1] = bot_to_radians (rpy[1]);
        rpy[2] = bot_to_radians (rpy[2]);

        chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Rotation::RPY(rpy[0], rpy[1], rpy[2]), Vector(xyz[0], xyz[1], xyz[2]))));
    }
    //add the last one which just rotates on the Z axis
    chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Rotation::RPY(bot_to_radians(0), bot_to_radians(0), bot_to_radians(0)), Vector(0.0,0.0,0))));


    fk = fk_new();

    fksolver = new KDL::ChainFkSolverPos_recursive(chain);
    fksolver_limits = new KDL::ChainFkSolverPos_recursive_joint_limits(chain);
    
    fksolver_limits->setJointLimits(max_limits, min_limits);
    
}

void ForwardK::getJointsToBotFramesUsingBotTrans(double *angles, BotTrans *trans){
    fk_forward_kinematics (fk, angles, trans);
}

int ForwardK::getJointsToBotFrames(double *angles, BotTrans *trans){
    int status = 0;
    KDL::Frame cartpos = getJointsToFrame(angles, &status);

    double quat[4] ={0};
    
    double rpy[3];
    cartpos.M.GetRPY(rpy[0], rpy[1], rpy[2]);

    //Warning - Do not use Quaternion - the KDL quat ordering is different
    
    /*std::cout << "Orientation \n" << bot_to_degrees(rpy[0]) << ", " << bot_to_degrees(rpy[1]) << " , " << bot_to_degrees(rpy[2]) << std::endl;

    cartpos.M.GetQuaternion(quat[0], quat[1], quat[2], quat[3]);
    fprintf(stderr, "KDL Quat : %f,%f,%f,%f\n", 
            quat[0], quat[1], quat[2], quat[3]);

    bot_roll_pitch_yaw_to_quat(rpy, quat);

    fprintf(stderr, "LIBBOT Quat : %f,%f,%f,%f\n", 
    quat[0], quat[1], quat[2], quat[3]);*/

    bot_roll_pitch_yaw_to_quat(rpy, trans->rot_quat);

    trans->trans_vec[0] = cartpos.p[0];
    trans->trans_vec[1] = cartpos.p[1];
    trans->trans_vec[2] = cartpos.p[2];
    
    //fprintf (stdout, "KDL Method : end_effector: trans = [%.4f, %.4f, %.4f]\n",
    //       trans->trans_vec[0], trans->trans_vec[1], trans->trans_vec[2]);

    //fprintf(stderr, "Done solver\n");
}

int ForwardK::getJointsToBotFramesLimited(double *angles, BotTrans *trans){
    int status = 0;
    KDL::Frame cartpos = getJointsToFrameLimited(angles, &status);

    double quat[4] ={0};
    
    double rpy[3];
    cartpos.M.GetRPY(rpy[0], rpy[1], rpy[2]);

    bot_roll_pitch_yaw_to_quat(rpy, trans->rot_quat);

    trans->trans_vec[0] = cartpos.p[0];
    trans->trans_vec[1] = cartpos.p[1];
    trans->trans_vec[2] = cartpos.p[2];
}

KDL::Frame ForwardK::getJointsToFrame(double *angles, int *status){
    unsigned int nj = chain.getNrOfJoints();
    KDL::JntArray jointpositions = JntArray(nj);
    for(unsigned int i=0;i<nj;i++){
        jointpositions(i)= angles[i];//30 * M_PI / 180.0;
    }
    
    //KDL is faster 
    KDL::Frame cartpos;    
    bool stat = fksolver_limits->JntToCart(jointpositions,cartpos);

    //this is the position 
    if(stat>=0){
        //        std::cout << cartpos << std::endl;
    }
    else{
        std::cout << "Error - FK failed\n" << std::endl;
    }
    if(stat >=0)
        *status = 0;
    else
        *status = -1;

    return cartpos;
}

KDL::Frame ForwardK::getJointsToFrameLimited(double *angles, int *status){
    unsigned int nj = chain.getNrOfJoints();
    KDL::JntArray jointpositions = JntArray(nj);
    for(unsigned int i=0;i<nj;i++){
        jointpositions(i)= angles[i];//30 * M_PI / 180.0;
    }
    
    //KDL is faster 
    KDL::Frame cartpos;    
    bool stat = fksolver_limits->JntToCart(jointpositions,cartpos);

    //this is the position 
    if(stat>=0){
        //        std::cout << cartpos << std::endl;
    }
    else{
        std::cout << "Error - FK failed\n" << std::endl;
    }
    if(stat >=0)
        *status = 0;
    else
        *status = -1;

    return cartpos;
}

BotTrans* ForwardK::getJointPos(double *angles, int *status, int *no_joints){
    unsigned int nj = chain.getNrOfJoints();
    KDL::JntArray jointpositions = JntArray(nj);
    for(unsigned int i=0;i<nj;i++){
        jointpositions(i)= angles[i];//30 * M_PI / 180.0;
    }

    int nsegments = chain.getNrOfSegments();
    BotTrans *joint_pos = (BotTrans *) calloc(nsegments, sizeof(BotTrans));

    //KDL is faster 
    KDL::Frame cartpos;    
    for(int i=0; i< nsegments; i++){
        bool stat = fksolver_limits->JntToCart(jointpositions,cartpos, i);
        
        if(stat !=0){
            fprintf(stderr, "Error getting FK\n"); 
            free(joint_pos);
            *no_joints = 0;
            return NULL;
        }
        double rpy[3];
        cartpos.M.GetRPY(rpy[0], rpy[1], rpy[2]);
        bot_roll_pitch_yaw_to_quat(rpy, joint_pos[i].rot_quat);
        
        joint_pos[i].trans_vec[0] = cartpos.p[0];
        joint_pos[i].trans_vec[1] = cartpos.p[1];
        joint_pos[i].trans_vec[2] = cartpos.p[2];
    }
    *no_joints = nsegments;
    return joint_pos;
}

