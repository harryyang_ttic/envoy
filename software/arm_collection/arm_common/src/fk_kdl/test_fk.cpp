// Copyright  (C)  2007  Francois Cauwe <francois at cauwe dot org>
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.

#include <kdl/chain.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/frames_io.hpp>
#include <stdio.h>
#include <iostream>
#include <bot_core/bot_core.h>

using namespace KDL;

int main( int argc, char** argv )
{
    //Definition of a kinematic chain & add segments to the chain
    KDL::Chain chain;
    
    BotTrans bt; 
    bt.trans_vec[0] = 1.0;
    bt.trans_vec[1] = .0;
    bt.trans_vec[2] = .0;
    double rpy[3] = {0, 0, 0};
    bot_roll_pitch_yaw_to_quat(rpy, bt.rot_quat);
    
    Vector origin(0.,0.,0.);
    Vector axis(0,-1.0,0);
    Vector axis1(0, 0.0, -1.0);
    
    //getting the orientation in the earlier chain's coord frame seems to fix the issue
    /*chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Rotation::RPY(bot_to_radians(0), bot_to_radians(0), bot_to_radians(180)), Vector(1.0,0.0,.0))));
    chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Rotation::RPY(bot_to_radians(0), bot_to_radians(0), bot_to_radians(180)), Vector(1.0,0.0,.0))));
    chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Rotation::RPY(bot_to_radians(0), bot_to_radians(0), bot_to_radians(0)), Vector(1.0,0.0,.0))));*/
    
    //base
    chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Rotation::RPY(bot_to_radians(90), bot_to_radians(0), bot_to_radians(0)), Vector(1,0.0,.0))));
    //shoulder
    chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Rotation::RPY(bot_to_radians(0), bot_to_radians(0), bot_to_radians(0)), Vector(1,0.0,.0))));
    //elbow
    chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Rotation::RPY(bot_to_radians(0), bot_to_radians(0), bot_to_radians(0)), Vector(1,0.0,.0))));
    //l_wrist
    chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Rotation::RPY(bot_to_radians(0), bot_to_radians(-90), bot_to_radians(0)), Vector(1,0.0,.0))));
    //t_wrist
    chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Rotation::RPY(bot_to_radians(90), bot_to_radians(0), bot_to_radians(0)), Vector(.0,.0,-1.0))));
    
    //chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Rotation::RPY(bot_to_radians(0), bot_to_radians(0), bot_to_radians(0)), Vector(1.0,0.0,.0))));
     
    //chain.addSegment(Segment(Joint("test", origin, axis, Joint::RotAxis),Frame(Vector(1.0,0.0,.0))));
    //chain.addSegment(Segment(Joint("test", origin, axis1, Joint::RotAxis),Frame(Vector(1.0,0.0,.0))));
    //
    
    //chain.addSegment(Segment(Joint(Joint::RotY),Frame(Vector(1.0,0.0,0.))));
    //chain.addSegment(Segment(Joint(Joint::RotX),Frame(Vector(0.0,1.0,0.))));
    //    chain.addSegment(Segment(Joint(Joint::RotX),Frame(Vector(0.0,0.0,0.645))));
    //chain.addSegment(Segment(Joint(Joint::RotZ)));
    //chain.addSegment(Segment(Joint(Joint::RotX),Frame(Vector(0.0,0.0,0.120))));
    //chain.addSegment(Segment(Joint(Joint::RotZ)));

    // Create solver based on kinematic chain
    ChainFkSolverPos_recursive fksolver = ChainFkSolverPos_recursive(chain);

    // Create joint array
    unsigned int nj = chain.getNrOfJoints();
    KDL::JntArray jointpositions = JntArray(nj);

    // Assign some values to the joint positions
    /*for(unsigned int i=0;i<nj;i++){
        //float myinput;
        //printf ("Enter the position of joint %i: ",i);
        //scanf ("%e",&myinput);
        jointpositions(i)= M_PI/2;//(double)myinput;
        }*/
    
    //rot around Z is anti clock wise
    jointpositions(0)= bot_to_radians(0);
    jointpositions(1)= bot_to_radians(0);// M_PI/180.0 * 30;
    jointpositions(2)= bot_to_radians(0);
    jointpositions(3)= bot_to_radians(0);
    jointpositions(4)= bot_to_radians(0);
    //jointpositions(1)= 0;//M_PI/180.0 * 30;
    //jointpositions(2)= M_PI/180.0 * 30;
    //jointpositions(2)= M_PI/180.0 * 10;



    // Create the frame that will contain the results
    KDL::Frame cartpos;    

    // Calculate forward position kinematics
    bool kinematics_status;
    kinematics_status = fksolver.JntToCart(jointpositions,cartpos);
    if(kinematics_status>=0){
        std::cout << cartpos << std::endl;

        std::cout << cartpos.p <<std::endl;
        //std::cout << "Orientation \n" << cartpos.M << std::endl;

        
        double rpy[3] ={0,0,0};
        
        cartpos.M.GetRPY(rpy[0],rpy[1],rpy[2]);
        std::cout << "Orientation \n" << bot_to_degrees(rpy[0]) << ", " << bot_to_degrees(rpy[1]) << " , " << bot_to_degrees(rpy[2]) << std::endl;
        printf("%s \n","Succes, thanks KDL!");
    }else{
        printf("%s \n","Error: could not calculate forward kinematics :(");
    }
}
