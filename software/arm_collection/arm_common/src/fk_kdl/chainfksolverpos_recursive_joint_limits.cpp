// Copyright  (C)  2007  Francois Cauwe <francois at cauwe dot org>
// Copyright  (C)  2007  Ruben Smits <ruben dot smits at mech dot kuleuven dot be>

// Version: 1.0
// Author: Ruben Smits <ruben dot smits at mech dot kuleuven dot be>
// Maintainer: Ruben Smits <ruben dot smits at mech dot kuleuven dot be>
// URL: http://www.orocos.org/kdl

// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.

// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#include "chainfksolverpos_recursive_joint_limits.hpp"
#include <iostream>

namespace KDL {

    ChainFkSolverPos_recursive_joint_limits::ChainFkSolverPos_recursive_joint_limits(const Chain& _chain):
        chain(_chain)
    {       
        max_joint_limits = NULL;
        min_joint_limits = NULL;
    }

    //this should be done at the end 
    int ChainFkSolverPos_recursive_joint_limits::setJointLimits(double *max_limits, double *min_limits){
        max_joint_limits = new double[chain.getNrOfSegments()];
        min_joint_limits = new double[chain.getNrOfSegments()];
        for(int i=0; i < chain.getNrOfSegments(); i++){
            max_joint_limits[i] = bot_mod2pi(max_limits[i]);
            min_joint_limits[i] = bot_mod2pi(min_limits[i]);
        }

        return 0;
    }

    int ChainFkSolverPos_recursive_joint_limits::JntToCart(const JntArray& q_in, Frame& p_out, int segmentNr)
    {
        if(segmentNr<0)
             segmentNr=chain.getNrOfSegments();

        p_out = Frame::Identity();

        if(q_in.rows()!=chain.getNrOfJoints())
            return -1;
        else if(segmentNr>chain.getNrOfSegments())
            return -1;
        else{
            int j=0;
            for(unsigned int i=0;i<segmentNr;i++){
                if(chain.getSegment(i).getJoint().getType()!=Joint::None){
                    if(max_joint_limits && min_joint_limits){
                        double angle = fmin(fmax(bot_mod2pi(q_in(j)), min_joint_limits[j]), max_joint_limits[j]);
                        p_out = p_out*chain.getSegment(i).pose(angle);
                    }
                    else{
                        p_out = p_out*chain.getSegment(i).pose(q_in(j));
                    }
                    j++;
                }else{
                    p_out = p_out*chain.getSegment(i).pose(0.0);
                }
            }
            return 0;
        }
    }


    ChainFkSolverPos_recursive_joint_limits::~ChainFkSolverPos_recursive_joint_limits()
    {
    }


}
