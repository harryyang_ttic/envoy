#include <arm_forward_kinematics/forward_kinematics.h>


int main() {
    ForwardKinematics *fk = fk_new ();

    double angles_rad[] = {10, 0, 0, 0, 0};
    BotTrans end_effector;

    fk_forward_kinematics (fk, angles_rad, &end_effector);

    fprintf (stdout, "end_effector: trans = [%.4f, %.4f, %.4f]\n",
             end_effector.trans_vec[0], end_effector.trans_vec[1], end_effector.trans_vec[2]);

    fk_destroy (fk);

    return 0;
}
