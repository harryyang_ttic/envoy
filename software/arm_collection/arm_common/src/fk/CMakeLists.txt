add_definitions(
    -std=gnu99
    )

add_library(arm-forward-kinematics SHARED
    forward_kinematics.c)

# make the header public
pods_install_headers(forward_kinematics.h DESTINATION arm_forward_kinematics)

# make the library public
pods_install_libraries(arm-forward-kinematics)

# uncomment these lines to link against another library via pkg-config
set(REQUIRED_PACKAGES lcm 
    bot2-core 
    bot2-param-client)

pods_use_pkg_config_packages(arm-forward-kinematics ${REQUIRED_PACKAGES})

# create a pkg-config file for the library, to make it easier for other
# software to use.
pods_install_pkg_config_file(arm-forward-kinematics
    CFLAGS
    LIBS -larm-forward-kinematics
    REQUIRES ${REQUIRED_PACKAGES}
    VERSION 0.0.1)

add_executable (test-forward-kinematics test_forward_kinematics.c)
pods_use_pkg_config_packages (test-forward-kinematics arm-forward-kinematics)
pods_install_executables (test-forward-kinematics)