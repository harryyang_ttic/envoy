#ifndef __INVERSE_KINEMATICS_H
#define __INVERSE_KINEMATICS_H

#include <lcm/lcm.h>
#include <bot_param/param_client.h>
#include <bot_core/bot_core.h>
#include <arm_forward_kinematics/forward_kinematics.h>
#include <kdl/chain.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainiksolverpos_lma.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>
#include <kdl/chainiksolverpos_nr.hpp>
#include <kdl/frames_io.hpp>
#include <stdio.h>
#include <iostream>

#ifdef __cplusplus
extern "C" {
#endif
    /*typedef struct _bot_joint_t {
        double deg_to_tick;
        char *relative_to;
        BotTrans base_frame_to_parent;
    } bot_joint_t;


    typedef struct _bot_joint_list_t {
        int no_joints;
        bot_joint_t *joints;
    } bot_joint_list_t;
    */
    
    typedef struct _inverse_kinematics_t InverseKinematics;

    struct _inverse_kinematics_t 
    {
        lcm_t *lcm;
        //we will just populate this chain at the init time using bot-param 
        KDL::Chain *chain;
        KDL::ChainFkSolverPos_recursive fksolver;
        //bot_joint_list_t jlist;
    };

    InverseKinematics *ik_new();
    void ik_destroy (InverseKinematics *ik);
    void ik_inverse_kinematics (InverseKinematics *ik, BotTrans *end_effector, double *angles_rad);
    //the test function that will forward project from the given angles - and then solve for ik 
    void ik_inverse_kinematics_test (InverseKinematics *ik, BotTrans *end_effector, double *angles_rad);

#ifdef __cplusplus
}
#endif

#endif
