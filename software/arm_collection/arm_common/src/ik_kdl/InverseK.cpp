#include "InverseK.hpp"
#define JOINT_PREFIX "arm_config.joints"

#define TICKS_TO_DEG 3.4133333333333336

using namespace KDL;

double InverseK::getAngleFromTicks(int ticks, int offset, int sign){
    return bot_to_radians(sign * ((float) (ticks - offset))/TICKS_TO_DEG);
}

InverseK::InverseK(){
    BotParam *param;

    if (!(lcm = bot_lcm_get_global (NULL))) {
        fprintf (stderr, "Unable to get LCM!\n");
        return;
    }
    
    
    if (!(param = bot_param_new_from_server(lcm, 0))) {
        fprintf (stderr, "Unable to get BotParam!\n");
        return;
    }
    
    //get information from param server
    char key[1024]; 

    char** joint_names = bot_param_get_subkeys(param, JOINT_PREFIX);

    int num_joints = 0;
    if (joint_names) {
        for (int pind = 0; joint_names[pind] != NULL; pind++) 
            num_joints++;
    }

    BotTrans trans_prev_to_base; 
    trans_prev_to_base.trans_vec[0] = 0;
    trans_prev_to_base.trans_vec[1] = 0;
    trans_prev_to_base.trans_vec[2] = 0;

    double rpy_base[3] = {0};
    bot_roll_pitch_yaw_to_quat (rpy_base, trans_prev_to_base.rot_quat);

    double *max_limits = new double[num_joints];
    double *min_limits = new double[num_joints];

    //incrementally fill them
    for(int i=0; i< num_joints; i++){
        sprintf(key,"%s.%s.servos", JOINT_PREFIX, joint_names[i]);
        //fprintf(stderr, "Checking Joint : %s - %d\n", key, i); 
        char **servos = bot_param_get_subkeys(param, key);

        int s_count = 0;

        if (servos) {
            for (int pind = 0; servos[pind] != NULL; pind++) {
                s_count++;
            }
        }

        char servo_sub[1024];

        for(int j=0; j < s_count; j++){
            int use_for_frame;
            sprintf(servo_sub,"%s.%s.servos.%s", JOINT_PREFIX, joint_names[i], servos[j]);
            sprintf(key,"%s.use_for_frame", servo_sub);
            bot_param_get_int(param, key, &use_for_frame);
            //fprintf(stderr, "Checking for servo : %s - Use for Frame : %d\n", key, use_for_frame); 
            if(use_for_frame == 0)
                continue;
            
            int max_tick, min_tick, offset, sign; 
            sprintf(key,"%s.min_range", servo_sub);
            bot_param_get_int(param, key, &min_tick);
            
            sprintf(key,"%s.max_range", servo_sub);
            bot_param_get_int(param, key, &max_tick);
            
            sprintf(key,"%s.offset", servo_sub);
            bot_param_get_int(param, key, &offset);

            sprintf(key,"%s.sign", servo_sub);
            bot_param_get_int(param, key, &sign);

            //fprintf(stderr, "Checking for servo : %s - Max : %d, Min : %d, Offset : %d\n", 
            //        servo_sub, max_tick, min_tick, offset);

            double max_range = getAngleFromTicks(max_tick, offset, sign);
            double min_range = getAngleFromTicks(min_tick, offset, sign);

            if(max_range > min_range){
                max_limits[i] = max_range;
                min_limits[i] = min_range;
            }
            else{
                max_limits[i] = min_range;
                min_limits[i] = max_range;                
            }
        }
    }

    for(int i=0; i < num_joints; i++){
        fprintf(stderr, "Joint : %d - [%f - %f]\n", i, bot_to_degrees(max_limits[i]), 
                bot_to_degrees(min_limits[i]));
    }

    
    //incrementally fill them 
    //skip the first one - because it doesn't contain the required frame info 
    for (int i=1; i< num_joints; i++) {

        sprintf (key,"%s.%s.joint_frame", JOINT_PREFIX, joint_names[i]);
       
        char *joint_frame;
        if (0 != bot_param_get_str (param, key, &joint_frame)){
            fprintf (stderr, "%s - frame not defined\n", key);
        }
        sprintf (key,"%s.%s.relative_to", JOINT_PREFIX, joint_names[i]);
        char *relative_to;
        if (0 != bot_param_get_str (param, key, &relative_to)) {
            fprintf (stderr, "%s - relative to not defined\n", key);
        }

        fprintf(stderr, "%s is relative to : %s\n", joint_frame, relative_to);

        // get fixed transformation wrt parent
        sprintf(key,"coordinate_frames.%s.initial_transform.translation", joint_frame);
        
        double xyz[3];
        bot_param_get_double_array (param, key, xyz, 3);

        fprintf(stderr, "Joint Name : %s XYZ : %.3f,%.3f,%.3f\n", 
                joint_names[i], xyz[0], xyz[1], xyz[2]);

        sprintf (key,"coordinate_frames.%s.initial_transform.rpy", joint_frame);
        double rpy[3];
        bot_param_get_double_array (param, key, rpy, 3);
        
        fprintf(stderr, "\t RPY : %.3f,%.3f,%.3f\n", 
                rpy[0], rpy[1], rpy[2]);

        // Convert to radians
        rpy[0] = bot_to_radians (rpy[0]);
        rpy[1] = bot_to_radians (rpy[1]);
        rpy[2] = bot_to_radians (rpy[2]);

        chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Rotation::RPY(rpy[0], rpy[1], rpy[2]), Vector(xyz[0], xyz[1], xyz[2]))));
    }
    //add the last one which just rotates on the Z axis
    chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Rotation::RPY(bot_to_radians(0), bot_to_radians(0), bot_to_radians(0)), Vector(0.0,0.0,0))));
        
    fksolver = new KDL::ChainFkSolverPos_recursive(chain);
    //this does break things - not sure how to do this one
    iksolver1v = new ChainIkSolverVel_pinv(chain);

    //fksolver_limits = new KDL::ChainFkSolverPos_recursive_joint_limits(chain);
    //fksolver_limits->setJointLimits(max_limits, min_limits);

    iksolver1 = new ChainIkSolverPos_NR(chain, *fksolver, *iksolver1v,100,1e-6);
    
    Eigen::Matrix<double,6,1> L;
    L(0)=1;L(1)=1;L(2)=1;
    L(3)=0.01;L(4)=0.01;L(5)=0.01;
    solver = new ChainIkSolverPos_LMA(chain,L);
}

//give a set of joints - this will use fk to forward project and use ik to get joint angles 
//and match them 
void InverseK::testIK(double *angles){
    fprintf(stderr, "Testing IK Routine - forward projecting and then solving for the given angles\n");
    unsigned int nj = chain.getNrOfJoints();
    KDL::JntArray jointpositions = JntArray(nj);
    fprintf(stderr, "Number of joints : %d\n", nj);
    for(unsigned int i=0;i<nj;i++){
        jointpositions(i)= angles[i];//30 * M_PI / 180.0;
    }

    JntArray q(chain.getNrOfJoints());
    JntArray q_init(chain.getNrOfJoints());

    KDL::Frame cartpos;    
    // Calculate forward position kinematics
    bool kinematics_status;
    kinematics_status = fksolver->JntToCart(jointpositions,cartpos);

    std::cout << "Pos \n" << cartpos.p << std::endl;

    int ret = iksolver1->CartToJnt(q_init, cartpos, q);

    fprintf(stderr, "Result : %d\n", ret);
    
    std::cout << "q   " << q.data.transpose()/M_PI*180.0 << std::endl;

    ret = solver->CartToJnt(q_init, cartpos ,q);

    fprintf(stderr, "Result Method 2 : %d\n", ret);
    
    std::cout << "q   " << q.data.transpose()/M_PI*180.0 << std::endl;

    fprintf(stderr, "Done testing\n");
}

int InverseK::solvePos(double *xyz, double *angles_rad){
    
    Frame cartpos(Vector(xyz[0], xyz[1], xyz[2]));

    JntArray q(chain.getNrOfJoints());
    JntArray q_init(chain.getNrOfJoints());

    //std::cout << "Pos \n" << cartpos.p << std::endl;
    
    int ret = iksolver1->CartToJnt(q_init, cartpos, q);
    
    //fprintf(stderr, "Result : %d\n", ret);
    
    //std::cout << "q   " << q.data.transpose()/M_PI*180.0 << std::endl;

    uint nj = chain.getNrOfJoints();
    for(unsigned int i=0;i<nj;i++){
        angles_rad[i] = bot_mod2pi(q(i));
    }

    return ret;
}

int InverseK::solvePosLMA(double *xyz, double *angles_rad){

    Frame cartpos(Vector(xyz[0], xyz[1], xyz[2]));

    JntArray q(chain.getNrOfJoints());
    JntArray q_init(chain.getNrOfJoints());

    //std::cout << "Pos \n" << cartpos.p << std::endl;

    int ret = solver->CartToJnt(q_init, cartpos ,q);

    //fprintf(stderr, "Result Method 2 : %d\n", ret);
    
    //std::cout << "q   " << q.data.transpose()/M_PI*180.0 << std::endl;
    
    uint nj = chain.getNrOfJoints();
    for(unsigned int i=0;i<nj;i++){
        angles_rad[i] = q(i);
    }

    return ret;
}

int InverseK::solvePos(Frame cartpos, double *angles_rad){
    
    JntArray q(chain.getNrOfJoints());
    JntArray q_init(chain.getNrOfJoints());

    //std::cout << "Pos \n" << cartpos.p << std::endl;
    //std::cout << "Frame \n" << cartpos << std::endl;
    //int64_t stime = bot_timestamp_now();
    int ret = iksolver1->CartToJnt(q_init, cartpos, q);
    //int64_t etime = bot_timestamp_now();

    //fprintf(stderr, "Time delta : %f\n", (etime - stime)/1.0e6);
    //fprintf(stderr, "Result : %d\n", ret);
    
    //std::cout << "q   " << q.data.transpose()/M_PI*180.0 << std::endl;

    uint nj = chain.getNrOfJoints();
    for(unsigned int i=0;i<nj;i++){
        angles_rad[i] = bot_mod2pi(q(i));
    }

    return ret;
}

int InverseK::solvePosRPY(double xyz[3], double rpy[3], double *angles_rad){
    KDL::Rotation rot = KDL::Rotation::RPY(rpy[0],rpy[1],rpy[2]);
    KDL::Vector pos(xyz[0], xyz[1], xyz[2]);

    KDL::Frame cartpos(rot, pos);

    return solvePos(cartpos, angles_rad);
}

/*int InverseK::solvePosQuat(double xyz[3], double quat[4], double *angles_rad){
    KDL::Rotation rot = KDL::Rotation::Quaternion(quat[0],quat[1],quat[2], quat[3]);
    KDL::Vector pos(xyz[0], xyz[1], xyz[2]);
    KDL::Frame cartpos(rot, pos);

    return solvePos(cartpos, angles_rad);
    }*/

int InverseK::solvePosLMARPY(double xyz[3], double rpy[3], double *angles_rad){
    KDL::Rotation rot = KDL::Rotation::RPY(rpy[0],rpy[1],rpy[2]);
    KDL::Vector pos(xyz[0], xyz[1], xyz[2]);

    KDL::Frame cartpos(rot, pos);

    return solvePosLMA(cartpos, angles_rad);
}

/*int InverseK::solvePosLMAQuat(double xyz[3], double quat[4], double *angles_rad){
    KDL::Rotation rot = KDL::Rotation::Quaternion(quat[0],quat[1],quat[2], quat[3]);
    KDL::Vector pos(xyz[0], xyz[1], xyz[2]);
    KDL::Frame cartpos(rot, pos);
    return solvePosLMA(cartpos, angles_rad);
    }*/

int InverseK::solvePosLMA(Frame cartpos, double *angles_rad){

    JntArray q(chain.getNrOfJoints());
    JntArray q_init(chain.getNrOfJoints());

    int ret = solver->CartToJnt(q_init, cartpos ,q);

    uint nj = chain.getNrOfJoints();
    for(unsigned int i=0;i<nj;i++){
        angles_rad[i] = q(i);
    }

    return ret;
}
