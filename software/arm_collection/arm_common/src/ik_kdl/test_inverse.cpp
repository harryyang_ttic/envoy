#include "InverseK.hpp"
#include <arm_forward_kinematics/ForwardK.hpp> 


int main() {
    InverseK ik;

    double angles_rad[] = {bot_to_radians(30), 0, bot_to_radians(40), 0, 0, 0 };

    double result_angles[6];// = NULL;
    ik.testIK(angles_rad);

    ForwardK fk;
    int result  = 0;
    KDL::Frame cartpos = fk.getJointsToFrame(angles_rad, &result);

    double rpy[3] ={0,0,0};
    cartpos.M.GetRPY(rpy[0],rpy[1],rpy[2]);
    double quat[4] ={0,0,0,0};

    cartpos.M.GetQuaternion(quat[0],quat[1],quat[2], quat[3]);

    fprintf(stderr, "RPY : %f,%f,%f\n", bot_to_degrees(rpy[0]), bot_to_degrees(rpy[1]),
            bot_to_degrees(rpy[2]));
    
    //this is how to create a rotation object from RPY 
    KDL::Rotation rot = KDL::Rotation::RPY(rpy[0],rpy[1],rpy[2]);
    //Quaternion to create from quaternion 
    //to get the rotation out in quat 
    //void GetQuaternion(double& x,double& y,double& z, double& w) const;

    KDL::Vector pos(cartpos.p[0], cartpos.p[1], cartpos.p[2]);
    double xyz[3] = {cartpos.p[0], cartpos.p[1], cartpos.p[2]};
    KDL::Frame cpos(rot, pos);

    std::cout << cartpos << std::endl;
    std::cout << cpos << std::endl;

    fprintf(stderr, "Pos : %.6f, %.6f, %.6f\n", cartpos.p[0], cartpos.p[1], cartpos.p[2]);
    fprintf(stderr, "Quat : %f,%f, %f, %f\n", quat[0], quat[1], quat[2], quat[3]);
    
    int status;

    status = ik.solvePos(cartpos, result_angles);
    
    if(!status){
        fprintf(stderr, "Basic Method\n");
        for(int i=0; i < 6; i++){
            fprintf(stderr, "\tJoint : %d - Angle : %f\n", i, bot_to_degrees(result_angles[i]));
        }
    }
    else{
        fprintf(stderr, "Failed to find solution\n");        
    }

    status = ik.solvePosRPY(xyz, rpy, result_angles);
    
    if(!status){
        fprintf(stderr, "Basic Method - With RPY\n");
        for(int i=0; i < 6; i++){
            fprintf(stderr, "\tJoint : %d - Angle : %f\n", i, bot_to_degrees(result_angles[i]));
        }
    }
    else{
        fprintf(stderr, "RPY - Failed to find solution\n");        
    }

    /*status = ik.solvePosQuat(xyz, quat, result_angles);
    
    if(!status){
        fprintf(stderr, "Basic Method - With Quat\n");
        for(int i=0; i < 6; i++){
            fprintf(stderr, "\tJoint : %d - Angle : %f\n", i, bot_to_degrees(result_angles[i]));
        }
    }
    else{
        fprintf(stderr, "Quat - Failed to find solution\n");        
    }

    //this is more expensive 
    status = ik.solvePosLMA(cartpos, result_angles);
    
    if(!status){
        fprintf(stderr, "LMA Method\n");
        for(int i=0; i < 6; i++){
            fprintf(stderr, "\tJoint : %d - Angle : %f\n", i, bot_to_degrees(result_angles[i]));
        }
    }

    else{
        fprintf(stderr, "Failed to find solution\n");        
        }*/
    
    return 0;
}
