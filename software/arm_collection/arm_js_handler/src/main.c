// file: main.c
//


#include <stdio.h>
#include <inttypes.h>
#include <unistd.h>

#include <getopt.h>

#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif

#include <lcmtypes/erlcm_joystick_full_state_t.h>
#include <lcmtypes/arm_cmd_object_manipulation_cmd_t.h>

#define PUT_OBJECT_BACK_ADDRESS 29
#define DETECT_OBJECT_ADDRESS 30
#define RELEASE_OBJECT_ADDRESS 31
#define GO_HOME_ADDRESS 32
#define PRESSED_THRESHOLD 100

typedef struct _state_t state_t;

typedef struct {
    int ticks;
    int64_t utime;
} servo_pos_t;


struct _state_t {
    lcm_t *lcm;

    int verbose;

    int last_sent_cmd;
    int64_t last_sent_utime; 

    GMainLoop *mainloop;
};

void send_obj_manipulation_cmd(state_t *state, int cmd_type){
    arm_cmd_object_manipulation_cmd_t msg;
    msg.utime = bot_timestamp_now();
    msg.type = cmd_type;
    msg.requester = ARM_CMD_OBJECT_MANIPULATION_CMD_T_JOYSTICK;        
    arm_cmd_object_manipulation_cmd_t_publish(state->lcm, "OBJECT_MANIPULATION_REQUEST", &msg);
}

//update this to convert to the proper servo's
void js_handler (const lcm_recv_buf_t * rbuf, const char * channel_name, const erlcm_joystick_full_state_t*  msg, void * user){
    state_t * self = (state_t *) user;

    if((msg->utime - self->last_sent_utime)/1.0e6 < 2.0){
        return;
    }

    if(msg->count > PUT_OBJECT_BACK_ADDRESS && msg->values[PUT_OBJECT_BACK_ADDRESS] > PRESSED_THRESHOLD){
        fprintf(stderr, "Pressed Put Object back - sending\n");
        send_obj_manipulation_cmd(self, ARM_CMD_OBJECT_MANIPULATION_CMD_T_TYPE_PUT_OBJECT_BACK);       
        return;
    }
    
    if(msg->count > DETECT_OBJECT_ADDRESS && msg->values[DETECT_OBJECT_ADDRESS] > PRESSED_THRESHOLD){
        fprintf(stderr, "Pressed Detect Object - sending\n");
        send_obj_manipulation_cmd(self, ARM_CMD_OBJECT_MANIPULATION_CMD_T_TYPE_DETECT_OBJECT);        
        return;
    }

    if(msg->count > RELEASE_OBJECT_ADDRESS && msg->values[RELEASE_OBJECT_ADDRESS] > PRESSED_THRESHOLD){
        fprintf(stderr, "Pressed Release Object - sending\n");
        send_obj_manipulation_cmd(self, ARM_CMD_OBJECT_MANIPULATION_CMD_T_TYPE_RELEASE_OBJECT);        
        return;
    }

    if(msg->count > GO_HOME_ADDRESS && msg->values[GO_HOME_ADDRESS] > PRESSED_THRESHOLD){
        fprintf(stderr, "Pressed Go Home - sending\n");
        send_obj_manipulation_cmd(self, ARM_CMD_OBJECT_MANIPULATION_CMD_T_TYPE_SEND_ARM_HOME);        
        return;
    }
}

static void usage() {
    fprintf (stdout, "usage: arm_command [options]\n"
             "\n"
             "  -h, --help              Shows this help text and exits\n"
             "  -c, --center_pos        Specify center offset for all joints\n"
             "  -s, --simulation        Simulation mode\n"
             "  -i, --cmd_list_id       Specify which joint command list to use\n"
             "  -v, --verbose           Verbose output\n\n"
             );
    exit(1);
}
int main(int argc, char ** argv) {

    state_t *self = (state_t*) calloc(1, sizeof(state_t));

    self->last_sent_cmd = -1;

    self->lcm = bot_lcm_get_global (NULL);
    if(!self->lcm) {
        fprintf (stderr, "Error getting LCM\n");
        goto failed;
    }
    bot_glib_mainloop_attach_lcm(self->lcm);

    //Are usage options correctly implemented????
    char *optstring = "hv";
    struct option long_opts[] = {
        {"help", no_argument, 0, 'h'},
        {"verbose", no_argument, 0, 'v'},
        {0, 0}
    };	

    char c;

    while ((c = getopt_long (argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
        case 'v':
            self->verbose = 1;
            break;
        case 'h':
        default:
            usage();
        }
    }

    erlcm_joystick_full_state_t_subscribe(self->lcm, "PS3_JS_FULL_CMD", js_handler, self);

    self->mainloop = g_main_loop_new(NULL, FALSE);

    g_main_loop_run(self->mainloop);
    return 0;

 failed:
    if (self)
        free(self);
    return -1;
}
