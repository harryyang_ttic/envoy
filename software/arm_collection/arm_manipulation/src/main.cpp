#include <stdio.h>
#include <getopt.h>
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <lcmtypes/arm_rrts_cmd_t.h>
#include <lcmtypes/arm_rrts_joint_cmd_t.h>
#include <lcmtypes/arm_cmd_trace_status_t.h>
#include <lcmtypes/arm_rrts_state_list_t.h>
#include <lcmtypes/arm_cmd_joint_status_list_t.h>
#include <lcmtypes/arm_cmd_grasp_plan_t.h>
#include <lcmtypes/arm_cmd_object_manipulation_cmd_t.h>
#include <arm_forward_kinematics/ForwardK.hpp>

#define ELBOW_COMPENSATION_DEG 5.0//5.0

typedef enum {
    IDLE, SENT_PRE_PLAN_STATE, AT_PREPLAN_STATE, SENT_PRE_GRASP_STATE, 
    AT_PRE_GRASP_STATE, SENT_TO_GOAL, AT_GOAL, GRASPING_OBJECT, GRASPED_OBJECT, 
    SENT_LIFT_OBJECT, LIFTED_OBJECT, PUTTING_BACK_OBJECT, PUT_BACK_OBJECT, RELEASING_OBJECT_ON_TABLE, 
    RELEASED_OBJECT, BACK_TO_PRE_GRASP_OBJECT, SENDING_HOME
} arm_state_t;

typedef struct{
    BotTrans joint_to_body;
    double *joints;
    int valid_joints;
} end_effector_pose_t;

typedef struct _state_t {
    lcm_t *lcm;
    GMainLoop *mainloop;
    BotFrames *frames; 
    BotParam *param;
    arm_state_t arm_state; 
    arm_cmd_joint_status_list_t *joint_state; 
    bot_core_pose_t *last_object_pose; 
    int num_joints;
    ForwardK *fk;
    int valid_goal;
    end_effector_pose_t home_pose; 
    BotTrans pre_grasp_to_local;
    BotTrans goal_to_local;
    end_effector_pose_t pre_plan_pose; 
    end_effector_pose_t pre_grasp_pose; 
    end_effector_pose_t goal_pose; 
    //end_effector_pose_t grasp_pose; 
    end_effector_pose_t lift_pose; 
    end_effector_pose_t lift_up_pose; 
    arm_cmd_trace_status_t *last_rrts_failure;

    int64_t last_goal_id;
} state_t;

int command_arm_position(state_t *self, double *joint_angles);

void set_joint_pose(state_t *self, end_effector_pose_t *ef, double *joints, int no_joints){
    if(ef->joints == NULL)
        ef->joints = new double[no_joints];

    for(int i=0; i < no_joints; i++){
        ef->joints[i] = joints[i];
    }
    //do FK also 
    self->fk->getJointsToBotFrames(joints, &ef->joint_to_body);
}

void init_joint_poses(state_t *self){
    self->pre_plan_pose.joints = NULL;
    self->pre_grasp_pose.joints = NULL;
    self->goal_pose.joints = NULL;
    self->lift_pose.joints = NULL;
    self->lift_up_pose.joints = NULL;
}

void update_trans(double pos[3], double rpy[3], BotTrans &trans){
    trans.trans_vec[0] = pos[0];
    trans.trans_vec[1] = pos[1];
    trans.trans_vec[2] = pos[2];
    bot_roll_pitch_yaw_to_quat(rpy, trans.rot_quat);
}

void print_bot_trans_full(BotTrans &trans, char *name=NULL){
    double rpy_b[3] = {0};

    bot_quat_to_roll_pitch_yaw(trans.rot_quat, rpy_b);
    
    if(name){
        fprintf(stderr, "%s =>", name);
    }
    
    fprintf(stderr, " Pose : %f,%f,%f - RPY : %f,%f,%f\n", 
            trans.trans_vec[0], 
            trans.trans_vec[1], 
            trans.trans_vec[2], 
            bot_to_degrees(rpy_b[0]),
            bot_to_degrees(rpy_b[1]),
            bot_to_degrees(rpy_b[2]));
}

void print_state(state_t *state){
    if(state->arm_state == IDLE){
        fprintf(stderr, "Idle\n");
    }
    if(state->arm_state == SENT_PRE_PLAN_STATE){
        fprintf(stderr, "At pre-plan state");
    }
    if(state->arm_state == SENT_PRE_GRASP_STATE){
        fprintf(stderr, "At pre-grasp state");
    }
    if(state->arm_state == SENT_TO_GOAL){
        fprintf(stderr, "At going to goal state\n");
    }
    if(state->arm_state == GRASPING_OBJECT){
        fprintf(stderr, "grasping object\n");
    }
    if(state->arm_state == SENT_LIFT_OBJECT){
        fprintf(stderr, "liftinting object\n");
    }
}

void publish_trace_goal(state_t *self, end_effector_pose_t *goal_pose){
    command_arm_position(self, goal_pose->joints);    
}

void publish_rrt_joint_goal(state_t *self, end_effector_pose_t *goal_pose, int no_iter, int skip_collision_checks){
    arm_rrts_joint_cmd_t *msg = (arm_rrts_joint_cmd_t *) calloc(1, sizeof(arm_rrts_joint_cmd_t));
    msg->id = ++self->last_goal_id; 
    msg->no_joints = self->num_joints;
    msg->joint_angles = (double *)calloc(msg->no_joints, sizeof(double));
    msg->joint_tollerances = (double *)calloc(msg->no_joints, sizeof(double));
    for(int i=0; i < self->num_joints; i++){
        msg->joint_angles[i] = goal_pose->joints[i];
        if(i == 2){
            fprintf(stderr, "Adding compensation to elbow\n");
            msg->joint_angles[i] += bot_to_radians(ELBOW_COMPENSATION_DEG);
        }
            
        msg->joint_tollerances[i] = bot_to_radians(3);
    }
    msg->iterations = no_iter;
    msg->skip_collision_checks = skip_collision_checks;
    arm_rrts_joint_cmd_t_publish(self->lcm, "ARM_RRTS_JOINT_COMMAND", msg);
    arm_rrts_joint_cmd_t_destroy(msg);
}

void publish_rrt_goal(state_t *self, BotTrans& goal, int no_iter, int skip_collision_checks){
    arm_rrts_cmd_t msg;
    msg.id = ++self->last_goal_id; 
    msg.gripper_enable = 0;
    msg.gripper_pos = 0.5;//gripper_pos;

    //transform this to local frame and send it out 
    msg.xyz[0] = goal.trans_vec[0];
    msg.xyz[1] = goal.trans_vec[1];
    msg.xyz[2] = goal.trans_vec[2];
                   
    bot_quat_to_roll_pitch_yaw(goal.rot_quat, msg.ef_rpy);

    msg.iterations = no_iter;
    msg.skip_collision_checks = skip_collision_checks;

    arm_rrts_cmd_t_publish(self->lcm, "ARM_RRTS_COMMAND", &msg);
}

void send_object_to_rrt(state_t *self, BotTrans object_to_base){
    BotTrans local_to_base; 
    bot_frames_get_trans(self->frames, "local", "base", &local_to_base);

    BotTrans base_to_local; 
    bot_frames_get_trans(self->frames, "base", "local", &base_to_local);

    double pos_to_base[3] = {0.357086,0.2 ,0.242834};

    double theta = atan2(object_to_base.trans_vec[1], object_to_base.trans_vec[0]);

    BotTrans object_to_local;
    bot_trans_apply_trans_to(&base_to_local, &object_to_base, &object_to_local);
    print_bot_trans_full(object_to_local, "Object To Local - Original");

    BotTrans pitched_obj_to_object;
    double xyz[3] = {0};
    double rpy_p[3] = {0, bot_to_radians(-180), bot_to_radians(90) + theta };
    update_trans(xyz, rpy_p, pitched_obj_to_object);

    BotTrans pitched_obj_to_local;
    bot_trans_apply_trans_to(&object_to_local, &pitched_obj_to_object, &pitched_obj_to_local);

    //publish_rrt_goal(self, pitched_obj_to_local,0);    
}


void send_to_pregrasp_state_trace(state_t *self){
    publish_trace_goal(self, &self->pre_grasp_pose);
    self->arm_state = SENT_PRE_GRASP_STATE;
}
void send_to_pregrasp_state(state_t *self){
    if(self->pre_grasp_pose.valid_joints)
        publish_rrt_joint_goal(self, &self->pre_grasp_pose, 5000, 0);
    else{
        fprintf(stderr, "Our IK failed - sending taskspace pre grasp goal\n");
        publish_rrt_goal(self, self->pre_grasp_to_local, 5000, 0);
    }
    self->arm_state = SENT_PRE_GRASP_STATE;
}

void send_to_grasp_state_trace(state_t *self){
    publish_trace_goal(self, &self->goal_pose);
    self->arm_state = SENT_TO_GOAL;
}
void send_to_grasp_state(state_t *self){
    if(self->goal_pose.valid_joints)
        publish_rrt_joint_goal(self, &self->goal_pose, 5000, 1);
    else{
        fprintf(stderr, "Our IK failed - sending taskspace goal\n");
        publish_rrt_goal(self, self->goal_to_local, 5000, 1);
    }
    self->arm_state = SENT_TO_GOAL;
}

void send_to_arm_back_to_pre_grasp_state(state_t *self){
    publish_trace_goal(self, &self->pre_grasp_pose);
    self->arm_state = BACK_TO_PRE_GRASP_OBJECT;
}

void send_to_arm_home_state(state_t *self){
    publish_trace_goal(self, &self->home_pose);
    self->arm_state = SENDING_HOME;
}

void send_to_put_object_on_table_state(state_t *self){
    publish_trace_goal(self, &self->lift_pose);
    self->arm_state = PUTTING_BACK_OBJECT;
}

void send_to_release_object_on_table_state(state_t *self){
    publish_trace_goal(self, &self->goal_pose);
    self->arm_state = RELEASING_OBJECT_ON_TABLE;
}

void send_to_grab_object_state(state_t *self){
    //tell to grab object
    publish_trace_goal(self, &self->lift_pose);
    self->arm_state = GRASPING_OBJECT;
}

void send_to_lift_object_state(state_t *self){
    publish_trace_goal(self, &self->lift_up_pose);
    self->arm_state = SENT_LIFT_OBJECT; 
}

int do_ik_hack_joints(BotTrans& goal_to_base, double *shoulder_joint, double *elbow_joint, 
                double *wrist_joint){
    //now calculate where the 
    //doing this by hand for now 
    double l_base = .04445;
    double l_shoulder = 0.3895;
    double l_elbow = 0.3826;
    double l_rest = 0.06985 + 0.04445;

    double dx = hypot(goal_to_base.trans_vec[0], goal_to_base.trans_vec[1]);
    double dy = goal_to_base.trans_vec[2] + 0.05;

       
    double k1 = dx - l_base - l_rest;
    fprintf(stderr, "K1 (>0) : %f\n", k1);
    
    double k2 = dy;
    
    double l1 = l_shoulder;
    double l2 = l_elbow;

    double lambda = (pow(l1,2) + pow(k1,2) + pow(k2,2) - pow(l2,2)) / (2 * l1 * hypot(k1,k2));

    double omega = atan2(k2, k1);

    fprintf(stderr, "Dx %f Dy %f l_base + lrest : %f\n", dx, dy, l_base+l_rest);

    fprintf(stderr, "K1 %f K2 %f\n", k1, k2);

    //for theta < 90
    
    //acos can give more than one sulution
    double gamma1 = acos(lambda);
    double theta1 = gamma1 + omega;
    double alpha1 = asin( (l1 * sin(theta1) - k2)/l2);

    double gamma2 = -acos(lambda);
    double theta2 = gamma2 + omega;
    double alpha2 = asin( (l1 * sin(theta2) - k2)/l2);

    double alpha, theta;
    int solution = 0;
    
    if(theta1 > 0 && alpha1 > 0){
        solution = 1;
        alpha = alpha1;
        theta = theta1;
    }

    if(theta2 > 0 && alpha2 > 0){
        solution = 1;
        alpha = alpha2;
        theta = theta2;
    }

    if(solution){
        //fprintf(stderr, "Theta : %f, Gamma : %f - Omega : %f\n", bot_to_degrees(theta), 
        //       bot_to_degrees(gamma), bot_to_degrees(omega));
        fprintf(stderr, "Shoulder : %f, Elbow : %f - Wrist : %f\n", bot_to_degrees(theta), 
                -bot_to_degrees(theta +alpha), bot_to_degrees(alpha));
        
        //this is the pre plan joint angles
        *shoulder_joint = theta;
        *elbow_joint = -(theta +alpha);
        *wrist_joint = alpha;
        
        return 0;
    }

    fprintf(stderr, "Shoulder joint greater than 90 - checking other solutions\n");
    
    //for theta > 90
    double lambda1 = (pow(l2,2) - pow(k1,2) - pow(k2,2) - pow(l1,2)) / (2 * l1 * hypot(k1,k2));
    //acos can give more than one sulution
    double gamma3 = acos(lambda1);
    double beta1 = gamma3 - omega;
    double theta3 = bot_to_radians(180) - beta1;
    double alpha3 = asin( (l1 * sin(beta1) - k2)/l2);
    
    double gamma4 = -acos(lambda1);
    double beta2 = gamma4 - omega;
    double theta4 = bot_to_radians(180) - beta2;
    double alpha4 = asin( (l1 * sin(beta2) - k2)/l2);
    
    if(theta3 > bot_to_radians(90) && alpha3 > 0 ){
        solution = 1;
        alpha = alpha3;
        theta = theta3;
    }
    
    if(theta4 > bot_to_radians(90) && alpha4 > 0){
        solution = 1;
        alpha = alpha4;
        theta = theta4;
    }

    if(solution){
        //fprintf(stderr, "Theta : %f, Gamma : %f - Omega : %f\n", bot_to_degrees(theta), 
        //       bot_to_degrees(gamma), bot_to_degrees(omega));
        fprintf(stderr, "Shoulder : %f, Elbow : %f - Wrist : %f\n", bot_to_degrees(theta), 
                -bot_to_degrees(theta +alpha), bot_to_degrees(alpha));
        
        //this is the pre plan joint angles
        *shoulder_joint = theta;
        *elbow_joint = -(theta +alpha);
        *wrist_joint = alpha;
        
        return 0;
    }

    //for theta < 0 //we will consider theta  = - theta (so we deal with positive thetas)
    fprintf(stderr, "Theta > 0 failed - checking for theta < 0\n");
    double lambda2 = (pow(l1,2) + pow(k1,2) + pow(k2,2) - pow(l2,2)) / (2 * l1 * hypot(k1,k2));
    
    //acos can give more than one sulution
    double gamma5 = acos(lambda);
    double theta5 = gamma5 - omega;
    double alpha5 = asin( (k2 + l1 * sin(theta5))/l2);

    double gamma6 = -acos(lambda);
    double theta6 = gamma6 - omega;
    double alpha6 = asin( (k2 + l1 * sin(theta6))/l2);

    if(theta5 > 0 && alpha5 > 0){
        solution = 1;
        alpha = alpha5;
        theta = -theta5;
    }

    if(theta6 > 0 && alpha6 > 0){
        solution = 1;
        alpha = alpha6;
        theta = -theta6;
    }

    if(solution){
        //fprintf(stderr, "Theta : %f, Gamma : %f - Omega : %f\n", bot_to_degrees(theta), 
        //       bot_to_degrees(gamma), bot_to_degrees(omega));
        fprintf(stderr, "Shoulder : %f, Elbow : %f - Wrist : %f\n", bot_to_degrees(theta), 
                bot_to_degrees(-theta +alpha), bot_to_degrees(alpha));
        
        //this is the pre plan joint angles
        *shoulder_joint = bot_mod2pi(theta);
        *elbow_joint = bot_mod2pi(-theta +alpha);
        *wrist_joint = bot_mod2pi(alpha);
        
        return 0;
    }
    
    else{
        fprintf(stderr, "Did not find the solution\n");

        fprintf(stderr, "Theta1 : %f Alpha1 : %f\n", 
                bot_to_degrees(theta1), bot_to_degrees(alpha1)); 

        fprintf(stderr, "Theta2 : %f Alpha2 : %f\n", 
                bot_to_degrees(theta2), bot_to_degrees(alpha2)); 

        fprintf(stderr, "Theta3 : %f Alpha3 : %f\n", 
                bot_to_degrees(theta3), bot_to_degrees(alpha3)); 

        fprintf(stderr, "Theta4 : %f Alpha4 : %f\n", 
                bot_to_degrees(theta4), bot_to_degrees(alpha4)); 
        

        return -1;
    }
}

int do_ik_hack(BotTrans& goal_to_base, end_effector_pose_t *ef){
    int status = do_ik_hack_joints(goal_to_base, &ef->joints[1], &ef->joints[2], &ef->joints[3]);
    if(status <0)
        ef->valid_joints = 0;
    else
        ef->valid_joints = 1;
    return status;
}

void on_trace_status(const lcm_recv_buf_t *rbuf, const char * channel, const arm_cmd_trace_status_t * msg, void * user) {
    state_t *state = (state_t *) user;

    if(msg->goal_id == state->last_goal_id){
        //we are receiving status from the correct goal 
        if(msg->status == ARM_CMD_TRACE_STATUS_T_FAILED){
            fprintf(stderr, "Error - Failed to get to goal\n");
            fprintf(stderr, "Goal Msg ID : %d - Current Internal Goal : %d\n", (int) msg->goal_id, (int) state->last_goal_id);
            print_state(state);
            //we should update our state machine 
            if(state->arm_state == IDLE){
                //fprintf(stderr, "Error - Our state should not be idle when getting goal reached message\n");
            }
            else if(state->arm_state == SENT_PRE_PLAN_STATE){
                fprintf(stderr, "At pre-plan state => Commanding RRT to go to pre-grasp state\n");
                send_to_pregrasp_state(state);                
            }
            else if(state->arm_state == SENT_PRE_GRASP_STATE){
                fprintf(stderr, "At pre-grasp state => Commanding RRT to go to grasp state\n");
                send_to_grasp_state(state);
                if(state->last_rrts_failure){
                    send_to_pregrasp_state_trace(state);
                    arm_cmd_trace_status_t_destroy(state->last_rrts_failure);
                }
            }
            else if(state->arm_state == SENT_TO_GOAL){
                fprintf(stderr, "At goal state => Commanding RRT to grasp the object\n");
                send_to_grab_object_state(state);
                if(state->last_rrts_failure){
                    send_to_grasp_state_trace(state);
                    arm_cmd_trace_status_t_destroy(state->last_rrts_failure);
                }
            }
            else if(state->arm_state == GRASPING_OBJECT){
                fprintf(stderr, "Grasped object => Commanding RRT to do something with the object\n");
                send_to_lift_object_state(state);
            }
            else if(state->arm_state == SENT_LIFT_OBJECT){
                //fprintf(stderr, "Lifted Object => Commanding RRT to do something with the object\n");                
            }
        }
        
        if(msg->status == ARM_CMD_TRACE_STATUS_T_REACHED_GOAL){
            fprintf(stderr, "Goal Msg ID : %d - Current Internal Goal : %d\n", (int) msg->goal_id, (int) state->last_goal_id);
            print_state(state);
            //we should update our state machine 
            if(state->arm_state == IDLE){
                //fprintf(stderr, "Error - Our state should not be idle when getting goal reached message\n");
            }
            else if(state->arm_state == SENT_PRE_PLAN_STATE){
                fprintf(stderr, "At pre-plan state => Commanding RRT to go to pre-grasp state\n");
                send_to_pregrasp_state(state);                
            }
            else if(state->arm_state == SENT_PRE_GRASP_STATE){
                fprintf(stderr, "At pre-grasp state => Commanding RRT to go to grasp state\n");
                //send_to_grasp_state(state);
                send_to_grasp_state_trace(state);
            }
            else if(state->arm_state == SENT_TO_GOAL){
                fprintf(stderr, "At goal state => Commanding RRT to grasp the object\n");
                send_to_grab_object_state(state);
            }
            else if(state->arm_state == GRASPING_OBJECT){
                fprintf(stderr, "Grasped object => Commanding RRT to do something with the object\n");
                send_to_lift_object_state(state);
            }
            else if(state->arm_state == SENT_LIFT_OBJECT){
                //fprintf(stderr, "Lifted Object => Commanding RRT to do something with the object\n");    
                fprintf(stderr, "Lifted Object\n");
                state->arm_state = LIFTED_OBJECT;
            }
            else if(state->arm_state == PUTTING_BACK_OBJECT){
                fprintf(stderr, "Put back Object on table - releasing\n");
                send_to_release_object_on_table_state(state);
            }
            else if(state->arm_state == RELEASING_OBJECT_ON_TABLE){
                fprintf(stderr, "Released object on table - going pre-grasp\n");
                send_to_arm_back_to_pre_grasp_state(state);
            }
            else if(state->arm_state == BACK_TO_PRE_GRASP_OBJECT){
                fprintf(stderr, "Released object on table - going home\n");
                send_to_arm_home_state(state);                
            }
            else if(state->arm_state == SENDING_HOME){
                fprintf(stderr, "Arm is back home - waiting for command\n");
                state->arm_state = IDLE;
            }
        }
    }    
}

void on_joints(const lcm_recv_buf_t *rbuf, const char * channel, const arm_cmd_joint_status_list_t * msg, void * user) {
     state_t *self = (state_t *) user;
     if(self->joint_state != NULL){
        arm_cmd_joint_status_list_t_destroy(self->joint_state);
    }
    self->joint_state = arm_cmd_joint_status_list_t_copy(msg);
}
/*
  package arm_cmd;

struct grasp_plan_t {

    int64_t utime;
    grasp_plan_t pre_plan;
    grasp_plan_t pre_grasp;       
    grasp_plan_t goal;       
    grasp_plan_t lift;       
}


 */
void update_grasp_pose(state_t *self, arm_cmd_ef_pose_t *ef_msg, end_effector_pose_t *ef_pose){

    ef_msg->no_joints = self->num_joints;
    ef_msg->joints = (double *) calloc(ef_msg->no_joints, sizeof(double));
    memcpy(ef_msg->joints, ef_pose->joints, self->num_joints * sizeof(double));

    memcpy(ef_msg->pose.pos, ef_pose->joint_to_body.trans_vec, 3 * sizeof(double));
    memcpy(ef_msg->pose.orientation, ef_pose->joint_to_body.rot_quat, 4 * sizeof(double));
}

void publish_arm_poses(state_t *self){
    arm_cmd_grasp_plan_t *msg = (arm_cmd_grasp_plan_t *) calloc(1, sizeof(arm_cmd_grasp_plan_t));
    msg->utime = bot_timestamp_now();
    update_grasp_pose(self, &msg->pre_plan, &self->pre_plan_pose);
    update_grasp_pose(self, &msg->pre_grasp, &self->pre_grasp_pose);
    update_grasp_pose(self, &msg->goal, &self->goal_pose);
    update_grasp_pose(self, &msg->lift, &self->lift_pose);
    //update_grasp_pose(self, &msg->lift, &self->lift_up_pose);
    
    arm_cmd_grasp_plan_t_publish(self->lcm, "OBJECT_GRASP_PLAN", msg);
    arm_cmd_grasp_plan_t_destroy(msg);
}

void calculate_grasp_poses(state_t *self, bot_core_pose_t * msg){
    //get the current joint poses as well 
    double current_joints[self->joint_state->njoints];
    for(int i=0; i < self->joint_state->njoints; i++){
        current_joints[i] = bot_to_radians(self->joint_state->joints[i].angle);
    }

    self->pre_grasp_pose.valid_joints = 0;
    self->goal_pose.valid_joints = 0;

    BotTrans local_to_base; 
    bot_frames_get_trans(self->frames, "local", "base", &local_to_base);
    
    BotTrans object_to_local; 
    memcpy(object_to_local.trans_vec, msg->pos, 3 * sizeof(double));
    memcpy(object_to_local.rot_quat, msg->orientation, 4 * sizeof(double));

    //not that the arm pose should be 5 cm further towards the base than the object
    //get the pose of point 10 cm in the y axis of the object's pose 

    BotTrans pre_grasp_to_object; 
    double pos[3] = { 0, .15, 0};
    double rpy[3] = {0};
    update_trans(pos, rpy, pre_grasp_to_object);
    
    //this is where the arm end effector needs to end up - for the pre grasp
    BotTrans pre_grasp_to_local;
    bot_trans_apply_trans_to(&object_to_local, &pre_grasp_to_object, &pre_grasp_to_local);

    memcpy(&self->pre_grasp_to_local, &pre_grasp_to_local, sizeof(BotTrans));

    //this is where the arm end effector needs to end up - for the pre grasp
    BotTrans pre_grasp_to_base;
    bot_trans_apply_trans_to(&local_to_base, &pre_grasp_to_local, &pre_grasp_to_base);

    BotTrans grasp_to_object; 
    double pos_g[3] = {0, 0.0, 0};
    double rpy_g[3] = {0};
    update_trans(pos_g, rpy_g, grasp_to_object);

    //this is where the arm end effector needs to end up - to be grasped
    BotTrans grasp_to_local;
    bot_trans_apply_trans_to(&object_to_local, &grasp_to_object, &grasp_to_local);

    memcpy(&self->goal_to_local, &grasp_to_local, sizeof(BotTrans));

    BotTrans grasp_to_base;
    bot_trans_apply_trans_to(&local_to_base, &grasp_to_local, &grasp_to_base);

    double base_yaw = atan2(grasp_to_base.trans_vec[1], grasp_to_base.trans_vec[0]);
    
    //this is the pre plan joint angles
    current_joints[0] = base_yaw;
    current_joints[4] = bot_to_radians(90);
    //the gripper needs to be open - figure out which is the right angle
    current_joints[5] = bot_to_radians(44);
    
    set_joint_pose(self, &self->pre_plan_pose, current_joints, self->joint_state->njoints);
    //we will update the rest of the joints later
    set_joint_pose(self, &self->pre_grasp_pose, current_joints, self->joint_state->njoints);
    set_joint_pose(self, &self->goal_pose, current_joints, self->joint_state->njoints);
    set_joint_pose(self, &self->lift_pose, current_joints, self->joint_state->njoints);

    
    //hard coded for now - but seems to work 
    //this might fail for theta > 90
    int status = do_ik_hack(pre_grasp_to_base, &self->pre_grasp_pose);
    if(status <0){
        fprintf(stderr, "IK Hack failed for pre-grasp - maybe send the task space goal\n");
    }

    status = do_ik_hack(grasp_to_base, &self->goal_pose);

    if(status <0){
        fprintf(stderr, "IK Hack failed for Goal - maybe send the task space goal\n");
    }

    current_joints[5] = bot_to_radians(20);
    
    set_joint_pose(self, &self->lift_up_pose, current_joints, self->joint_state->njoints);
    
    current_joints[1] = self->goal_pose.joints[1];
    current_joints[2] = self->goal_pose.joints[2];
    current_joints[3] = self->goal_pose.joints[3];
    set_joint_pose(self, &self->lift_pose, current_joints, self->joint_state->njoints);
    
    //send this out at the end 

    publish_arm_poses(self);
    publish_trace_goal(self, &self->pre_plan_pose);
    self->arm_state = SENT_PRE_PLAN_STATE;
}

void on_obj_man_cmd(const lcm_recv_buf_t *rbuf, const char * channel, const arm_cmd_object_manipulation_cmd_t * msg, void * user) {
    if(msg->type == ARM_CMD_OBJECT_MANIPULATION_CMD_T_TYPE_DETECT_OBJECT){
        //this is handled by the object detector
        return;        
    }
    
    fprintf(stderr, "Received Object Manipulation Cmd\n");
    
    state_t *self = (state_t *) user;

    if(msg->type == ARM_CMD_OBJECT_MANIPULATION_CMD_T_TYPE_RELEASE_OBJECT){
        fprintf(stderr,"Asked to release object - complying\n");
        
    }
    if(msg->type == ARM_CMD_OBJECT_MANIPULATION_CMD_T_TYPE_PUT_OBJECT_BACK){
        fprintf(stderr,"Asked to put object back - complying\n");

        //this needs to be done in two phases - first send to grasp state
        //then have it release the grasp
        //and then have the arm go home
        send_to_put_object_on_table_state(self);

    }
    if(msg->type == ARM_CMD_OBJECT_MANIPULATION_CMD_T_TYPE_SEND_ARM_HOME){
        fprintf(stderr,"Asked to send arm home - complying\n");
        send_to_arm_home_state(self);
    }
}

void on_object_pose(const lcm_recv_buf_t *rbuf, const char * channel, const bot_core_pose_t * msg, void * user) {
 
    fprintf(stderr, "Received Goal Pose\n");

    state_t *self = (state_t *) user;

    if(self->last_object_pose != NULL){
        bot_core_pose_t_destroy(self->last_object_pose);
    }
    self->last_object_pose = bot_core_pose_t_copy(msg);

    //do the calculations - the pre-plan position
    //use the joint state
    
    calculate_grasp_poses(self, self->last_object_pose);
    
    //we should wait for the confirmation before sending the other one 
}

void on_rrts_failure_status(const lcm_recv_buf_t *rbuf, const char * channel, const arm_cmd_trace_status_t * msg, void * user) {
    state_t *self = (state_t *) user;

    if(self->last_rrts_failure)
        arm_cmd_trace_status_t_destroy(self->last_rrts_failure);

    self->last_rrts_failure = arm_cmd_trace_status_t_copy(msg);        
}


int command_arm_position(state_t *self, double *joint_angles){
    //
    int num_dimensions = self->num_joints;//6;//system.getNumDimensions();
    arm_rrts_state_list_t *opttraj = (arm_rrts_state_list_t *) malloc (sizeof (arm_rrts_state_list_t));
    opttraj->num_states = 1;
    opttraj->id = ++self->last_goal_id;
    opttraj->states = (arm_rrts_state_t *) malloc (opttraj->num_states * sizeof (arm_rrts_state_t));

    opttraj->states[0].id = 0;
    opttraj->states[0].num_dimensions = num_dimensions;
      
    opttraj->states[0].angles = (double *) calloc(num_dimensions, sizeof(double));
        
    for(int i = 0; i < num_dimensions; i++){
        opttraj->states[0].angles[i] = joint_angles[i];//stateRef[i];
        fprintf(stderr, "\tJoint [%d] - %f\n", i, bot_to_degrees(joint_angles[i]));//stateRef[i]));
    }

    arm_rrts_state_list_t_publish (self->lcm, "ARM_RRTS_STATE_LIST", opttraj);
    
    arm_rrts_state_list_t_destroy (opttraj);
}


int main(int argc, char** argv)
{
    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    state->lcm =  bot_lcm_get_global(NULL);
    state->arm_state = IDLE;
    
    state->param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, state->param);
    state->mainloop = g_main_loop_new( NULL, FALSE );  
    state->last_object_pose = NULL;
    state->last_goal_id = 0;
    state->num_joints = 6;
    state->fk = new ForwardK();

    init_joint_poses(state);

    const char *optstring = "vhcfsdb";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "verbose", no_argument, 0, 'v' }}; 


    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    //usage(argv[0]);
            fprintf(stderr, "No help found - please add help\n");
	    break;
        case 'v':
	    //usage(argv[0]);
            fprintf(stderr, "No help found - please add help\n");
	    break;
        }
    }
    
    if (!state->mainloop){
	printf("Couldn't create main loop\n");
	return -1;
    }
    
     //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    bot_core_pose_t_subscribe(state->lcm, "OBJ_BOTTLE_POSE", on_object_pose, state);

    arm_cmd_object_manipulation_cmd_t_subscribe(state->lcm, "OBJECT_MANIPULATION_REQUEST", on_obj_man_cmd, state);

    arm_cmd_joint_status_list_t_subscribe(state->lcm, "ARM_JOINT_STATUS", on_joints, state);

    arm_cmd_trace_status_t_subscribe(state->lcm, "TRACE_STATUS", on_trace_status, state);

    arm_cmd_trace_status_t_subscribe(state->lcm, "ARM_RRTS_FAILURE_STATUS", on_rrts_failure_status, state);

    double joint_values[state->num_joints];

    for (int i=0; i<state->num_joints; i++) {
        joint_values[i] = 0;
    }

    joint_values[1] = bot_to_radians(107);
    joint_values[2] = bot_to_radians(-91.9);
    set_joint_pose(state, &state->home_pose, joint_values, state->num_joints);
    
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
    
    bot_glib_mainloop_detach_lcm(state->lcm);

    return 0;
}
