// THIS IS AN AUTOMATICALLY GENERATED FILE.  DO NOT MODIFY
// BY HAND!!
//
// Generated by lcm-gen

#include "lcmtypes/erlcm_robot_state_enum_t.h"
const char * erlcm_robot_state_enum_t_name(erlcm_robot_state_enum_t val)
{
    switch (val) {
        case ERLCM_ROBOT_STATE_ENUM_T_UNDEFINED:
            return "UNDEFINED";
        case ERLCM_ROBOT_STATE_ENUM_T_OVERRIDE:
            return "OVERRIDE";
        case ERLCM_ROBOT_STATE_ENUM_T_STOP:
            return "STOP";
        case ERLCM_ROBOT_STATE_ENUM_T_STANDBY:
            return "STANDBY";
        case ERLCM_ROBOT_STATE_ENUM_T_RUN:
            return "RUN";
        case ERLCM_ROBOT_STATE_ENUM_T_ERROR:
            return "ERROR";
        case ERLCM_ROBOT_STATE_ENUM_T_SHIFT:
            return "SHIFT";
        default:
            return NULL;
    }
}
