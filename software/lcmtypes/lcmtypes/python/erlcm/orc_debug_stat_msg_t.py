"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

class orc_debug_stat_msg_t(object):
    __slots__ = ["utime", "qei_position", "qei_velocity", "pwm_value", "s_desired_vel", "s_actual", "command_velocity"]

    def __init__(self):
        self.utime = 0
        self.qei_position = [ 0 for dim0 in range(2) ]
        self.qei_velocity = [ 0 for dim0 in range(2) ]
        self.pwm_value = [ 0.0 for dim0 in range(2) ]
        self.s_desired_vel = [ 0.0 for dim0 in range(2) ]
        self.s_actual = [ 0.0 for dim0 in range(2) ]
        self.command_velocity = [ 0.0 for dim0 in range(2) ]

    def encode(self):
        buf = BytesIO()
        buf.write(orc_debug_stat_msg_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">q", self.utime))
        buf.write(struct.pack('>2i', *self.qei_position[:2]))
        buf.write(struct.pack('>2i', *self.qei_velocity[:2]))
        buf.write(struct.pack('>2d', *self.pwm_value[:2]))
        buf.write(struct.pack('>2d', *self.s_desired_vel[:2]))
        buf.write(struct.pack('>2d', *self.s_actual[:2]))
        buf.write(struct.pack('>2d', *self.command_velocity[:2]))

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != orc_debug_stat_msg_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return orc_debug_stat_msg_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = orc_debug_stat_msg_t()
        self.utime = struct.unpack(">q", buf.read(8))[0]
        self.qei_position = struct.unpack('>2i', buf.read(8))
        self.qei_velocity = struct.unpack('>2i', buf.read(8))
        self.pwm_value = struct.unpack('>2d', buf.read(16))
        self.s_desired_vel = struct.unpack('>2d', buf.read(16))
        self.s_actual = struct.unpack('>2d', buf.read(16))
        self.command_velocity = struct.unpack('>2d', buf.read(16))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if orc_debug_stat_msg_t in parents: return 0
        tmphash = (0x459a07626bacccc9) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if orc_debug_stat_msg_t._packed_fingerprint is None:
            orc_debug_stat_msg_t._packed_fingerprint = struct.pack(">Q", orc_debug_stat_msg_t._get_hash_recursive([]))
        return orc_debug_stat_msg_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

