"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

class kinect_person_tracker_status_t(object):
    __slots__ = ["utime", "status", "sender", "x", "y"]

    STATUS_IDLE = 0
    STATUS_TRACKING = 1
    STATUS_LOOKING = 2
    STATUS_LOST = 3
    SENDER_KINECT = 1
    SENDER_LASER = 2
    SENDER_DM = 3

    def __init__(self):
        self.utime = 0
        self.status = 0
        self.sender = 0
        self.x = 0.0
        self.y = 0.0

    def encode(self):
        buf = BytesIO()
        buf.write(kinect_person_tracker_status_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">qhhdd", self.utime, self.status, self.sender, self.x, self.y))

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != kinect_person_tracker_status_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return kinect_person_tracker_status_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = kinect_person_tracker_status_t()
        self.utime, self.status, self.sender, self.x, self.y = struct.unpack(">qhhdd", buf.read(28))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if kinect_person_tracker_status_t in parents: return 0
        tmphash = (0xdb9b65549f8080f4) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if kinect_person_tracker_status_t._packed_fingerprint is None:
            kinect_person_tracker_status_t._packed_fingerprint = struct.pack(">Q", kinect_person_tracker_status_t._get_hash_recursive([]))
        return kinect_person_tracker_status_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

