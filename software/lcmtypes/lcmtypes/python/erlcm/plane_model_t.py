"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

class plane_model_t(object):
    __slots__ = ["utime", "params"]

    def __init__(self):
        self.utime = 0
        self.params = [ 0.0 for dim0 in range(4) ]

    def encode(self):
        buf = BytesIO()
        buf.write(plane_model_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">q", self.utime))
        buf.write(struct.pack('>4d', *self.params[:4]))

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != plane_model_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return plane_model_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = plane_model_t()
        self.utime = struct.unpack(">q", buf.read(8))[0]
        self.params = struct.unpack('>4d', buf.read(32))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if plane_model_t in parents: return 0
        tmphash = (0xb43d8c347baee69f) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if plane_model_t._packed_fingerprint is None:
            plane_model_t._packed_fingerprint = struct.pack(">Q", plane_model_t._get_hash_recursive([]))
        return plane_model_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

