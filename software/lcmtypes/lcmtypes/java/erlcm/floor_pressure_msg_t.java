/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package erlcm;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class floor_pressure_msg_t implements lcm.lcm.LCMEncodable
{
    public int no_floors;
    public erlcm.single_floor_pressure_t pressure[];
 
    public floor_pressure_msg_t()
    {
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0xe309853f320e0a8fL;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(erlcm.floor_pressure_msg_t.class))
            return 0L;
 
        classes.add(erlcm.floor_pressure_msg_t.class);
        long hash = LCM_FINGERPRINT_BASE
             + erlcm.single_floor_pressure_t._hashRecursive(classes)
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeInt(this.no_floors); 
 
        for (int a = 0; a < this.no_floors; a++) {
            this.pressure[a]._encodeRecursive(outs); 
        }
 
    }
 
    public floor_pressure_msg_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public floor_pressure_msg_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static erlcm.floor_pressure_msg_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        erlcm.floor_pressure_msg_t o = new erlcm.floor_pressure_msg_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.no_floors = ins.readInt();
 
        this.pressure = new erlcm.single_floor_pressure_t[(int) no_floors];
        for (int a = 0; a < this.no_floors; a++) {
            this.pressure[a] = erlcm.single_floor_pressure_t._decodeRecursiveFactory(ins);
        }
 
    }
 
    public erlcm.floor_pressure_msg_t copy()
    {
        erlcm.floor_pressure_msg_t outobj = new erlcm.floor_pressure_msg_t();
        outobj.no_floors = this.no_floors;
 
        outobj.pressure = new erlcm.single_floor_pressure_t[(int) no_floors];
        for (int a = 0; a < this.no_floors; a++) {
            outobj.pressure[a] = this.pressure[a].copy();
        }
 
        return outobj;
    }
 
}

