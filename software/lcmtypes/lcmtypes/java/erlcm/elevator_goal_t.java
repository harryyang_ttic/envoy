/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package erlcm;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class elevator_goal_t implements lcm.lcm.LCMEncodable
{
    public long utime;
    public double goal[];
    public double w_loc[];
 
    public elevator_goal_t()
    {
        goal = new double[2];
        w_loc = new double[2];
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0xbcc3d45ecc8f033bL;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(erlcm.elevator_goal_t.class))
            return 0L;
 
        classes.add(erlcm.elevator_goal_t.class);
        long hash = LCM_FINGERPRINT_BASE
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeLong(this.utime); 
 
        for (int a = 0; a < 2; a++) {
            outs.writeDouble(this.goal[a]); 
        }
 
        for (int a = 0; a < 2; a++) {
            outs.writeDouble(this.w_loc[a]); 
        }
 
    }
 
    public elevator_goal_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public elevator_goal_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static erlcm.elevator_goal_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        erlcm.elevator_goal_t o = new erlcm.elevator_goal_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.utime = ins.readLong();
 
        this.goal = new double[(int) 2];
        for (int a = 0; a < 2; a++) {
            this.goal[a] = ins.readDouble();
        }
 
        this.w_loc = new double[(int) 2];
        for (int a = 0; a < 2; a++) {
            this.w_loc[a] = ins.readDouble();
        }
 
    }
 
    public erlcm.elevator_goal_t copy()
    {
        erlcm.elevator_goal_t outobj = new erlcm.elevator_goal_t();
        outobj.utime = this.utime;
 
        outobj.goal = new double[(int) 2];
        System.arraycopy(this.goal, 0, outobj.goal, 0, 2); 
        outobj.w_loc = new double[(int) 2];
        System.arraycopy(this.w_loc, 0, outobj.w_loc, 0, 2); 
        return outobj;
    }
 
}

