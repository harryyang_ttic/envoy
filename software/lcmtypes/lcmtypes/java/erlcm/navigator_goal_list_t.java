/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package erlcm;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class navigator_goal_list_t implements lcm.lcm.LCMEncodable
{
    public long utime;
    public int numWaypoints;
    public erlcm.navigator_goal_msg_t waypoints[];
    public byte sender;
    public int nonce;
 
    public navigator_goal_list_t()
    {
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0x34a796b12a0112d7L;
 
    public static final byte SENDER_YOUR_MOM = (byte) -1;
    public static final byte SENDER_WAYPOINT_TOOL = (byte) 1;
    public static final byte SENDER_VIEWER = (byte) 2;
    public static final byte SENDER_NAVIGATOR = (byte) 3;
    public static final byte SENDER_MISSION_PLANNER = (byte) 4;
    public static final byte SENDER_USAR = (byte) 5;
    public static final byte SENDER_MAX_VALID = (byte) 6;
    public static final byte SENDER_MOVIE_MISSION = (byte) 7;

    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(erlcm.navigator_goal_list_t.class))
            return 0L;
 
        classes.add(erlcm.navigator_goal_list_t.class);
        long hash = LCM_FINGERPRINT_BASE
             + erlcm.navigator_goal_msg_t._hashRecursive(classes)
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeLong(this.utime); 
 
        outs.writeInt(this.numWaypoints); 
 
        for (int a = 0; a < this.numWaypoints; a++) {
            this.waypoints[a]._encodeRecursive(outs); 
        }
 
        outs.writeByte(this.sender); 
 
        outs.writeInt(this.nonce); 
 
    }
 
    public navigator_goal_list_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public navigator_goal_list_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static erlcm.navigator_goal_list_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        erlcm.navigator_goal_list_t o = new erlcm.navigator_goal_list_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.utime = ins.readLong();
 
        this.numWaypoints = ins.readInt();
 
        this.waypoints = new erlcm.navigator_goal_msg_t[(int) numWaypoints];
        for (int a = 0; a < this.numWaypoints; a++) {
            this.waypoints[a] = erlcm.navigator_goal_msg_t._decodeRecursiveFactory(ins);
        }
 
        this.sender = ins.readByte();
 
        this.nonce = ins.readInt();
 
    }
 
    public erlcm.navigator_goal_list_t copy()
    {
        erlcm.navigator_goal_list_t outobj = new erlcm.navigator_goal_list_t();
        outobj.utime = this.utime;
 
        outobj.numWaypoints = this.numWaypoints;
 
        outobj.waypoints = new erlcm.navigator_goal_msg_t[(int) numWaypoints];
        for (int a = 0; a < this.numWaypoints; a++) {
            outobj.waypoints[a] = this.waypoints[a].copy();
        }
 
        outobj.sender = this.sender;
 
        outobj.nonce = this.nonce;
 
        return outobj;
    }
 
}

