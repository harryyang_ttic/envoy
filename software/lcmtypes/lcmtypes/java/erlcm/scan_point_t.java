/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package erlcm;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class scan_point_t implements lcm.lcm.LCMEncodable
{
    public double pos[];
 
    public scan_point_t()
    {
        pos = new double[2];
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0x65694d43e75564f9L;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(erlcm.scan_point_t.class))
            return 0L;
 
        classes.add(erlcm.scan_point_t.class);
        long hash = LCM_FINGERPRINT_BASE
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        for (int a = 0; a < 2; a++) {
            outs.writeDouble(this.pos[a]); 
        }
 
    }
 
    public scan_point_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public scan_point_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static erlcm.scan_point_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        erlcm.scan_point_t o = new erlcm.scan_point_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.pos = new double[(int) 2];
        for (int a = 0; a < 2; a++) {
            this.pos[a] = ins.readDouble();
        }
 
    }
 
    public erlcm.scan_point_t copy()
    {
        erlcm.scan_point_t outobj = new erlcm.scan_point_t();
        outobj.pos = new double[(int) 2];
        System.arraycopy(this.pos, 0, outobj.pos, 0, 2); 
        return outobj;
    }
 
}

