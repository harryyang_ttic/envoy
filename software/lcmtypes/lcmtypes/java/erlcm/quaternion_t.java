/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package erlcm;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class quaternion_t implements lcm.lcm.LCMEncodable
{
    public double q0;
    public double q1;
    public double q2;
    public double q3;
 
    public quaternion_t()
    {
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0x8fda7eba08cbec7bL;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(erlcm.quaternion_t.class))
            return 0L;
 
        classes.add(erlcm.quaternion_t.class);
        long hash = LCM_FINGERPRINT_BASE
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeDouble(this.q0); 
 
        outs.writeDouble(this.q1); 
 
        outs.writeDouble(this.q2); 
 
        outs.writeDouble(this.q3); 
 
    }
 
    public quaternion_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public quaternion_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static erlcm.quaternion_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        erlcm.quaternion_t o = new erlcm.quaternion_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.q0 = ins.readDouble();
 
        this.q1 = ins.readDouble();
 
        this.q2 = ins.readDouble();
 
        this.q3 = ins.readDouble();
 
    }
 
    public erlcm.quaternion_t copy()
    {
        erlcm.quaternion_t outobj = new erlcm.quaternion_t();
        outobj.q0 = this.q0;
 
        outobj.q1 = this.q1;
 
        outobj.q2 = this.q2;
 
        outobj.q3 = this.q3;
 
        return outobj;
    }
 
}

