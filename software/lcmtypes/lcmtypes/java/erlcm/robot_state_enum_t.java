package erlcm;
 
import java.io.*;
import java.util.*;
 
public final class robot_state_enum_t implements lcm.lcm.LCMEncodable
{
    public int value;
 
    public static final int UNDEFINED        = 0;
    public static final int OVERRIDE         = 1;
    public static final int STOP             = 2;
    public static final int STANDBY          = 3;
    public static final int RUN              = 4;
    public static final int ERROR            = 5;
    public static final int SHIFT            = 6;
 
    public robot_state_enum_t(int value) { this.value = value; }
 
    public int getValue() { return value; }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeInt(this.value);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public static erlcm.robot_state_enum_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        erlcm.robot_state_enum_t o = new erlcm.robot_state_enum_t(0);
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.value = ins.readInt();
    }
 
    public robot_state_enum_t(DataInput ins) throws IOException
    {
        long hash = ins.readLong();
        if (hash != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
        _decodeRecursive(ins);
    }
 
    public erlcm.robot_state_enum_t copy()
    {
        return new erlcm.robot_state_enum_t(this.value);
    }
 
    public static final long _hashRecursive(ArrayList<Class<?>> clss)
    {
        return LCM_FINGERPRINT;
    }
 
    public static final long LCM_FINGERPRINT = 0x2c9aee5cf5c5d31aL;
}
