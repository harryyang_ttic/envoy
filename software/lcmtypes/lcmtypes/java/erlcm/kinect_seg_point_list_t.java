/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package erlcm;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class kinect_seg_point_list_t implements lcm.lcm.LCMEncodable
{
    public long utime;
    public int no_points;
    public int no_elements;
    public float xyz[];
    public int no_clusters;
    public short cluster[];
 
    public kinect_seg_point_list_t()
    {
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0xdf60550336b22794L;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(erlcm.kinect_seg_point_list_t.class))
            return 0L;
 
        classes.add(erlcm.kinect_seg_point_list_t.class);
        long hash = LCM_FINGERPRINT_BASE
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeLong(this.utime); 
 
        outs.writeInt(this.no_points); 
 
        outs.writeInt(this.no_elements); 
 
        for (int a = 0; a < this.no_elements; a++) {
            outs.writeFloat(this.xyz[a]); 
        }
 
        outs.writeInt(this.no_clusters); 
 
        for (int a = 0; a < this.no_points; a++) {
            outs.writeShort(this.cluster[a]); 
        }
 
    }
 
    public kinect_seg_point_list_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public kinect_seg_point_list_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static erlcm.kinect_seg_point_list_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        erlcm.kinect_seg_point_list_t o = new erlcm.kinect_seg_point_list_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.utime = ins.readLong();
 
        this.no_points = ins.readInt();
 
        this.no_elements = ins.readInt();
 
        this.xyz = new float[(int) no_elements];
        for (int a = 0; a < this.no_elements; a++) {
            this.xyz[a] = ins.readFloat();
        }
 
        this.no_clusters = ins.readInt();
 
        this.cluster = new short[(int) no_points];
        for (int a = 0; a < this.no_points; a++) {
            this.cluster[a] = ins.readShort();
        }
 
    }
 
    public erlcm.kinect_seg_point_list_t copy()
    {
        erlcm.kinect_seg_point_list_t outobj = new erlcm.kinect_seg_point_list_t();
        outobj.utime = this.utime;
 
        outobj.no_points = this.no_points;
 
        outobj.no_elements = this.no_elements;
 
        outobj.xyz = new float[(int) no_elements];
        if (this.no_elements > 0)
            System.arraycopy(this.xyz, 0, outobj.xyz, 0, this.no_elements); 
        outobj.no_clusters = this.no_clusters;
 
        outobj.cluster = new short[(int) no_points];
        if (this.no_points > 0)
            System.arraycopy(this.cluster, 0, outobj.cluster, 0, this.no_points); 
        return outobj;
    }
 
}

