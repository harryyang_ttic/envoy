/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package erlcm;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class slam_matched_point_list_t implements lcm.lcm.LCMEncodable
{
    public long utime;
    public int node_id;
    public double tf_pos[];
    public double theta;
    public erlcm.full_scan_point_list_t points;
 
    public slam_matched_point_list_t()
    {
        tf_pos = new double[3];
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0xa4e230b842548164L;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(erlcm.slam_matched_point_list_t.class))
            return 0L;
 
        classes.add(erlcm.slam_matched_point_list_t.class);
        long hash = LCM_FINGERPRINT_BASE
             + erlcm.full_scan_point_list_t._hashRecursive(classes)
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeLong(this.utime); 
 
        outs.writeInt(this.node_id); 
 
        for (int a = 0; a < 3; a++) {
            outs.writeDouble(this.tf_pos[a]); 
        }
 
        outs.writeDouble(this.theta); 
 
        this.points._encodeRecursive(outs); 
 
    }
 
    public slam_matched_point_list_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public slam_matched_point_list_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static erlcm.slam_matched_point_list_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        erlcm.slam_matched_point_list_t o = new erlcm.slam_matched_point_list_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.utime = ins.readLong();
 
        this.node_id = ins.readInt();
 
        this.tf_pos = new double[(int) 3];
        for (int a = 0; a < 3; a++) {
            this.tf_pos[a] = ins.readDouble();
        }
 
        this.theta = ins.readDouble();
 
        this.points = erlcm.full_scan_point_list_t._decodeRecursiveFactory(ins);
 
    }
 
    public erlcm.slam_matched_point_list_t copy()
    {
        erlcm.slam_matched_point_list_t outobj = new erlcm.slam_matched_point_list_t();
        outobj.utime = this.utime;
 
        outobj.node_id = this.node_id;
 
        outobj.tf_pos = new double[(int) 3];
        System.arraycopy(this.tf_pos, 0, outobj.tf_pos, 0, 3); 
        outobj.theta = this.theta;
 
        outobj.points = this.points.copy();
 
        return outobj;
    }
 
}

