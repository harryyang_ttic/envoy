#include "template_stack.hpp"
#include <stdio.h>
#include "complex.hpp" 
#include <iostream>

using namespace std;

template<class T> class CRectangle {
    int x, y;
    T val;
public:
    void set_values (int,int);
    void set_class(T);
    int area () {return (x*y);}
};

template<class T> void CRectangle<T>::set_values (int a, int b) {
  x = a;
  y = b;
}

template<class T> void CRectangle<T>::set_class(T v){
    val = v;
    fprintf(stderr,"Class set\n");
}

template<class In, class Out> void copy(In from, In too_far, Out to){
    fprintf(stderr,"Down\n");
    
}

int main(int argc, char **argv){
    Stack<char> st(20);

    CRectangle<char> *rect = new CRectangle<char>();

    rect->set_values(10,2);
    char a = 'a';
    rect->set_class(a);
    
    try{
        while(true) st.push('c');
    }
    catch(Stack<char>::Overflow){
        fprintf(stderr,"Error : Overflow\n");
    }

    try{
        while(true) st.pop();
    }
    catch(Stack<char>::Underflow){
        fprintf(stderr,"Error : Underflow\n");
    }

    st.test(new complex());
    
    return 0;

}
