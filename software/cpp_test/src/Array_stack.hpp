#include "virtual_stack.hpp"

class Array_stack: public Stack{
    char *v;
    int top;
    int max_size; 

public: 
    Array_stack(int s){
        top = 0;
        if(s <0 || s > 10000) throw Bad_size();
        max_size = s;
        v = new char[s];
    }

    ~Array_stack(){
        delete[] v;
    }

    
    void push(char c){
        if(top==max_size) throw Overflow();
        v[top] = c;
        top++;
    }
    
    char pop(){
        if(top == 0) throw Underflow();
        top = top -1;
        return v[top];
    }
};
