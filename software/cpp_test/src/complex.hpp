class complex{
    double re, im;

public:
    complex(double r, double i) { re = r; im = i;}
    complex(double r){ re = r; im = 0;}
    complex(){re = 0; im = 0;}

    friend complex operator+(complex a1);//,complex a2);
    friend complex operator-(complex);//,complex);
    //friend complex operator-(complex);
    friend complex operator*(complex);//, complex);
    //friend complex operator/(complex , complex);
    
    friend complex operator==(complex , complex);
    friend complex operator!=(complex , complex);

    complex operator+(complex a1){
        return complex(re + a1.re, im + a1.im);
    }

    complex operator-(complex a1){//, complex a2){
        return complex(re - a1.re, im - a1.im);
    }

    complex operator*(complex a1){
        return complex(re*a1.re - im * a1.im, (im * a1.re + a1.im * re));
    }

    /*complex operator/(complex a1, complex a2){

        double denom = (a2.re * a2*re + a2.im * a2.im);
        return complex(a1.re * a2.re + a2.im * a1.im) / denom, 
            ( a2.re * a1.im - a2.im * a1.re)/ denom);
            }*/

    complex operator/(complex a2){

        double denom = (a2.re * a2.re + a2.im * a2.im);
        return complex((re * a2.re + a2.im * im) / denom, 
            ( a2.re * im - a2.im * re)/ denom);
            }

    void print(){
        fprintf(stderr, "Re : %f Im : %f\n", re, im);
    }
    
};
