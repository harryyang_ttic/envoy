//#include "stack_class.hpp"
#include "virtual_stack.hpp"
#include "List_stack.hpp"
#include "Array_stack.hpp"
#include <stdio.h>
#include <stdio.h>
#include "complex.hpp" 

/*void f(){
    Stack::push('c');
    if(Stack::pop() !='c'){
        //cout<<"Error";
        fprintf(stderr,"Error\n");
    } // error("impossible");
    }*/

int main(int argc, char **argv){
    //f();
    
    complex cp = complex(1,2.3);
    
    complex cp1 = 2.3;//complex(1.5,2.3);
    
    complex cp2 = cp + cp1;
    
    cp.print();
    cp1.print();
    cp2.print();

    cp2 = cp - cp1;
    cp2.print();


    cp2 = cp * cp1;
    cp2.print();

    cp2 = cp / cp1;
    cp2.print();

    //cp - complex();

    /*try{
        while(true) Stack::push('c');
    }
    catch(Stack::Overflow){
        fprintf(stderr,"Error : Overflow\n");
    }

    try{
        while(true) Stack::pop();
    }
    catch(Stack::Underflow){
        fprintf(stderr,"Error : Underflow\n");
        }*/

    Stack *st = new Array_stack(10);//Stack(10);
    //Stack *st = new List_stack(10);//Stack(10);

    try{
        while(true) st->push('c');
    }
    catch(Stack::Overflow){
        fprintf(stderr,"Error : Overflow\n");
    }

    try{
        while(true) st->pop();
    }
    catch(Stack::Underflow){
        fprintf(stderr,"Error : Underflow\n");
        }
    
    
    return 0;
}
