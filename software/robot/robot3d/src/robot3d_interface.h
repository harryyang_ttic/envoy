/*********************************************************
 *
 * This source code is part of the Carnegie Mellon Robot
 * Navigation Toolkit (CARMEN)
 *
 * CARMEN Copyright (c) 2002 Michael Montemerlo, Nicholas
 * Roy, Sebastian Thrun, Dirk Haehnel, Cyrill Stachniss,
 * and Jared Glover
 *
 * CARMEN is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation; 
 * either version 2 of the License, or (at your option)
 * any later version.
 *
 * CARMEN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General 
 * Public License along with CARMEN; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, 
 * Suite 330, Boston, MA  02111-1307 USA
 *
 ********************************************************/

/** @addtogroup robot librobot_interface **/
// @{

/** \file robot_interface.h
 * \brief Definition of the interface of the module robot.
 *
 * This file specifies the interface to subscribe the messages of
 * that module and to receive its data via ipc.
 **/

#ifndef CARMEN3D_ROBOT_INTERFACE_H
#define CARMEN3D_ROBOT_INTERFACE_H

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcm/lcm.h>

#ifdef __cplusplus
extern "C" {
#endif

    void lcm_carmen3d_robot_velocity_command(double tv, double rv, lcm_t* lcm);
    void 
    lcm_carmen3d_robot_velocity_const_accel_command(double tv, double rv, lcm_t* lcm);
    void lcm_carmen3d_robot_move_along_vector(double distance, double theta, lcm_t* lcm);
    void lcm_carmen3d_robot_follow_trajectory(erlcm_traj_point_t *trajectory, 
                                              int trajectory_length,
                                              erlcm_traj_point_t *robot, lcm_t* lcm);
#ifdef __cplusplus
}
#endif

#endif
// @}
