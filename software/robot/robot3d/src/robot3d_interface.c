/*********************************************************
 *
 * This source code is part of the Carnegie Mellon Robot
 * Navigation Toolkit (CARMEN)
 *
 * CARMEN Copyright (c) 2002 Michael Montemerlo, Nicholas
 * Roy, Sebastian Thrun, Dirk Haehnel, Cyrill Stachniss,
 * and Jared Glover
 *
 * CARMEN is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation; 
 * either version 2 of the License, or (at your option)
 * any later version.
 *
 * CARMEN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General 
 * Public License along with CARMEN; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, 
 * Suite 330, Boston, MA  02111-1307 USA
 *
 ********************************************************/

#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
#include "robot3d_interface.h"
void 
lcm_carmen3d_robot_velocity_command(double tv, double rv, lcm_t* lcm)
{

  erlcm_velocity_msg_t v;

  v.tv = tv;
  v.rv = rv;
  v.utime = bot_timestamp_now();//carmen_get_time()*1e6;
  erlcm_velocity_msg_t_publish(lcm,"ROBOT_VELOCITY_CMD",&v);
}

void 
lcm_carmen3d_robot_velocity_const_accel_command(double tv, double rv, lcm_t* lcm)
{

  erlcm_velocity_msg_t v;

  v.tv = tv;
  v.rv = rv;
  v.utime = bot_timestamp_now();//carmen_get_time()*1e6;
  //this will not shift the velocity
  erlcm_velocity_msg_t_publish(lcm,"ROBOT_VELOCITY_CMD_JS",&v);
}

void
lcm_carmen3d_robot_move_along_vector(double distance, double theta, lcm_t* lcm)
{
 
  erlcm_vector_msg_t msg;
  msg.distance = distance;
  msg.theta = theta;
  msg.utime = bot_timestamp_now();//carmen_get_time()* 1e6;
  erlcm_vector_msg_t_publish(lcm,"ROBOT_VECTOR_CMD",&msg);
}

//we need data that is recognizable by the lcm-gen (which is why we cant use carmen_traj_points
void
lcm_carmen3d_robot_follow_trajectory(erlcm_traj_point_t *trajectory, 
				   int trajectory_length,
				   erlcm_traj_point_t *robot, lcm_t* lcm)
{  
  erlcm_trajectory_msg_t msg;

  msg.trajectory = trajectory;
  msg.trajectory_length = trajectory_length;
  msg.robot_position = *robot;

  msg.utime = bot_timestamp_now();//carmen_get_time()*1e6;
  erlcm_trajectory_msg_t_publish(lcm,"ROBOT_TRAJ_CMD",&msg);
}

